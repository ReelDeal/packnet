﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using PackNet.BrDeviceDiscoveryService.DTO;
using PackNet.BrDeviceDiscoveryService.L10N;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Communication;
using PackNet.Communication.Communicators;
using PackNet.Communication.WebRequestCreator;
using SharpSnmpLib.Full.ForkedForBrDevices;
using SharpSnmpLib.Full.ForkedForBrDevices.BrCustomizationConstants;
using SharpSnmpLib.Full.ForkedForBrDevices.BrCustomizationMessages;

namespace PackNet.BrDeviceDiscoveryService
{
    [Export(typeof(IService))]
    public class BrDeviceDiscoveryService : IBrDeviceDiscoveryService
    {
        private const string ServiceName = "BrDeviceDiscoveryService";
        private const int Port = 80;
        private const int getPlceVersionIndividualTimeoutInSeconds = 4;
        private readonly ILogger logger;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly BrDeviceSnmpAdministrator brDeviceSnmpAdministrator;
        private SimplePLCCommunicator simplePlcCommunicator;

        public string Name
        {
            get { return ServiceName; }
        }

        [ImportingConstructor]
        public BrDeviceDiscoveryService(IServiceLocator serviceLocator, ILogger logger)
        {
            this.logger = logger;
            Stopwatch ctorStopWatch = Stopwatch.StartNew();
            logger.Log(LogLevel.Debug, Strings.BrDeviceDiscoveryService_BrDeviceDiscoveryService_Starting__BrDeviceDiscoveryService_ctor_);
            serviceLocator.RegisterAsService(this);
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            brDeviceSnmpAdministrator = new BrDeviceSnmpAdministrator();
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(BrDeviceDiscoveryMessages.DiscoverBrDevices);
            BrDeviceDiscoveryMessages.DiscoverBrDevices.OnMessage
            (
                subscriber, logger, message =>
                {
                    IList<ScanResult> scanResults = null;
                    
                    //Scan for machines first so we can better predict total run time for the part where we assign the PLC versions
                    var machineScanTask = Task.Factory.StartNew(() =>
                    {
                        //call the discovery method and wait for response
                        Stopwatch discoveryStopWatch = Stopwatch.StartNew();
                        logger.Log(LogLevel.Debug, "Calling brDeviceSnmpAdministrator.DiscoverDevices() and waiting for response.");
                        List<BrDeviceDiscovered> devices = brDeviceSnmpAdministrator.DiscoverDevices();
                        discoveryStopWatch.Stop();
                        logger.Log(LogLevel.Debug, "brDeviceSnmpAdministrator.DiscoverDevices() returned {0} devices in {1} ms.", devices.Count, discoveryStopWatch.ElapsedMilliseconds);

                        //parse the data from the response into a POCO DTO
                        logger.Log(LogLevel.Debug, "Parsing data from brDeviceSnmpAdministrator.DiscoverDevices() into POCO DTO.");
                        Stopwatch pocoDtoStopwatch = Stopwatch.StartNew();
                        scanResults = devices.Select(r => new ScanResult
                        {
                            IpV4Address = r.DeviceAttributes[BrDeviceOidEnumeration.IpV4Address],
                            MacAddress = r.DeviceAttributes[BrDeviceOidEnumeration.MacAddress],
                            IpV4SubnetMask = r.DeviceAttributes[BrDeviceOidEnumeration.IpV4SubnetMask],
                            PlcModule = r.DeviceAttributes[BrDeviceOidEnumeration.TargetType],
                            PlcVersion = GetPlcVersion(r.DeviceAttributes[BrDeviceOidEnumeration.IpV4Address]),
                            RuntimeVersion = r.DeviceAttributes[BrDeviceOidEnumeration.AutomationRuntimeVersion]
                        }).OrderBy(sr => sr.MacAddress).ToList();
                        pocoDtoStopwatch.Stop();
                        logger.Log(LogLevel.Debug, "Parsing data from brDeviceSnmpAdministrator.DiscoverDevices() into POCO DTO took {0} ms.", pocoDtoStopwatch.ElapsedMilliseconds);

                        //send off to the UI
                        logger.Log(LogLevel.Debug, "Sending POCO DTO results to the UI.");
                        Stopwatch sendPocoDtoResultsStopwatch = Stopwatch.StartNew();
                        uiCommunicationService.SendMessageToUI(new Message<List<ScanResult>>
                        {
                            MessageType = BrDeviceDiscoveryMessages.DiscoveredBrDevices,
                            Data = scanResults.ToList(),
                            ReplyTo = message.ReplyTo
                        });
                        logger.Log(LogLevel.Debug, "Sending POCO DTO results to the UI took {0} ms.", sendPocoDtoResultsStopwatch.ElapsedMilliseconds);
                    });
                    machineScanTask.Wait(TimeSpan.FromSeconds(10));
                    machineScanTask.ContinueWith(t =>
                    {
                        if (t.IsFaulted)
                        {
                            logger.Log(LogLevel.Error, "BrDeviceDiscoveryService ctor machineScanTask had a faulted thread.");

                            if (t.Exception != null)
                            {
                                logger.Log(LogLevel.Error, "BrDeviceDiscoveryService ctor machineScanTask faulted thread exception was {0}", t.Exception.ToString());
                            }
                        }
                    });

                    int getPlcVersionTotalTimeoutInSeconds = GetPlcVersionTotalTimeoutInSeconds(scanResults);

                    //Assign the PLC version stuff after the fact
                    var assignPlcVersionTask = Task.Factory.StartNew(() =>
                    {
                        if (scanResults != null)
                        {
                            foreach (var scanResult in scanResults)
                            {
                                scanResult.PlcVersion = GetPlcVersion(scanResult.IpV4Address);
                            }
                        }
                    });
                    assignPlcVersionTask.Wait(TimeSpan.FromSeconds(getPlcVersionTotalTimeoutInSeconds));
                    assignPlcVersionTask.ContinueWith(t =>
                    {
                        if (t.IsFaulted)
                        {
                            logger.Log(LogLevel.Error, "BrDeviceDiscoveryService ctor assignPlcVersionTask had a faulted thread.");

                            if (t.Exception != null)
                            {
                                logger.Log(LogLevel.Error, "BrDeviceDiscoveryService ctor assignPlcVersionTask faulted thread exception was {0}", t.Exception.ToString());
                            }
                        }
                    });
                });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SetIpVersion4Message>(BrDeviceDiscoveryMessages.IpVersion4InfoSetRequest);
            BrDeviceDiscoveryMessages.IpVersion4InfoSetRequest.OnMessage<SetIpVersion4Message>
            (
                subscriber, logger, SetIpVersion4Info
            );

            ctorStopWatch.Stop();
            logger.Log(LogLevel.Debug, "BrDeviceDiscoveryService ctor took {0} ms.", ctorStopWatch.ElapsedMilliseconds);
        }

        /// <summary>
        /// GetPlcVersion is an expensive operation. Give it 4 seconds per head plus one additional head and if we didn't have anything 10 seconds total so we can bail out.
        /// </summary>
        /// <param name="scanResults"></param>
        /// <returns></returns>
        private static int GetPlcVersionTotalTimeoutInSeconds(IList<ScanResult> scanResults)
        {
            int getPlcVersionTotalTimeoutInSeconds = (scanResults != null && scanResults.Count > 1)
                ? (scanResults.Count * getPlceVersionIndividualTimeoutInSeconds) + getPlceVersionIndividualTimeoutInSeconds
                : 10;
            return getPlcVersionTotalTimeoutInSeconds;
        }

        public void SetIpVersion4Info(IMessage<SetIpVersion4Message> message)
        {
            Stopwatch snmpStopwatch = Stopwatch.StartNew();
            SetIpVersion4Message setIpVersion4Message = message.Data;
            logger.Log(LogLevel.Debug, "SetIpVersion4Info called with: {0}", setIpVersion4Message.ToString());

            //This has been short-circuited for now for Staples launch per Jeff - Gateways are not reliably reachable via ping
            //bool isDefaultGatewayReachable = Networking.IsIpAddressReachable(setIpVersion4Message.IpV4DefaultGateway);

            //if (!isDefaultGatewayReachable)
            //{
            //    string defaultGatewayErrorMessage = string.Format("Set IP version 4 info failed. Default Gateway {0} is not reachable.", setIpVersion4Message.IpV4DefaultGateway);
            //    logger.Log(LogLevel.Error, defaultGatewayErrorMessage);
            //    uiCommunicationService.SendMessageToUI(new ResponseMessage<string>
            //    {
            //        MessageType = BrDeviceDiscoveryMessages.IpVersion4InfoSetResponse,
            //        ReplyTo = message.ReplyTo,
            //        Result = ResultTypes.Fail,
            //        Message = defaultGatewayErrorMessage,
            //        Data = setIpVersion4Message.MacAddress
            //    });
            //    return;
            //}

            //Try to do a temporary set over SNMP. If that fails then give an outright failure message and stop here - most likely there is no physical connection to the machine or the broadcast to MAC address of all ff's was not received
            bool snmpSuccess = SetIpVersion4InfoViaSnmp(setIpVersion4Message);
            snmpStopwatch.Stop();
            logger.Log(LogLevel.Debug, "SetIpVersion4InfoViaSnmp called with: {0} took {1} ms and the result was: {2}", setIpVersion4Message.ToString(), snmpStopwatch.ElapsedMilliseconds, snmpSuccess);

            if (!snmpSuccess)
            {
                string snmpErrorMessage = string.Format("SNMPError, {0}", setIpVersion4Message);
                logger.Log(LogLevel.Error, snmpErrorMessage);
                uiCommunicationService.SendMessageToUI(new ResponseMessage<string>
                {
                    MessageType = BrDeviceDiscoveryMessages.IpVersion4InfoSetResponse,
                    ReplyTo = message.ReplyTo,
                    Result = ResultTypes.Fail,
                    Message = snmpErrorMessage,
                    Data = setIpVersion4Message.MacAddress
                });
                return;
            }

            //If we got past that we can try the permanent IP set over the IP address
            SetIpVersion4InfoPermanently(message);
        }

        public void SetIpVersion4InfoPermanently(IMessage<SetIpVersion4Message> message)
        {
            Stopwatch plcStopwatch = Stopwatch.StartNew();
            SetIpVersion4Message setIpVersion4Message = message.Data;
            bool plcSuccess = SetIpVersion4InfoViaPlc(setIpVersion4Message);
            logger.Log(LogLevel.Debug, "SetIpVersion4InfoViaPlc called with: {0} took {1} ms and the result was: {2}", setIpVersion4Message.ToString(), plcStopwatch.ElapsedMilliseconds, plcSuccess);
            plcStopwatch.Stop();

            if (!plcSuccess)
            {
                string plcErrorMessageForLog = string.Format("Temporary IP address set using PLC over HTTP/REST failed. Please ensure that the machine is pingable. The data sent during attempt was: {0}", setIpVersion4Message);
                logger.Log(LogLevel.Error, plcErrorMessageForLog, setIpVersion4Message);
                Stopwatch sendPlcErrorStopwatch = Stopwatch.StartNew();
                uiCommunicationService.SendMessageToUI(new ResponseMessage<string>
                {
                    MessageType = BrDeviceDiscoveryMessages.IpVersion4InfoSetResponse,
                    ReplyTo = message.ReplyTo,
                    //We mark this as a PartialSuccess and need to warn them in the UI that permanent set was not possible
                    Result = ResultTypes.PartialSuccess,
                    Message = "PLCError",
                    Data = message.Data.MacAddress
                });
                logger.Log(LogLevel.Error, "Sending PLC failed message to UI for {0} took {1} ms", setIpVersion4Message.ToString(), sendPlcErrorStopwatch.ElapsedMilliseconds);
                return;
            }

            logger.Log(LogLevel.Debug, "Sending PLC succeeded message to UI for {0}", setIpVersion4Message.ToString());
            Stopwatch sendPlcSuccessStopwatch = Stopwatch.StartNew();
            uiCommunicationService.SendMessageToUI(new ResponseMessage<string>
            {
                MessageType = BrDeviceDiscoveryMessages.IpVersion4InfoSetResponse,
                ReplyTo = message.ReplyTo,
                Result = ResultTypes.Success,
                Data = message.Data.MacAddress
            });
            logger.Log(LogLevel.Debug, "Sending PLC succeeded message to UI for {0} took {1} ms", setIpVersion4Message.ToString(), sendPlcSuccessStopwatch.ElapsedMilliseconds);
        }

        private bool SetIpVersion4InfoViaSnmp(SetIpVersion4Message setIpVersion4Message)
        {
            bool result = false;

            var task = Task.Factory.StartNew(
            () =>
            {
                logger.Log(LogLevel.Debug, "SetIpVersion4InfoViaSnmp called with {0} and spawning child thread.", setIpVersion4Message);
                Stopwatch snmpStopWatch = Stopwatch.StartNew();
                try
                {
                    brDeviceSnmpAdministrator.SetIpVersion4Info(setIpVersion4Message);
                    result = true;
                }
                catch
                {
                    result = false;
                }
                logger.Log(LogLevel.Debug, "SetIpVersion4InfoViaSnmp called with {0} and spawning child thread took {1} ms.", setIpVersion4Message, snmpStopWatch.ElapsedMilliseconds);
            });

            task.ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    logger.Log(LogLevel.Error, "SetIpVersion4InfoViaSnmp thread faulted. The data sent in was: {0}", setIpVersion4Message);

                    if (t.Exception != null)
                    {
                        logger.Log(LogLevel.Error, string.Format("SetIpVersion4InfoViaSnmp thread fault was: {0}", t.Exception.Message));
                    }
                }
            });

            //This Set via SNMP is very quick and should NEVER take more than 10 seconds.
            task.Wait(TimeSpan.FromSeconds(10));
            
            return result;
        }

        private bool SetIpVersion4InfoViaPlc(SetIpVersion4Message setIpVersion4Message)
        {
            bool result = false;

            var task = Task.Factory.StartNew(
            () =>
            {
                logger.Log(LogLevel.Debug, "SetIpVersion4InfoViaPlc called with {0} and spawning child thread.", setIpVersion4Message);
                Stopwatch plcStopwatch = Stopwatch.StartNew();

                Stopwatch plcStopwatch2 = Stopwatch.StartNew();
                try
                {
                    simplePlcCommunicator = new SimplePLCCommunicator(new WebRequestCreator(setIpVersion4Message.IpV4Address, Port), logger);
                    simplePlcCommunicator.WriteValue(PacksizeBaseMachineVariableMap.Instance.DHCP, "False");
                    simplePlcCommunicator.WriteValue(PacksizeBaseMachineVariableMap.Instance.StaticIp, setIpVersion4Message.IpV4Address);
                    simplePlcCommunicator.WriteValue(PacksizeBaseMachineVariableMap.Instance.SubnetMask, setIpVersion4Message.IpV4SubnetMask);
                    simplePlcCommunicator.WriteValue(PacksizeBaseMachineVariableMap.Instance.DefaultGateway, setIpVersion4Message.IpV4DefaultGateway);
                    result = true;
                }
                catch (Exception exception)
                {
                    plcStopwatch2.Stop();
                    logger.Log(LogLevel.Error, "Error in SetIpVersion4InfoViaPlc which took {0} ms: {1}", plcStopwatch2.ElapsedMilliseconds, exception.ToString());
                }

                Stopwatch plcStopwatch3 = Stopwatch.StartNew();
                try
                {
                    simplePlcCommunicator.WriteValue(PacksizeBaseMachineVariableMap.Instance.ExecuteNetworkSettings, true);
                }
                catch(Exception exception)
                {
                    //Gulp the last exception since setting the execute network bit may cause cyclic to respond back slower than the post response
                    plcStopwatch3.Stop();
                    logger.Log(LogLevel.Error, "Gulping error from SetIpVersion4InfoViaPlc execute network bit set which is most likely innocuous that took {0}: {1}", plcStopwatch3.ElapsedMilliseconds, exception.ToString());
                }

                logger.Log(LogLevel.Debug, "SetIpVersion4InfoViaPlc called with {0} and spawning child thread took {1} ms total.", setIpVersion4Message, plcStopwatch.ElapsedMilliseconds);
            });

            task.ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    logger.Log(LogLevel.Error, "SetIpVersion4InfoViaSnmp thread faulted.");

                    if (t.Exception != null)
                    {
                        logger.Log(LogLevel.Error, string.Format("Error: Failed to Set IP info. Message from the fault was: {0}", t.Exception.Message));
                    }
                }
            });
            
            //This Set via PLC could take _a while_ so give it 30 seconds to timeout.
            task.Wait(TimeSpan.FromSeconds(30));
            return result;
        }

        private string GetPlcVersion(string ipAddress)
        {
            string result = "Not available";

            if (string.IsNullOrWhiteSpace(ipAddress))
            {
                return result;
            }

            Task getPlcVersionTask = Task.Factory.StartNew(() =>
            {
                simplePlcCommunicator = new SimplePLCCommunicator(new WebRequestCreator(ipAddress, Port), logger);
                Stopwatch stopwatch = Stopwatch.StartNew();
                string message;

                try
                {
                    result = simplePlcCommunicator.ReadValue<string>(PacksizeBaseMachineVariableMap.Instance.MachinePlcVersion);
                    logger.Log(LogLevel.Debug, "GetPlcVersion for '{0}' port '{1}' succeeded.", ipAddress, Port);
                }
                catch (MissingVariableException missingVariableException)
                {
                    //PLEASE - Don't remove this handling for MissingVariableException - It will tell you when PLC variables can't be read because the PLC and PackNet code are out of sync
                    //in terms of variable names
                    string missingVariableError =
                        string.Format(
                            "ERROR: THE MACHINE WITH IP address '{0}' HAS PLC and PACKNET CODE OUT OF SYNC. The missing variable was '{1}'.",
                            ipAddress, PacksizeBaseMachineVariableMap.Instance.MachinePlcVersion);
                    logger.Log(LogLevel.Error, missingVariableError, missingVariableException);
                    //DON'T THROW HERE - other results could be valid on the other machines if they have correct versions of PLC and we should return those.
                    Console.Write(missingVariableError + "Check log for details.");
                    Console.WriteLine(missingVariableException);
                }
                catch (Exception exception)
                {
                    logger.LogException(LogLevel.Error,
                        string.Format("GetPlcVersion in BrDeviceDiscoveryService had an error talking to '{0}' port '{1}'",
                            ipAddress,
                            Port), exception);
                }
                stopwatch.Stop();
                message = string.Format("GetPlcVersion for '{0}' port '{1}' took '{2}' ms.", ipAddress, Port, stopwatch.ElapsedMilliseconds);
                logger.Log(LogLevel.Debug, message);
            });
            //Give up after a few seconds or we will be here foerever if there is an unknown or unstamped PLC in the environment as we iterate over each machine
            getPlcVersionTask.Wait(TimeSpan.FromSeconds(getPlceVersionIndividualTimeoutInSeconds));
            getPlcVersionTask.ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    logger.Log(LogLevel.Error, "GetPlcVersion thread faulted.");

                    if (t.Exception != null)
                    {
                        logger.Log(LogLevel.Error, "GetPlcVersion thread fault exception was: {0}", t.Exception.Message);
                    }
                }
            });
            return result;
        }

        public void Dispose()
        {
            if (logger != null)
            {
                logger.Dispose();
            }

            if (uiCommunicationService != null)
            {
                uiCommunicationService.Dispose();
            }

            if (subscriber != null)
            {
                subscriber.Dispose();
            }

            if (brDeviceSnmpAdministrator != null)
            {
                brDeviceSnmpAdministrator.Dispose();
            }

            if (simplePlcCommunicator != null)
            {
                simplePlcCommunicator.Dispose();
            }
        }
    }
}