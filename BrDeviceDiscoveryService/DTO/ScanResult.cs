﻿namespace PackNet.BrDeviceDiscoveryService.DTO
{
    public class ScanResult
    {
        public string MacAddress { get; set; }
        public string IpV4Address { get; set; }
        public string IpV4SubnetMask { get; set; }
        public string PlcModule { get; set; }
        public string PlcVersion { get; set; }
        public string RuntimeVersion { get; set; }
    }
}