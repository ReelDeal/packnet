﻿using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Services;
using SharpSnmpLib.Full.ForkedForBrDevices.BrCustomizationMessages;

namespace PackNet.BrDeviceDiscoveryService
{
    public interface IBrDeviceDiscoveryService : IService
    {
        void SetIpVersion4Info(IMessage<SetIpVersion4Message> setIpVersion4Message);
    }
}