﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.BrDeviceDiscoveryService
{
    public sealed class BrDeviceDiscoveryMessages : MessageTypes
    {
        private BrDeviceDiscoveryMessages(string name) : base(name) { }
         
        //Commands
        public static readonly BrDeviceDiscoveryMessages DiscoverBrDevices = new BrDeviceDiscoveryMessages("DiscoverBrDevices");
        
        //Discovery Responses
        public static readonly BrDeviceDiscoveryMessages DiscoveredBrDevices = new BrDeviceDiscoveryMessages("DiscoveredBrDevices");

        //Set IP v4 Info Request                                                                                 
        public static readonly BrDeviceDiscoveryMessages IpVersion4InfoSetRequest = new BrDeviceDiscoveryMessages("IpVersion4InfoSetRequest");
        
        //Set IP v4 Info Response
        public static readonly BrDeviceDiscoveryMessages IpVersion4InfoSetResponse = new BrDeviceDiscoveryMessages("IpVersion4InfoSetResponse");
    }
}