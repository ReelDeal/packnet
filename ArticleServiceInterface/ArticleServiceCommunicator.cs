﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Newtonsoft.Json.Linq;
using Packsize.ApplicationLogging;
using Packsize.MachineManager.RabbitCommunication;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Packsize.Packnet.ArticleServiceCommunicator
{
    using global::Packnet.Common.Interfaces;
    using global::Packnet.Common.Interfaces.DTO;

    /// <summary>
    /// This service will use a rabbitMq client to make requests to the article service and respond
    /// </summary>
    [Obsolete]
    public class ArticleServiceCommunicator : IArticleServiceCommunicator
    {
        // transaction id, delegate<success, orderId, number of cartons created>
        private IRabbitPublisher rabbitPublisher;

        private string replyQueueName;
        private QueueingBasicConsumer consumer;
        private ILogger logger;

        public ArticleServiceCommunicator(IRabbitPublisher publisher, ILogger logger)
        {
            this.rabbitPublisher = publisher;
            this.logger = logger;

            var channel = ((RabbitQueueing)this.rabbitPublisher).Channel;
            if (!channel.IsOpen)
                throw new ApplicationException(string.Format("IArticleServiceCommunicator connection is not available. Host:{0} Exchange:{1} "
                    , ((RabbitQueueing)this.rabbitPublisher).HostName, ((RabbitQueueing)this.rabbitPublisher).ExchangeName));


            replyQueueName = channel.QueueDeclare("dotServerArticleServiceQueue", false, false, true, null).QueueName;

            channel.QueueBind(replyQueueName, ((RabbitQueueing)this.rabbitPublisher).ExchangeName, "article-service.replys." + replyQueueName + ".Articles");
            consumer = new QueueingBasicConsumer(channel);
            // needs work to get the channel working
            channel.BasicConsume(replyQueueName, true, consumer);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        /// <exception cref="AmbiguousMatchException">When service returns more than one article.</exception>
        public Article GetArticle(string articleId)
        {
            Article article = null;
            string serviceReturnString = string.Empty;

            try
            {
                // request the article info.
                var request = "{\"Fact\":{\"Type\":\"DataRequest\",\"Data\":{\"ReplyQueue\":\"article-service.replys." +
                              replyQueueName + "\",\"Type\":\"Articles\",\"Filter\":{\"SearchValue\":\"" + articleId +
                              "\",\"SearchColumn\":\"ArticleId\",\"SortBy\":\"ArticleId\",\"SortOrder\":\"asc\",\"ChunkSize\":60,\"Chunk\":0}}}}";
                this.rabbitPublisher.PublishJson(request, "article-service.clients.facts", null);

                //Parse the article data.
                var delivery = (BasicDeliverEventArgs) consumer.Queue.Dequeue();
                serviceReturnString = Encoding.ASCII.GetString(delivery.Body);
                this.logger.Log(LogLevel.Trace, string.Format("Article service result:{0}", serviceReturnString));
                /*
                 * {"metadata":{"totalRecordsFound":4,"chunkSize":60,"chunk":0},
                 *  "data":[
                 *      {"LastUpdated":"10\/21\/2013 12:39:58 PM","UserName":"PackAdmin","ProducibleMachines":[704284,60722],"ArticleId":"2444","Description":"MUFFLER","Height":"6.75","MachineType":"","CorrugateQuality":"","Length":"27.75","Id":"ab1654ca-4783-4559-9202-8c9f55ab68fe","Width":"13.63","DesignId":"12"},
                 *      {"LastUpdated":"2013-10-21T12:35:01-06:00","UserName":"qa","ProducibleMachines":[704284,60722],"ArticleId":"444","Description":null,"Height":6,"MachineType":null,"CorrugateQuality":"","Length":12,"Id":"1fd0cf4a-27fb-41ea-ac56-895a8ec2faa3","Width":6,"DesignId":12},
                 *      {"LastUpdated":"2013-10-21T13:19:12-06:00","UserName":"qa","ProducibleMachines":[704284,60722],"ArticleId":"642444","Description":"","Height":4.75,"MachineType":"","CorrugateQuality":"","Length":25.5,"Id":"20dd20e5-ba2e-4c07-8982-a063833815a6","Width":10.5,"DesignId":12},
                 *      {"LastUpdated":"10\/21\/2013 12:39:58 PM","UserName":"PackAdmin","ProducibleMachines":[704284,60722],"ArticleId":"700444","Description":"MUFFLER","Height":"7.75","MachineType":"","CorrugateQuality":"","Length":"25.63","Id":"04365cc6-33cf-478f-95be-0a4bdce775d4","Width":"9.87","DesignId":"12"}]} 
                */
                var jsonResult = JObject.Parse(serviceReturnString);
                JToken dataList;
                if (jsonResult.TryGetValue("data", out dataList))
                    if (dataList.HasValues)
                    {
                        int x = 0;
                        var first = dataList[x++];
                        while (first != null && first["ArticleId"].Value<string>() != articleId)
                        {
                            this.logger.Log(LogLevel.Trace, string.Format("Article {0} found, does not match request {1}.  checking next...", first["ArticleId"].Value<string>(), articleId));
                            try
                            {
                                first = dataList[x++];
                            }
                            catch (Exception)
                            {
                                // do nothing because we've hit the end.
                                first = null;
                            }
                        }

                        if (first == null)
                        {
                            this.logger.Log(LogLevel.Trace, string.Format("Article {0} not found, returning null", articleId));
                            return null;
                        }

                        article = new Article();  // do not run object initiator because if an error occurs we want to be able to see which line.
                        article.LastUpdated = first["LastUpdated"].Value<DateTime>();
                        article.MachineType = first["MachineType"].HasValues ? first["MachineType"].Value<string>() : string.Empty;
                        article.CorrugateQuality = first["CorrugateQuality"].HasValues ? first["CorrugateQuality"].Value<int?>() : null;
                        article.Height = first["Height"].Value<double>();
                        article.Width = first["Width"].Value<double>();
                        article.Description = first["Description"].Value<string>();
                        article.DesignId = first["DesignId"].Value<int>();
                        article.ArticleId = first["ArticleId"].Value<string>();
                        article.Length = first["Length"].Value<double>();
                    }
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, string.Format("Call to GetArticle({0}) failed. ServiceResponse:{1}", articleId, serviceReturnString), e);
                throw;
            }

            return article;
           
        }

    }
}
