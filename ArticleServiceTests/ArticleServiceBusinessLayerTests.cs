﻿namespace ArticleServiceTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.Reactive.Testing;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MongoDB.Driver;
    using Moq;
    using PackNet.Common.Eventing;    
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Eventing;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Interfaces.Producible;    
    using PackNet.Common.Interfaces.Services;
    using PackNet.Plugin.ArticleService.Data;
    using PackNet.Plugin.ArticleService.Enums;
    using Testing.Specificity;
    using PackNet.Plugin.ArticleService;
    using PackNet.Plugin.ArticleService.BusinessLayer;
    using TestUtils;

    using IArticleRepository = PackNet.Plugin.ArticleService.Data.IArticleRepository;

    [TestClass]
    public class ArticleServiceBusinessLayerTests
    {
        #region create
        [TestMethod]
        [TestCategory("Unit")]
        public void CallingCreateArticle_ShouldCallCreateInRepository_AndHaveASuccessResult()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();
            
            var result = bl.Create(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success, "Create failed");
            repoMock.Verify(r => r.CreateEnsureNoDuplicate(It.Is<Article<IProducible>>(a => a.Equals(article))), Times.Once(), "Create in repo was never called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingCreateArticle_WithADuplicateKey_ShouldFailToCreateArticle_WithArticleAliasAlreadyInUseStatus()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();
            
            var result = bl.Create(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success);
            repoMock.Setup(r => r.CreateEnsureNoDuplicate(article)).Throws(new MongoDuplicateKeyException("Duplicate key", new WriteConcernResult(null)));
            result = bl.Create(article);
            Specify.That(result).Should.BeEqualTo(ArticleServiceResultTypes.ArticleAliasAlreadyInUse);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingCreateArticle_AndRepoThrowsException_ShouldFailToCreateArticle_WithFailStatus()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();

            repoMock.Setup(r => r.CreateEnsureNoDuplicate(article)).Throws(new Exception());
            var result = bl.Create(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Fail);
        }
        #endregion

        #region update
        [TestMethod]
        [TestCategory("Unit")]
        public void CallingUpdateArticle_ShouldCalUpdateInRepository_AndHaveASuccessResult()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();

            var result = bl.Update(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success, "Update failed");
            repoMock.Verify(r => r.Update(It.Is<Article<IProducible>>(a => a.Equals(article))), Times.Once(), "Update in repo was never called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingUpdateArticle_AndRepoThrowsDuplicateKeyException_ShouldFailToUpdateArticleWithFailStatus()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();

            var result = bl.Update(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success);
            repoMock.Setup(r => r.Update(article)).Throws(new MongoDuplicateKeyException("Duplicate key", new WriteConcernResult(null)));
            result = bl.Update(article);
            Specify.That(result).Should.BeEqualTo(ArticleServiceResultTypes.ArticleAliasAlreadyInUse);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingUpdateArticle_AndRepoThrowsException_ShouldFailToUpdateArticleWithFailStatus()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();

            repoMock.Setup(r => r.Update(article)).Throws(new Exception());
            var result = bl.Update(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Fail);
        }
        #endregion

        #region find

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingFindArticle_ShouldCalFindInRepository_AndReturnArticle()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();
            bl.Find(article.Id);
            repoMock.Verify(r => r.Find(It.Is<Guid>(a => a.Equals(article.Id))), Times.Once(), "Find in repo was never called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingFindOnMultipleExistingArticles_ReturnsAllArticles()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var articles = new List<Article<IProducible>>();
            for (var i = 0; i < 10; i++)
            {
                articles.Add(ArticleTestHelper.CreateArticleWithSingleLabel());
            }
            var articleIds = articles.Select(a => a.Id);

            bl.Find(articleIds);
            repoMock.Verify(r => r.Find(It.Is<IEnumerable<Guid>>(a => a.Equals(articleIds))), Times.Once(), "Find in repo was never called");
        }

        #endregion

        #region search

        [TestMethod]
        [TestCategory("Unit")]
        public void SearchingWithEmptyString_ReturnsAllAvailableArticles()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);

            bl.Search(string.Empty);
            repoMock.Verify(r => r.All(), Times.Once(), "All in repo was never called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SearchingWithNonEmptyString_ReturnsMatchingArticles()
        {
            const string pattern = "1";

            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);

            bl.Search(pattern);
            repoMock.Verify(r => r.Search(It.Is<string>(s => s.Equals(pattern)), It.IsAny<int>()), Times.Once(), "All in repo was never called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingFindByAlias_ReturnsMatchingArticle()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);

            var art = ArticleTestHelper.CreateArticleWithSingleLabel();

            bl.FindByAlias(art.Alias);
            repoMock.Verify(r => r.FindByAlias(It.Is<string>(s => s.Equals(art.Alias))), Times.Once(), "FindByAlias in repo was never called");
        }

        #endregion

        #region delete

        [TestMethod]
        [TestCategory("Unit")]
        public void DeletingAnArticle_ShoudReturnSuccessStatus()
        {
            var eventAggregator = new EventAggregator();
            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;
            testScheduler.AdvanceTo(DateTime.Now.Ticks);
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();

            var result = bl.Delete(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success, "Delete failed");
            repoMock.Verify(r => r.Delete(It.Is<Article<IProducible>>(a => a.Equals(article))), Times.Once(), "Delete in repo was never called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingDeleteMultipleArticles_ShouldReturnSuccessStatus()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);

            var articles = new List<Article<IProducible>>();
            for (var i = 0; i < 10; i++)
            {
                articles.Add(ArticleTestHelper.CreateArticleWithSingleLabel());
            }

            var result = bl.Delete(articles);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void DeletingANullArticle_ShoudReturn_FailStatus()
        {
            var eventAggregator = new EventAggregator();
            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;
            testScheduler.AdvanceTo(DateTime.Now.Ticks);
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            Article<IProducible> article = null;

            var result = bl.Delete(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Fail, "Delete succeded but should have failed");
            repoMock.Verify(r => r.Delete(It.Is<Article<IProducible>>(a => a.Equals(article))), Times.Never(), "Delete in repo was called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingDeleteArticle_AndRepoThrowsException_ShouldFailToDeleteArticle_WithFailStatus()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);
            var article = ArticleTestHelper.CreateArticleWithSingleLabel();

            repoMock.Setup(r => r.Delete(article)).Throws(new Exception());
            var result = bl.Delete(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Fail);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CallingDeleteMultipleArticles_AndRepoThrowsException_ShouldFailToDeleteArticles_WithFailStatus()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator);

            var articles = new List<Article<IProducible>>();
            for (var i = 0; i < 10; i++)
            {
                articles.Add(ArticleTestHelper.CreateArticleWithSingleLabel());
            }

            repoMock.Setup(r => r.Delete(articles)).Throws(new Exception());
            var result = bl.Delete(articles);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Fail);
        }
        #endregion

        #region helpers

        public static ArticleServiceBusinessLayer SetupArticleServiceBL(IArticleRepository repo, EventAggregator eventAggregator, IUICommunicationService uiCommService = null, IUserNotificationService userNotificationService = null)
        {
            var configMock = new Mock<IArticleConfigurationRepository>();
            configMock.Setup(m => m.CurrentServiceConfiguration).Returns(new ArticleServiceConfiguration());
            return SetupArticleServiceBL(repo, eventAggregator, configMock.Object, uiCommService, userNotificationService);
        }

        public static ArticleServiceBusinessLayer SetupArticleServiceBL(IArticleRepository repo, EventAggregator eventAggregator, IArticleConfigurationRepository configRepo, IUICommunicationService uiCommService = null, IUserNotificationService userNotificationService = null)
        {
            var serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(s => s.Locate<ILogger>()).Returns(new ConsoleLogger());
            serviceLocatorMock.Setup(s => s.Locate<IEventAggregatorPublisher>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(s => s.Locate<IEventAggregatorSubscriber>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(s => s.Locate<IUICommunicationService>()).Returns(uiCommService);
            serviceLocatorMock.Setup(s => s.Locate<IUserNotificationService>()).Returns(userNotificationService);
            var bl = new ArticleServiceBusinessLayer(repo, serviceLocatorMock.Object, configRepo);
            Specify.That(bl).Should.Not.BeNull("BL was null after creating it");

            return bl;
        }

        
        #endregion
    }

}
