﻿using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Plugin.ArticleService;

namespace ArticleServiceTests
{
    public static class ArticleTestHelper
    {
        public static Article<IProducible> CreateArticleWithSingleCartonWithQuantityOf1()
        {
            var kit = new Kit();
            kit.AddProducible(new Order(new Carton
            {
                CustomerUniqueId = "Single Carton",
                Length = 10,
                Width = 11,
                Height = 12,
                DesignId = 201221
            }, 1));

            return new Article<IProducible>
            {
                Alias = "Single Carton Article",
                Producible = kit,
            };
        }

        public static Article<IProducible> CreateArticleWithSingleCartonWithQuantityOf2()
        {
            var kit = new Kit { CustomerUniqueId = "Multi Carton" };
            kit.AddProducible(new Order(new Carton
                {
                    CustomerUniqueId = "Multi Carton",
                    Length = 10,
                    Width = 11,
                    Height = 12,
                    DesignId = 201221
                }, 2));
            return new Article<IProducible>
            {
                Alias = "Single Carton Article",
                Producible = kit,
            };
        }

        public static Article<IProducible> CreateArticleWithKitCartonCartonLabel()
        {
            var kit = new Kit();

            kit.AddProducible(new Order(new Carton
            {
                CustomerUniqueId = "kitItem1",
                Length = 10,
                Width = 11,
                Height = 12,
                DesignId = 201221
            }, 1));
            kit.AddProducible(new Order(new Carton
            {
                CustomerUniqueId = "kitItem2",
                Length = 5,
                Width = 10,
                Height = 20,
                DesignId = 3333
            }, 2));
            kit.AddProducible(new Order(new Label
            {
                CustomerUniqueId = "kitItem3"
            }, 3));
            return new Article<IProducible>
            {
                Alias = "Kit Article",
                Producible = kit,
            };
        }
        public static Article<IProducible> CreateArticleWithSingleLabel(int counter = 1)
        {
            var kit = new Kit();
            kit.AddProducible(new Order(new Label()
                    {
                        CustomerDescription = "xDescriptioNx" + "(" + counter + ")",
                        CustomerUniqueId = "LabelForMyBox" + counter,
                    }, 1));
            return new Article<IProducible>()
            {
                Alias = "Article" + counter,
                Description = "I have a description!" + "(" + counter + ")",
                Producible = kit
            };
        }

        public static Article<IProducible> CreateArticleWithKitCartonsAndLabel(int counter = 1)
        {
            var kit = new Kit
            {
                CustomerUniqueId = "dummy",
            };
            kit.AddProducible(new Order(new Label()
            {
                CustomerDescription = "xDescriptioNx" + "(" + counter + ")",
                CustomerUniqueId = "LabelForMyBox" + counter,
            }, 1));
            var carton = new Carton
            {
                CustomerUniqueId = "bob",
                Length = 10,
                Width = 11,
                Height = 12,
                DesignId = 201110
            };
            kit.AddProducible(new Order(carton, 2));
            return new Article<IProducible>()
            {
                Alias = "Article" + counter,
                Description = "I have a description!" + "(" + counter + ")",
                Producible = kit,
            };
        }

    }
}
