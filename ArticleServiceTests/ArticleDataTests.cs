﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Plugin.ArticleService;
using PackNet.Plugin.ArticleService.Data;
using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Utils;

namespace ArticleServiceTests
{

    using Testing.Specificity;

    [TestClass]
    public class ArticleDataTests
    {
        [TestInitialize]
        public void Setup()
        {
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<string>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<MustProduceOnCorrugateWithPropertiesRestriction>(new RestrictionDiscriminatorConvention());
        }

#if DEBUG
        [TestMethod]
        public void SeedTestArticlesOnlyForUserWhileTestingCommentOutOtherwise()
        {
            
            var articleOfCarton = new Article<IProducible> { Alias = "test article of carton 1" };

            var carton = new Carton
            {
                Id = Guid.NewGuid(),
                CustomerDescription = articleOfCarton.Description,
                CustomerUniqueId = articleOfCarton.Alias,
                Width = 5,
                Height = 15,
                DesignId = 19491198
            };
            carton.Restrictions.Add(new MustProduceOnCorrugateWithPropertiesRestriction { Quality = 41 });
            carton.XValues.Add("Robocoprah", 1.23);
            carton.XValues.Add("Winfrey", 441);
            articleOfCarton.Producible = new Order(carton, 10);

            var repository = new ArticleRepository();
            repository.DeleteAll();
            repository.Create(articleOfCarton);

            articleOfCarton = repository.FindByAlias(articleOfCarton.Alias);
            Specify.That(articleOfCarton).Should.Not.BeNull();

        }
#endif

        [TestMethod]
        [ExpectedException(typeof(MongoDB.Driver.MongoDuplicateKeyException))]
        public void ShouldNotAllowCreateToDuplicateArticleAlias()
        {
            var label = new Label
            {
                Id = Guid.NewGuid(),
                PrintData = new Dictionary<string, string>
                {
                    { "test", "test" }
                }
            };

            label.Restrictions.Add(new BasicRestriction<string>("Test Restriction"));
            label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            var testAlias = "testing dup 1";
            var subject = new Article<IProducible> { Alias = testAlias };
            subject.Producible = label;

            var repository = new ArticleRepository();
            repository.Create(subject);
            var result = repository.FindByAlias(subject.Alias);
            Specify.That(result).Should.Not.BeNull();

            var subject2 = new Article<IProducible> { Alias = testAlias };
            subject2.Producible = label;
            try
            {
                repository.Create(subject2);
                //Assert.Fail("Should not allow duplicate Alias.");
                Specify.That(repository.Search(testAlias).Count()).Should.BeEqualTo(1);

            }
            finally
            {

                //Delete and Cleanup
                repository.Delete(subject.Id);
                result = repository.Find(subject.Id);
                Specify.That(result).Should.BeNull();

                repository.Delete(subject2.Id);
                result = repository.Find(subject2.Id);
                Specify.That(result).Should.BeNull();
            }
        }

        [TestMethod]
        [ExpectedException(typeof(MongoDB.Driver.MongoDuplicateKeyException))]
        public void ShouldNotAllowUpdateToDuplicateArticleAlias()
        {
            var label = new Label
            {
                Id = Guid.NewGuid(),
                PrintData = new Dictionary<string, string>
                {
                    { "test", "test" }
                }
            };

            label.Restrictions.Add(new BasicRestriction<string>("Test Restriction"));
            label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var subject = new Article<IProducible> { Alias = "testing dup 1" };
            subject.Producible = label;

            var repository = new ArticleRepository();
            repository.Create(subject);
            var result = repository.FindByAlias(subject.Alias);
            Specify.That(result).Should.Not.BeNull();

            var subject2 = new Article<IProducible> { Alias = "testing dup 2" };
            subject2.Producible = label;
            repository.Create(subject2);

            //now try to update subject
            try
            {
                subject.Alias = subject2.Alias;
                repository.Update(subject);
                Assert.Fail("Duplicate key should not be allowed");
            }
            finally
            {

                //Delete and Cleanup
                repository.Delete(subject.Id);
                result = repository.Find(subject.Id);
                Specify.That(result).Should.BeNull();

                repository.Delete(subject2.Id);
                result = repository.Find(subject2.Id);
                Specify.That(result).Should.BeNull();
            }
        }

        [TestMethod]
        public void ShouldAllowArticleAndLabel()
        {
            var label = new Label
            {
                Id = Guid.NewGuid(),
                PrintData = new Dictionary<string, string>
                {
                    { "test", "test" }
                }
            };

            label.Restrictions.Add(new BasicRestriction<string>("Test Restriction"));
            label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var subject = new Article<IProducible>();
            subject.Producible = label;

            var repository = new ArticleRepository();
            repository.Create(subject);

            var result = repository.FindByAlias(subject.Alias);
            Specify.That(result).Should.Not.BeNull();
            Specify.That(result.Alias).Should.BeEqualTo(subject.Alias);
            Specify.That(result.Description).Should.BeEqualTo(subject.Description);
            var producible = result.Producible as Label;
            Specify.That(producible).Should.Not.BeNull();
            Specify.That(producible.Restrictions[0]).Should.BeInstanceOfType(typeof(BasicRestriction<string>));
            Specify.That(producible.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);

            //Delete and Cleanup
            repository.Delete(subject.Id);
            result = repository.Find(subject.Id);
            Specify.That(result).Should.BeNull();

        }

        [TestMethod]
        public void ShouldAllowArticleWithKit()
        {
            var label = new Label
            {
                Id = Guid.NewGuid(),
                PrintData = new Dictionary<string, string>
                {
                    { "test", "test" }
                }
            };

            label.Restrictions.Add(new BasicRestriction<string>("Test Restriction"));
            label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            var carton = new Carton
            {
                Id = Guid.NewGuid(),
                Width = 5,
                Length = 10,
                Height = 15,
            };
            carton.Restrictions.Add(new MustProduceOnCorrugateWithPropertiesRestriction { Quality = 1 });
            var kit = new Kit
            {
                ItemsToProduce = new ConcurrentList<IProducible> { label, carton },
                CustomerUniqueId = "kit1"
            };

            var subject =  new Article<IProducible>
            {
                Alias = "A2",
                Producible = kit,
                Description = "kit yo"
                
            };


            var repository = new ArticleRepository();
            repository.Create(subject);

            var result = repository.FindByAlias(subject.Alias);
            Specify.That(result).Should.Not.BeNull();
            Specify.That(result.Alias).Should.BeEqualTo(subject.Alias);
            Specify.That(result.Description).Should.BeEqualTo(subject.Description);
            var producibleKit = result.Producible as Kit;
            Specify.That(producibleKit).Should.Not.BeNull();
            Specify.That(producibleKit.CustomerUniqueId).Should.BeEqualTo(kit.CustomerUniqueId);
            Specify.That(producibleKit.Id).Should.BeEqualTo(kit.Id);
            Specify.That(producibleKit.ItemsToProduce[0]).Should.BeInstanceOfType(typeof(Label));
            Specify.That(producibleKit.ItemsToProduce[0].Restrictions.Single(r => r is BasicRestriction<string>)).Should.Not.BeNull();
            Specify.That(producibleKit.ItemsToProduce[1]).Should.BeInstanceOfType(typeof(Carton));
            Specify.That(producibleKit.ItemsToProduce[1].Restrictions.Single(r => r is MustProduceOnCorrugateWithPropertiesRestriction)).Should.Not.BeNull();

            //Delete and Cleanup
            repository.Delete(subject.Id);
            result = repository.Find(subject.Id);
            Specify.That(result).Should.BeNull();

        }

    }

}
