﻿using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Plugin.ArticleService;
using PackNet.Plugin.ArticleService.WorkflowCodeActivities;

using TestUtils;

namespace ArticleServiceTests
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    using PackNet.Common.Interfaces.DTO.PrintingMachines;
    using PackNet.Common.Interfaces.Eventing;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Interfaces.Services;

    using Testing.Specificity;

    [TestClass]
    public class CreateOrderFromArticleProductionRequestTests
    {
        private Mock<IUICommunicationService> uiCommunicator = new Mock<IUICommunicationService>();
        private Mock<IServiceLocator> slMock;
        private readonly ILogger logger = new ConsoleLogger();

        [TestInitialize]
        public void Setup()
        {
            this.slMock = new Mock<IServiceLocator>();
            this.slMock.Setup(sl => sl.Locate<ILogger>()).Returns(logger);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreate1OrderForCarton()
        {
            var articleWithSingleCartonOf1 = ArticleTestHelper.CreateArticleWithSingleCartonWithQuantityOf1();

            var pgGuid = Guid.NewGuid();
            var mgGuid = Guid.Empty;

            var request = new ArticleProductionRequest
            {
                Article = articleWithSingleCartonOf1,
                CustomerUniqueId = "abc123",
                ProductionGroupId = pgGuid,
                MachineGroupId = mgGuid,
                Quantity = 1
            };
            var activity = new CreateOrderFromArticleProductionRequest();

            var input1 = new Dictionary<string, object> 
            {
                { "Request", request },
                { "SendToSpecificProductionGroup", pgGuid },
                { "SendToSpecificMachineGroup", mgGuid },
                { "ServiceLocator", slMock.Object }
            };


            var result = WorkflowInvoker.Invoke(activity, input1);

            var order1 = result;
            Specify.That(order1).Should.Not.BeNull();
            Specify.That(order1.OriginalQuantity).Should.BeEqualTo(request.Quantity);
            Specify.That(order1.Producible.IsTypeOrSubclassOf(typeof(Kit))).Should.BeTrue();
            Specify.That(((Order)(order1.Producible as Kit).ItemsToProduce.First()).OriginalQuantity).Should.BeEqualTo(1);
            Specify.That(order1.CustomerUniqueId).Should.BeEqualTo(request.CustomerUniqueId);
            Specify.That(order1.Restrictions.FirstOrDefault(r => r is ProductionGroupSpecificRestriction && ((ProductionGroupSpecificRestriction)r).Value == request.ProductionGroupId)).Should.Not.BeNull("Restriction not found");


        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateOneOrderForCartonWithQty10()
        {
            var articleWithSingleCartonOf1 = ArticleTestHelper.CreateArticleWithSingleCartonWithQuantityOf1();

            var pgGuid = Guid.NewGuid();
            var mgGuid = Guid.Empty;

            var request = new ArticleProductionRequest
            {
                Article = articleWithSingleCartonOf1,
                CustomerUniqueId = "abc123",
                ProductionGroupId = pgGuid,
                MachineGroupId = mgGuid,
                Quantity = 10
            };
            var activity = new CreateOrderFromArticleProductionRequest();

            var input1 = new Dictionary<string, object> 
            {
                { "Request", request },
                { "SendToSpecificProductionGroup", pgGuid },
                { "SendToSpecificMachineGroup", mgGuid },
                { "ServiceLocator", slMock.Object }
            };


            var result = WorkflowInvoker.Invoke(activity, input1);

            var order1 = result;
            Specify.That(order1).Should.Not.BeNull();
            Specify.That(order1.OriginalQuantity).Should.BeEqualTo(request.Quantity);
            Specify.That(order1.Producible).Should.IsTypeOrSubclassOf(typeof(Order));
            Specify.That(((Order)(order1.Producible as Kit).ItemsToProduce.First()).OriginalQuantity).Should.BeEqualTo(1);

            Specify.That(((Order)(order1.Producible as Kit).ItemsToProduce.First()).Producible.IsTypeOrSubclassOf(typeof(Carton))).Should.BeTrue();
            Specify.That(order1.CustomerUniqueId).Should.BeEqualTo(request.CustomerUniqueId);
            var firstArticleItemId = ((Order) (articleWithSingleCartonOf1.Producible as Kit).ItemsToProduce.First()).Producible.CustomerUniqueId;
            var firstOrderItemId = ((Order)(order1.Producible as Kit).ItemsToProduce.First()).Producible.CustomerUniqueId;
            Specify.That(firstOrderItemId).Should.BeEqualTo(firstArticleItemId);
            Specify.That(order1.Restrictions.FirstOrDefault(r => r is ProductionGroupSpecificRestriction && ((ProductionGroupSpecificRestriction)r).Value == request.ProductionGroupId)).Should.Not.BeNull("Restriction not found");


        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetArticleQuantity_OnMainOrder_AndKeepTheOriginalQuantity_InOrdersInKit()
        {
            var articleWithSingleCartonOf1 = ArticleTestHelper.CreateArticleWithSingleCartonWithQuantityOf2();
           Specify.That(((Order)((Kit)articleWithSingleCartonOf1.Producible).ItemsToProduce.First()).OriginalQuantity).Should.BeEqualTo(2);

            var pgGuid = Guid.NewGuid();
            var mgGuid = Guid.Empty;

            var request = new ArticleProductionRequest
            {
                Article = articleWithSingleCartonOf1,
                CustomerUniqueId = "abc123",
                ProductionGroupId = pgGuid,
                MachineGroupId = mgGuid,
                Quantity = 10
            };
            var activity = new CreateOrderFromArticleProductionRequest();

            var input1 = new Dictionary<string, object> 
            {
                { "Request", request },
                { "SendToSpecificProductionGroup", pgGuid },
                { "SendToSpecificMachineGroup", mgGuid },
                { "ServiceLocator", slMock.Object }
            };


            var result = WorkflowInvoker.Invoke(activity, input1);

            var order1 = result;
            Specify.That(order1).Should.Not.BeNull();
            Specify.That(order1.OriginalQuantity).Should.BeEqualTo(request.Quantity);
            Specify.That(order1.Producible.IsTypeOrSubclassOf(typeof(Kit))).Should.BeTrue();
            Specify.That(order1.CustomerUniqueId).Should.BeEqualTo(request.CustomerUniqueId);
            Specify.That(order1.Restrictions.FirstOrDefault(r => r is ProductionGroupSpecificRestriction && ((ProductionGroupSpecificRestriction)r).Value == request.ProductionGroupId)).Should.Not.BeNull("Restriction not found");

            Specify.That(((Kit)order1.Producible).ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(((Kit)order1.Producible).ItemsToProduce.First().IsTypeOrSubclassOf(typeof(Order))).Should.BeTrue();
            Specify.That((((Kit)order1.Producible).ItemsToProduce.First() as Order).OriginalQuantity).Should.BeEqualTo(2);
        }




        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateOneOrder_WithProducibleOfTypeKit_WithProduciblesOfTypeOrder_ForArticles()
        {
            var articleWithSingleCartonOf1 = ArticleTestHelper.CreateArticleWithKitCartonCartonLabel();
            var kit = articleWithSingleCartonOf1.Producible as Kit;
            var kitOrder1 = ((Order)kit.ItemsToProduce[0]);
            var kitOrder2 = ((Order)kit.ItemsToProduce[1]);
            var kitOrder3 = ((Order)kit.ItemsToProduce[2]);
            var pgGuid = Guid.NewGuid();
            var mgGuid = Guid.Empty;

            var request = new ArticleProductionRequest
            {
                Article = articleWithSingleCartonOf1,
                CustomerUniqueId = "abc123",
                ProductionGroupId = pgGuid,
                MachineGroupId = mgGuid,
                Quantity = 10
            };
            var activity = new CreateOrderFromArticleProductionRequest();

            var input1 = new Dictionary<string, object> 
            {
                { "Request", request },
                { "SendToSpecificProductionGroup", pgGuid },
                { "SendToSpecificMachineGroup", mgGuid },
                { "ServiceLocator", slMock.Object }
            };


            var result = WorkflowInvoker.Invoke(activity, input1) as Order;
            var order1 = result;
            Specify.That(order1).Should.Not.BeNull();
            Specify.That(order1.OriginalQuantity).Should.BeEqualTo(request.Quantity);
            Specify.That(order1.Producible).Should.IsTypeOrSubclassOf(typeof(Kit));
            Specify.That(order1.CustomerUniqueId).Should.BeEqualTo(request.CustomerUniqueId);
            Specify.That(order1.Restrictions.FirstOrDefault(r => r is ProductionGroupSpecificRestriction && ((ProductionGroupSpecificRestriction)r).Value == request.ProductionGroupId)).Should.Not.BeNull("Restriction not found");

            var kitInOrder = order1.Producible as Kit;
            Specify.That(kitInOrder).Should.Not.BeNull();
            Specify.That(kitInOrder.ItemsToProduce.Count).Should.BeEqualTo(3);
            Specify.That(kitInOrder.ItemsToProduce.OfType<Order>().Count()).Should.BeEqualTo(3);
            Specify.That((kitInOrder.ItemsToProduce.ElementAt(0) as Order).Producible.GetType()).Should.BeEqualTo(typeof(Carton));
            Specify.That((kitInOrder.ItemsToProduce.ElementAt(0) as Order).OriginalQuantity).Should.BeEqualTo(kitOrder1.OriginalQuantity);

            Specify.That((kitInOrder.ItemsToProduce.ElementAt(1) as Order).Producible.GetType()).Should.BeEqualTo(typeof(Carton));
            Specify.That((kitInOrder.ItemsToProduce.ElementAt(1) as Order).OriginalQuantity).Should.BeEqualTo(kitOrder2.OriginalQuantity);

            Specify.That((kitInOrder.ItemsToProduce.ElementAt(2) as Order).Producible.GetType()).Should.BeEqualTo(typeof(Label));
            Specify.That((kitInOrder.ItemsToProduce.ElementAt(2) as Order).OriginalQuantity).Should.BeEqualTo(kitOrder3.OriginalQuantity);

        }

    }

}
