﻿using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using PackNet.Common.Communication.RabbitMQ;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Utils;
using PackNet.Plugin.ArticleService;
using PackNet.Plugin.ArticleService.BusinessLayer;

namespace ArticleServiceTests
{
    using System;
    using System.Reactive.Subjects;

    using Microsoft.Reactive.Testing;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    using PackNet.Common.Eventing;
    using PackNet.Common.Interfaces.DTO.Messaging;
    using PackNet.Common.Interfaces.DTO.PrintingMachines;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Eventing;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Interfaces.Producible;
    using PackNet.Common.Interfaces.Services;
    using PackNet.Common.Utils;
    using PackNet.Plugin.ArticleService.Enums;

    using Testing.Specificity;

    [TestClass]
    public class SerializationTests
    {
        private Mock<IUICommunicationService> uiCommunicator = new Mock<IUICommunicationService>();
        private JsonSerializerSettings jsonSettings;
        private JsonSerializerSettings settings = SerializationSettings.GetJsonSerializerSettings();

        [TestInitialize]
        public void TestInit()
        {
            jsonSettings = SerializationSettings.GetJsonSerializerSettings();

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CanDeserializeMessageFromUI()
        {
            var json = JsonConvert.SerializeObject(new
            {
                MessageType = ArticleServiceMessage.ArticleCreateNew.DisplayName,
                Data = this.GetArticleWithLabel(),
            }, jsonSettings);

            Console.WriteLine(json);
            var result = JsonConvert.DeserializeObject<Message<Article<Order>>>(json, jsonSettings);

            Specify.That(result.Data).Should.Not.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CanDeserializeKitMessageFromUI()
        {
            var json = JsonConvert.SerializeObject(this.GetArticleWithKit(), settings);

            Console.WriteLine(json);
            var result = JsonConvert.DeserializeObject<Article<IProducible>>(json, settings);

            var articleOrder = result.Producible as Order;
            Specify.That(articleOrder).Should.Not.BeNull();
            Specify.That(articleOrder.CustomerUniqueId).Should.BeEqualTo("Order");
            var orderKit = articleOrder.Producible as Kit;
            Specify.That(orderKit).Should.Not.BeNull();
            Specify.That(orderKit.CustomerUniqueId).Should.BeEqualTo("Order.Kit");

            var kitOrder = orderKit.ItemsToProduce[0] as Order;
            Specify.That(kitOrder).Should.Not.BeNull();
            Specify.That(kitOrder.CustomerUniqueId).Should.BeEqualTo("Order.Kit.Order1");
            var kitOrderCarton = kitOrder.Producible as Carton;
            Specify.That(kitOrderCarton).Should.Not.BeNull();
            Specify.That(kitOrderCarton.CustomerUniqueId).Should.BeEqualTo("Order.Kit.Order1.Carton");

            kitOrder = orderKit.ItemsToProduce[1] as Order;
            Specify.That(kitOrder).Should.Not.BeNull();
            Specify.That(kitOrder.CustomerUniqueId).Should.BeEqualTo("Order.Kit.Order2");
            var kitOrderLabel = kitOrder.Producible as Label;
            Specify.That(kitOrderLabel).Should.Not.BeNull();
            Specify.That(kitOrderLabel.CustomerUniqueId).Should.BeEqualTo("Order.Kit.Order2.Label");


        }


        [TestMethod]
        [TestCategory("Unit")]
        public void CanDeserializeConcurrentList()
        {
            var clist = new ConcurrentList<Guid>();
            var guid1 = Guid.NewGuid();
            var guid2 = Guid.NewGuid();
            clist.Add(guid1);
            clist.Add(guid2);

            var json = JsonConvert.SerializeObject(clist, settings);

            Console.WriteLine(json);

            var clist2 = JsonConvert.DeserializeObject<ConcurrentList<Guid>>(json, settings);
            Specify.That(clist2).Should.Not.BeNull();
            Specify.That(clist2[0]).Should.BeEqualTo(guid1);
            Specify.That(clist2[1]).Should.BeEqualTo(guid2);
        }

        /*
         {"MessageType":"CreateArticleNew","Data":{"Producible":{"ProducibleType":{"Value":0,"DisplayName":"Order"},"OrderId":null,"IsDistributable":false,"Producible":{"ProducibleType":{"Value":0,"DisplayName":"Carton"},"ArticleId":null,"TrackNumber":0,"CartonOnCorrugate":null,"Length":0.0,"Width":0.0,"Height":0.0,"XValues":{},"CorrugateQuality":null,"DesignId":201,"Quantity":1,"ProductionGroupAlias":null,"CompletedDate":"0001-01-01T00:00:00","Id":"4d8796d7-6d9b-488c-a115-f68290da6b5f","Created":"2015-06-02T12:02:45.0900117-06:00","CustomerUniqueId":"LabelForMyBox1","CustomerDescription":"xDescriptioNx","Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"},"OriginalQuantity":10,"ProducedQuantity":0,"RemainingQuantity":10,"FailedQuantity":0,"ReproducedQuantity":0,"InProgressQuantity":0,"ProductionSequence":0,"StatusChangedDateTime":"0001-01-01T00:00:00","MachinesThatHaveWorkedOnOrder":[],"Id":"e431fb98-6889-4007-b039-ea6ee7d69437","Created":"2015-06-02T18:02:45.1820176Z","CustomerUniqueId":null,"CustomerDescription":null,"Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"},"Id":"4a28471c-2e6c-4cb3-b94f-84823d2739ed","Alias":"Article with Carton","Description":"I have a description!"},"ReplyTo":"","BrowserFingerPrint":"","MachineGroupName":"","MachineGroupId":"","UserName":""}

         
         {"MessageType":"CreateArticleNew","Data":{"Producible":{"ProducibleType":{"Value":0,"DisplayName":"Kit"},"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000","ItemsToProduce":[{"ProducibleType":{"Value":0,"DisplayName":"Order"},"OrderId":null,"IsDistributable":false,"Producible":{"ProducibleType":{"Value":0,"DisplayName":"Carton"},"ArticleId":null,"TrackNumber":0,"CartonOnCorrugate":null,"Length":0.0,"Width":0.0,"Height":0.0,"XValues":{},"CorrugateQuality":null,"DesignId":201,"Quantity":1,"ProductionGroupAlias":null,"CompletedDate":"0001-01-01T00:00:00","Id":"c349de4f-0ab1-46cb-9974-ab7c42857113","Created":"2015-06-02T12:02:45.5670273-06:00","CustomerUniqueId":"LabelForMyBox1","CustomerDescription":"xDescriptioNx","Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"},"OriginalQuantity":10,"ProducedQuantity":0,"RemainingQuantity":10,"FailedQuantity":0,"ReproducedQuantity":0,"InProgressQuantity":0,"ProductionSequence":0,"StatusChangedDateTime":"0001-01-01T00:00:00","MachinesThatHaveWorkedOnOrder":[],"Id":"69f6ec44-3cca-4d2d-8a6f-40c4aed32212","Created":"2015-06-02T18:02:45.5670273Z","CustomerUniqueId":null,"CustomerDescription":null,"Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"},{"ProducibleType":{"Value":0,"DisplayName":"Order"},"OrderId":null,"IsDistributable":false,"Producible":{"ProducibleType":{"Value":0,"DisplayName":"Label"},"PrintData":null,"Id":"ec626512-4fa7-44aa-84d6-ae21b641eff7","Created":"2015-06-02T18:02:45.5710454Z","CustomerUniqueId":"LabelForMyBox1","CustomerDescription":"xDescriptioNx","Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"},"OriginalQuantity":5,"ProducedQuantity":0,"RemainingQuantity":5,"FailedQuantity":0,"ReproducedQuantity":0,"InProgressQuantity":0,"ProductionSequence":0,"StatusChangedDateTime":"0001-01-01T00:00:00","MachinesThatHaveWorkedOnOrder":[],"Id":"1f275b08-82bd-4b4a-a6cd-8abb667291f3","Created":"2015-06-02T18:02:45.5710454Z","CustomerUniqueId":null,"CustomerDescription":null,"Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"}],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"Id":"b1803460-8542-4442-9f00-02468a1d963f","Created":"2015-06-02T18:02:45.5670273Z","CustomerUniqueId":null,"CustomerDescription":null,"Restrictions":[],"History":[]},"Id":"d0404b35-a3a1-404e-b9f8-1c431b4e2141","Alias":"Article with Kit","Description":"I have a description!"},"ReplyTo":"","BrowserFingerPrint":"","MachineGroupName":"","MachineGroupId":"","UserName":""}

         
         {"MessageType":"CreateArticleNew","Data":
         * {"Producible":{"ProducibleType":{"Value":0,"DisplayName":"Order"},
         * "OrderId":null,"IsDistributable":false,
         *  "Producible":{"ProducibleType":{"Value":0,"DisplayName":"Label"},"PrintData":null,"Id":"8957be17-e7cf-4f6a-87c5-cac72c770763","Created":"2015-06-02T18:02:45.5920125Z","CustomerUniqueId":"LabelForMyBox1","CustomerDescription":"xDescriptioNx","Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"},"OriginalQuantity":10,"ProducedQuantity":0,"RemainingQuantity":10,"FailedQuantity":0,"ReproducedQuantity":0,"InProgressQuantity":0,"ProductionSequence":0,"StatusChangedDateTime":"0001-01-01T00:00:00","MachinesThatHaveWorkedOnOrder":[],"Id":"43ac0031-f505-4041-a4a1-ccb63a39d160","Created":"2015-06-02T18:02:45.5920125Z","CustomerUniqueId":null,"CustomerDescription":null,"Restrictions":[],"ProducibleStatus":{"Value":0,"DisplayName":"ProducibleImported"},"History":[],"ProducedOnMachineGroupId":"00000000-0000-0000-0000-000000000000","ProduceOnMachineId":"00000000-0000-0000-0000-000000000000"},"Id":"06e14169-8326-44cb-ab81-41ee6daf1466","Alias":"Article with Label","Description":"I have a description!"},"ReplyTo":"","BrowserFingerPrint":"","MachineGroupName":"","MachineGroupId":"","UserName":""}


         
         
         */
        #region helpers

        private Article<IProducible> GetArticleWithLabel()
        {
            return new Article<IProducible>()
            {
                Alias = "Article with Label",
                Description = "I have a description!",
                Producible = new Order(new Label()
                        {
                            CustomerDescription = "xDescriptioNx",
                            CustomerUniqueId = "LabelForMyBox1",
                            PrintData = new Dictionary<string, string> { { "key1", "value1" }, { "key2", "value2" } }
                        },
                     10)
            };
        }
        private Article<IProducible> GetArticleWithCarton()
        {
            return new Article<IProducible>()
            {
                Alias = "Article with Carton",
                Description = "I have a description!",
                Producible = new Order(
                    new Carton()
                    {
                        CustomerDescription = "xDescriptioNx",
                        CustomerUniqueId = "LabelForMyBox1",
                        DesignId = 201,

                    }, 10)
            };
        }

        private Article<IProducible> GetArticleWithKit()
        {
            return new Article<IProducible>
            {
                Alias = "Article with Label",
                Description = "I have a description!",
                Producible = new Order(new Kit
                {
                    CustomerDescription = "kit desc",
                    CustomerUniqueId = "Order.Kit",
                    ItemsToProduce = new ConcurrentList<IProducible>
                        {
                            new Order(new Carton
                                    {
                                        CustomerDescription = "xDescriptioNx",
                                        CustomerUniqueId = "Order.Kit.Order1.Carton",
                                        DesignId = 201
                                    },1){
                                CustomerUniqueId = "Order.Kit.Order1",
                                
                            },
                            new Order(new Label
                                    {
                                        CustomerDescription = "xDescriptioNx",
                                        CustomerUniqueId = "Order.Kit.Order2.Label",
                                        PrintData = new Dictionary<string, string> { { "key1", "value1" }, { "key2", "value2" } }
                                    },
                                 2){
                               CustomerUniqueId = "Order.Kit.Order2",
                               
                            }
                        }
                }, 10)
                {
                    CustomerUniqueId = "Order",

                }
            };
        }

        

        #endregion
    }

}
