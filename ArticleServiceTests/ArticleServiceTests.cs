﻿namespace ArticleServiceTests
{
    using System;
    using Microsoft.Reactive.Testing;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using PackNet.Common.Eventing;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Messaging;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.PrintingMachines;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Eventing;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Interfaces.Producible;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
    using PackNet.Common.Interfaces.Services;
    using PackNet.Common.Interfaces.Utils;
    using PackNet.Common.Utils;
    using PackNet.Data;
    using PackNet.Data.Repositories.MongoDB;
    using PackNet.Plugin.ArticleService.Data;
    using PackNet.Plugin.ArticleService.Enums;
    using Testing.Specificity;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using Newtonsoft.Json;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Orders;
    using PackNet.Plugin.ArticleService;
    using PackNet.Plugin.ArticleService.BusinessLayer;
    using TestUtils;

    using IArticleService = PackNet.Plugin.ArticleService.IArticleService;

    [TestClass]
    public class ArticleServiceTests
    {
        private Mock<IUICommunicationService> uiCommunicator = new Mock<IUICommunicationService>();

        [TestInitialize]
        public void TestSetup()
        {
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Template>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<MustProduceOnCorrugateWithPropertiesRestriction>(new RestrictionDiscriminatorConvention());
        }

        #region create
        [TestMethod]
        [TestCategory("Unit")]
        public void CallingCreateInService_ShouldCallCreateInBusinessLayer()
        {
            var eventAggregator = new EventAggregator();
            var businessLayerMock = new Mock<IArticleServiceBusinessLayer>();

            var service = this.SetupArticleService(eventAggregator, businessLayerMock.Object);
            var article = this.GetArticleWithSingleLabel();

            service.Create(article);
            businessLayerMock.Verify(r => r.Create(It.Is<Article<IProducible>>(a => a.Equals(article))), Times.Once(), "Create in BL was never called");
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void PublishingACreateArticleMessage_ShouldCallCreateInBusinessLayer()
        {
            var eventAggregator = new EventAggregator();
            var testScheduler = new TestScheduler();
            testScheduler.AdvanceTo(DateTime.Now.Ticks);
            eventAggregator.Scheduler = testScheduler;

            var repoMock = new Mock<IArticleServiceBusinessLayer>();
            repoMock.Setup(repo => repo.Create(It.IsAny<Article<IProducible>>())).Returns(ResultTypes.Success);

            var logger = new Mock<ILogger>();

            var service = this.SetupArticleService(eventAggregator, repoMock.Object, null, logger.Object);
            var article = this.GetArticleWithSingleLabel();
            var userName = "Mango";

            eventAggregator.Publish(new Message<Article<IProducible>>()
            {
                MessageType = ArticleServiceMessage.ArticleCreateNew,
                Data = article,
                UserName = userName
            });

            testScheduler.AdvanceBy(1000);

            Retry.For(
                () =>
                    repoMock.Verify(
                        r => r.Create(It.Is<Article<IProducible>>(a => a.Equals(article))),
                        Times.Once(),
                        "Create in BL was never called"),
                TimeSpan.FromMilliseconds(500));

            logger.Verify(l => l.Log(It.Is<LogLevel>(lv => lv == LogLevel.Info), It.Is<string>(msg =>
                msg.Contains("Article : " + article.Alias + " was created by : " + userName))), Times.Once);

        }
        #endregion

        #region update
        [TestMethod]
        [TestCategory("Unit")]
        public void CallingUpdateInService_ShouldCallUpdateInRepository()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleServiceBusinessLayer>();
            var service = this.SetupArticleService(eventAggregator, repoMock.Object);
            var article = this.GetArticleWithSingleLabel();

            service.Update(article);
            repoMock.Verify(r => r.Update(It.Is<Article<IProducible>>(a => a.Equals(article))), Times.Once(), "Update in repo was never called");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void PublishingAnUpdateArticleMessage_ShouldCallUpdateInBusinessLayer()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleServiceBusinessLayer>();
            repoMock.Setup(repo => repo.Update(It.IsAny<Article<IProducible>>())).Returns(ResultTypes.Success);

            var logger = new Mock<ILogger>();

            var service = this.SetupArticleService(eventAggregator, repoMock.Object, null, logger.Object);
            var article = this.GetArticleWithSingleLabel();
            var userName = "Mango";

            Console.WriteLine(JsonConvert.SerializeObject(article, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All }));

            eventAggregator.Publish(new Message<Article<IProducible>>()
            {
                MessageType = ArticleServiceMessage.ArticleUpdateNew,
                Data = article,
                UserName = userName
            });

            Retry.For(
                () =>
                    repoMock.Verify(
                        r => r.Update(It.Is<Article<IProducible>>(a => a.Equals(article))),
                        Times.Once(),
                        "Update in BL was never called"),
                TimeSpan.FromMilliseconds(500));

            logger.Verify(l => l.Log(It.Is<LogLevel>(lv => lv == LogLevel.Info), It.Is<string>(msg =>
                msg.Contains("Article : " + article.Alias + " was updated by : " + userName))), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void UpdatingAnArticle_ShoudGenerateAnArticleUpdatedMessage()
        {
            var eventAggregator = new EventAggregator();
            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;
            testScheduler.AdvanceTo(DateTime.Now.Ticks);
            Article<IProducible> updated = null;
            string replyTo = "replyTo";
            var triggered = false;

            uiCommunicator
                .Setup(c => c.SendMessageToUI(It.IsAny<ResponseMessage<Article<IProducible>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<Article<IProducible>>, bool, bool>((x, b, b2) =>
                {
                    var r = x as ResponseMessage<Article<IProducible>>;
                    if (r.ReplyTo == replyTo && r.Result == ResultTypes.Success)
                    {
                        triggered = true;
                        updated = r.Data;
                    }
                });

            var article = this.GetArticleWithSingleLabel();

            var boMock = new Mock<IArticleServiceBusinessLayer>();
            boMock.Setup(m => m.Update(article)).Returns(ResultTypes.Success);
            boMock.Setup(m => m.FindByAlias(article.Alias)).Returns(article);

            var service = this.SetupArticleService(eventAggregator, boMock.Object);

            eventAggregator.Publish(new Message<Article<IProducible>>()
            {
                MessageType = ArticleServiceMessage.ArticleUpdateNew,
                Data = article,
                ReplyTo = replyTo
            });

            testScheduler.AdvanceBy(1000);

            Specify.That(triggered).Should.BeTrue();
            Specify.That(updated).Should.BeEqualTo(article);
        }
        #endregion

        #region search
        [TestMethod]
        [TestCategory("Integration")]
        public void CallingSearchInService_CallsSearchInBusinessLayer()
        {
            var eventAggregator = new EventAggregator();
            var businessLayerMock = new Mock<IArticleServiceBusinessLayer>();
            var service = this.SetupArticleService(eventAggregator, businessLayerMock.Object);

            service.Search("MySearchString");
            businessLayerMock.Verify(r => r.Search(It.Is<string>(a => a.Equals("MySearchString"))), Times.Once(), "Search in BL was never called");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void GetArticlesTriggersSearch()
        {
            var testScheduler = new TestScheduler();
            var eventAggregator = new EventAggregator();
            var businessLayerMock = new Mock<IArticleServiceBusinessLayer>();
            var service = this.SetupArticleService(eventAggregator, businessLayerMock.Object);

            eventAggregator.Publish(new Message<string>()
            {
                MessageType = ArticleServiceMessage.ArticleSearchNew,
                Data = "search me"
            });

            Thread.Sleep(200);
            businessLayerMock.Verify(r => r.Search("search me"), Times.Once(), "Search in BL was never called");
        }
        #endregion

        #region delete
        [TestMethod]
        [TestCategory("Unit")]
        public void CallingDeleteInService_ShouldCallDeleteInBusinessLayer()
        {
            var eventAggregator = new EventAggregator();
            var businessLayerMock = new Mock<IArticleServiceBusinessLayer>();
            var service = this.SetupArticleService(eventAggregator, businessLayerMock.Object);
            var article = this.GetArticleWithSingleLabel();

            service.Delete(article);
            businessLayerMock.Verify(r => r.Delete(It.Is<Article<IProducible>>(a => a.Equals(article))), Times.Once(), "Delete in BL was never called");
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void PublishingMultipleDeleteArticleMessage_ShouldCallDeleteInBusinessLayer()
        {
            var eventAggregator = new EventAggregator();
            var businessLayerMock = new Mock<IArticleServiceBusinessLayer>();
            businessLayerMock.Setup(repo => repo.Delete(It.IsAny<IEnumerable<Article<IProducible>>>())).Returns(ResultTypes.Success);

            var logger = new Mock<ILogger>();

            this.SetupArticleService(eventAggregator, businessLayerMock.Object, null, logger.Object);
            var article = this.GetArticleWithSingleLabel();
            var article2 = this.GetArticleWithSingleLabel();
            article2.Alias = "article2";

            var userName = "Mango";

            eventAggregator.Publish(new Message<IEnumerable<Article<IProducible>>>()
            {
                MessageType = ArticleServiceMessage.ArticleDeleteNew,
                Data = new List<Article<IProducible>> { article, article2 },
                UserName = userName
            });

            businessLayerMock.Verify(r => r.Delete(It.Is<IEnumerable<Article<IProducible>>>(a => a.First().Equals(article))), Times.Once(), "Delete in BL was never called");

            logger.Verify(l => l.Log(It.Is<LogLevel>(lv => lv == LogLevel.Info), It.Is<string>(msg =>
                msg.Contains("Articles : " + article.Alias + ", " + article2.Alias + " was deleted by : " + userName))), Times.Once);
        }

        #endregion

        #region produce


        [TestMethod]
        [TestCategory("Unit")]
        public void PublishingMultipleProduceArticleMessage_ShouldWork()
        {
            var eventAggregator = new EventAggregator();
            var businessLayerMock = new Mock<IArticleServiceBusinessLayer>();
            var service = this.SetupArticleService(eventAggregator, businessLayerMock.Object);
            var article = this.GetArticleWithSingleLabel();

            //uiCommunicationService.SendMessageToUI(new ResponseMessage<List<string>>
            //{
            //    MessageType = ArticleServiceMessage.ArticleProducedNew,
            //    Result =  result,
            //    ReplyTo = m.ReplyTo,
            //    Data = resultData
            //});

            eventAggregator.Publish(new Message<IEnumerable<ArticleProductionRequest>>()
            {
                MessageType = ArticleServiceMessage.ArticleProduceNew,
                Data = new List<ArticleProductionRequest>
                {
                    new ArticleProductionRequest{Article = article,CustomerUniqueId = "test",ProductionGroupId = Guid.NewGuid(),Quantity = 10}
                }
            });

        }

        #endregion

        #region integration

        #region export



        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ExportArticles.xaml", "Workflows/")]
        public void ArticleService_ExportSendsMessagesToUI_AndMakesTheRightCalls()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var allArticles = new List<Article<IProducible>>();

            #region article
            var carton = new Carton()
            {
                CustomerUniqueId = "MyCarton",
                CustomerDescription = "CartonDescription",
                DesignId = 2010001,
                Length = 5,
                Width = 5,
                Height = 5,
            };
            var order = new Order(carton, 2);

            var article = new Article<IProducible>()
            {
                Alias = "MyArticle",
                Description = "ArticleDescription",
                Producible =
                    new Kit() { ItemsToProduce = new ConcurrentList<IProducible>() { order } }
            };
            #endregion

            allArticles.Add(article);
            repoMock.Setup(r => r.All()).Returns(allArticles);
            var uiCommServiceMock = new Mock<IUICommunicationService>();
            bool messageSent = false;
            uiCommServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<string>, bool, bool>((msg, b, b2) =>
            {
                messageSent = true;
            });
            var userNotificationService = new Mock<IUserNotificationService>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommServiceMock.Object, userNotificationService.Object);
            var service = this.SetupArticleService(eventAggregator, bl);

            eventAggregator.Publish(new Message() { MessageType = ArticleServiceMessage.ArticleExportNew, ReplyTo = "MyUI" });
            Retry.For(() => messageSent, TimeSpan.FromSeconds(10));
            repoMock.Verify(r => r.All(), Times.Once(), "All in repo was never called");
            uiCommServiceMock.Verify(s => s.SendMessageToUI(It.Is<ResponseMessage<string>>(m => string.IsNullOrWhiteSpace(m.Data) == false), It.IsAny<bool>(), It.IsAny<bool>()), Times.Once);
            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ExportArticles.xaml", "Workflows/")]
        public void ArticleService_CanExportSinglePieceArticles_WithOnlyCartons()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var allArticles = new List<Article<IProducible>>();

            #region article
            var carton = new Carton()
            {
                CustomerUniqueId = "MyCarton",
                CustomerDescription = "CartonDescription",
                DesignId = 2010001,
                Length = 5,
                Width = 5,
                Height = 5,
            };
            var order = new Order(carton, 2);

            var article = new Article<IProducible>()
            {
                Alias = "MyArticle",
                Description = "ArticleDescription",
                Producible =
                    new Kit() { ItemsToProduce = new ConcurrentList<IProducible>() { order } }
            };
            #endregion

            allArticles.Add(article);
            repoMock.Setup(r => r.All()).Returns(allArticles);
            var uiCommServiceMock = new Mock<IUICommunicationService>();
            IResponseMessage<string> responseMessage = null;
            uiCommServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<string>, bool, bool>((msg, b, b2) =>
            {
                responseMessage = msg as IResponseMessage<string>;
            });
            var userNotificationService = new Mock<IUserNotificationService>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommServiceMock.Object, userNotificationService.Object);
            var service = this.SetupArticleService(eventAggregator, bl);
            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;Quantity;Rotation;CorrugateQuality\r\nMyArticle;ArticleDescription;Article;;;;;;;;\r\nMyArticle;CartonDescription;Carton;MyCarton;2010001;5;5;5;2;;\r\n";

            eventAggregator.Publish(new Message() { MessageType = ArticleServiceMessage.ArticleExportNew, ReplyTo = "MyUI" });
            Retry.For(() => responseMessage != null, TimeSpan.FromSeconds(10));
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(data);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ExportArticles.xaml", "Workflows/")]
        public void ArticleService_CanExportSinglePieceArticles_WithOnlyLabels()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var allArticles = new List<Article<IProducible>>();

            #region article
            var label = new Label()
            {
                CustomerUniqueId = "MyLabel",
                CustomerDescription = "LabelDescription",
                PrintData = "TestPrintData",
            };
            label.Restrictions.Add(new BasicRestriction<Template>(new Template() { Name = "MyFileName", Content = "MyTemplate" }));

            var order = new Order(label, 2);

            var article = new Article<IProducible>()
            {
                Alias = "MyArticle",
                Description = "ArticleDescription",
                Producible =
                    new Kit() { ItemsToProduce = new ConcurrentList<IProducible>() { order } }
            };
            #endregion

            allArticles.Add(article);
            repoMock.Setup(r => r.All()).Returns(allArticles);
            var uiCommServiceMock = new Mock<IUICommunicationService>();
            IResponseMessage<string> responseMessage = null;
            uiCommServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<string>, bool, bool>((msg, b, b2) =>
            {
                responseMessage = msg as IResponseMessage<string>;
            });
            var userNotificationService = new Mock<IUserNotificationService>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommServiceMock.Object, userNotificationService.Object);
            var service = this.SetupArticleService(eventAggregator, bl);
            const string data = "ArticleName;Description;Type;Name;PrintData;LabelTemplate;Quantity\r\nMyArticle;ArticleDescription;Article;;;;\r\nMyArticle;LabelDescription;Label;MyLabel;TestPrintData;MyFileName;2\r\n";

            eventAggregator.Publish(new Message() { MessageType = ArticleServiceMessage.ArticleExportNew, ReplyTo = "MyUI" });
            Retry.For(() => responseMessage != null, TimeSpan.FromSeconds(10));
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(data);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ExportArticles.xaml", "Workflows/")]
        public void ArticleService_CanExportArticleWithLabel_WithPrintDataAsDictionary()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var allArticles = new List<Article<IProducible>>();

            #region article
            var label = new Label()
            {
                CustomerUniqueId = "MyLabel",
                CustomerDescription = "LabelDescription",
                PrintData = new Dictionary<string, string>() { { "Identifier", "IdentifierValue" }, { "SomeVariable", "SomeValue" } },
            };
            label.Restrictions.Add(new BasicRestriction<Template>(new Template() { Name = "MyFileName", Content = "MyTemplate" }));

            var order = new Order(label, 1);

            var article = new Article<IProducible>()
            {
                Alias = "MyArticle",
                Description = "ArticleDescription",
                Producible =
                    new Kit() { ItemsToProduce = new ConcurrentList<IProducible>() { order } }
            };
            #endregion

            allArticles.Add(article);
            repoMock.Setup(r => r.All()).Returns(allArticles);
            var uiCommServiceMock = new Mock<IUICommunicationService>();
            IResponseMessage<string> responseMessage = null;
            uiCommServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<string>, bool, bool>((msg, b, b2) =>
            {
                responseMessage = msg as IResponseMessage<string>;
            });
            var userNotificationService = new Mock<IUserNotificationService>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommServiceMock.Object, userNotificationService.Object);
            var service = this.SetupArticleService(eventAggregator, bl);
            const string data = "ArticleName;Description;Type;Name;PrintDataIdentifier;PrintDataSomeVariable;LabelTemplate;Quantity\r\nMyArticle;ArticleDescription;Article;;;;;\r\nMyArticle;LabelDescription;Label;MyLabel;IdentifierValue;SomeValue;MyFileName;1\r\n";

            eventAggregator.Publish(new Message() { MessageType = ArticleServiceMessage.ArticleExportNew, ReplyTo = "MyUI" });
            Retry.For(() => responseMessage != null, TimeSpan.FromSeconds(10));
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(data);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ExportArticles.xaml", "Workflows/")]
        public void ArticleService_CanExportArticles_WithLabelsAndCartons_AndPreserveTheirOrder()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var allArticles = new List<Article<IProducible>>();

            #region article
            var carton = new Carton()
            {
                CustomerUniqueId = "MyCarton",
                CustomerDescription = "CartonDescription",
                DesignId = 2010001,
                Length = 5,
                Width = 5,
                Height = 5,
            };
            var label = new Label()
            {
                CustomerUniqueId = "MyLabel",
                CustomerDescription = "LabelDescription",
                PrintData = "TestPrintData",
            };
            label.Restrictions.Add(new BasicRestriction<Template>(new Template() { Name = "MyFileName", Content = "MyTemplate" }));

            var cartonOrder = new Order(carton, 2);
            var labelOrder = new Order(label, 2);

            var article = new Article<IProducible>()
            {
                Alias = "MyArticle",
                Description = "ArticleDescription",
                Producible =
                    new Kit() { ItemsToProduce = new ConcurrentList<IProducible>() { cartonOrder, labelOrder } }
            };
            #endregion

            allArticles.Add(article);
            repoMock.Setup(r => r.All()).Returns(allArticles);
            var uiCommServiceMock = new Mock<IUICommunicationService>();
            IResponseMessage<string> responseMessage = null;
            uiCommServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<string>, bool, bool>((msg, b, b2) =>
            {
                responseMessage = msg as IResponseMessage<string>;
            });
            var userNotificationService = new Mock<IUserNotificationService>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommServiceMock.Object, userNotificationService.Object);
            var service = this.SetupArticleService(eventAggregator, bl);
            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;Quantity;Rotation;CorrugateQuality;PrintData;LabelTemplate"
                                 + "\r\nMyArticle;ArticleDescription;Article;;;;;;;;;;"
                                 + "\r\nMyArticle;CartonDescription;Carton;MyCarton;2010001;5;5;5;2;;;;"
                                 + "\r\nMyArticle;LabelDescription;Label;MyLabel;;;;;2;;;TestPrintData;MyFileName"
                                 + "\r\n";

            eventAggregator.Publish(new Message() { MessageType = ArticleServiceMessage.ArticleExportNew, ReplyTo = "MyUI" });
            Retry.For(() => responseMessage != null, TimeSpan.FromSeconds(10));
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(data);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ExportArticles.xaml", "Workflows/")]
        public void ArticleService_CanExportMultipleArticles_WithLabelsAndCartons_AndPreserveTheirOrder()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var allArticles = new List<Article<IProducible>>();

            #region article1
            var carton1 = new Carton()
            {
                CustomerUniqueId = "MyCarton",
                CustomerDescription = "CartonDescription",
                DesignId = 2010001,
                Length = 5,
                Width = 5,
                Height = 5,
            };
            var label1 = new Label()
            {
                CustomerUniqueId = "MyLabel",
                CustomerDescription = "LabelDescription",
                PrintData = "TestPrintData",
            };
            label1.Restrictions.Add(new BasicRestriction<Template>(new Template() { Name = "MyFileName", Content = "MyTemplate" }));

            var cartonOrder1 = new Order(carton1, 2);
            var labelOrder1 = new Order(label1, 2);

            var article1 = new Article<IProducible>()
            {
                Alias = "MyArticle",
                Description = "ArticleDescription",
                Producible =
                    new Kit() { ItemsToProduce = new ConcurrentList<IProducible>() { cartonOrder1, labelOrder1 } }
            };
            #endregion
            #region article2
            var carton2 = new Carton()
            {
                CustomerUniqueId = "MyCarton2",
                CustomerDescription = "CartonDescription2",
                DesignId = 2030001,
                Length = 6,
                Width = 6,
                Height = 6,
                CorrugateQuality = 1,
            };
            carton2.Restrictions.Add(new MustProduceWithSpecifiedRotationRestriction() { Orientation = OrientationEnum.Degree0 });
            var label2 = new Label()
            {
                CustomerUniqueId = "MyLabel2",
                CustomerDescription = "LabelDescription2",
                PrintData = "TestPrintData2",
            };
            label2.Restrictions.Add(new BasicRestriction<Template>(new Template() { Name = "MyFileName2", Content = "MyTemplate2" }));

            var cartonOrder2 = new Order(carton2, 1);
            var labelOrder2 = new Order(label2, 1);

            var article2 = new Article<IProducible>()
            {
                Alias = "MyArticle2",
                Description = "ArticleDescription2",
                Producible =
                    new Kit() { ItemsToProduce = new ConcurrentList<IProducible>() { labelOrder2, cartonOrder2 } }
            };
            #endregion

            allArticles.Add(article1);
            allArticles.Add(article2);
            repoMock.Setup(r => r.All()).Returns(allArticles);
            var uiCommServiceMock = new Mock<IUICommunicationService>();
            IResponseMessage<string> responseMessage = null;
            uiCommServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<string>, bool, bool>((msg, b, b2) =>
            {
                responseMessage = msg as IResponseMessage<string>;
            });
            var userNotificationService = new Mock<IUserNotificationService>();
            var bl = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommServiceMock.Object, userNotificationService.Object);
            var service = this.SetupArticleService(eventAggregator, bl);
            const string data =
                "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;Quantity;Rotation;CorrugateQuality;PrintData;LabelTemplate"
                + "\r\nMyArticle;ArticleDescription;Article;;;;;;;;;;"
                + "\r\nMyArticle;CartonDescription;Carton;MyCarton;2010001;5;5;5;2;;;;"
                + "\r\nMyArticle;LabelDescription;Label;MyLabel;;;;;2;;;TestPrintData;MyFileName"
                + "\r\nMyArticle2;ArticleDescription2;Article;;;;;;;;;;"
                + "\r\nMyArticle2;LabelDescription2;Label;MyLabel2;;;;;1;;;TestPrintData2;MyFileName2"
                + "\r\nMyArticle2;CartonDescription2;Carton;MyCarton2;2030001;6;6;6;1;Degree0;1;;"
                + "\r\n";

            eventAggregator.Publish(new Message() { MessageType = ArticleServiceMessage.ArticleExportNew, ReplyTo = "MyUI" });
            Retry.For(() => responseMessage != null, TimeSpan.FromSeconds(10));
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(data);
        }

        #endregion

        #region import

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldBeAbleToImportArticle_ContainingAKit_AndSingleCarton()
        {
            var eventAggregator = new EventAggregator();
            var articleServiceMock = new Mock<IArticleService>();
            var repoMock = new Mock<IArticleRepository>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var packagingDesignService = new Mock<IPackagingDesignService>();
            var templateService = new Mock<ITemplateService>();
            packagingDesignService.Setup(s => s.GetDesignName(2010001)).Returns("A-Valid-Design");
            var mockParams = new List<DesignParameter>();
            packagingDesignService.Setup(s => s.GetDesignFromId(2010001)).Returns(new PackagingDesign { Name = "A-Valid-Design", DesignParameters = mockParams });

            var importedArticles = new ArticleImportCollection();

            var called = false;
            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                called = true;
                importedArticles = msg.Data;
            });

            var message = "";

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object, packagingDesignService.Object);

            SetupArticleService(eventAggregator, businessLayer, userNotificationService.Object, null, packagingDesignService.Object);

            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;Quantity;Rotation;CorrugateQuality\r\n" +
                                "MyArticle;ArticleDescription;Article;;;;;;;;\r\n" +
                                "MyArticle;CartonDescription;Carton;MyCarton;2010001;5;5;5;2;Degree90;" +
                                "\r\n";

            var importItem = new ArticleImportFileItem()
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });
            Retry.For(() => called, TimeSpan.FromSeconds(10));
            Specify.That(called).Should.BeTrue();

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(1);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(0);

            Specify.That(message.Contains("Parsed 1 article(s)")).Should.BeTrue();

            var article = importedArticles.Items.First();
            Specify.That(article.ImportAction).Should.BeLogicallyEqualTo(ArticleImportAction.Import);

            Specify.That(article.Alias).Should.BeLogicallyEqualTo("MyArticle");
            Specify.That(article.Description).Should.BeLogicallyEqualTo("ArticleDescription");
            Specify.That(article.ProducibleType).Should.BeLogicallyEqualTo(ArticleProducibleTypes.Article);
            Specify.That(article.Producible as Kit).Should.Not.BeNull();

            var kit = article.Producible as Kit;

            Specify.That(kit.ItemsToProduce.Count).Should.BeEqualTo(1);

            var order = kit.ItemsToProduce.First() as Order;

            Specify.That(order).Should.Not.BeNull();
            Specify.That(order.OriginalQuantity).Should.BeEqualTo(2);

            var carton = order.Producible as Carton;

            Specify.That(carton).Should.Not.BeNull();
            Specify.That(carton.CustomerUniqueId).Should.BeLogicallyEqualTo("MyCarton");
            Specify.That(carton.DesignId).Should.BeLogicallyEqualTo(2010001);
            Specify.That(carton.CustomerDescription).Should.BeLogicallyEqualTo("CartonDescription");
            Specify.That(carton.Quantity).Should.BeEqualTo(1);
            Specify.That(carton.Length).Should.BeLogicallyEqualTo(5d);
            Specify.That(carton.Width).Should.BeLogicallyEqualTo(5d);
            Specify.That(carton.Height).Should.BeLogicallyEqualTo(5d);
            Specify.That(carton.Restrictions.OfType<MustProduceWithSpecifiedRotationRestriction>().Count()).Should.BeEqualTo(1);
            Specify.That(carton.Rotation.HasValue).Should.BeTrue();
            Specify.That(carton.Rotation.Value).Should.BeLogicallyEqualTo(OrientationEnum.Degree90);
            Specify.That(carton.Restrictions.OfType<MustProduceOnCorrugateWithPropertiesRestriction>().Count()).Should.BeEqualTo(0);
            Specify.That(carton.CorrugateQuality.HasValue).Should.BeFalse();
            Specify.That(carton.XValues.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldNotImportCartonWithoutAName()
        {
            var eventAggregator = new EventAggregator();
            var articleServiceMock = new Mock<IArticleService>();
            var repoMock = new Mock<IArticleRepository>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var packagingDesignService = new Mock<IPackagingDesignService>();
            var templateService = new Mock<ITemplateService>();
            packagingDesignService.Setup(s => s.GetDesignName(2010001)).Returns("A-Valid-Design");

            var importedArticles = new ArticleImportCollection();

            var called = false;
            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                called = true;
                importedArticles = msg.Data;
            });

            var message = "";

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object, packagingDesignService.Object);

            SetupArticleService(eventAggregator, businessLayer, userNotificationService.Object, null, packagingDesignService.Object);

            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;Quantity;Rotation;CorrugateQuality\r\n" +
                                "MyArticle;ArticleDescription;Article;;;;;;;;\r\n" +
                                "MyArticle;CartonDescription;Carton;;2010001;5;5;5;2;Degree90;" +
                                "\r\n";

            var importItem = new ArticleImportFileItem()
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });
            Retry.For(() => called, TimeSpan.FromSeconds(10));
            Specify.That(called).Should.BeTrue();

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(0);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(1);

            Specify.That(message.Contains("failed to process 1 item")).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldNotImportItemWithInvalidOrMissingType()
        {
            var eventAggregator = new EventAggregator();
            var articleServiceMock = new Mock<IArticleService>();
            var repoMock = new Mock<IArticleRepository>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var packagingDesignService = new Mock<IPackagingDesignService>();
            var templateService = new Mock<ITemplateService>();
            packagingDesignService.Setup(s => s.GetDesignName(2010001)).Returns("A-Valid-Design");

            var importedArticles = new ArticleImportCollection();

            var called = false;
            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                called = true;
                importedArticles = msg.Data;
            });

            var message = "";

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object, packagingDesignService.Object);

            SetupArticleService(eventAggregator, businessLayer, userNotificationService.Object, null, packagingDesignService.Object);

            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;Quantity;Rotation;CorrugateQuality\r\n" +
                                "MyArticle;ArticleDescription;;;;;;;;;\r\n" +
                                "MyArticle;CartonDescription;Crtn;;2010001;5;5;5;2;Degree90;" +
                                "\r\n";

            var importItem = new ArticleImportFileItem()
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });
            Retry.For(() => called, TimeSpan.FromSeconds(10));
            Specify.That(called).Should.BeTrue();

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(0);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(2);

            Specify.That(message.Contains("failed to process 2 item(s)")).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldNotImportArticlesWithoutSubArticles()
        {
            var eventAggregator = new EventAggregator();
            var articleServiceMock = new Mock<IArticleService>();
            var repoMock = new Mock<IArticleRepository>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var packagingDesignService = new Mock<IPackagingDesignService>();
            var templateService = new Mock<ITemplateService>();
            packagingDesignService.Setup(s => s.GetDesignName(2010001)).Returns("A-Valid-Design");

            var importedArticles = new ArticleImportCollection();

            var called = false;
            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                called = true;
                importedArticles = msg.Data;
            });

            var message = "";

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object, packagingDesignService.Object);

            SetupArticleService(eventAggregator, businessLayer, userNotificationService.Object, null, packagingDesignService.Object);

            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;Quantity;Rotation;CorrugateQuality\r\n" +
                                "MyArticle1;ArticleDescription1;Article;;;;;;;;\r\n" +
                                "MyArticle2;ArticleDescription2;Article;;;;;;;;\r\n" +
                                "\r\n";

            var importItem = new ArticleImportFileItem()
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });
            Retry.For(() => called, TimeSpan.FromSeconds(10));
            Specify.That(called).Should.BeTrue();

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(0);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(2);

            Specify.That(message.Contains("failed to process 2 item(s)")).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldBeAbleToImportArticle_ContainingAKit_AndSingleLabel()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var articleServiceMock = new Mock<IArticleService>();
            var templateService = new Mock<ITemplateService>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();

            ArticleImportCollection importedArticles = null;

            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                importedArticles = msg.Data;
            });

            var message = "";

            templateService.Setup(s => s.FindByName("MyFileName")).Returns(new Template
            {
                Name = "MyFileName",
                Content =
                    "This value: '{SomeVariable}' should be equal to 'SomeValue', and '{Identifier}' should be equal to 'IdentifierValue'"
            });

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object);

            const string data = "ArticleName;Description;Type;Name;PrintData;LabelTemplate;Quantity" +
                                "\r\nMyArticle;ArticleDescription;Article;;;;" +
                                "\r\nMyArticle;LabelDescription;Label;MyLabel;PrintDataTest;MyFileName;2";

            var importItem = new ArticleImportFileItem
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            SetupArticleService(eventAggregator, businessLayer);

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });

            Retry.For(() => importedArticles != null, TimeSpan.FromSeconds(10));

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(1);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(0);

            Specify.That(message.Contains("Parsed 1 article(s)")).Should.BeTrue();

            var article = importedArticles.Items.First();
            Specify.That(article.ImportAction).Should.BeLogicallyEqualTo(ArticleImportAction.Import);

            Specify.That(article.Alias).Should.BeLogicallyEqualTo("MyArticle");
            Specify.That(article.Description).Should.BeLogicallyEqualTo("ArticleDescription");
            Specify.That(article.ProducibleType).Should.BeLogicallyEqualTo(ArticleProducibleTypes.Article);
            Specify.That(article.Producible as Kit).Should.Not.BeNull();

            var kit = article.Producible as Kit;

            Specify.That(kit.ItemsToProduce.Count).Should.BeEqualTo(1);

            var order = kit.ItemsToProduce.First() as Order;

            Specify.That(order).Should.Not.BeNull();
            Specify.That(order.OriginalQuantity).Should.BeEqualTo(2);

            var label = order.Producible as Label;

            Specify.That(label).Should.Not.BeNull();
            Specify.That(label.CustomerUniqueId).Should.BeLogicallyEqualTo("MyLabel");
            Specify.That(label.CustomerDescription).Should.BeLogicallyEqualTo("LabelDescription");
            Specify.That(label.Restrictions.OfType<BasicRestriction<Template>>().Count()).Should.BeEqualTo(1);

            var restriction = label.Restrictions.OfType<BasicRestriction<Template>>().First();

            Specify.That(restriction.Value.Name).Should.BeLogicallyEqualTo("MyFileName");

            Specify.That(label.PrintData as string).Should.BeLogicallyEqualTo("PrintDataTest");
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldNotImportLabelWithoutAName()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var articleServiceMock = new Mock<IArticleService>();
            var templateService = new Mock<ITemplateService>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();

            ArticleImportCollection importedArticles = null;

            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                importedArticles = msg.Data;
            });

            var message = "";

            templateService.Setup(s => s.FindByName("MyFileName")).Returns(new Template
            {
                Name = "MyFileName",
                Content =
                    "This value: '{SomeVariable}' should be equal to 'SomeValue', and '{Identifier}' should be equal to 'IdentifierValue'"
            });

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object);

            const string data = "ArticleName;Description;Type;Name;PrintData;LabelTemplate;Quantity" +
                                "\r\nMyArticle;ArticleDescription;Article;;;;" +
                                "\r\nMyArticle;LabelDescription;Label;;PrintDataTest;MyFileName;2";

            var importItem = new ArticleImportFileItem
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            SetupArticleService(eventAggregator, businessLayer);

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });

            Retry.For(() => importedArticles != null, TimeSpan.FromSeconds(10));

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(0);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(1);

            Specify.That(message.Contains("failed to process 1 item")).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldBeAbleToImportArticle_ContainingAKit_AndSingleLabel_AndSingleCarton()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var articleServiceMock = new Mock<IArticleService>();
            var templateService = new Mock<ITemplateService>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var packagingDesignService = new Mock<IPackagingDesignService>();
            packagingDesignService.Setup(s => s.GetDesignName(2010001)).Returns("A-Valid-Design");
            var message = "";
            var mockParams = new List<DesignParameter>();
            packagingDesignService.Setup(s => s.GetDesignFromId(2010001)).Returns(new PackagingDesign { Name = "A-Valid-Design", DesignParameters = mockParams });


            templateService.Setup(s => s.FindByName("MyFileName")).Returns(new Template
                           {
                               Name = "MyFileName",
                               Content =
                                   "This value: '{SomeVariable}' should be equal to 'SomeValue', and '{Identifier}' should be equal to 'IdentifierValue'"
                           });

            ArticleImportCollection importedArticles = null;

            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                importedArticles = msg.Data;
            });

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object, packagingDesignService.Object);

            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;PrintDataIdentifier;PrintDataSomeVariable;LabelTemplate;Quantity" +
                                "\r\nMyArticle;ArticleDescription;Article;;;;;;;;;" +
                                "\r\nMyArticle;CartonDescription;Carton;MyCarton;2010001;5;5;5;;;;1" +
                                "\r\nMyArticle;LabelDescription;Label;MyLabel;;;;;IdentifierValue;SomeValue;MyFileName;1" +
                                "\r\n";

            var importItem = new ArticleImportFileItem
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            SetupArticleService(eventAggregator, businessLayer, userNotificationService.Object, null, packagingDesignService.Object, templateService.Object);

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });

            Retry.For(() => importedArticles != null, TimeSpan.FromSeconds(10));

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(1);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(0);

            Specify.That(message.Contains("Parsed 1 article(s)")).Should.BeTrue();

            var article = importedArticles.Items.First();
            Specify.That(article.ImportAction).Should.BeLogicallyEqualTo(ArticleImportAction.Import);

            Specify.That(article.Alias).Should.BeLogicallyEqualTo("MyArticle");
            Specify.That(article.Description).Should.BeLogicallyEqualTo("ArticleDescription");
            Specify.That(article.ProducibleType).Should.BeLogicallyEqualTo(ArticleProducibleTypes.Article);
            Specify.That(article.Producible as Kit).Should.Not.BeNull();

            var kit = article.Producible as Kit;

            Specify.That(kit.ItemsToProduce.Count).Should.BeEqualTo(2);

            var order = kit.ItemsToProduce.First() as Order;

            Specify.That(order).Should.Not.BeNull();
            Specify.That(order.OriginalQuantity).Should.BeEqualTo(1);

            var carton = order.Producible as Carton;

            Specify.That(carton).Should.Not.BeNull();
            Specify.That(carton.CustomerUniqueId).Should.BeLogicallyEqualTo("MyCarton");
            Specify.That(carton.DesignId).Should.BeLogicallyEqualTo(2010001);
            Specify.That(carton.CustomerDescription).Should.BeLogicallyEqualTo("CartonDescription");
            Specify.That(carton.Quantity).Should.BeEqualTo(1);
            Specify.That(carton.Length).Should.BeLogicallyEqualTo(5d);
            Specify.That(carton.Width).Should.BeLogicallyEqualTo(5d);
            Specify.That(carton.Height).Should.BeLogicallyEqualTo(5d);

            order = kit.ItemsToProduce.Last() as Order;

            var label = order.Producible as Label;

            Specify.That(label).Should.Not.BeNull();
            Specify.That(label.CustomerUniqueId).Should.BeLogicallyEqualTo("MyLabel");
            Specify.That(label.CustomerDescription).Should.BeLogicallyEqualTo("LabelDescription");
            Specify.That(label.Restrictions.OfType<BasicRestriction<Template>>().Count()).Should.BeEqualTo(1);

            var restriction = label.Restrictions.OfType<BasicRestriction<Template>>().First();

            Specify.That(restriction.Value.Name).Should.BeLogicallyEqualTo("MyFileName");

            Specify.That(label.PrintData as Dictionary<string, string>).Should.Not.BeNull();

            var printData = label.PrintData as Dictionary<string, string>;

            Specify.That(printData["Identifier"].ToString()).Should.BeLogicallyEqualTo("IdentifierValue");
            Specify.That(printData["SomeVariable"].ToString()).Should.BeLogicallyEqualTo("SomeValue");
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldBeAbleToImportMultiple_Articles()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var articleServiceMock = new Mock<IArticleService>();
            var templateService = new Mock<ITemplateService>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var packagingDesignService = new Mock<IPackagingDesignService>();
            packagingDesignService.Setup(s => s.GetDesignName(2010001)).Returns("A-Valid-Design");
            var mockParams = new List<DesignParameter>();
            packagingDesignService.Setup(s => s.GetDesignFromId(2010001)).Returns(new PackagingDesign { Name = "A-Valid-Design", DesignParameters = mockParams });


            templateService.Setup(s => s.FindByName("MyFileName")).Returns(new Template
            {
                Name = "MyFileName",
                Content =
                    "This value: '{SomeVariable}' should be equal to 'SomeValue', and '{Identifier}' should be equal to 'IdentifierValue'"
            });
            templateService.Setup(s => s.FindByName("MyFileName2")).Returns(new Template
            {
                Name = "MyFileName2",
                Content =
                    "This value: '{SomeVariableTwo}' should be equal to 'SomeValue', and '{IdentifierTwo}' should be equal to 'IdentifierValue'"
            });

            var message = "";

            userNotificationService.Setup(
                s =>
                    s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(),
                        It.IsAny<string>())).Callback<NotificationSeverity, string, string>((severity, msg, additional) =>
                        {
                            message = msg;
                        });

            ArticleImportCollection importedArticles = null;

            uiCommunicationService.Setup(s => s.SendMessageToUI(It.IsAny<ResponseMessage<ArticleImportCollection>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage<ArticleImportCollection>, bool, bool>((msg, b, b2) =>
            {
                importedArticles = msg.Data;
            });

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object, articleServiceMock.Object, templateService.Object, packagingDesignService.Object);

            const string data = "ArticleName;Description;Type;Name;DesignId;Length;Width;Height;PrintDataIdentifier;PrintDataSomeVariable;PrintDataIdentifierTwo;PrintDataSomeVariableTwo;LabelTemplate;Quantity" +
                    "\r\nMyArticle;ArticleDescription;Article;;;;;;;;;;;" +
                    "\r\nMyArticle;CartonDescription;Carton;MyCarton;2010001;5;5;5;;;;;;1" +
                    "\r\nMyArticle;LabelDescription;Label;MyLabel;;;;;IdentifierValue;SomeValue;IdentifierValue2;SomeValue2;MyFileName;1" +
                    "\r\nMySecondArticle;ArticleDescription;Article;;;;;;;;;;;" +
                    "\r\nMySecondArticle;CartonDescription;Carton;MyCarton;2010001;5;5;5;;;;;;1" +
                    "\r\nMySecondArticle;LabelDescription;Label;MyLabel;;;;;IdentifierValue;SomeValue;IdentifierValue2;SomeValue2;MyFileName;1" +
                    "\r\n";

            var importItem = new ArticleImportFileItem
            {
                Contents = data,
                CommentPrefix = '#',
                Delimiter = ';',
                HeadersInFile = true
            };

            SetupArticleService(eventAggregator, businessLayer, userNotificationService.Object, null, packagingDesignService.Object, templateService.Object);

            eventAggregator.Publish(new Message<ArticleImportFileItem>()
            {
                MessageType = ArticleServiceMessage.ArticleImportNew,
                Data = importItem,
                ReplyTo = "MangoUi"
            });

            Retry.For(() => importedArticles != null, TimeSpan.FromSeconds(10));

            userNotificationService.Verify(s => s.SendNotificationToSystem(It.Is<NotificationSeverity>(l => l == NotificationSeverity.Info), It.IsAny<string>(), It.IsAny<string>()), Times.Once);

            Specify.That(message.Contains("Parsed 2 article(s)")).Should.BeTrue();

            Specify.That(importedArticles.Items.Count()).Should.BeEqualTo(2);
            Specify.That(importedArticles.FailedImports).Should.BeEqualTo(0);

            var article = importedArticles.Items.First();

            Specify.That(article.ImportAction).Should.BeLogicallyEqualTo(ArticleImportAction.Import);
            Specify.That(article.Alias).Should.BeLogicallyEqualTo("MyArticle");
            Specify.That(article.Description).Should.BeLogicallyEqualTo("ArticleDescription");
            Specify.That(article.ProducibleType).Should.BeLogicallyEqualTo(ArticleProducibleTypes.Article);
            Specify.That(article.Producible as Kit).Should.Not.BeNull();

            var kit = article.Producible as Kit;

            Specify.That(kit.ItemsToProduce.Count).Should.BeEqualTo(2);

            var order = kit.ItemsToProduce.First() as Order;

            Specify.That(order).Should.Not.BeNull();
            Specify.That(order.OriginalQuantity).Should.BeEqualTo(1);

            var carton = order.Producible as Carton;
            Specify.That(carton).Should.Not.BeNull();

            order = kit.ItemsToProduce.Last() as Order;

            var label = order.Producible as Label;

            Specify.That(label).Should.Not.BeNull();
            Specify.That(label.PrintData as Dictionary<string, string>).Should.Not.BeNull();
            Specify.That((label.PrintData as Dictionary<string, string>).Keys.Count).Should.BeLogicallyEqualTo(2);

            article = importedArticles.Items.Last();
            Specify.That(article.ImportAction).Should.BeLogicallyEqualTo(ArticleImportAction.Import);

            kit = article.Producible as Kit;

            Specify.That(kit.ItemsToProduce.Count).Should.BeEqualTo(2);

            order = kit.ItemsToProduce.First() as Order;

            Specify.That(order).Should.Not.BeNull();
            Specify.That(order.OriginalQuantity).Should.BeEqualTo(1);

            order = kit.ItemsToProduce.Last() as Order;

            Specify.That(order).Should.Not.BeNull();

            label = order.Producible as Label;

            Specify.That(label).Should.Not.BeNull();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldFinalizeImport_WhenUI_SendsBackArticles_WithASingleCarton()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object);

            SetupArticleService(eventAggregator, businessLayer);

            var articlesToImport = new List<Article<IProducible>>
                {
                    new Article<IProducible>()
                    {
                        Alias = "MyArticle",
                        Description = "MyDescription",
                        Id = Guid.NewGuid(),
                        Producible = new Kit
                        {
                            ItemsToProduce = new ConcurrentList<IProducible>
                            {
                                new Order(new Carton
                                {
                                    DesignId = 2010001,
                                    Length = 5.0,
                                    Width = 5.0,
                                    Height = 5.0,
                                    CustomerDescription = "MyCartonDescription",
                                    CustomerUniqueId = "MyCarton"
                                }, 2)
                            }
                        }
                    }
                };

            eventAggregator.Publish(new Message<IEnumerable<Article<IProducible>>>
            {
                MessageType = ArticleServiceMessage.FinalizeArticleImport,
                Data = articlesToImport,
                ReplyTo = "MangoUi"
            });

            repoMock.Verify(repo => repo.CreateEnsureNoDuplicate(It.IsAny<Article<IProducible>>()), Times.Once);

            repoMock.Verify(repo => repo.CreateEnsureNoDuplicate(It.Is<Article<IProducible>>(item =>
                item.Alias == articlesToImport.First().Alias &&
                item.Description == articlesToImport.First().Description &&
                item.Producible as Kit != null &&
                ((Kit)item.Producible).ItemsToProduce.Count == 1 &&
                ((Kit)item.Producible).ItemsToProduce.First() as Order != null
                )));
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("Workflows/ImportArticles.xaml", "Workflows/")]
        public void ShouldFinalizeImport_WhenUI_SendsBackArticles_WithASingleCarton_AndSingleLabel()
        {
            var eventAggregator = new EventAggregator();
            var repoMock = new Mock<IArticleRepository>();
            var userNotificationService = new Mock<IUserNotificationService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();

            var businessLayer = SetupArticleServiceBL(repoMock.Object, eventAggregator, uiCommunicationService.Object, userNotificationService.Object);

            SetupArticleService(eventAggregator, businessLayer);

            var label = new Label
            {
                CustomerDescription = "MyCartonDescription",
                CustomerUniqueId = "MyCarton",
                PrintData = new Dictionary<string, string> { { "ShipTo", "Mango" } },
            };

            label.Restrictions.Add(new BasicRestriction<Template>
            {
                Value = new Template { Id = Guid.NewGuid(), Name = "MyName" }
            });

            var articlesToImport = new List<Article<IProducible>>
                {
                    new Article<IProducible>
                    {
                        Alias = "MyArticle",
                        Description = "MyDescription",
                        Id = Guid.NewGuid(),
                        Producible = new Kit
                        {
                            ItemsToProduce = new ConcurrentList<IProducible>
                            {
                                new Order(new Carton
                                {
                                    DesignId = 2010001,
                                    Length = 5.0,
                                    Width = 5.0,
                                    Height = 5.0,
                                    CustomerDescription = "MyCartonDescription",
                                    CustomerUniqueId = "MyCarton"
                                }, 2),
                                new Order(label, 1)
                            }
                        }
                    },
                };

            eventAggregator.Publish(new Message<IEnumerable<Article<IProducible>>>
            {
                MessageType = ArticleServiceMessage.FinalizeArticleImport,
                Data = articlesToImport,
                ReplyTo = "MangoUi"
            });

            repoMock.Verify(repo => repo.CreateEnsureNoDuplicate(It.IsAny<Article<IProducible>>()), Times.Once);

            repoMock.Verify(repo => repo.CreateEnsureNoDuplicate(It.Is<Article<IProducible>>(item =>
                item.Alias == articlesToImport.First().Alias &&
                item.Description == articlesToImport.First().Description &&
                item.Producible as Kit != null &&
                ((Kit)item.Producible).ItemsToProduce.Count == 2 &&
                ((Kit)item.Producible).ItemsToProduce.First() as Order != null &&
                ((Kit)item.Producible).ItemsToProduce.Last() as Order != null
            )));
        }

        #endregion

        [TestMethod]
        [TestCategory("Integration")]
        public void CreateArticle_ShouldCreateArticleInBusinessLayer()
        {

            var eventAggregator = new EventAggregator();
            var service = this.SetupArticleService(eventAggregator);
            var article = this.GetArticleWithSingleLabel();

            CleanupArticleRepo(article, service);

            var result = service.Create(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success);

            var createdArt = service.Find(article.Id);
            Specify.That(createdArt.Id).Should.Not.BeEqualTo(Guid.Empty);
            Specify.That(createdArt.Alias).Should.BeLogicallyEqualTo(article.Alias);
            Specify.That(createdArt.Description).Should.BeLogicallyEqualTo(article.Description);
            Specify.That(createdArt.Producible.ProducibleType).Should.BeLogicallyEqualTo(ProducibleTypes.Kit);
            Specify.That(createdArt.Producible.Id).Should.Not.BeEqualTo(Guid.Empty);
            Specify.That((((createdArt.Producible as Kit).ItemsToProduce.First() as Order).Producible as Label)).Should.Not.BeNull();
            Specify.That((((createdArt.Producible as Kit).ItemsToProduce.First() as Order).Producible as Label).CustomerDescription).Should.BeLogicallyEqualTo("xDescriptioNx");
            Specify.That((((createdArt.Producible as Kit).ItemsToProduce.First() as Order).Producible as Label).CustomerUniqueId).Should.BeLogicallyEqualTo("LabelForMyBox1");

            var foundArticle = service.Find(createdArt.Id);
            Specify.That(foundArticle).Should.Not.BeNull();
            Specify.That(foundArticle).Should.BeLogicallyEqualTo(createdArt);

            CleanupArticleRepo(article, service);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void CreateArticle_ShouldFailIfAliasExists()
        {
            var eventAggregator = new EventAggregator();
            var service = this.SetupArticleService(eventAggregator);
            var article = this.GetArticleWithSingleLabel();

            CleanupArticleRepo(article, service);

            var result = service.Create(article);
            Specify.That(result).Should.BeEqualTo(ResultTypes.Success);
            result = service.Create(article);
            Specify.That(result).Should.BeEqualTo(ArticleServiceResultTypes.ArticleAliasAlreadyInUse);

            CleanupArticleRepo(article, service);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void UpdateArticle_ShouldUpdateExistingArticle()
        {
            var eventAggregator = new EventAggregator();
            var service = this.SetupArticleService(eventAggregator);
            var article = this.GetArticleWithSingleLabel();

            CleanupArticleRepo(article, service);

            var createdArt = service.Create(article);
            Specify.That(createdArt).Should.BeEqualTo(ResultTypes.Success);
            article.Alias = "New alias";

            var updateResult = service.Update(article);
            Specify.That(createdArt).Should.BeEqualTo(ResultTypes.Success);

            var updatedArt = service.Find(article.Id);
            Specify.That(updatedArt.Id).Should.Not.BeEqualTo(Guid.Empty);
            Specify.That(updatedArt.Id).Should.BeEqualTo(article.Id);
            Specify.That(updatedArt.Alias).Should.BeLogicallyEqualTo("New alias");
            Specify.That(updatedArt.Description).Should.BeLogicallyEqualTo(article.Description);
            Specify.That(updatedArt.Producible.ProducibleType).Should.BeLogicallyEqualTo(ProducibleTypes.Label);
            Specify.That(updatedArt.Producible.Id).Should.Not.BeEqualTo(Guid.Empty);
            Specify.That((((updatedArt.Producible as Kit).ItemsToProduce.First() as Order).Producible as Label)).Should.Not.BeNull();
            Specify.That((((updatedArt.Producible as Kit).ItemsToProduce.First() as Order).Producible as Label).CustomerDescription).Should.BeLogicallyEqualTo("xDescriptioNx");
            Specify.That((((updatedArt.Producible as Kit).ItemsToProduce.First() as Order).Producible as Label).CustomerUniqueId).Should.BeLogicallyEqualTo("LabelForMyBox1");

            CleanupArticleRepo(article, service);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void DeleteArticle_RemovesArticleFromService()
        {
            var eventAggregator = new EventAggregator();
            var service = this.SetupArticleService(eventAggregator);
            var article = this.GetArticleWithSingleLabel();

            var createdArt = service.Create(article);
            Specify.That(createdArt).Should.BeEqualTo(ResultTypes.Success);

            var foundArt = service.Find(article.Id);
            Specify.That(foundArt).Should.Not.BeNull();

            service.Delete(foundArt);

            var shouldBeNull = service.Find(foundArt.Id);
            Specify.That(shouldBeNull).Should.BeNull();
        }
        #endregion

        #region helpers
        private ArticleService SetupArticleService(EventAggregator eventAggregator, IArticleServiceBusinessLayer bo = null, IUserNotificationService userNotService = null, ILogger logger = null, IPackagingDesignService designService = null, ITemplateService templateService = null, IServiceLocator serviceLocator = null)
        {
            if (logger == null)
                logger = new Mock<ILogger>().Object;

            var serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(s => s.Locate<ILogger>()).Returns(logger);
            serviceLocatorMock.Setup(s => s.Locate<IEventAggregatorPublisher>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(s => s.Locate<IEventAggregatorSubscriber>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(s => s.Locate<IUICommunicationService>()).Returns(uiCommunicator.Object);
            serviceLocatorMock.Setup(s => s.Locate<IPackagingDesignService>()).Returns(designService ?? new Mock<IPackagingDesignService>().Object);
            serviceLocatorMock.Setup(s => s.Locate<IUserNotificationService>()).Returns(userNotService ?? new Mock<IUserNotificationService>().Object);
            serviceLocatorMock.Setup(s => s.Locate<ITemplateService>()).Returns(templateService ?? new Mock<ITemplateService>().Object);
            var service = bo == null ? new ArticleService(serviceLocatorMock.Object, logger) : new ArticleService(serviceLocatorMock.Object, logger, bo);
            Specify.That(service).Should.Not.BeNull("Service was null after creating it");

            return service;
        }

        private Article<IProducible> GetArticleWithSingleLabel()
        {
            var kit = new Kit();
            kit.AddProducible(new Order(new Label()
                    {
                        CustomerDescription = "xDescriptioNx",
                        CustomerUniqueId = "LabelForMyBox1",
                    }, 1));
            return new Article<IProducible>()
            {
                Alias = "Article1",
                Description = "I have a description!",
                Producible = kit

            };
        }


        private void CleanupArticleRepo(Article<IProducible> article, ArticleService service)
        {
            var createdArt = service.Find(article.Id);
            if (createdArt != null)
            {
                service.Delete(createdArt);
                var shouldBeNull = service.Find(createdArt.Id);
                Specify.That(shouldBeNull).Should.BeNull();
            }
            else
            {
                createdArt = service.FindByAlias(article.Alias);
                if (createdArt != null)
                {
                    service.Delete(createdArt);
                    var shouldBeNull = service.Find(createdArt.Id);
                    Specify.That(shouldBeNull).Should.BeNull();
                }
            }
        }

        public static ArticleServiceBusinessLayer SetupArticleServiceBL(IArticleRepository repo, EventAggregator eventAggregator, IUICommunicationService uiCommService = null, IUserNotificationService userNotificationService = null, IArticleService articleService = null, ITemplateService templateService = null, IPackagingDesignService designService = null)
        {
            var repoMock = new Mock<IArticleConfigurationRepository>();
            repoMock.Setup(m => m.CurrentServiceConfiguration).Returns(new ArticleServiceConfiguration());
            return SetupArticleServiceBL(repo, eventAggregator, repoMock.Object, uiCommService, userNotificationService, articleService, templateService, designService);
        }

        public static ArticleServiceBusinessLayer SetupArticleServiceBL(IArticleRepository repo, EventAggregator eventAggregator, IArticleConfigurationRepository configRepo, IUICommunicationService uiCommService = null, IUserNotificationService userNotificationService = null, IArticleService articleService = null, ITemplateService templateService = null, IPackagingDesignService designService = null)
        {
            var serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(s => s.Locate<ILogger>()).Returns(new ConsoleLogger());
            serviceLocatorMock.Setup(s => s.Locate<IEventAggregatorPublisher>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(s => s.Locate<IArticleService>()).Returns(articleService);
            serviceLocatorMock.Setup(s => s.Locate<IEventAggregatorSubscriber>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(s => s.Locate<ITemplateService>()).Returns(templateService);
            serviceLocatorMock.Setup(s => s.Locate<IUICommunicationService>()).Returns(uiCommService);
            serviceLocatorMock.Setup(s => s.Locate<IUserNotificationService>()).Returns(userNotificationService);
            serviceLocatorMock.Setup(s => s.Locate<IPackagingDesignService>()).Returns(designService ?? new Mock<IPackagingDesignService>().Object);
            var bl = new ArticleServiceBusinessLayer(repo, serviceLocatorMock.Object, configRepo);
            Specify.That(bl).Should.Not.BeNull("BL was null after creating it");

            return bl;
        }
        #endregion
    }

}
