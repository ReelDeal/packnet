﻿using System;
using AzureQueueLogger;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Specificity;


namespace AzureQueueLoggerTest
{
    [TestClass]
    public class AzureLoggingWorkerRoleBaseTests
    {
        [TestMethod]
        [Ignore]
        public void WhenDateChanges_CreateBlobContainerIfNecessary_UpdatesBlobReference()
        {
            TestAzureLoggingWorkerRoleBase t = new TestAzureLoggingWorkerRoleBase("test", "http://blob", "DefaultEndpointsProtocol=https;AccountName=packsizeblobtest;AccountKey=QtgrXEOzLP4wPaQxMRZeT/RvN0zO8rKcF/fBjHF3P8v6AJhKHCG6U5usXzp9wpJYHhgzx/YWn8njlVkNPzczCw==");
            t.OnStart();
            var originalHashCode = t.Blob.GetHashCode();

            t.QueueInstanceDate = DateTime.Now.AddDays(-3);

            t.CreateBlobContainerIfNecessary_Test();

            Specify.That(originalHashCode).Should.Not.BeEqualTo(t.Blob.GetHashCode());
        }
    }

    public class TestAzureLoggingWorkerRoleBase : AzureLoggingWorkerRoleBase
    {

        public TestAzureLoggingWorkerRoleBase(string queueName, string blobUri, string connectionString)
        {
            Initialize(queueName, blobUri, connectionString);
        }

        public void CreateBlobContainerIfNecessary_Test()
        {
            this.CreateBlobContainerIfNecessary();
        }
    }
}