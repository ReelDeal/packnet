﻿using AzureQueueLogger;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Moq;
using Newtonsoft.Json;

using Packsize.NLogBackend;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Testing.Specificity;
using LogLevel = PackNet.Common.Logging.LogLevel;

namespace AzureQueueLoggerTest
{
    using PackNet.Common.Logging.NLogBackend;

    using ILogger = PackNet.Common.Logging.ILogger;
    using ILoggingBackend = PackNet.Common.Logging.ILoggingBackend;
    using LogManager = PackNet.Common.Logging.LogManager;

    [TestClass]
    [DeploymentItem("NLog.config")]
    public class AzureQLoggerTests
    {
        private ConnectionFactory _factory;
        private IConnection _connection;

        public Tuple<IModel, CloudQueue> Init(IAzureQLoggerAppSettings settings = null, bool clearQueue = true)
        {
            var _settings = settings ?? new AzureQLoggerAppSettings();

            //Setup logging in case composition fails
            ILoggingBackend loggingBackend = new NLogBackend("Do", @".\NLOG.config");
            LogManager.LoggingBackend = loggingBackend;

            return new Tuple<IModel, CloudQueue>(SetupRabbit(settings, clearQueue), SetupAzure(settings, clearQueue));
        }

        private CloudQueue SetupAzure(IAzureQLoggerAppSettings _settings, bool clearQueue)
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_settings.AzureConnectionString);

            // Create the queue client.
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            // Retrieve a reference to a queue.
            var _azureQueue = queueClient.GetQueueReference(_settings.QueueName);
            _azureQueue.CreateIfNotExists();
            if (clearQueue) _azureQueue.Clear();
            return _azureQueue;
        }

        private IModel SetupRabbit(IAzureQLoggerAppSettings _settings, bool clearQueue = true)
        {
            _factory = new ConnectionFactory { HostName = _settings.RabbitHost };
            _connection = _factory.CreateConnection();
            var _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(_settings.RabbitExchange, "topic", false, true, null);
            var connectionIsOpen = _channel.IsOpen;
            if (connectionIsOpen)
            {
                _channel.QueueDeclare(_settings.QueueName, true, false, false, null);
                if (clearQueue) _channel.QueuePurge(_settings.QueueName);
            }
            return _channel;
        }

        [TestCleanup]
        public void TestCleanup()
        {
        }

        [TestMethod]
        public void ShouldValidateAppSettingQueueNameToOnlyHaveLowerCaseOrDigits()
        {
            var ap = new AzureQLoggerAppSettings {QueueName = "staplesOrlando"};
            var mLogger = new Mock<ILogger>(MockBehavior.Loose);
            var tester = new AzureQLogger(ap, mLogger.Object);
            mLogger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Azure Queue Name must only contain lower case letters and/or numbers.") && s.Contains(ap.QueueName))), Times.Once);
            tester.Dispose();

            ap.QueueName = "peridsin";  // valid string.
            tester = new AzureQLogger(ap, mLogger.Object);
            mLogger.Verify(l => l.Log(It.IsAny<LogLevel>(), It.Is<string>(s => s.Contains(ap.QueueName))), Times.Never);
            tester.Dispose();

            ap.QueueName = "perids.in";
            tester = new AzureQLogger(ap, mLogger.Object);
            mLogger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Azure Queue Name must only contain lower case letters and/or numbers.") && s.Contains(ap.QueueName))), Times.Once);
            tester.Dispose();

            ap.QueueName = "underscore_in";
            tester = new AzureQLogger(ap, mLogger.Object);
            mLogger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Azure Queue Name must only contain lower case letters and/or numbers.") && s.Contains(ap.QueueName))), Times.Once);
            tester.Dispose();
            ap.QueueName = "underscore*in";
            tester = new AzureQLogger(ap, mLogger.Object);
            mLogger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Azure Queue Name must only contain lower case letters and/or numbers.") && s.Contains(ap.QueueName))), Times.Once);
            tester.Dispose();

            ap.QueueName = "underscore!in";
            tester = new AzureQLogger(ap, mLogger.Object);
            mLogger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Azure Queue Name must only contain lower case letters and/or numbers.") && s.Contains(ap.QueueName))), Times.Once);
            tester.Dispose();
        }

        [TestMethod]
        public void CtorFailsToReadAppSettingsEmptyValue()
        {
            var ap = new Mock<IAzureQLoggerAppSettings>();
            ap.SetupGet(a => a.RabbitExchange).Returns(string.Empty);
            ap.SetupGet(a => a.RabbitHost).Returns(string.Empty);
            var tester = new AzureQLogger(ap.Object);

            Specify.That(tester.LogName).Should.Not.BeNull("AzureQLogger");
            Specify.That(tester).Should.Not.BeNull();
            tester.LogException(LogLevel.Error, "test", new ApplicationException());
            tester.Log(LogLevel.Error, "test");

        }

        [TestMethod]
        public void CtorFailsToReadAppSettingsError()
        {
            var ap = new Mock<IAzureQLoggerAppSettings>();
            ap.SetupGet(a => a.RabbitExchange).Throws(new Exception());

            var tester = new AzureQLogger(ap.Object);
            Specify.That(tester.LogName).Should.Not.BeNull("AzureQLogger");
            Specify.That(tester).Should.Not.BeNull();

            //ensure that log can be called without throwing errors when the setup of the logger was incorrect.
            tester.LogException(LogLevel.Error, "test", new ApplicationException());
            tester.Log(LogLevel.Error, "test");

        }


        [TestMethod]
        public void CtorFailsToReadAppSettingsErrorConnectionString()
        {
            var ap = new AzureQLoggerAppSettings();
            ap.AzureConnectionString = "more";

            var tester = new AzureQLogger(ap);
            Specify.That(tester.LogName).Should.Not.BeNull("AzureQLogger");
            Specify.That(tester).Should.Not.BeNull();

            //ensure that log can be called without throwing errors when the setup of the logger was incorrect.
            tester.LogException(LogLevel.Error, "test", new ApplicationException());
            tester.Log(LogLevel.Error, () => "test");

        }


        [TestMethod]
        [Ignore]// ignoring because it uses actual service and needs rabbit installed on running machine.  Build server does not have.
        public void ShouldLogLevelSpecified()
        {
            var _settings = new AzureQLoggerAppSettings();
            _settings.QueueName = "shouldloglevelspecified";

            var x = Init(_settings);
            var tester = new AzureQLogger(_settings);
            tester.DisableAzure();

            tester.Log(LogLevel.Trace, ()=> "trace");
            tester.Log(LogLevel.Error, () => "error");
            tester.Log(LogLevel.Warning, () => "warning");
            tester.Log(LogLevel.Debug, () => "debug");
            tester.LogException(LogLevel.Debug, "debug exception", new Exception("exception bla bla"));

            Thread.Sleep(1000);
            var localQueue = new RabbitConsumer(_settings.RabbitHost, _settings.QueueName);
            var result = localQueue.PopXorAllIfLess(10);
            Specify.That(result.Count()).Should.BeEqualTo(4);
            Specify.That(result.FirstOrDefault(m => m.LogLevel == LogLevel.Trace)).Should.BeNull();

        }

        [TestMethod]
        [Ignore]// ignoring because it uses actual service and needs rabbit installed on running machine.  Build server does not have.
        public void ShouldFormatMessageAsExpectedUsingLogWithException()
        {
            var _settings = new AzureQLoggerAppSettings();
            _settings.QueueName = "shouldformatmessageasexpectedusinglogwithexception";
            var x = Init(_settings);
            var ex = new ApplicationException("this is my eception");
            var message = "this is a message without specifying log level or error";

            var tester = new AzureQLogger(_settings);
            tester.DisableAzure();

            tester.LogException(LogLevel.Warning, message, ex);
            Thread.Sleep(1000);
            var localQueue = new RabbitConsumer(_settings.RabbitHost, _settings.QueueName);
            var result = localQueue.PopXorAllIfLess(1);

            var testMessage = result.First();
            Specify.That(testMessage.Message).Should.BeEqualTo(message);
            Specify.That(testMessage.LogLevel).Should.BeEqualTo(LogLevel.Warning);
            Specify.That(testMessage.Exception.Contains(ex.Message)).Should.BeTrue();
        }

        [TestMethod]
        [Ignore]// ignoring because it uses actual service and needs rabbit installed on running machine.  Build server does not have.
        public void ShouldFormatMessageAsExpectedUsingLogWithLevel()
        {
            var _settings = new AzureQLoggerAppSettings();
            _settings.QueueName = "shouldformatmessageasexpectedusinglogwithlevel";
            Init(_settings);
            var message = "this is a message without specifying log level or error";

            var tester = new AzureQLogger(_settings);
            tester.DisableAzure();

            tester.Log(LogLevel.Error, message);
            Thread.Sleep(1000);
            var localQueue = new RabbitConsumer(_settings.RabbitHost, _settings.QueueName);
            var result = localQueue.PopXorAllIfLess(1);

            var testMessage = result.First();
            Specify.That(testMessage.Message).Should.BeEqualTo(message);
            Specify.That(testMessage.LogLevel).Should.BeEqualTo(LogLevel.Error);

        }

        [TestMethod]
        [Ignore]// ignoring because it uses actual service and needs rabbit installed on running machine.  Build server does not have.
        public void ShouldFormatMessageAsExpectedUsingLog()
        {

            var _settings = new AzureQLoggerAppSettings();
            _settings.QueueName = "shouldformatmessageasexpectedusinglog";
            Init(_settings);
            var message = "this is a message without specifying log level";

            var tester = new AzureQLogger(_settings);
            tester.DisableAzure();

            tester.Log(LogLevel.Info, message);
            Thread.Sleep(2000);
            var localQueue = new RabbitConsumer(_settings.RabbitHost, _settings.QueueName);
            var result = localQueue.PopXorAllIfLess(1);

            var testMessage = result.First();
            Specify.That(testMessage.Message).Should.BeEqualTo(message);
            Specify.That(testMessage.LogLevel).Should.BeEqualTo(LogLevel.Info);

        }


        [TestMethod]
        [Ignore]// ignoring because it uses actual service and needs rabbit installed on running machine.  Build server does not have.
        public void CanReadWriteAzureWithCompression()
        {
            var _settings = new AzureQLoggerAppSettings();
            _settings.QueueName = "readwritetest";
            _settings.MillisecondsToWaitBetweenAzurePushes = 1000;
            var origCount = 100;
            _settings.AzureBatchCount = origCount;
            _settings.CompressToAzure = true;

            var x = Init(_settings);

            var tester = new AzureQLogger(_settings);

            tester.Log(LogLevel.Info,
                "**1** message asdvvvvvvvvddddddfa   asdddd    dddddddddf   adssssssssssssasdfaaaaaaaaaaaadsaaaaaaaaaaaaaaaaaaaaaaasss   ssssssfaaaasdfaaaaaaaaaaa1aaaaaaass5ssssf");
            tester.Log(LogLevel.Error,
                "**2** message asdddddddvddddddfa   asdddd    dddddddddf   adssssssssssssasdfaaaaaaaaaaaaaaa235aaaaaaaaaaaaaaaaasss   ssssssfaaaaaaaaaa34aaa3aaaaaaaaaaaasssssssssf");
            tester.Log(LogLevel.Warning,
                "**3** message asddddddddddddddddddddfa   asdddd    dddddddddf   adssssssssssssasdfaaaaaaahgjaaaaaaaaaaaaaaaaaaasdfaaasss   ssssssfaaaaaaanhv453sfaaaaasssssssssf");
            tester.LogException(LogLevel.Debug, "**4** bla 564625 ", new Exception("exception bla bla"));

            Thread.Sleep(_settings.MillisecondsToWaitBetweenAzurePushes);
            while (true)
            {
                if (x.Item1.QueueDeclarePassive(_settings.QueueName).MessageCount <= 0)
                    break;
            }

            // dequeue all
            var listOfAll = new List<DotServerLogMessage>();
            while (true)
            {
                var m = x.Item2.GetMessage();
                if (m == null) break;

                var str = AzureQueueHelper.Decompress(m.AsBytes);
                var list = JsonConvert.DeserializeObject<List<DotServerLogMessage>>(str);
                listOfAll.AddRange(list);
            }
            var one = listOfAll.Where(m => m.Message.Contains("**1**"));
            Specify.That(one).Should.Not.BeNull();
            var two = listOfAll.Where(m => m.Message.Contains("**2**"));
            Specify.That(two).Should.Not.BeNull();
            var three = listOfAll.Where(m => m.Message.Contains("**3**"));
            Specify.That(three).Should.Not.BeNull();
            var four = listOfAll.Where(m => m.Message.Contains("**4**"));
            Specify.That(four).Should.Not.BeNull();

        }


        [TestMethod]
        [Ignore]// ignoring because it uses actual service and needs rabbit installed on running machine.  Build server does not have.
        public void CanReadFromRobelRobel()
        {
            var _settings = new AzureQLoggerAppSettings();
            _settings.QueueName = "load221";
            _settings.MillisecondsToWaitBetweenAzurePushes = 3000;
            var origCount = 100;
            _settings.AzureBatchCount = origCount;
            _settings.CompressToAzure = true;

            var x = Init(_settings, false);

            // dequeue all
            var listOfAll = new List<DotServerLogMessage>();
            while (true)
            {
                Parallel.For(0, 1, i => DisplayMessage(x, _settings));
            }
        }

        private static void DisplayMessage(Tuple<IModel, CloudQueue> x, AzureQLoggerAppSettings _settings)
        {
            try
            {
                var m = x.Item2.GetMessage(null);
                if (m == null)
                {
                    Debug.WriteLine("sleepting nothing to read");
                    Thread.Sleep(10000);
                    return;
                }
                ;

                var str = _settings.CompressToAzure ? AzureQueueHelper.Decompress(m.AsBytes) : m.AsString;

                var list = JsonConvert.DeserializeObject<List<DotServerLogMessage>>(str);
                var y = list.Count();
                list.ForEach(mm => Debug.WriteLine(mm.TimeStamp + " (" + y + ") " + mm.LogLevel+ " -" + mm.Message));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
