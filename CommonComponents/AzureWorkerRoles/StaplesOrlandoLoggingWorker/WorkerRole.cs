using AzureQueueLogger;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace StaplesOrlandoLoggingWorker
{
    public class WorkerRole : AzureLoggingWorkerRoleBase
    {
        public WorkerRole()
        {
            Initialize("staplesorlando", "https://packsizestaples.blob.core.windows.net/staplesorlando", RoleEnvironment.GetConfigurationSettingValue("StorageConnectionString"));
        }
    }
}
