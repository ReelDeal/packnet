﻿namespace Packsize.ImportExportLogicTests
{
    using System.Globalization;
    using System.IO;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.FileHandling.ImportExportLogic;
    using PackNet.Common.FileHandling.ImportExportLogic.Types;

    using Packsize.ImportExportLogic;

    using Testing.Specificity;

    [TestClass]
    public class ImportExportLogicSettingsTests
    {
        private ImportExportLogicSettings importExportLogicSettings;
        private string settingsFilename;

        [TestInitialize]
        public void Setup()
        {
            importExportLogicSettings = new ImportExportLogicSettings();
            settingsFilename = "ImportExportLogicSettings.cfg.xml";
            if (File.Exists(settingsFilename))
            {
                File.Delete(settingsFilename);
            }
        }

        [TestMethod]
        public void SettingsShouldBeSerializableToDisk()
        {
            importExportLogicSettings.Serialize(settingsFilename);
            Specify.That(File.Exists(settingsFilename));
        }

        [TestMethod]
        public void SettingsShouldHandleBothPointAsDecimalSeparatorEvenWhenSetupWithComma()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("sv-SE");
            importExportLogicSettings = ImportExportLogicSettings.Deserialize("TestData\\PointDecimal.xml");
        }

        [TestMethod]
        public void ChangedSettingsShouldBePersistedToFile()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("sv-SE");
            importExportLogicSettings = ImportExportLogicSettings.Deserialize("TestData\\PointDecimal.xml");
            importExportLogicSettings.ValueMapper[0].Multiplier = 12.3;
            importExportLogicSettings.ValueMapper[0].Precision = 2;
            importExportLogicSettings.Serialize("TestData\\PointDecimal.xml");

            importExportLogicSettings = ImportExportLogicSettings.Deserialize("TestData\\PointDecimal.xml");
            Assert.AreEqual(12.3, importExportLogicSettings.ValueMapper[0].Multiplier);
            Assert.AreEqual(2, importExportLogicSettings.ValueMapper[0].Precision);
        }

        [TestMethod]
        public void ShouldHandleToFromConversionsFromFile()
        {
            importExportLogicSettings = ImportExportLogicSettings.Deserialize("TestData\\ImportExportSettingsWithInputOutputMappings.xml");

            Specify.That(importExportLogicSettings.CommandColumnName == "Command");
            Specify.That(importExportLogicSettings.CommandMappings.Count == 4);
            Specify.That(importExportLogicSettings.CommandMappings[0].ExternalCommand == "Update");
            Specify.That(importExportLogicSettings.CommandMappings[0].InternalCommand == Command.Save);
            Specify.That(importExportLogicSettings.CommandMappings[1].ExternalCommand == "New");
            Specify.That(importExportLogicSettings.CommandMappings[1].InternalCommand == Command.Save);
            Specify.That(importExportLogicSettings.CommandMappings[2].ExternalCommand == "Remove");
            Specify.That(importExportLogicSettings.CommandMappings[2].InternalCommand == Command.Delete);
            Specify.That(importExportLogicSettings.CommandMappings[3].ExternalCommand == "Delete");
            Specify.That(importExportLogicSettings.CommandMappings[3].InternalCommand == Command.Delete);
            Specify.That(importExportLogicSettings.ValueMapper != null);
            Specify.That(importExportLogicSettings.ValueMapper.Count == 5);
            Specify.That(importExportLogicSettings.ValueMapper[0].Field == "DesignId");
            Specify.That(importExportLogicSettings.ValueMapper[0].Keys.Count == 2);
            Specify.That(importExportLogicSettings.ValueMapper[1].Field == "CorrugateQuality");
            Specify.That(importExportLogicSettings.ValueMapper[1].Keys.Count == 2);
        }

        [TestMethod]
        public void SettingsShouldHaveDefaultValues()
        {
            Specify.That(importExportLogicSettings.CommandColumnName == "Command");
            Specify.That(importExportLogicSettings.CommandMappings.Count == 4);
            Specify.That(importExportLogicSettings.CommandMappings[0].ExternalCommand == "Update");
            Specify.That(importExportLogicSettings.CommandMappings[0].InternalCommand == Command.Save);
            Specify.That(importExportLogicSettings.CommandMappings[1].ExternalCommand == "New");
            Specify.That(importExportLogicSettings.CommandMappings[1].InternalCommand == Command.Save);
            Specify.That(importExportLogicSettings.CommandMappings[2].ExternalCommand == "Remove");
            Specify.That(importExportLogicSettings.CommandMappings[2].InternalCommand == Command.Delete);
            Specify.That(importExportLogicSettings.CommandMappings[3].ExternalCommand == "Delete");
            Specify.That(importExportLogicSettings.CommandMappings[3].InternalCommand == Command.Delete);
            Specify.That(importExportLogicSettings.ValueMapper != null);
            Specify.That(importExportLogicSettings.ValueMapper.Count == 2);
            Specify.That(importExportLogicSettings.ValueMapper[0].Field == "DesignId");
            Specify.That(importExportLogicSettings.ValueMapper[0].Keys.Count == 2);
            Specify.That(importExportLogicSettings.ValueMapper[1].Field == "CorrugateQuality");
            Specify.That(importExportLogicSettings.ValueMapper[1].Keys.Count == 1);
            importExportLogicSettings.Serialize(@"ImportExportLogicSettings.xml");
            ImportExportLogicSettings.Deserialize(@"ImportExportLogicSettings.xml");
        }

        [TestMethod]
        public void SettingsShouldBeDeserializableFromDisk()
        {
            GivenAProperSettingsFileWithValuesSet();
            var deserializedSettings = ImportExportLogicSettings.Deserialize(settingsFilename);
            Specify.That(deserializedSettings).Should.Not.BeNull();
            AssertValues(deserializedSettings);
        }

        private void AssertValues(ImportExportLogicSettings settings)
        {
            Specify.That(importExportLogicSettings.CommandColumnName == "CommandTest");
        }

        private void GivenAProperSettingsFileWithValuesSet()
        {
            importExportLogicSettings.CommandColumnName = "CommandTest";
            importExportLogicSettings.Serialize(settingsFilename);
        }
    }
}
