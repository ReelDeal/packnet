﻿using System.Configuration;

namespace Packsize.ImportExportLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.FileHandling.ImportExportLogic;

    using Packsize.ImportExportLogic;
    using Testing.Specificity;

    [TestClass]
    public class ParserTests
    {
        #region Tests

        [TestMethod]
        public void ShouldParseDotDecimalValueRequests()
        {
            var parsedRequests = Parser.Parse(
                this.GetDotRequestsList(),
                new ValueMapper());

            this.AssertThatDotDecimalValueRequestsAreParsed(parsedRequests);
        }

        [TestMethod]
        public void ShouldParseCommaDecimalValueRequests()
        {
            var parsedRequests = Parser.Parse(
                this.GetCommaRequestsList(),
                new ValueMapper());

            this.AssertThatCommaDecimalValueRequestsAreParsed(parsedRequests);
        }

        [TestMethod]
        public void ShouldParseAllRequests()
        {
            var parsedRequests = Parser.Parse(
                this.GetThreeRequestsList(),
                new ValueMapper());

            Assert.AreEqual(4, parsedRequests.Count());
        }

        [TestMethod]
        public void ShouldMapValuesViaSettings()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId" });
            mapper[0].Add("1", "2");

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][1] == "Length");
            Specify.That(parsedRequests[1][1] == "1");
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "2");
        }

        [TestMethod]
        public void ShouldMapValuesToFirstMatchInMappingTable()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId" });
            mapper[0].Add("1", "2");
            mapper[0].Add("2", "3");

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "2");
        }

        [TestMethod]
        public void ShouldMapValuesInFromFieldToField()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId", FromField = "Article" });

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count()).Should.BeEqualTo(2);
            Specify.That(parsedRequests[0][1]).Should.BeEqualTo("Length");
            Specify.That(parsedRequests[1][1]).Should.BeEqualTo("1");
            Specify.That(parsedRequests[0][2]).Should.BeEqualTo("DesignId");
            Specify.That(parsedRequests[1][2]).Should.BeEqualTo("test");
        }

        [TestMethod]
        public void ShouldMapValuesInFromFieldToNonExistentField()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "PrintInfoField1", FromField = "Article" });

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count()).Should.BeEqualTo(2);
            Specify.That(parsedRequests[0].Contains("PrintInfoField1")).Should.BeTrue();
            Specify.That(parsedRequests[1][parsedRequests[0].IndexOf("PrintInfoField1")]).Should.BeEqualTo("test");
        }

        [TestMethod]
        public void ShouldMapValuesInFromFieldBeforeMappingTableIsApplied()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId", FromField = "PrintInfoField1", DefaultValue = "201"});
            mapper[0].Add("RSC", "202");
            mapper[0].Add("3", "203");

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestListForDesignId(),
                mapper);

            Specify.That(parsedRequests.Count()).Should.BeEqualTo(2);
            Specify.That(parsedRequests[0].Contains("PrintInfoField1")).Should.BeTrue();
            Specify.That(parsedRequests[1][parsedRequests[0].IndexOf("PrintInfoField1")]).Should.BeEqualTo("RSC");
            Specify.That(parsedRequests[1][parsedRequests[0].IndexOf("DesignId")]).Should.BeEqualTo("202");
        }

        [TestMethod]
        public void ShouldMapValuesInFromFieldBeforeMappingTableIsApplied_AndUseDefaultIfNoMatchIsFoundInMappingTable()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId", FromField = "PrintInfoField1", DefaultValue = "201" });
            mapper[0].Add("2", "202");
            mapper[0].Add("3", "203");

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestListForDesignId(),
                mapper);

            Specify.That(parsedRequests.Count()).Should.BeEqualTo(2);
            Specify.That(parsedRequests[0].Contains("PrintInfoField1")).Should.BeTrue();
            Specify.That(parsedRequests[1][parsedRequests[0].IndexOf("PrintInfoField1")]).Should.BeEqualTo("RSC");
            Specify.That(parsedRequests[1][parsedRequests[0].IndexOf("DesignId")]).Should.BeEqualTo("201");
        }

        [TestMethod]
        public void ShouldReportErrorsInFromFieldMapping()
        {
            try
            {
                var mapper = new ValueMapper();
                mapper.Add(new FieldMapping() { Field = "DesignId", FromField = "invalid" });

                Parser.Parse(this.GetMapValuesRequestList(), mapper);
            }
            catch (ConfigurationException ce)
            {
                Specify.That(ce.Message).Should.BeEqualTo("Invalid FromField value 'invalid' in mapping for field 'DesignId'");
            }
            catch (Exception)
            {
                Assert.Fail("Wrong exception");
            }

        }

        [TestMethod]
        public void ShouldMapDefaultValuesEvenIfValueFieldDontExist()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId" });
            mapper[0].DefaultValue = "3";

            var parsedRequests = Parser.Parse(
                this.GetRequestsWithoutMappedHeaders(),
                mapper);

            Specify.That(parsedRequests.Count).Should.BeEqualTo(2);
            Specify.That(parsedRequests[0].Count).Should.BeEqualTo(4);
            Specify.That(parsedRequests[0][0]).Should.BeEqualTo("Length");
            Specify.That(parsedRequests[1][0]).Should.BeEqualTo("100");
            Specify.That(parsedRequests[0][1]).Should.BeEqualTo("Width");
            Specify.That(parsedRequests[1][1]).Should.BeEqualTo("200");
            Specify.That(parsedRequests[0][2]).Should.BeEqualTo("Height");
            Specify.That(parsedRequests[1][2]).Should.BeEqualTo("300");
            Specify.That(parsedRequests[0][3]).Should.BeEqualTo("DesignId");
            Specify.That(parsedRequests[1][3]).Should.BeEqualTo("3");
        }

        [TestMethod]
        public void ShouldNotAddMappedColumnWIthNoDefaultValue()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId" });
            mapper.Add(new FieldMapping() { Field = "Command" });

            var parsedRequests = Parser.Parse(
                this.GetRequestsWithoutMappedHeaders(),
                mapper);

            Specify.That(parsedRequests.Count).Should.BeEqualTo(2);
            Specify.That(parsedRequests[0].Count).Should.BeEqualTo(3);
            Specify.That(parsedRequests[0][0]).Should.BeEqualTo("Length");
            Specify.That(parsedRequests[1][0]).Should.BeEqualTo("100");
            Specify.That(parsedRequests[0][1]).Should.BeEqualTo("Width");
            Specify.That(parsedRequests[1][1]).Should.BeEqualTo("200");
            Specify.That(parsedRequests[0][2]).Should.BeEqualTo("Height");
            Specify.That(parsedRequests[1][2]).Should.BeEqualTo("300");
        }

        [TestMethod]
        public void ShouldMultiplyValuesAccordingToMultiplier()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "Length", Multiplier = 0.1 });

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][1] == "Length");
            Specify.That(parsedRequests[1][1] == "0.1");
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "1");

            mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "Length" });

            parsedRequests = Parser.Parse(
                this.GetMapValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][1] == "Length");
            Specify.That(parsedRequests[1][1] == "1");
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "1");

            mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "PickZone", Multiplier = 2 });

            parsedRequests = Parser.Parse(
                this.GetMapValuesWithPickZoneRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][1] == "Length");
            Specify.That(parsedRequests[1][1] == "1");
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "1");
            Specify.That(parsedRequests[0][3] == "PickZone");
            Specify.That(parsedRequests[1][3] == "a");
        }

        [TestMethod]
        public void ShouldMapSingleFieldValue()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "Length", Multiplier = 100 });

            string reply = Parser.Parse("Length", "300", mapper);

            Assert.AreEqual("30000", reply);
        }

        [TestMethod]
        public void ShouldReturnValueIfFieldIsMissingInMapping()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "Length", Multiplier = 100 });

            string height = "200";
            string reply = Parser.Parse("Height", height, mapper);

            Assert.AreEqual(height, reply);
        }

        [TestMethod]
        public void ShouldMapToDefaultValueIfInValueIsMissingInMappings()
        {
            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "DesignId" });
            mapper[0].Add("2", "2");
            mapper[0].DefaultValue = "3";

            var parsedRequests = Parser.Parse(
                this.GetMapValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][1] == "Length");
            Specify.That(parsedRequests[1][1] == "1");
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "3");
        }

        [TestMethod]
        public void ShouldApplyPrecisionToValuesAccordingToPrecision()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "Length", Precision = 0 });

            var parsedRequests = Parser.Parse(
                this.GetMapPrecisionValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][1] == "Length");
            Specify.That(parsedRequests[1][1] == "22");
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "1");
        }

        [TestMethod]
        public void ShouldApplyPrecisionTwoToValuesAccordingToPrecision()
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");

            var mapper = new ValueMapper();
            mapper.Add(new FieldMapping() { Field = "Length", Precision = 2 });

            var parsedRequests = Parser.Parse(
                this.GetMapPrecisionValuesRequestList(),
                mapper);

            Specify.That(parsedRequests.Count() == 2);
            Specify.That(parsedRequests[0][1] == "Length");
            Specify.That(parsedRequests[1][1] == "21.62");
            Specify.That(parsedRequests[0][2] == "DesignId");
            Specify.That(parsedRequests[1][2] == "1");
        }

        #endregion

        #region Implementation

        private List<List<string>> GetThreeRequestsList()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "SerialNumber", "Length", "Width", "Height" });
            requests.Add(new List<string>() { "123", "100", "200", "300" });
            requests.Add(new List<string>() { "123", "100", "200", "300" });
            requests.Add(new List<string>() { "123", "100", "200", "300" });
            return requests;
        }

        private List<List<string>> GetDotRequestsList()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "SerialNumber", "Length", "Width", "Height" });
            requests.Add(new List<string>() { "123", "10.2", "20.2", "30.2" });
            return requests;
        }

        private List<List<string>> GetCommaRequestsList()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "SerialNumber", "Length", "Width", "Height" });
            requests.Add(new List<string>() { "123", "10,2", "20,2", "30,2" });
            return requests;
        }

        private List<List<string>> GetMapValuesRequestListForDesignId()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "Command", "Length", "DesignId", "PrintInfoField1" });
            requests.Add(new List<string>() { "Create", "1", "1", "RSC" });
            return requests;
        }

        private List<List<string>> GetMapValuesRequestList()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "Command", "Length", "DesignId", "Article" });
            requests.Add(new List<string>() { "Create", "1", "1", "test" });
            return requests;
        }

        private List<List<string>> GetRequestsWithoutMappedHeaders()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "Length", "Width", "Height" });
            requests.Add(new List<string>() { "100", "200", "300" });
            return requests;
        }

        private List<List<string>> GetMapPrecisionValuesRequestList()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "Command", "Length", "DesignId" });
            requests.Add(new List<string>() { "Create", "21.62262", "1" });
            return requests;
        }

        private List<List<string>> GetMapValuesWithPickZoneRequestList()
        {
            List<List<string>> requests = new List<List<string>>();
            requests.Add(new List<string>() { "Command", "Length", "DesignId", "PickZone" });
            requests.Add(new List<string>() { "Create", "1", "1", "a" });
            return requests;
        }

        private void AssertThatNoFieldsAreIgnored(List<List<string>> parsedList)
        {
            Specify.That(parsedList[0].Count).Should.BeEqualTo(4);

            Specify.That(parsedList[0][0]).Should.BeEqualTo("SerialNumber");
            Specify.That(parsedList[0][1]).Should.BeEqualTo("Length");
            Specify.That(parsedList[0][2]).Should.BeEqualTo("Width");
            Specify.That(parsedList[0][3]).Should.BeEqualTo("Height");

            Specify.That(parsedList[1][0]).Should.BeEqualTo("123");
            Specify.That(parsedList[1][1]).Should.BeEqualTo("100");
            Specify.That(parsedList[1][2]).Should.BeEqualTo("200");
            Specify.That(parsedList[1][3]).Should.BeEqualTo("300");
        }

        private void AssertThatFieldsAreIgnored(List<List<string>> parsedList)
        {
            Specify.That(parsedList[0].Count).Should.BeEqualTo(2);

            Specify.That(parsedList[0][0]).Should.BeEqualTo("SerialNumber");
            Specify.That(parsedList[0][1]).Should.BeEqualTo("Height");

            Specify.That(parsedList[1][0]).Should.BeEqualTo("123");
            Specify.That(parsedList[1][1]).Should.BeEqualTo("300");
        }

        private void AssertThatCommaDecimalValueRequestsAreParsed(List<List<string>> parsedList)
        {
            Specify.That(parsedList[0].Count).Should.BeEqualTo(4);

            Specify.That(parsedList[0][0]).Should.BeEqualTo("SerialNumber");
            Specify.That(parsedList[0][1]).Should.BeEqualTo("Length");
            Specify.That(parsedList[0][2]).Should.BeEqualTo("Width");
            Specify.That(parsedList[0][3]).Should.BeEqualTo("Height");

            Specify.That(parsedList[1][0]).Should.BeEqualTo("123");
            Specify.That(parsedList[1][1]).Should.BeEqualTo("10,2");
            Specify.That(parsedList[1][2]).Should.BeEqualTo("20,2");
            Specify.That(parsedList[1][3]).Should.BeEqualTo("30,2");
        }

        private void AssertThatDotDecimalValueRequestsAreParsed(List<List<string>> parsedList)
        {
            Specify.That(parsedList[0].Count).Should.BeEqualTo(4);

            Specify.That(parsedList[0][0]).Should.BeEqualTo("SerialNumber");
            Specify.That(parsedList[0][1]).Should.BeEqualTo("Length");
            Specify.That(parsedList[0][2]).Should.BeEqualTo("Width");
            Specify.That(parsedList[0][3]).Should.BeEqualTo("Height");

            Specify.That(parsedList[1][0]).Should.BeEqualTo("123");
            Specify.That(parsedList[1][1]).Should.BeEqualTo("10.2");
            Specify.That(parsedList[1][2]).Should.BeEqualTo("20.2");
            Specify.That(parsedList[1][3]).Should.BeEqualTo("30.2");
        }

        private void AssertThatSingleFieldIsIgnored(List<List<string>> parsedList)
        {
            Specify.That(parsedList[0].Count).Should.BeEqualTo(3);

            Specify.That(parsedList[0][0]).Should.BeEqualTo("SerialNumber");
            Specify.That(parsedList[0][1]).Should.BeEqualTo("Length");
            Specify.That(parsedList[0][2]).Should.BeEqualTo("Width");

            Specify.That(parsedList[1][0]).Should.BeEqualTo("123");
            Specify.That(parsedList[1][1]).Should.BeEqualTo("100");
            Specify.That(parsedList[1][2]).Should.BeEqualTo("200");
        }

        #endregion
    }
}
