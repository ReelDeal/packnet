﻿namespace Packsize.ImportExportLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.ExceptionServices;
    using System.Security.Permissions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using PackNet.Common.ComponentModel.DataImporter;
    using PackNet.Common.FileHandling.ImportExportLogic;
    using PackNet.Common.FileHandling.ImportExportLogic.Types;
    using PackNet.Common.Logging;

    using Packsize.ComponentModel.DataImporter;
    using Packsize.ImportExportLogic;

    using Testing.Specificity;

    [TestClass]
    public class ImportExportLogicTests : IDisposable
    {
        #region Fields

        private Mock<ValueMapper> valueMapperMock;
        private Mock<IDataImporter> cartonImporterMock;
        private Mock<IDataImporter> cartonImporterMock2;
        private ImportExportLogicSettings importExportLogicSettingsWithMock;
        private ImportExportLogic importExportLogicWithMocks;

        private ImportExportLogic importExportLogic;
        private ImportExportLogicSettings importExportLogicSettings;
        private bool waitingForRequestEvent;
        private bool parsingExceptionThrown;
        private string commandColumnName;
        private Guid failedFileId;

        #endregion

        #region Tests

        [TestInitialize]
        public void Setup()
        {
            cartonImporterMock = new Mock<IDataImporter>();
            cartonImporterMock.As<IDisposable>();
            importExportLogicSettings = new ImportExportLogicSettings();
            commandColumnName = "Command";

            waitingForRequestEvent = false;
            parsingExceptionThrown = false;

            importExportLogic = new ImportExportLogic(
                importExportLogicSettings,
                cartonImporterMock.Object,
                new Mock<ILogger>(MockBehavior.Loose).Object);
        }

        [TestCleanup]
        public void Teardown()
        {
            if (importExportLogic != null)
            {
                importExportLogic.Dispose();
                importExportLogic = null;
            }
        }

        [TestMethod]
        [SecurityPermission(SecurityAction.LinkDemand)]
        public void CartonCommandCreationReceivedCallbackIsCalledWhenImportingSuccessfully()
        {
            AppDomain.CurrentDomain.FirstChanceException += OnFirstChanceException;

            importExportLogic.CartonCommandCreationRequested += OnImportExportLogicCartonCommandCreationRequested;

            GivenImportExportLogicIsSetUp(commandColumnName, OnCommandCreationRequested);

            ParseRequestsFromSupplier(GetThreeRequestsList());

            Specify.That(parsingExceptionThrown).Should.BeFalse();
            Specify.That(waitingForRequestEvent).Should.BeFalse();

            importExportLogic.CartonCommandCreationRequested -= OnImportExportLogicCartonCommandCreationRequested;

            AppDomain.CurrentDomain.FirstChanceException -= OnFirstChanceException;
        }

        [TestMethod]
        [SecurityPermission(SecurityAction.LinkDemand)]
        public void ShouldRaiseCartonCommandCreatinFailedWhenCartonImporterRaisesParsingFailed()
        {
            importExportLogic.CartonCommandCreationFailed += OnCommandCreationFailed;

            var fileId = new Guid();
            cartonImporterMock.Raise(c => c.ParsingFailed += null, new ParsingFailedEventArgs(null, GetThreeRequestsList(), fileId));

            Specify.That(failedFileId).Should.BeEqualTo(fileId);
        }

        [TestMethod]
        [SecurityPermission(SecurityAction.LinkDemand)]
        public void ShouldRaiseImportFailedWhenCartonImporterRaisesImportFailed()
        {
            var importFailedMessage = string.Empty;
            var importFailedRaised = false;
            importExportLogic.ImportFailed += (a, b) => { importFailedRaised = true; importFailedMessage = "Import failed message"; };

            cartonImporterMock.Raise(c => c.ImportFailed += null, new ImportFailedEventArgs(importFailedMessage));

            Specify.That(importFailedRaised).Should.BeTrue();
            Specify.That(importFailedMessage).Should.BeEqualTo("Import failed message");
        }

        [TestMethod]
        [SecurityPermission(SecurityAction.LinkDemand)]
        public void ShouldRaiseImportFailedWhenParsingRequestedAndParsingFails()
        {
            SetupImportExportLogicWithMocks();
            valueMapperMock.CallBase = true;

            var parsingExceptionRaised = false;
            var parsingExceptionMessage = string.Empty;
            importExportLogicWithMocks.ImportFailed += (a, b) => { parsingExceptionRaised = true; parsingExceptionMessage = "Import failed"; };
            
            cartonImporterMock2.Raise(c => c.ParsingRequested += null, new ParsingRequestedEventArgs(GetThreeRequestsListWithMissingValue(), new Guid()));

            Specify.That(parsingExceptionRaised).Should.BeTrue();
            Specify.That(parsingExceptionMessage).Should.BeEqualTo("Import failed");
        }

        [TestMethod]
        [SecurityPermission(SecurityAction.LinkDemand)]
        public void ShouldRaiseImportFailedWhenParsingFailedAndParsingFails()
        {
            SetupImportExportLogicWithMocks();
            valueMapperMock.CallBase = true;

            var parsingExceptionRaised = false;
            var parsingExceptionMessage = string.Empty;
            importExportLogicWithMocks.ImportFailed += (a, b) => { parsingExceptionRaised = true; parsingExceptionMessage = "Import failed"; };

            cartonImporterMock2.Raise(
                c => c.ParsingFailed += null,
                new ParsingFailedEventArgs(GetThreeRequestsList(), GetThreeRequestsListWithMissingValue(), new Guid()));

            Specify.That(parsingExceptionRaised).Should.BeTrue();
            Specify.That(parsingExceptionMessage).Should.BeEqualTo("Import failed");
        }

        #endregion

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool dispose)
        {
            if (dispose)
            {
                Teardown();
            }
        }

        #region Implementation

        private void OnFirstChanceException(object sender, FirstChanceExceptionEventArgs e)
        {
            if (e.Exception is ParsingException)
            {
                parsingExceptionThrown = true;
            }
        }

        private void OnImportExportLogicCartonCommandCreationRequested(object sender, CommandCreationRequestedEventArgs e)
        {
            Specify.That(e.CommandColumnName).Should.BeEqualTo(commandColumnName);
            Specify.That(e.Requests).Should.BeLogicallyEqualTo(GetThreeRequestsList());
            Specify.That(e.CommandMappings).Should.BeLogicallyEqualTo(importExportLogicSettings.CommandMappings);
        }

        private void OnCommandCreationFailed(object sender, CommandCreationFailedEventArgs e)
        {
            failedFileId = e.CallbackId;

            Specify.That(e.Requests).Should.BeNull();
            Specify.That(e.FailedRequests).Should.BeLogicallyEqualTo(GetThreeRequestsList());
        }

        private List<List<string>> GetThreeRequestsList()
        {
            return new List<List<string>>
                       {
                           new List<string> { "SerialNumber", "Length", "Width", "Height" },
                           new List<string> { "123", "100", "200", "300" },
                           new List<string> { "123", "100", "200", "300" },
                           new List<string> { "123", "100", "200", "300" }
                       };
        }

        private List<List<string>> GetThreeRequestsListWithMissingValue()
        {
            return new List<List<string>>
                       {
                           new List<string> { "SerialNumber", "Length", "Width", "Height" },
                           new List<string> { "123", "100", "200", "300" },
                           new List<string> { "123", "100", "200", "300" },
                           new List<string> { "123", "100", "200" }
                       };
        }

        [SecurityPermission(SecurityAction.LinkDemand)]
        private void GivenImportExportLogicIsSetUp(string commandColName, EventHandler<CommandCreationRequestedEventArgs> onCommandCreationRequested)
        {
            importExportLogicSettings.CommandColumnName = commandColName;
            importExportLogic.CartonCommandCreationRequested += onCommandCreationRequested;
            importExportLogicSettings.ValueMapper = new ValueMapper();
            importExportLogic.Start();
            waitingForRequestEvent = true;
        }

        [SecurityPermission(SecurityAction.LinkDemand)]
        private ParsingRequestedEventArgs SetUpSupplierEvent(List<List<string>> requestsToImport)
        {
            return new ParsingRequestedEventArgs(requestsToImport, Guid.NewGuid());
        }

        [SecurityPermission(SecurityAction.LinkDemand)]
        private void FireSupplierEvent(ParsingRequestedEventArgs eventArgs)
        {
            cartonImporterMock.Raise(supplier => supplier.ParsingRequested += null, eventArgs);
        }

        [SecurityPermission(SecurityAction.LinkDemand)]
        private void ParseRequestsFromSupplier(List<List<string>> requestsToImport)
        {
            ParsingRequestedEventArgs eventArgs = SetUpSupplierEvent(requestsToImport);

            FireSupplierEvent(eventArgs);
        }

        private void OnCommandCreationRequested(object sender, CommandCreationRequestedEventArgs e)
        {
            importExportLogic.CartonCommandCreationReceivedCallback(e.CallbackId, true);
            waitingForRequestEvent = false;
        }

        private void SetupImportExportLogicWithMocks()
        {
            valueMapperMock = new Mock<ValueMapper>();
            valueMapperMock.Setup(m => m.Map(It.IsAny<string>(), It.IsAny<string>())).Throws(new ParsingException("Parsing failed"));

            importExportLogicSettingsWithMock = new ImportExportLogicSettings
                                                         {
                                                             ValueMapper = valueMapperMock.Object
                                                         };

            cartonImporterMock2 = new Mock<IDataImporter>();

            importExportLogicWithMocks = new ImportExportLogic(importExportLogicSettingsWithMock, cartonImporterMock2.Object, new Mock<ILogger>(MockBehavior.Loose).Object);
        }

        #endregion
    }
}
