﻿using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AzureQueueLogger
{
    using System;

    using PackNet.Common.Logging;

    public class AzureQLoader
    {
        private IAzureQLoggerAppSettings _settings;
        private volatile int _azureBatchCount ;
        private IAzureQueueHelper azureHelper;
        private IRabbitConsumer rabbitConsumer;

        public AzureQLoader(IAzureQLoggerAppSettings settings, IAzureQueueHelper azureHelper =  null, IRabbitConsumer consumer = null)
        {
            _settings = settings;
            this.azureHelper = azureHelper?? new AzureQueueHelper(_settings.AzureConnectionString, _settings.QueueName);
            rabbitConsumer = consumer?? new RabbitConsumer(_settings.RabbitHost, _settings.QueueName);

        }

        public void Go(CancellationToken ct)
        {
            AzureQLogger.LogInternal(LogLevel.Trace, "AzureQLoader.Go");
            // Were we already canceled?
            if (ct.IsCancellationRequested) return;
            try
            {
                _azureBatchCount = _settings.AzureBatchCount;
                

                while (true)
                {
                    AzureQLogger.LogInternal(LogLevel.Trace, "AzureQLoader.Go Looping");

                    var sw = Stopwatch.StartNew();
                    var list = rabbitConsumer.PopXorAllIfLess(_azureBatchCount);

                    if (list.Any())
                    {
                        var t = Task.Factory.StartNew(() =>
                        {
                            var cnt = azureHelper.Push(list, _settings.CompressToAzure);
                            // see if we should update our max number of items we're poping
                            if (cnt.HasValue && cnt < _azureBatchCount && list.Count() >= _azureBatchCount)
                            {
                                AzureQLogger.LogInternal(LogLevel.Trace, "Updating Batch count from {0} to {1}", null,
                                    _azureBatchCount, cnt);
                                _azureBatchCount = cnt.Value;
                            }
                        }, ct);

                        t.Wait(ct);
                    }

                    _settings.AzureBatchCount = _azureBatchCount;

                    if (ct.IsCancellationRequested)
                    {
                        AzureQLogger.LogInternal(LogLevel.Info, "AzureQLoader.Go Token was cancelled");
                        break;
                    }

                    sw.Stop();

                    var waitFor = (int) (_settings.MillisecondsToWaitBetweenAzurePushes - sw.ElapsedMilliseconds);
                    if (waitFor > 10)
                        Thread.Sleep(waitFor);
                }
            }
            catch (AggregateException e)
            {
                AzureQLogger.LogInternal(LogLevel.Error, "AzureQLoader.Go threw  AggregateException.", e.Flatten());
            }
            catch (Exception ex)
            {
                AzureQLogger.LogInternal(LogLevel.Error, "AzureQLoader.Go threw exception", ex);
                if (!ct.IsCancellationRequested)
                {
                    Task.Factory.StartNew(() => Go(ct), ct);
                }
            }
            finally
            {
                if (rabbitConsumer != null)
                    rabbitConsumer.Dispose();
            }
        }

    }
}