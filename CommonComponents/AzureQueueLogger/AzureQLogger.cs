﻿using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;

using Newtonsoft.Json;
using Packsize.NLogBackend;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

using RabbitMQ.Client.Events;

namespace AzureQueueLogger
{
    using PackNet.Common.Logging;
    using PackNet.Common.Logging.NLogBackend;

    /// <summary>
    /// See examples here http://simonwdixon.wordpress.com/2011/05/10/getting-started-with-rabbitmq-in-net-part-2/
    /// </summary>
    [Export(typeof(ILogger))]
    public class AzureQLogger : ILogger
    {
        //application logger only used to display error messages if this logger fails.
        private static ILogger Logger;
        private const string Name = "AzureQLogger";
        private ConnectionFactory _factory;
        private IConnection _connection;
        private IModel _channel;
        private bool _loaded = false;
        private IAzureQLoggerAppSettings _settings;
        private CloudQueue _azureQueue;
        private CancellationTokenSource _azureCancelationToken;

        /// <summary>
        /// testing only constructor
        /// </summary>
        /// <param name="settings"></param>
        /// <param name="logger">logger</param>
        public AzureQLogger(IAzureQLoggerAppSettings settings, ILogger logger = null)
        {
            _settings = settings;
            if (logger == null)
                SetupLogger();
            else
                Logger = logger;

            SetupAll();
        }

        public AzureQLogger()
        {
            _settings = new AzureQLoggerAppSettings();
            SetupAll();
        }

        static AzureQLogger()
        {
            SetupLogger();
        }

        private static void SetupLogger()
        {
            LogManager.LoggingBackend = new NLogBackend("jon", ".\\nlog.config");
            Logger = LogManager.GetLogForApplication();
        }

        public static void LogInternal(LogLevel level, string message, Exception e = null, params object[] p)
        {
            if (Logger == null) return;

            if (e != null)
                Logger.LogException(level, Name + " " + string.Format(message, p), e);

            Logger.Log(level, Name + " " + string.Format(message, p));
        }

        private void SetupAll()
        {
            try
            {

                _azureCancelationToken = new CancellationTokenSource();

                // validate required appsettings
                if (!AreAppSettingsValid()) return;
                // setup rabbit
                if (!SetupRabbit()) return;
                //setup cloud connection 
                if (!SetupAzureQueue()) return;

                KickOffAzureLoader(_azureCancelationToken.Token);

                _loaded = true;
            }
            catch (AggregateException e)
            {
                LogInternal(LogLevel.Error, LogName + " failed to load with AggregateException.", e.Flatten());
            }
            catch (Exception e)
            {
                LogInternal(LogLevel.Error, LogName + " failed to load with exception.", e);
            }
        }

        private void KickOffAzureLoader(CancellationToken token)
        {
            if (token.IsCancellationRequested) return;

            //create new task that will load in messages loaded into rabbit and then sent to azure
            Task.Factory.StartNew(() => new AzureQLoader(_settings).Go(token), token);

        }

        private bool AreAppSettingsValid()
        {
            foreach (var chr in _settings.QueueName.ToCharArray())
            {
                if (char.IsLower(chr))
                    continue;
                if (char.IsDigit(chr))
                    continue;
                LogInternal(LogLevel.Error, "Azure Queue Name must only contain lower case letters and/or numbers. Current setting:{0}", null, _settings.QueueName);
                break;
            }

            return !string.IsNullOrWhiteSpace(_settings.RabbitExchange)
                   || !string.IsNullOrWhiteSpace(_settings.RabbitHost)
                   || !string.IsNullOrWhiteSpace(_settings.AzureConnectionString)
                   || !string.IsNullOrWhiteSpace(_settings.QueueName);
        }

        private bool SetupAzureQueue()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(_settings.AzureConnectionString);

            // Create the queue client.
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            // Retrieve a reference to a queue.
            _azureQueue = queueClient.GetQueueReference(_settings.QueueName);
            _azureQueue.CreateIfNotExists();
            return _azureQueue.Exists();
        }

        public bool SetupRabbit()
        {
            _factory = new ConnectionFactory { HostName = _settings.RabbitHost };
            _connection = _factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(_settings.RabbitExchange, "topic", false, true, null);
            _channel.CallbackException += Channel_CallbackException;
            _connection.ConnectionShutdown += (connection, reason) => LogInternal(LogLevel.Error, "Rabbit connection lost!!");
            var connectionIsOpen = _channel.IsOpen;
            if (connectionIsOpen)
            {
                _channel.QueueDeclare(_settings.QueueName, true, false, false, null);
            }

            return connectionIsOpen;
        }

        private void Channel_CallbackException(object sender, CallbackExceptionEventArgs e)
        {
            _loaded = false;
            Debug.WriteLine(string.Format("{2} RabbitSubscriber.CallbackException: Detail:{0}, Exception:{1}", e.Detail, e.Exception, LogName));
            LogInternal(LogLevel.Error, e.ToString());
        }

        public string LogName
        {
            get
            {
                return Name;
            }
        }

        [Obsolete("no longer supported")]
        public LogLevel SelectedLevel { get; set; }

        public LogLevel? ConfiguredLogLevel { get; set; }

        public void Log(LogLevel logLevel, string logMessage, params object[] args)
        {
            LogToRabbit(logLevel, logMessage, null, args);
        }

        public void Log<T>(LogLevel logLevel, string id, string logMessage, params object[] args) 
        {
           // LogToRabbit(logLevel,);
            var str =  string.Format(logMessage, args);
            Log(logLevel, "{0}:{1} - {2}", typeof (T).Name, id, str);
        }

        public void Log(LogLevel logLevel, Func<string> logMessage)
        {
            LogToRabbit(logLevel, logMessage(), null);
        }

        public void Log(LogLevel logLevel, string logMessage)
        {
            LogToRabbit(logLevel, logMessage);
        }

        public void LogException(LogLevel logLevel, string logMessage, Exception exception, params object[] args)
        {
            LogToRabbit(logLevel, logMessage, exception, args);
        }
        [Obsolete("Use different overridden method.")]
        public void Log(string logMessage)
        {
            LogToRabbit(LogLevel.Info,logMessage);
        }
        [Obsolete("User Mock<ILogger>(Loose) for testing.")]
        public IEnumerable<ILogMessage> GetLogMessages(LogLevel logLevel)
        {
            return null;
        }


        public void Dispose()
        {
            //do nothing wait until we start up to try and push the next set of logs
            _azureCancelationToken.Cancel();
            if (_connection != null)
                _connection.Close();
            if (_channel != null)
                _channel.Abort();
        }

        private void LogToRabbit(LogLevel logLevel, string logMessage, Exception exception = null, params object[] args)
        {
            if (logLevel < _settings.MinimumConfiguredLogLevel) return;

            if (!ShouldWeLogThisMessage(logMessage)) return;

            var message = JsonConvert.SerializeObject(new DotServerLogMessage
            {
                Message = string.Format(logMessage, args),
                LogLevel = logLevel,
                Exception = exception == null ? string.Empty : exception.ToString(),
                TimeStamp = DateTime.Now
            });

            var encodedMessage = Encoding.ASCII.GetBytes(message);
            IBasicProperties basicProperties = _channel.CreateBasicProperties();
            basicProperties.SetPersistent(true);
            _channel.BasicPublish("", _settings.QueueName, basicProperties, encodedMessage);
        }


        /// <summary>
        /// Do not log if we are not loaded or the message is one this assembly created.
        /// </summary>
        /// <param name="logMessage"></param>
        /// <returns></returns>
        private bool ShouldWeLogThisMessage(string logMessage)
        {
            return _loaded && _connection.IsOpen && _channel.IsOpen && !logMessage.Contains(LogName);
        }

        public void DisableAzure()
        {
            _azureCancelationToken.Cancel(true);
        }
    }
}
