﻿
namespace AzureQueueLogger
{
    using System;

    using PackNet.Common.Logging;

    public class DotServerLogMessage
    {
        public DateTime TimeStamp { get; set; }
        public LogLevel LogLevel { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
    }
}
