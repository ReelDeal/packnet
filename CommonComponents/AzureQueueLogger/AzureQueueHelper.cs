using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using PackNet.Common.Logging;

namespace AzureQueueLogger
{
    using System;

    using LogLevel = PackNet.Common.Logging.LogLevel;

    public interface IAzureQueueHelper
    {
        int? Push(IEnumerable<DotServerLogMessage> messages, bool compress);
    }

    public class AzureQueueHelper : IAzureQueueHelper
    {
        private CloudQueue queue;

        public AzureQueueHelper(string cs, string queueName)
        {
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(cs);

            // Create the queue client.
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();

            // Retrieve a reference to a queue.
            queue = queueClient.GetQueueReference(queueName);

            // Create the queue if it doesn't already exist.
            queue.CreateIfNotExists();

        }

        public int? Push(IEnumerable<DotServerLogMessage> messages, bool compress)
        {
            // Create a message and add it to the queue.
            
#if DEBUG
            AzureQLogger.LogInternal(LogLevel.Trace, " Azure Push {0} compression:{1}", null,messages.Count() , compress);
#endif
            try
            {
                var m = JsonConvert.SerializeObject(messages);
                CloudQueueMessage message = compress ? new CloudQueueMessage(Compress(m)) : new CloudQueueMessage(m);
                
                queue.AddMessage(message);
            }
            catch (Exception e)
            {
                if (e.Message.Contains("Messages cannot be larger than"))
                {
                    var currentCount = messages.Count();
                    var newMaxMessageCount = (int)(currentCount * .90);

                    AzureQLogger.LogInternal(LogLevel.Warning, "{0} was too many records to sent to azure. Try updating MachineManagerWindow.exe.config value for AzureQLoggerAzureBatchCount was {1} changing in memory to {2}", null, currentCount, newMaxMessageCount, e.Message);

                    var list1 = messages.Take(newMaxMessageCount);
                    var list2 = messages.Skip(newMaxMessageCount);
                    int? listOne = Push(list1, compress);
                    int? listTwo = Push(list2, compress);

                    if (listOne.HasValue && listTwo.HasValue)
                        return listOne.Value > listTwo.Value ? listOne : listTwo;
                    if (listOne.HasValue) return listOne;
                    return listTwo;
                }
                else
                {
                    AzureQLogger.LogInternal(LogLevel.Error, "Unhandled exception in Push cnt:"+ messages.Count(), e);
                }
            }
            return null;
        }

        public static byte[] Compress(string s)
        {
            var memoryStream = new MemoryStream();
            var originalStream = new MemoryStream(Encoding.ASCII.GetBytes(s)) { Position = 0 };
            using (var compressionStream = new GZipStream(memoryStream, CompressionMode.Compress))
            {
                originalStream.CopyTo(compressionStream);
            }

            return memoryStream.ToArray();
        }

        public static string Decompress(byte[] s)
        {
            var memoryStream = new MemoryStream();
            var originalStream = new MemoryStream(s) { Position = 0 };
            using (var compressionStream = new GZipStream(originalStream, CompressionMode.Decompress))
            {
                compressionStream.CopyTo(memoryStream);
            }
            memoryStream.Seek(0, SeekOrigin.Begin);
            var sr = new StreamReader(memoryStream);
            return  sr.ReadToEnd();
        }

    }
}