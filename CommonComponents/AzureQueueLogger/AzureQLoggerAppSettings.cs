﻿using System.Configuration;

namespace AzureQueueLogger
{
    using System;

    using PackNet.Common.Logging;

    public interface IAzureQLoggerAppSettings
    {
        string AzureConnectionString { get; }
        string RabbitExchange { get; }
        string RabbitHost { get; }
        string QueueName { get; set; }
        int MillisecondsToWaitBetweenAzurePushes { get; set; }
        int AzureBatchCount { get; set; }
        bool CompressToAzure { get;  }
        LogLevel MinimumConfiguredLogLevel { get; }
    }

    public class AzureQLoggerAppSettings : IAzureQLoggerAppSettings
    {
        private int? _time;
        private string _queueName;
        private int? _batchCount;
        private bool? _compress;
        private string _cs;
        private LogLevel? _level;

        /// <summary>
        /// String value for the exchange to use when queueing up the log messages before pushing to Azure.
        /// Default Value: mm
        /// </summary>
        public string RabbitExchange
        {
            get { return ConfigurationManager.AppSettings.Get("AzureQLoggerRabbitExchange"); }
        }

        /// <summary>
        /// URI of the rabbit instance to use.  Usually
        /// Default Value: localhost.
        /// </summary>
        public string RabbitHost
        {
            get { return ConfigurationManager.AppSettings.Get("AzureQLoggerRabbitHost"); }
        }

        /// <summary>
        /// The name of the queue to use for RabbitMQ and Azure.  Azure requires that name to be only lower case characters or numbers.  
        /// We recommend using a value that will be easily distinguish where the data was sourced.
        /// Suggested Values: staplesorlando, healthypetscali. 
        /// </summary>
        public string QueueName
        {
            get
            {
                if(string.IsNullOrWhiteSpace(_queueName))
                _queueName = ConfigurationManager.AppSettings.Get("AzureQLoggerRabbitDurableQueueName");
                return _queueName;
            }
            set { _queueName = value; }
        }

        /// <summary>
        /// Connection string to the azure queue storage.  Will need to include AccountName and AccountKey.  Contact Packsize R & D for more information.
        /// Value Format: DefaultEndpointsProtocol=https;AccountName=[someStorageName];AccountKey=[someStorageKey]
        /// </summary>
        public string AzureConnectionString
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_cs)) _cs= ConfigurationManager.ConnectionStrings["AzureQLogDataSource"].ConnectionString;
                return _cs;
            }
            set { _cs = value; }
        }

        /// <summary>
        /// Time to wait between pushes into Azure.  Was created because we probably want to wait some amount of time so we can batch more than one record
        /// at a time.  Used in conjunction with AzureBatchCount. 
        /// Suggested Value: perhaps 5000 = 5seconds
        /// </summary>
        public int MillisecondsToWaitBetweenAzurePushes
        {
            get
            {
                if (!_time.HasValue)
                    _time = int.Parse(ConfigurationManager.AppSettings.Get("AzureQLoggerMillisecondsToWaitBetweenAzurePushes"));
                
                return _time.Value;
            }
            set { _time = value; }
        }

        /// <summary>
        /// How many records to batch into one call to Azure.  Set this to something high, the system will change this value internally if necessary.
        /// Suggested Value: 10000
        /// </summary>
        public int AzureBatchCount
        {
            get
            {
                if (!_batchCount.HasValue)
                    _batchCount = int.Parse(ConfigurationManager.AppSettings.Get("AzureQLoggerAzureBatchCount"));

                return _batchCount.Value;
            }
            set { _batchCount = value; }
        }

        /// <summary>
        /// Highly recommended for any production system or when trace is turned on because the number or records and network traffic will be greatly decreased.
        /// Possible Values: True (recommended), False
        /// </summary>
        public bool CompressToAzure
        {

            get
            {
                if (!_compress.HasValue)
                    _compress = bool.Parse(ConfigurationManager.AppSettings.Get("AzureQLoggerCompressData"));

                return _compress.Value;
            }
            set { _compress = value; }
        }

        /// <summary>
        /// Minimum level of logs that will be sent to Azure.
        /// Possible Values: from most verbose to least: Trace, Debug (recommended), Info, Warn, Error
        /// </summary>
        public LogLevel MinimumConfiguredLogLevel
        {
            get
            {
                try
                {
                    if (!_level.HasValue)
                        _level = (LogLevel?)Enum.Parse(typeof(LogLevel), ConfigurationManager.AppSettings.Get("AzureQLoggerMinimumConfiguredLogLevel"));
                }
                catch
                {
                    _level = (LogLevel?)LogLevel.Info;
                }
                return _level.Value;
            }
            set { _level = value; }
        }
    }

}
