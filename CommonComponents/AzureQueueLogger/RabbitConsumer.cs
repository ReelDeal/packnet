using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace AzureQueueLogger
{
    public interface IRabbitConsumer:IDisposable
    {
        /// <summary>
        /// Pull of specified number or items from the queue and if there arn't that many return what is there.
        /// </summary>
        /// <param name="numberToDequeue"></param>
        /// <returns></returns>
        IEnumerable<DotServerLogMessage> PopXorAllIfLess(int numberToDequeue);

    }

    public class RabbitConsumer: IRabbitConsumer
    {
        protected IModel Model;
        protected IConnection Connection;
        protected string QueueName;

        public RabbitConsumer(string hostName, string queueName)
        {
            QueueName = queueName;
            var connectionFactory = new ConnectionFactory {HostName = hostName};
            Connection = connectionFactory.CreateConnection();
            CreateChannel();
        }

        private void CreateChannel()
        {
            Model = Connection.CreateModel();
            Model.BasicQos(0, 1, false);
            Model.QueueDeclare(QueueName, true, false, false, null);
        }

        /// <summary>
        /// Pull of specified number or items from the queue and if there arn't that many return what is there.
        /// </summary>
        /// <param name="numberToDequeue"></param>
        /// <returns></returns>
        public IEnumerable<DotServerLogMessage> PopXorAllIfLess(int numberToDequeue)
        {
            var lst = new List<DotServerLogMessage>();
            try
            {
                CreateChannel();

                var consumer = new QueueingBasicConsumer(Model);
                Model.BasicConsume(QueueName, false, consumer);
                
                while (lst.Count < numberToDequeue && consumer.IsRunning)
                {

                    object result;
                    if (consumer.Queue.Dequeue(300, out result))
                    {
                        var e = (RabbitMQ.Client.Events.BasicDeliverEventArgs) result;
                        var s = Encoding.ASCII.GetString(e.Body);
                        if (string.IsNullOrWhiteSpace(s)) break;
                        // ... process the message
                        lst.Add(JsonConvert.DeserializeObject<DotServerLogMessage>(s));
                        Model.BasicAck(e.DeliveryTag, false);
                    }
                    else break;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                // The consumer was removed, either through
                // channel or connection closure, or through the
                // action of IModel.BasicCancel().
            }
            finally
            {
                Model.Dispose();
            }
            return lst;
        }
        
        public void Dispose()
        {
            if (Connection != null)
                Connection.Close();
            if (Model != null)
                Model.Abort();
        }
    }
}