﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Microsoft.WindowsAzure.ServiceRuntime;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;

namespace AzureQueueLogger
{
    public class AzureLoggingWorkerRoleBase : RoleEntryPoint
    {
        public string QueueName { get; private set; }
        public string BlobUri { get; private set; }
        public string ConnectionString { get; private set; }
        public int ThreadSleepTimeBetweenMessageChecks { get; private set; }
        public CloudStorageAccount StorageAccount { get; private set; }
        public CloudQueueClient QueueClient { get; private set; }
        public CloudQueue LoggingQueue { get; private set; }
        public CloudBlobClient BlobClient { get; private set; }
        public CloudBlobContainer CloudBlobContainer { get; private set; }
        public CloudBlockBlob Blob { get; private set; }
        public DateTime QueueInstanceDate { get; set; }

        public bool Shutdown { get; set; }
        public bool Initialized { get; set; }

        private const int FourMegaBytes = 4194304;

        public AzureLoggingWorkerRoleBase()
        {
            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = Environment.ProcessorCount * 12;
        }

        protected void Initialize(string queueName, string blobUri, string connectionString, int threadSleepTimeBetweenMessageChecks = 60000)
        {
            QueueName = queueName;
            BlobUri = blobUri;
            ConnectionString = connectionString;
            ThreadSleepTimeBetweenMessageChecks = threadSleepTimeBetweenMessageChecks;
        }

        protected virtual void CreateBlobContainerIfNecessary()
        {
            Trace.TraceInformation("Starting CreateBlobContainerIfNecessary");
            if (QueueInstanceDateUpdate())
            {
                if (Blob != null)
                {
                    var list = Blob.DownloadBlockList(BlockListingFilter.Uncommitted);
                    var blockIds = list.Select(x => x.Name);

                    Blob.PutBlockList(blockIds);
                }

                Trace.TraceInformation("Creating Blob Container");

                CloudBlobContainer = new CloudBlobContainer(new Uri(BlobUri), StorageAccount.Credentials);

                Trace.TraceInformation("Success creating container");

                CloudBlobContainer.CreateIfNotExists();

                Trace.TraceInformation("Passed CreateIfNotExists for Blob Container");

                Blob = CloudBlobContainer.GetBlockBlobReference(QueueInstanceDate.ToString("yyyyMd"));

                Trace.TraceInformation("Got block reference");
            }

            Trace.TraceInformation("Leaving CreateBlobContainerIfNecessary");
        }

        protected virtual bool QueueInstanceDateUpdate()
        {
            var now = DateTime.Today.ToUniversalTime();
            if (QueueInstanceDate.Day != now.Day)
            {
                Trace.TraceInformation("Updating Queue instance date to {0} was {1}", now, QueueInstanceDate);
                QueueInstanceDate = now;
                return true;
            }
            Trace.TraceInformation("Queue instance date does not need updating");
            return false;
        }

        public override void OnStop()
        {
            Shutdown = true;
            base.OnStop();
        }

        public override bool OnStart()
        {
            Trace.TraceInformation("OnStart called");

            try
            {
                StorageAccount = CloudStorageAccount.Parse(ConnectionString);

                Trace.TraceInformation("Created Storage account");

                QueueClient = StorageAccount.CreateCloudQueueClient();

                Trace.TraceInformation("Created Queue Client");

                LoggingQueue = QueueClient.GetQueueReference(QueueName);

                Trace.TraceInformation("Created Logging Queue Object");

                LoggingQueue.CreateIfNotExists();

                Trace.TraceInformation("Passed CreateIfNotExists for Logging Queue");

                BlobClient = StorageAccount.CreateCloudBlobClient();

                Trace.TraceInformation("Created Blob client");

                CreateBlobContainerIfNecessary();

                Trace.TraceInformation("Finishing OnStart");
            }
            catch (Exception e)
            {
                Trace.TraceError(e.ToString());
            }
            return base.OnStart();
        }

        public override void Run()
        {
            while (!Shutdown)
            {
                try
                {
                    var message = LoggingQueue.PeekMessage();

                    if (message != null)
                    {
                        ProcessQueue();
                    }
                    else
                    {
                        Thread.Sleep(ThreadSleepTimeBetweenMessageChecks);
                        CreateBlobContainerIfNecessary();
                    }
                }
                catch (Exception e)
                {
                    Trace.TraceError(e.ToString());
                    //Don't chew up the Trace logging, in case something is preventing this from running correctly
                    Thread.Sleep(ThreadSleepTimeBetweenMessageChecks);
                }
            }

            base.Run();
        }

        private void ProcessQueue()
        {
            var uploadData = new List<byte>(FourMegaBytes);
            CloudQueueMessage message = LoggingQueue.GetMessage();

            while (message != null)
            {
                var messageString = AzureQueueHelper.Decompress(message.AsBytes);

                var actualMessageBytes = Encoding.UTF8.GetBytes(messageString + Environment.NewLine);

                if ((uploadData.Count + actualMessageBytes.Length) < FourMegaBytes)
                {
                    uploadData.AddRange(actualMessageBytes);

                    LoggingQueue.DeleteMessage(message);

                    message = LoggingQueue.GetMessage();
                }
                else
                {
                    break;
                }
            }


            using (var ms = new MemoryStream(uploadData.ToArray()))
            {
                ms.Position = 0;

                var id =
                    Convert.ToBase64String(Encoding.UTF8.GetBytes(Guid.NewGuid().ToString("N")));

                Blob.PutBlock(id, ms, null);

                //   Blob.PutBlockList(new List<string> { id });
            }
        }
    }
}