﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Logging;

namespace TestUtils
{
    public class ConsoleTrackingParticipant : TrackingParticipant
    {
        protected override void Track(TrackingRecord record, TimeSpan timeout)
        {
            if (record != null)
            {
               Console.Write(record);
            }
        }
    }
}
