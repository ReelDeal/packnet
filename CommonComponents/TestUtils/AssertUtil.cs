﻿namespace TestUtils
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    public static class AssertUtil
    {
        public static T Throws<T>(Action action) where T : Exception
        {
            try
            {
                action();
            }
            catch (T ex)
            {
                return ex;
            }
            catch (Exception e)
            {
                Assert.Fail("Expected exception of type {0}, but found {1}", typeof(T), e.GetType());
            }

            Assert.Fail("Expected exception of type {0}, but no exception thrown", typeof(T));
            return null;
        }

        public static T Throws<T>(Action action, string expectedMessage) where T : Exception
        {
            try
            {
                action();
            }
            catch (T ex)
            {
                Assert.AreEqual(expectedMessage, ex.Message);
                return ex;
            }
            catch (Exception e)
            {
                Assert.Fail("Expected exception of type {0}, but found {1}", typeof(T), e.GetType());
            }

            Assert.Fail("Expected exception of type {0}, but no exception thrown", typeof(T));
            return null;
        }
    }
}