using System;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using PackNet.Common.Interfaces.Logging;

namespace TestUtils
{
    public class ConsoleLogger : ILogger
    {

        private Subject<Tuple<LogLevel, string>> messageSubject = new Subject<Tuple<LogLevel, string>>();
        private Stopwatch sw;

        public ConsoleLogger()
        {
            sw = Stopwatch.StartNew();
            messageSubject = new Subject<Tuple<LogLevel, string>>();
        }

        public void Dispose()
        {

        }

        public string Name { get; set; }
        public string LogName { get; set; }

        public IObservable<Tuple<LogLevel, string>> AllLogMessages { get { return messageSubject.AsObservable(); } }

        public void Log(LogLevel logLevel, string logMessage, params object[] args)
        {
            var msg = string.Format(logMessage, args);
            Console.WriteLine(LogName + "{2}::{0} {1}", logLevel, msg, sw.ElapsedMilliseconds);
            messageSubject.OnNext(new Tuple<LogLevel, string>(logLevel, msg));
        }

        public void Log<T>(LogLevel logLevel, string id, string logMessage, params object[] args)
        {
            var msg = string.Format(logMessage, args);
            Console.WriteLine(LogName + "{3}::{0} Id:{1} {2}", logLevel, id, msg, sw.ElapsedMilliseconds);
            messageSubject.OnNext(new Tuple<LogLevel, string>(logLevel, msg));
        }

        public void Log(LogLevel logLevel, Func<string> logMessage)
        {
            Console.WriteLine(LogName + "{2}::{0} {1}", logLevel, logMessage(), sw.ElapsedMilliseconds);
            messageSubject.OnNext(new Tuple<LogLevel, string>(logLevel, logMessage()));
        }

        public void Log(LogLevel logLevel, string logMessage)
        {
            Console.WriteLine(LogName + "{2}::{0} {1}", logLevel, logMessage, sw.ElapsedMilliseconds);
            messageSubject.OnNext(new Tuple<LogLevel, string>(logLevel, logMessage));
        }

        public void LogException(LogLevel logLevel, string message, Exception exception, params object[] args)
        {
            var msg = args.Length > 0 ? string.Format(message, args) : message;
            Console.WriteLine(LogName + "{3}::{0} {1} {2}", logLevel, msg,
                exception.ToString(), sw.ElapsedMilliseconds);
            messageSubject.OnNext(new Tuple<LogLevel, string>(logLevel, msg));
        }
    }
}