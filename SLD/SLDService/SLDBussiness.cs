﻿using System;
using System.Collections.Generic;

using PackNet.Business;
using PackNet.Common.Interfaces.Services;
using PackNet.SLDService.Common;
using PackNet.SLDService.Data;

namespace PackNet.SLDService
{
    public class SLDBussiness : BaseRepositoryBusinessCrud<UPCItem>, ISLDBussiness
    {
        private readonly IUPCRepository upcRepository;
        private readonly ISLDConfigurationRepository configurationRepository;
        private readonly ISLDWorkflowHandler sldWorkflowHandler;

        public SLDBussiness(IServiceLocator serviceLocator,IUPCRepository upcRepository, ISLDConfigurationRepository configurationRepository)
            :base(upcRepository)
        {
            this.upcRepository = upcRepository;
            this.configurationRepository = configurationRepository;
            
            sldWorkflowHandler = new SLDWorkflowHandler(serviceLocator);
        }

        public SLDConfiguration CurrentSldConfiguration
        {
            get { return configurationRepository.CurrentConfiguration; }
        }

        public void UpdateSLDConfiguration(SLDConfiguration newConfiguration)
        {
            if (newConfiguration.ConflictResolutionMethod == null)
                return;

            if (string.IsNullOrEmpty(newConfiguration.ImportWorkflowPath))
                return;

            if (string.IsNullOrEmpty(newConfiguration.UPCImportWorkflowPath))
                return;

            if (string.IsNullOrEmpty(newConfiguration.UPCExportWorkflowPath))
                return;

            configurationRepository.UpdateSldConfiguration(newConfiguration);
        }
        
        public UPCItem LocateUpcItem(string upcIdentifier)
        {
            return upcRepository.FindByUPC(upcIdentifier);
        }

        public override UPCItem Update(UPCItem current)
        {
            return base.Update(current);
        }

        public override void UpdateRange(List<UPCItem> upcsToUpdate)
        {
            base.UpdateRange(upcsToUpdate);
        }

        public void DispatchImportWorkflow(IEnumerable<UPCOrder> upcsToImport, string replyTo, Guid? machineGroupId)
        {
            sldWorkflowHandler.DispatchImportWorkflow(upcsToImport, CurrentSldConfiguration.ImportWorkflowPath, replyTo, machineGroupId);
        }

        public void DispatchImportUPCsWorkflow(List<object> dynamic, string replyTo)
        {
            sldWorkflowHandler.DispatchImportUPCsWorkflow(dynamic, CurrentSldConfiguration.UPCImportWorkflowPath, replyTo);
        }

        public void DispatchExportUPCsWorkflow(string replyTo)
        {
            sldWorkflowHandler.DispatchExportUPCsWorkflow(GetAll(), CurrentSldConfiguration.UPCExportWorkflowPath, replyTo);
        }

        public SLDWorkflows GetSLDWorkflows()
        {
            return sldWorkflowHandler.GetWorkflows();
        }
    }
}
