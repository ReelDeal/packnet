﻿
using PackNet.Common.Interfaces.Services;
using PackNet.SLDService.Common;

namespace PackNet.SLDService
{
    public interface ISLDService : IService
    {
        SLDConfiguration CurrentSLDConfiguration { get; }

        UPCItem LocateUpcItem(string UPC);
    }
}