﻿using System;
using System.Collections.Generic;

using PackNet.SLDService.Common;

namespace PackNet.SLDService
{
    public interface ISLDWorkflowHandler
    {
        void DispatchImportWorkflow(IEnumerable<UPCOrder> upcsToImport, string importWorkflowPath, string replyTo, Guid? machineGroupId);

        void DispatchExportUPCsWorkflow(IEnumerable<UPCItem> upcsToExport, string exportUPCsWorkflowPath, string replyTo);

        void DispatchImportUPCsWorkflow(IEnumerable<object> dynamic, string upcImportWorkflowPath, string replyTo);

        SLDWorkflows GetWorkflows();
    }
}
