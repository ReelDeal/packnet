﻿using System;
using System.Collections.Generic;

using PackNet.SLDService.Common;

namespace PackNet.SLDService
{
    public interface ISLDBussiness
    {
        IEnumerable<UPCItem> GetAll();

        UPCItem LocateUpcItem(string upcIdentifier);

        UPCItem Create(UPCItem data);

        UPCItem Update(UPCItem current);

        void Delete(UPCItem upc);
        
        SLDConfiguration CurrentSldConfiguration { get; }

        void UpdateSLDConfiguration(SLDConfiguration data);

        SLDWorkflows GetSLDWorkflows();

        void DispatchImportWorkflow(IEnumerable<UPCOrder> upcsToImport, string replyTo, Guid? machineGroupId);

        void DispatchImportUPCsWorkflow(List<object> dynamic, string replyTo);

        void DispatchExportUPCsWorkflow(string replyTo);
        
        void UpdateRange(List<UPCItem> upcsToUpdate);
    }
}
