﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

using PackNet.Common.FileHandling.DynamicImporter;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using PackNet.SLDService.Common;
using PackNet.SLDService.Data;

namespace PackNet.SLDService
{
    [Export(typeof(IService))]
    public class SLDService : ISLDService
    {
        private readonly ILogger logger;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IUserNotificationService userNotificationService;
        private readonly ISLDBussiness sldBussiness;

        [ImportingConstructor]
        public SLDService(IServiceLocator serviceLocator, ILogger logger)
        {
            UPCMessages.Touch();
            UPCImportAction.Touch();

            this.logger = logger;

            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            userNotificationService = serviceLocator.Locate<IUserNotificationService>();

            MongoDbHelpers.TryRegisterSerializer<UPCMessages>(new EnumerationSerializer());
            sldBussiness = new SLDBussiness(serviceLocator, new UPCRepository(), new SLDConfigurationRepository());

            Wireup(serviceLocator);

            serviceLocator.RegisterAsService(this);
        }

        private void Wireup(IServiceLocator serviceLocator)
        {
            var eventAggregatorSubscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(UPCMessages.DiscoverSLDService);
            UPCMessages.DiscoverSLDService.OnMessage(eventAggregatorSubscriber, logger,
                msg => uiCommunicationService.SendMessageToUI(new Message()
                {
                    MessageType = UPCMessages.DiscoverSLDServiceResponse
                }));

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<string>(UPCMessages.LocateUPC);
            UPCMessages.LocateUPC.OnMessage<string>(eventAggregatorSubscriber, logger,
                msg =>
                {
                    var upcItem = sldBussiness.LocateUpcItem(msg.Data);
                    var result = upcItem == null ? ResultTypes.Fail : ResultTypes.Success;

                    uiCommunicationService.SendMessageToUI(new ResponseMessage<UPCItem>
                    {
                        MessageType = UPCMessages.UPCLocated,
                        ReplyTo = msg.ReplyTo,
                        Result = result,
                        Data = upcItem
                    });
                });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<UPCItem>(UPCMessages.CreateUPC);
            UPCMessages.CreateUPC.OnMessage<UPCItem>(eventAggregatorSubscriber, logger,
                msg =>
                {
                    var upcItem = sldBussiness.LocateUpcItem(msg.Data.UPC);
                    if (upcItem != null)
                    {
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<UPCItem>
                        {
                            MessageType = UPCMessages.UPCCreated,
                            ReplyTo = msg.ReplyTo,
                            Result = ResultTypes.Exists,
                            Data = msg.Data
                        });
                    }
                    else
                    {
                        upcItem = sldBussiness.Create(msg.Data);
                        var result = upcItem == null ? ResultTypes.Fail : ResultTypes.Success;

                        uiCommunicationService.SendMessageToUI(new ResponseMessage<UPCItem>
                        {
                            MessageType = UPCMessages.UPCCreated,
                            ReplyTo = msg.ReplyTo,
                            Result = result,
                            Data = upcItem
                        });
                    }

                    PublishUPCsToUi();
                });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<UPCItem>(UPCMessages.UpdateUPC);
            UPCMessages.UpdateUPC.OnMessage<UPCItem>(eventAggregatorSubscriber, logger, msg =>
            {
                var newUpc = msg.Data;

                var current = sldBussiness.LocateUpcItem(newUpc.UPC);
                if (current == null)
                {
                    uiCommunicationService.SendMessageToUI(new ResponseMessage()
                    {
                        MessageType = UPCMessages.UPCUpdated,
                        Result = ResultTypes.Fail
                    });
                    return;
                }
                current.UpdateFrom(newUpc);
                sldBussiness.Update(current);
                uiCommunicationService.SendMessageToUI(new ResponseMessage()
                {
                    MessageType = UPCMessages.UPCUpdated,
                    Result = ResultTypes.Success
                });

                PublishUPCsToUi();
            });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<UPCItem>>(UPCMessages.DeleteUPCs);
            UPCMessages.DeleteUPCs.OnMessage<IEnumerable<UPCItem>>(eventAggregatorSubscriber, logger, msg =>
            {
                msg.Data.ForEach(upc => sldBussiness.Delete(upc));
                uiCommunicationService.SendMessageToUI(new ResponseMessage
                {
                    MessageType = UPCMessages.UPCsDeleted,
                    ReplyTo = msg.ReplyTo,
                    Result = ResultTypes.Success
                });
                PublishUPCsToUi();
            });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(UPCMessages.GetUPCs);
            UPCMessages.GetUPCs.OnMessage(eventAggregatorSubscriber, logger,
                msg => PublishUPCsToUi());

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<UPCImportFileItem>(UPCMessages.ImportUPCs);
            UPCMessages.ImportUPCs.OnMessage<UPCImportFileItem>(eventAggregatorSubscriber, logger,
                msg =>
                    sldBussiness.DispatchImportUPCsWorkflow(new FileEnumerator(logger, msg.Data.Contents, userNotificationService, msg.Data.Delimiter,
                            msg.Data.CommentPrefix, msg.Data.HeadersInFile ? null : msg.Data.HeaderFields).ToList(), msg.ReplyTo));

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ImportCollection>(UPCMessages.FinalizeUPCImport);
            UPCMessages.FinalizeUPCImport.OnMessage<ImportCollection>(eventAggregatorSubscriber, logger, msg =>
            {
                var preparedItems = msg.Data.Items.Where(i => i.ImportAction != UPCImportAction.Ignore).Cast<UPCItem>().Select(i =>
                {
                    var current = sldBussiness.LocateUpcItem(i.UPC);
                    if (current != null)
                    {
                        current.UpdateFrom(i);
                        return current;
                    }
                    return i;
                }).ToList();

                sldBussiness.UpdateRange(preparedItems);

                uiCommunicationService.SendMessageToUI(new ResponseMessage
                {
                    MessageType = UPCMessages.UPCsImported,
                    ReplyTo = msg.ReplyTo,
                    Result = ResultTypes.Success
                });
            });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(UPCMessages.ExportUPCs);
            UPCMessages.ExportUPCs.OnMessage(eventAggregatorSubscriber, logger, (msg) => sldBussiness.DispatchExportUPCsWorkflow(msg.ReplyTo));

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<UPCOrder>>(UPCMessages.ProcessUPCs);
            UPCMessages.ProcessUPCs.OnMessage<IEnumerable<UPCOrder>>(eventAggregatorSubscriber, logger, (msg) => sldBussiness.DispatchImportWorkflow(msg.Data, msg.ReplyTo, msg.MachineGroupId));

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(UPCMessages.GetWorkflows);
            UPCMessages.GetWorkflows.OnMessage(eventAggregatorSubscriber, logger, PublishWorkflowsToUi);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SLDConfiguration>(UPCMessages.SaveSLDConfiguration);
            UPCMessages.SaveSLDConfiguration.OnMessage<SLDConfiguration>(eventAggregatorSubscriber, logger, UpdateSldConfiguration);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(UPCMessages.GetCurrentSldConfiguration);
            UPCMessages.GetCurrentSldConfiguration.OnMessage(eventAggregatorSubscriber, logger, PublishCurrentSLDConfigurationToUi);
        }

        private void PublishCurrentSLDConfigurationToUi(IMessage m)
        {
              uiCommunicationService.SendMessageToUI(new ResponseMessage<SLDConfiguration>
            {
                MessageType = UPCMessages.GetCurrentSldConfigurationResponse,
                ReplyTo = m.ReplyTo,
                Result = ResultTypes.Success,
                Data = sldBussiness.CurrentSldConfiguration
            });   
        }

        private void UpdateSldConfiguration(IMessage<SLDConfiguration> msg)
        {
            if(msg.Data == null)
                return;

            sldBussiness.UpdateSLDConfiguration(msg.Data);
            
            uiCommunicationService.SendMessageToUI(new ResponseMessage()
            {
                MessageType = UPCMessages.SaveSLDConfigurationResponse,
                ReplyTo = msg.ReplyTo
            });
        }

        private void PublishWorkflowsToUi(IMessage m)
        {
            uiCommunicationService.SendMessageToUI(new ResponseMessage<object>
            {
                MessageType = UPCMessages.GetWorkflowsResponse,
                ReplyTo = m.ReplyTo,
                Result = ResultTypes.Success,
                Data = sldBussiness.GetSLDWorkflows()
            });
        }

     
        private void PublishUPCsToUi()
        {
            uiCommunicationService.SendMessageToUI(new Message<List<UPCItem>>()
            {
                MessageType = UPCMessages.AllUPCs,
                Data = sldBussiness.GetAll().ToList()
            });
        }

        public void Dispose()
        {
        }

        public string Name { get { return "SLDService"; } }

        public SLDConfiguration CurrentSLDConfiguration
        {
            get { return sldBussiness.CurrentSldConfiguration; }
        }

        public UPCItem LocateUpcItem(string UPC)
        {
            return sldBussiness.LocateUpcItem(UPC);
        }
    }
}
