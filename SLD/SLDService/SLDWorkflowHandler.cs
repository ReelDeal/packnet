﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using PackNet.Common;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.SLDService.Common;

namespace PackNet.SLDService
{
    public class SLDWorkflowHandler :ISLDWorkflowHandler
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IUserNotificationService userNotificationService;
        private readonly ILogger logger;

        private const string sldWorkflowPath = "%PackNetWorkflows%\\SLD";

        public SLDWorkflowHandler(IServiceLocator serviceLocator) 
        {
            this.serviceLocator = serviceLocator;
            userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            logger = serviceLocator.Locate<ILogger>();
        }


        public void DispatchImportWorkflow(IEnumerable<UPCOrder> upcsToImport, string importWorkflowPath, string replyTo, Guid? machineGroupId)
        {
            var inArgs = new Dictionary<string, object>()
                         {
                             { "UPCsToImport", upcsToImport },
                             { "ServiceLocator", serviceLocator},
                             { "ReplyTo", replyTo},
                             { "MachineGroupId", machineGroupId }
                         };

            WorkflowHelper.InvokeWorkFlow(logger, importWorkflowPath, GetType().Assembly, inArgs, userNotificationService);
        }


        public void DispatchExportUPCsWorkflow(IEnumerable<UPCItem> upcsToExport, string exportUPCsWorkflowPath, string replyTo)
        {
            var inArgs = new Dictionary<string, object>()
                         {
                             { "UPCsToExport", upcsToExport },
                             { "ServiceLocator", serviceLocator},
                             { "ReplyTo", replyTo}
                         };

            WorkflowHelper.InvokeWorkFlow(logger, exportUPCsWorkflowPath, GetType().Assembly, inArgs, userNotificationService);
        }

        public void DispatchImportUPCsWorkflow(IEnumerable<object> dynamic, string upcImportWorkflowPath, string replyTo)
        {
            var inArgs = new Dictionary<string, object>()
                         {
                             { "UPCsToImport", dynamic },
                             { "ServiceLocator", serviceLocator},
                             { "ReplyTo", replyTo}
                         };

            WorkflowHelper.InvokeWorkFlow(logger, upcImportWorkflowPath, GetType().Assembly, inArgs, userNotificationService);
        }

        public SLDWorkflows GetWorkflows()
        {
            var importWorkflows =
                Directory.EnumerateFiles(DirectoryHelpers.ReplaceEnvironmentVariables(Path.Combine(sldWorkflowPath, "Import")),
                    "*.xaml")
                    .Select(
                        f =>
                            new WorkflowContainer
                            {
                                WorkflowName = Path.GetFileNameWithoutExtension(f),
                                WorkflowPath = Path.Combine(sldWorkflowPath, "Import", Path.GetFileName(f))
                            }).ToList();

            var upcImportWorkflows =
                Directory.EnumerateFiles(DirectoryHelpers.ReplaceEnvironmentVariables(Path.Combine(sldWorkflowPath, "UPCImport")),
                    "*.xaml")
                    .Select(
                        f =>
                            new WorkflowContainer
                            {
                                WorkflowName = Path.GetFileNameWithoutExtension(f),
                                WorkflowPath = Path.Combine(sldWorkflowPath, "UPCImport", Path.GetFileName(f))
                            }).ToList();

             var upcExportWorkflows =
                Directory.EnumerateFiles(DirectoryHelpers.ReplaceEnvironmentVariables(Path.Combine(sldWorkflowPath, "UPCExport")),
                    "*.xaml")
                    .Select(
                        f =>
                            new WorkflowContainer
                            {
                                WorkflowName = Path.GetFileNameWithoutExtension(f),
                                WorkflowPath = Path.Combine(sldWorkflowPath, "UPCExport", Path.GetFileName(f))
                            }).ToList();

            return new SLDWorkflows()
            {
                ImportWorkflows = importWorkflows,
                UPCExportWorkflows = upcExportWorkflows,
                UPCImportWorkflows = upcImportWorkflows
            };
        }
    }
}
