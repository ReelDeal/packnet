﻿using MongoDB.Bson.Serialization.Attributes;

namespace PackNet.SLDService.Common
{
    public class UPCImportItem : UPCItem
    {
        [BsonIgnore]
        public UPCImportAction ImportAction { get; set; }
    }
}
