﻿using System.Collections.Generic;

namespace PackNet.SLDService.Common
{
    public class ImportCollection
    {
        public ImportCollection()
        {
            Items = new List<UPCImportItem>();
            FailedImportRows = new List<int>();
        }

        public List<UPCImportItem> Items { get; set; }

        public int SuccessfulImports
        {
            get { return Items.Count; }
        }

        public int FailedImports
        {
            get { return FailedImportRows.Count; }
        }

        public List<int> FailedImportRows { get; set; } 
    }
}
