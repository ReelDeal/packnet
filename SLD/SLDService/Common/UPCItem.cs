﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.SLDService.Common
{
    public class UPCItem : IPersistable
    {
        public UPCItem()
        {
            CreateDate = DateTime.Now;
            LastUpdatedDate = DateTime.Now;
            History = History ?? new List<UPCItem>();
        }

        private UPCItem(UPCItem upcItem)
        {
            Id = upcItem.Id;
            UPC = upcItem.UPC;
            Description = upcItem.Description;
            Length = upcItem.Length;
            Width = upcItem.Width;
            Height = upcItem.Height;
            Weight = upcItem.Weight;
            DesignId = upcItem.DesignId;
            CreateDate = upcItem.CreateDate;
            LastUpdatedDate = upcItem.LastUpdatedDate;
            History = new List<UPCItem>();
        }

        public Guid Id { get; set; }

        /// <summary>
        /// Universal Product Code
        /// </summary>
        public string UPC { get; set; }

        public string Description { get; set; }

        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }

        public int DesignId { get; set; }

        public DateTime CreateDate { get; set; }
        public DateTime LastUpdatedDate { get; set; }

        [JsonIgnore]
        public List<UPCItem> History { get; set; }

        public void UpdateFrom(UPCItem newUpc)
        {
            History.Add(new UPCItem(this));

            Description = newUpc.Description;
            Length = newUpc.Length;
            Width = newUpc.Width;
            Height = newUpc.Height;
            Weight = newUpc.Weight;
            DesignId = newUpc.DesignId;
            LastUpdatedDate = DateTime.Now;
        }
    }
}
