﻿using System.Collections.Generic;

namespace PackNet.SLDService.Common
{
    public class SLDWorkflows
    {
        public List<WorkflowContainer> ImportWorkflows { get; set; }
        public List<WorkflowContainer> UPCImportWorkflows { get; set; }
        public List<WorkflowContainer> UPCExportWorkflows { get; set; } 
    }
}
