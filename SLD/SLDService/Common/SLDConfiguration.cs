﻿using System;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.SLDService.Common
{
    public class SLDConfiguration : IPersistable
    {
        public Guid Id { get; set; }

        public string ImportWorkflowPath { get; set; }

        public string UPCImportWorkflowPath { get; set; }

        public string UPCExportWorkflowPath { get; set; }

        public string ImportFileDelimeter { get; set; }

        public string ImportFileCommentPrefix { get; set; }

        public UPCImportAction ConflictResolutionMethod { get; set; }
    }
}
