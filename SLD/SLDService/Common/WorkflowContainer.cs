﻿namespace PackNet.SLDService.Common
{
    public class WorkflowContainer
    {
        public string WorkflowName { get; set; }
        public string WorkflowPath { get; set; }
    }
}
