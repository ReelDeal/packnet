﻿namespace PackNet.SLDService.Common
{
    public class UPCOrder : UPCItem
    {
        public int Quantity { get; set; }
    }
}
