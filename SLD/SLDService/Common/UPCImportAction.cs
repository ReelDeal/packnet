﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.SLDService.Common
{
    public class UPCImportAction : Enumeration
    {
        private static readonly Dictionary<string, UPCImportAction> instance = new Dictionary<string, UPCImportAction>();

        public static UPCImportAction Import = new UPCImportAction("Import");
        public static UPCImportAction Ignore = new UPCImportAction("Ignore");
        public static UPCImportAction Update = new UPCImportAction("Update");

        public static void Touch()
        {
#pragma warning disable 1717
            Ignore = Ignore;
            Update = Update;
            Import = Import;
#pragma warning restore 1717
        }

        protected UPCImportAction(string name)
            : base(0, name)
        {
            instance[name] = this;
        }

        /* This method is to support explicit conversion */
        public static explicit operator UPCImportAction(string value)
        {
            UPCImportAction result;

            if (!instance.TryGetValue(value, out result))
                throw new InvalidCastException();

            return result;
        }
    }
}
