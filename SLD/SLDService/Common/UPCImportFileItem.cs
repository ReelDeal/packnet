﻿namespace PackNet.SLDService.Common
{
    public class UPCImportFileItem
    {
        public string Contents { get; set; }
        public char Delimiter { get; set; }
        public char CommentPrefix { get; set; }
        public bool HeadersInFile { get; set; }
        public string[] HeaderFields { get; set; }
    }
}
