﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.SLDService.Common
{
    public class UPCMessages : MessageTypes
    {
        /* Discovery */
        public static UPCMessages DiscoverSLDService = new UPCMessages("DiscoverSLDService");
        public static readonly UPCMessages DiscoverSLDServiceResponse = new UPCMessages("DiscoverSLDServiceResponse");

        /* Commands */
        public static readonly UPCMessages GetUPCs = new UPCMessages("GetUPCs");
        public static readonly UPCMessages LocateUPC = new UPCMessages("LocateUPC");
        public static readonly UPCMessages DeleteUPCs = new UPCMessages("DeleteUPCs");
        public static readonly UPCMessages UpdateUPC = new UPCMessages("UpdateUPC");
        public static readonly UPCMessages CreateUPC = new UPCMessages("CreateUPC");
        public static readonly UPCMessages ProcessUPCs = new UPCMessages("ProcessUPCs");
        public static readonly UPCMessages ImportUPCs = new UPCMessages("ImportUPCs");
        public static readonly UPCMessages FinalizeUPCImport = new UPCMessages("FinalizeUPCImport");
        public static readonly UPCMessages ExportUPCs = new UPCMessages("ExportUPCs");
        public static readonly UPCMessages GetWorkflows = new UPCMessages("GetWorkflows");
        public static readonly UPCMessages GetCurrentSldConfiguration = new UPCMessages("GetCurrentSldConfiguration");
        public static readonly UPCMessages SaveSLDConfiguration = new UPCMessages("SaveSLDConfiguration");

        /* Responses */
        public static readonly UPCMessages UPCLocated = new UPCMessages("UPCLocated");
        public static readonly UPCMessages UPCCreated = new UPCMessages("UPCCreated");
        public static readonly UPCMessages UPCUpdated = new UPCMessages("UPCUpdated");
        public static readonly UPCMessages UPCsDeleted = new UPCMessages("UPCsDeleted");
        public static readonly UPCMessages UPCsProcessed = new UPCMessages("UPCsProcessed");
        public static readonly UPCMessages UPCsUnderImport = new UPCMessages("UPCsUnderImport");
        public static readonly UPCMessages UPCsImported = new UPCMessages("UPCsImported");
        public static readonly UPCMessages AllUPCs = new UPCMessages("AllUPCs");
        public static readonly UPCMessages GetWorkflowsResponse = new UPCMessages("GetWorkflowsResponse");
        public static readonly UPCMessages ExportUPCsResponse = new UPCMessages("ExportUPCsResponse");
        public static readonly UPCMessages GetCurrentSldConfigurationResponse = new UPCMessages("GetCurrentSldConfigurationResponse");
        public static readonly UPCMessages SaveSLDConfigurationResponse = new UPCMessages("SaveSLDConfigurationResponse");

        public static void Touch()
        {
#pragma warning disable 1717
            DiscoverSLDService = DiscoverSLDService;
#pragma warning restore 1717
        }

        private UPCMessages(string name)
            : base(name)
        {
        }
    }
}
