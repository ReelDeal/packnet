﻿using System.Diagnostics;
using System.Linq;

using MongoDB.Driver.Builders;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.SLDService.Common;

namespace PackNet.SLDService.Data
{
    public class UPCRepository : MongoDbRepository<UPCItem>, IUPCRepository 
    {
        public UPCRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "UPCItems")
        {

        }

        public UPCItem FindByUPC(string upc)
        {
            var sw = Stopwatch.StartNew();
            var query = Query<UPCItem>.EQ(e => e.UPC, upc);
            var item = Collection.Find(query).FirstOrDefault();
            Trace.WriteLine(string.Format("Took {0} ms for db to find item by upc {1}", sw.ElapsedMilliseconds, typeof(UPCItem)));
            return item;
        }
    }
}
