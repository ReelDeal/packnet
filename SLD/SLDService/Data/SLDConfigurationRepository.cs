﻿using System.Linq;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using PackNet.SLDService.Common;

namespace PackNet.SLDService.Data
{
    public class SLDConfigurationRepository : MongoDbRepository<SLDConfiguration>, ISLDConfigurationRepository
    {
        private readonly object syncObject = new object();
        private readonly bool serializerRegistered;

        public SLDConfigurationRepository() : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "SLDConfiguration")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<UPCImportAction>(new EnumerationSerializer());

                serializerRegistered = true;
            }

            if (All().Any() == false)
            {
                Create(new SLDConfiguration()
                {
                    ImportWorkflowPath = "%PackNetWorkflows%\\SLD\\Import\\SLDOrderImportNoCube.xaml",
                    UPCExportWorkflowPath = "%PackNetWorkflows%\\SLD\\UPCExport\\ExportUPCs.xaml",
                    UPCImportWorkflowPath = "%PackNetWorkflows%\\SLD\\UPCImport\\ImportUPCs.xaml",
                    ImportFileDelimeter = ";",
                    ImportFileCommentPrefix = "~",
                    ConflictResolutionMethod = UPCImportAction.Ignore
                });
            }
        }

        public SLDConfiguration CurrentConfiguration
        {
            get { return All().First(); }
        }

        public void UpdateSldConfiguration(SLDConfiguration newConfiguration)
        {
            newConfiguration.Id = CurrentConfiguration.Id;

            Update(newConfiguration);
        }

    }
}
