﻿using PackNet.SLDService.Common;

namespace PackNet.SLDService.Data
{
    public interface ISLDConfigurationRepository 
    {
        void UpdateSldConfiguration(SLDConfiguration newConfiguration);

        SLDConfiguration CurrentConfiguration { get; }
    }
}
