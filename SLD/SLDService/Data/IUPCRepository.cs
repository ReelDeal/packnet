﻿using PackNet.Common.Interfaces.Repositories;
using PackNet.SLDService.Common;

namespace PackNet.SLDService.Data
{
    public interface IUPCRepository : IRepository<UPCItem>
    {
        UPCItem FindByUPC(string upc);
    }
}