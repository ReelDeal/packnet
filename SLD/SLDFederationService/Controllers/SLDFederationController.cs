﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using PackNet.Common.Interfaces.Logging;

namespace SLDFederationService.Controllers
{
    public class SLDFederationController : ApiController
    {
        private readonly ILogger logger;

        /// <summary>
        /// Creates a SLDFederationController
        /// </summary>
        /// <param name="logger"></param>
        public SLDFederationController(ILogger logger)
        {
            this.logger = logger;
        }

        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
