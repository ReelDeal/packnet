﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Business.CartonPropertyGroup
{
    using System;
    using System.Collections.Generic;

    using Common.Interfaces.DTO.Carton;
    using Common.Interfaces.DTO.CartonPropertyGroups;

    public interface ICartonPropertyGroups
    {
        IEnumerable<CartonPropertyGroup> Groups { get; }

        IEnumerable<CartonPropertyGroup> GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatuses status);

        CartonPropertyGroup GetCartonPropertyGroupByAlias(string alias);

        void SetCartonPropertyGroupStatus(string alias, CartonPropertyGroupStatuses status);


        CartonPropertyGroup Create(CartonPropertyGroup cartonPropertyGroup);
        void Delete(CartonPropertyGroup cartonPropertyGroup);
        void Delete(IEnumerable<CartonPropertyGroup> cartonPropertyGroups);
        CartonPropertyGroup Update(CartonPropertyGroup cartonPropertyGroup);
        CartonPropertyGroup Find(Guid cartonPropertyGroup);

        IEnumerable<CartonPropertyGroup> GetCartonPropertyGroups();
    }
}
