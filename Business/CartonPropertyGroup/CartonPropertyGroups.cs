﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using PackNet.Business.ProductionGroups;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Data.CartonPropertyGroups;

namespace PackNet.Business.CartonPropertyGroup
{
    using Common.Interfaces.DTO.CartonPropertyGroups;

    public class CartonPropertyGroups : ICartonPropertyGroups
    {
        private readonly ICartonPropertyGroupRepository repository;
        private readonly IProductionGroup productionGroups;

        public CartonPropertyGroups(ICartonPropertyGroupRepository repository,
            IEventAggregatorSubscriber subscriber,
            IProductionGroup productionGroups
            )
        {
            this.repository = repository;
            this.productionGroups = productionGroups;

            subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(m => m.MessageType == CartonPropertyGroupMessages.UpdateCartonPropertyGroupStatus)
                .Subscribe(m => SetCartonPropertyGroupStatus(m.Data.Alias, m.Data.Status));
        }

        public IEnumerable<CartonPropertyGroup> Groups
        {
            get { return repository.All(); }
        }

        public IEnumerable<CartonPropertyGroup> GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatuses status)
        {
            return Groups.Where(g => g.Status == status).AsEnumerable();
        }

        public CartonPropertyGroup GetCartonPropertyGroupByAlias(string alias)
        {
            return Groups.SingleOrDefault(g => string.Equals(g.Alias, alias, StringComparison.CurrentCultureIgnoreCase));
        }

        public void SetCartonPropertyGroupStatus(string alias, CartonPropertyGroupStatuses status)
        {
            var cpg = Groups.SingleOrDefault(g => g.Alias == alias);

            if (cpg != null)
            {
                cpg.Status = status;
                cpg.RemainingProduciblesToSurge = status == CartonPropertyGroupStatuses.Surge ? cpg.SurgeCount : 0;
                Update(cpg);
            }
        }

        public CartonPropertyGroup Create(CartonPropertyGroup cartonPropertyGroup)
        {
            if (cartonPropertyGroup.Id != Guid.Empty)
            {
                throw new Exception("Invalid Carton Property Group ID, Id is already populated");
            }

            if (GetCartonPropertyGroupByAlias(cartonPropertyGroup.Alias) != null)
            {
                throw new AlreadyPersistedException("Could not Create Carton Property Group, Alias is already in use.");
            }

            repository.Create(cartonPropertyGroup);

            return cartonPropertyGroup;
        }

        public void Delete(CartonPropertyGroup cartonPropertyGroup)
        {
            if (cartonPropertyGroup.NumberOfRequests > 0)
                throw new InUseException("Cannot delete carton property group with assigned work");

            var productionGroup = productionGroups.FindByCartonPropertyGroupAlias(cartonPropertyGroup.Alias);
            if (productionGroup != null)
                throw new RelationshipExistsException(string.Format("Unable to delete, Carton Property Group {0} is related to the Production Group {1}", cartonPropertyGroup.Alias, productionGroup.Alias));

            try
            {
                repository.Delete(cartonPropertyGroup);
            }
            catch (Exception e)
            {
                throw new Exception("Could not Delete Carton Property Group(s)", e);
            }
        }

        public void Delete(IEnumerable<CartonPropertyGroup> cartonPropertyGroups)
        {
            if (cartonPropertyGroups.Any(cpg => cpg.NumberOfRequests > 0))
                throw new InUseException("Cannot delete Carton Property Group with assigned work");
            try
            {
                repository.Delete(cartonPropertyGroups);
            }
            catch (Exception e)
            {
                throw new Exception("Could not Delete Carton Property Group(s)", e);
            }
        }

        public CartonPropertyGroup Update(CartonPropertyGroup cartonPropertyGroup)
        {
            if (cartonPropertyGroup.Id == Guid.Empty)
            {
                throw new Exception("Could not Update Carton Property Group, Repository Id was Empty");
            }

            var existing = GetCartonPropertyGroupByAlias(cartonPropertyGroup.Alias);

            if (existing != null && existing.Id != cartonPropertyGroup.Id)
            {
                throw new AlreadyPersistedException("Could not Update Carton Property Group, Alias is already in use.");
            }

            var existingCpg = Groups.SingleOrDefault(g => g.Id == cartonPropertyGroup.Id);
            var aliasHasChanged = (existingCpg != null && existingCpg.Alias != cartonPropertyGroup.Alias);

            if (aliasHasChanged && existingCpg.NumberOfRequests > 0)
            {
                throw new InUseException("Cannot update alias for Carton Property Group with assigned work");
            }

            var updatedCpg = repository.Update(cartonPropertyGroup);

            if (aliasHasChanged)
            {
                productionGroups.UpdateRelatedCartonPropertyGroup(cartonPropertyGroup);
            }

            return updatedCpg;
        }

        public CartonPropertyGroup Find(Guid repositoryId)
        {
            return repository.Find(repositoryId);
        }

        public IEnumerable<CartonPropertyGroup> GetCartonPropertyGroups()
        {
            return repository.All();
        }
    }
}
