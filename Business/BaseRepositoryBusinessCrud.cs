﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Business
{
    public class BaseRepositoryBusinessCrud<T> where T : IPersistable
    {
        public IRepository<T> Repo { get; set; }

        public BaseRepositoryBusinessCrud(IRepository<T> repo )
        {
            Repo = repo;
        }

        public virtual T Create(T obj)
        {
            Repo.Create(obj);
            return obj;
        }

        public virtual void CreateRange(IEnumerable<T> objs)
        {
            Repo.Create(objs);
        }
        
        public virtual void Delete(T obj)
        {
            Repo.Delete(obj);
        }

        public virtual T Update(T obj)
        {
            Repo.Update(obj);
            return obj;
        }

        public virtual void UpdateRange(List<T> objs)
        {
            Repo.Update(objs);
        }

        public virtual T Get(Guid guidId)
        {
            return Repo.Find(guidId);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Repo.All().ToList();
        }

    }
}
