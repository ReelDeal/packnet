﻿using PackNet.Business.CodeGenerator;
using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Business.EmJobCreator
{
    using DesignRulesApplicator;

    public class EmMachineInstructionCreator : MachineInstructionCreator<EmPhysicalMachineSettings>
    {
        public EmMachineInstructionCreator(ILogger logger) : base(logger)
        {
        }

        /// <summary>
        /// Shortens overlapping lines, replace connected lines with one line, etc.
        /// </summary>
        protected override RuleAppliedPhysicalDesign ApplyRules(PhysicalDesign measureAppliedDesign, EmPhysicalMachineSettings machineSettings, Corrugate corrugate, OrientationEnum rotation)
        {
            var designRuleApplicator = new DesignRulesApplicator(machineSettings.WasteAreaParameters);

            return designRuleApplicator.ApplyRules(measureAppliedDesign,
                new EmPerforationLineRule(machineSettings.CodeGeneratorSettings.PerforationSafetyDistance),
                new EmNunatabLinesRule(), corrugate, rotation);
        }


        protected override RuleAppliedPhysicalDesign ApplyDesignCompensation(CompensationParameters parameters)
        {
            return new EmDesignCompensator().Compensate(parameters);
        }

        protected override CompensationParameters GetCompensationParameters(RuleAppliedPhysicalDesign design,
            MicroMeter trackOffset, MicroMeter corrugateWidth, EmPhysicalMachineSettings machineSettings, IPacksizeCarton order)
        {
            var compensationParameters = new CompensationParameters(
                design,
                IsTrackRighSideFixed(machineSettings, order.TrackNumber),
                trackOffset,
                corrugateWidth,
                GetCrossHead(machineSettings).CutCompensation,
                GetCrossHead(machineSettings).CreaseCompensation,
                machineSettings.CodeGeneratorSettings.MinimumLineLength,
                machineSettings.CodeGeneratorSettings.MinimumLineDistanceToCorrugateEdge,
                machineSettings.LongHeadParameters.CutCompensation,
                machineSettings.LongHeadParameters.CreaseCompensation);

            return compensationParameters;
        }

        protected override void CreateCodeGenerator(EmPhysicalMachineSettings machineSettings)
        {
            if (CodeGenerator == null)
            {
                CodeGenerator = new EmCodeGenerator(GetCrossHead(machineSettings).Position, machineSettings.LongHeadParameters);
            }
        }

        private EmCrossHeadParameters GetCrossHead(EmPhysicalMachineSettings machineSettings)
        {
            return machineSettings.CrossHeadParameters;
        }

        protected override bool IsTrackRighSideFixed(EmPhysicalMachineSettings machineSettings, int trackNumber)
        {
            return false;
        }

        protected override MicroMeter GetCorrugateWidthTolerance(EmPhysicalMachineSettings machineSettings)
        {
            return machineSettings.TrackParameters.CorrugateWidthTolerance;
        }

        protected override bool IsOnTheFly(EmPhysicalMachineSettings machineSettings)
        {
            return machineSettings.OnTheFlyActive;
        }
    }
}