﻿using PackNet.Common.Interfaces.Logging;
using PackNet.Data.Classifications;

namespace PackNet.Business.Classifications
{
    public class Classifications : BaseRepositoryBusinessCrud<Common.Interfaces.DTO.Classifications.Classification>, IClassifications
    {
        public ILogger Logger { get; set; }
        public Classifications(IClassificationRepository repo, 
            ILogger logger) : base(repo)
        {
            Logger = logger;
        }
    }
}
