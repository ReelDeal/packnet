﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Logging;

namespace PackNet.Business.Classifications
{
    public interface IClassifications
    {
        Common.Interfaces.DTO.Classifications.Classification Create(Common.Interfaces.DTO.Classifications.Classification obj);

        void Delete(Common.Interfaces.DTO.Classifications.Classification obj);

        Common.Interfaces.DTO.Classifications.Classification Update(Common.Interfaces.DTO.Classifications.Classification obj);

        Common.Interfaces.DTO.Classifications.Classification Get(Guid guidId);

        IEnumerable<Common.Interfaces.DTO.Classifications.Classification> GetAll();
    }
}