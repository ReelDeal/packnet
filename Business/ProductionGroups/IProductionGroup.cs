﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Utils;

using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

namespace PackNet.Business.ProductionGroups
{
    public interface IProductionGroup
    {
        CommonProductionGroups.ProductionGroup Create(CommonProductionGroups.ProductionGroup productionGroup);
        void Delete(CommonProductionGroups.ProductionGroup productionGroup);
        CommonProductionGroups.ProductionGroup Update(CommonProductionGroups.ProductionGroup productionGroup);
        CommonProductionGroups.ProductionGroup Find(Guid productionGroupId);

        IEnumerable<CommonProductionGroups.ProductionGroup> GetProductionGroups();

        CommonProductionGroups.ProductionGroup GetProductionGroupForMachineGroup(Guid machineGroupId);

        CommonProductionGroups.ProductionGroup FindByCartonPropertyGroupId(Guid cartonPropertyGroupId);

        CommonProductionGroups.ProductionGroup FindByCartonPropertyGroupId(IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> productionGroups, Guid cartonPropertyGroupId);

        CommonProductionGroups.ProductionGroup FindByCartonPropertyGroupAlias(string cartonPropertyGroupAlias);

        CommonProductionGroups.ProductionGroup FindByCartonPropertyGroupAlias(IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> productionGroups, string cartonPropertyGroupAlias);

        CommonProductionGroups.ProductionGroup FindByAlias(string alias);

        ConcurrentList<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> GetProductionGroupsForCorrugate(Guid corrugateId);

        void RemoveCorrugateFromProductionGroups(Corrugate corrugate);

        void RemoveMachineGroupFromProductionGroup(MachineGroup machineGroup);

        void UpdateRelatedCartonPropertyGroup(Common.Interfaces.DTO.CartonPropertyGroups.CartonPropertyGroup cartonPropertyGroup);
    }
}
