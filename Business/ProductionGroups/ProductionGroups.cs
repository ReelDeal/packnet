﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Newtonsoft.Json;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.ProductionGroups;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;

namespace PackNet.Business.ProductionGroups
{
    public class ProductionGroups : IProductionGroup
    {
        private readonly IEventAggregatorSubscriber eventAggregatorSubscriber;
        private readonly IEventAggregatorPublisher eventAggregatorPublisher;
        private readonly IProductionGroupRepository productionGroupRepository;
        private IMachineGroupService machineGroupService;
        private IUserNotificationService userNotificationService;
        private readonly ILogger logger;

        public ProductionGroups(
            IEventAggregatorSubscriber eventAggregatorSubscriber,
            IEventAggregatorPublisher eventAggregatorPublisher,
            IProductionGroupRepository productionGroupRepository,
            IServiceLocator serviceLocator,
            ILogger logger)
        {
            this.eventAggregatorSubscriber = eventAggregatorSubscriber;
            this.eventAggregatorPublisher = eventAggregatorPublisher;
            this.productionGroupRepository = productionGroupRepository;

            IDisposable userNotificationServiceSubscription = null;
            userNotificationServiceSubscription = serviceLocator.ServiceAddedObservable.Where(s => s is IUserNotificationService).Subscribe(s =>
            {
                userNotificationService = s as IUserNotificationService;
                userNotificationServiceSubscription.Dispose();
            });

            IDisposable machineGroupServiceSubscription = null;
            machineGroupServiceSubscription = serviceLocator.ServiceAddedObservable.Where(s => s is IMachineGroupService).Subscribe(s =>
            {
                machineGroupService = s as IMachineGroupService;
                machineGroupServiceSubscription.Dispose();
            });
            
            this.logger = logger;

            CreateObservers();
        }

        private void CreateObservers()
        {
            eventAggregatorSubscriber.GetEvent<IMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>()
                .Where(evt => evt.MessageType == ProductionGroupMessages.ProductionGroupConfiguration)
                .DurableSubscribe(ProductionGroupUpdated, logger);
        }

        public void ProductionGroupUpdated(IMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> msg)
        {
            productionGroupRepository.Update(msg.Data);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup Create(
            Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
			if (FindByAlias(productionGroup.Alias) != null)
				throw new ItemExistsException("Invalid Production Group, Production Group Name already exists");

            productionGroupRepository.Create(productionGroup);
            //TODO: The communications shouldn't be in the manager layer, should be in the service layer instead.
            var message = new Message<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>
            {
                Data = productionGroup,
                MessageType = ProductionGroupMessages.ProductionGroupCreated
            };

            eventAggregatorPublisher.Publish(message);
            return productionGroup;
        }

        public void Delete(Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            productionGroupRepository.Delete(productionGroup);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup Update(
            Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
	        var pg = FindByAlias(productionGroup.Alias);

			if (pg != null && pg.Id != productionGroup.Id)
				throw new ItemExistsException("Invalid Production Group, Production Group Name already exists");

            var previousPG = Find(productionGroup.Id);
            var machinesThatHaveChanged = previousPG.ConfiguredMachineGroups.Except(productionGroup.ConfiguredMachineGroups);
            
			if (machinesThatHaveChanged.Any())
            {
                eventAggregatorPublisher.Publish(new Message<List<Guid>>
                {
                    MessageType = MachineGroupMessages.CompleteQueueAndPauseMachineGroup,
                    Data = machinesThatHaveChanged.ToList()
                });
            }

            pg = productionGroupRepository.Update(productionGroup);

            foreach (var machineGroupdIdThatMoved in machinesThatHaveChanged)
            {
                userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Warning,
                    String.Format("Machine {0} has been moved to a new production group. It may take some time to route work.",
                        machineGroupService.FindByMachineGroupId(machineGroupdIdThatMoved).Alias), "", machineGroupdIdThatMoved);
            }

            var message = new Message<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>
            {
                Data = pg,
                MessageType = ProductionGroupMessages.ProductionGroupChanged
            };
            
			eventAggregatorPublisher.Publish(message);

            return pg;
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup Find(Guid productionGroupId)
        {
            return productionGroupRepository.Find(productionGroupId);
        }

        public IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> GetProductionGroups()
        {
            return productionGroupRepository.All();
        }

        //private void PopulateCorrugates(params Common.Interfaces.DTO.ProductionGroups.ProductionGroup[] productionGroups)
        //{
        //    foreach (var productionGroup in productionGroups)
        //    {
        //        var corrugates = productionGroup.ConfiguredCorrugates.Select(x => corrugates.Find(x.Id)).ToList();
        //        productionGroup.ConfiguredCorrugates = corrugates;
        //    }
        //}

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup GetProductionGroupForMachineGroup(Guid machineGroupId)
        {
            return productionGroupRepository.FindByMachineGroup(machineGroupId);
            ////PopulateCorrugates(pg as Common.Interfaces.DTO.ProductionGroups.ProductionGroup);
            //return pg;
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupId(Guid cartonPropertyGroupId)
        {
            return FindByCartonPropertyGroupId(GetProductionGroups(), cartonPropertyGroupId);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupId(IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> productionGroups, Guid cartonPropertyGroupId)
        {
            var boxFirstGroups = productionGroups.Where(g => g.SelectionAlgorithm == SelectionAlgorithmTypes.BoxFirst);

            return
                boxFirstGroups.FirstOrDefault(
                    bf => bf.BoxFirstConfiguration().ConfiguredCartonPropertyGroups.Any(cpg => cpg.Id == cartonPropertyGroupId));
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupAlias(string cartonPropertyGroupAlias)
        {
            return FindByCartonPropertyGroupAlias(GetProductionGroups(), cartonPropertyGroupAlias);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupAlias(IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> productionGroups, string cartonPropertyGroupAlias)
        {
            var boxFirstGroups = productionGroups.Where(g => g.SelectionAlgorithm == SelectionAlgorithmTypes.BoxFirst);

            return
                boxFirstGroups.FirstOrDefault(
                    bf => bf.BoxFirstConfiguration().ConfiguredCartonPropertyGroups.Any(cpg => cpg.Alias == cartonPropertyGroupAlias));
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByAlias(string alias)
        {
            return GetProductionGroups().FirstOrDefault(g => g.Alias.ToLowerInvariant() == alias.ToLowerInvariant());
        }

        public ConcurrentList<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> GetProductionGroupsForCorrugate(Guid corrugateId)
        {
            return new ConcurrentList<ProductionGroup>(GetProductionGroups().Where(pg => pg.ConfiguredCorrugates.Any(m => m == corrugateId)).ToList());
        }

        public void RemoveCorrugateFromProductionGroups(Corrugate corrugate)
        {
            var productionGroupsWithCorrugate = GetProductionGroupsForCorrugate(corrugate.Id);
            productionGroupsWithCorrugate.ForEach(productionGroup =>
            {
                productionGroup.ConfiguredCorrugates.Remove(corrugate.Id);
                Update(productionGroup);
            });
        }

        public void RemoveMachineGroupFromProductionGroup(MachineGroup machineGroup)
        {
            var productionGroup = GetProductionGroupForMachineGroup(machineGroup.Id);
            if (productionGroup != null)
            {
                productionGroup.ConfiguredMachineGroups.Remove(machineGroup.Id);
                Update(productionGroup);
            }
        }

        public void UpdateRelatedCartonPropertyGroup(Common.Interfaces.DTO.CartonPropertyGroups.CartonPropertyGroup cartonPropertyGroup)
        {
            var productionGroup = FindByCartonPropertyGroupId(cartonPropertyGroup.Id);
            if (productionGroup == null)
            {
                return;
            }

            string configuredCartonPropertyGroupsString;
            if (!productionGroup.Options.TryGetValue("ConfiguredCartonPropertyGroups", out configuredCartonPropertyGroupsString))
            {
                return;
            }

            var configuredCartonPropertyGroups =
                JsonConvert.DeserializeObject<ConcurrentList<Dictionary<string, object>>>(
                    configuredCartonPropertyGroupsString,
                    SerializationSettings.GetJsonSerializerSettings());

            if (configuredCartonPropertyGroups == null)
            {
                return;
            }

            var cpg =
                configuredCartonPropertyGroups.SingleOrDefault(c => c["Id"].ToString() == cartonPropertyGroup.Id.ToString() &&
                                                                    c["Alias"].ToString() != cartonPropertyGroup.Alias);
            if (cpg == null)
            {
                return;
            }
            cpg["Alias"] = cartonPropertyGroup.Alias;
            productionGroup.Options["ConfiguredCartonPropertyGroups"] = JsonConvert.SerializeObject(configuredCartonPropertyGroups);
            Update(productionGroup);
        }
    }
}