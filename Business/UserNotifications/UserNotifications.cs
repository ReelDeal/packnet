﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Data.UserNotifications;

namespace PackNet.Business.UserNotifications
{
	public class UserNotifications : IUserNotifications
	{
		private readonly IUserNotificationRepository repository;

		public UserNotifications(IUserNotificationRepository repository)
		{
			this.repository = repository;
		}

		/// <summary>
		/// Create the specified user notification.
		/// </summary>
		/// <param name="userNotification">User notification.</param>
		public UserNotification Create(UserNotification userNotification)
		{
			if (userNotification.Id != Guid.Empty)
			{
				throw new Exception("Invalid User Notification ID, Id is already populated");
			}

			if (userNotification.NotifiedUsers.Count == 0)
			{
				throw new Exception("There must be at least one notified user");
			}

			repository.Create(userNotification);

			return userNotification;
		}

		/// <summary>
		/// Update the specified user notification.
		/// </summary>
		/// <param name="userNotification">User notification.</param>
		public UserNotification Update(UserNotification userNotification)
		{
			if (userNotification.Id == Guid.Empty)
			{
				throw new Exception("Could not Update User Notification, Repository Id was Empty");
			}

			return repository.Update(userNotification);
		}

		/// <summary>
		/// Delete the specified user notification.
		/// </summary>
		/// <param name="userNotification">User notification.</param>
		public void Delete(UserNotification userNotification)
		{
			try
			{
				repository.Delete(userNotification);
			}
			catch (Exception e)
			{
				throw new Exception("Could not Delete User Notification", e);
			}
		}

		/// <summary>
		/// Dismiss all notifications of type "System" for the specified user.
		/// </summary>
		/// <param name="userId">User identification.</param>
		public void DismissAllByUser(Guid userId)
		{
			repository.DismissAllByUser(userId);
		}

		/// <summary>
		/// Dismiss all notifications sent to the specified machine group for the specified user.
		/// </summary>
		/// <param name="machineGroupId">Machine Group identification.</param>
		/// <param name="userId">User identification.</param>
		public void DismissAllByUser(Guid machineGroupId, Guid userId)
		{
			repository.DismissAllByUser(machineGroupId, userId);
		}

		/// <summary>
		/// Dismiss the specified user notification.
		/// </summary>
		/// <param name="userNotificationId">User notification identification.</param>
		/// <param name="userId">User identification.</param>
		public void DismissByUser(Guid userNotificationId, Guid userId)
		{
			repository.DismissByUser(userNotificationId, userId);
		}

		/// <summary>
		/// Find a notification.
		/// </summary>
		/// <param name="userNotificationId">User notification identification.</param>
		/// <returns></returns>
		public UserNotification FindById(Guid userNotificationId)
		{
			return repository.Find(userNotificationId);
		}

		/// <summary>
		/// Find all notifications for the specified user.
		/// </summary>
		/// <param name="userId">User identification.</param>
		/// <returns></returns>
		public IEnumerable<UserNotification> FindByUserId(Guid userId)
		{
			return repository.FindByUserId(userId);
		}

		/// <summary>
		/// Find all active notifications between the 24 hour limit for the specified user.
		/// </summary>
		/// <param name="userId">User identification.</param>
		/// <returns></returns>
		public IEnumerable<UserNotification> FindActiveByUserId(Guid userId)
		{
			return repository.FindActiveByUserId(userId);
		}

		/// <summary>
		/// Get all users notifications.
		/// </summary>
		/// <returns></returns>
		public IEnumerable<UserNotification> GetUserNotifications()
		{
			return repository.All();
		}

		/// <summary>
		/// Search for user notifications that matches the supplied criterion
		/// </summary>
		/// <param name="dateFrom">Begin date to start looking for.</param>
		/// <param name="dateTo">End date to stop looking for.</param>
		/// <param name="notificationTypes">Notification types to match.</param>
		/// <param name="notificationSeverities">Notification severities to match.</param>
		/// <returns></returns>
		public IEnumerable<UserNotification> SearchUserNotifications(DateTime dateFrom, DateTime dateTo, IEnumerable<NotificationType> notificationTypes, IEnumerable<NotificationSeverity> notificationSeverities )
		{
			return repository.SearchUserNotifications(dateFrom, dateTo, notificationTypes, notificationSeverities);
		}
	}
}