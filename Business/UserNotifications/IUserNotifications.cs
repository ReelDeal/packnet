﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Business.UserNotifications
{
	public interface IUserNotifications
	{
		UserNotification Create(UserNotification userNotification);
		UserNotification Update(UserNotification userNotification);
		void Delete(UserNotification userNotification);
		void DismissAllByUser(Guid userId);
		void DismissAllByUser(Guid machineGroupId, Guid userId);
		void DismissByUser(Guid userNotificationId, Guid userId);
		UserNotification FindById(Guid userNotificationId);
		IEnumerable<UserNotification> FindByUserId(Guid userId);
		IEnumerable<UserNotification> FindActiveByUserId(Guid userId);
		IEnumerable<UserNotification> GetUserNotifications();
		IEnumerable<UserNotification> SearchUserNotifications(DateTime dateFrom, DateTime dateTo, IEnumerable<NotificationType> notificationTypes, IEnumerable<NotificationSeverity> notificationSeverities );
	}
}