﻿using System.Linq;

using PackNet.Business.DesignCompensator;

namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public static class LinesOutsideDesignRule
    {
        public static PhysicalDesign ApplyTo(PhysicalDesign design)
        {
            
            RemoveCompletelyOutsideHorizontalLines(design);
            MoveHorizontalLinesThatAreVerticallyOutsideToDesignEdge(design);

            RemoveCompletelyOutsideVerticalLines(design);
            MoveVerticalLinesThatAreHorizontallyOutsideToDesignEdge(design);
            ModifyPartiallyOutsideLines(design);

            return design;
        }

        private static void MoveVerticalLinesThatAreHorizontallyOutsideToDesignEdge(PhysicalDesign design)
        {
            foreach (var line in design.GetVerticalPhysicalLines())
            {
                if (line.StartCoordinate.X < 0)
                    line.StartCoordinate.X = line.EndCoordinate.X = 0;
                else if (line.StartCoordinate.X > design.Width)
                    line.StartCoordinate.X = line.EndCoordinate.X = design.Width;
            }
        }

        private static void RemoveCompletelyOutsideVerticalLines(PhysicalDesign design)
        {
            var linesToRemove =
                design.GetVerticalPhysicalLines()
                    .Where(
                        line =>
                            (line.StartCoordinate.Y <= 0 && line.EndCoordinate.Y <= 0) ||
                            (line.StartCoordinate.Y >= design.Length && line.EndCoordinate.Y >= design.Length))
                    .ToList();

            linesToRemove.ForEach(design.RemoveLine);
        }

        private static void MoveHorizontalLinesThatAreVerticallyOutsideToDesignEdge(PhysicalDesign design)
        {
            foreach (var line in design.GetHorizontalPhysicalLines())
            {
                if (line.StartCoordinate.Y < 0)
                    line.StartCoordinate.Y = line.EndCoordinate.Y = 0;
                else if (line.StartCoordinate.Y > design.Length)
                    line.StartCoordinate.Y = line.EndCoordinate.Y = design.Length;
            }
        }

        private static void RemoveCompletelyOutsideHorizontalLines(PhysicalDesign design)
        {
            var linesToRemove =
                design.GetHorizontalPhysicalLines()
                    .Where(
                        line =>
                            (line.StartCoordinate.X <= 0 && line.EndCoordinate.X <= 0) ||
                            (line.StartCoordinate.X >= design.Width && line.EndCoordinate.X >= design.Width))
                    .ToList();

            linesToRemove.ForEach(design.RemoveLine);
        }

        private static void ModifyPartiallyOutsideLines(PhysicalDesign design)
        {
            var modifier = new OutsideLineModifier(design.GetHorizontalPhysicalLines(), new PhysicalHorizontalLineModifier(), design.Width);
            modifier.ModifyLines();

            modifier = new OutsideLineModifier(design.GetVerticalPhysicalLines(), new PhysicalVerticalLineModifier(), design.Length);
            modifier.ModifyLines();
        }
    }
}
