﻿using System.Linq;

using PackNet.Common.Interfaces;

namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using Common.Interfaces.DTO;

    public class EmNunatabLinesRule : INunatabLinesRule
    {
        /// <summary>
        /// This rule will replace the nuna tab lines with cut line on the edges of the designs and
        /// leave nunatab lines within the designs as nunatab lines. The result will be that tiles
        /// are keept together with nuna tabs.
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            var minXCoordinate = lines.Select(l => l.StartCoordinate.X).Min();
            var maxXCoordinate = lines.Select(l => l.EndCoordinate.X).Max();
            var minYCoordinate = lines.Select(l => l.StartCoordinate.Y).Min();
            var maxYCoordinate = lines.Select(l => l.EndCoordinate.Y).Max();

            lines.GetVerticalLinesAtCoordinate(minXCoordinate).Where(l => l.Type == LineType.nunatab).ForEach(l => l.Type = LineType.cut);
            lines.GetVerticalLinesAtCoordinate(maxXCoordinate).Where(l => l.Type == LineType.nunatab).ForEach(l => l.Type = LineType.cut);
            lines.GetHorizontalLinesAtCoordinate(minYCoordinate).Where(l => l.Type == LineType.nunatab).ForEach(l => l.Type = LineType.cut);
            lines.GetHorizontalLinesAtCoordinate(maxYCoordinate).Where(l => l.Type == LineType.nunatab).ForEach(l => l.Type = LineType.cut);

            lines.Where(l => l.Type == LineType.nunatab).ForEach(l => l.Type = LineType.crease);

            return lines;
        }
    }
}
