﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    internal class LineModifier : LineModifierBase
    {
        internal LineModifier(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis) 
            : base(lines, axis)
        {            
        }

        internal override IEnumerable<PhysicalLine> ApplyRules()
        {
            foreach (var line in Lines)
            {
                CurrentLine = line;
                AdjustLinesWithOverlappingEndPos();
                AdjustLinesWithOverlappingStartPos();
                if (GetEndPos(line) - GetStartPos(line) > 0)
                    RuleAppliedLines.Add(line);
            }

            return RuleAppliedLines;
        }

        private void AdjustLinesWithOverlappingEndPos()
        {
            foreach (var line in GetOverlappingEndPos())
            {
                SetEndPos(line, GetStartPos(CurrentLine));
            }
        }

        private void AdjustLinesWithOverlappingStartPos()
        {
            foreach (var line in GetOverlappingStartPos())
            {
                SetStartPos(line, GetEndPos(CurrentLine));
            }
        }

        private IEnumerable<PhysicalLine> GetOverlappingEndPos()
        {
            IEnumerable<PhysicalLine> overLappingEndPos = from l in Lines
                                                       where
                                                            l.Equals(CurrentLine) == false &&
                                                            CurrentLine.IsDominantType(l.Type) &&
                                                            LineIsSharingCurrentLineCoordinate(l) &&
                                                            HasOverlappingEndPos(GetEndPos(l))
                                                       select l;
            return overLappingEndPos;
        }

        private IEnumerable<PhysicalLine> GetOverlappingStartPos()
        {
            IEnumerable<PhysicalLine> overLappingStartPos = from l in Lines
                                                       where
                                                            l.Equals(CurrentLine) == false &&
                                                            CurrentLine.IsDominantType(l.Type) &&
                                                            LineIsSharingCurrentLineCoordinate(l) &&
                                                            HasOverlappingStartPos(GetStartPos(l))
                                                       select l;
            return overLappingStartPos;
        }

        private bool HasOverlappingStartPos(MicroMeter lesserStart)
        {
            if (lesserStart >= GetStartPos(CurrentLine) && lesserStart < GetEndPos(CurrentLine))
            {
                return true;
            }
            
            return false;
        }

        private bool HasOverlappingEndPos(MicroMeter lesserEnd)
        {
            if (lesserEnd <= GetEndPos(CurrentLine) && (lesserEnd > GetStartPos(CurrentLine)))
            {
                return true;
            }
            
            return false;
        }
    }
}
