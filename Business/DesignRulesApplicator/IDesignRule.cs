﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public interface IDesignRule
    {
        IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines);
    }
}
