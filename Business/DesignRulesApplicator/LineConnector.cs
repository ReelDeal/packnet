﻿using System;

namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    internal class LineConnector
    {
        private IEnumerable<PhysicalLine> lines;
        private IPhysicalLineModifier axis;

        internal LineConnector(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis)
        {
            this.lines = lines;
            this.axis = axis;
        }

        internal IEnumerable<PhysicalLine> ApplyRule()
        {
            var ruleAppliedLines = new List<PhysicalLine>();

            foreach (var coordinate in axis.GetLineCoordinates(lines))
            {
                var linesAtCoordinate = new List<PhysicalLine>(axis.GetLinesAtCoordinate(lines, coordinate).OrderBy(l => axis.GetStartPos(l)));
                for (int i = 0; i < linesAtCoordinate.Count; i++)
                {
                    var line = linesAtCoordinate[i];
                    for (int l = i + 1; l < linesAtCoordinate.Count; l++)
                    {
                        var nextLine = linesAtCoordinate[l];
                        var start1 = axis.GetStartPos(line);
                        var end1 = axis.GetEndPos(line);

                        var start2 = axis.GetStartPos(nextLine);
                        var end2 = axis.GetEndPos(nextLine);

                        if (Math.Round((double)end1, 2) == Math.Round((double)start2, 2) && line.Type == nextLine.Type)
                        {
                            axis.SetEndPos(line, end2);
                            linesAtCoordinate.RemoveAt(l--);
                        }
                    }

                    ruleAppliedLines.Add(line);
                }
            }

            return ruleAppliedLines;
        }
    }
}
