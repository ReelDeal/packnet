﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public class OverLappingLinesRule : IDesignRule
    {
        private LineModifierBase modifier;

        public virtual IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {            
            // För varje linje:
            // Kontrollera att linjen är av rätt typ
            // Kolla om linjen överlappar med någon av de andra linjerna
            // Om den överlappar stöka om koordinater så cut > crease > perforation
            // Lägg till linje i ruleAppliedLinesListan
            var ruleAppliedLines = new List<PhysicalLine>();
            ruleAppliedLines = RemoveCompletelyOverlappedLines(lines);
            ruleAppliedLines = SplitCompletelyOverlappingLines(ruleAppliedLines);
            ruleAppliedLines = RemoveCompletelyOverlappedLines(ruleAppliedLines);
            ruleAppliedLines = AdjustStartAndEndPos(ruleAppliedLines);

            return ruleAppliedLines;
        }

        private List<PhysicalLine> RemoveCompletelyOverlappedLines(IEnumerable<PhysicalLine> lines)
        {
            var linesThatAreNotOverlapped = new List<PhysicalLine>();
            
            modifier = new LineRemover(lines.GetHorizontalLines(), new PhysicalHorizontalLineModifier());
            linesThatAreNotOverlapped.AddRange(modifier.ApplyRules());

            modifier = new LineRemover(lines.GetVerticalLines(), new PhysicalVerticalLineModifier());
            linesThatAreNotOverlapped.AddRange(modifier.ApplyRules());

            return linesThatAreNotOverlapped;
        }

        private List<PhysicalLine> SplitCompletelyOverlappingLines(IEnumerable<PhysicalLine> lines)
        {
            var linesThatAreNotOverlapping = new List<PhysicalLine>();

            modifier = new LineSplitter(lines.GetHorizontalLines(), new PhysicalHorizontalLineModifier());
            linesThatAreNotOverlapping.AddRange(modifier.ApplyRules());

            modifier = new LineSplitter(lines.GetVerticalLines(), new PhysicalVerticalLineModifier());
            linesThatAreNotOverlapping.AddRange(modifier.ApplyRules());

            return linesThatAreNotOverlapping;
        }

        private List<PhysicalLine> AdjustStartAndEndPos(IEnumerable<PhysicalLine> lines)
        {
            var adjustedLines = new List<PhysicalLine>();

            modifier = new LineModifier(lines.GetHorizontalLines(), new PhysicalHorizontalLineModifier());
            adjustedLines.AddRange(modifier.ApplyRules());

            modifier = new LineModifier(lines.GetVerticalLines(), new PhysicalVerticalLineModifier());
            adjustedLines.AddRange(modifier.ApplyRules());
            
            return adjustedLines;
        }
    }
}
