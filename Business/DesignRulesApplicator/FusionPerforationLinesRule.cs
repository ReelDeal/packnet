﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public class FusionPerforationLinesRule : IPerforationRule
    {
        private PerforationParameters parameters;

        public FusionPerforationLinesRule(PerforationParameters parameters)
        {
            this.parameters = parameters;
        }

        public virtual IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            List<PhysicalLine> ruleAppliedLines = new List<PhysicalLine>();
            PerforationLineModifier modifier;
            IEnumerable<PhysicalLine> axisLine;
            
            axisLine = lines.GetHorizontalLines();
            modifier = new PerforationLineModifier(axisLine, parameters, new PhysicalHorizontalLineModifier());
            ruleAppliedLines.AddRange(modifier.ApplyRules());

            axisLine = lines.GetVerticalLines();
            modifier = new PerforationLineModifier(axisLine, parameters, new PhysicalVerticalLineModifier());
            ruleAppliedLines.AddRange(modifier.ApplyRules());

            return ruleAppliedLines;
        }
    }
}
