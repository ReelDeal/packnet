﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;

namespace PackNet.Business.DesignRulesApplicator
{
    public static class WasteAreaFinder
    {
        public static IEnumerable<WasteArea> GetWasteAreasForPhysicalDesign(IEnumerable<PhysicalLine> lines, MicroMeter designLength, MicroMeter corrugateWidth, WasteAreaParameters wasteAreaParameters)
        {
            var horizontalWasteAreaFinder = new HorizontalWasteAreaFinder(wasteAreaParameters.MaximumWasteWidth, wasteAreaParameters.MinimumWasteWidth);
            var verticalWasteAreaFinder = new VerticalWasteAreaFinder(wasteAreaParameters.MaximumWasteLength, wasteAreaParameters.MinimumWasteLength);
            var wasteAreas = new List<WasteArea>();

            wasteAreas.AddRange(horizontalWasteAreaFinder.GetWasteAreas(lines, 0, corrugateWidth, TopMostYCoordinateForHorizontalCutLines));
            wasteAreas.AddRange(horizontalWasteAreaFinder.GetWasteAreas(lines, designLength, corrugateWidth, BottomMostYCoordinateForHorizontalCutLines));
            wasteAreas.AddRange(verticalWasteAreaFinder.GetWasteAreas(lines, 0, designLength, LeftMostXCoordinateForVerticalCutLines));
            wasteAreas.AddRange(verticalWasteAreaFinder.GetWasteAreas(lines, corrugateWidth, designLength, RightMostXCoordinateForVerticalCutLines));


            var horizontallyMergedWasteAreas = MergeWasteAreasHorizontally(wasteAreas, new List<WasteArea>(), wasteAreaParameters);
            var adjustedHorizontallyMergedWasteAreas = AdjustOrRemoveShortWasteAreas(horizontallyMergedWasteAreas, wasteAreaParameters);
            var mergedWasteAreas = MergeWasteAreasVertically(adjustedHorizontallyMergedWasteAreas, new List<WasteArea>());

            var verticallySplitWasteAreas = verticalWasteAreaFinder.SplitWasteAreas(lines, mergedWasteAreas);
            var horizontallySplitWasteAreas = horizontalWasteAreaFinder.SplitWasteAreas(lines, verticallySplitWasteAreas);

            return horizontallySplitWasteAreas;
        }

        private static IEnumerable<WasteArea> AdjustOrRemoveShortWasteAreas(IEnumerable<WasteArea> wasteAreas, WasteAreaParameters wasteAreaParameters)
        {
            var modifiedAreas = new List<WasteArea>();
            var shortWasteAreas = wasteAreas.Where(a => a.Length < wasteAreaParameters.MinimumWasteLength).ToList();

            shortWasteAreas.ForEach(shortArea =>
            {
                var matchingLongArea =
                    wasteAreas.FirstOrDefault(a => shortArea.CanDecreaseWidth(a) && shortArea.TopOrBottomIsMatchingTo(a));

                if (matchingLongArea != null)
                    modifiedAreas.Add(new WasteArea(
                        new PhysicalCoordinate(matchingLongArea.TopLeft.X, shortArea.TopLeft.Y),
                        new PhysicalCoordinate(matchingLongArea.BottomRight.X, shortArea.BottomRight.Y)));
            });

            modifiedAreas.AddRange(wasteAreas.Except(shortWasteAreas));
            return modifiedAreas;
        }

        private static MicroMeter TopMostYCoordinateForHorizontalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetYCoordinateForHorizontalCutLineOnXPosition(position, lines.GetHorizontalLines(), enumerable => enumerable.Min(l => l.StartCoordinate.Y));
        }

        private static MicroMeter BottomMostYCoordinateForHorizontalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetYCoordinateForHorizontalCutLineOnXPosition(position, lines.GetHorizontalLines(), enumerable => enumerable.Max(l => l.StartCoordinate.Y));
        }

        private static MicroMeter RightMostXCoordinateForVerticalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetXCoordinateForVerticalCutLineOnYPosition(position, lines.GetVerticalLines(), enumerable => enumerable.Max(l => l.StartCoordinate.X));
        }

        private static MicroMeter LeftMostXCoordinateForVerticalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetXCoordinateForVerticalCutLineOnYPosition(position, lines.GetVerticalLines(), enumerable => enumerable.Min(l => l.StartCoordinate.X));
        }

        private static MicroMeter GetYCoordinateForHorizontalCutLineOnXPosition(MicroMeter position, IEnumerable<PhysicalLine> lines, Func<IEnumerable<PhysicalLine>, MicroMeter> edgeLineFinder)
        {
            var linesOnPosition = lines.Where(l => l.StartCoordinate.X <= position && l.EndCoordinate.X > position).ToList();

            if (linesOnPosition.Any())
            {
                var line = linesOnPosition.First(ln => ln.StartCoordinate.Y == edgeLineFinder(linesOnPosition));
                if (line.Type == LineType.cut)
                    return line.StartCoordinate.Y;
            }

            return null;
        }

        private static MicroMeter GetXCoordinateForVerticalCutLineOnYPosition(MicroMeter position, IEnumerable<PhysicalLine> lines, Func<IEnumerable<PhysicalLine>, MicroMeter> edgeLineFinder)
        {
            var linesOnPosition = lines.Where(l => l.StartCoordinate.Y <= position && l.EndCoordinate.Y > position).ToList();

            if (linesOnPosition.Any())
            {
                var line = linesOnPosition.First(ln => ln.StartCoordinate.X == edgeLineFinder(linesOnPosition));
                if (line.Type == LineType.cut)
                    return line.StartCoordinate.X;
            }

            return null;
        }

        private static IEnumerable<WasteArea> MergeWasteAreasVertically(IEnumerable<WasteArea> areas, ICollection<WasteArea> processedAreas)
        {
            if (areas == null || areas.Any() == false)
                return processedAreas;

            var sortedAreas = areas.OrderBy(a => a.TopLeft.Y).ThenBy(a => a.TopLeft.X).ToList();
            var first = sortedAreas.FirstOrDefault();

            var connected = sortedAreas.FirstOrDefault(a => a.TopLeft.X == first.TopLeft.X &&
                                                a.BottomRight.X == first.BottomRight.X &&
                                                a.TopLeft.Y == first.BottomRight.Y);
            if (connected != null)
            {
                sortedAreas.Add(
                    new WasteArea(
                        new PhysicalCoordinate(first.TopLeft.X, first.TopLeft.Y),
                        new PhysicalCoordinate(connected.BottomRight.X, connected.BottomRight.Y)));
            }
            else
                processedAreas.Add(first);

            return MergeWasteAreasVertically(sortedAreas.Skip(1).Where(a => a != connected), processedAreas);
        }

        private static IEnumerable<WasteArea> MergeWasteAreasHorizontally(IEnumerable<WasteArea> areas, ICollection<WasteArea> processedAreas, WasteAreaParameters wasteAreaParameters)
        {
            if (areas == null || areas.Any() == false)
                return processedAreas;

            var sortedAreas = areas.OrderBy(a => a.TopLeft.Y).ThenBy(a => a.TopLeft.X).ToList();
            var first = sortedAreas.FirstOrDefault();
            var current = new WasteArea(new PhysicalCoordinate(first.TopLeft.X, first.TopLeft.Y), new PhysicalCoordinate(first.BottomRight.X, first.BottomRight.Y));
            var next = sortedAreas.Skip(1).FirstOrDefault();
            var skipCount = 1;

            while (next != null && next.TopLeft.X <= current.BottomRight.X && next.TopLeft.Y < current.BottomRight.Y)
            {
                if (next.BottomRight.Y < current.BottomRight.Y)
                {
                    sortedAreas.Add(current.SplitAreaBasedOnShorterArea(next));
                    current.BottomRight.Y = next.BottomRight.Y;
                }

                if (next.BottomRight.Y > current.BottomRight.Y)
                    sortedAreas.Add(next.SplitAreaBasedOnShorterArea(current));

                if (current.BottomRight.X < next.BottomRight.X)
                    current.BottomRight.X = next.BottomRight.X;
                skipCount++;
                next = sortedAreas.Skip(skipCount).FirstOrDefault();
            }

            if (current.BottomRight.X - current.TopLeft.X >= wasteAreaParameters.MinimumWasteWidth)
                processedAreas.Add(new WasteArea(new PhysicalCoordinate(current.TopLeft.X, current.TopLeft.Y), new PhysicalCoordinate(current.BottomRight.X, current.BottomRight.Y)));

            return MergeWasteAreasHorizontally(sortedAreas.Skip(skipCount), processedAreas, wasteAreaParameters);
        }
        private abstract class DirectionalWasteAreaFinder
        {
            protected readonly MicroMeter maximumDistance;
            protected readonly MicroMeter minimumDistance;

            protected DirectionalWasteAreaFinder(MicroMeter maximumDistance, MicroMeter minimumDistance)
            {
                this.maximumDistance = maximumDistance;
                this.minimumDistance = minimumDistance;
            }

            public IEnumerable<WasteArea> GetWasteAreas(IEnumerable<PhysicalLine> lines, MicroMeter edge, MicroMeter endPos, Func<MicroMeter, IEnumerable<PhysicalLine>, MicroMeter> startPointGetter)
            {
                var wasteAreas = new List<WasteArea>();
                var lastPosition = 0d;

                while (lastPosition < endPos)
                {
                    var cutLineClosestToEdge = startPointGetter(lastPosition, lines);
                    var nextPosition = GetNextPosition(lastPosition, lines, endPos);

                    if (cutLineClosestToEdge != null && cutLineClosestToEdge != edge)
                    {
                        var wasteArea = CreateWasteArea(cutLineClosestToEdge, lastPosition, edge, nextPosition);
                        wasteAreas.Add(wasteArea);
                    }
                    lastPosition = nextPosition;
                }

                return wasteAreas;
            }

            public IEnumerable<WasteArea> SplitWasteAreas(IEnumerable<PhysicalLine> lines, IEnumerable<WasteArea> wasteAreas)
            {
                var areas = new List<WasteArea>();
                foreach (var area in wasteAreas)
                    areas.AddRange(SplitWasteArea(lines, area, new List<WasteArea>()));

                return areas;
            }

            protected abstract IEnumerable<WasteArea> SplitWasteArea(IEnumerable<PhysicalLine> lines, WasteArea area, List<WasteArea> processedAreas);

            protected abstract MicroMeter GetNextPosition(MicroMeter lastPosition, IEnumerable<PhysicalLine> lines, MicroMeter endPos);

            protected abstract WasteArea CreateWasteArea(MicroMeter cutLineClosestToCorrugateEdge, MicroMeter startPosition, MicroMeter corrugateEdge, MicroMeter endPosition);
        }

        private class VerticalWasteAreaFinder : DirectionalWasteAreaFinder
        {
            public VerticalWasteAreaFinder(MicroMeter maximumDistance, MicroMeter minimumDistance)
                : base(maximumDistance, minimumDistance)
            {
            }

            protected override MicroMeter GetNextPosition(MicroMeter lastPosition, IEnumerable<PhysicalLine> lines, MicroMeter endPos)
            {
                var horizontalLines = lines.GetHorizontalLines().Where(l => l.Type == LineType.cut);
                var horizontalLineBetweenPositions = horizontalLines.OrderBy(l => l.StartCoordinate.Y).FirstOrDefault(l => l.StartCoordinate.Y > lastPosition);

                if (horizontalLineBetweenPositions != null)
                    return horizontalLineBetweenPositions.StartCoordinate.Y;

                return endPos;
            }

            protected override WasteArea CreateWasteArea(MicroMeter cutLineClosestToCorrugateEdge, MicroMeter startPosition, MicroMeter corrugateEdge, MicroMeter endPosition)
            {
                return new WasteArea(new PhysicalCoordinate(cutLineClosestToCorrugateEdge, startPosition), new PhysicalCoordinate(corrugateEdge, endPosition));
            }

            protected override IEnumerable<WasteArea> SplitWasteArea(IEnumerable<PhysicalLine> lines, WasteArea area, List<WasteArea> processedAreas)
            {
                if (area == null)
                    return processedAreas;

                if (area.Length <= maximumDistance)
                {
                    processedAreas.Add(area);
                    return processedAreas;
                }

                var nextSplitPosition = GetNextSplitPosition(area, lines);
                var newArea = new WasteArea(new PhysicalCoordinate(area.TopLeft.X, nextSplitPosition),
                    new PhysicalCoordinate(area.BottomRight.X, area.BottomRight.Y));
                if (minimumDistance <= newArea.BottomRight.Y - newArea.TopLeft.Y)
                {
                    area.BottomRight.Y = nextSplitPosition;
                }
                else
                {
                    var middlePosition = ((newArea.BottomRight.Y - area.TopLeft.Y) / 2) + area.TopLeft.Y;
                    area.BottomRight.Y = middlePosition;
                    newArea.TopLeft.Y = middlePosition;
                }
                processedAreas.Add(area);
                return SplitWasteArea(lines, newArea, processedAreas);
            }

            private MicroMeter GetNextSplitPosition(WasteArea area, IEnumerable<PhysicalLine> lines)
            {
                var positionsInArea = lines.GetHorizontalLineCoordinates().Where(c => c > area.TopLeft.Y + minimumDistance && c < area.TopLeft.Y + maximumDistance);
                return positionsInArea.Any() ? positionsInArea.Max() : area.TopLeft.Y + maximumDistance;
            }
        }

        private class HorizontalWasteAreaFinder : DirectionalWasteAreaFinder
        {
            public HorizontalWasteAreaFinder(MicroMeter maximumDistance, MicroMeter minimumDistance)
                : base(maximumDistance, minimumDistance)
            {
            }

            protected override IEnumerable<WasteArea> SplitWasteArea(IEnumerable<PhysicalLine> lines, WasteArea area, List<WasteArea> processedAreas)
            {
                if (area == null)
                    return processedAreas;

                if (area.Width <= maximumDistance)
                {
                    processedAreas.Add(area);
                    return processedAreas;
                }

                var nextSplitPosition = GetNextSplitPosition(area, lines);
                var newArea = new WasteArea(new PhysicalCoordinate(nextSplitPosition, area.TopLeft.Y),
                    new PhysicalCoordinate(area.BottomRight.X, area.BottomRight.Y));
                if (minimumDistance <= newArea.BottomRight.X - newArea.TopLeft.X)
                {
                    area.BottomRight.X = nextSplitPosition;
                }
                else
                {
                    var middlePosition = ((newArea.BottomRight.X - area.TopLeft.X) / 2) + area.TopLeft.X;
                    area.BottomRight.X = middlePosition;
                    newArea.TopLeft.X = middlePosition;
                }
                processedAreas.Add(area);
                return SplitWasteArea(lines, newArea, processedAreas);
            }

            protected override MicroMeter GetNextPosition(MicroMeter lastPosition, IEnumerable<PhysicalLine> lines, MicroMeter endPos)
            {
                var verticalLines = lines.GetVerticalLines().Where(l => l.Type == LineType.cut);
                var verticalLineBetweenPositions = verticalLines.OrderBy(l => l.StartCoordinate.X).FirstOrDefault(l => l.StartCoordinate.X > lastPosition);

                if (verticalLineBetweenPositions != null)
                    return verticalLineBetweenPositions.StartCoordinate.X;

                return endPos;
            }

            protected override WasteArea CreateWasteArea(MicroMeter cutLineClosestToCorrugateEdge, MicroMeter startPosition, MicroMeter corrugateEdge, MicroMeter endPosition)
            {
                return new WasteArea(new PhysicalCoordinate(startPosition, cutLineClosestToCorrugateEdge), new PhysicalCoordinate(endPosition, corrugateEdge));
            }

            private MicroMeter GetNextSplitPosition(WasteArea area, IEnumerable<PhysicalLine> lines)
            {
                var positionsInArea = lines.GetVerticalLineCoordinates().Where(c => c > area.TopLeft.X + minimumDistance && c <= area.TopLeft.X + maximumDistance);
                return positionsInArea.Any() ? positionsInArea.Max() : area.TopLeft.X + maximumDistance;
            }
        }
    }
}