﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public class WasteSeparationLinesRule : IDesignRule
    {
        public virtual IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            var ruleAppliedLines = new List<PhysicalLine>();

            foreach (var line in lines)
            {
                if (line.Type == LineType.separate)
                {
                    ruleAppliedLines.Add(new PhysicalLine(line.StartCoordinate, line.EndCoordinate, LineType.cut));
                    continue;
                }
                ruleAppliedLines.Add(line);
            }

            return ruleAppliedLines;
        }
    }
}
    