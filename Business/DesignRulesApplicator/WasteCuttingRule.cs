﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignRulesApplicator
{
    public class WasteCuttingRule : IDesignRule
    {
        private MicroMeter corrugateWidth;
        private MicroMeter maximumWasteHeight;
        private MicroMeter maximumWasteWidth;

        public WasteCuttingRule(MicroMeter corrugateWidth, MicroMeter maximumWasteHeight, MicroMeter maximumWasteWidth)
        {
            this.corrugateWidth = corrugateWidth;
            this.maximumWasteHeight = maximumWasteHeight;
            this.maximumWasteWidth = maximumWasteWidth;
        }

        public IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            var verticalLines = lines.GetVerticalLines().ToList();
            var length = verticalLines.Max(l => l.EndCoordinate.Y);
            var width = verticalLines.Max(l => l.EndCoordinate.X);

            var newLines = new List<PhysicalLine>();

            var horizontalWasteLineCreator = new HorizontalWasteLineCreator();
            newLines.AddRange(horizontalWasteLineCreator.AddWasteLines(lines, corrugateWidth, maximumWasteHeight, length, RightMostXCoordinateForVerticalCutLines));
            newLines.AddRange(horizontalWasteLineCreator.AddWasteLines(lines, 0, maximumWasteHeight, length, LeftMostXCoordinateForVerticalCutLines));

            var verticalWasteLineCreator = new VerticalWasteLineCreator();
            newLines.AddRange(verticalWasteLineCreator.AddWasteLines(lines, 0, maximumWasteWidth, width, TopMostYCoordinateForHorizontalCutLines));
            newLines.AddRange(verticalWasteLineCreator.AddWasteLines(lines, length, maximumWasteWidth, width, BottomMostYCoordinateForHorizontalCutLines));
            
            return newLines;
        }

        private MicroMeter TopMostYCoordinateForHorizontalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetYCoordinateForHorizontalCutLineOnXPosition(position, lines, enumerable => { return enumerable.Min(l => l.StartCoordinate.Y);});
        }

        private MicroMeter BottomMostYCoordinateForHorizontalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetYCoordinateForHorizontalCutLineOnXPosition(position, lines, enumerable => { return enumerable.Max(l => l.StartCoordinate.Y); });
        }

        private MicroMeter RightMostXCoordinateForVerticalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetXCoordinateForVerticalCutLineOnYPosition(position, lines, enumerable => { return enumerable.Max(l => l.StartCoordinate.X);});
        }

        private MicroMeter LeftMostXCoordinateForVerticalCutLines(MicroMeter position, IEnumerable<PhysicalLine> lines)
        {
            return GetXCoordinateForVerticalCutLineOnYPosition(position, lines, enumerable => { return enumerable.Min(l => l.StartCoordinate.X); });
        }

        private MicroMeter GetYCoordinateForHorizontalCutLineOnXPosition(MicroMeter position, IEnumerable<PhysicalLine> lines, Func<IEnumerable<PhysicalLine>, MicroMeter> edgeLineFinder)
        {
            var linesOnPosition = lines.Where(l => l.StartCoordinate.X <= position && l.EndCoordinate.X >= position).ToList();

            if (linesOnPosition.Any())
            {
                var line = linesOnPosition.First(ln => ln.StartCoordinate.Y == edgeLineFinder(linesOnPosition));
                if (line.Type == LineType.cut)
                    return line.StartCoordinate.Y;
            }

            return null;
        }

        private MicroMeter GetXCoordinateForVerticalCutLineOnYPosition(MicroMeter position, IEnumerable<PhysicalLine> lines, Func<IEnumerable<PhysicalLine>, MicroMeter> edgeLineFinder)
        {
            var linesOnPosition = lines.Where(l => l.StartCoordinate.Y <= position && l.EndCoordinate.Y >= position).ToList();

            if (linesOnPosition.Any())
            {
                var line = linesOnPosition.First(ln => ln.StartCoordinate.X == edgeLineFinder(linesOnPosition));
                if (line.Type == LineType.cut)
                    return line.StartCoordinate.X;
            }

            return null;
        }
    }
}
