﻿using System.Linq;

using PackNet.Common.Interfaces;

namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using Common.Interfaces.DTO;

    public class IqNunatabLinesRule : INunatabLinesRule
    {
        /// <summary>
        /// Nunatabs are not supported by the iQ machines because the long heads have no actuators.
        /// Nunatabs are achieved on the iQ machines by using a tiling knife which will leave part of the line uncut.
        /// Nunatabs are converted into regular cuts lines.
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            lines.Where(l => l.Type == LineType.nunatab).ForEach(l => l.Type = LineType.cut);
            return lines;
        }
    }
}
