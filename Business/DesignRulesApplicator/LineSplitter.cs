﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    internal class LineSplitter : LineModifierBase    
    {
        internal LineSplitter(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis)
            : base(lines, axis)
        {
        }

        internal override IEnumerable<PhysicalLine> ApplyRules()
        {
            for (int i = 0; i < Lines.Count; i++)
            {
                CurrentLine = Lines[i];
                SplitAndAddOverlappingLesserLines();
            }

            return Lines;
        }

        private void SplitAndAddOverlappingLesserLines()
        {
            var overlappingLesserLines = GetOverlappingLesserLines();

            for (int i = 0; i < overlappingLesserLines.Count(); i++)
            {
                AddFirstPartOfSplitLine(overlappingLesserLines.ElementAt(i));
                AddSecondPartOfSplitLine(overlappingLesserLines.ElementAt(i));
                Lines.Remove(overlappingLesserLines.ElementAt(i));
            }
        }

        private IEnumerable<PhysicalLine> GetOverlappingLesserLines()
        {
            IEnumerable<PhysicalLine> overlappingLesserLines = from l in Lines
                                                            where
                                                                l.Equals(CurrentLine) == false &&
                                                                CurrentLine.IsDominantType(l.Type) &&
                                                                LineIsOverlappingCurrentLine(l)
                                                            select l;
            return overlappingLesserLines;
        }

        private void AddFirstPartOfSplitLine(PhysicalLine line)
        {
            PhysicalLine preLine = CopyLine(line);
            SetEndPos(preLine, GetStartPos(CurrentLine));
            if (GetEndPos(preLine) - GetStartPos(preLine) > 0)
                Lines.Add(preLine);
        }

        private void AddSecondPartOfSplitLine(PhysicalLine line)
        {
            PhysicalLine postLine = CopyLine(line);
            SetStartPos(postLine, GetEndPos(CurrentLine));
            if (GetEndPos(postLine) - GetStartPos(postLine) > 0)
                Lines.Add(postLine);
        }

        private PhysicalLine CopyLine(PhysicalLine line)
        {            
            return new PhysicalLine(new PhysicalCoordinate(line.StartCoordinate.X, line.StartCoordinate.Y), 
                new PhysicalCoordinate(line.EndCoordinate.X, line.EndCoordinate.Y), line.Type);
        }
    }
}
