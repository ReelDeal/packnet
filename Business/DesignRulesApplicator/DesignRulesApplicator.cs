﻿using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;

namespace PackNet.Business.DesignRulesApplicator
{
    using Common.Interfaces.DTO;
    using System.Collections.Generic;
    using System.Linq;

    public class DesignRulesApplicator : IDesignRulesApplicator
    {
        private WasteAreaParameters wasteAreaParameters;

        public DesignRulesApplicator(WasteAreaParameters wasteAreaParameters)
        {
            this.wasteAreaParameters = wasteAreaParameters;
        }

        public RuleAppliedPhysicalDesign ApplyRules(PhysicalDesign design, IPerforationRule perforationRule, INunatabLinesRule nunatabLinesRule, Corrugate corrugate, OrientationEnum rotation)
        {
            IEnumerable<PhysicalLine> linesToApplyRulesTo = design.Lines.ToList();

            if (design.Lines.Any())
            {
                linesToApplyRulesTo = new WasteSeparationLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new ZeroLengthLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new OverLappingLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = nunatabLinesRule.ApplyTo(linesToApplyRulesTo);

                if (wasteAreaParameters.Enable)
                {
                    var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(linesToApplyRulesTo, design.Length,
                        corrugate.Width, wasteAreaParameters);

                    linesToApplyRulesTo =
                        linesToApplyRulesTo.Union(WasteAreaCutLineCreator.GetWasteCuttingLinesForWasteAreas(wasteAreas));
                }

                linesToApplyRulesTo = new ZeroLengthLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new OverLappingLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new MergeConnectedAndEqualLinesRule().ApplyTo(linesToApplyRulesTo);

                linesToApplyRulesTo = perforationRule.ApplyTo(linesToApplyRulesTo);

                linesToApplyRulesTo = new MergeConnectedAndEqualLinesRule().ApplyTo(linesToApplyRulesTo);
            }

            var ruleAppliedDesign = new RuleAppliedPhysicalDesign
            {
                Length = design.Length,
                Width = design.Width
            };


            var linesToAdd = linesToApplyRulesTo.ToList();
            linesToAdd.ForEach(ruleAppliedDesign.Add);

            return ruleAppliedDesign;
        }
    }
}
