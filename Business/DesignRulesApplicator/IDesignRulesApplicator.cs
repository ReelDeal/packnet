﻿using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace PackNet.Business.DesignRulesApplicator
{
    using Common.Interfaces.DTO;

    public interface IDesignRulesApplicator
    {
        RuleAppliedPhysicalDesign ApplyRules(PhysicalDesign design, IPerforationRule perforationRule, INunatabLinesRule nunatabLinesRule, Corrugate corrugate, OrientationEnum rotation);
    }
}
