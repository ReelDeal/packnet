﻿using System;

namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    internal class PerforationLineModifier
    {
        private IEnumerable<PhysicalLine> lines;
        private IPhysicalLineModifier axis;
        private List<PhysicalLine> ruleAppliedLines;
        private PerforationParameters parameters;

        internal PerforationLineModifier(IEnumerable<PhysicalLine> lines, PerforationParameters parameters, IPhysicalLineModifier axis)
        {
            this.lines = lines;
            this.axis = axis;
            ruleAppliedLines = new List<PhysicalLine>();
            this.parameters = parameters;
        }

        internal IEnumerable<PhysicalLine> ApplyRules()
        {
            AddNonPerforationLines();
            SplitAndAddPerforationLines();
            return ruleAppliedLines;
        }

        private void AddNonPerforationLines()
        {
            IEnumerable<PhysicalLine> nonPerforationLines = from l in lines
                                                         where
                                                            l.Type != LineType.perforation
                                                         select l;

            foreach (var line in nonPerforationLines)
            {
                ruleAppliedLines.Add(line);
            }
        }

        private void SplitAndAddPerforationLines()
        {
            foreach (var line in GetPerforationLines())
            {
                MicroMeter currentPos = axis.GetStartPos(line);
                MicroMeter endPos = axis.GetEndPos(line);
                if (parameters == null)
                {
                    throw new Exception("Perforation parameters not defined");
                }
                MicroMeter creaseSize = parameters.CreaseLength;
                MicroMeter cutSize = parameters.CutLength;
                MicroMeter offset = CalculateStartAndEndCreaseOffset(line, creaseSize, cutSize);

                AddCreaseLine(currentPos, creaseSize + offset, line);
                currentPos += creaseSize + offset;
                MicroMeter firstCreaseEnd = currentPos;

                while (currentPos + (cutSize * 2) + (creaseSize * 2) + offset <= endPos)
                {
                    AddCutLine(currentPos, cutSize, line);
                    currentPos += cutSize;
                    AddCreaseLine(currentPos, creaseSize, line);
                    currentPos += creaseSize;
                }

                if (currentPos + cutSize + creaseSize + offset <= endPos)
                {
                    AddCutLine(currentPos, cutSize, line);
                    currentPos += cutSize;
                }

                AddCreaseLine(currentPos, creaseSize + offset, line);
            }
        }

        private MicroMeter CalculateStartAndEndCreaseOffset(PhysicalLine line, MicroMeter creaseSize, MicroMeter cutSize)
        {
            MicroMeter startPos = axis.GetStartPos(line);
            MicroMeter endPos = axis.GetEndPos(line);
            MicroMeter totalLength = (endPos - startPos) - (creaseSize * 2);
            MicroMeter offset;

            if (totalLength > 0 && cutSize <= totalLength)
            {
                offset = (totalLength - cutSize) % (creaseSize + cutSize);                
            }
            else
            {
                offset = totalLength;
            }

            return offset / 2;
        }

        private void AddCreaseLine(MicroMeter startPos, MicroMeter size, PhysicalLine line)
        {
            var newLine = new PhysicalLine(new PhysicalCoordinate(line.StartCoordinate.X, line.StartCoordinate.Y),
                new PhysicalCoordinate(line.EndCoordinate.X, line.EndCoordinate.Y), LineType.crease);
            axis.SetStartPos(newLine, startPos);
            axis.SetEndPos(newLine, startPos + size);
            ruleAppliedLines.Add(newLine);
        }

        private void AddCutLine(MicroMeter startPos, MicroMeter size, PhysicalLine line)
        {
            var newLine = new PhysicalLine(new PhysicalCoordinate(line.StartCoordinate.X, line.StartCoordinate.Y),
                new PhysicalCoordinate(line.EndCoordinate.X, line.EndCoordinate.Y), LineType.cut);
            axis.SetStartPos(newLine, startPos);
            axis.SetEndPos(newLine, startPos + size);
            ruleAppliedLines.Add(newLine);
        }

        private IEnumerable<PhysicalLine> GetPerforationLines()
        {
            IEnumerable<PhysicalLine> perforationLines = from l in lines
                                                      where
                                                        (l.Type == LineType.perforation)
                                                      select l;
            return perforationLines;
        }
    }
}
