﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    internal class LineRemover : LineModifierBase
    {
        internal LineRemover(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis) 
            : base(lines, axis)
        {
        }

        internal override IEnumerable<PhysicalLine> ApplyRules()
        {
            var linesSortedByLength = Lines.OrderByDescending(l => Axis.GetEndPos(l) - Axis.GetStartPos(l)).Reverse().ToList();
            foreach (var line in linesSortedByLength)
            {                
                CurrentLine = line;
                if (IsNotOverlappedByDominantLine(linesSortedByLength.Skip(linesSortedByLength.IndexOf(line) + 1)))
                {
                    RuleAppliedLines.Add(line);
                }
            }

            return RuleAppliedLines;            
        }

        private bool IsNotOverlappedByDominantLine(IEnumerable<PhysicalLine> remaingLines)
        {
            IEnumerable<PhysicalLine> overlappingLines = from l in remaingLines
                                                      where
                                                        l.Equals(CurrentLine) == false &&
                                                        LineIsOverlappingCurrentLine(l) &&
                                                        l.IsDominantType(CurrentLine.Type)
                                                      select l;
            return overlappingLines.Count() == 0;            
        }
    }
}
