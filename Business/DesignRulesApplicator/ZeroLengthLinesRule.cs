﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public class ZeroLengthLinesRule : IDesignRule
    {
        public virtual IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            var ruleAppliedLines = new List<PhysicalLine>();

            foreach (var line in lines)
            {
                var xlength = line.EndCoordinate.X - line.StartCoordinate.X;
                var ylength = line.EndCoordinate.Y - line.StartCoordinate.Y;

                if (xlength != 0 || ylength != 0)
                {
                    ruleAppliedLines.Add(line);
                }
            }

            return ruleAppliedLines;
        }
    }
}
    