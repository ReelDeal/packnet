﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;

namespace PackNet.Business.DesignRulesApplicator
{
    public class DesignAndWasteRulesApplicator : IDesignRulesApplicator
    {
        private WasteAreaParameters wasteAreaParameters;

        public DesignAndWasteRulesApplicator(WasteAreaParameters wasteAreaParameters)
        {
            this.wasteAreaParameters = wasteAreaParameters;
        }

        public RuleAppliedPhysicalDesign ApplyRules(PhysicalDesign design, IPerforationRule perforationRule, INunatabLinesRule nunatabLinesRule, Corrugate corrugate, OrientationEnum rotation)
        {
            IEnumerable<PhysicalLine> linesToApplyRulesTo = design.Lines.ToList();

            var ruleAppliedDesign = new RuleAppliedPhysicalDesign()
            {
                Length = design.Length,
                Width = design.Width,
            };

            if (design.Lines.Any())
            {
                IEnumerable<WasteArea> wasteAreas = new List<WasteArea>();

                linesToApplyRulesTo = new WasteSeparationLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new ZeroLengthLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new OverLappingLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = nunatabLinesRule.ApplyTo(linesToApplyRulesTo);

                if (wasteAreaParameters.Enable)
                {
                    wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(
                        linesToApplyRulesTo,
                        design.Length,
                        corrugate.Width,
                        wasteAreaParameters);
                    linesToApplyRulesTo =
                        linesToApplyRulesTo.Union(WasteAreaCutLineCreator.GetWasteCuttingLinesForWasteAreas(wasteAreas));
                }

                linesToApplyRulesTo = new ZeroLengthLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new OverLappingLinesRule().ApplyTo(linesToApplyRulesTo);
                linesToApplyRulesTo = new MergeConnectedAndEqualLinesRule().ApplyTo(linesToApplyRulesTo);

                linesToApplyRulesTo = perforationRule.ApplyTo(linesToApplyRulesTo);

                linesToApplyRulesTo = new MergeConnectedAndEqualLinesRule().ApplyTo(linesToApplyRulesTo);




                var linesToAdd = linesToApplyRulesTo.ToList();
                linesToAdd.ForEach(ruleAppliedDesign.Add);
                wasteAreas.ForEach(ruleAppliedDesign.AddWasteArea);
            }
            return ruleAppliedDesign;
        }
    }
}
