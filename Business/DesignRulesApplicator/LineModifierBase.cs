﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    internal abstract class LineModifierBase
    {
        internal LineModifierBase(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis)
        {
            Lines = lines.OrderByDescending(l => l.Type, new LineTypeComparer()).ToList();
            Axis = axis;
            RuleAppliedLines = new List<PhysicalLine>();
        }

        protected internal List<PhysicalLine> Lines { get; set; }

        protected internal List<PhysicalLine> RuleAppliedLines { get; set; }

        protected internal IPhysicalLineModifier Axis { get; set; }

        protected internal PhysicalLine CurrentLine { get; set; }

        internal abstract IEnumerable<PhysicalLine> ApplyRules();

        protected MicroMeter GetStartPos(PhysicalLine line)
        {
            return Axis.GetStartPos(line);
        }

        protected MicroMeter GetEndPos(PhysicalLine line)
        {
            return Axis.GetEndPos(line);
        }

        protected MicroMeter GetLineCoordinate(PhysicalLine line)
        {
            return Axis.GetLineCoordinate(line);
        }

        protected void SetStartPos(PhysicalLine line, MicroMeter pos)
        {
            Axis.SetStartPos(line, pos);
        }

        protected void SetEndPos(PhysicalLine line, MicroMeter pos)
        {
            Axis.SetEndPos(line, pos);
        }

        protected bool LineIsSharingCurrentLineCoordinate(PhysicalLine line)
        {
            return GetLineCoordinate(CurrentLine) == GetLineCoordinate(line);
        }

        protected bool LineIsOverlappingCurrentLine(PhysicalLine line)
        {
            if (LineIsSharingCurrentLineCoordinate(line))
            {
                if (GetStartPos(line) <= GetStartPos(CurrentLine) && GetEndPos(line) >= GetEndPos(CurrentLine))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
