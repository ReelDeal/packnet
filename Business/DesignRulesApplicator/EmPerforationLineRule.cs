﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignRulesApplicator
{
    public class EmPerforationLineRule : IPerforationRule
    {
        private readonly MicroMeter creaseDistance;

        public EmPerforationLineRule(MicroMeter creaseDistance)
        {
            this.creaseDistance = creaseDistance;
        }

        public virtual IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            var ruleAppliedLines = new List<PhysicalLine>();

            foreach (var physicalLine in lines)
            {
                if (physicalLine.Type == LineType.perforation)
                {
                    ruleAppliedLines.AddRange(ConvertPerforationLine(physicalLine));
                    continue;
                }
                ruleAppliedLines.Add(physicalLine);
            }

            return ruleAppliedLines;
        }

        private IEnumerable<PhysicalLine> ConvertPerforationLine(PhysicalLine physicalLine)
        {
            if (physicalLine.Length() <= 2 * creaseDistance)
            {
                return new List<PhysicalLine>
                {
                    new PhysicalLine(physicalLine.StartCoordinate, physicalLine.EndCoordinate, LineType.crease)
                };
            }

            var lineSplit = physicalLine.Split(creaseDistance);
            lineSplit.InsertRange(1, lineSplit.Last().Split(lineSplit.Last().Length() - creaseDistance));

            lineSplit.ElementAt(0).Type = LineType.crease;
            lineSplit.ElementAt(2).Type = LineType.crease;

            return lineSplit.Take(3);
        }
    }
}
