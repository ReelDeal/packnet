﻿namespace PackNet.Business.DesignRulesApplicator
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public class MergeConnectedAndEqualLinesRule : IDesignRule
    {
        public virtual IEnumerable<PhysicalLine> ApplyTo(IEnumerable<PhysicalLine> lines)
        {
            var ruleAppliedLines = new List<PhysicalLine>();
            IEnumerable<PhysicalLine> axisLines;

            axisLines = lines.GetHorizontalLines();
            var connector = new LineConnector(axisLines, new PhysicalHorizontalLineModifier());
            ruleAppliedLines.AddRange(connector.ApplyRule());

            axisLines = lines.GetVerticalLines();
            connector = new LineConnector(axisLines, new PhysicalVerticalLineModifier());
            ruleAppliedLines.AddRange(connector.ApplyRule());
            return ruleAppliedLines;
        }
    }
}
