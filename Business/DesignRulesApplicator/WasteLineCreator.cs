﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignRulesApplicator
{
    public abstract class WasteLineCreator
    {
        public IEnumerable<PhysicalLine> AddWasteLines(IEnumerable<PhysicalLine> lines, MicroMeter edge, MicroMeter maxWaste, MicroMeter endPos, Func<MicroMeter, IEnumerable<PhysicalLine>, MicroMeter> startPointGetter)
        {
            var wasteLines = new List<PhysicalLine>();
            var lastPosition = 0;

            while (lastPosition + maxWaste < endPos)
            {
                var nextPosition = GetNextPosition(lastPosition, lines, maxWaste);
                var cutLineClosestToEdge = startPointGetter(nextPosition, lines);

                if (cutLineClosestToEdge != null && cutLineClosestToEdge != edge)
                {
                    var wasteLine = GetPhysicalLineForPosition(cutLineClosestToEdge, nextPosition, edge);
                    wasteLines.Add(wasteLine);
                }

                lastPosition = nextPosition;
            }

            return wasteLines;
        }

        protected abstract PhysicalLine GetPhysicalLineForPosition(MicroMeter cutLineClosestToEdge, MicroMeter position, MicroMeter edge);

        protected abstract MicroMeter GetNextPosition(MicroMeter lastPosition, IEnumerable<PhysicalLine> lines, MicroMeter maxWaste);
    }

    public class HorizontalWasteLineCreator : WasteLineCreator
    {
        protected override PhysicalLine GetPhysicalLineForPosition(MicroMeter cutLineClosestToEdge, MicroMeter position, MicroMeter edge)
        {
            return new PhysicalLine(new PhysicalCoordinate(cutLineClosestToEdge, position), new PhysicalCoordinate(edge, position), LineType.cut);
        }

        protected override MicroMeter GetNextPosition(MicroMeter lastPosition, IEnumerable<PhysicalLine> lines, MicroMeter maxWaste)
        {
            var horizontalLinesBetweenPositions = lines.GetHorizontalLines().Where(l => l.StartCoordinate.Y > lastPosition && l.StartCoordinate.Y <= lastPosition + maxWaste);

            if (horizontalLinesBetweenPositions.Any())
                return horizontalLinesBetweenPositions.Select(l => l.StartCoordinate.Y).Max();

            return lastPosition + maxWaste;

        }
    }

    public class VerticalWasteLineCreator : WasteLineCreator
    {
        protected override PhysicalLine GetPhysicalLineForPosition(MicroMeter cutLineClosestToEdge, MicroMeter position, MicroMeter edge)
        {
            return new PhysicalLine(new PhysicalCoordinate(position, cutLineClosestToEdge), new PhysicalCoordinate(position, edge), LineType.cut);
        }

        protected override MicroMeter GetNextPosition(MicroMeter lastPosition, IEnumerable<PhysicalLine> lines, MicroMeter maxWaste)
        {
            var verticalLinesBetweenPositions = lines.GetVerticalLines().Where(l => l.StartCoordinate.X > lastPosition && l.StartCoordinate.X <= lastPosition + maxWaste);

            if (verticalLinesBetweenPositions.Any())
                return verticalLinesBetweenPositions.Select(l => l.StartCoordinate.X).Max();

            return lastPosition + maxWaste;
        }
    }
}