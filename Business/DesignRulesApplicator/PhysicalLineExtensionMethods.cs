﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignRulesApplicator
{
    public static class PhysicalLineExtensionMethods
    {
        public static MicroMeter Length(this PhysicalLine line)
        {
            return
                Math.Sqrt(Math.Pow(line.EndCoordinate.X - line.
                StartCoordinate.X, 2) +
                          Math.Pow(line.EndCoordinate.Y - line.StartCoordinate.Y, 2));
        }

        public static bool IsHorizontal(this PhysicalLine line)
        {
            return line.StartCoordinate.Y == line.EndCoordinate.Y;
        }

        public static List<PhysicalLine> Split(this PhysicalLine line, MicroMeter length)
        {
            if (line.Length() <= length)
                return new List<PhysicalLine> { line };

            if (line.IsHorizontal())
            {
                return new List<PhysicalLine>()
                {
                    new PhysicalLine(line.StartCoordinate,
                        new PhysicalCoordinate(line.StartCoordinate.X + length, line.StartCoordinate.Y), line.Type),
                    new PhysicalLine(new PhysicalCoordinate(line.StartCoordinate.X + length, line.StartCoordinate.Y),
                        line.EndCoordinate, line.Type)
                };
            }

            return new List<PhysicalLine>()
            {
                new PhysicalLine(line.StartCoordinate,
                    new PhysicalCoordinate(line.StartCoordinate.X, line.StartCoordinate.Y + length), line.Type),
                new PhysicalLine(new PhysicalCoordinate(line.StartCoordinate.X, line.StartCoordinate.Y + length),
                    line.EndCoordinate, line.Type)
            };
        }
    }
}
