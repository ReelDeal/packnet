﻿using System.ComponentModel.Composition;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.SelectionAlgorithms;

namespace PackNet.Business.SelectionAlgorithms.Plugins.BoxFirst
{
    [Export(typeof(ISelectionAlgorithm))]
    public class BoxFirstSelectionAlgorithmPlugin : SelectionAlgorithmPluginBase, ISelectionAlgorithm
    {
        [ImportingConstructor]
        public BoxFirstSelectionAlgorithmPlugin(ILogger logger) : 
            base(SelectionAlgorithmTypes.BoxFirst, "BoxFirstSelectProducibleWorkFlow.xaml", "BoxFirstStagingWorkFlow.xaml",  logger)
        {
        }

        public string UniqueId { get { return "BoxFirstSelectionAlgorithmPlugin"; } }
    }
}
