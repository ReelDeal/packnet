﻿using System.ComponentModel.Composition;
using System.IO;

using PackNet.Business.SelectionAlgorithms.Plugins.BoxFirst;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.SelectionAlgorithms;

namespace PackNet.Business.SelectionAlgorithms.Plugins.Order
{
    [Export(typeof(ISelectionAlgorithm))]
    public class OrderSelectionAlgorithmPlugin : SelectionAlgorithmPluginBase, ISelectionAlgorithm
    {
        [ImportingConstructor]
        public OrderSelectionAlgorithmPlugin(ILogger logger) : 
            base(SelectionAlgorithmTypes.Order, "OrderSelectProducibleWorkFlow.xaml", "OrderStagingWorkFlow.xaml",  logger)
        {
        }

        public string UniqueId { get { return "OrderSelectionAlgorithmPlugin"; } }
    }
}
