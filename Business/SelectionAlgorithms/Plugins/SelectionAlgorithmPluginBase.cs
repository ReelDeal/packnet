﻿using System.IO;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Business.SelectionAlgorithms.Plugins
{
    public abstract class SelectionAlgorithmPluginBase
    {
        public SelectionAlgorithmPluginBase(SelectionAlgorithmTypes selectionAlgorithmType, string selectWorkflowName, string stagingWorkflowName, ILogger logger)
        {
            SelectionAlgorithmType = selectionAlgorithmType;
            SelectionWorkflowName = Path.Combine(SelectionAlgorithmType.ToString(), selectWorkflowName);
            StagingWorkflowName = Path.Combine(SelectionAlgorithmType.ToString(), stagingWorkflowName);
            logger.Log(LogLevel.Info, "Selection Algorithm plug-in '{0}' loaded", SelectionAlgorithmType);
        }

        public SelectionAlgorithmTypes SelectionAlgorithmType { get; private set; }
        public string SelectionWorkflowName { get; private set; }
        public string StagingWorkflowName { get; private set; }

        public void Validate(string pluginDirectory)
        {
            var fileName = Path.Combine(pluginDirectory, SelectionWorkflowName);
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("The select producible workflow " + SelectionWorkflowName + " does not exist.", fileName);
            } 
            
            fileName = Path.Combine(pluginDirectory, StagingWorkflowName);
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("The staging workflow " + StagingWorkflowName + " does not exist.", fileName);
            }
        }

        public void Dispose()
        {
        }
    }
}