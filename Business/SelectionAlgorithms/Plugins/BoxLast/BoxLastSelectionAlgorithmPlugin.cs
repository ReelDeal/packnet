﻿using System.ComponentModel.Composition;
using System.IO;

using PackNet.Business.SelectionAlgorithms.Plugins.BoxFirst;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.SelectionAlgorithms;

namespace PackNet.Business.SelectionAlgorithms.Plugins.BoxLast
{
    [Export(typeof(ISelectionAlgorithm))]
    public class BoxLastSelectionAlgorithmPlugin : SelectionAlgorithmPluginBase, ISelectionAlgorithm
    {
        [ImportingConstructor]
        public BoxLastSelectionAlgorithmPlugin(ILogger logger): 
            base(SelectionAlgorithmTypes.BoxLast, "BoxLastSelectProducibleWorkFlow.xaml", "BoxLastStagingWorkFlow.xaml",  logger)
        {
        }

        public string UniqueId { get { return "BoxLastSelectionAlgorithmPlugin"; } }
    }
}
