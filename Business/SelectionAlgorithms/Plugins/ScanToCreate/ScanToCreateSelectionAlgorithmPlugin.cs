﻿using System.ComponentModel.Composition;
using System.IO;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.SelectionAlgorithms;

namespace PackNet.Business.SelectionAlgorithms.Plugins.ScanToCreate
{
    [Export(typeof(ISelectionAlgorithm))]
    public class ScanToCreateSelectionAlgorithmPlugin : SelectionAlgorithmPluginBase, ISelectionAlgorithm
    {
        [ImportingConstructor]
        public ScanToCreateSelectionAlgorithmPlugin(ILogger logger) :
            base(SelectionAlgorithmTypes.ScanToCreate, "ScanToCreateSelectProducibleWorkFlow.xaml", "ScanToCreateStagingWorkFlow.xaml", logger)
        {
        }

        public string UniqueId { get { return "ScanToCreateSelectionAlgorithmPlugin"; } }

    }
}
