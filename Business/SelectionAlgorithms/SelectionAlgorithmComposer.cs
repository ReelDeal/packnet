﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.SelectionAlgorithms;
using PackNet.Common.Plugins;

namespace PackNet.Business.SelectionAlgorithms
{
    public class SelectionAlgorithmComposer : IDisposable
    {
        [ImportMany]
        public IEnumerable<ISelectionAlgorithm> SelectionAlgorithms { get; private set; }

        /// <summary>
        /// Composes a list of IWorkFlowSelectionAlgorithm instances from a given directory and the current assembly
        /// </summary>
        /// <param name="pluginDirectory"></param>
        /// <param name="importers"></param>
        /// <param name="logger"></param>
        public SelectionAlgorithmComposer(string pluginDirectory, IEnumerable<IWorkflowImporter> importers, ILogger logger)
        {
            var assembly = Assembly.GetExecutingAssembly();
            var assemblyDirectory = Path.GetDirectoryName(assembly.Location);
            var pluginPath = Path.Combine(assemblyDirectory, pluginDirectory);

            var batch = new CompositionBatch();
            batch.AddExportedValue(logger);
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(assembly));

            batch.AddPart(this);
            MefComposer.Compose(pluginDirectory, batch, catalog);
            SelectionAlgorithms.ForEach(i => i.Validate(pluginPath));
            
        }

        public void Dispose()
        {
            SelectionAlgorithms.ForEach(i => i.Dispose());
        }
    }
}
