﻿using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CloudReporting;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace PackNet.Business.CloudReporting
{
    public class ProducibleCompleteRecorder
    {
        private readonly ILogger logger;
        private readonly IEventAggregatorSubscriber subscriber;

        public ProducibleCompleteRecorder(IEventAggregatorSubscriber subscriber, ILogger logger)
        {
            this.subscriber = subscriber;
            this.logger = logger;

            Setup();
        }

        private void Setup()
        {
            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var machineProductionData = m.Data.Item2;
                    var time = TimeSpan.FromMilliseconds(machineProductionData.ProductionTime);

                    var producibles = new List<string>();
                    var tiledCarton = machineProductionData.Carton as TiledCarton;
                    var articleId = machineProductionData.Carton.ArticleId;
                    if (tiledCarton != null)
                    {
                        articleId = tiledCarton.Tiles.First().ArticleId;
                        foreach (var tile in tiledCarton.Tiles)
                        {
                            producibles.Add(tile.CustomerUniqueId);
                        }
                    }
                    else
                    {
                        producibles.Add(machineProductionData.Carton.CustomerUniqueId);
                    }

                    var pr = new BoxProductionResult
                    {
                        DateTimeStamp = DateTime.UtcNow,
                        MachineId = machineProductionData.Machine.Id,
                        MachineName = machineProductionData.Machine.Alias,
                        CorrugateName = machineProductionData.Corrugate.Alias,
                        CorrugateQuality = machineProductionData.Corrugate.Quality,
                        CorrugateThickness = machineProductionData.Corrugate.Thickness,
                        CorrugateWidth = machineProductionData.Corrugate.Width,
                        CorrugateUsedLength = machineProductionData.FedLength,
                        CorrugateUsedWidth = machineProductionData.CartonOnCorrugate.CartonWidthOnCorrugate * machineProductionData.ProducibleCount,
                        IsSuccess = !machineProductionData.Failure,
                        PackagingDesignId = machineProductionData.Carton.DesignId,
                        PackagingHeight = machineProductionData.Carton.Height,
                        PackagingWidth = machineProductionData.Carton.Width,
                        PackagingLength = machineProductionData.Carton.Length,
                        Producibles = producibles,
                        TimeToProduce = time.TotalSeconds,
                        Username = machineProductionData.UserName,
                        ArticleName = articleId,
                        AdditionalDimensions = machineProductionData.Carton.XValues
                    };
                    logger.Log(LogLevel.Info, "BoxProductionResult {0}", JsonConvert.SerializeObject(pr));
                }, logger);
        }
    }
}
