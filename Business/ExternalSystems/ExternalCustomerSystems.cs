﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.ExternalSystems;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Data.ExternalSystems;

namespace PackNet.Business.ExternalSystems
{
    public class ExternalCustomerSystems : IExternalCustomerSystems
    {
        private readonly IExternalCustomerSystemRepository ExternalCustomerSystemRepository;

        public ExternalCustomerSystems(IExternalCustomerSystemRepository ExternalCustomerSystemRepository)
        {
            this.ExternalCustomerSystemRepository = ExternalCustomerSystemRepository;
        }

        public ExternalCustomerSystem Create(ExternalCustomerSystem ExternalCustomerSystem)
        {
            if (ExternalCustomerSystem.Id != Guid.Empty)
                throw new Exception("Invalid ExternalCustomerSystem ID, Id is already populated");

            if (Find(ExternalCustomerSystem.Alias) != null)
                throw new ItemExistsException("Invalid ExternalCustomerSystem, ExternalCustomerSystem already exists");

            ExternalCustomerSystemRepository.Create(ExternalCustomerSystem);
            return ExternalCustomerSystem;
        }

        public ExternalCustomerSystem Update(ExternalCustomerSystem externalCustomerSystem)
        {
            var exisitng = Find(externalCustomerSystem.Alias);
            if (exisitng != null && exisitng.Id != externalCustomerSystem.Id)
                throw new ItemExistsException("Invalid ExternalCustomerSystem, ExternalCustomerSystem already exists");

            return ExternalCustomerSystemRepository.Update(externalCustomerSystem);
        }

        public void Delete(ExternalCustomerSystem ExternalCustomerSystem)
        {
            ExternalCustomerSystemRepository.Delete(ExternalCustomerSystem);
        }

        public void Delete(IEnumerable<ExternalCustomerSystem> ExternalCustomerSystems)
        {
            ExternalCustomerSystemRepository.Delete(ExternalCustomerSystems);
        }

        public ExternalCustomerSystem Find(Guid id)
        {
            return ExternalCustomerSystemRepository.Find(id);
        }

        public ExternalCustomerSystem Find(string ExternalCustomerSystemName)
        {
            return ExternalCustomerSystemRepository.Find(ExternalCustomerSystemName);
        }

        public IEnumerable<ExternalCustomerSystem> All()
        {
            return ExternalCustomerSystemRepository.All();
        }
    }
}
