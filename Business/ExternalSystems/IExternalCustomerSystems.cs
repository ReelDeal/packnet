﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.ExternalSystems;

namespace PackNet.Business.ExternalSystems
{
    public interface IExternalCustomerSystems
    {
        ExternalCustomerSystem Create(ExternalCustomerSystem externalCustomerSystem);
        ExternalCustomerSystem Update(ExternalCustomerSystem externalCustomerSystem);
        void Delete(ExternalCustomerSystem externalCustomerSystem);
        ExternalCustomerSystem Find(Guid id);
        ExternalCustomerSystem Find(string externalCustomerSystemName);
        IEnumerable<ExternalCustomerSystem> All();
    }
}