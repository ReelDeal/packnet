﻿using System.Activities.Statements;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Business.PackagingDesigns
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Common.Interfaces.DTO.PackagingDesigns;

    public class PackagingDesignManager : IDisposable, IPackagingDesignManager, IPackagingDesignCalculations
    {
        private readonly object loadingLock = new object();
        private readonly ICachingService cachingService;
        private FileSystemWatcher designFileWatcher;
        private List<PackagingDesignIdToFileNameMapping> designFiles;
        private readonly ILogger logger;
        private bool disposing;

        public PackagingDesignManager(string pathToDesigns, ICachingService cachingService, ILogger logger)
        {
            this.cachingService = cachingService;
            Path = pathToDesigns;
            designFiles = new List<PackagingDesignIdToFileNameMapping>();
            Designs = new Dictionary<int, IPackagingDesign>();
            this.logger = logger;
        }

        public event EventHandler DesignsChanged;
        public event Action<string> DesignLoadError;

        public string Path { get; set; }

        public Dictionary<int, IPackagingDesign> Designs
        {
            get;
            private set;
        }

        private static PhysicalDesign RotateDesign90Degrees(PhysicalDesign designToRotate)
        {
            var rotatedDesign = new PhysicalDesign { Length = designToRotate.Width, Width = designToRotate.Length };

            designToRotate.Lines.Select(l =>
                            new PhysicalLine(
                    new PhysicalCoordinate(designToRotate.Length - l.EndCoordinate.Y, l.StartCoordinate.X),
                    new PhysicalCoordinate(designToRotate.Length - l.StartCoordinate.Y, l.EndCoordinate.X),
                    l.Type)).ForEach(rotatedDesign.Add);

            return rotatedDesign;
        }

        private static PhysicalDesign RotateDesign180Degrees(PhysicalDesign designToRotate)
        {
            var rotatedDesign = new PhysicalDesign { Length = designToRotate.Length, Width = designToRotate.Width};

            designToRotate.Lines.Select(l =>
                new PhysicalLine(
                    new PhysicalCoordinate(designToRotate.Width - l.StartCoordinate.X, designToRotate.Length - l.StartCoordinate.Y),
                    new PhysicalCoordinate(designToRotate.Width - l.EndCoordinate.X, designToRotate.Length - l.EndCoordinate.Y),
                    l.Type)).ForEach(rotatedDesign.Add);

            return rotatedDesign;
        }

        private PhysicalDesign RotateDesign270Degrees(PhysicalDesign designToRotate)
        {
            var rotatedDesign = new PhysicalDesign { Length = designToRotate.Width, Width = designToRotate.Length };

            designToRotate.Lines.Select(l =>
                new PhysicalLine(
                    new PhysicalCoordinate(l.StartCoordinate.Y, designToRotate.Width - l.EndCoordinate.X), 
                    new PhysicalCoordinate(l.EndCoordinate.Y, designToRotate.Width - l.StartCoordinate.X), 
                    l.Type )).ForEach(rotatedDesign.Add);

            return rotatedDesign;
        }

        private PhysicalDesign FlipDesign(PhysicalDesign designToFlip)
        {
            var flippedDesign = new PhysicalDesign { Length = designToFlip.Length, Width = designToFlip.Width };

            designToFlip.Lines.Select(l =>
                new PhysicalLine(
                    new PhysicalCoordinate(designToFlip.Width - l.EndCoordinate.X, l.StartCoordinate.Y),
                    new PhysicalCoordinate(designToFlip.Width - l.StartCoordinate.X, l.EndCoordinate.Y),
                    l.Type)).ForEach(flippedDesign.Add);

            return flippedDesign;
        }

        public bool HasDesignWithId(int designId)
        {
            lock (loadingLock)
                return Designs.Keys.Any(id => id == designId);
        }

        public void ReloadDesignsFromDisk()
        {
            lock (loadingLock)
            {
                if (designFileWatcher != null)
                    designFileWatcher.EnableRaisingEvents = false;

                designFiles.Clear();
                Designs.Clear();
                //cachingService.Clear();

                if (Path != null && Directory.Exists(Path))
                {
                    LoadDesignsFromFiles();
                }

                if (designFileWatcher != null && !disposing)
                    designFileWatcher.EnableRaisingEvents = true;
            }
        }

        public IEnumerable<IPackagingDesign> GetDesigns()
        {
            return Designs.Values.OrderBy(c => c.Name);
        }

        public PackagingDesignCalculation CalculateUsage(ICarton carton, Corrugate corrugate, int allowedTileCount, bool isRotated)
        {
            var design = GetPhyscialDesignForCarton(carton, corrugate, isRotated ? OrientationEnum.Degree90 : OrientationEnum.Degree0, allowedTileCount);

            if (design == null)
                return PackagingDesignCalculation.MaxValue;

            return new PackagingDesignCalculation
            {
                Length = design.Length,
                Width = design.Width
            };
        }

        public PhysicalDesign GetPhyscialDesignForCarton(ICarton carton, Corrugate corrugate, OrientationEnum rotation = OrientationEnum.Degree0)
        {
            return GetPhyscialDesignForCarton(carton, corrugate, rotation, 1);
        }

        public PhysicalDesign GetPhyscialDesignForCarton(ICarton carton, Corrugate corrugate, OrientationEnum rotation, int tileCount)
        {
            PhysicalDesign physicalDesign;
            var cacheKey = GetCacheKey(carton, corrugate, rotation, tileCount);
            if (cachingService.TryGet(cacheKey, out physicalDesign))
            {
                    return physicalDesign.Clone() as PhysicalDesign;
            }
            lock (loadingLock)
            {
                var design = GetDesignFromId(carton.DesignId);
                if (design == null)
                    return null;
                physicalDesign = GetRotatedDesign(rotation, design, carton, corrugate, tileCount);

                if (physicalDesign != null)
                {
                    cachingService.CacheItem(cacheKey, physicalDesign);
                    return physicalDesign.Clone() as PhysicalDesign;
                }
            }
            return null;
        }

        public PackagingDesign GetDesignFromId(int designId)
        {
            IPackagingDesign design;
            if (!this.Designs.TryGetValue(designId, out design))
            {
                this.DesignLoadError(string.Format("Design with id {0} was not found", designId));
                return null;
            }

            return design.Clone() as PackagingDesign;
        }

        private PhysicalDesign GetRotatedDesign(OrientationEnum rotation, IPackagingDesign design, ICarton carton, Corrugate corrugate, int tileCount)
        {
            if (rotation == OrientationEnum.Degree0 && (design.AllowedRotations == null || design.AllowedRotations.Degree0.Allowed))
                return design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount);
            if (rotation == OrientationEnum.Degree0_flip && (design.AllowedRotations == null || design.AllowedRotations.Degree0Flip.Allowed))
                return FlipDesign(design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount));
            if (rotation == OrientationEnum.Degree90 && design.AllowedRotations.Degree90.Allowed)
                return  RotateDesign90Degrees(design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount, true));
            if (rotation == OrientationEnum.Degree90_flip && design.AllowedRotations.Degree90Flip.Allowed)
                return FlipDesign(RotateDesign90Degrees(design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount, true)));
            if (rotation == OrientationEnum.Degree180 && design.AllowedRotations.Degree180.Allowed)
                return RotateDesign180Degrees(design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount));
            if (rotation == OrientationEnum.Degree180_flip && design.AllowedRotations.Degree180Flip.Allowed)
                return FlipDesign(RotateDesign180Degrees(design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount)));
            if (rotation == OrientationEnum.Degree270 && design.AllowedRotations.Degree270.Allowed)
                return RotateDesign270Degrees(design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount, true));
            if (rotation == OrientationEnum.Degree270_flip && design.AllowedRotations.Degree270Flip.Allowed)
                return FlipDesign(RotateDesign270Degrees(design.AsPhysicalDesign(carton.GetDesignFormulaParameters(corrugate), tileCount, true)));

            return null;
        }

        private string GetCacheKey(ICarton carton, Corrugate corrugate, OrientationEnum rotation, int tileCount)
        {
            var key = carton.DesignId + ":" + rotation + ":" + corrugate.Width + ":" + corrugate.Thickness + ":" + carton.Width + ":" +
                   carton.Length + ":" + carton.Height + ":" + /*Tile count used for elastic lines*/ tileCount;
            carton.XValues.Values.ForEach(x => key += ":" + x);
            return key;
        }

        /// <summary>
        /// Returns DesignName for a given DesignId.
        /// </summary>
        /// <param name="designId"></param>
        /// <returns>string.Empty if not found</returns>
        public string GetDesignName(int designId)
        {
            IPackagingDesign packagingDesign = null;
            Designs.TryGetValue(designId, out packagingDesign);
            return packagingDesign == null ? string.Empty : packagingDesign.Name;
        }

        /// <summary>
        /// Logs all designs that uses XValues with XValues that does not have a name->alias mapping parameter defined
        /// </summary>
        private void LogLegacyDesign(string filePath, IPackagingDesign design)
        {
            logger.Log(LogLevel.Warning, string.Format("Design: {0} ({1}) uses XValues and does not have aliases associated - it should be updated", filePath, design.Id));
        }

        private void OnDesignFilesChanged(object sender, FileSystemEventArgs e)
        {
            //only watch for files we care about.
            if (e.Name.EndsWith(".ezd") && e.Name.EndsWith(".pcd") || disposing) return;

            ReloadDesignsFromDisk();

            var handler = DesignsChanged;
            if (handler != null)
            {
                handler(null, null);
            }
        }

        private void SetUpFileWatcher()
        {
            if (designFileWatcher == null)
            {
                designFileWatcher = new FileSystemWatcher(Path, "*.*");
                designFileWatcher.IncludeSubdirectories = false;
                designFileWatcher.Changed += new FileSystemEventHandler(OnDesignFilesChanged);
                designFileWatcher.Deleted += new FileSystemEventHandler(OnDesignFilesChanged);
            }

            designFileWatcher.EnableRaisingEvents = true;
        }

        private void LoadDesignsFromFiles()
        {
            var files =
                Directory.GetFiles(Path, "*.*", SearchOption.TopDirectoryOnly)
                         .Where(s => s.EndsWith(".ezd") || s.EndsWith(".pcd"))
                         .OrderBy(s =>
                         {
                             return s.EndsWith(".ezd") ? 1 : 2;
                         });

            foreach (string file in files)
            {
                LoadDesignFile(file);
            }
        }

        private void LoadDesignFile(string filePath)
        {
            PackagingFile packagingFile;

            try
            {
                packagingFile = PackagingFile.Deserialize(filePath);
                if (packagingFile.Designs != null)
                {
                    foreach (PackagingDesign design in packagingFile.Designs)
                    {
                        if (IsValidDesign(design, filePath))
                        {
                            if (design.DesignParameters.Any() == false)
                            {
                                var parameters = design.GetUsedParameters();
                                if (parameters.Any())
                                {
                                    LogLegacyDesign(filePath, design);
                                }

                                design.DesignParameters = parameters;
                            }

                            Designs.Add(design.Id, design);
                            designFiles.Add(new PackagingDesignIdToFileNameMapping(filePath, design.Id));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                DesignLoadError(
                    string.Format(
                        "Error loading design file:'{0}' ensure that it is a valid packaging file and does not use special xml characters like '< > &', Exception:{1}",
                        filePath, e.Message));
            }
        }

        /// <summary>
        /// Determine if the design is good or not.  Currently checking to see if the id is unique and if a logical design if all designs listed are found.
        /// </summary>
        /// <param name="packagingDesign"></param>
        /// <param name="designFilePath"></param>
        /// <exception cref="ApplicationException">Thrown when a designId in a condition is specified but does not exist in the design list.</exception>
        /// <returns></returns>
        private bool IsValidDesign(IPackagingDesign packagingDesign, string designFilePath)
        {
            return IsIdAlreadyUsed(packagingDesign, designFilePath) && DesignContainsRotationPermissions(packagingDesign, designFilePath);
        }

        private bool IsIdAlreadyUsed(IPackagingDesign packagingDesign, string designFilePath)
        {
            foreach (PackagingDesignIdToFileNameMapping existingId in designFiles)
            {
                if (existingId.Id == packagingDesign.Id)
                {
                    DesignLoadError(string.Format("Design with ID {0} already loaded. File {1} and {2} have the same ID. File {2} will be ignored.", packagingDesign.Id, existingId.FileName, designFilePath));

                    return false;
                }
            }
            return true;
        }

        private bool DesignContainsRotationPermissions(IPackagingDesign packagingDesign, string designFilePath)
        {
            if (packagingDesign is PackagingDesign && packagingDesign.AllowedRotations == null)
            {
                DesignLoadError(
                    string.Format(
                        "Design file {0} is missing rotation permission, please add rotation permissions to file.",
                        designFilePath));

                return false;
            }

            return true;
        }

        public void LoadDesigns()
        {
            ReloadDesignsFromDisk();
            SetUpFileWatcher();
        }

        /// <summary>
        /// Currently only used for testing.
        /// </summary>
        /// <param name="d"></param>
        public void AddDesign(PackagingDesign d)
        {
            lock (loadingLock)
            {
                if (Designs.ContainsKey(d.Id))
                    throw new ApplicationException("Design already existing please update design ID DesignId:" + d.Id);
                Designs.Add(d.Id, d);
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool dispose)
        {
            if (!disposing)
            {
                disposing = true;
                if (designFileWatcher != null)
                {
                    designFileWatcher.EnableRaisingEvents = false;
                    designFileWatcher.Dispose();
                }
            }
        }
    }
}
