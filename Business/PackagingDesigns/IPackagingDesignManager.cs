﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;

namespace PackNet.Business.PackagingDesigns
{
    using System;
    using System.Collections.Generic;

    using Common.Interfaces.DTO.PackagingDesigns;

    public interface IPackagingDesignManager
    {
        event Action<string> DesignLoadError;
        string Path { get; set; }
        bool HasDesignWithId(int designId);
        void ReloadDesignsFromDisk();
        IEnumerable<IPackagingDesign> GetDesigns();
        PhysicalDesign GetPhyscialDesignForCarton(ICarton carton, Corrugate corrugate, OrientationEnum rotation = OrientationEnum.Degree0);
        PhysicalDesign GetPhyscialDesignForCarton(ICarton carton, Corrugate corrugate, OrientationEnum rotation, int tileCount);
        PackagingDesignCalculation CalculateUsage(ICarton carton, Corrugate corrugate, int allowedTileCount, bool isRotated);
        void LoadDesigns();
        PackagingDesign GetDesignFromId(int designId);
        string GetDesignName(int designId);
        void Dispose();
    }
}