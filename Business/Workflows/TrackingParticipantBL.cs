﻿using System.Activities.Expressions;
using System.Reactive.Concurrency;
using System.Reflection;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Logging;
using PackNet.Common.Utils;
using PackNet.Common.WorkflowTracking;
using PackNet.Data.Workflows;
using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Utils;

namespace PackNet.Business.Workflows
{
    /// <summary>
    /// OK we're outputing to two places, the worflowLifetimes mongo repo where we only store the last record
    /// 
    /// 
    /// </summary>
    public class TrackingParticipantBL : ITrackingParticipantBL, IDisposable
    {
        private readonly IWorkflowTrackingRepository repo;
        private readonly ILogger logger;
        private WorkflowTrackingLevel trackingLevel;
        private readonly ConcurrentList<Tuple<WorkflowLifetime, TrackingParticipant>> activeWorkflows = new ConcurrentList<Tuple<WorkflowLifetime, TrackingParticipant>>();
        private CancellationTokenSource activeWorkflowCancellationToken = new CancellationTokenSource();
        private TimeSpan activeWorkflowSleep = TimeSpan.FromMinutes(1);
        private readonly double activeWorkflowLongRunningThresholdInSeconds =  PackNet.Business.Properties.Settings.Default.ActiveWorkflowLongRunningThresholdInSeconds;

        public TrackingParticipantBL(IWorkflowTrackingRepository repo, ILogger logger)
        {
            this.repo = repo;
            this.logger = LogManager.GetLogFor("workflowTracking");
            SetTrackingLevel(WorkflowTrackingLevel.TrackEverything);
        }

        /// <summary>
        /// Create a new tracking participant for each workflow in the system.  Create a buffer for that workflow instance and write data to the logs and database.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="workflowPath"></param>
        /// <param name="additionalInfo"></param>
        /// <returns></returns>
        public WorkflowLifetime Factory(WorkflowTypes type, string workflowPath, Dictionary<string, object> additionalInfo)
        {
                var lifetime = new WorkflowLifetime
                {
                    Type = type,
                    WorkflowPath = workflowPath,
                    AdditionalInformation = additionalInfo
                };
                repo.Create(lifetime);

            lifetime.WorkflowLifeTimeChangedObservable.Subscribe(lt =>
                        {
                repo.Update(lt);
                }, () =>
                {
                repo.Update(lifetime);
                });

            return lifetime;
        }

        public void SetTrackingLevel(WorkflowTrackingLevel level)
        {
            trackingLevel = level;
        }

        public void Dispose()
        {
            activeWorkflowCancellationToken.Cancel(false);
        }
    }
}
