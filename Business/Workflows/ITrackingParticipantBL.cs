﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Business.Workflows
{
    public interface ITrackingParticipantBL
    {
        WorkflowLifetime Factory(WorkflowTypes type, string workflowPath, Dictionary<string, object> additionalInfo);
        void SetTrackingLevel(WorkflowTrackingLevel level);
    }
}