﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Logging;
using PackNet.Data.Templates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace PackNet.Business.Templates
{
    public class Templates : ITemplates
    {
        private readonly ITemplateRepository repository;
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="Templates"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="logger">The logger.</param>
        public Templates(ITemplateRepository repository,
            ILogger logger)
        {
            this.repository = repository;
            this.logger = logger;

            //load from fs if we don't have them in the database
            IList<Template> dbTemplates = repository.All().ToList();
            var fsTemplates = GetInitialData();
            fsTemplates.ForEach(t =>
            {
                if (dbTemplates.FirstOrDefault(x => x.Name == t.Name) == null)
                    repository.Create(t);
            });
        }

        /// <summary>
        /// Gets the initial data.
        /// </summary>
        /// <returns></returns>
        public IList<Template> GetInitialData()
        {
            var filePaths = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "Data\\Labels"), "*.prn");

            var result = filePaths.Select(filePath => new Template
            {
                Name = Path.GetFileNameWithoutExtension(filePath),
                Content = File.ReadAllText(filePath)
            }).ToList();

            return result;
        }

        /// <summary>
        /// Updates the specified template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>
        /// The specified template.
        /// </returns>
        public Template Update(Template template)
        {
            return repository.Update(template);
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>
        /// List of available templates.
        /// </returns>
        public IList<Template> GetAll()
        {
            IList<Template> templates = repository.All().ToList();
            return templates;
        }

        /// <summary>
        /// Gets the specified template name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>
        /// The template.
        /// </returns>
        public Template Get(string name)
        {
            return GetAll().SingleOrDefault(t => t.Name == name);
        }
    }
}
