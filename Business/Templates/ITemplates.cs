﻿using PackNet.Common.Interfaces.DTO;
using System.Collections.Generic;

namespace PackNet.Business.Templates
{
    /// <summary>
    /// Defines behavior of Template Manager
    /// </summary>
    public interface ITemplates
    {
        /// <summary>
        /// Gets the initial data.
        /// </summary>
        /// <returns></returns>
        IList<Template> GetInitialData();
        
        /// <summary>
        /// Updates the specified template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>The specified template.</returns>
        Template Update(Template template);

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns>List of available templates.</returns>
        IList<Template> GetAll();

        /// <summary>
        /// Gets the specified template name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>The template.</returns>
        Template Get(string name);
    }
}