using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Business.DatabaseTask 
{
    public interface IDatabaseTasks 
    {
        IObservable<IMessage<Common.Interfaces.DTO.DatabaseTask>> Messaging { get; }

        void BackupDatabase(string replyTo);

        void RestoreDatabase(string zipFile, string replyTo, bool performBackupFirst);

        List<Tuple<string, string>> GetDatabaseRestoreFiles();
    }
}