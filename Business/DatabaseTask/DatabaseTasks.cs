﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.FileHandling;
using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Business.DatabaseTask
{
    public class DatabaseTasks : IDatabaseTasks
    {
        private const string CollectionName = "PackNetServer";
        private const string DatabaseBackupsDirectory = "DatabaseBackups";


        private readonly IFileSystem fileSystem;
        private readonly IDatabaseTaskRepository repository;

        private readonly ISubject<IMessage<Common.Interfaces.DTO.DatabaseTask>> _progressSubject = new Subject<IMessage<Common.Interfaces.DTO.DatabaseTask>>();
        public IObservable<IMessage<Common.Interfaces.DTO.DatabaseTask>> Messaging { get { return _progressSubject.AsObservable(); } }

        public DatabaseTasks(IFileSystem fileSystem, IDatabaseTaskRepository repository)
        {
            this.fileSystem = fileSystem;
            this.repository = repository;

            if (!Directory.Exists(DatabaseBackupsDirectory))
            {
                Directory.CreateDirectory(DatabaseBackupsDirectory);
            }
        }

        public void BackupDatabase(string replyTo = null)
        {
            var tempDir = fileSystem.CreateTemporaryDirectory();

            var startInfo = new ProcessStartInfo
            {
                WorkingDirectory = tempDir.FullName,
                FileName = @"C:\Program Files\MongoDB 2.6 Standard\bin\mongodump.exe",
                Arguments = string.Format("-vvvvv -d \"{0}\" -o \"{1}\"", CollectionName, tempDir.FullName),
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            MonitorProcess(Process.Start(startInfo), replyTo);

            SendMessage(new Common.Interfaces.DTO.DatabaseTask() { Message = "Backup Complete. Beginning compression" }, replyTo);

            Thread.Sleep(1000);

            var fileName = CompressBackup(tempDir.FullName);

            SendMessage(new Common.Interfaces.DTO.DatabaseTask() { IsComplete = true, Path = fileName, Message = String.Format("Compression Complete. File is stored at: {0}", fileName) }, replyTo, true);
        }

        public void RestoreDatabase(string zipFile, string replyTo, bool performBackupFirst)
        {
            if (performBackupFirst)
            {
                SendMessage("Backing up database before restore", replyTo);
                BackupDatabase(replyTo);
                SendMessage("Completed Backup", replyTo);
            }

            SendMessage("Creating temporary directory", replyTo);

            var tempDir = fileSystem.CreateTemporaryDirectory();

            SendMessage(string.Format("Temporary directory: {0} created. Beginning to extract archive", tempDir), replyTo);

            ZipFile.ExtractToDirectory(zipFile, tempDir.FullName);

            SendMessage("Archive finished extracting. Dropping database", replyTo);

            repository.DropDatabase();

            SendMessage("Database dropped. Beginning import", replyTo);

            var startInfo = new ProcessStartInfo
            {
                WorkingDirectory = tempDir.FullName,
                FileName = @"C:\Program Files\MongoDB 2.6 Standard\bin\mongorestore.exe",
                Arguments = string.Format("-vvvvv -d \"{0}\" {0}", CollectionName),
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            MonitorProcess(Process.Start(startInfo), replyTo);

            SendMessage("Import finished. A server restart may be required.", replyTo, true);
        }

        public List<Tuple<string, string>> GetDatabaseRestoreFiles()
        {
            var di = new DirectoryInfo(DatabaseBackupsDirectory);
            return di.GetFiles("*.zip").OrderByDescending(x => x.CreationTimeUtc)
                .Select(file => new Tuple<string, string>(Path.GetFileName(file.FullName), file.FullName)).ToList();
        }

        private string CompressBackup(string pathToBackup)
        {
            var fileName = fileSystem.GetTimestampedFileName("zip");

            fileName = Path.Combine(DatabaseBackupsDirectory, fileName);

            var pathForBackup = Path.Combine(pathToBackup, CollectionName);

            ZipFile.CreateFromDirectory(pathForBackup, fileName, CompressionLevel.Optimal, true);

            return fileName;
        }

        private void MonitorProcess(Process p, string replyTo)
        {
            while (!p.StandardOutput.EndOfStream)
            {
                var line = p.StandardOutput.ReadLine();
                SendMessage(new Common.Interfaces.DTO.DatabaseTask()
                {
                    IsComplete = false,
                    Message = line
                }, replyTo);
            }
        }

        private void SendMessage(string msg, string replyTo, bool isCompleted = false)
        {
            SendMessage(new Common.Interfaces.DTO.DatabaseTask()
            {
                Message = msg,
                IsComplete = isCompleted
            }, replyTo, isCompleted);
        }

        private void SendMessage(Common.Interfaces.DTO.DatabaseTask task, string replyTo, bool complete = false)
        {
            var msg = new Message<Common.Interfaces.DTO.DatabaseTask> { ReplyTo = replyTo, Data = task, MessageType = complete ? DatabaseTaskMessages.DatabaseActionComplete : DatabaseTaskMessages.DatabaseActionInProgress };

            _progressSubject.OnNext(msg);
        }
    }
}