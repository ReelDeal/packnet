﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

using PackNet.Business.L10N;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Data.UserManagement;

namespace PackNet.Business.UserManagement
{
    using Common.Interfaces.Exceptions;

    public class Users : IUsers
    {
        private readonly IUserRepository userRepository;

        public Users(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
            userRepository.EnsureDefaultUserExists();
        }

        public User Create(User user)
        {
            if (user.Id != Guid.Empty)
                throw new Exception(Strings.Users_Create_Invalid_user_ID__Id_is_already_populated);

            if (Find(user.UserName) != null)
                throw new ItemExistsException(Strings.Users_Create_Invalid_user__user_already_exists);

            userRepository.Create(user);
            return user;
        }

        public User Update(User user)
        {
            var existingUser = Find(user.UserName);

            if (existingUser.Id != user.Id)
                throw new ItemExistsException(Strings.Users_Create_Invalid_user__user_already_exists);

            return userRepository.Update(user);
        }

        public void Delete(User user)
        {
            userRepository.Delete(user);
        }

        public void Delete(IEnumerable<User> users)
        {
            userRepository.Delete(users);
        }

        public User Find(Guid id)
        {
            return userRepository.Find(id);
        }

        public User Find(string userName)
        {
            return userRepository.Find(userName);
        }

        public IEnumerable<User> All()
        {
            return userRepository.All();
        }

        public User Login(string userName, string password)
        {
            var user = userRepository.Find(userName, password);
            if (user != null)
            {
                user.Token = Guid.NewGuid().ToString("N");
                Update(user);
                
                // TODO: We should create a separate LoginUser, to avoid sending the password back to the Client
                user.Password = string.Empty;
            }
            return user;
        }

        public User ValidateToken(string token)
        {
            Contract.Requires(!string.IsNullOrEmpty(token), "Token cannot be null or empty");

            return userRepository.FindByToken(token);
        }

        public User FindAutoLoginUser()
        {
            return userRepository.FindByAutoLogin();
        }
    }
}
