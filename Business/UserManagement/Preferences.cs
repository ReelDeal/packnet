﻿using System;
using System.Diagnostics.Contracts;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Data.UserManagement;

namespace PackNet.Business.UserManagement
{
    public class Preferences : IPreferences
    {
        private readonly IPreferenceRepository preferenceRepository;

        public Preferences(IPreferenceRepository preferenceRepository)
        {
            this.preferenceRepository = preferenceRepository;
        }

        public Preference Update(Preference preference)
        {
            Contract.Requires(preference.Id != Guid.Empty);
            Contract.Requires(preference.UserId != Guid.Empty);

            return preferenceRepository.Update(preference);
        }

        public Preference Find(Guid id, bool returnDefaultPreferences = true)
        {
            Contract.Requires(id != Guid.Empty, "Guid must be specified");

            var result = preferenceRepository.Find(id);

            if (result == null && returnDefaultPreferences)
            {
                result = GetDefault();
                result.Id = id;
            }

            return result;
        }

        public Preference Find(User user, bool returnDefaultPreferences = true)
        {
            Contract.Requires(user != null, "User cannot be null");

            var result = preferenceRepository.Find(user);

            if (result == null && returnDefaultPreferences)
            {
                result = GetDefault();
                result.UserId = user.Id;
            }

            return result;
        }

        public Preference GetDefault()
        {
            return new Preference() { Id = Guid.NewGuid(), LogLevel = "Error", MaximumNumberOfLogMessagesToRecord = 8192, PreferredLocale = "en_US" };
        }
    }
}