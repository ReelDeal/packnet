﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO.Users;

namespace PackNet.Business.UserManagement
{
    public interface IUsers
    {
        User Create(User user);
        User Update(User user);
        void Delete(User user);
        User Find(Guid id);
        User Find(string userName);
        IEnumerable<User> All();

        User Login(string userName, string password);
        User ValidateToken(string token);
        User FindAutoLoginUser();
    }
}