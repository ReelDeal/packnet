﻿using System;
using PackNet.Common.Interfaces.DTO.Users;

namespace PackNet.Business.UserManagement
{
    public interface IPreferences
    {
        Preference Update(Preference preference);
        Preference Find(Guid id, bool returnDefaultPreferences = true);
        Preference Find(User user, bool returnDefaultPreferences = true);
        Preference GetDefault();
    }
}