namespace PackNet.Business.Settings
{
    public interface IPackNetServerSettings
    {
        Common.Interfaces.DTO.Settings.PackNetServerSettings Update(Common.Interfaces.DTO.Settings.PackNetServerSettings packNetServerSettings);
        Common.Interfaces.DTO.Settings.PackNetServerSettings GetSettings();
    }
}