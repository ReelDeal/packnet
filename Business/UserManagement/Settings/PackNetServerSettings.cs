﻿using System.Linq;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Data.Settings;

using DTO = PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.Settings
{
	public class PackNetServerSettings : IPackNetServerSettings
	{
		private readonly IPackNetServerSettingsRepository packNetServerSettingsRepository;
		private readonly ILogger logger;
	    private readonly IEventAggregatorPublisher publisher;

	    public PackNetServerSettings(IPackNetServerSettingsRepository packNetServerSettingsRepository,
			ILogger logger, IEventAggregatorPublisher publisher)
		{
			this.packNetServerSettingsRepository = packNetServerSettingsRepository;
			this.logger = logger;
		    this.publisher = publisher;
		}

		public DTO.Settings.PackNetServerSettings CreateSettings()
		{
			var settings = new DTO.Settings.PackNetServerSettings
			{
				MachineCallbackIpAddress = Networking.FindIPAddress(true),
				MachineCallbackPort = 9998,
				LoginExpiresSeconds = 28800,
				FileImportSettings = new DTO.FileImportSettings
					{
						CommentIndicator = '~',
						DefaultImportType = "BoxFirst",
						DeleteImportedFiles = false,
						FieldDelimiter = ';',
						FileExtension = "csv",
						HeaderFields = new string[0],
						MonitoredFolderPath = "DropFolder",
						SecondsToRetryToAccessMonitoredFilePath = 60,
						TimesToRetryToAccessMonitoredFilePath = 5,
						SecondsToRetryLockedFile = 15,
						TimesToRetryLockedFile = 5
					},
                    WorkflowTrackingLevel = WorkflowTrackingLevel.TrackWithoutArguments
			};
			packNetServerSettingsRepository.Create(settings);
			return settings;
		}

		public DTO.Settings.PackNetServerSettings Update(DTO.Settings.PackNetServerSettings packNetServerSettings)
		{
		    var resetAllMachines = false;
		    var currentSettings = GetSettings();
		    if (currentSettings != null &&
		        (currentSettings.MachineCallbackIpAddress.ToString() != packNetServerSettings.MachineCallbackIpAddress.ToString() ||
                 currentSettings.MachineCallbackPort != packNetServerSettings.MachineCallbackPort))
		    {
		        //Reset all machines cause callback changed
		        resetAllMachines = true;
		    }
			var newSettings = packNetServerSettingsRepository.Update(packNetServerSettings);

		    if (resetAllMachines)
		    {
		        publisher.Publish(new Message
		        {
                    MessageType = PackNetServerSettingsMessages.ServerCallBackIPSettingsUpdated
		        });
		    }

            if (currentSettings != null && currentSettings.WorkflowTrackingLevel != packNetServerSettings.WorkflowTrackingLevel)
                publisher.Publish(new Message
                {
                    MessageType = PackNetServerSettingsMessages.WorkflowTrackingLevelChanged
                });

		    return newSettings;
		}

		public DTO.Settings.PackNetServerSettings GetSettings()
		{
			var settings = packNetServerSettingsRepository.All().FirstOrDefault() ?? CreateSettings();
			return settings;
		}
	}
}
