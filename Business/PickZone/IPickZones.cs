﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Business.PickZone
{
    public interface IPickZones
    {
        IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> PickZoneInstances { get; }

        Common.Interfaces.DTO.PickZones.PickZone Create(Common.Interfaces.DTO.PickZones.PickZone pickZone);
        void Delete(Common.Interfaces.DTO.PickZones.PickZone pickZone);
        void Delete(IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> pickZones);
        Common.Interfaces.DTO.PickZones.PickZone Update(Common.Interfaces.DTO.PickZones.PickZone pickZone);
        Common.Interfaces.DTO.PickZones.PickZone Find(Guid pickZone);
        Common.Interfaces.DTO.PickZones.PickZone Find(string pickZoneAlias);

        IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> GetPickZones();

        IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> GetPickZonesByStatus(PickZoneStatuses status);

        void SetPickZoneStatus(Guid id, PickZoneStatuses status);
    }
}
