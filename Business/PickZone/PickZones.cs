﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Data.PickArea;

namespace PackNet.Business.PickZone
{
    public class PickZones : IPickZones
    {
        private readonly IPickZoneRepository repository;

        public PickZones(IPickZoneRepository repository)
        {
            this.repository = repository;
        }

        public IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> PickZoneInstances
        {
            get { return repository.All(); }
        }

        public Common.Interfaces.DTO.PickZones.PickZone Create(Common.Interfaces.DTO.PickZones.PickZone pickZone)
        {
            if (pickZone.Id != Guid.Empty)
            {
                throw new Exception("Invalid Pick Zone ID, Id is already populated");
            }

            if (Find(pickZone.Alias) != null)
            {
                throw new AlreadyPersistedException("Could not Create Pick Zone, Alias is already in use");
            }

            repository.Create(pickZone);

            return pickZone;
        }

        public void Delete(Common.Interfaces.DTO.PickZones.PickZone pickZone)
        {
            if(pickZone.NumberOfRequests > 0)
                throw new InUseException("Cannot delete Pick Zone with assigned work");
            try
            {
                repository.Delete(pickZone);
            }
            catch (Exception e)
            {
                throw new Exception("Could not delete Pick Zone(s)", e);
            }
        }

        public void Delete(IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> pickZones)
        {
            if(pickZones.Any(pz => pz.NumberOfRequests > 0))
                throw new InUseException("Cannot delete Pick Zone(s) with assigned work");
            try
            {
                repository.Delete(pickZones);
            }
            catch (Exception e)
            {
                throw new Exception("Could not delete Pick Zone(s)", e);
            }
        }

        public Common.Interfaces.DTO.PickZones.PickZone Update(Common.Interfaces.DTO.PickZones.PickZone pickZone)
        {
            if (pickZone.Id == Guid.Empty)
            {
                throw new Exception("Could not Update Pick Zone, Repository Id was Empty");
            }

            var existingPickZoneWithAlias = Find(pickZone.Alias);
            if (existingPickZoneWithAlias != null && existingPickZoneWithAlias.Id != pickZone.Id)
            {
                throw new ItemExistsException("Could not Update Pick Zone, Alias is already in use");
            }

            var existingPickZone = PickZoneInstances.SingleOrDefault(p => p.Id == pickZone.Id);
            var aliasHasChanged = (existingPickZone != null && existingPickZone.Alias != pickZone.Alias);

            if (aliasHasChanged && existingPickZone.NumberOfRequests > 0)
            {
                throw new InUseException("Cannot Update Alias for Pick Zone with assigned work");
            }

            return repository.Update(pickZone);
        }

        public Common.Interfaces.DTO.PickZones.PickZone Find(Guid Id)
        {
            return repository.Find(Id);
        }

        public Common.Interfaces.DTO.PickZones.PickZone Find(string alias)
        {
            return repository.FindByAlias(alias);
        }

        public IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> GetPickZones()
        {
            return PickZoneInstances;
        }

        public IEnumerable<Common.Interfaces.DTO.PickZones.PickZone> GetPickZonesByStatus(PickZoneStatuses status)
        {
            return PickZoneInstances.Where(pz => pz.Status == status).ToList();
        }

        public void SetPickZoneStatus(Guid id, PickZoneStatuses status)
        {
            var pickZone = PickZoneInstances.SingleOrDefault(g => g.Id == id);

            if (pickZone != null)
            {
                pickZone.Status = status;
                Update(pickZone);
            }
        }
    }
}
