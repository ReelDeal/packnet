﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using PackNet.Business.WmsIntegration.MessageProducer;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Business.WmsIntegration
{
    public class WmsSocketCommunicator : IWmsCommunicator
    {
        private readonly Thread keepAliveThread;
        private Encoding encoding;
        private bool isConnected;
        private ISocketCommunicatorSettings settings;
        private readonly ISocketWrapper socket;
        private ILogger logger;
        private IWmsMessageProducer messageProducer;
        private IAnswerResolver answerResolver;
        private object socketSemaphore = new object();

        public WmsSocketCommunicator(ISocketCommunicatorSettings settings, 
        ISocketWrapper socket, IWmsMessageProducer messageProducer, IAnswerResolver answerResolver, ILogger logger)
        {
            this.settings = settings;
            this.socket = socket;
            encoding = Encoding.GetEncoding(settings.EncodingName);
            this.messageProducer = messageProducer;
            this.answerResolver = answerResolver;
            this.logger = logger;
            keepAliveThread = new Thread(SendKeepAliveMessageLoop)
                              {
                                  IsBackground = true
                              };
        }

        ~WmsSocketCommunicator()
        {
            Dispose(false);
        }

        public event EventHandler ConnectionFailed;

        public bool Send(string message)
        {
            var timer = Stopwatch.StartNew();
            lock (socketSemaphore)
            {
                timer.Reset();
                logger.Log(LogLevel.Trace, String.Format("Took {0} Milliseconds to get access to critical section (socketSemaphore)", timer.ElapsedMilliseconds));

                var buffer = encoding.GetBytes(message);
                socket.Send(buffer);
                logger.Log(LogLevel.Trace, String.Format("Took {0} Milliseconds to send message {1}", timer.ElapsedMilliseconds, message));
                timer.Reset();
                var answer = GetResponse();
                logger.Log(LogLevel.Debug, "answer message: " + answer);
                logger.Log(LogLevel.Trace, String.Format("Took {0} Milliseconds to get response {1}", timer.ElapsedMilliseconds, answer));

                return answerResolver.IsAnswerOk(answer);
            }
        }

        public bool Connect()
        {
            isConnected = false;
            var retryCount = 0;

            while (!isConnected && retryCount < settings.NumberOfRetries)
            {
                try
                {
                    socket.Connect(settings.Host, settings.Port);
                    isConnected = true;
                    keepAliveThread.Start();
                }
                catch (SocketException ex)
                {
                    retryCount++;
                    logger.Log(LogLevel.Warning, "Exception #" + retryCount + " in WMSIntegration.WmsSocketCommunicator.ConnectToWms(). Exception: " + GetAllInnerExceptions(ex));
                }
            }

            if (isConnected == false)
            {
                OnConnectionFailed();
            }

            return isConnected;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (socket != null)
                {
                    socket.Dispose();
                }
            }
        }

        //TODO: If we send a message we don't need a keepalive for that interval. We should refactor this tcp communication for better performance
        private void SendKeepAliveMessageLoop()
        {
            var executionTime = new TimeSpan(0);
            while (isConnected)
            {
                if (settings.KeepAliveInterval - executionTime.TotalMilliseconds > 0)
                {
                    Thread.Sleep((int)(settings.KeepAliveInterval - executionTime.TotalMilliseconds));
                }

                var start = DateTime.Now;
                SendKeepAliveMessage();
                executionTime = DateTime.Now.Subtract(start);
                logger.Log(LogLevel.Trace, "Sending KPA executionTime: " + executionTime.TotalMilliseconds + " ms.");
            }
        }

        private void SendKeepAliveMessage()
        {
            try
            {
                logger.Log(LogLevel.Trace, "Sending KPA");

                var message = messageProducer.GetKeepAliveMessage();
                var sendResult = Send(message);

                if (!sendResult)
                {
                    OnConnectionFailed();
                }
            }
            catch (SocketException ex)
            {
                logger.Log(LogLevel.Warning, "Exception in WMSIntegration.WmsSocketCommunicator.SendKeepAliveMessage(). Exception: " + GetAllInnerExceptions(ex));
                OnConnectionFailed();
            }
        }
        
        private string GetResponse()
        {
            var response = new byte[255];
            var noResponse = true;
            var tryCount = 0;

            while (noResponse && tryCount < settings.NumberOfRetries)
            {
                try
                {
                    socket.Receive(response);
                    noResponse = false;
                }
                catch (SocketException ex)
                {
                    tryCount++;
                    logger.Log(LogLevel.Warning, "Exception #" + tryCount + " in WMSIntegration.WmsSocketCommunicator.GetResponse(). Exception: " + GetAllInnerExceptions(ex));
                }
            }

            if (noResponse)
            {
                logger.Log(LogLevel.Warning, "Got no response in WMSIntegration.WmsSocketCommunicator.GetResponse()");
                OnConnectionFailed();
                return string.Empty;
            }
            return encoding.GetString(response);
        }

        private string GetAllInnerExceptions(Exception ex)
        {
            var ex2 = ex;
            var errorMessage = string.Empty;
            while (ex2 != null)
            {
                errorMessage += ex2.ToString();
                ex2 = ex2.InnerException;
            }

            return errorMessage;
        }

        private void OnConnectionFailed()
        {
            isConnected = false;
            var handler = ConnectionFailed;
            if (handler != null)
            {
                handler.Invoke(this, new EventArgs());
            }
        }
    }
}
