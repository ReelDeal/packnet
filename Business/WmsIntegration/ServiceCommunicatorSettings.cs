﻿using System.Runtime.Serialization;

using PackNet.Common.ComponentModel.Configuration;
using PackNet.Common.WebServiceConsumer;

namespace PackNet.Business.WmsIntegration
{
    [DataContract(Name = "ServiceCommunicatorSettings", Namespace = "http://Packsize.MachineManager.com")]
    public class ServiceCommunicatorSettings : XmlConfiguration<ServiceCommunicatorSettings>, IServiceCommunicatorSettings
    {
        public ServiceCommunicatorSettings()
        {
            Url = "http://localhost:80/EventNotifcationService.asmx";
        }

        [DataMember(Name = "Url")]
        public string Url { get; set; }

        public IEventNotificationConsumer EventNotificationConsumer { get; set; }
    }
}
