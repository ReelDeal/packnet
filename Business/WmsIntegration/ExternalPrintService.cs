﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Business.Machines;
using PackNet.Business.Orders;
using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Printing;

namespace PackNet.Business.WmsIntegration
{
    public class ExternalPrintService : IWmsNotification
    {
        private readonly IPrintService printService;
        private readonly IMachinesManager machinesManager;
        private readonly ILogger logger;
        private readonly IEventAggregatorPublisher publisher;

        public event EventHandler ConnectionFailed;

        public ExternalPrintService(IPrintService printService, IMachinesManager machinesManager, ILogger logger, IEventAggregatorPublisher publisher)
        {
            this.printService = printService;
            this.machinesManager = machinesManager;
            this.logger = logger;
            this.publisher = publisher;
        }

        /// <summary>
        /// Print labels using Packsize external print service
        /// </summary>
        /// <param name="jobManager">Carton request job manager is notified of print service events</param>
        /// <param name="order">Order to print labels</param>
        public void PrintLabelsFor(ICartonRequestJobManager jobManager, ICartonRequestOrder order)
        {
            var printId = Guid.Empty;
            var isFusionMachine = machinesManager.IsFusionMachine(order.MachineId);
           
            // Wire up to events from print service
            printService.PrintRequestStatusChangedObservable
                //.Where(p => p.UniqueID == printId)
                .Subscribe(p =>
                {
                    // if this is an EM machine we should not do any notificatoins because label sync is all done in PS
                    if (p.UniqueID != printId || !isFusionMachine) return;

                    if (p.RequestStatus == PrintRequestStatuses.Error)
                    {
                        RaiseLabelFailed(order);
                    }
                    else if (p.RequestStatus == PrintRequestStatuses.Started)
                    {
                        // todo: This may be unnecessary logic, validate when fully implement PrintService
                        jobManager.HandlePrinterQueueFull(order.MachineId);
                    }
                    else if (p.RequestStatus == PrintRequestStatuses.Complete)
                    {
                        // Notify job manager that queue empty
                        jobManager.HandlePrinterQueueEmpty(order.MachineId);
                        logger.Log(LogLevel.Debug, string.Format("HandlePrinterQueueEmpty | Label queue empty and label peeled off for printer: {0} MachineId: {1}.",
                                order.Printer.IpOrDnsNameAndPort, order.MachineId));
                    }
                });
            
            // Create printables and send to print service
            var printables = new List<Printable>();
            order.Requests.ForEach(
                r => printables.Add(new Printable(r.AsDictionary())));
            printId = printService.Print(new Dictionary<Guid, List<Printable>> { { order.Printer.Id, printables } });

            // Notify job manager that all labels requested
            var requests = order.Requests.ToList();
            requests.ForEach(x => jobManager.HandleLabelRequested(order, isFusionMachine));
        }

        private void RaiseLabelFailed(ICartonRequestOrder order)
        {
            var errorMessage = string.Format("Print failed for carton request SerialNo:{0} on printer {1}", order.SerialNo,
                order.Printer.IpOrDnsNameAndPort);
            var message = new Message<Tuple<Guid, string, int>>
            {
                MessageType = MachineMessages.LabelRequestFailed,
                Data = new Tuple<Guid, string, int>(order.MachineId, errorMessage, Zebra.Constants.PrinterErrorPosition)
            };
            publisher.Publish(message);
        }

        public void ConnectToWMS()
        {
            //todo: are we online and working????
            var handler = ConnectionFailed;
            if (handler != null)
            {
                handler.Invoke(this, new EventArgs());
            }
            throw new Exception("don't call this method!");
        }

        public void ReportProduced(ICartonRequestOrder order)
        {
            // maybe log but we do not need to inform an external system that the job is complete
            // throw new NotImplementedException();
        }
    }
}
