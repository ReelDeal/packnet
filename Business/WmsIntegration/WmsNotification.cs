﻿using System;
using System.Linq;

using PackNet.Business.Machines;
using PackNet.Business.Orders;
using PackNet.Business.WmsIntegration.MessageProducer;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Printing;
using PackNet.Data.WMS;

namespace PackNet.Business.WmsIntegration
{
    public class WmsNotification : IWmsNotification
    {
        private readonly IMachinesManager machinesManager;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IWmsCommunicator communicator;
        private readonly IWmsMessageProducer messageProducer;
        private readonly ILogger logger;
        private readonly bool externalPrint;
        private readonly bool reportProduced;
        private readonly bool enabled;
        private bool isConnected;

        public WmsNotification(IWmsCommunicator communicator, IWmsMessageProducer messageProducer, ILogger logger, IMachinesManager machinesManager, WmsConfiguration config, IEventAggregatorPublisher publisher)
        {
            this.enabled = config.Enabled;
            this.reportProduced = config.ReportProduced;
            this.externalPrint = config.ExternalPrint;
            this.machinesManager = machinesManager;
            this.publisher = publisher;
            this.communicator = communicator;
            this.messageProducer = messageProducer;
            this.logger = logger;
            this.communicator.ConnectionFailed += OnWmsCommunicatorConnectionFailed;
        }

        public event EventHandler ConnectionFailed;

        public void PrintLabelsFor(ICartonRequestJobManager jobManager, ICartonRequestOrder order)
        {
            logger.Log(
                LogLevel.Trace,
                string.Format("WMSNotification.PrintLabelsFor orderId:{0} requestCount:{1}", order.Id, order.Requests.Count()));

            if (externalPrint == false)
            {
                return;
            }

            if (ShouldReport() == false)
            {
                return;
            }

            if (order.Printer == null)
            {
                return;
            }

            logger.Log(LogLevel.Trace, string.Format("WMSNotification.PrintLabelsFor Printing"));
            
            var invalidateTheRest = false;
            foreach (var request in order.Requests)
            {
                if (invalidateTheRest)
                {
                    InvalidateRequest(order, request);
                    continue;
                }

                logger.Log(
                    LogLevel.Info, 
                    string.Format("PrintLabelsFor | Will send label request for MachineId: {0} order: {1} request count: {2} printer: {3} ", order.MachineId, order.SerialNo, order.Requests.Count(), order.Printer.IpOrDnsNameAndPort));
                var message = messageProducer.GetLabelPrintMessage(order.Printer.Alias, request);
                logger.Log(
                    LogLevel.Info, 
                    string.Format("PrintLabelsFor | MachineId: {0} order: {1} printer: {2} sending RLP message: {3}", order.MachineId, order.SerialNo, order.Printer.IpOrDnsNameAndPort, message));
                
                var messageSentOk = communicator.Send(message);
                logger.Log(
                    LogLevel.Info,
                    string.Format("PrintLabelsFor | MachineId: {0} order: {1} printer: {2} RLP message sent successfully: {3}", order.MachineId, order.SerialNo, order.Printer.IpOrDnsNameAndPort, messageSentOk));
                
                if (messageSentOk == false)
                {
                    InvalidateRequest(order, request);
                    invalidateTheRest = true;
                }
                else
                {
                    var isFusionMachine = machinesManager.IsFusionMachine(order.MachineId);
                    jobManager.HandleLabelRequested(order, isFusionMachine);
                }
            }
        }

        public void ConnectToWMS()
        {
            if (enabled)
            {
                logger.Log(LogLevel.Debug, "Connecting to WMS.");

                // if Connect fails then we are wired up to the connectionFailed event in the ctor.
                isConnected = communicator.Connect();

                if (isConnected)
                {
                    logger.Log(LogLevel.Debug, "Connected");
                }
            }
        }

        public void ReportProduced(ICartonRequestOrder order)
        {
            if (reportProduced == false)
            {
                return;
            }

            if (ShouldReport() == false)
            {
                return;
            }

            if (GetMachineForOrder(order) == null)
            {
                return;
            }

            var invalidateTheRest = false;
            foreach (var request in order.Requests)
            {
                if (invalidateTheRest)
                {
                    InvalidateRequest(order, request);
                    continue;
                }

                LogDebug("Will send Produced report for request SerialNo: " + request.SerialNumber);
                var message = messageProducer.GetCartonProducedMessage(request);
                LogDebug("Sending report produced message: " + message);
                var messageSentOk = communicator.Send(message);

                LogDebug("Message sent was successful : " + messageSentOk);
                if (messageSentOk == false)
                {
                    InvalidateRequest(order, request);
                    invalidateTheRest = true;
                }
            }
        }

        private IPacksizeCutCreaseMachine GetMachineForOrder(ICartonRequestOrder order)
        {
            var machine = machinesManager.Machines.SingleOrDefault(m => order.RunsOnMachine(m.Id));
            return machine;
        }

        private bool ShouldReport()
        {
            if (enabled == false)
            {
                return false;
            }

            if (isConnected == false)
            {
                return false;
            }

            return true;
        }

        private void InvalidateRequest(ICartonRequestOrder order, ICarton carton)
        {
            var errorMessage = string.Format("Connection to WMS failed for carton request SerialNo:{0}", carton.SerialNumber);
            var message = new Message<Tuple<Guid, string, int>>
            {
                MessageType = MachineMessages.LabelRequestFailed,
                Data = new Tuple<Guid, string, int>(order.MachineId, errorMessage, Zebra.Constants.PrinterErrorPosition)
            };
            publisher.Publish(message);
        }

        private void OnWmsCommunicatorConnectionFailed(object sender, EventArgs e)
        {
            OnConnectionFailed();
        }

        private void OnConnectionFailed()
        {
            isConnected = false;
            var handler = ConnectionFailed;
            if (handler != null)
            {
                handler.Invoke(this, new EventArgs());
            }
        }

        private void LogDebug(string message)
        {
            logger.Log(LogLevel.Debug, message);
        }
    }
}
