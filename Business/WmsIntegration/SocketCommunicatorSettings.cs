﻿using System.Net.Sockets;
using System.Runtime.Serialization;

using PackNet.Common.ComponentModel.Configuration;

namespace PackNet.Business.WmsIntegration
{
    [DataContract(Name = "SocketCommunicatorSettings", Namespace = "http://Packsize.MachineManager.com")]
    public class SocketCommunicatorSettings : XmlConfiguration<SocketCommunicatorSettings>, ISocketCommunicatorSettings
    {
        public SocketCommunicatorSettings()
        {
            AddressFamily = AddressFamily.InterNetwork;
            SocketType = SocketType.Unknown;
            ProtocolType = ProtocolType.Tcp;
            ReceiveTimeout = 10000;
            Host = "localhost";
            Port = 9090;
            NumberOfRetries = 3;
            KeepAliveInterval = 5000;
            EncodingName = "UTF-8";
        }

        [DataMember(Name = "AddressFamily")]
        public AddressFamily AddressFamily { get; set; }

        [DataMember(Name = "SocketType")]
        public SocketType SocketType { get; set; }

        [DataMember(Name = "ProtocolType")]
        public ProtocolType ProtocolType { get; set; }

        [DataMember(Name = "Host")]
        public string Host { get; set; }

        [DataMember(Name = "Port")]
        public int Port { get; set; }

        [DataMember(Name = "KeepAliveInterval")]
        public double KeepAliveInterval { get; set; }

        [DataMember(Name = "EncodingName")]
        public string EncodingName { get; set; }

        [DataMember(Name = "ReceiveTimeout")]
        public int ReceiveTimeout { get; set; }

        [DataMember(Name = "NumberOfRetries")]
        public int NumberOfRetries { get; set; }
    }
}
