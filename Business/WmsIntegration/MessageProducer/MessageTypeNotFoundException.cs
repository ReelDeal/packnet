﻿using System;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public class MessageTypeNotFoundException : Exception
    {
        public MessageTypeNotFoundException(string message)
            : base(message)
        {
        }
    }
}
