﻿using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Utils;
using PackNet.Data.WMS;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public class TagResolver : ITagResolver
    {
        private int sequenceNumber;
        private int sequenceNumberLowerBound;
        private int sequenceNumberUpperBound;

        public TagResolver(WmsConfiguration config)
            : this(config.SequenceNumberLowerBound, config.SequenceNumberUpperBound)
        {}

        public TagResolver(int sequenceNumberLowerBound, int sequenceNumberUpperBound)
        {
            sequenceNumber = sequenceNumberLowerBound - 1;
            this.sequenceNumberLowerBound = sequenceNumberLowerBound;
            this.sequenceNumberUpperBound = sequenceNumberUpperBound;
        }

        public string ResolveTag(string tag, ICarton request, string printerId)
        {
            if (tag == "[PrinterId]")
            {
                return printerId;
            }
            if (tag == "[SequenceNumber]")
            {
                return GetNextSequenceNumber();
            }
            return GetPropertyValue(request, tag.Substring(1, tag.Length - 2));
        }

        private string GetPropertyValue(ICarton request, string propertyName)
        {
            string tag = string.Empty;
            if (ObjectModifier.TryGetValue(request, propertyName, ref tag))
            {
                return tag;
            }
            throw new TagNotFoundException(propertyName);
        }

        private string GetNextSequenceNumber()
        {
            sequenceNumber++;
            int nextSequenceNumber = sequenceNumber;

            if (sequenceNumber == sequenceNumberUpperBound)
            {
                sequenceNumber = sequenceNumberLowerBound - 1;
            }

            return nextSequenceNumber.ToString().PadLeft(5, '0');
        }
    }
}
