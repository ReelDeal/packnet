﻿using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public interface IWmsMessageProducer
    {
        string GetCartonProducedMessage(ICarton request);

        string GetLabelPrintMessage(string printerId, ICarton request);

        string GetKeepAliveMessage();
    }
}
