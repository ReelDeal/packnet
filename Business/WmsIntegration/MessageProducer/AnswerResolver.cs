﻿using System.Text.RegularExpressions;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public class AnswerResolver : IAnswerResolver
    {
        private IWmsMessageProducerConfiguration config;

        public AnswerResolver(IWmsMessageProducerConfiguration config)
        {
            this.config = config;
        }

        public bool IsAnswerOk(string answer)
        {
            Regex ack = new Regex(config.GetAnswerRegEx("ACK"));
            Regex nak = new Regex(config.GetAnswerRegEx("NAK"));

            if (ack.IsMatch(answer))
            {
                return true;
            }
            if (nak.IsMatch(answer))
            {
                return false;
            }
            throw new InvalidAnswerException(answer);
        }
    }
}
