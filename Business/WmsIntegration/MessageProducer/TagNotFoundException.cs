﻿using System;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public class TagNotFoundException : Exception
    {
        public TagNotFoundException(string message)
            : base(message)
        {
        }
    }
}
