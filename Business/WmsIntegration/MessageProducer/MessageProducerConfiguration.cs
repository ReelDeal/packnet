﻿using System.Collections.Generic;
using System.Xml;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public class MessageProducerConfiguration : IWmsMessageProducerConfiguration
    {
        private XmlDocument doc;
        protected Dictionary<string, string> Messages;
        protected Dictionary<string, string> Answers;

        /// <summary>
        /// Use for testing only
        /// </summary>
        protected MessageProducerConfiguration(){ }

        public MessageProducerConfiguration(string fileName)
        {
            LoadXmlDocumentFromFile(fileName);
            PopulateMessageDictionary();
            PopulateAnswersDictionary();
        }

        public string GetMessageFormat(string messageType)
        {
            if (Messages.ContainsKey(messageType))
            {
                return Messages[messageType];
            }
            throw new MessageTypeNotFoundException(messageType);
        }

        public string GetAnswerRegEx(string answerType)
        {
            if (Answers.ContainsKey(answerType))
            {
                return Answers[answerType];
            }
            throw new MessageTypeNotFoundException(answerType);
        }

        private void LoadXmlDocumentFromFile(string fileName)
        {
            doc = new XmlDocument();
            doc.Load(fileName);
        }

        private void PopulateMessageDictionary()
        {
            Messages = new Dictionary<string, string>();

            foreach (XmlNode node in doc.SelectNodes("configuration/messages/message"))
            {
                string messageType = node.Attributes["type"].Value;
                string format = doc.SelectSingleNode("configuration/messages/message[@type='" + messageType + "']/format").InnerText;
                Messages.Add(messageType, format);
            }
        }

        private void PopulateAnswersDictionary()
        {
            Answers = new Dictionary<string, string>();

            foreach (XmlNode node in doc.SelectNodes("configuration/answers/answer"))
            {
                string answerType = node.Attributes["type"].Value;
                string format = doc.SelectSingleNode("configuration/answers/answer[@type='" + answerType + "']/regex").InnerText;
                Answers.Add(answerType, format);
            }
        }
    }
}
