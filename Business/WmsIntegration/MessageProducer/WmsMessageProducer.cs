﻿using System;

using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public class WmsMessageProducer : IWmsMessageProducer
    {
        private IWmsMessageProducerConfiguration config;
        private ITagResolver resolver;
        public WmsMessageProducer(ITagResolver resolver, IWmsMessageProducerConfiguration config)
        {
            this.resolver = resolver;
            this.config = config;
        }

        public string GetCartonProducedMessage(ICarton request)
        {            
            string format = config.GetMessageFormat("CartonProducedMessage");
            return ReplaceTags(format, request);            
        }

        public string GetLabelPrintMessage(string printerId, ICarton request)
        {
            string format = config.GetMessageFormat("LabelPrintMessage");
            return ReplaceTags(format, request, printerId);            
        }

        public string GetKeepAliveMessage()
        {
            string format = config.GetMessageFormat("KeepAliveMessage");
            return ReplaceTags(format);
        }

        private string ReplaceTags(string format, ICarton request = null, string printerId = "")
        {
            int start = format.IndexOf('[');
            int end = format.IndexOf(']');

            if (start != -1 && end != -1)
            {
                string tag = format.Substring(start, end - (start - 1));
                string value = resolver.ResolveTag(tag, request, printerId);
                value = ParseValue(tag, value);
                format = format.Replace(tag, value);

                return ReplaceTags(format, request, printerId);
            }
            
            return format;
        }

        private string ParseValue(string tag, string value)
        {
            throw new NotImplementedException("WMS message should be a service");
            //string field = tag.Substring(1, tag.Length - 2);
            //return Parser.Parse(field, value, this.valueMapper);
        }
    }
}
