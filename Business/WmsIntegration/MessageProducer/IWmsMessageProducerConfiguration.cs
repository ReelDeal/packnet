﻿namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public interface IWmsMessageProducerConfiguration
    {
        string GetMessageFormat(string messageType);

        string GetAnswerRegEx(string answerType);
    }
}
