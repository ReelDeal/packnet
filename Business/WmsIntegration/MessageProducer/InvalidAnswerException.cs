﻿using System;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public class InvalidAnswerException : Exception
    {
        public InvalidAnswerException(string message)
            : base(message)
        {
        }
    }
}
