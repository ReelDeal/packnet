﻿using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public interface ITagResolver
    {
        string ResolveTag(string tag, ICarton req, string printerId);
    }
}
