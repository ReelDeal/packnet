﻿namespace PackNet.Business.WmsIntegration.MessageProducer
{
    public interface IAnswerResolver
    {
        bool IsAnswerOk(string answer);
    }
}
