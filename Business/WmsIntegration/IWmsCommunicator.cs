﻿using System;

namespace PackNet.Business.WmsIntegration
{
    public interface IWmsCommunicator : IDisposable
    {
        event EventHandler ConnectionFailed;

        bool Send(string message);

        bool Connect();
    }
}
