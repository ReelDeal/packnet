﻿using System.Net.Sockets;

namespace PackNet.Business.WmsIntegration
{
    public interface ISocketCommunicatorSettings
    {
        AddressFamily AddressFamily { get; }

        SocketType SocketType { get; }

        ProtocolType ProtocolType { get; }

        int ReceiveTimeout { get; }

        string Host { get; }

        int Port { get; }

        int NumberOfRetries { get; }

        double KeepAliveInterval { get; }

        string EncodingName { get; }
    }
}
