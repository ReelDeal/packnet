﻿using System;

using PackNet.Business.Orders;
using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Business.WmsIntegration
{
    public interface IWmsNotification
    {
        event EventHandler ConnectionFailed;

        void PrintLabelsFor(ICartonRequestJobManager jobManager, ICartonRequestOrder order);

        void ConnectToWMS();

        void ReportProduced(ICartonRequestOrder order);
    }
}
