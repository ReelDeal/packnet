﻿using PackNet.Common.WebServiceConsumer;

namespace PackNet.Business.WmsIntegration
{
    public interface IServiceCommunicatorSettings
    {
        string Url { get; }

        IEventNotificationConsumer EventNotificationConsumer { get; set; }
    }
}
