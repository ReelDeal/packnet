﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

using PackNet.Common.Interfaces.Logging;

namespace PackNet.Business.WmsIntegration
{
    public class ServiceCommunicator : IWmsCommunicator
    {
        private readonly IServiceCommunicatorSettings settings;
        private readonly ILogger logger;
        private bool isConnected;

        public ServiceCommunicator(IServiceCommunicatorSettings settings, ILogger logger)
        {
            this.settings = settings;
            this.logger = logger;
        }

        public event EventHandler ConnectionFailed;

        public bool Connect()
        {
            isConnected = settings.EventNotificationConsumer != null;

            if (isConnected == false)
            {
                OnConnectionFailed();
            }

            return isConnected;
        }

        public bool Send(string message)
        {
            if (isConnected)
            {
                try
                {
                    IEnumerable<XmlNode> reply = settings.EventNotificationConsumer.SendEventNotification(CreateRequest(message));

                    if (reply.ElementAt(0).HasChildNodes)
                    {
                        return reply.ElementAt(0).FirstChild.InnerText == message;
                    }
                }
                catch (Exception ex)
                {
                    LogDebug("Exception in ServiceCommunicator. Exception: " + GetAllInnerExceptions(ex));
                    OnConnectionFailed();
                }
            }

            return false;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        private XmlNode[] CreateRequest(string message)
        {
            var doc = new XmlDocument();
            doc.LoadXml(string.Format("<SendEventNotification><message>{0}</message></SendEventNotification>", message));            
            return CreateNodeList(doc);
        }

        private XmlNode[] CreateNodeList(XmlDocument doc)
        {
            return doc.SelectNodes("/").Cast<XmlNode>().ToArray();
        }

        private void LogDebug(string message)
        {
            logger.Log(LogLevel.Debug, message);
        }

        private string GetAllInnerExceptions(Exception ex)
        {
            Exception ex2 = ex;
            string errorMessage = string.Empty;
            while (ex2 != null)
            {
                errorMessage += ex2.ToString();
                ex2 = ex2.InnerException;
            }

            return errorMessage;
        }

        private void OnConnectionFailed()
        {
            isConnected = false;
            var handler = ConnectionFailed;
            if (handler != null)
            {
                handler.Invoke(this, new EventArgs());
            }
        }
    }
}
