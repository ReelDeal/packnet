﻿namespace PackNet.Business.ArticleManagement
{
    using System;
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public interface IArticles
    {
        Article Create(Article article);
        Article Update(Article article);
        void Delete(Article article);
        void Delete(Guid id);
        Article Find(Guid id);
        Article Find(string articleId);
        IEnumerable<Article> All();
    }
}