﻿using System;
using System.Linq;

namespace PackNet.Business.ArticleManagement
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;
    using Common.Interfaces.Exceptions;
    using Data.ArticleManagement;

    public class Articles : IArticles
    {
        private readonly IArticleRepository repository;

        public Articles(IArticleRepository repository)
        {
            this.repository = repository;
        }

        public Article GetByName(string name)
        {
            var allArticles = repository.All();
            var article = allArticles.FirstOrDefault(a => a.ArticleId == name);
            if(article == null)
                throw new Exception("Article doesn't exist");
            return article;
        }

        public Article Create(Article article)
        {
            if (article.Id != Guid.Empty)
                throw new Exception("Invalid article Id, Id is already populated");

            if (Find(article.ArticleId) != null)
                throw new ItemExistsException(String.Format("Invalid article, article '{0}' already exists", article.ArticleId));

            repository.Create(article);
            return article;
        }

        public Article Update(Article article)
        {
            var existing = Find(article.ArticleId);
            if (existing.Id != article.Id)
                article.Id = existing.Id;

            return repository.Update(article);
        }

        public void Delete(Article article)
        {
            repository.Delete(article);
        }

        public void Delete(Guid articleId)
        {
            repository.Delete(articleId);
        }

        public Article Find(Guid id)
        {
            return repository.Find(id);
        }

        public Article Find(string articleId)
        {
            return repository.Find(articleId);
        }

        public IEnumerable<Article> All()
        {
            return repository.All();
        }
    }
}
