﻿using System.Linq;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.Business.ExtensionMethods
{
    using Common.Interfaces.DTO.Carton;
    using Common.Interfaces.DTO.CartonPropertyGroups;

    //TODO: this needs to be a restriction resolver
    public static class CartonPropertyGroupExtensions
    {
        public static bool IsCartonRequestInGroup(this CartonPropertyGroup cpg, ICarton request)
        {
            return request.Restrictions
                .OfType<BasicRestriction<CartonPropertyGroup>>()
                .Any(r => r.Value.Alias == cpg.Alias);
        }

        public static bool ProducibleIsAssignedToCpg(this CartonPropertyGroup cpg, IProducible producible)
        {
            var cpgForProducible = producible.Restrictions.OfType<BasicRestriction<CartonPropertyGroup>>().FirstOrDefault();
            if (cpgForProducible == null)
                return false;
            return cpgForProducible.Value.Alias == cpg.Alias;
        }
    }
}
