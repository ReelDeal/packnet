﻿using System.Collections.Generic;
using System.Linq;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public class LineCompensator
    {
        private readonly IList<PhysicalLine> lines;
        private readonly IPhysicalLineModifier axis;

        public LineCompensator(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis)
        {
            this.lines = lines.ToList();
            this.axis = axis;
        }

        public IEnumerable<PhysicalLine> Compensate(MicroMeter cutCompensation, MicroMeter minimumLineLength)
        {
            for (var i = 0; i < lines.Count(); i++)
            {
                var line = lines.ElementAt(i);
                if (line.Type != LineType.cut)
                {
                    continue;
                }

                var startPos = axis.GetStartPos(line);
                var endPos = axis.GetEndPos(line);
                var length = endPos - startPos;

                if (length > cutCompensation * 2)
                {
                    axis.SetStartPos(line, startPos + cutCompensation);
                    axis.SetEndPos(line, endPos - cutCompensation);
                    LengthenAttachedCreaseLines(i, cutCompensation);
                }
                else
                {
                    var adjustment = (endPos - startPos - minimumLineLength) / 2;
                    axis.SetStartPos(line, startPos + adjustment);
                    axis.SetEndPos(line, endPos - adjustment);
                    LengthenAttachedCreaseLines(i, adjustment);
                }
            }

            return lines;
        }

        public IEnumerable<PhysicalLine> AddWasteCuttingToDesign(MicroMeter cutCompensation, MicroMeter corrugateWidth, MicroMeter designWidth, MicroMeter trackStartingPosition, bool rightSideFixed)
        {
            for (var index = 0; index < lines.Count; index++)
            {
                var line = lines[index];

                switch (line.Type)
                {
                    case LineType.cut:
                        ExtendCutLineIfOnDesignEdge(cutCompensation, corrugateWidth, designWidth, trackStartingPosition, rightSideFixed, line);
                        break;
                    case LineType.crease:
                        if (AddCutLineToCreaseLineOnDesignEdge(corrugateWidth,
                            designWidth,
                            trackStartingPosition,
                            rightSideFixed,
                            line,
                            index))
                        {
                            index++;
                        }

                        break;
                }
            }

            return lines;
        }

        private bool AddCutLineToCreaseLineOnDesignEdge(
            MicroMeter corrugateWidth,
            MicroMeter designWidth,
            MicroMeter trackStartingPosition,
            bool rightSideFixed,
            PhysicalLine line,
            int indexOfCurrentLine)
        {
            var startPos = axis.GetStartPos(line);
            var endPos = axis.GetEndPos(line);

            if (rightSideFixed)
            {
                if (startPos != trackStartingPosition + corrugateWidth - designWidth)
                {
                    return false;
                }
                if (trackStartingPosition == startPos)
                {
                    return false;
                }

                var newLine = new PhysicalLine(new PhysicalCoordinate(trackStartingPosition, line.StartCoordinate.Y),
                    new PhysicalCoordinate(startPos, line.EndCoordinate.Y), LineType.cut);

                lines.Insert(indexOfCurrentLine, newLine);
                return true;
            }
            else
            {
                if (endPos != (designWidth + trackStartingPosition))

                {
                    return false;
                }

                var newLine = new PhysicalLine(new PhysicalCoordinate(endPos, line.StartCoordinate.Y), 
                    new PhysicalCoordinate(trackStartingPosition + corrugateWidth, line.StartCoordinate.Y), LineType.cut);

                lines.Insert(indexOfCurrentLine + 1, newLine);

                return true;
            }
        }

        private void ExtendCutLineIfOnDesignEdge(
            MicroMeter cutCompensation,
            MicroMeter corrugateWidth,
            MicroMeter designWidth,
            MicroMeter trackStartingPosition,
            bool rightSideFixed,
            PhysicalLine line)
        {
            var startPos = axis.GetStartPos(line);
            var endPos = axis.GetEndPos(line);

            if (startPos == endPos)
                return;

            if (rightSideFixed)
            {
                if (startPos == trackStartingPosition + corrugateWidth + cutCompensation - designWidth)
                {
                    axis.SetStartPos(line, trackStartingPosition);
                }

                if (endPos == (trackStartingPosition + corrugateWidth - cutCompensation))
                {
                    axis.SetEndPos(line, trackStartingPosition + corrugateWidth);
                }
            }
            else
            {
                if (startPos == cutCompensation + trackStartingPosition)
                {
                    axis.SetStartPos(line, trackStartingPosition);
                }

                if (endPos == (designWidth + trackStartingPosition) - cutCompensation)
                {
                    axis.SetEndPos(line, corrugateWidth + trackStartingPosition);
                }
            }
        }

        private void LengthenAttachedCreaseLines(int lineIndex, MicroMeter cutCompensation)
        {
            if (IsPreviousLineAttached(lineIndex, cutCompensation))
            {
                ModifyEndPosIfLineIsCreaseLine(lines.ElementAt(lineIndex - 1), cutCompensation);
            }

            if (IsNextLineAttached(lineIndex, cutCompensation))
            {
                ModifyStartPosIfLineIsCreaseLine(lines.ElementAt(lineIndex + 1), cutCompensation);
            }            
        }

        private bool IsPreviousLineAttached(int lineIndex, MicroMeter cutCompensation)
        {
            if (lineIndex <= 0)
            {
                return false;
            }

            var line = lines.ElementAt(lineIndex);
            var previousLine = lines.ElementAt(lineIndex - 1);

            return (axis.GetEndPos(previousLine) + cutCompensation) == axis.GetStartPos(line);
        }

        private void ModifyEndPosIfLineIsCreaseLine(PhysicalLine line, MicroMeter cutCompensation)
        {
            if (line.Type != LineType.crease)
            {
                return;
            }

            var endPos = axis.GetEndPos(line);
            axis.SetEndPos(line, endPos + cutCompensation);
        }

        private bool IsNextLineAttached(int lineIndex, MicroMeter cutCompensation)
        {
            if (lineIndex >= lines.Count() - 1)
            {
                return false;
            }

            var line = lines.ElementAt(lineIndex);
            var nextLine = lines.ElementAt(lineIndex + 1);

            return (axis.GetStartPos(nextLine) - cutCompensation) == axis.GetEndPos(line);
        }

        private void ModifyStartPosIfLineIsCreaseLine(PhysicalLine line, MicroMeter cutCompensation)
        {
            if (line.Type != LineType.crease)
            {
                return;
            }

            var startPos = axis.GetStartPos(line);
            axis.SetStartPos(line, startPos - cutCompensation);
        }
    }
}
