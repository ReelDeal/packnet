﻿using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public class LineOffsetter
    {
        private readonly IEnumerable<PhysicalLine> lines;
        private readonly IPhysicalLineModifier axis;

        public LineOffsetter(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis)
        {
            this.lines = lines;
            this.axis = axis;
        }

        public IEnumerable<PhysicalLine> AdjustOffset(MicroMeter offset)
        {
            foreach (var line in lines)
            {
                axis.SetStartPos(line, axis.GetStartPos(line) + offset);
                axis.SetEndPos(line, axis.GetEndPos(line) + offset);
            }

            return lines;
        }
    }
}
