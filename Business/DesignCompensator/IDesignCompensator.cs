﻿using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public interface IDesignCompensator
    {
        RuleAppliedPhysicalDesign Compensate(CompensationParameters compensationParameters);
    }
}
