﻿using System.Collections.Generic;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public class EmDesignCompensator : IDesignCompensator
    {
        public RuleAppliedPhysicalDesign Compensate(CompensationParameters compensationParameters)
        {
            var compensatedLines = compensationParameters.PhysicalLines;
            var compensatedDesign = new RuleAppliedPhysicalDesign(compensationParameters.DesignToCompensate, compensationParameters.DesignToCompensate.WasteAreas,  false);

            CompensateHorizontalLines(compensatedLines, compensationParameters).ForEach(compensatedDesign.Add);
            CompensateVerticalLines(compensatedLines, compensationParameters).ForEach(compensatedDesign.Add);
            compensatedDesign.Add(compensationParameters.CreateCutOffLine());
            compensationParameters.DesignToCompensate.WasteAreas.ForEach(compensatedDesign.AddWasteArea);

            return compensatedDesign;
        }

        private IEnumerable<PhysicalLine> CompensateHorizontalLines(IEnumerable<PhysicalLine> lines, CompensationParameters parameters)
        {
            return new EmLineCompensator(lines.GetHorizontalLines(), new PhysicalHorizontalLineModifier(), parameters.TrackOffset,
                parameters.TrackOffset + parameters.DesignWidth).CompensateHorizontalLines(parameters);
        }

        private IEnumerable<PhysicalLine> CompensateVerticalLines(IEnumerable<PhysicalLine> lines, CompensationParameters parameters)
        {
            return new EmLineCompensator(lines.GetVerticalLines(), new PhysicalVerticalLineModifier(), 0, parameters.DesignLength).CompensateVerticalLines(parameters);
        }
    }
}
