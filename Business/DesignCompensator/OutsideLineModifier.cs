﻿using System.Collections.Generic;
using System.Linq;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public class OutsideLineModifier
    {
        private List<PhysicalLine> lines;
        private IPhysicalLineModifier axis;
        private MicroMeter maxPos;

        public OutsideLineModifier(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis, MicroMeter maxPos)
        {
            this.lines = lines.ToList();
            this.axis = axis;
            this.maxPos = maxPos;
        }

        public IEnumerable<PhysicalLine> ModifyLines()
        {
            var modifiedLines = new List<PhysicalLine>();

            foreach (var line in lines)
            {
                if (axis.GetStartPos(line) < 0)
                {
                    axis.SetStartPos(line, 0);
                }

                if (axis.GetEndPos(line) > maxPos)
                {
                    axis.SetEndPos(line, maxPos);
                }

                if (axis.GetStartPos(line) != axis.GetEndPos(line))
        {
                    modifiedLines.Add(line);
                }
            }

            return modifiedLines;
        }
    }
}
