﻿using System.Collections.Generic;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public class DesignCompensator : IDesignCompensator
    {
        public RuleAppliedPhysicalDesign Compensate(CompensationParameters compensationParameters)
        {
            var compensatedLines = compensationParameters.PhysicalLines;
            var compensatedDesign = new RuleAppliedPhysicalDesign(compensationParameters.DesignToCompensate, false);

            compensatedLines = CompensateHorizontalLines(compensatedLines, compensationParameters);
            compensatedLines = ExtendHorizontalCutLinesWithStartOrEndOnDesignEdge(compensatedLines, compensationParameters);
            compensatedLines.Add(compensationParameters.CreateCutOffLine());

            compensatedLines.ForEach(compensatedDesign.Add);
            compensationParameters.DesignToCompensate.WasteAreas.ForEach(compensatedDesign.AddWasteArea);

            return compensatedDesign;
        }

        private List<PhysicalLine> CompensateHorizontalLines(IEnumerable<PhysicalLine> lines, CompensationParameters parameters)
        {
            var compensatedLines = new List<PhysicalLine>();
            var compensator = new LineCompensator(lines.GetHorizontalLines(), new PhysicalHorizontalLineModifier());
            compensatedLines.AddRange(compensator.Compensate(parameters.CrossHeadCutCompensation, parameters.MinimumLineLength));
            compensatedLines.AddRange(lines.GetVerticalLines());
            return compensatedLines;
        }

        private List<PhysicalLine> ExtendHorizontalCutLinesWithStartOrEndOnDesignEdge(IEnumerable<PhysicalLine> compensatedLines, CompensationParameters parameters)
        {
            var extendedLines = new List<PhysicalLine>();
            var compensator = new LineCompensator(compensatedLines.GetHorizontalLines(), new PhysicalHorizontalLineModifier());
            extendedLines.AddRange(compensator.AddWasteCuttingToDesign(parameters.CrossHeadCutCompensation, parameters.CorrugateWidth, parameters.DesignWidth, parameters.TrackStartingPosition, parameters.IsTrackRightSideFixed));
            extendedLines.AddRange(compensatedLines.GetVerticalLines());
            return extendedLines;
        }
    }
}
