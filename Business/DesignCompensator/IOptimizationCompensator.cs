using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public interface IOptimizationCompensator
    {
        RuleAppliedPhysicalDesign Compensate(CompensationParameters parameters);
    }
}