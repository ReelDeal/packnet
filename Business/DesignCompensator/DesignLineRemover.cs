﻿using System.Collections.Generic;

namespace PackNet.Business.DesignCompensator
{
    using PackNet.Common.Interfaces.DTO;

    public static class DesignLineRemover
    {
        public static List<PhysicalLine> RemoveLinesCloseToDesignEdge(IEnumerable<PhysicalLine> lines, MicroMeter corrugateLength, MicroMeter corrugateWidth, MicroMeter trackOffset, MicroMeter minimumDistanceCorrugateEdgeToVerticalLine)
        {
            var linesToRemove = new List<PhysicalLine>();
            linesToRemove.AddRange(lines.GetHorizontalLinesAtCoordinate(0));
            linesToRemove.AddRange(lines.GetHorizontalLinesAtCoordinate(corrugateLength));
            linesToRemove.AddRange(lines.GetVerticalLinesWithMaximumCoordinate(trackOffset + minimumDistanceCorrugateEdgeToVerticalLine));
            linesToRemove.AddRange(lines.GetVerticalLinesWithMinimumCoordinate(trackOffset + corrugateWidth - minimumDistanceCorrugateEdgeToVerticalLine));

            var compensatedLines = new List<PhysicalLine>(lines);
            compensatedLines.RemoveAll(linesToRemove.Contains);
            return compensatedLines;
        }
    }
}
