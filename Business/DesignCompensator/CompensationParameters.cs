using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;

namespace PackNet.Business.DesignCompensator
{
    public class CompensationParameters
    {
        public CompensationParameters(RuleAppliedPhysicalDesign designToCompensate, bool isTrackRightSideFixed, MicroMeter trackOffset, MicroMeter corrugateWidth, MicroMeter crossHeadCutCompensation, MicroMeter crossHeadCreaseCompensation, MicroMeter minimumLineLength, MicroMeter minimumDistanceCorrugateEdgeToVerticalLine, MicroMeter longHeadCutCompensation, MicroMeter longHeadCreaseCompensation)
        {
            IsTrackRightSideFixed = isTrackRightSideFixed;
            TrackOffset = trackOffset;
            TrackStartingPosition = IsTrackRightSideFixed
                ? trackOffset - corrugateWidth
                : trackOffset;

            DesignToCompensate = designToCompensate;
            CorrugateWidth = corrugateWidth;
            CrossHeadCutCompensation = crossHeadCutCompensation;
            CrossHeadCreaseCompensation = crossHeadCreaseCompensation;
            LongHeadCutCompensation = longHeadCutCompensation;
            LongHeadCreaseCompensation = longHeadCreaseCompensation;
            MinimumLineLength = minimumLineLength;
            MinimumDistanceCorrugateEdgeToVerticalLine = minimumDistanceCorrugateEdgeToVerticalLine;
        }

        public MicroMeter DesignLength
        {
            get { return DesignToCompensate.Length; }
        }

        public MicroMeter DesignWidth
        {
            get { return DesignToCompensate.Width; }
        }

        public List<PhysicalLine> PhysicalLines
        {
            get { return DesignToCompensate.Lines.ToList(); }
        }

        public RuleAppliedPhysicalDesign DesignToCompensate { get; private set; }

        public MicroMeter CrossHeadCutCompensation { get; set; }

        public MicroMeter CrossHeadCreaseCompensation { get; set; }

        public MicroMeter LongHeadCutCompensation { get; set; }

        public MicroMeter LongHeadCreaseCompensation { get; set; }

        public MicroMeter MinimumLineLength { get; private set; }

        public MicroMeter MinimumDistanceCorrugateEdgeToVerticalLine { get; private set; }

        public MicroMeter CorrugateWidth { get; private set; }

        public MicroMeter TrackStartingPosition
        {
            get; private set;
        }

        public MicroMeter TrackOffset
        {
            get;
            private set;
        }

        public bool IsTrackRightSideFixed
        {
            get; private set;
        }

        public MicroMeter GetAlignmentOffset()
        {
            if (IsTrackRightSideFixed == false)
            {
                return 0;
            }
            return CorrugateWidth - DesignWidth;
        }

        public PhysicalLine CreateCutOffLine()
        {
            return new PhysicalLine(new PhysicalCoordinate(TrackStartingPosition, DesignLength), new PhysicalCoordinate(CorrugateWidth + TrackStartingPosition, DesignLength), LineType.cut);
        }
    }
}