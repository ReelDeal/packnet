﻿namespace PackNet.Business.DesignCompensator
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces.DTO;

    public class EmLineCompensator
    {
        private readonly IList<PhysicalLine> lines;
        private readonly IPhysicalLineModifier axis;
        private readonly MicroMeter designStart, designEnd;

        public EmLineCompensator(IEnumerable<PhysicalLine> lines, IPhysicalLineModifier axis, MicroMeter designStart, MicroMeter designEnd)
        {
            this.lines = lines.ToList();
            this.axis = axis;
            this.designStart = designStart;
            this.designEnd = designEnd;
        }

        public IEnumerable<PhysicalLine> Compensate(MicroMeter cutCompensation, MicroMeter creaseCompensation, MicroMeter minimumLineLength)
        {
            for (var i = 0; i < lines.Count(); i++)
            {
                var line = lines.ElementAt(i);

                var startPos = axis.GetStartPos(line);
                var endPos = axis.GetEndPos(line);
                var length = endPos - startPos;
                var adjustment = (endPos - startPos - minimumLineLength) / 2;
                double compensation;

                switch (line.Type)
                {
                    case LineType.crease:
                        compensation = length > creaseCompensation * 2 
                            ? creaseCompensation 
                            : adjustment;

                        if (startPos != designStart && IsPreviousLineAttached(i, 0) == false)
                            axis.SetStartPos(line, startPos + compensation);

                        if (endPos != designEnd && IsNextLineAttached(i, 0) == false)
                            axis.SetEndPos(line, endPos - compensation);

                        break;
                    case LineType.cut:
                        compensation = length > cutCompensation * 2 
                            ? cutCompensation 
                            : adjustment;
                        if (startPos != designStart)
                            axis.SetStartPos(line, startPos + compensation);
                        if (endPos != designEnd)
                            axis.SetEndPos(line, endPos - compensation);
                        LengthenAttachedCreaseLines(i, cutCompensation, compensation);
                        break;
                }
            }

            return lines;
        }

        private void LengthenAttachedCreaseLines(int lineIndex, MicroMeter cutCompensation, MicroMeter creaseCompensation)
        {
            if (IsPreviousLineAttached(lineIndex, cutCompensation))
            {
                ModifyEndPosIfLineIsCreaseLine(lines.ElementAt(lineIndex - 1), cutCompensation);
            }

            if (IsNextLineAttached(lineIndex, cutCompensation))
            {
                ModifyStartPosIfLineIsCreaseLine(lines.ElementAt(lineIndex + 1), cutCompensation);
            }
        }

        private bool IsPreviousLineAttached(int lineIndex, MicroMeter compensation)
        {
            if (lineIndex <= 0)
            {
                return false;
            }

            var line = lines.ElementAt(lineIndex);
            var previousLine = lines.ElementAt(lineIndex - 1);

            return (axis.GetEndPos(previousLine) + compensation) == axis.GetStartPos(line);
        }

        private void ModifyEndPosIfLineIsCreaseLine(PhysicalLine line, MicroMeter cutCompensation)
        {
            if (line.Type != LineType.crease)
            {
                return;
            }

            var endPos = axis.GetEndPos(line);
            axis.SetEndPos(line, endPos + cutCompensation);
        }

        private bool IsNextLineAttached(int lineIndex, MicroMeter compensation)
        {
            if (lineIndex >= lines.Count() - 1)
            {
                return false;
            }

            var line = lines.ElementAt(lineIndex);
            var nextLine = lines.ElementAt(lineIndex + 1);

            return (axis.GetStartPos(nextLine) - compensation) == axis.GetEndPos(line) && axis.GetLineCoordinate(line) == axis.GetLineCoordinate(nextLine);
        }

        private void ModifyStartPosIfLineIsCreaseLine(PhysicalLine line, MicroMeter cutCompensation)
        {
            if (line.Type != LineType.crease)
            {
                return;
            }

            var startPos = axis.GetStartPos(line);
            axis.SetStartPos(line, startPos - cutCompensation);
        }

        public IEnumerable<PhysicalLine> CompensateHorizontalLines(CompensationParameters parameters)
        {
            return Compensate(parameters.CrossHeadCutCompensation, parameters.CrossHeadCreaseCompensation, parameters.MinimumLineLength);
        }

        public IEnumerable<PhysicalLine> CompensateVerticalLines(CompensationParameters parameters)
        {
            return Compensate(parameters.LongHeadCutCompensation, parameters.LongHeadCreaseCompensation, parameters.MinimumLineLength);
        }
    }
}
