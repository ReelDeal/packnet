﻿using System.Linq;

using PackNet.Common.Interfaces.DTO;
using System.Collections.Generic;

namespace PackNet.Business.DesignCompensator
{
    public static class WasteAreaCutLineCreator
    {
        public static IEnumerable<PhysicalLine> GetWasteCuttingLinesForWasteAreas(IEnumerable<WasteArea> areas)
        {
            return areas.SelectMany(wasteArea => wasteArea.GetCutLines());
        }
    }
}
