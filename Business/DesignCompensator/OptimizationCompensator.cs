using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.DesignCompensator
{
    public class OptimizationCompensator : IOptimizationCompensator
    {
        public RuleAppliedPhysicalDesign Compensate(CompensationParameters parameters)
        {
            var physicalLines = parameters.PhysicalLines;
            var designLength = parameters.DesignLength;
            var corrugateWidth = parameters.CorrugateWidth;
            var trackStartingPosition = parameters.TrackStartingPosition;

            var compensatedDesign = new RuleAppliedPhysicalDesign(parameters.DesignToCompensate, false);

            var compensatedLines = AdjustHorizontalOffset(physicalLines, trackStartingPosition);
            compensatedLines = AdjustHorizontalOffset(compensatedLines, parameters.GetAlignmentOffset());
            compensatedLines = DesignLineRemover.RemoveLinesCloseToDesignEdge(compensatedLines, designLength, corrugateWidth, trackStartingPosition, parameters.MinimumDistanceCorrugateEdgeToVerticalLine);
            compensatedLines.ForEach(compensatedDesign.Add);

            //TODO: include when waste separation is complete
            GetCompensatedWasteAreas(parameters, trackStartingPosition).ForEach(compensatedDesign.AddWasteArea);

            return compensatedDesign;
        }

        private static IEnumerable<WasteArea> GetCompensatedWasteAreas(CompensationParameters parameters, MicroMeter trackStartingPosition)
        {
            return parameters.DesignToCompensate.WasteAreas.Select(a => new WasteArea(
                new PhysicalCoordinate(a.TopLeft.X + trackStartingPosition + parameters.GetAlignmentOffset(), a.TopLeft.Y),
                new PhysicalCoordinate(a.BottomRight.X + trackStartingPosition + parameters.GetAlignmentOffset(),
                   a.BottomRight.Y))
                );
        }

        private List<PhysicalLine> AdjustHorizontalOffset(IEnumerable<PhysicalLine> lines, MicroMeter trackOffset)
        {
            var offsetter = new LineOffsetter(lines, new PhysicalHorizontalLineModifier());
            return offsetter.AdjustOffset(trackOffset).ToList();
        }
    }
}
