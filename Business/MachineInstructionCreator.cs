﻿using System;

using PackNet.Business.CodeGenerator;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using System.Collections.Generic;
using System.IO;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace PackNet.Business
{
    public abstract class MachineInstructionCreator<T> : IMachineInstructionCreator<T> where T: PhysicalMachineSettings
    {
        protected ICodeGenerator<T> CodeGenerator;
        private readonly object generationLock = new object();

        protected readonly ILogger Logger;

        protected MachineInstructionCreator(ILogger logger)
        {
            Logger = logger;
        }

        public void ResetCodeGenerator()
        {
            lock (generationLock)
            {
                CodeGenerator = null;
            }
        }

        public IEnumerable<IInstructionItem> GetInstructionListForJob(IPacksizeCarton order, PhysicalDesign design, Track track, T machineSettings)
        {
            lock (generationLock)
            {
                var compensatedDesign = CreateCompensatedDesign(design, order, track.TrackOffset, machineSettings);
                CreateCodeGenerator(machineSettings);
                return GenerateCode(compensatedDesign, order, track, machineSettings);
            }
        }

        public bool CanProducePackagingFor(CanProducePackagingForParameters parameters, T machineSettings, int numberOfTracks, MicroMeter maxPosition)
        {
            // Increasing the offset to make sure that we can position LHs outside of the corrugate by adding tolerance * 3
            var corrugateWidthWithTolerance = (GetCorrugateWidthTolerance(machineSettings) * 3);
            
            return numberOfTracks < 2
                ? CanProducePackagingOnSpecificTrackFor(parameters, machineSettings, parameters.Corrugate.Width + corrugateWidthWithTolerance, 1)
                : CanProducePackagingOnSpecificTrackFor(parameters, machineSettings, maxPosition - parameters.Corrugate.Width + corrugateWidthWithTolerance, 2) &&
                  CanProducePackagingOnSpecificTrackFor(parameters, machineSettings, parameters.Corrugate.Width + corrugateWidthWithTolerance, 1);
        }

        private bool CanProducePackagingOnSpecificTrackFor(CanProducePackagingForParameters parameters, T machineSettings, MicroMeter trackOffset, int trackNumber)
        {
            var order = new Carton()
            {
                TrackNumber = trackNumber,
                CartonOnCorrugate = new CartonOnCorrugate
                {
                    Corrugate = parameters.Corrugate
                }
            };
            var compensatedDesign = CreateCompensatedDesign(parameters.PackagingDesign.Clone() as PhysicalDesign, order,
                 trackOffset, machineSettings);

            CreateCodeGenerator(machineSettings);
            return ValidateLongHeadPositions(compensatedDesign, parameters.Corrugate.Width, trackOffset, machineSettings, order);
        }

        public bool IsLongHeadPositioningValidFor(IPacksizeCarton order, PhysicalDesign design, Track track, T machineSettings)
        {
            var compensatedDesign = CreateCompensatedDesign(design, order, track.TrackOffset, machineSettings);
            CreateCodeGenerator(machineSettings);
            var isValid = ValidateLongHeadPositions(compensatedDesign, order.CartonOnCorrugate.Corrugate.Width, track.TrackOffset, machineSettings, order);

            return isValid;
        }

        private bool ValidateLongHeadPositions(PhysicalDesign physicalDesign, MicroMeter corrugateWidth,
             MicroMeter trackOffset, T machineSettings, IPacksizeCarton order)
        {
            return
                CodeGenerator.ValidateLongHeadPositions(physicalDesign, machineSettings,
                    corrugateWidth, IsTrackRighSideFixed(machineSettings, order.TrackNumber), trackOffset);
        }

        protected RuleAppliedPhysicalDesign CreateCompensatedDesign(PhysicalDesign design, IPacksizeCarton order,
            MicroMeter trackOffset, T machineSettings)
        {
            var rotation = order.Rotation ?? OrientationEnum.Degree0;
            var ruleAppliedDesign = ApplyRules(design, machineSettings, order.CartonOnCorrugate.Corrugate, rotation);

            var parameters = GetCompensationParameters(ruleAppliedDesign, trackOffset, order.CartonOnCorrugate.Corrugate.Width, machineSettings, order);
            var optimizedDesign = ApplyDesignOptimzation(parameters);

            parameters = GetCompensationParameters(optimizedDesign, trackOffset, order.CartonOnCorrugate.Corrugate.Width, machineSettings, order);
            var compensatedDesign = ApplyDesignCompensation(parameters);

            return compensatedDesign;
        }

        /// <summary>
        /// Compensate lines for cut and crease radius, extends horizontal cut lines to enable waste cutting by cross head, add the cut of line for the design
        /// </summary>
        private RuleAppliedPhysicalDesign ApplyDesignOptimzation(CompensationParameters parameters)
        {
            return new OptimizationCompensator().Compensate(parameters);
        }

        private IEnumerable<InstructionItem> GenerateCode(RuleAppliedPhysicalDesign design, IPacksizeCarton order,
            Track track, T machineSettings)
        {
            var instructions = CodeGenerator.Generate(design, machineSettings, order.CartonOnCorrugate.Corrugate.Width, track, IsOnTheFly(machineSettings));

            var otfName = machineSettings.OnTheFlyActive ? "otfon" : "otfoff";
            var logPath = String.Format("CodeInstructions/{0}-{1}.txt", order.DesignId,otfName);
            var fileInfo = new FileInfo(logPath);
            if (fileInfo.Directory != null && !fileInfo.Directory.Exists)
                fileInfo.Directory.Create();
            using (var streamWriter = new StreamWriter(fileInfo.Open(FileMode.OpenOrCreate, FileAccess.Write)))
            {
                instructions.ForEach(streamWriter.WriteLine);
            }

            return instructions;
        }

        protected abstract RuleAppliedPhysicalDesign ApplyRules(PhysicalDesign measureAppliedDesign, T machineSettings, Corrugate corrugate, OrientationEnum rotation);

        protected abstract RuleAppliedPhysicalDesign ApplyDesignCompensation(CompensationParameters parameters);

        protected abstract void CreateCodeGenerator(T machineSettings);

        protected abstract bool IsTrackRighSideFixed(T machineSettings, int trackNumber);

        protected abstract MicroMeter GetCorrugateWidthTolerance(T machineSettings);

        protected abstract bool IsOnTheFly(T machineSettings);

        protected abstract CompensationParameters GetCompensationParameters(RuleAppliedPhysicalDesign design, MicroMeter trackOffset, MicroMeter corrugateWidth, T machineSettings, IPacksizeCarton order);
    }
}
