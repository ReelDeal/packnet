﻿using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Logging;


namespace PackNet.Business.IqFusionJobCreator
{
    using CodeGenerator;
    using Common.Interfaces.DTO;
    using DesignCompensator;
    using DesignRulesApplicator;
    using System.Linq;

    public class FusionMachineInstructionCreator : MachineInstructionCreator<FusionPhysicalMachineSettings>
    {
        public FusionMachineInstructionCreator(ILogger logger)
            : base(logger)
        {
        }

        /// <summary>
        /// Shortens overlapping lines, replace connected lines with one line, etc.
        /// </summary>
        protected override RuleAppliedPhysicalDesign ApplyRules(PhysicalDesign measureAppliedDesign, FusionPhysicalMachineSettings machineSettings, Corrugate corrugate, OrientationEnum rotation)
        {
            var designRuleApplicator = new DesignRulesApplicator(new WasteAreaParameters()
            {
                Enable = false
            });

            return designRuleApplicator.ApplyRules(measureAppliedDesign, new FusionPerforationLinesRule(measureAppliedDesign.PerforationParameters), new IqNunatabLinesRule(), corrugate, rotation);
        }

        protected override RuleAppliedPhysicalDesign ApplyDesignCompensation(CompensationParameters parameters)
        {
            return new DesignCompensator().Compensate(parameters);
        }

        protected override CompensationParameters GetCompensationParameters(RuleAppliedPhysicalDesign design,
            MicroMeter trackOffset, MicroMeter corrugateWidth, FusionPhysicalMachineSettings machineSettings, IPacksizeCarton order)
        {
            var compensationParameters = new CompensationParameters(
                design,
                IsTrackRighSideFixed(machineSettings, order.TrackNumber),
                trackOffset,
                corrugateWidth,
                GetCrossHead(machineSettings).CutCompensation,
                GetCrossHead(machineSettings).CreaseCompensation,
                machineSettings.CodeGeneratorSettings.MinimumLineLength,
                machineSettings.CodeGeneratorSettings.MinimumLineDistanceToCorrugateEdge,
                0,
                0);

            return compensationParameters;
        }

        protected override void CreateCodeGenerator(FusionPhysicalMachineSettings machineSettings)
        {
            if (CodeGenerator == null)
            {
                CodeGenerator = new FusionCodeGenerator(GetCrossHead(machineSettings).Position, GetLongHeadPositions(machineSettings), Logger);
            }
        }

        private FusionCrossHeadParameters GetCrossHead(FusionPhysicalMachineSettings machineSettings)
        {
            return machineSettings.CrossHeadParameters;
        }

        private MicroMeter[] GetLongHeadPositions(FusionPhysicalMachineSettings machineSettings)
        {
            return machineSettings.LongHeadParameters.LongHeads.Select(lh => lh.Position).ToArray();
        }

        protected override bool IsTrackRighSideFixed(FusionPhysicalMachineSettings machineSettings, int trackNumber)
        {
            return machineSettings.TrackParameters.Tracks.Single(t => t.Number == trackNumber).RightSideFixed;
        }

        protected override MicroMeter GetCorrugateWidthTolerance(FusionPhysicalMachineSettings machineSettings)
        {
            return machineSettings.TrackParameters.CorrugateWidthTolerance;
        }

        protected override bool IsOnTheFly(FusionPhysicalMachineSettings machineSettings)
        {
            return machineSettings.OnTheFlyActive;
        }
    }
}