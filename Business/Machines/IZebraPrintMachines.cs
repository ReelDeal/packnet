﻿using System;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Printing;

namespace PackNet.Business.Machines
{
    public interface IZebraPrintMachines : IMachines<ZebraPrinter>
    {
        /// <summary>
        /// Produces the specified machine identifier.
        /// </summary>
        /// <param name="machineId">The machine identifier.</param>
        /// <param name="producible">The producible.</param>
        void Produce(Guid machineId, IProducible producible);
    }
}