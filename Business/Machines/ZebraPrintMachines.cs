﻿using System.Threading.Tasks;

using PackNet.Business.Templates;
using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Communication.Printer;
using PackNet.Data.PrintMachines;

using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace PackNet.Business.Machines
{
    public class ZebraPrintMachines : IZebraPrintMachines
    {
        private readonly IZebraPrinterRepository zebraPrinterRepository;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IServiceLocator serviceLocator;
        private readonly ILogger logger;
        private readonly ITemplates templates;

        private readonly ConcurrentDictionary<Guid, ZebraPrinter> createdMachines = new ConcurrentDictionary<Guid, ZebraPrinter>();
        private readonly ConcurrentDictionary<Guid, ZebraPrinterCommunicator> communicators = new ConcurrentDictionary<Guid, ZebraPrinterCommunicator>();
        private readonly ConcurrentDictionary<Guid, List<IDisposable>> printerWireups = new ConcurrentDictionary<Guid, List<IDisposable>>();

        public ZebraPrintMachines(IZebraPrinterRepository zebraPrinterRepository,
                                IEventAggregatorPublisher publisher,
                                IServiceLocator serviceLocator,
                                ILogger logger,
                                ITemplates templates)
        {
            this.zebraPrinterRepository = zebraPrinterRepository;
            this.publisher = publisher;
            this.serviceLocator = serviceLocator;
            this.logger = logger;
            this.templates = templates;

            zebraPrinterRepository.All().ForEach(m => createdMachines.TryAdd(m.Id, m));

            Parallel.ForEach(createdMachines.Values, (printer, state) =>
            {
                UpdateCapabilities(printer);
                StartMachineCommunication(printer);
            });
        }

        private void StartMachineCommunication(ZebraPrinter printer)
        {
            try
            {
                //remove old wireups
                List<IDisposable> wireups;
                if (printerWireups.TryRemove(printer.Id, out wireups))
                {
                    wireups.ForEach(w => w.Dispose());
                }
                wireups = new List<IDisposable>();

                printer.CurrentStatus = MachineStatuses.MachineOffline;

                wireups.Add(printer.CurrentStatusChangedObservable.DurableSubscribe(newStatus => SendMachineStatusChanged(printer), logger));
                //wireups.Add(printer.CurrentProductionStatusObservable.DurableSubscribe(newStatus => SendMachineStatusChanged(printer), logger));

                if (!printerWireups.TryAdd(printer.Id, wireups))
                {
                    throw new ApplicationException(string.Format("Unable to add printer wireups. {0}", printer));
                }

                var comm = new ZebraPrinterCommunicator(printer, logger, publisher, serviceLocator);
                Retry.Do(() => communicators.TryAdd(printer.Id, comm), TimeSpan.FromMilliseconds(100));
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, string.Format("Unable to add communicator for {0}", printer), e);
            }
        }

        private void SendMachineStatusChanged(ZebraPrinter printer)
        {
            logger.Log(LogLevel.Trace, "{0} CurrentStatusChanged to {1}", printer, printer.CurrentStatus);
            var message = new Message<IMachine>
            {
                MessageType = MachineMessages.MachineStatusChanged,
                Data = printer
            };
            publisher.Publish(message);
        }

        public ZebraPrinter Create(ZebraPrinter machine)
        {
            if (FindByAlias(machine.Alias) != null)
                throw new ItemExistsException("Invalid Zebra Printer, Zebra Printer Name already exists");

            UpdateCapabilities(machine);
            zebraPrinterRepository.Create(machine);
            createdMachines.TryAdd(machine.Id, machine);
            StartMachineCommunication(machine);
            return machine;
        }

        public void Delete(ZebraPrinter machine)
        {
            var machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            var machineGroup = machineGroupService.FindByMachineId(machine.Id);

            if (machineGroup == null)
            {
                RemoveCreatedMachine(machine);
                zebraPrinterRepository.Delete(machine);
            }
            else
            {
                throw new RelationshipExistsException(string.Format("Unable to delete, Machine '{0}' is related to the Machine Group '{1}'", machine.Alias, machineGroup.Alias));
            }
        }

        public ZebraPrinter Update(ZebraPrinter machine, Func<bool> machineSpecificSteps = null)
        {
            try
            {
                ZebraPrinter machineInstance = FindByAlias(machine.Alias);

                if (machineInstance != null && machineInstance.Id != machine.Id)
                    throw new ItemExistsException("Invalid Zebra Printer, Zebra Printer Name already exists");
                
                //todo ensure that machine is not working.
                machineInstance = null;

                Retry.Do(() => createdMachines.TryGetValue(machine.Id, out machineInstance), TimeSpan.FromMilliseconds(100));

                if (machineInstance.CurrentProductionStatus == MachineStatuses.MachineOffline)
                {
                    logger.Log(LogLevel.Warning, "Printer ");
                }

                ZebraPrinterCommunicator comm = null;
                Retry.Do(() => communicators.TryRemove(machine.Id, out comm), TimeSpan.FromMilliseconds(100));
                if (comm != null)
                    comm.Dispose();

                machineInstance.UpdateFrom(machine);
                //todo: find out why the CapabilityChangeObservable is not getting to the MachineGroup.
                UpdateCapabilities(machineInstance);

                StartMachineCommunication(machineInstance);
                return zebraPrinterRepository.Update(machineInstance);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, string.Format("Machine Group does not exit. MachineGroup:{0}", machine), e);

                throw;
            }
        }

        public ZebraPrinter Find(Guid machineId)
        {
            ZebraPrinter createdMachine;
            createdMachines.TryGetValue(machineId, out createdMachine);
            if (createdMachine != null)
                return createdMachine;

            return zebraPrinterRepository.Find(machineId);
        }

        public ZebraPrinter FindByAlias(string alias)
        {
            return GetMachines().FirstOrDefault(g => g.Alias != null && g.Alias.ToLowerInvariant() == alias.ToLowerInvariant());
        }

        private void UpdateCapabilities(ZebraPrinter machine)
        {

            var newCapabilities = new List<ICapability>
            {
                machine.IsPriorityPrinter
                    ? new BasicCapability<string>("PriorityLabel")
                    : new BasicCapability<string>("StandardLabel"),
                machine.IsPeelOff // this capability is not really used except to show in the ui.
                    ? new BasicCapability<string>("Peel Off")
                    : new BasicCapability<string>("Tear Off")
            };

            // TODO: Should be listening to template update or something like that.
            templates.GetAll().ForEach(template =>
                newCapabilities.Add(new BasicCapability<Template>(template)));

            machine.AddOrUpdateCapabilities(newCapabilities);
        }

        public IEnumerable<ZebraPrinter> GetMachines()
        {
            return createdMachines.Values;
        }

        private void RemoveCreatedMachine(ZebraPrinter machine)
        {
            ZebraPrinter createdMachine;
            createdMachines.TryRemove(machine.Id, out createdMachine);
            ZebraPrinterCommunicator communicator;
            communicators.TryRemove(machine.Id, out communicator);
            if (createdMachine != null)
            {
                createdMachine.Dispose();
            }
            if (communicator != null)
                communicator.Dispose();
        }

        /// <summary>
        /// Send the requested work to the machine.
        /// </summary>
        /// <param name="printerId"></param>
        /// <param name="producible"></param>
        public void Produce(Guid printerId, IProducible producible)
        {
            producible.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            ZebraPrinter printer;
            createdMachines.TryGetValue(printerId, out printer);
            if (printer == null)
                throw new ApplicationException(string.Format("ZebraPrinterMachine not found MachineId:{0}", printerId));

            ZebraPrinterCommunicator communicator;
            communicators.TryGetValue(printerId, out communicator);
            if (communicator == null)
                throw new ApplicationException(string.Format("Zebra Communicator not found MachineId:{0}", printerId));


            var itemToPrint = producible as IPrintable;
            if (itemToPrint == null)
                throw new ApplicationException(string.Format("Producible sent to Zebra Printer is not an IPrintable. {0}, {1}", printer, producible));

            try
            {
                //Template merge, 
                //todo: is this what we want?  currently after this occurs the producible in the database will have the merged values in place of the dictionary in printData
                
                //TFS 9563 - Assign to first or default and then throw app exception if still null and handle the exception correctly
                
                if (itemToPrint.PrintData is IDictionary)
                {
                    var restriction = (BasicRestriction<Template>)itemToPrint.Restrictions.FirstOrDefault(r => r.GetType() == typeof(BasicRestriction<Template>));
                    
                    if (restriction != null)
                    {
                        var template = templates.Get(restriction.Value.Name);
                        itemToPrint.PrintData = template.Content.Inject((IDictionary)itemToPrint.PrintData);
                    }
                }

                communicator.Print(itemToPrint);
                producible.ProduceOnMachineId = printerId;
                logger.Log(LogLevel.Debug, String.Format("{0} dispatched {1}", itemToPrint, printer));
            }
            catch (PrinterBusyException pbe)
            {
                var userNotificationService = serviceLocator.Locate<IUserNotificationService>();

                userNotificationService.SendNotificationToMachineGroup(
                    NotificationSeverity.Error,
                    String.Format("Error while printing label '{1}' on {0} printer", printer, itemToPrint),
                    string.Empty,
                    producible.ProducedOnMachineGroupId
                    );

                logger.Log(LogLevel.Error, "Printing failed on {0} for Producible:{1}, Exception:{2} ", printer, itemToPrint, pbe);
            }

        }

        public void Dispose()
        {
            Dispose(true);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                printerWireups.Values.ForEach(list=>list.ForEach(d=>d.Dispose()));
                createdMachines.Values.ForEach(m => m.Dispose());
                communicators.Values.ForEach(c => c.Dispose());
            }
        }
    }
}