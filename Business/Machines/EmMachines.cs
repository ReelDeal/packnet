﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Data.Machines;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PackNet.Business.Machines
{

    public class EmMachines : MachinesBase<EmMachine>, IEmMachines
    {
        private readonly IEventAggregatorPublisher publisher;
        
        public EmMachines(IEmMachineRepository emMachineRepository, IMachineCommunicatorFactory machineCommunicatorFactory, IEventAggregatorSubscriber subscriber, IEventAggregatorPublisher publisher, IServiceLocator serviceLocator, ILogger logger)
            : base(emMachineRepository, machineCommunicatorFactory, serviceLocator, logger)
        {
            this.publisher = publisher;
            //Get all machines
            CreatedMachines.AddRange(emMachineRepository.All());

            //Start communication for each machine
            CreatedMachines.ForEach(SetupMachineCommunication);

            PackNetServerSettingsMessages.ServerCallBackIPSettingsUpdated
                .OnMessage(subscriber, logger, 
                    message => CreatedMachines.ForEach(RestartMachine));
        }

        private void RestartMachine(EmMachine machine)
        {
            RemoveMachineCommunicator(machine);
            SetupMachineCommunication(machine);
        }

        protected override void MachinePoweredOffCorrugateUpdate(EmMachine localMachine, ITrackBasedCutCreaseMachine machine)
        {
            //todo: we have to tear down communication because when corrugates change it does something to the physical machine settings that are necessary when communication is established.
            RemoveMachineCommunicator(localMachine);

            machine.Tracks.ForEach(t => localMachine.LoadCorrugate(t, t.LoadedCorrugate));

            UpdateCapabilities(localMachine);

            SetupMachineCommunication(localMachine);
            Update(localMachine, () => UpdateEmSpecific(localMachine));
        }

        public void EnterChangeCorrugate(EmMachine localMachine, int trackNumber)
        {
            var communicator = GetCommunicator(localMachine.Id);
            communicator.SetChangeCorrugate(true, trackNumber);
        }

        public void LeaveChangeCorrugate(EmMachine localMachine)
        {
            var communicator = GetCommunicator(localMachine.Id);
            communicator.LeaveChangeCorrugate();
        }

        protected override void MachinePoweredOnCorrugateUpdate(EmMachine localMachine, ITrackBasedCutCreaseMachine machine)
        {
            var communicator = GetCommunicator(localMachine.Id);

            machine.Tracks.ForEach(t => localMachine.LoadCorrugate(t, t.LoadedCorrugate));

            UpdateCapabilities(localMachine);

            communicator.SetCorrugateWidths(localMachine.Tracks);
            communicator.SetTrackStartinPositions(localMachine.Tracks);

            Update(localMachine, () => UpdateEmSpecific(localMachine));
        }

        private bool UpdateEmSpecific(EmMachine machine)
        {
            var communicator = GetCommunicator(machine.Id);

            if (communicator.IsConnected)
            {
                communicator.SetNumberOfTracks(machine.NumTracks);
                communicator.LeaveChangeCorrugate();
            }

            return true;
        }

        public void SetMachineStatus(EmMachine machine, MachineStatuses status)
        {
            if (machine.CurrentStatus == status)
                return;

            if (status == MachineStatuses.MachineOnline && machine.Errors.Any())
                return;

            machine.CurrentStatus = status;

            var message = new Message<IPacksizeCutCreaseMachine>
            {
                MessageType = MachineMessages.ChangeMachineState,
                Data = machine
            };
            publisher.Publish(message);
        }
        
        public IEmMachineCommunicator GetCommunicator(Guid machineId)
        {
            var machineCommunicator = GetMachineCommunicator(machineId);
            return machineCommunicator as IEmMachineCommunicator;
        }

        public void Produce(Guid machineId, IProducible producible, IEnumerable<IInstructionItem> instructionList)
        {
            var communicator = GetCommunicator(machineId);
            var machine = CreatedMachines.FirstOrDefault(m => m.Id == machineId);
            if (machine.CurrentProductionStatus != MachineProductionStatuses.ProductionIdle && machine.CurrentProductionStatus != MachineProductionStatuses.ProductionCompleted)
            {
                communicator.PauseMachine();
                machine.CurrentStatus = MachineErrorStatuses.MachineAlreadyInProductionError;
                var message = String.Format("System sent a carton to the machine when a carton is already in production: Producible {0}, Machine {1}", producible, machine);
                machine.Errors.Add(new MachineError(MachineErrorStatuses.MachineAlreadyInProductionError, new List<string> { message }));
                throw new Exception(message);
            }
            communicator.Produce(producible, instructionList);
            producible.ProduceOnMachineId = machineId;
        }
    }
}