﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Business.Machines
{
    public interface IMachineGroupWorkflowDispatcher
    {
        void DispatchCreateProducibleWorkflow(MachineGroup machineGroup, IProducible producible);
    }
}