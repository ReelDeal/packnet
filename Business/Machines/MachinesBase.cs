﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace PackNet.Business.Machines
{
    public abstract class MachinesBase<T> where T : IMachineWithCapabilities, IPacksizeCutCreaseMachine
    {
        protected readonly List<T> CreatedMachines = new List<T>();
        private readonly IDictionary<Guid, IDisposable> machineVersionObservables = new Dictionary<Guid, IDisposable>();

        protected readonly ConcurrentDictionary<Guid, IMachineCommunicator> MachineComunication = new ConcurrentDictionary<Guid, IMachineCommunicator>();
        protected IRepository<T> Repository;
        protected readonly IMachineCommunicatorFactory MachineCommunicatorFactory;
        protected readonly IServiceLocator serviceLocator;
        private readonly ILogger logger;

        protected abstract void MachinePoweredOffCorrugateUpdate(T localMachine, ITrackBasedCutCreaseMachine machine);
        protected abstract void MachinePoweredOnCorrugateUpdate(T localMachine, ITrackBasedCutCreaseMachine machine);
        private ICodeGenerationService codeGenerationService;

        public ICodeGenerationService CodeGenerationService
        {
            get
            {
                return codeGenerationService ?? (codeGenerationService = serviceLocator.Locate<ICodeGenerationService>());
            }
        }

        protected MachinesBase(IRepository<T> repository, IMachineCommunicatorFactory machineCommunicatorFactory, IServiceLocator serviceLocator, ILogger logger)
        {
            Repository = repository;
            MachineCommunicatorFactory = machineCommunicatorFactory;
            this.serviceLocator = serviceLocator;
            this.logger = logger;
        }

        private void AddMachine(T machine)
        {
            CreatedMachines.Add(machine);

            if (machineVersionObservables.ContainsKey(machine.Id))
            {
                machineVersionObservables[machine.Id].Dispose();
                machineVersionObservables[machine.Id] =
                    machine.PlcVersionChangedObservable.Subscribe(s => SetPlcVersionCapability(machine));
                return;
            }

            machineVersionObservables.Add(machine.Id,
                machine.PlcVersionChangedObservable.Subscribe(s => SetPlcVersionCapability(machine)));
        }

        public T Create(T machine)
        {
            if (FindByAlias(machine.Alias) != null)
                throw new ItemExistsException("Invalid Machine, Machine Name already exists");

            Repository.Create(machine);
            SetupMachineCommunication(machine);
            AddMachine(machine);
            return machine;
        }

        public T Update(T machine, Func<bool> machineSpecificSteps = null)
        {
            T createdMachine = FindByAlias(machine.Alias);

            if (createdMachine != null && createdMachine.Id != machine.Id)
                throw new ItemExistsException("Invalid Machine, Machine Name already exists");

            createdMachine = CreatedMachines.FirstOrDefault(m => m.Id == machine.Id);

            if (createdMachine != null)
            {
                createdMachine.UpdateFrom(machine);
                UpdateMachineCommunication(machine);
            }
            else
            {
                return default(T);
            }

            SetupTracksForMachine(createdMachine, machine);
            SetupMachineSettings(createdMachine);
            UpdateCapabilities(createdMachine);

            if (machineSpecificSteps != null && machineSpecificSteps.Invoke() == false)
            {
                throw new Exception("Unable to process machine specific steps in update");
            }

            return Repository.Update(createdMachine);
        }

        public void Delete(T machine)
        {
            var machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            var machineGroup = machineGroupService.FindByMachineId(machine.Id);

            if (machineGroup == null)
            {
                RemoveCreatedMachine(machine);
                RemoveMachineCommunicator(machine);
                Repository.Delete(machine);
            }
            else
            {
                throw new RelationshipExistsException(string.Format("Unable to delete, Machine '{0}' is related to the Machine Group '{1}'", machine.Alias, machineGroup.Alias));
            }
        }

        public T Find(Guid machineId)
        {
            var machine = CreatedMachines.FirstOrDefault(m => m.Id == machineId);

            if (machine == null)
                return Repository.Find(machineId);
            return machine;
        }

        public T FindByAlias(string alias)
        {
            return GetMachines().FirstOrDefault(g => g.Alias != null && g.Alias.ToLowerInvariant() == alias.ToLowerInvariant());
        }

        public IEnumerable<T> GetMachines()
        {
            return CreatedMachines;
        }

        public void UpdateMachineSettings(T machine)
        {
            IMachineCommunicator communicator;

            if (MachineComunication.TryGetValue(machine.Id, out communicator))
            {
                if (communicator.IsConnected)
                {
                    communicator.SynchronizeMachine(true);
                }
            }
        }

        public void ConfigureTracks(ITrackBasedCutCreaseMachine machine)
        {
            if (machine == null || Find(machine.Id) == null)
            {
                return;
            }

            var localMachine = CreatedMachines.Single(mach => mach.Id == machine.Id);

            if (localMachine.CurrentStatus == MachineStatuses.MachineOffline)
            {
                MachinePoweredOffCorrugateUpdate(localMachine, machine);
            }
            else if (localMachine.CurrentStatus == MachinePausedStatuses.MachinePaused ||
                localMachine.CurrentStatus == MachineErrorStatuses.MachineError ||
                localMachine.CurrentStatus == MachineErrorStatuses.EmergencyStop ||
                localMachine.CurrentStatus == MachinePausedStatuses.MachineChangingCorrugate ||
                localMachine.CurrentStatus == PacksizePackagingMachineErrorStatuses.MachineInOutOfCorrugate)
            {
                MachinePoweredOnCorrugateUpdate(localMachine, machine);
            }
        }

        public bool CanProduce(T machine, IProducible producible)
        {
            var carton = producible as ICarton;
            if (carton == null)
                return false;

            return CodeGenerationService.CanProduce(producible, machine);
        }

        public void Dispose()
        {
            MachineComunication.Values.ForEach(m => m.Dispose());
        }

        protected void RemoveMachineCommunicator(T machine)
        {
            IMachineCommunicator communicator = null;
            if (MachineComunication.TryRemove(machine.Id, out communicator))
            {
                communicator.Dispose();
            }
        }

        protected void RemoveCreatedMachine(T machine)
        {
            var createdMachine = CreatedMachines.FirstOrDefault(m => m.Id == machine.Id);
            if (createdMachine != null)
            {
                if (machineVersionObservables.ContainsKey(machine.Id))
                {
                    machineVersionObservables[machine.Id].Dispose();
                    machineVersionObservables.Remove(machine.Id);
                }

                CreatedMachines.Remove(createdMachine);
            }
        }

        protected void SetupMachineCommunication(T machine)
        {
            try
            {
                machine.CurrentStatus = MachineStatuses.MachineOffline;
                if (MachineComunication.TryAdd(machine.Id, MachineCommunicatorFactory.GetMachineCommunicator(machine)))
                {
                    UpdateCapabilities(machine);
                    SetPlcVersionCapability(machine);
                }

                if (machineVersionObservables.ContainsKey(machine.Id))
                {
                    machineVersionObservables[machine.Id].Dispose();
                    machineVersionObservables[machine.Id] =
                        machine.PlcVersionChangedObservable.Subscribe(s => SetPlcVersionCapability(machine));
                    return;
                }

                machineVersionObservables.Add(machine.Id,
                    machine.PlcVersionChangedObservable.Subscribe(s => SetPlcVersionCapability(machine)));
            }
            catch (Exception)
            {
                //TODO publish error to ui
            }
        }

        protected IMachineCommunicator GetMachineCommunicator(Guid machineId)
        {
            IMachineCommunicator machineCommunicator;

            if (!MachineComunication.TryGetValue(machineId, out machineCommunicator))
            {
                throw new Exception("Failed to locate machine by Id " + machineId);
            }

            return machineCommunicator;
        }

        private void UpdateMachineCommunication(T machine)
        {
            IMachineCommunicator communicator;
            if (MachineComunication.TryGetValue(machine.Id, out communicator))
            {
                MachineCommunicatorFactory.UpdateWebRequestCreator(machine, communicator);
            }
        }

        private void SetupMachineSettings(T createdMachine)
        {
            createdMachine.UpdatePhysicalMachineSettings(MachineCommunicatorFactory.GetCallbackSetting());
            UpdateMachineSettings(createdMachine);
        }

        private static void SetPlcVersionCapability(T machine)
        {
            if (string.IsNullOrEmpty(machine.PlcVersion) == false)
                machine.AddOrUpdateCapabilities(new List<ICapability>() { new PlcVersionCapability(machine.PlcVersion) });
        }

        protected static void UpdateCapabilities(T machine)
        {
            //var newCapabilities = (machine as IMachineWithCapabilities).CurrentCapabilities.Where(c => (c is BasicCapability<Corrugate> == false)).ToList();
            //Fix for 11766 which is already in master - keeps machine type from getting appended in operator panel every time corrugates are changed.
            var newCapabilities = new List<ICapability>();
            
            AddCorrugatesOnMachineIdCapability(machine, newCapabilities);
            
            //todo: consider adding more capabilities like RSC box cutter, designs the machine is capable of creating, etc.

            //make the change to the value and the observable get's done for us behind the scenes.
            machine.AddOrUpdateCapabilities(newCapabilities);
        }

        private static void AddCorrugatesOnMachineIdCapability(T machine, List<ICapability> newCapabilities)
        {
            if (machine.Tracks != null)
            {
                var loadedCorrugates = machine.Tracks.Select(t => t.LoadedCorrugate).Where(c => c != null).ToList();
                if (loadedCorrugates.Any())
                    newCapabilities.Add(new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machine.Id, loadedCorrugates)));
            }
        }

        private static void SetupTracksForMachine(T local, T newData)
        {
            if (TracksHaveSameCorrugateLoaded(local, newData))
            {
                return;
            }

            //This assignment will recreate the tracks
            local.NumTracks = newData.NumTracks;

            for (var i = 0; i < local.NumTracks; i++)
            {
                var localTrack = local.Tracks[i];
                var newTrack = newData.Tracks[i];

                if (newTrack == null)
                {
                    continue;
                }

                // Don't Nuke the current loaded Corrugate
                if (newTrack.LoadedCorrugate != null)
                {
                    localTrack.LoadedCorrugate = newTrack.LoadedCorrugate;
                }

                localTrack.TrackNumber = newTrack.TrackNumber;

                // Don't nuke the offset
                if (newTrack.TrackOffset != 0)
                {
                    localTrack.TrackOffset = newTrack.TrackOffset;
                }
            }
        }

        private static bool TracksHaveSameCorrugateLoaded(T local, T newData)
        {
            if (local.NumTracks != newData.NumTracks)
                return false;

            var isSame = true;

            for (int i = 0; i < local.NumTracks; i++)
            {
                var localTrack = local.Tracks.ElementAt(i);
                var newTrack = newData.Tracks.ElementAt(i);

                if (BothTracksHaveNullCorrugateLoaded(localTrack, newTrack))
                    continue;

                if (OneTrackNullAndTheOtherIsNot(localTrack, newTrack))
                {
                    isSame = false;
                    break;
                }

                if (localTrack.LoadedCorrugate.Id != newTrack.LoadedCorrugate.Id)
                    isSame = false;
            }

            return isSame;
        }

        private static bool BothTracksHaveNullCorrugateLoaded(Track track1, Track track2)
        {
            return track1.LoadedCorrugate == null && track2.LoadedCorrugate == null;
        }

        private static bool OneTrackNullAndTheOtherIsNot(Track track1, Track track2)
        {
            return (track1.LoadedCorrugate == null && track2.LoadedCorrugate != null) ||
                   (track1.LoadedCorrugate != null && track2.LoadedCorrugate == null);
        }
    }
}
