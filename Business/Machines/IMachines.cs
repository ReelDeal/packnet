﻿using PackNet.Common.Interfaces.Machines;
using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Business.Machines
{
    public interface IMachines<T> : IDisposable where T : IMachine
    {
        /// <summary>
        /// Creates the specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns></returns>
        T Create(T machine);
        /// <summary>
        /// Deletes the specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        void Delete(T machine);
        /// <summary>
        /// Updates the specified machine.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <param name="machineSpecificSteps">Machine specific update steps. I.e. the fusion needs to update track alignment in the communicator</param>
        /// <returns></returns>
        T Update(T machine, Func<bool> machineSpecificSteps = null);
        /// <summary>
        /// Finds the specified machine identifier.
        /// </summary>
        /// <param name="machineId">The machine identifier.</param>
        /// <returns></returns>
        T Find(Guid machineId);
        /// <summary>
        /// Finds the specified machine alias.
        /// </summary>
        /// <param name="alias">The machine alias.</param>
        /// <returns></returns>
        T FindByAlias(string alias);
        /// <summary>
        /// Gets the machines.
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetMachines();
    }
}
