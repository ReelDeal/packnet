﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Business.Machines
{
    public interface IEmMachines : IPacksizeCutCreaseMachines<EmMachine>
    {
        /// <summary>
        /// Changes the physical machine state (Paused/Online) 
        /// </summary>
        /// <param name="machine"></param>
        /// <param name="status"></param>
        void SetMachineStatus(EmMachine machine, MachineStatuses status);

        void EnterChangeCorrugate(EmMachine localMachine, int trackNumber);

        void LeaveChangeCorrugate(EmMachine localMachine);
    }
}