﻿using System.Activities.Statements;
using System.IO;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace PackNet.Business.Machines
{
    using PackNet.Common.Interfaces.Exceptions;

    /// <summary>
    /// There are two queues involved in dispatching work to a machine group.
    /// Queue 1 "work queue" is the number of items we wanted queued up to speed up the selection process
    /// Queue 2 "currently working queue" is the work that has been taken from queue 1 and has had the workflow for the machine group invoked for that producible.
    /// </summary>
    public class MachineGroupBl : IMachineGroups, IDisposable
    {
        private readonly IMachineGroupRepository machineGroupRepository;
        private ConcurrentDictionary<Guid /* mg id*/, BlockingQueue<IProducible>> queues;
        private ConcurrentDictionary<Guid, CancellationTokenSource> consumerHandles;
        private ConcurrentDictionary<Guid /* mg id*/, List<Tuple<Guid /*machine id*/, IDisposable/* each observable*/>>> machineGroupMachineObservables;
        private ISelectionAlgorithmDispatchService selectionAlgorithmDispatchService;
        private readonly IAggregateMachineService machineService;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IUserNotificationService userNotificationService;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IMachineGroupWorkflowDispatcher machineGroupWorkflowDispatcher;
        private ILogger logger;
        private ConcurrentDictionary<Guid, MachineGroup> machineGroupInstances;

        public MachineGroupBl(
            IMachineGroupRepository machineGroupRepository,
            ISelectionAlgorithmDispatchService selectionAlgorithmDispatchService,
            IAggregateMachineService machineService,
            IEventAggregatorSubscriber subscriber,
            IMachineGroupWorkflowDispatcher machineGroupWorkflowDispatcher,
            ILogger logger,
            IEventAggregatorPublisher publisher,
            IUserNotificationService userNotificationService)
        {
            this.logger = logger;
            this.publisher = publisher;
            this.userNotificationService = userNotificationService;
            this.selectionAlgorithmDispatchService = selectionAlgorithmDispatchService;
            this.machineService = machineService;
            this.subscriber = subscriber;
            this.machineGroupWorkflowDispatcher = machineGroupWorkflowDispatcher;
            this.machineGroupRepository = machineGroupRepository;
            queues = new ConcurrentDictionary<Guid /*Machine Group ID*/, BlockingQueue<IProducible>/*Queue*/>();
            machineGroupInstances = new ConcurrentDictionary<Guid, MachineGroup>();
            consumerHandles = new ConcurrentDictionary<Guid, CancellationTokenSource>();
            machineGroupMachineObservables = new ConcurrentDictionary<Guid, List<Tuple<Guid, IDisposable>>>();
            //load machine from repo
            this.machineGroupRepository.All().ForEach(AddMachineGroupAndMonitorMachineInstances);
        }


        private void AddMachineGroupAndMonitorMachineInstances(MachineGroup mg)
        {
            mg.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupPaused;

            if (!machineGroupInstances.ContainsKey(mg.Id))
            {
                while (!machineGroupInstances.TryAdd(mg.Id, mg))
                {
                    Thread.Sleep(50);
                }
            }

            StartProducibleConsumer(mg);
            EstablishMachineMonitoring(mg);
        }

        public MachineGroup Create(MachineGroup machineGroup)
        {
            if (FindByAlias(machineGroup.Alias) != null)
                throw new ItemExistsException("Invalid Machine Group, Machine Group Name already exists");

            machineGroupRepository.Create(machineGroup);
            AddMachineGroupAndMonitorMachineInstances(machineGroup);

            publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
            return machineGroup;
        }

        /// <summary>
        /// Remove the MG from the system.
        /// Machine group and it's machines must not be working.
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <exception cref="System.ApplicationException">MachineGroupInProduction</exception>
        public void Delete(MachineGroup machineGroup)
        {
            var existingMachineGroup = Find(machineGroup.Id);
            //We can not make changes to the MG if any of it's machines are in progress.
            ValidateMachineNotInProduction(existingMachineGroup);
            EndProducibleConsumerAndRemoveQueue(existingMachineGroup);

            MachineGroup removed = null;
            Retry.UntilTrue(() => machineGroupInstances.TryRemove(existingMachineGroup.Id, out removed), TimeSpan.FromMilliseconds(200));
            machineGroupRepository.Delete(existingMachineGroup);
            removed.Dispose();
        }

        private void ValidateMachineNotInProduction(MachineGroup existingMachineGroup)
        {
            IProducible hasItemInQueue = null;
            BlockingQueue<IProducible> queue;
            if (queues.TryGetValue(existingMachineGroup.Id, out queue))
            {
                hasItemInQueue = queue.Peek();
            }

            if (existingMachineGroup.CurrentProductionStatus is MachineProductionInProgressStatuses || hasItemInQueue != null)
            {
                throw new MachineGroupException(existingMachineGroup.Id, MachineGroupMessages.MachineGroupInProduction);
            }
        }

        private void EndProducibleConsumerAndRemoveQueue(MachineGroup machineGroup)
        {
            var cancellationSource = consumerHandles[machineGroup.Id];
            cancellationSource.Cancel(true);
            queues[machineGroup.Id].Dispose();
        }

        /// <summary>
        /// Updates the current machine group instance with modified values.
        /// MG must not be in production when changes made.
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <returns></returns>
        /// <exception cref="ApplicationException">If MG or it's machine are currently producing.</exception>
        public MachineGroup Update(MachineGroup machineGroup)
        {
            MachineGroup existingMachineGroup = FindByAlias(machineGroup.Alias);

            if (existingMachineGroup != null && existingMachineGroup.Id != machineGroup.Id)
                throw new ItemExistsException("Invalid Machine Group, Machine Group Name already exists");

            existingMachineGroup = Find(machineGroup.Id);
            //We can not make changes to the MG if any of it's machines are in progress.
            ValidateMachineNotInProduction(existingMachineGroup);
            EndProducibleConsumerAndRemoveQueue(existingMachineGroup);

            var machinesToRemove = existingMachineGroup.ConfiguredMachines.Except(machineGroup.ConfiguredMachines);
            var machinesToAdd = machineGroup.ConfiguredMachines.Except(existingMachineGroup.ConfiguredMachines);

            //Keep the referenced object rather then creating a new one
            existingMachineGroup.ConfiguredMachines = machineGroup.ConfiguredMachines;
            existingMachineGroup.Alias = machineGroup.Alias;
            existingMachineGroup.Description = machineGroup.Description;

            //TODO: replace blocking queue
            existingMachineGroup.MaxQueueLength = machineGroup.MaxQueueLength;

            existingMachineGroup.WorkflowPath = machineGroup.WorkflowPath;
            machineGroupRepository.Update(existingMachineGroup);

            //remove all listeners from the machines that were in this group.
            machinesToRemove.ForEach(m => RemoveMonitoringOfMachineInstances(existingMachineGroup, m));

            machinesToAdd.ForEach(id =>
            {
                var machineInstance = machineService.FindById(id);
                if (machineInstance != null)
                {
                    MonitorMachineInstance(existingMachineGroup, machineInstance);
                }
            });
            existingMachineGroup.SetMachineGroupStatusBasedOnMachines(machineService, logger, false);

            StartProducibleConsumer(existingMachineGroup);
            return existingMachineGroup;
        }

        public MachineGroup Find(Guid machineGroupId)
        {
            MachineGroup mg;
            machineGroupInstances.TryGetValue(machineGroupId, out mg);
            return mg;
        }

        public MachineGroup FindByAlias(string alias)
        {
            return machineGroupRepository.FindByAlias(alias);
        }

        public IEnumerable<MachineGroup> GetMachineGroups()
        {
            return machineGroupInstances.Values;
        }

        public MachineGroup FindByMachineId(Guid id)
        {
            var mg = machineGroupInstances.Values.FirstOrDefault(m => m.ConfiguredMachines.Contains(id));
            return mg;
        }

        public MachineGroup FindByMachineGroupId(Guid id)
        {
            var mg = machineGroupInstances.Values.FirstOrDefault(m => m.Id == id);
            return mg;
        }

        /// <summary>
        /// Add this item and return true if I can still add more.  This will fail if the MG is not currently "Online"
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <param name="producible"></param>
        /// <returns>True if there is still room to add items to the queue.</returns>
        public bool AddItemToQueue(MachineGroup machineGroup, IProducible producible)
        {
            if (!queues.ContainsKey(machineGroup.Id))
                return false;
            if (machineGroup.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline)
            {
                if (queues[machineGroup.Id].Add(producible))
                {
                    producible.ProducibleStatus = NotInProductionProducibleStatuses.AddedToMachineGroupQueue;
                    return true;
                }

                return false;
            }

            logger.Log(LogLevel.Debug, "Machine group is not online.  We should not be adding to the queue {0} setting producible to removed {1}",
                    machineGroup, producible);

            producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
            return false;
        }

        public bool CanAddItemToQueue(MachineGroup machineGroup)
        {
            if (!queues.ContainsKey(machineGroup.Id))
                return false;

            if (machineGroup.CurrentStatus != MachineGroupAvailableStatuses.MachineGroupOnline)
                return false;

            return queues[machineGroup.Id].CanAddItem;
        }
        /// <summary>
        /// Clear the queue for a machine group.  Removing all items even if they have been started.  
        /// !!!Ensure that you have put the machine in a status other than Online.
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <exception cref="System.ApplicationException">When MG has a status of Online.  Items currently in the queue will be removed but another process could add something after this call to clear.</exception>
        public void ClearQueue(Guid machineGroup)
        {
            MachineGroup mg;
            machineGroupInstances.TryGetValue(machineGroup, out mg);
            if (mg == null)
                return;

            BlockingQueue<IProducible> queue;
            if (queues.TryGetValue(mg.Id, out queue))
            {

                logger.Log(LogLevel.Info, "Clearing Machine Group {0} Queue contained: {1}.", mg.Alias, queue.ToString());
                while (queue.Count > 0)
                {
                    var item = queue.Take();

                    if (item == null)
                        return;

                    logger.Log(LogLevel.Info, "Clearing Machine Group {0}: removed item: {1}.", mg.Alias, item);
                    
                    // Only set to removed if we not kicked off the workflow for the machine group
                    // All other status changes should be handled by workflow or the machines
                    if (item.ProducibleStatus == NotInProductionProducibleStatuses.AddedToMachineGroupQueue)
                    {
                        item.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                        logger.Log(LogLevel.Debug, "Clearing Machine Group {0} Queue set item to ProducibleRemoved: {1}.", mg.Alias, item);
                    }                 
                }
            }
            else
                logger.Log(LogLevel.Warning, "Unable to find queue to clear for Machine Group {0}", mg.Alias);

            //if this is occuring find out where an ensure that we are putting the machine in a state other than online before clearing queue.  Otherwise items could still be added by another process.
            if (mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline)
                throw new ApplicationException(string.Format("Clearing Machine Group when it is still online, ensure that we are changing MG status. {0}. ", mg.Alias));

        }


        /// <summary>
        /// if the machine group is not currently paused we will try to pause it.  If it is paused we will evaluate all the linked machines and try to put it online
        /// </summary>
        /// <param name="machineGroupId"></param>
        public void ToggleMachineGroupPaused(Guid machineGroupId)
        {
            MachineGroup mg;
            machineGroupInstances.TryGetValue(machineGroupId, out mg);
            if (mg == null)
                return;

            if (mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupPaused)
            {
                mg.SetMachineGroupStatusBasedOnMachines(machineService, logger);
            }
            else if (mg.CurrentStatus != MachineGroupAvailableStatuses.ProduceCurrentAndPause && mg.CurrentStatus != MachineGroupAvailableStatuses.ProduceQueueAndPause)
            {
                BlockingQueue<IProducible> queue;
                if (queues.TryGetValue(mg.Id, out queue) && queue.Count > 0)
                {
                    mg.CurrentStatus = MachineGroupAvailableStatuses.ProduceCurrentAndPause;
                    this.ClearQueue(mg.Id);
                }
                else
                    mg.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupPaused;
            }
            else
            {
                logger.Log(
                    LogLevel.Debug,
                    "Tried to toggle machine group paused for MG '{0}' while it was in status {1}.",
                    mg.Alias,
                    mg.CurrentStatus);
            }

        }

        public void PauseMachineGroup(Guid machineGroupId)
        {
            MachineGroup mg;
            machineGroupInstances.TryGetValue(machineGroupId, out mg);
            if (mg == null)
                return;

            PauseMachineGroup(mg);
        }

        public void UpdateProductionMode(MachineGroup machineGroup)
        {
            //Keep the referenced object rather then creating a new one
            var existing = Find(machineGroup.Id);
            existing.ProductionMode = machineGroup.ProductionMode;
            logger.Log(LogLevel.Info, "Machine Group {0} production mode changed to '{1}'", machineGroup.Alias, machineGroup.ProductionMode);
        }

        public void SetAssignedOperator(Guid machineGroupId, string userName)
        {
            var existing = Find(machineGroupId);
            existing.AssignedOperator = userName;
            existing.ConfiguredMachines.ForEach(machineId =>
            {
                var machine = machineService.FindById(machineId) as IHumanOperatedMachine;
                if (machine != null)
                {
                    machine.AssignedOperator = userName;
                }
            });

            if (!string.IsNullOrEmpty(userName))
            {
                logger.Log(LogLevel.Info, "Machine Group operator changed.  MachineGroupId:{0}, UserName:{1}", machineGroupId, userName);
            }
            else
            {
                logger.Log(LogLevel.Info, "Machine Group operator unassigned.  MachineGroupId:{0}", machineGroupId);
            }
        }

        public void CompleteQueueAndPause(Guid machineGroupId)
        {
            MachineGroup mg;
            machineGroupInstances.TryGetValue(machineGroupId, out mg);
            if (mg == null)
                return;

            BlockingQueue<IProducible> queue;
            if (queues.TryGetValue(mg.Id, out queue))
            {
                if (queue.Count > 0)
                {
                    mg.CurrentStatus = MachineGroupAvailableStatuses.ProduceQueueAndPause;
                    logger.Log(LogLevel.Info, "Pausing Machine Group {0} Queue it had items: {1}.", machineGroupId, queue.ToString());
                }
                else if (mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline)
                {
                    PauseMachineGroup(mg);
                }
            }
        }

        public int GetQueueCount(MachineGroup mg)
        {
            BlockingQueue<IProducible> queue;
            if (queues.TryGetValue(mg.Id, out queue))
            {
                return queue.Count;
            }
            return 0;
        }

        private void PauseMachineGroup(MachineGroup mg)
        {
            mg.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupPaused;
        }

        /// <summary>
        /// Starts a thread that dispatches workflows when work is ready, creates a cancellation token so the thread can be canceled later 
        /// </summary>
        /// <param name="machineGroup"></param>
        private void StartProducibleConsumer(MachineGroup machineGroup)
        {
            var cancellationSource = new CancellationTokenSource();
            consumerHandles[machineGroup.Id] = cancellationSource;
            queues[machineGroup.Id] = new BlockingQueue<IProducible>(machineGroup.MaxQueueLength);

            var task = Task.Factory.StartNew(MachineGroupProducibleConsumer, new Tuple<MachineGroup, CancellationTokenSource>(machineGroup, cancellationSource), cancellationSource.Token);
            task.ContinueWith((t) =>
            {
                if (t.Exception != null)
                    logger.LogException(LogLevel.Error, "MachineGroupProducibleConsumer for {0}", t.Exception, machineGroup);
                logger.Log(LogLevel.Error, "MachineGroupProducibleConsumer stopped for machine group {0} task reason: {1} ", machineGroup, (t.IsCanceled ? "cancelled" : (t.IsFaulted ? "faulted" : "completed")));
                if (!t.IsCanceled && !cancellationSource.IsCancellationRequested)
                {
                    StartProducibleConsumer(machineGroup);
                }
            }, cancellationSource.Token);
        }

        private void MachineGroupProducibleConsumer(object obj)
        {
            var t = obj as Tuple<MachineGroup, CancellationTokenSource>;
            var machineGroup = t.Item1;
            var cancellationSource = t.Item2;

            logger.Log(LogLevel.Debug, "Machine group producible consumer for {0} started", machineGroup);
            var queue = queues[machineGroup.Id];
            try
            {
                // TODO: Refactor this (at least break it down to smaller methods) -- also needs more integration/unit test coverage
                while (!cancellationSource.IsCancellationRequested)
                {

                    if (machineGroup.CurrentStatus == MachineGroupAvailableStatuses.ProduceQueueAndPause)
                    {
                        var anythingToCreate = queue.Peek();
                        if (anythingToCreate == null)
                        {
                            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupPaused;
                        }
                        else
                        {
                            logger.Log(LogLevel.Debug, "Machine group ProduceQueueAndPause creating {0} on  {1}",
                                anythingToCreate, machineGroup);
                        }
                    }
                    else if (machineGroup.CurrentStatus == MachineGroupAvailableStatuses.ProduceCurrentAndPause)
                    {
                        var anythingToCreate = queue.Peek();
                        if (anythingToCreate == null)
                        {
                            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupPaused;
                        }
                        else if (anythingToCreate.ProducibleStatus == NotInProductionProducibleStatuses.AddedToMachineGroupQueue)
                        {
                            anythingToCreate.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                            queue.Take();
                            continue;
                        }
                    }
                    // See if we have room to work.
                    if (machineGroup.CurrentStatus != MachineGroupAvailableStatuses.MachineGroupOnline &&
                        machineGroup.CurrentStatus != MachineGroupAvailableStatuses.ProduceQueueAndPause &&
                        machineGroup.CurrentStatus != MachineGroupAvailableStatuses.ProduceCurrentAndPause)
                    {
                        logger.Log(LogLevel.Trace,
                            "Waiting for room in currently working queue or machine group to come online for {0},  Queue:{1}",
                            machineGroup, queue.ToString());

                        Thread.Sleep(1500);

                        if (machineGroup.CurrentStatus != MachineGroupAvailableStatuses.MachineGroupOnline &&
                            machineGroup.CurrentStatus != MachineGroupAvailableStatuses.ProduceQueueAndPause &&
                            machineGroup.CurrentStatus != MachineGroupAvailableStatuses.ProduceCurrentAndPause)
                            continue;
                    }
                    
                    var producible = queue.Peek();

                    //don't block here because if the task gets cancelled we just want to exit and this was holding up the thread.  Another option would be to send the blocking queue the cancellation token but we don't have control of that object creation here...
                    if (producible == null)
                    {
                        logger.Log(LogLevel.Trace, "No work in queue for {0}", machineGroup);
                        Thread.Sleep(500);
                        continue;
                    }
                    
                    if (machineGroup.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline
                        || machineGroup.CurrentStatus == MachineGroupAvailableStatuses.ProduceQueueAndPause
                        || machineGroup.CurrentStatus == MachineGroupAvailableStatuses.ProduceCurrentAndPause)
                    {
                        try
                        {
                            if (producible.ProducibleStatus == ProducibleStatuses.ProducibleRemoved)
                            {
                                logger.Log(LogLevel.Warning, "Producible {0} not sent to Machine Group because it has been removed");
                                continue;
                            }

                            logger.Log(LogLevel.Debug, "Machine group producible consumer for {0}, starting to produce {1}, Queue:{2}", 
                                machineGroup, 
                                producible, 
                                queue.ToString());

                            producible.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup;

                            //blocking call below so we know that the job is done or failed                            
                            machineGroupWorkflowDispatcher.DispatchCreateProducibleWorkflow(machineGroup, producible);

                            logger.Log(LogLevel.Debug,
                                "Machine Group Workflow completed for {0} finished with {1}",
                                machineGroup, producible);

                        }
                        catch (WorkflowTerminatedException e)
                        {
                            logger.Log(LogLevel.Info,
                                "Machine Group Workflow terminated with reason {0} for {1} on producible {2}",
                                e.Message, machineGroup, producible);
                        }
                        catch (ApplicationException aex)
                        {
                            producible.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;

                            userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, String.Format(
                                "Producible with CustomerUniqueId:{0} failed while being processed by the workflow '{1}'",
                                producible.CustomerUniqueId, Path.GetFileNameWithoutExtension(machineGroup.WorkflowPath)), string.Empty,
                                machineGroup.Id);

                            logger.Log(LogLevel.Error,
                                "{0} producible consumer failed while creating {1} using Workflow:{3} -- {2} ",
                                machineGroup, producible, aex, machineGroup.WorkflowPath);
                        }
                        catch (Exception e)
                        {
                            producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
                            userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, String.Format(
                                "Producible with CustomerUniqueId:{0} cannot be processed by the workflow '{1}'",
                                producible.CustomerUniqueId, Path.GetFileNameWithoutExtension(machineGroup.WorkflowPath)), string.Empty,
                                machineGroup.Id);


                            //todo: clear the queue here because we don't know what happened.  remove it for now otherwise we get in a loop trying to create the same item.
                            logger.Log(LogLevel.Error,
                                "{0} producible consumer excepted while creating {1} using Workflow:{3} -- {2} ",
                                machineGroup, producible, e, machineGroup.WorkflowPath);
                        }
                        finally
                        {
                            //Clear Queue could have removed the item, we don't really care... we just want it removed.
                            IProducible tempProducible = queue.Take();
                            if (tempProducible == null)
                                logger.Log(LogLevel.Warning, "{0} take failed.  This should have given us {1}", machineGroup, producible);
                            if (tempProducible != null && tempProducible.Id != producible.Id)
                                logger.Log(LogLevel.Warning, "{0} take did not return the same producible as expected.  Expected:{1} Took:{2}", machineGroup, producible, tempProducible);

                            if (tempProducible != null && (tempProducible.ProducibleStatus == ProducibleStatuses.ProducibleRemoved || tempProducible.ProducibleStatus == ErrorProducibleStatuses.ProducibleFailed))
                            {
                                PauseMachineGroup(machineGroup);
                            }
                            //Fill the queue
                            publisher.Publish(new Message
                            {
                                MessageType = MachineGroupMessages.GetWorkForMachineGroup,
                                MachineGroupId = machineGroup.Id
                            });
                        }
                    }

                }
                logger.Log(LogLevel.Debug, "{0} producible consumer cancelled", machineGroup);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Fatal, "Unexpected error in producible consumer for {0}", e, machineGroup);
                throw;
            }
        }

        private void EstablishMachineMonitoring(MachineGroup machineGroup)
        {
            var machineIds = machineGroup.ConfiguredMachines;
            var machines = new List<IMachine>();
            var updateMg = false;

            List<Tuple<Guid, IDisposable>> oldList;
            machineGroupMachineObservables.TryRemove(machineGroup.Id, out oldList);
            if (oldList != null)
                oldList.Select(o => o.Item2).ForEach(o => o.Dispose());

            machineGroup.CurrentStatusChangedObservable.DurableSubscribe(status =>
            {
                logger.Log(LogLevel.Debug, "{0} status changed to {1}", machineGroup.Alias, status);
                if (status == MachineGroupAvailableStatuses.MachineGroupOnline)
                    MachineGroupMessages.GetWorkForMachineGroup.PublishGetWorkFor(publisher, machineGroup.Id);

            }, logger);

            machineGroup.CurrentProductionStatusObservable.DurableSubscribe(status =>
            {
                if (status == MachineProductionStatuses.ProductionCompleted &&
                    machineGroup.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline)
                    MachineGroupMessages.GetWorkForMachineGroup.PublishGetWorkFor(publisher, machineGroup.Id);
            }, logger);

            //handled capabilities changed
            machineGroup.CurrentCapabilitiesChangedObservable.DurableSubscribe(cap =>
            {
                publisher.Publish(new Message
                {
                    MessageType = MachineGroupMessages.CapabilitiesChanged,
                    MachineGroupId = machineGroup.Id,
                    MachineGroupName = machineGroup.Alias
                });
                MachineGroupMessages.GetWorkForMachineGroup.PublishGetWorkFor(publisher, machineGroup.Id);
            }, logger);

            //Wire up to the machines in my group so the above defined machineGroup.MachineInGroup... get raised.
            foreach (var machineId in machineIds)
            {
                //TODO: this is very dangerous... If the machineservice is not loaded it will return null and remove the configured machine group at startup
                var machineInstance = machineService.FindById(machineId);
                if (machineInstance == null)
                {
                    //Machine was removed from system but MG not updated
                    updateMg = true;
                    continue;
                }

                MonitorMachineInstance(machineGroup, machineInstance);
                //get initial values of capabilities
                machineGroup.UpdateMachineGroupCapabilities(machineId, machineInstance.CurrentCapabilities);

            }

            if (updateMg)
            {
                machineGroup.ConfiguredMachines.Clear();
                machineGroup.ConfiguredMachines.AddRange(machines.Select(m => m.Id));
                Update(machineGroup);
            }

            //Get the current status (machines could already be running)
            machineGroup.SetMachineGroupStatusBasedOnMachines(machineService, logger, canGoOnline: false);

        }

        private void MonitorMachineInstance(MachineGroup machineGroup, IMachine machineInstance)
        {
            logger.Log(LogLevel.Debug, "{0} wireup to {1}", machineGroup.Alias, machineInstance.Id);

            List<Tuple<Guid, IDisposable>> oldList;
            machineGroupMachineObservables.TryGetValue(machineGroup.Id, out oldList);
            if (oldList == null)
            {
                oldList = new List<Tuple<Guid, IDisposable>>();
                machineGroupMachineObservables.TryAdd(machineGroup.Id, oldList);
            }

            oldList.Add(new Tuple<Guid, IDisposable>(machineInstance.Id, machineInstance.CurrentStatusChangedObservable
                 .DurableSubscribe(status =>
                 {
                     logger.Log(LogLevel.Trace, "{0} CurrentStatusChanged on {1} to {2}", machineGroup.Alias, machineInstance.Id, status);
                     machineGroup.SetMachineGroupStatusBasedOnMachines(machineService, logger, canGoOnline: false);
                 }, logger)));
            oldList.Add(new Tuple<Guid, IDisposable>(machineInstance.Id, machineInstance.CurrentCapabilitiesChangedObservable
               .DurableSubscribe(status => machineGroup.UpdateMachineGroupCapabilities(machineInstance.Id, status), logger)));
            oldList.Add(new Tuple<Guid, IDisposable>(machineInstance.Id, machineInstance.CurrentProductionStatusObservable
                .DurableSubscribe(status => machineGroup.SetMachineGroupProductionStatus(machineService), logger)));

            //seed capabilites for mg
            machineGroup.UpdateMachineGroupCapabilities(machineInstance.Id, machineInstance.CurrentCapabilities);// remove capabilities of this machine

        }

        private void RemoveMonitoringOfMachineInstances(MachineGroup machineGroup, Guid m)
        {
            logger.Log(LogLevel.Debug, "!!! {0} wiredown from MachineId:{1}", machineGroup.Alias, m);
            List<Tuple<Guid, IDisposable>> observables;
            machineGroupMachineObservables.TryGetValue(machineGroup.Id, out observables);
            machineGroup.UpdateMachineGroupCapabilities(m, null);// remove capabilities of this machine
            if (observables != null)
            {
                var item = observables.Where(o => o.Item1 == m);
                item.ToList().ForEach(t =>
                {
                    t.Item2.Dispose();
                    observables.Remove(t);
                });
                observables.RemoveAll(o => o.Item1 == m);
            }
        }

        public void Dispose()
        {
            foreach (var cancellationSource in consumerHandles.Values)
            {
                if (!cancellationSource.IsCancellationRequested)
                {
                    cancellationSource.Cancel();
                }
            }
            consumerHandles.Clear();
            consumerHandles = null;

            foreach (var queue in queues.Values)
            {
                queue.Dispose();
            }
            queues.Clear();
        }
    }
}
