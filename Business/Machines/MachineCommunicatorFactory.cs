﻿using System.IO;

using PackNet.Business.Settings;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Communication;
using PackNet.Communication.Communicators.EM;
using PackNet.Communication.Communicators.Fusion;
using PackNet.Communication.WebRequestCreator;
using System;
using System.Net;

namespace PackNet.Business.Machines
{

    public class MachineCommunicatorFactory : IMachineCommunicatorFactory
    {
        private readonly IPackNetServerSettings packNetServerSettings;

        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ILogger logger;
        private readonly IUserNotificationService userNotificationService;

        public MachineCommunicatorFactory(IPackNetServerSettings packNetServerSettings, IEventAggregatorPublisher publisher, IEventAggregatorSubscriber subscriber, ILogger logger, IUserNotificationService userNotificationService)
        {
            this.packNetServerSettings = packNetServerSettings;

            this.publisher = publisher;
            this.subscriber = subscriber;
            this.logger = logger;
            this.userNotificationService = userNotificationService;
        }

        public IMachineCommunicator GetMachineCommunicator(IMachine machine)
        {
            var fusionMachine = machine as FusionMachine;
            if (fusionMachine != null)
            {
                return GetFusionMachineCommunicator(fusionMachine);
            }

            var emMachine = machine as EmMachine;
            if (emMachine != null)
            {
                return GetEmMachineCommunicator(emMachine);
            }


            throw new Exception("Unable to get Machine communicator of unknown IMachine");
        }

        private IMachineCommunicator GetEmMachineCommunicator(EmMachine emMachine)
        {
            var webRequestCreator = GetWebRequestCreator(emMachine, EmPhysicalMachineSettings.CreateFromFile);
            var plcCommunicator = new PLCHeartBeatCommunicator(webRequestCreator, EmMachineVariables.Instance, publisher, logger);

            return new EmCommunicator(emMachine, publisher, subscriber, plcCommunicator, EmMachineVariables.Instance, logger);
        }

        private IMachineCommunicator GetFusionMachineCommunicator(FusionMachine fusionMachine)
        {
            var webRequestCreator = GetWebRequestCreator(fusionMachine, FusionPhysicalMachineSettings.CreateFromFile);
            var plcCommunicator = new PLCHeartBeatCommunicator(webRequestCreator, IqFusionMachineVariables.Instance, publisher,
                logger);

            return new FusionCommunicator(fusionMachine, this.publisher, this.subscriber, plcCommunicator,
                IqFusionMachineVariables.Instance, this.logger);
        }

        private WebRequestCreator GetWebRequestCreator<T>(IPacksizeCutCreaseMachine machine, Func<IPEndPoint, string, T> createFromFile) 
            where T : PhysicalMachineSettings
        {
            try
            {
                machine.PhysicalMachineSettings = createFromFile(GetCallbackSetting(), machine.PhysicalSettingsFilePath);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error,
                    string.Format("Unable to parse Physical machine settings file for machine with Alias: {0}, Exception: {1}",
                        machine.Alias, e.Message));
                userNotificationService.SendNotificationToSystem(
                    NotificationSeverity.Error,
                    string.Format("Unable to parse Physical machine settings file {0} for machine with Alias: {1}",
                        Path.GetFileNameWithoutExtension(machine.PhysicalSettingsFilePath), 
                        machine.Alias),
                    e.ToString());
                throw;
            }

            return new WebRequestCreator(machine.IpOrDnsName, machine.Port);
        }

        public void UpdateWebRequestCreator(IPacksizeCutCreaseMachine machine, IMachineCommunicator communicator)
        {
            if (machine is FusionMachine)
                communicator.UpdateWebRequestCreator(GetWebRequestCreator(machine, FusionPhysicalMachineSettings.CreateFromFile));
            else if (machine is EmMachine)
                communicator.UpdateWebRequestCreator(GetWebRequestCreator(machine, EmPhysicalMachineSettings.CreateFromFile));
        }

        public IPEndPoint GetCallbackSetting()
        {
            var globalSettings = packNetServerSettings.GetSettings();
            var ipEndpoint = new IPEndPoint(globalSettings.MachineCallbackIpAddress, globalSettings.MachineCallbackPort);
            return ipEndpoint;
        }
    }
}
