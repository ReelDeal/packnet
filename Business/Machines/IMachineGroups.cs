﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Business.Machines
{
    public interface IMachineGroups
    {
       
        MachineGroup Create(MachineGroup machineGroup);
        void Delete(MachineGroup machineGroup);
        /// <summary>
        /// Update the MG.  It must not be in production when the changes are made.
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <returns></returns>
        MachineGroup Update(MachineGroup machineGroup);
        MachineGroup Find(Guid machineGroupId);

        MachineGroup FindByAlias(string alias);

        IEnumerable<MachineGroup> GetMachineGroups();

        MachineGroup FindByMachineId(Guid id);

        MachineGroup FindByMachineGroupId(Guid id);

        /// <summary>
        /// Adds an item to the machine group queue
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <param name="producible"></param>
        /// <returns>true if the queue still has room for more items</returns>
        bool AddItemToQueue(MachineGroup machineGroup, IProducible producible);

        /// <summary>
        /// True if an item can be added to the queue
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <returns></returns>
        bool CanAddItemToQueue(MachineGroup machineGroup);

        /// <summary>
        /// Removes all items from the machine group queue
        /// </summary>
        /// <param name="machineGroup"></param>
        void ClearQueue(Guid machineGroup);

        void ToggleMachineGroupPaused(Guid mg);

        void PauseMachineGroup(Guid machineGroupId);

        void UpdateProductionMode(MachineGroup machineGroup);

        void SetAssignedOperator(Guid machineGroupId, string userName);

        void CompleteQueueAndPause(Guid machineGroupId);

        int GetQueueCount(MachineGroup mg);
    }
}