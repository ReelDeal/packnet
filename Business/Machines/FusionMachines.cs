﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Machines.Fusion;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Data.Machines;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PackNet.Business.Machines
{

    public class FusionMachines : MachinesBase<FusionMachine>, IFusionMachines
    {
        public FusionMachines(IFusionMachineRepository fusionMachineRepository, 
            IMachineCommunicatorFactory machineCommunicatorFactory,
            IServiceLocator serviceLocator,
            IEventAggregatorSubscriber subscriber, 
            ILogger logger)
            : base(fusionMachineRepository, machineCommunicatorFactory, serviceLocator, logger)
        {
            //Get all machines
            CreatedMachines.AddRange(fusionMachineRepository.All());

            //Start communication for each machine
            CreatedMachines.ForEach(SetupMachineCommunication);
            CreatedMachines.ForEach(UpdateCapabilities);

            PackNetServerSettingsMessages.ServerCallBackIPSettingsUpdated
                .OnMessage(subscriber, logger, 
                    message => CreatedMachines.ForEach(RestartMachine));
        }

        private void RestartMachine(FusionMachine machine)
        {
            RemoveMachineCommunicator(machine);
            SetupMachineCommunication(machine);
        }

        protected override void MachinePoweredOffCorrugateUpdate(FusionMachine localMachine, ITrackBasedCutCreaseMachine machine)
        {
            //todo: we have to tear down communication because when corrugates change it does something to the physical machine settings that are necessary when communication is established.
            RemoveMachineCommunicator(localMachine);

            // ConfigureTracksForSingleOrMultitrack(machine);
            machine.Tracks.ForEach(t => localMachine.LoadCorrugate(t, t.LoadedCorrugate));

            UpdateCapabilities(localMachine);

            //todo: don't resetablish is we fix the todo above.
            SetupMachineCommunication(localMachine);
            Update(localMachine, () => UpdateFusionSpecific(localMachine));
        }

        protected override void MachinePoweredOnCorrugateUpdate(FusionMachine localMachine, ITrackBasedCutCreaseMachine machine)
        {
            var communicator = GetCommunicator(localMachine.Id);

            // ConfigureTracksForSingleOrMultitrack(machine);
            machine.Tracks.ForEach(t => localMachine.LoadCorrugate(t, t.LoadedCorrugate));

            UpdateCapabilities(localMachine);

            communicator.SetChangeCorrugate(true);
            communicator.SetCorrugateWidths(localMachine.Tracks);
            communicator.SetCorrugateThicknesses(localMachine.Tracks.Select(t => t.LoadedCorrugate != null ? (double)t.LoadedCorrugate.Thickness : 0));

            Update(localMachine, () => UpdateFusionSpecific(localMachine));
        }
        
        private bool UpdateFusionSpecific(FusionMachine machine)
        {
            var communicator = GetCommunicator(machine.Id);
            
            if (communicator.IsConnected)
            {
                communicator.SetAlignments();
                communicator.SetNumberOfTracks(machine.NumTracks);
                communicator.SetChangeCorrugate(false);
            }

            return true;
        }

        public void PauseMachine(FusionMachine machine)
        {
            var communicator = GetCommunicator(machine.Id);

            communicator.PauseMachine();
            // do nothing more and wait for the physical machine to tell us when the state changes.
        }

        public void Produce(Guid machineId, IProducible producible, IEnumerable<IInstructionItem> instructionList)
        {

            var communicator = GetCommunicator(machineId);
            var machine = CreatedMachines.FirstOrDefault(m => m.Id == machineId);
            if (machine.CurrentProductionStatus != MachineProductionStatuses.ProductionIdle && machine.CurrentProductionStatus != MachineProductionStatuses.ProductionCompleted)
            {
                communicator.PauseMachine();
                machine.CurrentStatus = MachineErrorStatuses.MachineAlreadyInProductionError;
                var message = String.Format("System sent a carton to the machine when a carton is already in production: Producible {0}, Machine {1}", producible, machine);
                machine.Errors.Add(new MachineError(MachineErrorStatuses.MachineAlreadyInProductionError, new List<string> { message }));
                throw new Exception(message);
            }
            communicator.Produce(producible, instructionList);
            producible.ProduceOnMachineId = machineId;
        }
        
        public IFusionCommunicator GetCommunicator(Guid machineId)
        {
            var machineCommunicator = GetMachineCommunicator(machineId);
            return machineCommunicator as IFusionCommunicator;
        }
    }
}