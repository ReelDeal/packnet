﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Business.Machines
{
    public interface IPacksizeCutCreaseMachines<T> : IMachines<T> where T : IPacksizeCutCreaseMachine
    {
        void UpdateMachineSettings(T machine);

        void Produce(Guid machineId, IProducible producible, IEnumerable<IInstructionItem> instructionList);

        void ConfigureTracks(ITrackBasedCutCreaseMachine machine);

        bool CanProduce(T machine, IProducible producible);
    }
}
