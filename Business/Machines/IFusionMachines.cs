﻿using PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.Business.Machines
{
    public interface IFusionMachines: IPacksizeCutCreaseMachines<FusionMachine>
    {
        void PauseMachine(FusionMachine machine);
    }
}