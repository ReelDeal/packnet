﻿using System;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services.Machines;



namespace PackNet.Business.Machines
{
    public static class MachineGroupExtensions
    {
        /// <summary>
        /// Sets the current machine group status based on the status of the configured machines
        /// </summary>
        /// <param name="machineGroup">The machine group.</param>
        /// <param name="machineService">The machine service.</param>
        /// <param name="logger"></param>
        /// <param name="canGoOnline">if set to <c>true</c> [can go online].</param>
        public static void SetMachineGroupStatusBasedOnMachines(this MachineGroup machineGroup, IAggregateMachineService machineService, ILogger logger, bool canGoOnline = true)
        {
            var machines = machineService.Machines.Where(m => machineGroup.ConfiguredMachines.Contains(m.Id));
            MachineGroupStatuses newStatus = MachineGroupUnavailableStatuses.MachineGroupOffline;

            //If all machines are on-line (most likely condition in production) set on-line and bypass status queries
            if (machines.Any(m => m.CurrentStatus == MachinePausedStatuses.LightBarrierBroken))
            {
                newStatus = MachineGroupWarningStatuses.MachineGroupLightBarrierBroken;
            }
            else if (machines.Any() && machines.All(m => m.CurrentStatus == MachineStatuses.MachineOnline))
            {
                if (machineGroup.CurrentStatus is MachineGroupWarningStatuses)
                    newStatus = machineGroup.LastStatus;
                else if (canGoOnline)
                {
                    newStatus = machineGroup.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupPaused
                        ? MachineGroupAvailableStatuses.MachineGroupOnline
                        : MachineGroupAvailableStatuses.MachineGroupPaused;
                }
                else
                {
                    newStatus = MachineGroupAvailableStatuses.MachineGroupPaused;
                }
            }
            else
            {
                //Errors take priority here, if one machine is in error state the entire machine group is in error
                var machinesInError = machines.Where(m => m.CurrentStatus is MachineErrorStatuses);
                if (machinesInError.Any())
                {
                    newStatus = MachineGroupUnavailableStatuses.MachineGroupError;
                    logger.Log(LogLevel.Debug, "{0} moved to MachineGroupError because the following machines are in an error status {1}", machineGroup, String.Join(", ", machinesInError));
                }

                    //Next priority is off-line, same deal, any off-line then the machine group is off-line or we don't have any assigned machines.
                else if (!machines.Any() || machines.Any(m => m.CurrentStatus == MachineStatuses.MachineOffline))
                    newStatus = MachineGroupUnavailableStatuses.MachineGroupOffline;
                    
                    //Initializing next
                else if (machines.Any(m => m.CurrentStatus == MachineStatuses.MachineInitializing))
                    newStatus = MachineGroupUnavailableStatuses.MachineGroupInitializing;

                else if (machines.Any(m => m.CurrentStatus == MachinePausedStatuses.MachineChangingCorrugate))
                    newStatus = MachineGroupUnavailableStatuses.MachineGroupChangingCorrugate;

                    //Finally paused
                else if (machines.Any(m => m.CurrentStatus is MachinePausedStatuses))
                {
                    newStatus = machineGroup.CurrentProductionStatus != MachineProductionStatuses.ProductionIdle ? MachineGroupAvailableStatuses.ProduceCurrentAndPause : MachineGroupAvailableStatuses.MachineGroupPaused;
                }

            }

            machineGroup.CurrentStatus = newStatus;
        }

        public static void SetMachineGroupProductionStatus(this MachineGroup machineGroup, IAggregateMachineService machineService)
        {
            var machines = machineService.Machines.Where(m => machineGroup.ConfiguredMachines.Contains(m.Id));
            var newStatus = MachineProductionStatuses.ProductionIdle;

            //If all machines are on-line (most likely condition in production) set on-line and bypass status queries
            if (machines.Any() && machines.Any(m => m.CurrentProductionStatus is MachineProductionErrorStatuses))
            {
                newStatus = machines.First(m => m.CurrentProductionStatus is MachineProductionErrorStatuses).CurrentProductionStatus;
            }
            else if (machines.Any() && machines.Any(m => m.CurrentProductionStatus is MachineProductionInProgressStatuses))
            {
                newStatus = MachineProductionInProgressStatuses.ProductionInProgress;
            }
            else
            {
                newStatus = MachineProductionStatuses.ProductionIdle;
            }
            try
            {
               machineGroup.CurrentProductionStatus = newStatus;
            }
            catch
            {
                ;//todo: something about this it's always giving a write exception.
            }
            finally
            {
                ;
            }

        }
    }
}
