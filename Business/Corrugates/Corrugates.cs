﻿using System;
using System.Linq;

using PackNet.Common.Interfaces.Exceptions;

namespace PackNet.Business.Corrugates
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO.Corrugates;
    using Data.Corrugates;

    public class Corrugates : ICorrugates
    {
        private readonly ICorrugateRepository corrugateRepository;

        public Corrugates(ICorrugateRepository corrugateRepository)
        {            
            this.corrugateRepository = corrugateRepository;
        }

        public Corrugate Create(Corrugate corrugate)
        {
            if (corrugate.Id != Guid.Empty)
                throw new AlreadyPersistedException("Id");

            if(GetCorrugates().Any(c => c.Alias.Equals(corrugate.Alias)))
                throw new AlreadyPersistedException("Alias");

            ValidateNoIdenticalCorrugates(corrugate, false);

            corrugateRepository.Create(corrugate);

            return corrugate;
        }

        public IEnumerable<Corrugate> GetCorrugates()
        {
            return corrugateRepository.All();
        }

        public void Delete(Corrugate corrugate)
        {
            try
            {
                corrugateRepository.Delete(corrugate);
            }
            catch (Exception e)
            {
                throw new Exception("Could not Delete Corrugate(s)", e);
            }
        }

        public void Delete(IEnumerable<Corrugate> corrugates)
        {
            try
            {
                corrugateRepository.Delete(corrugates);
            }
            catch (Exception e)
            {
                throw new Exception("Could not Delete Corrugate(s)", e);
            }
        }

        public Corrugate Update(Corrugate updatedCorrugate, IEnumerable<Corrugate> loadedCorrugates)
        {
            if (updatedCorrugate.Id == Guid.Empty)
            {
                throw new Exception("NotExists");
            }

            if (GetCorrugates().Any(c => c.Alias.Equals(updatedCorrugate.Alias) && c.Id != updatedCorrugate.Id))
            {
                throw new AlreadyPersistedException("Alias");
            }

            ValidateNoIdenticalCorrugates(updatedCorrugate, true);

            if (loadedCorrugates.Any(c => c.Id == updatedCorrugate.Id))
                throw new EditOnLoadedCorrugateException("CurrentlyLoaded");
            
            return corrugateRepository.Update(updatedCorrugate);
        }

        public Corrugate Find(Guid id)
        {
            return corrugateRepository.Find(id);
        }
        
        /// <summary>
        /// Validates whether or not an identical corrugate exists in the database.
        /// </summary>
        /// <param name="corrugate">Corrugate to validate if it exists in the database</param>
        /// <param name="update">True if the corrugate is being updated, false if the corrugate is being created</param>
        /// <remarks>Will check all corrugates in the database, excluding the given corrugate, using the equals operator for the corrugate DTO</remarks>
        private void ValidateNoIdenticalCorrugates(Corrugate corrugate, bool update)
        {
            var allCorrugates = GetCorrugates().Where(c => c.Id != corrugate.Id);

            var identical = allCorrugates.FirstOrDefault(corr => corr.Equals(corrugate));

            if (identical != null)
            {
                throw new AlreadyPersistedException(string.Format("Properties, {0}", identical.Alias));
            }
        }
    }
}