﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Business.Corrugates
{
    public class OptimalCorrugateCalculator : IOptimalCorrugateCalculator
    {
        private readonly ILogger logger;
        private readonly IPackagingDesignManager designMgr;
        private readonly IServiceLocator serviceLocator;
        private IAggregateMachineService machineService;
        private readonly IMachineGroupService machineGroupService;
        private IUserNotificationService userNotificationService;
        private IEventAggregatorPublisher eventAggregatorPublisher;

        // string for carton as first key, then the corrugate will have two carton on corrugates one with rotation and the other without.
        private readonly ConcurrentDictionary<string, Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>>> cache = new ConcurrentDictionary<string, Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>>>();

        public IAggregateMachineService MachineService
        {
            get { return machineService ?? (machineService = serviceLocator.Locate<IAggregateMachineService>()); }
        }

        public IUserNotificationService UserNotificationService
        {
            get { return userNotificationService ?? (userNotificationService = serviceLocator.Locate<IUserNotificationService>()); }
        }

        public OptimalCorrugateCalculator(
            IPackagingDesignManager designMgr,
            IServiceLocator serviceLocator,
            IMachineGroupService machineGroupService)
        {
            this.designMgr = designMgr;
            this.serviceLocator = serviceLocator;
            this.machineGroupService = machineGroupService;
            eventAggregatorPublisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            logger = serviceLocator.Locate<ILogger>();
        }

        public IEnumerable<CartonOnCorrugate> GetOptimalCorrugates(
            IEnumerable<Corrugate> corrugatesToCompare,
            IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate,
            ICarton carton,
            bool recalculateCachedItem = false, int allowedTileCount = Int32.MaxValue)
        {
            var cartonOnCorrugates = new List<CartonOnCorrugate>();
            var cartonOnCorrugateWithMaxTiles = GetOptimalCorrugate(corrugatesToCompare, machinesToEvaluate, carton, allowedTileCount, recalculateCachedItem);

            if (cartonOnCorrugateWithMaxTiles != null)
            {
                cartonOnCorrugates.Add(cartonOnCorrugateWithMaxTiles);
                var maxTiles = cartonOnCorrugateWithMaxTiles.TileCount;

                while (maxTiles > 1)
                    cartonOnCorrugates.Add(
                        GetOptimalCorrugate(corrugatesToCompare, machinesToEvaluate, carton, --maxTiles, recalculateCachedItem));
            }
            return cartonOnCorrugates;
        }

        public CartonOnCorrugate GetOptimalCorrugate(
            IEnumerable<Corrugate> corrugatesToCompare,
            IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate,
            ICarton carton,
            int allowedTileCount,
            bool recalculateCachedItem = false)
        {
            if (designMgr.HasDesignWithId(carton.DesignId) == false)
            {
                //TODO, this will produce one notification for each carton that failes due to a missing design. This should probably be filtered somewhere before we end up here
                //But since i don't feel comfortable doing big changes to the code base at this time i consider this acceptable 22/10/15 (Staples London emergencies)

                var userFriendlyMessage = string.Format("The item '{0}' could not be created since a design with ID: {1} could not be found within the system. ", carton.CustomerUniqueId, carton.DesignId);

                logger.Log(LogLevel.Warning, userFriendlyMessage);

                var firstMachineToEvaluate = machinesToEvaluate.FirstOrDefault();

                if (firstMachineToEvaluate != null && firstMachineToEvaluate.Id != Guid.Empty)
                {
                    var machineGroup = machineGroupService.FindByMachineId(firstMachineToEvaluate.Id);

                    if (machineGroup != null)
                    {
                        UserNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, userFriendlyMessage,
                            string.Empty, machineGroup.Id);
                    }
                }

                return null;
            } 

            if (corrugatesToCompare == null || !corrugatesToCompare.Any()) return null;

            corrugatesToCompare =
                corrugatesToCompare.Where(
                    c => carton.CorrugateQuality.HasValue == false || carton.CorrugateQuality.Value == c.Quality);

            var cacheKey = CreateCacheKey(carton);

            Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>> cachedItem = GetOrCreateCartonCacheValue(cacheKey, recalculateCachedItem);

            var rotationRestriction = carton.Restrictions.OfType<MustProduceWithSpecifiedRotationRestriction>().FirstOrDefault();
            if (rotationRestriction == null)
                return GetBestCorrugateForBox(corrugatesToCompare, machinesToEvaluate, cacheKey, cachedItem, carton, allowedTileCount);

            var bestCorrugate = GetBestCorrugateForBoxWithRotation(corrugatesToCompare, machinesToEvaluate, cacheKey, cachedItem, carton, allowedTileCount, rotationRestriction.Orientation.Value);

            if (bestCorrugate == null)
            {
                var firstMachineToEvaluate = machinesToEvaluate.FirstOrDefault();
                if (firstMachineToEvaluate != null)
                {
                    var machineGroup = machineGroupService.FindByMachineId(firstMachineToEvaluate.Id);
                    var userFriendlyMessage = string.Format("{0} no machines can produce when rotated on Corrugate:{1} with TileCount:{2}", carton, corrugatesToCompare.FirstOrDefault().Alias, allowedTileCount);
                    UserNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, userFriendlyMessage, string.Empty, machineGroup.Id);
                }
            }
            return bestCorrugate;
        }

        private CartonOnCorrugate GetBestCorrugateForBox(IEnumerable<Corrugate> corrugatesToCompare, IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate, string cacheKey, Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>> cachedItem, ICarton carton, int allowedTileCount)
        {
            var notRotatedCartonOnCorrugate = GetBestCorrugateForBoxWithRotation(corrugatesToCompare, machinesToEvaluate, cacheKey, cachedItem, carton, allowedTileCount, OrientationEnum.Degree0);
            var rotatedCartonOnCorrugate = GetBestCorrugateForBoxWithRotation(corrugatesToCompare, machinesToEvaluate, cacheKey, cachedItem, carton, allowedTileCount, OrientationEnum.Degree90);

            if (notRotatedCartonOnCorrugate != null && rotatedCartonOnCorrugate != null)
                return notRotatedCartonOnCorrugate.Yield > rotatedCartonOnCorrugate.Yield
                    ? notRotatedCartonOnCorrugate
                    : rotatedCartonOnCorrugate;

            if (notRotatedCartonOnCorrugate != null)
                return notRotatedCartonOnCorrugate;

            if (rotatedCartonOnCorrugate != null)
                return rotatedCartonOnCorrugate;

            LogAndNotifyFailureToProduce(carton, corrugatesToCompare, machinesToEvaluate);

            return null;
        }

        private void LogAndNotifyFailureToProduce(ICarton carton, IEnumerable<Corrugate> corrugatesToCompare, IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate)
        {
            string logMessage;
            string userFriendlyMessage;
            var firstCorrugate = corrugatesToCompare.FirstOrDefault();

            if (firstCorrugate == null)
            {
                logMessage = string.Format( "The item '{0}' could not be created on the currently loaded corrugates.", carton.CustomerUniqueId);
                userFriendlyMessage = logMessage;
            }
            else
            {
                var cartonWidthNotRotated = designMgr.CalculateUsage(carton, firstCorrugate, 1, false);
                var cartonWidthRotated = designMgr.CalculateUsage(carton, firstCorrugate, 1, true);

                var currentCorrugates = string.Join(", ", corrugatesToCompare.Select(c => c.Width.ToString()));
                var rotateWidthMessage = (cartonWidthRotated.Width == PackagingDesignCalculation.MaxValue.Width) ? string.Empty : " and " + cartonWidthRotated.Width + " when rotated";
                logMessage = string.Format(
                    "Unable to find CartonOnCorrugate for {0}. Width is {1} when not rotated and {2} when rotated. Available corrugate widths are: {3}.",
                    carton,
                    cartonWidthNotRotated.Width,
                    rotateWidthMessage,
                    currentCorrugates);

                userFriendlyMessage =
                    string.Format(
                        "The item '{0}' could not be created on the currently loaded corrugates. Carton width is {1}{2}, available corrugate widths are: {3}.",
                        carton.CustomerUniqueId,
                        cartonWidthNotRotated.Width,
                        rotateWidthMessage,
                        currentCorrugates);
            }


            logger.Log(LogLevel.Warning, logMessage);

            var firstMachineToEvaluate = machinesToEvaluate.FirstOrDefault();

            if (firstMachineToEvaluate != null && firstMachineToEvaluate.Id != Guid.Empty)
            {
                var machineGroup = machineGroupService.FindByMachineId(firstMachineToEvaluate.Id);

                if (machineGroup != null)
                {
                    UserNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, userFriendlyMessage,
                        string.Empty, machineGroup.Id);
                }
            }
        }

        private CartonOnCorrugate GetBestCorrugateForBoxWithRotation(IEnumerable<Corrugate> corrugatesToCompare, IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate, string cacheKey, Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>> cachedItem, ICarton carton, int allowedTileCount, OrientationEnum rotation)
        {
            CartonOnCorrugate bestCartonOnCorrugateForRotation = null;
            foreach (var corrugate in corrugatesToCompare)
            {
                var coc = GetCartonOnCorrugateForRotation(carton, cachedItem, cacheKey, corrugate, allowedTileCount, machinesToEvaluate, rotation);
                
                if (coc != null && (bestCartonOnCorrugateForRotation == null || coc.Yield > bestCartonOnCorrugateForRotation.Yield))
                    bestCartonOnCorrugateForRotation = coc;
            }

            return bestCartonOnCorrugateForRotation;
        }

        private CartonOnCorrugate GetCartonOnCorrugateForRotation(ICarton carton, Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>> cachedItem, string cacheKey, Corrugate corrugate, int allowedTileCount, IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate, OrientationEnum orientation)
        {
            // For some reason we are getting Not Loaded Corrugates. 
            // This is a dirty hack, we should never get here. Bug 18764
            if (corrugate.Id == Guid.Empty)
            {
                return null;
            }

            var leastCorrugateUsage = Double.MaxValue;
            CartonOnCorrugate bestCartonOnCorrugateForRotation = null;

            CartonOnCorrugate cartonOnCorrugate = GetOrCreateCartonOnCorrugateCacheValue(carton, cachedItem, cacheKey, corrugate, allowedTileCount, orientation.IsRotated());

            if (cartonOnCorrugate.AreaOnCorrugate != 0)
            {
                var tileCountToUse = allowedTileCount < cartonOnCorrugate.TileCount
                    ? allowedTileCount
                    : cartonOnCorrugate.TileCount;
                while (tileCountToUse > 0)
                {
                    var usageWithTile = cartonOnCorrugate.AreaOnCorrugate / tileCountToUse;
                    if (usageWithTile < leastCorrugateUsage)
                    {
                        var machines = GetMachinesProducibleOn(carton, corrugate, machinesToEvaluate, tileCountToUse, orientation.IsRotated());
                        machines.ForEach(m => cartonOnCorrugate.ProducibleMachines.Add(m.Id, m.Alias));
                        if (cartonOnCorrugate.ProducibleMachines.Any())
                        {
                            leastCorrugateUsage = usageWithTile;
                            bestCartonOnCorrugateForRotation = cartonOnCorrugate;
                            bestCartonOnCorrugateForRotation.TileCount = tileCountToUse;

                            logger.Log(LogLevel.Trace, "{0} found machines that can produces when NotRotated on Corrugate:{1} with TileCount:{2}.  CartonOnCorrugate:{3}", carton, corrugate.Alias, tileCountToUse, cartonOnCorrugate);
                        }
                        else
                        {
                            logger.Log(LogLevel.Trace, "{0} no machines can produces when NotRotated on Corrugate:{1} with TileCount:{2}", carton, corrugate.Alias, tileCountToUse);
                        }
                    }

                    tileCountToUse--;
                }
            }

            return bestCartonOnCorrugateForRotation;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="carton"></param>
        /// <param name="cachedItem"></param>
        /// <param name="cacheKey"></param>
        /// <param name="corrugate"></param>
        /// <param name="allowedTileCount"></param>
        /// <param name="isRotated">true if we should attempt to rotate for best corrugate</param>
        /// <returns></returns>
        private CartonOnCorrugate GetOrCreateCartonOnCorrugateCacheValue(
            ICarton carton,
            Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>> cachedItem,
            string cacheKey,
            Corrugate corrugate,
            int allowedTileCount,
            bool isRotated)
        {
            IEnumerable<CartonOnCorrugate> rotatedAndNotCartonOnCorrugate;
            cachedItem.TryGetValue(corrugate, out rotatedAndNotCartonOnCorrugate);
            if (rotatedAndNotCartonOnCorrugate == null)
            {
                // Load up our cache value w/ rotated and not
                var cartonAreaOnCorrugate = designMgr.CalculateUsage(carton, corrugate, allowedTileCount, true);
                var cartonOnCorrugate1 = new CartonOnCorrugate(carton as Carton, corrugate, cacheKey, cartonAreaOnCorrugate, OrientationEnum.Degree90);
                cartonAreaOnCorrugate = designMgr.CalculateUsage(carton, corrugate, allowedTileCount, false);
                var cartonOnCorrugate2 = new CartonOnCorrugate(carton as Carton, corrugate, cacheKey, cartonAreaOnCorrugate, OrientationEnum.Degree0);
                rotatedAndNotCartonOnCorrugate = new List<CartonOnCorrugate> { cartonOnCorrugate1, cartonOnCorrugate2 };
                cachedItem.Add(corrugate, rotatedAndNotCartonOnCorrugate);
            }

            var cocToUse = rotatedAndNotCartonOnCorrugate.First(c => c.Rotated == isRotated).Clone() as CartonOnCorrugate;
            if (carton.Rotation.HasValue)
                cocToUse.Rotation = carton.Rotation.Value;

            return cocToUse;
        }

        private Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>> GetOrCreateCartonCacheValue(string cacheKey, bool recalculateCachedItem)
        {

            Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>> cachedItem = null;


            if (recalculateCachedItem)
            {
                //this.cache.Remove(cacheKey);
                cache.TryRemove(cacheKey, out cachedItem);
                cachedItem = null;
            }

            cache.TryGetValue(cacheKey, out cachedItem);

            if (cachedItem == null)
            {
                cachedItem = new Dictionary<Corrugate, IEnumerable<CartonOnCorrugate>>();
                cache.TryAdd(cacheKey, cachedItem);
            }


            return cachedItem;
        }

        private IEnumerable<IPacksizeCutCreaseMachine> GetMachinesProducibleOn(ICarton carton, Corrugate corrugate, IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate, int tileCountToUse, bool rotate)
        {
            //Lock added to (hopefully) address TFS 9597:3.0 BoxFirst 800 carton dropfile crashes.
            // Is lock needed since the restrictions are in a concurrentlist?
            lock (carton)
            {
                carton.Restrictions.RemoveAll(r => r is CanProduceWithRotationRestriction ||
                    r is CanProduceWithTileCountRestriction ||
                    r is CanProduceWithCorrugateRestriction);
                carton.Restrictions.Add(new CanProduceWithTileCountRestriction(tileCountToUse));
                carton.Restrictions.Add(new CanProduceWithRotationRestriction(rotate));
                carton.Restrictions.Add(new CanProduceWithCorrugateRestriction(corrugate));

                //is this a machine specific item.
                //todo: does it make sense to have a machine specific restriction?  Machine Group makes sense but not machine, right?
                var machineSpecificRestriction = carton.Restrictions.OfType<MachineSpecificRestriction>().FirstOrDefault();

                return GetMachineToEvaluate(machinesToEvaluate, (machineSpecificRestriction != null ? machineSpecificRestriction.Value as Guid? : null))
                    .Where(machine => MachineService.CanProduce(machine.Id, carton))
                    .ToList();
            }
        }

        private IEnumerable<IPacksizeCutCreaseMachine> GetMachineToEvaluate(IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate, Guid? machineGroupId)
        {
            if (machineGroupId.HasValue)
            {
                return machineGroupService.GetMachinesInGroup(machineGroupId.Value)
                    .Where(machine => machinesToEvaluate.Any(machineToEval => machineToEval.Id == machine.Id))
                    .OfType<IPacksizeCutCreaseMachine>();
            }

            return machinesToEvaluate;
        }

        private string CreateCacheKey(ICarton carton)
        {
            var key = carton.DesignId + ":" + carton.Length + ":" + carton.Width + ":" + carton.Height;
            carton.XValues.Values.ForEach(x => key += ":" + x);
            return key;
        }
    }
}
