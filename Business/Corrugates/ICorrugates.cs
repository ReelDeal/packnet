﻿using System;

namespace PackNet.Business.Corrugates
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO.Corrugates;

    public interface ICorrugates
    {
        Corrugate Create(Corrugate corrugate);
        void Delete(Corrugate corrugate);
        void Delete(IEnumerable<Corrugate> corrugate);
        Corrugate Update(Corrugate corrugate, IEnumerable<Corrugate> loadedCorrugates);
        Corrugate Find(Guid corrugate);

        IEnumerable<Corrugate> GetCorrugates();
    }
}
