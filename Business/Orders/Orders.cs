﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;
using PackNet.Data.Orders;

namespace PackNet.Business.Orders
{
    public class Orders : IOrders
    {
        private readonly IOrdersRepository ordersRepository;

        public Orders(IOrdersRepository ordersRepository)
        {
            this.ordersRepository = ordersRepository;
        }

        public Order Create(Order order)
        {
            ordersRepository.Create(order);
            return order;
        }

        public void Delete(Order order)
        {
            ordersRepository.Delete(order);
        }

        public void Delete(IEnumerable<Order> orders)
        {
            orders.ForEach(Delete);
        }

        public Order Update(Order order)
        {
            return ordersRepository.UpdateWithRetries(order);
        }

        ///// <summary>
        /// This method was introduced to partially address TFS 10256. I explicitily _do not_ call Retry because I want to get a log of how long it took and how many times we
        ///// had to try.  NOTE: If this fails past the maxTries amount of times you will end up getting your original input back as your result.
        ///// </summary>
        ///// <param name="order"></param>
        ///// <param name="maxTries"></param>
        ///// <returns></returns>
        //public Order UpdateWithRetries(Order order, int maxTries = 50)
        //{
        //    Stopwatch stopwatch = Stopwatch.StartNew();
        //    bool saved = false;
        //    int tries = 0;
        //    Order orderSaveResult = order;
        //    //NOTE: If this fails past the maxTries amount of times you will end up getting your original input back as your result.

        //    //Please do not change this to a Retry
        //    while (!saved && tries < maxTries)
        //    {
        //        try
        //        {
        //            tries++;
        //            orderSaveResult = ordersRepository.Update(order);
        //            saved = true;
        //        }
        //        catch (InvalidOperationException)
        //        {
        //            saved = false;
        //        }
        //        catch (AggregateException)
        //        {
        //            saved = false;
        //        }
        //    }

        //    Trace.WriteLine(String.Format("Took {0} ms and {1} tries for db Update to finally stick for {2} and the result for save was {3}", stopwatch.ElapsedMilliseconds, tries, order.Id, saved));
        //    return orderSaveResult;
        //}

        public Order Find(Guid orderId)
        {
            return ordersRepository.Find(orderId);
        }

        public Order Find(string customerUniqueId)
        {
            return ordersRepository.Find(customerUniqueId);
        }

        public IEnumerable<Order> Search(string critera, int limit = -1)
        {
            return ordersRepository.Search(critera, limit);
        }

        public IEnumerable<Order> GetOpenOrders(int qty = 20)
        {
            return ordersRepository.GetOpenOrders(qty);
        }

        public IEnumerable<Order> GetCompletedOrders(int qty = 20)
        {
            return ordersRepository.GetCompletedOrders(qty);
        }

        public IEnumerable<Order> GetCompletedOrdersFor(ProductionGroup productionGroup, MachineGroup machineGroup, int qty = 20)
        {
            // If the MG is in Auto Mode, search for the orders that the MG has worked on, if not, search for the orders that has the MG Specific restriction.

            return machineGroup.ProductionMode == MachineGroupProductionModes.ManualProductionMode
                ? ordersRepository.GetCompletedOrdersForRestrictionId(machineGroup.Id, qty)
                : ordersRepository.GetCompletedOrdersForRestrictionId(productionGroup.Id, qty); 
        }

        public IEnumerable<Order> GetOpenOrdersFor(ProductionGroup productionGroup, MachineGroup machineGroup, int qty = 20)
        {
            // If the MG is in Auto Mode, search for the orders that the MG has worked on, if not, search for the orders that has the MG Specific restriction.

            return machineGroup.ProductionMode == MachineGroupProductionModes.ManualProductionMode
                ? ordersRepository.GetOpenOrdersForRestrictionId(machineGroup.Id, qty)
                : ordersRepository.GetOpenOrdersForRestrictionId(productionGroup.Id, qty);
        }

        public IEnumerable<Order> GetAllNotProducibleOrders()
        {
            return ordersRepository.GetAllNotProducibleOrders();
        }

        public IEnumerable<Order> GetOrders()
        {
            return ordersRepository.All();
        }

        public IEnumerable<Order> GetAllOpenOrders()
        {
            return ordersRepository.GetAllOpenOrders();
        }
    }

    public interface IOrders
    {
        Order Create(Order order);
        void Delete(Order order);
        void Delete(IEnumerable<Order> orders);
        Order Update(Order order);
        //Order UpdateWithRetries(Order order, int maxTries = 50);
        Order Find(Guid orderId);
        Order Find(string customerUniqueId);
        IEnumerable<Order> Search(string critera, int limit = -1);
        IEnumerable<Order> GetOrders();
        IEnumerable<Order> GetOpenOrders(int qty = 20);

        IEnumerable<Order> GetCompletedOrdersFor(ProductionGroup productionGroup, MachineGroup machineGroup, int qty = 20);
        IEnumerable<Order> GetOpenOrdersFor(ProductionGroup productionGroup, MachineGroup machineGroup, int qty = 20);

        IEnumerable<Order> GetAllNotProducibleOrders();
        IEnumerable<Order> GetCompletedOrders(int qty = 20);
        IEnumerable<Order> GetAllOpenOrders();
    }
}
