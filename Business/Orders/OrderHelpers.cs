﻿using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace PackNet.Business.Orders
{
    using System.Collections.Generic;
    using System.Linq;
    using Common.Interfaces.DTO;
    using Common.Interfaces.DTO.Carton;
    using Common.Interfaces.DTO.Corrugates;
    using PackagingDesigns;

    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;

    public static class OrderHelpers
    {
        public static PhysicalDesign MergeDesignsSideBySide(
            IEnumerable<ICarton> cartons,
            Corrugate corrugate,
            bool alignInEnd,
            IPackagingDesignManager designManager,
            bool rotateDesign = false)
        {
            // using the first carton in our list we will clone that design as our main design and add all other designs lines to this cloned design. 
            var firstCarton = cartons.ElementAt(0);

            var longestDesignLength = cartons.Max(c => designManager.CalculateUsage(c, corrugate, cartons.Count(), rotateDesign).Length);

            var rotation = rotateDesign ? OrientationEnum.Degree90 : OrientationEnum.Degree0;
            if (firstCarton.Rotation.HasValue)
                rotation = firstCarton.Rotation.Value;

            var mergedDesigns = designManager.GetPhyscialDesignForCarton(firstCarton, corrugate, rotation, cartons.Count());

            if (alignInEnd && mergedDesigns.Length < longestDesignLength)
            {
                SetOffsetLengthDelta(mergedDesigns, longestDesignLength - mergedDesigns.Length);
            }

            AdjustOutOfBoundsLines(mergedDesigns);

            // do the rest of the cartons
            for (int index = 1; index < cartons.Count(); index++)
            {
                var carton = cartons.ElementAt(index);
                var tile = designManager.GetPhyscialDesignForCarton(carton, corrugate, rotation, cartons.Count());

                AdjustOutOfBoundsLines(tile);
                SetOffsetLeftDelta(tile, mergedDesigns.Width);

                if (alignInEnd && tile.Length < longestDesignLength)
                {
                    SetOffsetLengthDelta(tile, longestDesignLength - tile.Length);
                }

                tile.Lines.ForEach(mergedDesigns.Add);
                mergedDesigns.Width = mergedDesigns.Width + tile.Width;
            }

            mergedDesigns.Length = longestDesignLength;

            // remove any nunatab lines we don't need
            // we are removing all nunatabs for now because fusion can not do it and PS does it for us.  As per work w/ AndreasH and RobL 6/13/2013
            //mergedDesigns.Lines = mergedDesigns.Lines.Where(l => KeepLine(l, mergedDesigns.WidthOnZFold, mergedDesigns.LengthOnZFold)).ToList();

            return mergedDesigns;
        }

        private static void SetOffsetLengthDelta(PhysicalDesign design, MicroMeter offset)
        {
            foreach (var line in design.Lines)
            {
                line.StartCoordinate.Y = line.StartCoordinate.Y + offset;

                line.EndCoordinate.Y = line.EndCoordinate.Y + offset;
            }
        }


        /// <summary>
        /// Shift all x line values to the right by offset amount.
        /// </summary>
        /// <param name="design"></param>
        /// <param name="offset"></param>
        private static void SetOffsetLeftDelta(PhysicalDesign design, MicroMeter offset)
        {
            foreach (var line in design.Lines)
            {
                line.StartCoordinate.X = line.StartCoordinate.X + offset;
                line.EndCoordinate.X = line.EndCoordinate.X + offset;
            }
        }

        /// <summary>
        /// Removes lines completely outside the designs width and length or shortens lines partly outside the design.
        /// </summary>
        private static void AdjustOutOfBoundsLines(PhysicalDesign design)
        {
            LinesOutsideDesignRule.ApplyTo(design);
        }
    }
}
