namespace PackNet.Business.CodeGenerator
{
    using Common.Interfaces.DTO;

    public class LongHeadInformation
    {
        public LongHeadInformation(int longHeadNumber, MicroMeter leftSideToTool, MicroMeter leftSideToSensorPin)
        {
            LongHeadNumber = longHeadNumber;
            LeftSideToSensorPin = leftSideToSensorPin;
            LeftSideToTool = leftSideToTool;
        }

        public int LongHeadNumber { get; private set; }

        public MicroMeter LeftSideToTool { get; private set; }

        public MicroMeter LeftSideToSensorPin { get; private set; }
    }
}