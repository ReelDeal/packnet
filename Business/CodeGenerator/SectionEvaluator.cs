﻿using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Business.CodeGenerator.AStarStuff;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator
{
    public class SectionEvaluator : ISectionEvaluator
    {
        private ISuccessorCreator successorCreator;

        protected static int CachingThreshold = 10;

        protected readonly IDictionary<Tuple<MicroMeter, MicroMeter>, IAStarNodeEM> CachedSolutions;

        public SectionEvaluator()
        {
            this.successorCreator = new EmSuccessorCreator();
            CachedSolutions = new Dictionary<Tuple<MicroMeter, MicroMeter>, IAStarNodeEM>();
        }

        public SectionEvaluator(ISuccessorCreator successorCreator)
        {
            this.successorCreator = successorCreator;
            CachedSolutions = new Dictionary<Tuple<MicroMeter, MicroMeter>, IAStarNodeEM>();
        }

        public LongHeadDistributionSolution GetLongHeadDistribution(EmPhysicalMachineSettings machineParameters, MicroMeter[] longHeadPositions,
            SectionPermutation sections, int originalSectionCount, RuleAppliedPhysicalDesign design, double scoreBestExpandedSolution)
        {
            return GetLongHeadDistribution(machineParameters, longHeadPositions, sections.Sections, originalSectionCount, design, scoreBestExpandedSolution);
        }

        public LongHeadDistributionSolution GetLongHeadDistribution(EmPhysicalMachineSettings machineParameters, MicroMeter[] longHeadPositions,
            IEnumerable<Section> sections, int originalSectionCount, RuleAppliedPhysicalDesign design, double scoreBestExpandedSolution)
        {
            var bestSectionSolution = FindSolutionForSectionPermutation(machineParameters, longHeadPositions,
             sections, new List<IAStarNodeEM>(), CachedSolutions, originalSectionCount, design, scoreBestExpandedSolution, 0);

            if (bestSectionSolution == null)
                return null;
            var longheadDistribution = new LongHeadDistributionSolution();
            longheadDistribution.Sections = sections.ToList();

            bestSectionSolution.ForEach(longheadDistribution.AddNode);

            return longheadDistribution;
        }

        private List<IAStarNodeEM> FindSolutionForSectionPermutation(EmPhysicalMachineSettings machineParameters,
            MicroMeter[] longHeadPositions, IEnumerable<Section> sections, List<IAStarNodeEM> solutions,
            IDictionary<Tuple<MicroMeter, MicroMeter>, IAStarNodeEM> cachedSolutions, int originalSectionCount,
            RuleAppliedPhysicalDesign design, double scoreBestExpandedSolution, double scoreOfEvaluatedSections)
        {
            if (!sections.Any())
            {
                return solutions;
            }

            IAStarNodeEM solution;

            if (originalSectionCount > CachingThreshold)
                solution = GetSolutionFromCache(machineParameters, longHeadPositions, sections.First(), cachedSolutions, design,
                    scoreBestExpandedSolution, scoreOfEvaluatedSections);
            else
                solution = FindSolution(machineParameters, longHeadPositions, sections.First(), design,
                    (a) => a.Score + scoreOfEvaluatedSections >= scoreBestExpandedSolution);

            if (solution != null)
            {
                scoreOfEvaluatedSections += solution.Score;
                solutions.Add(solution);
            }
            else
                return null;

            return FindSolutionForSectionPermutation(machineParameters, solutions.Last().NewLongHeadPositions, sections.Skip(1),
                solutions, cachedSolutions, originalSectionCount, design, scoreBestExpandedSolution, scoreOfEvaluatedSections);
        }

        private IAStarNodeEM GetSolutionFromCache(EmPhysicalMachineSettings machineParameters, MicroMeter[] longHeadPositions,
            Section section, IDictionary<Tuple<MicroMeter, MicroMeter>, IAStarNodeEM> cachedSolutions, RuleAppliedPhysicalDesign design,
            double scoreBestExpandedSolution, double scoreOfEvaluatedSections)
        {
            IAStarNodeEM solution;

            if (scoreOfEvaluatedSections >= scoreBestExpandedSolution)
            {
                return null;
            }

            if (cachedSolutions.TryGetValue(new Tuple<MicroMeter, MicroMeter>(section.StartCoordinateY, section.EndCoordinateY),
                out solution))
            {
                return solution;
            }

            solution = FindSolution(machineParameters, longHeadPositions, section, design,
                (a) => false);

            if(solution != null)
                cachedSolutions.Add(new Tuple<MicroMeter, MicroMeter>(section.StartCoordinateY, section.EndCoordinateY), solution);

            return solution;
        }

        private IAStarNodeEM FindSolution(EmPhysicalMachineSettings machineParameters, MicroMeter[] longHeadPositions,
            Section section, RuleAppliedPhysicalDesign design, Func<IAStarNodeEM, bool> abortBranchEvaluationCriteria)
        {
            var start = AStarNodeEM.CreateStartNode(machineParameters, longHeadPositions, section, design);
            var openList = new SortedEmNodeQueue();
            openList.Enqueue(start);

            return ReturnSolution(openList, machineParameters, section, design, abortBranchEvaluationCriteria);
        }

        private IAStarNodeEM ReturnSolution(SortedEmNodeQueue openList, EmPhysicalMachineSettings machineSettings, Section section,
            RuleAppliedPhysicalDesign design, Func<IAStarNodeEM, bool> abortBranchEvaluationCriteria)
        {
            IAStarNodeEM solution = null;
            while (openList.Empty == false)
            {
                var currentNode = openList.Dequeue();

                //if (currentNode.Score + scoreOfEvaluatedSections >= scoreBestExpandedSolution)
                if (abortBranchEvaluationCriteria(currentNode))
                {
                    return null;
                }

                if (solution != null && currentNode.Score >= solution.Score)
                {
                    return solution;
                }

                if (currentNode.IsGoal)
                {
                    if (solution == null || solution.Score > currentNode.Score)
                    {
                        solution = currentNode;
                    }

                    continue;
                }

                var successors = successorCreator.GetSuccessors(currentNode, machineSettings, section, design);

                openList.Enqueue(successors);
            }

            return solution;
        }
    }
}
