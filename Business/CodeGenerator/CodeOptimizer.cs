﻿using PackNet.Business.CodeGenerator.LongHeadPositioner;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using InstructionList;

    using PackNet.Common.Interfaces;

    public class CodeOptimizer : ICodeOptimizer
    {
        public IEnumerable<InstructionItem> Optimize(IEnumerable<InstructionItem> instructionList)
        {
            var listWithUnnecessaryMovementsRemoved = this.RemoveUnnecessaryCrossheadMove(instructionList);
            var listWithOptimizedMovement = MergeSequentialCrossheadMovementWithSameToolStateWhenPropertiesAllowIt(listWithUnnecessaryMovementsRemoved);
            var listWithOptimizedCrossHeadActivations = this.RemoveUnnecessaryCrossheadActivations(listWithOptimizedMovement);
            var listWithOptimizedToolActivations = MergeSequentialToolActivation(listWithOptimizedCrossHeadActivations);
            var listWithOptimizedLongHeadToolActivations = MergeSequentialLongHeadToolActivations(listWithOptimizedToolActivations);
            var listWithUnnecessaryLongheadDeactivationsRemoved = this.RemoveUnnecessaryLongheadDeactivations(listWithOptimizedLongHeadToolActivations);
            var optimizedInstructionList = MergeSequentialFeedInstructions(listWithUnnecessaryLongheadDeactivationsRemoved);

            return optimizedInstructionList;
        }

        private IEnumerable<InstructionItem> RemoveUnnecessaryCrossheadActivations(IEnumerable<InstructionItem> instructionList)
        {
            var instructions = instructionList.ToList();
            var prevIndex = -1;

            for (var i = 0; i < instructions.Count(); i++)
            {
                var crossHeadToolActivation = instructions.ElementAt(i) as InstructionCrossHeadToolActivation;
                if (crossHeadToolActivation != null)
                {
                    if (prevIndex == -1)
                    {
                        prevIndex = i;
                        continue;
                    }
                    instructions.RemoveAt(prevIndex);
                    i--;
                }
                else
                {
                    prevIndex = -1;
                }
            }

            return instructions;
        }

        private IEnumerable<InstructionItem> RemoveUnnecessaryCrossheadMove(IEnumerable<InstructionItem> instructionList)
        {
            var instructions = instructionList.ToList();
            var prevIndex = -1;

            for (var i = 0; i < instructions.Count(); i++)
            {
                var crossHeadMovement = instructions.ElementAt(i) as InstructionCrossHeadMovementItem;
                if (crossHeadMovement != null)
                {
                    if (prevIndex == -1)
                    {
                        prevIndex = i;
                        continue;
                    }
                    if (crossHeadMovement.Position ==
                        (instructions.ElementAt(prevIndex) as InstructionCrossHeadMovementItem).Position)
                    {
                        if (IsRollerMovementInSpan(instructions, prevIndex + 1, i - 1))
                        {
                            instructions.RemoveAt(i--);
                        }

                        continue;
                    }
                    if (IsCrossHeadMovementInSpan(instructions, prevIndex + 1, i - 1) == false)
                    {
                        prevIndex = i;
                        continue;
                    }
                    do
                    {
                        instructions.RemoveAt(prevIndex);
                        i--;
                    }
                    while (instructions.ElementAt(prevIndex) is InstructionCrossHeadMovementItem == false);
                    prevIndex = i;
                }
            }

            var unnecessaryCrossheadMovesRemoved = new List<InstructionItem>();
            instructions.ForEach(unnecessaryCrossheadMovesRemoved.Add);
            return unnecessaryCrossheadMovesRemoved;
        }

        private bool IsRollerMovementInSpan(IEnumerable<InstructionItem> instructions, int lowerIndex, int higherIndex)
        {
            var span = instructions.Skip(lowerIndex).Take(higherIndex - lowerIndex + 1);
            return span.Any(instruction => instruction is InstructionFeedRollerItem);
        }

        private bool IsCrossHeadMovementInSpan(IEnumerable<InstructionItem> instructions, int lowerIndex, int higherIndex)
        {
            var span = instructions.Skip(lowerIndex).Take(higherIndex - lowerIndex + 1);
            return span.Any(instruction => instruction is InstructionCrossHeadMovementItem);
        }

        private IEnumerable<InstructionItem> MergeSequentialCrossheadMovementWithSameToolStateWhenPropertiesAllowIt(IEnumerable<InstructionItem> instructionList)
        {
            var instructions = instructionList.ToList();
            var listWithMovementMerged = new List<InstructionItem>();
            for (int i = 0; i < instructions.Count(); i++)
            {
                var instructionItem = instructions[i];
                var crossHeadMovement = instructionItem as InstructionCrossHeadMovementItem;
                if (crossHeadMovement != null)
                {
                    var itemsUntilCurrent = instructions.Take(i + 1).ToList();
                    var itemsAfterCurrent = instructions.Skip(i + 1).ToList();
                    var itemsBetweenCurrentAndNextMovement =
                        itemsAfterCurrent.TakeWhile(x => !(x is InstructionCrossHeadMovementItem)).ToList();
                    var itemsBeforeNextMovement = itemsUntilCurrent.Concat(itemsBetweenCurrentAndNextMovement).ToList();
                    var itemsFromNextMovement = instructions.Except(itemsBeforeNextMovement);

                    var toolActivationForMovement =
                        itemsUntilCurrent.LastOrDefault(x => x is InstructionCrossHeadToolActivation) as
                        InstructionCrossHeadToolActivation;
                    var nextCrossHeadMovement =
                        itemsFromNextMovement.FirstOrDefault(x => x is InstructionCrossHeadMovementItem) as
                        InstructionCrossHeadMovementItem;
                    var toolActivationForNextMovement =
                        itemsBeforeNextMovement.LastOrDefault(x => x is InstructionCrossHeadToolActivation) as
                        InstructionCrossHeadToolActivation;

                    if (toolActivationForMovement == null)
                    {
                        listWithMovementMerged.Add(crossHeadMovement);
                    }
                    else if (nextCrossHeadMovement == null)
                    {
                        listWithMovementMerged.Add(crossHeadMovement);
                    }
                    else if (itemsBetweenCurrentAndNextMovement.Any(x => x is InstructionFeedRollerItem))
                    {
                        listWithMovementMerged.Add(crossHeadMovement);
                    }
                    else if (nextCrossHeadMovement.SimultaneousWithFeed != crossHeadMovement.SimultaneousWithFeed)
                    {
                        listWithMovementMerged.Add(crossHeadMovement);
                    }
                    else if (toolActivationForNextMovement != null && toolActivationForNextMovement.Toolstate != toolActivationForMovement.Toolstate)
                    {
                        listWithMovementMerged.Add(crossHeadMovement);
                    }
                    else
                    {
                        nextCrossHeadMovement.DisconnectTrack = crossHeadMovement.DisconnectTrack || nextCrossHeadMovement.DisconnectTrack;
                    }
                }
                else
                {
                    listWithMovementMerged.Add(instructionItem);
                }
            }

            return listWithMovementMerged;
        }

        private IEnumerable<InstructionItem> MergeSequentialLongHeadToolActivations(IEnumerable<InstructionItem> instructionList)
        {
            var merged = MergeConnectedLongHeadToolActivations(instructionList);
            var unnecessaryActivationsRemoved = this.RemoveUnnecessaryLongHeadToolActivations(merged);

            return unnecessaryActivationsRemoved;
        }

        private IEnumerable<InstructionItem> RemoveUnnecessaryLongheadDeactivations(IEnumerable<InstructionItem> instructionList)
        {
            var listWithUnnecessaryLhDeactivationsRemoved = new List<InstructionItem>();
            var instructions = instructionList.ToList();
            var previousLongheadToolInstruction = new Dictionary<int, ToolStates>();
            var index = -1;

            foreach (var instruction in instructions)
            {
                index++;
                var shouldAddInstruction = true;
                if (instruction is InstructionLongHeadToolActivations)
                {
                    var toolStatesForCurrentInstruction = new Dictionary<int, ToolStates>(previousLongheadToolInstruction);
                    (instruction as InstructionLongHeadToolActivations).ToolActivations.ForEach(a => toolStatesForCurrentInstruction[a.LongHeadNumber] = a.ToolState);

                    if (toolStatesForCurrentInstruction.All(l => l.Value == ToolStates.None))
                    {
                        var canSkipDeactivation = true;
                        var foundAnotherInstruction = false;
                        foreach (var nextInstruction in instructions.Skip(index+1))
                        {
                            if (nextInstruction is InstructionLongHeadToolActivations)
                            {
                                foundAnotherInstruction = true;
                                break;
                            }
                            if (nextInstruction is InstructionFeedRollerItem || nextInstruction is InstructionLongHeadPositionItem)
                                canSkipDeactivation = false;
                        }

                        shouldAddInstruction = canSkipDeactivation == false || foundAnotherInstruction == false;
                    }
                }

                if (shouldAddInstruction)
                    listWithUnnecessaryLhDeactivationsRemoved.Add(instruction);
            }

            return listWithUnnecessaryLhDeactivationsRemoved;
        }

        private IEnumerable<InstructionItem> MergeConnectedLongHeadToolActivations(IEnumerable<InstructionItem> instructionList)
        {
            var instructions = instructionList.ToList();
            var listWithMergedLongHeadToolActivations = new List<InstructionItem>();

            while (instructions.Any())
            {
                if (instructions.First() is InstructionLongHeadToolActivations)
                {
                    var sequentialItems = instructions.TakeWhile(i => i is InstructionLongHeadToolActivations).Cast<InstructionLongHeadToolActivations>();
                    var toolActivationGroupedByLongHead =
                        sequentialItems.SelectMany(i => i.ToolActivations).GroupBy(ta => ta.LongHeadNumber);

                    var newToolActivationsForLongHead =
                        toolActivationGroupedByLongHead.Select(
                            t =>
                                new LongHeadToolActivationInformation(t.First().LongHeadNumber, t.Last().ToolState,
                                    t.First().PreviousState)).ToList();

                    listWithMergedLongHeadToolActivations.Add(new InstructionLongHeadToolActivations(newToolActivationsForLongHead));

                    instructions = instructions.Skip(sequentialItems.Count()).ToList();
                    continue;
                }

                listWithMergedLongHeadToolActivations.Add(instructions.First());
                instructions = instructions.Skip(1).ToList();
            }

            return listWithMergedLongHeadToolActivations;
        }

        private IEnumerable<InstructionItem> RemoveUnnecessaryLongHeadToolActivations(IEnumerable<InstructionItem> instructionList)
        {
            var listWithUnnecessaryLhActivationsRemoved = new List<InstructionItem>();
            var instructions = instructionList.ToList();
            var previousToolStates = new Dictionary<int, ToolStates>();

            foreach (var instruction in instructions)
            {
                if (instruction is InstructionLongHeadToolActivations)
                {
                    var toolStates = new Dictionary<int, ToolStates>(previousToolStates);
                    (instruction as InstructionLongHeadToolActivations).ToolActivations.ForEach(a => toolStates[a.LongHeadNumber] = a.ToolState);

                    if (toolStates.All(l => previousToolStates.ContainsKey(l.Key) && toolStates[l.Key].Equals(previousToolStates[l.Key])) == false)
                        listWithUnnecessaryLhActivationsRemoved.Add(instruction);

                    previousToolStates = toolStates;
                }
                else
                    listWithUnnecessaryLhActivationsRemoved.Add(instruction);
            }

            return listWithUnnecessaryLhActivationsRemoved;
        }

        private IEnumerable<InstructionItem> MergeSequentialToolActivation(IEnumerable<InstructionItem> instructionList)
        {
            var instructions = instructionList.ToList();
            var listWithToolActivationsMerged = new List<InstructionItem>();
            var previousToolState = ToolStates.None;
            var previousToolStateHasBeenSet = false;

            for (int i = 0; i < instructions.Count(); i++)
            {
                var instructionItem = instructions[i];
                var toolActivation = instructionItem as InstructionCrossHeadToolActivation;

                if (toolActivation != null)
                {
                    if (previousToolStateHasBeenSet == false)
                    {
                        previousToolState = toolActivation.Toolstate;
                        listWithToolActivationsMerged.Add(instructionItem);
                        previousToolStateHasBeenSet = true;
                    }

                    if (previousToolState != toolActivation.Toolstate)
                    {
                        previousToolState = toolActivation.Toolstate;
                        listWithToolActivationsMerged.Add(instructionItem);
                    }
                }
                else
                {
                    listWithToolActivationsMerged.Add(instructionItem);
                }
            }

            return listWithToolActivationsMerged;
        }

        private IEnumerable<InstructionItem> MergeSequentialFeedInstructions(IEnumerable<InstructionItem> instructionList)
        {
            var instructions = instructionList.ToList();
            var prevIndex = -1;

            for (int i = 0; i < instructions.Count(); i++)
            {

                if (prevIndex == -1)
                {
                    prevIndex = i;
                    continue;
                }

                if (instructions.ElementAt(i).GetType() == typeof(InstructionFeedRollerItem)
                    && instructions.ElementAt(prevIndex).GetType() == typeof(InstructionFeedRollerItem))
                {
                    var currentPos = ((InstructionFeedRollerItem)instructions.ElementAt(i)).Position;

                    instructions.RemoveAt(i--);
                    ((InstructionFeedRollerItem)instructions.ElementAt(i)).Position += currentPos;
                }

                prevIndex = i;
            }
            return instructions;
        }
    }
}
