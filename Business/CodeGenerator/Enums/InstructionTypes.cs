﻿namespace PackNet.Business.CodeGenerator.Enums
{
    public enum InstructionTypes
    {
        CrossHeadPositioning = 1,
        LongHeadPositioning = 2,
        CrossHeadKnifeInstruction = 3,
        FeedInstruction = 4,
        MetaData = 5,
        OnTheFlyCrossHeadInstruction = 6,
        LongHeadToolActivation = 7,
        TrackActivation = 8,
        OnTheFlyLongHeadInstruction = 9,
        NewInstruction = 31000,
        OnTheFlyNewPositionMarker = 31001,
        EndMarker = 32000,
    }

    public enum ActivationStates
    {
        Inactive = 0,
        Active = 1
    }
}