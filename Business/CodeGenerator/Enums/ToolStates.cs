﻿namespace PackNet.Business.CodeGenerator.Enums
{
    public enum ToolStates
    {
        None = 0,
        Crease = 100,
        CreaseWithWasteSeparation = 110,
        Cut = 300,
        CutWithWasteSeparation = 310,
        Perforation = 400,
        PerforationWithWasteSeparation = 410,
    }
}
