﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using Enums;
    using InstructionList;
    using Common.Interfaces.DTO;

    public class PackagingPanelCreator : IPackagingPanelCreator
    {
        private List<InstructionItem> instructionList;
        private PhysicalDesign compensatedDesign;
        private MicroMeter corrugateWidth;
        private FusionPhysicalMachineSettings machineSettings;
        private MicroMeter trackOffset;
        private bool isToolActive;
        private bool isRightSideFixed;

        public IEnumerable<InstructionItem> Create(PhysicalDesign compensatedDesign, FusionPhysicalMachineSettings machineSettings, MicroMeter corrugateWidth, Track track)
        {
            instructionList = new List<InstructionItem>();
            this.compensatedDesign = compensatedDesign;
            this.corrugateWidth = corrugateWidth;
            this.machineSettings = machineSettings;
            trackOffset = track.TrackOffset;
            
            isRightSideFixed = machineSettings.TrackParameters.Tracks.Single(t => t.Number == track.TrackNumber).RightSideFixed;
            
            AddInstructionsForHorizontalLineCoordinates(this.compensatedDesign.GetHorizontalLineCoordinates().OrderBy(coordinate => coordinate));
            AddOutFeedInstruction();

            return instructionList;
        }

        private void AddOutFeedInstruction()
        {
            instructionList.Add(new InstructionFeedRollerItem(machineSettings.FeedRollerParameters.OutFeedLength));
        }

        private void AddInstructionsForHorizontalLineCoordinates(IEnumerable<MicroMeter> horizontalLineCoordinates)
        {
            var numberOfHorizontalCoordinates = horizontalLineCoordinates.Count();
            MicroMeter lastCoordinate = 0;
            var isForwardDirection = IsStartingDirectionForward(numberOfHorizontalCoordinates);

            for (var i = 0; i < numberOfHorizontalCoordinates; i++)
            {
                var coordinate = horizontalLineCoordinates.ElementAt(i);

                PositionCrossheadAtStartPointForHorizontalLine(isForwardDirection);

                FeedCorrugateACertainLength(coordinate - lastCoordinate);

                ActivateToolIfFistLineIsCutLineAndOnCorrugateEdge(coordinate, isForwardDirection);

                var isFinalCrossheadMovement = i == numberOfHorizontalCoordinates - 1;

                AddCrossHeadInstructionsForLinesAtCoordinate(coordinate, isForwardDirection, isFinalCrossheadMovement);

                isForwardDirection = !isForwardDirection;

                lastCoordinate = coordinate;
            }

            DeactiveToolIfActive();
        }

        private void PositionCrossheadAtStartPointForHorizontalLine(bool isForwardDirection)
        {
            MicroMeter startPos;

            if (isForwardDirection)
            {
                startPos = GetStartPosForTrack() - machineSettings.CrossHeadParameters.CreaseWheelOffset;
            }
            else
            {
                startPos = GetEndPosForTrack() + machineSettings.CrossHeadParameters.CreaseWheelOffset;
            }

            MoveCrossHead(startPos, false);
        }

        private void DeactiveToolIfActive()
        {
            if (isToolActive)
            {
                var toolState = new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease };
                instructionList.Add(toolState);
                isToolActive = false;
            }
        }

        private void FeedCorrugateACertainLength(MicroMeter length)
        {
            instructionList.Add(new InstructionFeedRollerItem(length));
        }

        private void ActivateToolIfFistLineIsCutLineAndOnCorrugateEdge(MicroMeter coordinate, bool isForwardDirection)
        {
            var line = GetSortedHorizontalLinesAtCoordinate(coordinate, isForwardDirection).First();
            MicroMeter corrugateEdge = GetCorrugateStartEdge(isForwardDirection);
            MicroMeter startCoordinate = GetStartCoordinate(line, isForwardDirection);

            if (isForwardDirection)
            {
                if (startCoordinate <= corrugateEdge)
                {
                    ActivateToolIfCutLine(GetSortedHorizontalLinesAtCoordinate(coordinate, true).First());
                }
            }
            else
            {
                if (startCoordinate >= corrugateEdge)
                {
                    ActivateToolIfCutLine(GetSortedHorizontalLinesAtCoordinate(coordinate, false).First());            
                }
            }            
        }
        
        private void AddCrossHeadInstructionsForLinesAtCoordinate(MicroMeter coordinate, bool isForwardDirection, bool isCutOffLine)
        {
            var linesAtCoordinate = GetSortedHorizontalLinesAtCoordinate(coordinate, isForwardDirection);
            foreach (var line in linesAtCoordinate)
            {
                if (line.Type == LineType.crease)
                {
                    DeactiveToolIfActive();
                    MoveCrossHead(GetEndCoordinate(line, isForwardDirection, isCutOffLine), isCutOffLine);
                    continue;
                }

                MoveCrossHead(GetStartCoordinate(line, isForwardDirection), false);
                ActivateToolIfCutLine(line);
                MoveCrossHead(GetEndCoordinate(line, isForwardDirection, isCutOffLine), isCutOffLine);
                if (LineEndIsNotConnectedToCorrugateEdge(line, isForwardDirection))
                {
                    DeactiveToolIfCutLine(line);
                }
            }
        }

        private bool LineEndIsNotConnectedToCorrugateEdge(PhysicalLine line, bool isForwardDirection)
        {
            var corrugateEdge = GetCorrugateEndEdge(isForwardDirection);
            var lineEnd = GetEndCoordinate(line, isForwardDirection);
            if (isForwardDirection)
            {
                return lineEnd < corrugateEdge;
            }

            return lineEnd > corrugateEdge;
        }

        private MicroMeter GetCorrugateStartEdge(bool isForwardDirection)
        {
            return isForwardDirection ? GetStartPosForTrack() : GetEndPosForTrack();
        }

        private MicroMeter GetCorrugateEndEdge(bool isForwardDirection)
        {
            return isForwardDirection ? GetEndPosForTrack() : GetStartPosForTrack();
        }

        private MicroMeter GetStartPosForTrack()
        {
            if (isRightSideFixed)
            {
                return trackOffset - corrugateWidth;
            }

            return trackOffset;
        }

        private MicroMeter GetEndPosForTrack()
        {
            if (isRightSideFixed)
            {
                return trackOffset;
            }

            return trackOffset + corrugateWidth;
        }

        private void ActivateToolIfCutLine(PhysicalLine line)
        {
            if (line.Type == LineType.cut && isToolActive == false)
            {
                var toolState = new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut };
                instructionList.Add(toolState);
                isToolActive = true;
            }
        }

        private void DeactiveToolIfCutLine(PhysicalLine line)
        {
            if (line.Type == LineType.cut)
            {
                DeactiveToolIfActive();
            }
        }

        private void MoveCrossHead(MicroMeter pos, bool isCutOffLine)
        {
            /*////////// Add this section when implementing the feature that the creator throw on bad data
            //var crossHeadParameters = this.machine.GetCrossHeadParameters();
            //if (pos < crossHeadParameters.GetMinimumPosition())
            //{
            //    throw new ArgumentOutOfRangeException("position", pos, "Crosshead position set outside minimum allowed position");
            //}
            //else if (pos > crossHeadParameters.GetMaximumPosition())
            //{
            //    throw new ArgumentOutOfRangeException("position", pos, "Crosshead position set outside maximum allowed position");
            //}
            //////////////////////////////////////*/

            var movement = new InstructionCrossHeadMovementItem(pos);
            if (isCutOffLine)
            {
                movement.DisconnectTrack = true;
            }

            instructionList.Add(movement);
        }

        private MicroMeter GetStartCoordinate(PhysicalLine line, bool isForwardDirection)
        {
            return isForwardDirection ? line.StartCoordinate.X : line.EndCoordinate.X;
        }

        private MicroMeter GetEndCoordinate(PhysicalLine line, bool isForwardDirection, bool isCutoff = false)
        {
            if (!isCutoff)
            {
                return isForwardDirection ? line.EndCoordinate.X : line.StartCoordinate.X;
            }

            if (isForwardDirection)
            {
                return GetEndPosForTrack() + machineSettings.CrossHeadParameters.CreaseWheelOffset;
            }
            return GetStartPosForTrack() - machineSettings.CrossHeadParameters.CreaseWheelOffset;

        }

        private bool IsStartingDirectionForward(MicroMeter numberOfLines)
        {
            var isEvenNumberOfLines = numberOfLines % 2 == 0;

            if (isRightSideFixed)
            {
                return isEvenNumberOfLines;
            }
            
            return isEvenNumberOfLines == false;
        }

        private IEnumerable<PhysicalLine> GetSortedHorizontalLinesAtCoordinate(MicroMeter coordinate, bool isForwardDirection)
        {
            var horizontalLines = compensatedDesign.GetHorizontalLinesAtCoordinate(coordinate);
            
            return isForwardDirection ? horizontalLines.OrderBy(l => l.StartCoordinate.X) : horizontalLines.OrderByDescending(l => l.StartCoordinate.X);
        }
    }
}
