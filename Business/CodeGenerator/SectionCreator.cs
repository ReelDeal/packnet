﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.CodeGenerator
{
    public class SectionPermutation
    {
        public SectionPermutation(IEnumerable<Section> sections)
        {
            Sections = sections;
        }

        public IEnumerable<Section> Sections { get; private set; }
    }

    public class Section
    {
        private readonly List<PhysicalLine> lines;
        private readonly List<WasteArea> wasteAreas;

        public IEnumerable<PhysicalLine> Lines { get { return lines; } }

        public IEnumerable<WasteArea> WasteAreas { get { return wasteAreas; } }

        public MicroMeter StartCoordinateY { get; private set; }

        public MicroMeter EndCoordinateY { get; private set; }

        public MicroMeter Length
        {
            get
            {
                return EndCoordinateY - StartCoordinateY;
            }
        }

        public Section()
        {
            lines = new List<PhysicalLine>();
            wasteAreas = new List<WasteArea>();
        }

        public Section(IEnumerable<PhysicalLine> lines, IEnumerable<WasteArea> wasteAreas)
            : this()
        {
            this.lines.AddRange(lines);
            this.wasteAreas.AddRange(wasteAreas);
        }

        public Section(IEnumerable<PhysicalLine> lines, IEnumerable<WasteArea> wasteAreas, MicroMeter startY, MicroMeter endY)
            : this(lines, wasteAreas)
        {
            double startCoordY = startY;
            double endCoordY = endY;
            StartCoordinateY = Math.Min(startCoordY, endCoordY);
            EndCoordinateY = Math.Max(endCoordY, startCoordY);
        }

        public void AddLine(PhysicalLine line)
        {
            lines.Add(line);
        }

        public void AddLines(IEnumerable<PhysicalLine> newLines)
        {
            lines.AddRange(newLines);
        }

        public void AdjustEndCoordinateBasedOnLines()
        {
            EndCoordinateY = lines.Max(l => l.EndCoordinate.Y);
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var line in lines)
                sb.Append(line + "\r\n");

            return String.Format("Section:{0}-{1}\r\nLines:{2}", StartCoordinateY, EndCoordinateY, sb.ToString());
        }

        public IEnumerable<PhysicalLine> GetTrimmedVerticalLines()
        {
            return lines.Select(this.CreateTrimmedLine);
        }

        private PhysicalLine CreateTrimmedLine(PhysicalLine l)
        {
            var newLine = new PhysicalLine(new PhysicalCoordinate(l.StartCoordinate.X, l.StartCoordinate.Y), new PhysicalCoordinate(l.EndCoordinate.X, l.EndCoordinate.Y), l.Type);
            if (newLine.EndCoordinate.Y > EndCoordinateY)
                newLine.EndCoordinate.Y = EndCoordinateY;
            if (newLine.StartCoordinate.Y < StartCoordinateY)
                newLine.StartCoordinate.Y = StartCoordinateY;

            return newLine;
        }
    }

    public class SectionCreator : ISectionCreator
    {
		private readonly MicroMeter longHeadOffset;

        public SectionCreator(MicroMeter longHeadOffset)
        {
            this.longHeadOffset = longHeadOffset;
        }

        private IEnumerable<SectionPermutation> MergeSections(IEnumerable<Section> sections, bool mergeLastSection)
        {
            var sectionsToMerge = mergeLastSection ? sections : sections.Take(sections.Count() - 1);
            var lastSection = sections.Last();

            var sectionsCount = sectionsToMerge.Count();
            

            var totalCombinations = (int)Math.Pow(2, sectionsCount - 1);
            var result = new List<SectionPermutation>();
            var sectionCache = new Dictionary<Tuple<int, int>, Section>();
            for (var x = 0; x < totalCombinations; x++)
            {
                var sectionCombinations = new List<Section>();
                var indexOfStartSection = 0;
                for (var i = 0; i < sectionsCount; i++)
                {
                    // if the ith bit of x is not 0
                    if (((1 << i) & x) != 0)
                    {
                        sectionCombinations.Add(GetSection(sectionCache, sectionsToMerge, indexOfStartSection, i));
                        indexOfStartSection = i + 1;
                    }
                    else if (i == sectionsCount - 1)
                    {
                        sectionCombinations.Add(GetSection(sectionCache, sectionsToMerge, indexOfStartSection, sectionsCount - 1));
                    }
                }

                if (!mergeLastSection)
                    sectionCombinations.Add(lastSection);

                result.Add(new SectionPermutation(sectionCombinations));
            }
      

            return result.OrderBy(r => r.Sections.Count()); 
        }

        public IEnumerable<SectionPermutation> MergeSections(IEnumerable<Section> sections)
        {
            return MergeSections(sections, sections.Last().Length > longHeadOffset);
        }

        private static Section GetSection(IDictionary<Tuple<int, int>, Section> sectionCache, IEnumerable<Section> sections, int startIndex, int endIndex)
        {
            Section section;

            var cacheKey = new Tuple<int, int>(startIndex, endIndex);

            if (sectionCache.TryGetValue(cacheKey, out section))
            {
                return section;
            }

            var sectionBlock = sections.Skip(startIndex).Take(endIndex + 1 - startIndex).ToList();

            section = new Section(sectionBlock.SelectMany(t => t.Lines).Distinct().ToList(), sectionBlock.SelectMany(t => t.WasteAreas).Distinct().ToList(),
                sectionBlock.Min(l => l.StartCoordinateY), sectionBlock.Max(l => l.EndCoordinateY));

            sectionCache.Add(cacheKey, section);

            return section;
        }

        public IEnumerable<Section> GetSectionsFromDesign(RuleAppliedPhysicalDesign design)
        {
            var verticals = design.GetVerticalPhysicalLines().ToList();
            var sectionCoordinates = verticals.Select(l => l.StartCoordinate.Y).Union(verticals.Select(l => l.EndCoordinate.Y)).Distinct().ToList();
            var adjustedSectionCoordinates = AdjustLastSectionIfLengthIsLessThanLongHeadOffset(sectionCoordinates, design.Length);

            sectionCoordinates.Sort();

            var sections = new List<Section>();

            for (var i = 0; i < adjustedSectionCoordinates.Count - 1; i++)
            {
                sections.Add(new Section(
                    verticals.Where(
                        l =>
                    (l.StartCoordinate.Y <= adjustedSectionCoordinates.ElementAt(i) &&
                            l.EndCoordinate.Y >= adjustedSectionCoordinates.ElementAt(i + 1))
                            || (l.StartCoordinate.Y <= adjustedSectionCoordinates.ElementAt(i) && l.EndCoordinate.Y > adjustedSectionCoordinates.ElementAt(i))
                            || (l.StartCoordinate.Y >= adjustedSectionCoordinates.ElementAt(i) && l.EndCoordinate.Y <= adjustedSectionCoordinates.ElementAt(i + 1))
                            || (l.StartCoordinate.Y >= adjustedSectionCoordinates.ElementAt(i) && l.StartCoordinate.Y < adjustedSectionCoordinates.ElementAt(i+1) && l.EndCoordinate.Y >= adjustedSectionCoordinates.ElementAt(i + 1))).ToList(),
                    design.WasteAreas.Where(
                        w =>
                            (w.TopLeft.Y <= adjustedSectionCoordinates.ElementAt(i) && w.BottomRight.Y >= adjustedSectionCoordinates.ElementAt(i + 1)
                             || (w.TopLeft.Y <= adjustedSectionCoordinates.ElementAt(i) && w.BottomRight.Y > adjustedSectionCoordinates.ElementAt(i))
                             || (w.TopLeft.Y >= adjustedSectionCoordinates.ElementAt(i) && w.BottomRight.Y <= adjustedSectionCoordinates.ElementAt(i + 1))
                             || (w.TopLeft.Y >= adjustedSectionCoordinates.ElementAt(i) && w.TopLeft.Y < adjustedSectionCoordinates.ElementAt(i+1) && w.BottomRight.Y >= adjustedSectionCoordinates.ElementAt(i + 1)))
                           
                            
                         ).ToList(), 
                         adjustedSectionCoordinates.ElementAt(i),
                    adjustedSectionCoordinates.ElementAt(i + 1)));
            }


            return GetSectionsToReturn(sections);
        }

        private IEnumerable<Section> GetSectionsToReturn(List<Section> sections)
        {
            if (!sections.Any())
                return sections;

            var maxSections = 10;
            if (sections.Last().Length > longHeadOffset)
                maxSections--;

            if (sections.Count > maxSections)
            {
                var newSections = sections.Take(maxSections - 1).ToList();
                var sectionsToMerge = sections.Skip(maxSections - 1).ToList();
                var newLastSection = this.GetMergedLastSection(sectionsToMerge.Except(new List<Section> { sections.Last() }), maxSections);
                newSections.Add(newLastSection);
                newSections.Add(sections.Last());
                return newSections;
            }
            return sections;
        }

        private List<MicroMeter> AdjustLastSectionIfLengthIsLessThanLongHeadOffset(IEnumerable<MicroMeter> coordinates, double designEndCoordinate)
        {
            var adjustedCoordinates = coordinates.ToList();

            if (adjustedCoordinates.Any())
            {
                adjustedCoordinates.Sort();

                if (adjustedCoordinates.All(s => s != designEndCoordinate))
                {
                    adjustedCoordinates.Add(designEndCoordinate);
                }

                if (adjustedCoordinates.Last() - adjustedCoordinates.First() <= longHeadOffset)
                {
                    return adjustedCoordinates;
                }
                else if (adjustedCoordinates.ElementAt(adjustedCoordinates.Count - 1)
                         - adjustedCoordinates.ElementAt(adjustedCoordinates.Count - 2) <= longHeadOffset)
                {
                    var last = adjustedCoordinates.Last();
                    var lastsectionStart = last - longHeadOffset;

                    adjustedCoordinates = adjustedCoordinates.Where(c => c < lastsectionStart).ToList();

                    adjustedCoordinates.Add(lastsectionStart);
                    adjustedCoordinates.Add(last);
                }
            }
            else
            {
                adjustedCoordinates.Add(0);
                adjustedCoordinates.Add(designEndCoordinate);
            }

            return adjustedCoordinates;
        }

        private Section GetMergedLastSection(IEnumerable<Section> sections, int maxSections)
        {
            if (!sections.Any())
                return null;

            if (sections.Count() == 1)
                return sections.First();

            var newSections = sections.Take(maxSections - 1).ToList();
            if (sections.Count() > maxSections)
            {
                var sectionsToMerge = sections.Skip(maxSections - 1);
                var newLastSection = GetMergedLastSection(sectionsToMerge, maxSections);
                newSections.Add(newLastSection);
            }
            else if (sections.Count() != newSections.Count)
                newSections.AddRange(sections.Except(newSections));

            var mergedSections = this.MergeSections(newSections, true);
            var theSection = mergedSections.FirstOrDefault(s => s.Sections.Count() == 1).Sections.First();

            return theSection;
        }

        public IEnumerable<Section> SplitSections(MicroMeter maxReverseDistance, KeyValuePair<IEnumerable<Section>, LongHeadDistributionSolution> bestSolution)
        {
            
            for (int i = 0; i < bestSolution.Value.Sections.Count(); i++)
            {
                var section = bestSolution.Value.Sections.ElementAt(i);
                if (section.Length > maxReverseDistance && bestSolution.Value.GetSolutionNodes().ElementAt(i).NumberOfReverses > 0)
                {
                    double minLastSectionStartCoordinate = section.EndCoordinateY - longHeadOffset;
                    double startCoordinate = section.StartCoordinateY;
                    double endCoordinate = section.StartCoordinateY + maxReverseDistance;
                    while (startCoordinate < section.EndCoordinateY)
                    {
                        yield return
                            new Section(GetAdjustedLinesForSection(section.Lines, startCoordinate, endCoordinate),
                                section.WasteAreas, startCoordinate, endCoordinate);

                        startCoordinate = endCoordinate;

                        double endCoordinateMaxReverseDistance = endCoordinate + maxReverseDistance;
                        double endCoordinateY = section.EndCoordinateY;
                        endCoordinate = Math.Min(endCoordinateMaxReverseDistance, endCoordinateY);
                        if (endCoordinate > minLastSectionStartCoordinate && endCoordinate < section.EndCoordinateY && startCoordinate < minLastSectionStartCoordinate)
                            endCoordinate = minLastSectionStartCoordinate;
                    }
                }
                else
                {
                    yield return section;
                }
            }
        }

        private static IEnumerable<PhysicalLine> GetAdjustedLinesForSection(IEnumerable<PhysicalLine> lines, double startCoordinate, double endCoordinate)
        {
            return (from line in lines
                    where
                        (line.StartCoordinate.Y >= startCoordinate && line.StartCoordinate.Y < endCoordinate)
                        || (line.StartCoordinate.Y < startCoordinate && line.EndCoordinate.Y <= endCoordinate && line.EndCoordinate.Y > startCoordinate)
                        || (line.StartCoordinate.Y < startCoordinate && line.EndCoordinate.Y > endCoordinate)
                    select
                        new PhysicalLine(
                            new PhysicalCoordinate(line.StartCoordinate.X, Math.Max(line.StartCoordinate.Y, startCoordinate)),
                            new PhysicalCoordinate(line.EndCoordinate.X, Math.Min(line.EndCoordinate.Y, endCoordinate)),
                            line.Type)).ToList();
        }
    }
}
