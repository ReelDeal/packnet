﻿using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using InstructionList;
    using Common.Interfaces.DTO;

    public interface IOnTheFlyCompensator
    {
        IEnumerable<InstructionItem> CompensateForVelocity(
            IEnumerable<InstructionItem> instructionList, 
            MicroMeter entireMovementStartPosition, 
            double maximumAcceleration,
            double maximumDeceleration,
            double maximumSpeed, OtfDelayHandler delayHandler);
    }
}
