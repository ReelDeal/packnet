using System.Collections.Generic;

using PackNet.Business.CodeGenerator.Enums;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    using System.Linq;

    using InstructionList;
    using Common.Interfaces.DTO;

    public class CrossHeadOnTheFlyCompensator : IOnTheFlyCompensator
    {
        public IEnumerable<InstructionItem> CompensateForVelocity(IEnumerable<InstructionItem> instructionList,
            MicroMeter entireMovementStartPosition,
            double maximumAcceleration,
            double maximumDeceleration,
            double maximumSpeed, OtfDelayHandler delayHandler)
        {
            var instructionItems = instructionList.ToList();
            var compensatedInstructionList = new List<InstructionItem>();
            var startPosition = entireMovementStartPosition;
            var lastCrossHeadToolStateBeforeOtf = ToolStates.None;
            foreach (var instructionItem in instructionItems)
            {
                if (instructionItem is InstructionCrossHeadOnTheFlyItem)
                {
                    var otfInstruction =
                        SplitNoneToCutOtfPositionIntoNoneToCreaseAndCreaseToCutPosition(
                            instructionItem as InstructionCrossHeadOnTheFlyItem, lastCrossHeadToolStateBeforeOtf, delayHandler);
                    var index = instructionItems.IndexOf(instructionItem);
                    if (index > 0)
                    {
                        var previousIndex = --index;

                        // Set start position for on the fly to position from previous CrossHeadMovementPosition or CrossHeadOnTheFlyItem
                        startPosition = GetStartPosition(entireMovementStartPosition, instructionItems, previousIndex);
                    }

                    var compensatedItem = CompensateForVelocity(
                                                 otfInstruction,
                                                 startPosition,
                                                 lastCrossHeadToolStateBeforeOtf,
                                                 maximumAcceleration,
                                                 maximumDeceleration,
                                                 maximumSpeed,
                                                 delayHandler);
                    //Sortera lh positionerna i varje otf
                    compensatedInstructionList.Add(compensatedItem);
                }
                else
                {
                    if (instructionItem is InstructionCrossHeadToolActivation)
                    {
                        lastCrossHeadToolStateBeforeOtf = (instructionItem as InstructionCrossHeadToolActivation).Toolstate;
                    }
                    compensatedInstructionList.Add(instructionItem);
                }
            }

            return compensatedInstructionList;
        }

        private static InstructionCrossHeadOnTheFlyItem SplitNoneToCutOtfPositionIntoNoneToCreaseAndCreaseToCutPosition(InstructionCrossHeadOnTheFlyItem otfItem, ToolStates previouStates, OtfDelayHandler delayHandler)
        {
            var newOtf = new InstructionCrossHeadOnTheFlyItem { FinalPosition = otfItem.FinalPosition };

            foreach (var item in otfItem.Positions)
            {
                if (item.ToolState == ToolStates.Cut && previouStates == ToolStates.None)
                {
                    newOtf.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(item.Position, ToolStates.Crease));
                }

                newOtf.AddOnTheFlyPosition(item);
                previouStates = item.ToolState;
            }

            return newOtf;
        }

        private static MicroMeter GetStartPosition(MicroMeter entireMovementStartPosition, IReadOnlyList<InstructionItem> instructionItems, int previousIndex)
        {
            var startPosition = entireMovementStartPosition;
            do
            {
                var previousItem = instructionItems[previousIndex];
                if (previousItem is InstructionCrossHeadOnTheFlyItem)
                {
                    var itm = previousItem as InstructionCrossHeadOnTheFlyItem;
                    startPosition = itm.FinalPosition;
                    break;
                }

                if (previousItem is InstructionCrossHeadMovementItem)
                {
                    var itm = previousItem as InstructionCrossHeadMovementItem;
                    startPosition = itm.Position;
                    break;
                }
                --previousIndex;
            }
            while (previousIndex > 0);
            return startPosition;
        }

        private static InstructionCrossHeadOnTheFlyItem CompensateForVelocity(InstructionCrossHeadOnTheFlyItem otfInstructionItem, MicroMeter movementStartPosition, ToolStates originalToolState, double maximumAcceleration, double maximumDeceleration, double maximumSpeed, OtfDelayHandler delayHandler)
        {
            var compensator =
                new PositionCompensator(
                    maximumSpeed,
                    maximumAcceleration,
                    maximumDeceleration,
                    movementStartPosition,
                    otfInstructionItem.FinalPosition, delayHandler);
            compensator.GetMovementCompensationInformation();

            var compensatedItem = new InstructionCrossHeadOnTheFlyItem
                {
                    FinalPosition = otfInstructionItem.FinalPosition,
                    HighSpeed = otfInstructionItem.HighSpeed
                };

            var prevToolState = originalToolState;
            for (int i = 0; i < otfInstructionItem.Positions.Count(); i++)
            {
                if (i > 0)
                {
                    prevToolState = otfInstructionItem.Positions.ElementAt(i - 1).ToolState;
                }
                var position = otfInstructionItem.Positions.ElementAt(i);

                var compensatedPosition = compensator.CompensatePosition(position, prevToolState);

                if (position.ToolState == ToolStates.None && prevToolState == ToolStates.Cut)
                {
                    compensatedItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(
                        compensatedPosition.GetCompensatedPosition(), ToolStates.Crease));
                    compensatedItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(
                        position.Position, ToolStates.None));
                }
                else
                {
                    var onTheFlyPosition = new CrossHeadOnTheFlyPosition(compensatedPosition.GetCompensatedPosition(),
                        position.ToolState);
                    compensatedItem.AddOnTheFlyPosition(onTheFlyPosition);
                }
            }

            return compensatedItem;
        }
    }
}
