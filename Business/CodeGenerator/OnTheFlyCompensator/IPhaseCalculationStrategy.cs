namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public interface IPhaseCalculationStrategy
    {
        double GetTimeInPhase(MovementCompensationInformation movementCompensationInformation);

        double GetCompensationInPhase(MovementCompensationInformation movementCompensationInformation, double timeToCompensatedPositionInPhase);
    }
}