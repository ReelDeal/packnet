namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    using System;

    using Common.Interfaces.DTO;

    public class DistanceCompensation
    {
        private readonly MovementCompensationInformation movementCompensationInformation;

        public DistanceCompensation(MovementCompensationInformation movementCompensationInformation)
        {
            this.movementCompensationInformation = movementCompensationInformation;
        }

        public double StartPosition { get; set; }

        public double Endposition { get; set; }

        public DistanceCompensationInPhase AccelerationCompensation { get; set; }

        public DistanceCompensationInPhase FullSpeedCompensation { get; set; }

        public DistanceCompensationInPhase DecelerationCompensation { get; set; }

        public double EarliestPositionThatCanBeCompensatedWithDelay { get; set; }
        
        public MicroMeter GetCompensatedPosition()
        {
            if (movementCompensationInformation.IsMovingInPositiveDirection())
            {
                return StartPosition +
                       DecelerationCompensation.CompensationInPhase
                       + FullSpeedCompensation.CompensationInPhase
                       + AccelerationCompensation.CompensationInPhase;
            }
            return StartPosition -
                   DecelerationCompensation.CompensationInPhase
                   - FullSpeedCompensation.CompensationInPhase
                   - AccelerationCompensation.CompensationInPhase;
        }

        public double GetTimeToPosition()
        {
            return AccelerationCompensation.TimeInPhase +
                   FullSpeedCompensation.TimeInPhase +
                   DecelerationCompensation.TimeInPhase;
        }

        public double GetTimeToCompensatedPosition()
        {
            return AccelerationCompensation.TimeToCompensatedPositionInPhase +
                   FullSpeedCompensation.TimeToCompensatedPositionInPhase +
                   DecelerationCompensation.TimeToCompensatedPositionInPhase;
        }

        public void CalculateCompensatedPosition()
        {
            var delayLeft = movementCompensationInformation.DelayInSeconds;
            DecelerationCompensation.CalculateCompensatedPosition(movementCompensationInformation, delayLeft);
            delayLeft = DecelerationCompensation.RemainingDelay;
            FullSpeedCompensation.CalculateCompensatedPosition(movementCompensationInformation, delayLeft);
            delayLeft = FullSpeedCompensation.RemainingDelay;
            AccelerationCompensation.CalculateCompensatedPosition(movementCompensationInformation, delayLeft);
            EarliestPositionThatCanBeCompensatedWithDelay = movementCompensationInformation.CalculateEarliestPositionThatCanBeCompensatedWithDelay(movementCompensationInformation.DelayInSeconds);
        }

        public bool IsDelayCompensatedFor()
        {
            return Math.Abs(GetTimeToPosition() - GetTimeToCompensatedPosition() - movementCompensationInformation.DelayInSeconds) < 0.001;
        }
    }
}