namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public interface ICalculateRelativePositionDependingOnMovementDirection
    {
        double GetPositionAtDistanceAfter(double distance, double position);

        double GetPositionAtDistanceBefore(double distance, double position);

        bool DoesPositionOccurBeforeReference(double position, double reference);

        bool DoesPositionOccurAfterReference(double position, double reference);
    }
}