﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class OtfKinematicCalculation
    {
        public OtfKinematicCalculation()
        {
            
        }

        public OtfKinematicCalculation(FusionPhysicalMachineSettings machineSettings)
        {
            var crossHeadParameters = machineSettings.CrossHeadParameters;
            MaximumAcceleration = crossHeadParameters.MaximumAcceleration
                                         * crossHeadParameters.Acceleration / 100.0;
            MaximumDeceleration = crossHeadParameters.MaximumDeceleration
                                         * crossHeadParameters.Acceleration / 100.0;
            MaximumSpeed = crossHeadParameters.MaximumSpeed * crossHeadParameters.Speed
                                  / 100.0;
        }

        public OtfKinematicCalculation(IKinematicDataHolder parameters)
        {
            MaximumAcceleration = parameters.ActualMaximumAcceleration;
            MaximumDeceleration = parameters.ActualMaximumAcceleration;
            MaximumSpeed = parameters.ActualMaximumMovementSpeed;
        }

        public double MaximumSpeed { get; set; }

        public double MaximumDeceleration { get; set; }

        public double MaximumAcceleration { get; set; }
    }
}
