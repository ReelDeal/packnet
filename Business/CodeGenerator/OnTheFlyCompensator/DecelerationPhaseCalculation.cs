namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    using System;

    public class DecelerationPhaseCalculation : IPhaseCalculationStrategy
    {
        public double GetTimeInPhase(MovementCompensationInformation movementCompensationInformation)
        {
            if (movementCompensationInformation.IsPositionReachedDuringDeceleration())
            {
                var acceleration = -movementCompensationInformation.Deceleration;
                var distance = movementCompensationInformation.GetDistanceFromDecelerationStartToPosition();
                return new SpeedChangeTimeEquationSolver().SolveQuadraticEquationForTimeInDeceleration(distance, movementCompensationInformation.TopSpeedReached, acceleration);
            }
            return 0;
        }

        public double GetCompensationInPhase(MovementCompensationInformation movementCompensationInformation, double timeToCompensatedPositionInPhase)
        {
            return (movementCompensationInformation.TopSpeedReached * timeToCompensatedPositionInPhase) -
                (movementCompensationInformation.Deceleration * Math.Pow(timeToCompensatedPositionInPhase, 2) / 2);
        }
    }
}