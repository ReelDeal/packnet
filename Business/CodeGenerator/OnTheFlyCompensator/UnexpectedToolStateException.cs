using System;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class UnexpectedToolStateException : Exception
    {
        public UnexpectedToolStateException()
        {

        }

        public UnexpectedToolStateException(string message) : base(message)
        {

        }
    }
}