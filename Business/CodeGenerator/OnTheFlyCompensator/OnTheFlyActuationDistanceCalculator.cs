﻿using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class OtfActuationDistanceCalculation
    {
        private readonly List<MaxSpeedActuationDistance> maxSpeedActuationDistance = new List<MaxSpeedActuationDistance>();
        private readonly List<AccelerationActuationDistance> accelerationActuationDistance = new List<AccelerationActuationDistance>();
        
        public OtfActuationDistanceCalculation(OtfKinematicCalculation kinematicCalculation, OtfDelayHandler delayHandler)
        {
            maxSpeedActuationDistance = new List<MaxSpeedActuationDistance>
            {
                new MaxSpeedActuationDistance(ToolStates.None, ToolStates.Cut, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.None, ToolStates.Cut, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.None, ToolStates.Crease, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.Cut, ToolStates.None, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.Cut, ToolStates.None, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.Cut, ToolStates.Crease, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.Crease, ToolStates.Cut, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.Crease, ToolStates.Cut, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.Crease, ToolStates.None, kinematicCalculation.MaximumSpeed,
                    delayHandler),
                new MaxSpeedActuationDistance(ToolStates.Crease, ToolStates.Perforation, kinematicCalculation.MaximumSpeed,
                    delayHandler),
            };

            accelerationActuationDistance = new List<AccelerationActuationDistance>
            {
                new AccelerationActuationDistance(ToolStates.None, ToolStates.Cut, kinematicCalculation.MaximumSpeed,
                    kinematicCalculation.MaximumAcceleration, delayHandler),
                new AccelerationActuationDistance(ToolStates.None, ToolStates.Crease, kinematicCalculation.MaximumSpeed,
                    kinematicCalculation.MaximumAcceleration, delayHandler),
                new AccelerationActuationDistance(ToolStates.Cut, ToolStates.Crease, kinematicCalculation.MaximumSpeed,
                    kinematicCalculation.MaximumAcceleration, delayHandler),
                new AccelerationActuationDistance(ToolStates.Cut, ToolStates.None, kinematicCalculation.MaximumSpeed,
                    kinematicCalculation.MaximumAcceleration, delayHandler),
                new AccelerationActuationDistance(ToolStates.Crease, ToolStates.Cut, kinematicCalculation.MaximumSpeed,
                    kinematicCalculation.MaximumAcceleration, delayHandler),
                new AccelerationActuationDistance(ToolStates.Crease, ToolStates.None, kinematicCalculation.MaximumSpeed,
                    kinematicCalculation.MaximumAcceleration, delayHandler),
                new AccelerationActuationDistance(ToolStates.Crease, ToolStates.Perforation, kinematicCalculation.MaximumSpeed,
                    kinematicCalculation.MaximumAcceleration, delayHandler)
            };
        }

        public double GetAccelerationActuationDistance(ToolStates start, ToolStates end)
        {
            var edgeActivationDistance = accelerationActuationDistance.FirstOrDefault(d => d.StartState == start && d.EndState == end);
            return edgeActivationDistance != null ? edgeActivationDistance.Distance : double.NaN;
        }
        
        public double GetMaxSpeedActuationDistance(ToolStates start, ToolStates next)
        {
            var middleActivationDistance =
                maxSpeedActuationDistance.FirstOrDefault(d => d.StartState == start && d.NextState == next);
            return middleActivationDistance != null ? middleActivationDistance.Distance : double.NaN;
        }
        
    }

    public class MaxSpeedActuationDistance
    {
        public MaxSpeedActuationDistance(ToolStates startState, ToolStates nextState, double speed, OtfDelayHandler delayHandler)
        {
            StartState = startState;
            NextState = nextState;

            Distance = CalculateMinimumDistanceBetweenToolChangesInOtf(speed, delayHandler);
        }

        public ToolStates StartState { get; private set; }

        public ToolStates NextState { get; private set; }

        public double Distance { get; private set; }

        private double CalculateMinimumDistanceBetweenToolChangesInOtf(double maximumSpeed, OtfDelayHandler delayHandler)
        {
            return maximumSpeed * delayHandler.GetDelayToUse(StartState, NextState);
        }
    }

    public class AccelerationActuationDistance
    {
        public AccelerationActuationDistance(ToolStates startState, ToolStates endState, double speed, double acceleration, OtfDelayHandler delayHandler)
        {
            StartState = startState;
            EndState = endState;

            Distance = CalculateMinimumDistancesForToolChanges(speed, acceleration, delayHandler);
        }

        public ToolStates StartState { get; private set; }

        public ToolStates EndState { get; private set; }

        public double Distance { get; private set; }

        private double CalculateMinimumDistancesForToolChanges(double speed, double acceleration, OtfDelayHandler delayHandler)
        {
            var comp = new PositionCompensator(speed, acceleration, acceleration, 0, 2000, delayHandler);
            comp.GetMovementCompensationInformation();
            var position = new CrossHeadOnTheFlyPosition(1000, EndState);
            return comp.CompensatePosition(position, StartState).EarliestPositionThatCanBeCompensatedWithDelay;
        }
    }
}
