namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    using System;

    public class SpeedChangeTimeEquationSolver
    {
        // s= s0 + v0*t + a*t^2/2
        // -a*t^2/2-v0*t+(s-s0)=0
        // t^2/2+(2*v0/a)*t  - (2/a)*(s-s0)    =0  where s-s0 = distance
        // t^2  +(2*v0*/a)*t + ( - 2/a)*(distance)=0
        public double SolveQuadraticEquationForTimeInDeceleration(double distance, double v0, double a)
        {
            var p = (2 / a) * v0;           // Always negative for deceleration (v0>0, a<0)
            var q = -(2 / a) * distance;  // Always positive for deceleration (distance>0, a<0)

            // when p is negative then t ~ p/2+-p/2 for small distances, selecting the ~ p/2+p/2 root =p would result in a negative time since a is negative.
            var time = (-p / 2) - CalculateRootOffset(p, q);
            return time;
        }

        public double SolveQuadraticEquationForTimeInAcceleration(double distance, double v0, double a)
        {
            var p = (2 / a) * v0;               // Always positive for acceleration (v0>=0, a>0)
            var q = -(2 / a) * distance;      // Always negative for acceleration (distance>0, a>0)

            // when p is positive then t ~ -p/2+-p/2 for small distances, selecting the ~ -p/2-p/2 root =p would result in a negative time since a is positive.
            var time = (-p / 2) + CalculateRootOffset(p, q);
            return time;
        }

        private static double CalculateRootOffset(double p, double q)
        {
            return Math.Sqrt(Math.Pow(p / 2, 2) - q);
        }
    }
}