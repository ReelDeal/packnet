namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class DistanceCompensationFactory
    {
        public DistanceCompensation GetDistanceCompensation(MovementCompensationInformation movementCompensationInformation)
        {
            return new DistanceCompensation(movementCompensationInformation)
                {
                    StartPosition = movementCompensationInformation.StartPosition, 
                    Endposition = movementCompensationInformation.EndPosition, 
                    AccelerationCompensation = new DistanceCompensationInPhase(new AccelerationPhaseCalculation()), 
                    FullSpeedCompensation = new DistanceCompensationInPhase(new FullSpeedPhaseCalculation()),
                    DecelerationCompensation = new DistanceCompensationInPhase(new DecelerationPhaseCalculation())
                };
        }
    }
}