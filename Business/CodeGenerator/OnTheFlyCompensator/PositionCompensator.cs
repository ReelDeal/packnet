using System;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    using Enums;
    using InstructionList;

    public class PositionCompensator
    {

        private readonly MovementCompensationInformation movementCompensationInformation;
        private readonly DistanceCompensationFactory distanceCompensationFactory;
        private readonly OtfDelayHandler delayHandler;

        public PositionCompensator(
            double maxSpeed,
            double acceleration,
            double deceleration,
            double startPosition,
            double endPosition,
            OtfDelayHandler delayHandler)
        {
            movementCompensationInformation = new MovementCompensationInformation(startPosition, endPosition, maxSpeed, acceleration, deceleration);
            distanceCompensationFactory = new DistanceCompensationFactory();
            this.delayHandler = delayHandler;
        }

        public MovementCompensationInformation GetMovementCompensationInformation()
        {
            movementCompensationInformation.CalculatePositions();
            return movementCompensationInformation;
        }

        public DistanceCompensation CompensatePosition(CrossHeadOnTheFlyPosition position, ToolStates previousToolState)
        {
            return CompensatePosition(position.Position, position.ToolState, previousToolState);
        }

        public DistanceCompensation CompensatePosition(MicroMeter position, ToolStates state, ToolStates previousState)
        {
            var delayInSeconds = delayHandler.GetDelayToUse(previousState, state);
            return CompensatePositionBasedOnDelay(position, delayInSeconds);
        }

        private DistanceCompensation CompensatePositionBasedOnDelay(MicroMeter position, double delay)
        { 
            movementCompensationInformation.Position = position;
            movementCompensationInformation.DelayInSeconds = delay;
            var distanceCompensation = distanceCompensationFactory.GetDistanceCompensation(movementCompensationInformation);
            distanceCompensation.CalculateCompensatedPosition();
            return distanceCompensation;
        }
    }
}