﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class LongheadOnTheFlyCompensator : IOnTheFlyCompensator
    {
        public IEnumerable<InstructionItem> CompensateForVelocity(IEnumerable<InstructionItem> instructionList,
            MicroMeter entireMovementStartPosition,
            double maximumAcceleration,
            double maximumDeceleration,
            double maximumSpeed, OtfDelayHandler delayHandler)
        {
            var compensatedInstructionList = new List<InstructionItem>();

            foreach (var instructionItem in instructionList)
            {
                if (instructionItem is InstructionLongHeadOnTheFlyItem)
                {
                    var splitItem = SplitNoneToCutOtfPositionIntoNoneToCreaseAndCreaseToCutPosition(instructionItem as InstructionLongHeadOnTheFlyItem);
                    splitItem = SplitLongheadOtfItemsBasedOnActivations(splitItem, delayHandler);
                    var compensatedItem = CompensateForVelocity(
                                                 splitItem,
                                                 entireMovementStartPosition,
                                                 maximumAcceleration,
                                                 maximumDeceleration,
                                                 maximumSpeed,
                                                 delayHandler);
                    
                    compensatedInstructionList.Add(compensatedItem);
                }
                else
                {
                    compensatedInstructionList.Add(instructionItem);
                }
            }

            return compensatedInstructionList;
        }

        private static InstructionLongHeadOnTheFlyItem SplitNoneToCutOtfPositionIntoNoneToCreaseAndCreaseToCutPosition(InstructionLongHeadOnTheFlyItem otfItem)
        {
            var newOtf = new InstructionLongHeadOnTheFlyItem { FinalPosition = otfItem.FinalPosition };

            foreach (var item in otfItem.Positions)
            {
                var activationFromNoneToCut =
                    item.ToolStates.Where(t => t.ToolState == ToolStates.Cut && t.PreviousState == ToolStates.None).ToList();

                if (activationFromNoneToCut.Any())
                {
                    var otherToolStateTransitions = item.ToolStates.Except(activationFromNoneToCut).ToList();
                    activationFromNoneToCut.ForEach(
                        a =>
                            otherToolStateTransitions.Add(new LongHeadToolActivationInformation(a.LongHeadNumber, a.ToolState,
                                ToolStates.Crease)));

                    newOtf.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(item.Position, item.PercentageOfMaxSpeed,
                        activationFromNoneToCut.Select(
                            a => new LongHeadToolActivationInformation(a.LongHeadNumber, ToolStates.Crease, a.PreviousState))));
                    newOtf.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(item.Position, item.PercentageOfMaxSpeed,
                        otherToolStateTransitions));
                }
                else
                {
                    newOtf.AddOnTheFlyPosition(item);
                }
            }

            return newOtf;
        }

        private static InstructionLongHeadOnTheFlyItem SplitLongheadOtfItemsBasedOnActivations(InstructionLongHeadOnTheFlyItem otfLh, OtfDelayHandler delayHandler)
        {
            var newOtf = new InstructionLongHeadOnTheFlyItem { FinalPosition = otfLh.FinalPosition };

            foreach (var item in otfLh.Positions)
            {
                var groupedTransitions =
                    item.ToolStates.GroupBy(t => delayHandler.GetDelayToUse(t.PreviousState, t.ToolState));

                groupedTransitions.ForEach(
                    g =>
                        newOtf.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(item.Position,
                            item.PercentageOfMaxSpeed, g)));
                      
            }

            return newOtf;
        }

        private static InstructionLongHeadOnTheFlyItem CompensateForVelocity(InstructionLongHeadOnTheFlyItem otfInstructionItem, MicroMeter movementStartPosition, double maximumAcceleration, double maximumDeceleration, double maximumSpeed, OtfDelayHandler delayHandler)
        {
            var compensator =
                new PositionCompensator(
                    maximumSpeed,
                    maximumAcceleration,
                    maximumDeceleration,
                    movementStartPosition,
                    otfInstructionItem.FinalPosition, delayHandler);
            compensator.GetMovementCompensationInformation();

            var compensatedItem = new InstructionLongHeadOnTheFlyItem
            {
                FinalPosition = otfInstructionItem.FinalPosition,
            };

            for (var i = 0; i < otfInstructionItem.Positions.Count(); i++)
            {
                var position = otfInstructionItem.Positions.ElementAt(i);

                var compensatedPosition = compensator.CompensatePosition(position.Position, position.ToolStates.First().ToolState,
                    position.ToolStates.First().PreviousState);

                if (position.ToolStates.First().PreviousState == ToolStates.Cut &&
                    position.ToolStates.First().ToolState == ToolStates.None)
                {
                    compensatedItem.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(compensatedPosition.GetCompensatedPosition(),
                        100, position.ToolStates.Select(t => new LongHeadToolActivationInformation(t.LongHeadNumber, ToolStates.Crease, t.PreviousState))));
                    compensatedItem.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(position.Position,
                        100, position.ToolStates.Select(t => new LongHeadToolActivationInformation(t.LongHeadNumber, t.ToolState, ToolStates.Crease))));
                }
                else
                {
                    compensatedItem.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(compensatedPosition.GetCompensatedPosition(), 100, position.ToolStates));
                }
            }

            return compensatedItem;
        }
    }
}
