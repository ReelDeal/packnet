namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class CalculateRelativePositionForMovementInNegativeDirection : ICalculateRelativePositionDependingOnMovementDirection
    {
        public double GetPositionAtDistanceAfter(double distance, double position)
        {
            return position - distance;
        }

        public double GetPositionAtDistanceBefore(double distance, double position)
        {
            return position + distance;
        }

        public bool DoesPositionOccurBeforeReference(double position, double reference)
        {
            return position.CompareTo(reference) > 0;
        }

        public bool DoesPositionOccurAfterReference(double position, double reference)
        {
            return position.CompareTo(reference) < 0;
        }
    }
}