namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class DistanceCompensationInPhase
    {
        private readonly IPhaseCalculationStrategy phaseCalculation;

        public DistanceCompensationInPhase(IPhaseCalculationStrategy phaseCalculation)
        {
            this.phaseCalculation = phaseCalculation;
        }

        public double TimeInPhase { get; private set; }

        public double TimeToCompensatedPositionInPhase { get; private set; }

        public double CompensationInPhase { get; private set; }

        public double RemainingDelay { get; private set; }

        public void CalculateCompensatedPosition(MovementCompensationInformation movementCompensationInformation, double delay)
        {
            TimeInPhase = phaseCalculation.GetTimeInPhase(movementCompensationInformation);
            var delayLeft = CalculateTimeToCompensatedPosition(delay);
            CompensationInPhase = phaseCalculation.GetCompensationInPhase(movementCompensationInformation, TimeToCompensatedPositionInPhase);
            RemainingDelay = delayLeft;
        }

        private double CalculateTimeToCompensatedPosition(double delay)
        {
            if (TimeInPhase.CompareTo(0) > 0)
            {
                if (TimeInPhase.CompareTo(delay) >= 0)
                {
                    TimeToCompensatedPositionInPhase = TimeInPhase - delay;
                    return 0;
                }
                TimeToCompensatedPositionInPhase = 0;
                return delay - TimeInPhase;
            }

            return delay;
        }
    }
}