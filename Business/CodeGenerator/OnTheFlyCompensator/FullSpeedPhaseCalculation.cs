using System;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class FullSpeedPhaseCalculation : IPhaseCalculationStrategy
    {
        public double GetTimeInPhase(MovementCompensationInformation movementCompensationInformation)
        {
            if (movementCompensationInformation.IsPartOfDistanceTravelledAtMaxSpeed())
            {
                double endPosition;
                if (movementCompensationInformation.IsPositionReachedDuringDeceleration() == false)
                {
                    endPosition = movementCompensationInformation.Position;
                }
                else
                {
                    endPosition = movementCompensationInformation.DecelerationStartPosition;
                }

                var distance = Math.Abs(endPosition - movementCompensationInformation.AccelerationStopPosition);
                var timeInFullSpeed = distance / movementCompensationInformation.TopSpeedReached;
                return timeInFullSpeed;
            }
            return 0;
        }

        public double GetCompensationInPhase(MovementCompensationInformation movementCompensationInformation, double timeToCompensatedPositionInPhase)
        {
            return timeToCompensatedPositionInPhase * movementCompensationInformation.TopSpeedReached;
        }
    }
}