using PackNet.Business.CodeGenerator.InstructionList;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    using System;

    public class CompensationPositionOccursToSoonException : Exception
    {
        public CompensationPositionOccursToSoonException(
            CrossHeadOnTheFlyPosition position, DistanceCompensation compensatedPosition)
            : base(
                string.Format(
                    "Position {0} occurs before ealiest position that can be compensated for {1} which is {2}",
                    position.Position,
                    position.ToolState,
                    compensatedPosition.EarliestPositionThatCanBeCompensatedWithDelay))
        {
        }
    }
}