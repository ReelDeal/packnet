﻿using PackNet.Business.CodeGenerator.Enums;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class OtfDelayHandler
    {
        private readonly double cutActivationDelayInSeconds;
        private readonly double cutDeactivationDelayInSeconds;
        private readonly double creaseActivationDelayInSeconds;
        private readonly double creaseDeactivationDelayInSeconds;

        public OtfDelayHandler(
            double cutActivationDelayInMilliSeconds,
            double cutDeactivationDelayInMilliSeconds,
            double creaseActivationDelayInMilliSeconds,
            double creaseDeactivationDelayInMilliSeconds)
        {
            cutActivationDelayInSeconds = cutActivationDelayInMilliSeconds / 1000;
            cutDeactivationDelayInSeconds = cutDeactivationDelayInMilliSeconds / 1000;
            creaseActivationDelayInSeconds = creaseActivationDelayInMilliSeconds / 1000;
            creaseDeactivationDelayInSeconds = creaseDeactivationDelayInMilliSeconds / 1000;
        }

        public OtfDelayHandler(EmCrossHeadParameters crossHeadParameters)
        {
            cutActivationDelayInSeconds = crossHeadParameters.CutDelayOn / 1000;
            cutDeactivationDelayInSeconds = crossHeadParameters.CutDelayOff / 1000;
            creaseActivationDelayInSeconds = crossHeadParameters.CreaseDelayOn / 1000;
            creaseDeactivationDelayInSeconds = crossHeadParameters.CreaseDelayOff / 1000;
        }

        public OtfDelayHandler(EmLongHeadParameters longHeadParameters)
        {
            cutActivationDelayInSeconds = longHeadParameters.CutDelayOn / 1000;
            cutDeactivationDelayInSeconds = longHeadParameters.CutDelayOff / 1000;
            creaseActivationDelayInSeconds = longHeadParameters.CreaseDelayOn / 1000;
            creaseDeactivationDelayInSeconds = longHeadParameters.CreaseDelayOff / 1000;
        }

        public OtfDelayHandler(FusionPhysicalMachineSettings fusionSettings)
        {
            cutActivationDelayInSeconds = fusionSettings.CrossHeadParameters.KnifeActivationDelay / 1000;
            cutDeactivationDelayInSeconds = fusionSettings.CrossHeadParameters.KnifeDeactivationDelay / 1000;
            creaseActivationDelayInSeconds = fusionSettings.CrossHeadParameters.KnifeActivationDelay / 1000;
            creaseDeactivationDelayInSeconds = fusionSettings.CrossHeadParameters.KnifeDeactivationDelay / 1000;
        }

        public double GetDelayToUse(ToolStates startState, ToolStates endState)
        {
            switch (startState)
            {
                case ToolStates.Crease:
                case ToolStates.CreaseWithWasteSeparation: // TODO: Check if we need to change this to take waste separator activation into consideration
                    return GetCreasePreviousToolActuationDelay(endState);
                case ToolStates.Cut:
                case ToolStates.CutWithWasteSeparation: // TODO: Check if we need to change this to take waste separator activation into consideration
                    return GetCutPreviousToolActuationDelay(endState);
                case ToolStates.None:
                    return GetNoPreviousToolActuationDelay(endState);
                case ToolStates.Perforation:
                case ToolStates.PerforationWithWasteSeparation: // TODO: Check if we need to change this to take waste separator activation into consideration
                    return GetPerforationToolActuationDelay(endState); 
                default:
                    throw new UnexpectedToolStateException("Unexpected start state: " + startState);
            }
        }

        private double GetPerforationToolActuationDelay(ToolStates state)
        {
            switch (state)
            {
                case ToolStates.Crease:
                case ToolStates.CreaseWithWasteSeparation:
                    return cutDeactivationDelayInSeconds;
                case ToolStates.Perforation:
                case ToolStates.PerforationWithWasteSeparation:
                    return 0;
                default:
                    throw new UnexpectedToolStateException("Unexpected end state in perforation: " + state);
            }
        }

        private double GetCreasePreviousToolActuationDelay(ToolStates state)
        {
            switch (state)
            {
                case ToolStates.Crease:
                case ToolStates.CreaseWithWasteSeparation:
                    return 0;
                case ToolStates.Cut:
                case ToolStates.CutWithWasteSeparation:
                case ToolStates.Perforation:
                case ToolStates.PerforationWithWasteSeparation:
                    return cutActivationDelayInSeconds;
                case ToolStates.None:
                    return creaseDeactivationDelayInSeconds;
                default:
                    throw new UnexpectedToolStateException("Unexpected end state in crease: " + state);
            }
        }

        private double GetCutPreviousToolActuationDelay(ToolStates state)
        {
            switch (state)
            {
                case ToolStates.Crease:
                case ToolStates.CreaseWithWasteSeparation:
                case ToolStates.Perforation:
                case ToolStates.PerforationWithWasteSeparation:
                case ToolStates.None:
                    return cutDeactivationDelayInSeconds;
                case ToolStates.Cut:
                case ToolStates.CutWithWasteSeparation:
                    return 0;
                default:
                    throw new UnexpectedToolStateException("Unexpected end state in cut: " + state);
            }
        }

        private double GetNoPreviousToolActuationDelay(ToolStates state)
        {
            switch (state)
            {
                case ToolStates.Crease:
                case ToolStates.CreaseWithWasteSeparation:
                case ToolStates.Cut:
                case ToolStates.CutWithWasteSeparation:
                case ToolStates.Perforation:
                case ToolStates.PerforationWithWasteSeparation:
                    return creaseActivationDelayInSeconds;
                case ToolStates.None:
                    return 0;
                default:
                    throw new UnexpectedToolStateException("Unexpected end state in none: " + state);
            }
        }
    }
}
