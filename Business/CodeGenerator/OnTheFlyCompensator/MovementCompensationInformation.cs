namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    using System;

    public class MovementCompensationInformation
    {
        private readonly double maxSpeed;
        private ICalculateRelativePositionDependingOnMovementDirection directionCalculator;

        public MovementCompensationInformation(double startPosition, double endPosition, double maxSpeed, double acceleration, double deceleration)
        {
            StartPosition = startPosition;
            EndPosition = endPosition;
            this.maxSpeed = maxSpeed;
            Acceleration = acceleration;
            Deceleration = deceleration;
            SetDirectionCalculator();
        }

        public double Position { get; set; }

        public double DelayInSeconds { get; set; }

        public double StartPosition { get; private set; }

        public double EndPosition { get; private set; }

        public double Deceleration { get; private set; }

        public double TopSpeedReached { get; set; }

        public double Acceleration { get; private set; }

        public double AccelerationStopPosition { get; private set; }

        public double DecelerationStartPosition { get; private set; }

        public bool IsMovingInPositiveDirection()
        {
            return StartPosition.CompareTo(EndPosition) < 0;
        }

        public void CalculatePositions()
        {
            CalculatePositionsWhenTopSpeedIsReached();
            TopSpeedReached = maxSpeed;
            if (MaxSpeedIsNeverReached())
            {
                CalculatePositionsWhenMaxSpeedIsNeverReached();

                // v^2 -v0^2 = 2as;
                // v^2-0 = 2as;
                // v^2 = (2as)^1/2;
                TopSpeedReached = Math.Sqrt(2 * Math.Abs(AccelerationStopPosition - StartPosition) * Acceleration);
            }
        }

        public double CalculateDistanceTravelledWhileChangingSpeed(
            double startSpeed, double targetSpeed, double unsignedRateOfSpeedChangePerTimeUnit)
        {
            var speedDifference = Math.Abs(targetSpeed - startSpeed);
            var timeInSpeedChange = GetTimeInSpeedChange(speedDifference, unsignedRateOfSpeedChangePerTimeUnit);
            var averageSpeedDuringSpeedChange = (targetSpeed + startSpeed) / 2;
            return Math.Abs(timeInSpeedChange * averageSpeedDuringSpeedChange);
        }

        public bool PositionOccursBefore(double position)
        {
            return directionCalculator.DoesPositionOccurBeforeReference(Position, position);
        }

        public bool PositionOccursAfter(double position)
        {
            return directionCalculator.DoesPositionOccurAfterReference(Position, position);
        }

        public double GetDistanceFromDecelerationStartToPosition()
        {
            return Math.Abs(DecelerationStartPosition - Position);
        }

        public double GetDistanceFromStartToPosition()
        {
            return Math.Abs(Position - StartPosition);
        }

        public double GetDistanceTravelledDuringAcceleration()
        {
            return Math.Abs(AccelerationStopPosition - StartPosition);
        }

        public bool IsPartOfDistanceTravelledAtMaxSpeed()
        {
            return PositionOccursAfter(AccelerationStopPosition);
        }

        public bool IsPositionReachedDuringDeceleration()
        {
            return PositionOccursAfter(DecelerationStartPosition);
        }

        public bool IsPositionReachedDuringAcceleration()
        {
            return PositionOccursBefore(AccelerationStopPosition);
        }

        public double CalculateEarliestPositionThatCanBeCompensatedWithDelay(double delayInSeconds)
        {
            var timeInAcceleration = Math.Sqrt(2 * Math.Abs(AccelerationStopPosition - StartPosition) / Acceleration);
            double distance;
            if (timeInAcceleration - delayInSeconds > 0)
            {
                distance = Acceleration * delayInSeconds * delayInSeconds / 2;
            }
            else if (MaxSpeedIsNeverReached())
            {
                var distanceDuringAcceleration = GetDistanceTravelledDuringAcceleration();
                var timeInDeceleration = delayInSeconds - timeInAcceleration;
                var distanceDuringDeceleration = (TopSpeedReached * timeInDeceleration)
                                                 - (Deceleration * timeInDeceleration * timeInDeceleration / 2);
                distance = distanceDuringAcceleration + distanceDuringDeceleration;
            }
            else
            {
                var distanceDuringAcceleration = GetDistanceTravelledDuringAcceleration();
                var timeInFullSpeedAndDeceleration = delayInSeconds - timeInAcceleration;
                var timeInFullSpeed = Math.Abs(DecelerationStartPosition - AccelerationStopPosition) / TopSpeedReached;
                if (timeInFullSpeed > timeInFullSpeedAndDeceleration)
                {
                    var distanceDuringFullSpeed = timeInFullSpeedAndDeceleration * TopSpeedReached;
                    distance = distanceDuringAcceleration + distanceDuringFullSpeed;
                }
                else
                {
                    var distanceDuringFullSpeed = timeInFullSpeed * TopSpeedReached;
                    var timeInDeceleration = timeInFullSpeedAndDeceleration - timeInFullSpeed;
                    var distanceDuringDeceleration = (TopSpeedReached * timeInDeceleration) - (Deceleration * timeInDeceleration * timeInDeceleration / 2);
                    distance = distanceDuringAcceleration + distanceDuringFullSpeed + distanceDuringDeceleration;
                }
            }

            var distanceAfterStartPosition = directionCalculator.GetPositionAtDistanceAfter(distance, StartPosition);
            return distanceAfterStartPosition;
        }

        private void SetDirectionCalculator()
        {
            if (IsMovingInPositiveDirection())
            {
                directionCalculator = new CalculateRelativePositionForMovementInPositiveDirection();
            }
            else
            {
                directionCalculator = new CalculateRelativePositionForMovementInNegativeDirection();
            }
        }

        private void CalculatePositionsWhenTopSpeedIsReached()
        {
            var distanceWhileAccelerating = CalculateDistanceTravelledWhileChangingSpeed(0, maxSpeed, Acceleration);
            SetAccelerationStopPositionToDistanceFromStartPosition(distanceWhileAccelerating);

            var distanceWhileDecelerating = CalculateDistanceTravelledWhileChangingSpeed(maxSpeed, 0, Deceleration);
            SetDecelerationStartPositionToDistanceFromEndPosition(distanceWhileDecelerating);
        }

        private bool MaxSpeedIsNeverReached()
        {
            return directionCalculator.DoesPositionOccurBeforeReference(AccelerationStopPosition, DecelerationStartPosition) == false;
        }

        private void CalculatePositionsWhenMaxSpeedIsNeverReached()
        {
            var distanceWhileAccelerating = DistanceToTravel() * GetAccelerationPartOfDistance();
            SetAccelerationStopPositionToDistanceFromStartPosition(distanceWhileAccelerating);

            var distanceWhileDecelerating = DistanceToTravel() * GetDecelerationPartOfDistance();
            SetDecelerationStartPositionToDistanceFromEndPosition(distanceWhileDecelerating);
        }

        private double DistanceToTravel()
        {
            return Math.Abs(EndPosition - StartPosition);
        }

        private double GetAccelerationPartOfDistance()
        {
            return 1 - (Acceleration / SumOfAccelerationAndDeceleration());
        }

        private double GetDecelerationPartOfDistance()
        {
            return 1 - (Deceleration / SumOfAccelerationAndDeceleration());
        }

        private double SumOfAccelerationAndDeceleration()
        {
            return Acceleration + Deceleration;
        }

        private double GetTimeInSpeedChange(double speedDifference, double unsignedRateOfSpeedChangePerTimeUnit)
        {
            return speedDifference / unsignedRateOfSpeedChangePerTimeUnit;
        }

        private void SetAccelerationStopPositionToDistanceFromStartPosition(double distance)
        {
            AccelerationStopPosition = directionCalculator.GetPositionAtDistanceAfter(distance, StartPosition);
        }

        private void SetDecelerationStartPositionToDistanceFromEndPosition(double distance)
        {
            DecelerationStartPosition = directionCalculator.GetPositionAtDistanceBefore(distance, EndPosition);
        }
    }
}