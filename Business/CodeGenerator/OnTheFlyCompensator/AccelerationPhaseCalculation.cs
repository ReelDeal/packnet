using System;

namespace PackNet.Business.CodeGenerator.OnTheFlyCompensator
{
    public class AccelerationPhaseCalculation : IPhaseCalculationStrategy
    {
        public double GetTimeInPhase(MovementCompensationInformation movementCompensationInformation)
        {
            double distance;
            if (movementCompensationInformation.IsPositionReachedDuringAcceleration())
            {
                distance = movementCompensationInformation.GetDistanceFromStartToPosition();
            }
            else
            {
                distance = movementCompensationInformation.GetDistanceTravelledDuringAcceleration();
            }

            return new SpeedChangeTimeEquationSolver().SolveQuadraticEquationForTimeInAcceleration(distance, 0, movementCompensationInformation.Acceleration);
        }

        public double GetCompensationInPhase(MovementCompensationInformation movementCompensationInformation, double timeToCompensatedPositionInPhase)
        {
            return movementCompensationInformation.Acceleration * Math.Pow(timeToCompensatedPositionInPhase, 2) / 2;
        }
    }
}