namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    public class AStarPathFinder
    {
        public AStarPathFinder(State start, State end)
        {
            Start = start;
            End = end;
            OpenList = new List<Node>();
            ClosedList = new List<Node>();
        }

        public State Start { get; private set; }

        public State End { get; private set; }

        public List<Node> OpenList { get; private set; }

        public List<Node> ClosedList { get; private set; }

        public void RunNumberOfIterations(int iterationNumbers)
        {
            var startNode = new StartNode(Start);
            OpenList.Add(startNode);
            AdvanceForTheLowestInOpenList();
            for (var i = 0; i < iterationNumbers; i++)
            {
                AdvanceForTheLowestInOpenList();
            }
        }

        public void FindBestSolution()
        {
            var startNode = new StartNode(Start);
            OpenList.Add(startNode);
            AdvanceForTheLowestInOpenList();
            while (IsPathFound() == false)
            {
                AdvanceForTheLowestInOpenList();
            }
        }

        public bool IsPathFound()
        {
            return OpenList.Any() == false;
        }

        public List<Tuple<MicroMeter, State>> BestChoice()
        {
            if (!ClosedList.Any())
            {
                throw new NoMovementOrderCouldBeFoundException();
            }

            var orderedResults = ClosedList.OrderBy(x => x.EstimatedCost(End));
            var bestPath = orderedResults.First();
            return bestPath.GetPath();
        }

        public List<Tuple<LongHeadPositioningInformation, MicroMeter>> GetBestPath()
        {
            var stateInformations = BestChoice();
            return stateInformations.Where(info => info.Item2.MovedLongHead != null).Select(CreatMovementTuple).ToList();
        }

        private void AdvanceForTheLowestInOpenList()
        {
            if (IsPathFound() == false)
            {
                var startNode = OpenList.OrderBy(x => x.EstimatedCost(End)).First();
                var newNodes = startNode.GetNewNodes(End);
                OpenList.AddRange(newNodes);
                ////                  TODO: Don't add nodes already in open list
                if (startNode.IsGoalReached(End))
                {
                    ClosedList.Add(startNode);
                }

                OpenList.Remove(startNode);
            }
        }

        private Tuple<LongHeadPositioningInformation, MicroMeter> CreatMovementTuple(Tuple<MicroMeter, State> stateInformation)
        {
            return new Tuple<LongHeadPositioningInformation, MicroMeter>(stateInformation.Item2.MovedLongHead, stateInformation.Item1);
        }
    }
}