﻿namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    public class MovementOrderOptimizer
    {
        public List<Tuple<LongHeadPositioningInformation, MicroMeter>> OptimizeMovement(IEnumerable<LongHeadPositioningInformation> targetPositions, IEnumerable<LongHeadPositioningInformation> currentPositions, MicroMeter currentCrossHeadPosition)
        {
            var targetPositionList = targetPositions.ToList();
            var currentPositionList = currentPositions.ToList();
            if (AnyLongHeadToBeMovedIsInAnUnknownPosition(currentPositionList, targetPositionList))
            {
                throw new CurrentLongHeadPositionUnknownException();
            }

            var optimizedMovementOrder = new List<Tuple<LongHeadPositioningInformation, MicroMeter>>();
            if (AnyLongHeadNeedsToBeMoved(currentPositionList, targetPositionList))
            {
                var movements = CreateMovementList(currentPositionList, targetPositionList);
                var movementsThatNeedsToBePerformed = movements.Where(x => x.Distance > 0);
                optimizedMovementOrder = FindBestOrder(movementsThatNeedsToBePerformed, currentCrossHeadPosition);
            }

            // TODO: Handle optimizationMovement when some of the long heads are not in positioning
            // TODO: Should sensor to tool be considered when determining obstacles for optimizationMovement
            return optimizedMovementOrder;
        }

        private static bool AnyLongHeadToBeMovedIsInAnUnknownPosition(IEnumerable<LongHeadPositioningInformation> currentPositions, List<LongHeadPositioningInformation> targetPositionList)
        {
            return
                targetPositionList.Count(target => currentPositions.Any(current => current.LongHeadNumber == target.LongHeadNumber)) != targetPositionList.Count();
        }

        private static bool AnyLongHeadNeedsToBeMoved(IEnumerable<LongHeadPositioningInformation> currentPositions, IEnumerable<LongHeadPositioningInformation> targetPositionList)
        {
            return
                targetPositionList.Any(
                    target =>
                    currentPositions.Any(
                        y => y.LongHeadNumber == target.LongHeadNumber && y.Position != target.Position));
        }

        private static IEnumerable<LongHeadMovement> CreateMovementList(IEnumerable<LongHeadPositioningInformation> currentPositionList, IEnumerable<LongHeadPositioningInformation> targetPositionList)
        {
            return targetPositionList.Select(target =>
                {
                    var positionInformation = currentPositionList.Single(current => current.LongHeadNumber == target.LongHeadNumber);
                    return new LongHeadMovement(positionInformation, target);
                }).ToList();
        }

        private List<Tuple<LongHeadPositioningInformation, MicroMeter>> FindBestOrder(IEnumerable<LongHeadMovement> movements, MicroMeter currentCrossHeadPosition)
        {
            var start = new State(currentCrossHeadPosition, movements.Select(x => new LongHeadPositioningInformation(x.LongHeadNumber, x.Start.Position, x.Start.ToolWidth)).ToList(), null);
            var end = new State(currentCrossHeadPosition, movements.Select(x => new LongHeadPositioningInformation(x.LongHeadNumber, x.End.Position, x.End.ToolWidth)).ToList(), null);
            var astarPathFinder = new AStarPathFinder(start, end);
            astarPathFinder.FindBestSolution();
            var findBestOrder = astarPathFinder.GetBestPath();
            return findBestOrder;
        }
    }
}