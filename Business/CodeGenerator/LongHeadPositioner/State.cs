namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    public class State
    {
        public State(MicroMeter crossHeadPosition, List<LongHeadPositioningInformation> longHeadPositions, LongHeadPositioningInformation movedLongHead)
        {
            MovedLongHead = movedLongHead;
            CrossHeadPosition = crossHeadPosition;
            LongHeadPositions = longHeadPositions;
        }

        public MicroMeter CrossHeadPosition { get; private set; }

        public List<LongHeadPositioningInformation> LongHeadPositions { get; private set; }

        public LongHeadPositioningInformation MovedLongHead { get; private set; }

        public MicroMeter GetEstimatedDistanceLeftToGetToState(State targetState)
        {
            ////Heuristic considers that the crosshead can move each long head from its starting positioning to destination
            ////That way the distance left is not overestimated.
            ////If implementation is changed so that more than one move is made in each state transition then this needs to be rewritten to consider more than one move.
            var longheadsMovementsLeft = GetLongHeadMovementsLeftToGetTo(targetState);

            return longheadsMovementsLeft
                .Select(x => x.CalculateDistanceToMoveLongHead(CrossHeadPosition))
                .Aggregate<MicroMeter, MicroMeter>(0.0, (current, totalDistance) => current + totalDistance);

        }

        public List<LongHeadMovement> GetLongHeadMovementsLeftToGetTo(State targetState)
        {
            return LongHeadPositions.Select(longHead => LongHeadMovement.Create(targetState, longHead)).Where(movement => movement.Distance > 0).ToList();
        }

        public LongHeadPositioningInformation GetPositionOfLongHead(LongHeadPositioningInformation longHead)
        {
            return LongHeadPositions.Single(y => y.LongHeadNumber == longHead.LongHeadNumber);
        }

        public List<State> GetNewStates(State state)
        {
            var movesLeftToEnd = GetLongHeadMovementsLeftToGetTo(state).ToList();

            var movesThatAreNotBlocked = movesLeftToEnd.Where(movement => IsAnyLongHeadBlockingMove(movement, LongHeadPositions) == false);

            return movesThatAreNotBlocked.Select(CreateState).ToList();
        }

        public bool AreLongHeadsInSamePositionAs(State state)
        {
            return GetLongHeadMovementsLeftToGetTo(state).Count == 0;
        }

        private State CreateState(LongHeadMovement movement)
        {
            var targetPosition = movement.End;
            var newCrossHeadPosition = targetPosition.Position;
            var longHeadNumber = movement.LongHeadNumber;
            var newLongHeadPositions = GenerateNewLongHeadPositions(targetPosition, longHeadNumber).ToList();
            var newState = new State(newCrossHeadPosition, newLongHeadPositions, targetPosition);
            return newState;
        }

        private IEnumerable<LongHeadPositioningInformation> GenerateNewLongHeadPositions(LongHeadPositioningInformation targetPositioning, int longHeadNumber)
        {
            return LongHeadPositions.Select(current => current.IsLonghead(longHeadNumber) ? targetPositioning : current);
        }

        private bool IsAnyLongHeadBlockingMove(LongHeadMovement movement, IEnumerable<LongHeadPositioningInformation> currentLongHeadPositions)
        {
            return currentLongHeadPositions.Any(movement.IsBlockedBy);            
        }
    }
}