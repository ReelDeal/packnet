using PackNet.Business.CodeGenerator.Enums;

namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;

    using Common.Interfaces.DTO;

    public class LongHeadPositioningInformation
    {
        public LongHeadPositioningInformation(int longHeadNumber, MicroMeter position, MicroMeter toolWidth)
        {
            ToolWidth = toolWidth;
            LongHeadNumber = longHeadNumber;
            Position = position;
        }

        public int LongHeadNumber { get; private set; }

        public MicroMeter Position { get; private set; }

        public MicroMeter ToolWidth { get; private set; }

        public MicroMeter GetDistanceToPosition(LongHeadPositioningInformation positioning)
        {
            return GetDistanceToPosition(positioning.Position);
        }

        public MicroMeter GetDistanceToPosition(MicroMeter position)
        {
            return GetDistanceBetweenPositions(Position, position);
        }

        public bool IsLonghead(int longHeadNumber)
        {
            return LongHeadNumber == longHeadNumber;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            if (obj.GetType() != GetType())
            {
                return false;
            }

            return Equals((LongHeadPositioningInformation)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (LongHeadNumber * 397) ^ Position.GetHashCode();
            }
        }

        public override string ToString()
        {
            return string.Format("LongHeadNumber: {0}, Position: {1}", LongHeadNumber, Position);
        }

        protected bool Equals(LongHeadPositioningInformation other)
        {
            return LongHeadNumber == other.LongHeadNumber && Position.Equals(other.Position);
        }

        private static double GetDistanceBetweenPositions(double positionOne, double positionTwo)
        {
            // TODO: Using double as type instead of MicroMeter because Math.Abs returns int and rounds for MicroMeter
            return Math.Abs(positionOne - positionTwo);
        }
    }
}