﻿namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;

    public class LinesOutsideCorrugateException : Exception
    {
        public override string ToString()
        {
            return "Vertical line is outside corrugate";
        }
    }
}
