﻿namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;

    public class LongHeadDistributionException : Exception
    {
        public override string ToString()
        {
            return "Could not position longheads";
        }
    }
}
