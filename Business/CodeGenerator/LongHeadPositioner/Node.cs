namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    public class Node
    {
        protected Node(State item, MicroMeter distanceFromStartToParent)
        {
            DistanceFromStartToParent = distanceFromStartToParent;
            CurrentState = item;
        }

        protected Node()
        {
        }

        public MicroMeter DistanceFromStartToParent { get; protected set; }

        public MicroMeter DistanceFromStart { get; protected set; }

        public State CurrentState { get; protected set; }

        public Node ParentNode { get; private set; }

        public static Node CreateNode(State state, Node parentNode)
        {
            var newNode = new Node(state, parentNode.DistanceFromStart) { ParentNode = parentNode };
            newNode.CalculateDistanceFromStart();
            return newNode;
        }

        public MicroMeter EstimatedCost(State end)
        {
            return DistanceFromStart + GetEstimatedDistanceToGetTo(end);
        }

        public List<Node> GetNewNodes(State end)
        {
            if (CurrentState.AreLongHeadsInSamePositionAs(end) == false)
            {
                var newStates = CurrentState.GetNewStates(end);
                return newStates.Select(stateInformation => CreateNode(stateInformation, this)).ToList();
            }

            return new List<Node>();
        }

        public bool IsGoalReached(State end)
        {
            return CurrentState.AreLongHeadsInSamePositionAs(end);
        }

        public virtual List<Tuple<MicroMeter, State>> GetPath()
        {
            var pathBefore = ParentNode.GetPath();
            pathBefore.Add(CreateMovementTuple());
            return pathBefore;
        }

        protected Tuple<MicroMeter, State> CreateMovementTuple()
        {
            return new Tuple<MicroMeter, State>(DistanceFromStart, CurrentState);
        }

        private void CalculateDistanceFromStart()
        {
            DistanceFromStart = DistanceFromStartToParent + ParentNode.GetEstimatedDistanceToGetTo(CurrentState);
        }

        private double GetEstimatedDistanceToGetTo(State targetState)
        {
            return CurrentState.GetEstimatedDistanceLeftToGetToState(targetState);
        }
    }
}