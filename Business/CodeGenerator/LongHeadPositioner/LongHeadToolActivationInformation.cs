﻿using PackNet.Business.CodeGenerator.Enums;

namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    public class LongHeadToolActivationInformation
    {
        public int LongHeadNumber { get; private set; }

        public ToolStates PreviousState { get; private set; }

        public ToolStates ToolState { get; private set; }

        public LongHeadToolActivationInformation(int number, ToolStates toolState, ToolStates previousState) : this(number,toolState)
        {
            PreviousState = previousState;
        }

        private LongHeadToolActivationInformation(int number, ToolStates toolState)
        {
            LongHeadNumber = number;
            ToolState = toolState;
        }
    }
}
