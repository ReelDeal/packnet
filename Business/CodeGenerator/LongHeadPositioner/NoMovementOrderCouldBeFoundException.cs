namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;

    public class NoMovementOrderCouldBeFoundException : Exception
    {
    }
}