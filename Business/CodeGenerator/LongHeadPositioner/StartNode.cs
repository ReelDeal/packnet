namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System;
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    public class StartNode : Node
    {
        public StartNode(State state)
        {
            DistanceFromStart = 0;
            DistanceFromStartToParent = 0;
            CurrentState = state;
        }

        public override List<Tuple<MicroMeter, State>> GetPath()
        {
            return new List<Tuple<MicroMeter, State>> { CreateMovementTuple() };
        }
    }
}