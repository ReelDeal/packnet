﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using System.Collections.Generic;
    using System.Linq;

    using AStarStuff;
    using InstructionList;
    using Common.Interfaces.DTO;
    using Common.Logging;

    public class LongHeadPositioner : ILongHeadPositioner
    {
        private MicroMeter[] longHeadPositions;
        private double toolWidth;

        public LongHeadPositioner(MicroMeter[] longHeadPositions)
        {
            this.longHeadPositions = longHeadPositions;
        }

        public IEnumerable<MicroMeter> GetLongheadPositions()
        {
            return longHeadPositions;
        }

        public bool CanDistributeLongHeadsFor(IEnumerable<PhysicalLine> verticalLines, FusionLongHeadParameters longHeadParameters, bool trackIsRightSideFixed, MicroMeter trackOffset, MicroMeter corrugateWidth, MicroMeter corrugateWidthTolerance)
        {
            try
            {
                TryDistributingLongHeadsWithAlgorithm(verticalLines, longHeadParameters, trackIsRightSideFixed, trackOffset, corrugateWidth, corrugateWidthTolerance, new AStarAlgorithmSimple()); 
                return true;
            }
            catch (LongHeadDistributionException)
            {
                return false;
            }
            catch (LinesOutsideCorrugateException)
            {
                return false;
            }
        }

        public InstructionLongHeadPositionItem DistributeLongHeads(IEnumerable<PhysicalLine> verticalLines, FusionLongHeadParameters longHeadParameters, bool trackIsRightSideFixed, MicroMeter trackOffset, MicroMeter corrugateWidth, MicroMeter crossHeadPosition, MicroMeter corrugateWidthTolerance)
        {
            var currentLongHeadPositions = longHeadPositions;
            LogLHs("DistributeLongHeads current ", longHeadPositions);

            var solution = TryDistributingLongHeadsWithAlgorithm(verticalLines, longHeadParameters, trackIsRightSideFixed, trackOffset, corrugateWidth, corrugateWidthTolerance, new AStarAlgorithmOptimized());
            var allLongHeadTargetToolPositions = solution.NewLongHeadPositions;

            var longHeadCurrentSensorPositons = GetLongHeadCurrentSensorPositions(longHeadParameters, currentLongHeadPositions);
            var longHeadTargetSensorPositions = GetLongHeadTargetSensorPositions(longHeadParameters, allLongHeadTargetToolPositions);
            var optimizedMovementOrder = FindOptimizedMovementOrder(longHeadTargetSensorPositions, longHeadCurrentSensorPositons, crossHeadPosition);
            longHeadPositions = CopyNewPositions(allLongHeadTargetToolPositions);

            LogLHs("DistributeLongHeads Done, ", longHeadPositions);
            return new InstructionLongHeadPositionItem(optimizedMovementOrder);
        }

        public IEnumerable<LongHeadPositioningInformation> FilterPositionsThatShouldBeMoved(MicroMeter[] newPositions)
        {
            LogLHs("FilterPositionsThatShouldBeMoved, original", longHeadPositions);
            LogLHs("New positions given ", newPositions);

            var originalPositions = longHeadPositions;
            var longHeadToCoordinatePairs = new List<LongHeadPositioningInformation>();
            for (var i = 0; i < originalPositions.Count(); i++)
            {
                if (originalPositions.ElementAt(i) != newPositions.ElementAt(i))
                {
                    var longHeadPositioningInformation = CreateLongHeadPositioningInformationFromIndex(newPositions, i);
                    longHeadToCoordinatePairs.Add(longHeadPositioningInformation);
                }
            }

            return longHeadToCoordinatePairs;
        }

        private AStarNode TryDistributingLongHeadsWithAlgorithm(IEnumerable<PhysicalLine> verticalLines, FusionLongHeadParameters longHeadParameters, bool trackIsRightSideFixed, MicroMeter trackOffset, MicroMeter corrugateWidth, MicroMeter corrugateWidthTolerance, AStarAlgorithm algorithm)
        {
            CheckIfAnyVerticalLineIsOutsideCorrugate(verticalLines, trackIsRightSideFixed, trackOffset, corrugateWidth);
            toolWidth = longHeadParameters.LongheadWidth;
            return algorithm.FindSolution(longHeadParameters, longHeadPositions, trackIsRightSideFixed, trackOffset, corrugateWidth, verticalLines, corrugateWidthTolerance);
        }

        private static void CheckIfAnyVerticalLineIsOutsideCorrugate(IEnumerable<PhysicalLine> verticalLines, bool trackIsRightSideFixed, MicroMeter trackOffset, MicroMeter corrugateWidth)
        {
            var trackLeftSide = trackIsRightSideFixed ? trackOffset - corrugateWidth : trackOffset;
            var trackRightSide = trackIsRightSideFixed ? trackOffset : trackOffset + corrugateWidth;

            if (verticalLines.Any(line => line.StartCoordinate.X > trackRightSide || line.StartCoordinate.X < trackLeftSide))
            {
                throw new LinesOutsideCorrugateException();
            }
        }

        private IEnumerable<LongHeadPositioningInformation> GetLongHeadCurrentSensorPositions(FusionLongHeadParameters longHeadParameters, MicroMeter[] currentLongHeadPositions)
        {
            var list = new List<LongHeadPositioningInformation>();
            for (var i = 0; i < currentLongHeadPositions.Count(); i++)
            {
                var longHeadPositioningInformation = CreateLongHeadPositioningInformationFromIndex(currentLongHeadPositions, i);
                list.Add(longHeadPositioningInformation);
            }

            var longHeadCurrentToolPositions = (IEnumerable<LongHeadPositioningInformation>)list;
            var longHeadCurrentSensorPositons = ConvertToolPositionsToSensorPositions(longHeadParameters, longHeadCurrentToolPositions);
            return longHeadCurrentSensorPositons;
        }

        private IEnumerable<LongHeadPositioningInformation> GetLongHeadTargetSensorPositions(FusionLongHeadParameters longHeadParameters, MicroMeter[] allLongHeadToolPositions)
        {
            var longHeadTargetToolPositions = FilterPositionsThatShouldBeMoved(allLongHeadToolPositions);
            var longHeadTargetSensorPositions = ConvertToolPositionsToSensorPositions(longHeadParameters, longHeadTargetToolPositions);
            return longHeadTargetSensorPositions;
        }

        private IEnumerable<LongHeadPositioningInformation> ConvertToolPositionsToSensorPositions(FusionLongHeadParameters longHeadParameters, IEnumerable<LongHeadPositioningInformation> longHeadsToReposition)
        {
            MicroMeter sensorOffset = longHeadParameters.LeftSideToSensorPin;
            var longHeadInformationEnumerable = 
                longHeadParameters.LongHeads.Select(x => new LongHeadInformation(x.Number, x.LeftSideToTool, sensorOffset));
            var positionConverter = new ToolToSensorPositionConverter(longHeadInformationEnumerable);
            var convertedPositions = positionConverter.ConvertPosition(longHeadsToReposition);
            return convertedPositions;
        }

        private LongHeadPositioningInformation CreateLongHeadPositioningInformationFromIndex(IEnumerable<MicroMeter> positions, int index)
        {
            var longHeadNumber = index + 1;
            var position = positions.ElementAt(index);
            return new LongHeadPositioningInformation(longHeadNumber, position, toolWidth);
        }

        private IEnumerable<LongHeadPositioningInformation> FindOptimizedMovementOrder(IEnumerable<LongHeadPositioningInformation> targetSensorPositions, IEnumerable<LongHeadPositioningInformation> currentSensorPositions, MicroMeter crossHeadPosition)
        {
            var movementOrderOptimizer = new MovementOrderOptimizer();
            var optimizedMovement = movementOrderOptimizer.OptimizeMovement(targetSensorPositions, currentSensorPositions, crossHeadPosition);
            return optimizedMovement.Select(order => order.Item1);
        }

        private MicroMeter[] CopyNewPositions(IEnumerable<MicroMeter> newLongHeadPositions)
        {
            return newLongHeadPositions.ToArray();
        }

        private void LogLHs(string message, MicroMeter[] positions)
        {
            var lhstring = "LHs : [ ";
            foreach (var lh in positions)
            {
                lhstring += lh + " ";
            }

            lhstring += " ]";

            Log(LogLevel.Debug, message + lhstring);
        }

        private void Log(LogLevel logLevel, string message)
        {
            if (LogManager.LoggingBackend != null)
            {
                LogManager.LoggingBackend.GetLog().Log(logLevel, message);
            }
        }
    }
}
