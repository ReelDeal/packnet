namespace PackNet.Business.CodeGenerator.LongHeadPositioner
{
    using Common.Interfaces.DTO;

    public class LongHeadMovement
    {
        public LongHeadMovement(LongHeadPositioningInformation start, LongHeadPositioningInformation end)
        {
            Start = start;
            End = end;
            LongHeadNumber = Start.LongHeadNumber;
            Distance = Start.GetDistanceToPosition(End);
        }

        public LongHeadPositioningInformation Start { get; private set; }

        public LongHeadPositioningInformation End { get; private set; }

        public int LongHeadNumber { get; private set; }

        public double Distance { get; private set; }

        public static LongHeadMovement Create(State targetState, LongHeadPositioningInformation longHead)
        {
            var longHeadPositionInState = targetState.GetPositionOfLongHead(longHead);
            return new LongHeadMovement(longHead, longHeadPositionInState);
        }

        public bool IsBlockedBy(LongHeadPositioningInformation longHead)
        {
            if (longHead.IsLonghead(LongHeadNumber))
            {
                return false;
            }

            return IsLongHeadPositionedBetweenStartAndEnd(longHead);
        }

        public MicroMeter CalculateDistanceToMoveLongHead(MicroMeter crossHeadPosition)
        {
            MicroMeter crossHeadMoveDistanceToGetLongHead = Start.GetDistanceToPosition(crossHeadPosition);
            return Distance + crossHeadMoveDistanceToGetLongHead;
        }

        private bool IsLongHeadPositionedBetweenStartAndEnd(LongHeadPositioningInformation longHead)
        {
            var position = longHead.Position;
            var startToSensorPin = Start.GetDistanceToPosition(position);
            var endToSensorPin = End.GetDistanceToPosition(position);
            if (IsLongHeadSensorInClosedIntervalOfStartAndEnd(endToSensorPin, startToSensorPin))
            {
                return true;
            }

            return IsLongHeadWithinToolWidthFromOpenIntervalOfStartAndEnd(longHead, startToSensorPin, endToSensorPin);
        }

        private bool IsLongHeadSensorInClosedIntervalOfStartAndEnd(MicroMeter distanceFromEndToSensorPosition, MicroMeter distanceFromStartToSensorPosition)
        {
            var longHeadIsCloserToStartThanEnd = distanceFromStartToSensorPosition <= Distance;
            var longHeadIsCloserToEndThanStart = distanceFromEndToSensorPosition <= Distance;
            return longHeadIsCloserToStartThanEnd && longHeadIsCloserToEndThanStart;
        }

        private bool IsLongHeadWithinToolWidthFromOpenIntervalOfStartAndEnd(LongHeadPositioningInformation longHead, MicroMeter startToSensorPin, MicroMeter endToSensorPin)
        {
            if (endToSensorPin < longHead.ToolWidth)
            {
                return true;
            }

            return false;
        }
    }
}