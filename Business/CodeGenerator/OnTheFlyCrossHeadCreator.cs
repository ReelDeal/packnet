﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Networking;

namespace PackNet.Business.CodeGenerator
{
    using PackNet.Common.Interfaces.DTO;

    public class OnTheFlyCrossHeadCreator : OnTheFlyCreator
    {
        protected override bool IsStartOfPossibleOtfSequence(List<InstructionItem> instructionItems,
            int currentIndex,
            IMovementInstructionItem prevMovement, 
            OtfActuationDistanceCalculation distanceCalculation)
        {
            var movement = instructionItems[currentIndex] as InstructionCrossHeadMovementItem;

            if (movement == null)
                return false;

            if (prevMovement == null)
                return true;

            return (Math.Abs(movement.Position - prevMovement.Position) < GetMimimDistanceToUse(instructionItems, currentIndex, distanceCalculation)) == false;
        }

        protected override List<InstructionItem> GetPossibleOtfItems(List<InstructionItem> instructionItems,
            int currentIndex, OtfActuationDistanceCalculation distanceCalculation)
        {
            var prevMovement = instructionItems[currentIndex] as InstructionCrossHeadMovementItem;
            var possibleSequence = new List<InstructionItem> { prevMovement };

            var startPositionIsSet = false;
            var startinPosition = new MicroMeter(0);
            var startingMovement = instructionItems.Take(currentIndex > 0 ? currentIndex - 1 : 0).LastOrDefault(i => i is InstructionCrossHeadMovementItem) as InstructionCrossHeadMovementItem;
            if (startingMovement != null)
            {
                startinPosition = startingMovement.Position;
                startPositionIsSet = true;
            }
            else if(prevMovement != null)
            {
                startinPosition = prevMovement.Position;
                startPositionIsSet = true;
            }
                

            var isAccelerationPhase = true;
            for (var indexOfNextItem = currentIndex + 1; indexOfNextItem < instructionItems.Count; indexOfNextItem++)
            {
                if (startPositionIsSet == false && prevMovement != null)
                {
                    startinPosition = prevMovement.Position;
                    startPositionIsSet = true;
                }

                if (IsUsableInOtf(instructionItems, indexOfNextItem, isAccelerationPhase, prevMovement, distanceCalculation, startinPosition, startPositionIsSet))
                {
                    if (instructionItems[indexOfNextItem] is InstructionCrossHeadMovementItem)
                    {
                        prevMovement = instructionItems[indexOfNextItem] as InstructionCrossHeadMovementItem;
                    }

                    possibleSequence.Add(instructionItems[indexOfNextItem]);
                }
                else
                {
                    //Is this item usable during deceleration?
                    if (IsUsableInOtf(instructionItems, indexOfNextItem, true, prevMovement, distanceCalculation, startinPosition, startPositionIsSet))
                    {
                        possibleSequence.Add(instructionItems[indexOfNextItem]);

                        if (instructionItems[indexOfNextItem] is InstructionCrossHeadToolActivation)
                            possibleSequence.Add(instructionItems[indexOfNextItem + 1]);
                    }

                    break;
                }

                isAccelerationPhase = false;
            }

            return possibleSequence;
        }

        protected override List<InstructionItem> MergeMovements(List<InstructionItem> possibleSequence)
        {
            return MergeMovements<InstructionCrossHeadMovementItem>(possibleSequence);
        }

        protected override bool CanSequenceBeReplacedByOtf(List<InstructionItem> sequence)
        {
            return CanSequenceBeReplacedByOtf<InstructionCrossHeadToolActivation>(sequence);
        }

        protected override InstructionItem CreateOtfInstruction(List<InstructionItem> mergeable)
        {
            var otfItem = new InstructionCrossHeadOnTheFlyItem();
            var mergeableIterator = mergeable.GetEnumerator();
            while (mergeableIterator.MoveNext())
            {
                var position = (mergeableIterator.Current as InstructionCrossHeadMovementItem).Position;
                if (mergeableIterator.MoveNext())
                {
                    var toolState = (mergeableIterator.Current as InstructionCrossHeadToolActivation).Toolstate;
                    otfItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(position, toolState));
                }
                else
                {
                    otfItem.FinalPosition = position;
                }
            }

            return otfItem;
        }

        private static double GetMimimDistanceToUse(List<InstructionItem> instructionItems, int index,
            OtfActuationDistanceCalculation distanceCalculation)
        {
            var previousActivation =
                instructionItems.Take(index).LastOrDefault(i => i is InstructionCrossHeadToolActivation) as
                    InstructionCrossHeadToolActivation;

            var nextActivation =
                instructionItems.GetRange(index, instructionItems.Count - index)
                    .FirstOrDefault(i => i is InstructionCrossHeadToolActivation) as InstructionCrossHeadToolActivation;

            if (nextActivation != null)
            {
                return
                    distanceCalculation.GetAccelerationActuationDistance(
                        previousActivation != null ? previousActivation.Toolstate : ToolStates.None, nextActivation.Toolstate);
            }

            return 0;
        }

        private static bool IsUsableInOtf(List<InstructionItem> items,
            int index,
            bool isAccelerationOrDecelerationPhase,
            InstructionCrossHeadMovementItem prevMovement, OtfActuationDistanceCalculation distanceCalculation, MicroMeter startingPosition, bool useStartingPositionValue)
        {
            if (items[index] is IMovementInstructionItem)
                return IsCrossHeadMovementsMergeable(items, index, prevMovement, isAccelerationOrDecelerationPhase, distanceCalculation, startingPosition, useStartingPositionValue);
            return items[index] is InstructionCrossHeadToolActivation && IsCrossHeadMovementsMergeable(items, index + 1, prevMovement, isAccelerationOrDecelerationPhase, distanceCalculation, startingPosition, useStartingPositionValue);
        }
        
        private static bool IsCrossHeadMovementsMergeable(List<InstructionItem> items,
            int index,
            InstructionCrossHeadMovementItem prevMovement,
            bool isAccelerationOrDecelerationPhase, OtfActuationDistanceCalculation distanceCalculation,
            MicroMeter startingPosition, bool useStartingPositionValue)
        {
            var movement = items.ElementAtOrDefault(index) as InstructionCrossHeadMovementItem;
            if (movement == null || prevMovement == null)
            {
                return false;
            }

            var prevActivation = GetPreviousActuation(items.GetRange(0, index));
            var currentActivation = items.ElementAtOrDefault(index + 1) as InstructionCrossHeadToolActivation;

            double minimumDistance;
            if (isAccelerationOrDecelerationPhase == false && IsLastOnTheFlyMovement(items, index) == false)
            {
                minimumDistance =
                    distanceCalculation.GetMaxSpeedActuationDistance(prevActivation == null ? ToolStates.None : prevActivation.Toolstate,
                    currentActivation == null ? prevActivation.Toolstate : currentActivation.Toolstate);
            }
            else
            {
                minimumDistance = distanceCalculation.GetAccelerationActuationDistance(prevActivation == null ? ToolStates.None : prevActivation.Toolstate,
                   currentActivation == null ? prevActivation.Toolstate : currentActivation.Toolstate);
            }

            if (movement.DisconnectTrack != prevMovement.DisconnectTrack ||
                movement.SimultaneousWithFeed != prevMovement.SimultaneousWithFeed ||
                Math.Abs(movement.Position - prevMovement.Position) < minimumDistance)
            {
                return false;
            }

            if (useStartingPositionValue)
            {
                if (startingPosition < prevMovement.Position && (startingPosition > movement.Position || movement.Position < prevMovement.Position))
                    return false;
                if (startingPosition > prevMovement.Position && (startingPosition < movement.Position || movement.Position > prevMovement.Position))
                    return false;
            }

            return true;
        }

        private static InstructionCrossHeadToolActivation GetPreviousActuation(IEnumerable<InstructionItem> items)
        {
            return items.LastOrDefault(i => i is InstructionCrossHeadToolActivation) as InstructionCrossHeadToolActivation;
        }

        private static bool IsLastOnTheFlyMovement(List<InstructionItem> items, int index)
        {
            var chInstructionsLeft = items.GetRange(index, items.Count - index).TakeWhile(i => i is InstructionCrossHeadMovementItem || i is InstructionCrossHeadToolActivation);
            return chInstructionsLeft.Count(i => i is InstructionCrossHeadMovementItem) <= 1;
        }
    }
}
