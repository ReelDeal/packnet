﻿using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using InstructionList;
    using Common.Interfaces.Machines;

    public abstract class OnTheFlyCreator : IOnTheFlyCreator
    {
        protected abstract bool IsStartOfPossibleOtfSequence(List<InstructionItem> instructionItems, int currentIndex, IMovementInstructionItem prevMovement, OtfActuationDistanceCalculation actuationDistances);

        protected abstract List<InstructionItem> GetPossibleOtfItems(List<InstructionItem> instructionItems, int currentIndex, OtfActuationDistanceCalculation actuationDistances);

        protected abstract List<InstructionItem> MergeMovements(List<InstructionItem> possibleSequence);

        protected abstract bool CanSequenceBeReplacedByOtf(List<InstructionItem> sequence);

        protected abstract InstructionItem CreateOtfInstruction(List<InstructionItem> mergeable);
        
        public IEnumerable<InstructionItem> Create(IEnumerable<InstructionItem> instructionList, OtfActuationDistanceCalculation actuationDistances)
        {
            var instructionItems = instructionList.ToList();
            var itemCount = instructionItems.Count();
            var newInstructions = new List<InstructionItem>();
            IMovementInstructionItem prevMovement = null;
            for (var currentIndex = 0; currentIndex < itemCount; currentIndex++)
            {
                if (IsStartOfPossibleOtfSequence(instructionItems, currentIndex, prevMovement, actuationDistances))
                {
                    var possibleSequence = GetPossibleOtfItems(instructionItems, currentIndex, actuationDistances);
                    currentIndex = currentIndex + possibleSequence.Count() - 1;

                    possibleSequence = MergeMovements(possibleSequence);

                    if (CanSequenceBeReplacedByOtf(possibleSequence))
                    {
                        newInstructions.Add(CreateOtfInstruction(possibleSequence));
                        prevMovement = possibleSequence.ElementAt(possibleSequence.Count - 1) as IMovementInstructionItem;
                    }
                    else
                    {
                        if (possibleSequence[0] is IMovementInstructionItem)
                        {
                            prevMovement = possibleSequence.ElementAt(possibleSequence.Count - 1) as IMovementInstructionItem;
                        }

                        newInstructions.AddRange(possibleSequence);
                    }

                    if (possibleSequence.Contains(instructionItems[currentIndex]) == false)
                    {
                        newInstructions.Add(instructionItems[currentIndex]);
                    }
                }
                else
                {
                    newInstructions.Add(instructionItems[currentIndex]);
                }
            }

            var newInstructionList = new List<InstructionItem>();

            newInstructions.ForEach(newInstructionList.Add);

            return newInstructionList;
        }

        protected List<InstructionItem> MergeMovements<T>(List<InstructionItem> possibleSequence) where T : class, IMovementInstructionItem
        {
            var mergedInstructionList = new List<InstructionItem>();
            var last = possibleSequence.ElementAt(0);
            var current = possibleSequence.ElementAt(0);

            for (var i = 1; i < possibleSequence.Count; i++)
            {
                current = possibleSequence.ElementAt(i);

                if (current is T && last is T)
                    (last as T).Position += (current as T).Position;
                else
                    mergedInstructionList.Add(last);

                last = current;
            }

            mergedInstructionList.Add(current);

            return mergedInstructionList;
        }

        protected bool CanSequenceBeReplacedByOtf<T>(List<InstructionItem> sequence) where T : class
        {
            return sequence.Count > 3 || (sequence.Count == 3 && sequence.ElementAt(1) is T);
        }
    }
}
