﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    public interface ISuccessorCreator
    {
        IEnumerable<IAStarNodeEM> GetSuccessors(IAStarNodeEM node, EmPhysicalMachineSettings machineSettings, Section section, RuleAppliedPhysicalDesign design);
    }
}
