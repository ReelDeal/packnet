﻿using System.Linq;

using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System.Collections.Generic;

    using LongHeadPositioner;
    using Common.Interfaces.DTO;

    internal abstract class AStarAlgorithm : IAStarAlgorithm
    {
        private List<AStarNode> openList = new List<AStarNode>();

        public AStarNode FindSolution(FusionLongHeadParameters longHeadParameters, MicroMeter[] longHeadPositions, bool trackIsRightSideFixed, MicroMeter trackOffset, MicroMeter corrugateWidth, IEnumerable<PhysicalLine> verticalLines, MicroMeter corrugateWidthTolerance)
        {
            //TODO abort if there are more lines than longheads or there are lines that start at different Y-coordinnates

            MicroMeter trackLeftSide = trackIsRightSideFixed
                                           ? trackOffset - corrugateWidth - corrugateWidthTolerance
                                           : trackOffset - corrugateWidthTolerance;
            MicroMeter trackRightSide = trackIsRightSideFixed
                                            ? trackOffset + corrugateWidthTolerance
                                            : trackOffset + corrugateWidth + corrugateWidthTolerance;
            var start = new AStarNode(null, longHeadParameters, longHeadPositions, new bool[longHeadPositions.Length], trackLeftSide, trackRightSide, verticalLines, verticalLines.Count());
            var openList = new Stack<AStarNode>();
            openList.Push(start);
            
            var solution = ReturnSolution(openList, longHeadPositions);

            if (solution == null)
            {
                throw new LongHeadDistributionException();
            }

            return solution;
        }

        protected abstract AStarNode ReturnSolution(Stack<AStarNode> openList, MicroMeter[] longHeadPositions);
    }
}
