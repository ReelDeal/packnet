﻿namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    internal class AStarAlgorithmOptimized : AStarAlgorithm
    {
        protected override AStarNode ReturnSolution(Stack<AStarNode> openList, MicroMeter[] longHeadPositions)
        {
            AStarNode solution = null;

            while (openList.Count > 0)
            {
                var currentNode = openList.Pop();
                if (currentNode.IsGoal())
                {
                    if (solution == null || solution.Score(longHeadPositions) >= currentNode.Score(longHeadPositions))
                    {
                        solution = currentNode;
                    }

                    continue;
                }

                currentNode.GetSuccessors().ForEach(openList.Push);
            }

            return solution;
        }
    }
}
