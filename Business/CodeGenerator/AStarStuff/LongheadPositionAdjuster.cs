﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using LongHeadPositioner;
    using Common.Interfaces.DTO;

    public class LongheadPositionAdjuster
    {
        private readonly EmLongHeadParameters longheadParameters;
        private MicroMeter[] lhPositionsForCurrentIteration;

        public LongheadPositionAdjuster(EmLongHeadParameters longheadParameters)
        {
            this.longheadParameters = longheadParameters;
        }

        public void AdjustPositionOfLongheadsInSolution(LongHeadDistributionSolution solution)
        {
            if (solution == null)
                return;

            foreach (var node in solution.GetSolutionNodes().Where(n => n.Iterations != null))
            {
                AdjustPositionOfLongheadsInSolutionNode(node);
            }
        }

        protected void AdjustPositionOfLongheadsInSolutionNode(IAStarNodeEM node)
        {
            foreach (var iteration in node.Iterations.Where(iteration => iteration.Any()))
            {
                AdjustPositionsForUsedLongheadsInCurrentIteration(iteration);
                AdjustPositionsForUnusedLongheadsInCurrentIteration(iteration);
            }
        }

        private static void AdjustPositionsForUsedLongheadsInCurrentIteration(IEnumerable<IAStarNodeEM> iteration)
        {
            for (var i = 0; i < iteration.Count(); i++)
            {
                iteration.ElementAt(0).NewLongHeadPositions[iteration.ElementAt(i).LongHead.Number - 1] = iteration.ElementAt(i).TargetCoordinate;
            }
        }

        private void AdjustPositionsForUnusedLongheadsInCurrentIteration(IEnumerable<IAStarNodeEM> iteration)
        {
            lhPositionsForCurrentIteration = iteration.First().NewLongHeadPositions;
            var unUsedLongheads = GetUnusedLongheadNumbers(iteration);

            while (unUsedLongheads.Any())
            {
                var currentUnusedLonghead = longheadParameters.GetLongHeadByNumber(unUsedLongheads.First());
                var previousPlacedLonghead = longheadParameters.GetPreviousLongHead(currentUnusedLonghead);

                var nextPlacedLonghead = GetNextPlacedLonghead(unUsedLongheads, currentUnusedLonghead);
                var numberOfUnplacedToFitBetweenMinMax = (nextPlacedLonghead == null
                    ? longheadParameters.LongHeads.Count() + 1
                    : nextPlacedLonghead.Number) - currentUnusedLonghead.Number;

                while (numberOfUnplacedToFitBetweenMinMax > 0)
                {
                    var minPositionForUnplacedLonghead = GetMinPositionForUnplacedLonghead(previousPlacedLonghead, currentUnusedLonghead);
                    var maxPositionForUnplacedLonghead = GetMaxPositionForUnplacedLonghead(nextPlacedLonghead, currentUnusedLonghead);

                    CheckForLongheadDistributionException(minPositionForUnplacedLonghead, maxPositionForUnplacedLonghead, currentUnusedLonghead, numberOfUnplacedToFitBetweenMinMax);
                    AdjustLongheadPositionIfNeeded(minPositionForUnplacedLonghead, maxPositionForUnplacedLonghead, currentUnusedLonghead, numberOfUnplacedToFitBetweenMinMax);

                    previousPlacedLonghead = GetPreviousPlacedLonghead(previousPlacedLonghead);

                    numberOfUnplacedToFitBetweenMinMax--;
                    
                    unUsedLongheads.Remove(unUsedLongheads.First());    
                    
                    if (unUsedLongheads.Any() == false)
                        break;
                    
                    currentUnusedLonghead = longheadParameters.GetLongHeadByNumber(unUsedLongheads.First());
                }
            }
        }

        private void AdjustLongheadPositionIfNeeded(
            MicroMeter minPositionForUnplacedLonghead,
            MicroMeter maxPositionForUnplacedLonghead,
            EmLongHead currentUnusedLonghead,
            int numberOfUnplacedToFitBetweenMinMax)
        {
            if (LongheadIsOnValidSpot(
                minPositionForUnplacedLonghead,
                maxPositionForUnplacedLonghead,
                lhPositionsForCurrentIteration[currentUnusedLonghead.Number - 1]))
            {
                AdjustPositionRelativeToNextLongheadIfNeeded(
                    currentUnusedLonghead,
                    numberOfUnplacedToFitBetweenMinMax,
                    minPositionForUnplacedLonghead,
                    maxPositionForUnplacedLonghead);
            }
            else
            {
                MoveLongheadTheMinimumDistanceNeeded(
                    numberOfUnplacedToFitBetweenMinMax,
                    currentUnusedLonghead,
                    minPositionForUnplacedLonghead,
                    maxPositionForUnplacedLonghead);
            }
        }

        private void AdjustPositionRelativeToNextLongheadIfNeeded(
            EmLongHead currentUnusedLonghead,
            int numberOfUnplacedToFitBetweenMinMax,
            MicroMeter minPositionForUnplacedLonghead,
            MicroMeter maxPositionForUnplacedLonghead)
        {
            var nextLonghead = longheadParameters.GetNextLongHead(currentUnusedLonghead);
            var leftEdgeOfNextLonghead = GetLeftEdgeOfNextLonghead(nextLonghead);

            var maxPositionForCurrentUnplacedIfNextShouldNotBeMoved = leftEdgeOfNextLonghead - currentUnusedLonghead.Width
                                                                      + currentUnusedLonghead.LeftSideToTool;

            if (CanFitRestOfLongheadsInbetween(
                numberOfUnplacedToFitBetweenMinMax - 1,
                lhPositionsForCurrentIteration[currentUnusedLonghead.Number - 1],
                maxPositionForUnplacedLonghead,
                currentUnusedLonghead.Width) == false)
            {
                MoveLongheadTheMinimumDistanceNeeded(
                    numberOfUnplacedToFitBetweenMinMax,
                    currentUnusedLonghead,
                    minPositionForUnplacedLonghead,
                    maxPositionForUnplacedLonghead);
            }
                //if we are on the right side of the next longhead, we check if we can move to the left of the next one and skip on moving the next one instead
            else if (IsCurrentLongheadPlacedToTheRightOfNextLonghead(
                currentUnusedLonghead,
                nextLonghead,
                maxPositionForUnplacedLonghead,
                maxPositionForCurrentUnplacedIfNextShouldNotBeMoved,
                minPositionForUnplacedLonghead))
            {
                SetPositionForLonghead(currentUnusedLonghead.Number, maxPositionForCurrentUnplacedIfNextShouldNotBeMoved);
            }
        }

        private void CheckForLongheadDistributionException(
            MicroMeter minPositionForUnplacedLonghead,
            MicroMeter maxPositionForUnplacedLonghead,
            EmLongHead currentUnusedLonghead,
            int numberOfUnplacedToFitBetweenMinMax)
        {
            if (minPositionForUnplacedLonghead > maxPositionForUnplacedLonghead)
            {
                throw new LongHeadDistributionException();
            }

            if (CanFitUnplacedLongheadsBetweenMinAndMax(
                currentUnusedLonghead,
                numberOfUnplacedToFitBetweenMinMax,
                minPositionForUnplacedLonghead,
                maxPositionForUnplacedLonghead) == false)
            {
                throw new LongHeadDistributionException();
            }
        }

        private void SetPositionForLonghead(int number, MicroMeter position)
        {
            lhPositionsForCurrentIteration[number - 1] = position;
        }

        private EmLongHead GetPreviousPlacedLonghead(EmLongHead previousPlacedLonghead)
        {
            return previousPlacedLonghead != null
                ? longheadParameters.GetNextLongHead(previousPlacedLonghead)
                : longheadParameters.GetLongHeadByNumber(1);
        }

        private bool IsCurrentLongheadPlacedToTheRightOfNextLonghead(EmLongHead currentUnused, EmLongHead nextLonghead, MicroMeter maxPositionForUnplacedLonghead, MicroMeter maxPositionForCurrentUnplacedIfNextShouldNotBeMoved, MicroMeter minPositionForUnplacedLonghead)
        {
            return lhPositionsForCurrentIteration[currentUnused.Number - 1]
                   > (nextLonghead != null
                       ? lhPositionsForCurrentIteration[nextLonghead.Number - 1]
                       : maxPositionForUnplacedLonghead)
                   && maxPositionForCurrentUnplacedIfNextShouldNotBeMoved >= minPositionForUnplacedLonghead;
        }

        private MicroMeter GetLeftEdgeOfNextLonghead(EmLongHead nextLonghead)
        {
            return nextLonghead != null
                ? nextLonghead.GetLeftEdgeForPosition(lhPositionsForCurrentIteration[nextLonghead.Number - 1]) : longheadParameters.MaximumPosition;
        }

        private bool CanFitUnplacedLongheadsBetweenMinAndMax(EmLongHead currentUnused, int numberOfUnplacedToFitBetweenMinMax, MicroMeter minPositionForUnplacedLonghead, MicroMeter maxPositionForUnplacedLonghead)
        {
            return currentUnused.Width * (numberOfUnplacedToFitBetweenMinMax - 1) + minPositionForUnplacedLonghead
                   <= maxPositionForUnplacedLonghead;
        }

        private MicroMeter GetMaxPositionForUnplacedLonghead(EmLongHead nextPlacedLonghead, EmLongHead currentUnused)
        {
            return nextPlacedLonghead != null ?
                nextPlacedLonghead.GetLeftEdgeForPosition(lhPositionsForCurrentIteration[nextPlacedLonghead.Number - 1])
                - currentUnused.Width + currentUnused.LeftSideToTool : longheadParameters.MaximumPosition;
        }

        private MicroMeter GetMinPositionForUnplacedLonghead(EmLongHead previousPlacedLonghead, EmLongHead currentUnused)
        {
            return previousPlacedLonghead != null ?
                previousPlacedLonghead.GetRightEdgeForPosition(lhPositionsForCurrentIteration[previousPlacedLonghead.Number - 1])
                + currentUnused.LeftSideToTool : longheadParameters.MinimumPosition;
        }

        private EmLongHead GetNextPlacedLonghead(IList<int> unUsedLongheads, EmLongHead currentUnused)
        {
            return longheadParameters.GetLongHeadByNumber(
                longheadParameters.LongHeads.Select(l => l.Number)
                    .Where(lh => unUsedLongheads.Contains(lh) == false)
                    .OrderBy(l => l)
                    .DefaultIfEmpty(longheadParameters.LongHeads.Count() + 1)
                    .FirstOrDefault(pl => pl > currentUnused.Number));
        }

        private IList<int> GetUnusedLongheadNumbers(IEnumerable<IAStarNodeEM> iteration)
        {
            var longheads = longheadParameters.LongHeads.Select(l => l.Number).ToList();
            foreach (var iterationNode in iteration)
            {
                longheads.Remove(iterationNode.LongHead.Number);
            }
            return longheads;
        }

        private void MoveLongheadTheMinimumDistanceNeeded(
            int numberOfUnplacedToFitBetweenMinMax,
            EmLongHead currentUnused,
            MicroMeter minPos,
            MicroMeter maxPos)
        {
            var currentPos = lhPositionsForCurrentIteration[currentUnused.Number - 1];

            if (Math.Abs(minPos - currentPos) < Math.Abs(currentPos - maxPos))
            {
                lhPositionsForCurrentIteration[currentUnused.Number - 1] = minPos;
                return;
            }

            SetPositionForLonghead(
                currentUnused.Number,
                maxPos - (numberOfUnplacedToFitBetweenMinMax - 1) * currentUnused.Width);
        }

        private static bool CanFitRestOfLongheadsInbetween(int numberOfLhsToFit, MicroMeter minPos, MicroMeter maxPos, MicroMeter lhWidth)
        {
            return minPos + numberOfLhsToFit * lhWidth <= maxPos;
        }

        private static bool LongheadIsOnValidSpot(MicroMeter minPos, MicroMeter maxPos, MicroMeter currentPos)
        {
            return currentPos >= minPos && currentPos <= maxPos;
        }
    }
}
