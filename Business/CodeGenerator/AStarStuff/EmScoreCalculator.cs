﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    public class EmScoreCalculator
    {
        private const double MaximumReverseScore = 1000000;

        public static double CalculateMovementScoreForIteration(IAStarNodeEM node, List<IAStarNodeEM> distinctLongheads)
        {
            double score = 0;
            var longHeadsMovingLeft =
                distinctLongheads.Where(lh => node.NewLongHeadPositions[lh.LongHead.Number - 1] > lh.TargetCoordinate).ToList();
            var longHeadsMovingRight =
                distinctLongheads.Where(lh => node.NewLongHeadPositions[lh.LongHead.Number - 1] < lh.TargetCoordinate).ToList();

            // Does any long head require reposition
            if (longHeadsMovingLeft.Any())
            {
                score += node.MachineParameters.LongHeadParameters.ConnectDelayOnInSeconds;
                score += GetMovementScore(node, longHeadsMovingLeft);
            }

            if (longHeadsMovingRight.Any())
            {
                score += node.MachineParameters.LongHeadParameters.ConnectDelayOnInSeconds;
                score += GetMovementScore(node, longHeadsMovingRight);
            }

            return score;
        }

        public static double GetCalculatedScore(AStarNodeEM node)
        {
            double score = 0;

            var scoreList = node.VisitedNodesInBranch.ToList();
            while (scoreList.Count > 0)
            {
                var distinctLongheads = GetLongHeadsForIteration(scoreList);
                node.AddIteration(distinctLongheads);
                score += CalculateMovementScoreForIteration(node, distinctLongheads);
            }

            var sectionLength = node.Section.EndCoordinateY - node.Section.StartCoordinateY;

            if (sectionLength > node.MachineParameters.FeedRollerParameters.MaximumReverseDistance && node.NumberOfReverses != 0)
            {
                return score + MaximumReverseScore + node.NumberOfReverses;
            }

            score += CalculateReverseScoreForDistance(
                sectionLength,
                node.NumberOfReverses,
                node.MachineParameters.FeedRollerParameters);

            score += CalculateUpcomingReverseScore(node, sectionLength);
            return score;
        }

        private static List<IAStarNodeEM> GetLongHeadsForIteration(List<IAStarNodeEM> nodesInBranch)
        {
            IAStarNodeEM lastPositionedLongHead = null;
            var nodesForThisIteration = new List<IAStarNodeEM>();
            var nodesGroupedByLongHead = nodesInBranch.GroupBy(l => l.LongHead.Number).ToList();
            foreach (var nodesForLongHead in nodesGroupedByLongHead)
            {
                if (lastPositionedLongHead == null)
                {
                    lastPositionedLongHead = nodesForLongHead.ElementAt(0);
                    nodesForThisIteration.Add(lastPositionedLongHead);
                    nodesInBranch.Remove(lastPositionedLongHead);
                    continue;
                }
                foreach (var node in nodesForLongHead)
                {
                    if (CanPositionNodeWithoutCollision(node, lastPositionedLongHead))
                    {
                        lastPositionedLongHead = node;
                        nodesForThisIteration.Add(lastPositionedLongHead);
                        nodesInBranch.Remove(lastPositionedLongHead);
                        break;
                    }
                }
            }

            return nodesForThisIteration;
        }

        private static bool CanPositionNodeWithoutCollision(IAStarNodeEM nodeToInsert, IAStarNodeEM lastPositionedNodeInIteration)
        {
            var numberOfSkippedLongHeads = lastPositionedNodeInIteration.LongHead.Number - nodeToInsert.LongHead.Number - 1;
            return nodeToInsert.TargetCoordinate <=
                   lastPositionedNodeInIteration.TargetCoordinate - lastPositionedNodeInIteration.LongHead.LeftSideToTool -
                   nodeToInsert.MachineParameters.LongHeadParameters.LongheadWidth -
                   (nodeToInsert.MachineParameters.LongHeadParameters.LongheadWidth * numberOfSkippedLongHeads) +
                   nodeToInsert.LongHead.LeftSideToTool;
        }

        private static double GetMovementScore(IAStarNodeEM iteration, List<IAStarNodeEM> movingLonheads)
        {
            double score = 0;

            var deltas =
                movingLonheads.Select(lh => Math.Abs(lh.TargetCoordinate - iteration.NewLongHeadPositions[lh.LongHead.Number - 1]))
                    .Distinct().OrderBy(d => d).ToList();

            movingLonheads.ForEach(lh => iteration.NewLongHeadPositions[lh.LongHead.Number - 1] = lh.TargetCoordinate);

            for (var i = 0; i < deltas.Count(); i++)
            {
                score += CalculateMovementScoreForDistance(deltas[i], iteration.MachineParameters.LongHeadParameters);
                for (var j = i + 1; j < deltas.Count(); j++)
                {
                    deltas[j] -= deltas[i];
                }
            }

            return score;
        }

        private static double CalculateUpcomingReverseScore(AStarNodeEM node, MicroMeter sectionLength)
        {
            var nrRemainingVerticalLines = node.RemainingCoordinates != null ? node.RemainingCoordinates.Count() : 0;
            var nrLongToRight = node.MachineParameters.LongHeadParameters.LongHeads.Count(lh => lh.Number >= node.LongHead.Number);

            if (nrRemainingVerticalLines == 0 || nrLongToRight == 0)
            {
                return 0d;
            }

            //Number of lines that can still be converted by unpositioned longheads in current iteration
            nrRemainingVerticalLines = nrRemainingVerticalLines - (nrLongToRight - 1);

            if (node.NumberOfReverses > 0)
            {
                //Number of lines that can be converted by unpositioned longheads in previous iterations                
                var onlyOneLong = node.MachineParameters.LongHeadParameters.LongHeads.Count() == 1;
                nrRemainingVerticalLines = nrRemainingVerticalLines -
                                           ((onlyOneLong ? 0 : nrLongToRight) * node.NumberOfReverses);
            }

            if (nrRemainingVerticalLines <= 0)
            {
                return 0d;
            }

            var minIterationsLeftAfterCurrent = (int)(Math.Ceiling((double)nrRemainingVerticalLines / nrLongToRight));

            return CalculateReverseScoreForDistance(sectionLength, minIterationsLeftAfterCurrent, node.MachineParameters.FeedRollerParameters);
        }

        public static double CalculateMovementScoreForDistance(double distance, EmLongHeadParameters lhParams)
        {
            var score = GetScoreDuringAccelerationPhase(distance / 2, lhParams.MovementData) * 2;
            score += GetScoreDuringMaxSpeed(distance - 2 * GetMaximumAccelerationDistance(lhParams.MovementData), lhParams.MovementData);

            score += lhParams.ConnectDelayOffInSeconds;

            return score;
        }

        public static double CalculateReverseScoreForDistance(double actualReverseDistance, int totalNumberOfReverses, EmFeedRollerParameters rollerParams)
        {
            if (actualReverseDistance <= 0 || totalNumberOfReverses <= 0)
            {
                return 0d;
            }

            var score = GetScoreDuringAccelerationPhase(actualReverseDistance / 2, rollerParams.MovementData) * 2;
            score += GetScoreDuringMaxSpeed(actualReverseDistance - 2 * GetMaximumAccelerationDistance(rollerParams.MovementData), rollerParams.MovementData);
            score *= 5; //penalty constant should be tweaked

            return score * totalNumberOfReverses;
        }

        /* t = v/a and t * (v / 2) = s -> v^2/ 2a = s */
        private static double GetMaximumAccelerationDistance(IKinematicDataHolder rollerParameters)
        {
            //todo does this hold for inches?
            return Math.Pow(rollerParameters.ActualMaximumMovementSpeed, 2) / (2 * rollerParameters.ActualMaximumAcceleration);
        }

        /* s = (at^2)/2 and since t is our score -> sqrt(2s/a) = t
         * If distance is bigger than acceleration distance return s/Vaverage*/
        private static double GetScoreDuringAccelerationPhase(double distance, IKinematicDataHolder rollerParameters)
        {
            var maximumAccelerationDistance = GetMaximumAccelerationDistance(rollerParameters);

            if (distance >= maximumAccelerationDistance)
            {
                return maximumAccelerationDistance / (rollerParameters.ActualMaximumMovementSpeed / 2);
            }

            return Math.Sqrt(2 * distance / rollerParameters.ActualMaximumAcceleration);
        }

        private static double GetScoreDuringMaxSpeed(double distance, IKinematicDataHolder rollerParameters)
        {
            if (distance <= 0)
            {
                return 0d;
            }

            return distance / rollerParameters.ActualMaximumMovementSpeed;
        }
    }
}
