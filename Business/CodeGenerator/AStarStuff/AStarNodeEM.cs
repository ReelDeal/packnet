﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    using Common.Interfaces.DTO.PhysicalMachine;

    public class AStarNodeEM : IAStarNodeEM
    {
        private readonly IEnumerable<MicroMeter> remainingCoordinates;
        private readonly IEnumerable<WasteArea> remainingWasteAreas;
        private readonly EmPhysicalMachineSettings machineParameters;
        private readonly SortedSet<IAStarNodeEM> visitedNodesInBranchSortedByLongHeadAndPriority;
        private readonly EmLongHead longHead;
        private readonly MicroMeter[] originalLongHeadPostitions;
        private readonly MicroMeter targetCoordinate;
        private MicroMeter? minYCoordinate;
        private List<PhysicalLine> linesInSectionAtCoordinate;
        private List<PhysicalLine> linesInDesignAtCoordinate;
        private MicroMeter? maxYCoordinate;
        private bool? isOutFeedCoordinate;
        private bool? isSplitCoordinate;
        private bool? isDirectlyContinued;
        private bool? isContinued;
        private readonly IList<List<IAStarNodeEM>> iterations; 
        private readonly Section section;
        private readonly PhysicalDesign design;

        public static int NumberOfExpandedNodes = 0;
        public AStarNodeEM(SortedSet<IAStarNodeEM> visitedNodesInBranchSortedByLongHeadAndPriority, EmPhysicalMachineSettings machineParameters, MicroMeter[] currentLongHeadPositions, IEnumerable<MicroMeter> remainingCoordinates, IEnumerable<WasteArea> remainingWasteAreas, MicroMeter targetCoordinate, Section section, EmLongHead longHead, RuleAppliedPhysicalDesign design)
        {
            this.remainingCoordinates = remainingCoordinates;
            this.remainingWasteAreas = remainingWasteAreas;

            this.IsGoal = remainingCoordinates.Any() == false && remainingWasteAreas.Any() == false;
            this.machineParameters = machineParameters;
            this.targetCoordinate = targetCoordinate;
            this.longHead = longHead;
            this.section = section;
            this.design = design;
            this.visitedNodesInBranchSortedByLongHeadAndPriority = new SortedSet<IAStarNodeEM>(visitedNodesInBranchSortedByLongHeadAndPriority, visitedNodesInBranchSortedByLongHeadAndPriority.Comparer);
            originalLongHeadPostitions = currentLongHeadPositions;
            NewLongHeadPositions = originalLongHeadPostitions.ToArray();
            iterations = new List<List<IAStarNodeEM>>();

            if (this.longHead != null)
            {
                this.visitedNodesInBranchSortedByLongHeadAndPriority.Add(this);
                Score = EmScoreCalculator.GetCalculatedScore(this);
            }

            NumberOfExpandedNodes++;
        }

        public int NumberOfReverses { get { return Math.Max(0, iterations.Count() - 1); } }

        public double Score { get; private set; }

        public Section Section { get { return section; } }

        public MicroMeter[] NewLongHeadPositions { get; private set; }

        public MicroMeter[] OriginalLongHeadPositions { get { return originalLongHeadPostitions; } }

        public SortedSet<IAStarNodeEM> VisitedNodesInBranch { get { return new SortedSet<IAStarNodeEM>(visitedNodesInBranchSortedByLongHeadAndPriority, visitedNodesInBranchSortedByLongHeadAndPriority.Comparer); } }

        public bool IsGoal { get; private set; }

        public IEnumerable<MicroMeter> RemainingCoordinates { get { return remainingCoordinates; } }

        public IEnumerable<WasteArea> RemainingWasteAreas { get { return remainingWasteAreas; } }

        public LongHead NodeLongHead { get { return longHead; } }
        
        public EmPhysicalMachineSettings MachineParameters { get { return machineParameters; } }

        public EmLongHead LongHead { get { return longHead; } }

        public MicroMeter TargetCoordinate { get { return targetCoordinate; } }

        public IEnumerable<IEnumerable<IAStarNodeEM>> Iterations { get { return iterations; } }

        #region Processing Order Properties
        public bool IsOutFeedCoordinate
        {
            get
            {
                if (isOutFeedCoordinate.HasValue == false)
                {
                    isOutFeedCoordinate = section.EndCoordinateY == design.Length &&
                            LinesInSectionAtCoordinate.Any(l => l.EndCoordinate.Y == design.Length);
                }

                return isOutFeedCoordinate.Value;
            }
        }

        public bool IsSplitCoordinate
        {
            get
            {
                if (isSplitCoordinate.HasValue == false)
                {
                    isSplitCoordinate =
                        LinesInSectionAtCoordinate.Any(
                            l =>
                                l.Type == LineType.cut && l.StartCoordinate.Y <= section.StartCoordinateY &&
                                l.EndCoordinate.Y >= section.EndCoordinateY);
                }

                return isSplitCoordinate.Value;
            }
        }

        public bool IsDirectlyContinued
        {
            get
            {
                if (isDirectlyContinued.HasValue == false)
                {
                    var lineLongerThanSection = LinesInDesignAtCoordinate.Any(l => l.StartCoordinate.Y < section.EndCoordinateY && l.EndCoordinate.Y > section.EndCoordinateY);
                    var lineThatEndsAtSectionEnd = LinesInDesignAtCoordinate.Any(l => l.StartCoordinate.Y < section.EndCoordinateY && l.EndCoordinate.Y == section.EndCoordinateY);
                    var lineThatStartsAtSectionEnd = LinesInDesignAtCoordinate.Any(l => l.StartCoordinate.Y == section.EndCoordinateY);
                    isDirectlyContinued = lineLongerThanSection || (lineThatEndsAtSectionEnd && lineThatStartsAtSectionEnd);
                }
                return isDirectlyContinued.Value;
            }
        }

        public bool IsContinued
        {
            get
            {
                if (isContinued.HasValue == false)
                {
                    isContinued = LinesInDesignAtCoordinate.Any(l => l.StartCoordinate.Y > section.EndCoordinateY);
                }
                return isContinued.Value;
            }
        }

        public MicroMeter MinYCoordinate
        {
            get
            {
                if (minYCoordinate.HasValue == false)
                {
                    //var minCoordN1 = LinesInSectionAtCoordinate.Min(l => l.StartCoordinate.Y);
                    //todo: minYCoordinate needs to be calculated with waste areas
                    var minCoordN1 = LinesInSectionAtCoordinate.Any() ? LinesInSectionAtCoordinate.Min(l => l.StartCoordinate.Y) : section.StartCoordinateY;
                    minYCoordinate = minCoordN1 < section.StartCoordinateY ? section.StartCoordinateY : minCoordN1;
                }

                return minYCoordinate.Value;
            }
        }

        public MicroMeter MaxYCoordinate
        {
            get
            {
                if (maxYCoordinate.HasValue == false)
                {
                    //var maxCoordN1 = LinesInSectionAtCoordinate.Max(l => l.EndCoordinate.Y);
                    //todo: maxYCoordinate needs to be calculated with waste areas
                    var maxCoordN1 = LinesInSectionAtCoordinate.Any() ? LinesInSectionAtCoordinate.Max(l => l.EndCoordinate.Y) : section.EndCoordinateY;
                    maxYCoordinate = maxCoordN1 > section.EndCoordinateY ? section.EndCoordinateY : maxCoordN1;
                }
                return maxYCoordinate.Value;
            }
        }

        private IEnumerable<PhysicalLine> LinesInDesignAtCoordinate
        {
            get {
                return linesInDesignAtCoordinate ??
                       (linesInDesignAtCoordinate = design.Lines.GetVerticalLinesAtCoordinate(targetCoordinate).ToList());
            }
        }

        private IEnumerable<PhysicalLine> LinesInSectionAtCoordinate
        {
            get {
                return linesInSectionAtCoordinate ??
                       (linesInSectionAtCoordinate = section.Lines.GetVerticalLinesAtCoordinate(targetCoordinate).ToList());
            }
        }
        #endregion;

        public static IAStarNodeEM CreateStartNode(EmPhysicalMachineSettings machineParameters, MicroMeter[] currentLongHeadPositions, Section section, RuleAppliedPhysicalDesign design)
        {
            //NumberOfExpandedNodes = 0;
            return new AStarNodeEM(
                new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(currentLongHeadPositions)),
                machineParameters,
                currentLongHeadPositions,
                section.Lines.DistinctBy(l => l.StartCoordinate.X).Select(l => l.StartCoordinate.X).OrderBy(x => x).ToList(),
                section.WasteAreas.DistinctBy(wa => wa.TopLeft.X).OrderBy(wa => wa.TopLeft.X).ToList(), null, section, null, design);
        }


        public void AddIteration(List<IAStarNodeEM> nodes)
        {
            iterations.Add(nodes);
        }


        public override string ToString()
        {
            return longHead == null ? "\n" : String.Format("Section: {3}->{4} | LH: {0}, pos: {1}, Score {2}", longHead.Number, targetCoordinate, Score, section.StartCoordinateY, section.EndCoordinateY);
        }
    }
}