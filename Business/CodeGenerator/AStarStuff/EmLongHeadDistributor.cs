﻿using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    public class EmLongHeadDistributor
    {
        public LongHeadDistributionSolution DistributeLongHeads(EmPhysicalMachineSettings machineSettings,
            MicroMeter[] longHeadPositions, ISectionCreator sectionCreator,
            ISectionEvaluator sectionEvaluator, RuleAppliedPhysicalDesign design)
        {
            var lineSections = sectionCreator.GetSectionsFromDesign(design);
            var merged = sectionCreator.MergeSections(lineSections);
            var bestSolution = new KeyValuePair<IEnumerable<Section>, LongHeadDistributionSolution>();

            double bestScore = double.MaxValue;
            foreach (var section in merged)
            {
                var longheadDistribution = sectionEvaluator.GetLongHeadDistribution(machineSettings, longHeadPositions, section, lineSections.Count(), design, bestScore);

                if(longheadDistribution == null)
                    continue;

                if (bestSolution.Value == null || bestSolution.Value.Score > longheadDistribution.Score)
                {
                    bestSolution = new KeyValuePair<IEnumerable<Section>, LongHeadDistributionSolution>(section.Sections,
                        longheadDistribution);
                    bestScore = longheadDistribution.Score;

                    if (longheadDistribution.NumberOfReverses == 0)
                        break;
                }
            }

            if(bestSolution.Value == null)
                throw new LongHeadDistributionException();

            if (bestSolution.Value.NumberOfReverses > 0)
            {

                if (bestSolution.Value.Sections.Any(s => s.Length > machineSettings.FeedRollerParameters.MaximumReverseDistance))
                {
                    var splitSections = sectionCreator.SplitSections(
                        machineSettings.FeedRollerParameters.MaximumReverseDistance,
                        bestSolution).ToList();

                    var newSolution = sectionEvaluator.GetLongHeadDistribution(
                        machineSettings,
                        longHeadPositions,
                        splitSections,
                        lineSections.Count(),
                        design,
                        double.MaxValue);

                    bestSolution = new KeyValuePair<IEnumerable<Section>, LongHeadDistributionSolution>(
                        bestSolution.Key,
                        newSolution);
                }


            }

            var adjuster = new LongheadPositionAdjuster(machineSettings.LongHeadParameters);

            adjuster.AdjustPositionOfLongheadsInSolution(bestSolution.Value);

            return bestSolution.Value;
        }
    } 
}
