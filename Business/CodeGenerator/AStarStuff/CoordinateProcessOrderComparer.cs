﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    public class CoordinateProcessOrderComparer : IComparer<IAStarNodeEM>
    {
        private MicroMeter[] originalPositions;
        public CoordinateProcessOrderComparer(MicroMeter[] originalPositions)
        {
            this.originalPositions = originalPositions;
        }

        public int Compare(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            var compareResult = n2.LongHead.Number.CompareTo(n1.LongHead.Number);
            if (compareResult != 0)
                return compareResult;

            compareResult = CompareOutFeedLines(n1, n2);
            if (compareResult != 0)
                return compareResult;

            compareResult = CompareSplitLines(n1, n2);
            if (compareResult != 0)
                return compareResult;

            compareResult = CompareDirectlyContinuedLines(n1, n2);
            if (compareResult != 0)
                return compareResult;

            compareResult = CompareContinuedLines(n1, n2);
            if (compareResult != 0)
                return compareResult;

            compareResult = CompareLinesToMinimzeReverseDistance(n1, n2);
            if (compareResult != 0)
                return compareResult;

            compareResult = CompareLongHeadAlreadyInPosition(n1, n2);
            if (compareResult != 0)
                return compareResult;

            compareResult = CompareStartPositionOfLines(n1, n2);
            if (compareResult != 0)
                return compareResult;

            return n1.TargetCoordinate.CompareTo(n2.TargetCoordinate);
        }

        private static int CompareStartPositionOfLines(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            return n1.MinYCoordinate.CompareTo(n2.MinYCoordinate);
        }

        private int CompareLongHeadAlreadyInPosition(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            var hasLongHeadInPositionN1 = originalPositions[n1.LongHead.Number - 1] == n1.TargetCoordinate;
            var hasLongHeadInPositionN2 = originalPositions[n2.LongHead.Number - 1] == n2.TargetCoordinate;

            return hasLongHeadInPositionN2.CompareTo(hasLongHeadInPositionN1);
        }

        private static int CompareLinesToMinimzeReverseDistance(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            var reverseDistanceN1ToN2 = n1.MaxYCoordinate - n2.MinYCoordinate;
            var reverseDistanceN2ToN1 = n2.MaxYCoordinate - n1.MinYCoordinate;

            return reverseDistanceN1ToN2.CompareTo(reverseDistanceN2ToN1);
        }

        private static int CompareDirectlyContinuedLines(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            return n1.IsDirectlyContinued.CompareTo(n2.IsDirectlyContinued);
        }

        private static int CompareContinuedLines(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            return n1.IsContinued.CompareTo(n2.IsContinued);
        }

        private static int CompareSplitLines(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            return n1.IsSplitCoordinate.CompareTo(n2.IsSplitCoordinate);
        }

        private static int CompareOutFeedLines(IAStarNodeEM n1, IAStarNodeEM n2)
        {
            var result = n1.IsOutFeedCoordinate.CompareTo(n2.IsOutFeedCoordinate);

            if (result == 0 && n1.IsOutFeedCoordinate)
            {
                var lineN1Length = n1.MaxYCoordinate - n1.MinYCoordinate;
                var lineN2Length = n2.MaxYCoordinate - n2.MinYCoordinate;

                return lineN1Length.CompareTo(lineN2Length);
            }

            return result;
        }
    }
}
