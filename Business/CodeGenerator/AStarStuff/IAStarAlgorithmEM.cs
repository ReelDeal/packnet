using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    using PackNet.Common.Interfaces.DTO.PhysicalMachine;

    internal interface IAStarAlgorithmEM
    {
        IAStarNodeEM FindSolution(EmPhysicalMachineSettings machineSettings, MicroMeter[] longHeadPositions, IEnumerable<PhysicalLine> verticalLines, MicroMeter SectionLength);
    }
}