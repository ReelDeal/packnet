﻿using PackNet.Common.Interfaces;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System.Collections.Generic;
    using System.Linq;

    public class SortedEmNodeQueue
    {
        private readonly SortedDictionary<double, Stack<IAStarNodeEM>> scoreDictionary;

        public bool Empty
        {
            get { return scoreDictionary.Any() == false; } 
        }

        public SortedEmNodeQueue()
        {
            scoreDictionary = new SortedDictionary<double, Stack<IAStarNodeEM>>();

        }

        public void Enqueue(IAStarNodeEM node)
        {
            if (scoreDictionary.ContainsKey(node.Score))
                scoreDictionary[node.Score].Push(node);
            else
                scoreDictionary.Add(node.Score, new Stack<IAStarNodeEM>(new List<IAStarNodeEM> { node }));
        }

        public void Enqueue(IEnumerable<IAStarNodeEM> nodes)
        {
            foreach (var node in nodes.Reverse())
            {
                Enqueue(node);
            }
        }

        public IAStarNodeEM Dequeue()
        {
            if (scoreDictionary.Keys.Any())
            {
                var stackWithLowestScore = scoreDictionary.First();
                var node = stackWithLowestScore.Value.Pop();
                if (!stackWithLowestScore.Value.Any())
                {
                    scoreDictionary.Remove(stackWithLowestScore.Key);
                }

                return node;
            }

            return null;
        }
    }
}
