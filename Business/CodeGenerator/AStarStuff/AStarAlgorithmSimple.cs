﻿namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO;

    internal class AStarAlgorithmSimple : AStarAlgorithm
    {
        protected override AStarNode ReturnSolution(Stack<AStarNode> openList, MicroMeter[] longHeadPositions)
        {
            AStarNode solution = null;

            while (openList.Count > 0)
            {
                var currentNode = openList.Pop();
                if (currentNode.IsGoal())
                {
                    solution = currentNode;
                    break;
                }

                currentNode.GetSuccessors().ForEach(openList.Push);
            }

            return solution;
        }
    }
}
