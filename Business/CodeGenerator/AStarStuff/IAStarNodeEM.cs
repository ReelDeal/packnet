﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System.Collections.Generic;

    using PackNet.Common.Interfaces.DTO;

    public interface IAStarNodeEM
    {
        bool IsGoal { get; }

        double Score { get; }

        int NumberOfReverses { get; }

        MicroMeter[] NewLongHeadPositions { get; }

        MicroMeter[] OriginalLongHeadPositions { get; }

        EmPhysicalMachineSettings MachineParameters { get; }

        EmLongHead LongHead { get; }
        
        MicroMeter TargetCoordinate { get; }

        IEnumerable<IEnumerable<IAStarNodeEM>> Iterations { get; }

        bool IsOutFeedCoordinate { get; }

        bool IsSplitCoordinate { get; }

        bool IsDirectlyContinued { get; }

        bool IsContinued { get; }

        MicroMeter MinYCoordinate { get; }

        MicroMeter MaxYCoordinate { get; }

        SortedSet<IAStarNodeEM> VisitedNodesInBranch { get; }

        IEnumerable<MicroMeter> RemainingCoordinates { get; }

        IEnumerable<WasteArea> RemainingWasteAreas { get; }
    }
}
