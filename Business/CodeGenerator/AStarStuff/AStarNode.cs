﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;
    using Common.Interfaces.DTO.PhysicalMachine;

    internal class AStarNode
    {
        private readonly IEnumerable<PhysicalLine> remainingVerticalLines;
        private readonly FusionLongHeadParameters longHeadParameters;
        private readonly int originalNumberOfVerticalLines ;
        private readonly MicroMeter trackLeftSide;
        private readonly MicroMeter trackRightSide;
        private readonly bool[] longHeadsEvaluated;
        private AStarNode parent;

        internal AStarNode(AStarNode parent, FusionLongHeadParameters longHeadParameters, MicroMeter[] currentLongHeadPositions, bool[] longHeadsEvaluated, MicroMeter trackLeftSide, MicroMeter trackRightSide, IEnumerable<PhysicalLine> remainingVerticalLines, int originalNumberOfVerticalLines)
        {
            this.parent = parent;
            this.remainingVerticalLines = remainingVerticalLines;
            this.originalNumberOfVerticalLines = originalNumberOfVerticalLines;
            this.longHeadParameters = longHeadParameters;
            this.trackLeftSide = trackLeftSide;
            this.trackRightSide = trackRightSide;
            NewLongHeadPositions = currentLongHeadPositions;
            this.longHeadsEvaluated = longHeadsEvaluated;
        }

        internal MicroMeter[] NewLongHeadPositions { get; private set; }

        internal bool IsGoal()
        {
            return !remainingVerticalLines.Any() && AreRemainingLongHeadsPositionedOutsideCorrugateWithCollision();
        }

        internal double Score(MicroMeter[] originalPos)
        {
            double repos = 0;
            for (int i = 0; i < NewLongHeadPositions.Length; i++)
            {
                if (originalPos[i] != NewLongHeadPositions[i])
                {
                    repos += 200;
                    repos += Math.Abs((double)(originalPos[i] - NewLongHeadPositions[i]));
                }
            }

            return repos;
        }

        internal List<AStarNode> GetSuccessors()
        {
            if (IsAllLongHeadsEvaluated())
            {
                return new List<AStarNode>();
            }

            int numberOfLongHeads = longHeadParameters.LongHeads.Count();
            var longHead = GetLongHeadToPosition();
            return GetSuccessors(longHead, numberOfLongHeads);
        }

        private List<AStarNode> GetSuccessors(FusionLongHead longHead, int numberOfLongHeads)
        {
            var successors = new List<AStarNode>();

            int currentLongHeadNumber = longHead.Number;
            if (remainingVerticalLines.Any())
            {
                var node = GetSuccessorThatPositionatesLongHeadOnLine(longHead);
                if (node != null)
                {
                    successors.Add(node);
                }

                successors.AddRange(GetSuccessorsThatPositionateLongHeadOutsideCorrugateOnLeftSideToEnableOtherLongheadsToBeUsed(longHead, numberOfLongHeads));

                if (NewLongHeadPositions[currentLongHeadNumber - 1] <= trackLeftSide && CollideWithPreviousLhOrMachineEdge(currentLongHeadNumber, NewLongHeadPositions) == false)
                {
                    var newLongHeadEvaluated = longHeadsEvaluated.ToArray();
                    newLongHeadEvaluated[currentLongHeadNumber - 1] = true;
                    successors.Add(new AStarNode(this, longHeadParameters, NewLongHeadPositions, newLongHeadEvaluated, trackLeftSide, trackRightSide, remainingVerticalLines, originalNumberOfVerticalLines));
                }
            }
            else
            {
                var node = GetSuccessorThatPositionatesOutsideCorrugateOnRightSide(longHead, numberOfLongHeads);
                if (node != null)
                {
                    successors.Add(node);
                }

                successors.AddRange(GetSuccessorsThatPositionateLongHeadOutsideCorrugateOnLeftSideToEnableOtherLongheadsToBeUsed(longHead,numberOfLongHeads));
            }

            return successors;
        }

        private AStarNode GetSuccessorThatPositionatesLongHeadOnLine(FusionLongHead longHead)
        {
            PhysicalLine line = remainingVerticalLines.ElementAt(0);
            int currentLongHeadNumber = longHead.Number;

            if (CanUseLongHeadOnLine(longHead, line))
            {
                var newLongHeadEvaluated = longHeadsEvaluated.ToArray();
                newLongHeadEvaluated[currentLongHeadNumber - 1] = true;
                var newLongHeadPositions = NewLongHeadPositions.ToArray();
                newLongHeadPositions[currentLongHeadNumber - 1] = line.StartCoordinate.X;
                return new AStarNode(this, longHeadParameters, newLongHeadPositions, newLongHeadEvaluated, trackLeftSide, trackRightSide, remainingVerticalLines.Skip(1), originalNumberOfVerticalLines);
            }

            return null;
        }

        private IEnumerable<AStarNode> GetSuccessorsThatPositionateLongHeadOutsideCorrugateOnLeftSideToEnableOtherLongheadsToBeUsed(FusionLongHead longHead, int numberOfLongHeads)
        {
            List<AStarNode> successors = new List<AStarNode>();
            int currentLongHeadNumber = longHead.Number;

            if (CanPositionLongHeadOnCoordinate(longHead, trackLeftSide))
            {
                var newLongHeadEvaluated = longHeadsEvaluated.ToArray();
                newLongHeadEvaluated[currentLongHeadNumber - 1] = true;
                MicroMeter coordinateOutSide = trackLeftSide;
                for (int lh = currentLongHeadNumber; lh <= numberOfLongHeads; lh++)
                {
                    if (lh != currentLongHeadNumber)
                    {
                        FusionLongHead prevLongHead = longHeadParameters.LongHeads.Single(h => h.Number == lh - 1);
                        FusionLongHead currentLongHead = longHeadParameters.LongHeads.Single(h => h.Number == lh); 
                        coordinateOutSide = coordinateOutSide + prevLongHead.LeftSideToTool - longHeadParameters.LongheadWidth - currentLongHead.LeftSideToTool;
                    }

                    if (CanPositionLongHeadOnCoordinate(longHead, coordinateOutSide) == false)
                    {
                        break;
                    }

                    if (coordinateOutSide <= trackLeftSide)
                    {
                        var newLongHeadPositions = NewLongHeadPositions.ToArray();
                        newLongHeadPositions[currentLongHeadNumber - 1] = coordinateOutSide;
                        successors.Add(new AStarNode(this, longHeadParameters, newLongHeadPositions, newLongHeadEvaluated, trackLeftSide, trackRightSide, remainingVerticalLines, originalNumberOfVerticalLines));
                    }
                }
            }

            return successors;
        }

        private AStarNode GetSuccessorThatPositionatesOutsideCorrugateOnRightSide(FusionLongHead longHead, int numberOfLongHeads)
        {
            int currentLongHeadNumber = longHead.Number;

            if (CanPositionLongHeadOnCoordinateOutsideCorrugateRightSide(longHead, trackRightSide, numberOfLongHeads))
            {
                var newLongHeadEvaluated = longHeadsEvaluated.ToArray();
                newLongHeadEvaluated[currentLongHeadNumber - 1] = true;
                MicroMeter coordinate = GetMinimumPositionOfLongHeadLeftSide(longHead);

                if (coordinate < trackRightSide)
                {
                    coordinate = MaximumPossiblePositionOnTheRightSideOfTheTrack();
                }
                                
                var newLongHeadPositions = NewLongHeadPositions.ToArray();
                newLongHeadPositions[currentLongHeadNumber - 1] = coordinate;
                return new AStarNode(this, longHeadParameters, newLongHeadPositions, newLongHeadEvaluated, trackLeftSide, trackRightSide, remainingVerticalLines, originalNumberOfVerticalLines);
            }

            return null;
        }

        private MicroMeter MaximumPossiblePositionOnTheRightSideOfTheTrack()
        {
            return trackRightSide > longHeadParameters.MaximumPosition ? longHeadParameters.MaximumPosition : trackRightSide;
        }

        private bool IsAllLongHeadsEvaluated()
        {
            return !longHeadsEvaluated.Where(b => b == false).Any();
        }

        private bool CanPositionLongHeadOnCoordinateOutsideCorrugateRightSide(FusionLongHead longHead, MicroMeter coordinate, int numberOfLongHeads)
        {
            MicroMeter minimumPosition = GetMinimumPositionOfLongHeadLeftSide(longHead);
            MicroMeter maximumPosition = GetMaximumPossiblePositionLongHeadRightSide(longHead, numberOfLongHeads);

            return maximumPosition >= minimumPosition;
        }

        private MicroMeter GetMaximumPossiblePositionLongHeadRightSide(FusionLongHead longHead, int numberOfLongHeads)
        {
            if (longHead.Number == numberOfLongHeads)
            {             
                return longHeadParameters.MaximumPosition;
            }
            FusionLongHead nextLongHead = longHeadParameters.LongHeads.Single(lh => lh.Number == longHead.Number + 1);
            MicroMeter maximumPositionOfRight = GetMaximumPossiblePositionLongHeadRightSide(nextLongHead, numberOfLongHeads);
            return maximumPositionOfRight - nextLongHead.LeftSideToTool - longHeadParameters.LongheadWidth + longHead.LeftSideToTool;
        }

        private FusionLongHead GetLongHeadToPosition()
        {
            for (int i = 0; i < longHeadsEvaluated.Length; i++)
            {
                if (longHeadsEvaluated[i] == false)
                {                    
                    return longHeadParameters.LongHeads.Single(lh => lh.Number == i + 1);
                }
            }

            return null;
        }

        private bool CanUseLongHeadOnLine(FusionLongHead currentLongHead, PhysicalLine line)
        {
            return IsLineAndToolTypeCompatible(line.Type, currentLongHead)
                && CanPositionLongHeadOnCoordinate(currentLongHead, line.StartCoordinate.X);
        }

        private bool CanPositionLongHeadOnCoordinate(FusionLongHead currentLongHead, MicroMeter coordinate)
        {
            return GetMinimumPositionOfLongHeadLeftSide(currentLongHead) <= coordinate;
        }

        private MicroMeter GetMinimumPositionOfLongHeadLeftSide(FusionLongHead currentLongHead)
        {
            var longHeadNumber = currentLongHead.Number;
            if (longHeadNumber == 1)
            {
                return longHeadParameters.MinimumPosition;
            }
            return NewLongHeadPositions[longHeadNumber - 2] - longHeadParameters.LongHeads.Single(lh => lh.Number == longHeadNumber - 1).LeftSideToTool + longHeadParameters.LongheadWidth + currentLongHead.LeftSideToTool;
        }

        private bool IsLineAndToolTypeCompatible(LineType lineType, FusionLongHead currentLongHead)
        {
            return (lineType == LineType.crease && currentLongHead.Type == ToolType.Crease)
                || (lineType == LineType.cut && currentLongHead.Type == ToolType.Cut && CanUseToolOnCutLine(currentLongHead));
        }

        private bool CanUseToolOnCutLine(FusionLongHead currentLongHead)
        {
            //We are only allowed to position for first or last long head on the first and last line if they are cut lines since the other tools of typ cut have the tiling knife
            if (remainingVerticalLines.Count() == originalNumberOfVerticalLines || remainingVerticalLines.Count() == 1)
            {
                if(currentLongHead == longHeadParameters.LongHeads.First() || currentLongHead == longHeadParameters.LongHeads.Last())
                    return true;

                return false;
            }

            return true;
        }

        private bool CollideWithPreviousLhOrMachineEdge(int longHeadNumber, MicroMeter[] pos)
        {
            if (longHeadNumber == 1)
            {
                return false;
            }

            MicroMeter leftSideCurrent = GetLeftSidePosition(longHeadNumber, pos[longHeadNumber - 1]);
            MicroMeter rightSideOfPrev = GetRightSidePosition(longHeadNumber - 1, pos[longHeadNumber - 2]);
            return rightSideOfPrev > leftSideCurrent;
        }

        private MicroMeter GetLeftSidePosition(int longHeadNumber, MicroMeter pos)
        {
            var lh = longHeadParameters.LongHeads.Single(h => h.Number == longHeadNumber);
            return pos - lh.LeftSideToTool;
        }

        private MicroMeter GetRightSidePosition(int longHeadNumber, MicroMeter pos)
        {
            var lh = longHeadParameters.LongHeads.Single(h => h.Number == longHeadNumber);
            return pos - lh.LeftSideToTool + longHeadParameters.LongheadWidth;
        }

        private bool AreRemainingLongHeadsPositionedOutsideCorrugateWithCollision()
        {
            int firstUnPositioned = 0;
            do
            {
                firstUnPositioned++;
            }
            while (firstUnPositioned < longHeadsEvaluated.Length && longHeadsEvaluated[firstUnPositioned]);

            for (int i = firstUnPositioned; i < longHeadsEvaluated.Length; i++)
            {
                if (NewLongHeadPositions[i] < trackRightSide || CollideWithPreviousLhOrMachineEdge(i + 1, NewLongHeadPositions))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
