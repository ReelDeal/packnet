﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System;

    public class EmSuccessorCreator : ISuccessorCreator
    {
        public IEnumerable<IAStarNodeEM> GetSuccessors(IAStarNodeEM node, EmPhysicalMachineSettings machineSettings, Section section, RuleAppliedPhysicalDesign design)
        {
            if (node.RemainingCoordinates.Any() || node.RemainingWasteAreas.Any())
            {
                var longHeads = machineSettings.LongHeadParameters.LongHeads;
                return node.LongHead == null
                    ? longHeads.Select(lh => GetSuccessorThatPositionsLongHeadOnLine(lh, node.RemainingCoordinates, node.RemainingWasteAreas, machineSettings, node, section, design)).Where(n => n != null).ToList()
                    : longHeads.Where(lh => lh.Number >= node.LongHead.Number).Select(lh => GetSuccessorThatPositionsLongHeadOnLine(lh, node.RemainingCoordinates, node.RemainingWasteAreas, machineSettings, node, section, design)).Where(n => n != null).ToList();
        
            }
            return new List<AStarNodeEM>();
        }

        private static AStarNodeEM GetSuccessorThatPositionsLongHeadOnLine(
            EmLongHead longheadToPosition,
            IEnumerable<MicroMeter> remainingCoordinates, 
            IEnumerable<WasteArea> remainingWasteAreas,
            EmPhysicalMachineSettings machineSettings, 
            IAStarNodeEM node,
            Section section,
            RuleAppliedPhysicalDesign design)
        {
            var coordinateInWaste = remainingCoordinates.First();
                if (!CanUseLongHeadOnLine(longheadToPosition, coordinateInWaste))
                    return null;
                return new AStarNodeEM(node.VisitedNodesInBranch, machineSettings, node.OriginalLongHeadPositions.ToArray(), remainingCoordinates.Skip(1).ToList(), remainingWasteAreas, coordinateInWaste, section, longheadToPosition, design);
        }

        private static bool CanSeparateWasteOnNextCoordinate(EmLongHead longheadToPosition, WasteArea wasteArea, MicroMeter coordinateForNextLine)
        {
            if((/*Waste separator is on left side */longheadToPosition.LeftSideToWasteSeparator - longheadToPosition.LeftSideToTool < 0 && coordinateForNextLine != wasteArea.BottomRight.X)
            || (/*Waste separator is on right side*/longheadToPosition.LeftSideToWasteSeparator - longheadToPosition.LeftSideToTool > 0 && coordinateForNextLine != wasteArea.TopLeft.X))
                return false;
            return true;
        }

        private static bool CanUseLongheadOnWasteArea(EmLongHead longheadToPosition, WasteArea wasteArea)
        {
            return (longheadToPosition.GetLeftEdgeForPosition(longheadToPosition.MinimumPosition) <= wasteArea.TopLeft.X
                   && (longheadToPosition.GetRightEdgeForPosition(longheadToPosition.MaximumPosition) >= wasteArea.BottomRight.X));
        }

        private static bool CanUseLongHeadOnLine(EmLongHead currentLongHead, MicroMeter coordinate)
        {
            return currentLongHead.MinimumPosition <= coordinate
                   && currentLongHead.MaximumPosition >= coordinate;
        }
    }
}
