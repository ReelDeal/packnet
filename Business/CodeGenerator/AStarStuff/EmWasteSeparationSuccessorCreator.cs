﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator.AStarStuff
{
    using System;

    public class EmWasteSeparationSuccessorCreator : ISuccessorCreator
    {
        public IEnumerable<IAStarNodeEM> GetSuccessors(IAStarNodeEM node, EmPhysicalMachineSettings machineSettings, Section section, RuleAppliedPhysicalDesign design)
        {
            if (node.RemainingCoordinates.Any() || node.RemainingWasteAreas.Any())
            {
                var longHeads = node.LongHead == null ? machineSettings.LongHeadParameters.LongHeads : machineSettings.LongHeadParameters.LongHeads.Skip(node.LongHead.Number - 1);
                var successors = new List<IAStarNodeEM>();
                foreach (var lh in longHeads)
                {
                    var successorsForLh = GetSuccessorThatPositionsLongHeadOnLine(lh, node.RemainingCoordinates, node.RemainingWasteAreas, machineSettings, node, section, design);
                    if(successorsForLh != null)
                        successors.AddRange(successorsForLh);
                }

                return successors;
        
            }
            return new List<AStarNodeEM>();
        }

        private static IEnumerable<AStarNodeEM> GetSuccessorThatPositionsLongHeadOnLine(
            EmLongHead longheadToPosition,
            IEnumerable<MicroMeter> remainingCoordinates,
            IEnumerable<WasteArea> remainingWasteAreas,
            EmPhysicalMachineSettings machineSettings,
            IAStarNodeEM node,
            Section section,
            RuleAppliedPhysicalDesign design)
        {
            if (!remainingWasteAreas.Any())
            {
                var coordinateForNextLine = remainingCoordinates.First();
                if (!CanUseLongHeadOnLine(longheadToPosition, coordinateForNextLine))
                    return null;

                return new[]
                {
                    new AStarNodeEM(node.VisitedNodesInBranch, machineSettings, node.OriginalLongHeadPositions.ToArray(),
                        remainingCoordinates.Skip(1).ToList(), remainingWasteAreas, coordinateForNextLine, section, longheadToPosition,
                        design)
                };
            }
            

            var wasteArea = remainingWasteAreas.First();
            if (!CanUseLongheadOnWasteArea(longheadToPosition, wasteArea))
                return null;

            if (remainingCoordinates.Any())
            {
                var coordinateForNextLine = remainingCoordinates.First();

                //We can use LH to convert next line
                if (CanUseLongHeadOnLine(longheadToPosition,coordinateForNextLine))
                {
                    //We can separate waste at the same time as we convert next line
                    if (CanSeparateWasteOnCoordinate(longheadToPosition, wasteArea, coordinateForNextLine, design))
                    {
                        return new[]
                        {
                            new AStarNodeEM(node.VisitedNodesInBranch, machineSettings, node.OriginalLongHeadPositions.ToArray(),
                                remainingCoordinates.Skip(1).ToList(), remainingWasteAreas.Skip(1).ToList(), coordinateForNextLine,
                                section, longheadToPosition, design)
                        };
                    }
                    
                    if(coordinateForNextLine <= wasteArea.TopLeft.X)
                    {
                        return
                            new[]
                            {
                                new AStarNodeEM(node.VisitedNodesInBranch, machineSettings,
                                    node.OriginalLongHeadPositions.ToArray(),
                                    remainingCoordinates.Skip(1).ToList(), remainingWasteAreas.ToList(), coordinateForNextLine,
                                    section, longheadToPosition, design)
                            };
                    }
                
                }
            }

            return CreateLongHeadInWastePositions(node, machineSettings,
                remainingCoordinates.ToList(), remainingWasteAreas.Skip(1).ToList(),
                section, longheadToPosition, design, wasteArea);
        }

        private static IEnumerable<AStarNodeEM> CreateLongHeadInWastePositions(IAStarNodeEM node,
            EmPhysicalMachineSettings machineSettings, List<MicroMeter> remainingCoordinates, List<WasteArea> remainingWasteAreas,
            Section section, EmLongHead longheadToPosition, RuleAppliedPhysicalDesign design, WasteArea wasteArea)
        {
            var inWastePositions = new List<AStarNodeEM>();
           
            // Position LH at min positions to still be in waste
            var minCoordinateInWaste = Math.Max(longheadToPosition.MinimumPosition, wasteArea.TopLeft.X + longheadToPosition.LeftSideToTool - longheadToPosition.LeftSideToWasteSeparator);

            inWastePositions.Add(new AStarNodeEM(node.VisitedNodesInBranch, machineSettings, node.OriginalLongHeadPositions.ToArray(), remainingCoordinates, remainingWasteAreas, minCoordinateInWaste, section, longheadToPosition, design));

            // Position LH at min position without moving previous
            var minCorridnateWithoutMovingPrevious = GetMinPositionForLongHeadWithoutMovingPrevious(node,
                machineSettings, longheadToPosition);
            if (minCorridnateWithoutMovingPrevious - longheadToPosition.LeftSideToTool +
                longheadToPosition.LeftSideToWasteSeparator >= wasteArea.TopLeft.X &&
                minCorridnateWithoutMovingPrevious + longheadToPosition.Width - longheadToPosition.LeftSideToTool -
                longheadToPosition.LeftSideToWasteSeparator <= wasteArea.BottomRight.X)
            {
                inWastePositions.Add(new AStarNodeEM(node.VisitedNodesInBranch, machineSettings,
                    node.OriginalLongHeadPositions.ToArray(), remainingCoordinates, remainingWasteAreas,
                    minCorridnateWithoutMovingPrevious, section, longheadToPosition, design));
            }

            // Leave LH in current position if alread position in waste
                if (longheadToPosition.CurrentLeftEdge + longheadToPosition.LeftSideToWasteSeparator >= wasteArea.TopLeft.X
                && longheadToPosition.CurrentRightEdge - longheadToPosition.LeftSideToWasteSeparator <= wasteArea.BottomRight.X)
            {
                inWastePositions.Add(
                    new AStarNodeEM(
                        node.VisitedNodesInBranch,
                        machineSettings,
                        node.OriginalLongHeadPositions.ToArray(),
                        remainingCoordinates,
                        remainingWasteAreas,
                        longheadToPosition.Position,
                        section,
                        longheadToPosition,
                        design));
            }

            return inWastePositions;
        }

        private static MicroMeter GetMinPositionForLongHeadWithoutMovingPrevious(IAStarNodeEM node, EmPhysicalMachineSettings machineSettings, EmLongHead longheadToPosition)
        {
            if (longheadToPosition.Number == 1)
                return longheadToPosition.MinimumPosition;

            var previousLongHeadPosition = node.NewLongHeadPositions[longheadToPosition.Number - 2];

            return machineSettings.LongHeadParameters.GetLongHeadByNumber(longheadToPosition.Number - 1).GetRightEdgeForPosition(previousLongHeadPosition) + longheadToPosition.LeftSideToTool;
        }

        private static bool CanSeparateWasteOnCoordinate(EmLongHead longheadToPosition, WasteArea wasteArea, MicroMeter coordinateForLine, RuleAppliedPhysicalDesign design)
        {
            var  separatorOnLeftSideOfTool = longheadToPosition.LeftSideToWasteSeparator - longheadToPosition.LeftSideToTool < 0;
            var separatorOnRightSideOfTool = longheadToPosition.LeftSideToWasteSeparator - longheadToPosition.LeftSideToTool > 0;

            if ((separatorOnLeftSideOfTool && coordinateForLine == wasteArea.BottomRight.X)
            || (separatorOnRightSideOfTool && coordinateForLine == wasteArea.TopLeft.X))
                return true;

            var cutOffLine = GetCutOffLineFromDesign(design);
            var toolToEndOfSeparator = longheadToPosition.GetDistanceFromKnifeToEndOfWasteSeparator();

            // If line is between waste min/max X
            if ((separatorOnRightSideOfTool && (coordinateForLine <= wasteArea.BottomRight.X - toolToEndOfSeparator || wasteArea.BottomRight.X == cutOffLine.EndCoordinate.X))
              || (separatorOnLeftSideOfTool && (coordinateForLine >= wasteArea.TopLeft.X + toolToEndOfSeparator || wasteArea.TopLeft.X == cutOffLine.StartCoordinate.X)))
                return true;

            return false;
        }

        private static PhysicalLine GetCutOffLineFromDesign(RuleAppliedPhysicalDesign design)
        {
            var line =
                design.GetHorizontalPhysicalLines()
                    .OrderByDescending(i => i.EndCoordinate.X)
                    .ThenBy(i => i.StartCoordinate.X)
                    .ToList();
            return line.FirstOrDefault();
        }

        private static bool CanUseLongheadOnWasteArea(EmLongHead longheadToPosition, WasteArea wasteArea)
        {
            return (longheadToPosition.GetLeftEdgeForPosition(longheadToPosition.MinimumPosition) <= wasteArea.TopLeft.X
                   && (longheadToPosition.GetRightEdgeForPosition(longheadToPosition.MaximumPosition) >= wasteArea.BottomRight.X));
        }

        private static bool CanUseLongHeadOnLine(EmLongHead currentLongHead, MicroMeter coordinate)
        {
            return currentLongHead.MinimumPosition <= coordinate
                   && currentLongHead.MaximumPosition >= coordinate;
        }
    }
}
