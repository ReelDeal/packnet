namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using LongHeadPositioner;
    using Common.Interfaces.DTO;

    public class ToolToSensorPositionConverter : IToolToSensorPositionConverter
    {
        private readonly IDictionary<int, LongHeadInformation> longHeadInformationDictionary;

        public ToolToSensorPositionConverter(IEnumerable<LongHeadInformation> longHeadInformationEnumerable)
        {
            longHeadInformationDictionary = longHeadInformationEnumerable.ToDictionary(x => x.LongHeadNumber);
        }

        public MicroMeter ConvertPosition(LongHeadPositioningInformation positioning)
        {
            var longHeadInformation = longHeadInformationDictionary[positioning.LongHeadNumber];
            var convertedPosition = positioning.Position - longHeadInformation.LeftSideToTool + longHeadInformation.LeftSideToSensorPin;
            return convertedPosition;
        }

        public IEnumerable<LongHeadPositioningInformation> ConvertPosition(IEnumerable<LongHeadPositioningInformation> positions)
        {
            return positions.Select(position => new LongHeadPositioningInformation(position.LongHeadNumber, ConvertPosition(position), position.ToolWidth));
        }
    }
}