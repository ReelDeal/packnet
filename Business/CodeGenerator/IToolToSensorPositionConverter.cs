namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using LongHeadPositioner;
    using Common.Interfaces.DTO;

    public interface IToolToSensorPositionConverter
    {
        MicroMeter ConvertPosition(LongHeadPositioningInformation positioning);

        IEnumerable<LongHeadPositioningInformation> ConvertPosition(IEnumerable<LongHeadPositioningInformation> positions);
    }
}