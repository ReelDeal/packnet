﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using InstructionList;
    using Common.Interfaces.DTO;

    public interface IPackagingPanelCreator
    {
        IEnumerable<InstructionItem> Create(PhysicalDesign compensatedDesign, FusionPhysicalMachineSettings machine, MicroMeter corrugateWidth, Track trackNumber);
    }
}
