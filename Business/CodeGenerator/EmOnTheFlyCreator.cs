﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator
{
    public class EmOnTheFlyCreator
    {
        private readonly IOnTheFlyCreator onTheFlyLongHeadCreator = new OnTheFlyLongHeadCreator();
        private readonly IOnTheFlyCreator onTheFlyCrossHeadCreator = new OnTheFlyCrossHeadCreator();
        
        private readonly IOnTheFlyCompensator crossheadOtfCompensator;
        private readonly IOnTheFlyCompensator longheadOtfCompensator;

        private readonly OtfActuationDistanceCalculation crossheadOtfCalculation;
        private readonly OtfActuationDistanceCalculation longHeadOtfCalculation;

        private readonly OtfDelayHandler crossHeadDelayHandler;
        private readonly OtfDelayHandler longheadDelayHandler;

        private readonly OtfKinematicCalculation crossheadkinematicData;
        private readonly OtfKinematicCalculation longheadKinematicData;

        public EmOnTheFlyCreator(EmPhysicalMachineSettings machineSettings)
        {
            crossheadOtfCompensator = new CrossHeadOnTheFlyCompensator();
            longheadOtfCompensator = new LongheadOnTheFlyCompensator();

            crossHeadDelayHandler = new OtfDelayHandler(machineSettings.CrossHeadParameters);
            longheadDelayHandler = new OtfDelayHandler(machineSettings.LongHeadParameters);

            crossheadkinematicData = new OtfKinematicCalculation(machineSettings.CrossHeadParameters.MovementData);
            longheadKinematicData = new OtfKinematicCalculation(machineSettings.FeedRollerParameters.MovementData);

            crossheadOtfCalculation = new OtfActuationDistanceCalculation(crossheadkinematicData, crossHeadDelayHandler);
            longHeadOtfCalculation = new OtfActuationDistanceCalculation(longheadKinematicData, longheadDelayHandler);

        }

        public IEnumerable<InstructionItem> Create(IEnumerable<InstructionItem> instructionList)
        {
            var instructionlist = instructionList.ToList();

            instructionlist = onTheFlyLongHeadCreator.Create(instructionlist, longHeadOtfCalculation).ToList();

            instructionlist = onTheFlyCrossHeadCreator.Create(instructionlist, crossheadOtfCalculation).ToList();

            return instructionlist;
        }

        public IEnumerable<InstructionItem> Compensate(IEnumerable<InstructionItem> optimizedInstructions,
            MicroMeter originalCrossHeadPosition)
        {
            optimizedInstructions = crossheadOtfCompensator.CompensateForVelocity(optimizedInstructions, originalCrossHeadPosition,
                crossheadkinematicData.MaximumAcceleration,
                crossheadkinematicData.MaximumDeceleration,
                crossheadkinematicData.MaximumSpeed, crossHeadDelayHandler);

            return longheadOtfCompensator.CompensateForVelocity(optimizedInstructions, 0,
                longheadKinematicData.MaximumAcceleration,
                longheadKinematicData.MaximumDeceleration,
                longheadKinematicData.MaximumSpeed, longheadDelayHandler);
        }
    }
}
