﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using InstructionList;
    using OnTheFlyCompensator;
    using Common.Interfaces.DTO;

    public class FusionCodeGenerator : ICodeGenerator<FusionPhysicalMachineSettings>
    {
        private readonly MicroMeter crossHeadPosition;
        private readonly IOnTheFlyCompensator otfCompensator;
        private readonly ILongHeadPositioner longHeadPositioner;
        private readonly IPackagingPanelCreator panelCreator;
        private readonly ICodeOptimizer optimizer;
        private readonly IOnTheFlyCreator otfCreator;
        private readonly ILogger logger;
        private IEnumerable<MicroMeter> updatedLongheadPositions;
        
        public FusionCodeGenerator(MicroMeter crossHeadPosition, MicroMeter[] longHeadPositions, ILogger log)
        {
            longHeadPositioner = new LongHeadPositioner.LongHeadPositioner(longHeadPositions);
            panelCreator = new PackagingPanelCreator();
            optimizer = new CodeOptimizer();

            otfCreator = new OnTheFlyCrossHeadCreator();

            this.crossHeadPosition = crossHeadPosition;
            otfCompensator = new OnTheFlyCompensator.CrossHeadOnTheFlyCompensator();

            logger = log;
        }

        internal FusionCodeGenerator(ILongHeadPositioner longHeadPositioner, IPackagingPanelCreator panelCreator, ICodeOptimizer optimizer, IOnTheFlyCreator otfCreator, MicroMeter crossHeadPosition, IOnTheFlyCompensator otfCompensator, ILogger log)
        {
            this.longHeadPositioner = longHeadPositioner;
            this.panelCreator = panelCreator;
            this.optimizer = optimizer;
            this.otfCreator = otfCreator;
            this.crossHeadPosition = crossHeadPosition;
            this.otfCompensator = otfCompensator;
            logger = log;
        }

        public MicroMeter[] GetUpdatedLongHeadPositions()
        {
            return updatedLongheadPositions.ToArray();
        }

        public IEnumerable<InstructionItem> Generate(RuleAppliedPhysicalDesign design, FusionPhysicalMachineSettings machineSettings, MicroMeter corrugateWidth, Track track, bool onTheFly)
        {
            var instructionList = Generate(design, machineSettings, corrugateWidth, track);

            if (onTheFly)
            {
                var delayHandler = new OtfDelayHandler(machineSettings);
                var kinematicCalculation = new OtfKinematicCalculation(machineSettings);
                var otfActuationDistanceCalculation = new OtfActuationDistanceCalculation(kinematicCalculation, delayHandler);
                MicroMeter entireMovementStartPosition = 0; // TODO: Hard coded to zero until we set it appropriately

                instructionList = otfCreator.Create(instructionList, otfActuationDistanceCalculation);

                instructionList = otfCompensator.CompensateForVelocity(instructionList, entireMovementStartPosition,
                    kinematicCalculation.MaximumAcceleration, 
                    kinematicCalculation.MaximumDeceleration,
                    kinematicCalculation.MaximumSpeed, delayHandler);
            }

            return instructionList;
        }

        public bool ValidateLongHeadPositions(PhysicalDesign design, FusionPhysicalMachineSettings settings, MicroMeter corrugateWidth, bool isTrackRightSideFixed, MicroMeter trackOffset)
        {
            return longHeadPositioner.CanDistributeLongHeadsFor(design.GetVerticalPhysicalLines(), settings.LongHeadParameters, isTrackRightSideFixed, trackOffset, corrugateWidth, settings.TrackParameters.CorrugateWidthTolerance);
        }

        private IEnumerable<InstructionItem> Generate(PhysicalDesign design, FusionPhysicalMachineSettings machineSettings, MicroMeter corrugateWidth, Track track)
        {
            var instructionList = new List<InstructionItem>();

            instructionList.Add(CreateMetaData(design.Length, track.TrackNumber));

            var positions = longHeadPositioner.DistributeLongHeads(design.GetVerticalPhysicalLines(), machineSettings.LongHeadParameters, machineSettings.TrackParameters.Tracks.Single(t => t.Number == track.TrackNumber).RightSideFixed, track.TrackOffset, corrugateWidth, crossHeadPosition, machineSettings.TrackParameters.CorrugateWidthTolerance);
            instructionList.Add(positions);
            
            LogPositions("Generate - PositioningItem - ", positions);

            updatedLongheadPositions = longHeadPositioner.GetLongheadPositions();
            LogLHs("Generate - updatedLongheadPositions - ", updatedLongheadPositions.ToArray());

            instructionList.AddRange(panelCreator.Create(design, machineSettings, corrugateWidth, track));

            instructionList.Add(new InstructionEndMarkerItem());

            return optimizer.Optimize(instructionList);
        }

        private void LogPositions(string message, InstructionLongHeadPositionItem positions)
        {
            var lhstring = positions.LongHeadsToPosition.Aggregate("LHs : [ ", (current, lh) => current + (lh + " "));
            lhstring += " ]";

            logger.Log(LogLevel.Debug, message + lhstring);
        }

        private InstructionItem CreateMetaData(MicroMeter designLength, int trackNumber)
        {
            return new InstructionMetaDataItem(designLength,
                trackNumber);
        }

        private void LogLHs(string message, MicroMeter[] positions)
        {
            var lhstring = positions.Aggregate("LHs : [ ", (current, lh) => current + (lh + " "));
            lhstring += " ]";

            logger.Log(LogLevel.Debug, message + lhstring);
        }
    }
}
