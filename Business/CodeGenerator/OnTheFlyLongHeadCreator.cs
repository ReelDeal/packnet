﻿using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using InstructionList;
    using Common.Interfaces.DTO;
    using Common.Interfaces.Machines;

    public class OnTheFlyLongHeadCreator : OnTheFlyCreator
    {
        protected override bool IsStartOfPossibleOtfSequence(List<InstructionItem> instructionItems,
            int currentIndex,
            IMovementInstructionItem prevMovement, OtfActuationDistanceCalculation actuationDistances)
        {
            var movement = instructionItems[currentIndex] as InstructionFeedRollerItem;

            if (movement == null)
                return false;

            if (movement.Position < 0)
                return false;

            if (prevMovement == null)
                return true;

            return movement.Position >= GetMimimDistanceToUse(instructionItems, currentIndex, actuationDistances);
        }

        protected override List<InstructionItem> GetPossibleOtfItems(List<InstructionItem> instructionItems,
            int currentIndex, OtfActuationDistanceCalculation distanceCalculation)
        {
            var prevMovement = instructionItems[currentIndex] as InstructionFeedRollerItem;
            var possibleSequence = new List<InstructionItem> { prevMovement };
            var previousActivations = GetPreviousActivations(instructionItems);
            var distanceFromStartOfOtf = prevMovement != null ? prevMovement.Position : new MicroMeter(0);

            var isAccelerationPhase = true;
            for (var indexOfNextItem = currentIndex + 1; indexOfNextItem < instructionItems.Count; indexOfNextItem++)
            {
                if (IsUsableInOtf(instructionItems, indexOfNextItem, isAccelerationPhase, prevMovement, previousActivations, distanceCalculation, distanceFromStartOfOtf))
                {
                    if (instructionItems[indexOfNextItem] is InstructionFeedRollerItem)
                    {
                        prevMovement = instructionItems[indexOfNextItem] as InstructionFeedRollerItem;
                    }
                    
                    possibleSequence.Add(instructionItems[indexOfNextItem]);
                }
                else
                {
                    //Is this item usable during deceleration?
                    if (IsUsableInOtf(instructionItems, indexOfNextItem, true, prevMovement, previousActivations, distanceCalculation, distanceFromStartOfOtf))
                    {
                        possibleSequence.Add(instructionItems[indexOfNextItem]);

                        if (instructionItems[indexOfNextItem] is InstructionLongHeadToolActivations)
                            possibleSequence.Add(instructionItems[indexOfNextItem + 1]);
                    }

                    break;
                }

                UpdateActivationPositions(ref distanceFromStartOfOtf, possibleSequence, previousActivations);
                isAccelerationPhase = false;
            }

            return possibleSequence;
        }



        private static Dictionary<int, MicroMeter> GetPreviousActivations(IEnumerable<InstructionItem> instructionItems)
        {
            var previousActivations = new Dictionary<int, MicroMeter>();
            var longheads =
                instructionItems.OfType<InstructionLongHeadToolActivations>()
                    .SelectMany(ta => ta.ToolActivations.Select(t => t.LongHeadNumber))
                    .Distinct();
            foreach (var longhead in longheads)
            {
                previousActivations[longhead] = 0;
            }
            return previousActivations;
        }

        private static bool IsUsableInOtf(List<InstructionItem> items, int index, bool isAccelerationOrDecelerationPhase, InstructionFeedRollerItem prevMovement, Dictionary<int, MicroMeter> previousActivations, OtfActuationDistanceCalculation distanceCalculation, MicroMeter distanceFromStartOfOtf)
        {
            if (items[index] is IMovementInstructionItem)
                return IsFeedMovementsMergable(items, index, prevMovement, previousActivations, isAccelerationOrDecelerationPhase, distanceCalculation, distanceFromStartOfOtf + (items[index] as IMovementInstructionItem).Position);
            return items[index] is InstructionLongHeadToolActivations
                   && items.ElementAtOrDefault(index + 1) is IMovementInstructionItem
                   && IsFeedMovementsMergable(
                       items,
                       index + 1,
                       prevMovement,
                       previousActivations,
                       isAccelerationOrDecelerationPhase,
                       distanceCalculation,
                       distanceFromStartOfOtf + (items[index + 1] as IMovementInstructionItem).Position);
        }

        private static void UpdateActivationPositions(
            ref MicroMeter distanceFromStartOfOtf,
            IEnumerable<InstructionItem> possibleSequence,
            IDictionary<int, MicroMeter> previousActivations)
        {
            distanceFromStartOfOtf = 0;
            foreach (var instruction in possibleSequence)
            {
                if (instruction is InstructionFeedRollerItem)
                {
                    distanceFromStartOfOtf += (instruction as InstructionFeedRollerItem).Position;
                    continue;
                }

                if (instruction is InstructionLongHeadToolActivations)
                {
                    foreach (var actication in (instruction as InstructionLongHeadToolActivations).ToolActivations)
                    {
                        previousActivations[actication.LongHeadNumber] = distanceFromStartOfOtf;
                    }
                    continue;
                }
            }
        }

        private static bool IsFeedMovementsMergable(List<InstructionItem> items, int index, InstructionFeedRollerItem prevMovement, Dictionary<int, MicroMeter> previousActivations, bool isAccelerationOrDecelerationPhase, OtfActuationDistanceCalculation distanceCalculation, MicroMeter distanceFromStartOfOtf)
        {
            var movement = items.ElementAtOrDefault(index) as InstructionFeedRollerItem;
            if (movement == null || prevMovement == null || movement.Position < 0)
            {
                return false;
            }
            
            var nextActivation = items.ElementAtOrDefault(index + 1) as InstructionLongHeadToolActivations;

            if (nextActivation == null)
                return true;

            var activationDistances = GetActivationDistancesForLongheads(items, index, isAccelerationOrDecelerationPhase, distanceCalculation, nextActivation);

            return CanLongheadsForNextInstructionBeActivated(previousActivations, distanceFromStartOfOtf, activationDistances);
        }

        private static Dictionary<int, MicroMeter> GetActivationDistancesForLongheads(List<InstructionItem> items, int index, bool isAccelerationOrDecelerationPhase, OtfActuationDistanceCalculation distanceCalculation, InstructionLongHeadToolActivations nextActivation)
        {
            Dictionary<int, MicroMeter> activationDistances;

            if (isAccelerationOrDecelerationPhase == false && IsLastOnTheFlyMovement(items, index) == false)
            {
                activationDistances =  GetMaxSpeedActuationDistanceForToolActivation(distanceCalculation, nextActivation);
            }
            else
            {
                activationDistances = GetAccelerationActuationDistanceForToolActivation(distanceCalculation, nextActivation);
            }
            return activationDistances;
        }

        private static Dictionary<int, MicroMeter> GetMaxSpeedActuationDistanceForToolActivation(OtfActuationDistanceCalculation distanceCalculation, InstructionLongHeadToolActivations nextActivation)
        {
            var activationDistances = new Dictionary<int, MicroMeter>();
            foreach (var activation in nextActivation.ToolActivations)
            {
                activationDistances[activation.LongHeadNumber] = distanceCalculation.GetMaxSpeedActuationDistance(
                    activation.PreviousState,
                    activation.ToolState);
            }
            return activationDistances;
        }

        private static Dictionary<int, MicroMeter> GetAccelerationActuationDistanceForToolActivation(OtfActuationDistanceCalculation distanceCalculation, InstructionLongHeadToolActivations nextActivation)
        {
            var activationDistances = new Dictionary<int, MicroMeter>();
            foreach (var activation in nextActivation.ToolActivations)
            {
                activationDistances[activation.LongHeadNumber] =
                    distanceCalculation.GetAccelerationActuationDistance(activation.PreviousState, activation.ToolState);
            }
            return activationDistances;
        }

        private static bool CanLongheadsForNextInstructionBeActivated(IReadOnlyDictionary<int, MicroMeter> previousActivations, MicroMeter distanceFromStartOfOtf, Dictionary<int, MicroMeter> activationDistances)
        {
            return
                activationDistances.All(
                    activationDistance => distanceFromStartOfOtf - previousActivations[activationDistance.Key] >= activationDistance.Value);
        }

        private static bool IsLastOnTheFlyMovement(List<InstructionItem> items, int index)
        {
            var chInstructionsLeft = items.GetRange(index, items.Count - index).TakeWhile(i => i is InstructionFeedRollerItem || i is InstructionLongHeadToolActivations);
            return chInstructionsLeft.Count(i => i is InstructionFeedRollerItem) <= 1;
        }

        protected override List<InstructionItem> MergeMovements(List<InstructionItem> possibleSequence)
        {
            return MergeMovements<InstructionFeedRollerItem>(possibleSequence);
        }

        protected override bool CanSequenceBeReplacedByOtf(List<InstructionItem> sequence)
        {
            return CanSequenceBeReplacedByOtf<InstructionLongHeadToolActivations>(sequence);
        }

        protected override InstructionItem CreateOtfInstruction(List<InstructionItem> mergeable)
        {
            var otfItem = new InstructionLongHeadOnTheFlyItem();

            var mergeableIterator = mergeable.GetEnumerator();
            MicroMeter currentPosition = 0;
            while (mergeableIterator.MoveNext())
            {
                currentPosition += (mergeableIterator.Current as InstructionFeedRollerItem).Position;

                if (mergeableIterator.MoveNext())
                {
                    var toolState = (mergeableIterator.Current as InstructionLongHeadToolActivations).ToolActivations;
                    otfItem.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(currentPosition, 66 ,toolState)); //TODO 66 should be exchanged for a calculated dynamic speed value
                }
            }

            otfItem.FinalPosition = currentPosition;

            return otfItem;
        }

        private static double GetMimimDistanceToUse(List<InstructionItem> instructionItems, int index, OtfActuationDistanceCalculation distanceCalculation)
        {
            var nextActivation =
                instructionItems.GetRange(index, instructionItems.Count - index)
                    .FirstOrDefault(i => i is InstructionLongHeadToolActivations) as InstructionLongHeadToolActivations;

            if (nextActivation == null)
                return 0;

            return nextActivation.ToolActivations.Select(t => distanceCalculation.GetAccelerationActuationDistance(t.PreviousState, t.ToolState)).Concat(new[] { 0.0 }).Max();
        }
    }
}
