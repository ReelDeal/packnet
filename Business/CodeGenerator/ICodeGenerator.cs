﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using InstructionList;
    using Common.Interfaces.DTO;

    public interface ICodeGenerator<in T> where T : PhysicalMachineSettings
    {
        IEnumerable<InstructionItem> Generate(RuleAppliedPhysicalDesign design, T machineSettings, MicroMeter corrugateWidth, Common.Interfaces.DTO.Machines.Track track, bool onTheFly);

        MicroMeter[] GetUpdatedLongHeadPositions();

        bool ValidateLongHeadPositions(PhysicalDesign design, T machine, MicroMeter corrugateWidth, bool isTrackRightSideFixed, MicroMeter trackOffset);
    }
}
