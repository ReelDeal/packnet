﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using InstructionList;
    using Common.Interfaces.DTO;
    using Common.Interfaces.DTO.PhysicalMachine;

    public interface ILongHeadPositioner
    {
        bool CanDistributeLongHeadsFor(
            IEnumerable<PhysicalLine> verticalLines,
            FusionLongHeadParameters longHeadParameters,
            bool trackIsRightSideFixed,
            MicroMeter trackOffset,
            MicroMeter corrugateWidth,
            MicroMeter corrugateWidthTolerance);

        InstructionLongHeadPositionItem DistributeLongHeads(
            IEnumerable<PhysicalLine> verticalLines,
            FusionLongHeadParameters longHeadParameters,
            bool trackIsRightSideFixed,
            MicroMeter trackOffset,
            MicroMeter corrugateWidth,
            MicroMeter crossHeadPosition,
            MicroMeter corrugateWidthTolerance);

        IEnumerable<MicroMeter> GetLongheadPositions();
    }
}
