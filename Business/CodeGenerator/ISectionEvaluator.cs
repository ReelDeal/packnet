﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Business.CodeGenerator
{
    public interface ISectionEvaluator
    {
        LongHeadDistributionSolution GetLongHeadDistribution(EmPhysicalMachineSettings machineParameters,
            MicroMeter[] longHeadPositions, SectionPermutation sections, int originalSectionCount, RuleAppliedPhysicalDesign design, double scoreBestExpandedSolution);

        LongHeadDistributionSolution GetLongHeadDistribution(EmPhysicalMachineSettings machineParameters,
            MicroMeter[] longHeadPositions, IEnumerable<Section> sections, int originalSectionCount, RuleAppliedPhysicalDesign design, double scoreBestExpandedSolution);
    }
}
