using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using AStarStuff;
    using Common.Interfaces.DTO;
    using Common.Interfaces.DTO.PhysicalMachine;

    internal interface IAStarAlgorithm
    {
        AStarNode FindSolution(FusionLongHeadParameters longHeadParameters, MicroMeter[] longHeadPositions, bool trackIsRightSideFixed, MicroMeter trackOffset, MicroMeter corrugateWidth, IEnumerable<PhysicalLine> verticalLines, MicroMeter corrugateWidthTolerance);
    }
}