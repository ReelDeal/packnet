﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Business.CodeGenerator.AStarStuff;

namespace PackNet.Business.CodeGenerator
{
    public class LongHeadDistributionSolution
    {
        private readonly List<IAStarNodeEM> nodes;

        public double Score { get { return nodes.Sum(n => n.Score); } }

        public int NumberOfReverses
        {
            get { return nodes.Sum(n => n.NumberOfReverses); }
        }

        public IEnumerable<Section> Sections { get; set; }

        public LongHeadDistributionSolution()
        {
            nodes = new List<IAStarNodeEM>();
        }

        public void AddNode(IAStarNodeEM node)
        {
            nodes.Add(node);
        }

        public IEnumerable<IAStarNodeEM> GetSolutionNodes()
        {
            return nodes.ToArray();
        }
    }
}
