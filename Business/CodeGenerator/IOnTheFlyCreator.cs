﻿using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using InstructionList;

    public interface IOnTheFlyCreator
    {
        IEnumerable<InstructionItem> Create(IEnumerable<InstructionItem> instructionList, OtfActuationDistanceCalculation actuationDistances);
    }
}
