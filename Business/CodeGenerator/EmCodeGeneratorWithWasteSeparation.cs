﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using PackNet.Business.CodeGenerator.AStarStuff;
using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

using Track = PackNet.Common.Interfaces.DTO.PhysicalMachine.Track;

namespace PackNet.Business.CodeGenerator
{
    public class EmCodeGeneratorWithWasteSeparation : ICodeGenerator<EmPhysicalMachineSettings>
    {
        private MicroMeter crossHeadPosition;
        private readonly MicroMeter longHeadOffset;
        private readonly MicroMeter wasteSeparatorActivationOffset;
        private readonly MicroMeter wasteSeparationDeactivationOffset;


        private MicroMeter[] updatedLongheadPositions;
        private readonly ToolStates[] longheadToolStates;

        private List<InstructionItem> instructions;
        private List<PhysicalLine> processedHorizontalLines;
        private readonly EmLongHeadDistributor longHeadDistributor;

        private readonly ICodeOptimizer optimizer;
        
        public EmCodeGeneratorWithWasteSeparation(MicroMeter crossHeadPosition, EmLongHeadParameters longHeadParameters)
            : this(crossHeadPosition, longHeadParameters, new CodeOptimizer(), new EmLongHeadDistributor())
        {

        }

        protected EmCodeGeneratorWithWasteSeparation(MicroMeter crossHeadPosition, EmLongHeadParameters longHeadParameters, ICodeOptimizer optimizer, EmLongHeadDistributor longHeadDistributor)
        {
            this.longHeadDistributor = longHeadDistributor;
            this.crossHeadPosition = crossHeadPosition;
            longHeadOffset = longHeadParameters.LongHeadYOffset;
            wasteSeparatorActivationOffset = longHeadParameters.WasteSeparatorActivationOffset;
            wasteSeparationDeactivationOffset = longHeadParameters.WasteSeparatorDeactivationOffset;
            this.optimizer = optimizer;
            updatedLongheadPositions = longHeadParameters.LongHeads.Select(l => l.Position).ToArray();
            longheadToolStates = longHeadParameters.LongHeads.Select((t) => ToolStates.None).ToArray();
            
        }

        public IEnumerable<InstructionItem> Generate(RuleAppliedPhysicalDesign design, EmPhysicalMachineSettings machineSettings,
            MicroMeter corrugateWidth, Common.Interfaces.DTO.Machines.Track track, bool onTheFly)
        {
            var originalLHPositions = updatedLongheadPositions.ToArray();
            try
            {
                var longHeadDistribution = longHeadDistributor.DistributeLongHeads(
                machineSettings,
                machineSettings.LongHeadParameters.LongHeads.Select(l => l.Position).ToArray(),
                new SectionCreator(machineSettings.LongHeadParameters.LongHeadYOffset),
                new SectionEvaluator(new EmWasteSeparationSuccessorCreator()),
                design);

                var originalCrossHeadPosition = crossHeadPosition;

                instructions = new List<InstructionItem>();

                AddTrackActivationInstruction(track.TrackNumber);

                AddInstructionsForEachSection(design, track.TrackNumber, longHeadDistribution, machineSettings);

                AddOutFeedInstruction(machineSettings);

                AddLongHeadDeactivationInstruction(machineSettings);

                AddEndMarker();

                for (var i = 0; i < updatedLongheadPositions.Length; i++)
                {
                    machineSettings.LongHeadParameters.LongHeads.ElementAt(i).Position = updatedLongheadPositions[i];
                }

                machineSettings.CrossHeadParameters.Position = crossHeadPosition;

                var optimizedInstructions = optimizer.Optimize(instructions);

                if (onTheFly == false)
                    return optimizedInstructions;
                var otfCreator = new EmOnTheFlyCreator(machineSettings);
                return otfCreator.Compensate(otfCreator.Create(optimizedInstructions), originalCrossHeadPosition);
            }
            catch (Exception)
            {
                updatedLongheadPositions = originalLHPositions;
                throw;
            }
            
        }

        public MicroMeter[] GetUpdatedLongHeadPositions()
        {
            return updatedLongheadPositions;
        }

        public bool ValidateLongHeadPositions(
            PhysicalDesign design,
            EmPhysicalMachineSettings machine,
            MicroMeter corrugateWidth,
            bool isTrackRightSideFixed,
            MicroMeter trackOffset)
        {
            //Em machine can produce anything
            return true;
        }


        //TODO, Techincal debt only used in test should be moved to test class
        [Obsolete("Should be moved to testclass, only used in test")]
        public IEnumerable<InstructionItem> Generate(RuleAppliedPhysicalDesign design, EmMachine machine,
            LongHeadDistributionSolution longHeadDistribution, MicroMeter corrugateWidth, Track track, bool onTheFly)
        {
            var machineSettings = machine.PhysicalMachineSettings as EmPhysicalMachineSettings;
            instructions = new List<InstructionItem>();

            AddTrackActivationInstruction(track.Number);

            AddInstructionsForEachSection(design, track.Number, longHeadDistribution, machineSettings);

            AddOutFeedInstruction(machineSettings);

            AddLongHeadDeactivationInstruction(machineSettings);

            AddEndMarker();

            var optimizedInstructions = optimizer.Optimize(instructions);
            
            if (onTheFly == false)
                return optimizedInstructions;
            var otfCreator = new EmOnTheFlyCreator(machineSettings);
            return otfCreator.Create(optimizedInstructions);

        }

        private void AddTrackActivationInstruction(int trackNumber)
        {
            instructions.Add(new InstructionTrackActivationItem((short)trackNumber, true));
        }

        private void AddInstructionsForEachSection(RuleAppliedPhysicalDesign design, int trackNumber, LongHeadDistributionSolution longHeadDistribution, EmPhysicalMachineSettings machineSettings)
        {
            var solutionNodes = longHeadDistribution.GetSolutionNodes();
            processedHorizontalLines = new List<PhysicalLine>();
            var lastYCoordinateForDesign = design.Lines.GetHorizontalLineCoordinates().Max();

            for (var i = 0; i < solutionNodes.Count(); i++)
            {
                var section = longHeadDistribution.Sections.ElementAt(i);
                var startCoordinateY = i > 0 ? section.StartCoordinateY + longHeadOffset : section.StartCoordinateY;

                var horizontalLinesForSection =
                    design.GetHorizontalPhysicalLines()
                        .Where(l => l.StartCoordinate.Y >= section.StartCoordinateY && l.EndCoordinate.Y <= section.EndCoordinateY);

                var affectedWasteAreasInSection = design.WasteAreas.Where(a => a.TopLeft.Y >= section.StartCoordinateY || a.BottomRight.Y <= section.EndCoordinateY);

                AddInstructionsForEachIteration(horizontalLinesForSection, section, i == solutionNodes.Count() - 1, (short)trackNumber, solutionNodes.ElementAt(i), machineSettings, startCoordinateY, lastYCoordinateForDesign, affectedWasteAreasInSection);
            }
        }

        private void AddInstructionsForEachIteration(IEnumerable<PhysicalLine> horizontalLines, Section section, bool shouldDeactiveTrack, int trackNumber, 
                                                    IAStarNodeEM solutionNode, EmPhysicalMachineSettings machineSettings, MicroMeter startCoordinateY, 
                                                    MicroMeter lastYCoordinate, IEnumerable<WasteArea> affectedWasteAreasInSection)
        {
            var currentY = startCoordinateY;
            
            for (int i = 0; i < solutionNode.Iterations.Count(); i++)
            {
                var isFinalIteration = (i == solutionNode.Iterations.Count() - 1);
                var iterationNode = solutionNode.Iterations.ElementAt(i).First();
                var coordinates = GetSortedUpcomingYCoordinates(horizontalLines, section, iterationNode, startCoordinateY, affectedWasteAreasInSection);
                var remainingIterations = solutionNode.Iterations.Skip(i + 1).ToList();

                AddLongHeadPositioningInstructionForIteration(iterationNode, machineSettings.LongHeadParameters);

                foreach (var coordinate in coordinates)
                {
                    AddFeedInstructionToNextCoordinate(currentY, coordinate);

                    if (IsFinalPassForHorizontalLine(coordinate, remainingIterations, horizontalLines, section.Lines.GetVerticalLines()))
                    {
                        AddCrossHeadInstructions(coordinate, horizontalLines, machineSettings.CrossHeadParameters);

                        AddTrackDeactivationInstruction(trackNumber, coordinate, lastYCoordinate);
                    }

                    AddLongHeadActivations(coordinate, section, shouldDeactiveTrack, isFinalIteration, iterationNode.NewLongHeadPositions);

                    AddWasteSeparationInstruction(coordinate, affectedWasteAreasInSection, iterationNode);

                    currentY = coordinate;
                }
            }
        }

        private IEnumerable<MicroMeter> GetSortedUpcomingYCoordinates(IEnumerable<PhysicalLine> horizontalLines, Section section, IAStarNodeEM iteration, 
                                                                        MicroMeter startCoordinateY, IEnumerable<WasteArea> affectedWasteAreasInSection)
        {
            var coordinatesFromHorizontalLines = horizontalLines.Select(l => l.StartCoordinate.Y);

            var trimmedLines = section.GetTrimmedVerticalLines().ToList();

            var coordinatesFromVerticalLines =
                trimmedLines.Where(l => iteration.NewLongHeadPositions.Contains(l.StartCoordinate.X))
                    .Select(l => l.StartCoordinate.Y + longHeadOffset)
                    .Union(trimmedLines.Select(l => l.EndCoordinate.Y + longHeadOffset))
                    .Where(c => c >= startCoordinateY && c <= section.EndCoordinateY + longHeadOffset);

            var startWasteAreaCoordinates = affectedWasteAreasInSection.Where(a => a.TopLeft.Y >= section.StartCoordinateY && a.TopLeft.Y <= section.EndCoordinateY).Select(a => a.TopLeft.Y + wasteSeparatorActivationOffset);

            var endWasteAreaCoordinates = affectedWasteAreasInSection.Where(a => a.BottomRight.Y >= section.StartCoordinateY && a.BottomRight.Y <= section.EndCoordinateY).Select(a => a.BottomRight.Y + wasteSeparationDeactivationOffset);

            var yCoordinates = coordinatesFromHorizontalLines.Union(coordinatesFromVerticalLines).Union(startWasteAreaCoordinates).Union(endWasteAreaCoordinates).ToList();
            yCoordinates.Add(startCoordinateY);
            if(section.EndCoordinateY > startCoordinateY)
                yCoordinates.Add(section.EndCoordinateY);
            return yCoordinates.OrderBy(c => c).Distinct();
        }

        private void AddLongHeadPositioningInstructionForIteration(IAStarNodeEM node, EmLongHeadParameters longHeadSettings)
        {
            var newLongHeadPositions = node.NewLongHeadPositions.ToArray();
            if (!updatedLongheadPositions.SequenceEqual(newLongHeadPositions))
            { 
                var uniqueMoves =
                    newLongHeadPositions.Select(
                        (pos, index) => GetLongHeadPositioningInformation(longHeadSettings, index, pos))
                        .Where(i => i != null)
                        .ToArray();

                var sortedMoves = GetSortedLongheadMovements(uniqueMoves, newLongHeadPositions);

                var lhsThatNeedToBeDeactivated = sortedMoves.Where(m => longheadToolStates[m.LongHeadNumber - 1] != ToolStates.None).ToList();
                if (lhsThatNeedToBeDeactivated.Any())
                {
                    instructions.Add(
                        new InstructionLongHeadToolActivations(
                            lhsThatNeedToBeDeactivated.Select(
                                lh =>
                                    new LongHeadToolActivationInformation(lh.LongHeadNumber, ToolStates.None,
                                        longheadToolStates[lh.LongHeadNumber - 1])).ToList()));
                    lhsThatNeedToBeDeactivated.ForEach(lh => longheadToolStates[lh.LongHeadNumber - 1] = ToolStates.None);
                }

                updatedLongheadPositions = newLongHeadPositions;
                if (sortedMoves.Any())
                {
                    instructions.Add(new InstructionLongHeadPositionItem(sortedMoves));
                }
                    
            }
        }

        private LongHeadPositioningInformation GetLongHeadPositioningInformation(
            EmLongHeadParameters longHeadSettings,
            int index,
            MicroMeter pos)
        {
            if (updatedLongheadPositions[index] != pos)
            {
                return new LongHeadPositioningInformation(
                    index + 1,
                    GetSensorPositionFromToolPosition(pos, longHeadSettings.LongHeads.ElementAt(index).ToolToSensorOffset),
                    longHeadSettings.LongheadWidth);
            }
            return null;
        }

        private MicroMeter GetSensorPositionFromToolPosition(MicroMeter pos, double toolToSensorOffset)
        {
            return pos + toolToSensorOffset;
        }

        /// <summary>
        /// The PLC expects the longhead positioning instruction to be sorted in this order:
        ///  * Short movements should be done before longer movements
        ///  * Longheads moving left are to be moved before longheads moving right
        /// </summary>
        /// <param name="uniqueMoves">A set of longheads that should be moved</param>
        /// <param name="newLongHeadPositions">The position all longheads will get after the instruction</param>
        /// <returns>A sorted list of movement instruction</returns>
        private LongHeadPositioningInformation[] GetSortedLongheadMovements(LongHeadPositioningInformation[] uniqueMoves, MicroMeter[] newLongHeadPositions)
        {
            if (uniqueMoves.Any() == false)
                return uniqueMoves;

            var movingLeft =
                uniqueMoves.Where(
                    m => newLongHeadPositions[m.LongHeadNumber - 1] - updatedLongheadPositions[m.LongHeadNumber - 1] < 0.0)
                    .OrderByDescending(
                        m => newLongHeadPositions[m.LongHeadNumber - 1] - updatedLongheadPositions[m.LongHeadNumber - 1])
                    .ToArray();

            var movingRight =
                uniqueMoves.Where(
                    m => newLongHeadPositions[m.LongHeadNumber - 1] - updatedLongheadPositions[m.LongHeadNumber - 1] > 0.0)
                    .OrderBy(m => newLongHeadPositions[m.LongHeadNumber - 1] - updatedLongheadPositions[m.LongHeadNumber - 1])
                    .ToArray();

            return movingLeft.Concat(movingRight).ToArray();
        }

        private void AddFeedInstructionToNextCoordinate(MicroMeter currentY, MicroMeter coordinate)
        {
            if (currentY != coordinate && coordinate - currentY != 0)
                instructions.Add(new InstructionFeedRollerItem(coordinate - currentY));
        }

        private void AddCrossHeadInstructions(MicroMeter coordinate, IEnumerable<PhysicalLine> horizontalLines, EmCrossHeadParameters crossHeadSettings)
        {
            var horizontalLinesAtCoordinate = horizontalLines.GetHorizontalLinesAtCoordinate(coordinate);
            var linesToBeProcessedAtCoordinate = horizontalLinesAtCoordinate.Except(processedHorizontalLines);
            AddCrossHeadIntructionsForCoordinate(linesToBeProcessedAtCoordinate, crossHeadSettings);
            processedHorizontalLines.AddRange(linesToBeProcessedAtCoordinate);
        }

        private bool IsFinalPassForHorizontalLine(MicroMeter coordinate, IEnumerable<IEnumerable<IAStarNodeEM>> remainingIterations, IEnumerable<PhysicalLine> horizontalLines, IEnumerable<PhysicalLine> verticaLines)
        {
            if (horizontalLines.Any(l => l.StartCoordinate.Y == coordinate) == false)
                return false;

            var coordinatesInRemainingIterations = remainingIterations.SelectMany(nodes => nodes.First().NewLongHeadPositions);
            var upcomingLines = verticaLines.Where(l => coordinatesInRemainingIterations.Contains(l.StartCoordinate.X) && l.StartCoordinate.Y - longHeadOffset <= coordinate);
            return upcomingLines.Any() == false;
        }

        private void AddCrossHeadIntructionsForCoordinate(IEnumerable<PhysicalLine> linesAtCoordinate, EmCrossHeadParameters crossHeadSettings)
        {
            if (linesAtCoordinate.Any())
            {
                var reverse = IsReverse(linesAtCoordinate);

                AddInstructionToPlaceCrossHeadOnStartingPosition(linesAtCoordinate, reverse, crossHeadSettings.ToolPositionToSensor);

                foreach (var line in GetSortedHorizontalLines(linesAtCoordinate, reverse))
                {
                    AddCrossHeadStartPositioningInstruction(reverse, line, crossHeadSettings.ToolPositionToSensor);

                    AddCrossHeadActivationInstruction(line);

                    AddCrossHeadEndPositioningInstruction(reverse, line, crossHeadSettings.ToolPositionToSensor);

                    AddCrossHeadDeactivationInstruction();
                }

            }
        }

        private bool IsReverse(IEnumerable<PhysicalLine> linesAtCoordinate)
        {
            var start = linesAtCoordinate.Min(l => l.StartCoordinate.X);
            var end = linesAtCoordinate.Max(l => l.EndCoordinate.X);

            return Math.Abs(crossHeadPosition - start) < Math.Abs(crossHeadPosition - end) == false;
        }

        private IEnumerable<PhysicalLine> GetSortedHorizontalLines(IEnumerable<PhysicalLine> linesAtCoordinate, bool isReverse)
        {
            return isReverse ? linesAtCoordinate.OrderByDescending(l => l.StartCoordinate.X) : linesAtCoordinate.OrderBy(l => l.StartCoordinate.X);
        }

        private void AddInstructionToPlaceCrossHeadOnStartingPosition(IEnumerable<PhysicalLine> linesAtCoordinate, bool isReverse, double toolToSensorOffset)
        {
            var start = linesAtCoordinate.First(line => line.StartCoordinate.X == linesAtCoordinate.Min(l => l.StartCoordinate.X));
            var end = linesAtCoordinate.First(line => line.EndCoordinate.X == linesAtCoordinate.Max(l => l.EndCoordinate.X));

            AddCrossHeadStartPositioningInstruction(isReverse, isReverse ? end : start, toolToSensorOffset);
        }

        private void AddCrossHeadStartPositioningInstruction(bool reverse, PhysicalLine line, double toolToSensorOffset)
        {
            AddCrossHeadPositioningInstruction(reverse ? line.EndCoordinate.X : line.StartCoordinate.X, toolToSensorOffset);
        }

        private void AddCrossHeadEndPositioningInstruction(bool reverse, PhysicalLine line, double toolToSensorOffset)
        {
            AddCrossHeadPositioningInstruction(reverse ? line.StartCoordinate.X : line.EndCoordinate.X, toolToSensorOffset);
        }

        private void AddCrossHeadPositioningInstruction(MicroMeter pos, double toolToSensorOffset)
        {
            var compensatedPosition = pos - toolToSensorOffset;
            if (crossHeadPosition != compensatedPosition)
            {
                instructions.Add(new InstructionCrossHeadMovementItem(compensatedPosition));
                crossHeadPosition = compensatedPosition;
            }
        }

        private void AddCrossHeadActivationInstruction(PhysicalLine line)
        {
            var activation = new InstructionCrossHeadToolActivation
            {
                Toolstate = GetToolstateFromLineType(line)
            };

            instructions.Add(activation);
        }

        private void AddCrossHeadDeactivationInstruction()
        {
            var deactivate = new InstructionCrossHeadToolActivation { Toolstate = ToolStates.None };
            instructions.Add(deactivate);
        }

        private void AddTrackDeactivationInstruction(int trackNumber, MicroMeter coordinate, MicroMeter endCoordinateY)
        {
            if(coordinate == endCoordinateY)
            {
                instructions.Add(new InstructionTrackActivationItem((short)trackNumber, false));
            }
        }

        private void AddLongHeadActivations(MicroMeter coordinate, Section section, bool shouldDeactivateTrack, bool isFinalIteration, IEnumerable<MicroMeter> longHeadPositions)
        {
            var activations = GetLongHeadActivationInstructionsForCoordinate(coordinate, section.GetTrimmedVerticalLines().GetVerticalLines(), longHeadPositions);

            if (activations.ToolActivations.Any())
            {
                if (coordinate > section.EndCoordinateY && shouldDeactivateTrack && isFinalIteration && HasOtherLhActivatedOnFanfold(activations, longheadToolStates) == false)
                {
                    var allLessDominantThanCrease = activations.ToolActivations.Where(t => t.ToolState < ToolStates.Crease).Select(t => new LongHeadToolActivationInformation(t.LongHeadNumber, ToolStates.Crease, longheadToolStates[t.LongHeadNumber - 1])).ToList();
                    var allCutsAndCreases = activations.ToolActivations.Where(t => t.ToolState >= ToolStates.Crease).Select(t => new LongHeadToolActivationInformation(t.LongHeadNumber, t.ToolState, longheadToolStates[t.LongHeadNumber - 1])).ToList();

                    var newActivations = allLessDominantThanCrease.Union(allCutsAndCreases).ToList();
                    newActivations.ForEach(a => longheadToolStates[a.LongHeadNumber - 1] = a.ToolState);

                    instructions.Add(new InstructionLongHeadToolActivations(newActivations));
                }
                else
                {
                    instructions.Add(activations);
                    activations.ToolActivations.ForEach(a => longheadToolStates[a.LongHeadNumber - 1] = a.ToolState);
                }
            }
        }

        private void AddWasteSeparationInstruction(MicroMeter coordinate, IEnumerable<WasteArea> affectedWasteAreasInSection, IAStarNodeEM iterationNode)
        {
            var startingWasteAreas = affectedWasteAreasInSection.Where(a => a.TopLeft.Y == coordinate - wasteSeparatorActivationOffset).ToList();
            var endingWasteAreas = affectedWasteAreasInSection.Where(a => a.BottomRight.Y == coordinate - wasteSeparationDeactivationOffset).ToList();

            var activationAreas = GetWasteSeparatorActivationsForAreas(startingWasteAreas, iterationNode, GetWasteActivationToolStateForLongHead);
            var deactivations = GetWasteSeparatorActivationsForAreas(endingWasteAreas, iterationNode, GetWasteDeactivationToolStateForLongHead);

            instructions.Add(new InstructionLongHeadToolActivations(activationAreas));
            instructions.Add(new InstructionLongHeadToolActivations(deactivations));
        }

        private IEnumerable<LongHeadToolActivationInformation> GetWasteSeparatorActivationsForAreas(IEnumerable<WasteArea> areas, IAStarNodeEM iterationNode, Func<int, ToolStates> toolStatesHandler)
        {
            var activations = new List<LongHeadToolActivationInformation>();
            foreach (var area in areas)
            {
                for (int i = 1; i <= iterationNode.NewLongHeadPositions.Length; i++)
                {
                    activations.AddRange(GetWasteSeparatorActivationsForArea(i, iterationNode, area, toolStatesHandler));
                }
            }
            return activations;
        }

        private IEnumerable<LongHeadToolActivationInformation> GetWasteSeparatorActivationsForArea(int number, IAStarNodeEM iterationNode, WasteArea area, Func<int, ToolStates> toolStatesHandler)
        {
            var activations = new List<LongHeadToolActivationInformation>();
            var lh = iterationNode.MachineParameters.LongHeadParameters.GetLongHeadByNumber(number);
            var leftPos = iterationNode.NewLongHeadPositions.ElementAt(number -1) - lh.LeftSideToTool + lh.LeftSideToWasteSeparator;
            var rightPos = leftPos + lh.WasteSeparatorWidth;

            if (leftPos >= area.TopLeft.X && rightPos <= area.BottomRight.X)
            {
                var wasteActivationToolState = toolStatesHandler(number - 1);
                activations.Add(new LongHeadToolActivationInformation(number, wasteActivationToolState, longheadToolStates[number - 1]));
                longheadToolStates[number - 1] = wasteActivationToolState;
            }

            return activations;
        }

        private ToolStates GetWasteActivationToolStateForLongHead(int number)
        {
            var currentState = longheadToolStates[number];
            
            if (currentState == ToolStates.Cut || currentState == ToolStates.CutWithWasteSeparation)
                return ToolStates.CutWithWasteSeparation;
            if (currentState == ToolStates.Perforation || currentState == ToolStates.PerforationWithWasteSeparation)
                return ToolStates.PerforationWithWasteSeparation;
            
            return ToolStates.CreaseWithWasteSeparation;
        }

        private ToolStates GetWasteDeactivationToolStateForLongHead(int number)
        {
            var currentState = longheadToolStates[number];

            if (currentState == ToolStates.CutWithWasteSeparation || currentState == ToolStates.Cut)
                return ToolStates.Cut;
            if (currentState == ToolStates.PerforationWithWasteSeparation || currentState == ToolStates.Perforation)
                return ToolStates.Perforation;
            if (currentState == ToolStates.CreaseWithWasteSeparation || currentState == ToolStates.Crease)
                return ToolStates.Crease;

            return ToolStates.None;
        }

        private bool HasOtherLhActivatedOnFanfold(InstructionLongHeadToolActivations activations, IEnumerable<ToolStates> toolStateses)
        {
            var notChanged =
                toolStateses.ToList()
                    .Select((t, i) => new { Index = i + 1, State = t })
                    .Where(t => activations.ToolActivations.Any(ta => ta.LongHeadNumber == t.Index) == false);

            return notChanged.Any(t => t.State >= ToolStates.Crease);
        }

        private InstructionLongHeadToolActivations GetLongHeadActivationInstructionsForCoordinate(MicroMeter currentY, IEnumerable<PhysicalLine> verticalLines, IEnumerable<MicroMeter> newLongHeadPositions)
        {
            var lhCurrentY = currentY - longHeadOffset;
            var linesWithStartOnPosition = verticalLines.Where(l => l.StartCoordinate.Y == lhCurrentY).ToList();
            var linesWithEndOnPosition = verticalLines.Where(l => l.EndCoordinate.Y == lhCurrentY).ToList();

            var longHeadActivationInfos = linesWithEndOnPosition.Select(line => GetLongHeadToolActivationInformationForLine(line, ToolStates.None, newLongHeadPositions)).Where(l => l != null).ToList();

            var activations = linesWithStartOnPosition.Select(line => GetLongHeadToolActivationInformationForLine(line, GetToolstateFromLineType(line), newLongHeadPositions)).Where(l => l != null);
            foreach (var activation in activations)
            {
                if (longHeadActivationInfos.Any(a => a.LongHeadNumber == activation.LongHeadNumber))
                {
                    longHeadActivationInfos.Remove(
                        longHeadActivationInfos.First(a => a.LongHeadNumber == activation.LongHeadNumber));
                }
                longHeadActivationInfos.Add(activation);
            }
            
            return new InstructionLongHeadToolActivations(longHeadActivationInfos);
        }

        private static ToolStates GetToolstateFromLineType(PhysicalLine line)
        {
            switch (line.Type)
            {
                case LineType.cut:
                case LineType.separate:
                case LineType.nunatab:
                    return ToolStates.Cut;
                case LineType.crease:
                    return ToolStates.Crease;
                case LineType.perforation:
                    return ToolStates.Perforation;
                default:
                    return ToolStates.None;
            }
        }

        private LongHeadToolActivationInformation GetLongHeadToolActivationInformationForLine(PhysicalLine line, ToolStates toolState, IEnumerable<MicroMeter> longHeadPositions)
        {
            var lhAtX = longHeadPositions.ToList().IndexOf(line.StartCoordinate.X) + 1;
            //return lhAtX > 0 ? new LongHeadToolActivationInformation(lhAtX, toolState, longheadToolStates[lhAtX - 1]) : null;
            return lhAtX > 0 ? new LongHeadToolActivationInformation(lhAtX, GetToolStateForLongHead(lhAtX, toolState), longheadToolStates[lhAtX - 1]) : null;
        }

        private ToolStates GetToolStateForLongHead(int number, ToolStates state)
        {
            if (state == ToolStates.Cut)
                return SeparationIsActive(number) ? ToolStates.CutWithWasteSeparation : ToolStates.Cut;
            if (state == ToolStates.Crease)
                return SeparationIsActive(number) ? ToolStates.CreaseWithWasteSeparation : ToolStates.Crease;
            if (state == ToolStates.Perforation)
                return SeparationIsActive(number) ? ToolStates.PerforationWithWasteSeparation : ToolStates.Perforation;
            
            return SeparationIsActive(number) ? ToolStates.CreaseWithWasteSeparation : ToolStates.None;
        }

        private bool SeparationIsActive(int longHeadNumber)
        {
            return (longheadToolStates[longHeadNumber - 1] == ToolStates.CreaseWithWasteSeparation
                    || longheadToolStates[longHeadNumber - 1] == ToolStates.CutWithWasteSeparation
                    || longheadToolStates[longHeadNumber - 1] == ToolStates.PerforationWithWasteSeparation);

        }

        private void AddLongHeadDeactivationInstruction(EmPhysicalMachineSettings machineSettings)
        {
            instructions.Add(
                new InstructionLongHeadToolActivations(
                    machineSettings.LongHeadParameters.LongHeads.Select(lh => new LongHeadToolActivationInformation(lh.Number, ToolStates.None, longheadToolStates[lh.Number - 1]))));
            for (var i = 0; i < longheadToolStates.Length; i++)
                longheadToolStates[i] = ToolStates.None;
        }

        private void AddOutFeedInstruction(EmPhysicalMachineSettings machineSettings)
        {
            instructions.Add(new InstructionFeedRollerItem(machineSettings.FeedRollerParameters.OutFeedLength));
        }

        private void AddEndMarker()
        {
            instructions.Add(new InstructionEndMarkerItem());
        }
    }
}
