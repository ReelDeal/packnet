﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.CodeGenerator
{
    public interface ISectionCreator
    {
        IEnumerable<Section> GetSectionsFromDesign(RuleAppliedPhysicalDesign design);

        IEnumerable<SectionPermutation> MergeSections(IEnumerable<Section> sections);

        IEnumerable<Section> SplitSections(MicroMeter maxReverseDistance, KeyValuePair<IEnumerable<Section>, LongHeadDistributionSolution> bestSolution);
    }
}
