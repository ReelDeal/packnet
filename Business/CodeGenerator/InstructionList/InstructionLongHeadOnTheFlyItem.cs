﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.LongHeadPositioner;

namespace PackNet.Business.CodeGenerator.InstructionList
{
    public class InstructionLongHeadOnTheFlyItem : OnTheFlyInstructionItem
    {
        private readonly List<LongHeadOnTheFlyPosition> positions;
 
        public IEnumerable<LongHeadOnTheFlyPosition> Positions { get { return positions; }}
        
        public InstructionLongHeadOnTheFlyItem()
        {
            positions = new List<LongHeadOnTheFlyPosition>();
        }

        public void AddOnTheFlyPosition(LongHeadOnTheFlyPosition position)
        {
            positions.Insert(positions.TakeWhile(x => x.Position <= position.Position).Count(), position);
        }

        public override string ToString()
        {
            var message = string.Format("OnTheFly long head : ");

            positions.ForEach(p => message += p + ", ");

            message = string.Format("{0}FINAL POSITION: {1}", message, FinalPosition);

            return message;
        }

        public override short[] ToPlcArray()
        {
            var positionsList = new List<short>
                {
                    31000,
                    (short)InstructionTypes.OnTheFlyLongHeadInstruction,
                    0,
                    GetDecimeterPart(FinalPosition),
                    GetHundredsMilliMeterPart(FinalPosition)
                };
            positions.ForEach(pos =>
            {
                positionsList.Add((short)InstructionTypes.OnTheFlyNewPositionMarker);
                positionsList.Add(GetDecimeterPart(pos.Position));
                positionsList.Add(GetHundredsMilliMeterPart(pos.Position));
                positionsList.Add(pos.PercentageOfMaxSpeed);
                positionsList.AddRange(GetPositionAsPlcArrayRepresentation(pos.ToolStates));
            });

            return positionsList.ToArray();
        }

        private IEnumerable<short> GetPositionAsPlcArrayRepresentation(IEnumerable<LongHeadToolActivationInformation> toolStates)
        {
            var activations = new List<short>();
            
            foreach(var state in toolStates)
            {
                activations.Add((short)state.LongHeadNumber);
                activations.Add((short)state.ToolState);
            }

            return activations;
        }
    }
}
