﻿namespace PackNet.Business.CodeGenerator.InstructionList
{
    public class InstructionEndMarkerItem : InstructionItem
    {
        #region Fields
        #endregion

        #region Ctor

        public InstructionEndMarkerItem()
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods

        public override short[] ToPlcArray()
        {
            var plcArray = new short[1];

            plcArray[0] = 32000;

            return plcArray;
        }

        public override string ToString()
        {
            return "End of packaging";
        }
        #endregion
    }
}
