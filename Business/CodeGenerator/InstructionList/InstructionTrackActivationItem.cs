﻿namespace PackNet.Business.CodeGenerator.InstructionList
{
    using System;
    using System.Collections.Generic;
    using PackNet.Business.CodeGenerator.Enums;

    public class InstructionTrackActivationItem : InstructionItem
    {
        public short TrackNumber { get; private set; }
        public bool Activate { get; private set; }

        public InstructionTrackActivationItem(short trackNumber, bool activate)
        {
            TrackNumber = trackNumber;
            Activate = activate;
        }

        public override string ToString()
        {
            return String.Format("{0} track {1}", Activate ? "Activate" : "Deactivate", TrackNumber);
        }

        public override short[] ToPlcArray()
        {
            var plcArray = new List<short>()
            {
                (short)InstructionTypes.NewInstruction,
                (short)InstructionTypes.TrackActivation,
                TrackNumber,
                Activate ? (short)ActivationStates.Active : (short)ActivationStates.Inactive
            };

            return plcArray.ToArray();
        }
    }
}
