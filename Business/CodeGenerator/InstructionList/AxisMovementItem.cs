﻿using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.CodeGenerator.InstructionList
{
    public abstract class AxisMovementItem : InstructionItem
    {
        public MicroMeter Position { get; set; }
    }
}
