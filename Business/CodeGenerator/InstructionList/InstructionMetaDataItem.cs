﻿namespace PackNet.Business.CodeGenerator.InstructionList
{
    using Common.Interfaces.DTO;

    public class InstructionMetaDataItem : InstructionItem
    {
        public MicroMeter PackagingLength { get; private set; }

        public int TrackNumber { get; private set; }

        public InstructionMetaDataItem(MicroMeter packagingLength, int trackNumber)
        {
            PackagingLength = packagingLength;
            TrackNumber = trackNumber;
        }

        public override short[] ToPlcArray()
        {
            var plcArray = new short[6];

            plcArray[0] = 31000;
            plcArray[1] = 5;
            plcArray[2] = GetDecimeterPart(PackagingLength);
            plcArray[3] = GetHundredsMilliMeterPart(PackagingLength);
            plcArray[4] = (short)TrackNumber;
            plcArray[5] = 0; //SynchronizeWithLabel was removed

            return plcArray;
        }

        public override string ToString()
        {
            return "Active Track: " + TrackNumber + " | Packaging Length: " + PackagingLength;
        }
    }
}
