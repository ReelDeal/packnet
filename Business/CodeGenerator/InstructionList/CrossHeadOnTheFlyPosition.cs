﻿namespace PackNet.Business.CodeGenerator.InstructionList
{
    using Enums;
    using Common.Interfaces.DTO;

    public class CrossHeadOnTheFlyPosition
    {
        public CrossHeadOnTheFlyPosition(MicroMeter position, ToolStates toolState)
        {
            Position = position;
            ToolState = toolState;
        }

        public MicroMeter Position { get; private set; }

        public ToolStates ToolState { get; private set; }

        public CrossHeadOnTheFlyPosition Copy()
        {
            return new CrossHeadOnTheFlyPosition(Position, ToolState);
        }

        public override string ToString()
        {
            return Position + "," + ToolState;
        }
    }
}
