﻿using System.Linq;

using PackNet.Business.CodeGenerator.Enums;

namespace PackNet.Business.CodeGenerator.InstructionList
{
    using System;
    using System.Collections.Generic;
    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    public class InstructionLongHeadToolActivations : InstructionItem
    {
        public IEnumerable<LongHeadToolActivationInformation> ToolActivations { get; private set; }

        public InstructionLongHeadToolActivations(IEnumerable<LongHeadToolActivationInformation> toolActivations)
        {
            ToolActivations = toolActivations;
        }

        public override string ToString()
        {
            return ToolActivations.Aggregate("Set longhead tool states - ", (current, lh) => current + String.Format("{0}: {1}|", lh.LongHeadNumber, lh.ToolState));
        }

        public override short[] ToPlcArray()
        {
            var array = new List<short>() { (short)InstructionTypes.NewInstruction, (short)InstructionTypes.LongHeadToolActivation };
            var activations = ToolActivations.SelectMany(GetActivationsAsPlcArrayRepresentation);
            return array.Concat(activations).ToArray();
        }

        private IEnumerable<short> GetActivationsAsPlcArrayRepresentation(LongHeadToolActivationInformation pair)
        {
            return new List<short>
                {
                    (short)pair.LongHeadNumber,
                    (short)pair.ToolState
                };
        }
    }
}
