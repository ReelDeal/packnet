﻿namespace PackNet.Business.CodeGenerator.InstructionList
{
    using System.Collections.Generic;
    using System.Linq;

    using LongHeadPositioner;

    public class InstructionLongHeadPositionItem : InstructionItem
    {
        private readonly IEnumerable<LongHeadPositioningInformation> longHeadsToPosition;

        public InstructionLongHeadPositionItem()
        {
        }

        public InstructionLongHeadPositionItem(IEnumerable<LongHeadPositioningInformation> longHeadsToPosition)
        {
            this.longHeadsToPosition = longHeadsToPosition.ToArray();
        }

        public IEnumerable<LongHeadPositioningInformation> LongHeadsToPosition
        {
            get
            {
                return longHeadsToPosition;
            }
        }

        public override short[] ToPlcArray()
        {
            var plcArrayHeader = new List<short> { 31000, 2 };
            var positionsAsPlcArray = LongHeadsToPosition.SelectMany(GetPositionAsPlcArrayRepresentation);
            var plcArray = plcArrayHeader.Concat(positionsAsPlcArray);
            return plcArray.ToArray();
        }

        public override string ToString()
        {
            var text = LongHeadsToPosition.Aggregate("Move long heads - ", (current, pair) => current + string.Format("{0}:{1}|", pair.LongHeadNumber.ToString(), pair.Position.ToString()));
            text = text.TrimEnd('|');
            return text;
        }

        private IEnumerable<short> GetPositionAsPlcArrayRepresentation(LongHeadPositioningInformation pair)
        {
            return new List<short>
                {
                    (short)pair.LongHeadNumber,
                    GetDecimeterPart(pair.Position),
                    GetHundredsMilliMeterPart(pair.Position)
                };
        }
    }
}
