﻿

namespace PackNet.Business.CodeGenerator.InstructionList
{
    using System;
    using Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.Machines;

    public abstract class InstructionItem : IInstructionItem
    {
        public abstract override string ToString();

        public abstract short[] ToPlcArray();

        public short GetDecimeterPart(MicroMeter microMeter)
        {
            return (short)(microMeter / 100);
        }

        public short GetHundredsMilliMeterPart(MicroMeter microMeter)
        {
            return (short)Math.Round(((double)microMeter % 100) * 100, 0);
        }
    }

    
}
