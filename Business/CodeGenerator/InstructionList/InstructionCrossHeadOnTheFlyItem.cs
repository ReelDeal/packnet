﻿using PackNet.Business.CodeGenerator.Enums;

namespace PackNet.Business.CodeGenerator.InstructionList
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO;

    public class InstructionCrossHeadOnTheFlyItem : OnTheFlyInstructionItem
    {
        private readonly List<CrossHeadOnTheFlyPosition> positions = new List<CrossHeadOnTheFlyPosition>();
        private bool highSpeed = true;

        public bool HighSpeed
        {
            get
            {
                return highSpeed;
            }

            set
            {
                highSpeed = value;
            }
        }

        public IEnumerable<CrossHeadOnTheFlyPosition> Positions
        {
            get
            {
                return positions;
            }
        }

        public void AddOnTheFlyPosition(CrossHeadOnTheFlyPosition positions)
        {
            this.positions.Add(positions);
        }

        public override short[] ToPlcArray()
        {
            var positionsList = new List<short>
                {
                    31000,
                    (short)InstructionTypes.OnTheFlyCrossHeadInstruction,
                    0,
                    GetDecimeterPart(FinalPosition),
                    GetHundredsMilliMeterPart(FinalPosition)
                };
            positions.ForEach(pos => 
                {
                    positionsList.Add(GetDecimeterPart(pos.Position)); 
                    positionsList.Add(GetHundredsMilliMeterPart(pos.Position)); 
                    positionsList.Add((short)pos.ToolState);
                });

            return positionsList.ToArray();
        }

        public override string ToString()
        {
            var message2 = string.Format("OnTheFly cross head @ {0}: ", HighSpeed ? "high speed" : "low speed");

            var message = Enumerable.Aggregate(Positions, string.Empty, (current, position) => current + (position + "|"));

            message = string.Format("{0}{1}FINAL POSITION: {2}", message2, message, FinalPosition);

            return message;
        }
    }
}
