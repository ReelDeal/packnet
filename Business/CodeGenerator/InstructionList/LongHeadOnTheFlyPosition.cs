﻿
namespace PackNet.Business.CodeGenerator.InstructionList
{
    using System.Collections.Generic;
    using System.Linq;

    using LongHeadPositioner;
    using Common.Interfaces.DTO;

    public class LongHeadOnTheFlyPosition
    {
        public LongHeadOnTheFlyPosition(MicroMeter position, short percentageOfMaxSpeed, IEnumerable<LongHeadToolActivationInformation> toolStates)
        {
            Position = position;
            PercentageOfMaxSpeed = percentageOfMaxSpeed;
            ToolStates = toolStates;
        }

        public MicroMeter Position { get; private set; }

        public short PercentageOfMaxSpeed { get; private set; }

        public IEnumerable<LongHeadToolActivationInformation> ToolStates { get; private set; }

        public LongHeadOnTheFlyPosition Copy()
        {
            return new LongHeadOnTheFlyPosition(Position, PercentageOfMaxSpeed, ToolStates.ToList());
        }

        public override string ToString()
        {
            var s = Position + ": [";
            s = ToolStates.Aggregate(s, (current, state) => current + ("LH " + state.LongHeadNumber + ":" + state.ToolState + ","));
            return s.Substring(0, s.Length - 1) + "]";
        }
    }
}
