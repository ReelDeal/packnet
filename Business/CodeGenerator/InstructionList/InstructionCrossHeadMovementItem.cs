﻿using PackNet.Common.Interfaces.Machines;

namespace PackNet.Business.CodeGenerator.InstructionList
{
    using Common.Interfaces.DTO;

    public class InstructionCrossHeadMovementItem : AxisMovementItem, IMovementInstructionItem
    {
        #region Ctor

        public InstructionCrossHeadMovementItem()
        {
        }

        public InstructionCrossHeadMovementItem(MicroMeter position)
        {
            Position = position;
        }

        #endregion

        #region Properties
        
        public bool DisconnectTrack { get; set; }

        public bool SimultaneousWithFeed { get; set; }
        
        #endregion

        #region Methods

        public override short[] ToPlcArray()
        {
            var plcInstructionArray = new short[6];

            plcInstructionArray[0] = 31000;
            plcInstructionArray[1] = 1;
            plcInstructionArray[2] = SimultaneousWithFeed ? (short)1 : (short)0;
            plcInstructionArray[3] = DisconnectTrack ? (short)1 : (short)0;
            plcInstructionArray[4] = GetDecimeterPart(Position); 
            plcInstructionArray[5] = GetHundredsMilliMeterPart(Position); 

            return plcInstructionArray;
        }

        public override string ToString()
        {
            return string.Format("Move cross head to position {0}. Disconnect: {1}, SimultaneousWithFeed: {2}", Position, DisconnectTrack, SimultaneousWithFeed);
        }

        #endregion
    }
}
