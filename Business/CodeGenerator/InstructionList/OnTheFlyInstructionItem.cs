﻿using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.CodeGenerator.InstructionList
{
    public abstract class OnTheFlyInstructionItem : InstructionItem
    {
        public MicroMeter FinalPosition { get; set; }
    }
}
