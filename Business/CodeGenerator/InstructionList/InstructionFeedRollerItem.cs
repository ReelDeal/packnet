﻿using PackNet.Common.Interfaces.Machines;

namespace PackNet.Business.CodeGenerator.InstructionList
{
    using Common.Interfaces.DTO;

    public class InstructionFeedRollerItem : AxisMovementItem, IMovementInstructionItem
    {
        #region Ctor

        public InstructionFeedRollerItem()
        {
        }

        public InstructionFeedRollerItem(MicroMeter position)
        {
            Position = position;
        }

        #endregion
        
        #region Methods

        public override short[] ToPlcArray()
        {
            var plcInstuctionArray = new short[4];

            plcInstuctionArray[0] = 31000;
            plcInstuctionArray[1] = 4;
            plcInstuctionArray[2] = GetDecimeterPart(Position);
            plcInstuctionArray[3] = GetHundredsMilliMeterPart(Position);

            return plcInstuctionArray;
        }

        public override string ToString()
        {
            return string.Format("Feed roller {0} mm", Position);
        }

        #endregion
    }
}
