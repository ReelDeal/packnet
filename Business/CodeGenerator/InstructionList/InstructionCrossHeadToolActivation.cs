﻿namespace PackNet.Business.CodeGenerator.InstructionList
{
    using Enums;

    public class InstructionCrossHeadToolActivation : InstructionItem
    {
        #region Ctor

        public InstructionCrossHeadToolActivation()
        {
        }

        public InstructionCrossHeadToolActivation(ToolStates toolState)
        {
            Toolstate = toolState;
        }

        #endregion

        #region Properties

        public ToolStates Toolstate { get; set; }

        #endregion

        #region Methods

        public override short[] ToPlcArray()
        {
            var plcInstructionArray = new short[3];

            plcInstructionArray[0] = 31000;
            plcInstructionArray[1] = 3;
            plcInstructionArray[2] = (short)Toolstate;

            return plcInstructionArray;
        }

        public override string ToString()
        {
            return string.Format("Set CrossHead toolstate to '{0}'", Toolstate);
        }

        #endregion
    }
}
