﻿namespace PackNet.Business.CodeGenerator
{
    using System.Collections.Generic;

    using InstructionList;

    public interface ICodeOptimizer
    {
        IEnumerable<InstructionItem> Optimize(IEnumerable<InstructionItem> instructionList);
    }
}
