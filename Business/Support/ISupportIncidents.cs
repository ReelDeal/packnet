﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Business.Support
{
    public interface ISupportIncidents
    {
        void Create(params SupportIncident[] supportIncidents);
        SupportIncident Find(Guid id);
        IEnumerable<SupportIncident> FindUnresolvedIncidents();
        void Update(SupportIncident incident);
    }
}