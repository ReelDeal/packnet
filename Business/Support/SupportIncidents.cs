﻿using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Common.Interfaces.DTO;
using PackNet.Data.Support;

namespace PackNet.Business.Support
{
    public class SupportIncidents : ISupportIncidents
    {
        private readonly ISupportIncidentRepository supportIncidentRepository;

        public SupportIncidents(ISupportIncidentRepository supportIncidentRepository)
        {
            this.supportIncidentRepository = supportIncidentRepository;
        }

        public void Create(params SupportIncident[] supportIncidents)
        {
            supportIncidentRepository.Create(supportIncidents);
        }

        public SupportIncident Find(Guid id)
        {
            return supportIncidentRepository.Find(id);
        }

        public IEnumerable<SupportIncident> FindUnresolvedIncidents()
        {
            return supportIncidentRepository.All().Where(x => !x.DispatchedOn.HasValue).ToList();
        }

        public void Update(SupportIncident incident)
        {
            supportIncidentRepository.Update(incident);
        }
    }
}