﻿namespace PackNet.Business.Events
{
    using System;

    public abstract class DataRequestedEventArgs : EventArgs
    {
        public DataRequestedEventArgs(string replyQueue)
        {
            ReplyQueue = replyQueue;
        }

        public string ReplyQueue { get; private set; }
    }
}
