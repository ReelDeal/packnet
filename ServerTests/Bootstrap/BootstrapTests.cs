﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Common.Interfaces.Logging;
using PackNet.Server.Bootstrapper;
using PackNet.Server.Bootstrapper.Interfaces;
using Testing.Specificity;

namespace ServerTests.Bootstrap
{
    [TestClass]
    public class BootstrapTests
    {
        private IBootstrap bootstrap;
        private Mock<IRegistryHelper> mockRegistryHelper;
        private Mock<IServiceHelper> mockServiceHelper;
        private Mock<IMachineHelper> mockMachineHelper;
        private Mock<ILogger> mockLogger;

        [TestInitialize]
        public void Initialize()
        {
            mockRegistryHelper = new Mock<IRegistryHelper>();
            mockServiceHelper = new Mock<IServiceHelper>();
            mockMachineHelper = new Mock<IMachineHelper>();
            mockLogger = new Mock<ILogger>();
            bootstrap = new Bootstrapper(mockRegistryHelper.Object, mockServiceHelper.Object, mockMachineHelper.Object, mockLogger.Object);
        }

        [TestCleanup]
        public void Cleanup() { }

        #region Methods return as expected

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsOkWhenRegistryEntryIsFound()
        {
            mockRegistryHelper.Setup(x => x.GetRegistryEntryValue("FAKE_REGISTRY_KEY", "FAKE_REGISTRY_VALUE")).Returns("I_FOUND_IT");

            var validate = bootstrap.ValidateInstall("doesntmatter", "FAKE_REGISTRY_KEY", "FAKE_REGISTRY_VALUE");

            Assert.AreEqual(BootstrapResultEnum.OK, validate.Item1);
            Specify.That(validate.Item2).Should.BeEqualTo(string.Empty);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsFatalWhenRegistryEntryIsNotFound()
        {
            var validate = bootstrap.ValidateInstall("doesntmatter", "FAKE_REGISTRY_KEY", "FAKE_REGISTRY_VALUE");

            Assert.AreEqual(BootstrapResultEnum.FATAL, validate.Item1);
            Specify.That(validate.Item2).Should.Not.BeEqualTo(string.Empty);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsWarningWhenServiceIsNotFoundWhenOverriden()
        {
            mockServiceHelper.Setup(x => x.IsServiceInstalled("FAKE_SERVICE_NAME")).Returns(false);

            var result = bootstrap.ValidateServiceInstalled("FAKE_SERVICE_NAME", false);

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.WARNING);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsFatalWhenServiceIsNotFoundByDefault()
        {
            mockServiceHelper.Setup(x => x.IsServiceInstalled("FAKE_SERVICE_NAME")).Returns(false);

            var result = bootstrap.ValidateServiceInstalled("FAKE_SERVICE_NAME");

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.FATAL);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsOkWhenServiceIsFound()
        {
            mockServiceHelper.Setup(x => x.IsServiceInstalled("FAKE_SERVICE_NAME")).Returns(true);

            var result = bootstrap.ValidateServiceInstalled("FAKE_SERVICE_NAME");

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.OK);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsWarningWhenServiceIsNotRunningWhenOverriden()
        {
            mockServiceHelper.Setup(x => x.IsServiceRunning("FAKE_SERVICE_NAME")).Returns(false);

            var result = bootstrap.ValidateServiceRunning("FAKE_SERVICE_NAME", false);

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.WARNING);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsFatalWhenServiceIsNotRunningAndCannotStartByDefault()
        {
            mockServiceHelper.Setup(x => x.IsServiceRunning("FAKE_SERVICE_NAME")).Returns(false);
            mockServiceHelper.Setup(x => x.StartService("FAKE_SERVICE_NAME")).Returns(false);

            var result = bootstrap.ValidateServiceRunning("FAKE_SERVICE_NAME");

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.FATAL);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsFatalWhenServiceIsNotRunningAndStartServiceIsSetToFalse()
        {
            mockServiceHelper.Setup(x => x.IsServiceRunning("FAKE_SERVICE_NAME")).Returns(false);
            mockServiceHelper.Setup(x => x.StartService("FAKE_SERVICE_NAME")).Returns(false);

            var result = bootstrap.ValidateServiceRunning("FAKE_SERVICE_NAME", tryToStartServiceIfNotRunning: false);

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.FATAL);
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsWarningWhenServiceIsNotRunningAndStartServiceSucceeds()
        {
            mockServiceHelper.Setup(x => x.IsServiceRunning("FAKE_SERVICE_NAME")).Returns(false);
            mockServiceHelper.Setup(x => x.StartService("FAKE_SERVICE_NAME")).Returns(true);

            var result = bootstrap.ValidateServiceRunning("FAKE_SERVICE_NAME");

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.WARNING);
            Specify.That(result.Item2).Should.Not.BeNullOrEmpty();
        }

        [TestMethod]
        [TestCategory("ValidateFunctionality")]
        public void BootstrapReturnsOkWhenServiceIsRunning()
        {
            mockServiceHelper.Setup(x => x.IsServiceRunning("FAKE_SERVICE_NAME")).Returns(true);

            var result = bootstrap.ValidateServiceRunning("FAKE_SERVICE_NAME");

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.OK);
        }

        [TestMethod]
        public void BootstrapValidatesRamSizeGreateThanRecommendedReturnsOk()
        {
            mockMachineHelper.Setup(x => x.GetPhysicalMemorySize()).Returns(5);

            var result = bootstrap.ValidateMemorySize(4);

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.OK);
            Specify.That(result.Item2).Should.BeEqualTo(string.Empty);
        }

        [TestMethod]
        public void BootstrapValidatesRamSizeLessThanRecommendedReturnsWarning()
        {
            var result = bootstrap.ValidateMemorySize(long.MaxValue);

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.WARNING);
            Specify.That(result.Item2).Should.Not.BeEqualTo(string.Empty);
        }

        [TestMethod]
        public void BootstrapValidatesFreeDiskSpaceGreateThanRecommendedReturnsOk()
        {
            mockMachineHelper.Setup(x => x.GetFreeDiskSpace(It.IsAny<string>())).Returns(long.MaxValue);

            var result = bootstrap.ValidateFreeDiskSpace(4);

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.OK);
            Specify.That(result.Item2).Should.BeEqualTo(string.Empty);
        }

        [TestMethod]
        public void BootstrapValidatesFreeDiskSpaceLessThanRecommendedReturnsWarning()
        {
            var result = bootstrap.ValidateFreeDiskSpace(long.MaxValue);

            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.WARNING);
            Specify.That(result.Item2).Should.Not.BeEqualTo(string.Empty);
        }

        #endregion Methods return as expected

        #region Validating all software is installed
                //todo: fix unit test
#if DEBUG
        [TestMethod]
        public void BootstrapValidatesErlangIsInstalled()
        {
            bootstrap.Validate().ToList();

            mockRegistryHelper.Verify(x => x.GetRegistryEntryValue(Bootstrapper.ERLANG_REGISTRY_LOCATION, Bootstrapper.ERLANG_REGISTRY_VALUE), Times.Once);
        }
        
#endif


        #endregion Validating all software is installed

       

       


        [TestMethod]
        public void BootstrapValidatesIisInstalled()
        {
            bootstrap.Validate().ToList();

            mockMachineHelper.Verify(x => x.ValidateIisInstalledAndAcceptableVersion(mockRegistryHelper.Object), Times.Once);
        }

        [TestMethod]
        public void BootstrapValidatesRamSize()
        {
            bootstrap.Validate().ToList();

            mockMachineHelper.Verify(x => x.GetPhysicalMemorySize(), Times.Once);
        }

        [TestMethod]
        public void BootstrapValidatesFreeSpaceSize()
        {
            bootstrap.Validate().ToList();

            mockMachineHelper.Verify(x => x.GetFreeDiskSpace(It.IsAny<string>()), Times.Once);
        }

        [TestMethod]
        public void BootstrapAttemptsToOpenFirewallPort()
        {
            bootstrap.Validate().ToList();

            mockMachineHelper.Verify(x => x.OpenFirewallPort(It.IsAny<int>()), Times.Once);
        }

        [TestMethod]
        public void BootstrapWarningResultWhenCannotOpenFirewall()
        {
            mockMachineHelper.Setup(x => x.OpenFirewallPort(1234)).Returns(false);

            var result = bootstrap.ValidateFirewallOpen(1234);
            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.WARNING);
            Specify.That(result.Item2).Should.Not.BeEqualTo(string.Empty);
        }

        [TestMethod]
        public void BootstrapFatalResultWhenServiceNotInstalledByDefault()
        {
            //mockMachineHelper.Setup(x => x.OpenFirewallPort(1234)).Returns(false);

            var result = bootstrap.ValidateServiceInstalled("doesntmatter");
            Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.FATAL);
            Specify.That(result.Item2).Should.Not.BeEqualTo(string.Empty);
        }

        
        //[TestMethod]
        //public void BootstrapFatalResultWhenExceptionThrown()
        //{
        //    mockRuntimeDbSetup.Setup(x => x.CreateDatabase()).Throws(new Exception());

        //    var result = bootstrap.ValidateDatabase();

        //    Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.FATAL);
        //    Specify.That(result.Item2).Should.Not.BeEqualTo(String.Empty);
        //}

        //[TestMethod]
        //public void BootstrapOkResultWhenNoExceptionThrown()
        //{
        //    var result = bootstrap.ValidateDatabase();

        //    Specify.That(result.Item1).Should.BeEqualTo(BootstrapResultEnum.OK);
        //    Specify.That(result.Item2).Should.BeEqualTo(String.Empty);
        //}

    }
}