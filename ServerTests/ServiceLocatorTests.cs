﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Services;
using System;
using System.Collections.Generic;
using Testing.Specificity;

namespace ServerTests
{
    [TestClass]
   public class ServiceLocatorTests
    {
        [TestMethod]
        public void ShouldRaiseOnNextWhenServiceAdded()
        {
            IService foundService = null;
            var objectInTest = new ServiceLocator("ome path", new List<IService>(), new Mock<ILogger>().Object);
            objectInTest.ServiceAddedObservable.Subscribe((s) =>
            {
                if (s is ITestService)
                    foundService = s;
            });

            try
            {
                foundService = objectInTest.Locate<ITestService>();
            }
            catch
            {
                
                objectInTest.RegisterAsService(new testingService());
            }

            Retry.Do(Specify.That(foundService).Should.Not.BeNull,TimeSpan.FromMilliseconds(20),3);
        }
    }
    public class testingService : ITestService
    {
        public void Dispose()
        {
            
        }

        public string Name { get { return "Tester"; } }
    }

    public interface ITestService:IService
    {
    }
}
