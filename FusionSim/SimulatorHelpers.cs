﻿using System.Collections.Generic;
using System.Linq;

namespace PackNet.FusionSim
{
    public static class SimulatorHelpers
    {
        public static double GetPackagingLength(IList<short> instructionList)
        {
            var packagingLength = 0d;

            for (var i = 0; i < instructionList.Count(); i++)
            {
                if (instructionList[i] == 31000 && instructionList[i + 1] == 4) // feed
                {
                    packagingLength += (((double)instructionList[i + 2]) * 100) + (((double)instructionList[i + 3]) / 100);
                }
                else if (instructionList[i] == 31000 && instructionList[i + 1] == 9) // LH OTF
                {
                    packagingLength += (((double)instructionList[i + 3]) * 100) + (((double)instructionList[i + 4]) / 100);
                }
                else if (instructionList[i] == 31000 && instructionList[i + 1] == 8 && instructionList[i + 3] == 0) // Track is deactivated
                {
                    return packagingLength;
                }
                else if (instructionList[i] == 31000 && instructionList[i + 1] == 1 && instructionList[i + 3] == 1) // Ch move with deactivate track
                {
                    return packagingLength;
                }
            }

            return packagingLength;
        }
    }
}
