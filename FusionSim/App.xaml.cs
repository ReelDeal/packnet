﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Common.Logging.NLogBackend;
using PackNet.FusionSim.ViewModels;

namespace PackNet.FusionSim
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        SimController simController;
        private ILogger logger;

        public App()
        {
            LogManager.LoggingBackend = new NLogBackend("FusionSimulator", ".\\NLog.config");
            logger = LogManager.GetLogFor("FusionSimulator");
            Startup += App_Startup;
            Exit +=App_Exit;
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exception = e.ExceptionObject as Exception;

            if (exception != null)
            {
                logger.Log(LogLevel.Fatal, "We're going down: {0}", exception.ToString());
            }
            else
            {
                logger.Log(LogLevel.Fatal, "Now this is truly bizarre.");
            }
        }

        void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            logger.LogException(LogLevel.Error, e.Exception.Message, e.Exception);
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            Task task = Task.Factory.StartNew(() =>
            {
                simController.Stop();
                simController.Dispose();
            });
            task.ContinueWith((t) =>
            {
                if (t.IsFaulted)
                {
                    logger.Log(LogLevel.Error, "App_Exit in App.xaml.cs has faulted!");

                    if (t.Exception != null)
                    {
                        logger.Log(LogLevel.Error, "App_Exit in App.xaml.cs has faulted: {0}", t.Exception.ToString());
                    }
                }
            });
            Thread.Sleep(5000);
            Current.Shutdown(0);
        }

        private void App_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                simController = new SimController();
                var window = new MainWindow { DataContext = simController };
                simController.Start();
                window.Show();
            }
            catch(Exception exception)
            {
                logger.Log(LogLevel.Error, exception.ToString());
                throw;
            }
        }
    }
}
