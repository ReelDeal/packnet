﻿using System.Net.Sockets;
using System.Windows;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PackNet.Common.Communication.RabbitMQ;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.FusionSim.Properties;
using PackNet.FusionSim.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Timers;
using System.Windows.Input;
using System.Windows.Threading;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Common.Logging.NLogBackend;
using PackNet.Common.WorkflowTracking;

namespace PackNet.FusionSim.ViewModels
{
    public class SimController : NotifyPropertyChangedBase, IService
    {
        private readonly Dispatcher dispatcher;
        private readonly ICommand startStopAllSimulatorsCommand;
        private readonly ICommand startupAllSimulatorsCommand;
        private readonly ICommand loopAllSimulatorsCommand;

        private readonly System.Timers.Timer checkIfAliveTimer;
        private readonly System.Timers.Timer serverDiedTimer;

        /// <summary>
        /// The server
        /// </summary>
        private RunningServer server;

        private SimulatorCollection simulatorCollection;
        private string simulationNextStep;
        private bool areSimsStarted;
        private ILogger logger;

        public ICommand StartStopAllSimulatorsCommand { get { return startStopAllSimulatorsCommand; } }

        public ICommand StartupAllSimulatorsCommand { get { return startupAllSimulatorsCommand; } }

        public ICommand LoopAllSimulatorsCommand { get { return loopAllSimulatorsCommand; } }

        public string SimulationNextStep
        {
            get { return simulationNextStep; }
            set
            {
                simulationNextStep = value; 
                OnPropertyChanged();
            }
        }

        public bool AreSimsStarted
        {
            get { return areSimsStarted; }
            set
            {
                areSimsStarted = value;
                SimulationNextStep = areSimsStarted ? "Stop" : "Start";
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimController"/> class.
        /// </summary>
        public SimController()
        {
            LogManager.LoggingBackend = new NLogBackend("FusionSimulator", ".\\NLog.config");
            logger = LogManager.GetLogFor("FusionSimulator");
            AreSimsStarted = false;
            Server = new RunningServer(GetMachines)
            {
                IpAddress = Properties.Settings.Default.ServerIpAddress
            };

            SimulatorCollection = new SimulatorCollection(Server.SendVariableNotification);
            Thread.Sleep(TimeSpan.FromSeconds(3));
            dispatcher = RootDispatcherFetcher.RootDispatcher;
            startStopAllSimulatorsCommand = new RelayCommand(StartStopAll, obj => Server.IsReady);
            startupAllSimulatorsCommand = new RelayCommand(StartupAll, obj => Server.IsReady && AreSimsStarted);
            loopAllSimulatorsCommand = new RelayCommand(LoopAll, obj => Server.IsReady && AreSimsStarted);

            var timersTime = TimeSpan.FromSeconds(int.Parse(Properties.Settings.Default.SecondsToWaitToCheckIfServerIsAlive)).TotalMilliseconds;
            checkIfAliveTimer = new System.Timers.Timer(timersTime);
            serverDiedTimer = new System.Timers.Timer(2 * timersTime);
        }

        private void LoopAll(object obj)
        {
            foreach (var simulator in SimulatorCollection.Sims.Where(simulator => simulator.IsSimulationStarted))
            {
                if (simulator.IsLooping)
                {
                    simulator.LoopComplete();
                }
            }
        }

        private void StartupAll(object obj)
        {
            foreach (var simulator in SimulatorCollection.Sims.Where(simulator => simulator.IsSimulationStarted))
            {
                simulator.StartupComplete();
            }
        }

        private void StartStopAll(object obj)
        {
            foreach (var simulator in SimulatorCollection.Sims.Where(simulator => simulator.IsSimulationStarted == AreSimsStarted))
            {
                simulator.StartStopSimulation();
            }

            AreSimsStarted = !AreSimsStarted;
        }

        /// <summary>
        /// Gets or sets the server.
        /// </summary>
        /// <value>
        /// The server.
        /// </value>
        public RunningServer Server
        {
            get { return server; }
            set
            {
                if (Equals(value, server))
                {
                    return;
                }
                server = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the simulator collection.
        /// </summary>
        /// <value>
        /// The simulator collection.
        /// </value>
        public SimulatorCollection SimulatorCollection
        {
            get { return simulatorCollection; }
            set
            {
                if (Equals(value, simulatorCollection))
                {
                    return;
                }
                simulatorCollection = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            Server.Start();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            checkIfAliveTimer.Stop();
            serverDiedTimer.Stop();
            Server.Stop();
            SimulatorCollection.Stop();

            checkIfAliveTimer.Dispose();
            serverDiedTimer.Dispose();
            //GC.Collect();
        }

        /// <summary>
        /// Gets the machines.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="body">The body.</param>
        private void GetMachines(string type, string body)
        {
            if (type == "ConnectionAlive" && !Server.IsReady)
            {
                Server.GetSimulators();
            }
            else if (type == "ConnectionAlive" && Server.IsReady)
            {
                serverDiedTimer.Enabled = false;
                serverDiedTimer.Elapsed -= ServerDied;
                serverDiedTimer.Stop();
                serverDiedTimer.Elapsed += ServerDied;
                serverDiedTimer.Enabled = true;
                serverDiedTimer.Start();
            }
            else if (type == "Machines" && !Server.IsReady)
            {
                var simulatorLists = GetSimulatorList(body).ToList();

                if(!simulatorLists.Any())
                    simulatorLists.Add(new NullMachineObject(){Alias = "No machines configured in .Server"});

                InitialMachineLoad(simulatorLists);

                checkIfAliveTimer.Elapsed -= CheckIfAlive;
                checkIfAliveTimer.Elapsed += CheckIfAlive;
                checkIfAliveTimer.Enabled = true;
            }
            else if (type == "Machines" && Server.IsReady)
            {
                var simulatorLists = GetSimulatorList(body).ToList();

                if (simulatorLists.Count() != SimulatorCollection.Sims.Count)
                {
                    UpdateMachineList(simulatorLists);
                }
            }
        }

        private void ServerDied(object sender, ElapsedEventArgs e)
        {
            foreach (var fusion in SimulatorCollection.Sims.Where(simulator => simulator.MachineType == MachineType.Fusion && simulator.IsSimulationStarted).Cast<FusionSimulatorViewModel>())
            {
                fusion.ClearQueue();
            }
        }

        private void CheckIfAlive(object sender, ElapsedEventArgs e)
        {
            Server.CheckIfAlive();
        }

        /// <summary>
        /// Updates the machine list.
        /// </summary>
        /// <param name="machines">The machines.</param>
        private void UpdateMachineList(List<MachineObject> machines)
        {
            Server.IsReady = false;

            // Add missing simulators
            foreach (var machine in machines)
            {
                if (SimulatorCollection.Sims.SingleOrDefault(s => s.Name == machine.Alias) == null)
                {
                    var simulator = machine;
                    dispatcher.Invoke(() =>
                    {
                        var sim = GetSimulator(simulator);
                        SimulatorCollection.Sims.Add(sim);
                    });
                }
            }

            // Remove non existing simulators.
            for (var index = SimulatorCollection.Sims.Count - 1; index >= 0; index--)
            {
                var sim = SimulatorCollection.Sims[index];
                if (machines.SingleOrDefault(s => s.Alias == sim.Name) == null)
                {
                    sim.Stop();
                    var machineIndexToRemove = index;
                    dispatcher.Invoke(() => SimulatorCollection.Sims.RemoveAt(machineIndexToRemove));
                }
            }

            Server.IsReady = true;
        }

        /// <summary>
        /// Initials the machine load.
        /// </summary>
        /// <param name="simulatorLists">The simulator lists.</param>
        private void InitialMachineLoad(IEnumerable<MachineObject> simulatorLists)
        {
            dispatcher.Invoke(() =>
            {
                simulatorLists = simulatorLists.OrderBy(s => s.Alias);

                foreach (var simulator in simulatorLists)
                {
                    var sim = GetSimulator(simulator);

                    SimulatorCollection.Sims.Add(sim);
                }

                SimulatorCollection.SelectedSimulator = SimulatorCollection.Sims.FirstOrDefault();
            });

            try
            {
                Server.IsReady = true;
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, "InitialMachineLoad Error: {0}", exception.ToString());
                throw;
                // Trying to kill all the process an exception rise, not sure what it is
            }
        }

        /// <summary>
        /// Gets the simulator.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns></returns>
        private Simulator GetSimulator(MachineObject machine)
        {
            Simulator simulator = new NullSimulatorViewModel();

            switch (machine.Type)
            {
                case  MachineType.Em:
                    simulator = new EmSimulatorViewModel(server.SendVariableNotification, new IoPanelViewModel(machine, server.SendVariableNotification));
                    break;
                case MachineType.Fusion:
                    simulator = new FusionSimulatorViewModel(server.SendVariableNotification, new IoPanelViewModel(machine, server.SendVariableNotification));
                    break;
                case MachineType.ZebraPrinter:
                    simulator = new ZebraPrinterSimulatorViewModel(machine.Alias);
                    break;
            }

            simulator.Name = machine.Alias;
            simulator.Ip = machine.IpOrDnsName;
            simulator.Port = machine.Port;
            simulator.MachineType = machine.Type;
            return simulator;
        }

        /// <summary>
        /// Gets the simulator list.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        private static IEnumerable<MachineObject> GetSimulatorList(string body)
        {
            var currentHostName = Dns.GetHostName();
            var host = Dns.GetHostEntry(currentHostName);
            var serverIp = IPAddress.Parse(Settings.Default.ServerIpAddress);
            var serverIsRunningOnLoopback = IPAddress.IsLoopback(serverIp);

            var message = JsonConvert.DeserializeObject<Message<string>>(body);
            var decompressed = RabbitSubscriber.Decompress(message.Data);
            var machines = JArray.Parse(decompressed).Select(c => new MachineObject
            {
                MachineType = c["MachineType"].ToString(),
                Alias = c["Alias"].ToString(),
                IpOrDnsName = (c["Data"])["IpOrDnsName"].ToString(),
                Port = (int)c["Data"]["Port"],
                MachineId = (Guid)c["Data"]["Id"]
            }).Where(s => s.MachineType == "Fusion" || s.MachineType == "Em" || s.MachineType == "ZebraPrinter").ToList();

            foreach (var machine in machines)
            {
                var simAddress = Networking.FindIPAddress(machine.IpOrDnsName, true);
                var simIsRunningOnLoopback = IPAddress.IsLoopback(simAddress);

                //Don't add the sim if it is using port 80 and running on the same machine as the server
                if (serverIsRunningOnLoopback && simIsRunningOnLoopback && machine.Port == 80)
                {
                    //TODO: Log messagebox
                    //MessageBox.Show("Machine " + machine.Alias + " was not added because port 80 is in use on the server");
                    continue;
                }
                //Don't add the sim if the machine is not configured to run on the current IP address
                if (host.AddressList.All(a => a.ToString() != simAddress.ToString()) &&
                    !(serverIsRunningOnLoopback && simIsRunningOnLoopback))
                {
                    //TODO: Log messagebox
                    //var addresses = String.Join(",", host.AddressList.ToList());
                    //MessageBox.Show("Machine " + machine.Alias + " was this host doesn't contain the IP " + simAddress + Environment.NewLine + Environment.NewLine + "Configured Addresses: " + addresses);
                    continue;
                }
                switch (machine.MachineType)
                {
                    case "ZebraPrinter":
                        machine.Type = MachineType.ZebraPrinter;
                        break;
                    case "Em":
                        machine.Type = MachineType.Em;
                        break;
                    case "Fusion":
                        machine.Type = MachineType.Fusion;
                        break;
                    default:
                        machine.Type = MachineType.NotSupported;
                        break;
                }

                    yield return machine;
                }
            }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~SimController()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                foreach (var simulator in SimulatorCollection.Sims)
                {
                    simulator.Dispose();
                }

                Server.Dispose();
            }
        }

        public class MachineObject
        {
            public string MachineType { get; set; }
            public string Alias { get; set; }
            public string IpOrDnsName { get; set; }
            public int Port { get; set; }
            public MachineType Type { get; set; }
            public Guid MachineId { get; set; }

            public MachineObject()
            {
                Type = Utilities.MachineType.NotSupported;
            }
        }
        private class NullMachineObject : MachineObject { }
    }
}