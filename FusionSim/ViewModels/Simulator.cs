﻿using System.Threading.Tasks;

using PackNet.FusionSim.Models;
using PackNet.FusionSim.Utilities;
using System;
using System.Threading;
using System.Windows.Input;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Common.WorkflowTracking;

namespace PackNet.FusionSim.ViewModels
{
    public abstract class Simulator : NotifyPropertyChangedBase, IService, IDisposable
    {
        private readonly ICommand startStopSimulationCommand;
        private readonly ICommand startupCompleteCommand;
        private readonly ICommand clearDetailsCommand;
        private readonly ICommand heartBeatCommand;
        private string name;
        private bool isSimulationStarted;
        private bool isLooping;
        private int loopTimeInSeconds;
        private Uri machineUri;

        protected CancellationTokenSource tokenSource = new CancellationTokenSource();
        protected string ip;
        protected int port;
        protected Task loopTask;
        private ILogger logger;

        /// <summary>
        /// The loop complete command
        /// </summary>
        protected ICommand loopCompleteCommand;

        private string heartBeatCommandStatus;

        /// <summary>
        /// Gets the simulator.
        /// </summary>
        /// <value>
        /// The simulator.
        /// </value>
        public abstract BaseSimulator Sim { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is editing enable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is editing enable; otherwise, <c>false</c>.
        /// </value>
        public abstract bool IsEditingEnable { get; }

        /// <summary>
        /// Gets the loop complete command.
        /// </summary>
        /// <value>
        /// The loop complete command.
        /// </value>
        public ICommand LoopCompleteCommand { get { return loopCompleteCommand; } }

        /// <summary>
        /// Gets the start stop simulation command.
        /// </summary>
        /// <value>
        /// The start stop simulation command.
        /// </value>
        public ICommand StartStopSimulationCommand { get { return startStopSimulationCommand; } }

        /// <summary>
        /// Gets the startup complete command.
        /// </summary>
        /// <value>
        /// The startup complete command.
        /// </value>
        public ICommand StartupCompleteCommand { get { return startupCompleteCommand; } }

        /// <summary>
        /// Gets the clear details command.
        /// </summary>
        /// <value>
        /// The clear details command.
        /// </value>
        public ICommand ClearDetailsCommand {get { return clearDetailsCommand; }}

        public ICommand HeartBeatCommand {get { return heartBeatCommand; }}

        /// <summary>
        /// Gets or sets a value indicating whether this instance is looping.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is looping; otherwise, <c>false</c>.
        /// </value>
        public bool IsLooping
        {
            get { return isLooping; }
            set
            {
                if (value.Equals(isLooping))
                {
                    return;
                }
                isLooping = value;
                OnPropertyChanged();
                OnPropertyChanged("LoopNextStep");
                OnPropertyChanged("IsEditingEnable");
            }
        }

        /// <summary>
        /// Gets or sets the loop time in seconds.
        /// </summary>
        /// <value>
        /// The loop time in seconds.
        /// </value>
        public int LoopTimeInSeconds
        {
            get { return loopTimeInSeconds; }
            set
            {
                if (value == loopTimeInSeconds)
                {
                    return;
                }
                loopTimeInSeconds = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the loop next step.
        /// </summary>
        /// <value>
        /// The loop next step.
        /// </value>
        public string LoopNextStep { get { return IsLooping ? "Stop" : "Loop"; } }

        /// <summary>
        /// Gets a value indicating whether this instance is simulation started.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is simulation started; otherwise, <c>false</c>.
        /// </value>
        public bool IsSimulationStarted
        {
            get { return isSimulationStarted; }
            private set
            {
                if (value.Equals(isSimulationStarted))
                {
                    return;
                }
                isSimulationStarted = value;
                OnPropertyChanged();
                OnPropertyChanged("SimulationNextStep");
            }
        }

        /// <summary>
        /// Gets the simulation next step.
        /// </summary>
        /// <value>
        /// The simulation next step.
        /// </value>
        public string SimulationNextStep
        {
            get { return IsSimulationStarted ? "Stop Simulation" : "Start Simulation"; }
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return name; }
            set
            {
                if (value == name)
                {
                    return;
                }
                name = value;
                OnPropertyChanged();
            }
        }

        public string HeartBeatCommandStatus
        {
            get { return heartBeatCommandStatus; }
            set
            {
                heartBeatCommandStatus = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the machine URI.
        /// </summary>
        /// <value>
        /// The machine URI.
        /// </value>
        public Uri MachineUri
        {
            get { return machineUri; }
            protected set
            {
                if (value == machineUri)
                {
                    return;
                }
                machineUri = value;
                OnPropertyChanged();
            } 
        }

        /// <summary>
        /// Gets or sets the ip.
        /// </summary>
        /// <value>
        /// The ip.
        /// </value>
        public string Ip
        {
            get { return ip; }
            set
            {
                if (value == ip)
                {
                    return;
                }
                ip = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the port.
        /// </summary>
        /// <value>
        /// The port.
        /// </value>
        public int Port
        {
            get { return port; }
            set
            {
                if (value == port)
                {
                    return;
                }
                port = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the type of the machine.
        /// </summary>
        /// <value>
        /// The type of the machine.
        /// </value>
        /// <remarks>
        /// This discriminator is required by the UI, to know what template to show.
        /// </remarks>
        public MachineType MachineType { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Simulator"/> class.
        /// </summary>
        protected Simulator()
        {
            logger = LogManager.GetLogFor("FusionSimulator");
            
            // Default Values
            Ip = "127.0.0.1";
            Port = 8081;
            LoopTimeInSeconds = 2;
            IsSimulationStarted = false;
            IsLooping = false;
            
            // Button Bindings
            startStopSimulationCommand = new RelayCommand(StartStopSimulation);
            startupCompleteCommand = new RelayCommand(StartupComplete, a => IsSimulationStarted && !Sim.IsStartupComplete);
            clearDetailsCommand = new RelayCommand(ClearDetails);
            heartBeatCommand = new RelayCommand(HeartBeat);
            HeartBeatCommandStatus = "Hide Heart Beat";
        }

        private void HeartBeat(object obj)
        {
            Sim.ShowHeartBeat = !Sim.ShowHeartBeat;
            HeartBeatCommandStatus = Sim.ShowHeartBeat ? "Hide Heart Beat" : "Show Heart Beat";
        }

        private void ClearDetails(object obj)
        {
            Sim.ClearDetails();
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public abstract void Start();

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public abstract void Stop();

        /// <summary>
        /// Startups the complete.
        /// </summary>
        /// <param name="obj">The object.</param>
        public abstract void StartupComplete(object obj = null);

        /// <summary>
        /// Loops the complete.
        /// </summary>
        /// <param name="obj">The object.</param>
        public abstract void LoopComplete(object obj = null);

        /// <summary>
        /// Starts the stop simulation.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void StartStopSimulation(object obj = null)
        {

            if (!IsSimulationStarted)
            {
                try
                {
                    Start();
                }
                catch(Exception exception)
                {
                    // The exception could be rise when trying to open 2 host with the same address.
                    logger.Log(LogLevel.Fatal, exception.ToString());
                    throw;
                    //return;
                }
            }
            else
            {
                Stop();
            }

            IsSimulationStarted = !IsSimulationStarted;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
         public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

         /// <summary>
         /// Finalizes an instance of the <see cref="Simulator"/> class.
         /// </summary>
        ~Simulator()
        {
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                Stop();
            }
        }
    }
}
