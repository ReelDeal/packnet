﻿using PackNet.Common.Interfaces.Utils;
using PackNet.Communication.PLCBase;
using PackNet.FusionSim.Communication;
using PackNet.FusionSim.Models;
using PackNet.FusionSim.Utilities;
using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Input;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Common.WorkflowTracking;

namespace PackNet.FusionSim.ViewModels
{
    public class FusionSimulatorViewModel:Simulator
    {
        private readonly ICommand startSCompletePackagingCommand;
        private readonly ICommand outOfCorrugateCommand;
        private readonly ICommand lowAirCommand;
        private readonly ICommand failCommand;
        private readonly ICommand emergencyCommand;

        private bool removeBox;
        private bool isPackaging;

        private FusionSimulator simulator;
        private WebServiceHost host;
        private ServiceEndpoint endPoint;
        private readonly ILogger logger;

        private readonly Func<string, object, string, string> sendVariableNotification;

        /// <summary>
        /// Gets or sets a value indicating whether the user can remove the box.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the user can remove the box; otherwise, <c>false</c>.
        /// </value>
        public bool RemoveBox
        {
            get { return removeBox; }
            set
            {
                if (value.Equals(removeBox) || simulator.InError || !simulator.IsStartupComplete)
                {
                    return;
                }
                removeBox = value;
                OnPropertyChanged();
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is packaging.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is packaging; otherwise, <c>false</c>.
        /// </value>
        public bool IsPackaging
        {
            get { return isPackaging; }
            set
            {
                if (value.Equals(isPackaging))
                {
                    return;
                }
                isPackaging = value;
                OnPropertyChanged();
                OnPropertyChanged("PackagingNextStep");
                OnPropertyChanged("IsEditingEnable");
            }
        }

        /// <summary>
        /// Gets the packaging next step.
        /// </summary>
        /// <value>
        /// The packaging next step.
        /// </value>
        public string PackagingNextStep
        {
            get { return IsPackaging ? "Complete" : "Start"; }
        }

        /// <summary>
        /// Gets or sets the fusion simulator.
        /// </summary>
        /// <value>
        /// The fusion simulator.
        /// </value>
        public FusionSimulator Simulator
        {
            get { return simulator; }
            set
            {
                if (Equals(value, simulator))
                    return;
                simulator = value;
                OnPropertyChanged();
                OnPropertyChanged("Sim");
            }
        }

        /// <summary>
        /// Gets the start s complete packaging command.
        /// </summary>
        /// <value>
        /// The start s complete packaging command.
        /// </value>
        public ICommand StartSCompletePackagingCommand { get { return startSCompletePackagingCommand; } }

        /// <summary>
        /// Gets the out of corrugate command.
        /// </summary>
        /// <value>
        /// The out of corrugate command.
        /// </value>
        public ICommand OutOfCorrugateCommand { get { return outOfCorrugateCommand; } }


        public ICommand LowAirCommand { get { return lowAirCommand; } }

        /// <summary>
        /// Gets the fail command.
        /// </summary>
        /// <value>
        /// The fail command.
        /// </value>
        public ICommand FailCommand { get { return failCommand; } }

        /// <summary>
        /// Gets the emergency command.
        /// </summary>
        /// <value>
        /// The emergency command.
        /// </value>
        public ICommand EmergencyCommand { get { return emergencyCommand; } }
        
        /// <summary>
        /// Gets the simulator.
        /// </summary>
        /// <value>
        /// The simulator.
        /// </value>
        public override BaseSimulator Sim
        {
            get { return simulator; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is editing enable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is editing enable; otherwise, <c>false</c>.
        /// </value>
        public override bool IsEditingEnable
        {
            get { return Simulator.IsStartupComplete && !IsLooping && !IsPackaging; }
        }

        public IoPanelViewModel IoPanelViewModel { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="FusionSimulatorViewModel"/> class.
        /// </summary>
        /// <param name="sendNotification">The send notification function.</param>
        /// <param name="ioPanelViewModelModel">View model for the Io panel</param>
        /// <param name="label"></param>
        public FusionSimulatorViewModel(Func<string, object, string, string> sendNotification, IoPanelViewModel ioPanelViewModelModel)
        {
            logger = LogManager.GetLogFor("FusionSimulator");
            IsPackaging = false;
            IoPanelViewModel = ioPanelViewModelModel;

            Simulator = new FusionSimulator(IoPanelViewModel.Set, IoPanelViewModel.Get);

            Simulator.PropertyChanged += FusionSimulator_PropertyChanged; 
            startSCompletePackagingCommand = new RelayCommand(StartStopPackaging, obj => Simulator.IsStartupComplete && !IsLooping && ActiveDeterminedByStartOrCompleteCommand());
            outOfCorrugateCommand = new RelayCommand(OutOfCorrugate, obj => IsPackaging);
            lowAirCommand = new RelayCommand(LowAir, obj => IsSimulationStarted);
            failCommand = new RelayCommand(Fail, obj => IsPackaging);
            loopCompleteCommand = new RelayCommand(LoopComplete, obj => Simulator.IsStartupComplete && !IsPackaging);
            emergencyCommand = new RelayCommand(EmergencyStop, a => IsSimulationStarted);
            LoopTimeInSeconds = 6;
            
            SetupLoop();

            sendVariableNotification = sendNotification;
        }

        private bool ActiveDeterminedByStartOrCompleteCommand()
        {
            bool result = PackagingNextStep == "Start" ? simulator.HasItemsInQueue : simulator.CanComplete;
            Console.WriteLine("ActiveDeterminedByStartOrCompleteCommand: {0}", result);
            return result;
        }

        private void SetupLoop()
        {
            loopTask = new Task(() =>
            {
                while (IsLooping)
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    logger.Log(LogLevel.Trace, "Fusion {0} StartPackaging on thread {1}", Name, Thread.CurrentThread.ManagedThreadId);
                    simulator.StartPackaging();
                    
                    do
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(LoopTimeInSeconds));
                    }
                    while (!simulator.CanComplete);

                    logger.Log(LogLevel.Trace, "Fusion {0} CompletePackaging on thread {1}", Name, Thread.CurrentThread.ManagedThreadId);
                    simulator.CompletePackaging(PackagingStatus.Completed);
                    logger.Log(LogLevel.Trace, "Fusion {0} total time on thread {1}: {2}", Name, Thread.CurrentThread.ManagedThreadId, stopwatch.ElapsedMilliseconds);
                }
            }, tokenSource.Token);
            loopTask.ContinueWith((t) =>
            {
                if (t.IsFaulted)
                {
                    logger.Log(LogLevel.Error, "SetupLoop in FusionSimulatorViewModel has faulted!");

                    if (t.Exception != null)
                    {
                        logger.Log(LogLevel.Error, t.Exception.ToString());
                    }
                }
            });
        }

        /// <summary>
        /// Handles the PropertyChanged event of the FusionSimulator control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void FusionSimulator_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsStartupComplete")
            {
                OnPropertyChanged("IsEditingEnable");
            }
        }

        /// <summary>
        /// Emergencies the stop.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void EmergencyStop(object obj)
        {
            simulator.EmergencyStop();

            if (simulator.IsEmergencyStopEngaged)
                IoPanelViewModel.EstopPressed();
        }

        /// <summary>
        /// Loops the complete.
        /// </summary>
        /// <param name="obj">The object.</param>
        public override void LoopComplete(object obj = null)
        {
            if (!IsLooping && Simulator.IsStartupComplete && !IsPackaging)
            {
                SetupLoop();
                loopTask.Start();
            }

            IsLooping = !IsLooping;
        }
        /// <summary>
        /// Outs the of corrugate.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void OutOfCorrugate(object obj = null)
        {
            simulator.CompletePackaging(PackagingStatus.OutOfFanfold);
            simulator.SetOutOfCorrugate();
            IsPackaging = false;
        }

        
        public void LowAir(object obj = null)
        {
            simulator.SetLowAir();
            if (IsPackaging)
            {
                simulator.CompletePackaging(PackagingStatus.Failure);
                IsPackaging = false;
            }
        }

        /// <summary>
        /// Fails the specified object.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void Fail(object obj = null)
        {
            simulator.CompletePackaging(PackagingStatus.Failure);
            IsPackaging = false;
        }

        /// <summary>
        /// Starts the stop packaging.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void StartStopPackaging(object obj = null)
        {
            if (!IsPackaging)
            {
                simulator.StartPackaging();
            }
            else
            {
                simulator.CompletePackaging(PackagingStatus.Completed);
            }

            IsPackaging = !IsPackaging;
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public override void Start()
        {
            MachineUri = new Uri(string.Format("http://{0}:{1}", ip, port));

            var hostHeader = Networking.ParseHostName(MachineUri.Host + ":" + MachineUri.Port);

            var service = new PacksizeCutCreaseMachineWebService(simulator);
            host = new WebServiceHost(service, MachineUri);

            endPoint = host.AddServiceEndpoint(typeof(IFusionWebService), new WebHttpBinding(), string.Empty);

            simulator.Initialize(HttpUtility.UrlEncode(hostHeader.ToString()), sendVariableNotification);

            host.Open();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public override void Stop()
        {
            ClearQueue();
            Simulator.IsStartupComplete = false;

            if (!loopTask.IsCompleted && loopTask.Status == TaskStatus.Running)
            {
                IsLooping = false;
                Thread.Sleep(TimeSpan.FromSeconds(LoopTimeInSeconds));
            }

            tokenSource.Cancel();
            Thread.Sleep(TimeSpan.FromSeconds(LoopTimeInSeconds));
            tokenSource = new CancellationTokenSource();

            if (host != null && host.State == CommunicationState.Opened)
            {
                host.Close();
            }
        }

        /// <summary>
        /// Startups the complete.
        /// </summary>
        /// <param name="obj">The object.</param>
        public override void StartupComplete(object obj = null)
        {
            IsPackaging = false;
            simulator.StartUpComplete();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                Stop();
                if (loopTask != null)
                {
                    loopTask.Dispose();
                }
            }

        }

        public void ClearQueue()
        {
            simulator.Set(FusionCommands.ClearQueue, "1");
        }
    }
}
