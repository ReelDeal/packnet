using PackNet.FusionSim.Models;

namespace PackNet.FusionSim.ViewModels
{
    public class NullSimulatorViewModel : Simulator
    {
        public override BaseSimulator Sim
        {
            get { return null; }
        }

        public override bool IsEditingEnable
        {
            get { return false; }
        }

        public override void Start()
        {
        }

        public override void Stop()
        {
        }

        public override void StartupComplete(object obj = null)
        {
        }

        public override void LoopComplete(object obj = null)
        {
        }
    }
}