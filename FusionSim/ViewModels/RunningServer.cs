﻿using Newtonsoft.Json;

using PackNet.Common.Communication.RabbitMQ;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;
using PackNet.Communication.Communicators;
using PackNet.FusionSim.Communication;
using PackNet.FusionSim.Utilities;
using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;

namespace PackNet.FusionSim.ViewModels
{
    public class RunningServer : NotifyPropertyChangedBase, IService
    {
        private RabbitConnectionManager rabbitConnectionManager = null;
        private string ipAddress;
        private Uri address;
        private bool isReady;
        private Task checkIfServerIsAlive;
        private bool isServerAlive;
        private readonly int checkIfServerIsAliveLoopSeconds;
        private readonly Encoding encoding;
        private readonly Action<string, string> getMachines;
        private readonly ILogger logger;
        private string routingKey;

        /// <summary>
        /// Gets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public Uri Address
        {
            get { return address; }
        }

        /// <summary>
        /// Gets or sets the ip address.
        /// </summary>
        /// <value>
        /// The ip address.
        /// </value>
        public string IpAddress
        {
            get { return ipAddress; }
            set
            {
                if (value == ipAddress)
                {
                    return;
                }

                if (TrySetServerAddress(value))
                {
                    ipAddress = value;
                    OnPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is ready.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is ready; otherwise, <c>false</c>.
        /// </value>
        public bool IsReady
        {
            get { return isReady; }
            set
            {
                if (value == isReady)
                {
                    return;
                }
                isReady = value;
                OnPropertyChanged();
                OnPropertyChanged("IfReadyShow");
            }
        }

        public Visibility IfReadyShow
        {
            get { return IsReady ? Visibility.Visible : Visibility.Hidden; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RunningServer"/> class.
        /// </summary>
        /// <param name="getMachines">The get machines callback.</param>
        public RunningServer(Action<string, string> getMachines)
        {
            logger = LogManager.GetLogFor("FusionSimulator");
            routingKey = Guid.NewGuid().ToString();
            this.getMachines = getMachines;
            IsReady = false;
            encoding = Encoding.GetEncoding(0);
            checkIfServerIsAliveLoopSeconds = int.Parse(Properties.Settings.Default.SecondsToWaitToCheckIfServerIsAlive);

            rabbitConnectionManager = new RabbitConnectionManager();
            var observable = rabbitConnectionManager.GetMessageObservable(routingKey);
            observable.Item1.Subscribe(s =>
            {
                try
                {
                    var message = JsonConvert.DeserializeObject<Message>(s);
                    getMachines(message.MessageType.DisplayName, s);
                }
                catch (Exception ex)
                {
                    logger.Log(LogLevel.Error, "Exception caught on Running Server, Deserialize Object. "+ ex.Message);
                }
            });
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            if (!IsReady)
            {
                checkIfServerIsAlive = Task.Factory.StartNew(() =>
                {
                    while (!isServerAlive)
                    {
                        Send(MessageTypes.ConnectionAlive);
                        Thread.Sleep(TimeSpan.FromSeconds(checkIfServerIsAliveLoopSeconds));
                    }
                });
                checkIfServerIsAlive.ContinueWith((t) =>
                {
                    if (t.IsFaulted)
                    {
                        logger.Log(LogLevel.Error, "Start in RunningServer has faulted!");

                        if (t.Exception != null)
                        {
                            logger.Log(LogLevel.Error, "Start in RunningServer has faulted: {0}", t.Exception.ToString());
                        }
                    }
                });
            }
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            IsReady = true;

            if (checkIfServerIsAlive != null)
            {
                if (!checkIfServerIsAlive.IsCompleted)
                {
                    isServerAlive = true;
                    Thread.Sleep(TimeSpan.FromSeconds(checkIfServerIsAliveLoopSeconds));
                }

                checkIfServerIsAlive.Dispose();
            }

            rabbitConnectionManager.Dispose();

            IsReady = false;
        }

        /// <summary>
        /// Tries the set server address.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        private bool TrySetServerAddress(string uri)
        {
            if (Uri.TryCreate(uri + ":9998", UriKind.Absolute, out address))
            {
                return true;
            }

            return Uri.TryCreate("http://" + uri + ":9998", UriKind.RelativeOrAbsolute, out address);
        }

        /// <summary>
        /// Gets the simulators.
        /// </summary>
        public void GetSimulators()
        {
            if (!IsReady)
            {
                isServerAlive = true;
                Thread.Sleep(TimeSpan.FromSeconds(checkIfServerIsAliveLoopSeconds));
                Thread.Sleep(TimeSpan.FromSeconds(checkIfServerIsAliveLoopSeconds));
                
                checkIfServerIsAlive.Dispose();
                Send(MachineMessages.GetMachines);
            }
        }

        private void Send(MessageTypes msg)
        {
            var message = new Message<string>()
            {
                MessageType = msg,
                Data = string.Empty,
                ReplyTo = routingKey,
                MachineGroupName = "",
                UserName = ""
            };
            rabbitConnectionManager.SendToServer(message);
        }

        public void CheckIfAlive()
        {
            Send(MessageTypes.ConnectionAlive);
        }

        /// <summary>
        /// Sends the variable notification.
        /// </summary>
        /// <param name="varName">Name of the variable.</param>
        /// <param name="value">The value.</param>
        /// <param name="machineAddressPort">The machine address with port.</param>
        /// <returns></returns>
        public string SendVariableNotification(string varName, object value, string machineAddressPort)
        {
            var result = string.Empty;
            var data = "";
            try
            {
                var addressRequest = Address.AbsoluteUri + "post";
                var request = (HttpWebRequest)WebRequest.Create(addressRequest);
                request.Timeout = 60000;
                request.Method = "POST";
                request.Headers.Add("MachineAddressPort", machineAddressPort);

                data = SerializeData(varName, value);
                byte[] loginDataBytes = encoding.GetBytes(data);
                request.ContentLength = loginDataBytes.Length;
                using (var stream = request.GetRequestStream())
                {
                    stream.Write(loginDataBytes, 0, loginDataBytes.Length);

                    using (var response = (HttpWebResponse)request.GetResponse())
                    {
                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            result += string.Format("!!!!!: {0}", response.StatusCode);
                        }

                        response.Close();
                    }
                }
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, exception.ToString());
                throw;
                //result += string.Format("Error Sending {0}. Exception: {1}", data, exception.Message);
            }
            return result != string.Empty ? result : string.Format("{0}>>EVENT: {1}", Environment.NewLine, data);
        }

        /// <summary>
        /// Serializes the data.
        /// </summary>
        /// <param name="varName">Name of the variable.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private static string SerializeData(string varName, object value)
        {
            // The real fusion handles these as integer arrays so we need to report on them as such
            if (varName == PacksizeBaseMachineVariableMap.Instance.StartedProductionItemId.ToString() ||
                varName == PacksizeBaseMachineVariableMap.Instance.ProductionHistoryItemId.ToString() ||
                varName == PacksizeBaseMachineVariableMap.Instance.RemovedProductionItemId1.ToString() ||
                varName == PacksizeBaseMachineVariableMap.Instance.RemovedProductionItemId2.ToString())
            {
                var guid = (Guid)value;
                return string.Format("{{\"{0}\":{1}}}", varName,
                    JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(guid)));
            }

            return string.Format("{{\"{0}\":{1}}}", varName, JsonConvert.SerializeObject(value));
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~RunningServer()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }

        // The bulk of the clean-up code is implemented in Dispose(bool)
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
             //   Stop();
            }
        }
    }
}