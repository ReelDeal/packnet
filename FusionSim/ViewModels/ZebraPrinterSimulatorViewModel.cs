﻿using PackNet.Common.Communication;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.FusionSim.Models;
using PackNet.FusionSim.Utilities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PackNet.FusionSim.ViewModels
{
    public class ZebraPrinterSimulatorViewModel:Simulator
    {
        private readonly ICommand getHostStatusCommand;
        private readonly ICommand startCompletePrintingCommand;
        private readonly ICommand headOpenCommand ;
        private readonly ICommand takeLabelCommand ;
        private readonly ICommand outOfRibbonCommand;

        private TcpListener listener;
        private List<Socket> sockets;
        private bool listen;
        private readonly ILogger logger;

        /// <summary>
        /// Gets the head open command.
        /// </summary>
        /// <value>
        /// The head open command.
        /// </value>
        public ICommand HeadOpenCommand
        {
            get { return headOpenCommand; }
        }
        /// <summary>
        /// Gets the take label command.
        /// </summary>
        /// <value>
        /// The take label command.
        /// </value>
        public ICommand TakeLabelCommand
        {
            get { return takeLabelCommand; }
        }

        /// <summary>
        /// Gets the out of ribbon command.
        /// </summary>
        /// <value>
        /// The out of ribbon command.
        /// </value>
        public ICommand OutOfRibbonCommand
        {
            get { return outOfRibbonCommand; }
        }

        /// <summary>
        /// Gets the get host status command.
        /// </summary>
        /// <value>
        /// The get host status command.
        /// </value>
        public ICommand GetHostStatusCommand
        {
            get { return getHostStatusCommand; }
        }

        /// <summary>
        /// Gets the start complete printing command.
        /// </summary>
        /// <value>
        /// The start complete printing command.
        /// </value>
        public ICommand StartCompletePrintingCommand
        {
            get { return startCompletePrintingCommand; }
        }

        /// <summary>
        /// Gets or sets the zebra printer.
        /// </summary>
        /// <value>
        /// The zebra printer.
        /// </value>
        public ZebraPrinterSimulator ZebraPrinter { get; set; }

        /// <summary>
        /// Gets the simulator.
        /// </summary>
        /// <value>
        /// The simulator.
        /// </value>
        public override BaseSimulator Sim
        {
            get { return ZebraPrinter; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is editing enable.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is editing enable; otherwise, <c>false</c>.
        /// </value>
        public override bool IsEditingEnable
        {
            get { return IsSimulationStarted && !IsLooping && !ZebraPrinter.IsPrinting; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZebraPrinterSimulatorViewModel"/> class.
        /// </summary>
        /// <param name="alias"></param>
        public ZebraPrinterSimulatorViewModel(string alias)
        {
            logger = LogManager.GetLogFor("FusionSimulator");

            ZebraPrinter = new ZebraPrinterSimulator(alias);
            getHostStatusCommand = new RelayCommand(GetHostStatus, obj => IsSimulationStarted);
            startCompletePrintingCommand = new RelayCommand(StartCompletePrinting, obj => ZebraPrinter.IsPrinting);
            headOpenCommand = new RelayCommand(HeadOpen, obj => IsSimulationStarted);
            takeLabelCommand = new RelayCommand(TakeLabel, obj => IsSimulationStarted && ZebraPrinter.HasLabelsInQueue);
            outOfRibbonCommand = new RelayCommand(OutOfRibbon, obj => IsSimulationStarted);
            loopCompleteCommand = new RelayCommand(LoopComplete, obj => IsSimulationStarted && !ZebraPrinter.IsPrinting && !ZebraPrinter.HasLabelsInQueue);

            SetupLoop();
        }

        private void SetupLoop()
        {
            loopTask = new Task(() =>
            {
                while (IsLooping)
                {
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    Thread.Sleep(TimeSpan.FromSeconds(LoopTimeInSeconds));
                    string templateInfo = ZebraPrinter.Template;

                    if (ZebraPrinter.IsPrinting)
                    {
                        logger.Log(LogLevel.Trace, "Printer {0} CompletePrinting on thread {1} payload {2}", Name, Thread.CurrentThread.ManagedThreadId, templateInfo);
                        
                        ZebraPrinter.CompletePrinting();

                        if (ZebraPrinter.HasLabelsInQueue)
                        {
                            Thread.Sleep(TimeSpan.FromSeconds(LoopTimeInSeconds));
                            logger.Log(LogLevel.Trace, "Printer {0} HasLabelsInQueue on thread {1} payload {2}", Name, Thread.CurrentThread.ManagedThreadId, templateInfo);
                            ZebraPrinter.TakeLabel();
                        }
                    }

                    logger.Log(LogLevel.Trace, "Printer {0} total time on thread {1}: {2} payload {3}", Name, Thread.CurrentThread.ManagedThreadId, stopwatch.ElapsedMilliseconds, templateInfo);
                }
            }, tokenSource.Token);
        }

        /// <summary>
        /// Accepts the socket.
        /// </summary>
        /// <returns></returns>
        public ISocketWrapper AcceptSocket()
        {
            Console.WriteLine("AcceptSocket");
            
            try
            {
                var socket = listener.AcceptSocket();
                sockets.Add(socket);
                logger.Log(LogLevel.Debug, "Printer " + Name + " Socket accepted: " + socket.RemoteEndPoint);
                return new SocketWrapper(socket);
            }
            catch (SocketException socketException)
            {
                logger.Log(LogLevel.Error, "AcceptSocket Error: {0}", socketException.ToString());
                throw;
                //This only occurs when we are closing the sim
            }
        }

        /// <summary>
        /// Loops the complete.
        /// </summary>
        /// <param name="obj">The object.</param>
        public override void LoopComplete(object obj = null)
        {
            if (!IsLooping && IsSimulationStarted && !ZebraPrinter.IsPrinting && !ZebraPrinter.HasLabelsInQueue)
            {
                SetupLoop();
                loopTask.Start();
            }

            IsLooping = !IsLooping;
        }

        /// <summary>
        /// Outs the of ribbon.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void OutOfRibbon(object obj)
        {
            ZebraPrinter.PaperOutStatus = !ZebraPrinter.PaperOutStatus;
        }

        /// <summary>
        /// Takes the label.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void TakeLabel(object obj = null)
        {
            ZebraPrinter.TakeLabel();
        }

        /// <summary>
        /// Heads the open.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void HeadOpen(object obj)
        {
            ZebraPrinter.HeadOpenStatus = !ZebraPrinter.HeadOpenStatus;
        }

        /// <summary>
        /// Starts the complete printing.
        /// </summary>
        /// <param name="obj">The object.</param>
        public void StartCompletePrinting(object obj = null)
        {
            ZebraPrinter.CompletePrinting();
        }

        /// <summary>
        /// Gets the host status.
        /// </summary>
        /// <param name="obj">The object.</param>
        private void GetHostStatus(object obj)
        {
            ZebraPrinter.AddDetails(Environment.NewLine + ZebraPrinter.GetHostStatusString());
        }

        /// <summary>
        /// Clients the listener.
        /// </summary>
        private void ClientListener()
        {
            //This while loop must be here to support external print. Do not remove it.
            while (listen)
            {
                ISocketWrapper socket = AcceptSocket();
                Task clientCommsTask = Task.Factory.StartNew(() =>
                {
                    HandleClientCommunication(socket);
                });
                clientCommsTask.ContinueWith((t) =>
                {
                    if (t.IsFaulted)
                    {
                        logger.Log(LogLevel.Error, "ClientListener in ZebraPrinterSimulatorViewModel has faulted!");
                        
                        if (t.Exception != null)
                        {
                            logger.Log(LogLevel.Error, "ClientListener in ZebraPrinterSimulatorViewModel has faulted: {0}", t.Exception.ToString());
                        }
                    }
                });
            }
        }

        private volatile object threadLocker = new object();

        /// <summary>
        /// Handles the client communication.
        /// </summary>
        /// <param name="socket">The socket.</param>
        private void HandleClientCommunication(ISocketWrapper socket)
        {
            string message = string.Empty;

            while (listen)
            {
                try
                {
                    var byteAnswer = new byte[1024];

                    if (socket == null || socket.IsConnected == false)
                    {
                        listen = false;
                        return;
                    }

                    socket.Receive(byteAnswer);
                    message += Encoding.UTF8.GetString(byteAnswer).Trim('\0');

                    do
                    {
                        if (message.StartsWith(ZebraPrinterCommands.HostStatusCommand))
                        {
                            socket.Send(Encoding.UTF8.GetBytes(ZebraPrinter.GetHostStatusString()));
                        }

                        if (!string.IsNullOrWhiteSpace(message))
                        {
                            message = ZebraPrinter.ProcessMessage(message);
                        }
                    }
                    while (!string.IsNullOrWhiteSpace(message));

                }
                catch (Exception exception)
                {
                    logger.Log(LogLevel.Error, "To: {0}", exception.ToString());
                    socket.Dispose();
                    return;
                }
                Thread.Sleep(100);
            }

            if (socket != null)
            {
                socket.Dispose();
            }
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public override void Start()
        {
            MachineUri = new Uri(string.Format("net.tcp://{0}:{1}", ip, port));
            listener = new TcpListener(IPAddress.Parse(Ip), port);
            sockets = new List<Socket>();
            listener.Start();
            listen = true;
            Task task = Task.Factory.StartNew(ClientListener);
            task.ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    logger.Log(LogLevel.Error, "Start in ZebraPrinterSimulatorViewModel has faulted!");

                    if (t.Exception != null)
                    {
                        logger.Log(LogLevel.Error, "Start in ZebraPrinterSimulatorViewModel has faulted: {0}", t.Exception.ToString());
                    }
                }
            });

            StartupComplete();
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public override void Stop()
        {
            if (sockets != null)
            {
                foreach (var socket in sockets.Where(socket => socket.Connected))
                {
                    socket.Close();
                }
                sockets.Clear();
            }

            if (listener != null)
            {
                listener.Stop();
            }

        }

        /// <summary>
        /// Startups the complete.
        /// </summary>
        /// <param name="obj">The object.</param>
        public override void StartupComplete(object obj = null)
        {
            ZebraPrinter.IsStartupComplete = true;
        }

        public void ClearQueue()
        {
            ZebraPrinter.TakeLabel();
            ZebraPrinter.TakeLabel();
            ZebraPrinter.TakeLabel();
            ZebraPrinter.HasLabelsInQueue = false;
        }
    }
}
