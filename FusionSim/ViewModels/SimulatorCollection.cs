﻿using PackNet.FusionSim.Utilities;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Threading;

namespace PackNet.FusionSim.ViewModels
{
    public class SimulatorCollection : NotifyPropertyChangedBase, IService
    {
        private readonly Dispatcher dispatcher;
        private readonly Func<string, object, string, string> sendNotificationCallback;
        private ObservableCollection<Simulator> simulators;
        private Simulator selectedSimulator;
        
        /// <summary>
        /// Gets or sets the sims.
        /// </summary>
        /// <value>
        /// The sims.
        /// </value>
        public ObservableCollection<Simulator> Sims
        {
            get { return simulators; }
            set
            {
                if (Equals(value, simulators))
                {
                    return;
                }
                simulators = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the selected simulator.
        /// </summary>
        /// <value>
        /// The selected simulator.
        /// </value>
        public Simulator SelectedSimulator
        {
            get { return selectedSimulator; }
            set
            {
                if (Equals(value, selectedSimulator))
                {
                    return;
                }
                selectedSimulator = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimulatorCollection"/> class.
        /// </summary>
        public SimulatorCollection()
        {
            dispatcher = RootDispatcherFetcher.RootDispatcher;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SimulatorCollection"/> class.
        /// </summary>
        public SimulatorCollection(Func<string, object, string, string> sendNotification)
        {
        
            Sims = new ObservableCollection<Simulator>();
            sendNotificationCallback = sendNotification;
        }

        /// <summary>
        /// Adds the specified sim.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="port">The port.</param>
        /// <returns></returns>
        /// 
        public FusionSimulatorViewModel AddFusion(string name, string ipAddress, int port)
        {
            return AddFusion(name, ipAddress, port, Guid.NewGuid());
        }

        public FusionSimulatorViewModel AddFusion(string name, string ipAddress, int port, Guid machineId)

        {
            var sim = new FusionSimulatorViewModel(sendNotificationCallback, new IoPanelViewModel(new SimController.MachineObject()
            {
                Alias = name,
                IpOrDnsName = ipAddress,
                Port = port,
                Type = MachineType.Fusion,
                MachineId = machineId
            }, sendNotificationCallback))
            {
                Name = name,
                Port = port,
                Ip = ipAddress
            };

            Sims.Add(sim);

            if (SelectedSimulator == null)
            {
                SelectedSimulator = Sims[0];
            }

            return sim;
        }

        public EmSimulatorViewModel AddEm(string name, string ipAddress, int port)
        {
            return AddEm(name, ipAddress, port, Guid.NewGuid());
        }

        public EmSimulatorViewModel AddEm(string name, string ipAddress, int port, Guid machineId)
        {
            var sim = new EmSimulatorViewModel(sendNotificationCallback, new IoPanelViewModel(new SimController.MachineObject()
            {
                Alias = name,
                IpOrDnsName = ipAddress,
                Port = port,
                Type = MachineType.Em,
                MachineId = machineId
            }, sendNotificationCallback))
            {
                Name = name,
                Port = port,
                Ip = ipAddress
            };

            Sims.Add(sim);

            if (SelectedSimulator == null)
            {
                SelectedSimulator = Sims[0];
            }

            return sim;
        }

        /// <summary>
        /// Adds the printer.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="ipAddress">The ip address.</param>
        /// <param name="port">The port.</param>
        /// <returns>The created printer.</returns>
        public ZebraPrinterSimulatorViewModel AddPrinter(string name, string ipAddress, int port)
        {
            var sim = new ZebraPrinterSimulatorViewModel(name)
            {
                Name = name,
                Port = port,
                Ip = ipAddress
            };

            Sims.Add(sim);

            if (SelectedSimulator == null)
            {
                SelectedSimulator = Sims[0];
            }

            return sim;
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        public void Stop()
        {
            simulators.AsParallel().ForAll(simulator => simulator.Stop());
        }

        /// <summary>
        /// Clears this instance.
        /// </summary>
        public void Clear()
        {
            SelectedSimulator = null;
            Stop();
            dispatcher.Invoke(() => Sims.Clear());
        }

        public void Dispose()
        {
            foreach (var simulator in simulators)
            {
                simulator.Dispose();
            }
        }
    }
}