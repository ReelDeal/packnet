﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Helpers;

using Newtonsoft.Json;

using PackNet.Common.Interfaces;
using PackNet.FusionSim.Utilities;
using PackNet.IODeviceService.Communication;
using PackNet.IODeviceService.DTO;
using PackNet.IODeviceService.Intefaces;

namespace PackNet.FusionSim.ViewModels
{
    public class IoPanelViewModel : NotifyPropertyChangedBase
    {
        private readonly IoDeviceVariableMap variableMap = IoDeviceVariableMap.Instance;

        private readonly Dictionary<int, InputTypes> registeredInputListeners = new Dictionary<int, InputTypes>();
        private readonly List<Thread> ongoingOutputThreads = new List<Thread>();

        private bool output1;
        private bool output2;
        private bool output3;
        private bool output4;
        private bool output5;
        private bool output6;
        private bool output7;
        private bool output8;
        private bool output9;
        private bool output10;
        private bool output11;
        private bool output12;
        
        private bool input1;
        private bool input2;
        private bool input3;
        private bool input4;
        private bool input5;
        private bool input6;
        private bool input7;
        private bool input8;
        private bool input9;
        private bool input10;
        private bool input11;
        private bool input12;


        private int outputNr, t2;//T1
        private OutputTypes outputType;
        
        private int inputNr;

        private InputTypes inputType;

        private readonly SimController.MachineObject machine;
        private readonly Func<string, object, string, string> sendNotification;

        public IoPanelViewModel(SimController.MachineObject machine, Func<string, object, string, string> sendNotification)
        {
            this.machine = machine;
            this.sendNotification = sendNotification;
        }

        #region Outputs
        public bool Output1
        {
            get { return output1; }
            set
            {
                if (value.Equals(output1))
                    return;

                output1 = value;
                OnPropertyChanged();
            }
        }

        public bool Output2
        {
            get { return output2; }
            set
            {
                if (value.Equals(output2))
                    return;

                output2 = value;
                OnPropertyChanged();
            }
        }

        public bool Output3
        {
            get { return output3; }
            set
            {
                if (value.Equals(output3))
                    return;

                output3 = value;
                OnPropertyChanged();
            }
        }

        public bool Output4
        {
            get { return output4; }
            set
            {
                if (value.Equals(output4))
                    return;

                output4 = value;
                OnPropertyChanged();
            }
        }

        public bool Output5
        {
            get { return output5; }
            set
            {
                if (value.Equals(output5))
                    return;

                output5 = value;
                OnPropertyChanged();
            }
        }

        public bool Output6
        {
            get { return output6; }
            set
            {
                if (value.Equals(output6))
                    return;

                output6 = value;
                OnPropertyChanged();
            }
        }

        public bool Output7
        {
            get { return output7; }
            set
            {
                if (value.Equals(output7))
                    return;

                output7 = value;
                OnPropertyChanged();
            }
        }

        public bool Output8
        {
            get { return output8; }
            set
            {
                if (value.Equals(output8))
                    return;

                output8 = value;
                OnPropertyChanged();
            }
        }

        public bool Output9
        {
            get { return output9; }
            set
            {
                if (value.Equals(output9))
                    return;

                output9 = value;
                OnPropertyChanged();
            }
        }

        public bool Output10
        {
            get { return output10; }
            set
            {
                if (value.Equals(output10))
                    return;

                output10 = value;
                OnPropertyChanged();
            }
        }

        public bool Output11
        {
            get { return output11; }
            set
            {
                if (value.Equals(output11))
                    return;

                output11 = value;
                OnPropertyChanged();
            }
        }

        public bool Output12
        {
            get { return output12; }
            set
            {
                if (value.Equals(output12))
                    return;

                output12 = value;
                OnPropertyChanged();
            }
        }

        #endregion

        #region Inputs
        public bool Input1
        {
            get { return input1; }
            set
            {
                if (value.Equals(input1))
                    return;

                if(registeredInputListeners.ContainsKey(1))
                    SendIOInput(1, input1, registeredInputListeners[1]);

                input1 = value;
                OnPropertyChanged();
            }
        }

        public bool Input2
        {
            get { return input2; }
            set
            {
                if (value.Equals(input2))
                    return;

                if (registeredInputListeners.ContainsKey(2))
                    SendIOInput(2, Input2, registeredInputListeners[2]);

                input2 = value;
                OnPropertyChanged();
            }
        }

        public bool Input3
        {
            get { return input3; }
            set
            {
                if (value.Equals(input3))
                    return;

                if (registeredInputListeners.ContainsKey(3))
                    SendIOInput(3, input3, registeredInputListeners[3]);

                input3 = value;
                OnPropertyChanged();
            }
        }

        public bool Input4
        {
            get { return input4; }
            set
            {
                if (value.Equals(input4))
                    return;

                if (registeredInputListeners.ContainsKey(4))
                    SendIOInput(4, input4, registeredInputListeners[4]);

                input4 = value;
                OnPropertyChanged();
            }
        }

        public bool Input5
        {
            get { return input5; }
            set
            {
                if (value.Equals(input5))
                    return;

                if (registeredInputListeners.ContainsKey(5))
                    SendIOInput(5, input5, registeredInputListeners[5]);

                input5 = value;
                OnPropertyChanged();
            }
        }

        public bool Input6
        {
            get { return input6; }
            set
            {
                if (value.Equals(input6))
                    return;

                if (registeredInputListeners.ContainsKey(6))
                    SendIOInput(6, Input6, registeredInputListeners[6]);

                input6 = value;
                OnPropertyChanged();
            }
        }

        public bool Input7
        {
            get { return input7; }
            set
            {
                if (value.Equals(input7))
                    return;

                if (registeredInputListeners.ContainsKey(7))
                    SendIOInput(7, Input7, registeredInputListeners[7]);

                input7 = value;
                OnPropertyChanged();
            }
        }

        public bool Input8
        {
            get { return input8; }
            set
            {
                if (value.Equals(input8))
                    return;

                if (registeredInputListeners.ContainsKey(8))
                    SendIOInput(8, Input8, registeredInputListeners[8]);

                input8 = value;
                OnPropertyChanged();
            }
        }

        public bool Input9
        {
            get { return input9; }
            set
            {
                if (value.Equals(input9))
                    return;

                if (registeredInputListeners.ContainsKey(9))
                    SendIOInput(9, Input9, registeredInputListeners[9]);

                input9 = value;
                OnPropertyChanged();
            }
        }

        public bool Input10
        {
            get { return input10; }
            set
            {
                if (value.Equals(input10))
                    return;

                if (registeredInputListeners.ContainsKey(10))
                    SendIOInput(10, Input10, registeredInputListeners[10]);

                input10 = value;
                OnPropertyChanged();
            }
        }

        public bool Input11
        {
            get { return input11; }
            set
            {
                if (value.Equals(input11))
                    return;

                if (registeredInputListeners.ContainsKey(11))
                    SendIOInput(11, Input11, registeredInputListeners[11]);

                input11 = value;
                OnPropertyChanged();
            }
        }

        public bool Input12
        {
            get { return input12; }
            set
            {
                if (value.Equals(input12))
                    return;

                if (registeredInputListeners.ContainsKey(12))
                    SendIOInput(12, Input12, registeredInputListeners[12]);

                input12 = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public void Set(string varName, object value)
        {
            if (varName == variableMap.OutputCommand.ToString())
            {
                var command = JsonConvert.DeserializeObject<OutputCommandTrigger>(value.ToString());

                outputType = command.Command.Type;
                outputNr = command.Command.OutputIndex;
                t2 = command.Command.T2;

                if(command.Execute)
                    HandleOutPutCommand();
            }
            
            if (varName == variableMap.InputCommandInputIndex.ToString())
                inputNr = int.Parse(value.ToString());

            if (varName == variableMap.InputCommandType.ToString())
            {
                Enum.TryParse(value.ToString(), true, out inputType);
            }

            if (varName == variableMap.OutputCommandExecute.ToString())
                HandleOutPutCommand();

            if (varName == variableMap.InputCommandExecute.ToString())
                HandleInputCommand();
        }

        private void HandleInputCommand()
        {
            if (inputType == InputTypes.RemoveMultipleEdge)
            {
                if (registeredInputListeners.ContainsKey(inputNr + 1))
                    registeredInputListeners.Remove(inputNr + 1);
                return;
            }
                

            if(registeredInputListeners.ContainsKey(inputNr + 1) == false)
                registeredInputListeners.Add(inputNr + 1, inputType);
            else
            {
                registeredInputListeners[inputNr + 1] = inputType;
            }
        }

        private void HandleOutPutCommand()
        {
            switch (outputType)
            {
                case OutputTypes.Enable:
                    EnableOutput(outputNr);
                    break;
                case OutputTypes.Disable:
                    DisableOutput(outputNr);
                    break;
                case OutputTypes.TemporaryEnable:
                    TemporaryEnable(outputNr, TimeSpan.FromMilliseconds(t2));
                    break;
                case OutputTypes.TemporaryDisable:
                    TemporaryDisable(outputNr, TimeSpan.FromMilliseconds(t2));
                    break;
            }
        }

        public void EstopPressed()
        {
            ongoingOutputThreads.ForEach(t => t.Abort());
            GetType().GetProperties().Where(p => p.Name.Contains("Output")).ForEach(p => p.SetValue(this, false));
        }

        public virtual object Get(string variable, bool internalCheck = false)
        {
            if (variable == variableMap.InputCommandExecute.ToString() || variable == variableMap.OutputCommandExecute.ToString())
                return 0;
            return "";
        }
        

        private void SendIOInput(int port, bool oldValue, InputTypes type)
        {
            if ((type == InputTypes.MultiplePositiveEdge || type == InputTypes.SinglePositiveEdge) && oldValue == true)
                return;

            if ((type == InputTypes.MultipleNegativeEdge || type == InputTypes.SingleNegativeEdge) && oldValue == false)
                return;

            var eventType = type == InputTypes.MultiplePositiveEdge || type == InputTypes.SinglePositiveEdge
                ? InputEventTypes.PositiveEdge
                : InputEventTypes.NegativeEdge;

            var json = JsonConvert.SerializeObject(new InputEvent(machine.MachineId, 0, port - 1, eventType));

            sendNotification(variableMap.InputEventMessageContainer.ToString(), json, machine.IpOrDnsName + ":" + machine.Port);

            if (type == InputTypes.SingleNegativeEdge || type == InputTypes.SinglePositiveEdge)
                registeredInputListeners.Remove(port);
        }
        
        public void EnableOutput(int index)
        {
            var prop = GetType().GetProperties().FirstOrDefault(p => p.Name.Contains("Output" + (index + 1)));
            prop.SetValue(this, true);
        }

        public void DisableOutput(int index)
        {
            var prop = GetType().GetProperties().FirstOrDefault(p => p.Name.Contains("Output" + (index + 1)));
            prop.SetValue(this, false);
        }

        public void TemporaryEnable(int index, TimeSpan time)
        {
            var prop = GetType().GetProperties().FirstOrDefault(p => p.Name.Contains("Output" + (index + 1)));
            prop.SetValue(this, true);

            var thread = new Thread(() =>
            {
                Thread.Sleep(time);
                prop.SetValue(this, false);
            });

            ongoingOutputThreads.Add(thread);
            
            thread.Start();
        }

        public void TemporaryDisable(int index, TimeSpan time)
        {
            var prop = GetType().GetProperties().FirstOrDefault(p => p.Name.Contains("Output" + (index + 1)));
            prop.SetValue(this, false);

            var thread = new Thread(() =>
            {
                Thread.Sleep(time);
                prop.SetValue(this, true);
            });
            
            ongoingOutputThreads.Add(thread);
            
            thread.Start();
        }
    }
}
