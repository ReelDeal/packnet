﻿using System.Windows;

namespace PackNet.FusionSim
{
    /// <summary>
    /// Interaction logic for AddSimulator.xaml
    /// </summary>
    public partial class AddSimulator
    {
        public AddSimulator()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
