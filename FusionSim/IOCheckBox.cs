﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace PackNet.FusionSim
{
    //Never allow the user to click into a false state
    public class IOCheckBox : CheckBox
    {
        protected override void OnToggle()
        {
            if (IsChecked == false) //currently in output mode 
            {
                this.IsChecked = true; // Go to input mode
            }
            else if (!IsChecked.HasValue) //currently in off
            {
                this.IsChecked = true; // Go to input mode
            }
            else if (IsChecked == true) //currently in input mode
            {
                this.IsChecked = null; // go to off
            }
        }
    }
}
