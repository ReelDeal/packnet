﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Communication.Communicators.EM;
using PackNet.FusionSim.Utilities;
using PackNet.IODeviceService.Communication;

namespace PackNet.FusionSim.Models
{
    public class EmSimulator : BaseCutCreaseMachineSimulator
    {
        private bool isInProductionRestartableError;
        private readonly Action<string, object> ioModelSet;
        private readonly Func<string, bool, object> ioModelGet;


        /// <summary>
		/// Initializes a new instance of the <see cref="EmSimulator"/> class.
		/// </summary>
        public EmSimulator(Action<string, object> ioModelSet, Func<string, bool, object> ioModelGet)
            : base(EmMachineVariables.Instance)
        {
            this.ioModelSet = ioModelSet;
            this.ioModelGet = ioModelGet;
        }

        public bool IsInProductionRestartableError
        {
            get { return isInProductionRestartableError; }
            set
            {
                if (value == isInProductionRestartableError)
                    return;

                isInProductionRestartableError = value;
                CanComplete = !isInProductionRestartableError;

                ErrorMessage = isInProductionRestartableError ? "Light Barrier Broken during production, clear this error before you can complete the current carton" : string.Empty;

                if (IsInProductionRestartableError == false && Get(VariableMap.ErrorCode1.ToString(), true).ToString() != "0")
                {
                    Set(VariableMap.ErrorCode1.ToString(), 0);
                }

                OnPropertyChanged();
            }
        }

        private new EmMachineVariables VariableMap { get { return base.VariableMap as EmMachineVariables; } }

        /// <summary>
		/// Sets the machine variable.
		/// </summary>
		/// <param name="name">Name of the variable.</param>
		/// <param name="value">The value.</param>
		public override void Set(string name, object value)
        {            // Show what we receive on Detail
            if ((ShowHeartBeat ||
                 name != VariableMap.PcLifeSign.ToString() &&
                 name != VariableMap.PlcLifeSign.ToString()) &&
                (VariableDictionary.EventNotificationVariables == null ||
                 VariableDictionary.EventNotificationVariables.Any(v => v.Name == name) == false))
            {
                AddDetails(string.Format("{0}>>POST: {1} = {2}", Environment.NewLine, name, value));
            }

            if (IoDeviceVariableMap.Instance.ContainsVariable(name))
            {
                ioModelSet(name, value);
                return;
            }
            if ((name == VariableMap.Track1ChangingCorrugate.ToString()
                || name == VariableMap.Track2ChangingCorrugate.ToString()
                || name == VariableMap.Track3ChangingCorrugate.ToString()
                || name == VariableMap.Track4ChangingCorrugate.ToString()) && value.ToString() == "1")
            {
                Set(VariableMap.ChangeCorrugate.ToString(), 1); 
            }
            if (name == VariableMap.StartupProcedureCompleted.ToString() && value.ToString() == "1")
            {
                Set(VariableMap.Track1ChangingCorrugate.ToString(), 0);
                Set(VariableMap.Track2ChangingCorrugate.ToString(), 0);
                Set(VariableMap.Track3ChangingCorrugate.ToString(), 0);
                Set(VariableMap.Track4ChangingCorrugate.ToString(), 0);
            }
            

            base.Set(name, value);
        }

        public override object Get(string variable, bool internalCheck = false)
        {
            return IoDeviceVariableMap.Instance.ContainsVariable(variable) ? ioModelGet(variable, internalCheck) : base.Get(variable, internalCheck);
        }

        /// <summary>
		/// Initializes the simulator.
		/// </summary>
		/// <param name="machineUri">The machine URI.</param>
		/// <param name="sendNotification">The send notification.</param>
		public void Initialize(string machineUri, Func<string, object, string, string> sendNotification)
		{
			AddDetails(BaseCommands.InitialMessage);

			MachineAddressPort = machineUri;
			SendVariableNotification = sendNotification;

            VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.ACP10Scaling.ToString(), 100);
            VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.FeedRollerAcceleration.ToString(), 16777);
            VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.FeedRollerSpeed.ToString(), 5905);
            VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.CrossHeadSpeed.ToString(), 14173);
            VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.CrossHeadAcceleration.ToString(), 59551);
            VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.MachinePlcVersion.ToString(), "Em Simulator");
		}

        public void SetLightBarrier()
        {
            if (Get(VariableMap.ErrorCode1.ToString(), true).ToString() == "0")
            {
                Set(VariableMap.ErrorCode1Arguments.ToString(), new List<int> { 1 });
                Set(VariableMap.ErrorCode1.ToString(), PacksizeMachineErrorCodes.LightBarrierBroken);

                if (isProducing)
                {
                    IsInProductionRestartableError = true;
                }
                else
                {
                    inError = true;
                    ErrorMessage = "Light Barrier Error triggered, Please Reset Error";
                    OnPropertyChanged("InError");
                }
            }
        }
	}
}