﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Communication.PLCBase;
using PackNet.FusionSim.Utilities;

namespace PackNet.FusionSim.Models
{
    /// <summary>
    /// The Zebra Printer Simulator
    /// </summary>
    public class ZebraPrinterSimulator : BaseSimulator
    {
        private readonly string name;
        private int paperOutStatus;
        private int headOpenStatus;
        private int printModeStatus;
        private int LabelWaitingStatus;
        private int labelsInQueue;
        private int labelPrintingStatus;
        private int printerPaused;
        private string template;
        private bool isPrinting;
        private bool hasLabelsInQueue;
        private ILogger logger;

        /// <summary>
        /// Gets or sets the template.
        /// </summary>
        /// <value>
        /// The template.
        /// </value>
        public string Template
        {
            get { return template; }
            set
            {
                if (value == template)
                {
                    return;
                }
                template = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the head is open.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [head open status]; otherwise, <c>false</c>.
        /// </value>
        public bool HeadOpenStatus
        {
            set
            {
                headOpenStatus = Convert.ToInt32(value);
                OnPropertyChanged("HeadCommandDisplay");
            }
            get { return Convert.ToBoolean(headOpenStatus); }
        }

        public string HeadCommandDisplay
        {
            get { return HeadOpenStatus ? "Close Head" : "Head Open"; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether peel off mode is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if peel off mode is enabled; otherwise, <c>false</c>.
        /// </value>
        private bool PeelOfModeEnabled
        {
            set
            {
                printModeStatus = Convert.ToInt32(value);
                OnPropertyChanged("PrintMode");
            }
            get { return printModeStatus == 1; }
        }

        /// <summary>
        /// Gets the print mode.
        /// </summary>
        /// <value>
        /// The print mode.
        /// </value>
        public string PrintMode
        {
            get { return PeelOfModeEnabled ? "Peel Off" : "Tear Off"; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the paper is out.
        /// </summary>
        /// <value>
        ///   <c>true</c> if the paper is out; otherwise, <c>false</c>.
        /// </value>
        public bool PaperOutStatus
        {
            set
            {
                paperOutStatus = Convert.ToInt32(value);
                OnPropertyChanged("RibbonCommandDisplay");
            }
            get { return Convert.ToBoolean(paperOutStatus); }
        }

        public string RibbonCommandDisplay
        {
            get { return PaperOutStatus ? "Fill Ribbon" : "Out of Ribbon"; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has labels in queue.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has labels in queue; otherwise, <c>false</c>.
        /// </value>
        public bool HasLabelsInQueue
        {
            get { return hasLabelsInQueue; }
            set
            {
                hasLabelsInQueue = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is pause.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is pause; otherwise, <c>false</c>.
        /// </value>
        public bool IsPause { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is printing.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is printing; otherwise, <c>false</c>.
        /// </value>
        public bool IsPrinting
        {
            get { return isPrinting; }
            set
            {
                isPrinting = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ZebraPrinterSimulator"/> class.
        /// </summary>
        public ZebraPrinterSimulator(string name)
        {
            this.name = name;
            logger = LogManager.GetLogFor("FusionSimulator");

            AddDetails("At least I'm making labels");
            IsPrinting = false;
        }

        /// <summary>
        /// Processes the message.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        public string ProcessMessage(string message)
        {
            logger.Log(LogLevel.Debug, "Process message for printer '" + name +"' raw message: " + message);
            if (ShowHeartBeat || message != ZebraPrinterCommands.HostStatusCommand)
            {
                AddDetails(Environment.NewLine + message);
            }

            if (message.StartsWith(ZebraPrinterCommands.HostStatusCommand))
            {
                return message.Replace(ZebraPrinterCommands.HostStatusCommand, string.Empty);
            }
            
            if (message.StartsWith(ZebraPrinterCommands.StartCommand))
            {
                HandlePausePlayCommand(0);
                if (labelsInQueue > 0)
                {
                    labelPrintingStatus = 1;
                    IsPrinting = true;
                } 

                return message.Replace(ZebraPrinterCommands.StartCommand, string.Empty);
            }
            
            if (message.StartsWith(ZebraPrinterCommands.PauseCommand))
            {
                HandlePausePlayCommand(1);
                return message.Replace(ZebraPrinterCommands.PauseCommand, string.Empty);
            }
            
            if (message.StartsWith(ZebraPrinterCommands.BeginFormat + ZebraPrinterCommands.PrintMode) && message.EndsWith(ZebraPrinterCommands.SaveSettings + ZebraPrinterCommands.EndFormat))
            {
                var cmd = message.Replace(ZebraPrinterCommands.BeginFormat + ZebraPrinterCommands.PrintMode, string.Empty);
                cmd = cmd.Replace(ZebraPrinterCommands.SaveSettings + ZebraPrinterCommands.EndFormat,
                    string.Empty);

                HandleInit(cmd.Contains(ZebraPrinterCommands.PeelOffModeCommand + ",Y"));

                return string.Empty;
            }

            if (message.StartsWith(ZebraPrinterCommands.BeginFormat) && message.Contains("^FO") && message.Contains(ZebraPrinterCommands.EndFormat))
            {

                Template = message.Replace(ZebraPrinterCommands.BeginFormat, string.Empty)
                    .Replace(ZebraPrinterCommands.EndFormat, string.Empty);
                HandleLabelCommand();
                int startIndex = message.IndexOf(ZebraPrinterCommands.EndFormat) + 3;
                return message.Substring(startIndex, message.Length - startIndex);
            }

            return string.Empty;
        }

        /// <summary>
        /// Handles the initialize.
        /// </summary>
        /// <param name="peelOfMode">if set to <c>true</c> set the printer to peel off.</param>
        private void HandleInit(bool peelOfMode)
        {
            PeelOfModeEnabled = peelOfMode;
        }

        /// <summary>
        /// Handles the label command.
        /// </summary>
        private void HandleLabelCommand()
        {
            labelsInQueue++;
        }

        /// <summary>
        /// Handles the pause play command.
        /// </summary>
        /// <param name="pauseMode">The pause mode.</param>
        private void HandlePausePlayCommand(int pauseMode)
        {
            printerPaused = pauseMode;
            IsPause = pauseMode == 1;
        }

        /// <summary>
        /// Gets the host status string.
        /// </summary>
        /// <returns></returns>
        public string GetHostStatusString()
        {
            return string.Format(
                ZebraPrinterCommands.HostStatusMessage,
                paperOutStatus,
                printerPaused,
                labelsInQueue == 0 ? "000" : labelsInQueue.ToString(),
                headOpenStatus,
                printModeStatus,
                LabelWaitingStatus,
                labelPrintingStatus);
        }

        /// <summary>
        /// Takes the label.
        /// </summary>
        public void TakeLabel()
        {
            LabelWaitingStatus--;
            HasLabelsInQueue = false;
        }

        /// <summary>
        /// Plays the pause.
        /// </summary>
        public void PlayPause()
        {
            HandlePausePlayCommand(printerPaused == 1 ? 0 : 1);
        }

        /// <summary>
        /// Completes the printing.
        /// </summary>
        public void CompletePrinting()
        {
            IsPrinting = false;
            labelPrintingStatus = 0;
            labelsInQueue = 0;
            LabelWaitingStatus = PeelOfModeEnabled ? 1 : 0;
            HasLabelsInQueue = PeelOfModeEnabled;
            Template = string.Empty;
        }
    }
}