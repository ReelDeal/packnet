﻿using System;

using Newtonsoft.Json;

using PackNet.Communication.Communicators;

using System.Collections.Generic;

namespace PackNet.FusionSim.Models
{
    /// <summary>
    /// The Fusion Machine Instruction table.
    /// </summary>
    public class InstructionDictionary:Dictionary<string, object>
    {
        /// <summary>
        /// Gets the event notification variables.
        /// </summary>
        /// <value>
        /// The event notification variables.
        /// </value>
        public IList<EventNotificationVariable> EventNotificationVariables { get; private set; }

        private readonly PacksizeBaseMachineVariableMap variableMap;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstructionDictionary"/> class.
        /// </summary>
        public InstructionDictionary(PacksizeBaseMachineVariableMap variableMap)
        {
            this.variableMap = variableMap;
            RegisterCommonStartValues();
        }

        private void RegisterCommonStartValues()
        {
            Add(variableMap.StartedProductionItemId.ToString(), new Guid());
            Add(variableMap.ProductionHistoryItemId.ToString(), new Guid());
            Add(variableMap.RemovedProductionItemId1.ToString(), new Guid());
            Add(variableMap.RemovedProductionItemId2.ToString(), new Guid());
            Add(variableMap.OutOfCorrugate.ToString(), 0);
            Add(variableMap.ChangeCorrugate.ToString(), 0);
            Add(variableMap.MachinePaused.ToString(), 1);
            Add(variableMap.StartupProcedureCompleted.ToString(), 0);
            Add(variableMap.InCalibrationMode.ToString(), 0);
            Add(variableMap.InServiceMode.ToString(), 0);

            Add(variableMap.ErrorCode1.ToString(), 0);
            Add(variableMap.ErrorCode1Arguments.ToString(), new short[5]);
            Add(variableMap.ErrorCode2.ToString(), 0);
            Add(variableMap.ErrorCode2Arguments.ToString(), new short[5]);
            Add(variableMap.ErrorCode3.ToString(), 0);
            Add(variableMap.ErrorCode3Arguments.ToString(), new short[5]);
            Add(variableMap.ErrorCode4.ToString(), 0);
            Add(variableMap.ErrorCode4Arguments.ToString(), new short[5]);
            Add(variableMap.ErrorCode5.ToString(), 0);
            Add(variableMap.ErrorCode5Arguments.ToString(), new short[5]);

            Add(variableMap.PcLifeSign.ToString(), 1);
            Add(variableMap.ConnectionStatus.ToString(), 0);
            Add(variableMap.ReleaseBox.ToString(), 1);
        }

        /// <summary>
        /// Sets the variable on instruction dictionary.
        /// </summary>
        /// <param name="varName">Name of the variable.</param>
        /// <param name="value">The value.</param>
        public bool SetVariableOnInstructionDictionary(string varName, object value)
        {
            if (ContainsKey(varName))
            {
                if (this[varName] != value)
                {
                    this[varName] = value;
                    return true;
                }

                return false;
            }
            
            Add(varName, value);
            return true;
        }

        public void RegisterNotificationVariables(object value, Action<string, object> sendNotification)
        {
            var items = JsonConvert.DeserializeObject<List<EventNotificationVariable>>(value.ToString());
            EventNotificationVariables = items;
            foreach (var eventNotificationVariable in EventNotificationVariables)
            {
                if(ContainsKey(eventNotificationVariable.Name))
                    sendNotification(eventNotificationVariable.Name, this[eventNotificationVariable.Name]);
            }
        }
    }

    public class EventNotificationVariable
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}
