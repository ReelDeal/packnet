﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;
using PackNet.Communication.Communicators;
using PackNet.Communication.PLCBase;
using PackNet.MachineCommandService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PackNet.FusionSim.Models
{
    public abstract class BaseCutCreaseMachineSimulator : BaseSimulator, IPacksizeCommunicationBasedSimulator
    {
        private bool inChangeCorrugate = false;

        private Guid idToAdd;
        private bool inService;
        private bool canComplete = true;
        protected bool inError;
        private bool play;
        private bool isEmergencyStopEngaged;

        protected bool isProducing;
        private string errorMessage;

        protected readonly Stopwatch ProductionTimeStopwatch = new Stopwatch();
        protected readonly IDictionary<Guid, double> ProducibleLengthQueue = new Dictionary<Guid, double>();
        
        protected readonly PacksizeBaseMachineVariableMap VariableMap;
        protected readonly MachineCommandVariableMap MachineCommandVariableMap;

        protected Guid NextProducibleId = Guid.Empty;
        protected InstructionDictionary VariableDictionary;

        protected string MachineAddressPort = string.Empty;
        protected Func<string, object, string, string> SendVariableNotification;
        
        protected BaseCutCreaseMachineSimulator(PacksizeBaseMachineVariableMap variableMap)
        {

            MachineCommandVariableMap = MachineCommandVariableMap.Instance;
            VariableMap = variableMap;
            VariableDictionary = new InstructionDictionary(variableMap);
            IsStartupComplete = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FusionSimulator"/> is play.
        /// </summary>
        /// <value>
        ///   <c>true</c> if play; otherwise, <c>false</c>.
        /// </value>
        public bool Play
        {
            get { return play; }
            set
            {
                //no change
                if (value.Equals(play) || InError || !IsStartupComplete)
                {
                    return;
                }

                play = value;
                Set(VariableMap.MachinePaused.ToString(), play ? 0 : 1);

                //If the machine is in an error state (not equal to 0)
                if (play && Get(VariableMap.ErrorCode1.ToString(), true).ToString() != "0" && !IsEmergencyStopEngaged && !InError)
                {

                    //Clear out the error state by setting to 0
                    Set(VariableMap.ErrorCode1.ToString(), 0);
                    StartUpComplete();
                    ErrorMessage = string.Empty;
                }

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [in change corrugate].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [in change corrugate]; otherwise, <c>false</c>.
        /// </value>
        public bool InChangeCorrugate
        {
            get { return inChangeCorrugate; }
            set
            {
                if (inChangeCorrugate == value)
                    return;

                inChangeCorrugate = value;

                Set(VariableMap.ChangeCorrugate.ToString(), inChangeCorrugate);

                if (inChangeCorrugate == false)
                {
                    Set(VariableMap.StartupProcedureCompleted.ToString(), "0");
                    IsStartupComplete = false;
                }

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Starts up complete.
        /// </summary>
        public void StartUpComplete()
        {;
            Set(VariableMap.StartupProcedureCompleted.ToString(), 1);
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether the e-stop is engaged
        /// </summary>
        public bool IsEmergencyStopEngaged
        {
            get { return isEmergencyStopEngaged; }
            set
            {
                //no change so abort
                if (value.Equals(isEmergencyStopEngaged))
                {
                    return;
                }

                isEmergencyStopEngaged = value;

                if (isEmergencyStopEngaged)
                {
                    play = !isEmergencyStopEngaged;
                    OnPropertyChanged("Play");
                    ErrorMessage = "Emergency Stop State: Engaged";
                }
                else
                {
                    AddDetails(Environment.NewLine + "Emergency Stop Disengaged. Click Reset and Play to bring the machine back online.");
                    ErrorMessage = "Emergency Stop State: Disengaged. Click Reset and Play to bring the machine back online.";
                }

                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the error message.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="EmSimulator"/> has an error.
        /// </summary>
        /// <value>
        ///   <c>true</c> if reset; otherwise, <c>false</c>.
        /// </value>
        public bool InError
        {
            get { return inError; }
            set
            {
                if (value.Equals(inError))
                {
                    return;
                }

                if (IsEmergencyStopEngaged)
                {
                    ErrorMessage = "Emergency Stop State: Engaged. Reset Emergency stop before trying to clear machine errors";
                    return;
                }
                ErrorMessage = string.Empty;

                ResetError();
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [in service].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [in service]; otherwise, <c>false</c>.
        /// </value>
        public bool InService
        {
            get { return inService; }
            set
            {
                if (value.Equals(inService))
                {
                    return;
                }
                inService = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance can complete.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance can complete; otherwise, <c>false</c>.
        /// </value>
        public bool CanComplete
        {
            get { return canComplete; }
            set
            {
                if (value.Equals(canComplete))
                {
                    return;
                }
                canComplete = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// SetError
        /// </summary>
        public void ResetError()
        {
            if (Get(VariableMap.ErrorCode1.ToString(), true).ToString() != "0")
            {
                Set(VariableMap.ErrorCode1.ToString(), 0);
                inError = false;
            }
            else
            {
                CompletePackaging(PackagingStatus.Failure);
                Set(VariableMap.StartupProcedureCompleted.ToString(), false);
            }
            
            IsStartupComplete = false;
            
            OnPropertyChanged("InError");
        }

        /// <summary>
        /// Emergencies the stop.
        /// </summary>
        public void EmergencyStop()
        {
            if (Get(VariableMap.ErrorCode1.ToString(), true).ToString() == "0")
            {
                Set(VariableMap.ErrorCode1.ToString(), 1);

                if (ProducibleQueue.Count > 0)
                {
                    var id = ProducibleQueue.Peek();

                    if (id != Guid.Empty)
                        CompletePackaging(PackagingStatus.Failure);

                    id = DequePackagingId();
                    Logger.Log(LogLevel.Info, "DequePackagingId {0}", id);
                    Set(VariableMap.RemovedProductionItemId1.ToString(), id);
                }

                inError = true;
            }

            OnPropertyChanged("InError");
        }


        /// <summary>
        /// Sets the low air.
        /// </summary>
        public void SetLowAir()
        {
            if (Get(VariableMap.ErrorCode1.ToString(), true).ToString() == "0")
            {
                Set(VariableMap.ErrorCode1Arguments.ToString(), new List<int> { 1 });
                Set(VariableMap.ErrorCode1.ToString(), PacksizeMachineErrorCodes.AirPressureLow);
                inError = true;
                ErrorMessage = "Low Air Pressure Error triggered, Please Reset Error";
            }

            OnPropertyChanged("InError");
        }

        /// <summary>
        /// Sets the out of corrugate.
        /// </summary>
        public void SetOutOfCorrugate()
        {
            if (Get(VariableMap.ErrorCode1.ToString(), true).ToString() == "0")
            {
                Set(VariableMap.ErrorCode1Arguments.ToString(), new List<int> { 1 });
                Set(VariableMap.ErrorCode1.ToString(), PacksizeMachineErrorCodes.OutOfCorrugate);
                inError = true;
                ErrorMessage = "Out Of Corrugate Error triggered, Please Reset Error";
            }

            OnPropertyChanged("InError");
        }

        /// <summary>
        /// Starts the packaging.
        /// </summary>
        public void StartPackaging()
        {
            var item = Guid.Empty;

            lock (QueueLock)
            {
                if (ProducibleQueue.Count > 0)
                {
                    item = ProducibleQueue.Peek();
                    Logger.Log(LogLevel.Info, "Peaking at {0}", item);
                }
            }

            if (item != Guid.Empty)
            {
                ProductionTimeStopwatch.Restart();
                ProductionTimeStopwatch.Start();
                Set(VariableMap.StartedProductionItemId.ToString(), item);
                isProducing = true;
            }
        }

        /// <summary>
        /// Completes the packaging.
        /// </summary>
        /// <param name="packagingStatus">The packaging status.</param>
        public void CompletePackaging(PackagingStatus packagingStatus)
        {
            if (isProducing)
            {
                var id = DequePackagingId();

                if (id != Guid.Empty)
                {

                    LogWithMachineId(string.Format("CompletePackaging for {0}", id));
                    ProductionTimeStopwatch.Stop();
                    Set(VariableMap.ProductionHistoryItemCompletionTime.ToString(), ProductionTimeStopwatch.ElapsedMilliseconds);
                    Set(VariableMap.ProductionHistoryItemCompletionStatus.ToString(), (int)packagingStatus);
                    Set(VariableMap.ProductionHistoryItemCorrugateUsage.ToString(), ProducibleLengthQueue[id]);
                    Set(VariableMap.ProductionHistoryItemId.ToString(), id);
                    CanComplete = false;
                }
                else
                {
                    LogWithMachineId("Attempting to complete for id which does not exist");
                }

                isProducing = false;
            }
        }

        /// <summary>
        /// Adds a packaging identifier to the end of the Packaging Queue
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="producibleLength">The length of the producible</param>
        protected void EnqueuePackagingId(Guid id, double producibleLength)
        {
            lock (QueueLock)
            {
                if (!Play)
                {
                    return;
                }

                LogWithMachineId(string.Format("EnqueuePackagingId for {0} and length {1}", id, producibleLength)); 
                ProducibleQueue.Enqueue(id);

                if (ProducibleLengthQueue.ContainsKey(id))
                {
                    ProducibleLengthQueue[id] = producibleLength;
                }
                else
                {
                    ProducibleLengthQueue.Add(id, producibleLength);
                }

                OnPropertyChanged("QueueDetails");
                OnPropertyChanged("HasItemsInQueue");
            }
        }

        /// <summary>
        /// Clears the packaging queue.
        /// </summary>
        protected void ClearPackagingQueue()
        {
            var removedProductionItemIds = new List<Guid>();

            while (ProducibleQueue.Any())
            {
                removedProductionItemIds.Add(DequePackagingId());
            }

            if (removedProductionItemIds.Any() == false)
                return;

            Set(VariableMap.RemovedProductionItemId1.ToString(), removedProductionItemIds.First());
            if (removedProductionItemIds.Count > 1)
                Set(VariableMap.RemovedProductionItemId2.ToString(), removedProductionItemIds.ElementAt(1));
        }

        /// <summary>
        /// Gets the machine variable.
        /// </summary>
        /// <param name="variable">The variable.</param>
        /// <returns></returns>
        public virtual object Get(string variable, bool internalCheck = false)
        {
            if (variable == VariableMap.MachineInPausedState.ToString())
            {
                return play == false;
            }
            
            var value = VariableDictionary.ContainsKey(variable) ? VariableDictionary[variable] : null;

            if (internalCheck == false)
                AddDetails(string.Format("{0}<<GET: {1} -> {2}", Environment.NewLine, variable, value));

            return value;
        }

        public virtual void Set(string name, object value)
        {
            var hasUpdated = VariableDictionary.SetVariableOnInstructionDictionary(name, value);

            if (name == VariableMap.EventNotificationRegistrationBuffer.ToString())
            {
                VariableDictionary.RegisterNotificationVariables(value, PublishVariableNotification);
            }

            if (MachineCommandVariableMap.ContainsVariable(name))
            {
                HandleMachineCommandVariableChanges(name);
            }
            
            if (VariableMap.ContainsVariable(name))
            {
                HandleCommonVariableChanges(name, value);
            }

            if (hasUpdated)
            {
                PublishVariableNotification(name, value);
            }
        }

        /// <summary>
        /// Publishes the variable notification.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        private void PublishVariableNotification(string name, object value)
        {
            if (VariableDictionary.EventNotificationVariables != null &&
                VariableDictionary.EventNotificationVariables.Any(c => c.Name == name) && SendVariableNotification != null)
            {
                AddDetails(SendVariableNotification(name, value, MachineAddressPort));
            }
        }

        private void HandleMachineCommandVariableChanges(string name)
        {
            if (name == MachineCommandVariableMap.ToggleServiceMode.ToString())
            {
                inService = !inService;
                if (inService)
                {
                    Set(VariableMap.InServiceMode.ToString(), inService);
                }
                else
                {
                    Set(VariableMap.StartupProcedureCompleted.ToString(), false);
                }
            }

        }

        /// <summary>
        /// Handles the common variable changes.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        private void HandleCommonVariableChanges(string name, object value)
        {
            if (name == VariableMap.StartupProcedureCompleted.ToString())
            {
                IsStartupComplete = value.ToString() == "1";
                return;
            }

            if (name == VariableMap.MachinePaused.ToString())
            {
                Play = value.ToString() == "0";
                OnPropertyChanged("Play");
                return;
            }

            if (name == VariableMap.TransferBuffer.ToString())
            {
                var item = JsonConvert.DeserializeObject<PLCProducible>(value.ToString());

                idToAdd = GuidHelpers.ConvertIntArrayToGuid(item.Id);

                if (ProducibleQueue.Count > 1)
                {
                    if (ProducibleQueue.Peek() == idToAdd)
                    {
                        return;
                    }
                }

                EnqueuePackagingId(idToAdd, SimulatorHelpers.GetPackagingLength(item.InstructionList.ToList()));
                CanComplete = !item.ShouldWaitForBoxRelease;
                return;
            }
            
            if (name == VariableMap.NumberOfLongHeads.ToString())
            {
                var longHeadCnt = ((JValue)value).ToObject<int>();
                InitializeLongHeads(longHeadCnt);
                return;
            }

            if (VariableMap.ChangeCorrugate.ToString() == name)
            {
                InChangeCorrugate = value.ToString() == "1" || value.ToString() == bool.TrueString;
                OnPropertyChanged("InChangeCorrugate");
                return;
            }

            if (VariableMap.RunPrestart.ToString() == name)
            {
                if (value.ToString() == "1")
                {
                    inChangeCorrugate = false;
                    OnPropertyChanged("InChangeCorrugate");
                }
            }

            if (VariableMap.ReleaseBox.ToString() == name)
            {
                CanComplete = true;
            }
        }
        
        /// <summary>
        /// Initializes the long heads.
        /// </summary>
        /// <param name="longHeadCnt">The long head count.</param>
        /// <remarks>Create longheads based on what .Server tells us we should have.</remarks>
        private void InitializeLongHeads(int longHeadCnt)
        {
            var array = new[] { 3.93, 7.84, 15.74, 19.68, 23.62, 27.56, 31.47 }.Take(longHeadCnt).ToList();
            var arrayList = new ArrayList(array);

            for (var i = 0; i < 14 - longHeadCnt; i++)
            {
                arrayList.Add(0);
            }

            VariableDictionary[VariableMap.LongHeadPositions.ToString()] = arrayList.ToArray();
        }
    }
}
