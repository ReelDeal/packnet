﻿using System;
using System.Linq;

using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Common.WorkflowTracking;
using PackNet.Communication.Communicators.Fusion;
using PackNet.FusionSim.Utilities;
using PackNet.IODeviceService.Communication;

namespace PackNet.FusionSim.Models
{
    public class FusionSimulator : BaseCutCreaseMachineSimulator
    {

        private readonly Action<string, object> ioModelSet;
        private readonly Func<string, bool, object> ioModelGet;

        public new IFusionMachineVaribleMap VariableMap { get { return base.VariableMap as IqFusionMachineVariables; } }
       
        /// <summary>
		/// Initializes a new instance of the <see cref="FusionSimulator"/> class.
		/// </summary>
        public FusionSimulator(Action<string, object> ioModelSet, Func<string, bool, object> ioModelGet)
            : base(IqFusionMachineVariables.Instance)
        {
            this.ioModelSet = ioModelSet;
            this.ioModelGet = ioModelGet;
             
        }

        /// <summary>
		/// Sets the machine variable.
		/// </summary>
		/// <param name="name">Name of the variable.</param>
		/// <param name="value">The value.</param>
        public override void Set(string name, object value)
        {
            // Show what we receive on Detail
            if ((ShowHeartBeat ||
                 name != VariableMap.PcLifeSign.ToString() &&
                 name != VariableMap.PlcLifeSign.ToString()) &&
                (VariableDictionary.EventNotificationVariables == null ||
                 VariableDictionary.EventNotificationVariables.Any(v => v.Name == name) == false))
            {
                //LogWithMachineId(string.Format("{0}>>POST: {1} = {2}", Environment.NewLine, name, value));
                AddDetails(string.Format("{0}>>POST: {1} = {2}", Environment.NewLine, name, value));
            }

            if (IoDeviceVariableMap.Instance.ContainsVariable(name))
            {
                ioModelSet(name, value);
                return;
            }

		    base.Set(name, value);
			
			switch (name)
			{
               
                case "Main:TRACK_SETTINGS.NumberOfTracks":
                    var trackCount = ((JValue)value).ToObject<int>();
                        VariableDictionary[FusionCommands.TracksStartingPositions] = trackCount ==1? new[] { 6.4, 0.0, 0 ,0 }:new[] { 32.69, 33.56, 0, 0 };
                    break;
                //TODO: Jeff - IO device
				case FusionCommands.ClearQueue:
					VariableDictionary[name] = value;
                    // TODO: Figure out how this is been called.
                    //if (int.Parse(value.ToString()) == 1)
                    //{
                    //    ClearPackagingQueue();
                    //}
					break;
                case FusionCommands.InputEventMessageContainer:
			        VariableDictionary.EventNotificationVariables.Add(new EventNotificationVariable() { Name = name });
			        break;
			}
		}

        public override object Get(string variable, bool internalCheck = false)
        {
            return IoDeviceVariableMap.Instance.ContainsVariable(variable) ? ioModelGet(variable, internalCheck) : base.Get(variable, internalCheck);
        }
        
		/// <summary>
		/// Initializes the simulator.
		/// </summary>
		/// <param name="machineUri">The machine URI.</param>
		/// <param name="sendNotification">The send notification.</param>
		public void Initialize(string machineUri, Func<string, object, string, string> sendNotification)
		{
			AddDetails(BaseCommands.InitialMessage);

			MachineAddressPort = machineUri;
			SendVariableNotification = sendNotification;

		    VariableDictionary.SetVariableOnInstructionDictionary(FusionCommands.ClearQueue, 0);
		    VariableDictionary.SetVariableOnInstructionDictionary(FusionCommands.InputEventMessageContainer, null);
		    VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.TrackOffset.ToString(), new[] { 32.69, 33.56, 0, 0 });
		    VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.FeedRollerGearRatio.ToString(), 0);

            //PLEASE: DO NOT add the line back in for MachinePlcVersion, or if you do, change it something that will serialize/de-serialize correctly
            //and have the discipline to set your exception handling correctly so you will see this and know it works or is broken. Id you need help ask.
            //VariableDictionary.SetVariableOnInstructionDictionary(VariableMap.MachinePlcVersion.ToString(), "Fusion Simulator");
		}
    }
}