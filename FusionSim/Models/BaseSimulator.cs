using PackNet.FusionSim.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;

namespace PackNet.FusionSim.Models
{
    public abstract class BaseSimulator : NotifyPropertyChangedBase
    {
        protected readonly object QueueLock = new object();
        protected readonly Queue<Guid> ProducibleQueue = new Queue<Guid>();
        private readonly Queue<string> details = new Queue<string>(); 
        private bool isStartupComplete;
        private bool showHeartBeat;
        private readonly ILogger logger;
        
        public void LogWithMachineId(string passedValue)
        {
            logger.Log(LogLevel.Info, "MachineUri: {0} - {1}", MachineUri, passedValue);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseSimulator"/> class.
        /// </summary>
        protected BaseSimulator()
        {
            showHeartBeat = true;
            logger = LogManager.GetLogFor("FusionSimulator");
        }

        public ILogger Logger
        {
            get { return logger; }
        }

        public Uri MachineUri { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is startup complete.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is startup complete; otherwise, <c>false</c>.
        /// </value>
        public bool IsStartupComplete
        {
            get { return isStartupComplete; }
            set
            {
                if (value.Equals(isStartupComplete))
                {
                    return;
                }
                isStartupComplete = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show heart beat.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show heart beat]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowHeartBeat
        {
            get { return showHeartBeat; }
            set
            {
                if (value.Equals(showHeartBeat))
                {
                    return;
                }
                showHeartBeat = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Gets the queue details.
        /// </summary>
        /// <value>
        /// The queue details.
        /// </value>
        public string QueueDetails
        {
            get
            {
                var result = new StringBuilder();

                foreach (var plcQueueItem in ProducibleQueue)
                {
                    result.AppendLine(plcQueueItem.ToString());
                }

                return result.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the details.
        /// </summary>
        /// <value>
        /// The details.
        /// </value>
        public string Details
        {
            get { return string.Concat(details); }
        }

        public void AddDetails(string newLine)
        {
            details.Enqueue(newLine);

            while (details.Count > 300)
            {
                details.Dequeue();
            }

            OnPropertyChanged("Details");
        }

        /// <summary>
        /// Gets a value indicating whether this instance has items in queue.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance has items in queue; otherwise, <c>false</c>.
        /// </value>
        public bool HasItemsInQueue
        {
            get { return ProducibleQueue.Any(); }
        }

        /// <summary>
        /// Gets the queue.
        /// </summary>
        /// <value>
        /// Queue.
        /// </value>
        public Queue<Guid> Queue
        {
            get { return ProducibleQueue; }
        }

        /// <summary>
        /// Deques the packaging identifier.
        /// </summary>
        /// <returns></returns>
        protected Guid DequePackagingId()
        {
            var id = Guid.Empty;

            lock (QueueLock)
            {
                if (ProducibleQueue.Any())
                {
                    id = ProducibleQueue.Dequeue();
                    OnPropertyChanged("QueueDetails");
                    OnPropertyChanged("HasItemsInQueue");
                }
            }
            return id;
        }

        /// <summary>
        /// Clears the details.
        /// </summary>
        public void ClearDetails()
        {
            details.Clear();
            OnPropertyChanged("Details");
        }
    }
}