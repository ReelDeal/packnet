﻿namespace PackNet.FusionSim.Models
{
    public interface IPacksizeCommunicationBasedSimulator
    {
        object Get(string json, bool internalCheck = false);

        void Set(string name, object value);
    }
}
