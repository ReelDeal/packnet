﻿using System;
using System.Collections.Generic;
using System.Security.Policy;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;

using PackNet.FusionSim.Models;
using PackNet.FusionSim.Utilities;
using PackNet.FusionSim.ViewModels;

namespace PackNet.FusionSim
{
    /// <summary>
    /// Interaction logic for SimulatorDetail.xaml
    /// </summary>
    public partial class SimulatorDetail
    {
        private readonly DispatcherTimer timer;

        public SimulatorDetail()
        {
            InitializeComponent();

            timer = new DispatcherTimer { Interval = new TimeSpan(0, 0, 2) };
            timer.Tick += ((sender, e) =>
            {
                if (Math.Abs(_scrollViewer.VerticalOffset - _scrollViewer.ScrollableHeight) < 20)
                {
                    _scrollViewer.ScrollToEnd();
                }
            });
            timer.Start();

            DataContextChanged +=SimulatorDetail_DataContextChanged;
            
        }

        private void SimulatorDetail_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (DataContext == null)
            {
                return;
            }

            var type = ((Simulator)DataContext).MachineType;
            switch (type)
            {
                case MachineType.Em:
                    _contentControl.Content = new Em { DataContext = DataContext };
                    break;
                case MachineType.Fusion:
                    _contentControl.Content = new Fusion { DataContext = DataContext };
                    break;
                default:
                    _contentControl.Content = new ZebraPrinter { DataContext = DataContext };
                    break;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (DataContext == null)
            {
                return;
            }

            var details = ((Simulator)DataContext).Sim.Details;
            Clipboard.SetText(details);
        }

        ~SimulatorDetail()
        {
            timer.Stop();
        }
    }
}
