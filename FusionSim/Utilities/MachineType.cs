﻿
namespace PackNet.FusionSim.Utilities
{
    public enum MachineType
    {
        NotSupported,
        Fusion,
        Em,
        ZebraPrinter
    }
}
