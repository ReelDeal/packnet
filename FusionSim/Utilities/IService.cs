﻿using System;

namespace PackNet.FusionSim.Utilities
{
    interface IService: IDisposable
    {
        /// <summary>
        /// Starts this instance.
        /// </summary>
        void Start();

        /// <summary>
        /// Stops this instance.
        /// </summary>
        void Stop();
    }
}
