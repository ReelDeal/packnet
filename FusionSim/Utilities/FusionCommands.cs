﻿namespace PackNet.FusionSim.Utilities
{
    public class FusionCommands : BaseCommands
    {
        public const string TracksStartingPositions = "Main:TRACK_SETTINGS.TrackStartingPositions";
        public const string ClearQueue = "Main:ClearQueue";
        public const string InputEventMessageContainer = "IODevice:InputEventMessageContainer";
    }
}