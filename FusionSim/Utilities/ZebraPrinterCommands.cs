﻿
namespace PackNet.FusionSim.Utilities
{
    public class ZebraPrinterCommands
    {
        public const string HostStatusCommand = "~HS";
        public const string StartCommand = "~PS";
        public const string PauseCommand = "~PP";

        public const string BeginFormat = "^XA";
        public const string EndFormat = "^XZ";

        public const string PrintMode = "^MM";
        public const string PeelOffModeCommand = "P";
        public const string TearOffModeCommand = "T";
        public const string SaveSettings = "^JUS";

        public const string HostStatusMessage = "\u0002aaa,{0},{1},dddd,{2},f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,{3},p,q,{4},s,{5},{6},v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
    }
}
