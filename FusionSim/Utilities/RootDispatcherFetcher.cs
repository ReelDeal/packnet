﻿using System.Windows;
using System.Windows.Threading;

namespace PackNet.FusionSim.Utilities
{
    /// <summary>
    /// Gets the application dispatcher
    /// </summary>
    /// <remarks>
    /// When using the simulator through Automation the test fails because of the Dispatcher. This is the work around.
    /// </remarks>
    public class RootDispatcherFetcher
    {
        private static Dispatcher _rootDispatcher;

        /// <summary>
        /// Gets the root dispatcher.
        /// </summary>
        /// <value>
        /// The root dispatcher.
        /// </value>
        public static Dispatcher RootDispatcher
        {
            get {
                return _rootDispatcher ??
                       (_rootDispatcher =
                           App.Current != null ? App.Current.Dispatcher : new Application().Dispatcher);
            }
        }
    }
}