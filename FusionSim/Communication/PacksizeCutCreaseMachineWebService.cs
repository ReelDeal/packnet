﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PackNet.FusionSim.Models;
using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PackNet.FusionSim.Communication
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class PacksizeCutCreaseMachineWebService : IFusionWebService
    {
        private readonly IPacksizeCommunicationBasedSimulator simulator;

        /// <summary>
        /// Initializes a new instance of the <see cref="PacksizeCutCreaseMachineWebService" /> class.
        /// </summary>
        /// <param name="simulator">The simulator.</param>
        /// <exception cref="System.ArgumentNullException">simulator</exception>
        public PacksizeCutCreaseMachineWebService(IPacksizeCommunicationBasedSimulator simulator)
        {
            if (simulator == null)
            {
                throw new ArgumentNullException("simulator");
            }

            this.simulator = simulator;
        }

        /// <summary>
        /// Gets the variable to the machine.
        /// </summary>
        /// <returns>
        /// The value of the variable.
        /// </returns>
        public string GetVariable()
        {
            if (WebOperationContext.Current == null)
            {
                return "0";
            }

            var s = WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri.ToString();
            var json = s.Substring(s.IndexOf("GetVariable?", StringComparison.Ordinal) + 12);

            var ret = JsonConvert.SerializeObject(simulator.Get(json)) ?? "0";

            return ret;
        }

        /// <summary>
        /// Posts the variable to the machine.
        /// </summary>
        /// <param name="contents">The contents.</param>
        /// <returns></returns>
        public Stream PostVariable(Stream contents)
        {
            var input = new StreamReader(contents).ReadToEnd();
            var deserialized = JsonConvert.DeserializeObject<JObject>(input);

            foreach (var child in deserialized.Children())
            {
                var childObject = child as JProperty;
                if (childObject != null && childObject.HasValues)
                {
                    var childValue = childObject.Value;
                    var childName = childObject.Name;

                    simulator.Set(childName, childValue);
                }
            }

            return new MemoryStream(Encoding.UTF8.GetBytes("{\"Status\":true}"));
        }
    }
}
