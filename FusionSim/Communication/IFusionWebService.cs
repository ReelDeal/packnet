﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace PackNet.FusionSim.Communication
{
    [ServiceContract]
    public interface IFusionWebService
    {
        /// <summary>
        /// Gets the variable to the machine.
        /// </summary>
        /// <returns>The value of the variable.</returns>
        [OperationContract]
        [WebGet(
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        string GetVariable();

        /// <summary>
        /// Posts the variable to the machine.
        /// </summary>
        /// <param name="contents">The contents.</param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(
            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json)]
        Stream PostVariable(Stream contents);
    }
}
