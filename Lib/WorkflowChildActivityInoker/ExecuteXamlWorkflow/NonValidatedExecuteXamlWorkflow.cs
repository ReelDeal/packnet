﻿//-----------------------------------------------------------------------
// <copyright file="ExecuteXamlWorkflow.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System.Reflection;

using PackNet.Common;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace Microsoft.Consulting.Workflow.Activities
{
    using System;
    using System.Activities;
    using System.Collections.Generic;

    /// <summary>
    /// Implements an activity that can execute a XAML file based workflow.
    /// </summary>
    public sealed class NonValidatedExecuteXamlWorkflow : CodeActivity
    {
        [RequiredArgument]
        public InArgument<Dictionary<string, Argument>> ChildArguments { get; set; }
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<string> WorkflowPath { get; set; }

        private static ILogger logger = null;

        /// <summary>
        /// Execute the child workflow through to completion.
        /// </summary>
        /// <param name="context">The execution context in which the activity executes.</param>
        protected override void Execute(CodeActivityContext context)
        {
            try
            {
                var inArgs = new Dictionary<string, object>();
                if (logger == null)
                {
                    logger = ServiceLocator.Get(context).Locate<ILogger>();
                }

                if (logger != null)
                    logger.Log(LogLevel.Debug, "ExecuteXamlWorkflow Workflow:{0}", WorkflowPath);

                foreach (string argumentKey in ChildArguments.Get(context).Keys)
                {
                    if (ChildArguments.Get(context)[argumentKey].Direction != ArgumentDirection.Out)
                    {
                        object value = ChildArguments.Get(context)[argumentKey].Get(context);
                        if (value == null && logger != null)
                            logger.Log(LogLevel.Warning, "ExecuteXamlWorkflow Workflow:{0}; InArgument:'{1}' is null", WorkflowPath, argumentKey);

                        inArgs.Add(argumentKey, value);
                    }
                }
                IDictionary<string, object> outArgs;

                DynamicActivity dynamicActivity = LoadDynamicActivityFromCache(context);

                if (dynamicActivity != null)
                {
                    var invoker = new WorkflowInvoker(dynamicActivity);
                    outArgs = invoker.Invoke(inArgs);

                    foreach (string argumentKey in outArgs.Keys)
                    {
                        ChildArguments.Get(context)[argumentKey].Set(context, outArgs[argumentKey]);
                    }
                }
            }
            catch (Exception e)
            {
                if (logger != null)
                    logger.LogException(LogLevel.Error, "ExecuteXamlWorkflow threw unhandled exception invoking:{0}", e, WorkflowPath);
                throw;
            }

        }

        /// <summary>
        /// Loads the dynamic activity from cache.
        /// </summary>
        /// <param name="context"></param>
        /// <returns>The activity or null if WorkflowPath property does not have a value.</returns>
        private DynamicActivity LoadDynamicActivityFromCache(CodeActivityContext context)
        {
            if (WorkflowPath != null && !string.IsNullOrEmpty(WorkflowPath.Get(context)))
            {
                return WorkflowHelper.GetActivity(DynamicActivityStore.ReplaceEnvironmentVariables(WorkflowPath.Get(context)), Assembly.GetExecutingAssembly()) as DynamicActivity;
                //DynamicActivityStore.GetActivity(this.WorkflowPath);
            }
            else
            {
                return null;
            }
        }
    }
}
