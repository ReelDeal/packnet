﻿//-----------------------------------------------------------------------
// <copyright file="ExecuteXamlWorkflow.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Tracking;
using System.Activities.Validation;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Microsoft.Consulting.Workflow.Activities.Properties;
using PackNet.Common;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace Microsoft.Consulting.Workflow.Activities
{
    /// <summary>
    /// Implements an activity that can execute a XAML file based workflow.
    /// </summary>
    [Designer(typeof(ExecuteXamlWorkflowDesigner))]
    public sealed class ExecuteXamlWorkflow : NativeActivity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ExecuteXamlWorkflow"/> class.
        /// </summary>
        public ExecuteXamlWorkflow()
        {
            WorkflowPath = Environment.GetEnvironmentVariable("PackNetWorkflows");
            this.ChildArguments = new Dictionary<string, Argument>();
        }

        /// <summary>
        /// Gets or sets the file path that specifies the location of the child XAML workflow to execute.
        /// </summary>
        /// <value>
        /// The workflow path.
        /// </value>
        public string WorkflowPath { get; set; }
       
        /// <summary>
        /// Gets or sets the arguments of the child workflow.
        /// </summary>
        /// <value>
        /// The child arguments.
        /// </value>
        public Dictionary<string, Argument> ChildArguments { get; set; }

        /// <summary>
        /// Creates and validates a description of the activity’s arguments, variables, child activities, and activity delegates.
        /// </summary>
        /// <param name="metadata">The activity’s metadata that encapsulates the activity’s arguments, variables, child activities, and activity delegates.</param>
        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {

            var containsServiceLocator = false;
            foreach (string argumentKey in this.ChildArguments.Keys)
            {
                var argument = this.ChildArguments[argumentKey];
                var runtimeArgument = new RuntimeArgument(argumentKey, argument.ArgumentType, argument.Direction);
                if (argument.ArgumentType == typeof(IServiceLocator))
                {
                    containsServiceLocator = true;
                    serviceLocatorKey = argumentKey;
                }
                metadata.Bind(argument, runtimeArgument);
                metadata.AddArgument(runtimeArgument);
            }

            if (!containsServiceLocator)
            {
                serviceLocatorKey = "ServiceLocator";
                this.ChildArguments.Add(serviceLocatorKey, new InArgument<IServiceLocator>());
                var argument = this.ChildArguments[serviceLocatorKey];
                var runtimeArgument = new RuntimeArgument(serviceLocatorKey, argument.ArgumentType, argument.Direction);
                metadata.Bind(argument, runtimeArgument);
                metadata.AddArgument(runtimeArgument);
            }

            try
            {
                DynamicActivity dynamicActivity = this.LoadDynamicActivityFromCache();

                if (dynamicActivity != null)
                {
                    this.Validate(metadata, dynamicActivity);
                }
                else
                {
                    metadata.AddValidationError(Resources.SpecifyValidWorkflowValidationErrorText);
                }
            }
            catch (Exception ex)
            {
                metadata.AddValidationError(string.Format(Resources.FailedToLoadWorkflowValidationErrorText, ex.Message));
            }
        }
        private string serviceLocatorKey;

        private static ILogger logger = null;
        private static IWorkflowTrackingService workflowTrackingService;
        private static IUserNotificationService userNotificationService;

        /// <summary>
        /// Execute the child workflow through to completion.
        /// </summary>
        /// <param name="context">The execution context in which the activity executes.</param>
        protected override void Execute(NativeActivityContext context)
        {
            try
            {
                var inArgs = new Dictionary<string, object>();


                //setup static variables
                if (logger == null || userNotificationService == null || workflowTrackingService == null)
                {
                    var sl = this.ChildArguments[serviceLocatorKey].Get<IServiceLocator>(context);
                    if (sl != null)
                    {
                        //setup static variables
                        logger = sl.Locate<ILogger>();
                        workflowTrackingService = sl.Locate<IWorkflowTrackingService>();
                        userNotificationService = sl.Locate<IUserNotificationService>();
                    }
                }

                if (logger != null)
                    logger.Log(LogLevel.Debug, "ExecuteXamlWorkflow Workflow:{0}", WorkflowPath);

                foreach (string argumentKey in this.ChildArguments.Keys)
                {
                    if (this.ChildArguments[argumentKey].Direction != ArgumentDirection.Out)
                    {
                        object value = this.ChildArguments[argumentKey].Get(context);
                        if (value == null && logger != null)
                            logger.Log(LogLevel.Warning, "ExecuteXamlWorkflow Workflow:{0}; InArgument:'{1}' is null", WorkflowPath, argumentKey);

                        inArgs.Add(argumentKey, value);
                    }
                }
                IDictionary<string, object> outArgs;

                DynamicActivity dynamicActivity = this.LoadDynamicActivityFromCache();

                if (dynamicActivity != null)
                {
                   outArgs = WorkflowHelper.InvokeWorkFlowAndWaitForResult(WorkflowPath, this.GetType().Assembly, inArgs,
                       userNotificationService, logger);

                    foreach (string argumentKey in outArgs.Keys)
                    {
                        this.ChildArguments[argumentKey].Set(context, outArgs[argumentKey]);
                    }
                }
            }
            catch (Exception e)
            {
                if (logger != null)
                    logger.LogException(LogLevel.Error, "ExecuteXamlWorkflow threw unhandled exception invoking:{0}", e, WorkflowPath);
                throw;
            }

        }

        /// <summary>
        /// Loads the dynamic activity from cache.
        /// </summary>
        /// <returns>The activity or null if WorkflowPath property does not have a value.</returns>
        private DynamicActivity LoadDynamicActivityFromCache()
        {
            if (!string.IsNullOrWhiteSpace(this.WorkflowPath))
            {
                string physicalPath = DynamicActivityStore.ReplaceEnvironmentVariables(WorkflowPath);

                if (!File.Exists(physicalPath))
                {
                    Console.WriteLine("Missing XAML File: {0}", physicalPath);
                }

                return WorkflowHelper.GetActivity(physicalPath, Assembly.GetExecutingAssembly()) as DynamicActivity;
                //DynamicActivityStore.GetActivity(this.WorkflowPath);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Validates the arguments in ChildArguments property against the arguments of specified dynamicActivity instance by adding a validation error
        /// to supplied metadata if the argument is wrong type, direction or does not exist.
        /// </summary>
        /// <param name="metadata">The metadata.</param>
        /// <param name="dynamicActivity">The dynamic activity.</param>
        private void Validate(NativeActivityMetadata metadata, DynamicActivity dynamicActivity)
        {
            foreach (string argumentKey in this.ChildArguments.Keys)
            {
                if (dynamicActivity.Properties.Contains(argumentKey))
                {
                    DynamicActivityProperty dynamicActivityProperty = dynamicActivity.Properties[argumentKey];
                    Argument arg = this.ChildArguments[argumentKey];
                    if (dynamicActivityProperty.Type.GetGenericTypeDefinition() == typeof(InArgument<>) && arg.Direction != ArgumentDirection.In)
                    {
                        metadata.AddValidationError(new ValidationError(string.Format(Resources.InvalidInArgumentDirectionValidationErrorText, argumentKey)));
                    }
                    else if (dynamicActivityProperty.Type.GetGenericTypeDefinition() == typeof(OutArgument<>) && arg.Direction != ArgumentDirection.Out)
                    {
                        metadata.AddValidationError(new ValidationError(string.Format(Resources.InvalidOutArgumentDirectionValidationErrorText, argumentKey)));
                    }
                    else if (dynamicActivityProperty.Type.GetGenericTypeDefinition() == typeof(InOutArgument<>) && arg.Direction != ArgumentDirection.InOut)
                    {
                        metadata.AddValidationError(new ValidationError(string.Format(Resources.InvalidInOutArgumentDirectionValidationErrorText, argumentKey)));
                    }

                    if (dynamicActivityProperty.Type.GetGenericArguments()[0] != arg.ArgumentType)
                    {
                        metadata.AddValidationError(new ValidationError(string.Format(Resources.InvalidIArgumentTypeValidationErrorText, argumentKey, dynamicActivityProperty.Type.GetGenericArguments()[0])));
                    }
                }
                else
                {
                    metadata.AddValidationError(new ValidationError(string.Format(Resources.InvalidIArgumentValidationErrorText, argumentKey)));
                }
            }
        }
    }
}
