﻿//-----------------------------------------------------------------------
// <copyright file="ExecuteXamlWorkflowDesigner.xaml.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation
// All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY 
// KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------
namespace Microsoft.Consulting.Workflow.Activities
{
    using System.Activities;
    using System.Activities.Presentation;
    using System.Activities.Presentation.Model;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows;

    /// <summary>
    /// Implements a custom designer for the ExecuteXamlWorkflow activity.
    /// </summary>
    public partial class ExecuteXamlWorkflowDesigner
    {
        /// <summary>
        /// The file path of a the currently specified child workflow.
        /// </summary>
        private string currentlyLoadedWorkflowPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExecuteXamlWorkflowDesigner"/> class.
        /// </summary>
        public ExecuteXamlWorkflowDesigner()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(this.ExecuteXamlWorkflowDesigner_Loaded);
        }

        /// <summary>
        /// Handles the Loaded event of the ExecuteXamlWorkflowDesigner control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        public void ExecuteXamlWorkflowDesigner_Loaded(object sender, RoutedEventArgs e)
        {
            this.ModelItem.PropertyChanged += new PropertyChangedEventHandler(this.ModelItem_PropertyChanged);
        }

        /// <summary>
        /// Handles the PropertyChanged event of the ModelItem control and detects if the workflow path has been modified. If changed, initialises
        /// the DynamicArgumentDialog with the newly identified child's workflow arguments.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        private void ModelItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("WorkflowPath"))
            {
                this.InitDynamicArgumentDialog();
            }
        }

        /// <summary>
        /// Handles the Click event of the FileDialogButton control to launch an OpenFileDialog instance.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void FileDialogButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog fileDialog = new Microsoft.Win32.OpenFileDialog();
            if (fileDialog.ShowDialog() == true)
            {
                this.ModelItem.Properties["WorkflowPath"].SetValue(fileDialog.FileName);       
            }
        }

        /// <summary>
        /// Handles the Click event of the DefineArgsButton control to launch a DynamicArgumentDialog instance for argument editing.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Windows.RoutedEventArgs"/> instance containing the event data.</param>
        private void DefineArgsButton_Click(object sender, RoutedEventArgs e)
        {
            DynamicArgumentDesignerOptions options = new DynamicArgumentDesignerOptions()
            {
                Title = Microsoft.Consulting.Workflow.Activities.Properties.Resources.DynamicArgumentDialogTitle
            };

            ModelItem modelItem = this.ModelItem.Properties["ChildArguments"].Dictionary;
            using (ModelEditingScope change = modelItem.BeginEdit("ChildArgumentEditing"))
            {
                if (DynamicArgumentDialog.ShowDialog(this.ModelItem, modelItem, Context, this.ModelItem.View, options))
                {
                    change.Complete();
                }
                else
                {
                    change.Revert();
                }
            }
        }

        /// <summary>
        /// Initialises the DynamicArgumentDialog instance with child workflow arguments by loading the specified child workflow then deriving its arguments.
        /// </summary>
        private void InitDynamicArgumentDialog()
        {
            string workflowPath = this.ModelItem.Properties["WorkflowPath"].Value.GetCurrentValue() as string;

            if (workflowPath != this.currentlyLoadedWorkflowPath)
            {
                Dictionary<string, Argument> argumentDictionary = new Dictionary<string, Argument>();
                this.ModelItem.Properties["ChildArguments"].SetValue(argumentDictionary);
                this.currentlyLoadedWorkflowPath = workflowPath;

                try
                {
                    DynamicActivity dynamicActivity = DynamicActivityStore.GetActivity(workflowPath);
                    if (dynamicActivity != null)
                    {
                        foreach (DynamicActivityProperty property in dynamicActivity.Properties)
                        {
                            Argument newArgument = null;
                            if (property.Type.GetGenericTypeDefinition().BaseType == typeof(InArgument))
                            {
                                newArgument = Argument.Create(property.Type.GetGenericArguments()[0], ArgumentDirection.In);
                            }

                            if (property.Type.GetGenericTypeDefinition().BaseType == typeof(OutArgument))
                            {
                                newArgument = Argument.Create(property.Type.GetGenericArguments()[0], ArgumentDirection.Out);
                            }

                            if (property.Type.GetGenericTypeDefinition().BaseType == typeof(InOutArgument))
                            {
                                newArgument = Argument.Create(property.Type.GetGenericArguments()[0], ArgumentDirection.InOut);
                            }

                            if (newArgument != null)
                            {
                                argumentDictionary.Add(property.Name, newArgument);
                            }
                        }
                    }
                }
                catch
                {
                    // ignore load failures - leave to handle in CacheMetadata
                }
            }
        }
    }
}