﻿General Description:
--------------------
This SharpSnmp library has been forked in order to make it work with BR devices. The original source page was: https://sharpsnmplib.codeplex.com/

The BR devices very loosely follow SNMP v3. Forking was done to allow the BR devices to communicate via raw socket format without all of the
additional network packet stuff that C# straps on to payloads (additional IP and UDP protocol segments). You can sniff these differences out
in WireShark by comparing a C# socket with the actual calls made when discovering. 

This SharpSnmp library is used to assemble the packets with the correct variable strings, then PcapDotNet (essentially a C# tie-in to WinPcap)
is used to send and receive them. Finally SharpSnmp is used again to interpret results.

This library deals with several idiosyncracies of the BR devices such as no security, no formal report/handshake before data interchange, etc.

Usage and Expected behaviors:
-----------------------------
Real result from a discovery call with the returned OID values in the BR device's MIB:
.1.3.6.1.4.1.29312.1.1.0 => X20CP1584 //model number
.1.3.6.1.4.1.29312.1.2.0 => C3700168838 //serial number
.1.3.6.1.4.1.29312.1.3.0 => CC03587882500317YH08 //unknown
.1.3.6.1.4.1.29312.1.4.1.0 => V04.02 //version
.1.3.6.1.4.1.29312.1.4.2.0 => 04.02.22 //additional version info
.1.3.6.1.4.1.29312.1.4.3.0 => 3 //unknown
.1.3.6.1.4.1.29312.1.4.4.0 => 40 //unknown
.1.3.6.1.4.1.29312.1.5.1.0 => IF2 //network interface name
.1.3.6.1.4.1.29312.1.5.10.0 => 11159 //unknown
.1.3.6.1.4.1.29312.1.5.11.0 => 2 //unknown
.1.3.6.1.4.1.29312.1.5.2.0 => 00-60-65-17-e4-a8 //mac address
.1.3.6.1.4.1.29312.1.5.3.0 => 0 //unknown
.1.3.6.1.4.1.29312.1.5.4.0 => 10.241.241.230 // ip v4 address
.1.3.6.1.4.1.29312.1.5.5.0 => 255.255.255.6 //ip v4 subnet
.1.3.6.1.4.1.29312.1.5.6.0 => 0 //unknown
.1.3.6.1.4.1.29312.1.5.7.0 => 0 //unknown
.1.3.6.1.4.1.29312.1.5.8.0 => 0 //unknown
.1.3.6.1.4.1.29312.1.5.9.0 => 1 //unknown
.1.3.6.1.4.1.29312.1.6.1.0 => br-automation //hostname
.1.3.6.1.4.1.29312.1.6.3.0 => 0 //unknown
.1.3.6.1.4.1.29312.1.6.5.0 => 0 //unknown

IP Address set results
----------------------
When a call is made to set the IP v4 address the value for .1.3.6.1.4.1.29312.1.5.4.0
=> 10.241.241.230 will change to the new value when the device is rediscovered. If the
call was made to set the IP v4 address to 10.241.241.231 the result would show:
.1.3.6.1.4.1.29312.1.5.4.0 => 10.241.241.231 A few seconds after the change the device
should be reachable over port 80 with this url:
http://10.241.241.231/sdm/index.html

Subnet Address set results
----------------------
When a call is made to set the IP subnet the value for .1.3.6.1.4.1.29312.1.5.5.0 =>
255.255.255.6 will change to the new value when the device is rediscovered. If the call
was made to set the IP v4 subnet to 255.255.255.7 the result would show:
.1.3.6.1.4.1.29312.1.5.5.0 => 255.255.255.7

What do the OID values mean and how can I interpret them?
---------------------------------------------------------
The OID values are often manufacturer specific. To translate the result into something
meaningful (like for a UI) you can use the translation dictionary values like this:
brDeviceDiscovered.DeviceAttributes[BrDeviceOid.HostName]. See the example below:

private const int TimeoutInSeconds = 3;
private BrDeviceSnmpAdministrator _brDeviceSnmpAdministrator; 
        
static Program()
{
    AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
}

static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
{
    Console.WriteLine("Top Level Exception: {0}", e);
}

public Program()
{
    _brDeviceSnmpAdministrator = new BrDeviceSnmpAdministrator();
}

public static void Main()
{
    Program program = new Program();
    program.DiscoverDevicesDemo();
}

private void DiscoverDevicesDemo()
{
    Console.WriteLine("--- Start DiscoverDevicesDemo ---");
    Console.WriteLine("Finding Devices. Please wait...");

    var machines = _brDeviceSnmpAdministrator.DiscoverDevices();
    int deviceCount = 0;

    foreach (var result in machines)
    {
        deviceCount++;
        DisplayMachineDetail(deviceCount, result);
    }

    Console.WriteLine("{0} devices found.", deviceCount);
    Console.WriteLine(deviceCount > 0 ? "Test passed" : "Test failed");
    Console.WriteLine("--- End DiscoverDevicesDemo ---");
}

private static void DisplayMachineDetail(int i, BrDeviceDiscovered brDeviceDiscovered)
{
    Console.WriteLine("------------------");
    Console.WriteLine("Machine {0}", i);
    Console.WriteLine("-------------", i);
    Console.WriteLine("{0}: {1}", BrDeviceOid.HostName.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.HostName]);
    Console.WriteLine("{0}: {1}", BrDeviceOid.IpV4Address.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.IpV4Address]);
    Console.WriteLine("{0}: {1}", BrDeviceOid.IpV4Subnet.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.IpV4Subnet]);
    Console.WriteLine("{0}: {1}", BrDeviceOid.MacAddress.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.MachineMacAddress]);
    Console.WriteLine("{0}: {1}", BrDeviceOid.ModelNumber.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.ModelNumber]);
    Console.WriteLine("{0}: {1}", BrDeviceOid.NetworkInterfaceName.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.NetworkInterfaceName]);
    Console.WriteLine("{0}: {1}", BrDeviceOid.SerialNumber.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.SerialNumber]);
    Console.WriteLine("{0}: {1}", BrDeviceOid.Version.Name, brDeviceDiscovered.DeviceAttributes[BrDeviceOid.Version]);
    Console.WriteLine("------------------");
}

How does it work?
-----------------
The library SharpSnmpLib.Full.ForkedForBrDevices uses raw SNMP packets sent to the
broadcast MAC address (all ff's). Devices which understand the SNMP packets will
respond with corresponding values regardless of TCP/IP configuration. The packets
must be sent raw in order for them to be acknowledged by BR devices and the sharpsnmplib
had major issues with this. WinPCapDotNet is used to send and receive the bytes.
The bytes are translated back to meaningful values with the SharpSnmpLib library.

Where did other sources come from?
----------------------------------
The library SharpSnmpLib.Full.ForkedForBrDevices has been forked in order to make it work
with BR devices. The original source page was: https://sharpsnmplib.codeplex.com/ I could
not find a complete source set for a working version so I had to de-compile a DLL version
from the same site. PcapDotNet is a C# port referencing the ever-popular wpcap.dll file
used in tools like Wireshark.

Caveats and Limitations:
------------------------
1) In order to traverse network switches, firewalls and other equipment so devices can
be discovered and manipulated, ports 161 and 162 may need to be allowed.
2) The BR devices use a loose implementation of SNMP v3 without security. The sharpsnmp
library has been forked _significantly_ to accommodate this so that the BR devices which
pass a plain text username like burUser and engine like burContext can communicate.
3) The BR devices are pretty much GET and SET requests only. Discovery is done through
a GET operation passing multiple known-good-value variables. The BR devices don't seem
to care about the typical two-way SNMP id's or time values.