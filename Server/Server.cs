﻿using System.Linq;

using Autofac;
using Autofac.Core;
using MongoDB.Bson.Serialization;
using PackNet.Business.ArticleManagement;
using PackNet.Business.CartonPropertyGroup;
using PackNet.Business.Classifications;
using PackNet.Business.Corrugates;
using PackNet.Business.DatabaseTask;
using PackNet.Business.ExternalSystems;
using PackNet.Business.Machines;
using PackNet.Business.Orders;
using PackNet.Business.PackagingDesigns;
using PackNet.Business.PickZone;
using PackNet.Business.ProductionGroups;
using PackNet.Business.Scheduler;
using PackNet.Business.SelectionAlgorithms;
using PackNet.Business.Settings;
using PackNet.Business.Support;
using PackNet.Business.Templates;
using PackNet.Business.UserManagement;
using PackNet.Business.WmsIntegration;
using PackNet.Business.WmsIntegration.MessageProducer;
using PackNet.Business.Workflows;
using PackNet.Common.Caching;
using PackNet.Common.Communication;
using PackNet.Common.Communication.RabbitMQ;
using PackNet.Common.Eventing;
using PackNet.Common.FileHandling;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Caching;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.FileHandling;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.ExternalSystems;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Logging;
using PackNet.Common.Logging.NLogBackend;
using PackNet.Common.Utils;
using PackNet.Communication;
using PackNet.Data;
using PackNet.Data.ArticleManagement;
using PackNet.Data.BoxFirstProducible;
using PackNet.Data.CartonPropertyGroups;
using PackNet.Data.Classifications;
using PackNet.Data.ComponentsConfigurations;
using PackNet.Data.Corrugates;
using PackNet.Data.ExternalSystems;
using PackNet.Data.Machines;
using PackNet.Data.Orders;
using PackNet.Data.PickArea;
using PackNet.Data.PrintMachines;
using PackNet.Data.Producibles;
using PackNet.Data.ProductionGroups;
using PackNet.Data.ScanToCreateProducible;
using PackNet.Data.Scheduler;
using PackNet.Data.Settings;
using PackNet.Data.Support;
using PackNet.Data.Templates;
using PackNet.Data.UserManagement;
using PackNet.Data.Workflows;
using PackNet.Importer;
using PackNet.Server.Bootstrapper;
using PackNet.Server.Properties;
using PackNet.Services;
using PackNet.Services.Articles;
using PackNet.Services.ExternalSystems;
using PackNet.Services.Importer;
using PackNet.Services.MachineServices;
using PackNet.Services.MachineServices.CutCreaseMachines;
using PackNet.Services.PrinterServices;
using PackNet.Services.ProductionGroup;
using PackNet.Services.Scheduler;
using PackNet.Services.SelectionAlgorithm;
using PackNet.Services.SelectionAlgorithm.BoxFirst;
using PackNet.Services.SelectionAlgorithm.BoxLast;
using PackNet.Services.SelectionAlgorithm.Orders;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using PackNet.Business.UserNotifications;
using PackNet.Data.UserNotifications;

namespace PackNet.Server
{
    using Services.CodeGeneration;

    public class Server : IDisposable
    {
        private readonly ContainerBuilder containerBuilder = new ContainerBuilder();
        private IContainer iocContainer;
        private readonly EventAggregator eventAggregator = new EventAggregator();
        private const string ComponentName = "PackNet.Server";
        // ReSharper disable NotAccessedField.Local
        //These Services need to exist in scope!!! They are used even though resharper says otherwise
        private IUserService userService;
        private IDataSupplierService fileDataSupplierService;
        private IImporterService importerService;
        private ISelectionAlgorithmDispatchService selectionAlgorithmDispatchService;
        private IMachineGroupService machineGroupService;
        private ICodeGenerationService codeGenerationService;
        private ICorrugateService corrugateService;
        private IServiceLocator serviceLocator;
        private readonly ILogger logger;
        private IBoxLastSelectionAlgorithmService boxLastSelectionAlgorithmService;
        private IBoxFirstSelectionAlgorithmService boxFirstSelectionAlgorithmService;
        private IScanToCreateSelectionAlgorithmService scanToCreateSelectionAlgorithmService;
        private IFusionMachineService fusionMachineService;
        private IPacksizeMachineRESTService machineREStService;
        private IEmMachineService emMachineService;
        private IZebraPrinterMachineService zebraPrinterMachineService;
        private IPackNetServerSettingsService packNetServerSettingsService;
        private IRestrictionResolverService restrictionResolverService;
        private IPackagingDesignService packagingDesignService;
        private ICartonPropertyGroupService cartonPropertyGroupService;
        private IPickZoneService pickZoneService;
        private IOrdersSelectionAlgorithmService ordersSelectionAlgorithmService;
        private IUserNotificationService userNotificationService;
        private IClassificationService classificationService;
        private IExternalCustomerSystemService externalCustomerSystemService;
        private IArticleService articleService;
        private ICloudReportingService cloudReportingService;
        private ITemplateService templateService;
        private IPhysicalMachineSettingsService physicalMachineSettingsService;
        private readonly IBootstrap bootstrapper;
        // ReSharper restore NotAccessedField.Local

        /// <summary>
        /// Main entry for PackNet.Server
        /// </summary>
        public Server()
        {
            try
            {
                //ThreadPool.SetMaxThreads(1024, 1024);
                ServicePointManager.Expect100Continue = false;
                TaskScheduler.UnobservedTaskException += (sender, e) =>
                {
                    e.SetObserved();
                    Report(e.Exception, LogLevel.Fatal);
                };
                AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
                {
                    Report((Exception)e.ExceptionObject, LogLevel.Fatal);
                };
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.Language);
                logger = CreateLogger();
                bootstrapper = new Bootstrapper.Bootstrapper(new RegistryHelper(), new ServiceHelper(), new MachineHelper(), logger);
                bootstrapper.ValidateServerReadiness();
                RegisterItemsWithIocContainer();
            }
            catch (Exception e)
            {
                Report(e, LogLevel.Fatal);
                Application.Current.Shutdown();
            }
        }

        //TODO: Move this to event aggregator... Logging or Error service???
        public void Report(Exception e, LogLevel logLevel, bool displayStackTrace = true)
        {
            EventLog.WriteEntry("PackNet.Server", e.ToString());
            logger.Log(logLevel, e.ToString());
            Trace.WriteLine(e);
            if (logLevel == LogLevel.Fatal)
                Environment.Exit(1);
        }

        /// <summary>
        /// Registers the application objects with the IOC container
        /// </summary>
        private void RegisterItemsWithIocContainer()
        {
            RegisterInfrastructureItems();
            RegisterRepositories();
            RegisterBusinessClasses();
            RegisterServices();
            RegisterImporter();
            RegisterSelectionAlgorithmWorkflows();
            RegisterRabbitCommunication();

            containerBuilder.Register(c => new PackagingDesignManager(Settings.Default.DesignsPath, new MemoryCache(c.Resolve<ILogger>(), Settings.Default.DesignManagerCacheName), c.Resolve<ILogger>()))
                .As<IPackagingDesignManager>()
                .SingleInstance();
            
            containerBuilder.RegisterType<OptimalCorrugateCalculator>().As<IOptimalCorrugateCalculator>().SingleInstance();

            //WMS crap --really printer stuff... needs some tlc
            containerBuilder.RegisterType<TagResolver>().As<ITagResolver>().SingleInstance();
            containerBuilder.RegisterType<AnswerResolver>().As<IAnswerResolver>().SingleInstance();

            containerBuilder.Register(c =>
                {
                    var socketSettings = c.Resolve<ISocketCommunicatorSettings>();
                    return new SocketWrapper(socketSettings.AddressFamily, socketSettings.SocketType, socketSettings.ProtocolType);
                })
                .As<ISocketWrapper>()
                .SingleInstance();

            iocContainer = containerBuilder.Build();

            //Additional setup --  ORDER MATTERS HERE 
            iocContainer.Resolve<IPackagingDesignManager>().DesignLoadError += message => iocContainer.Resolve<ILogger>().Log(LogLevel.Error, message);
            iocContainer.Resolve<IPackagingDesignManager>().DesignLoadError +=
                message =>
                    iocContainer.Resolve<IUserNotificationService>().SendNotificationToSystem(NotificationSeverity.Error, message, string.Empty);
            iocContainer.Resolve<IPackagingDesignManager>().LoadDesigns();
            CreateServicesInstances();
            serviceLocator = iocContainer.Resolve<IServiceLocator>();
        }

        /// <summary>
        /// Creates instances of services that would otherwise not be created in the IOC Container
        /// </summary>
        private void CreateServicesInstances()
        {
            restrictionResolverService = iocContainer.Resolve<IRestrictionResolverService>();
            userService = iocContainer.Resolve<IUserService>();
            var rs = iocContainer.Resolve<RabbitSubscriber>();
            

            userNotificationService = iocContainer.Resolve<IUserNotificationService>();

            selectionAlgorithmDispatchService = iocContainer.Resolve<ISelectionAlgorithmDispatchService>();
            iocContainer.Resolve<IAggregateMachineService>();

            //Machine communication
            machineREStService = iocContainer.Resolve<IPacksizeMachineRESTService>();

            //TODO: Register machine services with machine controller and load via mef
            fusionMachineService = iocContainer.Resolve<IFusionMachineService>();
            emMachineService = iocContainer.Resolve<IEmMachineService>();
            zebraPrinterMachineService = iocContainer.Resolve<IZebraPrinterMachineService>();

            //Machine group needs to start after all machine services are created 
            machineGroupService = iocContainer.Resolve<IMachineGroupService>();
            codeGenerationService = iocContainer.Resolve<ICodeGenerationService>();

            pickZoneService = iocContainer.Resolve<IPickZoneService>();
            cartonPropertyGroupService = iocContainer.Resolve<ICartonPropertyGroupService>();
            packagingDesignService = iocContainer.Resolve<IPackagingDesignService>();
            corrugateService = iocContainer.Resolve<ICorrugateService>();
            classificationService = iocContainer.Resolve<IClassificationService>();
            
            //Selection algorithms
            boxLastSelectionAlgorithmService = iocContainer.Resolve<IBoxLastSelectionAlgorithmService>();
            ordersSelectionAlgorithmService = iocContainer.Resolve<IOrdersSelectionAlgorithmService>();
            boxFirstSelectionAlgorithmService = iocContainer.Resolve<IBoxFirstSelectionAlgorithmService>();
            scanToCreateSelectionAlgorithmService = iocContainer.Resolve<IScanToCreateSelectionAlgorithmService>();

            articleService = iocContainer.Resolve<IArticleService>();

            cloudReportingService = iocContainer.Resolve<ICloudReportingService>();

            // Monitoring
            iocContainer.Resolve<IWorkflowMonitoringService>();
            iocContainer.Resolve<IWorkflowTrackingService>();

            //Physical Machine Settings
            physicalMachineSettingsService = iocContainer.Resolve<IPhysicalMachineSettingsService>();

            //Settings
            packNetServerSettingsService = iocContainer.Resolve<IPackNetServerSettingsService>();
            Networking.CheckIpAddressAndWarnIfNotBoundToThisMachine(packNetServerSettingsService.GetSettings().MachineCallbackIpAddress, logger);

            //Templates
            templateService = iocContainer.Resolve<ITemplateService>();

            //Support
            iocContainer.Resolve<SupportIncidentService>();
            iocContainer.Resolve<IDatabaseTasksService>();

            iocContainer.Resolve<ISchedulerService>();

            externalCustomerSystemService = iocContainer.Resolve<IExternalCustomerSystemService>();
            //DO LAST!!!
            //Order matters here, File importing should happen last so everything is wired up in time for imports
            fileDataSupplierService = iocContainer.Resolve<IDataSupplierService>();
            importerService = iocContainer.Resolve<IImporterService>();
            iocContainer.Resolve<IServiceLocator>().LoadPlugins();

            //todo: move to a better place but for workflow tracking we need to setup class maps for all services that could be passed to the service locator and loaded in workfows.
            SetupServiceClassmaps();
        }
        class ServiceBsonClassMap : BsonClassMap {
            public ServiceBsonClassMap(Type classType) : base(classType)
            {
                base.MapProperty("Name");
            }
        }
        private void SetupServiceClassmaps()
        {
            var type = typeof(IService);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface);
            
            types.ForEach(service =>
            {
                BsonClassMap.RegisterClassMap(new ServiceBsonClassMap(service));
            });
        }
        
        /// <summary>
        /// Register items  with the IOC container that pertain to rabbit communication
        /// </summary>
        private void RegisterRabbitCommunication()
        {
            containerBuilder.RegisterType<RabbitConnectionManager>().SingleInstance();

            containerBuilder.RegisterType<RabbitSubscriber>()
                .As<RabbitSubscriber>()
                .As<IUICommunicationService>()
                .SingleInstance()
                .WithParameters(new List<Parameter>
                        {
                            new NamedParameter("hostName", Settings.Default.MessageQueueIPAddress),
                            new NamedParameter("exchangeName", "mm"),
                            new NamedParameter("routingKey", "mm.clients.facts")
                        });
        }

        /// <summary>
        /// Register items with the IOC container that pertain to the business layer (mostly dependencies)
        /// </summary>
        private void RegisterBusinessClasses()
        {
            containerBuilder.RegisterType<Articles>().As<IArticles>().SingleInstance();
            containerBuilder.RegisterType<Users>().As<IUsers>().SingleInstance();
            containerBuilder.RegisterType<Preferences>().As<IPreferences>().SingleInstance();
            containerBuilder.RegisterType<MemoryCache>().As<IMemoryCache>();
            containerBuilder.RegisterType<SupportIncidents>().As<ISupportIncidents>();
            containerBuilder.RegisterType<MachineGroupBl>().As<IMachineGroups>();
            containerBuilder.RegisterType<ProductionGroups>().As<IProductionGroup>();
            containerBuilder.RegisterType<FusionMachines>().As<IFusionMachines>();
            containerBuilder.RegisterType<EmMachines>().As<IEmMachines>();
            containerBuilder.RegisterType<ZebraPrintMachines>().As<IZebraPrintMachines>();
            containerBuilder.RegisterType<PackNetServerSettings>().As<IPackNetServerSettings>();
            containerBuilder.RegisterType<Classifications>().As<IClassifications>();
            containerBuilder.RegisterType<Templates>().As<ITemplates>();
            containerBuilder.RegisterType<Orders>().As<IOrders>();
            containerBuilder.RegisterType<DatabaseTasks>().As<IDatabaseTasks>();
            containerBuilder.RegisterType<Corrugates>().As<ICorrugates>().SingleInstance();
            containerBuilder.RegisterType<PickZones>().As<IPickZones>().SingleInstance();
            containerBuilder.RegisterType<CartonPropertyGroups>().As<ICartonPropertyGroups>().SingleInstance();
            containerBuilder.RegisterType<FileSystem>().As<IFileSystem>();
            containerBuilder.RegisterType<UserNotifications>().As<IUserNotifications>();
            containerBuilder.RegisterType<ExternalCustomerSystems>().As<IExternalCustomerSystems>();
            containerBuilder.RegisterType<TrackingParticipantBL>().As<ITrackingParticipantBL>().SingleInstance();
            containerBuilder.RegisterType<JobScheduler>().As<IJobScheduler>().SingleInstance();
        }

        /// <summary>
        /// Register items with the IOC container that pertain to importing
        /// </summary>
        private void RegisterImporter()
        {
            containerBuilder.Register(c => new ImporterPluginComposer(DirectoryHelpers.ReplaceEnvironmentVariables(Settings.Default.ImporterPluginDirectory), c.Resolve<ILogger>())).SingleInstance();
            containerBuilder.Register(
                c =>
                    new ImporterWorkflowDispatcher(
                        Settings.Default.ImporterPluginDirectory,
                        c.Resolve<ImporterPluginComposer>().Importers,
                        c.Resolve<IEventAggregatorPublisher>(),
                        iocContainer.Resolve<IServiceLocator>(),
                        c.Resolve<ILogger>()))
                .As<IImporterWorkflowDispatcher>().SingleInstance();
            containerBuilder.RegisterType<FileDataSupplierService>().As<IDataSupplierService>().SingleInstance();
            containerBuilder.RegisterType<ImporterService>().As<IImporterService>().SingleInstance();
        }

        private void RegisterSelectionAlgorithmWorkflows()
        {
            containerBuilder.Register(c => new SelectionAlgorithmComposer(
                        Settings.Default.SelectionAlgorithmPluginDirectory,
                        c.Resolve<ImporterPluginComposer>().Importers,
                        c.Resolve<ILogger>()));
            
            containerBuilder.Register(c =>
                new SelectionAlgorithmWorkflowDispatcher(
                    Settings.Default.SelectionAlgorithmPluginDirectory,
                    c.Resolve<IServiceLocator>(),
                    c.Resolve<IProductionGroupService>(),
                        c.Resolve<SelectionAlgorithmComposer>().SelectionAlgorithms,
                        c.Resolve<IEventAggregatorPublisher>(),
                        c.Resolve<ILogger>()));

            containerBuilder.RegisterType<SelectionAlgorithmDispatchService>().As<ISelectionAlgorithmDispatchService>().SingleInstance();
        }

        /// <summary>
        /// Register items with the IOC container that pertain to system services
        /// </summary>
        private void RegisterServices()
        {
            containerBuilder.RegisterType<CloudReportingService>().As<ICloudReportingService>().SingleInstance();
            containerBuilder.RegisterType<ArticleService>().As<IArticleService>().SingleInstance();
            containerBuilder.RegisterType<UserService>().As<IUserService>().SingleInstance();
            containerBuilder.RegisterType<AggregateMachineService>().As<IAggregateMachineService>().SingleInstance();
            containerBuilder.RegisterType<ProductionGroupService>().As<IProductionGroupService>().SingleInstance();
            containerBuilder.RegisterType<PickZoneService>().As<IPickZoneService>().SingleInstance();
            containerBuilder.RegisterType<CorrugateService>().As<ICorrugateService>().SingleInstance();
            containerBuilder.RegisterType<ClassificationsService>().As<IClassificationService>().SingleInstance();
            containerBuilder.RegisterType<CachingService>().As<ICachingService>().SingleInstance();
            containerBuilder.RegisterType<TemplateService>().As<ITemplateService>().SingleInstance();
            containerBuilder.RegisterType<UserNotificationService>().As<IUserNotificationService>().SingleInstance();
            containerBuilder.RegisterType<SupportIncidentService>().SingleInstance();
            containerBuilder.Register(c => new PacksizeMachineRestService(
                c.Resolve<IEventAggregatorPublisher>(), c.Resolve<IEventAggregatorSubscriber>(), c.Resolve<ILogger>(), c.Resolve<IPackNetServerSettingsService>().GetSettings())).As<IPacksizeMachineRESTService>();
            containerBuilder.RegisterType<PackNetServerSettingsService>().As<IPackNetServerSettingsService>().SingleInstance();
            containerBuilder.RegisterType<PackagingDesignService>().As<IPackagingDesignService>().SingleInstance();
            containerBuilder.RegisterType<PickZoneService>().As<IPickZoneService>().SingleInstance();
            containerBuilder.RegisterType<CartonPropertyGroupService>().As<ICartonPropertyGroupService>().SingleInstance();
            containerBuilder.RegisterType<MachineGroupWorkflowDispatcher>().As<IMachineGroupWorkflowDispatcher>().SingleInstance();
            containerBuilder.RegisterType<MachineGroupService>().As<IMachineGroupService>().SingleInstance();
            containerBuilder.RegisterType<RestrictionResolverService>().As<IRestrictionResolverService>().SingleInstance();
            containerBuilder.RegisterType<FusionMachineService>().As<IFusionMachineService>().SingleInstance();
            containerBuilder.RegisterType<EmMachineService>().As<IEmMachineService>().SingleInstance();
            containerBuilder.RegisterType<ZebraPrinterMachineService>().As<IZebraPrinterMachineService>().SingleInstance();
            containerBuilder.RegisterType<BoxLastSelectionAlgorithmService>().As<IBoxLastSelectionAlgorithmService>().SingleInstance();
            containerBuilder.RegisterType<OrdersSelectionAlgorithmService>().As<IOrdersSelectionAlgorithmService>().SingleInstance();
            containerBuilder.RegisterType<BoxFirstSelectionAlgorithmService>().As<IBoxFirstSelectionAlgorithmService>().SingleInstance();
            containerBuilder.RegisterType<ScanToCreateSelectionAlgorithmService>().As<IScanToCreateSelectionAlgorithmService>().SingleInstance();
            containerBuilder.RegisterType<DatabaseTasksService>().As<IDatabaseTasksService>().SingleInstance();
            containerBuilder.RegisterType<WorkflowMonitoringService>().As<IWorkflowMonitoringService>().SingleInstance();
            containerBuilder.RegisterType<WorkflowTrackingService>().As<IWorkflowTrackingService>().SingleInstance();
            containerBuilder.RegisterType<CodeGenerationService>().As<ICodeGenerationService>().SingleInstance();
            containerBuilder.RegisterType<ExternalCustomerSystemService>().As<IExternalCustomerSystemService>().SingleInstance();
            containerBuilder.Register(c => new MachineServicesImporter(Settings.Default.ServicesPluginDirectory, c.Resolve<IServiceLocator>(), c.Resolve<ILogger>())).As<IMachineServicesImporter>().SingleInstance(); ;
            containerBuilder.RegisterType<SchedulerService>().As<ISchedulerService>().SingleInstance();
            containerBuilder.RegisterType<PhysicalMachineSettingsService>().As<IPhysicalMachineSettingsService>().SingleInstance();
            
            RegisterServiceLocator();
        }

        private void RegisterServiceLocator()
        {
            containerBuilder.Register(
                c => new ServiceLocator(Path.Combine(Environment.CurrentDirectory, Settings.Default.ServicesPluginDirectory),
                    new List<IService>
                    {
                        c.Resolve<ILogger>(),
                        c.Resolve<IUICommunicationService>(),
                        c.Resolve<IEventAggregatorPublisher>(),
                        c.Resolve<IRestrictionResolverService>()

                    },
                    c.Resolve<ILogger>()))
                .As<IServiceLocator>().SingleInstance();
        }

        /// <summary>
        /// Register items with the IOC container that pertain to persistence
        /// </summary>
        private void RegisterRepositories()
        {
            containerBuilder.RegisterType<ArticleRepository>().As<IArticleRepository>().SingleInstance();
            containerBuilder.RegisterType<ClassificationRepository>().As<IClassificationRepository>().SingleInstance();
            containerBuilder.RegisterType<UserRepository>().As<IUserRepository>().SingleInstance();
            containerBuilder.RegisterType<PreferenceRepository>().As<IPreferenceRepository>().SingleInstance();
            containerBuilder.RegisterType<BoxLastRepository>().As<IBoxLastRepository>().SingleInstance();
            containerBuilder.RegisterType<BoxFirstRepository>().As<IBoxFirstRepository>().SingleInstance();
            containerBuilder.RegisterType<ScanToCreateRepository>().As<IScanToCreateRepository>().SingleInstance();
            containerBuilder.RegisterType<SupportIncidentRepository>().As<ISupportIncidentRepository>().SingleInstance();
            containerBuilder.RegisterType<CorrugateRepository>().As<ICorrugateRepository>().SingleInstance();
            containerBuilder.RegisterType<PickZoneRepository>().As<IPickZoneRepository>().SingleInstance();
            containerBuilder.RegisterType<CartonPropertyGroupRepository>().As<ICartonPropertyGroupRepository>().SingleInstance();
            containerBuilder.RegisterType<MachineGroupRepository>().As<IMachineGroupRepository>().SingleInstance();
            containerBuilder.RegisterType<ProductionGroupRepository>().As<IProductionGroupRepository>().SingleInstance();
            containerBuilder.RegisterType<FusionMachineRepository>().As<IFusionMachineRepository>().SingleInstance();
            containerBuilder.RegisterType<EmMachineRepository>().As<IEmMachineRepository>().SingleInstance();
            containerBuilder.RegisterType<ZebraPrinterRepository>().As<IZebraPrinterRepository>().SingleInstance();
            containerBuilder.RegisterType<ExternalCustomerSystemRepository>().As<IExternalCustomerSystemRepository>().SingleInstance();
            containerBuilder.RegisterType<PackNetServerSettingsRepository>().As<IPackNetServerSettingsRepository>().SingleInstance();
            containerBuilder.RegisterType<OrdersRepository>().As<IOrdersRepository>().SingleInstance();
            containerBuilder.RegisterType<TemplateRepository>().As<ITemplateRepository>().SingleInstance();
            containerBuilder.RegisterType<DatabaseTaskRepository>().As<IDatabaseTaskRepository>().SingleInstance();
            containerBuilder.RegisterType<ComponentsConfigurationRepository>().As<IComponentsConfigurationRepository>().SingleInstance();
            containerBuilder.RegisterType<UserNotificationRepository>().As<IUserNotificationRepository>().SingleInstance();
            containerBuilder.RegisterType<WorkflowTrackingRepository>().As<IWorkflowTrackingRepository>().SingleInstance();
            containerBuilder.RegisterType<SchedulerRepository>().As<ISchedulerRepository>().SingleInstance();
        }

        /// <summary>
        /// Register items with the IOC container that pertain to general infrastructure
        /// </summary>
        private void RegisterInfrastructureItems()
        {
            containerBuilder.RegisterInstance(logger).As<ILogger>().SingleInstance();
            containerBuilder.RegisterInstance(eventAggregator).As<IEventAggregatorPublisher>().SingleInstance();
            containerBuilder.RegisterInstance(eventAggregator).As<IEventAggregatorSubscriber>().SingleInstance();
            containerBuilder.RegisterType<MachineCommunicatorFactory>().As<IMachineCommunicatorFactory>();
        }

        private ILogger CreateLogger()
        {
            if (EventLog.SourceExists(ComponentName) == false)
            {
                EventLog.CreateEventSource(ComponentName, "Application");
            }

            if (File.Exists(Settings.Default.NLogConfiguration))
            {
                //Setup logging in case composition fails
                ILoggingBackend loggingBackend = new NLogBackend(ComponentName, Settings.Default.NLogConfiguration);
                LogManager.LoggingBackend = loggingBackend;
                var realLogger = LogManager.GetLogFor("server");
                realLogger.Log(LogLevel.Info, "Logging started");
                return realLogger;
            }

            throw new ApplicationException("NLog configuration file could not be found.");
        }

        public void Dispose()
        {
            EventLog.WriteEntry(ComponentName, "Shutting down.", EventLogEntryType.Information);
            iocContainer.Resolve<ILogger>().Log(LogLevel.Warning, "Packnet.Server is shutting down.");

            iocContainer.Dispose();
        }
    }
}