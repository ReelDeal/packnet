﻿using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Server.L10N;

namespace PackNet.Server.Bootstrapper
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using Interfaces;

    public class Bootstrapper : IBootstrap
    {
        public const string ERLANG_REGISTRY_LOCATION = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\Erlang OTP R16B (5.10.1)";
        public const string ERLANG_REGISTRY_VALUE = "DisplayName";
        public const string RABBIT_REGISTRY_LOCATION = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\RabbitMQ";
        public const string RABBIT_REGISTRY_VALUE = "DisplayVersion";
        public const string MONGO_SERVICE_NAME = "MongoDB";
        public const string RABBIT_SERVICE_NAME = "RabbitMQ";
        public const string IIS_SERVICE_NAME = "W3SVC";
        public const int FUSION_PORT_NUM = 9998;
        public const long FOURGIGABYTES_IN_KILOBYTES = 4194304;
        public const long EIGHTYGIGABYTES_IN_BYTES = 85899345920;

        private readonly IRegistryHelper registryHelper;
        private readonly IServiceHelper serviceHelper;
        private readonly IMachineHelper machineHelper;
        private readonly ILogger logger;

        public Bootstrapper(IRegistryHelper registryHelper, IServiceHelper serviceHelper, IMachineHelper machineHelper, ILogger logger)
        {
            this.registryHelper = registryHelper;
            this.serviceHelper = serviceHelper;
            this.machineHelper = machineHelper;
            this.logger = logger;
        }

        public void ValidateServerReadiness()
        {
            var result = ValidateAll();

            if (result.Item1)
            {
                logger.Log(LogLevel.Fatal, result.Item2);
                Environment.Exit(0);
            }
            else
            {
                logger.Log(LogLevel.Warning, result.Item2);
            }
        }

        public Tuple<bool, string> ValidateAll()
        {
            var results = Validate().Where(x => x.Item1 != BootstrapResultEnum.OK);
            var sb = new StringBuilder();
            bool shouldDie = false;

            foreach (var result in results)
            {
                if (result.Item1 == BootstrapResultEnum.FATAL)
                {
                    shouldDie = true;
                }

                sb.AppendLine(String.Format("Error Level: {0}, Message: {1}", result.Item1, result.Item2));
            }

            return new Tuple<bool, string>(shouldDie, sb.ToString());
        }

        public IEnumerable<Tuple<BootstrapResultEnum, string>> Validate()
        {

#if !DEBUG
            yield return ValidateServiceRunning(RABBIT_SERVICE_NAME);
            yield return ValidateServiceRunning(MONGO_SERVICE_NAME);
            yield return SetEnvironmentVariable();
            yield return ValidateFirewallOpen(FUSION_PORT_NUM);
            yield return ValidateIisInstalledAndCorrectVersion();
            yield return ValidateServiceRunning(IIS_SERVICE_NAME);
#endif

            //Warnings
            yield return ValidateMemorySize(FOURGIGABYTES_IN_KILOBYTES);
            yield return ValidateFreeDiskSpace(EIGHTYGIGABYTES_IN_BYTES);
        }

        private Tuple<BootstrapResultEnum, string> SetEnvironmentVariable()
        {
            var envName = "PackNetWorkflows";
            var envValue = Path.Combine(Environment.CurrentDirectory, "Workflows");
            try
            {
                Environment.SetEnvironmentVariable(envName, envValue);
                if (Environment.GetEnvironmentVariable(envName) == envValue)
                    return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);

            }
            catch (Exception e)
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.FATAL, e.ToString());

            }
            return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.FATAL, string.Empty);

        }

        private Tuple<BootstrapResultEnum, string> ValidateIisInstalledAndCorrectVersion()
        {

            if (machineHelper.ValidateIisInstalledAndAcceptableVersion(registryHelper))
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);
            }

            return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.FATAL, Strings.Bootstrapper_ValidateIisInstalledAndCorrectVersion_IIS_is_either_not_installed__or_is_not_version_7_or_greater);
        }

        public Tuple<BootstrapResultEnum, string> ValidateFreeDiskSpace(long minimumFreeSpaceInBytes)
        {

            var entryAssembly = Assembly.GetEntryAssembly();
            var drive = entryAssembly == null ? "C:\\" : Path.GetPathRoot(entryAssembly.Location);

            var freeSpace = machineHelper.GetFreeDiskSpace(drive);

            if (freeSpace >= minimumFreeSpaceInBytes)
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);
            }

            return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.WARNING, String.Format("Drive does not have minimum recommended amount of free space.  System reported {0} bytes of free space, minimum is {1} bytes.", freeSpace, minimumFreeSpaceInBytes));
        }

        public Tuple<BootstrapResultEnum, string> ValidateMemorySize(long minimumSizeInKilobytes)
        {
            var memSize = machineHelper.GetPhysicalMemorySize();
            if (memSize >= minimumSizeInKilobytes)
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);
            }

            return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.WARNING, String.Format("System does not meet the minimum recommend amount of RAM. System reported {0} kilobytes of RAM, minimum is {1} kilobytes", memSize, minimumSizeInKilobytes));
        }

        public Tuple<BootstrapResultEnum, string> ValidateServiceRunning(string serviceName, bool fatalIfNotRunning = true, bool tryToStartServiceIfNotRunning = true)
        {
            var running = serviceHelper.IsServiceRunning(serviceName);

            if (running)
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);
            }

            if (tryToStartServiceIfNotRunning && serviceHelper.StartService(serviceName))
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.WARNING, String.Format("Service {0} was not running, successfully started", serviceName));
            }

            return new Tuple<BootstrapResultEnum, string>(fatalIfNotRunning ? BootstrapResultEnum.FATAL : BootstrapResultEnum.WARNING, String.Format("Service {0} is not running", serviceName));
        }

        public Tuple<BootstrapResultEnum, string> ValidateFirewallOpen(int portNum)
        {
            var result = machineHelper.OpenFirewallPort(portNum);

            if (result)
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);
            }

            return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.WARNING, string.Format("Failed to open Firewall port {0}", portNum));
        }

        public Tuple<BootstrapResultEnum, string> ValidateServiceInstalled(string serviceName, bool fatalIfNotInstalled = true)
        {
            var installed = serviceHelper.IsServiceInstalled(serviceName);

            if (installed)
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);
            }

            return new Tuple<BootstrapResultEnum, string>(fatalIfNotInstalled ? BootstrapResultEnum.FATAL : BootstrapResultEnum.WARNING, String.Format("Failed to find the service {0}", serviceName));
        }

        public Tuple<BootstrapResultEnum, string> ValidateInstall(string dependency, string registryPath, string registryValue)
        {
            var entryValue = registryHelper.GetRegistryEntryValue(registryPath, registryValue);

            if (string.IsNullOrWhiteSpace(entryValue))
            {
                return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.FATAL, string.Format("Failed to find {0} checked registry key {1}, value {2}", dependency, registryPath, registryValue));
            }

            return new Tuple<BootstrapResultEnum, string>(BootstrapResultEnum.OK, string.Empty);
        }
    }
}