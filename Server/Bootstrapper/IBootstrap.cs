﻿namespace PackNet.Server.Bootstrapper
{
    using System;
    using System.Collections.Generic;

    public interface IBootstrap
    {
        IEnumerable<Tuple<BootstrapResultEnum, string>> Validate();
        Tuple<BootstrapResultEnum, string> ValidateInstall(string dependency, string registryPath, string registryValue);
        Tuple<BootstrapResultEnum, string> ValidateServiceInstalled(string serviceName, bool fatalIfNotInstalled = true);
        Tuple<BootstrapResultEnum, string> ValidateServiceRunning(string serviceName, bool fatalIfNotRunning = true, bool tryToStartServiceIfNotRunning = true);
        Tuple<BootstrapResultEnum, string> ValidateMemorySize(long fourgigabytesInKilobytes);
        Tuple<BootstrapResultEnum, string> ValidateFreeDiskSpace(long minimumFreeSpaceInBytes);
        Tuple<BootstrapResultEnum, string> ValidateFirewallOpen(int portNum);
        void ValidateServerReadiness();
    }
}