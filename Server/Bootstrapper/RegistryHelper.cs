﻿namespace PackNet.Server.Bootstrapper
{
    using System;

    using Microsoft.Win32;

    using Interfaces;

    public class RegistryHelper : IRegistryHelper
    {
        /// <summary>
        /// Will check the 32 bit registry, if nothing is found it will then check the 64 bit registry
        /// </summary>
        /// <param name="regKey">The key to open</param>
        /// <param name="regValue">The value to receive</param>
        /// <returns></returns>
        public string GetRegistryEntryValue(string regKey, string regValue)
        {
            var registryValue = GetRegistryEntryValue(regKey, regValue, RegistryView.Registry32);

            if (String.IsNullOrEmpty(registryValue))
            {
                return GetRegistryEntryValue(regKey, regValue, RegistryView.Registry64);
            }

            return registryValue;
        }

        private string GetRegistryEntryValue(string regKey, string regValue, RegistryView registryView)
        {
            RegistryKey localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, registryView);
            localKey = localKey.OpenSubKey(regKey);
            if (localKey != null)
            {
                var value = localKey.GetValue(regValue);

                return value != null ? value.ToString() : String.Empty;
            }

            return string.Empty;
        }
    }
}