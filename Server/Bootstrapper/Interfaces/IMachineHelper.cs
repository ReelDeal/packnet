﻿namespace PackNet.Server.Bootstrapper.Interfaces
{
    using System.IO;

    public interface IMachineHelper
    {
        long GetPhysicalMemorySize();
        long GetFreeDiskSpace(string driveLetter);
        bool OpenFirewallPort(int portNum);
        FileInfo[] RecurseAndFindFile(DirectoryInfo baseDirectory, string filePattern);

        bool ExecuteProcess(FileInfo file, string arguments, bool waitForExit = true,
            int milliSecondsToWaitForExit = 10000);

        bool ValidateIisInstalledAndAcceptableVersion(IRegistryHelper registryHelper);
    }
}