﻿namespace PackNet.Server.Bootstrapper.Interfaces
{
    using System.Collections.Generic;
    using System.ServiceProcess;

    public interface IServiceHelper
    {
        /// <summary>
        /// List of all services installed on the machine.  This list is generated and cached at the moment of instantiation
        /// </summary>
        List<ServiceController> Services { get; }

        /// <summary>
        /// This call is used to refresh the cached list of services
        /// </summary>
        void RefreshServiceList();
        bool IsServiceInstalled(string serviceName);
        bool IsServiceRunning(string serviceName);
        bool StopService(string serviceName);
        bool StartService(string serviceName);
        bool RestartService(string serviceName);

    }
}