﻿namespace PackNet.Server.Bootstrapper.Interfaces
{
    public interface IRegistryHelper
    {
        string GetRegistryEntryValue(string regKey, string regValue);
    }
}