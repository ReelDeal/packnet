﻿namespace PackNet.Server.Bootstrapper
{
    public enum BootstrapResultEnum
    {
        OK = 0,
        WARNING = 1,
        FATAL = 2
    }
}
