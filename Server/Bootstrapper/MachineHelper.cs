﻿namespace PackNet.Server.Bootstrapper
{
    using Interfaces;
    using NetFwTypeLib;
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Management;

    public class MachineHelper : IMachineHelper
    {
        private const string IISRegKeyName = "Software\\Microsoft\\InetStp";
        private const string IISRegKeyValue = "MajorVersion";

        public long GetPhysicalMemorySize()
        {
            int memSize = -1;
            var wql = new ObjectQuery("SELECT * FROM Win32_OperatingSystem");
            var searcher = new ManagementObjectSearcher(wql);
            ManagementObjectCollection results = searcher.Get();

            foreach (var o in results)
            {
                var result = (ManagementObject)o;
                var visibleMemoryRaw = result["TotalVisibleMemorySize"];

                if (visibleMemoryRaw != null)
                {
                    if (int.TryParse(visibleMemoryRaw.ToString(), out memSize))
                    {
                        return memSize;
                    }

                    memSize = -1;
                }
            }
            return memSize;
        }

        public long GetFreeDiskSpace(string driveLetter)
        {
            var driveInfo = new DriveInfo(driveLetter);
            return driveInfo.AvailableFreeSpace;
        }

        public bool OpenFirewallPort(int portNum)
        {
            try
            {

                INetFwMgr icfMgr = null;
                Type TicfMgr = Type.GetTypeFromProgID("HNetCfg.FwMgr");
                icfMgr = (INetFwMgr)Activator.CreateInstance(TicfMgr);

                INetFwProfile profile;
                INetFwOpenPort portClass;
                Type TportClass = Type.GetTypeFromProgID("HNetCfg.FWOpenPort");
                portClass = (INetFwOpenPort)Activator.CreateInstance(TportClass);
                // Get the current profile
                profile = icfMgr.LocalPolicy.CurrentProfile;

                // Set the port properties
                portClass.Scope = NET_FW_SCOPE_.NET_FW_SCOPE_ALL;
                portClass.Enabled = true;
                portClass.Name = "Packnet .Server Communication";
                portClass.Port = portNum;
                portClass.Protocol = NET_FW_IP_PROTOCOL_.NET_FW_IP_PROTOCOL_TCP; ;
                // Add the port to the ICF Permissions List
                profile.GloballyOpenPorts.Add(portClass);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public FileInfo[] RecurseAndFindFile(DirectoryInfo baseDirectory, string filePattern)
        {
            return baseDirectory.GetFiles(filePattern, SearchOption.AllDirectories);
        }

        public bool ExecuteProcess(FileInfo file, string arguments, bool waitForExit = true, int milliSecondsToWaitForExit = 10000)
        {
            try
            {
                var process = new Process();
                process.StartInfo.FileName = file.FullName;
                process.StartInfo.Arguments = arguments;

                process.Start();

                if (waitForExit)
                {
                    return process.WaitForExit(milliSecondsToWaitForExit);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool ValidateIisInstalledAndAcceptableVersion(IRegistryHelper registryHelper)
        {
            var result = registryHelper.GetRegistryEntryValue(IISRegKeyName, IISRegKeyValue);
            int iisVersion;

            if (int.TryParse(result, out iisVersion))
            {
                if (iisVersion >= 7)
                {
                    return true;
                }
            }

            return false;
        }
    }
}