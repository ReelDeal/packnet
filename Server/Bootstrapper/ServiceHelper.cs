﻿namespace PackNet.Server.Bootstrapper
{
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceProcess;

    using Interfaces;

    public class ServiceHelper : IServiceHelper
    {
        public List<ServiceController> Services { get; private set; }

        public ServiceHelper()
        {
            RefreshServiceList();
        }

        public void RefreshServiceList()
        {
            Services = ServiceController.GetServices().ToList();
        }

        public bool IsServiceInstalled(string serviceName)
        {
            return Services.Any(s => s.ServiceName.ToLower().Contains(serviceName.ToLower()));
        }

        public bool IsServiceRunning(string serviceName)
        {

            var service = Services.FirstOrDefault(s => s.ServiceName.ToLower().Contains(serviceName.ToLower()));

            if (service != null)
            {
                return service.Status == ServiceControllerStatus.Running;
            }

            return false;
        }

        public bool StopService(string serviceName)
        {
            try
            {
                var service = Services.FirstOrDefault(x => x.ServiceName.ToLower().Contains(serviceName.ToLower()));

                if (service != null)
                {
                    service.Stop();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool StartService(string serviceName)
        {
            try
            {
                var service = Services.FirstOrDefault(x => x.ServiceName.ToLower().Contains(serviceName.ToLower()));

                if (service != null)
                {
                    service.Start();
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RestartService(string serviceName)
        {
            return StopService(serviceName) && StartService(serviceName);
        }
    }
}