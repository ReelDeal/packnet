﻿using System.Diagnostics;
using System.Threading.Tasks;

namespace PackNet.Server
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.ServiceProcess;
    using System.Threading;

    public class EntryPoint : ServiceBase
    {
        private static readonly Mutex mutex = new Mutex(true, "{8F6F0AA4-B9A1-4BFD-A8CF-19204E6BDE8F}");
        private Server app;


        protected override void OnStart(string[] args)
        {
            Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetAssembly(typeof(EntryPoint)).Location));
            app = new Server();
            base.OnStart(args);
        }

        protected override void OnStop()
        {
            app.Dispose();
            base.OnStop();

            //Nastiness to get the threads shut down after the service stops
            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(100);
                Environment.Exit(0);
            });
        }

        [STAThread]
        public static void Main(string[] args)
        {
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                if (args.Any() && args.Contains("--console"))
                {
                    var sw = Stopwatch.StartNew();
                    Console.WriteLine("Starting up.  This takes a minute.");
                    var app = new EntryPoint();
                    app.OnStart(args);
                    sw.Stop();
                    Console.WriteLine("Startup took "+ sw.ElapsedMilliseconds+" ms to start.");
                    Console.Read();
                    app.OnStop();
                }
                else
                {
                    Run(new EntryPoint());
                }
            }
            else
            {
                Console.WriteLine("Packnet.Server is already running.");
            }
        }
    }
}