﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.PrintingMachines;

using Testing.Specificity;

namespace CommonTests.DTO.BoxFirst
{
    [TestClass]
    public class BoxFirstCartonTilingEqualityComparerTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTrueIfUnhandledPropertiesDiffer()
        {
            var p1 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var p2 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var comparer = new BoxFirstCartonTilingEqualityComparer();
            
            Specify.That(comparer.Equals(p1, p2)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalseIfHandledPropertiesDiffer()
        {
            var p1 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var p2 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 2 };
            var comparer = new BoxFirstCartonTilingEqualityComparer();

            Specify.That(comparer.Equals(p1, p2)).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTrue_WhenCartonsInKitsAreEqual()
        {
            var k1 = new Kit();
            var c1 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var l1 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k1.AddProducible(c1);
            k1.AddProducible(l1);

            var k2 = new Kit();
            var c2 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var l2 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k2.AddProducible(c2);
            k2.AddProducible(l2);

            var comparer = new BoxFirstCartonTilingEqualityComparer();
            Specify.That(comparer.Equals(k1, k2)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTrue_WhenMultipleCartonsInKitsAreEqual()
        {
            var k1 = new Kit();
            var c1_1 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var c1_2 = new Carton() { DesignId = 1, Length = 200d, Width = 200d, Height = 200d, CorrugateQuality = 2 };
            var l1 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k1.AddProducible(c1_1);
            k1.AddProducible(c1_2);
            k1.AddProducible(l1);

            var k2 = new Kit();
            var c2_1 = new Carton() { DesignId = 1, Length = 200d, Width = 200d, Height = 200d, CorrugateQuality = 2 };
            var c2_2 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var l2 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k2.AddProducible(c2_1);
            k2.AddProducible(c2_2);
            k2.AddProducible(l2);

            var comparer = new BoxFirstCartonTilingEqualityComparer();
            Specify.That(comparer.Equals(k1, k2)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalse_WhenNumberOfCartonInKitsDiffers()
        {
            var k1 = new Kit();
            var l1 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k1.AddProducible(l1);

            var k2 = new Kit();
            var c2 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var l2 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k2.AddProducible(c2);
            k2.AddProducible(l2);

            var comparer = new BoxFirstCartonTilingEqualityComparer();
            Specify.That(comparer.Equals(k1, k2)).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalse_WhenKitHasNoCarton()
        {
            var k1 = new Kit();
            var l1 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k1.AddProducible(l1);

            var k2 = new Kit();
            var l2 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            k2.AddProducible(l2);

            var comparer = new BoxFirstCartonTilingEqualityComparer();

            Specify.That(comparer.Equals(k1, k2)).Should.BeFalse();
        }
    }
}
