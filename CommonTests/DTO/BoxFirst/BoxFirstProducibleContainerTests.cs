﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.PrintingMachines;

using Testing.Specificity;

namespace CommonTests.DTO.BoxFirst
{
    [TestClass]
    public class BoxFirstProducibleContainerTests
    {
        [TestMethod]
        public void ShouldReturnAllProducibles()
        {
            var container = new BoxFirstProducibleContainer();
            var p1 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p2 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p3 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p4 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p5 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p6 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p7 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p8 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p9 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6, p7, p8, p9 };

            container.AddProducibles(producibles);

            var returnedProducibles = container.GetAllProducibles();

            Specify.That(returnedProducibles).Should.Not.BeNull();
            Specify.That(returnedProducibles.Count()).Should.BeEqualTo(9);
        }

        [TestMethod]
        public void ShouldReturnEmptyListIfNoProduciblesExists()
        {
            var container = new BoxFirstProducibleContainer();
            var returnedProducibles = container.GetAllProducibles();
            var returnedGroups = container.GetGroupedProducibles();

            Specify.That(returnedProducibles).Should.Not.BeNull();
            Specify.That(returnedProducibles.Count()).Should.BeEqualTo(0);
            Specify.That(returnedGroups).Should.Not.BeNull();
            Specify.That(returnedGroups.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        public void ShouldReturnGroupedProducibles()
        {
            var container = new BoxFirstProducibleContainer();
            var p1 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p2 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p3 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p4 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p5 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p6 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p7 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p8 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p9 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6, p7, p8, p9 };

            container.AddProducibles(producibles);

            var returnedProducibles = container.GetGroupedProducibles();

            Specify.That(returnedProducibles).Should.Not.BeNull();
            Specify.That(returnedProducibles.Count()).Should.BeEqualTo(3);
            foreach(var group in returnedProducibles)
                Specify.That(group.Count()).Should.BeEqualTo(3);
        }

        [TestMethod]
        public void ShouldReturnIdentialCartons()
        {
            var container = new BoxFirstProducibleContainer();
            var p1 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p2 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p3 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p4 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p5 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p6 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p7 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p8 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p9 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6, p7, p8, p9 };

            container.AddProducibles(producibles);

            var p10 = GetBoxFirstCartonWithLabel(600, 400, 200);

            var returnedProducibles = container.GetIdenticalProducibles(p10);

            Specify.That(returnedProducibles).Should.Not.BeNull();
            Specify.That(returnedProducibles.Count()).Should.BeEqualTo(3);

            foreach (var producible in returnedProducibles)
            {
                var kit = producible.Producible as Kit;
                var carton = kit.ItemsToProduce.First(i => i is ICarton) as Carton;
                Specify.That(carton.Length).Should.BeEqualTo(600d);
                Specify.That(carton.Width).Should.BeEqualTo(400d);
                Specify.That(carton.Height).Should.BeEqualTo(200d);
            }
        }

        [TestMethod]
        public void ShouldReturnEmptyListIfNoIdenticalCartonsExist()
        {
            var container = new BoxFirstProducibleContainer();
            var p1 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p2 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p3 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p4 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p5 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p6 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var p7 = GetBoxFirstCartonWithLabel(600, 300, 200);
            var p8 = GetBoxFirstCartonWithLabel(600, 350, 200);
            var p9 = GetBoxFirstCartonWithLabel(600, 400, 200);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6, p7, p8, p9 };

            container.AddProducibles(producibles);

            var p10 = GetBoxFirstCartonWithLabel(700, 400, 200);

            var returnedProducibles = container.GetIdenticalProducibles(p10);

            Specify.That(returnedProducibles).Should.Not.BeNull();
            Specify.That(returnedProducibles.Count()).Should.BeEqualTo(0);
        }

        private BoxFirstProducible GetBoxFirstCartonWithLabel(MicroMeter length, MicroMeter width, MicroMeter height)
        {
            var prod = new BoxFirstProducible();
            prod.Id = Guid.NewGuid();
            var kit = new Kit();
            kit.ItemsToProduce.Add(new Carton() {Id = Guid.NewGuid(), Length = length, Width = width, Height = height });
            kit.ItemsToProduce.Add(new Label());
            prod.Producible = kit;

            return prod;
        }

        private BoxFirstProducible GetBoxFirstCarton(MicroMeter length, MicroMeter width, MicroMeter height)
        {
            var prod = new BoxFirstProducible();
            prod.Id = Guid.NewGuid();
            prod.Producible = new Carton() { Id = Guid.NewGuid(), Length = length, Width = width, Height = height };

            return prod;
        }
    }
}
