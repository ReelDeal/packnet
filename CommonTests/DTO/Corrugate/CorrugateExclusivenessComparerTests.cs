﻿using System;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Utils;

namespace CommonTests.DTO.Corrugate
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Corrugates;

    using Testing.Specificity;

    [TestClass]
    public class CorrugateExclusivenessComparerTests
    {
        private Mock<IPacksizeCutCreaseMachine> machine1;
        private IList<IPacksizeCutCreaseMachine> machines;
        private Guid id;

        [TestInitialize]
        public void TestInitialize()
        {
            id = Guid.NewGuid();
            machines = new List<IPacksizeCutCreaseMachine>();
            machine1 = new Mock<IPacksizeCutCreaseMachine>();
            machine1.Setup(m => m.Id).Returns(id);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ObjectReferenceTests()
        {
            machines.Add(machine1.Object);
            var comparer = new CorrugateExclusivenessComparer(machines);
            var corrugate1 = new Corrugate();

            Specify.That(comparer.Compare(null, null)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(corrugate1, null)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(null, corrugate1)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(corrugate1, corrugate1)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ExclusivenessTestByWidth()
        {
            SetUpMachineWithCorrugatesByWidth();
            AssertExclusiveness();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ExclusivenessTestByThickness()
        {
            SetUpMachineWithCorrugatesByThickness();
            AssertExclusiveness();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ExclusivenessTestByQuality()
        {
            SetUpMachineWithCorrugatesByQuality();
            AssertExclusiveness();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ActiveCorrugateTests()
        {
            SetUpMachineWithCorrugatesByActive();
            var comparer = new CorrugateExclusivenessComparer(machines);

            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(0).LoadedCorrugate, machine1.Object.Tracks.ElementAt(1).LoadedCorrugate)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(0).LoadedCorrugate, machine1.Object.Tracks.ElementAt(2).LoadedCorrugate)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(1).LoadedCorrugate, machine1.Object.Tracks.ElementAt(0).LoadedCorrugate)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(1).LoadedCorrugate, machine1.Object.Tracks.ElementAt(2).LoadedCorrugate)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(2).LoadedCorrugate, machine1.Object.Tracks.ElementAt(0).LoadedCorrugate)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(2).LoadedCorrugate, machine1.Object.Tracks.ElementAt(1).LoadedCorrugate)).Should.BeEqualTo(-1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void PrecedenceAreExclusivityThenWidthThenQuality()
        {
            var machine1 = new Mock<IPacksizeCutCreaseMachine>();
            machine1.Setup(m => m.Id).Returns(Guid.NewGuid());
            machine1.Setup(m => m.Tracks).Returns(new ConcurrentList<Track>
            {
                new Track { LoadedCorrugate = new Corrugate { Width = 200, Quality = 1 } }, // Exclusive 3
                new Track { LoadedCorrugate = new Corrugate { Width = 100, Quality = 2 } }, // Exclusive 2
                new Track { LoadedCorrugate = new Corrugate { Width = 170, Quality = 1 } }, // Exclusive 1
                new Track { LoadedCorrugate = new Corrugate { Width = 150, Quality = 1 } }, // Exclusive 1
                new Track { LoadedCorrugate = new Corrugate { Width = 150, Quality = 2 } }, // Exclusive 1
                new Track { LoadedCorrugate = new Corrugate { Width = 250, Quality = 2 } }, // Exclusive 1
                new Track { LoadedCorrugate = new Corrugate { Width = 300, Quality = 1 } }, // Exclusive 2
                new Track { LoadedCorrugate = new Corrugate { Width = 300, Quality = 2 } }, // Exclusive 2
            });

            var machine2 = new Mock<IPacksizeCutCreaseMachine>();
            machine2.Setup(m => m.Id).Returns(Guid.NewGuid());
            machine2.Setup(m => m.Tracks).Returns(new ConcurrentList<Track>
            {
                new Track { LoadedCorrugate = new Corrugate { Width = 200, Quality = 1 } },
                new Track { LoadedCorrugate = new Corrugate { Width = 100, Quality = 1 } },
                new Track { LoadedCorrugate = new Corrugate { Width = 300, Quality = 2 } }
            });

            var machine3 = new Mock<IPacksizeCutCreaseMachine>();
            machine3.Setup(m => m.Id).Returns(Guid.NewGuid());
            machine3.Setup(m => m.Tracks).Returns(new ConcurrentList<Track>
            {
                new Track { LoadedCorrugate = new Corrugate { Width = 200, Quality = 1 } },
                new Track { LoadedCorrugate = new Corrugate { Width = 100, Quality = 2 } },
                new Track { LoadedCorrugate = new Corrugate { Width = 300, Quality = 1 } }
            });

            var machineCorrugates = machine1.Object.Tracks.Select(t=> t.LoadedCorrugate).ToList();
            machineCorrugates.Sort(new CorrugateExclusivenessComparer(new List<IPacksizeCutCreaseMachine> { machine2.Object, machine3.Object }));

            AssertCorrugate(machineCorrugates[0], 250, 2);
            AssertCorrugate(machineCorrugates[1], 170, 1);
            AssertCorrugate(machineCorrugates[2], 150, 2);
            AssertCorrugate(machineCorrugates[3], 150, 1);
            AssertCorrugate(machineCorrugates[4], 300, 2);
            AssertCorrugate(machineCorrugates[5], 300, 1);
            AssertCorrugate(machineCorrugates[6], 100, 2);
            AssertCorrugate(machineCorrugates[7], 200, 1);
        }

        private static void AssertCorrugate(Corrugate corrugate, MicroMeter width, int quality)
        {
            Specify.That(corrugate.Width).Should.BeEqualTo(width);
            Specify.That(corrugate.Quality).Should.BeEqualTo(quality);
        }

        private void AssertExclusiveness()
        {
            var comparer = new CorrugateExclusivenessComparer(machines);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(0).LoadedCorrugate, machine1.Object.Tracks.ElementAt(1).LoadedCorrugate)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(0).LoadedCorrugate, machine1.Object.Tracks.ElementAt(2).LoadedCorrugate)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(1).LoadedCorrugate, machine1.Object.Tracks.ElementAt(0).LoadedCorrugate)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(1).LoadedCorrugate, machine1.Object.Tracks.ElementAt(2).LoadedCorrugate)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(2).LoadedCorrugate, machine1.Object.Tracks.ElementAt(0).LoadedCorrugate)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(machine1.Object.Tracks.ElementAt(2).LoadedCorrugate, machine1.Object.Tracks.ElementAt(1).LoadedCorrugate)).Should.BeEqualTo(1);
        }

        private void SetUpMachineWithCorrugatesByActive()
        {
            machine1.Setup(m => m.Tracks).Returns(new ConcurrentList<Track>
            {
                new Track{LoadedCorrugate = new Corrugate { Width = 1 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 3 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 3 }},
            });
            machines.Add(machine1.Object);
        }

        private void SetUpMachineWithCorrugatesByWidth()
        {
            machine1.Setup(m => m.Tracks).Returns(new ConcurrentList<Track>
            {
                new Track{LoadedCorrugate = new Corrugate { Width = 1 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 3 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 3 }},
                new Track{LoadedCorrugate = new Corrugate { Width = 3 }},
            });
            machines.Add(machine1.Object);
        }

        private void SetUpMachineWithCorrugatesByThickness()
        {
            machine1.Setup(m => m.Tracks).Returns(new ConcurrentList<Track>
            {
                new Track{LoadedCorrugate = new Corrugate { Thickness = 1 }},
                new Track{LoadedCorrugate = new Corrugate { Thickness = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Thickness = 3 }},
                new Track{LoadedCorrugate = new Corrugate { Thickness = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Thickness = 3 }},
                new Track{LoadedCorrugate = new Corrugate { Thickness = 3 }},
            });
            machines.Add(machine1.Object);
        }

        private void SetUpMachineWithCorrugatesByQuality()
        {
            machine1.Setup(m => m.Tracks).Returns(new ConcurrentList<Track>
            {
                new Track{LoadedCorrugate = new Corrugate { Quality = 1 }},
                new Track{LoadedCorrugate = new Corrugate { Quality = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Quality = 3 }},
                new Track{LoadedCorrugate = new Corrugate { Quality = 2 }},
                new Track{LoadedCorrugate = new Corrugate { Quality = 3 }},
                new Track{LoadedCorrugate = new Corrugate { Quality = 3 }},
            });
            machines.Add(machine1.Object);
        }
    }
}