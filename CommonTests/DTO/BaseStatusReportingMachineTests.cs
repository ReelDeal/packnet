﻿namespace CommonTests.DTO
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Utils;

    using Testing.Specificity;
    
    [TestClass]
    public class BaseStatusReportingMachineTests
    {
        private readonly BaseStatusReportingMachine<MachineStatuses> machine = new StatusReportingMachineTester();

        [TestInitialize]
        public void setup()
        {
            
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void StatusReportingMachine_ShouldPublishNewStatus_OnEventAggregator_WhenChangingMachineStatus()
        {
            bool eventCalled = false;
            machine.CurrentStatusChangedObservable.Subscribe(ms =>
            {
                Specify.That(ms).Should.BeEqualTo(MachineStatuses.MachineInitializing);
                eventCalled = true;
            });

            machine.CurrentStatus = MachineStatuses.MachineInitializing;

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void StatusReportingMachine_ShouldPublishNewStatus_OnEventAggregator_WhenChangingProductionStatus()
        {
            bool eventCalled = false;
            machine.CurrentProductionStatusObservable.Subscribe(ms =>
            {
                Specify.That(ms).Should.BeEqualTo(MachineProductionStatuses.ProductionCompleted);
                eventCalled = true;
            });

            machine.CurrentProductionStatus = MachineProductionStatuses.ProductionCompleted;

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }
    }

    public class StatusReportingMachineTester : BaseStatusReportingMachine<MachineStatuses>
    {
    }
}
