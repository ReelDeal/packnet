﻿using System;
using System.Net;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

using Testing.Specificity;

namespace CommonTests.DTO
{
    [TestClass]
    [DeploymentItem(@"TestFiles/PhysicalMachineSettings", @"PhysicalMachineSettingsFiles")]
    public class EmPhysicalMachineSettingsTest
    {

        [TestMethod]
        public void ShouldBeAbleToCreateEm7PhysicalMachineSettingsInchFromFile()
        {
            var physicalMachineSettingsFile =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em7PhysicalMachineSettings_inches.cfg.xml");

            Specify.That(physicalMachineSettingsFile).Should.Not.BeNull();
        }

        [TestMethod]
        public void ShouldBeAbleToCreateEm7PhysicalMachineSettingsMMFromFile()
        {
            var physicalMachineSettingsFile =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em7PhysicalMachineSettings_mm.cfg.xml");

            Specify.That(physicalMachineSettingsFile).Should.Not.BeNull();
        }

        [TestMethod]
        public void ShouldBeAbleToCreateEm650PhysicalMachineSettingsInchFromFile()
        {
            var physicalMachineSettingsFile =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em6-50PhysicalMachineSettings_inches.cfg.xml");

            Specify.That(physicalMachineSettingsFile).Should.Not.BeNull();
        }

        [TestMethod]
        public void ShouldBeAbleToCreateEm650PhysicalMachineSettingsMMFromFile()
        {
            var physicalMachineSettingsFile =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em6-50PhysicalMachineSettings_mm.cfg.xml");

            Specify.That(physicalMachineSettingsFile).Should.Not.BeNull();
        }

        [TestMethod]
        public void ShouldSetCodeGeneratorSettings_BasedOnUnitInMachineConfig()
        {
            var physicalMachineSettingsFileInches =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em7PhysicalMachineSettings_inches.cfg.xml");

            var physicalMachineSettingsFileMm =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em7PhysicalMachineSettings_mm.cfg.xml");


            Specify.That(physicalMachineSettingsFileInches).Should.Not.BeNull();
            Specify.That(physicalMachineSettingsFileMm).Should.Not.BeNull();

            Specify.That(physicalMachineSettingsFileInches.CodeGeneratorSettings).Should.Not.BeNull();
            Specify.That(physicalMachineSettingsFileMm.CodeGeneratorSettings).Should.Not.BeNull();

            Specify.That(physicalMachineSettingsFileInches.CodeGeneratorSettings.MinimumLineDistanceToCorrugateEdge).Should.BeLogicallyEqualTo(0.2d);
            Specify.That(physicalMachineSettingsFileMm.CodeGeneratorSettings.MinimumLineDistanceToCorrugateEdge).Should.BeLogicallyEqualTo(5d);

            Specify.That(physicalMachineSettingsFileInches.CodeGeneratorSettings.MinimumLineLength).Should.BeLogicallyEqualTo(0.078d);
            Specify.That(physicalMachineSettingsFileMm.CodeGeneratorSettings.MinimumLineLength).Should.BeLogicallyEqualTo(2d);

            Specify.That(physicalMachineSettingsFileInches.CodeGeneratorSettings.PerforationSafetyDistance).Should.BeLogicallyEqualTo(1.57d);
            Specify.That(physicalMachineSettingsFileMm.CodeGeneratorSettings.PerforationSafetyDistance).Should.BeLogicallyEqualTo(40d);

        }

        [TestMethod]
        public void ShouldSetWasteSeparatorOffsets_BasedOnUnitInMachineConfig()
        {
            var physicalMachineSettingsFileInches =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em7PhysicalMachineSettings_inches.cfg.xml");

            var physicalMachineSettingsFileMm =
                EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), @"PhysicalMachineSettingsFiles/Em7PhysicalMachineSettings_mm.cfg.xml");


            Specify.That(physicalMachineSettingsFileInches).Should.Not.BeNull();
            Specify.That(physicalMachineSettingsFileMm).Should.Not.BeNull();

            Specify.That(physicalMachineSettingsFileInches.LongHeadParameters).Should.Not.BeNull();
            Specify.That(physicalMachineSettingsFileMm.LongHeadParameters).Should.Not.BeNull();

            Specify.That(physicalMachineSettingsFileInches.LongHeadParameters.LeftSideToWasteSeparator).Should.BeLogicallyEqualTo(0.59d, "Expected LeftSideToWasteSeparator to have the default value (inch). This value is constant and should not be changed unless the machine is physically modified");
            Specify.That(physicalMachineSettingsFileMm.LongHeadParameters.LeftSideToWasteSeparator).Should.BeLogicallyEqualTo(15d, "Expected LeftSideToWasteSeparator to have the default value (mm). This value is constant and should not be changed unless the machine is physically modified");

            Specify.That(physicalMachineSettingsFileInches.LongHeadParameters.WasteSeparatorWidth).Should.BeLogicallyEqualTo(0.945d, "Expected WasteSeparatorWidth to have the default value (inch). This value is constant and should not be changed unless the machine is physically modified");
            Specify.That(physicalMachineSettingsFileMm.LongHeadParameters.WasteSeparatorWidth).Should.BeLogicallyEqualTo(24d, "Expected WasteSeparatorWidth to have the default value (mm). This value is constant and should not be changed unless the machine is physically modified");
        }
    }
}
