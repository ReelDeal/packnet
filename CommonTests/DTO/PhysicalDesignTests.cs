﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Enums;

using Testing.Specificity;

namespace CommonTests.DTO
{
    [TestClass]
    public class PhysicalDesignTests
    {
        [TestMethod]
        public void PhyscialLinesAreEqual_WhenCoordinatesAndLineTypeAreSame()
        {
            var line1 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(100, 100), LineType.cut);
            var line2 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(100, 100), LineType.cut);
            Specify.That(line1).Should.BeEqualTo(line2);
        }
    }
}
