﻿namespace CommonTests.DTO
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class FileImportSettingsTests
    {
        [TestMethod]
        public void FileExtensionShouldReturnCsvAsDefaultWhenItIsUnassignedOrSetToNullOrEmpty()
        {
            // Test when constructor does not set the value
            var fileImporterSettings = new FileImportSettings();
            Specify.That(fileImporterSettings.FileExtension).Should.BeEqualTo("csv");

            // Test when explictly set the value
            fileImporterSettings.FileExtension = string.Empty;
            Specify.That(fileImporterSettings.FileExtension).Should.BeEqualTo("csv");
;        }
    }
}
