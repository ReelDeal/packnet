﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Messaging;

namespace CommonTests.DTO
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.Enums;

    using Testing.Specificity;

    /// <summary>
    /// Summary description for JsonMessageTests
    /// </summary>
    [TestClass]
    public class MessageTests
    {
        private const string MrT = "testString";

        private MessageTypes MessageType = UserMessages.UpdateUser;

        private const string ReplyTo = "testReplyTo";
        
        [TestMethod]
        public void CanSerializeT()
        {
            var message = new Message<string>{MessageType = MessageType, ReplyTo = ReplyTo, Data = MrT, Created = DateTime.UtcNow};
         
            var result = JsonConvert.SerializeObject(message);
            var expected = String.Format("{{\"Data\":\"{0}\",\"MessageType\":\"{1}\",\"ReplyTo\":\"{2}\",\"Created\":\"{3}\",\"Compressed\":false}}", MrT, MessageType, ReplyTo, message.Created.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK"));
            Trace.WriteLine(expected);
            Trace.WriteLine(result);
            Specify.That(result).Should.BeEqualTo(expected);
        }

        [TestMethod]
        public void CanSerializeTWithUserName()
        {
            var message = new Message<string>{MessageType = MessageType, ReplyTo = ReplyTo, Data = MrT, UserName = "test", Created = DateTime.UtcNow};

            var result = JsonConvert.SerializeObject(message);

            var expected = String.Format("{{\"Data\":\"{0}\",\"MessageType\":\"{1}\",\"ReplyTo\":\"{2}\",\"UserName\":\"test\",\"Created\":\"{3}\",\"Compressed\":false}}", MrT, MessageType, ReplyTo, message.Created.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK"));
            Specify.That(result).Should.BeEqualTo(expected);
        }

        [TestMethod]
        public void CanSerializeTWithUserNameAndMachineGroupName()
        {
            var message = new Message<string>{MessageType = MessageType, ReplyTo = ReplyTo, Data = MrT, UserName = "test", MachineGroupName = "myMachine"};

            var result = JsonConvert.SerializeObject(message);

            var expected = String.Format("{{\"Data\":\"{0}\",\"MessageType\":\"{1}\",\"ReplyTo\":\"{2}\",\"UserName\":\"test\",\"MachineGroupName\":\"myMachine\",\"Created\":\"{3}\",\"Compressed\":false}}", MrT, MessageType, ReplyTo, message.Created.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK"));
            Specify.That(result).Should.BeEqualTo(expected);
        }

        [TestMethod]
        public void CanSerializeTWithMachineGroupId()
        {
            var message = new Message<string> { MessageType = MessageType, ReplyTo = ReplyTo, Data = MrT, MachineGroupId = Guid.Parse("4305cb5d-38f7-4639-b130-c2b8b1fe8f77") };

            var result = JsonConvert.SerializeObject(message);

            var expected = String.Format("{{\"Data\":\"{0}\",\"MessageType\":\"{1}\",\"ReplyTo\":\"{2}\",\"MachineGroupId\":\"4305cb5d-38f7-4639-b130-c2b8b1fe8f77\",\"Created\":\"{3}\",\"Compressed\":false}}", MrT, MessageType, ReplyTo, message.Created.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK"));
            Specify.That(result).Should.BeEqualTo(expected);
        }

        [TestMethod]
        public void CanDeserializeArticle()
        {
            var message =
                "{\"MessageType\": \"UpdateArticle\",\"Data\": {\"Id\": null,\"Custom\": false,\"ArticleId\": \"testme\",\"Description\": null,\"Length\": 7,\"Width\": 7,\"Height\": 7,\"DesignId\": 2010001,\"MachineType\": \"Fusion\",\"CorrugateQuality\": 1,\"UserName\": \"qa\",\"LastUpdated\": \"2014-05-13T22:28:12+00:00\"},\"ReplyTo\":\"something\"}";

            var settings = new JsonSerializerSettings
            {
                Converters = new[] {new IsoDateTimeConverter
                {
                    DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffK"
                }}
            };
            
            var result = JsonConvert.DeserializeObject<Message<Article>>(message);
            Specify.That(result.MessageType).Should.BeEqualTo(ArticleMessages.UpdateArticle);
            Specify.That(result.Data.ArticleId).Should.BeEqualTo("testme");
            Specify.That(result.Data.Height).Should.BeEqualTo(7D);
        }


        [TestMethod]
        public void CanSerializeAndDeserializeComplexT()
        {
            var article = new Article { ArticleId = "666", Height = 78.0D };

            var message = new Message<Article>{MessageType = MessageType, ReplyTo = ReplyTo, Data = article};

            var result = JsonConvert.SerializeObject(message);

            var result2 = JsonConvert.DeserializeObject<Message<Article>>(result);

            Specify.That(article.ArticleId).Should.BeEqualTo(result2.Data.ArticleId);
            Specify.That(article.Height).Should.BeEqualTo(result2.Data.Height);
        }

        [TestMethod]
        public void CanDeserializeT()
        {
            var all = MessageTypes.GetAll();
            var serializedObject = String.Format("{{\"MessageType\":\"{0}\",\"Data\":\"{1}\",\"ReplyTo\":\"{2}\"}}", MessageType, MrT, ReplyTo);
            var result = JsonConvert.DeserializeObject<Message<string>>(serializedObject);
            Specify.That(result.Data).Should.BeEqualTo(MrT);
            Specify.That(result.MessageType).Should.BeEqualTo(MessageType);
            Specify.That(result.ReplyTo).Should.BeEqualTo(ReplyTo);
        }

        [TestMethod]
        public void CanDeserializeTWithUsername()
        {
            var serializedObject = String.Format("{{\"MessageType\":\"{0}\",\"Data\":\"{1}\",\"ReplyTo\":\"{2}\",\"UserName\":\"test\"}}", MessageType, MrT, ReplyTo);
            var result = JsonConvert.DeserializeObject<Message<string>>(serializedObject);
            Specify.That(result.Data).Should.BeEqualTo(MrT);
            Specify.That(result.MessageType).Should.BeEqualTo(MessageType);
            Specify.That(result.ReplyTo).Should.BeEqualTo(ReplyTo);
            Specify.That(result.UserName).Should.BeEqualTo("test");
        }
    }
}
