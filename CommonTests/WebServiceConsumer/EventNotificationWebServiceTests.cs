﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.WebServiceConsumer;

namespace CommonTests.WebServiceConsumer
{
    [TestClass]
    public class EventNotificationWebServiceTests
    {
        private IEventNotificationConsumer service;

        [TestInitialize]
        public void Setup()
        {
            service = new EventNotifcationServiceWebService("http://localhost/EventNotifcationService.asmx");
        }

        [TestMethod]
        [Ignore]
        public void ShouldBeAbleToSendMessageToWebService()
        {
            string message = "testing..";
            string reply = service.SendEventNotification(message);

            Assert.AreEqual(message, reply);
        }
    }
}
