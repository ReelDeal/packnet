﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class GetProduciblesForNextCartonPropertyGroupMixTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnListOfProducibles_AccordingToNextCartonPropertyGroupInMix()
        {
            var cpg1 = new CartonPropertyGroup { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup { Id = Guid.NewGuid() };
            var cpgList = new Queue<CartonPropertyGroup>(new []{ cpg1, cpg2 });
            var producibles = SetUpProducibles(new []{ cpg2, cpg1 });
            var cpgServiceMock = new Mock<ICartonPropertyGroupService>();
            var productionGroup = new ProductionGroup();
            cpgServiceMock.Setup(s => s.GetNextCPGToDispatchWorkTo(productionGroup)).Returns(
                cpgList.Dequeue);

            var filteredProducibles = ExecuteWorkflowActivity(cpgServiceMock.Object, productionGroup, producibles);
            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
            Specify.That(AreAllProduciblesAssignedToCpg(filteredProducibles, cpg1))
                .Should.BeTrue("Collection contains producibles with other CPG restriction than cpg1");

            filteredProducibles = ExecuteWorkflowActivity(cpgServiceMock.Object, productionGroup, producibles);
            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
            Specify.That(AreAllProduciblesAssignedToCpg(filteredProducibles, cpg2))
                .Should.BeTrue("Collection contains producibles with other CPG restriction than cpg2");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnListOfProducibles_AccordingToSecondNextCartonPropertyGroupInMix_IfNoProduciblesAreAssignedToNext()
        {
            var cpg1 = new CartonPropertyGroup { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup { Id = Guid.NewGuid() };
            var cpgList = new Queue<CartonPropertyGroup>(new[] { cpg1, cpg2 });
            var producibles = SetUpProducibles(new[] { cpg2 });
            var cpgServiceMock = new Mock<ICartonPropertyGroupService>();
            var productionGroup = new ProductionGroup();
            cpgServiceMock.Setup(s => s.GetNextCPGToDispatchWorkTo(productionGroup)).Returns(
                cpgList.Dequeue);

            var filteredProducibles = ExecuteWorkflowActivity(cpgServiceMock.Object, productionGroup, producibles);
            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
            Specify.That(AreAllProduciblesAssignedToCpg(filteredProducibles, cpg2))
                .Should.BeTrue("Collection contains producibles with other CPG restriction than cpg2");
        }

        private bool AreAllProduciblesAssignedToCpg(IEnumerable<IProducible> filteredProducibles, CartonPropertyGroup cpg)
        {
            return filteredProducibles.All(
                p =>
                    p.Restrictions.Any(
                        r =>
                            r is BasicRestriction<CartonPropertyGroup> &&
                            (r as BasicRestriction<CartonPropertyGroup>).Value.Id == cpg.Id));
        }

        private IEnumerable<IProducible> SetUpProducibles(IEnumerable<CartonPropertyGroup> cartonPropertyGroups)
        {
            var producibles = new List<IProducible>();
            foreach (var group in cartonPropertyGroups)
            {
                var producible = new BoxFirstProducible();
                producible.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(group));
                producibles.Add(producible);
                producible = new BoxFirstProducible();
                producible.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(group));
                producibles.Add(producible);
            }

            return producibles;
        }

        private static IEnumerable<IProducible> ExecuteWorkflowActivity(ICartonPropertyGroupService service, ProductionGroup productionGroup, IEnumerable<IProducible> producibles)
        {
            IEnumerable<IProducible> output = null;
            var arguments = new Dictionary<string, object>
                            {
                                { "CartonPropertyGroupService", service},
                                { "ProductionGroup", productionGroup},
                                { "Producibles", producibles},
                            };

            var completed = false;
            var wfApp = new WorkflowApplication(new GetProduciblesForNextCartonPropertyGroupMix(), arguments)
            {
                Completed = e =>
                {
                    completed = true;
                    output = e.Outputs["Result"] as IEnumerable<IProducible>;
                }
            };

            wfApp.Run();
            Retry.For(() => completed, TimeSpan.FromSeconds(2));
            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");

            return output;
        }
    }
}
