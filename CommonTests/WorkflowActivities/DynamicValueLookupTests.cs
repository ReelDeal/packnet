﻿using System.Dynamic;
using System.Threading;

namespace CommonTests.WorkflowActivities
{
    using System;
    using System.Activities;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Common.Utils;
    using PackNet.Common.WorkflowActivities;
    using Testing.Specificity;

    /// <summary>
    /// Summary description for JsonMessageTests
    /// </summary>
    [TestClass]
    public class DynamicValueLookupTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetPropertyValueAsAString()
        {
            string output = ExecuteWorkflowActivity(CreateDynamicExpandoObject(), "Length");
            //Adding a 2 second delay in an attempt to prevent a non-deterministic result on build server since the test frequently fails only on the build server
            Thread.Sleep(2000);
            Specify.That(output).Should.BeEqualTo("123.4");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowArgumentExpection_WhenDynamicIsNotExpandoObject()
        {
            try
            {
                ExecuteWorkflowActivity(string.Empty as dynamic, string.Empty);
            }
            catch (ArgumentException e)
            {
                Specify.That(e.Message.Contains("must be a ExpandoObject")).Should.BeTrue();
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowArgumentExpection_WhenPropertyNameCannotBeFound()
        {
            try
            {
                ExecuteWorkflowActivity(CreateDynamicExpandoObject(), "test");
            }
            catch (ArgumentException e)
            {
                Specify.That(e.Message.Contains("contain a property named 'test'")).Should.BeTrue();
            }
        }

        private static dynamic CreateDynamicExpandoObject()
        {
            var expando = new ExpandoObject();
            var dictionary = expando as IDictionary<string, Object>;
            dictionary.Add("Length", "123.4");
            return expando;
        }

        private static string ExecuteWorkflowActivity(dynamic dynamicObject, string property)
        {
            var output = string.Empty;
            var arguments = new Dictionary<string, object>
                            {
                                { "PropertyToRetrieve", property},
                                { "Dynamic", dynamicObject},
                            };

            var completed = false;
            var wfApp = new WorkflowApplication(new DynamicValueLookup(), arguments)
            {
                Completed = e =>
                {
                    completed = true;
                    output = e.Outputs["OutputValue"] as string;
                }
            };

            wfApp.Run();
            Retry.For(() => completed, TimeSpan.FromSeconds(2));
            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");

            return output;
        }
    }
}
