﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;

    [TestClass]
    public class GetProduciblesThatCanBeProducedOnMachineGroupTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendInfoMessageToUI_WhenThereAre_NoRemainingProduciblesForMG()
        {
            var serviceMock = new Mock<IServiceLocator>();
            var userNotificationMock = new Mock<IUserNotificationService>();

            var pgMock = new Mock<ProductionGroup>();
            pgMock.Object.ConfiguredCorrugates = new ConcurrentList<Guid>() { Guid.NewGuid(), Guid.NewGuid() };

            var mgId = new Guid("10000000-0000-0000-0000-000000000000");
            var pgServiceMock = new Mock<IProductionGroupService>();
            pgServiceMock.Setup(p => p.GetProductionGroupForMachineGroup(mgId)).Returns(pgMock.Object);
            serviceMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(pgServiceMock.Object);
            
            userNotificationMock.Setup(
                m =>
                    m.SendNotificationToMachineGroup(
                        It.IsAny<NotificationSeverity>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        mgId));

            serviceMock.Setup(m => m.Locate<IUserNotificationService>()).Returns(userNotificationMock.Object);

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceMock.Object, this.GetZeroBoxFirstProducibles(), mgId, new GetProduciblesThatCanBeProducedOnMachineGroup());
            
            userNotificationMock.Verify(u => u.SendNotificationToMachineGroup(NotificationSeverity.Info, It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == mgId)), Times.Once);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendInfoMessageToUI_WhenThereAre_NoConfiguredCorrugatesForPG()
        {
            var serviceMock = new Mock<IServiceLocator>();
            var userNotificationMock = new Mock<IUserNotificationService>();

            var pgMock = new Mock<ProductionGroup>();
 
            var mgId = new Guid("10000000-0000-0000-0000-000000000000");
            var pgServiceMock = new Mock<IProductionGroupService>();
            pgServiceMock.Setup(p => p.GetProductionGroupForMachineGroup(mgId)).Returns(pgMock.Object);
            serviceMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(pgServiceMock.Object);

            userNotificationMock.Setup(
                m =>
                    m.SendNotificationToMachineGroup(
                        It.IsAny<NotificationSeverity>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        mgId));

            serviceMock.Setup(m => m.Locate<IUserNotificationService>()).Returns(userNotificationMock.Object);

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceMock.Object, this.GetZeroBoxFirstProducibles(), mgId, new GetProduciblesThatCanBeProducedOnMachineGroup());

            userNotificationMock.Verify(u => u.SendNotificationToProductionGroup(NotificationSeverity.Warning, It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == pgMock.Object.Id)), Times.Once);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendWarningMessageToUI_WhenThereAre_NoMachinesThatCanProduce_AClassification()
        {
            var serviceMock = new Mock<IServiceLocator>();
            var mgServiceMock = new Mock<IMachineGroupService>();
            var boxFirstServiceMock = new Mock<IBoxFirstSelectionAlgorithmService>();
            var userNotificationMock = new Mock<IUserNotificationService>();

            var mgId = new Guid("10000000-0000-0000-0000-000000000000");

            mgServiceMock.Setup(m => m.FindByMachineGroupId(mgId)).Returns(new MachineGroup());
            boxFirstServiceMock.Setup(
                m =>
                    m.FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(
                        It.IsAny<MachineGroup>(),
                        It.IsAny<IEnumerable<BoxFirstProducible>>())).Returns(new List<BoxFirstProducible>());

            userNotificationMock.Setup(
                m =>
                    m.SendNotificationToMachineGroup(
                        It.IsAny<NotificationSeverity>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        mgId));

            serviceMock.Setup(m => m.Locate<IMachineGroupService>()).Returns(mgServiceMock.Object);
            serviceMock.Setup(m => m.Locate<IBoxFirstSelectionAlgorithmService>()).Returns(boxFirstServiceMock.Object);
            serviceMock.Setup(m => m.Locate<IUserNotificationService>()).Returns(userNotificationMock.Object);

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceMock.Object, GetBoxFirstProducibles(), mgId, new GetProduciblesThatCanBeProducedOnMachineGroup());

            userNotificationMock.Verify(u => u.SendNotificationToMachineGroup(NotificationSeverity.Warning, It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == mgId)), Times.Once);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendWarningMessageToUI_WhenThereAre_NoMachinesThatHave_OptimalCorrugatLoaded()
        {
            var serviceMock = new Mock<IServiceLocator>();
            var mgServiceMock = new Mock<IMachineGroupService>();
            var boxFirstServiceMock = new Mock<IBoxFirstSelectionAlgorithmService>();
            var userNotificationMock = new Mock<IUserNotificationService>();

            var mgId = new Guid("10000000-0000-0000-0000-000000000000");

            mgServiceMock.Setup(m => m.FindByMachineGroupId(mgId)).Returns(new MachineGroup());
            boxFirstServiceMock.Setup(
                m =>
                    m.FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(
                        It.IsAny<MachineGroup>(),
                        It.IsAny<IEnumerable<BoxFirstProducible>>())).Returns(GetBoxFirstProducibles);

            mgServiceMock.Setup(m => m.FindByMachineGroupId(mgId)).Returns(new MachineGroup());
            boxFirstServiceMock.Setup(
                m =>
                    m.GetProduciblesWhereMachineGroupContainsMostOptimalCorrugate(
                        It.IsAny<MachineGroup>(),
                        It.IsAny<IEnumerable<BoxFirstProducible>>())).Returns(new List<BoxFirstProducible>());

            userNotificationMock.Setup(
                m =>
                    m.SendNotificationToMachineGroup(
                        It.IsAny<NotificationSeverity>(),
                        It.IsAny<string>(),
                        It.IsAny<string>(),
                        mgId));

            serviceMock.Setup(m => m.Locate<IMachineGroupService>()).Returns(mgServiceMock.Object);
            serviceMock.Setup(m => m.Locate<IBoxFirstSelectionAlgorithmService>()).Returns(boxFirstServiceMock.Object);
            serviceMock.Setup(m => m.Locate<IUserNotificationService>()).Returns(userNotificationMock.Object);

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceMock.Object, GetBoxFirstProducibles(), mgId, new GetProduciblesThatCanBeProducedOnMachineGroup());

            userNotificationMock.Verify(u => u.SendNotificationToMachineGroup(NotificationSeverity.Warning, It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == mgId)), Times.Once);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        private IEnumerable<BoxFirstProducible> GetZeroBoxFirstProducibles()
        {
            return new List<BoxFirstProducible>();
        }


        private IEnumerable<BoxFirstProducible> GetBoxFirstProducibles()
        {
            var cpg1 = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000001"), Alias = "cpg1" };
            var cpg2 = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cpg2" };
            var cls1 = new Classification() { Id = new Guid("00000000-0000-0000-0000-000000000001"), Alias = "cls1"};
            var cls2 = new Classification() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cls2"};
            var pz1 = new PickZone() { Id = new Guid("00000000-0000-0000-0000-000000000001") };
            var pz2 = new PickZone() { Id = new Guid("00000000-0000-0000-0000-000000000002") };

            var p1 = new BoxFirstProducible();
            p1.Id = new Guid("10000000-0000-0000-0000-000000000001");
            p1.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p1.Producible = new Carton();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg1));
            p1.Restrictions.Add(new BasicRestriction<Classification>(cls1));
            p1.Restrictions.Add(new BasicRestriction<PickZone>(pz1));
            p1.Restrictions.Add(new BasicRestriction<string>("StandardLabel"));

            var p2 = new BoxFirstProducible();
            p2.Id = new Guid("20000000-0000-0000-0000-000000000001");
            p2.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p2.Producible = new Carton();
            p2.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg1));
            p2.Restrictions.Add(new BasicRestriction<Classification>(cls1));
            p2.Restrictions.Add(new BasicRestriction<PickZone>(pz1));
            p2.Restrictions.Add(new BasicRestriction<string>("StandardLabel"));

            var p3 = new BoxFirstProducible();
            p3.Id = new Guid("30000000-0000-0000-0000-000000000002");
            p3.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p3.Producible = new Carton();
            p3.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg2));
            p3.Restrictions.Add(new BasicRestriction<Classification>(cls2));
            p3.Restrictions.Add(new BasicRestriction<PickZone>(pz2));
            p3.Restrictions.Add(new BasicRestriction<string>("PriorityLabel"));

            var p4 = new BoxFirstProducible();
            p4.Id = new Guid("40000000-0000-0000-0000-000000000002");
            p4.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p4.Producible = new Label();
            p4.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg2));
            p4.Restrictions.Add(new BasicRestriction<Classification>(cls2));
            p4.Restrictions.Add(new BasicRestriction<PickZone>(pz2));
            p4.Restrictions.Add(new BasicRestriction<string>("PriorityLabel"));

            return new List<BoxFirstProducible>() { p1, p2, p3, p4 };
        }
    }
}
