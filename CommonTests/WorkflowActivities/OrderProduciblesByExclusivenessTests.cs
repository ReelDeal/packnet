﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

using TestUtils;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class OrderProduciblesByExclusivenessTests
    {
        [TestMethod]
        public void ShouldOrderProducibliesByExclusiveness()
        {
            var c1 = new Corrugate() { Alias = "C1", Id = Guid.NewGuid(), Quality = 1, Thickness = 0.01, Width = 20 };
            var c2 = new Corrugate() { Alias = "C2", Id = Guid.NewGuid(), Quality = 1, Thickness = 0.01, Width = 30 };
            var corrugates = new List<Corrugate>() { c1, c2 };

            var m1 = new FusionMachine()
            {
                Alias = "M1",
                Id = Guid.NewGuid(),
                Tracks =
                    new ConcurrentList<Track>()
                    {
                        new Track() { TrackNumber = 1, LoadedCorrugate = c2 }
                    }
            };

            var m2 = new FusionMachine()
            {
                Alias = "M2",
                Id = Guid.NewGuid(),
                Tracks =
                    new ConcurrentList<Track>()
                    {
                        new Track() { TrackNumber = 1, LoadedCorrugate = c1 },
                        new Track() { TrackNumber = 2, LoadedCorrugate = c2 }
                    }
            };

            var machines = new List<IPacksizeCutCreaseMachine>() { m1, m2 };

            var mg1 = new MachineGroup() { Alias = "MG1", Id = Guid.NewGuid() };
            mg1.ConfiguredMachines.Add(m1.Id);
            mg1.ConfiguredMachines.Add(m2.Id);

            var pg1 = new ProductionGroup() { Alias = "PG1", Id = Guid.NewGuid() };
            pg1.ConfiguredMachineGroups.Add(mg1.Id);

            var p1 = GetProducible(15, 15, 14, corrugates, machines);
            var p2 = GetProducible(15, 15, 14, corrugates, machines);
            var p3 = GetProducible(15, 15, 14, corrugates, machines);
            var p4 = GetProducible(15, 15, 14, corrugates, machines);
            var p5 = GetProducible(15, 10, 9, corrugates, machines);
            var p6 = GetProducible(15, 10, 9, corrugates, machines);
            var p7 = GetProducible(15, 10, 9, corrugates, machines);
            var p8 = GetProducible(15, 10, 9, corrugates, machines);

            var producibles = new List<IProducible>() { p1, p2, p3, p4, p5, p6, p7, p8 };
            var serviceLocator = GetServiceLocatorMock(new List<ProductionGroup>() { pg1 }, new List<MachineGroup>() { mg1 }, machines);
            var orderedProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, mg1.Id, new OrderProduciblesByExclusiveness());

            Specify.That(orderedProducibles).Should.Not.BeNull();
            Specify.That(orderedProducibles.SequenceEqual(producibles)).Should.BeFalse();
            Specify.That(orderedProducibles.Count()).Should.BeEqualTo(8);

            var o1 = orderedProducibles.ElementAt(0) as BoxFirstProducible;
            Specify.That(((o1.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(10d);
            var o2 = orderedProducibles.ElementAt(1) as BoxFirstProducible;
            Specify.That(((o2.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(10d);
            var o3 = orderedProducibles.ElementAt(2) as BoxFirstProducible;
            Specify.That(((o3.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(10d);
            var o4 = orderedProducibles.ElementAt(3) as BoxFirstProducible;
            Specify.That(((o4.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(10d);
            var o5 = orderedProducibles.ElementAt(4) as BoxFirstProducible;
            Specify.That(((o5.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(15d);
            var o6 = orderedProducibles.ElementAt(5) as BoxFirstProducible;
            Specify.That(((o6.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(15d);
            var o7 = orderedProducibles.ElementAt(6) as BoxFirstProducible;
            Specify.That(((o7.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(15d);
            var o8 = orderedProducibles.ElementAt(7) as BoxFirstProducible;
            Specify.That(((o8.Producible as Kit).ItemsToProduce.First(i => i is Carton) as Carton).Width).Should.BeEqualTo(15d);
        }

        private IServiceLocator GetServiceLocatorMock(IEnumerable<ProductionGroup> productionGroups, IEnumerable<MachineGroup> machineGroups, IEnumerable<IPacksizeCutCreaseMachine> machines)
        {
            var mock = new Mock<IServiceLocator>();
            mock.Setup(m => m.Locate<IProductionGroupService>()).Returns(GetProductionGroupService(productionGroups));
            mock.Setup(m => m.Locate<IMachineGroupService>()).Returns(GetMachineGroupService(machineGroups));
            mock.Setup(m => m.Locate<IAggregateMachineService>()).Returns(GetMachineService(machines));
            mock.Setup(m => m.Locate<ILogger>()).Returns(new ConsoleLogger());
            return mock.Object;
        }

        private IAggregateMachineService GetMachineService(IEnumerable<IPacksizeCutCreaseMachine> machines)
        {
            var mock = new Mock<IAggregateMachineService>();
            mock.Setup(m => m.Machines).Returns(machines);
            return mock.Object;
        }

        private IMachineGroupService GetMachineGroupService(IEnumerable<MachineGroup> machineGroups)
        {
            var mock = new Mock<IMachineGroupService>();
            mock.Setup(m => m.MachineGroups).Returns(machineGroups);
            return mock.Object;
        }

        private IProductionGroupService GetProductionGroupService(IEnumerable<ProductionGroup> productionGroups)
        {
            var mock = new Mock<IProductionGroupService>();
            mock.Setup(m => m.ProductionGroups).Returns(productionGroups);
            return mock.Object;
        }

        private IProducible GetProducible(double l, double w, double h, IEnumerable<Corrugate> corrugates, IEnumerable<IPacksizeCutCreaseMachine> machines)
        {
            var p = new BoxFirstProducible();
            var k = new Kit();
            k.AddProducible(new Carton() {Length = l, Width = w, Height = h} );
            k.AddProducible(new Label());
            p.Producible = k;
            p.CartonOnCorrugates = GetCartonOnCorrugates(w, h, corrugates, machines);
            return p;
        }

        private IEnumerable<CartonOnCorrugate> GetCartonOnCorrugates(double w, double h, IEnumerable<Corrugate> corrugates, IEnumerable<IPacksizeCutCreaseMachine> machines)
        {
            var cartonOnCorrugates = new List<CartonOnCorrugate>();
            foreach (var corrugate in corrugates)
            {
                for (var i = 1; (w + h) * i < corrugate.Width; i++)
                {
                    var coc = new CartonOnCorrugate();
                    coc.Corrugate = corrugate;
                    coc.TileCount = i;
                    coc.ProducibleMachines = machines.Where(m => MachineHasCorrugateLoaded(m, corrugate)).ToDictionary(m => m.Id, m => m.Alias);
                    cartonOnCorrugates.Add(coc);
                }
            }
            return cartonOnCorrugates;
        }

        private bool MachineHasCorrugateLoaded(IPacksizeCutCreaseMachine machine, Corrugate corrugate)
        {
            return machine.Tracks.Any(t => t.LoadedCorrugate != null && t.LoadedCorrugate.Id == corrugate.Id);
        }
    }
}
