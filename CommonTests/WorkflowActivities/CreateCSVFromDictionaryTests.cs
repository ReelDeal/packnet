﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    using System.Activities;

    using PackNet.Common.Utils;

    [TestClass]
    public class CreateCSVFromDictionaryTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConvertToCSVWithHeaders()
        {
            var toBeConverted = new Dictionary<string, string>() { { "Field1", "1" }, { "Field2", "2" } };

            var args = new Dictionary<string, object>()
                       {
                           {"Dictionaries", new List<Dictionary<string, string>>{toBeConverted}},
                           {"UseHeaders", true},
                           {"Separator", ';'}
                       };
            var csv = Execute(args, new CreateCSVFromDictionary());

            Specify.That(csv).Should.Not.BeNull();
            Specify.That(csv).Should.Not.BeLogicallyEqualTo("");
            Specify.That(csv).Should.BeLogicallyEqualTo("Field1;Field2\r\n1;2\r\n");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConvertToCSVWithMisMatchedHeaders()
        {
            var toBeConverted = new List<Dictionary<string, string>>()
            {
                new Dictionary<string, string> { { "Field1", "1" }, { "Field2", "2" } },
                new Dictionary<string, string>{ { "Field1", "1" }, { "Field2", "2" }, { "Field3", "3" }, { "Field4", "4" } }
            };

            var args = new Dictionary<string, object>()
                       {
                           {"Dictionaries", toBeConverted},
                           {"UseHeaders", true},
                           {"Separator", ';'}
                       };
            var csv = Execute(args, new CreateCSVFromDictionary());

            Specify.That(csv).Should.Not.BeNull();
            Specify.That(csv).Should.Not.BeLogicallyEqualTo("");
            Specify.That(csv).Should.BeLogicallyEqualTo("Field1;Field2;Field3;Field4\r\n1;2;;\r\n1;2;3;4\r\n");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConvertToCSVWithoutHeaders()
        {
            var toBeConverted = new Dictionary<string, string>() { { "Field1", "1" }, { "Field2", "2" } };

            var args = new Dictionary<string, object>()
                       {
                           {"Dictionaries", new List<Dictionary<string, string>>{toBeConverted}},
                           {"UseHeaders", false},
                           {"Separator", ';'}
                       };

            var csv = Execute(args, new CreateCSVFromDictionary());
            Specify.That(csv).Should.Not.BeNull();
            Specify.That(csv).Should.Not.BeLogicallyEqualTo("");
            Specify.That(csv).Should.BeLogicallyEqualTo("1;2\r\n");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConvertToCSVWithChosenSeparator()
        {
            var toBeConverted = new Dictionary<string, string>() { { "Field1", "1" }, { "Field2", "2" } };

            var args = new Dictionary<string, object>()
                       {
                           {"Dictionaries", new List<Dictionary<string, string>>{toBeConverted}},
                           {"UseHeaders", true},
                           {"Separator", ','}
                       };

            var csv = Execute(args, new CreateCSVFromDictionary());
            Specify.That(csv).Should.Not.BeNull();
            Specify.That(csv).Should.Not.BeLogicallyEqualTo("");
            Specify.That(csv).Should.BeLogicallyEqualTo("Field1,Field2\r\n1,2\r\n");
        }

        private static string Execute(Dictionary<string, object> arguments, Activity workflowactivity)
        {
            var output = "";
            var completed = false;
            var wfApp = new WorkflowApplication(workflowactivity, arguments)
            {
                Completed = e =>
                {
                    completed = true;
                    output = e.Outputs["CSVString"] as string;
                }
            };

            wfApp.Run();
            Retry.For(() => completed, TimeSpan.FromSeconds(2));
            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");

            return output;
        }
    }
}
