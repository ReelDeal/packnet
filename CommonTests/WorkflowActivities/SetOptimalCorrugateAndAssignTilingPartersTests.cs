﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.WorkflowActivities;
using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Utils;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class SetOptimalCorrugateAndAssignTilingPartersTests
    {
        [TestMethod]
        public void ShouldAddAllCorrugatesAndTilingParters()
        {
            Assert.Inconclusive("not done yet");
            var producibles = GetBoxFirstProducibles().ToList();
            var modifiedProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(GetServiceLocatorMock().Object, producibles, new SetOptimalCorrugateAndAssignTilingPartners());

            Specify.That(modifiedProducibles).Should.Not.BeNull();
        }


        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var serviceMock = new Mock<IServiceLocator>();
            serviceMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(GetProductionGroupServiceMock().Object);
            serviceMock.Setup(s => s.Locate<ICorrugateService>()).Returns(GetCorrugateServiceMock().Object);
            serviceMock.Setup(s => s.Locate<IMachineGroupService>()).Returns(GetMachineGroupServiceMock().Object);
            serviceMock.Setup(s => s.Locate<IAggregateMachineService>()).Returns(GetMachineServiceMock().Object);

            return serviceMock;
        }

        private Mock<IAggregateMachineService> GetMachineServiceMock()
        {
            var serviceMock = new Mock<IAggregateMachineService>();
            serviceMock.Setup(s => s.Machines).Returns(GetMachines());

            return serviceMock;
        }

        private IEnumerable<IMachine> GetMachines()
        {
            var m1 = new EmMachine();
            m1.Id = new Guid("00000000-0000-0000-1000-000000000000");
            m1.Tracks = new ConcurrentList<Track>() { new Track() { TrackNumber = 1 }, new Track() { TrackNumber = 2 } };
            var corrugates = GetCorrugates();
            m1.Tracks.ElementAt(0).LoadedCorrugate = corrugates.First(c => c.Id == new Guid("00000000-1000-0000-0000-000000000000"));
            m1.Tracks.ElementAt(1).LoadedCorrugate = corrugates.First(c => c.Id == new Guid("00000000-2000-0000-0000-000000000000"));

            return new List<IMachine>() { m1 };
        }

        private Mock<IMachineGroupService> GetMachineGroupServiceMock()
        {
            var serviceMock = new Mock<IMachineGroupService>();
            serviceMock.Setup(s => s.MachineGroups).Returns(GetMachineGroups);

            return serviceMock;
        }

        private IEnumerable<MachineGroup> GetMachineGroups()
        {
            var mg = new MachineGroup();
            mg.Id = new Guid("00000000-0000-1000-0000-000000000000");
            mg.ConfiguredMachines.Add(new Guid("00000000-0000-0000-1000-000000000000"));
            mg.ConfiguredMachines.Add(new Guid("00000000-0000-0000-2000-000000000000"));

            return new List<MachineGroup>() { mg };
        }


        private Mock<ICorrugateService> GetCorrugateServiceMock()
        {
            var serviceMock = new Mock<ICorrugateService>();
            serviceMock.Setup(s => s.Corrugates).Returns(GetCorrugates());
            //serviceMock.Setup(s => s.GetOptimalCorrugates(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(), It.IsAny<ICarton>(), It.IsAny<bool>()))
            //    .Callback<IEnumerable<Corrugate>, IEnumerable<IPacksizeCutCreaseMachine>, ICarton, bool>((corrugates, machines, carton, recalculate) => GetOptimalCorrugates(corrugates, machines, carton, recalculate));
            serviceMock.Setup(s => s.GetOptimalCorrugates(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(), It.IsAny<ICarton>(), It.IsAny<bool>(), It.IsAny<int>())).Returns(GetOptimalCorrugates());
            
            return serviceMock;
        }

        //private IEnumerable<CartonOnCorrugate> GetOptimalCorrugates(IEnumerable<Corrugate> corrugates, IEnumerable<IPacksizeCutCreaseMachine> machines, ICarton carton, bool recalculateCachedItem)
        //{
        //    //var calc = new OptimalCorrugateCalculator(new Mock<IPackagingDesignManager>().Object, new CorrugateCalculations(new CachingService(new MemoryCache(new Mock<ILogger>().Object))), GetMachineGroupServiceMock().Object, new Mock<IProducibleValidator>().Object));
        //    //var calc = new OptimalCorrugateCalculator(new Mock<IPackagingDesignManager>().Object, new Mock<ICorrugateCalculations>().Object, GetMachineGroupServiceMock().Object, new Mock<IProducibleValidator>().Object);
        //    var calc = new OptimalCorrugateCalculator(new Mock<IPackagingDesignManager>().Object, new Mock<ICorrugateCalculations>().Object, new Mock<IMachineGroupService>().Object, new Mock<IProducibleValidator>().Object);
        //    return calc.GetOptimalCorrugates(corrugates, machines, carton, recalculateCachedItem);
        //}

        private IEnumerable<CartonOnCorrugate> GetOptimalCorrugates()
        {
            var c1 = new CartonOnCorrugate();
            c1.TileCount = 1;
            //c1.Rotated = false;
            c1.CartonWidthOnCorrugate = 300;

            var c2 = new CartonOnCorrugate();
            c2.TileCount = 2;
            //c2.Rotated = false;
            c2.CartonWidthOnCorrugate = 600;

            var c3 = new CartonOnCorrugate();
            c3.TileCount = 3;
            //c3.Rotated = false;
            c3.CartonWidthOnCorrugate = 900;

            return new List<CartonOnCorrugate>() { c1, c2, c3 };
        }

        private IEnumerable<Corrugate> GetCorrugates()
        {
            var c1 = new Corrugate();
            c1.Id = new Guid("00000000-1000-0000-0000-000000000000");
            c1.Width = 300;
            c1.Thickness = 3;

            var c2 = new Corrugate();
            c2.Id = new Guid("00000000-2000-0000-0000-000000000000");
            c2.Width = 600;
            c2.Thickness = 3;

            var c3 = new Corrugate();
            c3.Id = new Guid("00000000-3000-0000-0000-000000000000");
            c3.Width = 900;
            c3.Thickness = 3;

            return new List<Corrugate>() { c1, c2, c3 };
        }

        private Mock<IProductionGroupService> GetProductionGroupServiceMock()
        {
            var serviceMock = new Mock<IProductionGroupService>();
            serviceMock.Setup(s => s.FindByCartonPropertyGroupId(It.IsAny<Guid>()))
                .Returns(GetProductionGroup());
            return serviceMock;
        }

        private ProductionGroup GetProductionGroup()
        {
            var prodGroup = new ProductionGroup();
            prodGroup.ConfiguredCorrugates.Add(new Guid("00000000-1000-0000-0000-000000000000"));
            prodGroup.ConfiguredCorrugates.Add(new Guid("00000000-2000-0000-0000-000000000000"));
            prodGroup.ConfiguredMachineGroups.Add(new Guid("00000000-0000-1000-0000-000000000000"));

            return prodGroup;
        }

        private IEnumerable<BoxFirstProducible> GetBoxFirstProducibles()
        {
            var cpg1 = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000001") };
            var cpg2 = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000002") };
            var cpg3 = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000003") };

            var p1 = new BoxFirstProducible();
            p1.Id = new Guid("10000000-0000-0000-0000-000000000001");
            p1.Producible = new Carton();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg1));

            var p2 = new BoxFirstProducible();
            p2.Id = new Guid("20000000-0000-0000-0000-000000000001");
            p2.Producible = new Carton();
            p2.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg1));

            var p3 = new BoxFirstProducible();
            p3.Id = new Guid("30000000-0000-0000-0000-000000000002");
            p3.Producible = new Carton();
            p3.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg2));

            //var p4 = new BoxFirstProducible();
            //p4.Id = new Guid("40000000-0000-0000-0000-000000000002");
            //p4.Producible = new Label();
            //p4.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg2));

            //var p5 = new BoxFirstProducible();
            //p5.Id = new Guid("50000000-0000-0000-0000-000000000003");
            //p5.Producible = new Label();
            //p5.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg3));

            //return new List<BoxFirstProducible>() { p1, p2, p3, p4, p5 };
            return new List<BoxFirstProducible>() { p1, p2, p3 };
        }
    }
}
