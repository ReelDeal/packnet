﻿using System.Dynamic;
using System.Threading;

using Moq;

using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Services;

namespace CommonTests.WorkflowActivities
{
    using System;
    using System.Activities;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Common.Utils;
    using PackNet.Common.WorkflowActivities;
    using Testing.Specificity;

    /// <summary>
    /// Summary description for JsonMessageTests
    /// </summary>
    [TestClass]
    public class GetXValuesTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetXValues_FromDynamicObject()
        {

            var serviceLocator = new Mock<IServiceLocator>();
            var packagingDesignService = new Mock<IPackagingDesignService>();
            var mockParams = new List<DesignParameter>();
            packagingDesignService.Setup(s => s.GetDesignFromId(12345)).Returns(new PackagingDesign { Name = "A-Valid-Design", DesignParameters = mockParams });
            serviceLocator.Setup(s => s.Locate<IPackagingDesignService>()).Returns(packagingDesignService.Object);

            IDictionary<string, double> output = ExecuteWorkflowActivity(CreateDynamicExpandoObject(), serviceLocator.Object, 12345);
            //Adding a 2 second delay in an attempt to prevent a non-deterministic result on build server since the test frequently fails only on the build server
            Thread.Sleep(2000);
            Specify.That(output).Should.Not.BeNull("We should get XValues in the form of a dictionary of string/double");
            Specify.That(output.ContainsKey("X1")).Should.BeTrue("X1 should have been imported");
            Specify.That(output.ContainsKey("X2")).Should.BeTrue("X2 should have been imported");
            Specify.That(output.ContainsKey("X3")).Should.BeTrue("X3 should have been imported");
            Specify.That(output.ContainsKey("X4")).Should.BeTrue("X4 should have been imported");
            Specify.That(output.ContainsKey("X5")).Should.BeTrue("X5 should have been imported");
            Specify.That(output.ContainsKey("X6")).Should.BeFalse("X6 should not have been imported");
            Specify.That(output.ContainsKey("X7")).Should.BeFalse("X7 should not have been imported");
            Specify.That(output.ContainsKey("X8")).Should.BeFalse("X8 should not have been imported");
            Specify.That(output.ContainsKey("X9")).Should.BeFalse("X9 should not have been imported");

            Specify.That(output["X1"]).Should.BeLogicallyEqualTo(123.4);
            Specify.That(output["X2"]).Should.BeLogicallyEqualTo(1);
            Specify.That(output["X3"]).Should.BeLogicallyEqualTo(0.1);
            Specify.That(output["X4"]).Should.BeLogicallyEqualTo(0.1);
            Specify.That(output["X5"]).Should.BeLogicallyEqualTo(0);

        }

        private static dynamic CreateDynamicExpandoObject()
        {
            var expando = new ExpandoObject();
            var dictionary = expando as IDictionary<string, Object>;
            dictionary.Add("X1", "123.4");
            dictionary.Add("X2", "1");
            dictionary.Add("X3", "0.1");
            dictionary.Add("X4", ".1");
            dictionary.Add("X5", "0");
            dictionary.Add("X6", string.Empty);
            dictionary.Add("X7", "abc");
            dictionary.Add("X8", null);
            dictionary.Add("X9", "1.a");
            return expando;
        }

        private static IDictionary<string, double> ExecuteWorkflowActivity(dynamic dynamicObject, IServiceLocator serviceLocator, int designId)
        {
            IDictionary<string, double> output = null;
            var arguments = new Dictionary<string, object>
                            {
                                { "Dynamic", dynamicObject},
                                {"ServiceLocator", serviceLocator},
                                {"DesignId", designId}
                            };

            var completed = false;
            var getXValues = new GetXValues();
            var wfApp = new WorkflowApplication(getXValues, arguments)
            {
                Completed = e =>
                {
                    completed = true;
                    output = e.Outputs["ParsedOutputValue"] as IDictionary<string, double>;
                }
            };

            wfApp.Run();
            Retry.For(() => completed, TimeSpan.FromSeconds(200));
            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");

            return output;
        }
    }
}
