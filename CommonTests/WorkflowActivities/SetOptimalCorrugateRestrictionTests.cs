﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.Orders;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;
    using PackNet.Common.Interfaces.Enums.ProducibleStates;
    using PackNet.Common.Interfaces.Machines;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
    using PackNet.Common.Interfaces.Services.Machines;
    using PackNet.Services;
    using PackNet.Services.MachineServices;

    [TestClass]
    public class SetOptimalCorrugateRestrictionTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPropagateRestrictions_FromProduciblesInOrdersInKits_ToTheMainProducible()
        {

            var corrugate = new Corrugate() { Alias = "Cr1", Id = Guid.NewGuid(), Quality = 1, Thickness = 0.11d, Width = 29d };
            var machine = new FusionMachine() { Alias = "Fusion", Id = Guid.NewGuid() };
            machine.Tracks = new ConcurrentList<Track>();
            machine.Tracks.Add(new Track(){TrackNumber = 1, TrackOffset = 5});
            machine.LoadCorrugate(machine.Tracks.First(), corrugate);
            var mgGuid = Guid.NewGuid();
            var machineGroup = new MachineGroup()
                               {
                                   Alias = "MG1",
                                   Id = mgGuid,
                                   ConfiguredMachines = new ConcurrentList<Guid> { machine.Id }
                               };
            
            var mgServiceMock = new Mock<IMachineGroupService>();
            mgServiceMock.Setup(m => m.MachineGroups).Returns(new List<MachineGroup> { machineGroup });


            var machineServiceMock = new Mock<IAggregateMachineService>();
            machineServiceMock.Setup(s => s.FindByAlias("Fusion")).Returns(machine);
            machineServiceMock.Setup(s => s.FindById(machine.Id)).Returns(machine);
            machineServiceMock.Setup(s => s.IsCorrugateLoadedOnAnyMachine(corrugate)).Returns(true);
            machineServiceMock.Setup(s => s.Machines).Returns(new List<IPacksizeCutCreaseMachine>{machine});

            IProducible carton = new Carton(){Length = 5, Width = 5, Height = 5, DesignId = 2010001};
            var order = new Order(carton, 2);
            var kit = new Kit();
            kit.AddProducible(order);

            
            var serviceLocatorMock = GetServiceLocatorMock();
            var productionGroup = new ProductionGroup(){Alias = "PG1"};
            productionGroup.ConfiguredMachineGroups.Add(mgGuid);
            var pgService = new Mock<IProductionGroupService>();
            pgService.Setup(p => p.FindByAlias(productionGroup.Alias)).Returns(productionGroup);
            pgService.Setup(p => p.ProductionGroups).Returns(new List<ProductionGroup> { productionGroup });


            var corrugateServiceMock = new Mock<ICorrugateService>();
            corrugateServiceMock.Setup(
                s =>
                    s.GetOptimalCorrugate(
                        new List<Corrugate> { corrugate },
                        new List<IPacksizeCutCreaseMachine> { machine },
                        carton as ICarton,
                        It.IsAny<int>(),
                        It.IsAny<bool>())).Returns(new CartonOnCorrugate(carton as Carton, corrugate, "2010001:5:5:5", new PackagingDesignCalculation(){Length = 20, Width = 10}, OrientationEnum.Degree0));

            serviceLocatorMock.Setup(s => s.Locate<ICorrugateService>()).Returns(corrugateServiceMock.Object);
            serviceLocatorMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(pgService.Object);
            serviceLocatorMock.Setup(s => s.Locate<IMachineGroupService>()).Returns(mgServiceMock.Object);
            serviceLocatorMock.Setup(s => s.Locate<IAggregateMachineService>()).Returns(machineServiceMock.Object);

            var arguments = new Dictionary<string, object>
                            {
                                { "Producible", kit},
                                { "ProductionGroupAlias", productionGroup.Alias},
                                { "ServiceLocator", serviceLocatorMock.Object},
                                { "AllowedTileCount", 1},
                                { "RecalculateCachedItem", false},
                            };

            kit.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported;
            Specify.That(kit.Restrictions.Count).Should.BeEqualTo(0, "No restrictions should have have been added yet.");
            WorkflowActivitiesTestHelper.ExecuteWithoutReturnValue(arguments, new SetOptimalCorrugateRestrictionBasedOnProductionGroup(), TimeSpan.FromSeconds(5));
            Specify.That(kit.ProducibleStatus).Should.BeLogicallyEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(kit.Restrictions.Count).Should.BeEqualTo(1, "Should have a corrugate restriction on the kit now");
            Specify.That(kit.Restrictions.First() is MustProduceWithOptimalCorrugateRestriction).Should.BeTrue("The restriction should be a corrugate restriction");
            Specify.That((kit.Restrictions.First() as MustProduceWithOptimalCorrugateRestriction).Value.Corrugate.Alias).Should.BeLogicallyEqualTo(corrugate.Alias);
        }
 


        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var serviceMock = new Mock<IServiceLocator>();
            return serviceMock;
        }

    }
}
