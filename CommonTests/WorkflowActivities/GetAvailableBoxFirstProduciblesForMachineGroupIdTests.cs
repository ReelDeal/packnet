﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class GetAvailableBoxFirstProduciblesForMachineGroupIdTests
    {
        private MachineGroup machineGroup;

        [TestInitialize]
        public void Setup()
        {
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetAvailableProduciblesFromBoxFirstService()
        {
            machineGroup = new MachineGroup()
            {
                Id = Guid.NewGuid()
            };

            var stagedProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(GetServiceLocatorMock().Object, machineGroup.Id, new GetAvailableBoxFirstProduciblesForMachineGroupId());
            Specify.That(stagedProducibles.Count()).Should.BeEqualTo(4);
        }

        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var serviceMock = new Mock<IServiceLocator>();
            serviceMock.Setup(s => s.Locate<IBoxFirstSelectionAlgorithmService>()).Returns(GetBoxFirstServiceMock().Object);
            return serviceMock;
        }

        private Mock<IBoxFirstSelectionAlgorithmService> GetBoxFirstServiceMock()
        {
            var serviceMock = new Mock<IBoxFirstSelectionAlgorithmService>();
            serviceMock.Setup(s => s.GetAllStagedJobsForMachineGroup(It.IsAny<Guid>())).Callback<Guid>((g) => Specify.That(g).Should.BeEqualTo(machineGroup.Id)).Returns(GetBoxFirstProducibles());
            return serviceMock;
        }

        private IEnumerable<BoxFirstProducible> GetBoxFirstProducibles()
        {
            var p1 = new BoxFirstProducible();
            p1.Id = new Guid("10000000-0000-0000-0000-000000000001");
            p1.Producible = new Carton();
            
            var p2 = new BoxFirstProducible();
            p2.Id = new Guid("20000000-0000-0000-0000-000000000001");
            p2.Producible = new Carton();
            
            var p3 = new BoxFirstProducible();
            p3.Id = new Guid("30000000-0000-0000-0000-000000000002");
            p3.Producible = new Carton();
            
            var p4 = new BoxFirstProducible();
            p4.Id = new Guid("40000000-0000-0000-0000-000000000002");
            p4.Producible = new Label();
            
            return new List<BoxFirstProducible>() { p1, p2, p3, p4 };
        }
    }
}
