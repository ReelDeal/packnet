﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class GetProduciblesWithHighestClassificationTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterOutProduciblesWithLowerClassificationsThanTheHighestOne()
        {
            var c1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", Status = ClassificationStatuses.Expedite };
            var c2 = new Classification() { Id = Guid.NewGuid(), Alias = "cls2", Status = ClassificationStatuses.Normal };
            var c3 = new Classification() { Id = Guid.NewGuid(), Alias = "cls3", Status = ClassificationStatuses.Last };
            var classifications = new List<Classification>() { c1, c2, c3 };

            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<Classification>() {Value = c2 });

            var p2 = new BoxFirstProducible();
            p2.Restrictions.Add(new BasicRestriction<Classification>() { Value = c2 });

            var p3 = new BoxFirstProducible();
            p3.Restrictions.Add(new BasicRestriction<Classification>() { Value = c3 });

            var p4 = new BoxFirstProducible();
            p4.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var p5 = new BoxFirstProducible();
            p5.Restrictions.Add(new BasicRestriction<Classification>() { Value = c2 });

            var p6 = new BoxFirstProducible();
            p6.Restrictions.Add(new BasicRestriction<Classification>() { Value = c3 });

            var producibles = new List<IProducible>() { p1, p2, p3, p4, p5, p6 };
            var classificationService = GetClassificationServiceMock(classifications).Object;
            var serviceLocator = GetServiceLocatorMock(classificationService).Object;

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, new GetProduciblesWithHighestClassification());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(1);
            Specify.That((filteredProducibles.First() as BoxFirstProducible).Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>().Status).Should.BeEqualTo(ClassificationStatuses.Expedite);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnNormalIfThatIsHighest()
        {
            var c1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", Status = ClassificationStatuses.Normal };
            var c2 = new Classification() { Id = Guid.NewGuid(), Alias = "cls2", Status = ClassificationStatuses.Last };
            var classifications = new List<Classification>() { c1, c2 };

            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var p2 = new BoxFirstProducible();
            p2.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var p3 = new BoxFirstProducible();
            p3.Restrictions.Add(new BasicRestriction<Classification>() { Value = c2 });

            var p4 = new BoxFirstProducible();
            p4.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var p5 = new BoxFirstProducible();
            p5.Restrictions.Add(new BasicRestriction<Classification>() { Value = c2 });

            var producibles = new List<IProducible>() { p1, p2, p3, p4, p5 };
            var classificationService = GetClassificationServiceMock(classifications).Object;
            var serviceLocator = GetServiceLocatorMock(classificationService).Object;

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, new GetProduciblesWithHighestClassification());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(3);
            Specify.That((filteredProducibles.First() as BoxFirstProducible).Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>().Status).Should.BeEqualTo(ClassificationStatuses.Normal);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnLastIfThatIsHighest()
        {
            var c1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", Status = ClassificationStatuses.Last };
            var c2 = new Classification() { Id = Guid.NewGuid(), Alias = "cls2", Status = ClassificationStatuses.Stop };
            var classifications = new List<Classification>() { c1, c2 };

            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<Classification>() { Value = c2 });

            var p2 = new BoxFirstProducible();
            p2.Restrictions.Add(new BasicRestriction<Classification>() { Value = c2 });

            var p3 = new BoxFirstProducible();
            p3.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var p4 = new BoxFirstProducible();
            p4.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var producibles = new List<IProducible>() { p1, p2, p3, p4 };
            var classificationService = GetClassificationServiceMock(classifications).Object;
            var serviceLocator = GetServiceLocatorMock(classificationService).Object;

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, new GetProduciblesWithHighestClassification());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
            Specify.That((filteredProducibles.First() as BoxFirstProducible).Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>().Status).Should.BeEqualTo(ClassificationStatuses.Last);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnEmptyListIfOnlyStoppedClassificationsExists()
        {
            var c1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", Status = ClassificationStatuses.Stop };
            var classifications = new List<Classification>() { c1 };

            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var p2 = new BoxFirstProducible();
            p2.Restrictions.Add(new BasicRestriction<Classification>() { Value = c1 });

            var producibles = new List<IProducible>() { p1, p2};
            var classificationService = GetClassificationServiceMock(classifications).Object;
            var serviceLocator = GetServiceLocatorMock(classificationService).Object;

            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, new GetProduciblesWithHighestClassification());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        private Mock<IClassificationService> GetClassificationServiceMock(IEnumerable<Classification> classifications)
        {
            var serviceMock = new Mock<IClassificationService>();
            serviceMock.Setup(s => s.GetClassifications()).Returns(new ConcurrentList<Classification>(classifications.ToList()));
            serviceMock.SetupGet(s => s.Classifications).Returns(classifications);
            serviceMock.Setup(s => s.GetClassificationsByStatus(It.IsAny<ClassificationStatuses>())).Returns<ClassificationStatuses>((s) => classifications.Where(c => c.Status == s));
            return serviceMock;
        }

        private Mock<IServiceLocator> GetServiceLocatorMock(IClassificationService classificationService)
        {
            var serviceMock = new Mock<IServiceLocator>();
            serviceMock.Setup(s => s.Locate<IClassificationService>()).Returns(classificationService);
            return serviceMock;
        }

    }
}
