﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.WorkflowActivities;
using PackNet.Common.WorkflowActivities.Notification;

using Testing.Specificity;

using TestUtils;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class SendUiNotificationAndLogTests
    {
        private Mock<IServiceLocator> slMock;
        private Mock<IUserNotificationService> un;
        private ConsoleLogger logger;


        [TestInitialize]
        public void Setup()
        {
            un = new Mock<IUserNotificationService>();
            logger = new ConsoleLogger();
            slMock = new Mock<IServiceLocator>();
            slMock.Setup(s => s.Locate<ILogger>()).Returns(logger);
            slMock.Setup(s => s.Locate<IUserNotificationService>()).Returns(un.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendNotificationToProductionGroupForError()
        {
            var mgid = Guid.NewGuid();
            var messageLogged = false;
            var activity = new SendUiNotificationToProductionGroupAndLog();

            var input1 = new Dictionary<string, object> 
            {
                { "ServiceLocator", slMock.Object },
                { "ProductionGroupId", mgid },
                { "NotificationSeverity",NotificationSeverity.Error },
                { "Text", "one {0} two {1} three {2}" },
                { "TextArgument1", 5 },
                { "TextArgument2", "jon" },
                { "TextArgument3", 5.6 },
                //{ "TextArgument4", null },
                //{ "TextArgument5", null },
                //{ "TextArgument6", null },
                //{ "TextArgument7", null },
                //{ "TextArgument8", null },
                //{ "TextArgument9", null }
            };

            logger.AllLogMessages.Subscribe(log =>
            {
                messageLogged = true;
                Specify.That(log.Item1).Should.BeEqualTo(LogLevel.Error);
            });
            WorkflowInvoker.Invoke(activity, input1);

            Specify.That(messageLogged).Should.BeTrue();
            un.Verify(m => m.SendNotificationToProductionGroup(NotificationSeverity.Error, It.IsAny<string>(), It.IsAny<string>(), mgid));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendNotificationToMachineGroupForError()
        {
            var mgid = Guid.NewGuid();
            var messageLogged = false;
            var activity = new SendUiNotificationToMachineGroupAndLog();

            var input1 = new Dictionary<string, object> 
            {
                { "ServiceLocator", slMock.Object },
                { "MachineGroupId", mgid },
                { "NotificationSeverity",NotificationSeverity.Error },
                { "Text", "one {0} two {1} three {2}" },
                { "TextArgument1", 5 },
                { "TextArgument2", "jon" },
                { "TextArgument3", 5.6 },
                //{ "TextArgument4", null },
                //{ "TextArgument5", null },
                //{ "TextArgument6", null },
                //{ "TextArgument7", null },
                //{ "TextArgument8", null },
                //{ "TextArgument9", null }
            };

            logger.AllLogMessages.Subscribe(log =>
            {
                messageLogged = true;
                Specify.That(log.Item1).Should.BeEqualTo(LogLevel.Error);
            });
            WorkflowInvoker.Invoke(activity, input1);

            Specify.That(messageLogged).Should.BeTrue();
            un.Verify(m => m.SendNotificationToMachineGroup(NotificationSeverity.Error, It.IsAny<string>(), It.IsAny<string>(), mgid));
        }

       

    }
}
