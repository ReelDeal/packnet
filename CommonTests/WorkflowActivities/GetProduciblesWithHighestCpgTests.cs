﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class GetProduciblesWithHighestCpgTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterOUtProduciblesWithLowerCpgThanTheHighestOne()
        {
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ1", Status = CartonPropertyGroupStatuses.Stop };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ2", Status = CartonPropertyGroupStatuses.Surge };
            var cpg3 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ3", Status = CartonPropertyGroupStatuses.Normal };
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg2, cpg3 };

            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() {Value = cpg1});

            var p2 = new BoxFirstProducible();
            p2.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg2 });

            var p3 = new BoxFirstProducible();
            p3.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg3 });

            var p4 = new BoxFirstProducible();
            p4.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg3 });

            var producibles = new List<IProducible>() { p1, p2, p3, p4 };
            var cpgService = GetCpgServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cpgService).Object;
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, new GetProduciblesWithHighestCpg());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(1);
            Specify.That((filteredProducibles.First() as BoxFirstProducible).Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>().Status).Should.BeEqualTo(CartonPropertyGroupStatuses.Surge);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnNormalIfItExists_AndNoSurgeExist()
        {
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ1", Status = CartonPropertyGroupStatuses.Stop };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ3", Status = CartonPropertyGroupStatuses.Normal };
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg2 };

            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg1 });

            var p2 = new BoxFirstProducible();
            p2.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg2 });

            var p3 = new BoxFirstProducible();
            p3.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg2 });

            var producibles = new List<IProducible>() { p1, p2, p3 };
            var cpgService = GetCpgServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cpgService).Object;
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, new GetProduciblesWithHighestCpg());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
            Specify.That((filteredProducibles.First() as BoxFirstProducible).Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>().Status).Should.BeEqualTo(CartonPropertyGroupStatuses.Normal);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnEmptyListIfOnlyStoppedCpgsExists()
        {
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ1", Status = CartonPropertyGroupStatuses.Stop };
            var cpgs = new List<CartonPropertyGroup>() { cpg1 };

            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg1 });

            var p2 = new BoxFirstProducible();
            p2.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = cpg1 });

            var producibles = new List<IProducible>() { p1, p2, };
            var cpgService = GetCpgServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cpgService).Object;
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceLocator, producibles, new GetProduciblesWithHighestCpg());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        private Mock<ICartonPropertyGroupService> GetCpgServiceMock(IEnumerable<CartonPropertyGroup> cpgs)
        {
            var serviceMock = new Mock<ICartonPropertyGroupService>();
            serviceMock.Setup(s => s.Groups).Returns(cpgs.ToList());
            serviceMock.Setup(s => s.GetCartonPropertyGroupsByStatus(It.IsAny<CartonPropertyGroupStatuses>())).Returns<CartonPropertyGroupStatuses>((s) => cpgs.Where(c => c.Status == s));

            return serviceMock;
        }

        private Mock<IServiceLocator> GetServiceLocatorMock(ICartonPropertyGroupService cpgService)
        {
            var serviceMock = new Mock<IServiceLocator>();
            serviceMock.Setup(s => s.Locate<ICartonPropertyGroupService>()).Returns(cpgService);
            return serviceMock;
        }
    }
}
