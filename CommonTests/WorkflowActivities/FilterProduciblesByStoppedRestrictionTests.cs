﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    using PackNet.Common.Interfaces.DTO.ProductionGroups;

    [TestClass]
    public class FilterProduciblesByStoppedRestrictionTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterByCartonPropertyGroup()
        {
            var stoppedCpgs = new List<CartonPropertyGroup>()
            {
                new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cpg1"}
            };
            var pgId = new Guid("10000000-1000-0000-0000-000000000000");
            var mgId = new Guid("10000000-0000-0000-0000-000000000000");

            var serviceMock = GetServiceLocatorMock(stoppedCpgs);
            serviceMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(GetProductionGroupServiceMock(mgId, pgId).Object);
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(serviceMock.Object, GetBoxFirstProducibles(), mgId, new FilterUnstagedAndStoppedProducibles());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterByCartonPropertyGroup_AndSendMessageToUI_IfThereIsNoResult()
        {
            var stoppedCpgs = new List<CartonPropertyGroup>() { new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000001"), Alias = "cpg1"}, new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cpg2"} };
            var mgId = new Guid("10000000-0000-0000-0000-000000000000");
            var pgId = new Guid("10000000-1000-0000-0000-000000000000");
            var locatorMock = GetServiceLocatorMock(stoppedCpgs);
            locatorMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(GetProductionGroupServiceMock(mgId, pgId).Object);
            var uns = new Mock<IUserNotificationService>();
            uns.Setup(u => u.SendNotificationToProductionGroup(It.IsAny<NotificationSeverity>(), It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == pgId)));

            locatorMock.Setup(l => l.Locate<IUserNotificationService>()).Returns(uns.Object);
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(locatorMock.Object, GetBoxFirstProducibles(), mgId, new FilterUnstagedAndStoppedProducibles());

            uns.Verify(u => u.SendNotificationToProductionGroup(NotificationSeverity.Warning, It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == pgId)), Times.Once);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterByClassification()
        {
            var stoppedClassifications = new List<Classification>() { new Classification() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cls1"} };
            var mgId = new Guid("10000000-0000-0000-0000-000000000000");
            var pgId = new Guid("10000000-1000-0000-0000-000000000000");
            var locatorMock = GetServiceLocatorMock(null, stoppedClassifications);
            locatorMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(GetProductionGroupServiceMock(mgId, pgId).Object);
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(locatorMock.Object, GetBoxFirstProducibles(), mgId, new FilterUnstagedAndStoppedProducibles());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterByClassification_AndSendMessageToUI_IfThereIsNoResult()
        {
            var stoppedClassifications = new List<Classification>() { new Classification() { Id = new Guid("00000000-0000-0000-0000-000000000001"), Alias = "cls1"}, new Classification() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cls2"} };
            var mgId = new Guid("10000000-0000-0000-0000-000000000000");
            var pgId = new Guid("10000000-1000-0000-0000-000000000000");
            var locatorMock = GetServiceLocatorMock(null, stoppedClassifications, null);
            locatorMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(GetProductionGroupServiceMock(mgId, pgId).Object);
            var uns = new Mock<IUserNotificationService>();
            uns.Setup(u => u.SendNotificationToProductionGroup(It.IsAny<NotificationSeverity>(), It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == pgId)));

            locatorMock.Setup(l => l.Locate<IUserNotificationService>()).Returns(uns.Object);
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(locatorMock.Object, GetBoxFirstProducibles(), mgId, new FilterUnstagedAndStoppedProducibles());

            uns.Verify(u => u.SendNotificationToProductionGroup(NotificationSeverity.Warning, It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == pgId)), Times.Once);
            
            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterByPickZone()
        {
            var stoppedPickZones = new List<PickZone>() { new PickZone() { Id = new Guid("00000000-0000-0000-0000-000000000002") } };
            var mgId = new Guid("10000000-0000-0000-0000-000000000000");
            var pgId = new Guid("10000000-1000-0000-0000-000000000000");
            var locatorMock = GetServiceLocatorMock(null, null, stoppedPickZones);
            locatorMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(GetProductionGroupServiceMock(mgId, pgId).Object);
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(locatorMock.Object, GetBoxFirstProducibles(), mgId, new FilterUnstagedAndStoppedProducibles());

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(2);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterByPickZone_AndSendMessageToUI_IfThereIsNoResult()
        {
            var stoppedPickZones = new List<PickZone>() { new PickZone() { Id = new Guid("00000000-0000-0000-0000-000000000001") }, new PickZone() { Id = new Guid("00000000-0000-0000-0000-000000000002") } };
            var mgId = new Guid("10000000-0000-0000-0000-000000000000");
            var pgId = new Guid("10000000-1000-0000-0000-000000000000");
            var locatorMock = GetServiceLocatorMock(null, null, stoppedPickZones);
            locatorMock.Setup(s => s.Locate<IProductionGroupService>()).Returns(GetProductionGroupServiceMock(mgId, pgId).Object);
            var uns = new Mock<IUserNotificationService>();
            uns.Setup(u => u.SendNotificationToProductionGroup(It.IsAny<NotificationSeverity>(), It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == pgId)));

            locatorMock.Setup(l => l.Locate<IUserNotificationService>()).Returns(uns.Object);
            var filteredProducibles = WorkflowActivitiesTestHelper.ExecuteWorkflowActivity(locatorMock.Object, GetBoxFirstProducibles(), mgId, new FilterUnstagedAndStoppedProducibles());
            uns.Verify(u => u.SendNotificationToProductionGroup(NotificationSeverity.Warning, It.IsAny<string>(), It.IsAny<string>(), It.Is<Guid>(guid => guid == pgId)), Times.Once);
            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(0);
        }

        private Mock<IServiceLocator> GetServiceLocatorMock(IEnumerable<CartonPropertyGroup> stoppedCpgs = null, IEnumerable<Classification> stoppedClassifications = null, IEnumerable<PickZone> stoppedPickZones = null)
        {
            var serviceMock = new Mock<IServiceLocator>();
            serviceMock.Setup(s => s.Locate<ICartonPropertyGroupService>()).Returns(GetCpgServiceMock(stoppedCpgs).Object);
            serviceMock.Setup(s => s.Locate<IClassificationService>()).Returns(GetClassificationServiceMock(stoppedClassifications).Object);
            serviceMock.Setup(s => s.Locate<IPickZoneService>()).Returns(GetPickZoneServiceMock(stoppedPickZones).Object);
              return serviceMock;
        }

        private Mock<IProductionGroupService> GetProductionGroupServiceMock(Guid mgId, Guid pgId)
        {
            var serviceMock = new Mock<IProductionGroupService>();
            serviceMock.Setup(s => s.GetProductionGroupForMachineGroup(mgId)).Returns(new ProductionGroup{Id = pgId});
            return serviceMock;
        }

        private Mock<ICartonPropertyGroupService> GetCpgServiceMock(IEnumerable<CartonPropertyGroup> stopped = null)
        {
            var serviceMock = new Mock<ICartonPropertyGroupService>();
            serviceMock.Setup(s => s.GetCartonPropertyGroupsByStatus(CartonPropertyGroupStatuses.Stop))
                .Returns(stopped ?? new List<CartonPropertyGroup>());
            return serviceMock;
        }

        private Mock<IClassificationService> GetClassificationServiceMock(IEnumerable<Classification> stopped = null)
        {
            var serviceMock = new Mock<IClassificationService>();
            serviceMock.Setup(s => s.GetClassificationsByStatus(ClassificationStatuses.Stop)).Returns(stopped ?? new List<Classification>());
            return serviceMock;
        }

        private Mock<IPickZoneService> GetPickZoneServiceMock(IEnumerable<PickZone> stopped = null)
        {
            var serviceMock = new Mock<IPickZoneService>();
            serviceMock.Setup(s => s.GetPickZonesByStatus(PickZoneStatuses.Stop)).Returns(stopped ?? new List<PickZone>());
            return serviceMock;
        }

        private IEnumerable<BoxFirstProducible> GetBoxFirstProducibles()
        {
            var cpg1 = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000001"), Alias = "cpg1" };
            var cpg2 = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cpg2" };
            var cls1 = new Classification() { Id = new Guid("00000000-0000-0000-0000-000000000001"), Alias = "cls1"};
            var cls2 = new Classification() { Id = new Guid("00000000-0000-0000-0000-000000000002"), Alias = "cls2"};
            var pz1 = new PickZone() { Id = new Guid("00000000-0000-0000-0000-000000000001") };
            var pz2 = new PickZone() { Id = new Guid("00000000-0000-0000-0000-000000000002") };

            var p1 = new BoxFirstProducible();
            p1.Id = new Guid("10000000-0000-0000-0000-000000000001");
            p1.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p1.Producible = new Carton();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg1));
            p1.Restrictions.Add(new BasicRestriction<Classification>(cls1));
            p1.Restrictions.Add(new BasicRestriction<PickZone>(pz1));

            var p2 = new BoxFirstProducible();
            p2.Id = new Guid("20000000-0000-0000-0000-000000000001");
            p2.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p2.Producible = new Carton();
            p2.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg1));
            p2.Restrictions.Add(new BasicRestriction<Classification>(cls1));
            p2.Restrictions.Add(new BasicRestriction<PickZone>(pz1));

            var p3 = new BoxFirstProducible();
            p3.Id = new Guid("30000000-0000-0000-0000-000000000002");
            p3.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p3.Producible = new Carton();
            p3.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg2));
            p3.Restrictions.Add(new BasicRestriction<Classification>(cls2));
            p3.Restrictions.Add(new BasicRestriction<PickZone>(pz2));

            var p4 = new BoxFirstProducible();
            p4.Id = new Guid("40000000-0000-0000-0000-000000000002");
            p4.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            p4.Producible = new Label();
            p4.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg2));
            p4.Restrictions.Add(new BasicRestriction<Classification>(cls2));
            p4.Restrictions.Add(new BasicRestriction<PickZone>(pz2));

            var p5 = new BoxFirstProducible();
            p5.Id = new Guid("50000000-0000-0000-0000-000000000002");
            p5.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            p5.Producible = new Label();
            p5.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg2));
            p5.Restrictions.Add(new BasicRestriction<Classification>(cls2));
            p5.Restrictions.Add(new BasicRestriction<PickZone>(pz2));

            return new List<BoxFirstProducible>() { p1, p2, p3, p4, p5 };
        }
    }
}
