﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;

namespace CommonTests.WorkflowActivities
{
    [TestClass]
    public class FilterByCartonPropertyGroupsTests
    {
        private CartonPropertyGroup validCpg = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000001") };

#if DEBUG
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldFilterOutProduciblesBeloningToStoppedCartonPropertyGroups()
        {
            var producibles = GetBoxFirstProducibles();
            var cpgService = GetCpgServiceMock();
            //var filter = new FilterByCartonPropertyGroups();
            //filter.CartonPropertyGroupService = cpgService.Object;
            //filter.Producibles = producibles;
            
            //var arguments = new Dictionary<string, object>() {{"CartonPropertyGroupService", cpgService.Object},{"Producibles", producibles}};
            //var wfApp = new WorkflowApplication(new FilterByCartonPropertyGroups(), arguments);
            //wfApp.Run();

        }
#endif
        private Mock<ICartonPropertyGroupService> GetCpgServiceMock()
        {
            var serviceMock = new Mock<ICartonPropertyGroupService>();
            serviceMock.Setup(s => s.GetUnstoppedCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { validCpg });
            return serviceMock;
        }

        private IEnumerable<BoxFirstProducible> GetBoxFirstProducibles()
        {
            var p1 = new BoxFirstProducible { Id = new Guid("10000000-0000-0000-0000-000000000001") };
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = validCpg });

            var p2 = new BoxFirstProducible { Id = new Guid("20000000-0000-0000-0000-000000000001") };
            p2.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>() { Value = validCpg });

            var p3 = new BoxFirstProducible { Id = new Guid("30000000-0000-0000-0000-000000000002") };
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>()
            {
                Value = new CartonPropertyGroup() { Id = new Guid("00000000-0000-0000-0000-000000000002") }
            });

            return new List<BoxFirstProducible>() { p1, p2, p3 };
        }
    }
}
