﻿namespace CommonTests.WorkflowActivities
{
    using System.IO;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;

    using PackNet.Business.Corrugates;
    using PackNet.Business.Machines;
    using PackNet.Business.PackagingDesigns;
    using PackNet.Business.ProductionGroups;
    using PackNet.Common.Caching;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Eventing;
    using PackNet.Common.Interfaces.Machines;
    using PackNet.Common.Interfaces.Services;
    using PackNet.Common.Interfaces.Services.Machines;
    using PackNet.Common.Interfaces.Utils;
    using PackNet.Common.WorkflowActivities;
    using System;
    using System.Collections.Generic;
    using PackNet.Common.Eventing;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Data.Corrugates;
    using PackNet.Data.ProductionGroups;
    using PackNet.Services;
    using PackNet.Services.MachineServices;
    using PackNet.Services.ProductionGroup;

    using Testing.Specificity;

    using TestUtils;

    [TestClass]
    public class SetOptimalCorrugateRestrictionBasedOnProductionGroupTests
    {
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldGetAnOptimalCorrugate_WithATileCountGreaterThanZero_ForAKitWithAProducibleCarton()
        {
            var kit = new Kit();
            var carton = new Carton() { Length = 5, Width = 5, Height = 5, DesignId = 2010001, ProductionGroupAlias = "PG1" };
            kit.AddProducible(carton);
            var corrugate = new Corrugate()
            {
                Alias = "MyCorrugate",
                Quality = 1,
                Thickness = 0.16,
                Width = 18.5d
            };
            corrugate.Id = Guid.NewGuid();

            #region setup
            var logger = new ConsoleLogger();
            var eventAggregator = new EventAggregator();

            var fusion = new FusionMachine();
            fusion.Alias = "Fusion1";
            fusion.Tracks = new ConcurrentList<Track>()
                            {
                                new Track { TrackNumber = 1, TrackOffset = corrugate.Width },
                                new Track { TrackNumber = 2, TrackOffset = corrugate.Width }
                            };
            fusion.LoadCorrugate(fusion.Tracks.First(), corrugate);

            var mg = new MachineGroup(){ConfiguredMachines = new ConcurrentList<Guid>(){fusion.Id}};
            var pg = new ProductionGroup() { Alias = carton.ProductionGroupAlias, ConfiguredCorrugates = new ConcurrentList<Guid>() { corrugate.Id }, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast, ConfiguredMachineGroups = new ConcurrentList<Guid>() { mg.Id } };
            
            #region mocks
            var serviceLocator = new Mock<IServiceLocator>();
            serviceLocator.Setup(s => s.Locate<ILogger>()).Returns(logger);

            var designManMock = new Mock<IPackagingDesignManager>();
            designManMock.Setup(s => s.HasDesignWithId(2010001)).Returns(true);
            designManMock.Setup(s => s.CalculateUsage(carton, corrugate, It.IsAny<int>(), false)).Returns(new PackagingDesignCalculation() { Length = 20d, Width = 10.374d });
            designManMock.Setup(s => s.CalculateUsage(carton, corrugate, It.IsAny<int>(), true)).Returns(new PackagingDesignCalculation() { Width = 20d, Length = 10.374d });


            var uiComMock = new Mock<IUICommunicationService>();
            var userNotMock = new Mock<IUserNotificationService>();

            var corrugatesRepo = new Mock<ICorrugateRepository>();
            corrugatesRepo.Setup(s => s.All()).Returns(new List<Corrugate> { corrugate });
            corrugatesRepo.Setup(s => s.FindByCorrugateAlias(corrugate.Alias)).Returns(corrugate);

            var pgRepoMock = new Mock<IProductionGroupRepository>();       
            pgRepoMock.Setup(s => s.All()).Returns(new List<ProductionGroup> { pg });
            
            var pgsMock = new Mock<IProductionGroup>();
            pgsMock.Setup(s => s.Find(pg.Id)).Returns(pg);
            pgsMock.Setup(s => s.FindByAlias(pg.Alias)).Returns(pg);
            pgsMock.Setup(s => s.GetProductionGroups()).Returns(new List<ProductionGroup> { pg });
            pgsMock.Setup(s => s.GetProductionGroupForMachineGroup(mg.Id)).Returns(pg);
            pgsMock.Setup(s => s.GetProductionGroups()).Returns(new List<ProductionGroup> { pg });
            pgsMock.Setup(s => s.GetProductionGroupsForCorrugate(corrugate.Id)).Returns(new ConcurrentList<ProductionGroup>() { pg });

            var aggregatedMachineService = new Mock<IAggregateMachineService>();
            aggregatedMachineService.Setup(s => s.CanProduce(fusion.Id, carton)).Returns(true);
            aggregatedMachineService.Setup(s => s.Machines).Returns(new List<IMachine> { fusion });

            var machineServiceMock = new Mock<IMachineGroupService>();
            machineServiceMock.Setup(s => s.MachineGroups).Returns(new List<MachineGroup>{mg});
            #endregion

            var corrugatesManager = new Corrugates(corrugatesRepo.Object);
            var optimalCorrugateCalculator = new OptimalCorrugateCalculator(designManMock.Object, serviceLocator.Object, machineServiceMock.Object);
            var corrugateService = new CorrugateService(optimalCorrugateCalculator, corrugatesManager, serviceLocator.Object, aggregatedMachineService.Object, uiComMock.Object, eventAggregator, eventAggregator, logger);
            var pgService = new ProductionGroupService(pgsMock.Object, serviceLocator.Object, uiComMock.Object, eventAggregator, eventAggregator, null, logger);

            #region serviceLocator setup
            serviceLocator.Setup(s => s.Locate<ILogger>()).Returns(logger);
            serviceLocator.Setup(s => s.Locate<ICorrugateService>()).Returns(corrugateService);
            serviceLocator.Setup(s => s.Locate<IProductionGroupService>()).Returns(pgService);
            serviceLocator.Setup(s => s.Locate<IMachineGroupService>()).Returns(machineServiceMock.Object);
            serviceLocator.Setup(s => s.Locate<IAggregateMachineService>()).Returns(aggregatedMachineService.Object);
            serviceLocator.Setup(s => s.Locate<IUICommunicationService>()).Returns(uiComMock.Object);
            serviceLocator.Setup(s => s.Locate<IUserNotificationService>()).Returns(userNotMock.Object);
            serviceLocator.Setup(s => s.Locate<IProductionGroupService>()).Returns(pgService);
            serviceLocator.Setup(s => s.Locate<IMachineGroupService>()).Returns(machineServiceMock.Object);
            #endregion

            var arguments = new Dictionary<string, object>
                            {
                                { "Producible", kit },
                                { "ProductionGroupAlias", carton.ProductionGroupAlias },
                                { "ServiceLocator", serviceLocator.Object },
                            };
            #endregion

            WorkflowActivitiesTestHelper.ExecuteWithoutReturnValue(arguments, new SetOptimalCorrugateRestrictionBasedOnProductionGroup(), TimeSpan.FromSeconds(500));
            Specify.That(carton.CartonOnCorrugate).Should.Not.BeNull("A carton on corrugate should have been assigned by the workflow activity");
            Specify.That(carton.CartonOnCorrugate.TileCount).Should.BeLogicallyEqualTo(1, "One box of this size fits on this corrugate.");
        }
    }
}
