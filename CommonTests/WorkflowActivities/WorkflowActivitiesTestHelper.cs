﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Common.WorkflowActivities;

using Testing.Specificity;

namespace CommonTests.WorkflowActivities
{
    public static class WorkflowActivitiesTestHelper
    {
        public static IEnumerable<IProducible> ExecuteWorkflowActivity(IServiceLocator locator, IEnumerable<IProducible> producibles, Activity workflowactivity)
        {
            var arguments = new Dictionary<string, object>
                            {
                                { "Producibles", producibles},
                                { "ServiceLocator", locator},
                            };
            return Execute(arguments, workflowactivity);
        }

        public static IEnumerable<IProducible> ExecuteWorkflowActivity(IServiceLocator locator, IEnumerable<IProducible> producibles, Guid machineGroupId, Activity workflowactivity)
        {
            var arguments = new Dictionary<string, object>
                            {
                                { "Producibles", producibles},
                                { "ServiceLocator", locator},
                                { "MachineGroupId", machineGroupId},
                            };
            return Execute(arguments, workflowactivity);
        }

        public static IEnumerable<IProducible> ExecuteWorkflowActivity(IServiceLocator locator, Guid machineGroupId, Activity workflowactivity)
        {
            var arguments = new Dictionary<string, object>
                            {
                                { "MachineGroupId", machineGroupId},
                                { "ServiceLocator", locator},
                            };
            return Execute(arguments, workflowactivity);
        }

        public static IEnumerable<IProducible> ExecuteWorkflowActivity(IEnumerable<IProducible> producibles, Activity workflowactivity)
        {
            var arguments = new Dictionary<string, object>
                            {
                                { "Producibles", producibles},
                            };
            return Execute(arguments, workflowactivity);
        }

        private static IEnumerable<IProducible> Execute(Dictionary<string, object> arguments, Activity workflowactivity)
        {
            IEnumerable<IProducible> output = null;
            var completed = false;
            var wfApp = new WorkflowApplication(workflowactivity, arguments)
            {
                Completed = e =>
                {
                    completed = true;
                    output = e.Outputs["Result"] as IEnumerable<IProducible>;
                }
            };

            wfApp.Run();
            Retry.For(() => completed, TimeSpan.FromSeconds(2));
            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");

            return output;
        }

        public static void ExecuteWithoutReturnValue(Dictionary<string, object> arguments, Activity workflowactivity, TimeSpan timeout)
        {
            var completed = false;
            var wfApp = new WorkflowApplication(workflowactivity, arguments)
            {
                Completed = e =>
                {
                    completed = true;
                }
            };

            wfApp.Run();
            Retry.For(() => completed, timeout);
            Specify.That(completed).Should.BeTrue("The workflow didn't finish within the set time span.");
        }
    }
}
