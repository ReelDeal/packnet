﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using Testing.Specificity;

namespace CommonTests.ExtensionMethods
{
    [TestClass]
    public class CartonExtensionTests 
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPopulateDesignFormulasWithCartonAndCorrugateProperties()
        {
            var carton = new Carton { Length = 1.1, Width = 2.2, Height = 3.3 };
            carton.XValues.Add("X1", 4.4);
            carton.XValues.Add("test", 5.5);

            var corrugate = new Corrugate { Width = 6.6, Thickness = 7.7 };

            var dfp = carton.GetDesignFormulaParameters(corrugate);
            Specify.That(dfp.CartonHeight).Should.BeLogicallyEqualTo(carton.Height);
            Specify.That(dfp.CartonLength).Should.BeLogicallyEqualTo(carton.Length);
            Specify.That(dfp.CartonWidth).Should.BeLogicallyEqualTo(carton.Width);
            Specify.That(dfp.ZfoldThickness).Should.BeLogicallyEqualTo(corrugate.Thickness);
            Specify.That(dfp.ZfoldWidth).Should.BeLogicallyEqualTo(corrugate.Width);

            Specify.That(dfp.XValues.Count).Should.BeLogicallyEqualTo(carton.XValues.Count);
            Specify.That(dfp.XValues).Should.Not.BeSameAs(carton.XValues);
            foreach (var key in dfp.XValues.Keys)
            {
                Specify.That(dfp.XValues[key]).Should.BeLogicallyEqualTo(carton.XValues[key]);
            }
        }
    }
}
