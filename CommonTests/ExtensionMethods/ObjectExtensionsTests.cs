﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Specificity;

namespace CommonTests.ExtensionMethods
{
    using PackNet.Common.ExtensionMethods;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.Enums;
    

    [TestClass]
    public class ObjectExtensionsTests
    {
        [TestMethod]
        public void ConvertCartonRequestToDictionary()
        {
            var cartonRequest = new Carton
            {
                Length = 10.125,
                Width = 9,
                Height = 8,
                CorrugateQuality = 1,
                DesignId = 2010001
            };
            var cartonDictionary = cartonRequest.AsDictionary();

            Specify.That(cartonDictionary["Length"]).Should.BeEqualTo("10.125");
            Specify.That(cartonDictionary["Width"]).Should.BeEqualTo("9");
            Specify.That(cartonDictionary["Height"]).Should.BeEqualTo("8");
            Specify.That(cartonDictionary["CorrugateQuality"]).Should.BeEqualTo("1");
            Specify.That(cartonDictionary["DesignId"]).Should.BeEqualTo("2010001");
            Specify.That(cartonDictionary["Quantity"]).Should.BeEqualTo("1");
        }
    }
}
