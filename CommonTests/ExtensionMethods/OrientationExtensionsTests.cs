﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.ExtensionMethods;

using Testing.Specificity;

namespace CommonTests.ExtensionMethods
{
    [TestClass]
    public class OrientationExtensionsTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnRotatedTrueWhenOrientationIs90or270Degrees()
        {
            Specify.That(OrientationEnum.Degree90.IsRotated()).Should.BeTrue();
            Specify.That(OrientationEnum.Degree90_flip.IsRotated()).Should.BeTrue();
            Specify.That(OrientationEnum.Degree270.IsRotated()).Should.BeTrue();
            Specify.That(OrientationEnum.Degree270_flip.IsRotated()).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnRotatedFalseWhenOrientationIs0or180Degrees()
        {
            Specify.That(OrientationEnum.Degree0.IsRotated()).Should.BeFalse();
            Specify.That(OrientationEnum.Degree0_flip.IsRotated()).Should.BeFalse();
            Specify.That(OrientationEnum.Degree180.IsRotated()).Should.BeFalse();
            Specify.That(OrientationEnum.Degree180_flip.IsRotated()).Should.BeFalse();
        }
    }
}
