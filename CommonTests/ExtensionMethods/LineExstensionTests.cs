﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using Testing.Specificity;

namespace CommonTests.ExtensionMethods
{
    [TestClass]
    public class LineExstensionTests 
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnListOfParametersUsedInLineFormulas()
        {
            var line = new Line { StartX = "X2+X3+FW", StartY = "0", Length = "L+FT" };

            var parametersUsed = line.GetUsedParameters();

            Specify.That(parametersUsed).Should.Not.BeNull();
            Specify.That(parametersUsed.Count()).Should.BeEqualTo(5);
            Specify.That(parametersUsed.Count(p => p.Equals("X2"))).Should.BeEqualTo(1);
            Specify.That(parametersUsed.Count(p => p.Equals("X3"))).Should.BeEqualTo(1);
            Specify.That(parametersUsed.Count(p => p.Equals("FW"))).Should.BeEqualTo(1);
            Specify.That(parametersUsed.Count(p => p.Equals("FT"))).Should.BeEqualTo(1);                        
            Specify.That(parametersUsed).Should.Not.Contain("0");
        }
    }
}
