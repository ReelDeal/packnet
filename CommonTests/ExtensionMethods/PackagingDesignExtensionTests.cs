﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

using Testing.Specificity;

namespace CommonTests.ExtensionMethods
{
    [TestClass]
    public class PackagingDesignExtensionTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetDistictParametersUsedInLegacyDesign_AndAssignAliasToParameterName()
        {
            var design = GivenADesignWithSomeVerticalLines(new List<string> { "X1", "X2" });
            
            var parameters = design.GetUsedParameters();

            Specify.That(parameters).Should.Not.BeNull();
            Specify.That(parameters.Count()).Should.BeEqualTo(2);
            Specify.That(parameters.Any(p => p.Name.Equals("X1"))).Should.BeTrue();
            Specify.That(parameters.Any(p => p.Alias.Equals("X1"))).Should.BeTrue();
            Specify.That(parameters.Any(p => p.Name.Equals("X2"))).Should.BeTrue();
            Specify.That(parameters.Any(p => p.Alias.Equals("X2"))).Should.BeTrue();
            Specify.That(parameters).Should.Not.Contain("L");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetValidUserParametersUsedInLegacyDesign()
        {
            var design = GivenADesignWithSomeVerticalLines(new List<string> { "X1+XRoboCop+FW+FT-0.5+(X2+X1)*L" });

            var parameters = design.GetUsedParameters();

            Specify.That(parameters.Count()).Should.BeLogicallyEqualTo(2);
            Specify.That(parameters.Count(p => p.Name.Equals("X1"))).Should.BeEqualTo(1);
            Specify.That(parameters.Count(p => p.Name.Equals("X2"))).Should.BeEqualTo(1);

            Specify.That(parameters.Any(p => p.Name.Equals("0.5"))).Should.BeFalse();
            Specify.That(parameters.Any(p => p.Name.Equals("L"))).Should.BeFalse();
            Specify.That(parameters.Any(p => p.Name.Equals("FW"))).Should.BeFalse();
            Specify.That(parameters.Any(p => p.Name.Equals("FT"))).Should.BeFalse();
            Specify.That(parameters.Any(p => p.Name.Equals("XRoboCop"))).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetValidUserParametersUsedInLegacyDesign_WithinMultipleOfXValues()
        {
            var design = GivenADesignWithSomeVerticalLines(new List<string> { "3.5X1", "4X2", "5X3+4X4", "0.5L-4X5" });

            var parameters = design.GetUsedParameters();

            Specify.That(parameters.Count()).Should.BeLogicallyEqualTo(5, "Should be able to find 5 X values used in the lines formulas.");
            Specify.That(parameters.Count(p => p.Name.Equals("X1"))).Should.BeEqualTo(1, "Should be able to find and extract X1 in 3.5X1.");
            Specify.That(parameters.Count(p => p.Name.Equals("X2"))).Should.BeEqualTo(1, "Should be able to find and extract X2 in 4X2.");
            Specify.That(parameters.Count(p => p.Name.Equals("X3"))).Should.BeEqualTo(1, "Should be able to find and extract X3 in 5X3+4X4.");
            Specify.That(parameters.Count(p => p.Name.Equals("X4"))).Should.BeEqualTo(1, "Should be able to find and extract X4 in 5X3+4X4.");
            Specify.That(parameters.Count(p => p.Name.Equals("X5"))).Should.BeEqualTo(1, "Should be able to find and extract X5 in 0.5L-4X5.");

            Specify.That(parameters.Any(p => p.Name.Equals("L"))).Should.BeFalse("Should exclude L from used parameters in the design.");
        }

        private PackagingDesign GivenADesignWithSomeVerticalLines(List<string> xLines)
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW",
                LengthOnZFold = "L"
            };

            xLines.ForEach(val => packagingDesign.Lines.Add(new Line
            {
                StartX = val,
                StartY = "0",
                Length = "L",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            }));

            return packagingDesign;
        }
    }
}
