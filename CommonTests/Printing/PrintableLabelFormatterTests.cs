﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Printing;
using Testing.Specificity;


namespace CommonTests.Printing
{
    using Assert = Assert;

    [TestClass]
    public class PrintableLabelFormatterTests
    {
        private PrintableLabelFormatter formatter;
        private static string templateLabelPath;

        [TestInitialize]
        public void TestInit()
        {
            templateLabelPath = Path.Combine(Environment.CurrentDirectory, "Data\\Labels\\Template.prn");

            DeleteTemplateLabel();
            CreateTemplateLabel();
            
            formatter = new PrintableLabelFormatter();
        }

        [TestCleanup]
        public void Cleanup()
        {
            DeleteTemplateLabel();
        }

        private void DeleteTemplateLabel()
        {
            var dirInfo = new DirectoryInfo(Path.GetDirectoryName(templateLabelPath));
            if (dirInfo.Exists) dirInfo.Delete(true);
        }

        private static void CreateTemplateLabel()
        {
            const string barcodeLabelFormat = "^XA^FO40,50^BY3^A0N,100,80^BCN,200,Y,N,N^FD{SerialNumber}^FS^XZ";
            var fileInfo = new FileInfo(templateLabelPath);
            if (fileInfo.Directory != null && !fileInfo.Directory.Exists)
                fileInfo.Directory.Create();
            using (var streamWriter = new StreamWriter(fileInfo.Open(FileMode.OpenOrCreate, FileAccess.Write)))
            {
               streamWriter.Write(barcodeLabelFormat); 
           }
        }

        [TestMethod]
        public void WhenGivenNullPrintable_ExceptionThrown()
        {
            ArgumentException exception = null;

            try
            {
                formatter.ConvertToPrintableFormat(null);
            }
            catch (ArgumentException e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
            Specify.That(exception.Message.ToLower().Contains("null printable given")).Should.BeTrue();
        }

        [TestMethod]
        public void WhenGivenPrintableWithNullData_ExceptionThrown()
        {
            var printable = new Printable(null);

            ArgumentException exception = null;

            try
            {
                formatter.ConvertToPrintableFormat(printable);
            }
            catch (ArgumentException e)
            {
                exception = e;
            }

            Assert.IsNotNull(exception);
            Specify.That(exception.Message.ToLower().Contains("printable with null data given")).Should.BeTrue();
        }

        [TestMethod]
        public void WhenGivenPrintableWithEmptyStringItShouldPrintFineWithoutIncludingMissingField()
        {
            // Arrange
            var printable = new Printable(new Dictionary<string, string> {{"SerialNumber", string.Empty}});

            // Act
            var printData = formatter.ConvertToPrintableFormat(printable);

            // Assert 
            Specify.That(printData.Contains("^FD^FS")).Should.BeTrue("Should be nothing between ^FD^FS so blank label is printed");
        }

        [TestMethod]
        public void WhenGivenValidInput_NoExceptionThrown()
        {
            const string textToPrint = "0123456789";
            var printable = new Printable(new Dictionary<string, string> {{"SerialNumber", textToPrint}});

            ArgumentException exception = null;

            try
            {
                formatter.ConvertToPrintableFormat(printable);
            }
            catch (ArgumentException e)
            {
                exception = e;
            }

            Assert.IsNull(exception);
        }
    }
}
