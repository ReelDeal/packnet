﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Enums;

using Testing.Specificity;

namespace CommonTests.Enums
{
    [TestClass]
    public class EnumsTests
    {
        [TestMethod]
        public void WhenUsingDifferentClasses_AndCompareWithDotEquals_GetWorkFor_ShouldNotBeEqual()
        {
            var productionGroupMessageGetWorkFor = ProductionGroupMessages.GetWorkForProductionGroup;
            var machineGroupMessageGetWorkFor = MachineGroupMessages.GetWorkForMachineGroup;
            Specify.That(productionGroupMessageGetWorkFor.Equals(machineGroupMessageGetWorkFor))
                .Should.BeFalse("Should not be equal because comparison of the types will result in not equal");
        }
    }
}
