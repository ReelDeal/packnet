﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Enums;

using Testing.Specificity;

namespace CommonTests.Enums
{
    [TestClass]
    public class MessageTypesTests
    {
        [TestMethod]
        public void WhenUsingDifferentClasses_GetWorkFor_ShouldNotBeEqual()
        {
            var productionGroupMessageGetWorkFor = ProductionGroupMessages.GetWorkForProductionGroup;
            var machineGroupMessageGetWorkFor = MachineGroupMessages.GetWorkForMachineGroup;
            Specify.That(productionGroupMessageGetWorkFor == machineGroupMessageGetWorkFor).Should.BeFalse();
        }
    }
}
