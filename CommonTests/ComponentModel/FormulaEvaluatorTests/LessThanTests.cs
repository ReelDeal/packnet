﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;

    using Testing.Specificity;

    [TestClass]
    public class LessThanTests
    {
        [TestMethod]
        public void LeftOpLessThenRightOpReturnsTrue()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 < 2");
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void LeftOpNotLessThenRightOpReturnsFalse()
        {
            bool solution = FormulaEvaluator.Solve<bool>("2 < 1");
            Specify.That(solution).Should.BeFalse();
        }

        [TestMethod]
        public void LeftOpEqualToRightOpReturnsFalse()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 < 1");
            Specify.That(solution).Should.BeFalse();
        }

        [TestMethod]
        public void LeftStringOpLessThenRightStringOpReturnsTrue()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "AZ2");

            bool solution = FormulaEvaluator.Solve<bool>("PZ < \"AZ3\"", parameters);
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void LeftStringOpNotLessThenRightStringOpReturnsFalse()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "AZ3");

            bool solution = FormulaEvaluator.Solve<bool>("PZ < \"AZ2\"", parameters);
            Specify.That(solution).Should.BeFalse();
        }

        [TestMethod]
        public void LeftStringOpEqualToRightStringOpReturnsFalse()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "AZ1");

            bool solution = FormulaEvaluator.Solve<bool>("PZ < \"AZ1\"", parameters);
            Specify.That(solution).Should.BeFalse();
        }

    }
}
