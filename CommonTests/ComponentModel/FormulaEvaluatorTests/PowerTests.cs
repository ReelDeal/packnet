﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class PowerTests
    {
        [TestMethod]
        public void TwoToThePowerOfFourGivesSixteen()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("2^4");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(16.0));
        }
    }
}
