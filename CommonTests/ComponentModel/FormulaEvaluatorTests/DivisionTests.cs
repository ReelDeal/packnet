﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class DivisionTests
    {
        [TestMethod]
        public void DivisionTest()
        {
            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("10 / 5");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(2.0));

            solution = FormulaEvaluator.Solve<MicroMeter>("1 / 2");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0.5));

            solution = FormulaEvaluator.Solve<MicroMeter>("20 / 5");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(4.0));

            solution = FormulaEvaluator.Solve<MicroMeter>("80 / 4");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(20.0));
        }
    }
}
