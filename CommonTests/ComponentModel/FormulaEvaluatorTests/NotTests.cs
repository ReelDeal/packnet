﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;

    using Testing.Specificity;

    [TestClass]
    public class NotTests
    {
        [TestMethod]
        public void NotTrueReturnFalse()
        {
            var solution = FormulaEvaluator.Solve<bool>("!2 > 1");
            Specify.That(solution).Should.BeFalse();
        }

        [TestMethod]
        public void NotFalseReturnTrue()
        {
            var solution = FormulaEvaluator.Solve<bool>("!2 > 3");
            Specify.That(solution).Should.BeTrue();
        }
    }
}
