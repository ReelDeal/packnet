﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class AdditionTests
    {
        [TestMethod]
        public void BasicAddition()
        {
            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("1+1336");
            
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1337.0));
        }

        [TestMethod]
        public void MultiValueAddition()
        {
            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("1+2+3+4+5+1322");

            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1337.0));
        }

        [TestMethod]
        public void VariableAddition()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("FT", 1.0);

            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("FT+1336", parameters);

            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1337.0));
        }
    }
}
