﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;

    using Testing.Specificity;

    [TestClass]
    public class NotEqualTests
    {
        [TestMethod]
        public void NotEqualTrueReturnTrue()
        {
            var solution = FormulaEvaluator.Solve<bool>("2 != 1");
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void NotEqualFalseReturnFalse()
        {
            var solution = FormulaEvaluator.Solve<bool>("1 != 1");
            Specify.That(solution).Should.BeFalse();
        }

        [TestMethod]
        public void NotEqualStringsTrueReturnTrue()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "AZ2");

            var solution = FormulaEvaluator.Solve<bool>("PZ != \"AZ1\"", parameters);
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void NotEqualStringsFalseReturnFalse()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "AZ1");

            var solution = FormulaEvaluator.Solve<bool>("PZ != \"AZ1\"", parameters);
            Specify.That(solution).Should.BeFalse();
        }
    }
}
