﻿using System.Linq;

namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using System;
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class FormulaEvaluatorTests
    {
        [TestMethod]
        public void ShouldThrowIfInconsistentTypesAreGiven()
        {
            Specify.ThatAction(() => FormulaEvaluator.Solve<bool>("1 > 0 && 5"))
                .Should.HaveThrown(typeof(ArgumentException));

            Specify.ThatAction(() => FormulaEvaluator.Solve<bool>("!5"))
                .Should.HaveThrown(typeof(ArgumentException));
        }

        [TestMethod]
        public void ShouldReturnZeroOnEmptyQuery()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0.0));
        }

        [TestMethod]
        public void ShouldThrowIfThereAreToFewParametersForOperator()
        {
            Specify.ThatAction(() => FormulaEvaluator.Solve<bool>("1 >"))
                .Should.HaveThrown(typeof(ArgumentException));
        }

        [TestMethod]
        public void ShouldThrowIfThereAreToFewParametersForFunction()
        {
            Specify.ThatAction(() => FormulaEvaluator.Solve<bool>("sin()"))
                .Should.HaveThrown(typeof(ArgumentException));
        }

        [TestMethod]
        public void ShouldHandleNegativeValues()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("1--1");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(2.0));
        }

        [TestMethod]
        public void ShouldHandleWhiteSpaces()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("1- -1");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(2.0));
        }

        [TestMethod]
        public void ShouldHandleStandardVariables()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("L", 1);
            parameters.Add("W", 1);
            parameters.Add("H", 1);
            parameters.Add("FW", 1);
            parameters.Add("FT", new MicroMeter(1000));

            var solution = FormulaEvaluator.Solve<MicroMeter>("L + W + H + FW + FT", parameters);
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(5.0));
        }

        [TestMethod]
        public void ShouldThrowIfInvalidParameterIsUsed()
        {
            Specify.ThatAction(() => FormulaEvaluator.Solve<MicroMeter>("Z + FT")).
                Should.HaveThrown(typeof(ArgumentException));

            Specify.ThatAction(() => FormulaEvaluator.Solve<MicroMeter>("X100 + FT")).
                Should.HaveThrown(typeof(ArgumentException));
        }

        [TestMethod]
        public void ShouldThrowIfInvalidFunctionIsUsed()
        {
            Specify.ThatAction(() => FormulaEvaluator.Solve<MicroMeter>("bil(1337)")).
                Should.HaveThrown(typeof(ArgumentException));
        }

        [TestMethod]
        public void ShouldHandleParantesis()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("((4/2)) * ((1+3)/2)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(4.0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFilterParametersFromFormulaString()
        {            
            const string formula = "X1+0.5+L+FW+(FW-L)-X2+FT+X20";

            var parameters = FormulaEvaluator.GetFormulaParameters(formula);

            var enumerable = parameters as IList<string> ?? parameters.ToList();

            Specify.That(enumerable.Count()).Should.BeLogicallyEqualTo(8);
            Specify.That(enumerable).Should.Contain("X1");
            Specify.That(enumerable).Should.Contain("X2");
            Specify.That(enumerable).Should.Contain("X20");
            Specify.That(enumerable).Should.Contain("L");
            Specify.That(enumerable).Should.Not.Contain("0.5");            
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldOnlyAcceptCorrectXValues()
        {
            // Throw exception if invalid XVal???

            var formula = "X1+XRoboCop";

            var parameters = FormulaEvaluator.GetFormulaParameters(formula);

            var enumerable = parameters as IList<string> ?? parameters.ToList();

            Specify.That(enumerable.Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(enumerable).Should.Contain("X1");
            Specify.That(enumerable).Should.Not.Contain("XRoboCop");

            formula = "X1+XRoboCop+X2+X45.4";

            parameters = FormulaEvaluator.GetFormulaParameters(formula);

            enumerable = parameters as IList<string> ?? parameters.ToList();

            Specify.That(enumerable.Count()).Should.BeLogicallyEqualTo(2);
            Specify.That(enumerable).Should.Contain("X1");
            Specify.That(enumerable).Should.Contain("X2");
            Specify.That(enumerable).Should.Not.Contain("XRoboCop");
            Specify.That(enumerable).Should.Not.Contain("X45.4"); 
        }
    }
}
