﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.Exceptions;
    using PackNet.Common.ComponentModel.FormulaEvaluator;

    using Testing.Specificity;

    [TestClass]
    public class FormulaValidationTests
    {
        [TestMethod]
        public void ShouldHandleNullAsEmptyFormula()
        {
            Specify.ThatAction(delegate { FormulaEvaluator.Validate(null); }).Should.Not.HaveThrown();
        }

        [TestMethod]
        public void FormulasStartingWithPlusShouldValidate()
        {
            Specify.ThatAction(delegate { FormulaEvaluator.Validate("+1"); }).Should.Not.HaveThrown();
        }

        [TestMethod]
        public void FormulasStartingWithMinusShouldValidate()
        {
            Specify.ThatAction(delegate { FormulaEvaluator.Validate("-1"); }).Should.Not.HaveThrown();
        }

        [TestMethod]
        public void OperatorsWithNotEnoughArgumentShouldThrowException()
        {
            Specify.ThatAction(delegate { FormulaEvaluator.Validate("1-"); }).Should.HaveThrown(typeof(InvalidFormulaException));
        }

        [TestMethod]
        public void FunctionsWithNotEnoughArgumentShouldThrowException()
        {
            Specify.ThatAction(delegate { FormulaEvaluator.Validate("SIN()"); }).Should.HaveThrown(typeof(InvalidFormulaException));
        }
    }
}
