﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using System;
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.Exceptions;
    using PackNet.Common.ComponentModel.FormulaEvaluator;

    using Testing.Specificity;

    [TestClass]
    public class EqualTests
    {
        [TestMethod]
        public void EqualWithBooleansAreNotSupported()
        {
            Specify.ThatAction(() => FormulaEvaluator.Solve<bool>("1 < 2 == 1 < 2")).
                Should.HaveThrown(typeof(ArgumentException));
        }

        [TestMethod]
        public void SingleEqualSyntaxIsNotValid()
        {
            Specify.ThatAction(() => FormulaEvaluator.Solve<bool>("1 = 2 == 1 = 2")).
                Should.HaveThrown(typeof(ArgumentException));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EqualBooleanFalse()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 = 2 == 1 = 1");
        }

        [TestMethod]
        public void OneEqualsOneReturnsTrue()
        {
            bool result = FormulaEvaluator.Solve<bool>("1 == 1");

            Specify.That(result).Should.BeTrue();
        }

        [TestMethod]
        public void OneEqualsTwoReturnsFalse()
        {
            bool result = FormulaEvaluator.Solve<bool>("1 == 2");

            Specify.That(result).Should.BeFalse();
        }

        [TestMethod]
        public void StringEqualsStringGiveTrue()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "test");

            bool result = FormulaEvaluator.Solve<bool>("PZ == \"test\"", parameters);

            Specify.That(result).Should.BeTrue();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidFormulaException))]
        public void StringThrowsExceptionWhenStringNotClosed()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "test");

           FormulaEvaluator.Solve<bool>("PZ == \"test", parameters);

        }

        [TestMethod]
        public void StringDoNotEqualStringGiveFalse()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("PZ", "test45");

            bool result = FormulaEvaluator.Solve<bool>("PZ == \"test\"", parameters);

            Specify.That(result).Should.BeFalse();
        }


        [TestMethod]
        public void ShouldHandleStringsAsComparible()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("L", "test");
            var solution = FormulaEvaluator.Solve<bool>("L == \"fail\"", parameters);
            Specify.That(solution).Should.BeFalse();

            parameters = new Dictionary<string, object>();
            parameters.Add("L", "fail");
            solution = FormulaEvaluator.Solve<bool>("L == \"fail\"", parameters);
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldHandleStringsAsComparibleWithOperatorInString()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("L", "test-1");
            var solution = FormulaEvaluator.Solve<bool>("L == \"test-1\"", parameters);
            Specify.That(solution).Should.BeTrue();

            parameters = new Dictionary<string, object>();
            parameters.Add("L", "test+1");
            solution = FormulaEvaluator.Solve<bool>("L == \"test+1\"", parameters);
            Specify.That(solution).Should.BeTrue();

            parameters = new Dictionary<string, object>();
            parameters.Add("L", "test/1");
            solution = FormulaEvaluator.Solve<bool>("L == \"test/1\"", parameters);
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldHandleStringsAsComparibleWithSpaces()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("L", "OutFeedTable 500 700");
            var solution = FormulaEvaluator.Solve<bool>("L == \"OutFeedTable500700\"", parameters);
            Specify.That(solution).Should.BeTrue();

             parameters = new Dictionary<string, object>();
            parameters.Add("L", "OutFeedTable500700");
             solution = FormulaEvaluator.Solve<bool>("L == \"OutFe edTable 500700\"", parameters);
            Specify.That(solution).Should.BeTrue();

            parameters = new Dictionary<string, object>();
            parameters.Add("L", "OutFeedTable 500 700");
            solution = FormulaEvaluator.Solve<bool>("L == \"OutFeedTable 500 700\"", parameters);
            Specify.That(solution).Should.BeTrue();
        }
    }
}
