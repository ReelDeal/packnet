﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class SubtractionTests
    {
        [TestMethod]
        public void BasicSubtraction()
        {
            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("1338 - 1");

            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1337.0));
        }

        [TestMethod]
        public void MultiValueSubtraction()
        {
            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("1345 - 5 - 2 - 1");

            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1337.0));
        }

        [TestMethod]
        public void VariableSubtraction()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("X20", 1);

            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("1338 - X20", parameters);

            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1337.0));
        }
    }
}
