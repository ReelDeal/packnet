﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class OldPowerTests
    {
        [TestMethod]
        public void POWTwoGivesFour()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("POW(2)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(4.0));
        }
    }
}
