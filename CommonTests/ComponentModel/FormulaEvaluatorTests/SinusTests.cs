﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class SinusTests
    {
        [TestMethod]
        public void SinZeroGivesZero()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Sin(0)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0));
        }

        [TestMethod]
        public void Sin90GivesOne()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Sin(90)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1.0));
        }

        [TestMethod]
        public void Sin180GivesZero()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Sin(180)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0));
        }

        [TestMethod]
        public void Sin360GivesZero()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Sin(360)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0));
        }
    }
}
