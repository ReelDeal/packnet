﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class CosinusTests
    {
        [TestMethod]
        public void CosinusTest()
        {
            MicroMeter solution = FormulaEvaluator.Solve<MicroMeter>("Cos(0)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1.0));

            solution = FormulaEvaluator.Solve<MicroMeter>("Cos(90)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0.0));

            solution = FormulaEvaluator.Solve<MicroMeter>("Cos(180)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(-1.0));

            solution = FormulaEvaluator.Solve<MicroMeter>("Cos(360)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(1.0));
        }
    }
}
