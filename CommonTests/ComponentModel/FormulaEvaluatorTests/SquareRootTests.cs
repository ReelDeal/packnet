﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class SquareRootTests
    {
        [TestMethod]
        public void SquareOfEightyOneGivesNine()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Sqr(81)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(9.0));
        }
    }
}
