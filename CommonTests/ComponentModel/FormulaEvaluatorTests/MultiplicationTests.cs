﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class MultiplicationTests
    {
        [TestMethod]
        public void MultiplicationWithConstants()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("5 * 4");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(20.0));
        }

        [TestMethod]
        public void MultiplicationWithVariables()
        {
            var parameters = new Dictionary<string, object>();
            parameters.Add("X1", 5);
            parameters.Add("X2", 4);

            var solution = FormulaEvaluator.Solve<MicroMeter>("X1 * X2", parameters);
                
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(20.0));
        }
    }
}
