﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;

    using Testing.Specificity;

    [TestClass]
    public class AndTests
    {
        [TestMethod]
        public void AndLeftOpFalseRightOpTrue()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 < 0 && 1 > 0");

            Specify.That(solution).Should.BeEqualTo(false);
        }

        [TestMethod]
        public void AndLeftOpTrueRightOpFalse()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 > 0 && 1 < 0");

            Specify.That(solution).Should.BeEqualTo(false);
        }

        [TestMethod]
        public void AndLeftOpFalseRightOpFalse()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 < 0 && 1 < 0");

            Specify.That(solution).Should.BeEqualTo(false);
        }

        [TestMethod]
        public void AndLeftOpTrueRightOpTrue()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 > 0 && 1 > 0");

            Specify.That(solution).Should.BeEqualTo(true);
        }

        [TestMethod]
        public void MultipleAndTrue()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 > 0 && 1 > 0 && 1 > 0");

            Specify.That(solution).Should.BeEqualTo(true);
        }

        [TestMethod]
        public void MultipleAndFalse()
        {
            bool solution = FormulaEvaluator.Solve<bool>("1 > 0 && 1 > 0 && 1 < 0");

            Specify.That(solution).Should.BeEqualTo(false);
        }
    }
}
