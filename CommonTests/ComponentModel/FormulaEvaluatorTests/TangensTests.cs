﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class TangensTests
    {
        [TestMethod]
        public void TanZeroGivesZero()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Tan(0)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0));
        }

        [TestMethod]
        public void Tan180GivesZero()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Tan(180)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0));
        }

        [TestMethod]
        public void Tan360GivesZero()
        {
            var solution = FormulaEvaluator.Solve<MicroMeter>("Tan(360)");
            Specify.That(solution).Should.BeEqualTo(new MicroMeter(0));
        }
    }
}
