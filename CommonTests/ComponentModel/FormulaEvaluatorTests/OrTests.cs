﻿namespace CommonTests.ComponentModel.FormulaEvaluatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.FormulaEvaluator;

    using Testing.Specificity;
    using System.Collections.Generic;

    [TestClass]
    public class OrTests
    {
        [TestMethod]
        public void TrueOrTrueGivesTrue()
        {
            var solution = FormulaEvaluator.Solve<bool>("1 > 0 || 1 > 0", new Dictionary<string, object>());
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void FalseOrTrueGivesTrue()
        {
            var solution = FormulaEvaluator.Solve<bool>("1 > 2 || 1 > 0", new Dictionary<string, object>());
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void TrueOrFalseGivesTrue()
        {
            var solution = FormulaEvaluator.Solve<bool>("1 > 0 || 1 > 2", new Dictionary<string, object>());
            Specify.That(solution).Should.BeTrue();
        }

        [TestMethod]
        public void FalseOrFalseGivesFalse()
        {
            var solution = FormulaEvaluator.Solve<bool>("1 > 2 || 1 > 2", new Dictionary<string, object>());
            Specify.That(solution).Should.BeFalse();
        }
    }
}
