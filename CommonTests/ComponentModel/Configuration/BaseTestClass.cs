namespace CommonTests.ComponentModel.Configuration
{
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://Packsize.MachineManager.com")]
    public class BaseTestClass
    {
        [DataMember]
        public virtual string Alias { get; set; }

        [DataMember]
        public virtual int Id { get; set; }

        public virtual int PropertyNotMappedInBaseClass { get; set; }
    }
}