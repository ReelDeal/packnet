namespace CommonTests.ComponentModel.Configuration
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using PackNet.Common.ComponentModel.Configuration;

    [DataContract(Namespace = "http://Packsize.MachineManager.com")]
    [KnownType(typeof(DerivedTestClass))]
    public class TestProvider : XmlConfiguration<TestProvider>
    {
        [DataMember]
        public List<BaseTestClass> TestClasses { get; set; }
    }
}