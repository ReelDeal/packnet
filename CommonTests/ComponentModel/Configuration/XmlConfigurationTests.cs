﻿namespace CommonTests.ComponentModel.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.ComponentModel.Configuration;

    using Testing.Specificity;

    [TestClass]
    [DeploymentItem("ComponentModel\\Configuration\\tst.xml", "Configuration")]
    public class XmlConfigurationTests
    {
        [TestMethod]
        public void ShouldBeAbleToSerializeListOfTestClasses()
        {
            var testProvider = new TestProvider
                {
                    TestClasses =
                        new List<BaseTestClass>
                            {
                                new BaseTestClass { Id = 1, Alias = "BaseTestClass", PropertyNotMappedInBaseClass = 3 },
                                new DerivedTestClass
                                    { Id = 2, Alias = "DerivedTestClass", PropertyNotMappedInBaseClass = 10 }
                            }
                };
            var memoryStream = new MemoryStream();

            try
            {
                testProvider.SerializeToStream(memoryStream);

                memoryStream.Position = 0;
                var sr = new StreamReader(memoryStream);
                var myStr = sr.ReadToEnd();
                Console.WriteLine(myStr);
            }
            finally
            {
                memoryStream.Close();
            }
        }

        [TestMethod]
        public void ShouldBeAbleToDeserializeProviderWithBothBaseAndDerivedClasses()
        {
            var path = Environment.CurrentDirectory + @"\Configuration\tst.xml";
            var testProvider = XmlConfiguration<TestProvider>.Deserialize(path);
            Specify.That(testProvider.TestClasses.Count).Should.BeEqualTo(2);

            var baseTestClass = testProvider.TestClasses.First();
            Specify.That(baseTestClass).Should.BeInstanceOfType(typeof(BaseTestClass));
            Specify.That(baseTestClass.Id).Should.BeEqualTo(1);
            Specify.That(baseTestClass.Alias).Should.BeEqualTo("BaseTestClass");
            Specify.That(baseTestClass.PropertyNotMappedInBaseClass).Should.BeEqualTo(0);

            var derivedTestClass = testProvider.TestClasses.ElementAt(1);
            Specify.That(derivedTestClass).Should.BeInstanceOfType(typeof(DerivedTestClass));
            Specify.That(derivedTestClass.Id).Should.BeEqualTo(2);
            Specify.That(derivedTestClass.Alias).Should.BeEqualTo("DerivedTestClass");
            Specify.That(derivedTestClass.PropertyNotMappedInBaseClass).Should.BeEqualTo(10);
        }
    }
}