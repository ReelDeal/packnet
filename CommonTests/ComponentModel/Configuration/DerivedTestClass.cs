namespace CommonTests.ComponentModel.Configuration
{
    using System.Runtime.Serialization;

    [DataContract(Namespace = "http://Packsize.MachineManager.com")]
    public class DerivedTestClass : BaseTestClass
    {
        [DataMember]
        public int PropertyNotExistingInBaseClass
        {
            get
            {
                return PropertyNotMappedInBaseClass;
            }

            set
            {
                PropertyNotMappedInBaseClass = value;
            }
        }
    }
}