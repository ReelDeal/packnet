﻿namespace CommonTests.ComponentModel.Types
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class MicroMeterTests
    {
        [TestMethod]
        public void ShouldWorkWithImplicitIntOperators()
        {
            int value = 20;
            MicroMeter m = value;
            Specify.That(value).Should.BeLogicallyEqualTo(m);
            Specify.That(m).Should.BeLogicallyEqualTo(value);
        }

        [TestMethod]
        public void ShouldWorkWithImplicitDoubleOperators()
        {
            double value = 20;
            MicroMeter m = value;
            Specify.That(value).Should.BeLogicallyEqualTo(m);
            Specify.That(m).Should.BeLogicallyEqualTo(value);
        }

        [TestMethod]
        public void OverloadedOperators()
        {
            MicroMeter m1 = 1;
            MicroMeter m2 = 2;
            double d1 = 1;
            double d2 = 2;
            Specify.That(m1 + m2).Should.BeLogicallyEqualTo(d1 + d2);
            Specify.That(m1 - m2).Should.BeLogicallyEqualTo(d1 - d2);
            Specify.That(-m2).Should.BeLogicallyEqualTo(-d2);
            Specify.That(m1 * m2).Should.BeLogicallyEqualTo(d1 * d2);
            Specify.That(m1 / m2).Should.BeLogicallyEqualTo(d1 / d2);
            Specify.That(m1 > m2).Should.BeLogicallyEqualTo(d1 > d2);
            Specify.That(m1 < m2).Should.BeLogicallyEqualTo(d1 < d2);
            Specify.That(m1 >= m2).Should.BeLogicallyEqualTo(d1 >= d2);
            Specify.That(m1 <= m2).Should.BeLogicallyEqualTo(d1 <= d2);
            Specify.That(m1 == m2).Should.BeLogicallyEqualTo(d1 == d2);
            Specify.That(m1 != m2).Should.BeLogicallyEqualTo(d1 != d2);
        }

        [TestMethod]
        public void MaxAndMinValueShouldBeEqualToAThousandOfInt()
        {
            Specify.That(MicroMeter.MaxValue).Should.BeLogicallyEqualTo(((double)int.MaxValue) / 1000D);
            Specify.That(MicroMeter.MinValue).Should.BeLogicallyEqualTo(((double)int.MinValue) / 1000D);
        }

        [TestMethod]
        public void CompareShouldReturnMinus1IfSelfIsSmallerThanOther()
        {
            MicroMeter small = 1;
            MicroMeter large = 2;
            Specify.That(small.CompareTo(large)).Should.BeEqualTo(-1);
        }

        [TestMethod]
        public void CompareShouldReturn0IfSelfIsEqualThanOther()
        {
            MicroMeter small = 1;
            MicroMeter large = 1;
            Specify.That(small.CompareTo(large)).Should.BeEqualTo(0);
        }

        [TestMethod]
        public void CompareShouldReturn1IfSelfIsLargerThanOther()
        {
            MicroMeter small = 1;
            MicroMeter large = 2;
            Specify.That(large.CompareTo(small)).Should.BeEqualTo(1);
        }

        [TestMethod]
        public void EqualsShouldReturnFalseForNonMicroMeter()
        {
            MicroMeter m = 1;
            object o = 2;
            Specify.That(m.Equals(o)).Should.BeFalse();
        }

        [TestMethod]
        public void GetHashCodeShouldReturnHashCodeOfInteger()
        {
            MicroMeter m = new MicroMeter(1000);
            Specify.That(m.GetHashCode()).Should.BeEqualTo(1000.GetHashCode());
        }

        [TestMethod]
        public void ToStringTest()
        {
            MicroMeter m = new MicroMeter(12.34);
            Specify.That(m.ToString()).Should.BeEqualTo("12.34");
        }

        [TestMethod]
        public void ImplicitNullableDoubleConstructorTest()
        {
            double? value = null;
            MicroMeter m = value;
            Assert.AreEqual(new MicroMeter(0), m);

            value = 1.23;
            m = value;
            Specify.That(value.Value == m);
        }

        [TestMethod]
        public void ImplicitNullableDoubleToNullableConstructorTest()
        {
            double? value = null;
            MicroMeter? m = value;
            Assert.IsNull(m);

            value = 1.23;
            m = value;
            Specify.That(value.Value == m);
        }
    }
}
