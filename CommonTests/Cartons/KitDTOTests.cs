﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;


using Testing.Specificity;

namespace CommonTests.Cartons
{
    [TestClass]
    public class KitDTOTests
    {
        [TestMethod]
        public void ShouldHandleCartonAndLabelFromStagedToCompleted()
        {
            var label = new Label();
            var carton = new Carton();
            carton.ProducibleStatus = label.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged; 

            var objectInTest = new Kit();
            objectInTest.AddProducible(label);
            objectInTest.AddProducible(carton);

             label.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleSentToMachine);

             carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleSentToMachine);

             label.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);

             carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);

             carton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);

             label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        public void ShouldHandleCartonAndLabelFromStagedToRemoved()
        {
            var label = new Label();
            var carton = new Carton();
            carton.ProducibleStatus = label.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged; 

            var objectInTest = new Kit();
            objectInTest.AddProducible(label);
            objectInTest.AddProducible(carton);

             label.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleSentToMachine);

             carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleSentToMachine);

             label.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);

             carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);

             carton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);

             label.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
            Specify.That(objectInTest.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleRemoved);
        }
    }
}
