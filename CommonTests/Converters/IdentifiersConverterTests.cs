﻿using System;
using System.Configuration;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;

using Testing.Specificity;

namespace CommonTests.Converters
{
    [TestClass]
    public class IdentifiersConverterTests
    {
        private JsonSerializerSettings settings;

        [TestInitialize]
        public void setup()
        {
            settings = SerializationSettings.GetJsonSerializerSettings();
        }

        [TestMethod]
        public void ShouldFormatProducibleAsExpected()
        {
            var sut = new Carton
            {
                Id = Guid.NewGuid(),
                ArticleId = "33",
                ProducibleStatus = ProducibleStatuses.ProducibleCompleted,
                CustomerUniqueId = "minimal"
            };

            var serialized = JsonConvert.SerializeObject(sut, new ProducibleIdentifiersConverter());//don't use the global SerializationSettings.GetJsonSerializerSettings() because we will be calling this directyly
            /* Expected
             *{"ProducibleType":"Carton","CustomerUniqueId":"minimal","Id":"4f93e46f-db60-4205-ad88-4f3f483b87f6","ProducibleStatus":"ProducibleCompleted"}
             */
            Specify.That(serialized).Should.Contain("\"Producible\":\"Carton\"");
            Specify.That(serialized).Should.Contain("\"CustomerUniqueId\":\"minimal\"");
            Specify.That(serialized).Should.Contain("\"Id\":");
            Specify.That(serialized).Should.Contain("\"Status\":\"ProducibleCompleted\"");

        }

        [TestMethod]
        public void ShouldFormatMachineAsExpected()
        {
            var sut = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "33",
                CurrentProductionStatus = MachineProductionInProgressStatuses.ProductionInProgress,
                CurrentStatus = MachineStatuses.MachineOffline
            };

            var serialized = JsonConvert.SerializeObject(sut, new MachineIdentifiersConverter());//don't use the global SerializationSettings.GetJsonSerializerSettings() because we will be calling this directyly
            /* Expected
             *{"ProducibleType":"Carton","CustomerUniqueId":"minimal","Id":"4f93e46f-db60-4205-ad88-4f3f483b87f6","ProducibleStatus":"ProducibleCompleted"}
             */
            Specify.That(serialized).Should.Contain("\"Alias\":\"33\"");
            Specify.That(serialized).Should.Contain("\"Id\":");
            Specify.That(serialized).Should.Contain("\"Status\":\"MachineOffline\"");
            Specify.That(serialized).Should.Contain("\"ProductionStatus\":\"ProductionInProgress\"");

        }


        [TestMethod]
        public void ShouldFormatMachineGroupAsExpected()
        {
            var sut = new MachineGroup
            {
                Id = Guid.NewGuid(),
                Alias = "33",
                CurrentStatus = MachineGroupUnavailableStatuses.MachineGroupOffline
            };

            var serialized = JsonConvert.SerializeObject(sut, new MachineGroupIdentifiersConverter());//don't use the global SerializationSettings.GetJsonSerializerSettings() because we will be calling this directyly
            /* Expected
             *{"ProducibleType":"Carton","CustomerUniqueId":"minimal","Id":"4f93e46f-db60-4205-ad88-4f3f483b87f6","ProducibleStatus":"ProducibleCompleted"}
             */
            Specify.That(serialized).Should.Contain("\"MachineGroup\":\"33\"");
            Specify.That(serialized).Should.Contain("\"Id\":");
            Specify.That(serialized).Should.Contain("\"Status\":\"MachineGroupOffline\"");

        }

    }
}
