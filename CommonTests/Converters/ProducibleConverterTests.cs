﻿using System;
using System.Collections.Generic;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.Utils;

using Testing.Specificity;

namespace CommonTests.Converters
{
    [TestClass]
    public class ProducibleConverterTests
    {
        [TestMethod]
        public void CanDeserializeCartonWithRestrictionsTest()
        {
            var settings = SerializationSettings.GetJsonSerializerSettings();
            var carton = new Carton { CustomerUniqueId = "carton1", Width = 10 };
            carton.Restrictions.Add(new MustProduceOnCorrugateWithPropertiesRestriction { Quality = 41 });
            var jsonCarton = JsonConvert.SerializeObject(carton, settings);
            var carton2 = JsonConvert.DeserializeObject<Carton>(jsonCarton, settings);
            Specify.That(carton2).Should.Not.BeNull("order null");
            var restriction = carton2.Restrictions[0] as MustProduceOnCorrugateWithPropertiesRestriction;
            Specify.That(restriction.Quality).Should.BeEqualTo(41);
       }

        [TestMethod]
        public void CanDeserializeUsingSerializeWithRestrictionsTest()
        {
            var settings = SerializationSettings.GetJsonSerializerSettings();
            var carton = new Carton { CustomerUniqueId = "carton1", Width = 10 };
            carton.Restrictions.Add(new MustProduceOnCorrugateWithPropertiesRestriction { Quality = 41 });
            var orderO = new Order(carton, 100);
            var jsonCarton = JsonConvert.SerializeObject(orderO, settings);
            Console.WriteLine(jsonCarton);
            var order = JsonConvert.DeserializeObject<Order>(jsonCarton, settings);
            Specify.That(order).Should.Not.BeNull("order null");
            Specify.That(order.Producible).Should.Not.BeNull("order.Producible null");

        }
        [TestMethod]
        public void CanDeserializeMessageUsingSerializeWithRestrictionsTest()
        {
            var settings = SerializationSettings.GetJsonSerializerSettings();
            var carton = new Carton { CustomerUniqueId = "carton1", Width = 10 };
            carton.Restrictions.Add(new MustProduceOnCorrugateWithPropertiesRestriction { Quality = 41 });
            var orderO = new Order(carton, 100);
            var jsonCarton = JsonConvert.SerializeObject(new Message<Order> { Data = orderO }, settings);
            Console.WriteLine(jsonCarton);
            var order = JsonConvert.DeserializeObject<Message<Order>>(jsonCarton, settings);
            Specify.That(order).Should.Not.BeNull("order null");
            Specify.That(order.Data).Should.Not.BeNull("order.Producible null");
            Specify.That(order.Data.Producible).Should.Not.BeNull("order.Producible null");

        }
        [TestMethod]
        public void CanDeserializeKitUsingSerializeWithRestrictionsTest()
        {
            var settings = SerializationSettings.GetJsonSerializerSettings();
            var carton = new Carton { CustomerUniqueId = "carton1", Width = 10 };
            carton.Restrictions.Add(new MustProduceOnCorrugateWithPropertiesRestriction { Quality = 41 });
            var orderO = new Order(carton, 100){CustomerUniqueId = "order in kit"};

            var kit = new Kit { CustomerUniqueId = "kit1", ItemsToProduce = new ConcurrentList<IProducible> { orderO, new Carton { CustomerUniqueId = "carton2", Width = 11.1 } } };
            var jsonCarton = JsonConvert.SerializeObject(kit, settings);

            Console.WriteLine(jsonCarton);
            var skit = JsonConvert.DeserializeObject<Kit>(jsonCarton, settings);

            var order = skit.ItemsToProduce[0] as Order;
            Specify.That(order).Should.Not.BeNull("order null");
            Specify.That(order.Producible).Should.Not.BeNull("order.Producible null");

            var cton = skit.ItemsToProduce[1] as Carton;
            Specify.That(cton).Should.Not.BeNull("carton null");

        }
    }
}
