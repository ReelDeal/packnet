﻿using System.ComponentModel;
using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO;
using Testing.Specificity;

namespace CommonTests.Converters
{
    [TestClass]
    public class MicroMeterConverterTests
    {
        private JsonSerializerSettings settings;

        [TestInitialize]
        public void Setup()
        {
            settings = SerializationSettings.GetJsonSerializerSettings();
        }

        [TestMethod]
        public void MicroMeterConverterShouldBeAbleToConvertMicroMeter()
        {
            var sut = new MicroMeterConverter();
            Specify.That(sut.CanConvert(typeof(MicroMeter))).Should.BeEqualTo(true);
        }
        
        [TestMethod]
        public void MicroMeterConverterShouldConvertMicroMeterAsExpected()
        {
            var res = JsonConvert.SerializeObject(new MicroMeter(12000), settings);
            Specify.That(res).Should.BeEqualTo("12.0");

        }

        [TestMethod]
        public void MicroMeterConverterShouldConvertTwelvePointZeroToExpectedMicroMeter()
        {
            var res = JsonConvert.DeserializeObject<MicroMeter>("12.0", settings);
            Specify.That(res).Should.BeEqualTo(new MicroMeter(12000));

        }

        [TestMethod]
        public void MicroMeterConverterShouldConvertTwelveToExpectedMicroMeter()
        {
            var res = JsonConvert.DeserializeObject<MicroMeter>("12", settings);
            Specify.That(res).Should.BeEqualTo(new MicroMeter(12000));
        }

        [TestMethod]
        public void MicroMeterConverterShouldConvertTwelvePointFiftyFiveToExpectedMicroMeter()
        {
            var res = JsonConvert.DeserializeObject<MicroMeter>("12.55", settings);
            Specify.That(res).Should.BeEqualTo(new MicroMeter(12.55d));
            Specify.That(res).Should.BeEqualTo(new MicroMeter(12550));
        }
    }
}
