﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;

using Testing.Specificity;

namespace CommonTests.Converters
{
    [TestClass]
    public class BrPlcBoolConverterTests
    {
        private JsonSerializerSettings settings = SerializationSettings.GetJsonSerializerSettings();

        [TestMethod]
        public void BoolConverterShouldBeAbleToConvertBool()
        {
            var sut = new BrPlcBoolConverter();
            Specify.That(sut.CanConvert(typeof(bool))).Should.BeEqualTo(true);
        }

        [TestMethod]
        public void BoolConverterShouldConvertTrueToOne()
        {
            var res = JsonConvert.SerializeObject(true, settings);
            Specify.That(res).Should.BeEqualTo("1");
        }

        [TestMethod]
        public void BoolConverterShouldConvertFalseToZero()
        {
            var res = JsonConvert.SerializeObject(false, settings);
            Specify.That(res).Should.BeEqualTo("0");
        }

        [TestMethod]
        public void BoolConverterShouldConvertOneToTrue()
        {
            var res = JsonConvert.DeserializeObject<bool>("1", settings);
            Specify.That(res).Should.BeEqualTo(true);
        }

        [TestMethod]
        public void BoolConverterShouldConvertZeroToFalse()
        {
            var res = JsonConvert.DeserializeObject<bool>("0", settings);
            Specify.That(res).Should.BeEqualTo(false);
        }
    }
}
