﻿namespace CommonTests.Eventing
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Common.Eventing;
    using System;

    using PackNet.Common.Interfaces.Eventing;

    using Testing.Specificity;

    [TestClass]
    public class EventAggregatorTests
    {
        private string expectedValue;
        private EventAggregator eventAggregator;
        private IEventAggregatorSubscriber subscriber;
        private IEventAggregatorPublisher publisher;

        [TestInitialize]
        public void Init()
        {
            eventAggregator = new EventAggregator();
            subscriber = eventAggregator;
            publisher = eventAggregator;
        }

        [TestMethod]
        public void EventAggregatorCanPublishAndSubscribe()
        {
            var observable = subscriber.GetEvent<string>();

            var publishedValue = "";
            using (observable.Subscribe(s => publishedValue = s))
            {
                expectedValue = "some event";
                publisher.Publish(expectedValue);

                Specify.That(publishedValue).Should.BeEqualTo(expectedValue);
            }
        }
    }
}
