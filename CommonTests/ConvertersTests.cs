﻿namespace CommonTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Newtonsoft.Json;

    using PackNet.Common.Interfaces.Converters;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class ConvertersTests
    {
        [TestMethod]
        public void CanConvertNullableIntFromString()
        {
            var articleString =
                "{\r\n    \"Id\": null,\r\n    \"Custom\": false,\r\n    \"ArticleId\": \"9869756\",\r\n    \"Description\": null,\r\n    \"Length\": 7,\r\n    \"Width\": 7,\r\n    \"Height\": 7,\r\n    \"DesignId\": 2010003,\r\n    \"MachineType\": \"Fusion\",\r\n    \"CorrugateQuality\": 345,\r\n    \"UserName\": \"qa\",\r\n    \"LastUpdated\": \"2014-05-14T13:12:49-06:00\"\r\n  }";
            var article = JsonConvert.DeserializeObject<Article>(articleString, SerializationSettings.GetJsonSerializerSettings());
            Specify.That(article.CorrugateQuality.Value).Should.BeEqualTo(345);

            articleString =
                "{\r\n    \"Id\": null,\r\n    \"Custom\": false,\r\n    \"ArticleId\": \"9869756\",\r\n    \"Description\": null,\r\n    \"Length\": 7,\r\n    \"Width\": 7,\r\n    \"Height\": 7,\r\n    \"DesignId\": 2010003,\r\n    \"MachineType\": \"Fusion\",\r\n    \"CorrugateQuality\": null,\r\n    \"UserName\": \"qa\",\r\n    \"LastUpdated\": \"2014-05-14T13:12:49-06:00\"\r\n  }";
            article = JsonConvert.DeserializeObject<Article>(articleString, SerializationSettings.GetJsonSerializerSettings());
            Specify.That(article.CorrugateQuality.HasValue).Should.BeFalse();
            
            articleString =
                "{\r\n    \"Id\": null,\r\n    \"Custom\": false,\r\n    \"ArticleId\": \"9869756\",\r\n    \"Description\": null,\r\n    \"Length\": 7,\r\n    \"Width\": 7,\r\n    \"Height\": 7,\r\n    \"DesignId\": 2010003,\r\n    \"MachineType\": \"Fusion\",\r\n    \"CorrugateQuality\": \"\",\r\n    \"UserName\": \"qa\",\r\n    \"LastUpdated\": \"2014-05-14T13:12:49-06:00\"\r\n  }";
            article = JsonConvert.DeserializeObject<Article>(articleString, SerializationSettings.GetJsonSerializerSettings());
            Specify.That(article.CorrugateQuality.HasValue).Should.BeFalse();

            articleString =
                "{\r\n    \"Id\": null,\r\n    \"Custom\": false,\r\n    \"ArticleId\": \"9869756\",\r\n    \"Description\": null,\r\n    \"Length\": 7,\r\n    \"Width\": 7,\r\n    \"Height\": 7,\r\n    \"DesignId\": 2010003,\r\n    \"MachineType\": \"Fusion\",\r\n    \"CorrugateQuality\": \"345\",\r\n    \"UserName\": \"qa\",\r\n    \"LastUpdated\": \"2014-05-14T13:12:49-06:00\"\r\n  }";
            article = JsonConvert.DeserializeObject<Article>(articleString, SerializationSettings.GetJsonSerializerSettings());
            Specify.That(article.CorrugateQuality.Value).Should.BeEqualTo(345);
            //If you get here without throwing... much success
        }
    }
}
