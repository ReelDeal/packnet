﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.FileHandling.DynamicImporter;
using PackNet.Common.Interfaces.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

using Testing.Specificity;

namespace CommonTests.FileHandling.DynamicImporter
{
    using System.Globalization;

    [TestClass]
    public class FileEnumeratorTests
    {
        private Mock<ILogger> logger;
        private Mock<IUserNotificationService> notificationService;

        [TestInitialize]
        public void Init()
        {
            logger = new Mock<ILogger>();
            notificationService = new Mock<IUserNotificationService>();
        }

        [TestMethod]
        [TestCategory("3812")]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\MOCK_DATA.csv")]
        public void DynamicImporterCanBeUsedInForEach()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA.csv");
            var count = 0;
            using (var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object))
            {
                //id,first_name,last_name,email,country,ip_address
                foreach (dynamic row in csvData)
                {
                    Specify.That((object)row.id).Should.Not.BeNull();
                    Specify.That((object)row.first_name).Should.Not.BeNull();
                    Specify.That((object)row.last_name).Should.Not.BeNull();
                    Specify.That((object)row.email).Should.Not.BeNull();
                    Specify.That((object)row.country).Should.Not.BeNull();
                    Specify.That((object)row.ip_address).Should.Not.BeNull();
                    count++;
                }
            }
            Specify.That(count).Should.BeEqualTo(1000);
        }

        [TestMethod]
        [TestCategory("3812")]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\MOCK_DATA.csv")]
        public void DynamicFileImporterCanBeUsedInLinqQuery()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA.csv");
            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object);
            var query = (from dynamic c in csvData
                         where c.first_name == "Brian"
                         orderby c.id
                         select new { ID = (string)c.id, FirstName = (string)c.first_name })
                .ToDictionary(a => a.ID, a => a.FirstName);

            Specify.That(query.Count()).Should.BeEqualTo(4);
            Specify.That(query.ContainsKey("746")).Should.BeTrue();
            Specify.That(query.ContainsKey("955")).Should.BeTrue();
            Specify.That(query.ContainsKey("957")).Should.BeTrue();
            Specify.That(query.ContainsKey("992")).Should.BeTrue();
            csvData.Dispose();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("3825")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - Beverlys Extra Data.csv")]
        public void DynamicFileImporterIgnoresAnyDataInARowWithMoreValuesThanTheHeadersAndCreatesLogEntry()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - Beverlys Extra Data.csv");
            var properties = new List<string>();

            using (var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object))
            {
                foreach (dynamic row in csvData)
                {
                    if (row.first_name != "Beverly") continue;

                    foreach (var property in row)
                    {
                        // Only six key-value properties are read in, additional data is ignored
                        //id, first_name, last_name, email, country, ip_address

                        properties.Add(property.Key);
                    }
                }
            }

            Specify.That(properties.Count).Should.BeEqualTo(7);
            Specify.That(properties.Any(p => p.Contains("id"))).Should.BeTrue();
            Specify.That(properties.Any(p => p.Contains("first_name"))).Should.BeTrue();
            Specify.That(properties.Any(p => p.Contains("last_name"))).Should.BeTrue();
            Specify.That(properties.Any(p => p.Contains("email"))).Should.BeTrue();
            Specify.That(properties.Any(p => p.Contains("country"))).Should.BeTrue();
            Specify.That(properties.Any(p => p.Contains("ip_address"))).Should.BeTrue();
            Specify.That(properties.Any(p => p.Contains("rowNumber"))).Should.BeTrue();
            logger.Verify(
                l =>
                    l.Log(LogLevel.Info,
                        It.IsAny<string>(),
                        It.Is<object[]>(o =>
                            o.Contains(12)   // given values count
                            && o.Contains(6) // expected headers count
                            && o.Contains("795,Beverly,Wright,bwright@camimbo.org,Andorra,114.120.31.141,some,extra,data,should,be,ignored"))));
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("3825")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - Beverlys Missing Data.csv")]
        public void DynamicFileImporterHandlesWhenDataMissingInARow()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - Beverlys Missing Data.csv");

            dynamic rowToValidate;
            using (var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object))
            {
                rowToValidate = csvData.Cast<dynamic>().FirstOrDefault(row => row.first_name == "Beverly");
            }

            Assert.IsNotNull(rowToValidate);
            Specify.That((object)rowToValidate.id).Should.Not.BeNull();
            Specify.That((object)rowToValidate.first_name).Should.Not.BeNull();
            Specify.That((object)rowToValidate.last_name).Should.Not.BeNull();
            Specify.That((object)rowToValidate.email).Should.Not.BeNull();
            Specify.That((object)rowToValidate.country).Should.BeNull(); // missing data
            Specify.That((object)rowToValidate.ip_address).Should.BeNull(); // missing data
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("3805")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - NoHeader.csv")]
        public void DynamicFileImporterCanBeUsedWithNoHeaderLine_WhenHeaderGivenInConstructor()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - NoHeader.csv");
            dynamic rowToValidate;
            using (var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, headerFields: "id, first_name, last_name,email,country,ip address".Split(',')))
            {
                rowToValidate = csvData.Cast<dynamic>().FirstOrDefault(row => row.first_name == "Beverly");
            }

            // 795,Beverly,Wright,bwright@camimbo.org,Andorra,114.120.31.141
            Assert.IsNotNull(rowToValidate);
            Specify.That(rowToValidate.id).Equals("795");
            Specify.That(rowToValidate.first_name).Equals("Beverly");
            Specify.That(rowToValidate.last_name).Equals("Wright");
            Specify.That(rowToValidate.email).Equals("bwright@camimbo.org");
            Specify.That(rowToValidate.country).Equals("Andorra");
            Specify.That(rowToValidate.ip_address).Equals("114.120.31.141");
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("3805")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - CommentedHeader.csv")]
        public void DynamicFileImporter_WhenNoHeaderGivenInConstructor_AndHeaderCommentedInFile_ExceptionThrown()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - CommentedHeader.csv"); // commented out header

            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object); // no header given in constructor 
            var query = from dynamic in csvData select new object();
            IEnumerable<object> queryList = null;
            InvalidOperationException ex = null;
            try
            {
                queryList = query.ToList(); // force iteration so Enumerator is called
            }
            catch (InvalidOperationException e)
            {
                ex = e;
            }
            finally
            {
                csvData.Dispose();
            }

            Specify.That(queryList).Should.BeNull("Iteration of query should have thrown exception so queryList should be Null");
            Assert.IsNotNull(ex);
            Specify.That(ex.Message.ToLower().Contains("no headers defined")).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - NoHeader.csv")]
        public void DynamicFileImporter_WhenFieldNameFirstCharacterNotUnderscoreNorAlpha_ExceptionThrown()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - NoHeader.csv");

            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, headerFields: "good_name, 8bad_name".Split(','));
            var query = from dynamic in csvData select new object();
            IEnumerable<object> queryList = null;
            InvalidCharacterException ex = null;
            try
            {
                queryList = query.ToList(); // force iteration so Enumerator is called
            }
            catch (InvalidCharacterException e)
            {
                ex = e;
            }
            finally
            {
                csvData.Dispose();
            }

            Specify.That(queryList).Should.BeNull("Iteration of query should have thrown exception so queryList should be Null");
            Assert.IsNotNull(ex);
            Specify.That(ex.Message.Contains("8bad_name")).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - NoHeader.csv")]
        public void DynamicFileImporter_WhenFieldNameCharacterIsInvalid_ExceptionThrown()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - NoHeader.csv");

            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, headerFields: "good_name1,good_name2,good_name3,bad#!%^&*()_name".Split(','));
            var query = from dynamic in csvData select new object();
            IEnumerable<object> queryList = null;
            InvalidCharacterException ex = null;
            try
            {
                queryList = query.ToList(); // force iteration so Enumerator is called
            }
            catch (InvalidCharacterException e)
            {
                ex = e;
            }
            finally
            {
                csvData.Dispose();
            }

            Specify.That(queryList).Should.BeNull("Iteration of query should have thrown exception so queryList should be Null");
            Assert.IsNotNull(ex);
            Specify.That(ex.Message.Contains("bad#!%^&*()_name")).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - Empty.csv")]
        public void DynamicFileImporterCanBeUsedWithEmptyFile()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - Empty.csv");
            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object);

            var query = from dynamic in csvData select new object();

            Specify.That(query.Count()).Should.BeEqualTo(0);
            csvData.Dispose();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("3805")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - CommentedHeader.csv")]
        public void DynamicFileImporterCanBeUsedWithACommentedHeader()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - CommentedHeader.csv");
            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, headerFields: "id, first_name, last_name,email,country,ip address".Split(','));
            Specify.That(csvData.Count()).Should.BeEqualTo(1000);
            var query = (from dynamic c in csvData
                         where c.first_name == "Brian"
                         orderby c.id
                         select new { ID = (string)c.id, FirstName = (string)c.first_name, IPAddress = (string)c.ip_address })
                .ToDictionary(a => a.ID, a => a.FirstName);

            Specify.That(query.Count()).Should.BeEqualTo(4);
            Specify.That(query.ContainsKey("746")).Should.BeTrue();
            Specify.That(query.ContainsKey("955")).Should.BeTrue();
            Specify.That(query.ContainsKey("957")).Should.BeTrue();
            Specify.That(query.ContainsKey("992")).Should.BeTrue();
            csvData.Dispose();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - CommentedData.csv")]
        public void DynamicFileImporterCanBeUsedWithCommentedData()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - CommentedData.csv");
            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, headerFields: null, delimiter: ',', comment: '`');
            Specify.That(csvData.Count()).Should.BeEqualTo(999);
            var query = (from dynamic c in csvData
                         where c.first_name == "Brian"
                         orderby c.id
                         select new { ID = (string)c.id, FirstName = (string)c.first_name, IPAddress = (string)c.ip_address })
                .ToDictionary(a => a.ID, a => a.FirstName);

            Specify.That(query.Count()).Should.BeEqualTo(4);
            Specify.That(query.ContainsKey("746")).Should.BeTrue();
            Specify.That(query.ContainsKey("955")).Should.BeTrue();
            Specify.That(query.ContainsKey("957")).Should.BeTrue();
            Specify.That(query.ContainsKey("992")).Should.BeTrue();
            csvData.Dispose();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\100Articles.csv")]
        public void DynamicFileImporterCanBeUsedWithCustomSeperator()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "100Articles.csv");
            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, delimiter: ';');

            var query = (from dynamic c in csvData where double.Parse(c.Length, CultureInfo.InvariantCulture) > 15 select c).ToList();

            Specify.That(query.Count()).Should.BeEqualTo(51, "Count incorrect, Should be 51, was: " + query.Count());
            csvData.Dispose();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem(@"TestFiles\StaplesData1.csv")]
        public void DynamicFileImporterCanBeUsedWithExistingStaplesData()
        {
            var header =
                "Command;SerialNumber;OrderId;Quantity;Name;Length;Width;Height;ClassificationNumber;CorrugateQuality;PickZone;DesignId;PrintInfoField1;PrintInfoField2;PrintInfoField3;PrintInfoField4;PrintInfoField5;PrintInfoField6;PrintInfoField7;PrintInfoField8;PrintInfoField9;PrintInfoField10";
            var filePath = Path.Combine(Environment.CurrentDirectory, "StaplesData1.csv");
            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, headerFields: header.Split(';'), delimiter: ';');
            var data = csvData.ToList<dynamic>();
            csvData.Dispose();

            Specify.That(data.Count()).Should.BeEqualTo(499);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void DynamicFileImporterCanBeUsedWithStringBasedStream()
        {
            string input = "Name;Age\r\nEmil;13\r\nMagnus;15\r\n";
            var csvData = new FileEnumerator(logger.Object, input, notificationService.Object, ';');
            var data = csvData.ToList<dynamic>();

            Specify.That(data.Count()).Should.BeEqualTo(2);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [DeploymentItem(@"TestFiles\MOCK_DATA - CommentedData.csv")]
        public void DynamicFileImporterCanBeQueriedSeveralTimesWithAHeaderRowInTheFile()
        {
            var filePath = Path.Combine(Environment.CurrentDirectory, "MOCK_DATA - CommentedData.csv");
            var csvData = new FileEnumerator(filePath, notificationService.Object, logger.Object, null, ',', '`');

            Specify.That(csvData.Count()).Should.BeEqualTo(999);
            Specify.That(csvData.Count()).Should.BeEqualTo(999);
            csvData.Reset();
            Specify.That(csvData.Count()).Should.BeEqualTo(999);
            csvData.Dispose();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void DynamicFileImporterCanBeQueriedSeveralTimesWithAHeaderRowInTheInputString()
        {
            string input = "Name;Age\r\nEmil;13\r\nMagnus;15\r\n";
            var csvData = new FileEnumerator(logger.Object, input, notificationService.Object, ';');
            var data = csvData.ToList<dynamic>();
            var data2 = csvData.ToList<dynamic>();
            csvData.Reset();
            var data3 = csvData.ToList<dynamic>();
            csvData.Dispose();

            Specify.That(data.Count()).Should.BeEqualTo(2);
            Specify.That(data2.Count()).Should.BeEqualTo(2);
            Specify.That(data3.Count()).Should.BeEqualTo(2);
        }
    }
}
