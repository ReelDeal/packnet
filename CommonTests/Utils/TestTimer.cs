namespace CommonTests.Utils
{
    using System;
    using System.Collections.Generic;

    public class TestTimer
    {
        public bool HasElapsed {get; set; }
        public DateTime ElapseSetAt { get; set; }
        public IList<DateTime> TimerElapsedTimes { get; set; }

        public TestTimer()
        {
            TimerElapsedTimes = new List<DateTime>();
        }

        public void Elapse()
        {
            ElapseSetAt = DateTime.UtcNow;
            HasElapsed = true;
            TimerElapsedTimes.Add(ElapseSetAt);
        }
    }
}