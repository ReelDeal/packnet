﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces;
using PackNet.Common.Utils;

using Testing.Specificity;

namespace CommonTests.Utils
{
    [TestClass]
    public class BlockingQueueTests
    {
        [TestMethod]
        public void BlockingQueueGetAll()
        {
            var bq = new BlockingQueue<string>(2);
            var tasks = new List<Task>();

            //Start 10 threads that are all adding
            Enumerable.Repeat(1, 10)
                .ForEach(i =>
                {
                    var task = Task.Factory.StartNew(() =>
                    {
                        bq.Add(i.ToString());
                    });
                });

            Retry.For(() => bq.GetAll().Count() == 2, TimeSpan.FromSeconds(1));
            Specify.That(bq.GetAll().Count()).Should.BeEqualTo(2);
        }

        [TestMethod]
        public void BlockingQueueDoesNotRemoveItem()
        {
            var bq = new BlockingQueue<string>(2);
            var tasks = new ConcurrentDictionary<Guid, Task>();

            //Start 10 threads that are all adding
            Enumerable.Repeat(1, 10)
                .ForEach(i =>
                {
                    var thisTaskGuid = Guid.NewGuid();
                    var task = Task.Factory.StartNew(() =>
                    {
                        Thread.Sleep(50);
                        while (!bq.Add(Guid.NewGuid().ToString()))
                            Thread.Sleep(50);
                    });
                    task.ContinueWith(t =>
                    {
                        Task tt;
                        tasks.TryRemove(thisTaskGuid, out tt);
                    });
                    tasks.TryAdd(thisTaskGuid, task);
                });

            Specify.That(tasks.Count).Should.BeEqualTo(10, "This is the first part.");

            Retry.UntilTrue(() => tasks.Count == 8, TimeSpan.FromSeconds(5));
            Specify.That(tasks.Count).Should.BeEqualTo(8, "After a ridiculous 5 second rety time our threads were not started.");

            var peeked = bq.Peek();
            Specify.That(tasks.Count).Should.BeEqualTo(8, "We got past the Peak part.");
            var taken = bq.Take();
            Specify.That(peeked).Should.BeEqualTo(taken, "This is the last check.");

            //Clear out the threads
            while (bq.Count > 0)
            {
                bq.Take();
                Thread.Sleep(50);
            }
            Retry.UntilTrue(() => !tasks.Any(), TimeSpan.FromSeconds(2));
            Specify.That(tasks.Any()).Should.BeFalse("Tasks still running" + tasks.Count());
        }

        [TestMethod]
        public void BlockingQueueShouldNotBlockPeekOrTakeWhenBlockingOnAdd()
        {
            Enumerable.Range(1, 20).ForEach(i => DoMe());
        }

        private void DoMe()
        {
            int count = 3;
            var bq = new BlockingQueue<int>(count);
            var don = false;
            //fill the queue it should block on each add after count is reached.
            var task = Task.Factory.StartNew(() =>
            {
                bq.Add(1);
                bq.Add(2);
                bq.Add(3);
                bq.Add(4);
                bq.Add(5);
                bq.Add(6);
            });
            var task2 = Task.Factory.StartNew(() =>
            {
                Retry.UntilTrue(() => { return bq.Count == count; }, TimeSpan.FromMilliseconds(100));
                Console.WriteLine(string.Join(", ", bq.GetAll()));

                var peeked = bq.PeekAndWait(new CancellationToken());
                Specify.That(peeked).Should.BeEqualTo(1);
                var taken = bq.Take();
                Specify.That(peeked).Should.BeEqualTo(taken);
                Retry.UntilTrue(() => { return bq.Count == count; }, TimeSpan.FromMilliseconds(100));
                Specify.That(bq.Count).Should.BeEqualTo(count);
                Console.WriteLine(string.Join(", ", bq.GetAll()));

                peeked = bq.PeekAndWait(new CancellationToken());
                Specify.That(peeked).Should.BeEqualTo(2);
                taken = bq.Take();
                Specify.That(peeked).Should.BeEqualTo(taken);
                Retry.UntilTrue(() => { return bq.Count == count; }, TimeSpan.FromMilliseconds(100));
                Specify.That(bq.Count).Should.BeEqualTo(count);
                Console.WriteLine(string.Join(", ", bq.GetAll()));
                don = true;
            });
            Retry.UntilTrue(() => { return don; }, TimeSpan.FromMilliseconds(1000));
        }
    }


}
