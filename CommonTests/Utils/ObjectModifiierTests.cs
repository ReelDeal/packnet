namespace CommonTests.Utils
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Utils;

    using Testing.Specificity;

    [TestClass]
    public class ObjectModifierTests
    {
        private Mock mock;

        [TestInitialize]
        public void Setup()
        {
            mock = new Mock();
        }

        [TestMethod]
        public void SetValue()
        {
            ObjectModifier.SetValue(mock, "Property", "Property");
            ObjectModifier.SetValue(mock, "Numeric", 10);
            ObjectModifier.SetValue(mock, "Nullable", null);
            ObjectModifier.SetValue(mock, "Custom", new Mock());

            Assert.AreEqual(mock.Property, "Property");
            Assert.AreEqual(mock.Numeric, 10);
            Assert.AreEqual(mock.Nullable, null);

            ObjectModifier.SetValue(mock, "Nullable", 10);
            Assert.AreEqual(mock.Nullable.Value, 10);
            ObjectModifier.SetValue(mock, "Numeric", "11");
            Assert.AreEqual(mock.Numeric, 11);
            ObjectModifier.SetValue(mock, "Float", "11,1");
            Assert.AreEqual(mock.Float, 11.1);
        }

        [TestMethod]
        public void GetValue()
        {
            mock.Numeric = 1000;
            mock.Property = "Property";

            Assert.IsTrue("1000" == ObjectModifier.GetValue(mock, "Numeric"));
            Assert.IsTrue("Property" == ObjectModifier.GetValue(mock, "Property"));
        }

        [TestMethod]
        public void TryGetValue_Should_return_false_when_proptery_name_is_missing()
        {
            mock.Numeric = 1000;
            string field = null;

            var result = ObjectModifier.TryGetValue(mock, "Missing", ref field);

            Specify.That(result).Should.BeFalse();
            Specify.That(field).Should.BeNull();
        }

        [TestMethod]
        public void TryGetValue_Should_return_true_when_proptery_name_is_found_and_set_the_value_to_field_parameter()
        {
            mock.Numeric = 1000;
            string field = null;

            var result = ObjectModifier.TryGetValue(mock, "Numeric", ref field);

            Specify.That(result).Should.BeTrue();
            Specify.That(field).Should.BeEqualTo("1000");
        }

        private class Mock
        {
            private string property;
            public virtual string Property
            {
                get { return property; }
                set { property = value; }
            }

            private int numeric;
            public virtual int Numeric
            {
                get { return numeric; }
                set { numeric = value; }
            }

            private int? nullable;
            public virtual int? Nullable
            {
                get { return nullable; }
                set { nullable = value; }
            }

            private double m_float;
            public virtual double Float
            {
                get { return m_float; }
                set { m_float = value; }
            }

            private Mock custom;
            public virtual Mock Custom
            {
                get { return custom; }
                set { custom = value; }
            }
        }
    }
}
