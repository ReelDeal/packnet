﻿using System.Linq;
using System.Net.NetworkInformation;
using System;
using System.Net;
using System.Net.Sockets;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using Testing.Specificity;

namespace CommonTests.Utils
{
    
    [TestClass]
    public class NetworkingTests
    {
        [TestMethod]
        public void TestGetIPEndPointUsingDNSName()
        {
            const int port = 9998;
            var ip = Networking.FindIpEndPoint("packsize.com", port, true);
            Assert.AreEqual(port, ip.Port);
        }

        [TestMethod]
        public void TestGetIPEndPointUsingLocalhost()
        {
            var ip = Networking.FindIpEndPoint("localhost", 9998, true);

            Assert.AreEqual("127.0.0.1", ip.Address.ToString());
            Assert.AreEqual(9998, ip.Port);

        }

        [TestMethod]
        public void TestGetIPEndPointUsingLocalhostPreferLocalFalse()
        {
            var ip = Networking.FindIpEndPoint("localhost", 9998, false);

            Assert.AreEqual("127.0.0.1", ip.Address.ToString());
            Assert.AreEqual(9998, ip.Port);

        }

        [TestMethod]
        public void TestGetIPEndPointUsingLocssalhostPreferLocalFalse()
        {
            var ip = Networking.FindIpEndPoint("192.168.3.137", 9998, false);

            Assert.AreEqual("192.168.3.137", ip.Address.ToString());
            Assert.AreEqual(9998, ip.Port);
        }

        [TestMethod]
        public void ShouldGetPortFromIpAddressWhenAddressContainsPort()
        {
            var ip = Networking.FindIpEndPointFromAddressWithPort("192.168.3.137:9876", false);

            Assert.AreEqual("192.168.3.137", ip.Address.ToString());
            Assert.AreEqual(9876, ip.Port);
        }

        [TestMethod]
        public void ShouldGetPortFromDnsNameWhenDnsNameContainsPort()
        {
            var ip = Networking.FindIpEndPointFromAddressWithPort("localhost:9876", false);

            Specify.That(ip.Address.ToString()).Should.BeEqualTo("127.0.0.1");
            Specify.That(ip.Port).Should.BeEqualTo(9876);
        }

        [TestMethod]
        public void ShouldThrowArgumentExceptionWhenAddressDontSpecifyPort()
        {
            try
            {
                var ip = Networking.FindIpEndPointFromAddressWithPort("localhost", false);
            }
            catch (ArgumentException e)
            {
                Specify.That(e.ParamName).Should.BeEqualTo("ipOrDnsName");
                Specify.That(e.Message.StartsWith("IpOrDnsName is expected in the format address:port.")).Should.BeTrue();
                return;
            }

            Specify.That(false).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldThrowArgumentExceptionWhenAddressHasEmptyPort()
        {
            try
            {
                var ip = Networking.FindIpEndPointFromAddressWithPort("localhost:", false);
            }
            catch (ArgumentException e)
            {
                Specify.That(e.ParamName).Should.BeEqualTo("ipOrDnsName");
                Specify.That(e.Message.StartsWith("Port number must be expressed as an integer.")).Should.BeTrue();
                return;
            }

            Specify.That(false).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldThrowArgumentExceptionWhenAddressIsNotAnInteger()
        {
            try
            {
                var ip = Networking.FindIpEndPointFromAddressWithPort("localhost:a", false);
            }
            catch (ArgumentException e)
            {
                Specify.That(e.ParamName).Should.BeEqualTo("ipOrDnsName");
                Specify.That(e.Message.StartsWith("Port number must be expressed as an integer.")).Should.BeTrue();
                return;
            }

            Specify.That(false).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldReturnIpAddressWhenNoPortSpecified()
        {
            var ipAddress = "192.168.1.1";

            var result = Networking.ParseIpAddress(ipAddress);

            Specify.That(result.Port).Should.BeEqualTo(0);
            Specify.That(result.Address.ToString()).Should.BeEqualTo(ipAddress);
        }

        [TestMethod]
        public void ShouldReturnIpAddressWithPortWhenPortIsSpecified()
        {
            var ipAddress = "192.168.1.1";
            var ipAddressWithPort = ipAddress + ":1234";

            var result = Networking.ParseIpAddress(ipAddressWithPort);

            Specify.That(result.Port).Should.BeEqualTo(1234);
            Specify.That(result.Address.ToString()).Should.BeEqualTo(ipAddress);
        }

        [TestMethod]
        public void ShouldReturnLoopbackIpAddressWithNoPortSpecifiedAndDNSNameIsUsed()
        {
            var ipAddress = "localhost";

            var result = Networking.ParseIpAddress(ipAddress);

            Specify.That(result.Address).Should.BeEqualTo(IPAddress.Loopback);
            Specify.That(result.Port).Should.BeEqualTo(0);
        }



        [TestMethod]
        public void ShouldReturnLoopbackIpAddressWithPortSpecifiedAndDNSNameIsUsed()
        {
            var ipAddress = "localhost:1234";

            var result = Networking.ParseIpAddress(ipAddress);

            Specify.That(result.Address).Should.BeEqualTo(IPAddress.Loopback);
            Specify.That(result.Port).Should.BeEqualTo(1234);
        }


        [TestMethod]
        [ExpectedException(typeof(SocketException))]
        public void RobSmellsFunnyThrowsDNSSocketException()
        {
            var ipAddress = "ROBSMELLSFUNNY";
            Networking.ParseIpAddress(ipAddress);
        }

        [TestMethod]
        public void IsIpAddressReachableReturnsTrueForLocalhost()
        {
            string ipAddress = "127.0.0.1";
            bool result = Networking.IsIpAddressReachable(ipAddress);
            Specify.That(result).Should.BeTrue();
        }

        [TestMethod]
        [Ignore]
        public void IsIpAddressReachableReturnsTrueForDefaultGatewayOfThisDevice()
        {
            NetworkInterface card = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault();
            Specify.That(card).Should.Not.BeNull();
            GatewayIPAddressInformation ipAddress = card.GetIPProperties().GatewayAddresses.FirstOrDefault();
            Specify.That(ipAddress).Should.Not.BeNull();
            bool result = Networking.IsIpAddressReachable(ipAddress.ToString());
            Specify.That(result).Should.BeFalse();
            
        }

        [TestMethod]
        public void IsIpAddressReachableReturnsFalseForBunkString()
        {
            string ipAddress = "192.168.1.500";
            bool result = Networking.IsIpAddressReachable(ipAddress);
            Specify.That(result).Should.BeFalse();

            ipAddress = "192.500.1.1";
            result = Networking.IsIpAddressReachable(ipAddress);
            Specify.That(result).Should.BeFalse();
        }
    }
}
