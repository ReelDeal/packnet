﻿namespace CommonTests.Utils.ExtentionMethodTests
{
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.ExtensionMethods;

    using Testing.Specificity;

    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        public void ReplaceLastHandlesValueNotFound()
        {
            var testString = "MyTestStringThatDoesn'tContainTheSearchValue";
            var result = testString.ReplaceLast("IDon'tExistInString", "We Need More Lemon Pledge");
            Specify.That(result).Should.BeEqualTo(testString);
        }

        [TestMethod]
        public void ReplaceLastOnlyReplacesLastValue()
        {
            var testString = "1.. 3.. 4... 4 ones... NoooNooo";
            var result = testString.ReplaceLast("Nooo", "We Need More Lemon Pledge");
            Specify.That(result).Should.BeEqualTo("1.. 3.. 4... 4 ones... NoooWe Need More Lemon Pledge");
        }

        [TestMethod]
        public void ReplaceLastReplacesLastValue()
        {
            var testString = "1.. 3.. 4... 4 ones...";
            var result = testString.ReplaceLast("4", "3");
            Specify.That(result).Should.BeEqualTo("1.. 3.. 4... 3 ones...");
        }

        [TestMethod]
        public void ShouldToStreamGivenString()
        {
            var testString = "Here is a string to change to a stream";
            var stream = testString.ToStream();
            var streamReader = new StreamReader(stream);
            Specify.That(testString).Should.BeEqualTo(streamReader.ReadToEnd());
            stream.Dispose();
        }
    }
}
