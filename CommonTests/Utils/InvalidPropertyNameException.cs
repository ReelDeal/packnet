﻿namespace CommonTests.Utils
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Utils;

    [TestClass]
    public class InvalidPropertyNameExceptionTest
    {
        [TestMethod]
        public void InvalidPropertyName()
        {
            InvalidPropertyNameException exception = new InvalidPropertyNameException(new object(), "Name");

            Assert.IsNotNull(exception.Holder);
            Assert.AreEqual("Name", exception.PropertyName);
        }
    }
}
