﻿namespace CommonTests.Utils.JobObjectsTests
{
    using System;
    using System.Diagnostics;
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Utils;
    using PackNet.Common.Utils.JobObjects;

    using Testing.Specificity;

    [TestClass]
    [Ignore] //This fails on CI. All we are doing is testing job objects so we can run this by hand when the code changes
    public class JobObjectsTests
    {
        [TestMethod]
        public void JobTerminatesAllProcessesWhenClosed()
        {
            var job = new Job();
            var calcPath = Path.Combine(Environment.SystemDirectory, "calc.exe");
            var process1 = Process.Start(calcPath);
            var process2 = Process.Start(calcPath);

            try
            {
                Specify.That(process1.HasExited).Should.BeFalse();
                Specify.That(process2.HasExited).Should.BeFalse();
                Specify.That(job.AddProcess(process1)).Should.BeTrue();
                Specify.That(job.AddProcess(process2)).Should.BeTrue();

                job.Close();

                Retry.For(() => process1.HasExited, TimeSpan.FromSeconds(5));
                Retry.For(() => process2.HasExited, TimeSpan.FromSeconds(5));
                Specify.That(process1.HasExited).Should.BeTrue();
                Specify.That(process2.HasExited).Should.BeTrue();
            }
            finally
            {
                process1.Close();
                process2.Close();
            }
        }

        [TestMethod]
        [Ignore] //This fails on CI. All we are doing is testing job objects so we can run this by hand when the code changes
        public void JobTerminatesAllProcessesWhenDisposed()
        {
            Process process1;
            Process process2;
            using (var job = new Job())
            {
                var calcPath = Path.Combine(Environment.SystemDirectory, "calc.exe");
                process1 = Process.Start(calcPath);
                process2 = Process.Start(calcPath);

                Specify.That(job.AddProcess(process1)).Should.BeTrue();
                Specify.That(job.AddProcess(process2)).Should.BeTrue();
                Specify.That(process1.HasExited).Should.BeFalse();
                Specify.That(process2.HasExited).Should.BeFalse();
            }

            try
            {
                Retry.For(() => process1.HasExited, TimeSpan.FromSeconds(5));
                Retry.For(() => process2.HasExited, TimeSpan.FromSeconds(5));
                Specify.That(process1.HasExited).Should.BeTrue();
                Specify.That(process2.HasExited).Should.BeTrue();
            }
            finally
            {
                process1.Close();
                process2.Close();
            }
        }
    }
}
