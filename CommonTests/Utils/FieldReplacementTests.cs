﻿namespace CommonTests.Utils
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Utils;

    [TestClass]
    public class FieldReplacementTests
    {
        [TestMethod]
        public void FiledPatternMatch()
        {
            var testData = new { Title = "Title", DesignId = 2 };
            var result = FieldReplacement.RegexReplaceObjectFields(testData, "Title=[Title][DesignId]", @"[\[][a-zA-Z0-9_-]+[\]]");
            Assert.AreEqual(result, "Title=Title2");
        }
    }
}
