﻿namespace CommonTests.Utils.LocalizationUtilsTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.ExtensionMethods;
    using PackNet.Common.Interfaces.Utils.LocalizationUtils;

    using Testing.Specificity;

    [TestClass]
    public class LocalizableAttributeTests
    {
        //Example usage
        public enum TestEnum
        {
            [Localizable] //Looks up string in resource file TestEnum_Value1
            Value1 = 1,
            [Localizable(DefaultValue = "Some default value for Value2")] // no string in resource but default value set
            Value2 = 2,
            [Localizable] // no string in resource
            Value3 = 3,
            Value4 = 4,
            [Localizable(ResourceNameHint = "commontests.utils.teststrings")]
            Value5,
            [Localizable(ResourceKeyName = "MyLocalizedString")]
            Value6
        }
        
        [TestMethod]
        public void EnumLooksUpLocalizedStringFromGivenResourceFile()
        {
            var localizedString = TestEnum.Value1.GetLocalizedString(TestStrings.ResourceManager);
            Specify.That(localizedString).Should.BeEqualTo("Value One");
        }

        [TestMethod]
        public void EnumLooksUpLocalizedStringFromResourceFile()
        {
            var localizedString = TestEnum.Value1.GetLocalizedString();
            Specify.That(localizedString).Should.BeEqualTo("Value One");
        }

        [TestMethod]
        public void EnumReturnsDefaultValueWhenResourceNotFound()
        {
            var localizedString = TestEnum.Value2.GetLocalizedString();
            Specify.That(localizedString).Should.BeEqualTo("Some default value for Value2");
        }

        [TestMethod]
        public void EnumReturnsDefaultToStringWhenResourceNotFound()
        {
            var localizedString = TestEnum.Value3.GetLocalizedString();
            Specify.That(localizedString).Should.BeEqualTo("Value3");
        }

        [TestMethod]
        public void EnumReturnsDefaultToStringWhenAttributeNotFound()
        {
            var localizedString = TestEnum.Value4.GetLocalizedString();
            Specify.That(localizedString).Should.BeEqualTo("Value4");
        }

        [TestMethod]
        public void EnumReturnsCorrectStringBasedOnResourceHint()
        {
            var localizedString = TestEnum.Value5.GetLocalizedString();
            Specify.That(localizedString).Should.BeEqualTo("Strings Value 5");
        }

        [TestMethod]
        public void EnumReturnsCorrectStringBasedOnResourceKey()
        {
            var localizedString = TestEnum.Value6.GetLocalizedString();
            Specify.That(localizedString).Should.BeEqualTo("My Localized String");
        }
        
    }
}
