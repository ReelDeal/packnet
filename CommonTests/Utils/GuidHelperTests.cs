﻿using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces;
using PackNet.Common.Utils;

using Testing.Specificity;

namespace CommonTests.Utils
{
    [TestClass]
    public class GuidHelperTests
    {
        [TestMethod]
        public void ConvertBackandForthShouldAlwaysBeConsistent()
        {
            Enumerable.Range(1,900000).ForEach((i) =>
            {
                var expected = Guid.NewGuid();
                var converted = GuidHelpers.ConvertGuidToIntArray(expected);
                var rehidrated = GuidHelpers.ConvertIntArrayToGuid(converted);

                Specify.That(expected).Should.BeEqualTo(rehidrated);
            });
        }
    }
}
