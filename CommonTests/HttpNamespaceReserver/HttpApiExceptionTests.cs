﻿using PackNet.Common.HttpNamespaceReserver.CommandsAndQueries;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonTests.HttpNamespaceReserver
{
    [TestClass]
    public class HttpApiExceptionTests
    {
        [TestMethod]
        public void ShouldSetExpectedErrorMessage()
        {
            var sut = new HttpApiException("FCN", 6);
            Assert.AreEqual(new LocalizationTestHelper().GetErrorMessageForCode6(), sut.Message);
        }

        [TestMethod]
        public void ShouldSetExpectedErrorMessageForNegativeErrorCode()
        {
            var sut = new HttpApiException("FCN", -6);
            Assert.AreEqual( "FCN failed: Exception from HRESULT: 0xFFFFFFFA", sut.Message);
        }
    }
}