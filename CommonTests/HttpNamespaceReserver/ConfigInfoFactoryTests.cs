using PackNet.Common.HttpNamespaceReserver.Structs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonTests.HttpNamespaceReserver
{
    [TestClass]
    public class ConfigInfoFactoryTests
    {
        private readonly string validAccount = new LocalizationTestHelper().GetGuestAccount();
        private const string ValidUrl = "http://+:2222/TheTestStuff";

        [TestMethod]
        public void ShouldSetStringsOnConfigInfoAsExpectedForGuests()
        {
            var res = HttpServiceConfigUrlaclSet.CreateConfigInfo(ValidUrl, validAccount);
            Assert.AreEqual(ValidUrl, res.KeyDesc.PUrlPrefix);

            Assert.AreEqual(@"D:(A;;GX;;;S-1-5-32-546)", res.ParamDesc.PStringSecurityDescriptor);
        }
    }
}