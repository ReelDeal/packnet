﻿using System.Threading;

namespace CommonTests.HttpNamespaceReserver
{
    public class LocalizationTestHelper
    {
        public string GetGuestAccount()
        {
            var culture = Thread.CurrentThread.CurrentUICulture;
            return culture.ThreeLetterISOLanguageName == "swe" ? @"BUILTIN\Gäster" : @"BUILTIN\Guests";
        }

        public string GetErrorMessageForCode6()
        {
            var culture = Thread.CurrentThread.CurrentUICulture;
            const string swedishMessage = "FCN failed: Referensen (handle) är felaktig. (Exception from HRESULT: 0x80070006 (E_HANDLE))";
            const string englishMessage = "FCN failed: The handle is invalid. (Exception from HRESULT: 0x80070006 (E_HANDLE))";
            return culture.ThreeLetterISOLanguageName == "swe" ? swedishMessage : englishMessage;
        }
    }
}