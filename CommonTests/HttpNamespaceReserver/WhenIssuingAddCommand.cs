﻿using System;
using PackNet.Common.HttpNamespaceReserver;
using PackNet.Common.HttpNamespaceReserver.CommandsAndQueries;
using PackNet.Common.HttpNamespaceReserver.Structs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace CommonTests.HttpNamespaceReserver
{
    [TestClass]
    public class WhenIssuingAddCommand
    {
        [TestMethod]
        public void AndNoErrorIsReportedShouldPerformInitAddAndTerminate()
        {
            var parameters = new AddParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);

            sut.Execute();
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.DeleteParams);
            Assert.AreSame(parameters, httpApiFacade.AddParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInInitShouldThrowExpectedExceptionAndNotCallAnyOtherMethods()
        {
            var parameters = new AddParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.InitReturnCode = 1;
            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreEqual(null, httpApiFacade.DeleteParams);
            Assert.AreSame(null, httpApiFacade.AddParams);
            Assert.AreEqual(0, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInAddShouldThrowExpectedExceptionAndTerminate()
        {
            var parameters = new AddParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.AddReturnCode = 1;
            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.DeleteParams);
            Assert.AreSame(parameters, httpApiFacade.AddParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInDeleteCallShouldAddAndTerminateNormally()
        {
            var parameters = new AddParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.DeleteReturnCode = 1;
            sut.Execute();
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.DeleteParams);
            Assert.AreEqual(parameters, httpApiFacade.AddParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInTerminateShouldThrowExpectedException()
        {
            var parameters = new AddParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.TerminateReturnCode = 1;
            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.DeleteParams);
            Assert.AreSame(parameters, httpApiFacade.AddParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }
    }
}