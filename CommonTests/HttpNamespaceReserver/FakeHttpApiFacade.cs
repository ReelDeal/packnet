using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Common.HttpNamespaceReserver;
using PackNet.Common.HttpNamespaceReserver.CommandsAndQueries;

namespace CommonTests.HttpNamespaceReserver
{
    public class FakeHttpApiFacade : IHttpApiFacade
    {
        public FakeHttpApiFacade()
        {
            QueryResults = new List<Tuple<uint, Dictionary<string, string>>>();
        }

        public IList<Tuple<uint, Dictionary<string, string>>> QueryResults { get; private set; }
        public QueryParams QueryParams { get; set; }
        public DeleteParams DeleteParams { get; set; }
        public AddParams AddParams { get; set; }
        public int InitCallCount { get; set; }
        public int TerminateCallCount { get; set; }
        public int DeleteReturnCode { get; set; }
        public int AddReturnCode { get; set; }
        public int InitReturnCode { get; set; }
        public int TerminateReturnCode { get; set; }

        public int DeleteServiceConfigAcl(DeleteParams deleteParams)
        {
            DeleteParams = deleteParams;
            return DeleteReturnCode;
        }

        public int SetServiceConfigAcl(AddParams addParams)
        {
            AddParams = addParams;
            return AddReturnCode;
        }

        public int InitializeHttp()
        {
            InitCallCount++;
            return InitReturnCode;
        }

        public int TerminateHttp()
        {
            TerminateCallCount++;
            return TerminateReturnCode;
        }

        public uint HttpQueryServiceConfig(QueryParams queryParams)
        {
            QueryParams = queryParams;
            if (!QueryResults.Any())
            {
                throw new HttpApiException("", 1);
            }
            var match = QueryResults.First();
            QueryResults = QueryResults.Except(new[] {match}).ToList();
            queryParams.Result = match.Item2;
            return match.Item1;
        }
    }
}