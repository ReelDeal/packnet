﻿using System;
using System.Collections.Generic;
using PackNet.Common.HttpNamespaceReserver;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CommonTests.HttpNamespaceReserver
{
    [TestClass]
    public class HttpNamespaceReserverTests : IHttpCommandFactory
    {
        private QueryParams queryParameters;
        private Dictionary<string, string> reservedNamespaces;
        private NamespaceReserver sut;
        private AddParams addParameters;
        private DeleteParams removeParameters;
        private readonly string validAccount = new LocalizationTestHelper().GetGuestAccount();
        private const string ValidUrl = "http://+:2222/TheTestStuff";

        [TestMethod]
        public void ShouldQueryHttpApiWithExpectedParametersWhenGettingAclEntries()
        {
            reservedNamespaces = new Dictionary<string, string>();
            sut = new NamespaceReserver(this);
            sut.GetAclEntries();
            Assert.AreEqual(IntPtr.Zero, queryParameters.ServiceHandle);
            Assert.AreEqual(2, queryParameters.ConfigId);
            Assert.AreEqual(IntPtr.Zero, queryParameters.InputConfigInfoPointer);
            Assert.AreEqual(IntPtr.Zero, queryParameters.OutputConfigInfoPointer);
            Assert.AreEqual((uint)0, queryParameters.OutputConfigInfoLength);
            Assert.AreEqual(IntPtr.Zero, queryParameters.Overlapped);
            Assert.AreEqual((uint)0, queryParameters.ReturnSize);
        }

        [TestMethod]
        public void ShouldCallHttpApiWithExpectedParametersWhenAdding()
        {
            reservedNamespaces = new Dictionary<string, string>();
            sut = new NamespaceReserver(this);
            sut.ReserveNamespace(ValidUrl, validAccount);
            Assert.AreEqual(IntPtr.Zero, addParameters.ServiceHandle);
            Assert.AreEqual(2, addParameters.ConfigId);
            Assert.AreEqual(ValidUrl, addParameters.ConfigInfo.KeyDesc.PUrlPrefix);
            Assert.AreEqual("D:(A;;GX;;;S-1-5-32-546)", addParameters.ConfigInfo.ParamDesc.PStringSecurityDescriptor);
            Assert.AreEqual(IntPtr.Zero, addParameters.Overlapped);
            Assert.AreEqual(ValidUrl, addParameters.UrlPrefix);
            Assert.AreEqual(validAccount, addParameters.AccountName);
        }

        [TestMethod]
        public void ShouldCallHttpApiWithExpectedParametersWhenRemoving()
        {
            reservedNamespaces = new Dictionary<string, string>();
            sut = new NamespaceReserver(this);
            sut.RemoveNamespaceReservation(ValidUrl, validAccount);
            Assert.AreEqual(IntPtr.Zero, removeParameters.ServiceHandle);
            Assert.AreEqual(2, removeParameters.ConfigId);
            Assert.AreEqual(ValidUrl, removeParameters.ConfigInfo.KeyDesc.PUrlPrefix);
            Assert.AreEqual("D:(A;;GX;;;S-1-5-32-546)", removeParameters.ConfigInfo.ParamDesc.PStringSecurityDescriptor);
            Assert.AreEqual(IntPtr.Zero, removeParameters.Overlapped);
            Assert.AreEqual(ValidUrl, removeParameters.UrlPrefix);
            Assert.AreEqual(validAccount, removeParameters.AccountName);
        }

        [TestMethod]
        public void ShouldReturnResultFromIHttpApiWhenGettingAclEntries()
        {
            reservedNamespaces = new Dictionary<string, string>();
            sut = new NamespaceReserver(this);
            var res = sut.GetAclEntries();
            Assert.AreSame(reservedNamespaces, res);
        }

        [TestMethod]
        public void ShouldCallAddItemOnIHttpApiWhenReservingNamespace()
        {
            reservedNamespaces = new Dictionary<string, string>();
            sut = new NamespaceReserver(this);
            Assert.AreEqual(0, reservedNamespaces.Count);
            sut.ReserveNamespace(ValidUrl, validAccount);
            Assert.AreEqual(1, reservedNamespaces.Count);
        }

        [TestMethod]
        public void ShouldBeAbleToAddAndRemoveEntriesFromAcl()
        {
            reservedNamespaces = new Dictionary<string, string>();
            sut = new NamespaceReserver(this);
            var before = sut.GetAclEntries().Count;
            sut.ReserveNamespace(ValidUrl, validAccount);
            var afterAdd = sut.GetAclEntries().Count;
            Assert.AreEqual(1, afterAdd - before);
            sut.RemoveNamespaceReservation(ValidUrl, validAccount);
            var afterRemove = sut.GetAclEntries().Count;
            Assert.AreEqual(1, afterAdd - afterRemove);
        }

        public ICommand GetCommand(QueryParams queryParams)
        {
            return new ActionCommand(() =>
                                  {
                                      queryParameters = queryParams;
                                      queryParameters.Result = reservedNamespaces;
                                  });

        }

        public ICommand GetCommand(AddParams parameters)
        {
            return new ActionCommand(() =>
                                         { 
                                             reservedNamespaces.Add(parameters.UrlPrefix, parameters.AccountName);
                                             addParameters = parameters;
                                         }
                );
        }

        public ICommand GetCommand(DeleteParams parameters)
        {
            return new ActionCommand(() =>
                                         {
                                             reservedNamespaces.Remove(parameters.UrlPrefix);
                                             removeParameters = parameters;
                                         });
        }
    }

    public class ActionCommand : ICommand
    {
        private readonly Action action;

        public ActionCommand(Action action)
        {
            this.action = action;
        }

        public void Execute()
        {
            action();
        }
    }
}