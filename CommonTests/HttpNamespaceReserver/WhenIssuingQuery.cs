using System;
using System.Collections.Generic;
using PackNet.Common.HttpNamespaceReserver;
using PackNet.Common.HttpNamespaceReserver.CommandsAndQueries;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace CommonTests.HttpNamespaceReserver
{
    [TestClass]
    public class WhenIssuingQuery
    {
        [TestMethod]
        public void AndNoErrorIsReportedShouldPerformInitQueryAndTerminate()
        {
            var parameters = new QueryParams(IntPtr.Zero, 0, IntPtr.Zero, IntPtr.Zero, 0, IntPtr.Zero, 0);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.QueryResults.Add(new Tuple<uint, Dictionary<string, string>>(GetAllReservationsQuery.ErrorInsufficientBuffer, new Dictionary<string, string>()));
            httpApiFacade.QueryResults.Add(new Tuple<uint, Dictionary<string, string>>(GetAllReservationsQuery.ErrorNoMoreItems, new Dictionary<string, string>()));
            httpApiFacade.QueryResults.Add(new Tuple<uint, Dictionary<string, string>>(GetAllReservationsQuery.ErrorNoMoreItems, new Dictionary<string, string>()));

            sut.Execute();
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.QueryParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInInitShouldThrowExceptionAndNotCallAnyOtherMethod()
        {
            var parameters = new QueryParams(IntPtr.Zero, 0, IntPtr.Zero, IntPtr.Zero, 0, IntPtr.Zero, 0);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.InitReturnCode = 1;

            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(null, httpApiFacade.QueryParams);
            Assert.AreEqual(0, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInQueryShouldTerminateAndThrowException()
        {
            var parameters = new QueryParams(IntPtr.Zero, 0, IntPtr.Zero, IntPtr.Zero, 0, IntPtr.Zero, 0);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);

            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.QueryParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInTerminateShouldTerminateAndThrowException()
        {
            var parameters = new QueryParams(IntPtr.Zero, 0, IntPtr.Zero, IntPtr.Zero, 0, IntPtr.Zero, 0);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.TerminateReturnCode = 1;

            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.QueryParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }
    }
}