﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.HttpNamespaceReserver.Structs;

namespace CommonTests.HttpNamespaceReserver
{
    [TestClass]
    public class HttpApiVersionTests
    {
        [TestMethod]
        public void ShouldSetMajorToExpectedValue()
        {
            var sut = new HttpApiVersion(1, 2);
            Assert.AreEqual(1, sut.Major);
        }

        [TestMethod]
        public void ShouldSetMinorToExpectedValue()
        {
            var sut = new HttpApiVersion(1, 2);
            Assert.AreEqual(1, sut.Major);
        }
    }
}