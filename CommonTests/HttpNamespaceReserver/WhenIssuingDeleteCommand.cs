using System;
using PackNet.Common.HttpNamespaceReserver;
using PackNet.Common.HttpNamespaceReserver.CommandsAndQueries;
using PackNet.Common.HttpNamespaceReserver.Structs;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using TestUtils;

namespace CommonTests.HttpNamespaceReserver
{
    [TestClass]
    public class WhenIssuingDeleteCommand
    {
        [TestMethod]
        public void AndNoErrorIsReportedShouldPerformInitDeleteAndTerminate()
        {
            var parameters = new DeleteParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);

            sut.Execute();
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.DeleteParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInInitShouldThrowExceptionAndNotCallAnyOtherMethod()
        {
            var parameters = new DeleteParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.InitReturnCode = 1;

            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreEqual(null, httpApiFacade.DeleteParams);
            Assert.AreEqual(0, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInDeleteShouldTerminateAndThrowException()
        {
            var parameters = new DeleteParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.DeleteReturnCode = 1;

            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.DeleteParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }

        [TestMethod]
        public void AndErrorIsReportedInTerminateShouldTerminateAndThrowException()
        {
            var parameters = new DeleteParams(IntPtr.Zero, 0, new HttpServiceConfigUrlaclSet(), IntPtr.Zero);
            var httpApiFacade = new FakeHttpApiFacade();
            var factory = new CommandFactory(httpApiFacade);
            var sut = factory.GetCommand(parameters);
            httpApiFacade.TerminateReturnCode = 1;

            AssertUtil.Throws<HttpApiException>(sut.Execute);
            Assert.AreEqual(1, httpApiFacade.InitCallCount);
            Assert.AreSame(parameters, httpApiFacade.DeleteParams);
            Assert.AreEqual(1, httpApiFacade.TerminateCallCount);
        }
    }
}