﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.ServiceEventPublisher;

namespace CommonTests.ServiceEventPublisher
{
    using PackNet.Common.Interfaces.Eventing;

    using ServiceEventPublisher = PackNet.Common.ServiceEventPublisher.ServiceEventPublisher;

    [TestClass]
    public class ServiceEventPublisherTests
    {
        private ServiceEventPublisher serviceEventPublisher;
        private Mock<IPublisher> jobDonePublisherMock;
        private Mock<IPublisher> packagingDonePublisherMock;
        private Mock<ILogger> log;

        [TestInitialize]
        public void SetUp()
        {
            var settings = new ServiceEventPublisherSettings();

            jobDonePublisherMock = new Mock<IPublisher>();
            jobDonePublisherMock.Setup(publisher => publisher.Type).Returns(PublisherType.JobDone);

            packagingDonePublisherMock = new Mock<IPublisher>();
            packagingDonePublisherMock.Setup(publisher => publisher.Type).Returns(PublisherType.PackagingDone);
            
            var publishers = new List<IPublisher> { jobDonePublisherMock.Object, packagingDonePublisherMock.Object };

            log = new Mock<ILogger>(MockBehavior.Loose);
            serviceEventPublisher = new ServiceEventPublisher(publishers, log.Object);
        }
                
        [TestMethod]
        public void ShouldPublishJobDoneEvent()
        {
            var testData = new TestDataObject { Number = "666" };
            var eventTime = DateTime.UtcNow;

            serviceEventPublisher.JobDone(testData, eventTime);

            jobDonePublisherMock.Verify(publisher =>
                publisher.Publish(testData, eventTime));
        }

        [TestMethod]
        public void ShouldPublishPackagingDoneEvent()
        {
            var testData = new TestDataObject { Number = "666" };
            var eventTime = DateTime.UtcNow;

            serviceEventPublisher.PackagingDone(testData, eventTime);

            packagingDonePublisherMock.Verify(publisher =>
                publisher.Publish(testData, eventTime));
        }

        [TestMethod]
        public void ShouldLogErrorWhenPackagingDonePublisherThrowsException()
        {
            SetUpForPackagingDoneException(new Exception());
            CheckLog(LogLevel.Warning, "Could not read queue item properties.");
        }

        [TestMethod]
        public void ShouldLogErrorWhenPackagingDonePublisherThrowsNullReferenceException()
        {
            SetUpForPackagingDoneException(new NullReferenceException());
            CheckLog(LogLevel.Warning, "Null reference in publisher");
        }

        [TestMethod]
        public void ShouldLogErrorWhenJobDonePublisherThrowsException()
        {
            SetupForJobDoneException(new Exception());
            CheckLog(LogLevel.Warning, "Could not read queue item properties.");
        }

        private void SetUpForPackagingDoneException(Exception exception)
        {            
            packagingDonePublisherMock.Setup(publisher =>
                publisher.Publish(It.IsAny<object>(), It.IsAny<DateTime>())).
                Throws(exception);

            serviceEventPublisher.PackagingDone(null, DateTime.UtcNow);
        }

        private void SetupForJobDoneException(Exception exception)
        {
            jobDonePublisherMock.Setup(publisher =>
                publisher.Publish(It.IsAny<object>(), It.IsAny<DateTime>())).
                Throws(exception);

            serviceEventPublisher.JobDone(null, DateTime.UtcNow);
        }

        private void CheckLog(LogLevel logLevel, string message)
        {
            log.Verify(l => l.Log(logLevel, message), Times.Once());

        }
    }
}

