﻿namespace CommonTests.ServiceEventPublisher
{
    internal class TestDataObject
    {
        #region Properties

        public string Number { get; set; }

        public string Title { get; set; }

        public string Width { get; set; }

        public string Length { get; set; }

        public string Height { get; set; }

        #endregion
    }
}
