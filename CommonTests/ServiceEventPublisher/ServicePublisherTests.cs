﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Common.ServiceEventPublisher;
using PackNet.Common.WebServiceConsumer;

namespace CommonTests.ServiceEventPublisher
{
    using PackNet.Common.Interfaces.Eventing;

    using Assert = Assert;

    [TestClass]
    public class ServicePublisherTests
    {
        private ServicePublisher publisher;
        private Mock<IEventNotificationConsumer> service;
        private TestDataObject testObject;

        [TestInitialize]
        public void Setup()
        {
            publisher = new ServicePublisher();

            publisher.Enabled = true;
            publisher.Type = PublisherType.JobDone;
            publisher.FieldDelimiter = ";";
            publisher.Fields = "Title;Width;Length;Height";
            publisher.Url = "http://myInvalidAddress/EventNotifcationService.asmx";

            service = new Mock<IEventNotificationConsumer>();

            testObject = new TestDataObject();
            testObject.Length = "500";
            testObject.Width = "300";
            testObject.Height = "400";
            testObject.Number = "Number";
            testObject.Title = "Title";            
        }

        [TestMethod]
        public void ShouldOnlyPublishIfEnabled()
        {            
            SetupServiceMock("Title;300;500;400;");
            publisher.Enabled = false;
            publisher.Publish(testObject, DateTime.UtcNow);

            service.Verify(s => s.SendEventNotification(It.IsAny<string>()), Times.Never());
        }

        [TestMethod]
        public void ShouldPublishIfEnabled()
        {
            DateTime time = DateTime.UtcNow;
            string message = time + ";Title;300;500;400";
            SetupServiceMock(message);
            publisher.Enabled = true;
            publisher.Publish(testObject, time);

            service.Verify(s => s.SendEventNotification(message), Times.Exactly(1));
        }

        [TestMethod]
        public void ShouldCreateInstanceOfConsumerIfServicePropertyIsNull()
        {
            string msg = "";
            try
            {
                publisher.Enabled = true;
                publisher.Publish(testObject, DateTime.UtcNow);
            }
            catch (WebException ex)
            {
                msg = ex.Message;
            }

            Assert.AreEqual("The remote name could not be resolved: 'myinvalidaddress'", msg);
        }

        private void SetupServiceMock(string message)
        {
            service.Setup(s => s.SendEventNotification(message)).Returns(message);
            publisher.Service = service.Object;
        }
    }
}
