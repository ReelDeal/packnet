﻿using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.ExtensionMethods;

using Testing.Specificity;

namespace CommonTests.Producibles
{
    [TestClass]
    public class ProducibleExtensionTests
    {
        [TestMethod]
        public void ShouldFindICartonInWrapper()
        {
            var c2 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var p = new BoxFirstProducible(c2);
            Specify.That(p.FindFirstICarton()).Should.BeEqualTo(c2);

        }

        [TestMethod]
        public void ShouldFindICartonInKit()
        {
            var c2 = new Carton() { DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var l2 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            var k2 = new Kit();
            k2.AddProducible(c2);
            k2.AddProducible(l2);

            Specify.That(k2.FindFirstICarton()).Should.BeEqualTo(c2);
        }


        [TestMethod]
        public void ShouldFindAllICartonInKit()
        {
            var c1 = new Carton() { CustomerUniqueId = "c1", DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var c2 = new Carton() { CustomerUniqueId = "c2", DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var c3 = new Carton() { CustomerUniqueId = "c3", DesignId = 1, Length = 100d, Width = 100d, Height = 100d, CorrugateQuality = 1 };
            var tc1 = new TiledCarton{ CustomerUniqueId = "tc1"};
            var l2 = new Label() { CustomerUniqueId = "1", PrintData = "test" };
            var k2 = new Kit();
            k2.AddProducible(c1);
            k2.AddProducible(tc1);
            k2.AddProducible(c2);
            k2.AddProducible(l2);
            k2.AddProducible(new BoxFirstProducible(c3));
            k2.FindAllICarton().Select(p=>p.CustomerUniqueId).ForEach( Console.WriteLine);
            Specify.That(k2.FindAllICarton().Count()).Should.BeEqualTo(4);
        }
    }
}
