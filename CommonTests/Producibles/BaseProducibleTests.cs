﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;

using Testing.Specificity;

using TestUtils;

namespace CommonTests.Producibles
{
    [TestClass]
    public class BaseProducibleTests
    {
        [TestMethod]
        
        public void DoesAnExceptionCauseOtherObserversToWaitOrFail()
        {
            var sw = Stopwatch.StartNew();
            var logger = new ConsoleLogger();
            var label = new Label();
            var completed = 0;
            label.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

            Task.Factory.StartNew(() => label.ProducibleStatusObservable.Subscribe(status =>
            {
                logger.Log(LogLevel.Info, "t1 at {0}: got the message", sw.ElapsedMilliseconds);
                completed ++;
            }));
            Task.Factory.StartNew(() => label.ProducibleStatusObservable.Subscribe(status =>
            {
                logger.Log(LogLevel.Info, "t2 at {0}: got the message", sw.ElapsedMilliseconds);
                Thread.Sleep(5000);
                completed ++;
            }));
            Task.Factory.StartNew(() => label.ProducibleStatusObservable.Subscribe(status =>
            {
                logger.Log(LogLevel.Info, "t3 at {0}: got the message", sw.ElapsedMilliseconds);
                completed ++;
            }));

            Thread.Sleep(200);
            label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            Retry.UntilTrue(()=> completed == 3, TimeSpan.FromSeconds(1));
            Specify.That(completed).Should.BeEqualTo(3);
        }
    }
   
}
