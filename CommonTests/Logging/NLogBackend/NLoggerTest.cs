﻿namespace CommonTests.Logging.NLogBackend
{
    using System;
    using System.IO;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Logging;
    using PackNet.Common.Logging.NLogBackend;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Utils;

    using Testing.Specificity;

    [TestClass]
    [DeploymentItem("Logging\\NLogBackend\\NLogTest.config", "Logging")]
    public class NLoggerTest
    {
        private readonly string logPath = Environment.CurrentDirectory + @"\log.txt";

        [TestInitialize]
        public void SetUp()
        {
            if (File.Exists(logPath))
            {
                File.Delete(logPath);
            }

            LogManager.LoggingBackend = new NLogBackend("NLoggerTest", Environment.CurrentDirectory + @"\Logging\NLogTest.config");
        }

        [TestMethod]
        public void ShouldLogInfo()
        {
            AssertThatLogWasWritten("info log message", LogLevel.Info, "INFO");
        }

        [TestMethod]
        public void ShouldLogTrace()
        {
            AssertThatLogWasWritten("Trace log message", LogLevel.Trace, "TRACE");
        }

        [TestMethod]
        public void ShouldLogDebug()
        {
            AssertThatLogWasWritten("Debug log message", LogLevel.Debug, "DEBUG");
        }

        [TestMethod]
        public void ShouldLogWarning()
        {
            AssertThatLogWasWritten("Warning log message", LogLevel.Warning, "WARN");
        }

        [TestMethod]
        public void ShouldLogError()
        {
            AssertThatLogWasWritten("Error log message", LogLevel.Error, "ERROR");
        }

        [TestMethod]
        public void ShouldLogFatal()
        {
            AssertThatLogWasWritten("Fatal log message", LogLevel.Fatal, "FATAL");
        }

        [TestMethod]
        public void ShouldRecreateMissingLogFiles()
        {
            var log = LogManager.GetLogForApplication();

            log.Log(LogLevel.Info, "This is a log message");

            Assert.IsTrue(File.Exists(logPath));

            var logFromFile = File.ReadAllLines(logPath);

            Specify.That(logFromFile[0]).Should.Contain("This is a log message", StringComparison.InvariantCulture);

            File.Delete(logPath);

            Assert.IsFalse(File.Exists(logPath));

            log.Log(LogLevel.Info, "MEGAMESSAGE");

            logFromFile = File.ReadAllLines(logPath);

            Specify.That(logFromFile[0]).Should.Not.Contain("This is a log message", StringComparison.InvariantCulture);
            Specify.That(logFromFile[0]).Should.Contain("MEGAMESSAGE", StringComparison.InvariantCulture);
        }

        //[TestMethod]
        //public void ShouldBeAbleToChangeLoggerName()
        //{
        //    var log = LogManager.GetLogForApplication();

        //    log.To(LogLevel.Warning).That("I'm imba").If(() => true);

        //    this.VerifyLoggerName("NLoggerTest", 1);

        //    log.SetLoggerName("NewName");

        //    log.To(LogLevel.Fatal).That("I'm imba").If(() => true);

        //    this.VerifyLoggerName("NewName", 2);
        //}

        private void AssertThatLogWasWritten(string logMessage, PackNet.Common.Interfaces.Logging.LogLevel level, string levelString)
        {
            var log = LogManager.GetLogForApplication();

            log.Log(level, logMessage);

            VerifyLogOutPut(logMessage, levelString);
        }

        private void VerifyLogOutPut(string logMessage, string levelString)
        {
            Retry.For(()=>File.Exists(logPath), TimeSpan.FromSeconds(5));
            Specify.That(File.Exists(logPath)).Should.BeTrue("File '" + logPath+ "' should have been written");
            var logFromFile = File.ReadAllLines(logPath);

            Specify.That(logFromFile.Count()).Should.BeEqualTo(1);
            Specify.That(logFromFile[0]).Should.Contain(logMessage, StringComparison.InvariantCulture);
            Specify.That(logFromFile[0]).Should.Contain(levelString, StringComparison.InvariantCulture);
        }
    }
}
