﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;

using Testing.Specificity;

namespace CommonTests.RestrictionsAndCapabilities
{
    [TestClass]
    public class MustProduceWithOptimalCorrugateResolverTests
    {
        [TestMethod]
        public void ShouldEvaluateQualityWithNoMachinesShouldReturnFalse()
        {
            var resolver = new MustProduceWithOptimalCorrugateResolver();
            var corrugate = new Corrugate
            {
                Alias = "Alias",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = .11,
                Width = 48
            };
            var cartonOnCorrugate = new CartonOnCorrugate(new Carton(), corrugate, string.Empty, null, OrientationEnum.Degree0);
            var restriction = new MustProduceWithOptimalCorrugateRestriction(cartonOnCorrugate);
            var capabilities = new List<ICapability>();
            capabilities.Add(new BasicCapability<Corrugate>(new Corrugate { Quality = 1, Thickness = .11 , Width = 48}));

            var result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeFalse();
        }

        [TestMethod]
        public void ShouldEvaluateQualityWithMachinesButNoMachineMatchShouldReturnFalse()
        {
            var resolver = new MustProduceWithOptimalCorrugateResolver();
            var corrugate = new Corrugate
            {
                Alias = "Alias",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = .11,
                Width = 48
            };
            var cartonOnCorrugate = new CartonOnCorrugate(new Carton(), corrugate, string.Empty, null, OrientationEnum.Degree0);
            cartonOnCorrugate.ProducibleMachines.Add(Guid.NewGuid(), "testMachine");
            var restriction = new MustProduceWithOptimalCorrugateRestriction(cartonOnCorrugate);
            var capabilities = new List<ICapability>();
            capabilities.Add(new BasicCapability<Corrugate>(new Corrugate { Quality = 1, Thickness = .11, Width = 48 }));
            capabilities.Add(new MachineIdCapability(Guid.NewGuid()));

            var result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeFalse();
        }

        [TestMethod]
        public void ShouldEvaluateQualityWithMachinesAndAMachineMatchShouldReturnTrue()
        {
            var resolver = new MustProduceWithOptimalCorrugateResolver();
            var corrugate = new Corrugate
            {
                Alias = "Alias",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = .11,
                Width = 48
            };
            var cartonOnCorrugate = new CartonOnCorrugate(new Carton(), corrugate, string.Empty, null, OrientationEnum.Degree0);
            Guid machineGuid = Guid.NewGuid();
            cartonOnCorrugate.ProducibleMachines.Add(machineGuid, "testMachine");
            var restriction = new MustProduceWithOptimalCorrugateRestriction(cartonOnCorrugate);
            var capabilities = new List<ICapability>();
            capabilities.Add(new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineGuid, new List<Corrugate> { corrugate })));
            capabilities.Add(new MachineIdCapability(machineGuid));

            var result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldConsiderWhichMachineTheOptimalCorrugateIsLoadedOn()
        {
            var resolver = new MustProduceWithOptimalCorrugateResolver();

            var c1 = new Corrugate
            {
                Alias = "Corrugate 30",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = .11,
                Width = 30
            };
            var c2 = new Corrugate
            {
                Alias = "Corrugate 18",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = .11,
                Width = 18
            };
            var cartonOnCorrugate = new CartonOnCorrugate(new Carton(), c1, string.Empty, null, OrientationEnum.Degree90);
            Guid fusionGuid = Guid.NewGuid();
            Guid emGuid = Guid.NewGuid();

            cartonOnCorrugate.ProducibleMachines.Add(emGuid, "EM Machine");
            var restriction = new MustProduceWithOptimalCorrugateRestriction(cartonOnCorrugate);
            
            var capbilitiesWhen30LoadedOnFusion = new List<ICapability>();
            capbilitiesWhen30LoadedOnFusion.Add(new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(fusionGuid, new List<Corrugate> { c1 })));
            capbilitiesWhen30LoadedOnFusion.Add(new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(emGuid, new List<Corrugate> { c2 })));
            capbilitiesWhen30LoadedOnFusion.Add(new MachineIdCapability(fusionGuid));
            capbilitiesWhen30LoadedOnFusion.Add(new MachineIdCapability(emGuid));

            var capbilitiesWhen30LoadedOnEM = new List<ICapability>();
            capbilitiesWhen30LoadedOnEM.Add(new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(fusionGuid, new List<Corrugate> { c2 })));
            capbilitiesWhen30LoadedOnEM.Add(new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(emGuid, new List<Corrugate> { c1 })));
            capbilitiesWhen30LoadedOnEM.Add(new MachineIdCapability(fusionGuid));
            capbilitiesWhen30LoadedOnEM.Add(new MachineIdCapability(emGuid));
            
            var fusionResult = resolver.Resolve(restriction, capbilitiesWhen30LoadedOnFusion);
            Specify.That(fusionResult).Should.BeFalse("Fusion cannot rotate");

            var emResult = resolver.Resolve(restriction, capbilitiesWhen30LoadedOnEM);
            Specify.That(emResult).Should.BeTrue("EM can rotate");
        }
    }
}
