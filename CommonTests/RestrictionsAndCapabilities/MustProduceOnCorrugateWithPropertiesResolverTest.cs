﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using System;
using System.Collections.Generic;
using Testing.Specificity;

namespace CommonTests.RestrictionsAndCapabilities
{
    [TestClass]
    public class MustProduceOnCorrugateWithPropertiesResolverTest
    {
        [TestMethod]
        public void ShouldEvaluateQuality()
        {
            var resolver = new MustProduceOnCorrugateWithPropertiesResolver();

            var restriction = new MustProduceOnCorrugateWithPropertiesRestriction { Quality = 1 };
            var corrugateList = new List<Corrugate> { new Corrugate { Quality = 0 } };
            var corrugatesCapability = new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(Guid.NewGuid(),
                corrugateList));
            var capabilities = new List<ICapability>
            {
                corrugatesCapability
            };
            var result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeFalse();

            corrugateList.Add(new Corrugate { Quality = 1 });

            result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldEvaluateThickness()
        {
            var resolver = new MustProduceOnCorrugateWithPropertiesResolver();

            var restriction = new MustProduceOnCorrugateWithPropertiesRestriction { Thickness = 1 };
            var corrugateList = new List<Corrugate> { new Corrugate { Thickness = 0 } };
            var corrugatesCapability = new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(Guid.NewGuid(),
                corrugateList));
            var capabilities = new List<ICapability>
            {
                corrugatesCapability
            };

            var result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeFalse();

            corrugateList.Add(new Corrugate { Thickness = 1 });
            result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldEvaluateWidth()
        {
            var resolver = new MustProduceOnCorrugateWithPropertiesResolver();

            var restriction = new MustProduceOnCorrugateWithPropertiesRestriction { Width = 1 };
            var corrugateList = new List<Corrugate> { new Corrugate { Width = 0 } };
            var corrugatesCapability = new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(Guid.NewGuid(),
                corrugateList));
            var capabilities = new List<ICapability>
            {
                corrugatesCapability
            };
            var result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeFalse();

            corrugateList.Add(new Corrugate { Width = 1 });
            result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldEvaluateWidthAndQuality()
        {
            var resolver = new MustProduceOnCorrugateWithPropertiesResolver();

            var restriction = new MustProduceOnCorrugateWithPropertiesRestriction { Width = 1, Quality = 22 };
            var corrugateList = new List<Corrugate> { new Corrugate { Width = 0 } };
            var corrugatesCapability = new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(Guid.NewGuid(),
                corrugateList));
            var capabilities = new List<ICapability>
            {
                corrugatesCapability
            };
            var result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeFalse();

            corrugateList.Add(new Corrugate { Width = 1 });
            result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeFalse();

            corrugateList.Add(new Corrugate { Width = 1, Quality = 22});
            result = resolver.Resolve(restriction, capabilities);
            Specify.That(result).Should.BeTrue();
        }
    }
}
