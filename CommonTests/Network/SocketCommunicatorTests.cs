﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Network;

using Testing.Specificity;

using TestUtils;

namespace CommonTests.Network
{
    using System.Diagnostics;

    using PackNet.Common.Utils;

    [TestClass]
    public class SocketCommunicatorTests
    {
        private SocketCommunicator communicator;
        private int port;
        private TcpListener server;
        private string recievedMessage;
        private readonly Random rand = new Random();
        private const int MaxConnectionRetryCount = 5;
        private int currentConnectionRetryCount;

        [TestInitialize]
        public void Setup()
        {
            try
            {
                port = rand.Next(1024, 65535);
                Trace.WriteLine("test port: " + port);
                server = new TcpListener(new IPEndPoint(IPAddress.Loopback, port));
                server.Start();
            }
            catch
            {
                currentConnectionRetryCount++;
                if(currentConnectionRetryCount < MaxConnectionRetryCount)
                    Setup();
                else
                {
                    throw;
                }
            }
            communicator = new SocketCommunicator(new ConsoleLogger(), new IPEndPoint(IPAddress.Loopback, port));
            ListenForMessage();
        }

        private void ListenForMessage()
        {
            Task.Factory.StartNew(() =>
            {
                var client = server.AcceptTcpClient();
                using (var sr = new StreamReader(client.GetStream()))
                {
                    recievedMessage = sr.ReadToEnd();
                }
            });
        }

        [TestCleanup]
        public void Cleanup()
        {
            communicator.Dispose();
        }

        [TestMethod]
        public void SendTest()
        {
            const string messageToSend = "Hello World";

            communicator.Send(messageToSend);
            communicator.Dispose();

            Retry.For(() => !String.IsNullOrEmpty(recievedMessage), TimeSpan.FromSeconds(2));

            Specify.That(recievedMessage).Should.BeEqualTo(messageToSend);
        }

        [TestMethod]
        public void SendShouldAttemptResendByDefault()
        {
            const string messageToSend = "Hello World";

            server.Stop();

            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1500);
                server.Start();
                ListenForMessage();
            });

            communicator.Send(messageToSend);
            communicator.Dispose();

            Retry.For(() => !String.IsNullOrEmpty(recievedMessage), TimeSpan.FromSeconds(2));

            Specify.That(recievedMessage).Should.BeEqualTo(messageToSend);
        }

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void SendShouldNotAttemptResendWhenOverriden()
        {
            const string messageToSend = "Hello World";

            server.Stop();

            communicator.Send(messageToSend, 0);
            communicator.Dispose();

            Retry.For(() => !String.IsNullOrEmpty(recievedMessage), TimeSpan.FromSeconds(2));
        }
    }
}