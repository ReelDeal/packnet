﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Network;

using Testing.Specificity;

namespace CommonTests.Network
{
    [TestClass]
    [Ignore]//TODO: Refactor these tests so they are deterministic... They fail on the build server
    public class SocketCommunicatorWithHeartbeatTests
    {
        private static int hostPort = 9090;
        private static string hostAddress = "127.0.0.1";
        private static CancellationTokenSource cts = new CancellationTokenSource();

        [ClassInitialize]
        public static void TestInit(TestContext context)
        {
            cts = new CancellationTokenSource();
            Task.Factory.StartNew(Host, cts.Token);
            Thread.Sleep(20);//sleep long enough for the host to get up and running.
        }

        [ClassCleanup]
        public static void TestCleanup()
        {
            cts.Cancel();
        }

        [TestMethod]
        [Ignore]
        public void HeartbeatShouldWorkWithoutEverInterupting()
        {
            var events = 0;
            var watch = new Stopwatch();
            watch.Start();
            var socket = new SocketCommunicatorWithHeartBeat(new IPEndPoint(IPAddress.Parse(hostAddress), hostPort), new Mock<ILogger>().Object, 3000, 3000, 900);
            socket.HeartbeatObservable.Subscribe((b) =>
            {
                events++; Console.WriteLine("isAlive recieved " + b);
            }, (e) => Console.WriteLine("Exception " + e.ToString()), () => Console.WriteLine("Completed"));


            while (events < 3 && watch.ElapsedMilliseconds < 3000)
                ;
            watch.Stop();
            socket.Disconnect();
            socket.Dispose();
            Specify.That(events).Should.BeEqualTo(3);
            Specify.That(watch.ElapsedMilliseconds < 3400).Should.BeTrue();

        }

        [TestMethod]
        public void HeartbeatShouldWorkWithUserDefinedHeartbeatFunction()
        {
            var events = 0;
            var watch = new Stopwatch();
            watch.Start();
            var socket = new SocketCommunicatorWithHeartBeat(new IPEndPoint(IPAddress.Parse(hostAddress), hostPort),new Mock<ILogger>().Object, 3000, 3000, 800, FailHeartbeat);
            socket.HeartbeatObservable.Subscribe((b) =>
            {
                Specify.That(b.Item2.Contains("user defined."));
                events++;
            }, (e) => Console.WriteLine("Exception " + e.ToString()), () => Console.WriteLine("Completed"));

            while (events < 3 && watch.ElapsedMilliseconds < 3100)
                ;
            watch.Stop();
            socket.Disconnect();
            socket.Dispose();
            Specify.That(events).Should.BeEqualTo(3);
            Specify.That(watch.ElapsedMilliseconds < 3200).Should.BeTrue();

        }

        [TestMethod]
        [Ignore]
        public void HeartbeatShouldWorkWithInterupt()
        {
            var events = 0;
            var watch = new Stopwatch();
            watch.Start();
            var socket = new SocketCommunicatorWithHeartBeat(new IPEndPoint(IPAddress.Parse(hostAddress), hostPort), new Mock<ILogger>().Object, 3000, 3000, 900);
            socket.HeartbeatObservable.Subscribe((b) =>
            {
                events++; Console.WriteLine("isAlive received " + b);
            }, (e) => Console.WriteLine("Exception " + e.ToString()), () => Console.WriteLine("Completed"));

            var token = new CancellationTokenSource();
            var task = Task.Factory.StartNew(() =>
            {
                try
                {
                    //WE SHOULD GET ONE HB BEFORE THIS CALL
                    Thread.Sleep(1000);
                    Specify.That(events).Should.BeEqualTo(1);
                    socket.Send("gogogo");

                    //
                    Thread.Sleep(100);
                    socket.Send("gogogo2");

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }

            },token.Token);

            while (events < 3 && watch.ElapsedMilliseconds < 3000)
                ;
            /*
             Expected HB
             * ~900
             * ~2000
             */
            watch.Stop();
            token.Cancel(false);
            task.Dispose();
            socket.Disconnect();
            socket.Dispose();
            Specify.That(events).Should.BeEqualTo(2);
            Specify.That(watch.ElapsedMilliseconds < 3200).Should.BeTrue();
            
        }

        private static void Host()
        {
            TcpListener server = null;
            try
            {
                // Set the TcpListener on port 13000.
                Int32 port = hostPort;
                IPAddress localAddr = IPAddress.Parse(hostAddress);

                // TcpListener server = new TcpListener(port);
                server = new TcpListener(localAddr, port);

                // Start listening for client requests.
                server.Start();

                // Buffer for reading data
                Byte[] bytes = new Byte[256];
                String data = null;

                // Enter the listening loop. 
                while (true)
                {
                    if (cts.IsCancellationRequested)
                        break;
                    Console.Write("Waiting for a connection... ");

                    // Perform a blocking call to accept requests. 
                    // You could also user server.AcceptSocket() here.
                    TcpClient client = server.AcceptTcpClient();

                    Console.WriteLine("Connected!");

                    data = null;

                    // Get a stream object for reading and writing
                    NetworkStream stream = client.GetStream();

                    int i;

                    // Loop to receive all the data sent by the client. 
                    while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                    {
                        if (cts.IsCancellationRequested)
                            break;

                        // Translate data bytes to a ASCII string.
                        data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
                        Console.WriteLine("Received: {0}", data);

                        // Process the data sent by the client.
                        data = data.ToUpper();

                        byte[] msg = System.Text.Encoding.UTF8.GetBytes(data);

                        // Send back a response.
                        stream.Write(msg, 0, msg.Length);
                        Console.WriteLine("Sent: {0}", data);
                    }

                    // Shutdown and end connection
                    client.Close();
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
            finally
            {
                // Stop listening for new clients.
                server.Stop();

            }

        }

        private bool FailHeartbeat(SocketCommunicatorWithHeartBeat arg)
        {
            throw new Exception("user defined.");
        }
    }
}
