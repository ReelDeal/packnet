﻿using System;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Common.Interfaces.Logging;

using Testing.Specificity;

namespace CommonTests.Network
{
    using PackNet.Common.Network;
    using PackNet.Common.Utils;

    [TestClass]
    public class ObservableStreamTests
    {
        private Stream stream;
        private Mock<ILogger> mockLogger;

        [TestInitialize]
        public void Setup()
        {
            stream = new MemoryStream();
            mockLogger = new Mock<ILogger>();
        }

        [TestMethod]
        public void WhenBytesAreWrittenToStream_MessageIsPublished()
        {
            const string message = "Hello World";
            string readMessage = null;

            using (var sw = new StreamWriter(stream))
            {
                sw.Write(message);
                sw.Flush();
                stream.Position = 0;

                var testObservableStream = new TestObservableStream(stream, mockLogger.Object, (x) => { readMessage = x; });
                
                Retry.For(() => !String.IsNullOrWhiteSpace(readMessage), TimeSpan.FromSeconds(5));
                Specify.That(readMessage).Should.BeEqualTo(message);
            }
        }
    }

    public class TestObservableStream : ObservableStream<string>
    {
        private Action<string> messagePublished;

        protected virtual void OnMessagePublished(string obj)
        {
            if (messagePublished != null) 
                messagePublished(obj);
        }

        public TestObservableStream(Stream stream, ILogger logger, Action<string> messagePublished, Encoding encoding = null, int receiveBufferSize = 2048)
            : base(stream, logger, encoding, receiveBufferSize)
        {
            this.messagePublished = messagePublished;
            BeginRead();
        }

        public override void StreamMessageReceived(string message)
        {
            OnMessagePublished(message);
        }
    }
}