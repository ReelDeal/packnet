﻿<!-- 
 Used to setup the serviceinstall on the harvested MachineManagerWindow.exe file
 See http://www.neovolve.com/post/2013/08/12/Deploying-websites-with-WiX.aspx 
 and http://www.chriskonces.com/using-xslt-with-heat-exe-wix-to-create-windows-service-installs/
 -->

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:msxsl="urn:schemas-microsoft-com:xslt"
   exclude-result-prefixes="msxsl"
   xmlns:wix="http://schemas.microsoft.com/wix/2006/wi"
   xmlns:util="http://schemas.microsoft.com/wix/UtilExtension">
  <xsl:output method="xml" indent="yes" />

  <xsl:strip-space elements="*"/>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="wix:Component[wix:File[@Source='$(var.Server.TargetDir)\PackNet.Server.exe']]">

    <xsl:copy>
      <xsl:apply-templates select="node() | @*" />

      <wix:ServiceInstall
          Id="PackNetService"
          Type="ownProcess"
          Vital="yes"
          Name="PackNet.Server"
          DisplayName="Packsize PackNet.Server"
          Description="Packsize PackNet.Server"
          Start="auto"
          Account="LocalSystem"
          ErrorControl="ignore"
          Interactive="no">
            <wix:ServiceDependency Id="RabbitMQ"/>
        
        <util:ServiceConfig
                 FirstFailureActionType="restart"
                 SecondFailureActionType="restart"
                 ThirdFailureActionType="restart"
                 RestartServiceDelayInSeconds="30"
                 ResetPeriodInDays="1" />
        </wix:ServiceInstall>
      

      <wix:ServiceControl Id="StartPackNetService" Start="install" Stop="both" Remove="uninstall" Name="PackNet.Server" Wait="yes"></wix:ServiceControl>

    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>