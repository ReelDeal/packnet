﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.ArticleManagement;
using PackNet.Data.CartonPropertyGroups;
using PackNet.Data.Classifications;
using PackNet.Data.Corrugates;
using PackNet.Data.Machines;
using PackNet.Data.Orders;
using PackNet.Data.PickArea;
using PackNet.Data.PrintMachines;
using PackNet.Data.Producibles;
using PackNet.Data.ProductionGroups;
using PackNet.Data.Settings;
using PackNet.Data.Templates;
using PackNet.Data.UserManagement;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.Repositories;
using PackNet.ScandataService;

using Testing.Specificity;

using ZebraPrinter = PackNet.Common.Interfaces.DTO.Machines.ZebraPrinter;
using PackNet.Common.Interfaces.Producible;
using MachineManagerAutomationTests.Macros;
using MachineManagerAutomationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace MachineManagerAutomationTests
{
    public enum SearchOrdersColumns
    {
        CustomerId = 1,
        RemainingOriginal = 2,
        Status = 3,
        Type = 4,
        Dimensions = 5,
        Design = 6,
        Corrugate = 7,
        Machine = 8
    }

    public enum SearchArticlesColumns
    {
        Name = 1,
        Description = 2
    }

    public enum MachineStatus
    {
        Online,
        Paused,
        Error,
        Empty,
        Offline
    }

    public enum FootpedalPurpose
    {
        Trigger,
        PausePlayMachineGroup
    }

    public static class Databases
    {
        public static readonly string Localization = "Localization";
        public static readonly string BoxFirstWithWasteConveyor = "BoxFirstWithWasteConveyor";
        public static readonly string BoxFirstWithCrossConveyor = "BoxFirstWithCrossConveyor";
    }

    public static class DashboardWidgets
    {
        public static readonly string Classifications = "Classifications";
        public static readonly string CartonPropertyGroups = "Carton Property Groups";
        public static readonly string Machines = "Machines";
    }

    public static class DashboardLayouts
    {
        public static readonly string Row1_1Col_Row2_3Cols = "12/4-4-4";
        public static readonly string Row1_12Col_Row2_2Cols = "12/6-6";
    }

    public static class PrinterTypes
    {
        public static readonly string GX420t = "GX420t";
    }
    public static class JobStatus
    {
        public static readonly string All = "All";
        public static readonly string Closed = "Closed";
        public static readonly string Failed = "Failed";
        public static readonly string InProgress = "In Progress";
        public static readonly string Open = "Open";
        public static readonly string Completed = "Completed";
        public static readonly string NotProducible = "Not Producible";
        public static readonly string FlaggedForReproduction = "Flagged For Reproduction";
        public static readonly string Started = "Started";
        public static readonly string SentToMachine = "Sent To Machine";
        public static readonly string Triggered = "Triggered";
    }

    public static class MGWorkflows
    {
        public static readonly string CreateProducibleOnMachineGroupWithWasteConveyor = "CreateProducibleOnMachineGroupWithWasteConveyor";
        public static readonly string CreateProducibleOnMachineGroupWithConveyor = "CreateProducibleOnMachineGroupWithConveyor";
        public static readonly string CreateProducibleOnMachineGroup = "CreateProducibleOnMachineGroup";
        public static readonly string CreateProducibleOnMachineGroupWithTrigger = "CreateProducibleOnMachineGroupWithTrigger";
    }

    public static class LabelTemplates
    {
        public static readonly string DefaultLicensePlate = "DefaultLicensePlate";
        public static readonly string DoubleBarcode = "DoubleBarcode";
        public static readonly string Template = "Template";
        public static readonly string UPStemplate = "UPStemplate";
    }

    public static class DesignsIds
    {
        public static readonly string IQ02000001INCHES = "02000001";
    }
    public static class Designs
    {
        public static readonly string IQ02000001INCHES = "IQ 0200-0001 INCHES";
        public static readonly string IQ02000003TAPEJOINT = "IQ 0200-0003 TAPE JOINT";
        public static readonly string IQ2010001INCHES = "IQ 201-0001 INCHES";
        public static readonly string TrackOffsetInch = "Track Offset Inch";
        public static readonly string IQ02030001INCHES = "IQ 0203-0001 INCHES";
        public static readonly string IQ02000002MM = "IQ 0200-0002 MM";
    }

    public static class ProductionModes
    {
        public static readonly string BoxFirst = "BoxFirst";
        public static readonly string BoxLast = "BoxLast";
        public static readonly string Orders = "Orders";
        public static readonly string ScanToCreate = "ScanToCreate";
    }

    public static class ProductionModesFriendlyNames
    {
        public static readonly string BoxFirst = "Box First";
        public static readonly string BoxLast = "Box Last";
        public static readonly string Orders = "Orders";
        public static readonly string ScanToCreate = "Scan to Create";
    }

    public static class BarcodeParsingWorkflows
    {
        public static readonly string ParseArticleFromScanData = "ParseArticleFromScanData";
        public static readonly string ParseCartonFromScanData = "ParseCartonFromScanData";
        public static readonly string ParseArticleFromScanDataWithLabel = "ParseArticleFromScanDataWithLabel";
    }

    public static class MachineTypeFriendlyNames
    {
        public static readonly string Em = "EM";
        public static readonly string Fusion = "Fusion";
        public static readonly string ZebraPrinter = "Zebra Printer";
        public static readonly string Scanner = "Network Scanner";
    }

    public static class TestInserts
    {
        public static readonly string VerifyOverlappingTracks = "VerifyOverlappingTracks";
    }

    public enum MachineType { EM, Fusion, ZebraPrinter, Scanner, ExternalSystem, None };

    public enum ConveyorType
    {
        CrossConveyor,
        WasteConveyor
    }

    public enum DatabaseTables
    {
        Articles = 1,
        BarcodeScanners,
        BoxLast,
        BoxFirst,
        ScanToCreate,
        Carton,
        CartonPropertyGroups,
        Corrugates,
        Classifications,
        EmMachines,
        FusionMachines,
        JobOrders,
        MachineGroups,
        PackNetServerSettings,
        PickZones,
        Preference,
        ProductionGroups,
        Templates,
        Users,
        ZebraPrinters,
        Orders
    }

    public static class TestDatabaseManagement
    {
        #region Variables

        #region Article variables
        public static readonly ArticleExt Article111;
        public static readonly ArticleExt Article222;
        public static readonly ArticleExt Article333;
        public static LabelExt Label1;
        public static LabelExt Label_Edit;
        #endregion

        #region Carton variables
        public static readonly Carton Carton1_d2000001_l10w10h10;
        public static readonly Carton Carton2_d2000001_l11w11h11;
        public static readonly Carton Carton3_d2010001_l5w5h5;
        #endregion

        #region Carton Property Group variables
        public static readonly CartonPropertyGroup Cpg1;
        public static readonly CartonPropertyGroup Cpg2;
        #endregion

        #region Corrugate variables
        public static readonly Corrugate Corrugate_w18625t016;
        public static readonly Corrugate Corrugate_w199t011;
        public static readonly Corrugate Corrugate_w22t011;
        public static readonly Corrugate Corrugate_w2755t011;
        public static readonly Corrugate Corrugate_w279t011;
        public static readonly Corrugate Corrugate_w2925t016;
        public static readonly Corrugate Corrugate_w315t011;
        public static readonly Corrugate Corrugate_w37t011;
        public static readonly Corrugate Corrugate_w4724t011;
        #endregion

        #region Machine Group variables
        public static readonly MachineGroupExt Mg6_fm2zp2;

        public static MachineGroupExt Mg1_em6;
        public static MachineGroupExt Mg2_fm1zp1;
        public static MachineGroupExt Mg3_fm1;
        public static MachineGroupExt Mg4_fm2;
        public static MachineGroupExt Mg5_fm1zp1zp2;
        public static MachineGroupExt Mg7_fm2fm3zp3;
        public static MachineGroupExt Mg8_fm2fm3zp2zp3;
        public static MachineGroupExt Mg9_noMachine;
        public static MachineGroupExt Mg12_fm3zp2zp3;
        public static MachineGroupExt Mg14_em7zp1zp2;
        public static MachineGroupExt Mg_Localization;

        #endregion

        #region Constants

        public const string CorrugateNotLoaded = "Not Loaded";

        #endregion

        #region Machine variables
   

        public static readonly FusionMachineExt Fm1;
        public static readonly FusionMachineExt Fm2;
        public static readonly FusionMachineExt Fm3;
        public static readonly EmMachineExt Em6;
        public static readonly EmMachineExt Em7;
        public static readonly ScannerExt Scanner1;

        #endregion

        #region Production Group variables
        public static readonly ProductionGroupExt Pg2_boxfirst_mg14_cpg1cpg2_c1862c2755;
        public static readonly ProductionGroupExt Pg3_boxfirst_mg2mg6_c199c2755c37;
        public static readonly ProductionGroupExt Pg8_boxfirst_Mg5_Cpg1Cpg2_c2755;
        public static readonly ProductionGroupExt Pg10_boxfirst_mg3mg4_c199c2755c37;

        public static readonly ProductionGroupExt Pg11_boxlast_mg10_c279;
        public static readonly ProductionGroupExt Pg13_boxlast_mg2_c199c2755;
        public static readonly ProductionGroupExt Pg23_boxlast_mg2mg8_5Corrs;

        public static readonly ProductionGroupExt Pg1_orders_mg1_c2755;
        public static readonly ProductionGroupExt Pg12_orders_mg2_c199c2755;
        public static readonly ProductionGroupExt Pg16_orders_mg7_c199c2755;
        public static readonly ProductionGroupExt Pg18_orders_Mg1_c2755c4724;
        public static readonly ProductionGroupExt Pg24_order_mg3_c22;

        public static readonly ProductionGroupExt Pg16_stc_mg2_c199c2755;
        public static readonly ProductionGroupExt Pg17_stc_mg7_c199c2755;
        public static readonly ProductionGroupExt Pg16_stc_mg3_c199c2755;

        public static readonly ProductionGroupExt Pg_Localization;

        #endregion

        #region Printer variables
        public static readonly ZebraPrinterExt Zp1;
        public static readonly ZebraPrinterExt Zp2;
        public static readonly ZebraPrinterExt Zp3;
        #endregion

        #region PickZone variables
        public static readonly PickZone PickZone1;
        public static readonly PickZone PickZone2;
        public static readonly PickZone PickZone3;
        public static readonly PickZone PickZone4;
        #endregion

        #region User variables
        public static readonly User QaUser;
        public static readonly User AdminUser;
        #endregion

        #region Classifíation variables
        public static readonly Classification Classification1;
        public static readonly Classification Classification2;
        #endregion

        #region CustomJob variables
        public static readonly CustomJob CustomJob_h5w5l5;
        public static readonly CustomJob CustomJob_h10w10l10;
        public static readonly CustomJob CustomJob_h30w30l30_TAPEJOINT;
        public static readonly CustomJob CustomJob_h30w30l30_TrackOffsetInch;
        #endregion

        #region Accessory Objects
        public static readonly FootPedal FootpedalPurposeTriggerMod2Port3;
        public static readonly FootPedal FootpedalPurposePausePlayMod2Port3;
        public static readonly FootPedal FootpedalPurposeTriggerMod1Port3;
        public static readonly FootPedal FootpedalPurposePausePlayMod1Port2;
        public static readonly ConveyorExt ConveyorCross;
        public static readonly ConveyorExt ConveyorCrossWithPickZone3;
        public static readonly ConveyorExt ConveyorWaste;

        #endregion

        #endregion

        static TestDatabaseManagement()
        {
            #region Carton definitions
            Carton1_d2000001_l10w10h10 = new Carton()
            {
                CustomerUniqueId = "Carton1",
                CustomerDescription = "Carton1 desc",
                Length = 10,
                Width = 10,
                Height = 10,
                DesignId = 2000001,
                CorrugateQuality = 1

            };
            Carton2_d2000001_l11w11h11 = new Carton()
            {
                CustomerUniqueId = "Carton2",
                CustomerDescription = "Carton2 desc",
                Length = 11,
                Width = 11,
                Height = 11,
                DesignId = 2000001,
                CorrugateQuality = 2
            };
            Carton3_d2010001_l5w5h5 = new Carton()
            {
                CustomerUniqueId = "Carton3",
                CustomerDescription = "Carton3 desc",
                Length = 5,
                Width = 5,
                Height = 5,
                DesignId = 2010001,
                CorrugateQuality = 2
            };

            #endregion

            #region Article definitions

            Article111 = new ArticleExt()
            {
                ArticleId = "111",
                Description = "111 Description",
                DesignName = Designs.IQ2010001INCHES,
                Length = 5,
                Width = 5,
                Height = 5,
                CorrugateQuality = 1,
                SubArticles = new List<IProducible>() { Carton1_d2000001_l10w10h10, Carton3_d2010001_l5w5h5 },
            };

            Article222 = new ArticleExt()
            {
                ArticleId = "222",
                Description = "222 Description",
                DesignName = Designs.IQ2010001INCHES,
                Length = 6,
                Width = 6,
                Height = 6,
                CorrugateQuality = 1,
                SubArticles = new List<IProducible>() { Carton1_d2000001_l10w10h10, Carton2_d2000001_l11w11h11 },
            };

            Article333 = new ArticleExt()
            {
                ArticleId = "333",
                Description = "333 Description",
                DesignName = Designs.IQ2010001INCHES,
                Length = 7,
                Width = 7,
                Height = 7,
                CorrugateQuality = 1,
                SubArticles = new List<IProducible>() { Carton1_d2000001_l10w10h10, Carton2_d2000001_l11w11h11 },
            };

            Label1 = new LabelExt
            {
                CustomerUniqueId = "TestLabel",
                CustomerDescription = "Test Description",
                Template = LabelTemplates.Template
            };

            Label_Edit = new LabelExt
            {
                CustomerUniqueId = "EditLabel",
                CustomerDescription = "EditLabelDescription",
                Template = LabelTemplates.DefaultLicensePlate
            };
            #endregion

            #region Carton Property Group definitions
            Cpg1 = new CartonPropertyGroup()
            {
                Alias = "AZ1"
            };

            Cpg2 = new CartonPropertyGroup()
            {
                Alias = "AZ2",
            };
            #endregion

            #region Corrugate definitions
            Corrugate_w18625t016 = new Corrugate()
            {
                Alias = "Corrugate 18.625 0.11 1",
                Thickness = 0.16,
                Width = 18.625,
                Quality = 1
            };
            Corrugate_w199t011 = new Corrugate()
            {
                Alias = "Corrugate 19.9 0.11 1",
                Thickness = 0.11,
                Width = 19.9,
                Quality = 1
            };
            Corrugate_w22t011 = new Corrugate()
            {
                Alias = "Corrugate 22 0.11 1",
                Thickness = 0.11,
                Width = 22,
                Quality = 1
            };
            Corrugate_w2755t011 = new Corrugate()
            {
                Alias = "Corrugate 27.55 0.11 1",
                Thickness = 0.11,
                Width = 27.55,
                Quality = 1
            };
            Corrugate_w279t011 = new Corrugate()
            {
                Alias = "Corrugate 27.9 0.11 1",
                Thickness = 0.11,
                Width = 27.9,
                Quality = 1
            };
            Corrugate_w2925t016 = new Corrugate()
            {
                Alias = "Corrugate 29.25 0.11 1",
                Thickness = 0.16,
                Width = 29.25,
                Quality = 1
            };

            Corrugate_w315t011 = new Corrugate()
            {
                Alias = "Corrugate 31.5 0.11 1",
                Thickness = 0.11,
                Width = 31.5,
                Quality = 1
            };
            Corrugate_w37t011 = new Corrugate()
            {
                Alias = "Corrugate 37 0.11 1",
                Thickness = 0.11,
                Width = 37,
                Quality = 1
            };
            Corrugate_w4724t011 = new Corrugate()
            {
                Alias = "Corrugate 47.24 0.11 1",
                Thickness = 0.11,
                Width = 47.24,
                Quality = 1
            };

            #endregion

            #region PickZone definitions
            PickZone1 = new PickZone()
            {
                Alias = "AZ1",
                Description = "AZ1 Desc"
            };
            PickZone2 = new PickZone()
            {
                Alias = "AZ2",
                Description = "AZ2 Desc"
            };
            PickZone3 = new PickZone()
            {
                Alias = "AZ3",
                Description = "AZ3 Desc"
            };
            PickZone4 = new PickZone()
            {
                Alias = "AZ4",
                Description = "AZ4 Desc"
            };
            #endregion

            #region User definitions
            QaUser = new User()
            {
                UserName = TestSettings.Default.MachineManagerOperatorUserName,
                Password = TestSettings.Default.MachineManagerOperatorPassword
            };
            AdminUser = new User()
            {
                UserName = TestSettings.Default.MachineManagerAdminUserName,
                Password = TestSettings.Default.MachineManagerAdminPassword,
                IsAdmin = true,
                IsAutoLogin = false
            };
            #endregion

            #region Classification definitions
            Classification1 = new Classification()
            {
                Alias = "Classification 1",
                Number = "1"
            };
            Classification2 = new Classification()
            {
                Alias = "Classification 2",
                Number = "2"
            };
            #endregion

            #region CustomJob definitions
            CustomJob_h5w5l5 = new CustomJob()
            {
                OrderNumber = "CustomJob h5w5l5",
                DesignAlias = Designs.IQ02000001INCHES,
                Width = 5,
                Height = 5,
                Length = 5,
                Quantity = 1,
                CorrugateQuality = 1
            };
            CustomJob_h10w10l10 = new CustomJob()
            {
                OrderNumber = "CustomJob h10w10l10",
                DesignAlias = Designs.IQ02000001INCHES,
                Width = 10,
                Height = 10,
                Length = 10,
                Quantity = 1,
                CorrugateQuality = 1
            };
            CustomJob_h30w30l30_TAPEJOINT = new CustomJob()
            {
                OrderNumber = "CustomJob h30w30l30",
                DesignAlias = Designs.IQ02000003TAPEJOINT,
                Width = 30,
                Height = 30,
                Length = 30,
                Quantity = 1,
                CorrugateQuality = 1
            };
            CustomJob_h30w30l30_TrackOffsetInch = new CustomJob()
            {
                OrderNumber = "CustomJob h30w30l30",
                DesignAlias = Designs.TrackOffsetInch,
                Width = 30,
                Height = 30,
                Length = 30,
                Quantity = 1,
                CorrugateQuality = 1
            };
            #endregion

            #region Machine definitions
            Fm1 = new FusionMachineExt()
            {
                Alias = "FM1",
                AssignedOperator = "",
                Description = "FM1 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 8081,
                NumTracks = 2,
                PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\machineTheoretical_inches.xml",
            };

            Fm2 = new FusionMachineExt()
            {
                Alias = "FM2",
                AssignedOperator = "",
                Description = "FM2 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 8082,
                NumTracks = 2,
                PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\machineTheoretical_inches.xml",

            };

            Fm3 = new FusionMachineExt()
            {
                Alias = "FM3",
                AssignedOperator = "",
                Description = "FM3 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 8083,
                PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\machineTheoretical_inches.xml",
                NumTracks = 2
            };

            Em6 = new EmMachineExt()
            {
                Alias = "EM6",
                AssignedOperator = "",
                Description = "EM6 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 8085,
                NumTracks = 2,
                PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\Em6-50PhysicalMachineSettings_inches.cfg.xml",
            };

            Em7 = new EmMachineExt()
            {
                Alias = "EM7",
                AssignedOperator = "",
                Description = "EM7 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 8086,
                NumTracks = 2,
                PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\Em7PhysicalMachineSettings_inches.cfg.xml"
            };

            Scanner1 = new ScannerExt()
            {
                Alias = "Scanner1",
                Description = "Scanner1 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 8090
            };

            #endregion

            #region Printer definitions
            Zp1 = new ZebraPrinterExt()
            {
                Alias = "ZP1",
                Description = "ZP1 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 9101,
                IsPriorityPrinter = false,
            };

            Zp2 = new ZebraPrinterExt()
            {
                Alias = "ZP2",
                Description = "ZP2 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 9102,
                IsPriorityPrinter = true
            };

            Zp3 = new ZebraPrinterExt()
            {
                Alias = "ZP3",
                Description = "ZP3 desc",
                IpOrDnsName = "127.0.0.1",
                Port = 9103,
                IsPriorityPrinter = false
            };
            #endregion

            #region Accessories

            FootpedalPurposeTriggerMod2Port3 = new FootPedal()
            {
                Alias = "FP1",
                Description = "FP1 desc",
                Module = 2,
                Port = 3,
                Purpose = FootpedalPurpose.Trigger
            };

            FootpedalPurposeTriggerMod1Port3 = new FootPedal()
            {
                Alias = "FP3",
                Description = "FP3 desc",
                Module = 1,
                Port = 3,
                Purpose = FootpedalPurpose.Trigger
            };

            FootpedalPurposePausePlayMod2Port3 = new FootPedal()
            {
                Alias = "FP2",
                Description = "FP2 desc",
                Module = 2,
                Port = 3,
                Purpose = FootpedalPurpose.PausePlayMachineGroup
            };

            FootpedalPurposePausePlayMod1Port2 = new FootPedal()
            {
                Alias = "FP4",
                Description = "FP4 desc",
                Module = 1,
                Port = 2,
                Purpose = FootpedalPurpose.PausePlayMachineGroup
            };

            ConveyorCross = new ConveyorExt()
            {
                Alias = "Cross Conveyor",
                Description = "Cross Conveyor Description",
                Port = 1,
                Module = 1,
                PickZone1 = PickZone1,
                Port2 = 1,
                Module2 = 2,
                PickZone2 = PickZone2,
                Type = ConveyorType.CrossConveyor
            };
            ConveyorCrossWithPickZone3 = new ConveyorExt()
            {
                Alias = "Cross Conveyor",
                Description = "Cross Conveyor Description",
                Port = 1,
                Module = 1,
                PickZone1 = PickZone3,
                Type = ConveyorType.CrossConveyor
            };

            ConveyorWaste = new ConveyorExt()
            {
                Alias = "Conveyor 1",
                Description = "Conveyor 1 Description",
                Port = 1,
                Module = 1,
                Type = ConveyorType.WasteConveyor
            };

            #endregion

            #region Machine Group definitions
            Mg6_fm2zp2 = new MachineGroupExt()
            {
                Alias = "MG6",
                Description = "MG6 desc1",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm2, Zp2 }
            };

            Mg1_em6 = new MachineGroupExt()
            {
                Alias = "MG1",
                Description = "MG1 Desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Em6 }
            };

            Mg2_fm1zp1 = new MachineGroupExt()
            {
                Alias = "MG2",
                Description = "MG2 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm1, Zp1 }
            };

            Mg_Localization = new MachineGroupExt()
            {
                Alias = "MG1",
                Description = "MG1 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm1, Zp1 }
            };
            
            Mg3_fm1 = new MachineGroupExt()
            {
                Alias = "MG3",
                Description = "MG3 Desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm1 }
            };

            Mg4_fm2 = new MachineGroupExt()
            {
                Alias = "MG4",
                Description = "MG 4 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm2 }
            };

            Mg5_fm1zp1zp2 = new MachineGroupExt()
            {
                Alias = "MG5",
                Description = "MG5 Desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm1, Zp1, Zp2 }
            };

            Mg7_fm2fm3zp3 = new MachineGroupExt()
            {
                Alias = "MG7",
                Description = "MG7 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm2, Fm3, Zp3 }
            };

            Mg8_fm2fm3zp2zp3 = new MachineGroupExt()
            {
                Alias = "MG8",
                Description = "MG8 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm2, Fm3, Zp2, Zp3 }
            };

            Mg9_noMachine = new MachineGroupExt()
            {
                Alias = "MG9",
                Description = "MG9 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { }
            };

            Mg12_fm3zp2zp3 = new MachineGroupExt()
            {
                Alias = "MG12",
                Description = "MG12 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Fm3, Zp2, Zp3 }
            };

            Mg14_em7zp1zp2 = new MachineGroupExt()
            {
                Alias = "MG8",
                Description = "MG8 desc",
                MaxQueueLength = 2,
                WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroup,
                Machines = new List<IMachine>() { Em7, Zp1, Zp2 }
            };

            #endregion

            #region Production Group definitions
 
            Pg1_orders_mg1_c2755 = new ProductionGroupExt()
            {
                Alias = "PG1",
                Description = "PG1 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.Order,
                MachineGroups = new List<MachineGroupExt>() { Mg1_em6 },
                Corrugates = new List<Corrugate>() { Corrugate_w2755t011, Corrugate_w22t011 },
                Options = new Dictionary<string, string>() { },
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Em6, Corrugate_w2755t011),
                    new MachineTrackConnector(Em6, Corrugate_w22t011, 2, 35)
                }
            };
            

            Pg2_boxfirst_mg14_cpg1cpg2_c1862c2755 = new ProductionGroupExt()
            {
                Alias = "PG2",
                Description = "PG2 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                MachineGroups = new List<MachineGroupExt>() { Mg14_em7zp1zp2 },
                Corrugates = new List<Corrugate>() { Corrugate_w18625t016, Corrugate_w2755t011 },
                CPGs = new List<CartonPropertyGroup>() { Cpg1, Cpg2 },
                Classifications = new List<Classification>() { Classification1, Classification2 },
                PickZones = new List<PickZone>() { PickZone1, PickZone2 },
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Em7, Corrugate_w2755t011),
                    new MachineTrackConnector(Em7, Corrugate_w18625t016, 2),
                }
            };

            Pg3_boxfirst_mg2mg6_c199c2755c37 = new ProductionGroupExt()
            {
                Alias = "PG3",
                Description = "PG3 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                MachineGroups = new List<MachineGroupExt>() { Mg2_fm1zp1, Mg6_fm2zp2 },
                Corrugates = new List<Corrugate>() { Corrugate_w18625t016, Corrugate_w315t011 },
                CPGs = new List<CartonPropertyGroup>() { Cpg1, Cpg2 },
                Classifications = new List<Classification>() { Classification1, Classification2 },
                PickZones = new List<PickZone>() { PickZone1, PickZone2 },
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Fm1, Corrugate_w2755t011),
                    new MachineTrackConnector(Fm1, Corrugate_w18625t016, 2),
                    new MachineTrackConnector(Fm2, Corrugate_w2755t011),
                    new MachineTrackConnector(Fm2, Corrugate_w18625t016, 2),
                }
            };

            Pg8_boxfirst_Mg5_Cpg1Cpg2_c2755 = new ProductionGroupExt()
            {
                Alias = "PG8",
                Description = "PG8 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                MachineGroups = new List<MachineGroupExt>() { Mg5_fm1zp1zp2 },
                Corrugates = new List<Corrugate>() { Corrugate_w18625t016, Corrugate_w2755t011 },
                CPGs = new List<CartonPropertyGroup>() { Cpg1, Cpg2 },
                Classifications = new List<Classification>() { Classification1, Classification2 },
                PickZones = new List<PickZone>() { PickZone1, PickZone2 },
                TrackConnectors = new List<MachineTrackConnector>() { 
                    new MachineTrackConnector(Fm1, Corrugate_w18625t016), new MachineTrackConnector(Fm1, Corrugate_w2755t011, 2) },
                Options = new Dictionary<string, string>() { }
            };

            Pg10_boxfirst_mg3mg4_c199c2755c37 = new ProductionGroupExt()
            {
                Alias = "PG10",
                Description = "PG10 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                Workflow = "Scan to create",
                MachineGroups = new List<MachineGroupExt>() { Mg3_fm1, Mg4_fm2 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w2755t011, Corrugate_w37t011 },
                TrackConnectors = new List<MachineTrackConnector>() { 
                    { new MachineTrackConnector(Fm1, Corrugate_w37t011,2) }, 
                    { new MachineTrackConnector(Fm2, Corrugate_w199t011) }, { new MachineTrackConnector(Fm2, Corrugate_w2755t011,2) } },
                Options = new Dictionary<string, string>() { { "ParseCartonFromScanData", "True" } }
            };

            Pg11_boxlast_mg10_c279 = new ProductionGroupExt()
            {
                Alias = "PG11",
                Description = "PG11 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
                Workflow = "Scan to create",
                MachineGroups = new List<MachineGroupExt>() { Mg9_noMachine },
                Corrugates = new List<Corrugate>() { Corrugate_w279t011 },
                Options = new Dictionary<string, string>() { { "ShowTriggerInterface", "False" } }
            };

            Pg12_orders_mg2_c199c2755 = new ProductionGroupExt()
            {
                Alias = "PG12",
                Description = "PG12 Desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.Order,
                MachineGroups = new List<MachineGroupExt>() { Mg2_fm1zp1 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w2755t011 },
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Fm1, Corrugate_w199t011),
                    new MachineTrackConnector(Fm1, Corrugate_w2755t011, 2),
                }
            };

            Pg13_boxlast_mg2_c199c2755 = new ProductionGroupExt()
            {
                Alias = "PG13",
                Description = "PG13 Desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
                MachineGroups = new List<MachineGroupExt>() { Mg2_fm1zp1 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w2755t011 },
                Options = new Dictionary<string, string>() { { "ShowTriggerInterface", "True" } }
            };

            Pg16_orders_mg7_c199c2755 = new ProductionGroupExt()
            {
                Alias = "PG16",
                Description = "PG16 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.Order,
                MachineGroups = new List<MachineGroupExt>() { Mg7_fm2fm3zp3 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w2755t011 },
                Options = new Dictionary<string, string>() { },
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Fm2, Corrugate_w199t011),
                    new MachineTrackConnector(Fm2, Corrugate_w2755t011, 2),
                    new MachineTrackConnector(Fm3, Corrugate_w199t011),
                    new MachineTrackConnector(Fm3, Corrugate_w2755t011, 2)
                }
            };

            Pg16_stc_mg2_c199c2755 = new ProductionGroupExt()
            {
                Alias = "PG16",
                Description = "PG16 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
                MachineGroups = new List<MachineGroupExt>() { Mg2_fm1zp1 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w2755t011 },
                Workflow = BarcodeParsingWorkflows.ParseCartonFromScanData,
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Fm1, Corrugate_w199t011),
                    new MachineTrackConnector(Fm1, Corrugate_w2755t011, 2),
                }
            };

            Pg16_stc_mg3_c199c2755 = new ProductionGroupExt()
            {
                Alias = "PG16",
                Description = "PG16 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
                MachineGroups = new List<MachineGroupExt>() { Mg3_fm1 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w2755t011 },
                //Options = new Dictionary<string, string>() { { "ParseCartonFromScanData", "True" } },
                Workflow = BarcodeParsingWorkflows.ParseCartonFromScanData,
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Fm1, Corrugate_w199t011),
                    new MachineTrackConnector(Fm1, Corrugate_w2755t011, 2),
                }
            };

            Pg17_stc_mg7_c199c2755 = new ProductionGroupExt()
            {
                Alias = "PG17",
                Description = "PG17 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
                MachineGroups = new List<MachineGroupExt>() { Mg7_fm2fm3zp3 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w2755t011 },
                Workflow = BarcodeParsingWorkflows.ParseArticleFromScanData,
                TrackConnectors = new List<MachineTrackConnector>()
                {
                    new MachineTrackConnector(Fm2, Corrugate_w199t011),
                    new MachineTrackConnector(Fm2, Corrugate_w2755t011, 2),
                    new MachineTrackConnector(Fm3, Corrugate_w199t011),
                    new MachineTrackConnector(Fm3, Corrugate_w2755t011, 2)
                }
            };

            Pg18_orders_Mg1_c2755c4724 = new ProductionGroupExt()
            {
                Alias = "PG17",
                Description = "PG17 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.Order,
                MachineGroups = new List<MachineGroupExt>() { Mg1_em6 },
                Corrugates = new List<Corrugate>() { Corrugate_w2755t011, Corrugate_w4724t011 },
                Options = new Dictionary<string, string>() { },
                TrackConnectors = new List<MachineTrackConnector>() { 
                    { new MachineTrackConnector(Em6, Corrugate_w4724t011,1,3.94) },
                    { new MachineTrackConnector(Em6, Corrugate_w2755t011,2,39.37) }}
            };
     
            Pg23_boxlast_mg2mg8_5Corrs = new ProductionGroupExt()
            {
                Alias = "PG23",
                Description = "PG22 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
                MachineGroups = new List<MachineGroupExt>() { Mg2_fm1zp1, Mg8_fm2fm3zp2zp3 },
                Corrugates = new List<Corrugate>() { Corrugate_w199t011, Corrugate_w22t011, Corrugate_w2755t011, Corrugate_w279t011, Corrugate_w37t011 },
                TrackConnectors = new List<MachineTrackConnector>() { 
                    { new MachineTrackConnector(Fm1, Corrugate_w199t011) }, 
                    { new MachineTrackConnector(Fm1, Corrugate_w2755t011,2) }, 
                    { new MachineTrackConnector(Fm2, Corrugate_w199t011) }, 
                    { new MachineTrackConnector(Fm2, Corrugate_w2755t011,2) },
                    { new MachineTrackConnector(Fm3, Corrugate_w199t011) }, 
                    { new MachineTrackConnector(Fm3, Corrugate_w2755t011,2) } 
                },
                Options = new Dictionary<string, string>() { }
            };

            Pg24_order_mg3_c22 = new ProductionGroupExt()
            {
                Alias = "PG24",
                Description = "PG24 desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.Order,
                MachineGroups = new List<MachineGroupExt>() { Mg3_fm1 },
                Corrugates = new List<Corrugate>() { Corrugate_w22t011 },
                TrackConnectors = new List<MachineTrackConnector>() { 
                    { new MachineTrackConnector(Fm1, Corrugate_w22t011) } 
                },
            };

            Pg_Localization = new ProductionGroupExt()
            {
                Alias = "TestBF",
                Description = "TestBF desc",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                MachineGroups = new List<MachineGroupExt>() { Mg_Localization },
                Corrugates = new List<Corrugate>() { Corrugate_w18625t016, Corrugate_w2925t016 },
                PickZones = new List<PickZone>() { PickZone1, PickZone2 },
                CPGs = new List<CartonPropertyGroup>() { Cpg1, Cpg2},
                TrackConnectors = new List<MachineTrackConnector>() { 
                    { new MachineTrackConnector(Fm1, Corrugate_w18625t016) }, 
                    { new MachineTrackConnector(Fm1, Corrugate_w2925t016,2) }, 
                },
            };


        }

            #endregion

        #region Methods

        public static string GetDropDownFriendlySelectionAlgorithmName(string type)
        {
            if (type == SelectionAlgorithmTypes.BoxFirst.DisplayName)
                return ProductionModesFriendlyNames.BoxFirst;
            if (type == SelectionAlgorithmTypes.BoxLast.DisplayName)
                return ProductionModesFriendlyNames.BoxLast;
            if (type == SelectionAlgorithmTypes.Order.DisplayName)
                return ProductionModesFriendlyNames.Orders;
            if (type == SelectionAlgorithmTypes.ScanToCreate.DisplayName)
                return ProductionModesFriendlyNames.ScanToCreate;

            return type;
        }

        public static void CascadedTruncateOfTableInDatabase(DatabaseTables tableName)
        {
            switch (tableName)
            {
                case DatabaseTables.Users:
                    new UserRepository().DeleteAll();
                    break;
                case DatabaseTables.Articles:
                    new ArticleRepository().DeleteAll();
                    break;
                case DatabaseTables.Preference:
                    new PreferenceRepository().DeleteAll();
                    break;
                case DatabaseTables.Corrugates:
                    new CorrugateRepository().DeleteAll();
                    break;
                case DatabaseTables.Classifications:
                    new ClassificationRepository().DeleteAll();
                    break;
                case DatabaseTables.EmMachines:
                    new EmMachineRepository().DeleteAll();
                    break;
                case DatabaseTables.BarcodeScanners:
                    new NetworkScannerRepository().DeleteAll();
                    break;
                case DatabaseTables.FusionMachines:
                    new FusionMachineRepository().DeleteAll();
                    break;
                case DatabaseTables.ZebraPrinters:
                    new ZebraPrinterRepository().DeleteAll();
                    break;
                case DatabaseTables.MachineGroups:
                    new MachineGroupRepository().DeleteAll();
                    break;
                case DatabaseTables.Orders:
                    new OrdersRepository().DeleteAll();
                    break;
                case DatabaseTables.BoxLast:
                    new BoxLastRepository().DeleteAll();
                    break;
                default:
                    Specify.Failure("invalid database to truncate");
                    break;
            }
        }

        public static void CascadedTruncateAllTablesInDatabase()
        {
            new ArticleRepository().DeleteAll();
            new NetworkScannerRepository().DeleteAll();

            var ur = new UserRepository();
            ur.DeleteAll();
            ur.EnsureDefaultUserExists();

            new PreferenceRepository().DeleteAll();
            new PickZoneRepository().DeleteAll();
            new CorrugateRepository().DeleteAll();
            new FusionMachineRepository().DeleteAll();
            new EmMachineRepository().DeleteAll();
            new MachineGroupRepository().DeleteAll();
            new ProductionGroupRepository().DeleteAll();
            new BoxLastRepository().DeleteAll();
            new ZebraPrinterRepository().DeleteAll();
            new CartonPropertyGroupRepository().DeleteAll();
            new PackNetServerSettingsRepository().DeleteAll();
            new TemplateRepository().DeleteAll();
            new ClassificationRepository().DeleteAll();
            new OrdersRepository().DeleteAll();
        }

        //updates the DB value for the metric
        public static void AddToProducibleImportCount(string value)
        {
            var metricRepository = new MetricsRepository<string>();
            var metric = metricRepository.FindByType(MetricType.ProducibleImportCount);
            //metric.Value = value;
            metric.UpdateValue(value);
            metricRepository.Update(metric);
        }

        //updates the DB value for the metric
        public static void AddToProducibleCreatedCount(string value)
        {
            var metricRepository = new MetricsRepository<string>();
            var metric = metricRepository.FindByType(MetricType.ProducibleCreatedCount);
            //metric.Value = value;
            metric.UpdateValue(value);
            metricRepository.Update(metric);
        }

        //updates the DB value for the metric
        public static void AddToProducibleFailedCount(string value)
        {
            var metricRepository = new MetricsRepository<string>();
            var metric = metricRepository.FindByType(MetricType.ProducibleFailedCount);
            //metric.Value = value;
            metric.UpdateValue(value);
            metricRepository.Update(metric);
        }

        //updates the DB value for the metric, notice that value is converted to Time
        public static void AddToAverageProductionTime(long value)
        {
            var metricRepository = new MetricsRepository<string>();
            var metric = metricRepository.FindByType(MetricType.AverageProductionTime);
            //metric.Value = new TimeSpan(value).ToString("hh\\:mm\\:ss");
            metric.UpdateValue(new TimeSpan(value).ToString("hh\\:mm\\:ss"));
            metricRepository.Update(metric);
        }

        public static void AddUserToDataBase(string username, string password, bool isAutoLogin = false, bool isAdmin = false, Guid? id = null)
        {
            AddUserToDataBase(UserMacros.CreateUserObject(username, password, isAdmin, isAdmin));
        }
        public static void AddUserToDataBase(User user, Guid? id = null)
        {
            if (!id.HasValue)
                id = Guid.NewGuid();
            var userRepo = new UserRepository();
            var newUser = new User
            {
                Id = id.Value,
                UserName = user.UserName,
                Password = user.Password,
                IsAutoLogin = user.IsAutoLogin,
                IsAdmin = user.IsAdmin
            };

            if ((user.IsAutoLogin && user.IsAdmin))
            {
                Assert.Fail("User can't be Admin and have autologin too");
            }
            else
            {
                userRepo.Create(newUser);
            }
        }

        public static void DropDatabase()
        {
            //drop existing db
            var dbDropProcess = new Process
            {
                StartInfo =
                {
                    FileName = Path.Combine(TestSettings.Default.MongoInstallPath, "mongo.exe"),
                    Arguments = "PackNetServer --eval \"db.dropDatabase()\"",
                    RedirectStandardError = false,
                    RedirectStandardOutput = false,
                    UseShellExecute = false,
                    CreateNoWindow = false

                }
            };
            dbDropProcess.Start();
            dbDropProcess.WaitForExit();
        }

        public static void CreateMongoDump(string dbKey)
        {
            var dbPath = GetDbRestorePath(dbKey.ToLower());
            CreateMongoDumpSub(dbPath);
        }
        public static void CreateMongoDump(SelectionAlgorithmTypes algorithmType)
        {
            var dbPath = GetDbRestorePath(algorithmType.DisplayName.ToLower());
            CreateMongoDumpSub(dbPath);
        }
        public static void CreateMongoDumpSub(string dbPath)
        {
            var dbProcess = new Process
            {
                StartInfo =
                {
                    FileName = Path.Combine(TestSettings.Default.MongoInstallPath, "mongodump.exe"),
                    Arguments = "-d PackNetServer -o " + Path.Combine(dbPath, Guid.NewGuid().ToString()),
                    RedirectStandardError = false,
                    RedirectStandardOutput = false,
                    UseShellExecute = false,

                }
            };
            dbProcess.Start();
        }

        public static void RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes algorithmType)
        {
            RestoreLatestMongoDump(algorithmType);
            TestUtilities.RestartPackNetService();
            Playback.Wait(5000);
        }
        public static void RestoreLatestMongoDump(SelectionAlgorithmTypes algorithmType)
        {
            RestoreLatestMongoDumpSub(GetDbRestorePath(algorithmType.DisplayName.ToLower()));
        }

        public static void RestoreLatestMongoDumpAndRestartService(string dbKey)
        {
            RestoreLatestMongoDump(dbKey);
            TestUtilities.RestartPackNetService();
            Playback.Wait(5000);
        }
        public static void RestoreLatestMongoDump(string dbKey)
        {
            RestoreLatestMongoDumpSub(GetDbRestorePath(dbKey.ToLower()));
        }

        private static void RestoreLatestMongoDumpSub(string dbPath)
        {
            //drop existing db
            DropDatabase();

            var oldDirectory = Directory.GetCurrentDirectory();

            try
            {
                var sortedListOfDirectories = Directory.EnumerateDirectories(dbPath)
                .Select(x => new DirectoryInfo(x))
                .OrderByDescending(x => x.LastWriteTime);

                Directory.SetCurrentDirectory(sortedListOfDirectories.First().FullName);

            }
            catch (InvalidOperationException)
            {
                Console.Write("No database backup found, please run the preconfig at the top the the automation test class");
                return;
            }
            catch (DirectoryNotFoundException)
            {
                Console.Write("No database backup found, please run the preconfig at the top the the automation test class");
                return;
            }


            var dbProcess = new Process
            {
                StartInfo =
                {
                    FileName = Path.Combine(TestSettings.Default.MongoInstallPath, "mongorestore.exe"),
                    Arguments = "-d PackNetServer PackNetServer",
                    RedirectStandardError = false,
                    RedirectStandardOutput = false,
                    UseShellExecute = false,
                    CreateNoWindow = false

                }
            };
            dbProcess.Start();
            dbProcess.WaitForExit();

            //var output = dbProcess.StandardOutput;

            //Console.Write(output.ReadToEnd());
            Directory.SetCurrentDirectory(oldDirectory);
        }

        public static string GetDbRestorePath(string key)
        {
            string basePath = TestSettings.Default.DatabaseDump;

            switch (key)
            {
                case "boxlast":
                    return Path.Combine(basePath, ProductionModes.BoxLast);
                case "order":
                    return Path.Combine(basePath, ProductionModes.Orders);
                case "boxfirst":
                    return Path.Combine(basePath, ProductionModes.BoxFirst);
                case "scantocreate":
                    return Path.Combine(basePath, ProductionModes.ScanToCreate);
                case "localization":
                    return Path.Combine(basePath, Databases.Localization);
                default:
                    return Path.Combine(basePath, "Default");
            }
        }


        #endregion
    }

    /// <summary>
    /// Connects Machine and corrugate to be used when automatically select a corrugate for a machine
    /// </summary>
    public class MachineTrackConnector
    {
        public IMachine Machine { get; set; }
        public Corrugate Corrugate { get; set; }
        public int Track { get; set; }
        public double Offset { get; set; }

        public MachineTrackConnector()
        { }

        public MachineTrackConnector(IMachine machine, Corrugate corrugate, int track = 1, double offset = 5)
        {
            Machine = machine;
            Corrugate = corrugate;
            Track = track;
            Offset = offset;
        }
    }

    /// <summary>
    /// Classes extending the base classes for each type to be able to add test specific properties or methods
    /// suitable for the tests
    /// </summary>
    #region Test specific classes extending the "base classes"

    public class MachineGroupExt : MachineGroup
    {
        public List<IMachine> Machines { get; set; }
        public ProductionGroupExt ProductionGroup { get; set; }

        public MachineGroupExt()
            : base()
        {
            Machines = new List<IMachine>();
        }
    }

    public class ProductionGroupExt : ProductionGroup
    {
        public List<MachineGroupExt> MachineGroups { get; set; }
        public List<MachineTrackConnector> TrackConnectors { get; set; }
        public List<Corrugate> Corrugates { get; set; }
        public string Workflow { get; set; }
        public List<CartonPropertyGroup> CPGs { get; set; }
        public List<Classification> Classifications { get; set; }
        public List<PickZone> PickZones { get; set; }
        public void AddAccessoryToMachine(string machineAlias, IAccessory accessory, string workflow = null)
        {
            if (workflow == null)
                workflow = MGWorkflows.CreateProducibleOnMachineGroup;

            IMachine m = GetMachine(machineAlias);
            if(m is IMachineWIthAccessories)
            {
                if (accessory is ConveyorExt)
                    m.MachineGroup.WorkflowPath = MGWorkflows.CreateProducibleOnMachineGroupWithConveyor;
                else
                    m.MachineGroup.WorkflowPath = workflow;

                ((IMachineWIthAccessories)m).Accessories.Add(accessory);

            }
            else
            {
                Specify.Failure("Only machines that can have accessories can be used in method AddAccessoryToMachine");
            }
        }
        public IMachine GetMachine(string alias)
        {
            foreach(var mg in this.MachineGroups)
            {
                foreach(var m in mg.Machines)
                {
                    m.MachineGroup = mg;
                    if (m.MachineAlias.Equals(alias))
                        return m;
                }
            }

            return null;
        }

        public ProductionGroupExt()
        {
            MachineGroups = new List<MachineGroupExt>();
            Corrugates = new List<Corrugate>();
            CPGs = new List<CartonPropertyGroup>();
            Classifications = new List<Classification>();
            PickZones = new List<PickZone>();
        }

        public ProductionGroupExt(SelectionAlgorithmTypes t, string name) : this()
        {
            this.SelectionAlgorithm = t;
            this.Alias = name;
        }

        public List<string> MachineGroupAliases()
        {
            return MachineGroups.Select(m => m.Alias).ToList();
        }
    }

    public class EmMachineExt : EmMachine, IMachine, IMachineWIthAccessories
    {
        public List<IAccessory> Accessories { get; set; }
        public string MachineAlias
        {
            get { return base.Alias; }
            set { base.Alias = value; }
        }
        public string MachineDescription
        {
            get { return base.Description; }
            set { base.Description = value; }
        }
        public MachineGroupExt MachineGroup { get; set; }
        public EmMachineExt()
        {
            Accessories = new List<IAccessory>();
        }
    }

    public interface IMachineWIthAccessories
    {
        List<IAccessory> Accessories { get; set; }
    }
    public class FusionMachineExt : FusionMachine, IMachine, IMachineWIthAccessories
    {
        public List<IAccessory> Accessories { get; set; }
        public string MachineAlias
        {
            get { return base.Alias; }
            set { base.Alias = value; }
        }
        public string MachineDescription
        {
            get { return base.Description; }
            set { base.Description = value; }
        }
        public MachineGroupExt MachineGroup { get; set; }
        public FusionMachineExt()
        {
            Accessories = new List<IAccessory>();
        }
    }

    public class ZebraPrinterExt : ZebraPrinter, IMachine
    {
        public string MachineAlias
        {
            get { return base.Alias; }
            set { base.Alias = value; }
        }
        public string MachineDescription
        {
            get { return base.Description; }
            set { base.Description = value; }
        }
        public MachineGroupExt MachineGroup { get; set; }

        public ZebraPrinterExt()
        {
        }

    }

    public class ScannerExt : BarcodeScannerMachine, IMachine
    {
        public string MachineAlias
        {
            get { return base.Alias; }
            set { base.Alias = value; }
        }
        public string MachineDescription
        {
            get { return base.Description; }
            set { base.Description = value; }
        }
        public MachineGroupExt MachineGroup { get; set; }
    }

    public interface IAccessory
    {
        string Alias { get; set; }
        string Description { get; set; }
        int Module { get; set; }
        int Port { get; set; }
    }

    public class ConveyorExt : IAccessory
    {
        public string Alias { get; set; }
        public string Description { get; set; }
        public int Module { get; set; }
        public int Port { get; set; }
        public PickZone PickZone1 { get; set; }
        public int Module2 { get; set; }
        public int Port2 { get; set; }
        public PickZone PickZone2 { get; set; }
        public ConveyorType Type { get; set; }
    }

    public class FootPedal : IAccessory
    {
        public string Alias { get; set; }
        public string Description { get; set; }
        public int Module { get; set; }
        public int Port { get; set; }
        public FootpedalPurpose Purpose { get; set; }
    }

    public class LabelExt : Label
    {
        public string Alias
        {
            get { return base.CustomerUniqueId; }
            set { base.CustomerUniqueId = value; }
        }

        public string Template { get; set; }

    }

    public class CustomJob
    {
        public string OrderNumber { get; set; }
        public string DesignAlias { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Length { get; set; }
        public int CorrugateQuality { get; set; }
        public int Quantity { get; set; }
    }

    public class ArticleExt : Article
    {
        public ArticleExt(string alias, string description)
        {
            this.ArticleId = alias;
            this.Description = description;
        }
        public List<IProducible> SubArticles { get; set; }

        public ArticleExt()
        {
            SubArticles = new List<IProducible>();
        }
    }

    /// <summary>
    /// Interface for machines and printers to be used for Ext classes
    /// </summary>
    public interface IMachine
    {
        string MachineAlias { get; set; }
        string MachineDescription { get; set; }
        MachineGroupExt MachineGroup { get; set; }
    }

    public class TestStep
    {
        public int TestNumber { get; set; }
        public string Verification { get; set; }
        public string Message { get; set; }

        public TestStep(string verification, int testNumber = 0)
        {
            Verification = verification;
            TestNumber = testNumber;
        }
    }

    public class TestSteps : List<TestStep>
    {
        public bool Exists(string verification)
        {
            return this.Any(z => z.Verification == verification);
        }

        public TestStep Get(string verification)
        {
            return this.Find(z => z.Verification == verification);
        }
    }

    #endregion

}
