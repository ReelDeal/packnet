﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.WebUITests.Repository.Performance;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Data.ScanToCreateProducible;
using PackNet.FusionSim.ViewModels;

using Testing.Specificity;


namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class ScanToQueueOperatorConsoleTests : ExternalApplicationTestBase
    {
        SimulatorUtilities simUtilities = null;

        #region Constructor and initializers
        public ScanToQueueOperatorConsoleTests()
            : base(TestSettings.Default.MachineManagerProcessName, Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe), false, true)
        {
        }

        protected override void BeforeTestInitialize()
        {
            TestDatabaseManagement.DropDatabase();
            TestUtilities.RestartPackNetServer();
            //simUtilities = new SimulatorUtilities();
        }
        protected override void AfterClassCleanup()
        {
            simUtilities.Close();
            base.AfterClassCleanup();

        }
        #endregion


        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void SetupScanToCreateEnviroment()
        {
            ProductionGroupTests.BuildProductionGroup(TestDatabaseManagement.Pg21_stc_mg3_c199c2755);
            ProductionGroupTests.BuildProductionGroup(TestDatabaseManagement.Pg17_stc_mg7_c199c2755, null, null, false);
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>();

            ArticleManagementTests.CreateArticle(TestDatabaseManagement.Article111, adminConsole);
            ArticleManagementTests.CreateArticle(TestDatabaseManagement.Article222, adminConsole);
            ArticleManagementTests.CreateArticle(TestDatabaseManagement.Article333,adminConsole);

            TestDatabaseManagement.CreateMongoDump(SelectionAlgorithmTypes.ScanToCreate);

        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void ScanToCreateLoadPreconfigs()
        {
            //Blank because handled in BeforeTestInitialize
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8115")]
        public void ScanToQueueCartonCanBeReproducedFromOperatorPanel()
        {
            /*
            What are the manual steps?
            1 - Install PackNet.Server 3
            2 - Use Scan To Create Preconfigs
            3 - Using a scanner, the trigger interface, or the trigger test page, trigger a carton. (If inputting by a method other than a scanner, the expected format is "DesignId;Length;Width;Height;Quantity")	
            4 - Unpause machine	
            5 - Put machine in automode	Machine starts production
            6 - Produce all scanned jobs	
            7 - In Completed Jobs, select two and click Reproduce	 Machine starts production
            */
            
            var mg1Guid = TestDatabaseManagement.Mg3_fm1.SetId();
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            
            opConsole.SelectMachineGroup(mg1Guid);

            opConsole.NavigateToPage(OpNavPages.MachineProduction);
 
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            
            Retry.For(() => Specify.That(opConsole.MachineGroupStatus.InnerText.Contains("Paused")).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));

            opConsole.AutoButton.SafeClick();
            TriggerJob(opConsole, TestDatabaseManagement.Article111.ArticleId);
            TriggerJob(opConsole, TestDatabaseManagement.Article222.ArticleId);
            opConsole.PlayButton.SafeClick();
            
            CompleteJobOnSim(opConsole, sim);
            CompleteJobOnSim(opConsole, sim);
            var closedOrdersWidget = new ClosedOrdersWidget();
            var scanToCreateRepository = new ScanToCreateRepository();
            var orderNumbers = scanToCreateRepository.All().AsQueryable().OrderByDescending(a => a.Created);

            foreach (var orderNumber in orderNumbers)
            {
                var button = closedOrdersWidget.GetReproduceButtonForScanToCreate(orderNumber.Id.ToString());
                button.Click();
                Thread.Sleep(2000);
                closedOrdersWidget.ReproduceScanToCreateOKButton.SafeClick();
                Thread.Sleep(2000);
            }
            
            //Assert that machine starts production by checking that sim has a job and that Jobs sent to machine grid has status of ProducibleSentToMachine
            Retry.For(
                () => { Specify.That(opConsole.JobsSentToMachineDiv.InnerText.Contains("ProducibleSentToMachine")).Should.BeTrue("ProducibleSentToMachine not true"); }, TimeSpan.FromSeconds(10)
            );

            CompleteJobOnSim(opConsole, sim);

            Retry.For(
                () => { Specify.That(opConsole.JobsSentToMachineDiv.InnerText.Contains("ProducibleSentToMachine")).Should.BeTrue("ProducibleSentToMachine not true"); }, TimeSpan.FromSeconds(10)
            );
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8116")]
        public void ScanToQueueCartonCanBeReproducedFromOperatorPanelSearch()
        {
            /*
            What are the manual steps?
            1 - Configure .Server for ScanToQueue
            2 - Attach one machine to PG1	
            3 - Using a scanner, the trigger interface, or the trigger test page, trigger a carton. (If inputting by a method other than a scanner, the expected format is "DesignId;Length;Width;Height;Quantity")	
            4 - Unpause machine	
            5 - Put machine in automode	Machine starts production
            6 - Produce all scanned jobs	
            7 - Open the search window from the operator panel	
            8 - Press the search in wave history search	 List is populated with produced jobs
            9 - Mark two jobs for reproduction	 Machine starts production
            */

            var mg1Guid = TestDatabaseManagement.Mg3_fm1.SetId();
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();

            opConsole.SelectMachineGroup(mg1Guid);

            opConsole.NavigateToPage(OpNavPages.MachineProduction);
            
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            Retry.For(() => Specify.That(opConsole.MachineGroupStatus.InnerText.Contains("Paused")).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));
            
            opConsole.AutoButton.SafeClick();
            TriggerJob(opConsole, TestDatabaseManagement.Article111.ArticleId);
            TriggerJob(opConsole, TestDatabaseManagement.Article222.ArticleId);
            opConsole.PlayButton.SafeClick();
            
            CompleteJobOnSim(opConsole, sim);
            CompleteJobOnSim(opConsole, sim);

            //7 - Open the search window from the operator panel	
            opConsole.NavigateToPage(OpNavPages.SearchScanToCreate);
            
            //8 - Press the search in wave history search	 List is populated with produced jobs
            //We are looking for this: <button id="searchButton" class="btn btn-default" ng-click="search()">Search</button> 
            //when we find it we want to click it
            var boxLastSearchWidget = new BoxLastSearchWidget();
            boxLastSearchWidget.SearchButton.SafeClick();
            
            //9 - Mark two jobs for reproduction and send them on their way
            boxLastSearchWidget.ResultTable.SelectRow(0);
            boxLastSearchWidget.ReproduceButton.SafeClick();
            boxLastSearchWidget.ReproduceConfirmButton.SafeClick();
            Thread.Sleep(TimeSpan.FromSeconds(3));

            boxLastSearchWidget.ResultTable.SelectRow(1);
            boxLastSearchWidget.ReproduceButton.Click();
            boxLastSearchWidget.ReproduceConfirmButton.Click();
            Thread.Sleep(TimeSpan.FromSeconds(3));
            opConsole.Close();
            
            opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opConsole.SelectMachineGroup(mg1Guid);

            //Assert that machine starts production by checking that sim has a job and that Jobs sent to machine grid has status of ProducibleSentToMachine
            Retry.For(
                () => { Specify.That(opConsole.JobsSentToMachineDiv.InnerText.Contains("ProducibleSentToMachine")).Should.BeTrue("ProducibleSentToMachine not true"); }, TimeSpan.FromSeconds(10)
            );

            CompleteJobOnSim(opConsole, sim);

            Retry.For(
                () => { Specify.That(opConsole.JobsSentToMachineDiv.InnerText.Contains("ProducibleSentToMachine")).Should.BeTrue("ProducibleSentToMachine not true"); }, TimeSpan.FromSeconds(10)
            );
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8117")]
        public void ScanToQueueCartonCanBeReproducedFromAdminPanelSearch()
        {
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id;
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opConsole.SelectMachineGroup(mg1Guid);
            
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            opConsole.AutoButton.SafeClick();
            TriggerJob(opConsole, TestDatabaseManagement.Article111.ArticleId);
            TriggerJob(opConsole, TestDatabaseManagement.Article222.ArticleId);
            opConsole.PlayButton.SafeClick();
            Thread.Sleep(3000);
            CompleteJobOnSim(opConsole, sim);
            CompleteJobOnSim(opConsole, sim);


            var scanToCreateRepository = new ScanToCreateRepository();
           
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>();

            //7 - Open the search window from the admin panel	
            adminConsole.NavigateToPage(NavPages.SearchScanToCreate);

            //8 - Press the search in wave history search	 List is populated with produced jobs
            //We are looking for this: <button id="searchButton" class="btn btn-default" ng-click="search()">Search</button> 
            //when we find it we want to click it
            var boxLastSearchWidget = new BoxLastSearchWidget();
            boxLastSearchWidget.SearchButton.SafeClick();

            //9 - Mark two jobs for reproduction - Machine starts production so check that
            boxLastSearchWidget.ResultTable.SelectRows(0,1);

            boxLastSearchWidget.ReproduceButton.SafeClick();
            boxLastSearchWidget.ReproduceConfirmButton.SafeClick();
            Thread.Sleep(TimeSpan.FromSeconds(3));
            adminConsole.Close();

            opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opConsole.SelectMachineGroup(mg1Guid);

            //Assert that machine starts production by checking that sim has a job and that Jobs sent to machine grid has status of ProducibleSentToMachine
            Retry.For(
                () => { Specify.That(opConsole.JobsSentToMachineDiv.InnerText.Contains("ProducibleSentToMachine")).Should.BeTrue("ProducibleSentToMachine not true"); }, TimeSpan.FromSeconds(10)
            );

            CompleteJobOnSim(opConsole, sim);

            Retry.For(
                () => { Specify.That(opConsole.JobsSentToMachineDiv.InnerText.Contains("ProducibleSentToMachine")).Should.BeTrue("ProducibleSentToMachine not true"); }, TimeSpan.FromSeconds(10)
            );
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8118")]
        public void TriggerJobFromDimensionalDataUsingOperatorPanelTriggerInterfaceInScanToCreate()
        {
            //Start a sim
            var sim1 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var sim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm2);
            var sim3 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm3);
            
            //Navigate to the operator panel
            var mg2Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg12_fm3zp2zp3.Alias).Id;
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();

            //Click MG2
            opConsole.SelectMachineGroup(mg2Guid);

            //Navigate to Machine production
            opConsole.NavigateToPage(OpNavPages.MachineProduction);

            Retry.For(() => Specify.That(opConsole.MachineGroupStatus.InnerText.Contains("Paused")).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));

            opConsole.AutoButton.SafeClick();
            Thread.Sleep(1000);
            opConsole.PlayButton.SafeClick();
            Thread.Sleep(1000);

            //Using the trigger interface textbox, Enter the following without quotes: "2010001:5:5:5:2" then hit Enter.
            TriggerJob(opConsole, "2010001:5:5:5:2");

            CompleteJobOnSim(opConsole, sim2);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8119")]
        public void ScanToQueueEnableDisableTriggerInterface()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>();

            var pgPageUrl = TestUtilities.GetMachineManagerURL() + "/" + ProductionGroupPage.pageUrl;
            adminConsole.NavigateToUrl(pgPageUrl);

            var pgPage = new ProductionGroupPage();

            //verify that show trigger checkbox is checked
            pgPage.ProductionGroupTableDiv.FindUiGridRowCheckBoxDiv(TestDatabaseManagement.Pg21_stc_mg3_c199c2755.Alias).SafeClick();
            Specify.That(pgPage.EditButton.Enabled).Should.BeTrue("Edit button not enabled");
            pgPage.EditButton.SafeClick();

            Specify.That(pgPage.STCShowTriggerInterfaceCheckbox.Checked).Should.BeTrue("Trigger checkbox should be checked");

            pgPage.SaveButton.SafeClick();

            //verify trigger interface is shown on operator console
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id;
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opConsole.SelectMachineGroup(mg1Guid);
            opConsole.NavigateToPage(OpNavPages.MachineProduction);
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            opConsole.AutoButton.SafeClick();

            Specify.That(opConsole.TriggerInterface.Enabled && opConsole.TriggerInterface.Exists).Should.BeTrue("Trigger Textbox not found");
            Specify.That(opConsole.TriggerInterfaceButton.Enabled && opConsole.TriggerInterfaceButton.Exists).Should.BeTrue("Trigger Button not found");

            //uncheck trigger interface button
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>();
            adminConsole.NavigateToUrl(pgPageUrl);

            pgPage = new ProductionGroupPage();

            //verify that show trigger checkbox is checked
            pgPage.ProductionGroupTableDiv.FindUiGridRowCheckBoxDiv(TestDatabaseManagement.Pg21_stc_mg3_c199c2755.Alias).SafeClick();
            Specify.That(pgPage.EditButton.Enabled).Should.BeTrue("Edit button not enabled");
            pgPage.EditButton.SafeClick();

            pgPage.STCShowTriggerInterfaceCheckbox.UnCheck();
            Specify.That(pgPage.STCShowTriggerInterfaceCheckbox.Checked).Should.BeFalse("Trigger checkbox should be un-checked");

            pgPage.SaveButton.SafeClick();

            
            //verify trigger interface is NOT shown on operator console
            opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opConsole.SelectMachineGroup(mg1Guid);
            opConsole.NavigateToPage(OpNavPages.MachineProduction);
            
            Specify.That(opConsole.TriggerInterface.Enabled && opConsole.TriggerInterface.Exists).Should.BeFalse("Trigger Textbox should not be visible");
            Specify.That(opConsole.TriggerInterfaceButton.Enabled && opConsole.TriggerInterfaceButton.Exists).Should.BeFalse("Trigger Button should not be visible");



        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8121")]
        public void TriggerJobFromArticleUsingOperatorPanelTriggerInterfaceInScanToCreate()
        {
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id;
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opConsole.SelectMachineGroup(mg1Guid);
            opConsole.SelectMachineGroup(mg1Guid);

            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            opConsole.AutoButton.SafeClick();
            TriggerJob(opConsole, "111");

            opConsole.PlayButton.SafeClick();
            Thread.Sleep(3000);
            CompleteJobOnSim(opConsole, sim);
           

            var closedOrdersWidget = new ClosedOrdersWidget();
            var scanToCreateRepository = new ScanToCreateRepository();
            var orderNumber = scanToCreateRepository.All().First().Id.ToString();

            Retry.For(
                () => Specify.That(closedOrdersWidget.IsOrderPresentScanToCreate(orderNumber)).Should.BeTrue("order not found in closed orders"), TimeSpan.FromSeconds(10)
            ); 

            TriggerJob(opConsole, "111:2");

            CompleteJobOnSim(opConsole, sim);
            Thread.Sleep(3000);
            CompleteJobOnSim(opConsole, sim);


            var orderNumbers = scanToCreateRepository.All().AsQueryable().OrderByDescending(a => a.Created);

            foreach (var orderNumber2 in orderNumbers)
            {
                Retry.For(
                    () =>
                    {
                        Specify.That(closedOrdersWidget.IsOrderPresentScanToCreate(orderNumber2.Id.ToString())).Should.BeTrue("order not found in closed orders");
                    }, TimeSpan.FromSeconds(10)
                ); 
            }



        }
        
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8124")]
        public void ShouldGiveErrorMessageInOpWhenPassingInvalidCustomData() //BUG:9298 blocking
        {
            //Start a sim
            var sim1 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            var sim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine2);
            var sim3 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine3);

            //Navigate to the operator panel
            var mg2Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg12_fm3zp2zp3.Alias).Id;
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();

            //Click MG2
            opConsole.SelectMachineGroup(mg2Guid);

            //Navigate to Machine production
            opConsole.NavigateToPage(OpNavPages.MachineProduction);

            //Put MG2 in auto
            opConsole.AutoButton.SafeClick();

            //Using the trigger interface trigger 2000001;10;11;12;1 incorrect delimiter
            TriggerJob(opConsole, "2000001;10;11;12;1");

            //verify notifications
            Retry.For(() => Specify.That(opConsole.NotificationCountBubble.InnerText).Should.BeEqualTo("1"), TimeSpan.FromSeconds(10));
            opConsole.ExpandCollapseNotifications.SafeClick();
            Specify.That(opConsole.GetNotificationMessage(0).InnerText).Should.Contain("Invalid");

            //Repeat step above with incorrect number of fields
            TriggerJob(opConsole, "2000001;10;11;12;1");

            Retry.For(() => Specify.That(opConsole.NotificationCountBubble.InnerText).Should.BeEqualTo("2"), TimeSpan.FromSeconds(10));
            Specify.That(opConsole.GetNotificationMessage(1).InnerText).Should.Contain("Invalid");

            //clear notifications
            opConsole.DismissAllNotificationsButton.SafeClick();

            //Click MG1
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg12_fm3zp2zp3.Alias).Id;
            opConsole.SelectMachineGroup(mg1Guid);

            //Using the trigger interface trigger 111;2 incorrect delimiter
            TriggerJob(opConsole, "111;2");
            Retry.For(() => Specify.That(opConsole.NotificationCountBubble.InnerText).Should.BeEqualTo("1"), TimeSpan.FromSeconds(10));
            opConsole.ExpandCollapseNotifications.SafeClick();
            Specify.That(opConsole.GetNotificationMessage(0).InnerText).Should.Contain("Invalid");
            opConsole.ExpandCollapseNotifications.SafeClick();

            //Using the trigger interface trigger 111;2 incorrect delimiter
            TriggerJob(opConsole, "111:2:2");
            Retry.For(() => Specify.That(opConsole.NotificationCountBubble.InnerText).Should.BeEqualTo("2"), TimeSpan.FromSeconds(10));
            opConsole.ExpandCollapseNotifications.SafeClick();
            Specify.That(opConsole.GetNotificationMessage(1).InnerText).Should.Contain("Invalid");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8126")]
        public void ScannedToQueueJobsArePersistedWhenRestarting() //blocked by sim not getting jobs after restarting server
        {
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id;
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opConsole.SelectMachineGroup(mg1Guid);
            //opPanel.NavigateToMachineProduction();
            opConsole.AutoButton.SafeClick();
            TriggerJob(opConsole, "111");
            
            Thread.Sleep(3000);
            
            var scanToCreateRepository = new ScanToCreateRepository();
            var orderNumber = scanToCreateRepository.All().First();

            TestUtilities.RestartPackNetServer();

            Thread.Sleep(3000);
            sim.StartupComplete();
            opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>(); //create a new op
            opConsole.SelectMachineGroup(mg1Guid);

            Retry.For(
                () => { Specify.That(opConsole.MachineGroupStatus.InnerText.Contains("Paused")).Should.BeTrue(); }, TimeSpan.FromSeconds(10)
            );
            opConsole.AutoButton.SafeClick();
            opConsole.PlayButton.SafeClick();
            

            CompleteJobOnSim(opConsole, sim);
            var closedOrdersWidget = new ClosedOrdersWidget();
            Retry.For(() => Specify.That(closedOrdersWidget.GetScanToCreateDiv(orderNumber.Id.ToString()).Exists).Should.BeTrue("order not found in closed orders"), TimeSpan.FromSeconds(10));
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8129")]
        public void JobsShouldBeProducedByMostEfficientMGinPG()
        {
            //FusionMachine FusionMachine4 = new FusionMachine();
            //FusionMachine FusionMachine5 = new FusionMachine();
            //FusionMachine4 = new FusionMachine()
            //{
            //    Alias = "FM4",
            //    AssignedOperator = "",
            //    Description = "test4",
            //    IpOrDnsName = "127.0.0.1",
            //    Port = 8084,
            //    NumTracks = 2,
            //    PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\machineTheoretical_inches.xml"
            //};
            //FusionMachine5 = new FusionMachine()
            //{
            //    Alias = "FM5",
            //    AssignedOperator = "",
            //    Description = "test5",
            //    IpOrDnsName = "127.0.0.1",
            //    Port = 8085,
            //    NumTracks = 2,
            //    PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\machineTheoretical_inches.xml"
            //};
            //add some more machines and PGs and MGs
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>();

            ProductionGroupTests.BuildProductionGroup(TestDatabaseManagement.Pg10_boxfirst_mg3mg4_c199c2755c37, adminConsole);
            
            //MachinesTests.CreateMachine(TestDatabaseManagement.Fm1, adminConsole);
            //MachinesTests.CreateMachine(TestDatabaseManagement.Fm2, adminConsole);

            //MachineGroupTests.CreateMachineGroup(TestDatabaseManagement.Mg3_fm1);
            //MachineGroupTests.CreateMachineGroup(TestDatabaseManagement.Mg4_fm2);

            //MachineGroupTests.CreateNewMachineGroup("MG4", "", MGWorkflows.CreateProducibleOnMachineGroup, 2, new List<string>() { "FM4" }, adminConsole);
            //MachineGroupTests.CreateNewMachineGroup("MG5", "", MGWorkflows.CreateProducibleOnMachineGroup, 2, new List<string>() { "FM5" }, adminConsole);

            //ProductionGroupTests.CreateProductionGroup(TestDatabaseManagement.Pg10_boxfirst_mg3mg4_c199c2755c37, adminConsole);
            //ProductionGroupTests.CreateNewProductionGroup("PG3", "", TestDatabaseManagement.TestProductionGroupStc1.SelectionAlgorithm.DisplayName, 
            //    new List<string>() { TestDatabaseManagement.Mg3_fm1.Alias, TestDatabaseManagement.Mg4_fm2.Alias }, new List<string>() 
            //    { TestDatabaseManagement.Corrugate_w199t011.Alias, TestDatabaseManagement.Corrugate_w2755t011.Alias, TestDatabaseManagement.Corrugate_w37t011.Alias }, 
            //    "ParseCartonFromScanData", adminConsole: adminConsole);


            //load corrugates on machines
            //var mg3Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg3_fm1.Alias).Id;
            //var trackToCorrugate = new Dictionary<int, string>()
            //{
            //    { 2, TestDatabaseManagement.Corrugate_w37t011.Alias }
            //};
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            //opConsole.SelectMachineGroup(mg3Guid);
            //OperatorConsoleTests.ChangeCorrugates(mg3Guid, 0, trackToCorrugate, opConsole);

            //var mg4Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg4_fm2.Alias).Id;
            //var trackToCorrugate2 = new Dictionary<int, string>()
            //{
            //    { 1, TestDatabaseManagement.Corrugate_w199t011.Alias },
            //    { 2, TestDatabaseManagement.Corrugate_w2755t011.Alias }
            //};
            //OperatorConsole.SelectMachineGroup(mg4Guid, opConsole);
            //OperatorConsoleTests.ChangeCorrugates(mg4Guid, 0, trackToCorrugate2, opConsole);

            //Start a sim
            var sim4 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var sim5 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);

            //Click MG4
            opConsole.SelectMachineGroup(TestDatabaseManagement.Mg3_fm1.SetId());

            //Put MG5 in auto
            opConsole.AutoButton.SafeClick();
            Thread.Sleep(1000);
            opConsole.PlayButton.SafeClick();

            //Using the trigger interface trigger
            TriggerJob(opConsole, "2000001:5:5:5:1");

            //Assert that machine in MG5
            Specify.That(opConsole.JobsSentToMachineDiv.InnerText).Should.BeEmpty();

            //Click MG5
            opConsole.SelectMachineGroup(TestDatabaseManagement.Mg4_fm2.SetId());

            //Put MG5 in auto
            opConsole.AutoButton.SafeClick();
            opConsole.PlayButton.SafeClick();

            CompleteJobOnSim(opConsole, sim5);

        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8130")]
        public void BarcodeScanDoesNotProduceArticleThatIsNotPresentInTheDatabase()
        {
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id;
            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();

            opConsole.SelectMachineGroup(mg1Guid);
            
            opConsole.NavigateToPage(OpNavPages.MachineProduction);

            //Retry.For(() => Specify.That(opPanel.MachineGroupStatus.InnerText.Contains("Paused")).Should.BeTrue("MG never got out of Initializing"),
            //    TimeSpan.FromSeconds(60));

            opConsole.AutoButton.SafeClick();
            opConsole.PlayButton.SafeClick();
            
            TriggerJob(opConsole, "articleNotPresent");
            Thread.Sleep(3000);

            //verify notifications
            Retry.For(() => Specify.That(int.Parse(opConsole.NotificationCountBubble.InnerText) >= 1), TimeSpan.FromSeconds(10));
            opConsole.ExpandCollapseNotifications.SafeClick();
            Specify.That(opConsole.GetNotificationMessage(int.Parse(opConsole.NotificationCountBubble.InnerText)-1).InnerText).Should.Contain("failed");

            //verify that the job is not sent to fusion 
            Retry.For(() => Specify.That(sim.Simulator.Queue.Count == 0).Should.BeTrue("fusion should not have a job in queue"),
                TimeSpan.FromSeconds(10));

            //verify that it is not in the scantocreaterepo
            var scanToCreateRepository = new ScanToCreateRepository();
            
            Retry.For(() => Specify.That( scanToCreateRepository.Count() == 0).Should.BeTrue("repo should not have a job in it"),
                TimeSpan.FromSeconds(10));


        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8316")]
        public void JobsShouldBeProducedByMostEfficientMachineInMG()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>();

            //add 37" corrugate to PG17
            ProductionGroupTests.AddCorrugatesToExistingProductionGroup(TestDatabaseManagement.Pg17_stc_mg7_c199c2755,
                new List<string>(){TestDatabaseManagement.Corrugate_w37t011.Alias}, adminConsole);

            List<MachineTrackConnector> mtcList = new List<MachineTrackConnector>()
            {
                    new MachineTrackConnector(TestDatabaseManagement.Fm2, TestDatabaseManagement.Corrugate_w37t011),
                    new MachineTrackConnector(TestDatabaseManagement.Fm2, null,2)
             };

            ////load corrugates on machines
            //var trackToCorrugate = new Dictionary<int, string>()
            //{
            //    { 1, TestDatabaseManagement.Corrugate_w37t011.Alias },
            //    { 2, TestDatabaseManagement.CorrugateNotLoaded }
            //};

            var opConsole = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();

            MachineGroupExt mg = TestDatabaseManagement.Pg17_stc_mg7_c199c2755.MachineGroups[0];
            opConsole.SelectMachineGroup(mg.SetId());

            MachineGroupTests.ChangeCorrugatesForOneMachine(mg, 0, mtcList, opConsole);
            
            //Click MG4
            opConsole.SelectMachineGroup(mg.Id);

            //Start a sim
            var sim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm2);
            var sim3 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm3);

            Retry.For(() => Specify.That(opConsole.MachineGroupStatus.InnerText.Contains("Paused")).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));

            //Put MG5 in auto
            opConsole.AutoButton.SafeClick();
            opConsole.PlayButton.SafeClick();

            //Using the trigger interface trigger
            TriggerJob(opConsole, "2000001:5:5:5:1");

            //verify that the job is not sent to fusion 2 with the 37" corrugate
            Retry.For(() => Specify.That(sim2.Simulator.Queue.Count == 0).Should.BeTrue("job should not be sent to fusion 2"),
                TimeSpan.FromSeconds(10));

            //complete job on correct sim
            CompleteJobOnSim(opConsole, sim3);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void ScanToCreateTriggeredUntilJobSentToMachineQueue()
        {
            var fusionSim = new SimulatorUtilities().ConnectAndGetFusionSim(TestDatabaseManagement.FusionMachine1);
            var printerSim = new SimulatorUtilities().ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            new ScanToCreateRepository().DeleteAll();
            var results = new PerformanceTestResult() { SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate, Description = "STC Triggered Until Job Sent to Queue" };
            var operCon = OperatorConsoleTests.OpenOperatorPageAndLoginAsAdmin(new MachineGroupRepository().FindByAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id);

            results.Tests.Add("warmup", MeasureTimeBetweenTriggerAndJobSentToQueue("111", fusionSim, operCon));
            results.Tests.Add("111", MeasureTimeBetweenTriggerAndJobSentToQueue("111", fusionSim, operCon));
            results.Tests.Add("111:1", MeasureTimeBetweenTriggerAndJobSentToQueue("111:1", fusionSim, operCon));
            results.Tests.Add("111:10", MeasureTimeBetweenTriggerAndJobSentToQueue("111:10", fusionSim, operCon));
            results.Tests.Add("average", new TimeSpan(Convert.ToInt64(results.Tests.Average(t => t.Value.Ticks))));
            
            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\STC_TriggeredUntilJobSentToQueue_testresults.txt", results.ToString());
        }

        private TimeSpan MeasureTimeBetweenTriggerAndJobSentToQueue(string cartonToTrigger, FusionSimulatorViewModel fusionSim, OperatorConsole operCon = null)
        {
            new ScanToCreateRepository().DeleteAll();
            File.Delete(Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log"));
            Thread.Sleep(2000);
            operCon = OperatorConsole.SelectMachineGroup(new MachineGroupRepository().FindByAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id, operCon);
            if (!operCon.TriggerInterface.Enabled)
                operCon.AutoButton.SafeClick();
            if (operCon.IsMachineGroupPaused)
                operCon.PlayButton.SafeClick();

            
            operCon.TriggerInterface.SafeSetText(cartonToTrigger);
            operCon.TriggerInterfaceButton.SafeClick();
            
            var stopWatch = Stopwatch.StartNew();
            while (!CheckFileForJobSentToQueueUpdate())
                Thread.Sleep(1);
            stopWatch.Stop();

            return stopWatch.Elapsed;
        }

        public bool CheckFileForJobSentToQueueUpdate()
        {
            string[] lines = { };
            bool readSuccess = false, purgeSuccess = false;
            var logFilePath = Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log");
            var matchStrA = "AddProductionItemToQueue|AddProductionItemToQueue for item Carton ProducibleId:";
            var matchStrB = "QueueLength:";
            
            while (!readSuccess) {
                try {
                    lines = File.ReadAllLines(logFilePath);
                    readSuccess = true;
                }
                catch  {  }
            }
            for (int i = lines.Length - 1; i > 0; i--)
            {
                if (lines[i].ToLower().Contains(matchStrA.ToLower()) || lines[i].ToLower().Contains(matchStrB.ToLower()))
                {
                    while (!purgeSuccess)
                    {
                        try
                        {
                            File.Delete(logFilePath);
                            purgeSuccess = true;
                        }
                        catch {  }
                    }
                    return true;
                }
            }
            return false;
        }

        private static void TriggerJob(OperatorConsole opPanel, string articleName)
        {
            opPanel.TriggerInterface.SafeSetText(articleName);
            Thread.Sleep(1000);
            opPanel.TriggerInterfaceButton.SafeClick();
            Thread.Sleep(2000);
        }       
    }
}
