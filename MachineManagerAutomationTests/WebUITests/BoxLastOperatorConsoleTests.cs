﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

using Testing.Specificity;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Data.Producibles;
using PackNet.FusionSim.ViewModels;
using PackNet.Services;
using PackNet.Common.Interfaces.Producible;

using MachineManagerAutomationTests.Macros;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.WebUITests.Repository.Performance;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    [DeploymentItem("NLog.config")]
    public class BoxLastOperatorConsoleTests : ExternalApplicationTestBase
    {
        #region Variables
        RestrictionResolverService restrictionResolverService;
        SimulatorUtilities simUtilities = null;
        #endregion

        #region Constructor and initializers
        public BoxLastOperatorConsoleTests()
            : base(TestSettings.Default.MachineManagerProcessName, Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe), TestSettings.Default.CloseAppBetweenTests)
        {
            restrictionResolverService = new RestrictionResolverService();
        }

        protected override void BeforeTestInitialize()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxLast);
            simUtilities = new SimulatorUtilities();
        }

        protected override void AfterTestCleanup()
        {
            simUtilities.Close();
            Thread.Sleep(1000);
            base.AfterTestCleanup();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void ScratchTest()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var a1 = ArticleMacros.AddSubArticlesToArticle(TestDatabaseManagement.Article111, 
                new List<IProducible>() { TestDatabaseManagement.Carton1_d2000001_l10w10h10 }, true);
            ArticleMacros.BuildArticle(a1, adminConsole);

            var a2 = ArticleMacros.AddSubArticlesToArticle(TestDatabaseManagement.Article222, 
                new List<IProducible>() { TestDatabaseManagement.Carton2_d2000001_l11w11h11 }, true);
            ArticleMacros.BuildArticle(a2, adminConsole);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void SetupABoxLastEnvironment()
        {
            TestUtilities.DropDbAndRestart();
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg23_boxlast_mg2mg8_5Corrs, adminConsole);
            ArticleMacros.BuildArticles(new List<ArticleExt>() { TestDatabaseManagement.Article111, TestDatabaseManagement.Article222, 
                TestDatabaseManagement.Article333 }, adminConsole);
            TestDatabaseManagement.CreateMongoDump(SelectionAlgorithmTypes.BoxLast);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5851")]
        public void BoxLastCartonsShouldBeProducedWhenItIsScanned()
        {
            DropFileUtilities.DropDefaultBoxLastData(10);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var page = MachineProductionPage.GetWindow();

            page.ClickAutoAndPlayButtons();

            page.TriggerJob("1");

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);

            page.CompletedItemsTable.VerifyCellValue(0, 1, JobStatus.Completed);
        }


        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5887")]
        public void BoxLastLabelSyncAfterOutOfCorrugate()
        {
            DropFileUtilities.DropDefaultBoxLastData();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();

            page.TriggerJob("1");

            SimulatorHelper.VerifySimulatorHasItemsInQueue(fusionSim);

            fusionSim.StartStopPackaging();
            Playback.Wait(3000); //simulate packaging time

            page.ItemsInProductionTable.VerifyContainsValue(JobStatus.Started);

            fusionSim.OutOfCorrugate();
            Thread.Sleep(2000);

            printerSim.StartCompletePrinting();
            printerSim.TakeLabel();

            fusionSim.Simulator.ResetError();
            Thread.Sleep(1000);
            fusionSim.StartupComplete();
            Thread.Sleep(3000);

            page.ClickPlayButton();
            Thread.Sleep(2000);

            page.CompletedItemsTable.VerifyCellValue(0, 1, JobStatus.Completed);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5867")]
        public void BoxLastJobIsNotLostWhenServerIsRestarted()
        {
            var customerId = "1";
            var mg1Guid = TestDatabaseManagement.Mg2_fm1zp1.SetId();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(mg1Guid);

            DropFileUtilities.DropDefaultBoxLastData(5);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();
            page.TriggerJob(customerId);

            Thread.Sleep(2000);

            page.ItemsInProductionTable.VerifyCellValue(0, 0, JobStatus.SentToMachine);
            printerSim.TakeLabel();
            Thread.Sleep(2000);
            TestUtilities.RestartPackNetService();

            Thread.Sleep(2000);

            adminConsole.SelectMachineGroup(mg1Guid);

            printerSim.ClearQueue();

            fusionSim.StartupComplete();
            printerSim.StartCompletePrinting();

            Thread.Sleep(2000);
            printerSim.TakeLabel();
            Thread.Sleep(2000);
            printerSim.ClearQueue();
            Thread.Sleep(2000);

            page.ClickAutoButton();
            page.ClickPlayButton();

            page.TriggerJob(customerId);

            //StartCompleteVerifyCarton(fusionSim, printerSim, adminConsole);

            page.CompletedItemsTable.VerifyContainsValue(JobStatus.Completed);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5870")]
        public void BoxLastJobCanBeReproducedAfterShuttingDownMachineWhileProducing()
        {
            var page = MachineProductionPage.GetWindow();
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            DropFileUtilities.DropDefaultBoxLastData();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            page.ClickAutoAndPlayButtons();
            page.TriggerJob("1");

            SimulatorHelper.VerifySimulatorHasItemsInQueue(fusionSim);

            printerSim.ClearQueue();
            Thread.Sleep(2000);

            fusionSim.StartStopPackaging();
            printerSim.Stop();
            Thread.Sleep(15000);

            fusionSim.Start();
            Thread.Sleep(4000);
            fusionSim.StartupComplete();
            Thread.Sleep(2000);

            if (!string.IsNullOrEmpty(printerSim.ZebraPrinter.Template))
            {
                printerSim.StartStopSimulation();
                printerSim.StartCompletePrinting();
                Thread.Sleep(1000);
                printerSim.TakeLabel();
                Thread.Sleep(2000);
            }
            page.ClickPlayButton();
            Thread.Sleep(3000);

            page.CompletedItemsTable.SelectRow(0);
            page.ClickCompletedJobReproduceButton(0);
            Thread.Sleep(2000);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);

            page.CompletedItemsTable.VerifyCellValue(0, 2, 1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5997")]
        public void BoxLastOperatorCanReproduceCartonFromOperatorPanel()
        {
            var page = MachineProductionPage.GetWindow(null, ProductionMode.Auto, UserType.Administrator);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            DropFileUtilities.DropDefaultBoxLastData(5);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            page.ClickAutoAndPlayButtons();

            page.TriggerJob("1");

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);

            page.ClickCompletedJobReproduceButton(0);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);

            page.CompletedItemsTable.VerifyCellValue(0, 2, 1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5891")]
        public void BoxLastOperatorCanRetriggerJobsToAnotherPG()
        {
            DropFileUtilities.DropDefaultBoxLastData(4);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var sim1 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var sim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm2);
            var sim3 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm3);
            var printerSim1 = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printerSim2 = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);
            var printerSim3 = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp3);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = MachineProductionPage.GetWindow();
            page.VerifyIsMachineGroupStatus(MachineStatus.Paused);
            page.ClickAutoAndPlayButtons();
            for (int custId = 1; custId <= 4; custId++)
            {
                page.TriggerJob(custId);
                Playback.Wait(1000);
            }

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg8_fm2fm3zp2zp3.SetId());

            page.ClickAutoAndPlayButtons();

            var serial4 = "4";
            page.TriggerJob(serial4);
            Playback.Wait(2000);

            Specify.That(page.ItemsInProductionTable.GetCellValue(0, 0).Contains(JobStatus.SentToMachine)).Should.BeTrue("Job is not queued");
            Specify.That(page.ItemsInProductionTable.GetCellValue(0, 1).Equals(serial4)).Should.BeTrue("job not found in sent");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5922")]
        public void BoxLastTriggerJobUsingOperatorPanel()
        {
            var customerId = "1";
            var page = MachineProductionPage.GetWindow(SelectionAlgorithmTypes.BoxLast, ProductionMode.Auto, UserType.Administrator);

            DropFileUtilities.DropDefaultBoxLastData(10);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            page.ClickAutoAndPlayButtons();

            page.TriggerJob(customerId);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);

            page.CompletedItemsTable.VerifyCellValue(0, 2, customerId);
        }


        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5878")]
        public void BoxLastOperatorCanReproduceCartonFromOperatorPanelSearch()
        {
            DropFileUtilities.DropDefaultBoxLastData();
            var mg1Guid = TestDatabaseManagement.Mg2_fm1zp1.SetId();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(mg1Guid);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var page = MachineProductionPage.GetWindow();

            page.ClickAutoAndPlayButtons();

            page.TriggerJob("1");

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);

            adminConsole.NavigateToPage(NavPages.SearchBoxLast);

            var searchPage = new SearchPage(SelectionAlgorithmTypes.BoxLast);
            searchPage.ClickSearchButton();

            searchPage.ResultTable.SelectRow(0);
            searchPage.ClickReproduceButton();

            adminConsole.SelectMachineGroup(mg1Guid);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5903")]
        public void WhenLabelIsMissingCartonShouldBeReportedAsCompletedAndReproducible()
        {
            var customerId = "1";

            DropFileUtilities.DropDefaultBoxLastData();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();
            page.EnterTextInTriggerInterface(customerId);
            printerSim.Stop();
            page.ClickTriggerInterfaceButton();

            SimulatorHelper.VerifySimulatorHasItemsInQueue(fusionSim);

            fusionSim.StartStopPackaging();
            Playback.Wait(2000);
            fusionSim.StartStopPackaging();

            Playback.Wait(15000);

            printerSim.Start();
            Thread.Sleep(3000);
            printerSim.StartupComplete();
            Playback.Wait(5000);

            if (!string.IsNullOrEmpty(printerSim.ZebraPrinter.Template))
            {
                printerSim.StartCompletePrinting();
                Thread.Sleep(1000);
                printerSim.TakeLabel();
                Thread.Sleep(2000);
            }

            page.ClickPlayButton();
            page.TriggerJob(customerId);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6615")]
        public void LabelOnlyJobIsProducedSuccessfullyViaDropFile()
        {
            DropFileUtilities.DropLabelOnlyBoxLastData(1);
            DropFileUtilities.DropLabelOnlyBoxLastData(1, startingId: 2, priorityLabels: true);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg8_fm2fm3zp2zp3.SetId());

            var fusionSim1 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm2);
            var fusionSim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm3);
            var printerSim2 = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);
            var printerSim3 = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp3);

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();
            page.TriggerJob("1");
            Thread.Sleep(1000);

            TestUtilities.VerifyPrinterHasLabelTemplate(printerSim2, "Printer 2", false);

            SimulatorHelper.ProduceLabelInQueue(printerSim3, "Printer 3");

            page.TriggerJob("2");
            Thread.Sleep(1000);

            TestUtilities.VerifyPrinterHasLabelTemplate(printerSim3, "Printer 3", false);

            SimulatorHelper.ProduceLabelInQueue(printerSim2, "Printer 2");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5999")]
        public void BoxLastCartonCanBeReproducedFromAdminSearch()
        {
            var mgId = TestDatabaseManagement.Mg2_fm1zp1.SetId();

            DropFileUtilities.DropDefaultBoxLastData();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(mgId);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var page = MachineProductionPage.GetWindow();

            page.ClickAutoAndPlayButtons();
            
            page.TriggerJob(1);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);

            adminConsole.NavigateToPage(NavPages.SearchBoxLast);

            var searchPage = new SearchPage(SelectionAlgorithmTypes.BoxLast);
            searchPage.ClickSearchButton();
            searchPage.ResultTable.SelectRow(0);
            searchPage.ClickReproduceButton();

            adminConsole.SelectMachineGroup(mgId);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);
        }


        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void MeasureTimeToCompleteJob()
        {
            TestDatabaseManagement.CascadedTruncateOfTableInDatabase(DatabaseTables.BoxLast);

            var totalNoOfJobs = 10;
            var simSpeedInSeconds = 4;
            var results = new PerformanceTestResult { SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast, 
                Description = String.Format("BL Time To Complete {0} Jobs, sim loop at {1} seconds", totalNoOfJobs, simSpeedInSeconds) };

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            fusionSim.LoopTimeInSeconds = simSpeedInSeconds;
            fusionSim.LoopComplete();
            printerSim.LoopTimeInSeconds = simSpeedInSeconds;
            printerSim.LoopComplete();

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();

            var stopWatch = Stopwatch.StartNew();
            MeasureImportComplete(totalNoOfJobs);

            Thread.Sleep(2000);
            for (int i = 1; i <= totalNoOfJobs; i++)
            {
                page.TriggerJob(i);
                Thread.Sleep(250);
            }

            var boxLastRepo = new BoxLastRepository();
            while (boxLastRepo.FindWithStatus(ProducibleStatuses.ProducibleCompleted).Count() != totalNoOfJobs)
            {
                Playback.Wait(1);
            }

            stopWatch.Stop();
            results.Tests.Add(totalNoOfJobs.ToString(), stopWatch.Elapsed);

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\BL_timetocompletedrop_testresults.txt", results.ToString());
        }


        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void BoxLastImportCompleteTest()
        {
            TestDatabaseManagement.CascadedTruncateOfTableInDatabase(DatabaseTables.BoxLast);

            TestUtilities.DeleteLogFile(LogTypes.Regular);
            Playback.Wait(2000);
            var results = new PerformanceTestResult { SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast, Description = "BL Import Performance" };

            results.Tests.Add("warmup", MeasureImportComplete(1));
            results.Tests.Add("1", MeasureImportComplete(1));
            results.Tests.Add("10", MeasureImportComplete(10));
            results.Tests.Add("100", MeasureImportComplete(100));
            results.Tests.Add("1000", MeasureImportComplete(1000));
            results.Tests.Add("10000", MeasureImportComplete(10000));
            results.Tests.Add("100000", MeasureImportComplete(100000));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\BL_import_testresults.txt", results.ToString());

            Specify.That(results.Tests["1"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("BL Import 1 running unusually slow");
            Specify.That(results.Tests["10"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("BL Import 10 running unusually slow");
            Specify.That(results.Tests["100"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("BL Import 100 running unusually slow");
            Specify.That(results.Tests["1000"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("BL Import 1000 running unusually slow");
            Specify.That(results.Tests["10000"].Duration() < new TimeSpan(0, 0, 0, 3)).Should.BeTrue("BL Import 10000 running unusually slow");
            Specify.That(results.Tests["100000"].Duration() < new TimeSpan(0, 0, 2, 0)).Should.BeTrue("BL Import 100000 running unusually slow");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void BoxLastDropFileLoadStagingCompleteTest()
        {
            TestDatabaseManagement.CascadedTruncateOfTableInDatabase(DatabaseTables.BoxLast);

            TestUtilities.DeleteLogFile(LogTypes.Regular);
            Thread.Sleep(2000);

            var results = new PerformanceTestResult { SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast, Description = "BL Import/Staging Performance" };

            results.Tests.Add("warmup", MeasureDropFileAvailability(1));
            results.Tests.Add("1", MeasureDropFileAvailability(1));
            results.Tests.Add("10", MeasureDropFileAvailability(10));
            results.Tests.Add("100", MeasureDropFileAvailability(100));
            results.Tests.Add("1000", MeasureDropFileAvailability(1000));
            results.Tests.Add("10000", MeasureDropFileAvailability(10000));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\BL_importstaging_testresults.txt", results.ToString());

            Specify.That(results.Tests["1"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("BL Import/Staging 1 running unusually slow");
            Specify.That(results.Tests["10"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("BL Import/Staging 10 running unusually slow");
            Specify.That(results.Tests["100"].Duration() < new TimeSpan(0, 0, 0, 10)).Should.BeTrue("BL Import/Staging 100 running unusually slow");
            Specify.That(results.Tests["1000"].Duration() < new TimeSpan(0, 0, 10, 0)).Should.BeTrue("BL Import/Staging 1000 running unusually slow");
            Specify.That(results.Tests["10000"].Duration() < new TimeSpan(0, 0, 1, 30)).Should.BeTrue("BL Import/Staging 10000 running unusually slow");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void BoxLastTriggeredUntilJobSentToMachineQueue()
        {
            TestDatabaseManagement.CascadedTruncateOfTableInDatabase(DatabaseTables.BoxLast);

            TestUtilities.DeleteLogFile(LogTypes.Regular);

            var noOfCartons = 10;
            var simUtils = new SimulatorUtilities();
            var results = new PerformanceTestResult() { SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast, 
                Description = "BL Triggered Until Job Sent to Queue" };

            var fusionSim = simUtils.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtils.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            
            DropFileUtilities.DropDefaultBoxLastData(noOfCartons);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            for (int i = 1; i <= Math.Min(noOfCartons, 5); i++)
            {
                var timeSpan = MeasureTimeBetweenTriggerAndJobSentToQueue(i, fusionSim, printerSim, adminConsole);
                results.Tests.Add(i.ToString(), timeSpan);
            }

            results.Tests.Add("Average", new TimeSpan(Convert.ToInt64(results.Tests.Average(k => k.Value.Ticks))));
            new PerformanceTestResultRepository().Create(results);

            File.AppendAllText(@"C:\BL_TriggerTilJobSentToQueue_testresults.txt", results.ToString());

            Specify.That(results.Tests["Average"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("BL Trigger Until Job Sent to Machine running unusually slow");
        }

        #endregion

        #region Helper methods

        TimeSpan MeasureTimeBetweenTriggerAndJobSentToQueue(int cartonToTrigger, FusionSimulatorViewModel fusionSim, ZebraPrinterSimulatorViewModel printerSim, AdminConsole adminConsole = null)
        {

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();
            page.TriggerJob(cartonToTrigger);

            var stopWatch = Stopwatch.StartNew();

//            TestUtilities.FindInLogFile(LogTypes.Regular, "Status:ProducibleSentToMachineGroup", string.Format(@"CustomerUniqueId"":""{0}""", cartonToTrigger), true);

            while (!CheckFileForJobSentToQueueUpdate(cartonToTrigger))
                Thread.Sleep(1);
            stopWatch.Stop();

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim, true);
            return stopWatch.Elapsed;
        }

        TimeSpan MeasureImportComplete(int cartonsToDrop)
        {
            var fullFileName = DropFileUtilities.DropDefaultBoxLastData(cartonsToDrop);
            var filename = fullFileName.Split('\\')[fullFileName.Split('\\').Count() - 1];
            var sw = Stopwatch.StartNew();
            while (!CheckFileForImportUpdate(filename))
                Thread.Sleep(50);
            sw.Stop();
            return sw.Elapsed;
        }

        TimeSpan MeasureDropFileAvailability(int cartonsToDrop)
        {
            var boxLastRepo = new BoxLastRepository();
            DropFileUtilities.DropDefaultBoxLastData(cartonsToDrop);
            var sw = Stopwatch.StartNew();
            long count = 0;
            while (count < cartonsToDrop)
                count = boxLastRepo.Count();

            sw.Stop();
            return sw.Elapsed;
        }

        bool CheckFileForJobSentToQueueUpdate(int cartonToTrigger)
        {
            string[] lines = { };
            bool readSuccess = false, purgeSuccess = false;
            string matchStrA = "Status:ProducibleSentToMachineGroup";
            string matchStrB = string.Format(@"CustomerUniqueId"":""{0}""", cartonToTrigger); //"CustomerUniqueId":"1"
            while (!readSuccess)
            {
                try
                {
                    lines = File.ReadAllLines(TestUtilities.GetLogPath(LogTypes.Regular));
                    readSuccess = true;
                }
                catch { }
            }
            for (int i = lines.Length - 1; i > 0; i--)
            {
                if (lines[i].ToLower().Contains(matchStrA.ToLower()) && lines[i].ToLower().Contains(matchStrB.ToLower()))
                {
                    while (!purgeSuccess)
                    {
                        try
                        {
                            File.Delete(TestUtilities.GetLogPath(LogTypes.Regular));
                            purgeSuccess = true;
                        }
                        catch { }
                    }
                    return true;
                }
            }
            return false;
        }
  
        #endregion
    }
}
