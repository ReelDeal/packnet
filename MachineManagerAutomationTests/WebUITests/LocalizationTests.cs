﻿using System;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows;

using CUITe.Controls.HtmlControls;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Enums;

using Testing.Specificity;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.FusionSim.ViewModels;
using PackNet.Services;
using MachineManagerAutomationTests.Macros;
using System.Diagnostics;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.FusionSim.Models;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    [DeploymentItem("NLog.config")]
    public class LocalizationTests : ExternalApplicationTestBase
    {
        SimulatorUtilities simUtilities;

        #region Constructors
        public LocalizationTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }

        protected override void BeforeTestInitialize()
        {
            simUtilities = new SimulatorUtilities();
        }

        protected override void AfterTestCleanup()
        {
            base.AfterTestCleanup();
        }
        #endregion

        #region Screenshotting Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void SetupLocalizationDatabase()
        {
            TestUtilities.DropDbAndRestart();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var page = ProductionGroupPage.GetWindow();
            var corrugates = new List<Corrugate>() { TestDatabaseManagement.Corrugate_w18625t016, TestDatabaseManagement.Corrugate_w2925t016};

            ProductionGroupMacros.Build(TestDatabaseManagement.Pg_Localization, adminConsole);

            ProductionGroupExt pg = new ProductionGroupExt(SelectionAlgorithmTypes.BoxLast, "TestBL");
            pg.Corrugates.AddRange(corrugates);
            page.Create(pg, adminConsole);

            pg = new ProductionGroupExt(SelectionAlgorithmTypes.Order, "TestOR");
            pg.Corrugates.AddRange(corrugates);
            page.Create(pg, adminConsole);

            pg = new ProductionGroupExt(SelectionAlgorithmTypes.ScanToCreate,"TestSC");
            pg.Corrugates.AddRange(corrugates);
            pg.Workflow = BarcodeParsingWorkflows.ParseArticleFromScanData;
            page.Create(pg, adminConsole);

            TestDatabaseManagement.CreateMongoDump("localization");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_da_DK()
        {
            ScreenshotAllThePagesForLanguage("da_DK");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_de_AT()
        {
            ScreenshotAllThePagesForLanguage("de_AT");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_de_CH()
        {
            ScreenshotAllThePagesForLanguage("de_CH");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_de_DE()
        {
            ScreenshotAllThePagesForLanguage("de_DE");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_en_US()
        {
            ScreenshotAllThePagesForLanguage("en_US");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_eo()
        {
            ScreenshotAllThePagesForLanguage("eo");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_es_ES()
        {
            ScreenshotAllThePagesForLanguage("es_ES");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_fr_CA()
        {
            ScreenshotAllThePagesForLanguage("fr_CA");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_fr_FR()
        {
            ScreenshotAllThePagesForLanguage("fr_FR");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_hu_HU()
        {
            ScreenshotAllThePagesForLanguage("hu_HU");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_nl_BE()
        {
            ScreenshotAllThePagesForLanguage("nl_BE");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_pl_PL()
        {
            ScreenshotAllThePagesForLanguage("pl_PL");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_sv_SE()
        {
            ScreenshotAllThePagesForLanguage("sv_SE");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_es_MX()
        {
            ScreenshotAllThePagesForLanguage("es_MX");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_it_IT()
        {
            ScreenshotAllThePagesForLanguage("it_IT");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_nl_NL()
        {
            ScreenshotAllThePagesForLanguage("nl_NL");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesFor_ro_RO()
        {
            ScreenshotAllThePagesForLanguage("ro_RO");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllThePagesForAllLanguages()
        {
            List<string> languages = new List<string>();
            languages.Add("da_DK");
            languages.Add("de_AT");
            languages.Add("de_CH");
            languages.Add("de_DE");
            languages.Add("en_US");
            languages.Add("es_ES");
            languages.Add("fr_CA");
            languages.Add("fr_FR");
            languages.Add("hu_HU");
            languages.Add("pl_PL");
            languages.Add("sv_SE");
            languages.Add("es_MX");
            languages.Add("it_IT");
            languages.Add("nl_NL");
            languages.Add("ro_RO");
            ScreenshotAllLanguagesStart(languages);
        }

        private void ScreenshotAllThePagesForLanguage(string language)
        {
            List<string> languages = new List<string>();
            languages.Add(language);
            ScreenshotAllLanguagesStart(languages);
        }

        private void ScreenshotAllLanguagesStart(List<string> languages)
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService("localization");

            var startTime = DateTime.Now.ToString("yyyyMMdd-hhmm");

            simUtilities = new SimulatorUtilities();
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            AdminConsole adminConsole = null;
            var firstLanguage = true;
            
            foreach(string lang in languages)
            {
                var directory = @"C:\PackNetScreenshots\" + lang + @"\" + startTime + @"\";
                Directory.CreateDirectory(directory);

                TestUtilities.StartServiceIfNotStarted();

                if (firstLanguage)
                {
                    adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: lang);
                }
                else
                {
                    adminConsole.Logout();

                    var loginPage = DynamicBrowserWindowWithLogin.GetWindow();
                    loginPage.Login(TestDatabaseManagement.AdminUser.UserName, TestDatabaseManagement.AdminUser.Password, lang);
                }

                firstLanguage = false;
                ScreenshotAllThePagesForLanguageSub(lang, directory, adminConsole, fusionSim);
            }

            fusionSim.Dispose();
            printerSim.Dispose();
        }

        private void ScreenshotAllThePagesForLanguageSub(string language, string directory, AdminConsole adminConsole, FusionSimulatorViewModel fusionSim)
        {
            // in case the simulator has decided to go down 
            SimulatorUtilities.StartAndCompleteSimulation(fusionSim);

            //Screenshot all the Admin side screens (except ServicePage that is done at the bottom of the method)
            ScreenshotTheAdminMenu(adminConsole, directory);
            ScreenshotTheWidgetsSelectionPage(adminConsole, directory);
            ScreenshotTheAccessoriesPage(adminConsole, directory);
            ScreenshotTheCrudPages(adminConsole, directory);
            ScreenshotAllTheWidgets(adminConsole, directory);
            ScreenshotTheAdminSearchPages(adminConsole, directory);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg_Localization.SetId());

            ScreenshotTheOperatorMenu(adminConsole, directory);
            GC.Collect();
            ScreenshotTheOperatorConfig(adminConsole, directory);
            ScreenshotTheOperatorSearchPages(adminConsole, directory);
            ScreenshotTheCartonCreationPages(adminConsole, directory);

            // screenshot pages where sim is needed
            ScreenshotTheServicePage(adminConsole, directory, fusionSim);
            ScreenshotTheMachineProductionPage(adminConsole, directory, fusionSim);
        }

        private void ScreenshotTheAdminMenu(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Dashboard);
            adminConsole.CreateScreendump(directory, "AdminConsoleMenuReports");
            Playback.Wait(250);
            adminConsole.ClickReportsMenuSectionButton();
            adminConsole.ClickSearchMenuSectionButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleMenuSearch");
            Playback.Wait(250);
            adminConsole.ClickSearchMenuSectionButton();
            Playback.Wait(250);
            adminConsole.ClickConfigurationMenuSectionButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleMenuConfig");
            Playback.Wait(250);
            adminConsole.ClickConfigurationMenuSectionButton();
            adminConsole.ClickAdminSideAdministratorButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleMenuAdmin");
            Playback.Wait(1000);
            adminConsole.NavigateToPage(NavPages.About);
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutReleaseNotes1");
            Mouse.MoveScrollWheel(adminConsole, -6);
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutReleaseNotes2");
            Mouse.MoveScrollWheel(adminConsole, -6);
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutReleaseNotes3");
            Mouse.MoveScrollWheel(adminConsole, -3);
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutReleaseNotes4");

            GC.Collect();

            Playback.Wait(100);
            adminConsole.ClickEulaButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutEULA1");
            Playback.Wait(500);

            Mouse.MoveScrollWheel(adminConsole, -3);
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutEULA2");
            Playback.Wait(500);

            Mouse.MoveScrollWheel(adminConsole, -3);
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutEULA3");
            Playback.Wait(500);

            Mouse.MoveScrollWheel(adminConsole, -3);
            adminConsole.CreateScreendump(directory, "AdminConsoleAboutEULA4");
            Playback.Wait(500);

            adminConsole.ClickSupportButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleSupport");
            adminConsole.ClickLoggedInUsername();
            adminConsole.CreateScreendump(directory, "AdminConsoleLoggedInUserMenu");

            adminConsole.ClickAdminSideAdministratorButton();
        }

        private void ScreenshotTheWidgetsSelectionPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Dashboard);
            adminConsole.ClickNewWidgetButton();

            adminConsole.CreateScreendump(directory, "AdminConsoleWidgetsGeneral");
            adminConsole.ClickDailyWidgetsTab();
            adminConsole.CreateScreendump(directory, "AdminConsoleWidgetsDaily");
            adminConsole.ClickWeeklyWidgetsTab();
            adminConsole.CreateScreendump(directory, "AdminConsoleWidgetsWeekly");
            adminConsole.ClickCloseAddWidgetsModalButton();
            Playback.Wait(1000);
            adminConsole.ClickEditDashboardButton();
            adminConsole.ClickReportsMenuSectionButton();
        }

        private void ScreenshotTheCrudPages(AdminConsole adminConsole, string directory)
        {
            GC.Collect();
            ScreenshotTheNewArticlesPage(adminConsole, directory);
            ScreenshotTheCpgPage(adminConsole, directory);
            ScreenshotTheClassificationPage(adminConsole, directory);
            ScreenshotTheCorrugatesPage(adminConsole, directory);
            ScreenshotTheMachinePages(adminConsole, directory);
            ScreenshotTheMachineGroupsPage(adminConsole, directory);
            ScreenshotTheProductionGroupPage(adminConsole, directory);
            ScreenshotThePickZonePage(adminConsole, directory);
            ScreenshotTheSettingsPage(adminConsole, directory);
            ScreenshotTheUsersPage(adminConsole, directory);
            adminConsole.ClickConfigurationMenuSectionButton();
        }

        private void ScreenshotTheUsersPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.User);
            var page = UserManagementPage.GetWindow();
            adminConsole.CreateScreendump(directory, "AdminConsoleUserList");

            page.ClickNewButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleUserNew");
            page.ClickCancelButton(true);
        }

        private void ScreenshotTheSettingsPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Settings);

            var page = ServerSettingsPage.GetWindow();

            adminConsole.CreateScreendump(directory, "AdminConsoleSettingsGeneral");

            page.ClickFileImportTab();
            adminConsole.CreateScreendump(directory, "AdminConsoleSettingsFileImport");

            page.ClickSldSettingsTab();
            adminConsole.CreateScreendump(directory, "AdminConsoleSettingsSLD");
        }

        private void ScreenshotThePickZonePage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.PickZone);

            var page = PickZonesPage.GetWindow();
            adminConsole.CreateScreendump(directory, "AdminConsolePickZoneList");
            page.ClickNewButton();
            adminConsole.CreateScreendump(directory, "AdminConsolePickZoneNew");
            page.ClickCancelButton(true);
        }

        private void ScreenshotTheProductionGroupPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.ProductionGroup);
            var page = ProductionGroupPage.GetWindow();

            adminConsole.CreateScreendump(directory, "AdminConsoleProductionGroupList");

            page.ClickNewButton();
 
            page.SetProductionModeMenuByIndex(1);
            adminConsole.CreateScreendump(directory, "AdminConsoleProductionGroupNewBoxFirst");

            page.SetProductionModeMenuByIndex(2);
            adminConsole.CreateScreendump(directory, "AdminConsoleProductionGroupNewBoxLast");

            page.SetProductionModeMenuByIndex(3);
            adminConsole.CreateScreendump(directory, "AdminConsoleProductionGroupNewOrders");

            page.SetProductionModeMenuByIndex(4);
            adminConsole.CreateScreendump(directory, "AdminConsoleProductionGroupNewScanToCreate");

            page.ClickCancelButton();
        }

        private void ScreenshotTheMachineGroupsPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.MachineGroup);
            var page = MachineGroupPage.GetWindow();
            adminConsole.CreateScreendump(directory, "AdminConsoleMachineGroupList");
            page.ClickNewButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleMachineGroupNew");
            page.ClickCancelButton(true);
        }

        private void ScreenshotTheMachinePages(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Machine);
            adminConsole.CreateScreendump(directory, "AdminConsoleMachineList");

            var page = MachineManagementPage.GetWindow();

            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.Fusion);
            adminConsole.CreateScreendump(directory, "AdminConsoleMachineFusionNew");
            page.ClickCancelButton();

            //adminConsole.CreateScreendump(directory, "AdminConsoleMachineDiscoverySearching");
            //var macDiscoMgmt = new MachineDiscoveryPage();
            //macDiscoMgmt.WaitForCancelButton();
            //adminConsole.CaptureImage()
            //    .Save(directory + "AdminConsoleMachineDiscoverySearchResults.png", System.Drawing.Imaging.ImageFormat.Png);
            //macDiscoMgmt.ClickCancelButton();

            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.EM);
            adminConsole.CreateScreendump(directory, "AdminConsoleMachineEmNew");
            page.ClickCancelButton();

            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.ZebraPrinter);
            page.ClickPrinterToggleShowAdvancedSettings();
            adminConsole.CreateScreendump(directory, "AdminConsoleMachinePrinterNew");
            page.ClickCancelButton();

            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.Scanner);
            adminConsole.CreateScreendump(directory, "AdminConsoleMachineScannerNew");
            page.ClickCancelButton();

            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.ExternalSystem);
            adminConsole.CreateScreendump(directory, "AdminConsoleMachineExternalSystemNew");
            page.ClickExternalSystemCancelButton();
        }

        private void ScreenshotTheCorrugatesPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Corrugate);
            adminConsole.CreateScreendump(directory, "AdminConsoleCorrugatesList");
            
            var page = CorrugatesPage.GetWindow();
            page.ClickNewButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleCorrugatesNew");
            page.ClickCancelButton(true);
        }

        private void ScreenshotTheClassificationPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Classification);
            adminConsole.CreateScreendump(directory, "AdminConsoleClassificationList");
            var page = new ClassificationsPage();
            page.ClickNewButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleClassificationNew");
            page.ClickCancelButton(true);
        }

        private void ScreenshotTheCpgPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.CartonPropertyGroup);
            adminConsole.CreateScreendump(directory, "AdminConsoleCartonPropertyGroupList");

            var page = CartonPropertyGroupsPage.GetWindow();
            page.ClickNewButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleCartonPropertyGroupNew");
            page.ClickCancelButton(true);
        }

        private void ScreenshotTheNewArticlesPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Article);
            adminConsole.CreateScreendump(directory, "AdminConsoleNewArticleList");

            var page = ArticlesManagementPage.GetWindow();

            page.ClickNewArticleButton();

            // new Carton
            Playback.Wait(500);
            page.ClickDescriptionField(); //trigger the validation, allowing the test to progress smoothly
            page.ClickNewSubArticleButton();
            page.ClickNewCartonButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleNewArticleNewCarton");
            page.ClickCancelCartonButton(true);

            //new Label
            Playback.Wait(500);
            page.ClickDescriptionField(); //trigger the validation, allowing the test to progress smoothly
            page.ClickNewSubArticleButton();
            page.ClickNewLabelButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleNewArticleNewLabel");
            page.ClickCancelLabelButton(true);

        }

        private void ScreenshotAllTheWidgets(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Dashboard);
            Playback.Wait(500);
            adminConsole.ClickEditDashboardButton();
            adminConsole.ClickEditWidgetStructureButton();
            Playback.Wait(500);
            adminConsole.ClickDefaultDashboardLook();
            adminConsole.ClickCloseDashboardLayoutButton();
            Playback.Wait(1000);
            adminConsole.ClickNewWidgetButton();
            Playback.Wait(500);

            //foreach of the tabs - dig through the html & count the links so we know how many widgets we have
            var generalWidgetsCount = adminConsole.Get<CUITe_HtmlDiv>("id=Metrics").GetChildren()[0].GetChildren().Sum(e => e.GetChildren().OfType<CUITe_HtmlHyperlink>().Count());
            adminConsole.ClickDailyWidgetsTab();
            var dailyWidgetsCount = adminConsole.Get<CUITe_HtmlDiv>("id=MetricsByDay").GetChildren()[0].GetChildren().Sum(e => e.GetChildren().OfType<CUITe_HtmlHyperlink>().Count());
            adminConsole.ClickWeeklyWidgetsTab();
            var weeklyWidgetsCount = adminConsole.Get<CUITe_HtmlDiv>("id=MetricsByWeek").GetChildren()[0].GetChildren().Sum(e => e.GetChildren().OfType<CUITe_HtmlHyperlink>().Count());
            adminConsole.ClickCloseAddWidgetsModalButton();
            Playback.Wait(500);
            adminConsole.ClickEditDashboardButton();

            for (int i = 0; i < generalWidgetsCount; i++)
                ScreenshotWidget(adminConsole, directory, WidgetTabs.General, i);
            for (int i = 0; i < dailyWidgetsCount; i++)
                ScreenshotWidget(adminConsole, directory, WidgetTabs.Daily, i);
            for (int i = 0; i < weeklyWidgetsCount; i++)
                ScreenshotWidget(adminConsole, directory, WidgetTabs.Weekly, i);

            adminConsole.ClickReportsMenuSectionButton(); ;
        }

        private void ScreenshotWidget(AdminConsole adminConsole, string directory, WidgetTabs tab, int widgetId)
        {
            adminConsole.ClickNewWidgetButton();
            string widgetName = tab.ToString() + "Widget_" + widgetId;
            if (tab == WidgetTabs.Daily)
                adminConsole.ClickDailyWidgetsTab();
            else if (tab == WidgetTabs.Weekly)
                adminConsole.ClickWeeklyWidgetsTab();
            else
                adminConsole.ClickGeneralWidgetsTab();

            adminConsole.SelectWidget(widgetId);
            Playback.Wait(500); //let it render & load on the dashboard
            adminConsole.CreateScreendump(directory, widgetName);
            adminConsole.Get<CUITe_HtmlHyperlink>("id=killWidget").SafeClick();
        }

        private void ScreenshotTheAdminSearchPages(AdminConsole adminConsole, string directory)
        {
            var searchPage = SearchPage.GetWindow();

            // boxlast
            adminConsole.NavigateToPage(NavPages.SearchBoxLast);
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchBoxLast");
            searchPage.ClickToggleAdditionalOptionsButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchBoxLastAdditionalOptions");

            // orders
            adminConsole.NavigateToPage(NavPages.SearchOrders);
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchOrders");
            searchPage.ClickToggleAdditionalOptionsButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchOrdersAdditionalOptions");

            // boxfirst
            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchBoxFirst");
            searchPage.AdditionalSearchOptionsToggleButton.SafeClick();
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchBoxFirstAdditionalOptions");

            // scan to create
            adminConsole.NavigateToPage(NavPages.SearchScanToCreate);
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchScanToCreate");
            searchPage.ClickToggleAdditionalOptionsButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleSearchScanToCreateAdditionalOptions");

            adminConsole.ClickSearchMenuSectionButton();
        }

        private void ScreenshotTheServicePage(AdminConsole adminConsole, string directory, FusionSimulatorViewModel sim)
        {
            adminConsole.NavigateToUrl(TestUtilities.GetMachineManagerURL() + "/index.html#/Service");
            adminConsole.CreateScreendump(directory, "AdminConsoleServiceLandingPage");

            var search = "machine" + MachineMacros.FindByAlias(TestDatabaseManagement.Fm1.Alias, MachineType.Fusion).Id + "Button";
            adminConsole.Get<CUITe_HtmlButton>("id=" + search).SafeClick();

            adminConsole.Get<CUITe_HtmlButton>("id=machineServicePause").SafeClick();
            adminConsole.Get<CUITe_HtmlButton>("id=enableMachineServiceMode").SafeClick();

            adminConsole.CreateScreendump(directory, "AdminConsoleServicePage");
            adminConsole.Get<CUITe_HtmlButton>("id=disableMachineServiceMode").SafeClick();
            Playback.Wait(500);
            sim.StartupComplete();
        }

        private void ScreenshotTheAccessoriesPage(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.Machine);
            var page = MachineManagementPage.GetWindow();

            page.PageTableSelectRow(0);

            page.ClickEditButton();
            page.ClickNewFootpedalButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleFootPedalNew");
            page.ClickCancelFootPedalButton();
            page.ClickNewConveyorButton();
            adminConsole.CreateScreendump(directory, "AdminConsoleConveyorNew");
            page.ClickCancelConveyorButton();
            adminConsole.ClickConfigurationMenuSectionButton();
        }

        private void ScreenshotTheOperatorMenu(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.MachineProduction);
            adminConsole.CreateScreendump(directory, "OperatorConsoleMenuOperations");
            adminConsole.ClickOpOperationsMenuSectionButton();

            adminConsole.ClickOpConfigurationMenuSectionButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMenuConfiguration");
            adminConsole.ClickOpConfigurationMenuSectionButton();

            adminConsole.ClickOpSearchMenuSectionButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMenuSearch");
            adminConsole.ClickOpSearchMenuSectionButton();

            adminConsole.ClickOpCartonCreationMenuSectionButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMenuCartonCreation");
            adminConsole.ClickOpCartonCreationMenuSectionButton();

            adminConsole.ClickOpAdministratorMenuSectionButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMenuAdministrator");
            adminConsole.ClickOpAdministratorMenuSectionButton();
        }

        private void ScreenshotTheOperatorConfig(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.ChangeCorrugate);
            adminConsole.CreateScreendump(directory, "OperatorConsoleChangeCorrugates");

            adminConsole.NavigateToPage(NavPages.ChangeProductionGroup);
            adminConsole.CreateScreendump(directory, "OperatorConsoleChangeProductionGroup");

            adminConsole.NavigateToPage(NavPages.MachineControl);
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineCommands");

            adminConsole.ClickOpConfigurationMenuSectionButton();
        }

        private void ScreenshotTheOperatorSearchPages(AdminConsole adminConsole, string directory)
        {
            var searchPage = SearchPage.GetWindow();

            // boxlast
            adminConsole.NavigateToPage(NavPages.OpSearchBoxLast);
            adminConsole.CreateScreendump(directory, "OperatoronsoleSearchBoxLast");
            searchPage.ClickToggleAdditionalOptionsButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleSearchBoxLastAdditionalOptions");

            // orders
            adminConsole.NavigateToPage(NavPages.OpSearchOrders);
            adminConsole.CreateScreendump(directory, "OperatorConsoleSearchOrders");
            searchPage.ClickToggleAdditionalOptionsButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleSearchOrdersAdditionalOptions");

            // boxfirst
            adminConsole.NavigateToPage(NavPages.OpSearchBoxFirst);
            adminConsole.CreateScreendump(directory, "OperatorConsoleSearchBoxFirst");
            searchPage.ClickToggleAdditionalOptionsButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleSearchBoxFirstAdditionalOptions");

            // scan to create
            adminConsole.NavigateToPage(NavPages.OpSearchScanToCreate);
            adminConsole.CreateScreendump(directory, "OperatorConsoleSearchScanToCreate");
            searchPage.ClickToggleAdditionalOptionsButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleSearchScanToCreateAdditionalOptions");

            adminConsole.ClickOpSearchMenuSectionButton();
        }

        private void ScreenshotTheCartonCreationPages(AdminConsole adminConsole, string directory)
        {
            adminConsole.NavigateToPage(NavPages.CustomJobFromArticle);
            adminConsole.CreateScreendump(directory, "OperatorConsoleCustomJobFromArticle");
            adminConsole.NavigateToPage(NavPages.CustomJob);
            adminConsole.CreateScreendump(directory, "OperatorConsoleCustomJob");
            adminConsole.ClickOpCartonCreationMenuSectionButton();
        }

        private void ScreenshotTheMachineProductionPage(AdminConsole adminConsole, string directory, FusionSimulatorViewModel sim)
        {
            adminConsole.NavigateToPage(NavPages.MachineProduction);
            sim.Simulator.Play = true;
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionBoxFirstManual");

            var cpPage = new SelectProductionGroupPage();

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionBoxFirstAuto");
            page.ClickMoreInfoButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionBoxFirstMoreInfo");

            cpPage.Select("TestBL", adminConsole);
            adminConsole.NavigateToPage(NavPages.MachineProduction);
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionBoxLastAuto");
            page.ClickManualButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionBoxLastManual");
            page.ClickMoreInfoButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionBoxLastMoreInfo");
            cpPage.Select("TestOR", adminConsole);
            adminConsole.NavigateToPage(NavPages.MachineProduction);
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionOrdersManual");
            page.ClickAutoButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionOrdersAuto");
            page.ClickMoreInfoButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionOrdersMoreInfo");
            cpPage.Select("TestSC", adminConsole);
            adminConsole.NavigateToPage(NavPages.MachineProduction);
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionScanToCreateAuto");
            page.ClickManualButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionScanToCreateManual");
            page.ClickMoreInfoButton();
            adminConsole.CreateScreendump(directory, "OperatorConsoleMachineProductionScanToCreateMoreInfo");

            adminConsole.ClickOpOperationsMenuSectionButton();
            adminConsole.ClickOpConfigurationMenuSectionButton();
        }

        #endregion

        #region Admin Config Menu Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("xxxx")]
        public void ScreenshotAllTheThings()
        {
            var startTime = DateTime.Now.ToString("yyyyMMdd-hhmm");
            var directory = @"C:/PackNetScreenshots/" + startTime + "/";
            Directory.CreateDirectory(directory);
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            ScreenshotTheAdminMenu(adminConsole, directory);
        }

        //[TestMethod]
        //[TestCategory("UIAutomation")]
        //[TestCategory("9446")]
        //public void VerifyCartonPropertyGroupsCrudLocalization()
        //{
        //    var cpgPageTitleStr = "Carton Property Groupso";
        //    var newCpgNameEditLabelStr = "Nameo";
        //    var cpgNameEditValidationStr = "Carton Property Group Name Cannot Be Emptyo";
        //    var cpgToCreateName = "blergh";
        //    var expectedSuccessAlert = "Carton Property Group '" + cpgToCreateName + "' created successfully";
        //    var expectedDeleteAlert = "Carton Property Group '" + cpgToCreateName + "' deleted successfully";
        //    var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
        //    //change language to spanish
        //    adminConsole.ConfigurationLink.SafeClick();
        //    adminConsole.CartonPropertyGroupsLink.SafeClick();
        //    var cpgPage = new CartonPropertyGroupsPage();
        //    Specify.That(cpgPage.PageTitle.InnerText == cpgPageTitleStr).Should.BeTrue();
        //    cpgPage.NewButton.SafeClick();
        //    Specify.That(cpgPage.CpgNameEditLabel.InnerText == newCpgNameEditLabelStr).Should.BeTrue();
        //    cpgPage.SaveButton.SafeClick();
        //    Specify.That(cpgPage.NameRequiredValidation.InnerText == cpgNameEditValidationStr).Should.BeTrue();
        //    cpgPage.AliasEdit.SafeSetText(cpgToCreateName);
        //    cpgPage.SaveButton.SafeClick();
        //    Retry.For(() => cpgPage.AlertWidget.Exists && cpgPage.AlertWidget.Enabled, TimeSpan.FromSeconds(10));
        //    cpgPage.AlertWidget.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);
        //    Playback.Wait(500);
        //    cpgPage.CPGTable.GetCheckBox(0).SafeClick();
        //    cpgPage.EditButton.SafeClick();
        //    Specify.That(cpgPage.CpgNameEditLabel.InnerText == newCpgNameEditLabelStr).Should.BeTrue();
        //    cpgPage.CancelButton.SafeClick();
        //    cpgPage.CPGTable.GetCheckBox(0).SafeClick();
        //    cpgPage.DeleteButton.SafeClick();
        //    Playback.Wait(250);
        //    cpgPage.AlertWidget.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        //}

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9486")]
        public void VerifyArticlesCrudLocalization()
        {
            //var articlePageTitleStr = "Article Managemento";
            //var newArticleIdEditLabelStr = "Article IDo";
            //var articleNameEditValidationStr = "Article ID Cannot Be Emptyo";
            //var articleToCreateName = "blergh";
            //var expectedSuccessAlert = "Article created successfully";
            //var expectedDeleteAlert = "Delete complete, article(s) deleted.";
            //var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
            //adminConsole.ConfigurationLink.SafeClick();
            //adminConsole.ArticlesLink.SafeClick();
            //var articlesPage = new ArticlesManagement();
            //Playback.Wait(5000);
            //Specify.That(articlesPage.ArticlesPageTitle.InnerText == articlePageTitleStr).Should.BeTrue();
            //articlesPage.ArticleButtonNew.SafeClick();
            //Specify.That(articlesPage.ArticleIdLabel.InnerText == newArticleIdEditLabelStr).Should.BeTrue();
            ////articlesPage.ArticleNewSave.SafeClick();
            //articlesPage.DescriptionEdit.SafeClick();
            //Specify.That(articlesPage.ArticleIdValidation.InnerText == articleNameEditValidationStr).Should.BeTrue();
            //articlesPage.ArticleIdEdit.SafeSetText(articleToCreateName);
            //articlesPage.DesignIdList.SafeSelectItem(Designs.IQ2010001INCHES);
            //articlesPage.LengthEdit.SafeSetText("5");
            //articlesPage.WidthEdit.SafeSetText("5");
            //articlesPage.HeightEdit.SafeSetText("5");
            //articlesPage.CorrugateQualityEdit.SafeSetText("1");
            //articlesPage.ArticleNewSave.SafeClick();
            //articlesPage.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);
            //articlesPage.ArticleNewClose.SafeClick();
            //Playback.Wait(5000);
            //articlesPage.ArticlesTable.FindCell(0, 0).Click();
            //articlesPage.ArticleButtonEdit.SafeClick();
            //Specify.That(articlesPage.ArticleIdLabel.InnerText == newArticleIdEditLabelStr).Should.BeTrue();
            //articlesPage.ArticleEditClose.SafeClick();
            ////articlesPage.ArticlesTableDiv.FindCellAndClick(0, 0);
            //Playback.Wait(5000);
            //articlesPage.ArticleButtonDelete.SafeClick();
            //articlesPage.ArticleDeleteConfirm.SafeClick();
            //articlesPage.DeleteArticleWindowAlert.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9487")]
        public void VerifyClassificationsCrudLocalization()
        {
            var language = "es_ES";
            var alias = "blaha";
            var pageTitle = "Classification Managemento";
            var nameLabel = "Classification Nameo";
            var nameValidation = "Classification Name Requiredo";
            var expectedCreatedSuccessAlert = "Classification '" + alias + "' created successfully";
            var expectedDeletedOkAlert = "Classification '" + alias + "' deleted successfully";

            VerifyCrudLocationnHelper(language, NavPages.Classification, ClassificationsPage.GetWindow(), pageTitle, nameValidation, nameLabel, expectedCreatedSuccessAlert, expectedDeletedOkAlert);
        }

        public void VerifyCrudLocationnHelper(string language, NavPages testPage, ICrudLocalizationPage pageObj, string pageTitle, string nameValidation, string nameLabel, string createdOkAlert, string deletedOkAlert)
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: language);

            string alias = "Testname";
            adminConsole.NavigateToPage(testPage);

            var page = ClassificationsPage.GetWindow();
//            pageObj.VerifyPageTitle(pageTitle);
            pageObj.ClickNewButton();
            page.VerifyNameLabel(nameLabel);
            page.ClickSaveButton();
            page.VerifyNameValidationIsDisplayed();
            page.SetName(nameLabel);
            page.ClickSaveButton();
            page.VerifyCreatedSuccessfulMessage(alias, createdOkAlert);
            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.VerifyNameLabel(nameLabel);
            page.ClickCancelButton();
            page.PageTableSelectRow(0);
            page.ClickDeleteButton();
            page.VerifyDeleteSuccessfulMessage(alias, deletedOkAlert);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9488")]
        public void VerifyCorrugatesCrudLocalization()
        {
            //var pageTitle = "Corrugateso";
            //var nameLabel = "Nameo";
            //var NameValidation = "Corrugate Name Requiredo";
            //var alias = "blergh";
            //var expectedCreatedOkAlert = "Corrugate '" + alias + "' created successfully";
            //var expectedDeletedOkAlert = "Corrugate '" + alias + "' deleted successfully";

//            VerifyCrudLocationnHelper("es_ES", NavPages.Corrugate, Corrugate.GetWindow(), pageTitle, NameValidation, nameLabel, expectedCreatedOkAlert, expectedDeletedOkAlert);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9490")]
        public void VerifyFusionMachinesCrudLocalization()
        {
            var pageTitleStr = "Machine Managemento";
            var nameEditLabelStr = "Fusion Machine Nameo";
            var toCreateName = "blergh";
            var expectedSuccessAlert = "Machine '" + toCreateName + "' created successfully"; //Machine 'blergh' created successfully
            var expectedDeleteAlert = "Machine '" + toCreateName + "' deleted successfully"; //Machine 'blergh' deleted successfully
            var discoverySaveBtnStr = "Saveo";
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
            adminConsole.NavigateToPage(NavPages.Machine);

            //verify page
            var page = new MachineManagementPage();
            Specify.That(page.PageTitle.InnerText == pageTitleStr).Should.BeTrue();

            //verify new fusion
            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.Fusion);
            page.VerifyNameLabel(nameEditLabelStr);
            page.SetName(toCreateName);
            page.SetIPAddress("127.0.0.1");
            page.SetMachinePort(8080);
            page.SetPhysicalMachineSettings("machineTheoretical_inches");
            page.ClickSaveButton();
            page.MachineTopAlert.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);

            //verify discovery
            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.Fusion);
            page.ClickDiscoveryButton();
            var discoPage = new MachineDiscoveryPage();
            discoPage.VerifySaveButtonText(discoverySaveBtnStr);
            discoPage.ClickCancelButton();
            Playback.Wait(5000);

            //verify Edit
            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.VerifyNameLabel(nameEditLabelStr);
            page.ClickCancelButton();

            //verify delete
            page.PageTableSelectRow(0);
            page.ClickDeleteButton();
            page.MachineTopAlert.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9490")]
        public void VerifyEmMachinesCrudLocalization()
        {
            var pageTitleStr = "Machine Managemento";
            var nameEditLabelStr = "EM Machine Nameo";
            var toCreateName = "blergh";
            var expectedSuccessAlert = "Machine '" + toCreateName + "' created successfully"; //Machine 'blergh' created successfully
            var expectedDeleteAlert = "Machine '" + toCreateName + "' deleted successfully"; //Machine 'blergh' deleted successfully
            var discoverySaveBtnStr = "Saveo";
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
            adminConsole.NavigateToPage(NavPages.Machine);

            //verify page
            var page = new MachineManagementPage();
            Specify.That(page.PageTitle.InnerText == pageTitleStr).Should.BeTrue();

            //verify new fusion
            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.EM);
            page.VerifyNameLabel(nameEditLabelStr);
            page.SetName(toCreateName);
            page.SetIPAddress("127.0.0.1");
            page.SetMachinePort(8080);
            page.SetPhysicalMachineSettings("EmPhysicalMachineSettings_inches.cfg");
            page.ClickSaveButton();
            page.VerifyCreatedSuccessfulMessage(toCreateName, expectedSuccessAlert);

            //verify discovery
            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.EM);
            page.ClickDiscoveryButton();
            var discoPage = new MachineDiscoveryPage();
            discoPage.VerifySaveButtonText(discoverySaveBtnStr);
            discoPage.ClickCancelButton();
            Playback.Wait(5000);

            //verify Edit
            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.VerifyNameLabel(nameEditLabelStr);
            page.ClickCancelButton();

            //verify delete
            page.PageTableSelectRow(0);
            page.ClickDeleteButton();
            page.MachineTopAlert.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9490")]
        public void VerifyZebraPrinterCrudLocalization()
        {
            var pageTitleStr = "Machine Managemento";
            var nameEditLabelStr = "Printer Nameo";
            var toCreateName = "blergh";
            var expectedSuccessAlert = "Machine '" + toCreateName + "' created successfully"; //Machine 'blergh' created successfully
            var expectedDeleteAlert = "Machine '" + toCreateName + "' deleted successfully"; //Machine 'blergh' deleted successfully
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");

            adminConsole.NavigateToPage(NavPages.Machine);

            //verify page
            var page = new MachineManagementPage();
            Specify.That(page.PageTitle.InnerText == pageTitleStr).Should.BeTrue();

            //verify new fusion
            page.ClickNewButton();
            page.SelectItemInNewMachineMenu(MachineType.ZebraPrinter);
            page.VerifyNameLabel(nameEditLabelStr);
            page.SetName(toCreateName);
            page.SetPrinterPort(9195);
            page.SetIPAddress("127.0.0.1");
            page.SetPrinterModel("Gx420T");
            page.ClickSaveButton();
            page.MachineTopAlert.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);

            //verify Edit
            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.VerifyNameLabel(nameEditLabelStr);
            page.ClickCancelButton();

            //verify delete
            page.PageTableSelectRow(0);
            page.ClickDeleteButton();
            page.MachineTopAlert.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        }

        //[TestMethod]
        //[TestCategory("UIAutomation")]
        //[TestCategory("9490")]
        //public void VerifyMachineGroupsCrudLocalization()
        //{
        //    var pageTitleStr = "Machine Group Managemento";
        //    var nameEditLabelStr = "Machine Group Nameo";
        //    var nameEditValidationStr = "Machine Group Name Requiredo";
        //    var toCreateName = "blergh";
        //    var expectedSuccessAlert = "Machine Group '" + toCreateName + "' created successfully";
        //    var expectedDeleteAlert = "Machine Group '" + toCreateName + "' deleted successfully";
        //    var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
        //    adminConsole.ConfigurationLink.SafeClick();
        //    adminConsole.MachineGroupsLink.SafeClick();
        //    var mgPage = new MachineGroupPage();
        //    Specify.That(mgPage.PageHeading.InnerText == pageTitleStr).Should.BeTrue();
        //    mgPage.NewButton.SafeClick();
        //    Specify.That(mgPage.AliasLabel.InnerText == nameEditLabelStr).Should.BeTrue();
        //    mgPage.SaveButton.SafeClick();
        //    Specify.That(mgPage.AliasValidation.InnerText == nameEditValidationStr).Should.BeTrue();
        //    mgPage.AliasText.SafeSetText(toCreateName);
        //    mgPage.WorkflowText.SafeSelectItem(MGWorkflows.CreateProducibleOnMachineGroup);
        //    mgPage.SaveButton.SafeClick();
        //    //mgPage.MachineGroupAlert.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);
        //    mgPage.MachineGroupTable.GetCheckBox(0).SafeClick();
        //    mgPage.EditButton.SafeClick();
        //    Specify.That(mgPage.AliasLabel.InnerText == nameEditLabelStr).Should.BeTrue();
        //    mgPage.CloseButton.SafeClick();
        //    mgPage.MachineGroupTable.GetCheckBox(0).SafeClick();
        //    mgPage.DeleteButton.SafeClick();
        //    //mgPage.MachineGroupAlert.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        //}

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9491")]
        public void VerifyProductionGroupsCrudLocalization()
        {
            var pageTitleStr = "Production Group Managemento";
            var nameEditLabelStr = "Production Group Nameo";
            var nameEditValidationStr = "Production Group Name Requiredo";
            var toCreateName = "blergh";
            var expectedSuccessAlert = "Production Group '" + toCreateName + "' created successfully";
            var expectedDeleteAlert = "Production Group '" + toCreateName + "' deleted successfully";
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var page = new ProductionGroupPage();
            page.VerifyPageTitle(pageTitleStr);
            page.ClickNewButton();
            page.VerifyNameLabel(nameEditLabelStr);
            page.ClickSaveButton();

            page.VerifyNameValidationIsDisplayed(true, nameEditValidationStr);
            page.SetName(toCreateName);
            page.SetProductionModeMenu(ProductionModesFriendlyNames.BoxFirst);
            page.ClickSaveButton();
            page.VerifyCreatedSuccessfulMessage(toCreateName, expectedSuccessAlert);
            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.VerifyNameLabel(nameEditValidationStr);
            page.ClickCancelButton();
            page.PageTableSelectRow(0);
            page.ClickDeleteButton();
            page.VerifyDeleteSuccessfulMessage(toCreateName, expectedDeleteAlert);
        }

        //[TestMethod]
        //[TestCategory("UIAutomation")]
        //[TestCategory("9492")]
        //public void VerifyPickZonesCrudLocalization()
        //{
        //    var pageTitleStr = "Pick Zoneso";
        //    var nameEditLabelStr = "Nameo";
        //    var nameEditValidationStr = "Pick Zone Name Requiredo";
        //    var toCreateName = "blergh";
        //    var expectedSuccessAlert = "Pick Zone '" + toCreateName + "' created successfully";
        //    var expectedDeleteAlert = "Pick Zone '" + toCreateName + "' deleted successfully";
        //    var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
        //    adminConsole.ConfigurationLink.SafeClick();
        //    adminConsole.PickZoneLink.SafeClick();
        //    var pgPage = new PickZonesPage();
        //    Specify.That(pgPage.PageHeading.InnerText == pageTitleStr).Should.BeTrue();
        //    pgPage.NewButton.SafeClick();
        //    Specify.That(pgPage.PickZoneNameLabel.InnerText == nameEditLabelStr).Should.BeTrue();
        //    pgPage.SaveButton.SafeClick();
        //    Specify.That(pgPage.PickZoneNameValidation.InnerText == nameEditValidationStr).Should.BeTrue();
        //    pgPage.PickZoneNameEdit.SafeSetText(toCreateName);
        //    pgPage.SaveButton.SafeClick();
        //    pgPage.AlertWidget.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);
        //    pgPage.PickZoneTable.GetCheckBox(0).SafeClick();
        //    pgPage.EditButton.SafeClick();
        //    Specify.That(pgPage.PickZoneNameLabel.InnerText == nameEditLabelStr).Should.BeTrue();
        //    pgPage.CancelButton.SafeClick();
        //    pgPage.PickZoneTable.GetCheckBox(0).SafeClick();
        //    pgPage.DeleteButton.SafeClick();
        //    pgPage.AlertWidget.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        //}

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9494")]
        public void VerifyAdminSettingsLocalization()
        {
            var pageTitleStr = "Server Settingso";
            var generalTab = "Generalo";
            var callback = "Callback IP Address/DNS Nameo";
            //var expectedSuccessAlert = "Server settings updated successfully";
            var fileImportTab = "File Importo";
            var defaultImportType = "Default Import Typeo";
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
            adminConsole.NavigateToPage(NavPages.Settings);
            var sPage = new ServerSettingsPage();
            Specify.That(sPage.PageHeader.InnerText == pageTitleStr).Should.BeTrue();
            Specify.That(sPage.GeneralTab.InnerText == generalTab).Should.BeTrue();
            Specify.That(sPage.CallbackIpAddressLabel.InnerText == callback).Should.BeTrue();
            Specify.That(sPage.FileImportTab.InnerText == fileImportTab).Should.BeTrue();
            sPage.FileImportTab.SafeClick();
            Specify.That(sPage.DefaultImportTypeLabel.InnerText == defaultImportType).Should.BeTrue();
            //sPage.SaveButton.SafeClick();
            //sPage.Alerts.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);
        }

        //[TestMethod]
        //[TestCategory("UIAutomation")]
        //[TestCategory("9493")]
        //public void VerifyUsersCrudLocalization()
        //{
        //    var pageTitleStr = "User Managemento";
        //    var nameEditLabelStr = "Usernameo";
        //    var nameEditValidationStr = "Username requiredo";
        //    var toCreateName = "blergh";
        //    var expectedSuccessAlert = "User '" + toCreateName + "' created successfully";
        //    var expectedDeleteAlert = "User '" + toCreateName + "' has been deleted";
        //    var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
        //    adminConsole.ConfigurationLink.SafeClick();
        //    adminConsole.UsersLink.SafeClick();
        //    var uPage = new UserManagement();
        //    Specify.That(uPage.PageHeading.InnerText == pageTitleStr).Should.BeTrue();
        //    uPage.NewUserButton.SafeClick();
        //    Specify.That(uPage.NewUserUserNameLabel.InnerText == nameEditLabelStr).Should.BeTrue();
        //    uPage.NewUserSaveButton.SafeClick();
        //    Specify.That(uPage.NewUserUserNameValidation.InnerText == nameEditValidationStr).Should.BeTrue();
        //    uPage.NewUserUserNameEdit.SafeSetText(toCreateName);
        //    uPage.NewUserPasswordEdit.SafeSetText(toCreateName + "pw");
        //    uPage.NewUserPasswordConfirmEdit.SafeSetText(toCreateName + "pw");
        //    uPage.NewUserSaveButton.SafeClick();
        //    uPage.Alerts.VerifyAlert(true, AlertStates.Success, expectedSuccessAlert);
        //    uPage.UsersTable.GetCheckBox(0).SafeClick();
        //    uPage.EditUserButton.SafeClick();
        //    Specify.That(uPage.NewUserUserNameLabel.InnerText == nameEditLabelStr).Should.BeTrue();
        //    uPage.NewUserCancelButton.SafeClick();
        //    uPage.UsersTable.GetCheckBox(0).SafeClick();
        //    uPage.DeleteUserButton.SafeClick();
        //    uPage.Alerts.VerifyAlert(true, AlertStates.Success, expectedDeleteAlert);
        //}

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9498")]
        public void VerifyOperatorPanelCustomJobFromArticleLocalization()
        {
//            var pageTitleStr = "Create Job From Articleso";
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg2_fm1zp1.Alias).Id;
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(null, language: "es_ES");
            adminConsole.SelectMachineGroup(mg1Guid); //todo: fix change machines page loading blank
            adminConsole.NavigateToPage(NavPages.CustomJobFromArticle);


            // not complete!! pageTitleStr above looks for a string that is not an existing language
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9499")]
        public void VerifyOperatorPanelCustomJobLocalization()
        {
            //var pageTitleStr = "Create Custom Job For Machine Groupo ''";
            //var quantityStr = "Quantityo";
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg2_fm1zp1.Alias).Id;
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(language: "es_ES");
            adminConsole.SelectMachineGroup(mg1Guid);
            adminConsole.NavigateToPage(NavPages.CustomJob);

            var aPage = new CustomJobPage();
            //Specify.That(aPage.PageHeading.InnerText == pageTitleStr).Should.BeTrue();
            //Specify.That(aPage.QuantityLabel.InnerText == quantityStr).Should.BeTrue();
        }
    }
        #endregion
}
