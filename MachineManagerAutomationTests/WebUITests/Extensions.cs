﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using CUITe.Controls;
using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;

using MachineManagerAutomationTests.WebUITests.Repository;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;

using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests
{
    public static class Extensions
    {
        /// <summary>
        /// Looks at the controls under the parent for an item with the css class... This is pretty much a hack. Be careful that you only use it on a very limited scope where you know only one item with the css class should exists
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="control"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public static bool ContainsCSS<T>(this CUITe_HtmlControl<T> control, string className)
            where T : HtmlControl
        {
            return control.UnWrap().Class.Contains(className);
        }

        public static bool HasFocus<T>(this CUITe_ControlBase<T> control) where T : HtmlControl
        {

            var unwrapped = control.UnWrap();
            Retry.For(() => unwrapped.HasFocus, TimeSpan.FromSeconds(5));
            return unwrapped.HasFocus;
        }

        public static void VerifyAlert(this AlertWidget alert, bool expectedEnableStatus, AlertStates status = AlertStates.NotSet, string expectedMessage = "")
        {
            if (expectedEnableStatus)
                Retry.For(() => Specify.That(alert.AlertDiv.Enabled).Should.BeTrue("Alert didn't show up"), TimeSpan.FromSeconds(10));
            else
                Specify.That(alert.AlertDiv.Exists).Should.BeFalse("Alert is visible");

            switch (status)
            {
                case AlertStates.Error:
                    Specify.That(alert.HasError).Should.BeTrue("Alert should be an error");
                    break;
                case AlertStates.Success:
                    Specify.That(alert.HasSuccess).Should.BeTrue("Alert should be a success");
                    break;
                case AlertStates.Warn:
                    Specify.That(alert.HasWarning).Should.BeTrue("Alert should be a warning");
                    break;
                case AlertStates.Danger:
                    Specify.That(alert.HasDanger).Should.BeTrue("Alert should be a danger");
                    break;
            }

            if (!String.IsNullOrEmpty(expectedMessage))
                Specify.That(alert.AlertText).Should.Contain(expectedMessage, "Failed to set the correct alert msg");
        }

        public static void Clear(this CUITe_HtmlEdit edit)
        {
            var text = edit.GetText();
            edit.SetFocus();
            var unwrapped = edit.UnWrap();
            foreach (var c in text)
            {
                Keyboard.SendKeys(unwrapped, "{BACK}");
            }
        }

        public static void SafeClick(this CUITe_HtmlButton btn, int waitSeconds = 10)
        {
            Retry.For(() => btn.Exists && btn.Enabled, TimeSpan.FromSeconds(waitSeconds));
            btn.Click();
        }

        public static void SafeClick(this CUITe_HtmlHyperlink link, int waitSeconds = 10)
        {
            Retry.For(() => link.Exists && link.Enabled, TimeSpan.FromSeconds(waitSeconds));
            link.Click();
        }

        public static void SafeClick(this CUITe_HtmlListItem li, int waitSeconds = 10)
        {
            Retry.For(() => li.Exists && li.Enabled, TimeSpan.FromSeconds(waitSeconds));
            li.Click();
        }

        public static void SafeClick(this CUITe_HtmlDiv div, int waitSeconds = 10)
        {
            Retry.For(() => div.Exists && div.Enabled, TimeSpan.FromSeconds(waitSeconds));
            div.Click();
        }

        public static void SafeClick(this CUITe_HtmlEdit input, int waitSeconds = 10)
        {
            Retry.For(() => input.Exists && input.Enabled, TimeSpan.FromSeconds(waitSeconds));
            input.Click();
        }

        public static void SafeClick(this CUITe_HtmlSpan span, int waitSeconds = 10)
        {
            Retry.For(() => span.Exists && span.Enabled, TimeSpan.FromSeconds(waitSeconds));
            span.Click();
        }

        public static void SafeClick(this CUITe_HtmlLabel label, int waitSeconds = 10)
        {
            Retry.For(() => label.Exists && label.Enabled, TimeSpan.FromSeconds(waitSeconds));
            label.Click();
        }

        public static void SafeClick(this CUITe_HtmlImage img, int waitSeconds = 10)
        {
            Retry.For(() => img.Exists && img.Enabled, TimeSpan.FromSeconds(waitSeconds));
            img.Click();
        }

        public static void SafeClick(this CUITe_HtmlTextArea textArea, int waitSeconds = 10)
        {
            Retry.For(() => textArea.Exists && textArea.Enabled, TimeSpan.FromSeconds(waitSeconds));
            textArea.Click();
        }

        public static void SafeClick(this CUITe_HtmlCheckBox cbx, int waitSeconds = 10)
        {
            Retry.For(() => cbx.Exists && cbx.Enabled, TimeSpan.FromSeconds(waitSeconds));
            cbx.Click();
        }

        public static void SafeClick(this CUITe_HtmlComboBox cbx, int waitSeconds = 10)
        {
            Retry.For(() => cbx.Exists && cbx.Enabled, TimeSpan.FromSeconds(waitSeconds));
            cbx.Click();
        }

        public static void SafeCheck(this CUITe_HtmlCheckBox cbx, bool check = true, int waitSeconds = 10)
        {
            Retry.For(() => cbx.Exists && cbx.Enabled, TimeSpan.FromSeconds(waitSeconds));
            if (check)
                cbx.Check();
            else
                cbx.UnCheck();
        }

        public static void SafeSelectItem(this CUITe_HtmlComboBox cbx, string item, int waitSeconds = 10)
        {
            Retry.For(() => cbx.Exists && cbx.Enabled, TimeSpan.FromSeconds(waitSeconds));
            Retry.For(() => cbx.SelectItem(item), TimeSpan.FromSeconds(waitSeconds));
        }

        public static void SafeSelectItem(this CUITe_HtmlComboBox cbx, int itemIndex, int waitSeconds = 10)
        {
            Retry.For(() => cbx.Exists && cbx.Enabled, TimeSpan.FromSeconds(waitSeconds));
            Retry.For(() => cbx.SelectItem(itemIndex), TimeSpan.FromSeconds(waitSeconds));
        }

        public static void SafeSetText(this CUITe_HtmlEdit edit, string text, int waitSeconds = 10)
        {
            Retry.For(() => edit.Exists && edit.Enabled, TimeSpan.FromSeconds(waitSeconds));
            Retry.For(() => edit.SetText(text), TimeSpan.FromSeconds(waitSeconds));
        }

        public static void SafeSetText(this CUITe_HtmlEdit edit, double text, int waitSeconds = 10)
        {
            edit.SafeSetText(text.ToString(CultureInfo.InvariantCulture), waitSeconds);
        }

        public static void SafeSetText(this CUITe_HtmlTextArea edit, string text, int waitSeconds = 10)
        {
            if (!string.IsNullOrEmpty(text))
            {
                Retry.For(() => edit.Exists, TimeSpan.FromSeconds(waitSeconds));
                edit.SetText(text);
            }
        }

    }
}
