﻿using MachineManagerAutomationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UITesting;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class CustomJobFromArticlePage : WindowBase
    {
        // list
        private Table _articlesList = null;
        public Table ArticlesTable
        {
            get
            {
                var ctrl = Get<CUITe_HtmlDiv>("id=articlesList");
                return _articlesList ?? (_articlesList = new Table(ctrl,new TableConfigData(TableTypes.TwoColumn, "Articles")));
            }
        }
        
        public void SetQuantity(int rowIndex, int value)
        {
            var cuite_row = ArticlesTable.FindRow(rowIndex);
            var cuite_cell = cuite_row.Get<CUITe_HtmlDiv>("class~ui-grid-coluiGrid-0009");
            var cuite_view = (CUITe_HtmlDiv)cuite_cell.FirstChild;
            cuite_view.DoubleClick();

            var cu_row = Table.FindRowsWithCodedUI(this, "articlesList")[rowIndex];
            var cu_cell = TestUtilities.GetUITestControl(cu_row, "ui-grid-cell ng-scope ui-grid-coluiGrid-0009", "Class");
            var cu_view = ((HtmlDiv)cu_cell.GetChildren()[0]);

            // not working
            cu_view.SetProperty("InnerText", "2");
        }

        // buttons
        private CUITe_HtmlButton ProduceButton { get { return Get<CUITe_HtmlButton>("id=produceArticle"); } }

        public void PageTableSelectRow(int rowIndex)
        {
            ArticlesTable.SelectRow(rowIndex);
        }

        // button actions
        public void ClickProduceButton()
        {
            ProduceButton.Click();
        }

        public static CustomJobFromArticlePage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<CustomJobFromArticlePage>();
        }
    }
}
