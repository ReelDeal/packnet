﻿using System;
using System.Collections.Generic;
using CUITe.Controls.HtmlControls;
using PackNet.Common.Utils;
using PackNet.Common.Interfaces.DTO.PickZones;
using Testing.Specificity;
using PackNet.Common.Interfaces.Enums;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public enum PickZoneStatus
    {
        Surge,
        Normal,
        Stop
    }

    public class PickZonesWidget : WindowBase
    {
        public bool GetStatus(PickZone pz, PickZoneStatus status)
        {
            return GetStatus(pz.Alias, status);
        }

        public bool GetStatus(string alias, PickZoneStatus status)
        {
            if (status == PickZoneStatus.Stop)
                return GetStopButton(alias).IsSelected;

            return GetNormalButton(alias).IsSelected;
        }

        public void SetStatus(PickZone pz, PickZoneStatus status)
        {
            SetStatus(pz.Alias, status);
        }

        public void SetStatus(string alias, PickZoneStatus status)
        {
            CUITe_HtmlRadioButton rbtn = null;
            if (status == PickZoneStatus.Stop)
                rbtn = GetStopButton(alias);
            else
                rbtn = GetNormalButton(alias);

            if (rbtn.Exists && rbtn.Enabled)
                Retry.For(rbtn.Click, TimeSpan.FromSeconds(2));

            Specify.That(rbtn.IsSelected).Should.BeEqualTo(true);

        }

        public void VerifyNumberOfRequests(PickZone pz, int noOfRequests)
        {
            Specify.That(GetNumberOfRequests(pz)).Should.BeEqualTo(noOfRequests);
        }

        public string GetNumberOfRequests(PickZone pz)
        {
            return GetNumberOfRequests(pz.Alias);
        }

        public string GetNumberOfRequests(string alias)
        {
            var ctrl = Get<CUITe_HtmlLabel>("id=pickzone-NumberOfRequests-" + alias);
            Specify.That(ctrl.Exists && ctrl.Exists).Should.BeTrue();
            return ctrl.InnerText.Trim();
        }

        public void VerifyStatus(PickZone c, PickZoneStatus status, bool value)
        {
            Specify.That(GetStatus(c, status)).Should.BeEqualTo(value);
        }

        #region internal
        private CUITe_HtmlRadioButton GetStopButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=pickzone-stop-button-" + alias);
        }

        private CUITe_HtmlRadioButton GetNormalButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=pickzone-normal-button-" + alias);
        }

        private CUITe_HtmlRadioButton GetSurgeButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=pickzone-surge-button-" + alias);
        }
        #endregion
    }
}




