﻿//using CUITe.Controls.HtmlControls;

//namespace MachineManagerAutomationTests.WebUITests.Repository
//{
//    using System.Linq;

//    using Testing.Specificity;

//    public class OperatorConsoleCustomJobDialog : DynamicBrowserWindowWithLogin
//    {

//        public OperatorConsoleCustomJobDialog() : base("Packnet.Server") { }

//        public OperatorConsoleCustomJobDialog(string title) : base(title) { }

//        public static string pageUrl = @"index.html#/OperatorConsole/CreateCustomJob";

//        public CUITe_HtmlDiv Dialog
//        {
//            get { return Get<CUITe_HtmlDiv>("id=OperatorConsoleChangeProductionGroupsDialog"); }
//        }

//        public CUITe_HtmlEdit QuantityEdit
//        {
//            get { return Get<CUITe_HtmlEdit>("id=nudInputText"); }
//        }

//        public CUITe_HtmlButton IncrementButton
//        {
//            get { return Get<CUITe_HtmlButton>("id=nudIncrementButton"); }
//        }

//        public CUITe_HtmlButton DecrementButton
//        {
//            get { return Get<CUITe_HtmlButton>("id=nudDecrementButton"); }
//        }

//        public CUITe_HtmlComboBox DesignList
//        {
//            get { return Get<CUITe_HtmlComboBox>("name=DesignId"); }
//        }

//        public CUITe_HtmlEdit LengthEdit
//        {
//            get { return Get<CUITe_HtmlEdit>("name=Length"); }
//        }

//        public CUITe_HtmlEdit WidthEdit
//        {
//            get { return Get<CUITe_HtmlEdit>("name=Width"); }
//        }

//        public CUITe_HtmlEdit HeightEdit
//        {
//            get { return Get<CUITe_HtmlEdit>("name=Height"); }
//        }

//        public CUITe_HtmlEdit CorrugateQualityEdit
//        {
//            get { return Get<CUITe_HtmlEdit>("name=CorrugateQuality"); }
//        }

//        public CUITe_HtmlButton ProduceButton
//        {
//            get { return Get<CUITe_HtmlButton>("id=produce"); }
//        }

//        public CUITe_HtmlButton CancelButton
//        {
//            get { return Get<CUITe_HtmlButton>("id=cancel"); }
//        }

//        public void SetDesign(string design)
//        {
//            DesignList.Click();
//            var designs = (from i in DesignList.GetChildren() select i as CUITe_HtmlCustom).ToList();
//            Specify.That(designs.Any()).Should.BeTrue("No designs found");
//            DesignList.SelectItem(design);
//        }

//        public AlertWidget ProduceCustomJobWindowAlert
//        {
//            get { return Get<AlertWidget>("id=produceCustomJobAlert"); }
//        }

//        public CUITe_HtmlHeading4 PageHeading { get { return Get<CUITe_HtmlHeading4>("id=CustomJobPageHeader"); } }
//        public CUITe_HtmlLabel QuantityLabel { get { return Get<CUITe_HtmlLabel>("id=quantityLabel"); } }

//        public CUITe_HtmlDiv DesignWraper { get { return Get<CUITe_HtmlDiv>("id=designWrapper"); } }
//        public CUITe_HtmlDiv PreviewContainer { get { return Get<CUITe_HtmlDiv>("id=previewContainer"); } }
//        public CUITe_HtmlButton PreviewHideButton { get { return Get<CUITe_HtmlButton>("id=hidePreview"); } }
//    }
//}
