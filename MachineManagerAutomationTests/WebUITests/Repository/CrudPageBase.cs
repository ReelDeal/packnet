﻿using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UITesting;
using PackNet.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class CrudPageBase : WindowBase
    {
        public CrudPageBase()
            : base("Packnet.Server")
        { }

        public CrudPageBase(string title)
            : base(title)
        { }

        protected void VerifyValidationIsDisplayed(CUITe_HtmlParagraph validation, string validationName, bool shouldBeDisplayed = true, string expectedText = null)
        {
            if (shouldBeDisplayed)
            {
                if (string.IsNullOrEmpty(expectedText))
                    expectedText = "Required";

                Specify.That(validation.Exists).Should.BeTrue(validationName + " validation was not displayed");
                Specify.That(validation.InnerText).Should.BeEqualTo(expectedText, string.Format("{0} validation should have value '{1}'", validationName,  expectedText));
            }
            else
            {
                TestUtilities.VerifyHtmlElementIsDisplayed(this, validation.UnWrap().Id, string.Format("'{0} validation should not be displayed", validationName), false);
            }
        }
        protected void VerifyDuplicateAliasMessage(AlertWidget widget, string objectName, string alias)
        {
            TestUtilities.VerifyAlert(widget, alias,
                string.Format("{0} '{1}' already exists.", objectName, alias),
                objectName + " duplicate alias notification was not shown.", true);
        }
        /// <summary>
        /// Verify that the edit page is open or closed. 
        /// </summary>
        /// <param name="cancelButton">Must be supplied to verify if edit form is opened</param>
        /// <param name="newButton">Must be supplied to verify if the edit form is closed. Parameter 'shouldBeOpen' must be set to 'false'.</param>
        /// <param name="shouldBeOpen">Set to 'true' if testing if edit form is open, set to 'false' if test if edit form is closed. Default is 'true'.</param>
        protected void VerifyEditPageIsOpen(CUITe_HtmlButton cancelButton, CUITe_HtmlButton newButton, bool shouldBeOpen = true)
        {
            if (shouldBeOpen)
            {
                Specify.That(cancelButton).Should.Not.BeNull("Edit page was not displayed.");
                Specify.That(cancelButton.Exists).Should.BeTrue("Edit page was not displayed.");
            }
            else
            {
                Specify.That(newButton).Should.Not.BeNull("Edit page was not closed.");
                Specify.That(newButton.Exists).Should.BeTrue("Edit page was not closed.");
            }
        }
        /// <summary>
        /// Check that the validation message is shown correctly
        /// </summary>
        /// <param name="widget">The alert widget showing the message</param>
        /// <param name="objectName">The name of the object, for example Corrugate, Machine Group or Pick Zone</param>
        /// <param name="alias">The alias of the object to be included in the message</param>
        /// <param name="expectedText">When we validate languages, use this parameter with the complete text to validate</param>
        protected void VerifyUpdatedSuccessfulMessage(AlertWidget widget, string objectName, string alias, string expectedText = null)
        {
            if (string.IsNullOrEmpty(expectedText))
                expectedText = string.Format("{0} '{1}' updated successfully.", objectName, alias);

            TestUtilities.VerifyAlert(widget, alias, expectedText,
                objectName + " updated notification was not shown.", false);
        }
        protected void VerifyButtonIsEnabled(CUITe_HtmlButton btn, string buttonName, bool enabled = true)
        {
            if (enabled)
                Specify.That(btn.Exists && btn.Enabled).Should.BeTrue("The " + buttonName + " button should exists and be enabled.");
            else
                Specify.That(btn.Exists && !btn.Enabled).Should.BeTrue("The " + buttonName + " button should be disabled");
        }
        /// <summary>
        /// Check that the validation message is shown correctly
        /// </summary>
        /// <param name="widget">The alert widget showing the message</param>
        /// <param name="objectName">The name of the object, for example Corrugate, Machine Group or Pick Zone</param>
        /// <param name="alias">The alias of the object to be included in the message</param>
        /// <param name="expectedText">When we validate languages, use this parameter with the complete text to validate</param>
        protected void VerifyCreatedSuccessfulMessage(AlertWidget widget, string objectName, string alias, string expectedText = null)
        {
            if (string.IsNullOrEmpty(expectedText))
                expectedText = string.Format("{0} '{1}' created successfully.", objectName, alias);

            // Verify that the message exists...
            Specify.That(widget.AlertText).Should.BeEqualTo(expectedText);
            Specify.That(widget.Exists).Should.BeTrue();

            // ...and that it goes away
            // TODO: this does not work if the control has disappeared, which is the purpose of this test!!
            //Retry.For(() => !widget.Exists, TimeSpan.FromSeconds(10));
            //Specify.That(widget.Exists).Should.BeFalse();
        }
        protected void VerifyTableRowContainsSpecifiedData(int row, Table table, string alias, string description = null, string number = null)
        {
            Specify.That(table.GetCellValue(row, 0)).Should.BeEqualTo(alias);
            if (description != null)
            {
                Specify.That(table.GetCellValue(row, 1)).Should.BeEqualTo(description);
            }
            if (!string.IsNullOrEmpty(number))
            {
                Specify.That(table.GetCellValue(row, 1)).Should.BeEqualTo(number);
            }
        }
        /// <summary>
        /// Check that the validation message is shown correctly
        /// </summary>
        /// <param name="widget">The alert widget showing the message</param>
        /// <param name="objectName">The name of the object, for example Corrugate, Machine Group or Pick Zone</param>
        /// <param name="alias">The alias of the object to be included in the message</param>
        /// <param name="expectedText">When we validate languages, use this parameter with the complete text to validate</param>
        protected void VerifyDeleteSuccessfulMessage(AlertWidget widget, string objectName, string alias, string expectedText = null)
        {
            if(string.IsNullOrEmpty(expectedText))
                expectedText = string.Format("{0} '{1}' deleted successfully.", objectName, alias);

            TestUtilities.VerifyAlert(widget, alias, expectedText, objectName + " deleted notification was not shown.", false);
        }
        protected void VerifyTableIsEmpty(Table table, string objectName, int remainingNumberOfRecords = 0)
        {
            Specify.That(table.RowCount()).Should.BeEqualTo(remainingNumberOfRecords, objectName + " table is not empty");
        }
        protected void VerifyNameLabel(CUITe_HtmlLabel nameLabel, string expectedValue)
        {
            Specify.That(nameLabel.InnerText == expectedValue).Should.BeTrue(string.Format("The name label has incorrect value. Actual: {0} Expected: {1}", nameLabel.InnerText, expectedValue));
        }

        protected void VerifyPageTitle(string actualValue, string expectedValue)
        {
            Specify.That(actualValue == expectedValue).Should.BeTrue("Page title is not correct. Actual: {0} Expected: {1}", actualValue, expectedValue);
        }

        public void VerifyValidationExists(string idPropertyName, string validationName, bool shouldExist = true)
        {
            var validation = TestUtilities.GetUITestControl(this, idPropertyName);

            if(shouldExist)
                Specify.That(validation.Exists).Should.BeTrue(string.Format("'{0}' validation should exist but it doesn't.", validationName));
            else
                Specify.That(validation.Exists).Should.BeFalse(string.Format("'{0}' validation should NOT exist but it does.", validationName));
        }

    }
    public interface ICrudLocalizationPage
    {
        void ClickNewButton();
        void VerifyNameLabel(string nameLabel);
        void ClickSaveButton();
        void SetName(string nameLabel);
        void VerifyNameValidationIsDisplayed(bool shouldExist = true, string expectedValue = null);
        void VerifyNameValidationExists(bool shouldExist = true);
        void VerifyCreatedSuccessfulMessage(string alias, string expectedText = null);
        void PageTableSelectRow(int rowIndex);
        void ClickEditButton();
        void ClickCancelButton(bool doubleClick = false);
        void ClickDeleteButton();
        void VerifyDeleteSuccessfulMessage(string alias, string expectedText = null);
    }
}

