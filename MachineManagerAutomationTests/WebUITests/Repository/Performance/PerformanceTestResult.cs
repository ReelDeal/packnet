﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;

namespace MachineManagerAutomationTests.WebUITests.Repository.Performance
{
    public class PerformanceTestResult : IPersistable
    {
        public Guid Id { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Description { get; set; }
        public SelectionAlgorithmTypes SelectionAlgorithm { get; set; }
        public IDictionary<string, TimeSpan> Tests { get; set; }

        public PerformanceTestResult()
        {
            TimeStamp = DateTime.Now;
            Tests = new Dictionary<string, TimeSpan>();
        }

        public override string ToString()
        {
            var str = Description + " | Run on " + TimeStamp.ToShortDateString() + " at " + TimeStamp.ToShortTimeString() + Environment.NewLine;
            foreach (var test in Tests)
                str += test.Key + " " + test.Value + Environment.NewLine;
            return str;
        }
    }
}
