﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;

namespace MachineManagerAutomationTests.WebUITests.Repository.Performance
{
    public interface IPerformanceTestResultRepository : IRepository<PerformanceTestResult>
    {
        IEnumerable<PerformanceTestResult> FindBySelectionAlgorithm(SelectionAlgorithmTypes algorithm);
    }
}