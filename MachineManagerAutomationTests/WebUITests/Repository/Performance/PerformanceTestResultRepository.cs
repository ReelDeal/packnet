﻿using System.Collections.Generic;
using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;

namespace MachineManagerAutomationTests.WebUITests.Repository.Performance
{
    public class PerformanceTestResultRepository : MongoDbRepository<PerformanceTestResult>, IPerformanceTestResultRepository
    {
        public PerformanceTestResultRepository() : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "PerformanceTestResults")
        { }

        public IEnumerable<PerformanceTestResult> FindBySelectionAlgorithm(SelectionAlgorithmTypes algorithm)
        {
            return Collection.AsQueryable().Where(r => r.SelectionAlgorithm == algorithm).OrderBy(r => r.TimeStamp);
        }
    }
}
