﻿using System;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;

using Testing.Specificity;
using PackNet.Common.Interfaces.Enums;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;
using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public enum ProductionMode
    {
        Auto,
        Manual
    }

    public enum UserType
    {
        Administrator,
        Operator
    }

    public enum CompletedItemsSortBy
    {
        LastChangedDate,
        OrderId
    }

    public class MachineProductionPage : WindowBase
    {
        private SelectionAlgorithmTypes _algorithmType = null;
        private ProductionMode _prodMode = ProductionMode.Auto;
        private UserType _userType = UserType.Administrator;

        #region Constructors
        public MachineProductionPage()
        {
        }
        public MachineProductionPage(ProductionMode mode, SelectionAlgorithmTypes selectionAlgorithType, UserType userType)
            : base("Packnet.Server")
        {
            _algorithmType = selectionAlgorithType;
            _prodMode = mode;
            _userType = userType;
        }
        #endregion

        // ---------------------------------
        // tables
        // ---------------------------------
        //autoOperatorPanelJobsSent
        //autoOperatorPanelCompletedJobs
        public TableConfigData GetTableConfig(TableTypes tableType)
        {
            TableConfigData config = new TableConfigData();
            config.TableType = tableType;

            if (tableType == TableTypes.ProductionOpen)
            {
            }
            else if (tableType == TableTypes.ProductionInProgress)
            {
                config.TableName = "ItemsInProduction";
                config.TableId = _prodMode.ToString().ToLower() + "OperatorPanelJobsSent";
            }
            else if (tableType == TableTypes.ProductionCompleted)
            {
                config.TableId = _prodMode.ToString().ToLower() + "OperatorPanelCompletedJobs";
                config.ReproduceButtonId = "completedJobReproduceButton";
                config.TableName = "CompletedItems";
            }

            //if (_prodMode == ProductionMode.Manual && _algorithmType == SelectionAlgorithmTypes.Order)
            //{
            //    if (tableType == TableTypes.ProductionOpen)
            //    {
            //        config.TableName = "OpenOrders";
            //        config.TableId = "ordersListDiv";
            //        config.RowName = "orderRowContainer";
            //        config.RowAsIdOrClass = "class";
            //        config.ReproduceButtonId = "orderReproduce";
            //        config.CheckBoxClassName = "orderRowCheckbox";
            //    }
            //    else if (tableType == TableTypes.ProductionInProgress)
            //    {
            //        config.TableName = "ItemsInProduction";
            //        config.TableId = _prodMode.ToString().ToLower() + "OperatorPanelJobsSent";
            //    }
            //    else
            //    {
            //        config.TableId = "ordersHistory";
            //        config.RowName = "";
            //        config.RowAsIdOrClass = "class";
            //        config.ReproduceButtonId = "orderReproduce";
            //    }
            //}
            //else if (_prodMode == ProductionMode.Auto)
            //{
            //    if (tableType == TableTypes.ProductionOpen)
            //    {
            //        config.TableId = "autoOperatorPanelJobsSent";
            //    }
            //    else if (tableType == TableTypes.ProductionCompleted)
            //    {
            //        config.TableId = "autoOperatorPanelCompletedJobs";
            //        config.ReproduceButtonId = "completedJobReproduceButton";
            //    }
            //}
            
            return config;
        }

        #region Tables

        // open orders table
        private Table _openOrdersTable = null;
        public Table OpenOrdersTable
        {
            get
            {
                if (_openOrdersTable == null)
                {
                    var config = GetTableConfig(TableTypes.ProductionOpen);
                    var ctrl = Get<CUITe_HtmlDiv>("id=" + config.TableId);
                    _openOrdersTable = new Table(ctrl, config);
                }
                return _openOrdersTable;
            }
        }

        private Table _itemsInProductionTable = null;
        public Table ItemsInProductionTable
        {
            get
            {
                if (_itemsInProductionTable == null)
                {
                    var config = GetTableConfig(TableTypes.ProductionInProgress);
                    var ctrl = Get<CUITe_HtmlDiv>("id=" + config.TableId);
                    _itemsInProductionTable = new Table(ctrl, config);
                }
                return _itemsInProductionTable;
            }
        }

        private Table _machineProductionTable = null;
        public Table MachineProductionTable
        {
            get
            {
                if (_machineProductionTable == null)
                {
                    var config = GetTableConfig(TableTypes.MachineProductionTable);
                    var ctrl = Get<CUITe_HtmlDiv>("id=" + config.TableId);
                    _machineProductionTable = new Table(ctrl, config);
                }
                return _machineProductionTable;
            }
        }

        // completed jobs table
        private Table _completedJobs = null;
        public Table CompletedItemsTable
        {
            get
            {
                if (_completedJobs == null)
                {
                    var config = GetTableConfig(TableTypes.ProductionCompleted);
                    var ctrl = Get<CUITe_HtmlDiv>("id=" + config.TableId);
                    _completedJobs = new Table(ctrl, config);
                }
                return _completedJobs;
            }
        }

        #endregion

        #region Buttons
        public void ClickPlayButton()
        {
            var btn = Get<CUITe_HtmlLabel>("id=machinePlay");
            Specify.That(btn.Enabled).Should.BeTrue("Play button is not available.");
            btn.Click();
        }
        /// <summary>
        /// Clicks auto and play button. Same as running methods ClickAutoButton and ClickPlayButton separatly.
        /// </summary>
        public void ClickAutoAndPlayButtons()
        {
            ClickAutoButton();
            ClickPlayButton();
        }
        /// <summary>
        /// Clicks auto and play button. Same as running methods ClickManualButton and ClickPlayButton separatly.
        /// </summary>
        public void ClickManualAndPlayButtons()
        {
            ClickManualButton();
            ClickPlayButton();
        }
        public void ClickPauseButton()
        {
            var btn = Get<CUITe_HtmlLabel>("id=machinePause");
            btn.Click();
        }
        public void ClickAutoButton()
        {
            var btn = Get<CUITe_HtmlLabel>("id=machineAuto");
            Retry.For(() => btn.Enabled, TimeSpan.FromSeconds(10));
            btn.Click();
        }
        public void ClickManualButton()
        {
            var btn = Get<CUITe_HtmlLabel>("id=machineManual");
            btn.Click();
        }
        public void ClickMoreInfoButton()
        {
            var btn = Get<CUITe_HtmlHyperlink>("id=MoreInfoToggle");
            btn.Click();
        }
        public void ClickReproduceOkButton()
        {
            var btn = Get<CUITe_HtmlButton>("id=reproduceButton");
            Specify.That(btn.Exists && btn.Enabled).Should.BeTrue("ClickReproduceOkButton can't be found.");
            btn.Click();
        }

        public void ClickCompletedJobReproduceButton(int rowIndex, bool confirm = true)
        {
            var row = CompletedItemsTable.FindRow(rowIndex);
            var btn = row.Get<CUITe_HtmlButton>("id=completedJobReproduceButton");
            Specify.That(btn.Exists && btn.Enabled).Should.BeTrue("CompletedJobReproduceButton can't be found.");
            btn.Click();

            if(confirm)
            {
                ClickReproduceConfirmButton();
            }
        }

        public void ClickReproduceConfirmButton()
        {
            var btn = Get<CUITe_HtmlButton>("id=deleteMachineButtonConfirm");
            Specify.That(btn.Exists && btn.Enabled).Should.BeTrue("ClickReproduceConfirmButton can't be found.");
            btn.Click();
        }

        public void ClickReproduceCancelButton()
        {
            var btn = Get<CUITe_HtmlButton>("id=deleteMachineButtonCancel");
            Specify.That(btn.Exists && btn.Enabled).Should.BeTrue("ClickReproduceCancelButton can't be found.");
            btn.Click();
        }
        public void ClickCreateNextButton()
        {
            var btn = Get<CUITe_HtmlButton>("id=moveOrdersToNext");
            btn.Click();
        }
        public void ClickCancelOrdersButton()
        {
            var btn = Get<CUITe_HtmlButton>("id=removeOrders");
            btn.Click();
        }
        public void SortCompletedItemsTable(CompletedItemsSortBy selection)
        {
            var div = Get<CUITe_HtmlDiv>("id=ordersHistorySortyBy");
            div.Click();

            string innertext = "Order ID";
            if (selection == CompletedItemsSortBy.LastChangedDate)
                innertext = "Last Changed Date";
            var link = Get<CUITe_HtmlHyperlink>("innertext=" + innertext);
            link.Click();
        }
        #endregion

        #region Table Filters
        public void EnterTextIntoOpenOrdersFilter(string text)
        {
            var edit = Get<CUITe_HtmlEdit>("id=orderFilter");
            edit.SetText(text);
        }
        public void EnterTextIntoClosedOrdersFilter(string text)
        {
            var edit = Get<CUITe_HtmlEdit>("id=orderFilter");
            edit.SetText(text);
        }
        #endregion

        #region TriggerInterface
        public void EnterTextInTriggerInterface(int text)
        {
            EnterTextInTriggerInterface(text.ToString());
        }
        /// <summary>
        /// Enters supplied barcode into barcode field and clicks the Trigger button. Same as methods 'EnterTextInTriggerInterface' and 'ClickTriggerInterfaceButton' separatly.
        /// </summary>
        /// <param name="text">The value to enter into the barcode field</param>
        public void TriggerJob(int text)
        {
            TriggerJob(text.ToString());
        }
        /// <summary>
        /// Enters supplied barcode into barcode field and clicks the Trigger button. Same as methods 'EnterTextInTriggerInterface' and 'ClickTriggerInterfaceButton' separatly.
        /// </summary>
        /// <param name="text">The value to enter into the barcode field</param>
        public void TriggerJob(string text)
        {
            EnterTextInTriggerInterface(text);
            ClickTriggerInterfaceButton();
        }
        public void EnterTextInTriggerInterface(string text)
        {
            var edit = Get<CUITe_HtmlEdit>("name=triggeredSerialNo");
            Specify.That(edit.Enabled && edit.Exists).Should.BeTrue("Trigger Textbox not found");
            edit.SetText(text);
        }

        public void ClickTriggerInterfaceButton()
        {
            var btn = Get<CUITe_HtmlButton>("id=triggerInterfaceSubmitBtn");
            Specify.That(btn.Enabled && btn.Exists).Should.BeTrue("Trigger Button not found");
            btn.Click();
        }

        #endregion

        #region Machine Details
        public void VerifyIsMachineGroupStatus(MachineStatus expectedStatus, string message = null)
        {
            var actualStatus = GetMachineStatus();

            if(string.IsNullOrEmpty(message))
                message = string.Format("Actual machine group status: {0} Expected status: {1}.", actualStatus.ToString(), expectedStatus.ToString());

            Specify.That(actualStatus == expectedStatus).Should.BeTrue(message); 
        }
        public MachineDetails GetMachineDetails()
        {
            var details = new MachineDetails();
            details.ProductionGroupAlias = Get<CUITe_HtmlDiv>("id=productionGroupAlias").InnerText;
            details.SelectionAlgorithm = Get<CUITe_HtmlDiv>("id=currentProductionGroupSelectionAlgorithm").InnerText.Replace("Production Mode: ", "");

            var capabilities = Get<CUITe_HtmlDiv>("id=machineGroupCapabilitiesDetails").GetChildren().FindAll(z => z is CUITe_HtmlDiv);
            foreach (CUITe_HtmlDiv c in capabilities)
                details.Capabilities.Add(c.InnerText);

            var configMachines = Get<CUITe_HtmlDiv>("id=configuredMachinesDetails").GetChildren().FindAll(z => z is CUITe_HtmlDiv);
            foreach (CUITe_HtmlDiv cm in configMachines)
            {
                ConfiguredMachine m = new ConfiguredMachine();
                var r = cm.Get<CUITe_HtmlDiv>("class~row").FirstChild;

                var alias = ((CUITe_HtmlDiv)r).FirstChild;
                m.Alias = ((CUITe_HtmlSpan)alias).InnerText;
                var data = r.GetChildren().FindAll(z => z is CUITe_HtmlDiv);
                m.MachineStatus = ((CUITe_HtmlDiv)data[0]).InnerText.Replace("Machine Status: ", "");
                m.ProductionStatus = ((CUITe_HtmlDiv)data[1]).InnerText.Replace("Production Status: ", "");

                var machineCapabilities = cm.Get<CUITe_HtmlDiv>("class~row col-md-12").GetChildren().FindAll(z => z is CUITe_HtmlDiv);
                bool firstRow = true;
                foreach (CUITe_HtmlDiv mc in machineCapabilities)
                {
                    if (!firstRow)
                        m.Capabilities.Add(mc.InnerText);
                    firstRow = false;
                }

                details.ConfiguredMachines.Add(m);
            }
            return details;

        }

        #endregion

        public void RefreshTables()
        {
            _openOrdersTable = null;
            _itemsInProductionTable = null;
            _completedJobs = null;
        }

        public bool IsMachineStatus(MachineStatus status)
        {
            return GetMachineStatus().Equals(status);
        }

        public MachineStatus GetMachineStatus()
        {
            var ctrl = Get<CUITe_HtmlSpan>("id=divMachineStatus");
            if (string.IsNullOrEmpty(ctrl.InnerText))
                return MachineStatus.Empty;

            return (MachineStatus)Enum.Parse(typeof(MachineStatus), ctrl.InnerText.Replace(" More Info",""), true);
        }

        public static MachineProductionPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<MachineProductionPage>();
        }
        public static MachineProductionPage GetWindow(SelectionAlgorithmTypes selectionAlgorithmType, ProductionMode mode = ProductionMode.Auto, UserType userType = UserType.Administrator)
        {
            var page = CUITe_BrowserWindow.GetBrowserWindow<MachineProductionPage>();
            page._prodMode = mode;
            page._algorithmType = selectionAlgorithmType;
            page._userType = userType;
            return page;
        }
    }

    public class MachineDetails
    {
        public string ProductionGroupAlias { get; set; }
        public string SelectionAlgorithm { get; set; }
        public List<ConfiguredMachine> ConfiguredMachines { get; set; }
        public List<String> Capabilities { get; set; }

        public MachineDetails()
        {
            ConfiguredMachines = new List<ConfiguredMachine>();
            Capabilities = new List<string>();
        }
    }

    public class ConfiguredMachine
    {
        public string Alias { get; set; }
        public string MachineStatus { get; set; }
        public string ProductionStatus { get; set; }
        public List<string> Capabilities { get; set; }

        public ConfiguredMachine()
        {
            Capabilities = new List<string>();
        }

        public override string ToString()
        {
            return Alias + " - MachineStatus: " + MachineStatus + " ProductionStatus: " + ProductionStatus;
        }
    }
}
