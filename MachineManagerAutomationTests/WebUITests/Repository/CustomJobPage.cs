﻿using MachineManagerAutomationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UITesting;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class CustomJobPage : WindowBase
    {
        // alerts
        private AlertWidget Alerts { get { return Get<AlertWidget>("id=produceCustomJobAlert"); } }

        // edit fields
        private CUITe_HtmlEdit LengthEdit { get { return Get<CUITe_HtmlEdit>("id=customJobLengthInputBox"); } }
        private CUITe_HtmlEdit WidthEdit { get { return Get<CUITe_HtmlEdit>("id=customJobWidthInputBox"); } }
        private CUITe_HtmlEdit HeightEdit { get { return Get<CUITe_HtmlEdit>("id=customJobHeightInputBox"); } }
        private CUITe_HtmlEdit CorrugateQualityEdit { get { return Get<CUITe_HtmlEdit>("id=quantityInputBox"); } }
        private CUITe_HtmlEdit OrderNumberEdit { get { return Get<CUITe_HtmlEdit>("name=CustomerUniqueId"); } }
        private CUITe_HtmlComboBox DesignDropDown { get { return Get<CUITe_HtmlComboBox>("id=CustomJobDesignIdButton"); } }
        private CUITe_HtmlComboBox RotationsDropDown { get { return Get<CUITe_HtmlComboBox>("id=AllowedRotationsDropDown"); } }

        // validations
        private CUITe_HtmlLabel LengthValidation { get { return Get<CUITe_HtmlLabel>("id=customJobLengthValidationMessage"); } }
        private CUITe_HtmlSpan WidthValidation { get { return Get<CUITe_HtmlSpan>("id=customJobWidthValidationMessage"); } }
        private CUITe_HtmlSpan HeightValidation { get { return Get<CUITe_HtmlSpan>("id=customJobHeightValidationMessage"); } }
        private CUITe_HtmlSpan CorrugateQualityValidation { get { return Get<CUITe_HtmlSpan>("id=customJobCorrugateQualityValidationMessage"); } }

        // preview
        private CUITe_HtmlHyperlink HidePreviewButton { get { return Get<CUITe_HtmlHyperlink>("id=hidePreview"); } }
        private CUITe_HtmlDiv PreviewContainer { get { return Get<CUITe_HtmlDiv>("id=previewContainer"); } }

        // buttons
        private CUITe_HtmlButton ProduceButton { get { return Get<CUITe_HtmlButton>("id=produce"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=cancel"); } }

        // misc methods
        public static CustomJobPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<CustomJobPage>();
        }

        // crud
        public void Create(CustomJob cj, AdminConsole adminConsole, bool withValidation = false)
        {
            // check if design exists
            if(!DesignHelper.DesignExists(cj.DesignAlias))
            {
                Specify.Failure(string.Format("Specified design '{0}' does not exist.", cj.DesignAlias));
            }

            adminConsole.NavigateToPage(NavPages.CustomJob);

            var page = new CustomJobPage();

            if (withValidation)
            {
                VerifyProduceButtonEnabled(false);
                Specify.That(CancelButton.Enabled).Should.BeTrue("Cancel button should be enabled.");

                Specify.That(DesignDropDown.Enabled).Should.BeTrue("The Design menu must be available.");
                Specify.That(LengthEdit.Exists).Should.BeTrue("The input field Length must be available.");
                Specify.That(WidthEdit.Exists).Should.BeTrue("The input field Width must be available.");

                VerifyPreviewIsShown(false);
            }

            SetQuality(cj.CorrugateQuality);
            SetOrderNumber(cj.OrderNumber);
            Playback.Wait(1000);
            SetDesignMenu(cj.DesignAlias);
            VerifyPreviewIsShown(true);
            SetLength(cj.Length);
            VerifyPreviewIsShown(true);
            SetWidth(cj.Width);
            VerifyPreviewIsShown(true);

            Specify.That(HeightEdit.Exists).Should.BeTrue("The input field Height must be available.");
            SetHeight(cj.Height);
            VerifyPreviewIsShown(true);

            if (withValidation)
            {
                ClickPreviewButton();
                VerifyPreviewIsShown(false);

                ClickPreviewButton();
                VerifyPreviewIsShown();

                Specify.That(ProduceButton.Enabled).Should.BeTrue("After entering design and sizes the Produce button is enabled.");

                Specify.That(RotationsDropDown.Enabled).Should.BeTrue("The Rotations menu must be available.");
                var rotations = DesignHelper.GetAllowedRotationsAsStringArray(cj.DesignAlias);
                var rotationMenu = new DropDownList(RotationsDropDown);
                Specify.That(rotationMenu.ListsAreEqual(rotations))
                    .Should.BeTrue(
                        string.Format("The allowed rotations for the design '{0}' is not the same as in the menu Rotation",
                            cj.DesignAlias));
            }
            ClickProduceButton();

            if (withValidation)
            {
                TestUtilities.VerifyAlert(Alerts, null, "Carton successfully sent to production.", "Custom Job was not created successfully.", false);
            }
        }

        // assign values to fields
        public void SetOrderNumber(string orderNumber)
        {
            OrderNumberEdit.SetText(orderNumber);
        }
        public void SetLength(int length)
        {
            LengthEdit.SetText(length.ToString());
        }
        public void SetHeight(int height)
        {
            HeightEdit.SetText(height.ToString());
        }
        public void SetWidth(int width)
        {
            WidthEdit.SetText(width.ToString());
        }
        public void SetQuality(int quality)
        {
            CorrugateQualityEdit.SetText(quality.ToString());
        }
        public void SetDesignMenu(string design)
        {
            DesignDropDown.SelectItem(design);
        }
        public void SetRotationMenu(string rotation)
        {
            RotationsDropDown.SelectItem(rotation);
        }
        public int GetQuantity()
        {
            return int.Parse(CorrugateQualityEdit.GetText());
        }

        // button action
        public void ClickProduceButton()
        {
            ProduceButton.SafeClick();
        }
        public void ClickCancelButton()
        {
            CancelButton.SafeClick();
        }
        public void ClickPreviewButton()
        {
            HidePreviewButton.Click();
        }

        // verify
        public void VerifyLengthValidationExists()
        {
            Specify.That(LengthValidation.Exists).Should.BeTrue("Length validation does not exist.");
        }
        public void VerifyHeightValidationExists()
        {
            Specify.That(HeightValidation.Exists).Should.BeTrue("Height validation does not exist.");
        }
        public void VerifyWidthValidationExists()
        {
            Specify.That(WidthValidation.Exists).Should.BeTrue("Width validation does not exist.");
        }
        public void VerifyPreviewIsShown(bool status = true)
        {
            if (status)
                Specify.That(PreviewContainer.Enabled).Should.BeTrue("Design preview container should be shown");
            else
                Specify.That(PreviewContainer.Enabled).Should.BeFalse("Design preview container should be hidden");
        }
        public void VerifyProduceButtonEnabled(bool enabled)
        {
            if(enabled)
                Specify.That(ProduceButton.Enabled)
                    .Should.BeTrue("Produce button should be enabled.");
            else
                Specify.That(ProduceButton.Enabled)
                    .Should.BeFalse("Before entering design and sizes the Produce button is disabled.");
        }
    }
}
