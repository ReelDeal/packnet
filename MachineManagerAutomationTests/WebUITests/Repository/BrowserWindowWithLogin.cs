﻿namespace MachineManagerAutomationTests.WebUITests.Repository
{
    using CUITe.Controls.HtmlControls;
    using MachineManagerAutomationTests.Helpers;
    using PackNet.Common.Utils;
    using System;
    using Testing.Specificity;

    public class DynamicBrowserWindowWithLogin : WindowBase
    {
        #region Constructors
        public DynamicBrowserWindowWithLogin()
            : base()
        {
        }

        public DynamicBrowserWindowWithLogin(string title)
            : base(title)
        {
            sWindowTitle = title;
            SearchProperties[PropertyNames.Name] = sWindowTitle;
        }
        #endregion

        private CUITe_HtmlEdit LoginUsername { get { return Get<CUITe_HtmlEdit>("id=username"); } }
        private CUITe_HtmlEdit LoginPassword { get { return Get<CUITe_HtmlEdit>("id=password"); } }
        private CUITe_HtmlButton LanguageSelectDropDownButton { get { return Get<CUITe_HtmlButton>("id=language"); } }
        public CUITe_HtmlUnorderedList LanguageSelectionUnorderedList { get { return Get<CUITe_HtmlUnorderedList>("id=languageUnorderedList"); } }
        public CUITe_HtmlButton LoginButton { get { return Get<CUITe_HtmlButton>("id=loginButton"); } }
        public CUITe_HtmlLabel LoadingLabel { get { return Get<CUITe_HtmlLabel>("id=loadingLabel"); } }
        public CUITe_HtmlButton ReconnectButton { get { return Get<CUITe_HtmlButton>("id=reconnectButton"); } }
        public CUITe_HtmlDiv LoginDialogAlert { get { return Get<CUITe_HtmlDiv>("id=loginDialogAlerts"); } }
        public AlertWidget LoginDialogAlerts { get { return Get<AlertWidget>("id=loginDialogAlerts"); } }

        // assign values to fields
        public void SetLoginUsername(string username)
        {
            LoginUsername.SetText(username);
        }
        public void SetLoginPassword(string password)
        {
            LoginPassword.SetText(password);
        }

        // button actions
        public void ClickLoginButton()
        {
            LoginButton.Click();
        }
        public void SetLoginLanguage(string language = null)
        {
            LanguageSelectDropDownButton.SafeClick();
            if(!string.IsNullOrEmpty(language))
                this.Get<CUITe_HtmlHyperlink>("id=" + language).SafeClick();
        }

        // verify
        public void VerifyLoginButtonIsEnabled(bool enabled = true, string message = null)
        {
            if (enabled)
            {
                if (string.IsNullOrEmpty(message))
                    message = "Login button is disabled";

                Retry.For(() => LoginButton.Exists, TimeSpan.FromSeconds(60));
                Specify.That(LoginButton.Enabled).Should.BeTrue(message);
            }
            else
            {
                if (string.IsNullOrEmpty(message))
                    message = "Login button is enabled";

                Retry.For(() => LoginButton.Exists, TimeSpan.FromSeconds(60));
                Specify.That(LoginButton.Enabled).Should.BeFalse(message);
            }
        }

        public void VerifyLoginUsernameFieldIsEnabled()
        {
            Retry.For(() => LoginUsername.Enabled, TimeSpan.FromSeconds(60));
            Specify.That(LoginUsername.Enabled).Should.BeTrue("Username Edit was not enabled");
        }
        public void VerifySelectLanguageDropDownExists()
        {
            Specify.That(LanguageSelectDropDownButton.Exists).Should.BeEqualTo(true);
        }

        /// <summary>
        /// Specify a specific user to login in with
        /// </summary>
        /// <param name="console">The page that requires a login</param>
        /// <param name="username">Username</param>
        /// <param name="password">Password</param>
        /// <param name="language">Language</param>
        public void Login(string username, string password, string language = null)
        {
            //Wait for the page to come up.
            Retry.For(() => this.LoginButton.Exists, TimeSpan.FromSeconds(60));

            this.VerifyLoginButtonIsEnabled();
            this.VerifyLoginUsernameFieldIsEnabled();

            this.SetLoginUsername(username);
            this.SetLoginPassword(password);

            if (!string.IsNullOrEmpty(language))
            {
                this.SetLoginLanguage(language);
            }

            this.VerifyLoginButtonIsEnabled();
            this.ClickLoginButton();
        }

        public static DynamicBrowserWindowWithLogin GetWindow()
        {
            return GetBrowserWindow<DynamicBrowserWindowWithLogin>();
        }

        /// <summary>
        ///// Use the default login settings.
        ///// </summary>
        ///// <param name="machineId">The machine identifier.</param>
        ///// <param name="pageUrl">page that requires a login</param>
        ///// <returns></returns>
        ////public static T LoginAsOperatorAndCreateOperatorConsole<T>(int machineId, string pageUrl = @"index.html#/SelectMachine") where T : OperatorConsole, new()
        //public static OperatorConsole LoginAsOperatorAndCreateOperatorConsole(int machineId, string pageUrl = "index.html#/OperatorConsole/Console")
        //{
        //    var loginPage = TestUtilities.CreatePage<DynamicBrowserWindowWithLogin>(String.Format("{0}?machineId={1}", pageUrl, machineId));
        //    loginPage.Login(TestSettings.Default.MachineManagerOperatorUserName, TestSettings.Default.MachineManagerOperatorPassword);
        //    return loginPage;
        //}
    }
}
