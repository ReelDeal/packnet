﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UITesting;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;

using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public enum WidgetTabs
    {
        General,
        Daily,
        Weekly
    }
    public enum NavPages
    {
        Classification,
        PickZone,
        Article,
        Corrugate,
        Machine,
        CartonPropertyGroup,
        MachineGroup,
        MachineCommands,
        ChangeProductionGroup,
        ProductionGroup,
        OperatorPanel,
        User,
        Settings,
        Dashboard,
        SearchBoxFirst,
        SearchBoxLast,
        SearchScanToCreate,
        SearchOrders,
        ChangeCorrugate,
        ChangeMachineGroup,
        MachineProduction,
        CustomJob,
        CustomJobFromArticle,
        About,
        Support,
        MachineControl,
        OpSearchBoxFirst,
        OpSearchBoxLast,
        OpSearchOrders,
        OpSearchScanToCreate,
        CreateCustomJobLink,
        MachineProductionLink,
        ServicePage

    }

    public class AdminConsole : ConsoleBase
    {
        #region Page objects

        #region Navigation Page Objects

        // dashboard
        private CUITe_HtmlLabel ReportsLink { get { return Get<CUITe_HtmlLabel>("id=ReportsLink"); } }
        private CUITe_HtmlHyperlink DashboardLink { get { return Get<CUITe_HtmlHyperlink>("id=DashboardLink"); } }

        // search
        private CUITe_HtmlLabel SearchLabel { get { return Get<CUITe_HtmlLabel>("InnerText=Search"); } }
        private CUITe_HtmlLabel SearchLink { get { return Get<CUITe_HtmlLabel>("id=SearchLink"); } }
        private CUITe_HtmlLabel OpSearchLink { get { return Get<CUITe_HtmlLabel>("id=op_SearchLink"); } }
        private CUITe_HtmlHyperlink SearchScanToCreateLink { get { return Get<CUITe_HtmlHyperlink>("id=AdminSearchScanToCreateHyperlink"); } }
        private CUITe_HtmlHyperlink SearchBoxFirstLink { get { return Get<CUITe_HtmlHyperlink>("id=AdminSearchBoxFirstHyperlink"); } }
        private CUITe_HtmlHyperlink SearchBoxLastLink { get { return Get<CUITe_HtmlHyperlink>("id=AdminSearchBoxLastHyperlink"); } }
        private CUITe_HtmlHyperlink OpSearchBoxLastLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchBoxLastLink"); } }
        private CUITe_HtmlHyperlink SearchOrdersLink { get { return Get<CUITe_HtmlHyperlink>("id=AdminSearchOrdersHyperlink"); } }
        private CUITe_HtmlHyperlink OpSearchBoxFirstLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchBoxFirstLink"); } }
        private CUITe_HtmlHyperlink OpSearchOrdersLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchOrdersLink"); } }
        private CUITe_HtmlHyperlink OpSearchScanToCreateLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchScanToCreateLink"); } }

        // configuration section
        private CUITe_HtmlLabel ConfigurationLink { get { return Get<CUITe_HtmlLabel>("id=ConfigurationLink"); } }
        private CUITe_HtmlLabel OpConfigurationLink { get { return Get<CUITe_HtmlLabel>("id=Op_ConfigurationLink"); } }
        private CUITe_HtmlHyperlink ArticlesLink { get { return Get<CUITe_HtmlHyperlink>("id=ArticlesNewLink"); } }
        private CUITe_HtmlHyperlink CartonPropertyGroupsLink { get { return Get<CUITe_HtmlHyperlink>("id=CartonPropertyGroupsLink"); } }
        private CUITe_HtmlHyperlink ClassificationsLink { get { return Get<CUITe_HtmlHyperlink>("id=ClassificationsLink"); } }
        private CUITe_HtmlHyperlink CorrugatesLink { get { return Get<CUITe_HtmlHyperlink>("id=CorrugatesLink"); } }
        private CUITe_HtmlHyperlink MachinesLink { get { return Get<CUITe_HtmlHyperlink>("id=MachinesLink"); } }
        private CUITe_HtmlHyperlink MachineGroupsLink { get { return Get<CUITe_HtmlHyperlink>("id=MachineGroupsLink"); } }
        private CUITe_HtmlHyperlink MachineCommands { get { return Get<CUITe_HtmlHyperlink>("id=MachineCommandsLink"); } }
        private CUITe_HtmlHyperlink ProductionGroupLink { get { return Get<CUITe_HtmlHyperlink>("id=ProductionGroupsLink"); } }
        private CUITe_HtmlHyperlink PickZoneLink { get { return Get<CUITe_HtmlHyperlink>("id=PickZonesLink"); } }
        private CUITe_HtmlHyperlink SettingsLink { get { return Get<CUITe_HtmlHyperlink>("id=SettingsLink"); } }
        private CUITe_HtmlHyperlink UsersLink { get { return Get<CUITe_HtmlHyperlink>("id=UsersLink"); } }
        private CUITe_HtmlHyperlink MachineControlLink { get { return Get<CUITe_HtmlHyperlink>("id=MachineCommandsLink"); } }
        private CUITe_HtmlLabel SldMenuLink { get { return Get<CUITe_HtmlLabel>("id=SldMenuLink"); } }

        // change console
        private CUITe_HtmlLabel AdminSideAdministratorLink { get { return Get<CUITe_HtmlLabel>("id=AdAdministratorLink"); } }
        private CUITe_HtmlHyperlink OperatorConsoleLink { get { return Get<CUITe_HtmlHyperlink>("id=OperatorPanelLink"); } }

        // operator console 
        private CUITe_HtmlHyperlink OpChangedMachineGroupLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeMachinesLink"); } }
        private CUITe_HtmlHyperlink OpChangeCorrugatesLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeCorrugatesLink"); } }
        private CUITe_HtmlLabel OpConfigurationLabel { get { return Get<CUITe_HtmlLabel>("id=Op_ConfigurationLink"); } }
        private CUITe_HtmlLabel opOperationsLabel { get { return Get<CUITe_HtmlLabel>("id=OperationsLink"); } }
        private CUITe_HtmlLabel OpAdministratorLink { get { return Get<CUITe_HtmlLabel>("id=OpAdministratorLink"); } }
        private CUITe_HtmlHyperlink OpAdministratorPanelLink { get { return Get<CUITe_HtmlHyperlink>("id=AdministratorPanelLink"); } }
        private CUITe_HtmlLabel OpCartonCreationLink { get { return Get<CUITe_HtmlLabel>("id=CartonCreationLink"); } }
        private CUITe_HtmlHyperlink OpCreateCustomJobLink { get { return Get<CUITe_HtmlHyperlink>("id=CreateCustomJobLink"); } }
        private CUITe_HtmlHyperlink OpCustomJobLink { get { return Get<CUITe_HtmlHyperlink>("id=CreateCustomJobLink"); } }
        private CUITe_HtmlHyperlink OpCustomJobFromArticleLink { get { return Get<CUITe_HtmlHyperlink>("id=CreateFromArticleLink"); } }
        private CUITe_HtmlButton ChangeProductionGroupSaveButton { get { return Get<CUITe_HtmlButton>("id=ProductionGroupsSaveButton"); } }
        private CUITe_HtmlHyperlink ChangeProductionGroupLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeProductionGroupLink"); } }
        private CUITe_HtmlLabel OperationsLink { get { return Get<CUITe_HtmlLabel>("id=OperationsLink"); } }
        private CUITe_HtmlHyperlink MachineProductionLink { get { return Get<CUITe_HtmlHyperlink>("id=MachineProductionLink"); } }
        private CUITe_HtmlLabel CartonCreationLink { get { return Get<CUITe_HtmlLabel>("id=CartonCreationLink"); } }
        private CUITe_HtmlHyperlink CreateFromArticleLink { get { return Get<CUITe_HtmlHyperlink>("id=CreateFromArticleLink"); } }
        private CUITe_HtmlHyperlink CreateCustomJobLink { get { return Get<CUITe_HtmlHyperlink>("id=CreateCustomJobLink"); } }

        // sld

        private CUITe_HtmlHyperlink ChangeMachineGroupLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeMachinesLink"); } }
        private CUITe_HtmlHyperlink SLD { get { return Get<CUITe_HtmlHyperlink>("id=SldLink"); } }

        #endregion

        #region Notification Page Objects

        private CUITe_HtmlDiv ExpandCollapseNotifications { get { return Get<CUITe_HtmlDiv>("id=expandCollapseNotifications"); } }
        private CUITe_HtmlSpan NotificationCountBubble { get { return Get<CUITe_HtmlSpan>("id=notificationCountBubble"); } }
        private CUITe_HtmlListItem ListOfNotifications { get { return Get<CUITe_HtmlListItem>("id=listOfNotifications"); } }
        private CUITe_HtmlListItem DismissAllNotificationsButton { get { return Get<CUITe_HtmlListItem>("id=dismissAllButton"); } }

        #endregion

        #region Widget Page Objects
        
        private CUITe_HtmlHyperlink NewWidgetButton { get { return Get<CUITe_HtmlHyperlink>("id=addWidget"); } }
        private CUITe_HtmlHyperlink EditWidgetStructureButton { get { return Get<CUITe_HtmlHyperlink>("id=editStructure"); } }
        private CUITe_HtmlHyperlink EditDashboardButton { get { return Get<CUITe_HtmlHyperlink>("id=editDashboard"); } }
        private CUITe_HtmlButton closeEditDash { get { return Get<CUITe_HtmlButton>("id=closeEditDash"); } }
        private CUITe_HtmlRadioButton DefaultDashboardLook { get { return Get<CUITe_HtmlRadioButton>("id=12/4-4-4"); } }
        public CUITe_HtmlSpan TotalRequestsCountLabel { get { return Get<CUITe_HtmlSpan>("id=totalRequestsCount"); } }
        private CUITe_HtmlHyperlink GeneralWidgetsTab { get { return Get<CUITe_HtmlHyperlink>("id=generalWidgetsTab"); } }
        private CUITe_HtmlHyperlink DailyWidgetsTab { get { return Get<CUITe_HtmlHyperlink>("id=dailyWidgetsTab"); } }
        private CUITe_HtmlHyperlink WeeklyWidgetsTab { get { return Get<CUITe_HtmlHyperlink>("id=weeklyWidgetsTab"); } }
        private CUITe_HtmlListItem GeneralWidgetsTabWrapper { get { return Get<CUITe_HtmlListItem>("id=generalWidgetsTabWrapper"); } }
        private CUITe_HtmlListItem DailyWidgetsTabWrapper { get { return Get<CUITe_HtmlListItem>("id=dailyWidgetsTabWrapper"); } }
        private CUITe_HtmlListItem WeeklyWidgetsTabWrapper { get { return Get<CUITe_HtmlListItem>("id=weeklyWidgetsTabWrapper"); } }
        private CUITe_HtmlButton CloseAddWidgetsModalBtn { get { return Get<CUITe_HtmlButton>("id=addWidgetCloseBtn"); } }
        
        #endregion

        #region Login and Logout Page objects
        
        private CUITe_HtmlHyperlink LoggedInUsername { get { return Get<CUITe_HtmlHyperlink>("class=dropdown-toggle ng-binding"); } }
        private CUITe_HtmlHyperlink LogOutLink { get { return Get<CUITe_HtmlHyperlink>("href~LogOut"); } }
        
        #endregion

        #region About, eula and support links

        private CUITe_HtmlHyperlink AboutLink { get { return Get<CUITe_HtmlHyperlink>("id=aboutLink"); } }
        private CUITe_HtmlButton EulaBtn { get { return Get<CUITe_HtmlButton>("id=ViewEULA"); } }
        private CUITe_HtmlHyperlink SupportLink { get { return Get<CUITe_HtmlHyperlink>("id=supportLink"); } }

        #endregion

        #endregion

        #region Notifications

        public int GetNumberOfNotifications()
        {
            return int.Parse(NotificationCountBubble.InnerText);
        }
        public Notification GetLastNotification()
        {
            return GetNotification(GetNumberOfNotifications() - 1);
        }
        public Notification GetNotification(int index)
        {
            if (!ListOfNotifications.Enabled)
                ClickExpandCollapseNotificationsButton();

            var child = ListOfNotifications.GetChildren()[index] as CUITe_HtmlDiv;

            Specify.That(child.Exists).Should.BeTrue(string.Format("The notification with index '{0}' does not exist.", index));

            return GetNotificationObject(child);
        }
        public List<Notification> GetNotifications()
        {
            ExpandCollapseNotifications.Click();
            List<Notification> lst = new List<Notification>();
            foreach (var child in ListOfNotifications.GetChildren())
                lst.Add(GetNotificationObject((CUITe_HtmlDiv)child));
            return lst;
        }
        public void ClickExpandCollapseNotificationsButton()
        {
            ExpandCollapseNotifications.Click();
        }
        public void ClickDismissAllNotificationsButton()
        {
            DismissAllNotificationsButton.Click();
        }
        private Notification GetNotificationObject(CUITe_HtmlDiv div)
        {
            Notification notification = new Notification();
            notification.MessageCount = int.Parse(div.Get<CUITe_HtmlSpan>("class~message-count").InnerText);
            notification.Text = div.Get<CUITe_HtmlSpan>("id=notificationMessage").InnerText;
            notification.NotificationTime = div.Get<CUITe_HtmlSpan>("id=notificationTimeStamp").InnerText;
            return notification;
        }

        #endregion

        #region Navigation

        public void NavigateToPage(NavPages page)
        {
            // are we navigating to a page on the operator side or the administrator side??
            var admin = CheckIfMenuItemIsInAdminConsole(page);

            // the menu link is on the admin side
            if (admin)
            {
                if (!AdminSideAdministratorLink.Enabled)
                {
                    // if we are on operator side we must first change to admin side
                    if (!OpAdministratorPanelLink.Enabled)
                        OpAdministratorLink.Click();
                    OpAdministratorPanelLink.Click();
                }
            }
            else if (AdminSideAdministratorLink.Enabled)
            {
                // if the menu link is on operator side and we are on the admin side, change to op side
                if (!OperatorConsoleLink.Enabled)
                    AdminSideAdministratorLink.SafeClick();
                OperatorConsoleLink.SafeClick();
            }

            // ... and here it's time to click the link in the menu
            switch (page)
            {
                case NavPages.About:
                    AboutLink.Click();
                    break;
                case NavPages.Support:
                    SupportLink.Click();
                    break;
                case NavPages.Classification:
                    OpenConfigurationLinkInMenu(ClassificationsLink);
                    break;
                case NavPages.Article:
                    OpenConfigurationLinkInMenu(ArticlesLink);
                    break;
                case NavPages.PickZone:
                    OpenConfigurationLinkInMenu(PickZoneLink);
                    break;
                case NavPages.Corrugate:
                    OpenConfigurationLinkInMenu(CorrugatesLink);
                    break;
                case NavPages.Machine:
                    OpenConfigurationLinkInMenu(MachinesLink);
                    break;
                case NavPages.SearchScanToCreate:
                    if (!SearchScanToCreateLink.Enabled)
                        SearchLink.SafeClick();
                    SearchScanToCreateLink.SafeClick();
                    break;
                case NavPages.SearchBoxFirst:
                    if (!SearchBoxFirstLink.Enabled)
                        SearchLink.SafeClick();
                    SearchBoxFirstLink.SafeClick();
                    break;
                case NavPages.SearchBoxLast:
                    if (!SearchBoxLastLink.Enabled)
                        SearchLink.SafeClick();
                    SearchBoxLastLink.SafeClick();
                    break;
                case NavPages.SearchOrders:
                    if (!SearchOrdersLink.Enabled)
                        SearchLink.SafeClick();
                    SearchOrdersLink.SafeClick();
                    break;
                case NavPages.CartonPropertyGroup:
                    if (!CartonPropertyGroupsLink.Enabled)
                        ConfigurationLink.Click();
                    CartonPropertyGroupsLink.Click();
                    break;
                case NavPages.MachineGroup:
                    if (!MachineGroupsLink.Enabled)
                        ConfigurationLink.SafeClick();
                    MachineGroupsLink.SafeClick();
                    break;
                case NavPages.MachineCommands:
                    if (!MachineCommands.Enabled)
                        ConfigurationLink.SafeClick();
                    MachineCommands.SafeClick();
                    break;
                case NavPages.ProductionGroup:
                    OpenConfigurationLinkInMenu(ProductionGroupLink);
                    break;
                case NavPages.OperatorPanel:
                    if (!OperatorConsoleLink.Enabled)
                        AdminSideAdministratorLink.SafeClick();
                    OperatorConsoleLink.SafeClick();
                    break;
                case NavPages.User:
                    OpenConfigurationLinkInMenu(UsersLink);
                    break;
                case NavPages.Settings:
                    OpenConfigurationLinkInMenu(SettingsLink);
                    break;
                case NavPages.Dashboard:
                    if (!DashboardLink.Enabled)
                        ReportsLink.Click();
                    DashboardLink.Click();
                    break;
                case NavPages.ChangeCorrugate:
                    if (!OpChangeCorrugatesLink.Enabled)
                        OpConfigurationLabel.Click();
                    OpChangeCorrugatesLink.Click();
                    break;
                case NavPages.ChangeMachineGroup:
                    if (!OpChangedMachineGroupLink.Enabled)
                        opOperationsLabel.Click();
                    OpChangedMachineGroupLink.Click();
                    break;
                case NavPages.MachineProduction:
                    if (!MachineProductionLink.Enabled)
                        opOperationsLabel.Click();
                    MachineProductionLink.Click();
                    break;
                case NavPages.CustomJob:
                    if (!OpCustomJobLink.Enabled)
                        OpCartonCreationLink.Click();
                    OpCustomJobLink.Click();
                    break;
                case NavPages.CustomJobFromArticle:
                    if (!CreateFromArticleLink.Enabled)
                        OpCartonCreationLink.Click();
                    CreateFromArticleLink.Click();
                    break;
                case NavPages.MachineControl:
                    if (!MachineControlLink.Enabled)
                        OpConfigurationLabel.Click();
                    MachineControlLink.SafeClick();
                    break;
                case NavPages.ChangeProductionGroup:
                    if (!ChangeProductionGroupLink.Enabled)
                        OpConfigurationLink.SafeClick();
                    ChangeProductionGroupLink.SafeClick();
                    break;
                case NavPages.OpSearchBoxFirst:
                    if (!OpSearchBoxFirstLink.Enabled)
                        OpSearchLink.SafeClick();
                    OpSearchBoxFirstLink.SafeClick();
                    break;
                case NavPages.OpSearchBoxLast:
                    if (!OpSearchBoxLastLink.Enabled)
                        OpSearchLink.SafeClick();
                    OpSearchBoxLastLink.SafeClick();
                    break;
                case NavPages.OpSearchOrders:
                    if (!OpSearchOrdersLink.Enabled)
                        OpSearchLink.SafeClick();
                    OpSearchOrdersLink.SafeClick();
                    break;
                case NavPages.OpSearchScanToCreate:
                    if (!OpSearchScanToCreateLink.Enabled)
                        OpSearchLink.SafeClick();
                    OpSearchScanToCreateLink.SafeClick();
                    break;
                case NavPages.CreateCustomJobLink:
                    if (!CreateCustomJobLink.Enabled)
                        OpCartonCreationLink.Click();
                    CreateCustomJobLink.Click();
                    break;
                case NavPages.MachineProductionLink:
                    if (!MachineProductionLink.Enabled)
                        OperationsLink.Click();
                    MachineProductionLink.Click();
                    break;
                case NavPages.ServicePage:
                    NavigateToUrl(TestUtilities.GetMachineManagerURL() + "/index.html#/Service");
                    Playback.Wait(500);
                    break;
            }
        }
        private static bool CheckIfMenuItemIsInAdminConsole(NavPages page)
        {
            var admin = true;
            switch (page)
            {
                case NavPages.ChangeCorrugate:
                    admin = false;
                    break;
                case NavPages.ChangeMachineGroup:
                    admin = false;
                    break;
                case NavPages.MachineProduction:
                    admin = false;
                    break;
                case NavPages.CustomJob:
                    admin = false;
                    break;
                case NavPages.MachineControl:
                    admin = false;
                    break;
                case NavPages.ChangeProductionGroup:
                    admin = false;
                    break;
                case NavPages.MachineCommands:
                    admin = false;
                    break;
                case NavPages.OpSearchBoxFirst:
                    admin = false;
                    break;
                case NavPages.OpSearchBoxLast:
                    admin = false;
                    break;
                case NavPages.OpSearchOrders:
                    admin = false;
                    break;
                case NavPages.OpSearchScanToCreate:
                    admin = false;
                    break;
                //case NavPages.OpCartonCreation:
                //    admin = false;
                //    break;
                //case NavPages.CreateFromArticle:
                //    admin = false;
                //    break;
                case NavPages.CreateCustomJobLink:
                    admin = false;
                    break;
                case NavPages.MachineProductionLink:
                    admin = false;
                    break;
                case NavPages.CustomJobFromArticle:
                    admin = false;
                    break;

            }
            return admin;
        }
        private void OpenConfigurationLinkInMenu(CUITe_HtmlHyperlink link)
        {
            if (!link.Enabled)
                ConfigurationLink.SafeClick();
            link.SafeClick();

        }

        // open and close menu sections
        public void ClickReportsMenuSectionButton()
        {
            ReportsLink.SafeClick(); ;
        }
        public void ClickSearchMenuSectionButton()
        {
            SearchLink.SafeClick(); ;
        }
        public void ClickConfigurationMenuSectionButton()
        {
            ConfigurationLink.SafeClick(); ;
        }
        public void ClickAdminSideAdministratorButton()
        {
            AdminSideAdministratorLink.SafeClick(); ;
        }
        public void ClickOpSearchMenuSectionButton()
        {
            OpSearchLink.SafeClick();
        }
        public void ClickOpConfigurationMenuSectionButton()
        {
            OpConfigurationLink.Click();
        }
        public void ClickOpCartonCreationMenuSectionButton()
        {
            OpCartonCreationLink.Click();
        }
        public void ClickOpAdministratorMenuSectionButton()
        {
            OpAdministratorLink.Click();
        }
        public void ClickOpOperationsMenuSectionButton()
        {
            opOperationsLabel.Click();
        }

        #endregion

        #region Select machine group
        public void SelectMachineGroup(Guid mgId, bool navigateToPage = true)
        {
            if (navigateToPage)
            {
                NavigateToPage(NavPages.ChangeMachineGroup);
                Playback.Wait(3000);
            }
            base.SelectMachineGroup(mgId, this);
        }
        public void SelectMachineGroup(string alias, bool navigateToPage = true)
        {
            if (navigateToPage)
                NavigateToPage(NavPages.ChangeMachineGroup);

            base.SelectMachineGroup(alias, this);
        }
        #endregion

        #region Widgets

        private CUITe_HtmlHyperlink GetWidget(int widgetId)
        {
            var tabSearch = "";
            if (GeneralWidgetsTabWrapper.ContainsCSS("active"))
                tabSearch = "Metrics";
            else if (DailyWidgetsTabWrapper.ContainsCSS("active"))
                tabSearch = "MetricsByDay";
            else if (WeeklyWidgetsTabWrapper.ContainsCSS("active"))
                tabSearch = "MetricsByWeek";

            var search = tabSearch + "widget" + widgetId;
            return Get<CUITe_HtmlHyperlink>("id=" + search);
        }

        public void SelectDashbordLayout(string layout)
        {
            Specify.That(this.Get<CUITe_HtmlRadioButton>("value=" + layout).Exists).Should.BeTrue();
            this.Get<CUITe_HtmlRadioButton>("value=" + layout).Click();
        }
        public void AddWidgetToDashboard(string widgetName, string layout = null)
        {
            NavigateToPage(NavPages.Dashboard);

            ClickEditDashboardButton();
            ClickNewWidgetButton();

            SelectWidget(widgetName);

            Playback.Wait(1000);

            if (layout != null)
            {
                ChangeDashboardLayout(layout);
                Playback.Wait(500);
            }

            ClickEditDashboardButton();
        }
        public void SelectWidget(string name)
        {
            GetWidget(name).SafeClick();
        }
        public void SelectWidget(int widgetId)
        {
            GetWidget(widgetId).SafeClick();
        }
        public void ChangeDashboardLayout(string layout)
        {
            ClickEditWidgetStructureButton();
            SelectDashbordLayout(layout);
            ClickCloseDashboardLayoutButton();
        }
        public CUITe_HtmlHyperlink GetWidget(string widgetName) //not L10N safe!
        {
            var modal = Get<CUITe_HtmlDiv>("class=modal-content");
            return modal.Get<CUITe_HtmlHyperlink>("InnerText=" + widgetName);
        }

        // widget button actions
        public void ClickNewWidgetButton()
        {
            if (!NewWidgetButton.Exists || !NewWidgetButton.Enabled)
                ClickEditDashboardButton();

            NewWidgetButton.Click();
        }
        public void ClickEditWidgetStructureButton()
        {
            EditWidgetStructureButton.Click();
        }
        public void ClickEditDashboardButton()
        {
            EditDashboardButton.Click();
        }
        public void ClickCloseDashboardLayoutButton()
        {
            closeEditDash.Click();
        }
        public void ClickDailyWidgetsTab()
        {
            DailyWidgetsTab.SafeClick();
        }
        public void ClickWeeklyWidgetsTab()
        {
            WeeklyWidgetsTab.SafeClick();
        }
        public void ClickGeneralWidgetsTab()
        {
            GeneralWidgetsTab.SafeClick();
        }
        public void ClickCloseAddWidgetsModalButton()
        {
            CloseAddWidgetsModalBtn.Click();
        }
        public void ClickDefaultDashboardLook()
        {
            DefaultDashboardLook.Click();
        }

        #endregion

        #region Buttons at bottom of menu
        public void ClickSupportButton()
        {
            SupportLink.Click();
        }
        public void ClickEulaButton()
        {
            EulaBtn.Click();
        }
        public void ClickAboutButton()
        {
            AboutLink.Click();
        }
        #endregion

        #region Misc.

        public void Logout()
        {
            ClickLoggedInUsername();
            LogOutLink.Click();
            Playback.Wait(1000);
            Specify.That(LoginButton.Exists).Should.BeTrue("Login button should be displayed");
        }
        public void ClickLoggedInUsername()
        {
            LoggedInUsername.SafeClick();
        }
        public void CreateScreendump(string directory, string name)
        {
            Playback.Wait(250);
            var image = CaptureImage();
            image.Save(directory + name + ".png", System.Drawing.Imaging.ImageFormat.Png);
            image.Dispose();
        }
        
        #endregion

        #region Verify

        // notification
        public void VerifyNotificationContainsValue(int index, string value)
        {
            Specify.That(GetNotification(index).Text).Should.Contain(value,"The notification does not contain specified value");
        }
        public void VerifyLastNotificationContainsValue(string value)
        {
            Specify.That(GetLastNotification().Text).Should.Contain(value, "The notification does not contain specified value");
        }
        public void VerifyNumberOfNotifications(int numberOfNotifications)
        {
            Retry.For(() => Specify.That(GetNumberOfNotifications()).Should.BeEqualTo(numberOfNotifications), TimeSpan.FromSeconds(10));
        }

        // login
        public void VerifyDashboardIsAvailable()
        {
            var div = Get<CUITe_HtmlDiv>("id=dashboard");
            Specify.That(div.Exists && div.Enabled).Should.BeTrue("Dashboard is not available");
        }
        public void VerifyLoggedInUsernameFieldHasExpectedValue(string username)
        {
            Specify.That(LoggedInUsername.ToString().Contains(username));
        }
        public void VerifyMessageWhenLogInWithoutPassword()
        {
            var area = this.Get<CUITe_HtmlDiv>("class~dashboard-page");
            bool ok = area.InnerText.Contains("Invalid Username");
            Specify.That(ok).Should.BeTrue("When password is empty an error message should be shown.");
        }

        // miscellaneous
        public void VerifySearchBoxFirstLinkExists()
        {
            Specify.That(SearchBoxFirstLink.Enabled).Should.BeFalse("BoxFirst menu option should not exist");
        }

        #endregion

    }
}
