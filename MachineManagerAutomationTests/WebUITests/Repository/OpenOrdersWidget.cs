﻿using CUITe.Controls.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class OpenOrdersWidget : DynamicBrowserWindowWithLogin
    {
        #region Constructors
        public OpenOrdersWidget()
            : base("")
        { }
        #endregion

        private CUITe_HtmlDiv OrdersListDiv { get { return Get<CUITe_HtmlDiv>("id=ordersListDiv"); } }

        private CUITe_HtmlEdit Filter { get { return Get<CUITe_HtmlEdit>("id=orderFilter"); } }
        private CUITe_HtmlCheckBox ShowAllProductionGroupsCheckBox { get { return Get<CUITe_HtmlCheckBox>("id=displayAllPG"); } }

        private CUITe_HtmlButton CreateNextButton { get { return Get<CUITe_HtmlButton>("id=moveOrdersToNext"); } }
        private CUITe_HtmlButton CreateNextContinueButton { get { return Get<CUITe_HtmlButton>("id=btnContinueCreateNext"); } }
        private CUITe_HtmlButton CreateNextCloseButton { get { return Get<CUITe_HtmlButton>("id=btnCloseCreateNext"); } }

        private CUITe_HtmlButton CancelOrdersButton { get { return Get<CUITe_HtmlButton>("id=removeOrders"); } }
        private CUITe_HtmlButton CancelOrdersConfirmButton { get { return Get<CUITe_HtmlButton>("id=CancelOrderConfirmBtn"); } }
        private CUITe_HtmlButton CancelOrdersCloseButton { get { return Get<CUITe_HtmlButton>("id=CancelOrderCloseBtn"); } }

        private CUITe_HtmlEdit ReproduceOrderEdit { get { return Get<CUITe_HtmlDiv>("id=ReproduceQuantityNudDiv").Get<CUITe_HtmlEdit>(); } }
        private CUITe_HtmlButton ReproduceOrderUpButton { get { return Get<CUITe_HtmlDiv>("id=ReproduceQuantityNudDiv").Get<CUITe_HtmlButton>("id=nudIncrementButton"); } }
        private CUITe_HtmlButton ReproduceOrderDownButton { get { return Get<CUITe_HtmlDiv>("id=ReproduceQuantityNudDiv").Get<CUITe_HtmlButton>("id=nudDecrementButton"); } }
        private CUITe_HtmlButton ReproduceOrderSaveButton { get { return Get<CUITe_HtmlButton>("id=ReproduceItemsSave"); } }
        private CUITe_HtmlButton ReproduceOrderCloseButton { get { return Get<CUITe_HtmlButton>("id=ReproduceItemsClose"); } }

        private CUITe_HtmlButton ContinueDistributeRequestButton { get { return Get<CUITe_HtmlButton>("id=btnContinueDistributeRequest"); } }
        private CUITe_HtmlButton CloseDistributeRequestButton { get { return Get<CUITe_HtmlButton>("id=btnCloseDistributeRequest"); } }

        // button actions
        public void ClickCreateNextButton(bool confirm = true)
        {
            CreateNextButton.Click();
            if (confirm)
                CreateNextContinueButton.Click();
            else
                CreateNextCloseButton.Click();
        }
        public void ClickCreateNextContinueButton()
        {
            CreateNextContinueButton.Click();
        }
        public void ClickCancelOrdersButton(bool confirm = true)
        {
            CancelOrdersButton.Click();
            if (confirm)
                CancelOrdersConfirmButton.Click();
            else
                CancelOrdersCloseButton.Click();
        }
        public void SetReproduceOrderNumber(int number)
        {
            ReproduceOrderEdit.SafeSetText(number);
        }
        public void ClickCheckBoxForOrder(string orderName)
        {
            var cb = OrdersListDiv.Get<CUITe_HtmlCheckBox>(string.Format("id=cb{0}", orderName));
            cb.Check();
        }
        public void ClickReproduceButton(string orderName)
        {
            var div = OrdersListDiv.Get<CUITe_HtmlDiv>(string.Format("InnerText={0}", orderName));
            var btn = div.Get<CUITe_HtmlButton>("id=orderReproduce");
            btn.Click();
        }

        public bool IsOrderPresent(string orderName)
        {
            try { return OrdersListDiv.InnerText.Contains(orderName); }
            catch
            { return false; }
        }

        private CUITe_HtmlDiv GetOrderDiv(string orderName, CUITe_HtmlDiv ordersList)
        {
            return ordersList.Get<CUITe_HtmlDiv>(string.Format("InnerText={0}", orderName));
        }

        public bool IsOrderPresent(string orderName, CUITe_HtmlDiv ordersList)
        {
            try { return GetOrderDiv(orderName, ordersList).Exists; }
            catch
            { return false; }
        }

        public bool IsAnyOrderPresent()
        {
            try  { return Get<CUITe_HtmlDiv>("class=orderRowContainer").Exists; }
            catch
            { return false; }
        }
    }
}
