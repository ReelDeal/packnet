﻿using CUITe.Controls.HtmlControls;
using PackNet.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ConsoleBase : DynamicBrowserWindowWithLogin
    {
        internal void SelectMachineGroup(string alias, ConsoleBase console)
        {
            var repo = new PackNet.Data.Machines.MachineGroupRepository();
            var mg = repo.FindByAlias(alias);
            if (mg != null)
            {
                SelectMachineGroup(mg.Id, console);
            }
        }
        internal void SelectMachineGroup(Guid mgId, ConsoleBase console)
        {
            if (console is AdminConsole)
                ((AdminConsole)console).NavigateToPage(NavPages.ChangeMachineGroup);
            else
                ((OperatorConsole)console).NavigateToPage(OpNavPages.ChangeMachineGroup);

            Retry.For(() => Get<CUITe_HtmlButton>("id=machine" + mgId + "Button").Click(), TimeSpan.FromSeconds(10));
        }
    }
}
