﻿using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.Helpers;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Data.CartonPropertyGroups;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class CartonPropertyGroupsPage : CrudPageBase
    {
        private static string NameValidationIdProperty = "cpgNameRequired";

        // header and widget
        private CUITe_HtmlHeading4 PageTitle { get { return Get<CUITe_HtmlHeading4>("id=cpgPageTitle"); } }
        private AlertWidget AlertWidget { get { return Get<AlertWidget>("id=cpgAlerts"); } }

        // list
        private CUITe_HtmlButton NewButton { get { return Get<CUITe_HtmlButton>("id=newCartonPropertyGroup"); } }
        private CUITe_HtmlButton DeleteButton { get { return Get<CUITe_HtmlButton>("id=deleteCartonPropertyGroup"); } }
        private CUITe_HtmlButton EditButton { get { return Get<CUITe_HtmlButton>("id=editCartonPropertyGroup"); } }
        private Table _cpgTable = null;
        public Table CPGTable
        {
            get { 
                var div = Get<CUITe_HtmlDiv>("id=cartonPropertyGroupListTable");
                return _cpgTable ?? (_cpgTable = new Table(div, new TableConfigData(TableTypes.TwoColumn, "CartonPropertyGroup"))); 
            }
        }

        // form
        private CUITe_HtmlLabel NameLabel { get { return Get<CUITe_HtmlLabel>("id=lblEditCartonPropertyGroupName"); } }
        private CUITe_HtmlEdit NameEdit { get { return Get<CUITe_HtmlEdit>("name=cartonPropertyGroupAliasInputBox"); } }
        private CUITe_HtmlParagraph NameValidation { get { return Get<CUITe_HtmlParagraph>("id=cpgNameRequired"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newCartonPropertyGroupSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newCartonPropertyGroupCancelButton"); } }

        // methods
        public static CartonPropertyGroupsPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<CartonPropertyGroupsPage>();
        }
        public void RefreshTables()
        {
            _cpgTable = null;
        }
        public void PageTableSelectRow(int rowIndex)
        {
            CPGTable.SelectRow(rowIndex);
        }

        // button actions
        public void ClickSaveButton()
        {
            SaveButton.SafeClick();
        }
        public void ClickCancelButton(bool doubleClick = false)
        {
            CancelButton.SafeClick();
            if(doubleClick)
                CancelButton.SafeClick();
        }
        public void ClickNewButton()
        {
            NewButton.SafeClick();
        }
        public void ClickEditButton()
        {
            EditButton.SafeClick();
        }
        public void ClickDeleteButton()
        {
            DeleteButton.SafeClick();
        }

        // assign values to form fields
        public void SetName(string name)
        {
            NameEdit.SetText(name);
        }

        // crud
        public void Create(CartonPropertyGroup cpg, AdminConsole adminConsole = null, bool useValidation = false)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            adminConsole.NavigateToPage(NavPages.CartonPropertyGroup);
            ClickNewButton();
            VerifyEditPageIsOpen();
            EnterInfo(cpg);
            ClickSaveButton();

            if (useValidation)
            {
                // verify widget message
                VerifyCreatedSuccessfulMessage(cpg.Alias);

                //verify the dialog closed
                VerifyEditPageIsOpen(false);
            }
        }
        public void UpdateInfo(int rowIndex, CartonPropertyGroup cpg)
        {
            //Check the row
            PageTableSelectRow(rowIndex);

            //Verify that the edit button is enabled
            VerifyButtonIsEnabled(EditButton, "edit");

            //Click edit and change alias
            ClickEditButton();
            EnterInfo(cpg);
            ClickSaveButton();
            VerifyUpdatedSuccessfulMessage(this.AlertWidget, "Carton Property Group", cpg.Alias);
            RefreshTables();
        }
        private void EnterInfo(CartonPropertyGroup cpg)
        {
            SetName(cpg.Alias);
        }

        #region Page Verification
        public void VerifyDeleteButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(DeleteButton, "delete", enabled);
        }
        public void VerifyTableRowContainsSpecifiedData(int row, string alias)
        {
            base.VerifyTableRowContainsSpecifiedData(row, CPGTable, alias);
        }
        public void VerifyEditPageIsOpen(bool shouldBeOpen = true)
        {
            base.VerifyEditPageIsOpen(CancelButton, NewButton, shouldBeOpen);
        }
        public void VerifyNameValidation(bool shouldExist = true, string expectedText = null)
        {
            if (shouldExist)
                base.VerifyValidationIsDisplayed(NameValidation, "Name", shouldExist, expectedText);
            else
                VerifyNameValidationExists(shouldExist);
        }
        public void VerifyNameValidationExists(bool shouldExist = true)
        {
            base.VerifyValidationExists(CartonPropertyGroupsPage.NameValidationIdProperty, "Name", shouldExist);
        }
        public void VerifyTableIsEmpty(int remainingNumberOfRecords = 0)
        {
            base.VerifyTableIsEmpty(CPGTable, "Carton Property Group", remainingNumberOfRecords);
        }
        public void VerifyCreatedSuccessfulMessage(string alias)
        {
            base.VerifyCreatedSuccessfulMessage(this.AlertWidget, "Carton Property Group", alias);
        }
        public void VerifyUpdatedSuccessfulMessage(string alias)
        {
            base.VerifyDeleteSuccessfulMessage(this.AlertWidget, "Classification", alias);
        }
        public void VerifyDeleteSuccessfulMessage(string alias)
        {
            base.VerifyDeleteSuccessfulMessage(AlertWidget, "Carton Property Group", alias);
        }
        public void VerifyDuplicateAliasMessage(string alias)
        {
            base.VerifyDuplicateAliasMessage(this.AlertWidget, "Carton Property Group", alias);
        }
        #endregion
    }
}
