﻿using CUITe.Controls.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ServerSettingsPage : WindowBase
    {
        public CUITe_HtmlHeading4 PageHeader { get { return Get<CUITe_HtmlHeading4>("id=serverSettingsPageHeader"); } }

        public AlertWidget Alerts { get { return Get<AlertWidget>("id=packNetServerSettingsAlert"); } }

        public CUITe_HtmlListItem GeneralTab { get { return Get<CUITe_HtmlListItem>("id=generalTab"); } }
        public CUITe_HtmlListItem FileImportTab { get { return Get<CUITe_HtmlListItem>("id=fileImportTab"); } }
        public CUITe_HtmlListItem SldSettingsTab { get { return Get<CUITe_HtmlListItem>("id=articleImportSettingsTab"); } }

        public CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=packNetServerSettingsSaveButton"); } }
        public CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=packNetServerSettingsCancelButton"); } }

        //General Controls
        public CUITe_HtmlLabel CallbackIpAddressLabel { get { return Get<CUITe_HtmlLabel>("id=callbackIPAddressLabel"); } }
        public CUITe_HtmlEdit CallbackIpAddressEdit { get { return Get<CUITe_HtmlEdit>("id=callbackIPAddress"); } }
        public CUITe_HtmlEdit CallbackPorEditEdit { get { return Get<CUITe_HtmlEdit>("id=callbackPort"); } }
        public CUITe_HtmlButton IncrementButton { get { return Get<CUITe_HtmlButton>("id=nudIncrementButton"); } }
        public CUITe_HtmlButton DecrementButton { get { return Get<CUITe_HtmlButton>("id=nudDecrementButton"); } }
        public CUITe_HtmlComboBox WorkflowTrackingLevel { get { return Get<CUITe_HtmlComboBox>("id=workflowTrackingLevel"); } }

        //File Import Controls
        public CUITe_HtmlLabel DefaultImportTypeLabel { get { return Get<CUITe_HtmlLabel>("id=defaultImportTypeLabel"); } }
        public CUITe_HtmlComboBox DefaultImportType { get { return Get<CUITe_HtmlComboBox>("id=defaultImportType"); } }
        public CUITe_HtmlEdit DropfolderEdit { get { return Get<CUITe_HtmlEdit>("id=monitoredFolderPath"); } }
        public CUITe_HtmlEdit FileExtensionEdit { get { return Get<CUITe_HtmlEdit>("id=fileExtension"); } }
        public CUITe_HtmlEdit TimesToRetryToAccessMonitoredFilePathEdit { get { return Get<CUITe_HtmlEdit>("id=timesToRetryToAccessMonitoredFilePath"); } }
        public CUITe_HtmlEdit SecondsToRetryToAccessMonitoredFilePathEdit { get { return Get<CUITe_HtmlEdit>("id=secondsToRetryToAccessMonitoredFilePath"); } }
        public CUITe_HtmlEdit TimesToRetryLockedFileEdit { get { return Get<CUITe_HtmlEdit>("id=timesToRetryLockedFile"); } }
        public CUITe_HtmlEdit SecondsToRetryLockedFileEdit { get { return Get<CUITe_HtmlEdit>("id=secondsToRetryLockedFile"); } }
        public CUITe_HtmlEdit FieldDelimiterEdit { get { return Get<CUITe_HtmlEdit>("id=fieldDelimiter"); } }
        public CUITe_HtmlEdit CommentIndicatorEdit { get { return Get<CUITe_HtmlEdit>("id=commentIndicator"); } }

        public static ServerSettingsPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<ServerSettingsPage>();
        }

        // tabs
        public void ClickGeneralTab()
        {
            GeneralTab.SafeClick();
        }
        public void ClickFileImportTab()
        {
            FileImportTab.SafeClick();
        }
        public void ClickSldSettingsTab()
        {
            SldSettingsTab.SafeClick();
        }
    }
}
