﻿using CUITe.Controls.HtmlControls;
using Microsoft.VisualStudio.TestTools.UITesting;
using PackNet.Common.Utils;
using System;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class MachineDiscoveryPage : WindowBase

    {
        private Table _discoveredMachinesTable = null;

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newSaveMachineButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=discoverCancelButton"); } }
        private CUITe_HtmlButton RefreshButton { get { return Get<CUITe_HtmlButton>("id=discoverBrMachinesButton"); } }
        private AlertWidget Alerts { get { return Get<AlertWidget>("id=machinesManagementFusionTopAlert"); } }
        public Table DiscoveredMachinesTable
        {
            get { return _discoveredMachinesTable ?? (_discoveredMachinesTable = new Table(Get<CUITe_HtmlDiv>("id=discoveredMachinesListTable"), new TableConfigData(TableTypes.TwoColumn, "DiscoveredMachines"))); }
        }

        public void ClickSaveButton()
        {
            SaveButton.Click();
        }
        public void ClickCancelButton()
        {
            CancelButton.Click();
        }
        public void ClickRefreshButton()
        {
            RefreshButton.Click();
        }

        public void WaitForCancelButton()
        {
            while (!CancelButton.Exists)
            {
                Playback.Wait(2000); //wait for search to complete
            }
        }
        
        // verify

        public void VerifySaveButtonText(string nameOfButton)
        {
            Retry.For(() => SaveButton.Exists && SaveButton.Enabled, TimeSpan.FromSeconds(60));
            Specify.That(SaveButton.InnerText == nameOfButton).Should.BeTrue();
        }
    }
}
