﻿//using CUITe.Controls.HtmlControls;

//namespace MachineManagerAutomationTests.WebUITests.Repository
//{
//    public class BoxLastSearchWidget : DynamicBrowserWindowWithLogin
//    {
//        #region Constructors
//        public BoxLastSearchWidget()  : base("PackNet.Server")
//        { }

//        public BoxLastSearchWidget(string title)
//            : base(title)
//        {
//            sWindowTitle = title;
//            SearchProperties[PropertyNames.Name] = sWindowTitle;
//        }
//        #endregion

//        // table
//        private Table _resultTable = null;
//        public Table ResultTable
//        {
//            get {
//                var div = Get<CUITe_HtmlDiv>("id=resultGrid");
//                return _resultTable ?? (_resultTable = new Table(div, new TableConfigData(TableTypes.TwoColumn, "BoxLastSearchResult"))); 
//            }
//        }

//        // buttons
//        private CUITe_HtmlButton SearchButton { get { return Get<CUITe_HtmlButton>("id=searchButton"); } }
//        private CUITe_HtmlButton ReproduceButton { get { return Get<CUITe_HtmlButton>("id=reproduceButton"); } }
//        private CUITe_HtmlButton ReproduceConfirmButton { get { return Get<CUITe_HtmlButton>("id=ReproduceItemsSave"); } }

//        // button actions
//        public void ClickSearchButton()
//        {
//            SearchButton.Click();
//        }
//        public void ClickReproduceButton()
//        {
//            ReproduceButton.Click();
//        }
//        public void ClickReproduceConfirmButton()
//        {
//            ReproduceConfirmButton.Click();
//        }
//    }
//}
