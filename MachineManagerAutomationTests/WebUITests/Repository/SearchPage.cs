﻿using CUITe.Controls;
using CUITe.Controls.HtmlControls;
using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.Enums;
using Testing.Specificity;
using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class SearchPage : WindowBase
    {
        #region Constructors
        private Table _resultTable = null;
        private SelectionAlgorithmTypes _algorithm = null;
        public SearchPage(SelectionAlgorithmTypes algoritm)
            : base("Packnet.Server") {
                _algorithm = algoritm;
        }
        #endregion

        public CUITe_HtmlHeading4 PageTitle { get { return Get<CUITe_HtmlHeading4>("InnerText=Search Box First"); } }

        // general search fields
        public CUITe_HtmlComboBox StatusComboBox { get { return Get<CUITe_HtmlComboBox>("id=status"); } }
        public CUITe_HtmlEdit CustomerIdTextBox { get { return Get<CUITe_HtmlEdit>("id=customerUniqueId"); } }

        // calendar
        public CUITe_HtmlHyperlink OpenStartDatePickerButton { get { return Get<CUITe_HtmlHyperlink>("id=beginDate"); } }
        public CUITe_HtmlTable BeginDateTimePicker { 
            get {
                var wrapper = Get<CUITe_HtmlDiv>("id=beginDateWrapper");
                return wrapper.Get<CUITe_HtmlTable>("class=table table-striped"); 
            } 
        }

        public CUITe_HtmlHyperlink OpenEndDatePickerButton { get { return Get<CUITe_HtmlHyperlink>("id=endDate"); } }
        public CUITe_HtmlTable EndDateTimePicker 
        { 
            get {
                var wrapper = Get<CUITe_HtmlDiv>("id=endDateWrapper");
                return wrapper.Get<CUITe_HtmlTable>("class=table table-striped");
            } 
        }


        // additional search fields
        // -------------------------------------------

        // carton details
        public CUITe_HtmlEdit ArticleIdEdit { get { return Get<CUITe_HtmlEdit>("id=articleId"); } }
        public CUITe_HtmlComboBox DesignComboBox { get { return Get<CUITe_HtmlComboBox>("id=designId"); } }
        public CUITe_HtmlEdit LengthEdit { get { return Get<CUITe_HtmlEdit>("id=length"); } }
        public CUITe_HtmlEdit WidthEdit { get { return Get<CUITe_HtmlEdit>("id=width"); } }
        public CUITe_HtmlEdit HeightEdit { get { return Get<CUITe_HtmlEdit>("id=height"); } }

        // corrugate details
        public CUITe_HtmlComboBox CorrugateNameComboBox { get { return Get<CUITe_HtmlComboBox>("id=corrugateAlias"); } }
        public CUITe_HtmlComboBox CorrugateQualityComboBox { get { return Get<CUITe_HtmlComboBox>("id=corrugateQuality"); } }

        // machine group
        public CUITe_HtmlComboBox MachineGroupComboBox { get { return Get<CUITe_HtmlComboBox>("id=machineGroup"); } }

        // machine
        public CUITe_HtmlComboBox MachineComboBox { get { return Get<CUITe_HtmlComboBox>("id=machine"); } }

        // box first properties
        public CUITe_HtmlComboBox PickZoneComboBox { get { return Get<CUITe_HtmlComboBox>("id=searchPickZone"); } }
        public CUITe_HtmlComboBox ClassificationComboBox { get { return Get<CUITe_HtmlComboBox>("id=searchClassification"); } }
        public CUITe_HtmlComboBox CartonPropertyGroupComboBox { get { return Get<CUITe_HtmlComboBox>("id=searchCartonPropertyGroup"); } }

        // buttons
        public CUITe_HtmlButton ReproduceButton { get { return Get<CUITe_HtmlButton>("id=reproduceButton"); } }
        public CUITe_HtmlButton ReproduceConfirmButton { get { return Get<CUITe_HtmlButton>("id=ReproduceItemsSave"); } }
        public CUITe_HtmlButton ReproduceCancelButton { get { return Get<CUITe_HtmlButton>("id=ReproduceItemsClose"); } }
        public CUITe_HtmlButton SearchButton { get { return Get<CUITe_HtmlButton>("id=searchButton"); } }
        public CUITe_HtmlButton RemoveButton { get { return Get<CUITe_HtmlButton>("id=removeButton"); } }
        public CUITe_HtmlButton ToggleFiltering { get { return Get<CUITe_HtmlButton>("id=toggleFiltering"); } }
        public CUITe_HtmlButton CreateNextButton { get { return Get<CUITe_HtmlButton>("id=createNextButton"); } }
        public CUITe_HtmlButton CreateNextConfirmButton { get { return Get<CUITe_HtmlButton>("id=btnContinueCreateNext"); } }
        public CUITe_HtmlButton CreateNextCancelButton { get { return Get<CUITe_HtmlButton>("id=btnCloseCreateNext"); } }
        public CUITe_HtmlHyperlink AdditionalSearchOptionsToggleButton { get { return Get<CUITe_HtmlHyperlink>("id=toggleDetails"); } }

        // result table
        public Table ResultTable
        {
            get { return _resultTable ?? (_resultTable = new Table(Get<CUITe_HtmlDiv>("id=resultGrid"), new TableConfigData(TableTypes.TwoColumn, "SearchPageResult"))); }
        }


        // additional search fields
        // -------------------------------------------

        // general
        public void SetStatus(string status)
        {
            if(!TestUtilities.CheckIfValueExistsInComboBox(StatusComboBox, status))
                Specify.Failure(string.Format("There is no status called '{0}'.", status));

            StatusComboBox.SelectItem(status);
        }
        public void SetCustomerId(string customerId)
        {
            CustomerIdTextBox.SetText(customerId);
        }
        public void SetStartDate(DateTime date)
        {
            SetDateTimeInPicker(date, OpenStartDatePickerButton, BeginDateTimePicker);
        }
        public void SetEndDate(DateTime date)
        {
            SetDateTimeInPicker(date, OpenEndDatePickerButton, EndDateTimePicker);
        }

        // carton details
        public void SetArticle(string alias)
        {
            if (_algorithm != SelectionAlgorithmTypes.Order)
                Specify.Failure(string.Format("The search criteria ArticleId can't be used for '{0}'", _algorithm.ToString()));

            ArticleIdEdit.SetText(alias);
        }
        public void SetDesign(string designAlias)
        {
            if (!TestUtilities.CheckIfValueExistsInComboBox(DesignComboBox, designAlias))
                Specify.Failure(string.Format("There is no design in the design menu called '{0}'.", designAlias));

            DesignComboBox.SelectItem(designAlias);
        }
        public void SetLength(int length)
        {
            LengthEdit.SetText(length.ToString());
        }
        public void SetHeight(int height)
        {
            HeightEdit.SetText(height.ToString());
        }
        public void SetWidth(int width)
        {
            WidthEdit.SetText(width.ToString());
        }

        // corrugate details
        public void SetCorrugateName(string name)
        {
            if (!TestUtilities.CheckIfValueExistsInComboBox(CorrugateNameComboBox, name))
                Specify.Failure(string.Format("There is no corrugate called '{0}'.", name));

            CorrugateNameComboBox.SelectItem(name);
        }
        public void SetCorrugateQuality(int quality)
        {
            if (!TestUtilities.CheckIfValueExistsInComboBox(CorrugateQualityComboBox, quality.ToString()))
                Specify.Failure(string.Format("There is no corrugate quality called '{0}'.", quality.ToString()));

            CorrugateQualityComboBox.SelectItem(quality.ToString());
        }

        // machine
        public static SearchPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<SearchPage>();
        }
        public void SetMachine(string name)
        {
            if (_algorithm != SelectionAlgorithmTypes.Order)
                Specify.Failure(string.Format("The search criteria Machine can't be used for '{0}'", _algorithm.ToString()));

            if (!TestUtilities.CheckIfValueExistsInComboBox(MachineComboBox, name))
                Specify.Failure(string.Format("There is no machine called '{0}'.", name));

            MachineComboBox.SelectItem(name);
        }

        // machine group
        public void SetMachineGroup(string name)
        {
            if (_algorithm != SelectionAlgorithmTypes.BoxFirst)
                Specify.Failure(string.Format("The search criteria Machine Group can't be used for '{0}'", _algorithm.ToString()));

            if (!TestUtilities.CheckIfValueExistsInComboBox(MachineGroupComboBox, name))
                Specify.Failure(string.Format("There is no machine group called '{0}'.", name));

            MachineGroupComboBox.SelectItem(name);
        }

        // box first properties
        public void SetPickZone(string name)
        {
            if (_algorithm != SelectionAlgorithmTypes.BoxFirst)
                Specify.Failure(string.Format("The search criteria PickZone can't be used for '{0}'", _algorithm.ToString()));

            if (!TestUtilities.CheckIfValueExistsInComboBox(PickZoneComboBox, name))
                Specify.Failure(string.Format("There is no pick zone called '{0}'.", name));

            PickZoneComboBox.SelectItem(name);
        }
        public void SetClassification(string name)
        {
            if (_algorithm != SelectionAlgorithmTypes.BoxFirst)
                Specify.Failure(string.Format("The search criteria Classification can't be used for '{0}'", _algorithm.ToString()));

            if (!TestUtilities.CheckIfValueExistsInComboBox(ClassificationComboBox, name))
                Specify.Failure(string.Format("There is no classification called '{0}'.", name));

            ClassificationComboBox.SelectItem(name);
        }
        public void SetCartonPropertyGroup(string name)
        {
            if (_algorithm != SelectionAlgorithmTypes.BoxFirst)
                Specify.Failure(string.Format("The search criteria Carton Property Group can't be used for '{0}'", _algorithm.ToString()));

            if (!TestUtilities.CheckIfValueExistsInComboBox(CartonPropertyGroupComboBox, name))
                Specify.Failure(string.Format("The value '{0}' does not exist in the menu '{1}'.", name, "Carton Property Group"));
                    
            CartonPropertyGroupComboBox.SelectItem(name);
        }

        // button actions
        public void ClickSearchButton()
        {
            SearchButton.Click();
        }
        public void ClickCreateNextButton(bool confirm = true)
        {
            CreateNextButton.Click();
            if (confirm)
                CreateNextConfirmButton.Click();
            else
                CreateNextCancelButton.Click();
        }
        public void ClickReproduceButton(bool confirm = true)
        {
            Specify.That(ReproduceButton.Enabled).Should.BeTrue();
            ReproduceButton.Click();

            if (confirm)
                ReproduceConfirmButton.Click();
            else
                ReproduceCancelButton.Click();
        }

        public void ClickReproduceConfirmButton()
        {
            ReproduceConfirmButton.Click();
        }

        public void ClickRemoveButton()
        {
            RemoveButton.Click();
        }
        public void ClickToggleFilteringButton()
        {
            ToggleFiltering.Click();
        }
        public void ClickToggleAdditionalOptionsButton()
        {
            AdditionalSearchOptionsToggleButton.Click();
        }
        public void ClickOpenStartDatePickerButton()
        {
            Specify.That(OpenStartDatePickerButton.Exists && OpenEndDatePickerButton.Exists).Should.BeEqualTo(true);
            OpenStartDatePickerButton.Click();
        }

        // verify
        public void VerifySearchPageIsLoaded()
        {
            Specify.That(IsSearchBoxFirstPageLoaded()).Should.BeTrue("Search page did not load");
        }

        #region Misc. methods
        public void PickDayAnd12AMInTheTable(CUITe_HtmlTable timePicker, int index)
        {
            ClickLeftArrowInDateTable(timePicker);
            GetDayInCalanderAndClick(timePicker, index);
            Get12AMCellAndClick(timePicker);
        }
        private void ClickLeftArrowInDateTable(CUITe_HtmlTable timePicker)
        {
            timePicker.GetHeader(0, 0);
        }
        private void GetDayInCalanderAndClick(CUITe_HtmlTable timePicker, int index)
        {
            var dateTable = timePicker.GetCell(2, 0);
            dateTable.FirstChild.Click();
            //timePicker.FindCellAndClick(2, 0);
        }
        private void Get12AMCellAndClick(CUITe_HtmlTable timePicker)
        {
            timePicker.FindCellAndClick(2, 0);
        }
        public bool IsSearchBoxFirstPageLoaded()
        {
            return this.PageTitle.Exists;
        }
        public CUITe_HtmlDiv BoxFirst_FindResultGridRowCheckBoxDiv(int rowId)
        {
            var cbxColWrapper = ResultTable.TableDiv.FirstChild;

            var nextColWrapper = (CUITe_HtmlDiv)cbxColWrapper.GetChildren()[1];

            var nextNextColWrapper = (CUITe_HtmlDiv)nextColWrapper.FirstChild;

            var nonHeaderRowRapper = (CUITe_HtmlDiv)nextNextColWrapper.GetChildren()[1];

            var rowCanvas = (CUITe_HtmlDiv)nonHeaderRowRapper.Get<CUITe_HtmlDiv>("class=ui-grid-canvas");

            var row = (CUITe_HtmlDiv)rowCanvas.GetChildren()[rowId];

            var checkBoxCell = (CUITe_HtmlDiv)row.FirstChild.GetChildren()[1];

            var cellWrapper = checkBoxCell.Get<CUITe_HtmlDiv>("class=ui-grid-cell-contents");

            var cell = cellWrapper.Get<CUITe_HtmlDiv>();

            return cell;
        }
        public CUITe_HtmlDiv BoxFirst_FindResultGridHeaderCheckBoxDiv()
        {
            var cbxColWrapper = ResultTable.TableDiv.FirstChild;

            var nextColWrapper = (CUITe_HtmlDiv)cbxColWrapper.GetChildren()[1];

            var nextNextColWrapper = (CUITe_HtmlDiv)nextColWrapper.FirstChild;

            var HeaderRowRapper = (CUITe_HtmlDiv)nextNextColWrapper.FirstChild;

            var checkBoxCell = (CUITe_HtmlDiv)HeaderRowRapper.Get<CUITe_HtmlDiv>("class=ui-grid-header-cell-row").GetChildren()[1];

            var cellWrapper = checkBoxCell.Get<CUITe_HtmlDiv>("class=ui-grid-cell-contents");

            var cell = cellWrapper.Get<CUITe_HtmlDiv>();

            return cell;
        }
        private void SetDateTimeInPicker(DateTime date, CUITe_HtmlHyperlink openButton, CUITe_HtmlTable picker)
        {
            openButton.Click();

            string s = string.Empty;

            var data = picker.GetChildren().FindAll(z => z is CUITe_HtmlDiv);
            var div = (CUITe_HtmlDiv)data[0];

            //List<ICUITe_ControlBase> items = picker.GetChildren();
            //string month = ((CUITe_HtmlCustom)items)[0].InnerText.Split('\r');
            //foreach (var c in picker.GetChildren())
            //{
            //    var h = (CUITe_HtmlCustom)c;
            //    var h2 = (CUITe_HtmlCustom)h.FirstChild;
            //}

            //var leftArrow = picker.Get<CUITe_Html>("class=left");
            //leftArrow.Click();

            //var rightArrow = picker.Get<CUITe_HtmlCell>("class=right");
            //rightArrow.Click();

            var day = picker.Get<CUITe_HtmlCell>("InnerText=" + date.Day);
            day.Click();

            string partOfDay = "AM";
            if (date.Hour > 12)
                partOfDay = "PM";

            var time = picker.Get<CUITe_HtmlSpan>("InnerText=" + date.Hour + ":00 " + partOfDay);
            time.Click();
        }
        #endregion

        public void RefreshTables()
        {
            _resultTable = null;
        }

        public bool ContainsValue(string value)
        {
            return this.Get<CUITe_HtmlSpan>("InnerText=" + value).Exists;
        }
        public void VerifyPageContainsValue(string value)
        {
            Specify.That(ContainsValue(value)).Should.BeTrue(string.Format("The page should contain the value '{0}'",value));
        }
    }
}
