﻿//using CUITe.Controls.HtmlControls;

//namespace MachineManagerAutomationTests.WebUITests.Repository
//{
//    public class OrderWidget
//    {
//        private CUITe_HtmlDiv div;

//        public OrderWidget(CUITe_HtmlDiv cuiTeHtmlDiv)
//        {
//            div = cuiTeHtmlDiv;
//        }

//        public string ArticleId
//        {
//            get
//            {

//                string varId = div.Get<CUITe_HtmlDiv>("id=articleId").InnerText.Replace("Article ", string.Empty);
//                return varId;
//            }
//        }

//        public string OrderId
//        {
//            get
//            {
//                string varId = div.Get<CUITe_HtmlDiv>("id=orderId").InnerText;
//                return varId;
//            }
//        }

//        public string OrderStatus
//        {
//            get
//            {
//                string varId = ProgressBar.Get<CUITe_HtmlDiv>("id=orderStatus").InnerText;
//                return varId;
//            }
//        }

//        public CUITe_HtmlCheckBox SelectCheckBox { get { return div.Get<CUITe_HtmlCheckBox>("id=checkbox"); } }

//        public int RemainingQuantity
//        {
//            get
//            {
//                return int.Parse(div.Get<CUITe_HtmlDiv>("id=orderQuantity").InnerText.Split('/')[0]);
//            }
//        }

//        public int OriginalQuantity
//        {
//            get
//            {
//                return int.Parse(div.Get<CUITe_HtmlDiv>("id=orderQuantity").InnerText.Split('/')[1]);
//            }
//        }

//        public CUITe_HtmlButton Reproduce
//        {
//            get
//            {
//                return div.Get<CUITe_HtmlButton>("id=orderReproduce");
//            }
//        }

//        public CUITe_HtmlButton Distribute
//        {
//            get
//            {
//                return div.Get<CUITe_HtmlButton>("id=orderDistribute");
//            }
//        }

//        public CUITe_HtmlDiv ProgressBar
//        {
//            get
//            {
//                return div.Get<CUITe_HtmlDiv>("id=progressBar");
//            }
//        }

//        public CUITe_HtmlDiv OrderDetails
//        {
//            get
//            {
//                return div.Get<CUITe_HtmlDiv>("id=orderDetails");
//            }
//        }

//        public int MachinesThatHaveWorkedOnOrder
//        {
//            get
//            {
//                return int.Parse(OrderDetails.Get<CUITe_HtmlSpan>("id=orderMachineCounts").InnerText.Split('/')[0]);
//            }
//        }

//        public int AvailableMachinesCount
//        {
//            get
//            {
//                return int.Parse(OrderDetails.Get<CUITe_HtmlSpan>("id=orderMachineCounts").InnerText.Split('/')[1]);
//            }
//        }

//        public CUITe_HtmlDiv OrderBestCorrugate
//        {
//            get
//            {
//                return OrderDetails.Get<CUITe_HtmlDiv>("id=orderBestCorrugate");
//            }
//        }


//    }
//}
