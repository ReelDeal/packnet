﻿using System;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Utils;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public enum CpgWidgetListColumn
    {
        Requests,
        Expedite,
        Stopped,
        Last
    }

    public class CartonPropertyGroupsWidget : WindowBase
    {
        public bool GetStatus(CartonPropertyGroup cpg, CartonPropertyGroupStatuses status)
        {
            return GetStatus(cpg.Alias, status);
        }

        public bool GetStatus(string alias, CartonPropertyGroupStatuses status)
        {
            if (status == CartonPropertyGroupStatuses.Surge)
                return GetSurgeButton(alias).IsSelected;
            if (status == CartonPropertyGroupStatuses.Stop)
                return GetStopButton(alias).IsSelected;

            return GetNormalButton(alias).IsSelected;
        }

        public void SetStatus(CartonPropertyGroup cpg, CartonPropertyGroupStatuses status)
        {
            SetStatus(cpg.Alias, status);
        }

        public void SetStatus(string alias, CartonPropertyGroupStatuses status)
        {
            CUITe_HtmlRadioButton rbtn = null;
            if (status == CartonPropertyGroupStatuses.Surge)
                rbtn = GetSurgeButton(alias);
            else if (status == CartonPropertyGroupStatuses.Stop)
                rbtn = GetStopButton(alias);
            else
                rbtn = GetNormalButton(alias);

            if (rbtn.Exists && rbtn.Enabled)
                Retry.For(rbtn.Click, TimeSpan.FromSeconds(2));

            Specify.That(rbtn.IsSelected).Should.BeEqualTo(true);

        }

        public int GetNumberOfRequests(CartonPropertyGroup cpg, CpgWidgetListColumn column)
        {
            return GetNumberOfRequests(cpg.Alias, column);
        }

        public int GetNumberOfRequests(string alias, CpgWidgetListColumn column)
        {
            string columnName = "";
            if (column == CpgWidgetListColumn.Last)
                columnName = "Last";
            else if (column == CpgWidgetListColumn.Expedite)
                columnName = "Expedited";
            else if (column == CpgWidgetListColumn.Stopped)
                columnName = "Stopped";
            else
                columnName = ""; // Requests does not need value here

            var ctrl = Get<CUITe_HtmlLabel>("id=cpg-NumberOf" + columnName + "Requests-" + alias);
            Specify.That(ctrl.Exists && ctrl.Exists).Should.BeTrue();
            return Convert.ToInt32(ctrl.InnerText.Trim());
        }

        public void VerifyNumberOfRequests(CartonPropertyGroup cpg, CpgWidgetListColumn column, int expectedNoOfRequests)
        {
            var noOfRequests = GetNumberOfRequests(cpg, column);
            Specify.That(noOfRequests).Should.BeEqualTo(expectedNoOfRequests, string.Format("VerifyNumberOfRequests for cpg '{0}' failed. Expected: {1} Actual: {2}.", cpg.Alias, expectedNoOfRequests, noOfRequests));
        }

        public void VerifyStatus(CartonPropertyGroup cpg, CartonPropertyGroupStatuses status, bool value)
        {
            bool actualStatus = GetStatus(cpg, status);
            Specify.That(actualStatus).Should.BeEqualTo(value, string.Format("VerifyStatus for cpg '{0}' and status '{1}' failed. Expected: {2} Actual: {3}.", cpg.Alias, status.DisplayName, value, actualStatus));
        }

        #region internal
        private CUITe_HtmlRadioButton GetStopButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=cpg-stop-button-" + alias);
        }

        private CUITe_HtmlRadioButton GetNormalButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=cpg-normal-button-" + alias);
        }

        private CUITe_HtmlRadioButton GetSurgeButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=cpg-surge-button-" + alias);
        }
        #endregion
    }
}