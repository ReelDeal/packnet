﻿using Testing.Specificity;

using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.Helpers;
using PackNet.Common.Interfaces.DTO.Classifications;
using System;
using PackNet.Data.Classifications;
using PackNet.Common.Utils;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ClassificationsPage : CrudPageBase, ICrudLocalizationPage
    {
        private static string NameValidationIdProperty = "ClassificationNameValidation";

        // header and widget
        private CUITe_HtmlHeading4 PageTitle { get { return Get<CUITe_HtmlHeading4>("id=ClassificationsAdminTitle"); } }
        private AlertWidget AlertWidget { get { return Get<AlertWidget>("id=ClassificationAlert"); } }

        // list
        private CUITe_HtmlButton NewButton { get { return Get<CUITe_HtmlButton>("id=newClassification"); } }
        private CUITe_HtmlButton EditButton { get { return Get<CUITe_HtmlButton>("id=editClassification"); } }
        private CUITe_HtmlButton DeleteButton { get { return Get<CUITe_HtmlButton>("id=deleteClassification"); } }
        private Table _classificationsTable = null;
        public Table ClassificationsTable
        {
            get
            {
                var ctrl = Get<CUITe_HtmlDiv>("id=classificationListTable");
                return _classificationsTable ?? (_classificationsTable = new Table(ctrl, new TableConfigData(TableTypes.TwoColumn, "Classification")));
            }
        }

        // form
        private CUITe_HtmlLabel NameLabel { get { return Get<CUITe_HtmlLabel>("id=ClassificationNameLbl"); } }
        private CUITe_HtmlEdit NameEdit { get { return Get<CUITe_HtmlEdit>("id=newClassificationClassificationName"); } }
        private CUITe_HtmlParagraph NameValidation { get { return Get<CUITe_HtmlParagraph>("id=ClassificationNameValidation"); } }
        private CUITe_HtmlEdit NumberEdit { get { return Get<CUITe_HtmlEdit>("id=newClassificationNumberNUD"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newClassificationSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newClassificationCancelButton"); } }

        // methods
        public void RefreshTables()
        {
            _classificationsTable = null;
        }
        public static ClassificationsPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<ClassificationsPage>();
        }
        public void PageTableSelectRow(int rowIndex)
        {
            ClassificationsTable.SelectRow(rowIndex);
        }

        // buttons actions
        public void ClickSaveButton()
        {
            SaveButton.SafeClick();
        }
        public void ClickCancelButton(bool doubleClick = false)
        {
            CancelButton.SafeClick();
            if(doubleClick)
                CancelButton.SafeClick();
        }
        public void ClickNewButton()
        {
            Specify.That(NewButton).Should.Not.BeNull();
            NewButton.SafeClick();
        }
        public void ClickEditButton()
        {
            VerifyEditButtonIsEnabled();
            EditButton.SafeClick();
        }
        public void ClickDeleteButton()
        {
            DeleteButton.SafeClick();
        }

        // crud
        public void Create(Classification classification, AdminConsole adminConsole = null, bool useValidation = false)
        {
            // login if adminConsole is null
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            // go to classification page
            adminConsole.NavigateToPage(NavPages.Classification);

            //create a new classification
            ClickNewButton();

            // check that the edit page is opened
            VerifyEditPageIsOpen();

            // enter info to form
            EnterInfo(classification);
            
            //click save
            ClickSaveButton();

            if (useValidation)
            {
                // verify widget message
                VerifyCreatedSuccessfulMessage(classification.Alias);

                // verify the dialog closed
                VerifyEditPageIsOpen(false);
            }
        }
        public void UpdateInfo(int rowIndex, Classification classification, bool useValidation = false)
        {
            PageTableSelectRow(rowIndex);
            ClickEditButton();
            EnterInfo(classification);
            ClickSaveButton();

            if (useValidation)
            {
                base.VerifyUpdatedSuccessfulMessage(this.AlertWidget, "Classification", classification.Alias);
            }
        }
        /// <summary>
        /// Check that columns 'Alias' and 'Number' contain the supplied values
        /// </summary>
        /// <param name="row">The row to check at</param>
        /// <param name="page">The page where the table is located</param>
        /// <param name="alias">The alias the table cell 'Alias' should contain</param>
        /// <param name="number">The number the table cell 'Number' should contain. If omitted the value is not used when verifying</param>
        public void EnterInfo(Classification classification)
        {
            VerifyEditPageIsOpen();
            SetName(classification.Alias);
            SetNumber(classification.Number);
        }

        // assign values to fields
        public void SetName(string name)
        {
            NameEdit.SafeSetText(name);
        }
        public void SetNumber(string number)
        {
            NumberEdit.SafeSetText(number);
        }
        public string GetPageTitle()
        {
            return PageTitle.InnerText;
        }

        #region Page Verification
        public void VerifyDuplicateAliasMessage(string alias)
        {
            base.VerifyDuplicateAliasMessage(this.AlertWidget, "Classification", alias);
        }
        public void VerifyEditButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.EditButton, "edit", enabled);
        }
        public void VerifyDeleteButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.DeleteButton, "delete", enabled);
        }
        public void VerifyNameValidationIsDisplayed(bool shouldExist = true, string expectedText = null)
        {
            base.VerifyValidationIsDisplayed(NameValidation,"Name", shouldExist);
        }
        public void VerifyNameValidationExists(bool shouldExist = true)
        {
            base.VerifyValidationExists(ClassificationsPage.NameValidationIdProperty, "Name", shouldExist);
        }
        public void VerifyTableIsEmpty(int remainingNumberOfRecords = 0)
        {
            base.VerifyTableIsEmpty(ClassificationsTable, "Classification", remainingNumberOfRecords);
        }
        public void VerifyTableRowContainsSpecifiedData(int row, string alias, string number = null)
        {
            base.VerifyTableRowContainsSpecifiedData(0, ClassificationsTable, alias, null, number);
        }
        public void VerifyEditPageIsOpen(bool shouldBeOpen = true)
        {
            base.VerifyEditPageIsOpen(CancelButton, NewButton, shouldBeOpen);
        }
        public void VerifyCreatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyCreatedSuccessfulMessage(this.AlertWidget, "Classification", alias, expectedText);
        }
        public void VerifyUpdatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyDeleteSuccessfulMessage(this.AlertWidget, "Classification", alias, expectedText);
        }
        public void VerifyDeleteSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyDeleteSuccessfulMessage(AlertWidget, "Classification", alias, expectedText);
        }
        public void VerifyPageTitle(string value)
        {
            base.VerifyPageTitle(GetPageTitle(), value);
        }
        public void VerifyNameLabel(string value)
        {
            base.VerifyNameLabel(NameLabel, value);
        }
        #endregion
    }
}
