﻿using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.Macros;
using Microsoft.VisualStudio.TestTools.UITesting;
using PackNet.FusionSim.ViewModels;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ServicePage : WindowBase
    {
        #region Private properties
        private CUITe_HtmlButton ServicePauseButton { get { return Get<CUITe_HtmlButton>("id=machineServicePause"); } }
        private CUITe_HtmlButton EnableServiceMode { get { return Get<CUITe_HtmlButton>("id=enableMachineServiceMode"); } }
        private CUITe_HtmlButton DisableServiceMode { get { return Get<CUITe_HtmlButton>("id=disableMachineServiceMode"); } }
        private CUITe_HtmlButton GetMachineButton(string alias)
        {
            var search = "machine" + MachineMacros.FindByAlias(alias, MachineType.Fusion).Id + "Button";
            return Get<CUITe_HtmlButton>("id=" + search);
        }
        #endregion

        #region Buttons
        public void ClickServicePauseButton()
        {
            ServicePauseButton.SafeClick();
        }

        public void ClickEnableServiceModeButton()
        {
            EnableServiceMode.SafeClick();
        }

        public void ClickDisableServiceModeButton()
        {
            DisableServiceMode.SafeClick();
        }
        public void ClickMachineButton(string alias)
        {
            GetMachineButton(alias).Click();
        }
        #endregion

        public void CreateScreenshot(AdminConsole adminConsole, string directory, FusionSimulatorViewModel sim)
        {
            Playback.Wait(1000);

            adminConsole.NavigateToPage(NavPages.ServicePage);

            adminConsole.CreateScreendump(directory, "ServicePage_LandingPage");

            ClickMachineButton(TestDatabaseManagement.Fm1.Alias);
            ClickServicePauseButton();
            ClickEnableServiceModeButton();
            Playback.Wait(2000); //let the page load
            adminConsole.CreateScreendump(directory, "ServicePage_ServicePage");
            ClickDisableServiceModeButton();
            Playback.Wait(500);
            sim.StartupComplete();
        }
    }
}
