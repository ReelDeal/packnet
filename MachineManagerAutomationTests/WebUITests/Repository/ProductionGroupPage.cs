﻿using System;
using System.Linq;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;

using Testing.Specificity;
using MachineManagerAutomationTests.Helpers;
using PackNet.Common.Interfaces.Enums;
using System.Collections.Generic;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ProductionGroupPage : CrudPageBase, ICrudLocalizationPage
    {
        private static string NameValidationIdProperty = "newProductionGroupAliasError";
        private static string ProductionModeValidationIdProperty = "newProductionGroupSelectionAlgorithmError";
        

        // title and widget
        private CUITe_HtmlHeading4 PageTitle { get { return Get<CUITe_HtmlHeading4>("id=ProductionGroupsPageHeader"); } }
        private AlertWidget ProductionGroupAlert { get { return Get<AlertWidget>("id=ProductionGroupAlert"); } }
        private AlertWidget newProductionGroupAlert { get { return Get<AlertWidget>("id=newProductionGroupAlert"); } }

        // list
        private CUITe_HtmlButton NewButton { get { return Get<CUITe_HtmlButton>("id=newProductionGroup"); } }
        private CUITe_HtmlButton DeleteButton { get { return Get<CUITe_HtmlButton>("id=deleteProductionGroup"); } }
        private CUITe_HtmlButton EditButton { get { return Get<CUITe_HtmlButton>("id=editProductionGroup"); } }
        public Table ProductionGroupTable
        {
            get
            {
                var div = Get<CUITe_HtmlDiv>("id=productionGroupListTable");
                return new Table(div, new TableConfigData(TableTypes.TwoColumn, "ProductionGroup"));
            }
        }

        // form
        private CUITe_HtmlLabel NameLabel { get { return Get<CUITe_HtmlLabel>("id=newProductionGroupAliasLabel"); } }
        private CUITe_HtmlEdit NameEdit { get { return Get<CUITe_HtmlEdit>("id=newProductionGroupAlias"); } }
        private CUITe_HtmlTextArea DescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=newProductionGroupDescription"); } }
        private CUITe_HtmlComboBox ProductionModeMenu { get { return Get<CUITe_HtmlComboBox>("id=newProductionGroupSelectionAlgorithm"); } }

        private CUITe_HtmlDiv MachineGroupsDiv { get { return Get<CUITe_HtmlDiv>("id=machineGroupsDiv"); } }
        private CUITe_HtmlDiv CorrugatesDiv { get { return Get<CUITe_HtmlDiv>("id=corrugatesDiv"); } }
        private CUITe_HtmlDiv CartonPropertyGroupsDiv { get { return Get<CUITe_HtmlDiv>("id=cartonPropertyGroupsDiv"); } }

        private CUITe_HtmlCheckBox ShowTriggerInterfaceCheckBox { get { return Get<CUITe_HtmlCheckBox>("id=showTriggerInterface"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newProductionGroupSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newProductionGroupCancelButton"); } }

        // validation Controls
        private CUITe_HtmlParagraph NameValidation { get { return Get<CUITe_HtmlParagraph>("id=newProductionGroupAliasError"); } }
        private CUITe_HtmlParagraph ProductionModeValidation { get { return Get<CUITe_HtmlParagraph>("id=newProductionGroupSelectionAlgorithmError"); } }
        private CUITe_HtmlParagraph MachineGroupsError { get { return Get<CUITe_HtmlParagraph>("id=newProductionGroupMachineGroupsError"); } }
        private CUITe_HtmlParagraph CorrugatesError { get { return Get<CUITe_HtmlParagraph>("id=newProductionGroupCorrugatesError"); } }

        // scanToCreate Specific
        private CUITe_HtmlComboBox BarcodeParsingWorkflowComboBox { get { return Get<CUITe_HtmlComboBox>("id=newBarcodeParsingWorkflowPath"); } }
        private CUITe_HtmlCheckBox STCShowTriggerInterfaceCheckbox { get { return Get<CUITe_HtmlCheckBox>("id=stq_showTriggerInterface"); } }

        // methods
        private CUITe_HtmlCheckBox GetMachineGroupCheckBox(string id)
        {
            Retry.For(() => MachineGroupsDiv.Exists, TimeSpan.FromSeconds(60));
            return MachineGroupsDiv.Get<CUITe_HtmlCheckBox>("id=" + id);
        }
        private CUITe_HtmlCheckBox GetCorrugateCheckBox(string id)
        {
            Retry.For(() => CorrugatesDiv.Exists, TimeSpan.FromSeconds(60));
            return CorrugatesDiv.Get<CUITe_HtmlCheckBox>("id=" + id);
        }
        private CUITe_HtmlCheckBox GetCPGCheckBox(string id)
        {
            Retry.For(() => CartonPropertyGroupsDiv.Exists, TimeSpan.FromSeconds(60));
            return CartonPropertyGroupsDiv.Get<CUITe_HtmlCheckBox>("id=" + id);
        }
        public void SetCpgSurgeCount(int count)
        {
            var countEdit = GetCpgSurgeCountEdit();
            Retry.For(() => countEdit.SetText(count.ToString()), TimeSpan.FromSeconds(10));
            Specify.That(countEdit.GetText()).Should.BeEqualTo(count.ToString());
        }
        private CUITe_HtmlEdit GetCpgSurgeCountEdit()
        {
            Retry.For(() => Specify.That(CartonPropertyGroupsDiv.Exists).Should.BeTrue(), TimeSpan.FromSeconds(60));
            return CartonPropertyGroupsDiv.Get<CUITe_HtmlEdit>("id=pgSurgeCountNUD");
        }
        public void VerifySTCShowTriggerInterfaceIsChecked()
        {
            Specify.That(STCShowTriggerInterfaceCheckbox.Checked).Should.BeTrue("Trigger checkbox should be checked");
        }
        public static ProductionGroupPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<ProductionGroupPage>();
        }
        public void PageTableSelectRow(int rowIndex)
        {
            ProductionGroupTable.SelectRow(rowIndex);
        }

        // button actions
        public void ClickNewButton()
        {
            NewButton.SafeClick();
        }
        public void ClickEditButton()
        {
            VerifyEditButtonIsEnabled();
            EditButton.SafeClick();
        }
        public void ClickDeleteButton()
        {
            Specify.That(DeleteButton.Enabled).Should.BeTrue("Delete button not enabled");
            DeleteButton.SafeClick();
        }
        public void ClickSaveButton()
        {
            Specify.That(SaveButton.Enabled).Should.BeTrue("Save button should be true");
            SaveButton.SafeClick();
        }
        public void ClickCancelButton(bool doubleClick = false)
        {
            CancelButton.SafeClick();
            if (doubleClick)
                CancelButton.SafeClick();
        }
        public void ClickNameField()
        {
            NameEdit.SafeClick();
        }
        public void ClickDescriptionField()
        {
            DescriptionEdit.SafeClick();
        }
        public void ClickProductionModeMenu()
        {
            ProductionModeMenu.SafeClick();
        }
        public void ClickSTCShowTriggerInterface(bool check = true)
        {
            if (check)
            {
                STCShowTriggerInterfaceCheckbox.Check();
                Specify.That(STCShowTriggerInterfaceCheckbox.Checked).Should.BeFalse("Trigger checkbox should be checked");
            }
            else
            {
                STCShowTriggerInterfaceCheckbox.UnCheck();
                Specify.That(STCShowTriggerInterfaceCheckbox.Checked).Should.BeFalse("Trigger checkbox should be un-checked");
            }
        }

        public void ClickBarcodeParsingWorkflow(string workflow)
        {
            BarcodeParsingWorkflowComboBox.SafeSelectItem(workflow);
        }
        public void ToggleCartonPropertyGroupCheckboxes(List<string> cpgList)
        {
            foreach (var cb in cpgList.Select(GetCPGCheckBox))
            {
                Specify.That(cb.Exists && cb.Enabled).Should.BeTrue();
                bool isCpgCheckBoxChecked = cb.Checked;

                Retry.For(cb.Click, TimeSpan.FromSeconds(6));
                Specify.That(cb.Checked).Should.BeEqualTo(!isCpgCheckBoxChecked);
            }
        }

        // assign values to form fields
        public string GetPageTitle()
        {
            return PageTitle.InnerText;
        }
        public void SetName(string name)
        {
            Specify.That(NameEdit.Enabled).Should.BeTrue("Name field is not available.");
            NameEdit.SetText(name);
        }
        public void SetDescription(string description)
        {
            DescriptionEdit.SafeSetText(description);
        }
        public void SetProductionModeMenu(SelectionAlgorithmTypes productionMode)
        {
            SetProductionModeMenu(productionMode.DisplayName);
        }
        public void SetProductionModeMenu(string productionMode)
        {
            ProductionModeMenu.SelectItem(TestDatabaseManagement.GetDropDownFriendlySelectionAlgorithmName(productionMode));
        }
        public void SetProductionModeMenuByIndex(int index)
        {
            ProductionModeMenu.SelectItem(index);
        }

        // crud
        public void Create(ProductionGroupExt pg, AdminConsole adminConsole)
        {
            adminConsole.NavigateToPage(NavPages.ProductionGroup);
            ClickNewButton();
            EnterInfo(pg);
            ClickSaveButton();
        }
        public void UpdateProductionGroup(int rowIndex, ProductionGroupExt pg, AdminConsole adminConsole, List<KeyValuePair<string, string>> extraValues = null)
        {
            adminConsole.NavigateToPage(NavPages.ProductionGroup);
            PageTableSelectRow(rowIndex);
            ClickEditButton();
            EnterInfo(pg, extraValues);
            ClickSaveButton();
        }
        public void EnterInfo(ProductionGroupExt pg, List<KeyValuePair<string, string>> extraValues = null)
        {
            SetName(pg.Alias);
            SetDescription(pg.Description);
            SetProductionModeMenu(pg.SelectionAlgorithm);

            if (extraValues != null)
            {
                foreach (var v in extraValues)
                {
                    var keys = v.Key.Split(':');
                    var type = keys[0];
                    var alias = keys[1];

                    if (type.Equals("MachineGroup"))
                        if (MachineGroupsDiv.InnerText.Contains(alias))
                            GetMachineGroupCheckBox(alias).SafeCheck(v.Value.ToLower() == "true");

                    if (type.Equals("Corrugate"))
                        if (CorrugatesDiv.InnerText.Contains(alias))
                            GetCorrugateCheckBox(alias).SafeCheck(v.Value.ToLower() == "true");
                }
            }

            if (pg.MachineGroups != null)
                foreach (var mg in pg.MachineGroups)
                    if (MachineGroupsDiv.InnerText.Contains(mg.Alias))
                        GetMachineGroupCheckBox(mg.Alias).SafeCheck();

            if (pg.Corrugates != null)
                foreach (var c in pg.Corrugates)
                    if (CorrugatesDiv.InnerText.Contains(c.Alias))
                        GetCorrugateCheckBox(c.Alias).SafeCheck();

            if (pg.SelectionAlgorithm == SelectionAlgorithmTypes.BoxFirst)
            {
                if (pg.CPGs != null)
                {
                    foreach (var cpg in pg.CPGs)
                        GetCPGCheckBox(cpg.Alias).SafeCheck();
                }
            }

            if (pg.SelectionAlgorithm == SelectionAlgorithmTypes.BoxLast)
                ShowTriggerInterfaceCheckBox.SafeCheck();

            if (pg.SelectionAlgorithm == SelectionAlgorithmTypes.ScanToCreate)
                STCShowTriggerInterfaceCheckbox.SafeCheck();

            if (BarcodeParsingWorkflowComboBox.Exists && BarcodeParsingWorkflowComboBox.Enabled)
            {
                Specify.That(pg.Workflow).Should.Not.BeNull("Workflow variable for the production group can't be null");
                BarcodeParsingWorkflowComboBox.SafeSelectItem(pg.Workflow);
            }
        }
        public void AddCorrugatesToExistingProductionGroup(ProductionGroupExt pg, List<string> corrugates, AdminConsole adminConsole = null)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);
            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            ProductionGroupTable.SelectRowWithValue(pg.Alias, 0);
            ClickEditButton();

            foreach (var corrugate in corrugates)
                GetCorrugateCheckBox(corrugate).SafeCheck();

            ClickSaveButton();
        }

        // verify
        public void VerifyCreatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyCreatedSuccessfulMessage(this.ProductionGroupAlert, "Production Group", alias, expectedText);
        }
        public void VerifyTableOneCorrugateAndMachineGroup(int rowIndex, ProductionGroupExt updatedPg)
        {
            var data = ProductionGroupTable;
            Specify.That(data.GetCellValue(rowIndex, 0)).Should.BeEqualTo(updatedPg.Alias, "PG Name is not correct");
            Specify.That(data.GetCellValue(rowIndex, 1)).Should.BeEqualTo(updatedPg.Description, "PG Description is not correct");
            Specify.That(data.GetCellValue(rowIndex, 2)).Should.BeEqualTo(TestDatabaseManagement.GetDropDownFriendlySelectionAlgorithmName(updatedPg.SelectionAlgorithm.DisplayName), "PG Production Mode is not correct");
            Specify.That(data.GetCellValue(rowIndex, 3)).Should.BeEqualTo(updatedPg.Corrugates[0].Alias, "PG Corrugates are not correct");
            Specify.That(data.GetCellValue(rowIndex, 4)).Should.BeEqualTo(updatedPg.MachineGroups[0].Alias, "PG Machines are not correct");
        }
        public void VerifyNumberOfRecordsIsAsExpected(int cnt)
        {
            int currentRecCount = ProductionGroupTable.RowCount(true);
            Specify.That(currentRecCount == cnt).Should.BeTrue(string.Format("Current number of rows is {0}. The correct would have been {1}.", currentRecCount, cnt));
        }
        public void VerifyNameValidationIsDisplayed(bool shouldBeDisplayed = true, string expectedText = null)
        {
            base.VerifyValidationIsDisplayed(NameValidation, "Name", shouldBeDisplayed, expectedText);
        }
        public void VerifyNameValidationExists(bool shouldExist = true)
        {
            base.VerifyValidationExists(ProductionGroupPage.NameValidationIdProperty, "Name", shouldExist);
        }
        public void VerifyNameLabel(string value)
        {
            base.VerifyNameLabel(NameLabel, value);
        }
        public void VerifyDuplicateAliasMessage(string alias)
        {
            base.VerifyDuplicateAliasMessage(this.newProductionGroupAlert, "Production Group", alias);
        }
        public void VerifyEditButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.EditButton, "edit", enabled);
        }
        public void VerifyEditPageIsOpen(bool shouldBeOpen = true)
        {
            base.VerifyEditPageIsOpen(CancelButton, NewButton, shouldBeOpen);
        }
        public void VerifyProductionModeValidationIsDisplayed(bool shouldExist = true, string expectedText = null)
        {
            base.VerifyValidationIsDisplayed(ProductionModeValidation, "Production Mode", shouldExist, expectedText);
        }
        public void VerifyProductionModeValidationExists(bool shouldExist = true)
        {
            base.VerifyValidationExists(ProductionGroupPage.ProductionModeValidationIdProperty, "Production Mode", shouldExist);
        }
        public void VerifyDeleteSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyDeleteSuccessfulMessage(this.ProductionGroupAlert, "Production Group", alias, expectedText);
        }
        public void VerifyUpdatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyUpdatedSuccessfulMessage(this.ProductionGroupAlert, "Production Group", alias, expectedText);
        }
        public void VerifyPageTitle(string value)
        {
            base.VerifyPageTitle(GetPageTitle(), value);
        }
    }
}
