﻿using System;

using CUITe.Controls.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class AboutPage : WindowBase
    { 
        // page controls
        public CUITe_HtmlHeading4 PacksizeVersion { get { return Get<CUITe_HtmlHeading4>(); } }
        public CUITe_HtmlButton ReleaseNotesButton { get { return Get<CUITe_HtmlButton>("id=ViewReleaseNotes"); } }
        public CUITe_HtmlButton EulaButton { get { return Get<CUITe_HtmlButton>("id=ViewEULA"); } }
        public CUITe_HtmlButton SupportInfoButton { get { return Get<CUITe_HtmlButton>("id=ViewSupportInfo"); } }
        public CUITe_HtmlIFrame IFrame { get { return Get<CUITe_HtmlIFrame>(); } }
        public CUITe_HtmlDocument IFrameContents { get { return IFrame.Get<CUITe_HtmlDocument>(); } }
    }

    public class AlertWidget : CUITe_HtmlDiv
    {
        public AlertWidget(string id)
            : base(id)
        {
        }

        // header and widget
        public CUITe_HtmlDiv AlertDiv { get { return this; } }

        // page controls
        public CUITe_HtmlButton CloseButton { get { return Get<CUITe_HtmlButton>(); } }
        public string AlertText { get { return Get<CUITe_HtmlDiv>().Get<CUITe_HtmlSpan>().InnerText; } }
        public bool HasSuccess { get { return this.ContainsCSS("alert-success"); } }
        public bool HasInfo { get { return this.ContainsCSS("alert-info"); } }
        public bool HasWarning { get { return this.ContainsCSS("alert-warn"); } }
        public bool HasDanger { get { return this.ContainsCSS("alert-danger"); } }
        public bool HasError { get { return HasDanger; } }

        /// <summary>
        /// Exists in the framework is throwing a stale reference exception... It appears that this really just means the div no longer exists
        /// </summary>
        public new bool Exists
        {
            get
            {
                try
                {
                   return AlertDiv.Exists;
                }
                catch (Exception e) //TODO: Trying to catch StaleElementReferenceException
                {
                    if (e.ToString().Contains("StaleElementReferenceException"))
                        return false;
                    throw;
                }
            }
        }
    }

    public enum AlertStates
    {
        NotSet,
        Error,
        Success,
        Warn,
        Danger
    }
}
