﻿using CUITe.Controls.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class AdminProduceFromArticlePage : WindowBase
    {
        Table _articleTable = null;
        public Table ArticlesTable
        {
            get
            {
                var div = Get<CUITe_HtmlDiv>("id=articlesList");
                return _articleTable ?? (_articleTable = new Table(div, new TableConfigData(TableTypes.TwoColumn, "ArticlesTable")));
            }
        }

        private AlertWidget AlertWidget { get { return Get<AlertWidget>("id=changeProductionGroupAlerts"); } }
        private CUITe_HtmlButton DeleteButton { get { return Get<CUITe_HtmlButton>("id=removeArticles"); } }
        private CUITe_HtmlButton ProduceButton { get { return Get<CUITe_HtmlButton>("id=produceArticles"); } }

        public void ClickDeleteButton()
        {
            DeleteButton.Click();
        }
        public void ClickProduceButton()
        {
            ProduceButton.Click();
        }

        public void SelectProductionGroup(int rowIndex)
        {

        }
        public void SetQuantity(int rowIndex)
        {

        }
    }
}
