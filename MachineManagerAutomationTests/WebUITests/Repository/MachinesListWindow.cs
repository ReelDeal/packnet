﻿//using System;
//using CUITe.Controls.HtmlControls;

//namespace MachineManagerAutomationTests.WebUITests.Repository
//{
//    /// <summary>
//    /// the current actual html file is index
//    /// </summary>
//    public class MachinesListWindow : DynamicBrowserWindowWithLogin
//    {
//        #region Constructors
//        public MachinesListWindow()
//        {
//            sWindowTitle = "Packnet.Server";
//            SearchProperties[PropertyNames.Name] = sWindowTitle;
//        }
//        #endregion

//        public CUITe_HtmlImage PacksizeLogo { get { return Get<CUITe_HtmlImage>("id=aboutPacknetServer"); } }

//        public CUITe_HtmlButton AdminConsoleButton { get { return Get<CUITe_HtmlButton>("id=adminConsoleButton"); } }
//        public CUITe_HtmlButton GetButtonForMachineId(Guid machineId)
//        {
//            return Get<CUITe_HtmlButton>(String.Format("id=machine{0}Button", machineId));
//        }
//        public CUITe_HtmlButton GetButtonForMachineId(int machineId)
//        {
//            return Get<CUITe_HtmlButton>(String.Format("id=machine{0}Button", machineId));
//        }
//        //sidebar footer links
//        public CUITe_HtmlDiv SideBarFooterMenu { get { return Get<CUITe_HtmlDiv>("class=sidebar-footer"); } }
//        public CUITe_HtmlHyperlink HelpLink { get { return SideBarFooterMenu.Get<CUITe_HtmlHyperlink>("TagName=div;TagInstance=1"); } }
//        public CUITe_HtmlHyperlink AboutLink { get { return SideBarFooterMenu.Get<CUITe_HtmlHyperlink>("TagName=div;TagInstance=2"); } }
//        public CUITe_HtmlHyperlink SupportLink { get { return SideBarFooterMenu.Get<CUITe_HtmlHyperlink>("TagName=div;TagInstance=3"); } }
//    }
//}
