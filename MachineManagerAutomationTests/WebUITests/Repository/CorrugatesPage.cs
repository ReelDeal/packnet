﻿using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.Macros;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Utils;
using PackNet.Data.Corrugates;
using System;
using System.Collections.Generic;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class CorrugatesPage : CrudPageBase
    {
        // header and widget
        private CUITe_HtmlHeading4 PageTitle { get { return Get<CUITe_HtmlHeading4>("id=corrugatesTitleHeader"); } }
        private AlertWidget AlertWidget { get { return Get<AlertWidget>("id=CorrugateAlert"); } }
        private AlertWidget DuplicateAlertWidget { get { return Get<AlertWidget>("id=newCorrugateAlert"); } }

        // list
        private CUITe_HtmlButton NewButton { get { return Get<CUITe_HtmlButton>("id=newCorrugate"); } }
        private CUITe_HtmlButton DeleteButton { get { return Get<CUITe_HtmlButton>("id=deleteCorrugate"); } }
        private CUITe_HtmlButton EditButton { get { return Get<CUITe_HtmlButton>("id=editCorrugate"); } }
        private Table _corrugatesTable = null;
        public Table CorrugatesTable
        {
            get {
                var div = Get<CUITe_HtmlDiv>("id=corrugateListTable");
                return _corrugatesTable ?? (_corrugatesTable = new Table(div, new TableConfigData(TableTypes.TwoColumn, "Corrugate"))); 
            }
        }

        // form
        private CUITe_HtmlLabel NameLabel { get { return Get<CUITe_HtmlLabel>("id=newCorrugateCorrugateNameLabel"); } }
        private CUITe_HtmlEdit NameEdit { get { return Get<CUITe_HtmlEdit>("id=newCorrugateCorrugateName"); } }
        private CUITe_HtmlParagraph NameValidation { get { return Get<CUITe_HtmlParagraph>("id=newCorrugateCorrugateNameValidation"); } }
        private CUITe_HtmlEdit WidthEdit { get { return Get<CUITe_HtmlEdit>("id=newCorrugateWidth"); } }
        private CUITe_HtmlEdit ThicknessEdit { get { return Get<CUITe_HtmlEdit>("id=newCorrugateThickness"); } }
        private CUITe_HtmlEdit QualityEdit { get { return Get<CUITe_HtmlEdit>("id=newCorrugateQualityNUD"); } }
        private CUITe_HtmlButton QualityUpButton { get { return Get<CUITe_HtmlButton>("id=nudIncrementButton"); } }
        private CUITe_HtmlButton QualityDownButton { get { return Get<CUITe_HtmlButton>("id=nudDecrementButton"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newCorrugateSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newCorrugateCancelButton"); } }

        // methods
        public static CorrugatesPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<CorrugatesPage>();
        }
        public void RefreshTables()
        {
            _corrugatesTable = null;
        }
        public void PageTableSelectRow(int rowIndex)
        {
            CorrugatesTable.SelectRow(rowIndex);
        }

        // button actions
        public void ClickSaveButton()
        {
            SaveButton.SafeClick();
        }
        public void ClickCancelButton(bool doubleClick = false)
        {
            CancelButton.SafeClick();
            if(doubleClick)
                CancelButton.SafeClick();
        }
        public void ClickNewButton()
        {
            NewButton.SafeClick();
        }
        public void ClickEditButton()
        {
            VerifyEditButtonIsEnabled();
            EditButton.SafeClick();
        }
        public void ClickDeleteButton()
        {
            Specify.That(DeleteButton.Enabled).Should.BeTrue();
            DeleteButton.SafeClick();
        }

        // assign values to form fields
        public void SetName(string alias)
        {
            NameEdit.SafeSetText(alias);
        }
        public void SetWidth(MicroMeter width)
        {
            WidthEdit.SafeSetText(width.ToString());
        }
        public void SetThickness(MicroMeter thickness)
        {
            ThicknessEdit.SafeSetText(thickness);
        }
        public void SetQuality(int quality)
        {
            QualityEdit.SafeSetText(quality);
        }

        // crud
        public void Create(List<Corrugate> corrugates, AdminConsole adminConsole, bool useValidation = false)
        {
            foreach (var c in corrugates)
                Create(c, adminConsole, useValidation);
        }
        public void Create(Corrugate corrugate, AdminConsole adminConsole, bool useValidation = false)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            // select corrugates in the menu
            adminConsole.NavigateToPage(NavPages.Corrugate);

            // opens a window to create a new corrugate
            ClickNewButton();

            // enter corrugate info and save
            EnterInfo(corrugate);

            // save
            ClickSaveButton();

            if (useValidation)
            {
                // verify widget message
                VerifyCreatedSuccessfulMessage(corrugate.Alias);

                //verify the dialog closed
                VerifyEditPageIsOpen(false);
            }
        }
        public void UpdateInfo(int rowIndex, Corrugate corr)
        {
            PageTableSelectRow(rowIndex);
            ClickEditButton();
            EnterInfo(corr);
            ClickSaveButton();
            VerifyUpdatedSuccessfulMessage(corr.Alias);
        }
        public void EnterInfo(Corrugate corrugate)
        {
            VerifyEditPageIsOpen();
            SetName(corrugate.Alias);
            SetWidth(corrugate.Width);
            SetThickness(corrugate.Thickness);
            SetQuality(corrugate.Quality);
        }

        #region Page Verification
        public void VerifyEditButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(EditButton, "edit", enabled);
        }
        public void VerifyTableRowContainsSpecifiedData(int row, string alias)
        {
            base.VerifyTableRowContainsSpecifiedData(row, CorrugatesTable, alias);
        }
        public void VerifyCorrugate(Corrugate corrugate, AdminConsole adminConsole = null)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            adminConsole.NavigateToPage(NavPages.Corrugate);

            var tbl = CorrugatesTable;

            Specify.That(tbl.RowCount()).Should.BeEqualTo(1, "We are assuming a clean environment but multiple rows wer found");

            //verify the table
            Specify.That(tbl.GetCellValue(0, 0)) //Name is first column
                .Should.BeEqualTo(corrugate.Alias, "Corrugate's Name is not correct");
            Specify.That(tbl.GetCellValue(0, 1)) //Width is second column
                .Should.BeEqualTo(corrugate.Width.ToString(), "Corrugate's Width is not correct");
            Specify.That(tbl.GetCellValue(0, 2)) //Thickness is third column
                .Should.BeEqualTo(corrugate.Thickness.ToString(), "Corrugate's Thickness is not correct");
            Specify.That(tbl.GetCellValue(0, 3)) //Quality is forth column
                .Should.BeEqualTo(corrugate.Quality.ToString(), "Corrugate's quality is not correct");

            //verify Corrugate in mongo
            var repo = new CorrugateRepository();
            var dbRecord = CorrugateMacros.FindByAlias(corrugate.Alias);

            Specify.That(dbRecord.Alias)
                .Should.BeEqualTo(corrugate.Alias, "Corrugate's Name is not correct in DB");
            Specify.That(dbRecord.Width)
                .Should.BeEqualTo(corrugate.Width, "Corrugate's Width is not correct in DB");
            Specify.That(dbRecord.Thickness)
                .Should.BeEqualTo(corrugate.Thickness, "Corrugate's Thickness is not correct in DB");
            Specify.That(dbRecord.Quality)
                .Should.BeEqualTo(corrugate.Quality, "Corrugate's quality is not correct in DB");
        }
        public void VerifyEditPageIsOpen(bool shouldBeOpen = true)
        {
            base.VerifyEditPageIsOpen(CancelButton, NewButton, shouldBeOpen);
        }
        public void VerifyDeleteButtonIsEnabled(bool enabled = true)
        {
            if (enabled)
                Specify.That(this.DeleteButton.Exists && this.DeleteButton.Enabled).Should.BeTrue("The delete button should be enabled.");
            else
                Specify.That(this.DeleteButton.Exists && !this.DeleteButton.Enabled).Should.BeTrue("The delete button should be disabled");
        }
        public void VerifyDeleteSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyDeleteSuccessfulMessage(this.AlertWidget, "Corrugate", alias, expectedText);
        }
        public void VerifyUpdatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyUpdatedSuccessfulMessage(this.AlertWidget, "Corrugate", alias, expectedText);
        }
        public void VerifyCreatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyCreatedSuccessfulMessage(this.AlertWidget, "Corrugate", alias, expectedText);
        }
        public void VerifyTableIsEmpty(int remainingNumberOfRecords = 0)
        {
            base.VerifyTableIsEmpty(CorrugatesTable, "Corrugate", remainingNumberOfRecords);
        }
        public void VerifyDuplicateAliasMessage(string alias)
        {
            base.VerifyDuplicateAliasMessage(DuplicateAlertWidget, "Corrugate", alias);
        }
        #endregion
    }
}
