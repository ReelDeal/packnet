﻿//using System;
//using CUITe.Controls.HtmlControls;

//namespace MachineManagerAutomationTests.WebUITests.Repository
//{
//    using PackNet.Common.Utils;

//    public class OperatorConsoleArticleDialog : DynamicBrowserWindowWithLogin
//    {
//        #region Constructors
//        public OperatorConsoleArticleDialog()
//            : base("Packnet.Server")
//        { }

//        public OperatorConsoleArticleDialog(string title):base(title)
//        { }
//        #endregion

//        public CUITe_HtmlHeading4 PageHeading { get { return Get<CUITe_HtmlHeading4>("id=CreateFromArticleHeader"); } }

//        //modal items
//        public CUITe_HtmlDiv Dialog { get { return Get<CUITe_HtmlDiv>("id=operatorConsoleCreateFromArticleDialog"); } }
//        public CUITe_HtmlButton CreateButton { get { return Get<CUITe_HtmlButton>("id=btnOperatorProduceMultiple"); } }
//        public CUITe_HtmlButton CloseButton { get { return Get<CUITe_HtmlButton>("id=btnOperatorProduceMultipleCloseButton"); } }
//        public AlertWidget Alert { get { return Get<AlertWidget>("id=createFromArticleAlerts"); } }

//        //table items
//        public CUITe_HtmlDiv CreateFromArticleTableDiv { get { return Get<CUITe_HtmlDiv>("id=articlesTable"); } }
//        public Table ArticleTable { get { return new Table(CreateFromArticleTableDiv, new TableConfigData(TableTypes.TwoColumn, "Article")); } }
//        public CUITe_HtmlButton IncrementButton(int row) { return ArticleTable.FindRow(row).Get<CUITe_HtmlButton>("id=nudIncrementButton"); }
//        public CUITe_HtmlEdit InputBox(int row) { return ArticleTable.FindRow(row).Get<CUITe_HtmlEdit>("id=nudInputText"); }

//        public void IncrementQuantityForArticle( int numberOfRows, string articleId)
//        {
//            for (int i = 0; i < numberOfRows; i++)
//            {
//                if (ArticleTable.FindCell(i, 1).InnerText.Trim().Equals(articleId.Trim()))
//                {
//                    IncrementButton(i).Click();
//                }
//            }
//        }

//        /// <summary>
//        /// Helper function that will wait for the Article button on the OP to become available and click it to open the Dialog returning a object representation of the dialog.
//        /// </summary>
//        /// <param name="operatorConsole"></param>
//        /// <returns></returns>
//        public static OperatorConsoleArticleDialog OpenArticlesDialog(OperatorConsole operatorConsole)
//        {
//            Retry.For(() => operatorConsole.ArticleLink.Exists && operatorConsole.ArticleLink.Enabled, TimeSpan.FromSeconds(5));

//            operatorConsole.ArticleLink.Click();


//            var createFromArticlesDialog = GetBrowserWindow < OperatorConsoleArticleDialog>(operatorConsole.Title);
//            //Specify.That(createFromArticlesDialog.Dialog.Enabled).Should.BeTrue("Article dialog is not enabled.");

//            return createFromArticlesDialog;
//        }
//    }
//}
