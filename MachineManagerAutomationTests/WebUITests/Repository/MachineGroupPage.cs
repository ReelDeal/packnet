﻿using System;
using System.Linq;
using System.Threading;

using Testing.Specificity;

using Microsoft.VisualStudio.TestTools.UITesting;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.Macros;
using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.WebUITests.Repository;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class MachineGroupPage : CrudPageBase, ICrudLocalizationPage
    {
        private static string NameValidationIdProperty = "newMachineGroupAliasValidation";
        private Table _machineGroupTable = null;

        // page heading and alert
        private CUITe_HtmlHeading4 PageHeading { get { return Get<CUITe_HtmlHeading4>("id=machineGroupPageTitleHeader"); } }
        private AlertWidget MachineGroupAlert { get { return Get<AlertWidget>("id=MachineGroupAlert"); } }
        private AlertWidget DuplicateMachineGroupAlert { get { return Get<AlertWidget>("id=newMachineGroupAlert"); } }

        // list
        private CUITe_HtmlButton NewButton { get { return Get<CUITe_HtmlButton>("id=newMachineGroup"); } }
        private CUITe_HtmlButton DeleteButton { get { return Get<CUITe_HtmlButton>("id=deleteMachineGroup"); } }
        private CUITe_HtmlButton EditButton { get { return Get<CUITe_HtmlButton>("id=editMachineGroup"); } }
        public Table MachineGroupTable
        {
            get
            {
                var div = Get<CUITe_HtmlDiv>("id=machineGroupListTable");
                return _machineGroupTable ?? (_machineGroupTable = new Table(div,new TableConfigData(TableTypes.TwoColumn, "MachineGroup")));
            }
        }

        //edit screen
        private CUITe_HtmlLabel NameLabel { get { return Get<CUITe_HtmlLabel>("id=newMachineGroupAliasLabel"); } }
        private CUITe_HtmlEdit NameEdit { get { return Get<CUITe_HtmlEdit>("id=newMachineGroupAlias"); } }
        private CUITe_HtmlParagraph NameValidation { get { return Get<CUITe_HtmlParagraph>("id=newMachineGroupAliasValidation"); } }
        private CUITe_HtmlTextArea DescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=newMachineDescription"); } }
        private CUITe_HtmlComboBox WorkflowPathMenu { get { return Get<CUITe_HtmlComboBox>("id=newMachineGroupWorkflowPath"); } }
        private CUITe_HtmlEdit QueueLengthEdit { get { return Get<CUITe_HtmlEdit>("id=newMachineGroupQueueLengthNUD"); } }
        private CUITe_HtmlButton QueueLengthUpButton { get { return Get<CUITe_HtmlButton>("id=nudIncrementButton"); } }
        private CUITe_HtmlButton QueueLengthDownButton { get { return Get<CUITe_HtmlButton>("id=nudDecrementButton"); } }
        private CUITe_HtmlDiv MachinesListDiv { get { return Get<CUITe_HtmlDiv>("id=newMachineGroupMachinesList"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newMachineGroupSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newMachineGroupCancelButton"); } }

        // methods
        private CUITe_HtmlCheckBox GetMachineCheckBox(string id)
        {
            Retry.For(() => MachinesListDiv.Exists, TimeSpan.FromSeconds(60));
            return MachinesListDiv.Get<CUITe_HtmlCheckBox>("id=" + id);
        }
        public static MachineGroupPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<MachineGroupPage>();
        }
        public void RefreshTables()
        {
            _machineGroupTable = null;
        }
        public void PageTableSelectRow(int rowIndex)
        {
            MachineGroupTable.SelectRow(rowIndex);
        }

        // button actions
        public void ClickSaveButton()
        {
            SaveButton.SafeClick();
        }
        public void ClickCancelButton(bool doubleClick = false)
        {
            CancelButton.SafeClick();
            if (doubleClick)
                CancelButton.SafeClick();
        }
        public void ClickNewButton()
        {
            NewButton.SafeClick();
        }
        public void ClickEditButton()
        {
            VerifyEditButtonIsEnabled();
            EditButton.SafeClick();
        }
        public void ClickDeleteButton()
        {
            DeleteButton.SafeClick();
        }
        public void ClickQueueLengthUpButton()
        {
            QueueLengthUpButton.SafeClick();
        }
        public void ClickQueueLengthDownButton()
        {
            QueueLengthDownButton.SafeClick();
        }

        // assign values to form fields
        public void SetName(string name)
        {
            NameEdit.SetText(name);
        }
        public void SetDescription(string description)
        {
            DescriptionEdit.SetText(description);
        }
        public int GetQueueLength()
        {
            return int.Parse(QueueLengthEdit.GetText());
        }
        public void SetQueueLength(int queueLength)
        {
            QueueLengthEdit.SetText(queueLength.ToString());
        }
        public void SetQueueLengthWithUpDownButtons(int queueLength)
        {
            TestUtilities.SetUpDownValue(QueueLengthEdit, QueueLengthUpButton, QueueLengthDownButton, queueLength);
        }
        public void SetWorkflow(string workflow)
        {
            WorkflowPathMenu.SelectItem(workflow);
        }
        public void SetMachine(string alias, bool check = true)
        {
            if (!string.IsNullOrWhiteSpace(alias))
            {
                if (MachinesListDiv.InnerText.Contains(alias))
                {
                    var chk = GetMachineCheckBox(alias);
                    if (check)
                        chk.Check();
                    else
                        chk.UnCheck();
                }
            }
        }

        // crud
        public void Create(MachineGroupExt mg, AdminConsole adminConsole = null, bool checkMachinesAreCreated = false, bool useValidation = true)
        {
            // check that all machines that is supposed to be in the machine group are created
            if(checkMachinesAreCreated)
            { 
                var allExists = MachineMacros.CheckIfMachinesExistInDatabase(mg.Machines);
                Specify.That(allExists).Should.BeTrue(string.Format("Some machines of those in the machine group '{0}' are not created.", mg.Alias));
            }

            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            adminConsole.NavigateToPage(NavPages.MachineGroup);

            ClickNewButton();

            EnterInfo(mg);

            ClickSaveButton();

            if (useValidation)
            {
                VerifyCreatedSuccessfulMessage(mg.Alias);
            }
        }
        public void UpdateInfo(int rowIndex, MachineGroupExt mg, bool save = true)
        {
            PageTableSelectRow(rowIndex);
            ClickEditButton();
            SetName(mg.Alias);
            SetDescription(mg.Description);
            SetWorkflow(mg.WorkflowPath);
            SetQueueLengthWithUpDownButtons(mg.MaxQueueLength);
            if(save)
            {
                ClickSaveButton();
                VerifyUpdatedSuccessfulMessage(mg.Alias);
                RefreshTables();
            }
        }

        public void UpdateWorkflow(MachineGroupExt mg, string workflow, AdminConsole adminConsole)
        {
            adminConsole.NavigateToPage(NavPages.MachineGroup);
            MachineGroupTable.SelectRowWithValue(mg.Alias);
            ClickEditButton();
            SetWorkflow(workflow);
            ClickSaveButton();
        }

        public void EnterInfo(MachineGroupExt mg)
        {
            VerifyEditPageIsOpen();
            SetName(mg.Alias);
            SetDescription(mg.Description);
            if (WorkflowPathMenu.ItemCount > 0)
            {
                SetWorkflow(mg.WorkflowPath);
            }

            SetQueueLength(mg.MaxQueueLength);

            foreach(var m in mg.Machines)
            {
                SetMachine(m.MachineAlias);
            }
        }

        // verify
        public void VerifyEditPageIsOpen(bool shouldBeOpen = true)
        {
            base.VerifyEditPageIsOpen(CancelButton, NewButton, shouldBeOpen);
        }
        public void VerifyNameValidationIsDisplayed(bool shouldExist = true, string expectedText = null)
        {
            base.VerifyValidationIsDisplayed(NameValidation, "Name", shouldExist);
        }
        public void VerifyNameValidationExists(bool shouldExist = true)
        {
            base.VerifyValidationExists(MachineGroupPage.NameValidationIdProperty, "Name", shouldExist);
        }
        public void VerifyDeleteButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.DeleteButton, "delete", enabled);
        }
        public void VerifyEditButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.EditButton, "edit", enabled);
        }
        public void VerifyUpdatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyUpdatedSuccessfulMessage(this.MachineGroupAlert, "Machine Group", alias, expectedText);
        }
        public void VerifyQueueLength(int queueLength)
        {
            Specify.That(queueLength == GetQueueLength()).Should.BeTrue(string.Format("The queue length is not correct. Actual: {0} Expected: {1}.", queueLength, GetQueueLength()));
        }
        public void VerifyDuplicateAliasMessage(string alias)
        {
            base.VerifyDuplicateAliasMessage(DuplicateMachineGroupAlert, "Machine Group", alias);
        }
        public void VerifyCreatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyCreatedSuccessfulMessage(this.MachineGroupAlert, "Machine Group", alias, expectedText);
        }
        public void VerifyDeleteSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyDeleteSuccessfulMessage(this.MachineGroupAlert, "Machine Group", alias, expectedText);
        }
        public void VerifyNameLabel(string value)
        {
            base.VerifyNameLabel(NameLabel, value);
        }
    }
}
