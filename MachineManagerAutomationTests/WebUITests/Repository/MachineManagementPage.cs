﻿using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.Macros;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Utils;
using PackNet.Data.BarcodeScanner;
using PackNet.Data.Machines;
using PackNet.Data.PickArea;
using PackNet.Data.PrintMachines;
using System;
using System.Collections.Generic;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public static class MachinePageObjects
    {
        public static string EmMachine { get { return "Em"; }}
        public static string Fusion { get { return "Fusion"; } }
        public static string FootPedal { get { return "Foot Pedal"; } }
        public static string Conveyor { get { return "Conveyor"; } }
        public static string Scanner { get { return "Network Scanner"; } }
        public static string Printer { get { return "ZebraPrinter"; } }
    }

    public class MachineManagementPage : CrudPageBase
    {
        private Table _accessoriesTable = null;

        #region Page objects

        #region Page title and alerts
        public CUITe_HtmlHeading4 PageTitle { get { return Get<CUITe_HtmlHeading4>("id=machinesTitleHeader"); } }
        public AlertWidget MachineManagementFusionTopAlert { get { return Get<AlertWidget>("id=machinesManagementFusionTopAlert"); } }
        public AlertWidget MachineManagementEmTopAlert { get { return Get<AlertWidget>("id=machinesManagementEmTopAlert"); } }
        public AlertWidget MachineManagementZebraPrinterTopAlert { get { return Get<AlertWidget>("id=machinesManagementZebraPrinterTopAlert"); } }
        public AlertWidget MachineManagementTopScannerAlert { get { return Get<AlertWidget>("id=machinesManagementScannerTopAlert"); } }
        public AlertWidget MachineTopAlert { get { return Get<AlertWidget>("id=MachinesAlert"); } }
        #endregion

        #region Machine list
        private CUITe_HtmlButton NewMachineButton { get { return Get<CUITe_HtmlButton>("id=newMachineButton"); } }
        private CUITe_HtmlButton EditMachineButton { get { return Get<CUITe_HtmlButton>("id=editMachine"); } }
        private CUITe_HtmlButton DeleteMachineButton { get { return Get<CUITe_HtmlButton>("id=deleteMachines"); } }
        private CUITe_HtmlButton DeleteMachineConfirmButton { get { return Get<CUITe_HtmlButton>("id=deleteMachineButtonConfirm"); } }
        private CUITe_HtmlButton DeleteMachineCancelButton { get { return Get<CUITe_HtmlButton>("id=deleteMachineButtonCancel"); } }
        public Table MachinesTable
        {
            get {
                var div = Get<CUITe_HtmlDiv>("id=machinesListTable");
                return new Table(div, new TableConfigData(TableTypes.TwoColumn, "Machine"));
            }
        }

        // machine Type, opens when you click NewMachineButton
        private CUITe_HtmlHyperlink MachineTypeEmOption { get { return Get<CUITe_HtmlHyperlink>("id=machinesNewEm"); } }
        private CUITe_HtmlHyperlink MachineTypeFusionOption { get { return Get<CUITe_HtmlHyperlink>("id=machinesNewFusion"); } }
        private CUITe_HtmlHyperlink MachineTypePrinterOption { get { return Get<CUITe_HtmlHyperlink>("id=machinesNewPrinter"); } }
        private CUITe_HtmlHyperlink MachineTypeScannerOption { get { return Get<CUITe_HtmlHyperlink>("id=machinesNewScanner"); } }
        private CUITe_HtmlHyperlink MachineTypeExternalSystemOption { get { return Get<CUITe_HtmlHyperlink>("id=machinesNewExternalSystem"); } }
        #endregion

        #region Common controls
        private CUITe_HtmlLabel NameLabel { get { return Get<CUITe_HtmlLabel>("id=newMachineAliasLabel"); } }
        private CUITe_HtmlEdit NameEdit { get { return Get<CUITe_HtmlEdit>("id=newMachineAlias"); } }
        private CUITe_HtmlTextArea DescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=newMachineDescription"); } }
        private CUITe_HtmlEdit IPAddressEdit { get { return Get<CUITe_HtmlEdit>("id=newMachineIpOrDnsName"); } }
        private CUITe_HtmlEdit PortEdit { get { return Get<CUITe_HtmlEdit>("id=newMachinePortNUD"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newMachineSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newMachineCancelButton"); } }

        #endregion

        #region Machine
        private CUITe_HtmlComboBox MachinePhysicalSettingsMenu { get { return Get<CUITe_HtmlComboBox>("id=newMachinePhysicalSettings"); } }
        private CUITe_HtmlButton MachineDiscoveryButton { get { return Get<CUITe_HtmlButton>("id=discoverBrMachinesButton"); } }
        // em
        private CUITe_HtmlEdit NumberOfTracksEdit { get { return Get<CUITe_HtmlEdit>("id=newMachineTracksNUD"); } }
        // fusion
        private CUITe_HtmlLabel SingleTrackButton { get { return Get<CUITe_HtmlLabel>("id=singleTrackBtn"); } }
        private CUITe_HtmlLabel MultiTrackButton { get { return Get<CUITe_HtmlLabel>("id=multiTrackBtn"); } }
        #endregion

        #region Printer
        private CUITe_HtmlComboBox PrinterModel { get { return Get<CUITe_HtmlComboBox>("id=newMachineModel"); } }
        private CUITe_HtmlHyperlink ToggleAdvancedSettings { get { return Get<CUITe_HtmlHyperlink>("id=toggleAdvancedSettings"); } }
        private CUITe_HtmlCheckBox PriorityPrinterCheckbox { get { return Get<CUITe_HtmlCheckBox>("id=newMachinePriorityPrinter"); } }
        private CUITe_HtmlRadioButton PrinterModeList { get { return Get<CUITe_HtmlRadioButton>("id=printerMode"); } }
        private CUITe_HtmlEdit RetryCountEdit { get { return Get<CUITe_HtmlEdit>("id=retryCount"); } }
        private CUITe_HtmlEdit PollIntervalEdit { get { return Get<CUITe_HtmlEdit>("id=pollInterval"); } }
        private CUITe_HtmlEdit LabelSecondCheckWaitTimeEdit { get { return Get<CUITe_HtmlEdit>("id=labelSecondCheckWaitTime"); } }
        #endregion

        #region Scanner
        private CUITe_HtmlCheckBox AutoTabBetweenFields { get { return Get<CUITe_HtmlCheckBox>("id=newMachineAutoTabBetweenFields"); } }
        #endregion

        #region External system
        private CUITe_HtmlEdit ExternalSystemNameEdit { get { return Get<CUITe_HtmlEdit>("id=newExternalSystemAlias"); } }
        private CUITe_HtmlTextArea ExternalSystemDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=newExternalSystemDescription"); } }
        private CUITe_HtmlEdit ExternalSystemIPAddressEdit { get { return Get<CUITe_HtmlEdit>("id=newExternalSystemIPAddress"); } }
        private CUITe_HtmlEdit ExternalSystemPortEdit { get { return Get<CUITe_HtmlEdit>("id=newExternalSystemPortNUD"); } }
        private CUITe_HtmlComboBox ExternalSystemWorkflowMenu { get { return Get<CUITe_HtmlComboBox>("id=newExternalSystemWorkflowPath"); } }
        private CUITe_HtmlButton ExternalSystemCancelButton { get { return Get<CUITe_HtmlButton>("id=newExternalSystemCancelButton"); } }
        private CUITe_HtmlButton ExternalSystemSaveButton { get { return Get<CUITe_HtmlButton>("id=newExternalSystemSaveButton"); } }
        #endregion

        #region Validation
        private CUITe_HtmlParagraph NameValidation { get { return Get<CUITe_HtmlParagraph>("id=newMachineAliasErrorRequired"); } }
        private CUITe_HtmlParagraph PortValidation { get { return Get<CUITe_HtmlParagraph>("id=newMachinePortErrorRequired"); } }
        private CUITe_HtmlParagraph TrackNumberError { get { return Get<CUITe_HtmlParagraph>("id=newMachinesTracksErrorRequired"); } }
        private CUITe_HtmlParagraph IpOrDnsNamnValidation { get { return Get<CUITe_HtmlParagraph>("id=newMachineIpOrDnsNameErrorRequired"); } }
        private CUITe_HtmlParagraph PhysicalMachineSettingsValidation { get { return Get<CUITe_HtmlParagraph>("id=newMachinePhysicalSettingsErrorRequired"); } }
        private CUITe_HtmlParagraph ConveyorNoPickZonesConfiguredValidation { get { return Get<CUITe_HtmlParagraph>("id=newConveyorNoPickzonesConfigured"); } }
        private CUITe_HtmlParagraph ConveyorOneDirectionNeedsToBeEnabledValidation { get { return Get<CUITe_HtmlParagraph>("id=newConveyorOneDirectionNeedsToBeEnabled"); } }
        #endregion
        
        #region Accessories
        private AlertWidget AccessoriesAlerts { get { return Get<AlertWidget>("id=machinesFootPedalTopAlert"); } }
        private CUITe_HtmlButton NewAccessoryButton { get { return Get<CUITe_HtmlButton>("id=newAccessoryButton"); } }
        private CUITe_HtmlHyperlink NewFootPedalOption { get { return Get<CUITe_HtmlHyperlink>("id=FootPedalOption"); } }
        private CUITe_HtmlHyperlink NewConveyorOption { get { return Get<CUITe_HtmlHyperlink>("id=ConveyorOption"); } }
        private CUITe_HtmlButton EditAccessoryButton { get { return Get<CUITe_HtmlButton>("id=editAccessory"); } }
        private CUITe_HtmlButton DeleteAccessoryButton { get { return Get<CUITe_HtmlButton>("id=deleteMachines"); } }
        public Table AccessoriesTable
        {
            get { return _accessoriesTable ?? (_accessoriesTable = new Table(Get<CUITe_HtmlDiv>("id=AccessoriesGridDiv"), new TableConfigData(TableTypes.TwoColumn, "Accessories"))); }
        }
        #endregion

        #region Foot pedal
        private CUITe_HtmlEdit FootPedalNameEdit { get { return Get<CUITe_HtmlEdit>("id=newFootPedalName"); } }
        private CUITe_HtmlTextArea FootPedalDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=newFootPedalDescription"); } }
        private CUITe_HtmlComboBox FootPedalPurposeComboBox { get { return Get<CUITe_HtmlComboBox>("id=newFootPedalPurpose"); } }
        private CUITe_HtmlEdit FootPedalIoModuleEdit { get { return Get<CUITe_HtmlEdit>("id=newFootPedalIoModuleNUD"); } }
        private CUITe_HtmlEdit FootPedalPortEdit { get { return Get<CUITe_HtmlEdit>("id=newFootPedalPortNUD"); } }
        private CUITe_HtmlButton FootPedalSaveButton { get { return Get<CUITe_HtmlButton>("id=newFootPedalSaveButton"); } }
        private CUITe_HtmlButton FootPedalCancelButton { get { return Get<CUITe_HtmlButton>("id=newFootPedalCancelButton"); } }
        #endregion

        #region Conveyor
        private AlertWidget newConveyorAlert { get { return Get<AlertWidget>("id=machinesConveyorTopAlert"); } }
        private CUITe_HtmlEdit ConveyorNameEdit { get { return Get<CUITe_HtmlEdit>("id=newConveyorName"); } }
        private CUITe_HtmlTextArea ConveyorDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=newConveyorDescription"); } }
        private CUITe_HtmlComboBox ConveyorTypeDropDown { get { return Get<CUITe_HtmlComboBox>("id=conveyorTypeSelection"); } }
        private CUITe_HtmlEdit WasteConveyorModule { get { return Get<CUITe_HtmlEdit>("id=wasteConveyorModule"); } }
        private CUITe_HtmlEdit WasteConveyorPort { get { return Get<CUITe_HtmlEdit>("id=wasteConveyorPort"); } }
        private CUITe_HtmlButton CrossConveyorFirstPickzoneFeedEnabled { get { return Get<CUITe_HtmlButton>("id=crossConveyorFirstFeedEnabled"); } }
        private CUITe_HtmlButton CrossConveyorSecondPickzoneFeedEnabled { get { return Get<CUITe_HtmlButton>("id=crossConveyorSecondFeedEnabled"); } }
        private CUITe_HtmlEdit CrossConveyorFirstModule { get { return Get<CUITe_HtmlEdit>("id=crossConveyorFirstModule"); } }
        private CUITe_HtmlEdit CrossConveyorSecondModule { get { return Get<CUITe_HtmlEdit>("id=crossConveyorSecondModule"); } }
        private CUITe_HtmlEdit CrossConveyorFirstPort { get { return Get<CUITe_HtmlEdit>("id=crossConveyorFirstPort"); } }
        private CUITe_HtmlEdit CrossConveyorSecondPort { get { return Get<CUITe_HtmlEdit>("id=crossConveyorSecondPort"); } }
        private CUITe_HtmlComboBox CrossConveyorFirstPickZone { get { return Get<CUITe_HtmlComboBox>("id=crossConveyorFirstPickZoneSelection"); } }
        private CUITe_HtmlComboBox CrossConveyorSecondPickZone { get { return Get<CUITe_HtmlComboBox>("id=crossConveyorSecondPickZoneSelection"); } }
        private CUITe_HtmlButton ConveyorSaveButton { get { return Get<CUITe_HtmlButton>("id=newConveyorSaveButton"); } }
        private CUITe_HtmlButton ConveyorCancelButton { get { return Get<CUITe_HtmlButton>("id=newConveyorCancelButton"); } }
        #endregion

        #endregion

        #region Enter values into fields
        // machine
        public void SetMachineName(string name)
        {
            NameEdit.SafeSetText(name);
        }
        public void SetMachineDescription(string description)
        {
            DescriptionEdit.SafeSetText(description);
        }
        public void SetMachineIpAddress(string ipNumber)
        {
            IPAddressEdit.SafeSetText(ipNumber);
        }
        public void SetMachinePort(int port)
        {
            PortEdit.SafeSetText(port.ToString());
        }
        public void SetPhysicalMachineSettings(string value)
        {
            MachinePhysicalSettingsMenu.SelectItem(value);
        }
        public void SetNumberOfTracks(int numberOfTracks)
        {
            NumberOfTracksEdit.SafeSetText(numberOfTracks.ToString());
        }
        public void EnterMachineInfo(IPacksizeCutCreaseMachine m)
        {
            if (m.NumTracks == 0) m.NumTracks = 1;

            SetMachineName(m.Alias);
            SetMachineDescription(m.Description);
            SetMachineIpAddress(m.IpOrDnsName);
            SetMachinePort(m.Port);
            SetPhysicalMachineSettings(MachineMacros.TranslatePhysicalMachineSettingsFilePathToName(m.PhysicalSettingsFilePath));
            if(m is FusionMachine)
            {
                if (m.NumTracks == 1)
                    ClickFusionSingleTrackButton();
                if(m.NumTracks == 2)
                    ClickFusionMultiTrackButton();
            }
            else
            {
                SetNumberOfTracks(m.NumTracks);
            }
        }

        // printer
        public void SetPrinterModel(string model)
        {
            PrinterModel.SelectItem(model);
        }
        public void SetPrinterName(string name)
        {
            NameEdit.SafeSetText(name);
        }
        public void SetPrinterDescription(string description)
        {
            DescriptionEdit.SafeSetText(description);
        }
        public void SetPrinterIpAddress(string ipNumber)
        {
            IPAddressEdit.SafeSetText(ipNumber);
        }
        public void SetPrinterPort(int port)
        {
            PortEdit.SafeSetText(port.ToString());
        }
        public void SetPrinterMode(string port)
        {
            throw new NotImplementedException();
        }
        public void SetPriorityPrinter(bool check = true)
        {
            if(check)
                PriorityPrinterCheckbox.Check();
            else
                PriorityPrinterCheckbox.UnCheck();
        }
        public void SetPrinterRetryCount(int retryCount)
        {
            RetryCountEdit.SafeSetText(retryCount.ToString());
        }
        public void SetPrinterPollInterval(int pollIntervalMs)
        {
            PollIntervalEdit.SafeSetText(pollIntervalMs.ToString());
        }
        public void SetPrinterLabelSecondCheckWaitTime(int timeMs)
        {
            LabelSecondCheckWaitTimeEdit.SafeSetText(timeMs.ToString());
        }
        public void EnterPrinterInfo(ZebraPrinter printer)
        {
            SetPrinterModel("GX420t"); // TODO: what property is used?? Now it's hardcoded
            SetPrinterName(printer.Alias);
            SetPrinterDescription(printer.Description);
            SetPrinterIpAddress(printer.IpOrDnsName);
            SetPrinterPort(printer.Port);
            //SetPrinterMode(printer.PrinterMode); // TODO: what property is used?
            SetPriorityPrinter(printer.IsPriorityPrinter);
            ClickPrinterToggleShowAdvancedSettings();
            SetPrinterRetryCount(printer.RetryCount);
            SetPrinterPollInterval(printer.PollInterval);
            SetPrinterLabelSecondCheckWaitTime(printer.LabelSecondCheckWaitTime);
        }

        // scanner
        public void SetScannerName(string name)
        {
            NameEdit.SafeSetText(name);
        }
        public void SetScannerDescription(string description)
        {
            DescriptionEdit.SafeSetText(description);
        }
        public void SetScannerIpAddress(string ipAddress)
        {
            IPAddressEdit.SafeSetText(ipAddress);
        }
        public void SetScannerPort(int port)
        {
            PortEdit.SafeSetText(port.ToString());
        }
        public void SetScannerAutoTabBetweenFields(bool check = true)
        {
            if (check)
                AutoTabBetweenFields.Check();
            else
                AutoTabBetweenFields.UnCheck();
        }
        public void EnterScannerInfo(BarcodeScannerMachine scanner)
        {
            SetScannerName(scanner.Alias);
            SetScannerDescription(scanner.Description);
            SetScannerPort(scanner.Port);

            //ClickScannerToggleShowAdvancedSettings();
            //SetScannerAutoTabBetweenFields(false); // TODO: which property is used for autotabBetweenFields??
        }

        // external system
        public void SetExtSysName(string name)
        {
            ExternalSystemNameEdit.SafeSetText(name);
        }
        public void SetExtSysDescription(string description)
        {
            ExternalSystemDescriptionEdit.SafeSetText(description);
        }
        public void SetExtSysIpAddress(string ipAddress)
        {
            ExternalSystemIPAddressEdit.SafeSetText(ipAddress);
        }
        public void SetExtSysPort(string port)
        {
            ExternalSystemPortEdit.SafeSetText(port);
        }
        public void SetExtSysWorkflow(string workflow)
        {
            ExternalSystemWorkflowMenu.SelectItem(workflow);
        }

        // conveyor
        public void SetConveyorName(string name)
        {
            ConveyorNameEdit.SafeSetText(name);
        }
        public void SetConveyorDescription(string description)
        {
            ConveyorDescriptionEdit.SafeSetText(description);
        }
        public void SetConveyorType(ConveyorType type)
        {
            ConveyorTypeDropDown.SelectItem(MachineMacros.GetConveyorTypeFriendlyName(type));
        }
        public void SetWasteConveyorModule(int module)
        {
            WasteConveyorModule.SafeSetText(module);
        }
        public void SetWasteConveyorOutput(int port)
        {
            WasteConveyorPort.SafeSetText(port);
        }
        public void SelectCrossConveyorFeed(int pickzoneRow)
        {
            if (pickzoneRow == 1)
                CrossConveyorFirstPickzoneFeedEnabled.Click();
            else if (pickzoneRow == 2)
                CrossConveyorSecondPickzoneFeedEnabled.Click();
        }
        public void SetCrossConveyorModule(int pickzoneRow, int module)
        {
            CUITe_HtmlEdit moduleObj = null;
            if(pickzoneRow == 1)
                moduleObj = CrossConveyorFirstModule;
            else if (pickzoneRow == 2)
                moduleObj = CrossConveyorSecondModule;

            Specify.That(moduleObj.Exists && moduleObj.Enabled).Should.BeTrue(string.Format("The field 'Crossconveyor Module for row {0} does not exist", pickzoneRow));
            moduleObj.SafeSetText(module.ToString());

        }
        public string GetConveyorName()
        {
            Specify.That(ConveyorNameEdit.Exists && ConveyorNameEdit.Enabled).Should.BeTrue("The field Conveyor name does not exist or is not enabled.");
            return ConveyorNameEdit.GetText();
        }
        public void VerifyConveyorName(string name)
        {
            Specify.That(GetConveyorName()).Should.BeEqualTo(name);
        }
        public string GetConveyorDescription()
        {
            Specify.That(ConveyorDescriptionEdit.Exists && ConveyorDescriptionEdit.Enabled).Should.BeTrue("The field Conveyor description does not exist or is not enabled.");
            return ConveyorDescriptionEdit.Text;
        }
        public void VerifyConveyorDescription(string description)
        {
            Specify.That(GetConveyorDescription()).Should.BeEqualTo(description);
        }
        public void VerifyCrossConveyorModuleValue(int pickzoneRow, int module)
        {
            Specify.That(GetCrossConveyorModule(pickzoneRow)).Should.BeEqualTo(module);
        }
        public void VerifyCrossConveyorPortValue(int pickzoneRow, int port)
        {
            Specify.That(GetCrossConveyorPort(pickzoneRow)).Should.BeEqualTo(port);
        }
        public int GetCrossConveyorPort(int pickzoneRow)
        {
            CUITe_HtmlEdit portObj = null;
            if (pickzoneRow == 1)
                portObj = CrossConveyorFirstPort;
            else if (pickzoneRow == 2)
                portObj = CrossConveyorSecondPort;

            Specify.That(portObj.Exists && portObj.Enabled).Should.BeTrue(string.Format("The field 'Crossconveyor Port for row {0} does not exist", pickzoneRow));
            return int.Parse(portObj.GetText());
        }
        public int GetCrossConveyorModule(int pickzoneRow)
        {
            CUITe_HtmlEdit moduleObj = null;
            if (pickzoneRow == 1)
                moduleObj = CrossConveyorFirstModule;
            else if (pickzoneRow == 2)
                moduleObj = CrossConveyorSecondModule;

            Specify.That(moduleObj.Exists && moduleObj.Enabled).Should.BeTrue(string.Format("The field 'Crossconveyor Module for row {0} does not exist", pickzoneRow));
            return int.Parse(moduleObj.GetText());
        }
        public void SetCrossConveyorPort(int pickzoneRow, int port)
        {
            CUITe_HtmlEdit portObj = null;
            if (pickzoneRow == 1)
                portObj = CrossConveyorFirstPort;
            else if (pickzoneRow == 2)
                portObj = CrossConveyorSecondPort;

            Specify.That(portObj.Exists && portObj.Enabled).Should.BeTrue(string.Format("The field 'CrossConveyor Port for row {0} does not exist", pickzoneRow));
            portObj.SafeSetText(port.ToString());

        }
        public void SetCrossConveyorFeedsToPickZone(int pickzoneRow, PickZone pickzone)
        {
            CUITe_HtmlComboBox pickzoneObj = null;
            if(pickzoneRow == 1)
                pickzoneObj = CrossConveyorFirstPickZone;
            else if (pickzoneRow == 2)
                pickzoneObj= CrossConveyorSecondPickZone;

            Specify.That(pickzoneObj.Exists && pickzoneObj.Enabled).Should.BeTrue(string.Format("The field 'CrossConveyor PickZone for row {0} does not exist", pickzoneRow));
            pickzoneObj.SelectItem(pickzone.Alias);
        }
        public void EnterConveyorInfo(ConveyorExt c)
        {
            var pzRepo = new PickZoneRepository();

            SetConveyorName(c.Alias);
            SetConveyorDescription(c.Description);
            SetConveyorType(c.Type);
            if(c.Type == ConveyorType.WasteConveyor)
            {
                SetWasteConveyorModule(c.Module);
                SetWasteConveyorOutput(c.Port);
            }
            else
            {
                if(c.Module > 0)
                {
                    Specify.That(PickZoneMacros.FindByAlias(c.PickZone1.Alias)).Should.Not.BeNull(string.Format("The pickzone '{0}' has not been created. It can't be selected for the conveyor '{1}'.", c.PickZone1.Alias, c.Alias));

                    SelectCrossConveyorFeed(1);
                    SetCrossConveyorModule(1, c.Module);
                    SetCrossConveyorPort(1, c.Port);
                    SetCrossConveyorFeedsToPickZone(1, c.PickZone1);
                }
                if (c.Module2 > 0)
                {
                    Specify.That(PickZoneMacros.FindByAlias(c.PickZone2.Alias)).Should.Not.BeNull(string.Format("The pickzone '{0}' has not been created. It can't be selected for the conveyor '{1}'.", c.PickZone2.Alias, c.Alias));

                    SelectCrossConveyorFeed(2);
                    SetCrossConveyorModule(2, c.Module2);
                    SetCrossConveyorPort(2, c.Port2);
                    SetCrossConveyorFeedsToPickZone(2, c.PickZone2);
                }
            }
        }

        // footpedal
        public void SetFootPedalName(string name)
        {
            FootPedalNameEdit.SafeSetText(name);
        }
        public void SetFootPedalDescription(string description)
        {
            FootPedalDescriptionEdit.SafeSetText(description);
        }
        public void SetFootPedalModule(int module)
        {
            FootPedalIoModuleEdit.SafeSetText(module.ToString());
        }
        public void SetFootPedalPort(int port)
        {
            FootPedalPortEdit.SafeSetText(port.ToString());
        }
        public void SetFootPedalPurpose(FootpedalPurpose purpose)
        {
            FootPedalPurposeComboBox.SelectItem(MachineMacros.GetFootPedalPurposeFriendlyName(purpose));
        }
        public void EnterFootPedalInfo(FootPedal pedal)
        {
            SetFootPedalName(pedal.Alias);
            SetFootPedalDescription(pedal.Description);
            SetFootPedalPurpose(pedal.Purpose);
            SetFootPedalModule(pedal.Module);
            SetFootPedalPort(pedal.Port);
        }

        #endregion

        #region Button actions

        // list
        public void ClickNewButton()
        {
            Specify.That(NewMachineButton.Enabled).Should.BeTrue();
            NewMachineButton.SafeClick();
        }
        public void SelectItemInNewMachineMenu(MachineType machineType)
        {
            if (machineType == MachineType.Fusion)
                MachineTypeFusionOption.Click();
            else if (machineType == MachineType.EM)
                MachineTypeEmOption.Click();
            else if (machineType == MachineType.Scanner)
                MachineTypeScannerOption.Click();
            else if (machineType == MachineType.ZebraPrinter)
                MachineTypePrinterOption.Click();
            else if (machineType == MachineType.ExternalSystem)
                MachineTypeExternalSystemOption.Click();
        }
        public void ClickEditButton()
        {
            VerifyEditButtonIsEnabled();
            EditMachineButton.SafeClick();
        }
        public void ClickDeleteButton(bool confirm = true)
        {
            DeleteMachineButton.SafeClick();
            if (confirm)
                DeleteMachineConfirmButton.Click();
            else
                DeleteMachineCancelButton.Click();
        }
        public void ClickDeleteCancelButton()
        {
            DeleteMachineCancelButton.SafeClick();
        }
        public void ClickDeleteConfirmButton()
        {
            DeleteMachineConfirmButton.SafeClick();
        }

        // machine
        public void ClickSaveButton()
        {
            SaveButton.SafeClick();
        }
        public void ClickCancelButton()
        {
            CancelButton.SafeClick();
        }

        
        public void ClickExternalSystemSaveButton()
        {
            ExternalSystemSaveButton.SafeClick();
        }
        public void ClickExternalSystemCancelButton()
        {
            ExternalSystemCancelButton.SafeClick();
        }
        public void ClickFusionSingleTrackButton()
        {
            SingleTrackButton.SafeClick();
        }
        public void ClickFusionMultiTrackButton()
        {
            MultiTrackButton.SafeClick();
        }
        public void ClickDiscoveryButton()
        {
            MachineDiscoveryButton.SafeClick();
        }

        // printer
        public void ClickPrinterToggleShowAdvancedSettings()
        {
            ToggleAdvancedSettings.Click();
        }

        // scanner
        public void ClickScannerToggleShowAdvancedSettings()
        {
            ToggleAdvancedSettings.Click();
        }

        // accessory
        private void ClickNewAccessoryButton()
        {
            Specify.That(NewAccessoryButton.Exists && NewAccessoryButton.Enabled).Should.BeTrue();
            NewAccessoryButton.SafeClick();
        }
        public void ClickNewFootpedalButton()
        {
            ClickNewAccessoryButton();
            NewFootPedalOption.SafeClick();
        }
        public void ClickNewConveyorButton()
        {
            ClickNewAccessoryButton();
            NewConveyorOption.SafeClick();
        }
        public void ClickEditAccessoryButton()
        {
            EditAccessoryButton.Click();
        }
        public void ClickDeleteAccessoryButton()
        {
            DeleteAccessoryButton.Click();
        }

        // footpedal
        public void ClickSaveFootPedalButton()
        {
            FootPedalSaveButton.SafeClick();
        }
        public void ClickCancelFootPedalButton()
        {
            FootPedalCancelButton.SafeClick();
        }

        // conveyor
        public void ClickSaveConveyorButton()
        {
            ConveyorSaveButton.SafeClick();
        }
        public void ClickCancelConveyorButton()
        {
            ConveyorCancelButton.SafeClick();
        }

        #endregion

        #region Misc methods
        public static MachineManagementPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<MachineManagementPage>();
        }
        public void RefreshTables()
        {
            _accessoriesTable = null;
        }
        public AlertWidget GetTopWidget(MachineType machineType)
        {
            return GetTopWidget(machineType.ToString());
        }
        public AlertWidget GetTopWidget(string objectName)
        {
            AlertWidget widget = null;
            if (objectName.Contains("Fusion"))
                widget = MachineManagementFusionTopAlert;
            else if (objectName.Contains("Printer"))
                widget = MachineManagementZebraPrinterTopAlert;
            else if (objectName.Contains("Scanner"))
                widget = MachineManagementTopScannerAlert;
            else if (objectName.Contains("EM"))
                widget = MachineManagementEmTopAlert;
            else if (objectName.Contains("Conveyor"))
                widget = newConveyorAlert;
            else
                widget = MachineTopAlert;

            return widget;
        }
        public void PageTableSelectRow(int rowIndex)
        {
            MachinesTable.SelectRow(rowIndex);
        }
        #endregion

        // assign values to form fields
        public void SetName(string name)
        {
            NameEdit.SetText(name);
        }
        public void SetDescription(string description)
        {
            DescriptionEdit.SetText(description);
        }
        public void SetIPAddress(string ipAddress)
        {
            if (IPAddressEdit.Exists)
                IPAddressEdit.SetText(ipAddress);
        }

        public void AddRequiredPickZonesForAccessories(List<IAccessory> acc, AdminConsole adminConsole)
        {
            List<PickZone> requiredPickZones = new List<PickZone>();
            foreach (var a in acc)
            {
                if (a is ConveyorExt)
                {
                    var c = (ConveyorExt)a;
                    if(c.Type == ConveyorType.CrossConveyor)
                    {
                        if(c.PickZone1 != null && c.Module > 0)
                            if(!requiredPickZones.Exists(z => z.Alias == c.PickZone1.Alias))
                                requiredPickZones.Add(((ConveyorExt)a).PickZone1);
                        if(c.PickZone2 != null && c.Module2 > 0)
                            if(!requiredPickZones.Exists(z => z.Alias == c.PickZone2.Alias))
                                requiredPickZones.Add(((ConveyorExt)a).PickZone2);
                    }
                }
            }

            PickZoneRepository repo = null;
            if (requiredPickZones.Count > 0)
                repo = new PickZoneRepository();
            foreach (var pz in requiredPickZones)
            {
                if(PickZoneMacros.FindByAlias(pz.Alias, repo)==null)
                PickZoneMacros.Create(pz, adminConsole);
            }
        }

        // crud
        public void Create(IMachine machine, AdminConsole adminConsole = null, bool includeValidation = false)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            adminConsole.NavigateToPage(NavPages.Machine);

            ClickNewButton();

            if (machine is IPacksizeCutCreaseMachine)
            {
                if(machine is EmMachine)
                    SelectItemInNewMachineMenu(MachineType.EM);
                else
                    SelectItemInNewMachineMenu(MachineType.Fusion);

                EnterMachineInfo((IPacksizeCutCreaseMachine)machine);
            }
            else if (machine is ZebraPrinter)
            {
                SelectItemInNewMachineMenu(MachineType.ZebraPrinter);
                EnterPrinterInfo((ZebraPrinter)machine);
            }
            else if (machine is BarcodeScannerMachine)
            {
                SelectItemInNewMachineMenu(MachineType.Scanner);
                EnterScannerInfo((BarcodeScannerMachine)machine);
            }

            ClickSaveButton();

            if (includeValidation)
            {
                VerifyCreatedSuccessfulMessage(machine.MachineAlias, MachineMacros.GetMachineTypeFriendlyName(MachineMacros.GetMachineType(machine)));
            }
        }
        public void CreateFootPedal(FootPedal fp)
        {
            ClickNewFootpedalButton();
            EnterFootPedalInfo(fp);
            ClickSaveFootPedalButton();
        }
        public void CreateConveyor(ConveyorExt conveyor)
        {
            ClickNewConveyorButton();
            EnterConveyorInfo(conveyor);
            ClickSaveConveyorButton();
        }
        public void UpdateMachineInfo(int rowIndex, IMachine machine, AdminConsole adminConsole = null)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            adminConsole.NavigateToPage(NavPages.Machine);

            PageTableSelectRow(rowIndex);

            ClickEditButton();

            if (machine is IPacksizeCutCreaseMachine)
            {
                EnterMachineInfo((IPacksizeCutCreaseMachine)machine);
            }
            else if (machine is ZebraPrinter)
            {
                EnterPrinterInfo((ZebraPrinter)machine);
            }
            else if (machine is BarcodeScannerMachine)
            {
                EnterScannerInfo((BarcodeScannerMachine)machine);
            }

            ClickSaveButton();
        }
        public void UpdateFootPedal(int rowIndex, FootPedal fp)
        {
            AccessoriesTable.SelectRow(rowIndex);
            ClickEditAccessoryButton();
            EnterFootPedalInfo(fp);
            ClickSaveFootPedalButton();
        }
        public void UpdateConveyor(int rowIndex, ConveyorExt conveyor)
        {
            AccessoriesTable.SelectRow(rowIndex);
            ClickEditAccessoryButton();
            EnterConveyorInfo(conveyor);
            ClickSaveConveyorButton();
        }
        public void CreateAccessoriesForMachine(List<IAccessory> acc)
        {
            foreach (var a in acc)
            {
                if (a != null)
                {
                    if (a is FootPedal)
                    {
                        CreateFootPedal((FootPedal)a);
                    }
                    else if (a is ConveyorExt)
                    {
                        CreateConveyor((ConveyorExt)a);
                    }
                }
            }
        }

        // verify
        public void VerifyEditButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.EditMachineButton, "edit", enabled);
        }
        public void VerifyDuplicateAliasMessage(string alias, string objectName)
        {
            if (objectName.Equals("Conveyor"))
            {
                TestUtilities.VerifyAlert(GetTopWidget(objectName), alias,
                    string.Format("Conveyor '{0}' already exists on this machine.", alias),
                    "Conveyor duplicate alias notification was not shown.", true);
            }
            else
            {
                base.VerifyDuplicateAliasMessage(GetTopWidget(objectName), objectName, alias);
            }
        }
        public void VerifyNameLabel(string value)
        {
            base.VerifyNameLabel(NameLabel, value);
        }
        public void VerifyMachineTableRowContainsSpecifiedData(int row, string alias, string type)
        {
            base.VerifyTableRowContainsSpecifiedData(row, MachinesTable, alias, type, null);
        }
        public void VerifyAccessoryTableRowContainsSpecifiedData(int row, string alias, string type)
        {
            if (type.Contains("Conveyor")) type = "Conveyor";
            base.VerifyTableRowContainsSpecifiedData(row, AccessoriesTable, alias, type, null);
        }
        public void VerifyDeleteSuccessfulMessage(string alias, string objectName)
        {
            base.VerifyDeleteSuccessfulMessage(this.MachineTopAlert, objectName, alias);
        }
        public void VerifyUpdatedSuccessfulMessage(string alias, string objectName)
        {
            base.VerifyUpdatedSuccessfulMessage(GetTopWidget(objectName), objectName, alias);
        }
        public void VerifyCreatedSuccessfulMessage(string alias, string objectName)
        {
            base.VerifyCreatedSuccessfulMessage(this.MachineTopAlert, objectName, alias);
        }
        public void VerifyDeleteButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.DeleteMachineButton, "delete", enabled);
        }
        public void VerifyTableIsEmpty(string objectName, int remainingNumberOfRecords = 0)
        {
            base.VerifyTableIsEmpty(MachinesTable, objectName, remainingNumberOfRecords);
        }
        public void VerifyAlertAndTableCorrectlyDisplayed(MachineType machineType, string alias, string descr, bool machineCreated)
        {
            //verify alert indicating machines has been created successfully 
            if (machineCreated)
                MachineTopAlert.VerifyAlert(true, AlertStates.Success, TestUtilities.GetMachineTypeFriendlyName(machineType) + " \'" + alias + "\' created successfully");
            else
                Specify.That(MachineTopAlert.AlertText)
                   .Should.BeEqualTo(String.Format(TestUtilities.GetMachineTypeFriendlyName(machineType) + " '{0}' updated successfully", alias), "Machine updated notification was not shown properly.");

            //checking 1 row added to the table
            var data = MachinesTable.GetData(true);

            Specify.That(data.Rows.Count).Should.BeEqualTo(1, "Row not added to the table");
            Specify.That(data.GetCellValue(0, 0)).Should.BeEqualTo(alias, "Machine Name does not match, expected: " + alias);
            if (descr != null)
            {
                Specify.That(data.GetCellValue(0, 2)).Should.BeEqualTo(descr, "Machine description does not match, expected: " + descr);
            }
        }

    }
}
