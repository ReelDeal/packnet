﻿using System;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Interfaces.DTO.Users;
using Testing.Specificity;
using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class UserManagementPage : WindowBase
    {
        #region Page controls

        // list
        private CUITe_HtmlButton NewUserButton { get { return Get<CUITe_HtmlButton>("id=newUser"); } }
        private CUITe_HtmlButton EditUserButton { get { return Get<CUITe_HtmlButton>("id=editUser"); } }
        private CUITe_HtmlButton DeleteUserButton { get { return Get<CUITe_HtmlButton>("id=deleteUser"); } }

        public AlertWidget Alerts { get { return Get<AlertWidget>("id=userListAlerts"); } }
        public CUITe_HtmlDiv UsersListDiv { get { return Get<CUITe_HtmlDiv>("id=userListTable"); } }
        public Table UsersTable { get { return new Table(UsersListDiv, new TableConfigData(TableTypes.TwoColumn, "User")); } }

        // form
        private CUITe_HtmlLabel UsernameLabel { get { return Get<CUITe_HtmlLabel>("id=newUserUserNameLabel"); } }
        private CUITe_HtmlEdit UsernameEdit { get { return Get<CUITe_HtmlEdit>("id=newUserUserName"); } }
        private CUITe_HtmlParagraph UsernameValidation { get { return Get<CUITe_HtmlParagraph>("id=newUserUserNameValidation"); } }
        private CUITe_HtmlEdit PasswordEdit { get { return Get<CUITe_HtmlEdit>("id=newUserPassword"); } }
        private CUITe_HtmlEdit PasswordConfirmEdit { get { return Get<CUITe_HtmlEdit>("id=newUserPasswordConfirm"); } }
        private CUITe_HtmlCheckBox AutoLoginCheckBox { get { return Get<CUITe_HtmlCheckBox>("id=newUserAutoLogin"); } }
        private CUITe_HtmlCheckBox AdministratorCheckBox { get { return Get<CUITe_HtmlCheckBox>("id=newUserAdmin"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newUserSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newUserCancelButton"); } }

        #endregion

        // list
        public void ClickNewButton()
        {
            NewUserButton.SafeClick();
        }
        public void ClickEditButton()
        {
            EditUserButton.SafeClick();
        }
        public void ClickDeleteButton()
        {
            DeleteUserButton.SafeClick();
        }

        // form
        public void SetUsername(string username)
        {
            UsernameEdit.SafeSetText(username);
        }
        public void SetPassword(string password)
        {
            PasswordEdit.SafeSetText(password);
        }
        public void SetConfirmPassword(string password)
        {
            PasswordConfirmEdit.SafeSetText(password);
        }
        public void CheckAutoLoginCheckbox(bool check = true)
        {
            if (check)
                AutoLoginCheckBox.Check();
            else
                AutoLoginCheckBox.UnCheck();
        }
        public void CheckAdministratorCheckbox(bool check = true)
        {
            if (check)
                AdministratorCheckBox.Check();
            else
                AdministratorCheckBox.UnCheck();
        }

        public void ClickSaveButton()
        {
            SaveButton.Click();
        }
        public void ClickCancelButton(bool doubleClick = false)
        {
            CancelButton.Click();
            if(doubleClick)
                CancelButton.Click();
        }

        // crud
        public void Create(User user, AdminConsole adminConsole = null)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            adminConsole.NavigateToPage(NavPages.User);

            ClickNewButton();
            EnterInfo(user);
            ClickSaveButton();
        }
        public void EnterInfo(User user)
        {
            SetUsername(user.UserName);
            SetPassword(user.Password);
            SetConfirmPassword(user.Password);
            CheckAutoLoginCheckbox(user.IsAutoLogin);
            CheckAdministratorCheckbox(user.IsAdmin);
        }

        // other methods
        public void SearchForUserAndVerifyItExists(string username, bool desiredResult, bool verifyIsAdmin = false)
        {
            var users = UsersTable.GetData(true);
            var row = users.GetRowWithValue(username, 0);

            if (desiredResult)
                Specify.That(row).Should.Not.BeNull(string.Format("user {0} not found in Users List", username));
            else
                Specify.That(row).Should.BeNull(string.Format("user {0} already exists", username));

            if (verifyIsAdmin && row != null)
            {
                Specify.That(row.Columns[1].Value == "Administrator").Should.Not.BeTrue("User is not an admin");
            }
        }
        public static UserManagementPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<UserManagementPage>();
        }
        public void PageTableSelectRow(int rowIndex)
        {
            UsersTable.SelectRow(rowIndex);
        }

    }
}