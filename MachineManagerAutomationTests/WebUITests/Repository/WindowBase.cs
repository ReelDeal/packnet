﻿using CUITe.Controls.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
   public class WindowBase : CUITe_DynamicBrowserWindow
    {
        #region Constructors and Destructors
        public WindowBase(string title = "Packnet.Server") : base(title) {
            SearchProperties[PropertyNames.Name] = title;
        }
        #endregion
   }
    }