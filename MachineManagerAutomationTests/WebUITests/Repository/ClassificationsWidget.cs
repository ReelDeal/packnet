﻿using System;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;

namespace MachineManagerAutomationTests.WebUITests.Repository
{

    public class ClassificationsWidget : WindowBase
    {
        public void SetPrinterPriority(Classification c, bool value)
        {
            SetPrinterPriority(c.Alias, value);
        }

        public void SetPrinterPriority(string alias, bool value)
        {
            var chk = GetPriorityCheckBox(alias);
            if (value)
            {
                Retry.For(chk.Check, TimeSpan.FromSeconds(2));
                Specify.That(chk.Checked).Should.BeEqualTo(true);
            }
            else
            {
                Retry.For(chk.UnCheck, TimeSpan.FromSeconds(2));
                Specify.That(chk.Checked).Should.BeEqualTo(false);
            }
        }

        public bool GetStatus(Classification classification, ClassificationStatuses prio)
        {
            return GetStatus(classification.Alias, prio);
        }
        public bool GetStatus(string alias, ClassificationStatuses prio)
        {
            if (prio == ClassificationStatuses.Expedite)
                return GetExpediteButton(alias).IsSelected;
            else if (prio == ClassificationStatuses.Normal)
                return GetNormalButton(alias).IsSelected;
            else if (prio == ClassificationStatuses.Normal)
                return GetLastButton(alias).IsSelected;
            else
                return GetStopButton(alias).IsSelected;
        }

        public void SetStatus(Classification classification, ClassificationStatuses prio)
        {
            SetStatus(classification.Alias, prio);
        }
        public void SetStatus(string alias, ClassificationStatuses prio)
        {
            CUITe_HtmlRadioButton rbtn = null;
            if (prio == ClassificationStatuses.Expedite)
                rbtn = GetExpediteButton(alias);
            else if (prio == ClassificationStatuses.Normal)
                rbtn = GetNormalButton(alias);
            else if (prio == ClassificationStatuses.Normal)
                rbtn = GetLastButton(alias);
            else
                rbtn = GetStopButton(alias);

            if (rbtn.Exists && rbtn.Enabled)
                Retry.For(rbtn.Click, TimeSpan.FromSeconds(2));

            Specify.That(rbtn.IsSelected).Should.BeEqualTo(true);

        }

        public int GetNumberOfRequests(Classification classification)
        {
            return GetNumberOfRequests(classification.Alias);
        }
        public int GetNumberOfRequests(string alias)
        {
            var lbl = Get<CUITe_HtmlLabel>("id=classifications-NumberOfRequests-" + alias);
            return int.Parse(lbl.InnerText);
        }

        // verify
        public void VerifyNumberOfRequests(Classification c, int expectedNoOfRequests)
        {
            var noOfRequests = GetNumberOfRequests(c);
            Specify.That(noOfRequests).Should.BeEqualTo(expectedNoOfRequests,string.Format("VerifyNumberOfRequests for classification '{0}' failed. Expected: {1} Actual: {2}.", c.Alias, expectedNoOfRequests, noOfRequests));
        }
        public void VerifyStatus(Classification c, ClassificationStatuses status, bool value)
        {
            bool actualStatus = GetStatus(c, status);
            Specify.That(actualStatus).Should.BeEqualTo(value, string.Format("VerifyStatus for classification '{0}' and status '{1}' failed. Expected: {2} Actual: {3}.", c.Alias, status.DisplayName,value, actualStatus));
        }

        #region internal
        private bool GetPrinterPriority(Classification classification)
        {
            return GetPrinterPriority(classification.Alias);
        }

        private bool GetPrinterPriority(string alias)
        {
            return Get<CUITe_HtmlCheckBox>("id=classifications-priority-checkbox-" + alias).Checked;
        }

        private CUITe_HtmlRadioButton GetStopButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=classification-stop-button-" + alias);
        }

        private CUITe_HtmlRadioButton GetNormalButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=classification-normal-button-" + alias);
        }

        private CUITe_HtmlCheckBox GetPriorityCheckBox(string alias)
        {
            return Get<CUITe_HtmlCheckBox>("id=classifications-priority-checkbox-" + alias);
        }

        private CUITe_HtmlRadioButton GetExpediteButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=classification-expedite-button-" + alias);
        }

        private CUITe_HtmlRadioButton GetLastButton(string alias)
        {
            return Get<CUITe_HtmlRadioButton>("id=classification-last-button-" + alias);
        }
        #endregion
    }
}




