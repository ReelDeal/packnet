﻿//using System.Diagnostics;

//using CUITe.Controls.HtmlControls;

//namespace MachineManagerAutomationTests.WebUITests.Repository
//{
//    public class ArticlesManagement : DynamicBrowserWindowWithLogin
//    {
//        //private Table _articlesTable = null;
//        private Table _produceArticleTable = null;

//        #region Constructors
//        public ArticlesManagement()
//            : base("Packnet.Server")
//        {
//            //sWindowTitle = "Article Management";
//            //SearchProperties[PropertyNames.Name] = sWindowTitle;
//        }
//        #endregion

//        //management page
//        public CUITe_HtmlButton NewArticle { get { return Get<CUITe_HtmlButton>("id=newArticle"); } }
//        public CUITe_HtmlButton DeleteArticle { get { return Get<CUITe_HtmlButton>("id=deleteArticles"); } }
//        public CUITe_HtmlButton EditArticle { get { return Get<CUITe_HtmlButton>("id=editArticle"); } }
//        public CUITe_HtmlButton ProduceArticles { get { return Get<CUITe_HtmlButton>("id=produceArticle"); } }

//        //new article
//        public CUITe_HtmlLabel ArticleNameLabel { get { return Get<CUITe_HtmlLabel>("id=kitAliasLbl"); } }
//        public CUITe_HtmlEdit ArticleNameEdit { get { return Get<CUITe_HtmlEdit>("id=kitAlias"); } }
//        public CUITe_HtmlParagraph ArticleNameValidation { get { return Get<CUITe_HtmlParagraph>("id=kitAliasValidation"); } }
//        public CUITe_HtmlLabel ArticleDescriptionLabel { get { return Get<CUITe_HtmlLabel>("id=kitDescriptionLbl"); } }
//        public CUITe_HtmlTextArea ArticleDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=kitDescription"); } }
//        public CUITe_HtmlButton ArticleSaveButton { get { return Get<CUITe_HtmlButton>("id=kitSaveButton"); } }
//        public CUITe_HtmlButton ArticleCancelButton { get { return Get<CUITe_HtmlButton>("id=kitCancelButton"); } }
//        public CUITe_HtmlButton NewArticleDropDown { get { return Get<CUITe_HtmlButton>("id=newArticleDropDown"); } }
//        public CUITe_HtmlHyperlink NewCarton { get { return Get<CUITe_HtmlHyperlink>("id=newCarton"); } }
//        public CUITe_HtmlHyperlink NewLabel { get { return Get<CUITe_HtmlHyperlink>("id=newLabel"); } }
//        public CUITe_HtmlHyperlink NewKit { get { return Get<CUITe_HtmlHyperlink>("id=newKit"); } }

//        //new carton
//        public CUITe_HtmlLabel CartonAliasLabel { get { return Get<CUITe_HtmlLabel>("id=aliasLbl"); } }
//        public CUITe_HtmlEdit CartonAliasEdit { get { return Get<CUITe_HtmlEdit>("id=alias"); } }
//        public CUITe_HtmlParagraph CartonAliasValidation { get { return Get<CUITe_HtmlParagraph>("id=aliasValidation"); } }
//        public CUITe_HtmlLabel CartonDescriptionLabel { get { return Get<CUITe_HtmlLabel>("id=descriptionLbl"); } }
//        public CUITe_HtmlTextArea CartonDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=description"); } }
//        public CUITe_HtmlLabel CartonQuantityLabel { get { return Get<CUITe_HtmlLabel>("id=quantityLbl"); } }
//        public CUITe_HtmlEdit CartonQuantityEdit { get { return Get<CUITe_HtmlEdit>("id=quantity"); } }
//        public CUITe_HtmlLabel CartonDesignLabel { get { return Get<CUITe_HtmlLabel>("id=designLbl"); } }
//        public CUITe_HtmlComboBox CartonDesignEdit { get { return Get<CUITe_HtmlComboBox>("id=design"); } }
//        public CUITe_HtmlParagraph CartonDesignValidation { get { return Get<CUITe_HtmlParagraph>("id=designValidation"); } }
//        public CUITe_HtmlLabel CartonVariablesLabel { get { return Get<CUITe_HtmlLabel>("id=variablesLbl"); } }
//        public CUITe_HtmlLabel CartonLengthLabel { get { return Get<CUITe_HtmlLabel>("id=lengthLbl"); } }
//        public CUITe_HtmlEdit CartonLengthEdit { get { return Get<CUITe_HtmlEdit>("id=Length"); } }
//        public CUITe_HtmlParagraph CartonLengthValidation { get { return Get<CUITe_HtmlParagraph>("id=lengthValidation"); } }
//        public CUITe_HtmlLabel CartonWidthLabel { get { return Get<CUITe_HtmlLabel>("id=widthLbl"); } }
//        public CUITe_HtmlEdit CartonWidthEdit { get { return Get<CUITe_HtmlEdit>("id=Width"); } }
//        public CUITe_HtmlParagraph CartonWidthValidation { get { return Get<CUITe_HtmlParagraph>("id=widthValidation"); } }
//        public CUITe_HtmlLabel CartonHeightLabel { get { return Get<CUITe_HtmlLabel>("id=heightLbl"); } }
//        public CUITe_HtmlEdit CartonHeightEdit { get { return Get<CUITe_HtmlEdit>("id=Height"); } }
//        public CUITe_HtmlParagraph CartonHeightValidation { get { return Get<CUITe_HtmlParagraph>("id=heightValidation"); } }
//        public CUITe_HtmlLabel CartonRestrictionsLabel { get { return Get<CUITe_HtmlLabel>("id=restrictionsLbl"); } }
//        public CUITe_HtmlLabel CartonCorrugateQualityLabel { get { return Get<CUITe_HtmlLabel>("id=corrugateQualityLbl"); } }
//        public CUITe_HtmlEdit CartonCorrugateQualityEdit { get { return Get<CUITe_HtmlEdit>("id=corrugateQuality"); } }
//        public CUITe_HtmlLabel CartonCorrugateWidthLabel { get { return Get<CUITe_HtmlLabel>("id=corrugateWidthLbl"); } }
//        public CUITe_HtmlEdit CartonCorrugateWidthEdit { get { return Get<CUITe_HtmlEdit>("id=CorrugateWidth"); } }
//        public CUITe_HtmlParagraph CartonCorrugateWidthValidation { get { return Get<CUITe_HtmlParagraph>("id=corrugateWidthValidation"); } }
//        public CUITe_HtmlLabel CartonCorrugateThicknessLabel { get { return Get<CUITe_HtmlLabel>("id=corrugateThicknessLbl"); } }
//        public CUITe_HtmlEdit CartonCorrugateThicknessEdit { get { return Get<CUITe_HtmlEdit>("id=CorrugateThickness"); } }
//        public CUITe_HtmlParagraph CartonCorrugateThicknessValidation { get { return Get<CUITe_HtmlParagraph>("id=corrugateThicknessValidation"); } }
//        public CUITe_HtmlLabel CartonRotationLabel { get { return Get<CUITe_HtmlLabel>("id=rotationLbl"); } }
//        public CUITe_HtmlComboBox CartonRotationEdit { get { return Get<CUITe_HtmlComboBox>("id=rotation"); } }
//        public CUITe_HtmlParagraph CartonRotationValidation { get { return Get<CUITe_HtmlParagraph>("id=rotationValidation"); } }
//        public CUITe_HtmlHyperlink HidePackageDesignPreview { get { return Get<CUITe_HtmlHyperlink>("id=toggleDesignPreviewContainer"); } }
//        public CUITe_HtmlButton CartonSaveButton { get { return Get<CUITe_HtmlButton>("id=cartonSaveButton"); } }
//        public CUITe_HtmlButton CartonCancelButton { get { return Get<CUITe_HtmlButton>("id=cartonCancelButton"); } }

//        //new label
//        public CUITe_HtmlLabel LabelAliasLabel { get { return Get<CUITe_HtmlLabel>("id=labelAliasLbl"); } }
//        public CUITe_HtmlEdit LabelAliasEdit { get { return Get<CUITe_HtmlEdit>("id=labelAlias"); } }
//        public CUITe_HtmlParagraph LabelAliasValidation { get { return Get<CUITe_HtmlParagraph>("id=labelAliasValidation"); } }
//        public CUITe_HtmlLabel LabelDescriptionLabel { get { return Get<CUITe_HtmlLabel>("id=labelDescriptionLbl"); } }
//        public CUITe_HtmlTextArea LabelDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=labelDescription"); } }
//        public CUITe_HtmlLabel LabelQuantityLabel { get { return Get<CUITe_HtmlLabel>("id=labelQuantityLbl"); } }
//        public CUITe_HtmlEdit LabelQuantityEdit { get { return Get<CUITe_HtmlEdit>("id=labelQuantity"); } }
//        public CUITe_HtmlLabel LabelTemplateLabel { get { return Get<CUITe_HtmlLabel>("id=labelTemplateLbl"); } }
//        public CUITe_HtmlComboBox LabelTemplateEdit { get { return Get<CUITe_HtmlComboBox>("id=labelTemplate"); } }
//        public CUITe_HtmlParagraph LabelTemplateValidation { get { return Get<CUITe_HtmlParagraph>("id=lableTemplateValidation"); } }
//        public CUITe_HtmlLabel LabelVariablesLabel { get { return Get<CUITe_HtmlLabel>("id=labelVariablesLbl"); } }
//        public CUITe_HtmlButton LabelSaveButton { get { return Get<CUITe_HtmlButton>("id=labelSaveButton"); } }
//        public CUITe_HtmlButton LabelCancelButton { get { return Get<CUITe_HtmlButton>("id=labelCancelButton"); } }






//        //public CUITe_HtmlHeading4 ArticlesPageTitle { get { return Get<CUITe_HtmlHeading4>("id=articlesTitle"); } }
//        //public CUITe_HtmlButton ArticleButtonNew { get { return Get<CUITe_HtmlButton>("id=articleButtonNew"); } }
//        //public CUITe_HtmlButton ArticleButtonCopy { get { return Get<CUITe_HtmlButton>("id=articleButtonCopy"); } }
//        //public CUITe_HtmlButton ArticleButtonEdit { get { return Get<CUITe_HtmlButton>("id=articleButtonEdit"); } }
//        //public CUITe_HtmlButton ArticleButtonDelete { get { return Get<CUITe_HtmlButton>("id=articleButtonDelete"); } }
//        //public CUITe_HtmlButton ArticleButtonProduce { get { return Get<CUITe_HtmlButton>("id=articleButtonProduce"); } }
//        //public CUITe_HtmlCustom ArticleList { get { return Get<CUITe_HtmlParagraph>("id=articleList"); } }
//        //public CUITe_HtmlDiv ArticlesTableDiv { get { return Get<CUITe_HtmlDiv>("id=articlesTable"); } }
//        //public Table ArticlesTable
//        //{
//        //    get { return _articlesTable ?? (_articlesTable = new Table(ArticlesTableDiv, "Article")); }
//        //}

//        //public CUITe_HtmlLabel ArticleIdLabel { get { return Get<CUITe_HtmlLabel>("id=lblArticleId"); } }
//        //public CUITe_HtmlEdit ArticleIdEdit { get { return Get<CUITe_HtmlEdit>("Name=ArticleId"); } }
//        //public CUITe_HtmlParagraph ArticleIdValidation { get { return Get<CUITe_HtmlParagraph>("id=ArticleIdNotEmptyValidation"); } }
//        //public CUITe_HtmlTextArea DescriptionEdit { get { return Get<CUITe_HtmlTextArea>("Name=Description"); } }
//        //public CUITe_HtmlComboBox DesignIdList { get { return Get<CUITe_HtmlComboBox>("Name=DesignId"); } }
//        //public CUITe_HtmlEdit LengthEdit { get { return Get<CUITe_HtmlEdit>("Name=Length"); } }
//        //public CUITe_HtmlEdit WidthEdit { get { return Get<CUITe_HtmlEdit>("Name=Width"); } }
//        //public CUITe_HtmlEdit HeightEdit { get { return Get<CUITe_HtmlEdit>("Name=Height"); } }
//        //public CUITe_HtmlEdit CorrugateQualityEdit { get { return Get<CUITe_HtmlEdit>("Name=CorrugateQuality"); } }
//        //public CUITe_HtmlButton ArticleNewSave { get { return Get<CUITe_HtmlButton>("id=newArticleButtonSave"); } }
//        //public CUITe_HtmlButton ArticleNewClose { get { return Get<CUITe_HtmlButton>("id=newArticleButtonClose"); } }
//        //public CUITe_HtmlButton ArticleCopyConfirm { get { return Get<CUITe_HtmlButton>("id=copyArticleButtonConfirm"); } }
//        //public CUITe_HtmlButton ArticleCopyCancel { get { return Get<CUITe_HtmlButton>("id=copyArticleButtonCancel"); } }
//        //public CUITe_HtmlButton ArticleEditSave { get { return Get<CUITe_HtmlButton>("id=editArticleButtonSave"); } }
//        //public CUITe_HtmlButton ArticleEditClose { get { return Get<CUITe_HtmlButton>("id=editArticleButtonClose"); } }
//        //public CUITe_HtmlButton ArticleDeleteConfirm { get { return Get<CUITe_HtmlButton>("id=deleteArticleButtonConfirm"); } }
//        //public CUITe_HtmlButton ArticleDeleteCancel { get { return Get<CUITe_HtmlButton>("id=deleteArticleButtonCancel"); } }
//        public CUITe_HtmlDiv ProduceArticleTableDiv { get { return Get<CUITe_HtmlDiv>("id=produceArticleTable"); } }
//        public Table ProduceArticleTable
//        {
//            get { return _produceArticleTable ?? (_produceArticleTable = new Table(ProduceArticleTableDiv, "ProduceArticle")); }
//        }

//        public CUITe_HtmlComboBox ProduceArticleProductionGroup(int rowId, int rowIdx)
//        {
//            return ProduceArticleTable.FindRow(rowId).Get<CUITe_HtmlComboBox>("id=articleJobProductionGroup_" + rowIdx);
//        }
//        public CUITe_HtmlButton ProduceArticleRemoveButton(int rowId)
//        {
//            return ProduceArticleTable.FindRow(rowId).Get<CUITe_HtmlButton>("id=removeArticleToProduce");
//        }
//        public CUITe_HtmlEdit ProduceArticleOrderNumber(int rowId)
//        {
//            return ProduceArticleTable.FindRow(rowId).Get<CUITe_HtmlEdit>("id=articleToProduceOrderId");
//        }

//        public CUITe_HtmlEdit ProduceArticleNudInputText(int rowId)
//        {
//            return ProduceArticleTable.FindRow(rowId).Get<CUITe_HtmlEdit>("id=produceArticleNUD");
//        }
//        public CUITe_HtmlButton ProduceArticleNudIncrementButton(int rowId)
//        {
//            return ProduceArticleTable.FindRow(rowId).Get<CUITe_HtmlButton>("id=nudIncrementButton");
//        }
//        public CUITe_HtmlButton ProduceArticleNudDecrementButton(int rowId)
//        {
//            return ProduceArticleTable.FindRow(rowId).Get<CUITe_HtmlButton>("id=nudDecrementButton");
//        }
//        public CUITe_HtmlButton ArticleProduceConfirm { get { return Get<CUITe_HtmlButton>("id=produceArticleButtonConfirm"); } }
//        public CUITe_HtmlButton ArticleProduceCancel { get { return Get<CUITe_HtmlButton>("id=produceArticleButtonCancel"); } }
//        public AlertWidget NewArticleWindowAlert { get { return Get<AlertWidget>("id=newArticleAlert"); } }
//        public AlertWidget EditArticleWindowAlert { get { return Get<AlertWidget>("id=editArticleAlert"); } }
//        public AlertWidget DeleteArticleWindowAlert { get { return Get<AlertWidget>("id=deleteArticleAlerts"); } }
//        public AlertWidget ProduceArticleWindowAlert { get { return Get<AlertWidget>("id=produceArticleAlert"); } }


//        public CUITe_HtmlButton PreviewHideButton { get { return Get<CUITe_HtmlButton>("id=hidePreview"); } }

//        public static int Col_ArticleId = 1;
//        public static int Col_Description = 2;
//        public static int Col_Length = 3;
//        public static int Col_Width = 4;
//        public static int Col_Height = 5;
//        public static int Col_DesignName = 6;
//        public static int Col_Quality = 7;
//    }


//}
