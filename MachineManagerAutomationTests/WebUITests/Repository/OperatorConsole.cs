﻿using System;
using System.Threading;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public enum OpNavPages
    {
        MachineProduction,
        ChangeMachineGroup,
        ChangeCorrugate,
        ProductionGroup,
        ChangeProductionGroup,
        MachineControl,
        SearchOrders,
        SearchBoxLast,
        SearchBoxFirst,
        SearchScanToCreate,
        CustomJob,
        CustomJobFromArticle,
        AdministratorPanel
    }

    public class OperatorConsole : ConsoleBase
    {
        #region Page Objects

        private CUITe_HtmlLabel ConfigurationLink { get { return Get<CUITe_HtmlLabel>("id=Op_ConfigurationLink"); } }
        private CUITe_HtmlHyperlink ChangeCorrugatesLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeCorrugatesLink"); } }
        private CUITe_HtmlHyperlink ProductionGroupLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeProductionGroupLink"); } }
        private CUITe_HtmlHyperlink MachineControlLink { get { return Get<CUITe_HtmlHyperlink>("id=MachineCommandsLink"); } }

        // search
        private CUITe_HtmlLabel SearchLink { get { return Get<CUITe_HtmlLabel>("id=op_SearchLink"); } }
        private CUITe_HtmlHyperlink SearchBoxFirstLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchBoxFirstLink"); } }
        private CUITe_HtmlHyperlink SearchBoxLastLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchBoxLastLink"); } }
        private CUITe_HtmlHyperlink SearchOrdersLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchOrdersLink"); } }
        private CUITe_HtmlHyperlink SearchScanToCreateLink { get { return Get<CUITe_HtmlHyperlink>("id=SearchScanToCreateLink"); } }

        // carton Creation
        private CUITe_HtmlLabel CartonCreationLink { get { return Get<CUITe_HtmlLabel>("id=CartonCreationLink"); } }
        private CUITe_HtmlHyperlink ArticleLink { get { return Get<CUITe_HtmlHyperlink>("id=CreateFromArticleLink"); } }
        private CUITe_HtmlHyperlink CustomJobLink { get { return Get<CUITe_HtmlHyperlink>("id=CreateCustomJobLink"); } }

        private CUITe_HtmlHyperlink ChangeMachineGroupLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeMachinesLink"); } }
        private CUITe_HtmlLabel OperationsLink { get { return Get<CUITe_HtmlLabel>("id=OperationsLink"); } }
        private CUITe_HtmlHyperlink MachineProductionLink { get { return Get<CUITe_HtmlHyperlink>("id=MachineProductionLink"); } }

        private CUITe_HtmlHyperlink ChangeProductionGroupLink { get { return Get<CUITe_HtmlHyperlink>("id=ChangeProductionGroupLink"); } }

        //Administrator
        private CUITe_HtmlLabel OpAdministratorLink { get { return Get<CUITe_HtmlLabel>("id=OpAdministratorLink"); } }
        private CUITe_HtmlHyperlink AdministratorPanelLink { get { return Get<CUITe_HtmlHyperlink>("id=AdministratorPanelLink"); } }

        #endregion

        public void SelectMachineGroup(string alias)
        {
            base.SelectMachineGroup(alias, this);
        }
        public void SelectMachineGroup(Guid mgId)
        {
            base.SelectMachineGroup(mgId, this);
        }

        public void NavigateToPage(OpNavPages page)
        {
            switch (page)
            {
                case OpNavPages.MachineProduction:
                    if (!MachineProductionLink.Enabled)
                        OperationsLink.SafeClick();
                    MachineProductionLink.SafeClick();
                    break;
                case OpNavPages.ChangeMachineGroup:
                    if (!ChangeMachineGroupLink.Enabled)
                        OperationsLink.SafeClick();
                    ChangeMachineGroupLink.SafeClick();
                    break;
                case OpNavPages.ChangeCorrugate:
                    if (!ChangeCorrugatesLink.Enabled)
                        ConfigurationLink.SafeClick();
                    ChangeCorrugatesLink.SafeClick();
                    break;
                case OpNavPages.ProductionGroup:
                    if (!ProductionGroupLink.Enabled)
                        ConfigurationLink.SafeClick();
                    ProductionGroupLink.SafeClick();
                    break;
                case OpNavPages.MachineControl:
                    if (!MachineControlLink.Enabled)
                        ConfigurationLink.SafeClick();
                    MachineControlLink.SafeClick();
                    break;
                case OpNavPages.SearchBoxFirst:
                    if (!SearchBoxFirstLink.Enabled)
                        SearchLink.SafeClick();
                    SearchBoxFirstLink.SafeClick();
                    break;
                case OpNavPages.SearchBoxLast:
                    if (!SearchBoxLastLink.Enabled)
                        SearchLink.SafeClick();
                    SearchBoxLastLink.SafeClick();
                    break;
                case OpNavPages.SearchOrders:
                    if (!SearchOrdersLink.Enabled)
                        SearchLink.SafeClick();
                    SearchOrdersLink.SafeClick();
                    break;
                case OpNavPages.SearchScanToCreate:
                    if (!SearchScanToCreateLink.Enabled)
                        SearchLink.SafeClick();
                    SearchScanToCreateLink.SafeClick();
                    break;
                case OpNavPages.CustomJob:
                    if (!CustomJobLink.Enabled)
                        CartonCreationLink.SafeClick();
                    CustomJobLink.SafeClick();
                    break;
                case OpNavPages.CustomJobFromArticle:
                    if (!ArticleLink.Enabled)
                        CartonCreationLink.SafeClick();
                    ArticleLink.SafeClick();
                    break;
                case OpNavPages.AdministratorPanel:
                    if (!AdministratorPanelLink.Enabled)
                        OpAdministratorLink.SafeClick();
                    AdministratorPanelLink.SafeClick();
                    break;
                case OpNavPages.ChangeProductionGroup:
                    if (!ChangeProductionGroupLink.Enabled)
                        ConfigurationLink.SafeClick();
                    ChangeProductionGroupLink.SafeClick();
                    break;
            }
        }
    }
}