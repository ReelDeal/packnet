﻿using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class SelectProductionGroupPage : WindowBase
    {
        private AlertWidget ChangeProductionGroupWidget { get { return Get<AlertWidget>("id=changeProductionGroupAlerts"); } }
        private CUITe_HtmlButton ChangeProductionGroupSaveButton { get { return Get<CUITe_HtmlButton>("id=ProductionGroupsSaveButton"); } }

        public void Select(string name, ConsoleBase console)
        {
            ((AdminConsole)console).NavigateToPage(NavPages.ChangeProductionGroup);
            Get<CUITe_HtmlDiv>("id=" + name).SafeClick();
            ChangeProductionGroupSaveButton.SafeClick();
            VerifySuccessfullySavedMessage();
        }

        public void VerifySuccessfullySavedMessage()
        {
            TestUtilities.VerifyAlert(ChangeProductionGroupWidget, string.Empty, "Production group changed successfully.","The success message is incorrect.", false);
        }
    }
}
