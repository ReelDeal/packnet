﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UITesting;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Common.Interfaces.Machines;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ChangeCorrugatePage : WindowBase
    {
        // form
        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=SaveCorrugateButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=CloseCorrugateButton"); } }

        // methods to get individual controls
        private CUITe_HtmlButton GetChangeCorrugateDropDownList(CUITe_HtmlDiv trackDiv, int machineIndex, int trackNumber)
        {
            return trackDiv.Get<CUITe_HtmlButton>("id=machineTracks" + machineIndex + "Track" + trackNumber);
        }
        private CUITe_HtmlButton GetChangeCorrugateButton(int machineIndex)
        {
            return Get<CUITe_HtmlButton>("id=pauseMachine" + machineIndex);
        }
        private CUITe_HtmlDiv GetChangeCorrugateTrackDiv(int machineIndex, int trackNumber)
        {
            return Get<CUITe_HtmlDiv>("id=machine" + machineIndex + "track" + trackNumber + "div");
        }
        private CUITe_HtmlHyperlink GetCorrugateInDropDownList(CUITe_HtmlDiv trackDiv, string corrugateAlias)
        {
            return trackDiv.Get<CUITe_HtmlHyperlink>("innertext=" + corrugateAlias);
        }
        private CUITe_HtmlEdit GetChangeCorrugateOffsetBox(CUITe_HtmlDiv trackDiv, int trackNumber)
        {
            return trackDiv.Get<CUITe_HtmlEdit>("id=track" + trackNumber + "OffsetBox");
        }

        // methods to change corrugate
        public bool ChangeCorrugatesForAllMachineGroups(ProductionGroupExt pg, TestSteps testSteps, AdminConsole adminConsole)
        {
            var corrugatesChanged = false;
            if (pg.TrackConnectors != null && pg.MachineGroups.Count > 0)
            {
                bool firstMg = true;
                foreach (var mg in pg.MachineGroups)
                {
                    if (firstMg)
                    {
                        adminConsole.NavigateToPage(NavPages.ChangeCorrugate);
                        Playback.Wait(1000);
                        adminConsole.SelectMachineGroup(mg.SetId(), false);
                    }
                    else
                    {
                        Playback.Wait(1000);
                        adminConsole.SelectMachineGroup(mg.SetId(), false);
                        adminConsole.NavigateToPage(NavPages.ChangeCorrugate);
                    }

                    ChangeCorrugateForMachineGroup(mg.Machines, pg.TrackConnectors);

                    if (SaveButton.Enabled)
                    {
                        ClickSaveButton();

                        if (testSteps != null && testSteps.Exists(TestInserts.VerifyOverlappingTracks))
                            VerifyOverlappingTracks();
                    }

                    firstMg = false;
                }

                corrugatesChanged = true;
            }
            return corrugatesChanged;
        }
        public void ChangeCorrugateForMachineGroup(List<IMachine> machines, List<MachineTrackConnector> trackConnectors)
        {
            var machineIdx = 0;
            foreach (var m in machines)
            {
                var tracksForAMachine = trackConnectors.FindAll(z => z.Machine.MachineAlias == m.MachineAlias);
                if (tracksForAMachine.Count > 0)
                {
                    if (tracksForAMachine.Count > ((IPacksizeCutCreaseMachine)m).NumTracks)
                        Specify.Failure(string.Format("Number of tracks in TrackConnectors are larger than set up for the machine '{0}'.", m.MachineAlias));

                    ChangeCorrugateForOneMachine(machineIdx, tracksForAMachine);
                }
                machineIdx++;
            }

        }
        public void ChangeCorrugateForOneMachine(int machineIndex, List<MachineTrackConnector> tracksForAMachine)
        {
            ClickChangeCorrugateButton(machineIndex);

            foreach (var track in tracksForAMachine)
            {
                var trackDiv = GetChangeCorrugateTrackDiv(machineIndex, track.Track);
                ClickChangeCorrugateDropDownList(trackDiv, machineIndex, track.Track);
                ClickCorrugateInDropDownList(trackDiv, track.Corrugate.Alias);
                if (track.Machine is EmMachineExt)
                    SetOffsetField(trackDiv, track.Track, track.Offset);
            }
        }

        // button actions
        public void ClickSaveButton()
        {
            SaveButton.SafeClick();
        }
        public void ClickCancelButton()
        {
            CancelButton.SafeClick();
        }

        private void ClickChangeCorrugateButton(int machineIndex)
        {
            var btn = GetChangeCorrugateButton(machineIndex);
            if (btn.Enabled)
                btn.Click();
        }
        private void ClickCorrugateInDropDownList(CUITe_HtmlDiv trackDiv, string alias)
        {
            Playback.Wait(250);
            GetCorrugateInDropDownList(trackDiv, alias).Click();
        }
        private void ClickChangeCorrugateDropDownList(CUITe_HtmlDiv trackDiv, int machineIndex, int trackNumber)
        {
            GetChangeCorrugateDropDownList(trackDiv, machineIndex, trackNumber).Click();
        }

        // assign values to fields
        private void SetOffsetField(CUITe_HtmlDiv trackDiv, int trackNumber, double offset)
        {
            GetChangeCorrugateOffsetBox(trackDiv, trackNumber).SafeSetText(offset);
        }

        // verify
        private void VerifyOverlappingTracks()
        {
            var msg = "Track 1,2 overlapping on machine '";
            Retry.For(
                () =>
                    Specify.That(this.Get<CUITe.Controls.HtmlControls.CUITe_HtmlSpan>("InnerText=" + msg).Exists)
                        .Should.BeTrue(), TimeSpan.FromSeconds(6));
        }
    }
}
