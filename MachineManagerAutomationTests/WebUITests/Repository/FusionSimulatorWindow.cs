﻿//using CUITe.Controls.WinControls;
//using CUITe.Controls.WpfControls;

//namespace MachineManagerAutomationTests.WebUITests.Repository
//{
//    public class FusionSimulatorWindow : CUITe_WinWindow
//    {
//        public FusionSimulatorWindow()
//            : base("Name=Fusion Simulator")
//        {
//        }

//        //public CUITe_WinEdit HostIP { get { return Get<CUITe_WinWindow>("ControlName=txtHost").Get<CUITe_WinEdit>(); } }
//        //public CUITe_WinComboBox HostPort { get { return Get<CUITe_WinWindow>("ControlName=nudPort").Get<CUITe_WinComboBox>(); } }
//        //public CUITe_WinEdit PrinterIP { get { return Get<CUITe_WinWindow>("ControlName=printerIPTB").Get<CUITe_WinEdit>(); } }
//        //public CUITe_WinComboBox PrinterPort { get { return Get<CUITe_WinWindow>("ControlName=printerPortNUD").Get<CUITe_WinComboBox>(); } }

//        //public CUITe_WinButton ClearLogButton { get { return Get<CUITe_WinWindow>("ControlName=btnClearLog").Get<CUITe_WinButton>(); } }
//        // public CUITe_WinButton ToggleChangeCorrugateButton { get { return Get<CUITe_WinWindow>("ControlName=btnToggleCorrugate").Get<CUITe_WinButton>(); } }

//        //public CUITe_WinButton TakeLabelButton { get { return Get<CUITe_WinWindow>("ControlName=buttonTakeLabel").Get<CUITe_WinButton>(); } }
//        public CUITe_WpfButton PlayPauseButton { get { return Get<CUITe_WpfWindow>("Name=PausePlayButton").Get<CUITe_WpfButton>(); } }
//        public CUITe_WpfList SimulatorList { get { return Get<CUITe_WpfWindow>("Name=SimulatorsList").Get<CUITe_WpfList>(); } }
//        public CUITe_WpfButton StartButton { get { return Get<CUITe_WpfWindow>("Name=StartStopSimulationButton").Get<CUITe_WpfButton>(); } }

//        public CUITe_WpfEdit HostName { get { return Get<CUITe_WpfWindow>("Name=nameTextBox").Get<CUITe_WpfEdit>(); } }
//        public CUITe_WpfEdit HostIP { get { return Get<CUITe_WpfWindow>("Name=ipAddressTextBox").Get<CUITe_WpfEdit>(); } }
//        public CUITe_WpfEdit HostPort { get { return Get<CUITe_WpfWindow>("Name=portTextBox").Get<CUITe_WpfEdit>(); } }



//        public CUITe_WinButton RButton { get { return Get<CUITe_WinWindow>("ControlName=btnRPhysical").Get<CUITe_WinButton>(); } }
//        public CUITe_WinButton MButton { get { return Get<CUITe_WinWindow>("ControlName=btnMPhysical").Get<CUITe_WinButton>(); } }
//        public CUITe_WinButton LButton { get { return Get<CUITe_WinWindow>("ControlName=btnLPhysical").Get<CUITe_WinButton>(); } }
//        public CUITe_WinButton EStopButton { get { return Get<CUITe_WinWindow>("ControlName=btnEStopPhysical").Get<CUITe_WinButton>(); } }
//        //public CUITe_WinButton StartButton { get { return Get<CUITe_WinWindow>("ControlName=btnStartSimulation").Get<CUITe_WinButton>(); } }
//        public CUITe_WinButton StopButton { get { return Get<CUITe_WinWindow>("ControlName=btnStopSimulation").Get<CUITe_WinButton>(); } }
//        public CUITe_WinButton StartUpCompleteButton { get { return Get<CUITe_WinWindow>("ControlName=btnStartupComplete").Get<CUITe_WinButton>(); } }

//        public CUITe_WinEdit Log { get { return Get<CUITe_WinWindow>("ControlName=txtLog").Get<CUITe_WinEdit>(); } }
//        public CUITe_WinEdit LopTime { get { return Get<CUITe_WinWindow>("ControlName=txtLoopTimeSeconds").Get<CUITe_WinEdit>(); } }
//        public CUITe_WinButton LoopButton { get { return Get<CUITe_WinWindow>("ControlName=btnLoopStartComplete").Get<CUITe_WinButton>(); } }
//        public CUITe_WinButton CompleteButton { get { return Get<CUITe_WinWindow>("ControlName=btnCompletePackaging").Get<CUITe_WinButton>(); } }
//        public CUITe_WinButton StartProductionButton { get { return Get<CUITe_WinWindow>("ControlName=btnStartPackaging").Get<CUITe_WinButton>(); } }
//        public CUITe_WinList JobList { get { return Get<CUITe_WinWindow>("ControlName=listBoxMachineQueue").Get<CUITe_WinList>(); } }

//        public CUITe_WinWindow Window { get { return Get<CUITe_WinWindow>(); } }
//    }
//}

////        //UI Detection Details -- ControlType:ControlType.CheckBox, Automation ID:checkBoxPeelOffMode, Name:Is In Peel Off
////        //UI Detection Details -- ControlType:ControlType.CheckBox, Automation ID:checkBoxHeadOpen, Name:Is Head Open
////        //UI Detection Details -- ControlType:ControlType.CheckBox, Automation ID:checkBoxPaperOut, Name:Is Paper Out
////    //UI Detection Details -- ControlType:ControlType.CheckBox, Automation ID:cbxShowInstructions, Name:Show InstructionList
////    //UI Detection Details -- ControlType:ControlType.CheckBox, Automation ID:cbxLS, Name:Show Lifesign

////        //UI Detection Details -- ControlType:ControlType.Edit, Automation ID:txtSimulatedZebraPrinterAddress, Name:0
////        //UI Detection Details -- ControlType:ControlType.List, Automation ID:listBoxMachineQueue, Name:Queue:
////    //UI Detection Details -- ControlType:ControlType.Edit, Automation ID:txtServerAddress, Name:
