﻿namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class Notification
    {
        public string Text { get; set; }
        public string NotificationTime { get; set; }
        public int MessageCount { get; set; }

        public override string ToString()
        {
            return Text + " (" + NotificationTime + ")";
        }
    }
}
