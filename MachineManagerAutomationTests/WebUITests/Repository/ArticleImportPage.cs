﻿using CUITe.Controls.HtmlControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ArticleImportPage : WindowBase
    {
        Table _articleTable = null;
        public Table ArticlesTable
        {
            get
            {
                var div = Get<CUITe_HtmlDiv>("id=CurrentArticleItemsList");
                return _articleTable ?? (_articleTable = new Table(div, new TableConfigData(TableTypes.TwoColumn, "ArticlesTable")));
            }
        }

        private AlertWidget ArticleImportAlert { get { return Get<AlertWidget>("id=changeProductionGroupAlerts"); } }
        private CUITe_HtmlSpan BrowseFileButton { get { return Get<CUITe_HtmlSpan>("id=ArticlesImportBrowseFile"); } }
        private CUITe_HtmlButton ProcessFileButton { get { return Get<CUITe_HtmlButton>("id=ArticleImportProcessFile"); } }
        private CUITe_HtmlButton FinalizeImportButton { get { return Get<CUITe_HtmlButton>("id=ArticleImportFinalizeImport"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=ArticleImportCancelImport"); } }

        public static ArticleImportPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<ArticleImportPage>();
        }

        public void ClickBrowseFileButton()
        {
            BrowseFileButton.Click();
        }
        public void ClickProcessFileButton()
        {
            ProcessFileButton.Click();
        }
        public void ClickFinalizeImportButton()
        {
            FinalizeImportButton.Click();
        }
        public void ClickCancelImportButton()
        {
            CancelButton.Click();
        }
        public void SelectFile(string fileName)
        {
            new UIMap().SelectImportFile(fileName);
        }
    }
}
