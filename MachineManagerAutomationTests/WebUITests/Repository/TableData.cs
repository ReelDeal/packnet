﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class DataTable
    {
        public List<string> Headers { get; set; }
        public List<DataRow> Rows { get; set; }
        public DataRow FirstRow
        {
            get {
                return Rows.Count > 0 ? Rows[0] : null;
            }
        }
        public string GetValue(int rowIndex, int columnIndex)
        {
            var row = Rows[rowIndex];
            if (row != null)
            {
                if (row.Columns.Count > 0)
                {
                    return row.Columns[columnIndex].Value;
                }
            }
            return null;
        }
        
        public DataTable()
        {
            Rows = new List<DataRow>();
        }

        public bool ValueExistsInTable(string value)
        {
            return this.Rows.SelectMany(r => r.Columns).Any(c => c.Value.Equals(value));
        }
    }
    public class DataRow
    {
        public List<DataColumn> Columns { get; set; }

        public DataRow()
        {
            Columns = new List<DataColumn>();
        }

        public override string ToString()
        {
            return "Columns: " + Columns.Count;
        }

    }
    public class DataColumn
    {
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}
