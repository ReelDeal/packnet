﻿using System;
using System.Collections.Generic;
using System.Linq;

using CUITe.Controls;
using CUITe.Controls.HtmlControls;

using Microsoft.VisualStudio.TestTools.UITesting.HtmlControls;

using PackNet.Common.Utils;
using Testing.Specificity;
using MachineManagerAutomationTests.Helpers;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public enum TableTypes
    {
        Unspecified,
        OneColumn,
        TwoColumn,
        ProductionOpen,
        ProductionInProgress,
        ProductionCompleted,
        MachineProductionTable
    }

    public class Table
    {
        private readonly CUITe_HtmlDiv tableDiv;
        private readonly TableConfigData _config;

        public Table(CUITe_HtmlDiv tableDiv, TableConfigData config)
        {
            this.tableDiv = tableDiv;
            TableName = config.TableName;
            TableType = config.TableType;
            _config = config;
        }

        public TableTypes TableType { get; set; }

        public void SelectRowWithValue(string value, int columnIndex = 0)
        {
            var data = GetData();
            if(data!=null)
            {
                var index = data.GetRowIndexWithValue(value, columnIndex);
                if (index < 0)
                    Specify.Failure(string.Format("SelectRowWithValue: There is no row with value '{0}' in column {1}.", value, columnIndex));

                SelectRow(index);
            }
            else
            {
                Specify.Failure(string.Format("SelectRowWithValue: When looking for value '{0}' in table '{1}' data could not be retrieved", value, TableName));
            }
        }

        public int GetCellIntValue(int rowIndex, int columnIndex)
        {
            return int.Parse(GetCellValue(rowIndex, columnIndex));
        }

        public string GetCellValue(int rowIndex, int columnIndex)
        {
            var data = GetData();
            Specify.That(data.Rows.Count > 0).Should.BeTrue(string.Format("There are no rows in the table {0}.",TableName));
            return data.GetCellValue(rowIndex, columnIndex);
        }

        // this method is only used to find out which type of table a new table may be
        // not used in regular code
        public bool CheckTableType()
        {
            try
            {
                if (TableType == TableTypes.Unspecified)
                {
                    if (_config != null)
                    {
                        TableType = _config.TableType;
                    }
                    else
                    {
                        var canvas = tableDiv.Get<CUITe_HtmlDiv>("class~ngCanvas");
                        TableType = canvas.Exists ? TableTypes.OneColumn : TableTypes.TwoColumn;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public CUITe_HtmlDiv TableDiv
        {
            get { return tableDiv; }
        }

        public string TableName { get; set; }
        public CUITe_HtmlButton ToggleFiltering { get { return tableDiv.Get<CUITe_HtmlButton>("id=toggleFiltering"); } }
        public Boolean Exists
        {
            get { return tableDiv.Exists; }
        }

        public CUITe_HtmlComboBox PageSize
        {
            get
            {
                var rowPicker = tableDiv.Get<CUITe_HtmlDiv>("class=ngRowCountPicker");
                var children = rowPicker.GetChildren();

                return children.OfType<CUITe_HtmlComboBox>().FirstOrDefault();

            }
        }

        public CUITe_HtmlDiv FindCell(int rowIndex, int columnIndex)
        {
            var row = FindRow(rowIndex);
            return FindCell(row, columnIndex);
        }

        public CUITe_HtmlDiv FindCell(CUITe_HtmlDiv row, int columnIndex)
        {
            return (CUITe_HtmlDiv)row.GetChildren()[columnIndex];
        }

        public int RowCount(bool refreshData = false)
        {
            if (refreshData)
                _cachedDataTable = null;

            if (TableType == TableTypes.TwoColumn)
            {
                return TableDiv.Get<CUITe_HtmlDiv>("class=ui-grid-canvas").GetChildren().Count;
            }
            else
            {
                var tbl = GetData();
                if (tbl != null)
                    return tbl.Rows.Count;
            }

            return -1;
        }

        public List<CUITe_HtmlDiv> FindRows()
        {
            return FindRows(string.Empty);
        }

        public List<CUITe_HtmlDiv> FindRows(string listType)
        {
            CUITe_HtmlDiv canvas = null;
            var contentsWrapper = tableDiv.Get<CUITe_HtmlDiv>("class=ui-grid-contents-wrapper");
            //This call will not find class="ui-grid-contents-wrapper row somethingelse"
            if (listType == "checkbox")
            {
                var container = contentsWrapper.Get<CUITe_HtmlDiv>("class~ui-grid-viewport");
                canvas = container.Get<CUITe_HtmlDiv>("class~ui-grid-canvas");
            }
            else
            {
                var containerBody = contentsWrapper.Get<CUITe_HtmlDiv>("class~ui-grid-render-container-body");
                var viewPort = containerBody.Get<CUITe_HtmlDiv>("class~ui-grid-viewport");
                canvas = viewPort.Get<CUITe_HtmlDiv>("class~ui-grid-canvas");
            }
            return canvas != null ? canvas.GetChildren().Cast<CUITe_HtmlDiv>().ToList() : null;
        }

        public static UITestControlCollection FindRowsWithCodedUI(CUITe_DynamicBrowserWindow win, string tableId)
        {
            var tableDiv = TestUtilities.GetUITestControl(win, tableId);

            var contentsWrapper = TestUtilities.GetUITestControl(tableDiv,"ui-grid-contents-wrapper","Class");
            var containerBody = TestUtilities.GetUITestControl(contentsWrapper, "ui-grid-render-container ng-isolate-scope ui-grid-render-container-body", "Class");
            var viewPort = TestUtilities.GetUITestControl(containerBody, "ui-grid-viewport ng-isolate-scope", "Class");
            var canvas = TestUtilities.GetUITestControl(viewPort, "ui-grid-canvas", "Class");
            return canvas.GetChildren();
        }

        public void SearchWithSearchField(string value, int columnIndex)
        {
            var tag = tableDiv.Get<CUITe_HtmlEdit>("class~ui-grid-filter-input ui-grid-filter-input-0");
            tag.SetSearchProperty(HtmlControl.PropertyNames.TagInstance, columnIndex.ToString());
            tag.SetText(value);
        }

        public CUITe_HtmlDiv FindRow(int index)
        {
            return FindRow(index, null);
        }

        public CUITe_HtmlDiv FindRow(int rowIndex, string listType)
        {
            var rows = Retry.For(() => FindRows(listType), System.TimeSpan.FromSeconds(10));
            var rowCount = rows.Count;

            if (rowCount == 0)
            {
                throw new Exception("There is no row in the table.");
            }

            if (rowCount > 0 && (rowCount < (rowIndex + 1) || (rowIndex + 1) > rowCount))
            {
                throw new ArgumentOutOfRangeException("Row index can't be large than the number of records in the list.");
            }

            return rows[rowIndex];
        }

        public void SelectRow(int rowIndex)
        {
            SelectRows(rowIndex);
        }

        public void SelectRows(params int[] rowIndexes)
        {
            var rows = Retry.For(() => FindRows("checkbox"), System.TimeSpan.FromSeconds(10));
            foreach (var i in rowIndexes)
            {
                SelectRowInner(rows[i]);
            }
        }

        private void SelectRowInner(CUITe_HtmlDiv row)
        {
            switch (TableType)
            {
                case TableTypes.OneColumn:
                    var chk = row.Get<CUITe_HtmlCheckBox>("class~ngSelectionCheckbox");
                    chk.Check();
                    break;
                case TableTypes.TwoColumn:
                    var div = row.Get<CUITe_HtmlDiv>("class~ui-grid-selection-row-header-buttons");
                    div.Click();
                    break;
            }
        }

        /// <summary>
        /// Only used on page ArticleImportPage
        /// </summary>
        /// <param name="rowIndex"></param>
        public void ClickArticleImportUpdate(int rowIndex)
        {
            var rows = Retry.For(() => FindRows(), System.TimeSpan.FromSeconds(10));
            var lbl = rows[rowIndex].Get<CUITe_HtmlLabel>("class~btn-success");
            lbl.Click();
        }

        /// <summary>
        /// Only used on page ArticleImportPage
        /// </summary>
        /// <param name="rowIndex"></param>
        public void ClickArticleImportIgnore(int rowIndex)
        {
            var rows = Retry.For(() => FindRows(), System.TimeSpan.FromSeconds(10));
            var lbl = rows[rowIndex].Get<CUITe_HtmlLabel>("class~btn-warning");
            lbl.Click();
        }

        public void ClickReproduceButton(int index)
        {
            var rows = Retry.For(() => FindRows(), System.TimeSpan.FromSeconds(10));
            var btn = rows[index].Get<CUITe_HtmlButton>("id=" + _config.ReproduceButtonId);
            Specify.That(btn.Enabled).Should.BeTrue("ReproduceButton must be enabled when clicked on");
            btn.Click();
        }

        public void SetNumberToReproduce(int rowIndex, int number)
        {
            var rows = Retry.For(() => FindRows(), System.TimeSpan.FromSeconds(10));
            var edit = rows[rowIndex].Get<CUITe_HtmlEdit>("id=newMachinePortNUD");
            edit.SafeSetText(number);
        }

        // does not work properly, use SetNumberToReproduce, or fix the method
        //public void ClickReproduceIncrementButton(int index, int numberOfClicks)
        //{
        //    var rows = Retry.For(() => FindRows(), System.TimeSpan.FromSeconds(10));

        //    for (int i = 0; i < numberOfClicks; i++)
        //    {
        //        var btn = rows[index].Get<CUITe_HtmlButton>("id=nudIncrementButton");
        //        btn.Click();
        //    }
        //}

        // does not work properly, use SetNumberToReproduce, or fix the method
        //public void ClickReproduceDecrementButton(int rowIndex)
        //{
        //    var rows = Retry.For(() => FindRows(), System.TimeSpan.FromSeconds(10));
        //    var btn = rows[rowIndex].Get<CUITe_HtmlButton>("id=nudDecrementButton");
        //    if (btn.Enabled)
        //        btn.Click();
        //}

        public void RefreshData()
        {
            _cachedDataTable = null;
        }

        private DataTable _cachedDataTable = null;
        public DataTable GetData(bool refreshData = false)
        {
            if (refreshData)
                _cachedDataTable = null;

            if (_cachedDataTable == null)
            {
                var tbl = new DataTable();
                var rows = FindRows("data");
                List<CUITe_HtmlDiv> cols = null;
                string[] stringSeparators = new string[] {"\r\n"};
                foreach (var row in rows)
                {
                    var r = new DataRow();
                    if (TableType == TableTypes.TwoColumn || TableType == TableTypes.ProductionInProgress)
                    {
                        var children = row.GetChildren().Cast<CUITe_HtmlDiv>();
                        cols = children.SelectMany(c => c.GetChildren()).Cast<CUITe_HtmlDiv>().ToList();
                    }
                    else if (TableType == TableTypes.OneColumn)
                    {
                        cols = row.GetChildren().Cast<CUITe_HtmlDiv>().ToList();
                    }
                    else if (TableType == TableTypes.ProductionCompleted)
                    {
                        cols = null;
                        var TempSplit = row.InnerText.Split(stringSeparators, StringSplitOptions.None);
                        foreach (var item in TempSplit)
                        {
                            var d = new DataColumn { Value = item.Trim() };
                            r.Columns.Add(d);
                        }
                    }
                    if (cols != null)
                    {
                        foreach (var t in cols)
                        {
                            var c = new DataColumn { Value = t.InnerText.Trim() };
                            r.Columns.Add(c);
                        }
                    }
                    tbl.Rows.Add(r);
                }

                _cachedDataTable = tbl;
                return tbl;
            }
            else
            {
                return _cachedDataTable;
            }
        }

        public bool RowContainsValue(int rowIndex, string value)
        {
            var row = FindRow(rowIndex);
            if (row.GetChildren().OfType<CUITe_HtmlDiv>().Any(child => child.InnerText.Trim().ToLower().Contains(value.ToLower())))
            {
                return true;
            }
            return false;
        }

        public bool ContainsValue(int value)
        {
            return ContainsValue(value.ToString());
        }

        public bool ContainsValue(string value)
        {
            return tableDiv.InnerText.Contains(value);
        }

        public void VerifyTableHasExpectedNumberOfRecords(int noOfRecords)
        {
            int currentNoOfRecords = RowCount();
            Specify.That(currentNoOfRecords).Should.BeEqualTo(noOfRecords, string.Format("The table '{0}' should have {1} rows. Now it has {2} rows.", TableName, noOfRecords, currentNoOfRecords));
        }
        public void VerifyContainsValue(int value, bool shouldContainValue = true)
        {
            VerifyContainsValue(value.ToString(), shouldContainValue);
        }
        public void VerifyContainsValue(string value, bool shouldContainValue = true, string message = null)
        {
            bool containsValue = ContainsValue(value);
            if (shouldContainValue)
            {
                if (string.IsNullOrEmpty(message))
                    message = string.Format("The table '{0}' does not contain the value '{1}', but it should.", TableName, value);

                Specify.That(containsValue).Should.BeTrue(string.Format(message, value));
            }
            else
            {
                if (string.IsNullOrEmpty(message))
                    message = string.Format("The table '{0}' contains the value '{1}', but it shouldn't.", TableName, value);

                Specify.That(containsValue).Should.BeFalse(string.Format(message, value));
            }
        }

        public void VerifyTableHasData(bool hasData = true)
        {
            if(hasData)
                Specify.That(this.RowCount() > 0).Should.BeTrue("The table should have at least one rows.");
            else
                Specify.That(this.RowCount() == 0).Should.BeTrue("The table should NOT have any data.");
        }
        public void VerifyCellValue(int rowIndex, int columnIndex, int value)
        {
            VerifyCellValue(rowIndex, columnIndex, value.ToString());
        }
        public void VerifyCellValue(int rowIndex, int columnIndex, string value)
        {
            string existingValue = GetCellValue(rowIndex, columnIndex);
            string errorMsg = string.Format("Cell ({0},{1}) has value '{2}' but should have value '{3}'", rowIndex, columnIndex, existingValue, value);
            Specify.That(existingValue).Should.BeEqualTo(value,errorMsg);
        }
    }

    public class TableConfigData
    {
        public TableTypes TableType { get; set; }
        public string TableName { get; set; }
        public string TableId { get; set; }
        public string RowName { get; set; }
        public string RowAsIdOrClass { get; set; }
        public string ReproduceButtonId { get; set; }
        public string CheckBoxClassName { get; set; }

        public TableConfigData ()
        {
            ReproduceButtonId = "reproduceButton";
        }
        public TableConfigData(TableTypes type, string tableName) : this()
        {
            TableType = type;
            TableName = tableName;
        }
    }
}
