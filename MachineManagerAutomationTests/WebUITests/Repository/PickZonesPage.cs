﻿using System;

using Testing.Specificity;

using MachineManagerAutomationTests.Helpers;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Utils;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class PickZonesPage : CrudPageBase, ICrudLocalizationPage
    {
        private static string NameValidationIdProperty = "newPickZonePickZoneNameValidation";

        // header and widget
        private CUITe_HtmlHeading4 PageTitle { get { return Get<CUITe_HtmlHeading4>("id=pageHeading"); } }
        private AlertWidget AlertWidget { get { return Get<AlertWidget>("id=pickZoneAlerts"); } }

        // list
        private CUITe_HtmlButton NewButton { get { return Get<CUITe_HtmlButton>("id=newPickZone"); } }
        private CUITe_HtmlButton DeleteButton { get { return Get<CUITe_HtmlButton>("id=deletePickZone"); } }
        private CUITe_HtmlButton EditButton { get { return Get<CUITe_HtmlButton>("id=editPickZone"); } }

        private Table _pickzoneTable = null;
        public Table PickZoneTable
        {
            get {
                var div = Get<CUITe_HtmlDiv>("id=pickZoneListTable");
                return _pickzoneTable ?? (_pickzoneTable = new Table(div, new TableConfigData(TableTypes.TwoColumn, "PickZone"))); 
            }
        }

        // form
        private CUITe_HtmlLabel NameLabel { get { return Get<CUITe_HtmlLabel>("id=newPickZonePickZoneNameLabel"); } }
        private CUITe_HtmlEdit NameEdit { get { return Get<CUITe_HtmlEdit>("id=newPickZonePickZoneName"); } }
        private CUITe_HtmlParagraph NameValidation { get { return Get<CUITe_HtmlParagraph>("id=newPickZonePickZoneNameValidation"); } }
        private CUITe_HtmlTextArea DescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=newMachineDescription"); } }

        private CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("id=newPickZoneSaveButton"); } }
        private CUITe_HtmlButton CancelButton { get { return Get<CUITe_HtmlButton>("id=newPickZoneCancelButton"); } }

        // methods
        public static PickZonesPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<PickZonesPage>();
        }
        public void RefreshTables()
        {
            _pickzoneTable = null;
        }
        public void PageTableSelectRow(int rowIndex)
        {
            PickZoneTable.SelectRow(rowIndex);
        }

        // button actions
        public void ClickSaveButton()
        {
            SaveButton.Click();
        }
        public void ClickCancelButton(bool doubleClick = false)
        {
            CancelButton.Click();
            if(doubleClick)
                CancelButton.Click();
        }
        public void ClickNewButton()
        {
            NewButton.SafeClick();
        }
        public void ClickEditButton()
        {
            VerifyEditButtonIsEnabled();
            EditButton.SafeClick();
        }
        public void ClickDeleteButton()
        {
            DeleteButton.SafeClick();
        }
        public void ClickNameEditField()
        {
            NameEdit.Click();
        }
        public void ClickDescriptionEditField()
        {
            DescriptionEdit.Click();
        }

        // assign values to form fields
        public void SetName(string name)
        {
            NameEdit.SetText(name);
        }
        public void SetDescription(string description)
        {
            DescriptionEdit.SetText(description);
        }

        // crud
        public void Create(PickZone pz, AdminConsole adminConsole = null, bool useValidation = false)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            adminConsole.NavigateToPage(NavPages.PickZone);

            ClickNewButton();

            base.VerifyEditPageIsOpen(CancelButton, null);

            EnterInfo(pz);

            ClickSaveButton();

            if (useValidation)
            {
                //Verify the created alert exists
                base.VerifyCreatedSuccessfulMessage(this.AlertWidget, "Pick Zone", pz.Alias);

                VerifyEditButtonIsEnabled(false);
            }
        }
        public void UpdateInfo(int rowIndex, PickZone pz)
        {
            PageTableSelectRow(rowIndex);
            ClickEditButton();
            EnterInfo(pz);
            ClickSaveButton();
            base.VerifyUpdatedSuccessfulMessage(this.AlertWidget, "Pick Zone", pz.Alias);
            RefreshTables();
        }
        public void EnterInfo(PickZone pz)
        {
            Specify.That(CancelButton.Exists).Should.BeTrue("New PickZone page was not shown.");
            SetName(pz.Alias);
            SetDescription(pz.Description);
        }

        #region Page Verification
        public void VerifyDuplicateAliasMessage(string alias)
        {
            base.VerifyDuplicateAliasMessage(this.AlertWidget, "Pick Zone", alias);
        }
        public void VerifyEditPageIsOpen(bool shouldBeOpen = true)
        {
            base.VerifyEditPageIsOpen(CancelButton, NewButton, shouldBeOpen);
        }
        public void VerifyNameLabel(string value)
        {
            base.VerifyNameLabel(NameLabel, value);
        }
        public void VerifyEditButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.EditButton, "edit", enabled);
        }
        public void VerifySaveButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.SaveButton, "save", enabled);
        }
        public void VerifyTableIsEmpty(int remainingNumberOfRecords = 0)
        {
            base.VerifyTableIsEmpty(PickZoneTable, "Pick Zone", remainingNumberOfRecords);
        }
        public void VerifyCreatedSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyCreatedSuccessfulMessage(this.AlertWidget, "Pick Zone", alias, expectedText);
        }
        public void VerifyDeleteButtonIsEnabled(bool enabled = true)
        {
            base.VerifyButtonIsEnabled(this.DeleteButton, "delete", enabled);
        }
        /// <summary>
        /// If parameter shouldExist is true, verify that a validation is displayed and what text is displayed, if false
        /// verify that the validation is NOT displayed
        /// </summary>
        /// <param name="shouldExist">True equals Validation should be displayed, False equals validation should NOT be displayed</param>
        public void VerifyNameValidationIsDisplayed(bool shouldExist = true, string expectedText = null)
        {
            base.VerifyValidationIsDisplayed(NameValidation, "Name", shouldExist, expectedText);
        }
        public void VerifyNameValidationExists(bool shouldExist = true)
        {
            base.VerifyValidationExists(PickZonesPage.NameValidationIdProperty, "Name", shouldExist);
        }
        public void VerifyDeleteSuccessfulMessage(string alias, string expectedText = null)
        {
            base.VerifyDeleteSuccessfulMessage(this.AlertWidget, "Pick Zone", alias, expectedText);
        }
        public void VerifyDeleteSuccessfulMessage(string alias)
        {
            base.VerifyDeleteSuccessfulMessage(this.AlertWidget, "Pick Zone", alias);
        }
        public void VerifyUpdatedSuccessfulMessage(string alias)
        {
            base.VerifyUpdatedSuccessfulMessage(this.AlertWidget, "Pick Zone", alias);
        }
        public void VerifyTableRowContainsSpecifiedData(int row, string alias, string description = null)
        {
            base.VerifyTableRowContainsSpecifiedData(row, PickZoneTable, alias, description, null);
        }
        public void VerifyNewEditAndDeleteButtonExist()
        {
            Specify.That(Retry.For(() => NewButton.Exists, TimeSpan.FromSeconds(10))).Should.BeTrue();
            Specify.That(Retry.For(() => EditButton.Exists, TimeSpan.FromSeconds(10))).Should.BeTrue();
            Specify.That(Retry.For(() => DeleteButton.Exists, TimeSpan.FromSeconds(10))).Should.BeTrue();
        }
        #endregion
    }
}
