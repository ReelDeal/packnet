﻿using System;
using System.Linq;
using System.Globalization;

using Microsoft.VisualStudio.TestTools.UITesting;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.Macros;
using System.Collections.Generic;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ArticlesManagementPage : CrudPageBase
    {
        #region Page controls

        // page title and widget
        private AlertWidget AlertWidget { get { return Get<AlertWidget>("id=alert"); } }

        // list
        private CUITe_HtmlButton NewSubArticleButton { get { return Get<CUITe_HtmlButton>("id=newArticleDropDown"); } }
        private CUITe_HtmlButton ArticleNewButton { get { return Get<CUITe_HtmlButton>("id=newArticle"); } }
        private CUITe_HtmlHyperlink NewLabelButton { get { return Get<CUITe_HtmlHyperlink>("id=newLabel"); } }
        private CUITe_HtmlHyperlink NewCartonButton { get { return Get<CUITe_HtmlHyperlink>("id=newCarton"); } }
        private CUITe_HtmlButton DeleteArticleButton { get { return Get<CUITe_HtmlButton>("id=deleteArticles"); } }
        private CUITe_HtmlButton EditArticleButton { get { return Get<CUITe_HtmlButton>("id=editArticle"); } }
        private CUITe_HtmlButton CopyArticleButton { get { return Get<CUITe_HtmlButton>("id=copyArticle"); } }
        private CUITe_HtmlButton ExportButton { get { return Get<CUITe_HtmlButton>("id=exportArticles"); } }
        private CUITe_HtmlButton ImportButton { get { return Get<CUITe_HtmlButton>("id=importArticles"); } }
        private CUITe_HtmlButton ProduceButton { get { return Get<CUITe_HtmlButton>("id=produceArticle"); } }
        
        Table _articleTable = null;
        public Table ArticlesTable
        {
            get
            {
                var div = Get<CUITe_HtmlDiv>("id=articlesList");
                return _articleTable ?? (_articleTable = new Table(div, new TableConfigData(TableTypes.TwoColumn, "Article")));
            }
        }

        // article
        private CUITe_HtmlEdit ArticleNameEdit { get { return Get<CUITe_HtmlEdit>("id=kitAlias"); } }
        private CUITe_HtmlTextArea ArticleDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=kitDescription"); } }
        private CUITe_HtmlButton ArticleSaveButton { get { return Get<CUITe_HtmlButton>("id=kitSaveButton"); } }
        private CUITe_HtmlButton ArticleCancelButton { get { return Get<CUITe_HtmlButton>("id=kitCancelButton"); } }
        private CUITe_HtmlButton ArticleDeleteConfirmButton { get { return Get<CUITe_HtmlButton>("id=deleteArticleButtonConfirm"); } }
        private CUITe_HtmlButton ArticleDeleteCancelButton { get { return Get<CUITe_HtmlButton>("id=deleteArticleButtonCancel"); } }

        // label
        private CUITe_HtmlEdit LabelNameEdit { get { return Get<CUITe_HtmlEdit>("id=labelAlias"); } }
        private CUITe_HtmlTextArea LabelDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=labelDescription"); } }
        private CUITe_HtmlEdit LabelQuantityEdit { get { return Get<CUITe_HtmlEdit>("id=labelQuantity"); } }
        private CUITe_HtmlComboBox LabelTemplateList { get { return Get<CUITe_HtmlComboBox>("id=labelTemplate"); } }
        private CUITe_HtmlButton LabelSaveButton { get { return Get<CUITe_HtmlButton>("id=labelSaveButton"); } }
        private CUITe_HtmlButton LabelCancelButton { get { return Get<CUITe_HtmlButton>("id=labelCancelButton"); } }
        public CUITe_HtmlEdit GetLabelVariableInputByNumber(int row)
        {
            return Get<CUITe_HtmlEdit>(string.Format("id=variable{0}", row));
        }

        //carton
        private CUITe_HtmlEdit CartonNameEdit { get { return Get<CUITe_HtmlEdit>("id=alias"); } }
        private CUITe_HtmlTextArea CartonDescriptionEdit { get { return Get<CUITe_HtmlTextArea>("id=description"); } }
        private CUITe_HtmlEdit CartonQuantityEdit { get { return Get<CUITe_HtmlEdit>("id=quantity"); } }
        private CUITe_HtmlComboBox CartonDesignComboBox { get { return Get<CUITe_HtmlComboBox>("id=design"); } }
        private CUITe_HtmlEdit CartonCorrugateQualityEdit { get { return Get<CUITe_HtmlEdit>("id=CorrugateQuality"); } }
        private CUITe_HtmlEdit CartonLengthEdit { get { return Get<CUITe_HtmlEdit>("id=Length"); } }
        private CUITe_HtmlEdit CartonWidthEdit { get { return Get<CUITe_HtmlEdit>("id=Width"); } }
        private CUITe_HtmlEdit CartonHeightEdit { get { return Get<CUITe_HtmlEdit>("id=Height"); } }
        private CUITe_HtmlDiv CartonPreviewContainer { get { return Get<CUITe_HtmlDiv>("id=previewContainer"); } }
        private CUITe_HtmlHyperlink CartonToggleDesignPreviewButton { get { return Get<CUITe_HtmlHyperlink>("id=toggleDesignPreviewContainer"); } }
        private CUITe_HtmlButton CartonSaveButton { get { return Get<CUITe_HtmlButton>("id=cartonSaveButton"); } }
        private CUITe_HtmlButton CartonCancelButton { get { return Get<CUITe_HtmlButton>("id=cartonCancelButton"); } }

        #endregion

        // methods
        public void RefreshTables()
        {
            _articleTable = null;
        }
        public static ArticlesManagementPage GetWindow()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<ArticlesManagementPage>();
        }
        public void PageTableSelectRow(int rowIndex)
        {
            ArticlesTable.SelectRow(rowIndex);
        }

        // button actions
        // article
        public void ClickSaveArticleButton()
        {
            ArticleSaveButton.Click();
        }
        public void ClickCancelArticleButton()
        {
            ArticleCancelButton.Click();
        }
        public void ClickNewArticleButton()
        {
            ArticleNewButton.Click();
        }
        public void ClickEditArticleButton()
        {
            EditArticleButton.SafeClick();
        }
        public void ClickProduceButton()
        {
            ProduceButton.SafeClick();
        }
        public void ClickCopyButton()
        {
            CopyArticleButton.SafeClick();
        }
        public void ClickImportButton()
        {
            ImportButton.SafeClick();
        }
        public void ClickExportButton()
        {
            ExportButton.SafeClick();
            Specify.That(ExportButton.InnerText == "Exporting...").Should.BeTrue("The export button's text should be changed to Exporting...");
            Specify.That(AlertWidget.InnerText.Contains("Export started. This may take a while. Please do not leave this page until the export is complete.")).Should.BeTrue("Export alert is wrong");
            
        }
        public void ClickDescriptionField()
        {
            ArticleDescriptionEdit.SafeClick();
        }
        public void ClickDeleteSubArticleButton()
        {
            DeleteArticleButton.Click();
        }
        public void ClickDeleteArticleButton(bool confirm = true)
        {
            DeleteArticleButton.Click();
            Playback.Wait(500);
            if (confirm)
                ClickDeleteArticleConfirmButton();
            else
                ClickDeleteArticleCancelButton();
        }
        public void ClickDeleteArticleConfirmButton()
        {
            ArticleDeleteConfirmButton.Click();
        }
        public void ClickDeleteArticleCancelButton()
        {
            ArticleDeleteCancelButton.Click();
        }
        // carton
        public void ClickNewCartonButton()
        {
            NewCartonButton.Click();
        }
        public void ClickNewSubArticleButton()
        {
            NewSubArticleButton.Click();
        }
        public void ClickSaveCartonButton()
        {
            Specify.That(CartonSaveButton.Enabled).Should.BeTrue("Carton Save button is not available");
            CartonSaveButton.SafeClick();
        }
        public void ClickCancelCartonButton(bool doubleClick = false)
        {
            CartonCancelButton.SafeClick();
            if(doubleClick)
                CartonCancelButton.SafeClick();
        }
        public void ClickToggleDesignPreviewButton()
        {
            CartonToggleDesignPreviewButton.Click();
        }

        // label
        public void ClickNewLabelButton()
        {
            NewLabelButton.Click();
        }
        public void ClickSaveLabelButton()
        {
            Specify.That(LabelSaveButton.Enabled).Should.BeTrue("Label Save button is not available");
            LabelSaveButton.SafeClick();
        }
        public void ClickCancelLabelButton(bool doubleClick = false)
        {
            LabelCancelButton.SafeClick();
            if (doubleClick)
                LabelCancelButton.SafeClick();
        }

        // assign values to form fields
        // article
        public void SetArticleName(string name)
        {
            if (this.ArticleNameEdit.Enabled)
                this.ArticleNameEdit.SetText(name);
        }
        public void SetArticleDescription(string description)
        {
            this.ArticleDescriptionEdit.SetText(description);
        }
        // label
        public void SetLabelName(string name)
        {
            LabelNameEdit.SafeSetText(name);
        }
        public void SetLabelDescription(string description)
        {
            LabelDescriptionEdit.SetText(description);
        }
        public void SetLabelQuantity(int quantity)
        {
            LabelQuantityEdit.SetText(quantity.ToString());
        }
        // carton
        public void SetCartonName(string name)
        {
            this.CartonNameEdit.SetText(name);
        }
        public void SetCartonDesign(string design)
        {
            if (!TestUtilities.CheckIfValueExistsInComboBox(CartonDesignComboBox, design))
                Specify.Failure(string.Format("There is no design in the design menu called '{0}'.", design));

            CartonDesignComboBox.SelectItem(design);
        }
        public void SetCartonDescription(string description)
        {
            this.CartonDescriptionEdit.SetText(description);
        }
        public void SetCartonQuantity(int quantity)
        {
            this.CartonQuantityEdit.SetText(quantity.ToString());
        }
        public void SetCartonWidth(double width)
        {
            this.CartonWidthEdit.SetText(width.ToString());
        }
        public void SetCartonHeight(double height)
        {
            this.CartonHeightEdit.SetText(height.ToString());
        }
        public void SetCartonLength(double length)
        {
            this.CartonLengthEdit.SetText(length.ToString());
        }
        public void SetCartonCorrugateQuality(int? quality)
        {
            if (quality != null)
                this.CartonCorrugateQualityEdit.SetText(quality.ToString());
            else
                this.CartonCorrugateQualityEdit.SetText("");
        }

        // crud

        /// <summary>
        /// Create an article. In contrary to other create methods this does not save, since an article
        /// must have subarticles (cartons, labels) added before it can be saved
        /// </summary>
        /// <param name="article">The article object to saved</param>
        /// <param name="adminConsole"></param>
        public void CreateArticle(ArticleExt article, AdminConsole adminConsole = null)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);
            var page = new ArticlesManagementPage();
            adminConsole.NavigateToPage(NavPages.Article);
            ClickNewArticleButton();
            EnterArticleInfo(article);
        }
        public void EnterArticleInfo(ArticleExt article)
        {
            SetArticleName(article.ArticleId);
            SetArticleDescription(article.Description);
        }
        public void UpdateArticleInfo(int rowIndex, ArticleExt article, bool save = true)
        {
            PageTableSelectRow(rowIndex);
            ClickEditArticleButton();
            EnterArticleInfo(article);
            if (save)
                ClickSaveArticleButton();
        }
        public void CreateCarton(Carton carton, int quantity = 1, string[] variableList = null)
        {
            ClickNewSubArticleButton();
            ClickNewCartonButton();
            EnterCartonInfo(carton, quantity, variableList);
            ClickSaveCartonButton();
        }
        public void UpdateCartonInfo(int rowIndex, Carton carton, int quantity = 1, string[] variableList = null)
        {
            PageTableSelectRow(rowIndex);
            EditArticleButton.SafeClick();
            EnterCartonInfo(carton, quantity, variableList);
            ClickSaveCartonButton();
        }
        public void EnterCartonInfo(Carton carton, int quantity = 1, string[] variableList = null)
        {
            var designName = DesignHelper.GetDesignNameFromId(carton.DesignId);

            SetCartonName(carton.CustomerUniqueId);
            SetCartonDescription(carton.CustomerDescription);
            SetCartonQuantity(quantity);
            SetCartonDesign(designName);
            SetCartonLength(carton.Length);
            SetCartonWidth(carton.Width);
            SetCartonHeight(carton.Height);

            if (variableList != null)
            {
                for (int i = 0; i < variableList.Count(); i++)
                {
                    GetLabelVariableInputByNumber(i).SetText(variableList[i]);
                }
            }

            //set quality restriction
            if (carton.CorrugateQuality.HasValue)
                SetCartonCorrugateQuality(carton.CorrugateQuality);
        }
        public void CreateLabel(LabelExt label, int quantity = 1, string[] variableList = null)
        {
            ClickNewSubArticleButton();
            ClickNewLabelButton();
            EnterLabelInfo(label, quantity, variableList);
            ClickSaveLabelButton();
        }
        public void UpdateLabelInfo(int rowIndex, LabelExt label, int quantity = 1, string[] variableList = null)
        {
            PageTableSelectRow(rowIndex);
            ClickEditArticleButton();
            EnterLabelInfo(label, quantity, variableList);
            ClickSaveLabelButton();
        }
        public void EnterLabelInfo(LabelExt label, int quantity = 1, string[] variableList = null)
        {
            SetLabelName(label.Alias);
            SetLabelDescription(label.CustomerDescription);
            SetLabelQuantity(quantity);

            var items = LabelTemplateList.GetChildren().OfType<CUITe_HtmlCustom>().Select(i => i.InnerText).ToList();
            Specify.That(items.Any()).Should.BeTrue("No designs found");

            if (items.Any(i => i.Contains(label.Template)))
                LabelTemplateList.SelectItem(items.First(i => i.Contains(label.Template)));
            else
                LabelTemplateList.SelectItem(1);

            if (variableList != null)
            {
                for (int i = 0; i < variableList.Count(); i++)
                {
                    GetLabelVariableInputByNumber(i).SetText(variableList[i]);
                }
            }
        }

        #region Page Validation
        public void VerifyCartonInGrid(Carton carton, int quantity, string[] variableList = null)
        {
            Specify.That(ArticlesTable.ContainsValue(carton.CustomerDescription));
            Specify.That(ArticlesTable.ContainsValue(carton.CustomerUniqueId));
            Specify.That(ArticlesTable.ContainsValue("Carton"));
        }
        public void VerifyLabelInGrid(LabelExt label, int quantity, string[] variableList = null)
        {
            Specify.That(ArticlesTable.ContainsValue(label.Alias));
            Specify.That(ArticlesTable.ContainsValue(label.CustomerDescription));
            Specify.That(ArticlesTable.ContainsValue(quantity));
            Specify.That(ArticlesTable.ContainsValue(label.Alias));
            Specify.That(ArticlesTable.ContainsValue("Label"));
        }
        public void VerifyArticle(ArticleExt article, Carton carton, int cartonQuantity, LabelExt label, int labelQuantity)
        {
            Specify.That(ArticlesTable.ContainsValue(article.ArticleId));
            Specify.That(ArticlesTable.ContainsValue(article.Description));

            var kit1FromDatabase = ArticleMacros.VerifyArticleInDatabase(article.ArticleId, article.Description);

            // article
            Kit articleFromDatabase = (Kit)kit1FromDatabase;
            Specify.That(articleFromDatabase.CustomerDescription == article.Description);
            Specify.That(articleFromDatabase.CustomerUniqueId == article.ArticleId);

            foreach (var producible in articleFromDatabase.ItemsToProduce)
            {
                Order order = (Order)producible;

                if (producible.ProducibleType == ProducibleTypes.Carton)
                {
                    Specify.That(order.OriginalQuantity == cartonQuantity);
                    ArticleMacros.VerifyCartonInDatabase((Carton)order.Producible, carton);
                }
                else if (producible.ProducibleType == ProducibleTypes.Label)
                {
                    Specify.That(order.OriginalQuantity == labelQuantity);
                    ArticleMacros.VerifyLabelInDatabase((Label)order.Producible, label);
                }
            }
        }
        /// <summary>
        /// Verify that the show/hide Preview is shown as expected
        /// </summary>
        /// <param name="page"></param>
        public void VerifyCartonPreview(Carton carton)
        {
            string showDesign = "Show Package Design Preview";
            string hideDesign = "Hide Package Design Preview";

            Specify.That(CartonToggleDesignPreviewButton.InnerText == hideDesign).Should.BeTrue("Toggle button text should be '" + showDesign + "'");
            SetCartonDesign(DesignHelper.GetDesignNameFromId(carton.DesignId));
            Specify.That(CartonToggleDesignPreviewButton.InnerText == hideDesign).Should.BeTrue("Toggle button text should be '" + showDesign + "'");
            SetCartonHeight(carton.Length);
            Specify.That(CartonToggleDesignPreviewButton.InnerText == hideDesign).Should.BeTrue("Toggle button text should be '" + showDesign + "'");
            SetCartonWidth(carton.Width);
            Specify.That(CartonToggleDesignPreviewButton.InnerText == hideDesign).Should.BeTrue("Toggle button text should be '" + showDesign + "'");
            SetCartonHeight(carton.Height);
            Specify.That(CartonToggleDesignPreviewButton.InnerText == hideDesign).Should.BeTrue("Toggle button text should be '" + showDesign + "'");

            ClickToggleDesignPreviewButton();
            Specify.That(CartonToggleDesignPreviewButton.InnerText == hideDesign).Should.BeFalse("Toggle button text should be '" + hideDesign + "'");
            ClickToggleDesignPreviewButton();
            Specify.That(CartonToggleDesignPreviewButton.InnerText == hideDesign).Should.BeTrue("Toggle button text should be '" + showDesign + "'");
        }
        public void VerifySubArticleRequiredMessage(string articleAlias)
        {
            TestUtilities.VerifyAlert(this.AlertWidget, null, "Article Must Contain At Least One Item", string.Format("There are no subarticles for the current article '{0}'.", articleAlias), true);
        }
        public void VerifyTableIsEmpty(int remainingNumberOfRecords = 0)
        {
            base.VerifyTableIsEmpty(ArticlesTable, "Articles", remainingNumberOfRecords);
        }

        #endregion
    }
}
