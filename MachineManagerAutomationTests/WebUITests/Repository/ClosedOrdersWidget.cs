﻿using System;
using System.Collections.Generic;

using CUITe.Controls.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class ClosedOrdersWidget: DynamicBrowserWindowWithLogin
    {
        private readonly bool _isAuto;

        public ClosedOrdersWidget(bool isAuto = true) : base("")
        {
            _isAuto = isAuto;
        }

        public CUITe_HtmlDiv ClosedOrdersDiv { get { return Get<CUITe_HtmlDiv>("id=ordersHistory"); } }
        public CUITe_HtmlEdit Filter { get { return Get<CUITe_HtmlEdit>("id=orderFilter"); } }
        public CUITe_HtmlButton SortByDiv { get { return Get<CUITe_HtmlButton>("id=ordersHistorySortyBy"); } }
        public CUITe_HtmlEdit ReproduceOrderEdit { get { return Get<CUITe_HtmlDiv>("id=ReproduceQuantityNudDiv").Get<CUITe_HtmlEdit>(); } }
        public CUITe_HtmlButton ReproduceOrderUpButton { get { return Get<CUITe_HtmlDiv>("id=ReproduceQuantityNudDiv").Get<CUITe_HtmlButton>("id=nudIncrementButton"); } }
        public CUITe_HtmlButton ReproduceOrderDownButton { get { return Get<CUITe_HtmlDiv>("id=ReproduceQuantityNudDiv").Get<CUITe_HtmlButton>("id=nudDecrementButton"); } }
        public CUITe_HtmlButton ReproduceOrderSaveButton { get { return Get<CUITe_HtmlButton>("id=ReproduceItemsSave"); } }
        public CUITe_HtmlButton ReproduceOrderCloseButton { get { return Get<CUITe_HtmlButton>("id=ReproduceItemsClose"); } }
        private CUITe_HtmlDiv AutoOperatorPanelCompletedJobsDiv { get { return Get<CUITe_HtmlDiv>("id=autoOperatorPanelCompletedJobs"); } }
        private CUITe_HtmlDiv ManualOperatorPanelCompletedJobsDiv { get { return Get<CUITe_HtmlDiv>("id=manualOperatorPanelCompletedJobs"); } }
        public CUITe_HtmlButton ReproduceScanToCreateOKButton { get { return Get<CUITe_HtmlButton>("id=reproduce_okBtn"); } }

        public CUITe_HtmlDiv CompletedJobsDiv
        {
            get { return _isAuto ? AutoOperatorPanelCompletedJobsDiv : ManualOperatorPanelCompletedJobsDiv; }
        }

        public CUITe_HtmlButton GetReproduceButtonForOrder(string orderName)  {
            return GetOrderDiv(orderName).Get<CUITe_HtmlButton>("id=orderReproduce");
        }

        public CUITe_HtmlDiv GetScanToCreateDiv(string orderName)
        {
            return AutoOperatorPanelCompletedJobsDiv.Get<CUITe_HtmlDiv>(string.Format("InnerText={0}", orderName));
        }

        public CUITe_HtmlDiv GetOrderDiv(string orderName)
        {
            return CompletedJobsDiv.Get<CUITe_HtmlDiv>(string.Format("InnerText={0}", orderName));
        }

        public bool IsOrderPresent(string orderName)
        {
            try
            {
                return GetOrderDiv(orderName).Exists;
            }
            catch
            {
                return false;
            }
        }
    }
}
