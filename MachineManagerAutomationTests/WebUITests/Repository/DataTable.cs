﻿using System.Collections.Generic;
using System.Linq;

namespace MachineManagerAutomationTests.WebUITests.Repository
{
    public class DataTable
    {
        public List<DataRow> Rows { get; set; }
        public string GetCellValue(int rowIndex, int columnIndex)
        {
            var row = Rows[rowIndex];
            if (row != null)
            {
                if (row.Columns.Count > 0)
                {
                    return row.Columns[columnIndex].Value.Trim();
                }
            }
            return null;
        }

        public DataTable()
        {
            Rows = new List<DataRow>();
        }

        public bool ValueExistsInTable(string value)
        {
            return this.Rows.SelectMany(r => r.Columns).Any(c => c.Value.Equals(value));
        }

        public DataRow GetRowWithValue(string value, int columnIndex)
        {
            foreach (var row in Rows)
            {
                if(row.Columns[columnIndex].Value.Equals(value))
                {
                    return row;
                }
            }
            return null;
        }
        public int GetRowIndexWithValue(string value, int columnIndex)
        {
            var idx = 0;
            bool itemFound = false;
            foreach (var row in Rows)
            {
                if (row.Columns[columnIndex].Value.Equals(value))
                {
                    itemFound = true;
                    break;
                }
                idx++;
            }

            if (itemFound)
                return idx;
            else
                return -1;
        }
    }
    public class DataRow
    {
        public List<DataColumn> Columns { get; set; }

        public DataRow()
        {
            Columns = new List<DataColumn>();
        }

        public override string ToString()
        {
            return "Columns: " + Columns.Count;
        }

    }
    public class DataColumn
    {
        public string Value { get; set; }

        public override string ToString()
        {
            return Value;
        }
    }
}
