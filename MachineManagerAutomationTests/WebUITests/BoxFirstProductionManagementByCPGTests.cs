﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Enums;
using PackNet.Data.Machines;
using PackNet.FusionSim.ViewModels;

using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests
{

    using PackNet.Common.Utils;

    [CodedUITest]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    [DeploymentItem("NLog.config")]
    public class BoxFirstProductionManagementByCPGTests : ExternalApplicationTestBase
    {
        #region Variables
        SimulatorUtilities simUtilities;
        #endregion

        #region Constructor and initiators
        public BoxFirstProductionManagementByCPGTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        {
        }

        protected override void TestInitializeBeforeStartProcess()
        {
            //The database snapshot should created by SetupBoxFirstProductionManagementTestingEnviroment from BoxFirstProductionManagementByClassificationtests
            TestDatabaseManagement.DropDatabase();
            simUtilities = new SimulatorUtilities();
        }

        protected override void AfterTestInitialize()
        {
            //Give the webserver some more time to start up
            Thread.Sleep(1000);
        }

        protected override void AfterTestCleanup()
        {
            simUtilities.Close();
            Thread.Sleep(1000);
            base.AfterTestCleanup();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void SetupBoxFirstEnviroment()
        {
            BoxFirstSearchTest.SetupBoxFirstEnvironment();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("143")]
        [TestCategory("8079")]
        public void BoxFirst_AddACartonPropertyGroupToProductionGroup_WillTriggerTheJobsInThatCPG()
        {

            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var pgPage = new ProductionGroupPage();

            ToggleCpgCheckBoxInProductionGroupPage(pgPage,
                new List<string>
                {
                    TestDatabaseManagement.Cpg1.Alias,
                    TestDatabaseManagement.Cpg2.Alias
                });

            adminConsole.NavigateToPage(NavPages.Dashboard);

            DropFileUtilities.DropBoxFirstDropFile(2);

            var widget = new CartonPropertyGroupsWidget();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            adminConsole.AddWidgetToDashboard(DashboardWidgets.CartonPropertyGroups);

            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg1, CpgWidgetListColumn.Requests, 1);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg2, CpgWidgetListColumn.Requests, 1);

            adminConsole.ClickExpandCollapseNotificationsButton();

            adminConsole.VerifyNotificationContainsValue(1, "no available producibles");

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            ToggleCpgCheckBoxInProductionGroupPage(pgPage,
                new List<string>
                {
                    TestDatabaseManagement.Cpg1.Alias
                });

            var page = MachineProductionPage.GetWindow();

            page.ItemsInProductionTable.VerifyCellValue(0, 1, 0);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            Retry.For(
                () => Specify.That(page.CompletedItemsTable.GetCellValue(0, 2)).Should.BeEqualTo("0"),
                TimeSpan.FromSeconds(10));

            Thread.Sleep(1000);

            ToggleCpgCheckBoxInProductionGroupPage(pgPage, new List<string>() { TestDatabaseManagement.Cpg2.Alias });

            adminConsole.NavigateToPage(NavPages.Dashboard);

            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg1, CpgWidgetListColumn.Requests, 0);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg2, CpgWidgetListColumn.Requests, 1);

            page.ItemsInProductionTable.VerifyCellValue(0, 1, 1);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            page.CompletedItemsTable.VerifyCellValue(0, 2, 1);

            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg1, CpgWidgetListColumn.Requests, 0);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg2, CpgWidgetListColumn.Requests, 0);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8085")]
        public void BoxFirst_JobsInStoppedCPGWillBeSuspended()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            DropFileUtilities.DropBoxFirstDropFile(2);

            var widget = new CartonPropertyGroupsWidget();
            adminConsole.AddWidgetToDashboard(DashboardWidgets.CartonPropertyGroups, DashboardLayouts.Row1_1Col_Row2_3Cols);

            widget.SetStatus(TestDatabaseManagement.Cpg1.Alias, CartonPropertyGroupStatuses.Stop);
            widget.SetStatus(TestDatabaseManagement.Cpg2.Alias, CartonPropertyGroupStatuses.Stop);

            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg1, CpgWidgetListColumn.Requests, 1);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Cpg2, CpgWidgetListColumn.Requests, 1);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            adminConsole.ClickExpandCollapseNotificationsButton();

            adminConsole.VerifyNotificationContainsValue(2, "no available producibles");

            widget.SetStatus(TestDatabaseManagement.Cpg1.Alias, CartonPropertyGroupStatuses.Normal);

            var page = MachineProductionPage.GetWindow();

            page.ItemsInProductionTable.VerifyCellValue(0, 1, 0);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            page.CompletedItemsTable.VerifyCellValue(0, 2, 0);

            page.ItemsInProductionTable.VerifyTableHasData(false);

            widget.SetStatus(TestDatabaseManagement.Cpg2.Alias, CartonPropertyGroupStatuses.Normal);

            page.ItemsInProductionTable.VerifyTableHasData();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8068")]
        public void BoxFirst_CPGStatusWillGoBackToNormalWhenSurgeCountIsReached()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            DropFileUtilities.DropBoxFirstDropFile(10);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.AddWidgetToDashboard(DashboardWidgets.CartonPropertyGroups, DashboardLayouts.Row1_1Col_Row2_3Cols);

            var widget = new CartonPropertyGroupsWidget();
            widget.SetStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge);
            widget.SetStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Normal);

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var pgPage = new ProductionGroupPage();
            SetCpgSurgeCount(pgPage, 3);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            var machineProdPage = MachineProductionPage.GetWindow();
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            machineProdPage.CompletedItemsTable.VerifyCellValue(0, 1, 4);
            machineProdPage.CompletedItemsTable.VerifyCellValue(0, 2, 2);
            machineProdPage.CompletedItemsTable.VerifyCellValue(1, 2, 0);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            Assert.Inconclusive("Connected with bug 9141");
            Specify.That(machineProdPage.ItemsInProductionTable.GetCellIntValue(0, 1)).Should.BeEqualTo(1);

            adminConsole.NavigateToPage(NavPages.Dashboard);
            widget.VerifyStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge, false);
            widget.VerifyStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Normal, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8078")]
        public void BoxFirst_CPGStateWillStillInSurge_WhenJobsInThatCPGAreFinished_ButSurgeCountNotReached()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            DropFileUtilities.DropBoxFirstDropFile(6);

            adminConsole.AddWidgetToDashboard(DashboardWidgets.CartonPropertyGroups, DashboardLayouts.Row1_1Col_Row2_3Cols);

            var widget = new CartonPropertyGroupsWidget();

            widget.SetStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            var page = MachineProductionPage.GetWindow();

            page.CompletedItemsTable.VerifyCellValue(0, 2, 4);
            page.CompletedItemsTable.VerifyCellValue(1, 2, 2);
            page.CompletedItemsTable.VerifyCellValue(2, 2, 0);

            page.ItemsInProductionTable.VerifyCellValue(0, 1, 1);

            adminConsole.NavigateToPage(NavPages.Dashboard);
            widget.VerifyStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Normal, false);
            widget.VerifyStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge, false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8075")]
        public void BoxFirst_CPGProductionWillGoBackToNormal_WhenChangeItsStatusFromSurgeToNormal()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var widget = new CartonPropertyGroupsWidget();

            DropFileUtilities.DropBoxFirstDropFile(20);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.AddWidgetToDashboard(DashboardWidgets.CartonPropertyGroups, DashboardLayouts.Row1_1Col_Row2_3Cols);

            // by default, surge count is 10
            widget.SetStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            widget.SetStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Normal);
            widget.VerifyStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge, false);

            //At this state, one job from AZ1 is sent to machine, one job from AZ1 is sent to machine group.
            //And one job from AZ1 is selected. So three more jobs from AZ1 will be produced.
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            var machineProdPage = MachineProductionPage.GetWindow();

            machineProdPage.CompletedItemsTable.VerifyCellValue(0, 2, 6);
            machineProdPage.CompletedItemsTable.VerifyCellValue(1, 2, 4);
            machineProdPage.CompletedItemsTable.VerifyCellValue(2, 2, 2);
            machineProdPage.CompletedItemsTable.VerifyCellValue(3, 2, 0);
            machineProdPage.ItemsInProductionTable.VerifyCellValue(3, 2, 1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8089")]
        public void BoxFirst_CPGStatusWillJumpToNormalWhenSurgeCountIsReachedAndThatCPGWasSurgedFromStop()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            DropFileUtilities.DropBoxFirstDropFile(10);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var widget = new CartonPropertyGroupsWidget();

            adminConsole.AddWidgetToDashboard(DashboardWidgets.CartonPropertyGroups, DashboardLayouts.Row1_1Col_Row2_3Cols);

            widget.SetStatus(TestDatabaseManagement.Cpg1.Alias, CartonPropertyGroupStatuses.Stop);

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var pgPage = new ProductionGroupPage();

            SetCpgSurgeCount(pgPage, 2);

            adminConsole.NavigateToPage(NavPages.Dashboard);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            var machineProdPage = MachineProductionPage.GetWindow();

            // check state before production starts
            machineProdPage.ItemsInProductionTable.VerifyCellValue(0, 1, 1);

            widget.SetStatus(TestDatabaseManagement.Cpg1.Alias, CartonPropertyGroupStatuses.Surge);

            // produce 3 jobs, check state in CompletedItemsTable, in widget and in ItemsInProduction
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            machineProdPage.CompletedItemsTable.VerifyCellValue(0, 2, 5);
            machineProdPage.CompletedItemsTable.VerifyCellValue(1, 2, 3);
            machineProdPage.CompletedItemsTable.VerifyCellValue(2, 2, 1);

            widget.VerifyStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge, false);
            widget.VerifyStatus(TestDatabaseManagement.Cpg1, CartonPropertyGroupStatuses.Surge, true);

            machineProdPage.ItemsInProductionTable.VerifyCellValue(0, 1, 0);

            // produce more
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            // check state
            machineProdPage.ItemsInProductionTable.VerifyCellValue(0, 1, 2);

            // produce more
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            // check state
            machineProdPage.ItemsInProductionTable.VerifyCellValue(0, 1, 7);
        }
        #endregion

        #region Helper methods
        private void ToggleCpgCheckBoxInProductionGroupPage(ProductionGroupPage page, List<string> cPGs)
        {
            page.ProductionGroupTable.SelectRowWithValue(TestDatabaseManagement.Pg1_orders_mg1_c2755.Alias, 0);
            page.ClickEditButton();
            page.ToggleCartonPropertyGroupCheckboxes(cPGs);
            page.ClickSaveButton();
            page.VerifyEditPageIsOpen(false);
        }

        private void SetCpgSurgeCount(ProductionGroupPage page, int count)
        {
            page.ProductionGroupTable.VerifyCellValue(0, 0, TestDatabaseManagement.Pg1_orders_mg1_c2755.Alias);
            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.SetCpgSurgeCount(count);
            page.ClickSaveButton();
        }

        #endregion
    }
}