﻿using System;
using System.Collections.Generic;
using CUITe.Controls.HtmlControls;
using MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Constants;
using MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy
{
    public abstract class AbstractIdBasedChecker
    {
        List<string> _htmlElementsWithIdsChecked;

        public List<string> HtmlElementsWithIdsChecked
        {
            get
            {
                if (_htmlElementsWithIdsChecked == null || _htmlElementsWithIdsChecked.Count == 0)
                {
                    _htmlElementsWithIdsChecked = new List<string>();
                }

                return _htmlElementsWithIdsChecked;
            }
        }

        public abstract List<IdBasedControlsTranslationMap> IdBasedControlsTranslationMap { get; }
        public abstract CUITe_DynamicBrowserWindow Page { get; set; }

        /// <summary>
        /// Either we find this by a pattern or we mark this abstract so children implement this if the html element ids are radically different
        /// </summary>
        /// <returns></returns>
        public virtual string GetHeaderInnerText()
        {
            return null;
        }

        /// <summary>
        /// Either we find this by a pattern or we mark this abstract so children implement this if the html element ids are radically different
        /// </summary>
        /// <returns></returns>
        public string GetHeaderTitle()
        {
            return null;
        }

        /// <summary>
        /// Either we find this by a pattern or we mark this abstract so children implement this if the html element ids are radically different
        /// </summary>
        /// <returns></returns>
        public string GetAlertBannerInnerText()
        {
            return null;
        }

        /// <summary>
        /// Either we find this by a pattern or we mark this abstract so children implement this if the html element ids are radically different
        /// </summary>
        /// <returns></returns>
        public string GetAlertBannerTitle()
        {
            return null;
        }

        /// <summary>
        /// Given &lt;h1 id="myGreeting" title="Greetings"&gt;Hello there&lt;/h1&gt;
        /// If passed myGreeting and title, "Greetings" would be returned - we may need to do this with RegEx parsing the HTML if CUITe can't handle it.
        /// </summary>
        /// <param name="htmlElementId"></param>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public string GetTitleAttributeFromElement(string htmlElementId, string attributeName)
        {
            return null;
        }

        /// <summary>
        /// Broadbrush method for getting everything for those times when you just need to check if something is there. Hopefully CUITe can give this to us.
        /// </summary>
        public string GetAllHtmlInPage()
        {
            return null;
        }

        private void RunLocalizationTestsPreCondition()
        {
            if (IdBasedControlsTranslationMap == null)
            {
                throw new InvalidOperationException("IdBasedControlsTranslationMap in the derived class was null. Test cannot continue.");
            }

            if (IdBasedControlsTranslationMap.Count == 0)
            {
                throw new InvalidOperationException("IdBasedControlsTranslationMap in the derived class was empty. Test cannot continue.");
            }
        }

        internal List<IdBasedLocalizationTestResult> RunLocalizationTests()
        {
            RunLocalizationTestsPreCondition();
            List<IdBasedLocalizationTestResult> finalResults = new List<IdBasedLocalizationTestResult>();

            foreach (IdBasedControlsTranslationMap translationMap in IdBasedControlsTranslationMap)
            {
                IdBasedLocalizationTestResult result = new IdBasedLocalizationTestResult(translationMap);
                //var controlElement = LocateTheControl<CUITe_HtmlControl>(Page, translationMap.HtmlElementId);
                HtmlElementsWithIdsChecked.Add(translationMap.HtmlElementId);
                //result.ControlWasLocated = controlElement != null && controlElement.Exists;
                //TODO: Check that our expected type from dictionary matches the type coming back from CUITe
                //TODO: Grab out the stuff we are looking for from CUITe and assign it
                
                //TODO: Figure this out!
                translationMap.CuiteControlInstance = Page.Get<CUITe_HtmlControl>(string.Format("id={0}", translationMap.HtmlElementId));
                
                result.ControlWasLocated = translationMap.CuiteControlInstance != null && translationMap.CuiteControlInstance.Exists;
                result.ActualText = CuitePropertyReader.GetInnerText<CUITe_HtmlControl>(translationMap.CuiteControlInstance);
                //TODO: Get ExpectedText from the JSON node
                result.ExpectedText = "";
                //Assign result for that element - we can aggregate it later.
                result.TestStatus = result.ActualText.Equals(result.ExpectedText, StringComparison.CurrentCulture) ? TestStatus.Pass : TestStatus.Fail;
                finalResults.Add(result);
            }

            return finalResults;
        }
    }
}