﻿using System.Collections.Generic;
using CUITe.Controls.HtmlControls;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.ConcreteIdBasedCheckers
{
    public class MachinesFusionNewFusionMachineIdBasedChecker : AbstractIdBasedChecker
    {
        private static volatile object threadLocker = new object();
        private List<IdBasedControlsTranslationMap> _idBasedControlsTranslationMap;
        
        public override CUITe_DynamicBrowserWindow Page { get; set; }

        public override List<IdBasedControlsTranslationMap> IdBasedControlsTranslationMap
        {
            get
            {
                lock (threadLocker)
                {
                    if (_idBasedControlsTranslationMap != null && _idBasedControlsTranslationMap.Count > 0)
                    {
                        return _idBasedControlsTranslationMap;
                    }

                    _idBasedControlsTranslationMap = new List<IdBasedControlsTranslationMap>
                    {
                        new IdBasedControlsTranslationMap
                        {
                            HtmlElementId = "newMachineAliasLabel",
                            CuiteControlType = typeof(CUITe_HtmlLabel),
                            TranslationFileNodeStructure = "Machines.Admin.Edit.FusionMachineName"
                        }
                        //new IdBasedControlsTranslationMap
                        //{
                        //    HtmlElementId = "Machines.Admin.Edit.DescriptionLabel",
                        //    CuiteControlType = typeof(CUITe_HtmlLabel),
                        //    TranslationFileNodeStructure = "Machines.Admin.Edit.Description"
                        //},
                        //new IdBasedControlsTranslationMap
                        //{
                        //    HtmlElementId = "Machines.Admin.Edit.IpOrDnsNameLabel",
                        //    CuiteControlType = typeof(CUITe_HtmlLabel),
                        //    TranslationFileNodeStructure = "Machines.Admin.Edit.IpOrDnsName"
                        //},
                        //new IdBasedControlsTranslationMap
                        //{
                        //    HtmlElementId = "Machines.Admin.Edit.Discover.Admin.SubnetMaskLabel",
                        //    CuiteControlType = typeof(CUITe_HtmlLabel),
                        //    TranslationFileNodeStructure = "Machines.Admin.Edit.Discover.Admin.SubnetMask"
                        //}
                    };

                    return _idBasedControlsTranslationMap;
                }
            }
        }
    }
}