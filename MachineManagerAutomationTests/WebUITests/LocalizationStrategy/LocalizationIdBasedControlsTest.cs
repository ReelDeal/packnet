﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.LocalizationStrategy.ConcreteIdBasedCheckers;
using MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy
{
    [CodedUITest]
    [DeploymentItem(@"AdditionalFiles\Template.prn", @"Data\Labels")]
    [DeploymentItem("NLog.config")]
    public class LocalizationIdBasedControlsTest : ExternalApplicationTestBase
    {
        private readonly HashSet<string> htmlElementsWithIdsChecked = new HashSet<string>();
        private readonly HashSet<string> allHtmlElementsWithIdsFoundInPage = new HashSet<string>();

        private SimulatorUtilities simUtilities;
        
        public LocalizationIdBasedControlsTest() : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }

        protected override void BeforeTestInitialize()
        {
            simUtilities = new SimulatorUtilities();
        }

        protected override void AfterTestInitialize()
        {
            //Give the webserver some more time to start up
            Thread.Sleep(1000);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("Localization")]
        public void ExpectedLanguagesAreDisplayed()
        {
            TestUtilities.RestartPackNetService();
            LanguageSelector languageSelector = new LanguageSelector();
            HashSet<string> languages = languageSelector.GetLanguagesFromUserInterface();
            Assert.IsTrue(languages.Count > 0);
            Assert.IsTrue(LanguageMap.ExpectedLanguages.Count > 0);
            Assert.IsTrue(languages.Count == LanguageMap.ExpectedLanguages.Count);
            
            foreach (LanguageMap map in LanguageMap.ExpectedLanguages)
            {
                Assert.IsTrue(languages.Contains(map.Display), string.Format("Language '{0} {1}' was in expected map but not found in display.", map.Display, map.Abbreviation));
            }

            foreach (string s in languages)
            {
                Assert.IsTrue(LanguageMap.ExpectedLanguages.Select(a => a.Display).Contains(s), string.Format("Language '{0} ' was in display but but not in expected map.", s));
            }
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("Localization")]
        public void AllLanguageFilesDisplayedInUserInterfaceShouldBeInFileSystem()
        {
            TestUtilities.RestartPackNetService();
            JsonFileChecker jsonFileChecker = new JsonFileChecker();
            List<string> missingLanguages = jsonFileChecker.CheckThatAllLanguagesInUiSelectionAreAlsoInFileSystem();
            Assert.IsTrue(missingLanguages.Count == 0, "A language was displayed in the UI with no corresponding json file! Missing languages: {0}",
                string.Join(", ", missingLanguages));
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("Localization")]
        public void AllLanguageNodesInFilesShouldMatch()
        {
            JsonNodeStructureChecker jsonNodeStructureChecker = new JsonNodeStructureChecker();
            var structureCheckResultAll = jsonNodeStructureChecker.CheckAllLanguages();
            List<string> failures = new List<string>();

            // ReSharper disable once LoopCanBeConvertedToQuery - keep this structure for legibility
            foreach (var result in structureCheckResultAll)
            {
                if (result.MissingNodes.Count != 0)
                {
                    failures.Add
                    (
                        string.Format(
                            "Language file {0} has {1} missing nodes when compared to {2}. They are:{3}{4}",
                            result.LanguageFile,
                            result.MissingNodes.Count,
                            JsonNodeStructureChecker.BasisLanguage,
                            "\n\t",
                            string.Join("\n\t", result.MissingNodes)
                        )
                    );
                }
            }

            if (failures.Count > 0)
            {
                Assert.Fail("One or more languages had missing nodes.{0}{1}", "\n", string.Join("\n", failures));
            }
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("Localization")]
        public void IdBasedControlsShouldBeLocalized()
        {
            List<string> pagesChecked = new List<string>();
            List<IdBasedLocalizationTestResult> localizationCheckingResult = new List<IdBasedLocalizationTestResult>();

            //TODO: Populate all of these concrete checkers
            List<AbstractIdBasedChecker> abstractIdBasedCheckers = new List<AbstractIdBasedChecker>
            {
                new MachinesFusionNewFusionMachineIdBasedChecker(),
            };

            //foreach (AbstractIdBasedChecker checker in abstractIdBasedCheckers)
            //{
            //    checker.NavigateToViewForTesting();
            //    //TODO: Make abstract property in the base class for this name
            //    pagesChecked.Add(checker.GetType().ToString());
            //    localizationCheckingResult = checker.RunLocalizationTests();
            //}

            Specify.That(localizationCheckingResult.Count > 0);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("Localization")]
        public void AllIdBasedControlsWereChecked()
        {
            //This test by definition will not be purely atomic because it will need to reference this: *allHtmlElementsWithIdsFoundInPage* after the other IdBasedControlsShouldBeLocalized has run

            HashSet<string> idsNotChecked = new HashSet<string>();

            foreach(string found in allHtmlElementsWithIdsFoundInPage)
            {
                if (!htmlElementsWithIdsChecked.Contains(found))
                {
                    idsNotChecked.Add(found);
                }
            }
            
            //TODO: Put the assertion in here so we know we have checked every control that has an ID for localization
        }
    }
}