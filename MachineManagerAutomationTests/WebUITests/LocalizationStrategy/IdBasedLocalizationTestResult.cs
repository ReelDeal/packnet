﻿using MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Constants;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy
{
    /// <summary>
    /// This class represents the result of the comparison. You pass the IdBasedControlsTranslationMap to the ctor and then set actual vs expected.
    /// </summary>
    public class IdBasedLocalizationTestResult
    {
        public IdBasedLocalizationTestResult(IdBasedControlsTranslationMap idBasedControlsTranslationMap)
        {
            IdBasedControlsTranslationMap = idBasedControlsTranslationMap;
        }

        public IdBasedControlsTranslationMap IdBasedControlsTranslationMap { get; private set; }

        public bool ControlWasLocated { get; set; }
        public string ExpectedText { get; set; }
        public string ActualText { get; set; }
        public TestStatus TestStatus { get; set; }
    }
}