﻿using System;

using CUITe.Controls.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy
{
    public class IdBasedControlsTranslationMap
    {
        public string HtmlElementId { get; set; }
        public Type CuiteControlType { get; set; }
        public CUITe_HtmlControl CuiteControlInstance { get; set; }
        public string TranslationFileNodeStructure { get; set; }
    }
}