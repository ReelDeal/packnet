﻿using System;
using CUITe.Controls.HtmlControls;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers
{
    public static class CuitePropertyReader
    {
        public static string GetInnerText<T>(CUITe_HtmlControl cuiteHtmlControl) where T : CUITe_HtmlControl
        {
            Type typeInfo = cuiteHtmlControl.GetType();
            var result = (string)typeInfo.GetProperty("InnerText").GetValue(cuiteHtmlControl, null);
            return result;
        }
    }
}