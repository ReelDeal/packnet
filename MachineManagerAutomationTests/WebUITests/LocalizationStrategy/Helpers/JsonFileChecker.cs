﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers
{
    public class JsonFileChecker
    {
        private readonly LanguageSelector languageSelector;

        public JsonFileChecker()
        {
            languageSelector = new LanguageSelector();
        }

        /// <summary>
        /// Checks that all of the languages displayed in the drop down are relfected with a corresponding json language file
        /// </summary>
        /// <returns></returns>
        public List<string> CheckThatAllLanguagesInUiSelectionAreAlsoInFileSystem()
        {
            HashSet<string> languages = languageSelector.GetLanguagesFromUserInterface();
            return languages.Where(languageFromUi => !InFileSystem(languageFromUi)).ToList();
        }

        private bool InFileSystem(string languageFromUi)
        {
            if (string.IsNullOrWhiteSpace(languageFromUi))
            {
                throw new ArgumentNullException("languageFromUi is required.");
            }

            string fileName = string.Concat(LanguageMap.ToAbbreviation(languageFromUi), ".json");
            string filePath = Path.Combine(TestSettings.Default.LanguageFilesPath, fileName);
            return File.Exists(filePath);
        }
    }
}