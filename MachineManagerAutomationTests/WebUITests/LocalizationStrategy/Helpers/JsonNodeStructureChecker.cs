﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using MachineManagerAutomationTests.Helpers;

using Newtonsoft.Json.Linq;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers
{
    public class JsonNodeStructureChecker
    {
        public const string BasisLanguage = "English";
        private readonly string _basisLanguageAbbreviation;
        private readonly HashSet<string> _languagesFromUserInterface;
        
        public JsonNodeStructureChecker()
        {
            _basisLanguageAbbreviation = LanguageMap.ToAbbreviation(BasisLanguage);
            LanguageSelector languageSelector = new LanguageSelector();
            _languagesFromUserInterface = languageSelector.GetLanguagesFromUserInterface();
        }

        public List<JsonNodeStructureCheckResult> CheckAllLanguages()
        {
            var finalResult = new List<JsonNodeStructureCheckResult>();

            foreach (string language in _languagesFromUserInterface)
            {
                //Skip English which is the basis language
                if(language.Equals(BasisLanguage))
                    continue;

                JsonNodeStructureCheckResult result = new JsonNodeStructureCheckResult
                {
                    LanguageComparisonBasisFile = _basisLanguageAbbreviation,
                    LanguageFile = LanguageMap.ToAbbreviation(language)
                };

                result.MissingNodes = FindAllMissingNodes(
                    result.LanguageComparisonBasisFile,
                    result.LanguageFile
                );

                finalResult.Add(result);
            }

            return finalResult;
        }


        /// <summary>
        /// Loop throught all the nodes in the comparison basis, and find the missing item;
        /// The strategy is check from the root of the tree to the end, if the root node is missing, that branch will not be checked anymore.
        /// </summary>
        /// <param name="languageComparisonBasisFile">Name of language used as comparison basis, like "en_US"</param>
        /// <param name="languageFileToCheck">Name of the language that need to be checked, like "da_DK"</param>
        public List<string> FindAllMissingNodes(string languageComparisonBasisFile, string languageFileToCheck)
        {
            var result = new List<string>();

            var jObjectComparisonBasis = GetJsonObjectfromFile(languageComparisonBasisFile);
            
            if (jObjectComparisonBasis == null)
            {
                throw new Exception("Comparison basis language file can not be parsed!");
            }

            var topLayernodesInComparisonJson = jObjectComparisonBasis.Properties().Select(p => p.Name).ToList();

            var jObjectToCheck = GetJsonObjectfromFile(languageFileToCheck);

            if (jObjectToCheck == null)
            {
                return topLayernodesInComparisonJson;
            }

            LoopThroughAllItemsInNode(jObjectComparisonBasis, jObjectToCheck, "", result);

            return result;
        }

        private void LoopThroughAllItemsInNode(JObject ComparisonBasisNode, JObject nodeToCheck, string parent, List<string> result)
        {
            var itemsFromComparisonBasis = ComparisonBasisNode.Properties().Select(p => p.Name).ToList();
            var itemsToCheck = nodeToCheck.Properties().Select(p => p.Name).ToList();

            if (itemsFromComparisonBasis.Count == 0)
            {
                return;
            }

            foreach (var node in itemsFromComparisonBasis)
            {
                string parentNextRecursion = string.Concat(parent, ".", node);
                JObject comparisonBasisNodeNextRecursion = null;
                JObject nodeToCheckNextRecursion = null;

                if (itemsToCheck.Contains(node))
                {
                    //TODO: Refactor this try catch, figure out when is the end of a branch in a tree.
                    if(ComparisonBasisNode[node].Type == JTokenType.String)
                        continue;

                    if (!nodeToCheck[node].HasValues && nodeToCheck[node].Type == JTokenType.String)
                        result.Add(parentNextRecursion);
                    
                    comparisonBasisNodeNextRecursion = ComparisonBasisNode[node].ToObject<JObject>();
                    nodeToCheckNextRecursion = nodeToCheck[node].ToObject<JObject>();

                    LoopThroughAllItemsInNode(comparisonBasisNodeNextRecursion, nodeToCheckNextRecursion, parentNextRecursion, result);
                }
                else
                {
                    result.Add(parentNextRecursion);
                }
            }
        }

        private JObject GetJsonObjectfromFile(string languageFile)
        {
            string filePath = Path.Combine(TestSettings.Default.LanguageFilesPath, string.Concat(languageFile, ".json"));
            string languageContext;

            using (var sr = new StreamReader(filePath))
            {
                languageContext = sr.ReadToEnd();
            }

            return JObject.Parse(languageContext);
        }
    }
}
