﻿using System.Collections.Generic;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers
{
    public class JsonNodeStructureCheckResult
    {
        //TODO: May need to flesh this out more... The idea is that using en_US as basis file we can compare the other nodes and repor which are missing
        //TODO: So for example - if Machines.Admin.Validation.Name.Required exists in en_US but not sp_ES it would report the list of missing nodes for each language.

        /// <summary>
        /// The language file we were examining and comparing to LanguageComparisonBasisFile. 
        /// </summary>
        public string LanguageFile { get; set; }

        /// <summary>
        /// This is our 'known-good' source file to compare to. It will almost always be English (en_US).
        /// </summary>
        public string LanguageComparisonBasisFile { get; set; }

        /// <summary>
        /// The list of missing nodes for the language file (LanguageFile property).
        /// </summary>
        public List<string> MissingNodes { get; set; }
    }
}