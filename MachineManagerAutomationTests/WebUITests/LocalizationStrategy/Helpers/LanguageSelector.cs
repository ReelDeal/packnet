﻿using System.Collections.Generic;
using CUITe.Controls.HtmlControls;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using Testing.Specificity;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers
{
    public class LanguageSelector
    {
        /// <summary>
        /// Get the list of languages from the UI drop down in the login page
        /// </summary>
        /// <returns></returns>
        public HashSet<string> GetLanguagesFromUserInterface()
        {
            HashSet<string> result = new HashSet<string>();
            var loginPage = TestUtilities.CreatePage<DynamicBrowserWindowWithLogin>(@"index.html#/AdminConsole");

            loginPage.VerifySelectLanguageDropDownExists();
            loginPage.SetLoginLanguage();
            var selectionList = loginPage.LanguageSelectionUnorderedList.GetChildren();

            foreach (var language in selectionList)
            {
                result.Add(((CUITe_HtmlListItem)language).InnerText);
            }

            return result;
        }
    }
}