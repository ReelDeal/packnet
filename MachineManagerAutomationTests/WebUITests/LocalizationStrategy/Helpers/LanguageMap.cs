﻿using System.Collections.Generic;
using System.Linq;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers
{
    public class LanguageMap
    {
        private static List<LanguageMap> _resultList;
        
        private LanguageMap(string display, string abbreviation)
        {
            Display = display;
            Abbreviation = abbreviation;
        }

        public string Display { get; private set; }
        public string Abbreviation { get; private set; }

        public static List<LanguageMap> ExpectedLanguages
        {
            get
            {
                if (_resultList == null || _resultList.Count == 0)
                {
                    _resultList = new List<LanguageMap>
                    {
                        new LanguageMap("Dansk", "da_DK"),
                        new LanguageMap("Deutsch", "de_DE"),
                        new LanguageMap("English", "en_US"),
                        new LanguageMap("Español", "es_ES"),
                        new LanguageMap("Français Canadien", "fr_CA"),
                        new LanguageMap("Français", "fr_FR"),
                        new LanguageMap("Italiano", "it_IT"),
                        new LanguageMap("Magyar", "hu_HU"),
                        new LanguageMap("Nederlands", "nl_NL"),
                        new LanguageMap("Polski", "pl_PL"),
                        new LanguageMap("Română", "ro_RO"),
                        new LanguageMap("Svenska", "sv_SE")
                    };
                }
                
                return _resultList;
            }
        }

        public static string ToAbbreviation(string display)
        {
            return ExpectedLanguages.First(a => a.Display.Equals(display)).Abbreviation;
        }

        public static string ToDisplay(string abbreviation)
        {
            return ExpectedLanguages.First(a => a.Display.Equals(abbreviation)).Display;
        }
    }
}