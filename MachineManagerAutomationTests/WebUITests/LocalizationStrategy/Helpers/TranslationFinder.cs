﻿using System.IO;


using Newtonsoft.Json.Linq;

namespace MachineManagerAutomationTests.WebUITests.LocalizationStrategy.Helpers
{
    public class TranslationFinder
    {
        private readonly JObject _languageContext;
        
        public TranslationFinder(string language)
        {
            _languageContext = GetJsonObjectfromFile(language);
        }

        private JObject GetJsonObjectfromFile(string languageFile)
        {
            string filePath = Path.Combine(TestSettings.Default.LanguageFilesPath, string.Concat(languageFile, ".json"));
            string languageNodeString;
            
            using (var sr = new StreamReader(filePath))
            {
                languageNodeString = sr.ReadToEnd();
            }

            var result = JObject.Parse(languageNodeString);
            return result;
        }
    }
}

