﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using CUITe.Controls.HtmlControls;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.FusionSim.ViewModels;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;
using PackNet.Common.Interfaces.DTO.Machines;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    [DeploymentItem("AdditionalFiles\\TestingWorkflows", "TestingWorkflows")]
    public class ConveyorPageTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public ConveyorPageTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9955")]
        [TestCategory("9956")]
        public void ConveyorPageVerification()
        {
            CreateAFusionMachineWithACrossConveyor();

            var page = new MachineManagementPage();

            // wait for the alert to go away. Every exists check takes a few second so likely at most two checks will be needed
            // SelectRowWithValue below didn't work properly if the alert was still there
            for (int i = 0; i < 5; i++)
                if(!page.GetTopWidget(MachineMacros.GetMachineType(TestDatabaseManagement.Fm1)).Exists)
                    break;

            page.MachinesTable.SelectRowWithValue(TestDatabaseManagement.Fm1.Alias, 0);
            page.ClickEditButton();
            page.VerifyAccessoryTableRowContainsSpecifiedData(0, TestDatabaseManagement.ConveyorCross.Alias, TestDatabaseManagement.ConveyorCross.Type.ToString());
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void ConveyorPage_CreateWasteConveyor()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            TestDatabaseManagement.Fm1.Accessories.Add(TestDatabaseManagement.ConveyorWaste);
            MachineMacros.Build(TestDatabaseManagement.Fm1, adminConsole);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void WasteConveyorShouldBeEnabledWhenProductionEnds()
        {
            SetupMinimumTestingEnvironmentForWasteConveyor();

            DropFileUtilities.DropBoxFirstDropFile(1, 10, 10, 10, "2010001", pickzone: TestDatabaseManagement.PickZone2.Alias);

            var simUtilities = new SimulatorUtilities();
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var page = MachineProductionPage.GetWindow();

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel>() { printerSim }, TestDatabaseManagement.Mg5_fm1zp1zp2.SetId(), adminConsole);

            var customerId = page.ItemsInProductionTable.GetCellValue(0, 1);

            ProduceOneKitAndVerifyWasteConveyor(fusionSim, printerSim);

            page.CompletedItemsTable.VerifyContainsValue(customerId);

            simUtilities.Close();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9960")]
        public void ConveyorPage_DirectionModuleCorrection()
        {
            CreateAFusionMachineWithACrossConveyor();

            var page = MachineManagementPage.GetWindow();
            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.ClickNewConveyorButton();

            page.SetConveyorType(ConveyorType.CrossConveyor);

            page.SetCrossConveyorPort(1, 0);
            page.SetConveyorName(TestDatabaseManagement.ConveyorCross.Alias);

            page.VerifyCrossConveyorModuleValue(1, 1);
            page.VerifyConveyorName(TestDatabaseManagement.ConveyorCross.Alias);

            page.SetCrossConveyorModule(1, 3);
            page.SetConveyorDescription(TestDatabaseManagement.ConveyorCross.Description);

            page.VerifyCrossConveyorModuleValue(1, 2);
            page.VerifyConveyorDescription(TestDatabaseManagement.ConveyorCross.Description);
            page.SetCrossConveyorModule(2, 0);
            page.SelectCrossConveyorFeed(1);

            page.VerifyCrossConveyorModuleValue(2, 1);

            page.SetCrossConveyorModule(2, 3);
            page.SelectCrossConveyorFeed(2);

            page.VerifyCrossConveyorModuleValue(2, 2);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9961")]
        public void ConveyorPage_DirectionOutputCorrection()
        {
            var cc = TestDatabaseManagement.ConveyorCross;

            CreateAFusionMachineWithACrossConveyor();

            var page = MachineManagementPage.GetWindow();

            page.PageTableSelectRow(0);
            page.ClickEditButton();
            page.ClickNewConveyorButton();

            page.SetConveyorType(ConveyorType.CrossConveyor);

            page.SetConveyorName(cc.Alias);
            page.SetCrossConveyorPort(1, 1);

            page.VerifyConveyorName(cc.Alias);
            page.VerifyCrossConveyorPortValue(1, 1);

            page.SetCrossConveyorPort(1, 13);
            page.SetConveyorDescription(cc.Description);

            page.VerifyCrossConveyorPortValue(1, 12);
            page.VerifyConveyorDescription(cc.Description);

            page.SetCrossConveyorPort(2, 0);
            page.SelectCrossConveyorFeed(1);

            page.VerifyCrossConveyorPortValue(2, 1);

            page.SetCrossConveyorPort(2, 13);
            page.SelectCrossConveyorFeed(2);

            page.VerifyCrossConveyorPortValue(2, 12);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9959")]
        [TestCategory("9962")]
        [TestCategory("9963")]
        public void ConveyorPage_DuplicateNameIsNotAllowed()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // create machine, pickzones and conveyor
            PickZoneMacros.Create(TestDatabaseManagement.PickZone3, adminConsole);

            var page = MachineManagementPage.GetWindow();

            CreateAFusionMachineWithACrossConveyor(adminConsole);

            page.PageTableSelectRow(0);
            page.ClickEditButton();

            // create a new conveyor which has the same name as the one already created
            page.CreateConveyor(TestDatabaseManagement.ConveyorCrossWithPickZone3);

            page.VerifyDuplicateAliasMessage(TestDatabaseManagement.ConveyorCross.Alias, "Conveyor");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void SetupMiniSystemForCrossConveyorTest()
        {
            SetupMinimumTestingEnvironmentForCrossConveyor();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void SetupMiniSystemForWasteConveyorTest()
        {
            SetupMinimumTestingEnvironmentForWasteConveyor();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10174")]
        public void ConveyorShouldBeActivated_ToMoveLeft_AfterACartonToLeftPickZoneIsFinished()
        {
            SelectionAlgorithmSetUp.ChangeImportWorkflow(SelectionAlgorithmTypes.BoxFirst, "BoxFirstCartonImportWithConveyor.xaml");

            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(Databases.BoxFirstWithCrossConveyor);

            DropFileUtilities.DropBoxFirstWithConveyorDropFile(1, 10, 10, 10, "2010001");

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var simUtilities = new SimulatorUtilities();
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerNormalSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printerPrioSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel>() { printerNormalSim, printerPrioSim },
                TestDatabaseManagement.Mg5_fm1zp1zp2.SetId(), adminConsole);

            var page = MachineProductionPage.GetWindow();

            var customerId = page.ItemsInProductionTable.GetCellValue(0, 1);

            ProduceOneKitAndVerifyConveyor(fusionSim, printerNormalSim, TestDatabaseManagement.PickZone1.Alias);

            page.CompletedItemsTable.VerifyContainsValue(customerId);

            simUtilities.Close();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10176")]
        public void ConveyorShouldBeActivated_ToMoveRight_AfterACartonToRightPickZoneIsFinished()
        {
            SelectionAlgorithmSetUp.ChangeImportWorkflow(SelectionAlgorithmTypes.BoxFirst, "BoxFirstCartonImportWithConveyor.xaml");

            TestDatabaseManagement.RestoreLatestMongoDump(Databases.BoxFirstWithCrossConveyor);

            DropFileUtilities.DropBoxFirstWithConveyorDropFile(1, 10, 10, 10, "2010001", pickzone: TestDatabaseManagement.PickZone2.Alias);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var simUtilities = new SimulatorUtilities();
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerNormalSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printerPrioSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel>() { printerNormalSim, printerPrioSim },
                TestDatabaseManagement.Mg5_fm1zp1zp2.SetId(), adminConsole);

            var page = MachineProductionPage.GetWindow();

            var customerId = page.ItemsInProductionTable.GetCellValue(0, 1);

            ProduceOneKitAndVerifyConveyor(fusionSim, printerNormalSim, TestDatabaseManagement.PickZone2.Alias);

            page.CompletedItemsTable.VerifyContainsValue(customerId);

            simUtilities.Close();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10177")]
        public void ConveyorShouldBeActivated_ToMoveLeftAndRightInOrder()
        {
            SelectionAlgorithmSetUp.ChangeImportWorkflow(SelectionAlgorithmTypes.BoxFirst, "BoxFirstCartonImportWithConveyor.xaml");

            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(Databases.BoxFirstWithCrossConveyor);

            DropFileUtilities.DropBoxFirstWithConveyorDropFile(2, 10, 10, 10, "2010001");

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var simUtilities = new SimulatorUtilities();
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerNormalSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printerPrioSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel>()  { printerNormalSim, printerPrioSim }, 
                TestDatabaseManagement.Mg5_fm1zp1zp2.SetId(), adminConsole);

            var page = MachineProductionPage.GetWindow();

            var customerId = page.ItemsInProductionTable.GetCellValue(0, 1);

            ProduceOneKitAndVerifyConveyor(fusionSim, printerNormalSim, customerId == "0" ? TestDatabaseManagement.PickZone1.Alias : TestDatabaseManagement.PickZone2.Alias);

            page.CompletedItemsTable.VerifyContainsValue(customerId);

            customerId = page.ItemsInProductionTable.GetCellValue(0, 1);

            ProduceOneKitAndVerifyConveyor(fusionSim, printerNormalSim, customerId == "0" ? TestDatabaseManagement.PickZone1.Alias : TestDatabaseManagement.PickZone2.Alias);

            page.CompletedItemsTable.VerifyContainsValue(customerId);

            simUtilities.Close();
        }

        #endregion

        #region Helper methods

        private static void CreateAFusionMachineWithACrossConveyor(AdminConsole adminConsole = null)
        {
            TestDatabaseManagement.Fm1.Accessories.Add(TestDatabaseManagement.ConveyorCross);
            MachineMacros.Build(TestDatabaseManagement.Fm1, adminConsole);
        }

        private void SetupMinimumTestingEnvironmentForCrossConveyor()
        {
            TestUtilities.DropDbAndRestart();
            var pg = TestDatabaseManagement.Pg8_boxfirst_Mg5_Cpg1Cpg2_c2755;
            pg.AddAccessoryToMachine(TestDatabaseManagement.Fm1.Alias, TestDatabaseManagement.ConveyorCross, MGWorkflows.CreateProducibleOnMachineGroupWithConveyor);
            pg.PickZones.Add(TestDatabaseManagement.PickZone3);
            pg.PickZones.Add(TestDatabaseManagement.PickZone4);
            ProductionGroupMacros.Build(pg);
            TestDatabaseManagement.CreateMongoDump(Databases.BoxFirstWithCrossConveyor);
        }

        private void SetupMinimumTestingEnvironmentForWasteConveyor()
        {
            TestUtilities.DropDbAndRestart();
            var pg = TestDatabaseManagement.Pg8_boxfirst_Mg5_Cpg1Cpg2_c2755;
            pg.AddAccessoryToMachine(TestDatabaseManagement.Fm1.Alias, 
                TestDatabaseManagement.ConveyorWaste, MGWorkflows.CreateProducibleOnMachineGroupWithWasteConveyor);

            ProductionGroupMacros.Build(pg);
            TestDatabaseManagement.CreateMongoDump(Databases.BoxFirstWithWasteConveyor);
        }

        private void ProduceOneKitAndVerifyConveyor(FusionSimulatorViewModel fusionSim, ZebraPrinterSimulatorViewModel printerSim, string pickzone)
        {
            SimulatorHelper.ProduceCartonInQueue(fusionSim);

            VerifyConveyor(fusionSim, pickzone);

            SimulatorHelper.ProduceLabelInQueue(printerSim);
        }

        private void ProduceOneKitAndVerifyWasteConveyor(FusionSimulatorViewModel fusionSim,
            ZebraPrinterSimulatorViewModel printerSim)
        {
            SimulatorHelper.ProduceCartonInQueue(fusionSim);

            VerifyWasteConveyor(fusionSim);

            SimulatorHelper.ProduceLabelInQueue(printerSim);
        }

        private void VerifyConveyor(FusionSimulatorViewModel fusionSim, string pickzone)
        {
            var stopWatch = new Stopwatch();
            switch (pickzone)
            {
                case "AZ1":
                    Retry.For(() => Specify.That(fusionSim.IoPanelViewModel.Output1).Should.BeTrue("Conveyor to AZ1 is not on"),
                        TimeSpan.FromSeconds(10));
                    stopWatch.Start();
                    Retry.For(
                        () => Specify.That(fusionSim.IoPanelViewModel.Output1)
                                .Should.BeFalse("Conveyor to AZ1 is not off after a while"), TimeSpan.FromSeconds(10));
                    break;
                case "AZ2":
                    Retry.For(() => Specify.That(fusionSim.IoPanelViewModel.Output2).Should.BeTrue("Conveyor to AZ2 is not on"),
                        TimeSpan.FromSeconds(10));
                    stopWatch.Start();
                    Retry.For(
                        () => Specify.That(fusionSim.IoPanelViewModel.Output2)
                                .Should.BeFalse("Conveyor to AZ2 is not off after a while"), TimeSpan.FromSeconds(10));
                    break;
            }
            stopWatch.Stop();
            Console.WriteLine("Conveyor activated for " + stopWatch.ElapsedMilliseconds + " ms");
        }

        private void VerifyWasteConveyor(FusionSimulatorViewModel fusionSim)
        {
            var stopWatch = new Stopwatch();
            Retry.For(() => Specify.That(fusionSim.IoPanelViewModel.Output1).Should.BeTrue("Conveyor to AZ1 is not on"), TimeSpan.FromSeconds(10));
            stopWatch.Start();
            Retry.For(() => Specify.That(fusionSim.IoPanelViewModel.Output1).Should.BeFalse("Conveyor to AZ1 is not off after a while"), TimeSpan.FromSeconds(10));
            stopWatch.Stop();
            Console.WriteLine("Conveyor activated for " + stopWatch.ElapsedMilliseconds + " ms");

        }

        private void AddConveyorToMachine(IMachine machine, MachineGroupExt mg, ConveyorExt conv, AdminConsole adminConsole)
        {
            adminConsole.NavigateToPage(NavPages.Machine);
            var mmPage = MachineManagementPage.GetWindow();
            mmPage.MachinesTable.SelectRowWithValue(machine.MachineAlias);
            mmPage.ClickEditButton();
            mmPage.CreateAccessoriesForMachine(new List<IAccessory>() { conv });
            mmPage.ClickSaveButton();

            MachineGroupMacros.UpdateWorkflow(mg, MGWorkflows.CreateProducibleOnMachineGroupWithConveyor, adminConsole);

            adminConsole.NavigateToPage(NavPages.Dashboard);
            adminConsole.AddWidgetToDashboard(DashboardWidgets.Classifications);
            var widget = new ClassificationsWidget();
            widget.SetPrinterPriority(TestDatabaseManagement.Classification1, false);
        }
        #endregion
    }
}