﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.FusionSim.ViewModels;

using MachineManagerAutomationTests.Macros;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    [DeploymentItem("NLog.config")]
    public class ExternalTriggerTests : ExternalApplicationTestBase
    {
        #region Constructor and initiators
        SimulatorUtilities simUtilities;

        public ExternalTriggerTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9575")]
        public void CreateFootPedal()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var fp = TestDatabaseManagement.FootpedalPurposeTriggerMod1Port3;
            TestDatabaseManagement.Fm1.Accessories.Add(fp);
            MachineMacros.Build(TestDatabaseManagement.Fm1, adminConsole);

            var page = MachineManagementPage.GetWindow();
            page.PageTableSelectRow(0);
            page.ClickEditButton();

            page.AccessoriesTable.VerifyCellValue(0, 0, fp.Alias); // alias
            page.AccessoriesTable.VerifyCellValue(0, 1, "FootPedal"); // type
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void SetupMiniSystemForFootPedalTest()
        {
            SetupMinimumTestingEnvironmentForFootPedal();
            TestDatabaseManagement.CreateMongoDump(SelectionAlgorithmTypes.BoxFirst);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9577")]
        public void UseFootPedalToTriggerPackageReleasing()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            simUtilities = new SimulatorUtilities();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = MachineProductionPage.GetWindow();

            page.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            page.ClickPlayButton();

            CreateOrdersFromArticle(adminConsole, 1);

            CompleteJobOnSimWithReleaseTrigger(adminConsole, fusionSim, false);

            simUtilities.Close();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10369")]
        public void UseFootPedalTo_PauseAndPlayMachineGroup_WhenItIsNotInProductionMode()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            simUtilities = new SimulatorUtilities();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            Specify.That(fusionSim.IsSimulationStarted).Should.BeTrue("Fusion sim should be started already");
            Specify.That(printerSim.IsSimulationStarted).Should.BeTrue("Printer sim should be started already");

            Playback.Wait(2000);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = MachineProductionPage.GetWindow();

            page.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            page.ClickPlayButton();

            page.VerifyIsMachineGroupStatus(MachineStatus.Online);

            SendFootPedalPauseOrPlaySignal(fusionSim);

            page.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            SendFootPedalPauseOrPlaySignal(fusionSim);

            page.VerifyIsMachineGroupStatus(MachineStatus.Online);

            simUtilities.Close();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10370")]
        public void UseFootPedalTo_PauseAndPlayMachineGroup_WillNotWorkWhenItIsNotOnlineOrPaused()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            simUtilities = new SimulatorUtilities();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            Assert.IsTrue(fusionSim.IsSimulationStarted, "Fusion sim should be started already");
            Assert.IsTrue(printerSim.IsSimulationStarted, "Printer sim should be started already");

            Playback.Wait(2000);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var machineProdPage = MachineProductionPage.GetWindow();
            Specify.That(machineProdPage.IsMachineStatus(MachineStatus.Paused)).Should.BeTrue();

            fusionSim.Simulator.EmergencyStop();

            machineProdPage.VerifyIsMachineGroupStatus(MachineStatus.Error, "Machine Group is not in Estop");

            SendFootPedalPauseOrPlaySignal(fusionSim);

            Assert.Inconclusive("Related to bug 10973");
            Specify.That(machineProdPage.IsMachineStatus(MachineStatus.Error)).Should.BeTrue();

            SendFootPedalPauseOrPlaySignal(fusionSim);
            Specify.That(machineProdPage.IsMachineStatus(MachineStatus.Error)).Should.BeTrue();
            simUtilities.Close();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10368")]
        [TestCategory("10367")]
        public void UseFootPedalTo_PauseAndPlayMachineGroup_WhenItIsInProductionMode()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            simUtilities = new SimulatorUtilities();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            Assert.IsTrue(fusionSim.IsSimulationStarted, "Fusion sim should be started already");
            Assert.IsTrue(printerSim.IsSimulationStarted, "Printer sim should be started already");
            Thread.Sleep(2000);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var machineProdPage = MachineProductionPage.GetWindow();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            machineProdPage.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            machineProdPage.ClickPlayButton();

            machineProdPage.VerifyIsMachineGroupStatus(MachineStatus.Online);

            CreateOrdersFromArticle(adminConsole, 5);

            adminConsole.NavigateToPage(NavPages.MachineProduction);

            machineProdPage.ItemsInProductionTable.VerifyContainsValue("Sent");

            SendFootPedalPauseOrPlaySignal(fusionSim);

            machineProdPage.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            CompleteJobOnSimWithReleaseTrigger(adminConsole, fusionSim, false, isPlay: false);

            machineProdPage.ItemsInProductionTable.VerifyContainsValue("Sent");

            machineProdPage.ItemsInProductionTable.VerifyContainsValue("Selected");

            SendFootPedalPauseOrPlaySignal(fusionSim);

            machineProdPage.ItemsInProductionTable.VerifyContainsValue("Sent");

            machineProdPage.VerifyIsMachineGroupStatus(MachineStatus.Online);

            simUtilities.Close();
        }
        #endregion

        #region Helper methods

        private void SetupMinimumTestingEnvironmentForFootPedal()
        {
            var pg = TestDatabaseManagement.Pg12_orders_mg2_c199c2755;
            pg.AddAccessoryToMachine(TestDatabaseManagement.Fm1.Alias, TestDatabaseManagement.FootpedalPurposeTriggerMod1Port3);
            pg.AddAccessoryToMachine(TestDatabaseManagement.Fm1.Alias, TestDatabaseManagement.FootpedalPurposePausePlayMod1Port2, MGWorkflows.CreateProducibleOnMachineGroupWithTrigger);
            pg.Classifications.Add(TestDatabaseManagement.Classification1);
            pg.CPGs.Add(TestDatabaseManagement.Cpg1);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            ProductionGroupMacros.Build(pg, adminConsole);
            ArticleMacros.BuildArticle(TestDatabaseManagement.Article111, adminConsole);
        }

        private void CompleteJobOnSimWithReleaseTrigger(AdminConsole adminConsole, FusionSimulatorViewModel sim, bool isAuto = true, int packagingTime = 3, bool isPlay = true)
        {
            adminConsole.NavigateToPage(NavPages.MachineProduction);

            var page = MachineProductionPage.GetWindow();

            SimulatorHelper.VerifySimulatorHasItemsInQueue(sim);

            page.ItemsInProductionTable.VerifyContainsValue(JobStatus.SentToMachine);

            sim.StartStopPackaging();
            Thread.Sleep(TimeSpan.FromSeconds(packagingTime));

            page.ItemsInProductionTable.VerifyContainsValue(JobStatus.Started);

            var customerUniqueId = page.ItemsInProductionTable.GetCellValue(0, 1).Split(':').First();

            SendFootPedalTriggerSignal(sim);

            page.ItemsInProductionTable.VerifyContainsValue(JobStatus.Triggered);

            sim.StartStopPackaging();
            Thread.Sleep(TimeSpan.FromSeconds(packagingTime));

            page.ItemsInProductionTable.VerifyContainsValue(JobStatus.Triggered, false);

            page.CompletedItemsTable.VerifyContainsValue(customerUniqueId);
        }

        private void CreateOrdersFromArticle(AdminConsole adminConsole, int quantity)
        {
            adminConsole.NavigateToPage(NavPages.CustomJobFromArticle);
            var page = CustomJobFromArticlePage.GetWindow();
            page.PageTableSelectRow(0);
            page.ClickProduceButton();

//            page.SetQuantity(1, quantity);
        }

        private void SendFootPedalPauseOrPlaySignal(FusionSimulatorViewModel fusionSim)
        {
            fusionSim.IoPanelViewModel.Input2 = !fusionSim.IoPanelViewModel.Input2;
            Thread.Sleep(1000);
            fusionSim.IoPanelViewModel.Input2 = !fusionSim.IoPanelViewModel.Input2;
        }

        private void SendFootPedalTriggerSignal(FusionSimulatorViewModel fusionSim)
        {
            fusionSim.IoPanelViewModel.Input3 = !fusionSim.IoPanelViewModel.Input3;
            Thread.Sleep(1000);
            fusionSim.IoPanelViewModel.Input3 = !fusionSim.IoPanelViewModel.Input3;
        }

        #endregion
    }
}
