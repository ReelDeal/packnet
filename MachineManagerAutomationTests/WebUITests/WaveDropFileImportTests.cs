﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Data.BoxFirstProducible;
using PackNet.Data.Machines;
using PackNet.Common.Utils;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.WebUITests.Repository.Performance;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    ///<summary>
    ///Testing Importing Process for Wave.
    ///</summary>

    [CodedUITest]
    [DeploymentItem("NLog.config")]
    [DeploymentItem("AdditionalFiles")]
    public class BoxFirstDropFileImportTests : ExternalApplicationTestBase
    {
        #region Variables
        int performanceTestTotalCartonsDropped;
        #endregion

        #region Constructor and initializers
        public BoxFirstDropFileImportTests()
            : base(TestSettings.Default.MachineManagerProcessName, TestUtilities.MachineManagerWindowLocation, false, true)
        {
            performanceTestTotalCartonsDropped = 0;
        }

        protected override void BeforeTestInitialize()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);
        }

        protected override void AfterTestInitialize()
        {
            //Give the webserver some more time to start up
            Thread.Sleep(1000);
        }
        #endregion

        #region Tests
        //[TestMethod]
        //[TestCategory("UIAutomation")]
        //[TestCategory("")]
        //public void SetupABoxFirstEnviroment()
        //{
        //    BoxFirstSearchTest.SetupBoxFirstEnvironment();
        //}

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void JustCreateMeABoxFirstDropFile()
        {
            DropFileUtilities.DropStaplesBoxFirstDropFile(500, 2001, randomizeDimentions: true);
            DropFileUtilities.DropStaplesBoxFirstDropFile(500, 2501, randomizeDimentions: true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void BoxFirstImportCompleteTest()
        {
            var boxFirstRepo = new BoxFirstRepository();
            boxFirstRepo.DeleteAll();
            Thread.Sleep(2000);
            var results = new PerformanceTestResult { SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst, Description = "BF Import Performance" };
            results.Tests.Add("warmup", MeasureImportComplete(1));
            results.Tests.Add("1", MeasureImportComplete(1));
            results.Tests.Add("10", MeasureImportComplete(10));
            results.Tests.Add("50", MeasureImportComplete(50));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\BF_import_testresults.txt", results.ToString());
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void BoxFirstDropFileLoadStagingCompleteTest()
        {
            var boxFirstRepo = new BoxFirstRepository();
            boxFirstRepo.DeleteAll();

            var results = new PerformanceTestResult { SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst, Description = "BF Import/Staging Performance" };
            results.Tests.Add("warmup", MeasureDropFileAvailability(1));
            results.Tests.Add("1", MeasureDropFileAvailability(1));
            results.Tests.Add("10", MeasureDropFileAvailability(10));
            results.Tests.Add("100", MeasureDropFileAvailability(100));
            results.Tests.Add("1000", MeasureDropFileAvailability(1000));
            results.Tests.Add("10000", MeasureDropFileAvailability(10000));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\BF_importstaging_testresults.txt", results.ToString());
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void BoxFirstDropFileUntilJobSentToMachineQueue()
        {
            var results = new PerformanceTestResult() { SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst, Description = "BF Drop File Until Job Sent to Queue" };

            results.Tests.Add("1000", MeasureTimeBetweenDropFileAndJobSentToQueue(1000));

            //due to printers not being able to reconnect, we can only run one at a time
            //results.Tests.Add("10", MeasureTimeBetweenDropFileAndJobSentToQueue(10));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\BF_DropToJobSentToQueue_testresults.txt", results.ToString());
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("382")]
        public void BoxFirstDropFileImport()
        {
            TestUtilities.DropDbAndRestart();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            //Import Sample Drop File
            DropFileUtilities.DropFileInDirectory(new List<string>
            {
                "Type;SerialNumber;OrderId;Quantity;Name;Length;Width;Height;ClassificationNumber;CorrugateQuality;PickZone;DesignId;PrintInfoField1;PrintInfoField2;PrintInfoField3;PrintInfoField4;PrintInfoField5;PrintInfoField6;PrintInfoField7;PrintInfoField8;PrintInfoField9;PrintInfoField10",
                "BoxFirstCarton;1;;1;Test1;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;2;;1;Test2;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;3;;1;Test3;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;4;;1;Test4;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;5;;1;Test5;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;6;;1;Test6;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;7;;1;Test7;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;8;;1;Test8;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10",
                "BoxFirstCarton;9;;1;Test9;15;15;15;1;1;AZ1;2000001;r1;r2;r3;r4;r5;r6;r7;r8;r9;r10"

            });

            Assert.Inconclusive("Not implemented yet!");

            Retry.For(() => adminConsole.TotalRequestsCountLabel.InnerText == "90", TimeSpan.FromSeconds(5));
            Assert.IsTrue(adminConsole.TotalRequestsCountLabel.InnerText == "90", string.Format("There were only {0} requests", adminConsole.TotalRequestsCountLabel.InnerText));
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("281")]
        public void NoDuplicatesAfterWaveDropFileImport()
        {
            TestUtilities.DropDbAndRestart();

            DropFileSampleForBoxFirst("SampleDropFileForWaveMultClass.csv");

            Assert.Inconclusive("Not implemented yet!");

            var adminWindow = new AdminConsole();
            Retry.For(() => adminWindow.TotalRequestsCountLabel.InnerText == "90", TimeSpan.FromSeconds(5));
            Assert.IsTrue(adminWindow.TotalRequestsCountLabel.InnerText == "90", string.Format("There were only {0} requests", adminWindow.TotalRequestsCountLabel.InnerText));
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("487")]
        public void OnlyManualJobsAreProducedInManualMode()
        {
            TestUtilities.DropDbAndRestart();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = new ArticlesManagementPage();

            // create an article
            page.CreateArticle(TestDatabaseManagement.Article111, adminConsole);

            // set a new name to the article
            TestDatabaseManagement.Article333.ArticleId = "steve";

            // create an article
            page.CreateArticle(TestDatabaseManagement.Article333, adminConsole);

            // create a machine
            MachineMacros.Create(TestDatabaseManagement.Fm1, adminConsole);

            // start machine
            var simUtilities = new SimulatorUtilities();
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            sim.Start();
            sim.StartupComplete();

            Assert.Inconclusive("Not implemented yet!");

            //CreateArticle333();
            //1. Start .Server with a EM6/7 or IQ SIM configured in the available machines. 
            //var machineId = 3;
            //2. Start Machine/SIM and wait for connection.
            //var sim = FusionSimulatorTests.RunAndGetFusionSim("127.0.0.1", 8081, "127.0.0.1", 9104);
            //sim.StartButton.Click();
            //Retry.For(() => sim.StartUpCompleteButton.Enabled, TimeSpan.FromSeconds(10));
            ////3. Attach/Connect Machine to .Server
            //sim.StartUpCompleteButton.Click();

            var machineProdPage = MachineProductionPage.GetWindow();

            if (!machineProdPage.IsMachineStatus(MachineStatus.Empty))
            {
                adminConsole.Refresh();

                var loginPage = DynamicBrowserWindowWithLogin.GetWindow();
                loginPage.Login(TestSettings.Default.MachineManagerAdminUserName, TestSettings.Default.MachineManagerAdminPassword);

                //                var selectMachinePage = new SelectMachine(opPage.Title);
                //                selectMachinePage.GetMachineButton(machineId).Click();

            }

            machineProdPage.ClickAutoButton();



            //            var createFromArticlesDialog = OperatorConsoleArticleDialog.OpenArticlesDialog(opPage);
            //createFromArticlesDialog.InputBox(0).SetText("2");
            //createFromArticlesDialog.CreateButton.UnWrap().DrawHighlight();
            //createFromArticlesDialog.CreateButton.Click();
            //Specify.That(createFromArticlesDialog.Alert.AlertText.Equals("2 total cartons created")).Should.BeTrue();
            //opPage.MachineProductionLink.Click();
            //check that no job is sent to machine
            machineProdPage.ClickManualButton();
            //verify that the job went to machine
            //var producibleOrders = opPage.OpenOrdersWidget.Orders.Count.ToString(CultureInfo.InvariantCulture);
            //Specify.That(producibleOrders == "1").Should.BeTrue();


        }
        #endregion

        #region Helper methods
        public static void DropFileSampleForBoxFirst(string fileName)
        {
            var devDropFilePath = Path.Combine(TestSettings.Default.InstallLocation, "DropFolder\\") + fileName;
            var ordersFilePath = Path.Combine(@Environment.CurrentDirectory, fileName);
            File.Copy(ordersFilePath, devDropFilePath);
        }

        private TimeSpan MeasureTimeBetweenDropFileAndJobSentToQueue(int cartonsToTrigger)
        {
            DateTime? stopTime = null;
            var start = DateTime.Now;

            TestDatabaseManagement.CascadedTruncateOfTableInDatabase(DatabaseTables.BoxFirst);
            TestUtilities.RestartPackNetService();

            TestUtilities.DeleteLogFile(LogTypes.Regular);

            var fusionSim = new SimulatorUtilities().ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = new SimulatorUtilities().ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            // start machine sim
            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();

            DropFileUtilities.DropBoxFirstDropFile(cartonsToTrigger);

            while (!CheckFileForJobSentToQueueUpdate(cartonsToTrigger, out stopTime))
                Thread.Sleep(1);

            fusionSim.Dispose();
            printerSim.Dispose();

            return stopTime.Value - start;
        }

        public bool CheckFileForJobSentToQueueUpdate(int cartonId, out DateTime? stopTime)
        {
            string[] lines = { };
            stopTime = null;
            bool readSuccess = false, purgeSuccess = false;
            var logFilePath = Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log");
            //real log entry: 2015-04-06 10:33:02.1256|INFO|17808(16)|PackNet.Services.SelectionAlgorithm.Orders.OrdersSelectionAlgorithmService.AddOrderToQueue|Order 'order1(b6584de6-957c-4e5b-a903-2d9119f76027)' added to queue 'b00a7186-71c4-4fd5-bd51-720d3a852155' 

            var matchStrA = "addproductionitemtoqueue";
            var matchStrB = "QueueLength:1";

            while (!readSuccess)
            {
                try
                {
                    lines = File.ReadAllLines(logFilePath);
                    readSuccess = true;
                }
                catch { }
            }
            for (int i = lines.Length - 1; i > 0; i--)
            {
                if (lines[i].ToLower().Contains(matchStrA.ToLower()) || lines[i].ToLower().Contains(matchStrB.ToLower()))
                {
                    while (!purgeSuccess)
                    {
                        try
                        {
                            File.Delete(logFilePath);
                            purgeSuccess = true;
                        }
                        catch { }
                    }
                    stopTime = Convert.ToDateTime(lines[i].Split('|')[0]);
                    return true;
                }
            }
            return false;
        }

        public TimeSpan MeasureImportComplete(int cartonsToDrop)
        {
            TestUtilities.DeleteLogFile(LogTypes.Regular);

            var fullFileName = DropFileUtilities.DropBoxFirstDropFile(cartonsToDrop);

            var filename = fullFileName.Split('\\')[fullFileName.Split('\\').Count() - 1];
            var sw = Stopwatch.StartNew();
            while (!CheckFileForImportUpdate(filename))
                Thread.Sleep(1);
            sw.Stop();
            return sw.Elapsed;
        }

        public TimeSpan MeasureDropFileAvailability(int cartonsToDrop, int qtyPerOrder = 1)
        {
            var boxFirstRepo = new BoxFirstRepository();
            var startingRepoCount = boxFirstRepo.Count();
            var allCartonsStaged = startingRepoCount + cartonsToDrop;
            File.Delete(Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log"));

            DropFileUtilities.DropBoxFirstDropFile(cartonsToDrop, performanceTestTotalCartonsDropped);

            performanceTestTotalCartonsDropped += cartonsToDrop;

            long count = 0;

            var sw = Stopwatch.StartNew();
            while (count < allCartonsStaged)
            {
                count = boxFirstRepo.Count();
                Thread.Sleep(50);
            }

            sw.Stop();
            return sw.Elapsed;
        }

        #endregion
    }
}
