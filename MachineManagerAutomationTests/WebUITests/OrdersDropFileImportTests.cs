﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.WebUITests.Repository.Performance;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Data.Orders;
using PackNet.Data.Producibles;
using PackNet.FusionSim.ViewModels;
using PackNet.Services;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;
using PackNet.Common.Interfaces.DTO.Corrugates;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    //[DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    [DeploymentItem("NLog.config")]
    public class OrdersDropFileImportTests : ExternalApplicationTestBase
    {
        #region Constructor and initiators
        RestrictionResolverService restrictionResolverService;
        SimulatorUtilities simUtilities = null;
        private IServiceLocator serviceLocator = new Mock<IServiceLocator>().Object;

        public OrdersDropFileImportTests()
            : base(TestSettings.Default.MachineManagerProcessName, TestUtilities.MachineManagerWindowLocation, false, true)
        {
            restrictionResolverService = new RestrictionResolverService();
            BoxLastRepository boxlastrepo = new BoxLastRepository(); //this is here because the boxlastrepo registers the resolvers needed to serialize orders
        }

        protected override void BeforeTestInitialize()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.Order);
            simUtilities = new SimulatorUtilities();

        }

        protected override void AfterTestCleanup()
        {
            simUtilities.Close();
            base.AfterTestCleanup();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void SetupAnOrdersEnvironment()
        {
            TestUtilities.DropDbAndRestart();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            ProductionGroupMacros.Build(TestDatabaseManagement.Pg12_orders_mg2_c199c2755, adminConsole); // fm1, zp1
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg16_orders_mg7_c199c2755, adminConsole); // fm2, fm3, zp3

            CorrugateMacros.Create(new List<Corrugate>() { TestDatabaseManagement.Corrugate_w22t011, 
                TestDatabaseManagement.Corrugate_w279t011, TestDatabaseManagement.Corrugate_w37t011 }, adminConsole);

            ArticleMacros.BuildArticles(new List<ArticleExt>() { TestDatabaseManagement.Article111, 
                TestDatabaseManagement.Article222, TestDatabaseManagement.Article333 }, adminConsole);

            //take DB snapshot
            TestDatabaseManagement.CreateMongoDump(SelectionAlgorithmTypes.Order);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void JustCreateMeAnOrdersDropFile()
        {
            DropFileUtilities.DropOrdersDropFile(100);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5559")]
        public void ProduceCustomJobsFromOperatorPanel()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            CustomJobMacros.Create(TestDatabaseManagement.CustomJob_h5w5l5, adminConsole);

            adminConsole.NavigateToPage(NavPages.MachineProduction);
            var machineProdPage = MachineProductionPage.GetWindow();

            machineProdPage.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            machineProdPage.ClickPlayButton();

            var allOrders = new OrdersRepository(serviceLocator).All();
            var orderGuid = allOrders.First().OrderId;

            //Verify presence in Open Orders
            Retry.For(() => Specify.That(new OpenOrdersWidget().IsOrderPresent(orderGuid)).Should.BeTrue("order not present in openOrders"),
                TimeSpan.FromSeconds(10));

            SimulatorHelper.ProduceCartonInQueue(sim);

            //verify presence in Closed Orders
            Retry.For(() => Specify.That(new ClosedOrdersWidget(false).IsOrderPresent(orderGuid)).Should.BeTrue("order not present in closed orders"),
                TimeSpan.FromSeconds(10));
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5589")]
        public void ReproduceCustomJobsFromClosedOrders()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            CustomJobMacros.Create(TestDatabaseManagement.CustomJob_h5w5l5, adminConsole);
            Thread.Sleep(2000);
            adminConsole.NavigateToPage(NavPages.MachineProduction);
            var machineProdPage = MachineProductionPage.GetWindow();
            Retry.For(() => Specify.That(machineProdPage.IsMachineStatus(MachineStatus.Paused)).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));
            machineProdPage.ClickPlayButton();

            var allOrders = new OrdersRepository(serviceLocator).All();
            var orderGuid = allOrders.First().OrderId;
            Retry.For(() => Specify.That(new OpenOrdersWidget().IsOrderPresent(orderGuid)).Should.BeTrue(orderGuid + " not found in open orders"),
                TimeSpan.FromSeconds(15));

            SimulatorHelper.ProduceCartonInQueue(fusionSim);

            Retry.For(() => Specify.That(new ClosedOrdersWidget(false).IsOrderPresent(orderGuid)).Should.BeTrue("order not present in closed orders"),
                TimeSpan.FromSeconds(10));

            var closedOrders = new ClosedOrdersWidget(false);
            closedOrders.GetReproduceButtonForOrder(orderGuid).SafeClick();
            closedOrders.ReproduceOrderSaveButton.SafeClick();

            Retry.For(() => Specify.That(new OpenOrdersWidget().IsOrderPresent(orderGuid)).Should.BeTrue("Order Not present in open orders"),
               TimeSpan.FromSeconds(10));

            Retry.For(() => Specify.That(fusionSim.Simulator.Queue.Count > 0).Should.BeTrue("Did not find any cartons in the sim queue"),
               TimeSpan.FromSeconds(10));
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            Retry.For(() => Specify.That(new ClosedOrdersWidget(false).IsOrderPresent(orderGuid)).Should.BeTrue("order not present in closed orders"),
                TimeSpan.FromSeconds(10));
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("7495")]
        public void ImportOrdersDropFileIntoServer()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var machineProdPage = MachineProductionPage.GetWindow();

            DropFileUtilities.DropOrdersDropFile(3, 5);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            Retry.For(() => Specify.That(machineProdPage.IsMachineStatus(MachineStatus.Paused)).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));

            machineProdPage.ClickAutoButton();
            machineProdPage.ClickPlayButton();

            Retry.For(() => Specify.That(new OpenOrdersWidget().IsOrderPresent("order1")).Should.BeTrue("Order1 not found in open orders"),
                TimeSpan.FromSeconds(15));
            Specify.That(new OpenOrdersWidget().IsOrderPresent("order2")).Should.BeTrue("Order2 not found in open orders");
            Specify.That(new OpenOrdersWidget().IsOrderPresent("order3")).Should.BeTrue("Order3 not found in open orders");

            //complete order1 qty is 5 but because of tiling the order should be complete after 3 jobs are produced
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
            Thread.Sleep(2000);
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
            Thread.Sleep(2000);
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            //verify in closed orders
            Retry.For(() =>
                Specify.That(new ClosedOrdersWidget(false).IsOrderPresent("order1")).Should.BeTrue("Order1 not found in closed orders"),
                TimeSpan.FromSeconds(10));
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("7501")]
        public void CancelMultipleOrdersInNotStartedStatusWithASim()
        {
            var mg1Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg2_fm1zp1.Alias).Id;
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.SelectMachineGroup(mg1Guid);
            var machineProdPage = MachineProductionPage.GetWindow();
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            Retry.For(() => Specify.That(machineProdPage.IsMachineStatus(MachineStatus.Online)).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));
            machineProdPage.ClickAutoButton();

            DropFileUtilities.DropOrdersDropFile();
            machineProdPage.ClickPlayButton();

            var openOrders = new OpenOrdersWidget();
            openOrders.ClickCheckBoxForOrder("order1");
            openOrders.ClickCheckBoxForOrder("order2");
            openOrders.ClickCancelOrdersButton();

            //let the ui catch up
            Retry.For(() => Specify.That(openOrders.IsOrderPresent("order1")).Should.BeFalse(),
                TimeSpan.FromSeconds(10));
            Specify.That(openOrders.IsOrderPresent("order2")).Should.BeFalse();

            //verify presence in Closed Orders
            Retry.For(() => Specify.That(new ClosedOrdersWidget(false).IsOrderPresent("order1")).Should.BeTrue("Order1 not found in closed orders"),
                TimeSpan.FromSeconds(30));
            Retry.For(() => Specify.That(new ClosedOrdersWidget(false).IsOrderPresent("order2")).Should.BeTrue("Order2 not found in closed orders"),
                            TimeSpan.FromSeconds(30));

        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("7504")]
        public void CancelMultipleOrdersInInProgressStatusWithASim()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var machineProdPage = MachineProductionPage.GetWindow();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            Retry.For(() => Specify.That(machineProdPage.IsMachineStatus(MachineStatus.Online)).Should.BeTrue("MG never got out of Initializing"),
                TimeSpan.FromSeconds(60));
            machineProdPage.ClickAutoButton();

            DropFileUtilities.DropOrdersDropFile(3, 20);

            machineProdPage.ClickPlayButton();

            var openOrders = new OpenOrdersWidget();
            //wait for UI to update
            Retry.For(() => Specify.That(openOrders.IsOrderPresent("order1")).Should.BeTrue("Order1 not present"),
                TimeSpan.FromSeconds(20));

            //partially complete order1
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            //wait for UI to update
            Thread.Sleep(3000);
            //move order2 to create next
            openOrders.ClickCheckBoxForOrder("order2");
            openOrders.ClickCreateNextButton();
            Thread.Sleep(3000);

            //create second carton from order1 and start order 2
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
            //wait for UI to update
            Thread.Sleep(3000);
            openOrders.ClickCheckBoxForOrder("order1");
            openOrders.ClickCheckBoxForOrder("order2");
            openOrders.ClickCancelOrdersButton();
            Thread.Sleep(3000);

            Retry.For(() => Specify.That(new OpenOrdersWidget().IsOrderPresent("order1")).Should.BeFalse(), TimeSpan.FromSeconds(4));
            Specify.That(openOrders.IsOrderPresent("order2")).Should.BeFalse();
            //verify presence in Closed Orders
            Retry.For(() => Specify.That(new ClosedOrdersWidget(false).IsOrderPresent("order1")).Should.BeTrue("Order1 not found in closed orders"),
                TimeSpan.FromSeconds(10));
            Retry.For(() => Specify.That(new ClosedOrdersWidget(false).IsOrderPresent("order2")).Should.BeTrue("Order2 not found in closed orders"),
                            TimeSpan.FromSeconds(10));

        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void OrdersImportCompleteTest()
        {
            TestUtilities.DeleteLogFile(LogTypes.Regular);
            var ordersRepo = new OrdersRepository(serviceLocator);
            ordersRepo.DeleteAll();

            var results = new PerformanceTestResult { SelectionAlgorithm = SelectionAlgorithmTypes.Order, Description = "Orders Import Performance" };
            results.Tests.Add("warmup", MeasureImportComplete(1));
            results.Tests.Add("1", MeasureImportComplete(1));
            results.Tests.Add("10", MeasureImportComplete(10));
            results.Tests.Add("100", MeasureImportComplete(100));
            results.Tests.Add("1000", MeasureImportComplete(1000));
            results.Tests.Add("10000", MeasureImportComplete(10000));
            //results.Tests.Add("100000", MeasureImportComplete(100000)); //takes 8-10 mins last i checked

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\OR_import_testresults.txt", results.ToString());

            Specify.That(results.Tests["1"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("OR Import 1 running unusually slow");
            Specify.That(results.Tests["10"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("OR Import 10 running unusually slow");
            Specify.That(results.Tests["100"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("OR Import 100 running unusually slow");
            Specify.That(results.Tests["1000"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("OR Import 1000 running unusually slow");
            Specify.That(results.Tests["10000"].Duration() < new TimeSpan(0, 0, 10, 0)).Should.BeTrue("OR Import 10000 running unusually slow");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void OrdersDropFileLoadStagingCompleteTest()
        {
            TestUtilities.DeleteLogFile(LogTypes.Regular);
            var ordersRepo = new OrdersRepository(serviceLocator);
            ordersRepo.DeleteAll();

            var results = new PerformanceTestResult { SelectionAlgorithm = SelectionAlgorithmTypes.Order, Description = "Orders Import/Staging Performance" };
            results.Tests.Add("warmup", MeasureDropFileAvailability(1));
            results.Tests.Add("1", MeasureDropFileAvailability(1));
            results.Tests.Add("10", MeasureDropFileAvailability(10));
            results.Tests.Add("100", MeasureDropFileAvailability(100));
            results.Tests.Add("1000", MeasureDropFileAvailability(1000));
            //results.Tests.Add("10000", MeasureDropFileAvailability(10000));
            //results.Tests.Add("100000", MeasureDropFileAvailability(100000));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\OR_importstaging_testresults.txt", results.ToString());

            Specify.That(results.Tests["1"].Duration() < new TimeSpan(0, 0, 0, 1)).Should.BeTrue("OR Import/Staging 1 running unusually slow");
            Specify.That(results.Tests["10"].Duration() < new TimeSpan(0, 0, 0, 3)).Should.BeTrue("OR Import/Staging 10 running unusually slow");
            Specify.That(results.Tests["100"].Duration() < new TimeSpan(0, 0, 0, 5)).Should.BeTrue("OR Import/Staging 100 running unusually slow");
            Specify.That(results.Tests["1000"].Duration() < new TimeSpan(0, 0, 30, 0)).Should.BeTrue("OR Import/Staging 1000 running unusually slow");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void OrdersDropFileUntilJobSentToMachineQueue()
        {
            TestUtilities.DeleteLogFile(LogTypes.Regular);
            var fusionSim = new SimulatorUtilities().ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = new SimulatorUtilities().ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            new OrdersRepository(serviceLocator).DeleteAll();
            var results = new PerformanceTestResult() { SelectionAlgorithm = SelectionAlgorithmTypes.Order, Description = "Order Drop File Until Job Sent to Queue" };
            //var operCon = OperatorConsoleTests.OpenOperatorPageAndLoginAsAdmin(new MachineGroupRepository().FindByMachineGroupAlias(TestDatabaseManagement.TestMachineGroup1.Alias).Id.ToString());

            results.Tests.Add("warmup", MeasureTimeBetweenDropFileAndJobSentToQueue(1, fusionSim));
            results.Tests.Add("1", MeasureTimeBetweenDropFileAndJobSentToQueue(1, fusionSim));
            results.Tests.Add("10", MeasureTimeBetweenDropFileAndJobSentToQueue(10, fusionSim));
            results.Tests.Add("100", MeasureTimeBetweenDropFileAndJobSentToQueue(100, fusionSim));
            results.Tests.Add("1000", MeasureTimeBetweenDropFileAndJobSentToQueue(1000, fusionSim));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\OR_DropToJobSentToQueue_testresults.txt", results.ToString());

            Specify.That(results.Tests["1"].Duration() < new TimeSpan(0, 0, 0, 2)).Should.BeTrue("OR Import Til Job Sent To Queue 1 running unusually slow");
            Specify.That(results.Tests["10"].Duration() < new TimeSpan(0, 0, 0, 2)).Should.BeTrue("OR Import Til Job Sent To Queue 10 running unusually slow");
            Specify.That(results.Tests["100"].Duration() < new TimeSpan(0, 0, 0, 2)).Should.BeTrue("OR Import Til Job Sent To Queue 100 running unusually slow");
            Specify.That(results.Tests["1000"].Duration() < new TimeSpan(0, 0, 0, 2)).Should.BeTrue("OR Import Til Job Sent To Queue 1000 running unusually slow");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void OrdersMeasureTotalTimeToCompleteADrop()
        {
            new OrdersRepository(serviceLocator).DeleteAll();
            var simUtils = new SimulatorUtilities();
            var fusionSim = simUtils.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtils.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var results = new PerformanceTestResult() { SelectionAlgorithm = SelectionAlgorithmTypes.Order, Description = "Order Drop File Until Order Complete with Sim on 2 second loop" };

            results.Tests.Add("1", MeasureTimeToCompleteOrder(1, fusionSim, printerSim));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\OR_DropToJobComplete_testresults.txt", results.ToString());
        }
        #endregion

        #region Helper methods
        private TimeSpan MeasureTimeToCompleteOrder(int ordersToDrop, FusionSimulatorViewModel fusionSim, ZebraPrinterSimulatorViewModel printerSim)
        {
            var ordersRepo = new OrdersRepository(serviceLocator);
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();

            var closedOrders = new ClosedOrdersWidget();

            var stopWatch = Stopwatch.StartNew();
            MeasureImportComplete(ordersToDrop);
            bool allCartonsComplete = false;
            while (!allCartonsComplete)
            {
                SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printerSim);
                var allCartons = ordersRepo.All();
                var allIncompleteCartons = allCartons.Where(c => c.ProducibleStatus != ProducibleStatuses.ProducibleCompleted);
                allCartonsComplete = !allIncompleteCartons.Any();
            }
            stopWatch.Stop();
            return stopWatch.Elapsed;

        }

        private TimeSpan MeasureTimeBetweenDropFileAndJobSentToQueue(int cartonsToDrop, FusionSimulatorViewModel fusionSim)
        {
            TestUtilities.DeleteLogFile(LogTypes.Regular);
            DropFileUtilities.DropOrdersDropFile(cartonsToDrop);

            var stopWatch = Stopwatch.StartNew();
            while (!CheckFileForJobSentToQueueUpdate(cartonsToDrop))
                Thread.Sleep(1);
            stopWatch.Stop();

            fusionSim.StartStopPackaging();
            Thread.Sleep(500);
            fusionSim.StartStopPackaging();

            return stopWatch.Elapsed;
        }

        public bool CheckFileForJobSentToQueueUpdate(int cartons)
        {
            string[] lines = { };
            bool readSuccess = false, purgeSuccess = false;
            var logFilePath = Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log");

            var matchStrPt1 = "added to queue";
            var matchStrPt2 = string.Format("order1");

            while (!readSuccess)
            {
                try
                {
                    lines = File.ReadAllLines(logFilePath);
                    readSuccess = true;
                }
                catch { }
            }
            for (int i = lines.Length - 1; i > 0; i--)
            {
                if (lines[i].ToLower().Contains(matchStrPt1.ToLower()) && lines[i].ToLower().Contains(matchStrPt2))
                {
                    while (!purgeSuccess)
                    {
                        try
                        {
                            File.Delete(logFilePath);
                            purgeSuccess = true;
                        }
                        catch { }
                    }
                    return true;
                }
            }
            return false;
        }

        public TimeSpan MeasureImportComplete(int cartonsToDrop)
        {
            var fullFileName = DropFileUtilities.DropOrdersDropFile(cartonsToDrop);
            var filename = fullFileName.Split('\\')[fullFileName.Split('\\').Count() - 1];
            var sw = Stopwatch.StartNew();
            while (!CheckFileForImportUpdate(filename))
                Thread.Sleep(50);
            sw.Stop();
            return sw.Elapsed;
        }

        public TimeSpan MeasureDropFileAvailability(int cartonsToDrop, int qtyPerOrder = 1)
        {
            var ordersRepo = new OrdersRepository(serviceLocator);
            DropFileUtilities.DropOrdersDropFile(cartonsToDrop, qtyPerOrder);
            long count = 0;

            var sw = Stopwatch.StartNew();
            while (count < cartonsToDrop)
                count = ordersRepo.Count();

            sw.Stop();
            return sw.Elapsed;
        }
        #endregion
    }
}
