﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Enums;
using PackNet.Data.Machines;

using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.Macros;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;


namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class SearchPageTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public SearchPageTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            //TestDatabaseManagement.DropDatabase();
        }
        #endregion

        #region Tests
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("11314")]
        public void PrepareBoxFirstSearchData()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            //adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg5_fm1zp1zp2.SetId());
            //CustomJobMacros.Create(TestDatabaseManagement.CustomJob_h10w10l10, adminConsole);
            adminConsole.NavigateToPage(NavPages.SearchOrders);

            SearchPage page = new SearchPage(SelectionAlgorithmTypes.Order);

            page.SetStatus(JobStatus.Open);

            page.ClickSearchButton();
            page.ResultTable.SelectRow(0);
            //page.ClickToggleAdditionalOptionsButton();
            // general
            //page.SetStatus(JobStatus.Failed);
            //page.SetCustomerId("444");
            //// carton details
            //page.SetArticle("123");
            //page.SetDesign(Designs.IQ02000002MM);
            //page.SetLength(5);
            //page.SetWidth(6);
            //page.SetHeight(7);
            //// corrugate details
            //page.SetCorrugateName(TestDatabaseManagement.Corrugate_w2755t011.Alias);
            //page.SetCorrugateQuality(1);
            //// machine group
            //page.SetMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.Alias);
            //// machine
            //page.SetMachine(TestDatabaseManagement.Fm1.Alias);
            //// box first properties
            //page.SetPickZone(TestDatabaseManagement.PickZone1.Alias);
            //page.SetClassification(TestDatabaseManagement.Classification1.Alias);
            //page.SetCartonPropertyGroup(TestDatabaseManagement.Cpg1.Alias);esultTable.SetNumberToReproduce(0, 5);
            page.ClickCreateNextButton();
            page.ClickReproduceButton();


        }
        #endregion
    }
}
