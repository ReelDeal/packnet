﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.WebUITests.Repository.Performance;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Data.ScanToCreateProducible;
using PackNet.FusionSim.ViewModels;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;
using PackNet.Common.Interfaces.DTO.Corrugates;


namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class ScanToCreateOperatorConsoleTests : ExternalApplicationTestBase
    {
        SimulatorUtilities simUtilities = null;

        #region Constructor and initializers
        public ScanToCreateOperatorConsoleTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }

        protected override void BeforeTestInitialize()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.ScanToCreate);
            simUtilities = new SimulatorUtilities();
        }
        protected override void AfterClassCleanup()
        {
            simUtilities.Close();
            base.AfterClassCleanup();

        }
        #endregion

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void SetupScanToCreateEnviroment()
        {
            TestUtilities.DropDbAndRestart();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            ProductionGroupMacros.Build(TestDatabaseManagement.Pg16_stc_mg2_c199c2755, adminConsole); // fm1, zp1
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg17_stc_mg7_c199c2755, adminConsole); // fm2, fm3, zp3

            CorrugateMacros.Create(new List<Corrugate>() { TestDatabaseManagement.Corrugate_w22t011, 
                TestDatabaseManagement.Corrugate_w279t011, TestDatabaseManagement.Corrugate_w37t011 }, adminConsole);

            ArticleMacros.BuildArticles(new List<ArticleExt>() { TestDatabaseManagement.Article111, 
                TestDatabaseManagement.Article222, TestDatabaseManagement.Article333 }, adminConsole);

            //take DB snapshot
            TestDatabaseManagement.CreateMongoDump(SelectionAlgorithmTypes.ScanToCreate);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8115")]
        public void ScanToQueueCartonCanBeReproducedFromOperatorPanel()
        {
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = new MachineProductionPage(ProductionMode.Auto,SelectionAlgorithmTypes.ScanToCreate,UserType.Administrator);

            // set machine online
            page.ClickAutoAndPlayButtons();

            // enter and trigger the trigger interface
            page.TriggerJob("2010001:5:5:5");

            SimulatorHelper.ProduceCartonInQueue(fusionSim);

            // select and reproduce
            page.CompletedItemsTable.SelectRow(0);
            page.ClickReproduceOkButton();

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            // check that first row has status Completed
            page.CompletedItemsTable.VerifyCellValue(0, 1, JobStatus.Completed);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8116")]
        public void ScanToQueueCartonCanBeReproducedFromOperatorPanelSearch()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var mpPage = new MachineProductionPage(ProductionMode.Auto, SelectionAlgorithmTypes.ScanToCreate, UserType.Administrator);
            mpPage.ClickAutoAndPlayButtons();

            mpPage.TriggerJob("2010001:5:5:5");

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            adminConsole.NavigateToPage(NavPages.SearchScanToCreate);

            var searchPage = new SearchPage(SelectionAlgorithmTypes.ScanToCreate);
            searchPage.ClickSearchButton();

            searchPage.ResultTable.SelectRow(0);
            searchPage.ClickReproduceButton();

            adminConsole.NavigateToPage(NavPages.MachineProduction);

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8117")]
        public void ScanToQueueCartonCanBeReproducedFromAdminPanelSearch()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var mpPage = new MachineProductionPage(ProductionMode.Auto, SelectionAlgorithmTypes.ScanToCreate, UserType.Administrator);

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            mpPage.ClickAutoAndPlayButtons();

            mpPage.TriggerJob("2010001:5:5:5");

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            adminConsole.NavigateToPage(NavPages.SearchScanToCreate);

            var searchPage = new SearchPage(SelectionAlgorithmTypes.ScanToCreate);
            searchPage.ClickSearchButton();
            searchPage.ResultTable.SelectRow(0);

            searchPage.ClickReproduceButton();

            adminConsole.Close();
            Thread.Sleep(TimeSpan.FromSeconds(3));

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8118")]
        public void TriggerJobFromDimensionalDataUsingOperatorPanelTriggerInterfaceInScanToCreate()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();
            page.TriggerJob("2010001:5:5:5");

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8119")]
        public void ScanToQueueEnableDisableTriggerInterface()
        {
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var pgPage = new ProductionGroupPage();

            pgPage.ProductionGroupTable.SelectRowWithValue(TestDatabaseManagement.Pg16_stc_mg3_c199c2755.Alias, 0);
            pgPage.ClickEditButton();

            pgPage.VerifySTCShowTriggerInterfaceIsChecked();

            pgPage.ClickSaveButton();

            var mg2Guid = TestDatabaseManagement.Mg2_fm1zp1.SetId();

            var machineProdPage = MachineProductionPage.GetWindow();
            adminConsole.NavigateToPage(NavPages.MachineProduction);

            adminConsole.SelectMachineGroup(mg2Guid);
            machineProdPage.ClickAutoButton();

            //uncheck trigger interface button
            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            pgPage = new ProductionGroupPage();

            //verify that show trigger checkbox is checked
            pgPage.ProductionGroupTable.SelectRowWithValue(TestDatabaseManagement.Pg16_stc_mg3_c199c2755.Alias, 0);
            pgPage.ClickEditButton();

            pgPage.ClickSTCShowTriggerInterface(false);

            pgPage.ClickSaveButton();

            //verify trigger interface is NOT shown on operator console
            adminConsole.SelectMachineGroup(mg2Guid);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8121")]
        public void TriggerJobFromArticleUsingOperatorPanelTriggerInterfaceInScanToCreate()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var pgPage = new ProductionGroupPage();
            pgPage.ProductionGroupTable.SelectRowWithValue(TestDatabaseManagement.Pg16_stc_mg3_c199c2755.Alias, 0);
            pgPage.ClickEditButton();
            pgPage.ClickBarcodeParsingWorkflow(BarcodeParsingWorkflows.ParseArticleFromScanData);
            pgPage.ClickSaveButton();

            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var mpPage = MachineProductionPage.GetWindow();
            mpPage.ClickAutoAndPlayButtons();
            mpPage.TriggerJob("111");
            mpPage.TriggerJob("222:2");

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);
            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            mpPage.CompletedItemsTable.VerifyTableHasExpectedNumberOfRecords(3);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8124")]
        public void ShouldGiveErrorMessageInOpWhenPassingInvalidCustomData() //BUG:9298 blocking
        {
            //Start a sim
            var sim1 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var sim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm2);
            var sim3 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm3);

            //Navigate to the operator panel
            var mg2Guid = TestDatabaseManagement.Mg12_fm3zp2zp3.SetId();
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            //Click MG2
            adminConsole.SelectMachineGroup(mg2Guid);

            //Navigate to Machine production
            adminConsole.NavigateToPage(NavPages.MachineProduction);

            //Put MG2 in auto
            var page = MachineProductionPage.GetWindow();
            page.ClickAutoButton();
            
            //Using the trigger interface trigger 2000001;10;11;12;1 incorrect delimiter
            page.TriggerJob("2000001;10;11;12;1");

            //verify notifications
            adminConsole.VerifyNumberOfNotifications(1);
            adminConsole.VerifyNotificationContainsValue(0, "Invalid");

            //Repeat step above with incorrect number of fields
            page.TriggerJob("2000001;10;11;12;1");

            adminConsole.VerifyNumberOfNotifications(2);
            adminConsole.VerifyNotificationContainsValue(1, "Invalid");

            //clear notifications
            adminConsole.ClickDismissAllNotificationsButton();

            //Click MG1
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg12_fm3zp2zp3.SetId());

            //Using the trigger interface trigger 111;2 incorrect delimiter
            page.TriggerJob("111;2");

            adminConsole.VerifyNumberOfNotifications(1);
            adminConsole.VerifyNotificationContainsValue(0, "Invalid");
            adminConsole.ClickExpandCollapseNotificationsButton();

            //Using the trigger interface trigger 111;2 incorrect delimiter
            page.TriggerJob("111:2:2");
            adminConsole.VerifyNumberOfNotifications(2);
            adminConsole.VerifyNotificationContainsValue(1, "Invalid");
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8126")]
        public void ScannedToQueueJobsArePersistedWhenRestarting() //blocked by sim not getting jobs after restarting server
        {
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var mg1Guid = TestDatabaseManagement.Mg2_fm1zp1.SetId();
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.SelectMachineGroup(mg1Guid);

            var mpPage = MachineProductionPage.GetWindow();
            mpPage.ClickAutoAndPlayButtons();
            mpPage.TriggerJob("111");

            Thread.Sleep(3000);

            var scanToCreateRepository = new ScanToCreateRepository();
            var orderNumber = scanToCreateRepository.All().First();

            TestUtilities.RestartPackNetService();

            Thread.Sleep(3000);
            fusionSim.StartupComplete();

            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(); //create a new op
            adminConsole.SelectMachineGroup(mg1Guid);

            mpPage.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            mpPage.ClickAutoAndPlayButtons();

            SimulatorHelper.ProduceCartonInQueue(fusionSim, true);

            mpPage.CompletedItemsTable.VerifyContainsValue(orderNumber.Id.ToString());
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8129")]
        public void JobsShouldBeProducedByMostEfficientMGinPG()
        {
            //FusionMachine FusionMachine4 = new FusionMachine();
            //FusionMachine FusionMachine5 = new FusionMachine();
            //FusionMachine4 = new FusionMachine()
            //{
            //    Alias = "FM4",
            //    AssignedOperator = "",
            //    Description = "test4",
            //    IpOrDnsName = "127.0.0.1",
            //    Port = 8084,
            //    NumTracks = 2,
            //    PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\machineTheoretical_inches.xml"
            //};
            //FusionMachine5 = new FusionMachine()
            //{
            //    Alias = "FM5",
            //    AssignedOperator = "",
            //    Description = "test5",
            //    IpOrDnsName = "127.0.0.1",
            //    Port = 8085,
            //    NumTracks = 2,
            //    PhysicalSettingsFilePath = "Data\\PhysicalMachineSettings\\machineTheoretical_inches.xml"
            //};
            //add some more machines and PGs and MGs
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            ProductionGroupMacros.Build(TestDatabaseManagement.Pg10_boxfirst_mg3mg4_c199c2755c37, adminConsole);

            //MachineMacros.CreateMachine(TestDatabaseManagement.Fm1, adminConsole);
            //MachineMacros.CreateMachine(TestDatabaseManagement.Fm2, adminConsole);

            //MachineGroupTests.CreateMachineGroup(TestDatabaseManagement.Mg3_fm1);
            //MachineGroupTests.CreateMachineGroup(TestDatabaseManagement.Mg4_fm2);

            //MachineGroupTests.CreateNewMachineGroup("MG4", "", MGWorkflows.CreateProducibleOnMachineGroup, 2, new List<string>() { "FM4" }, adminConsole);
            //MachineGroupTests.CreateNewMachineGroup("MG5", "", MGWorkflows.CreateProducibleOnMachineGroup, 2, new List<string>() { "FM5" }, adminConsole);

            //ProductionGroupTests.CreateProductionGroup(TestDatabaseManagement.Pg10_boxfirst_mg3mg4_c199c2755c37, adminConsole);
            //ProductionGroupTests.CreateNewProductionGroup("PG3", "", TestDatabaseManagement.TestProductionGroupStc1.SelectionAlgorithm.DisplayName, 
            //    new List<string>() { TestDatabaseManagement.Mg3_fm1.Alias, TestDatabaseManagement.Mg4_fm2.Alias }, new List<string>() 
            //    { TestDatabaseManagement.Corrugate_w199t011.Alias, TestDatabaseManagement.Corrugate_w2755t011.Alias, TestDatabaseManagement.Corrugate_w37t011.Alias }, 
            //    "ParseCartonFromScanData", adminConsole: adminConsole);


            //load corrugates on machines
            //var mg3Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg3_fm1.Alias).Id;
            //var trackToCorrugate = new Dictionary<int, string>()
            //{
            //    { 2, TestDatabaseManagement.Corrugate_w37t011.Alias }
            //};
            //opConsole.SelectMachineGroup(mg3Guid);
            //OperatorConsoleTests.ChangeCorrugates(mg3Guid, 0, trackToCorrugate, opConsole);

            //var mg4Guid = new MachineGroupRepository().FindByAlias(TestDatabaseManagement.Mg4_fm2.Alias).Id;
            //var trackToCorrugate2 = new Dictionary<int, string>()
            //{
            //    { 1, TestDatabaseManagement.Corrugate_w199t011.Alias },
            //    { 2, TestDatabaseManagement.Corrugate_w2755t011.Alias }
            //};
            //OperatorConsole.SelectMachineGroup(mg4Guid, opConsole);
            //OperatorConsoleTests.ChangeCorrugates(mg4Guid, 0, trackToCorrugate2, opConsole);

            //Start a sim
            var fusionSim1 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var fusionSim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm2);

            var page = MachineProductionPage.GetWindow();

            //Click MG4
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg3_fm1.SetId());

            //Put MG5 in auto
            page.ClickAutoAndPlayButtons();

            //Using the trigger interface trigger
            page.TriggerJob("2000001:5:5:5:1");

            //Assert that machine in MG5
// TODO:            Specify.That(opConsole.JobsSentToMachineDiv.InnerText).Should.BeEmpty();

            //Click MG5
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg4_fm2.SetId());

            //Put MG5 in auto
            page.ClickAutoAndPlayButtons();

            SimulatorHelper.ProduceCartonInQueue(fusionSim2, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8130")]
        public void BarcodeScanDoesNotProduceArticleThatIsNotPresentInTheDatabase()
        {
            var sim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            adminConsole.NavigateToPage(NavPages.MachineProduction);

            var page = MachineProductionPage.GetWindow();

            page.VerifyIsMachineGroupStatus(MachineStatus.Paused);

            page.ClickAutoAndPlayButtons();

            page.TriggerJob("articleNotPresent");

            Thread.Sleep(3000);

            //verify notifications
            adminConsole.VerifyLastNotificationContainsValue("failed");

            //verify that the job is not sent to fusion 
            SimulatorHelper.VerifySimulatorHasItemsInQueue(sim);

            //verify that it is not in the scantocreaterepo
            var scanToCreateRepository = new ScanToCreateRepository();

            Retry.For(() => Specify.That(scanToCreateRepository.Count() == 0).Should.BeTrue("repo should not have a job in it"),
                TimeSpan.FromSeconds(10));


        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8316")]
        public void JobsShouldBeProducedByMostEfficientMachineInMG()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var pgPage = ProductionGroupPage.GetWindow();

            //add 37" corrugate to PG17
            pgPage.AddCorrugatesToExistingProductionGroup(TestDatabaseManagement.Pg17_stc_mg7_c199c2755,
                new List<string>() { TestDatabaseManagement.Corrugate_w37t011.Alias }, adminConsole);

            List<MachineTrackConnector> mtcList = new List<MachineTrackConnector>()
            {
                    new MachineTrackConnector(TestDatabaseManagement.Fm2, TestDatabaseManagement.Corrugate_w37t011),
                    new MachineTrackConnector(TestDatabaseManagement.Fm2, null,2)
             };

            ////load corrugates on machines
            //var trackToCorrugate = new Dictionary<int, string>()
            //{
            //    { 1, TestDatabaseManagement.Corrugate_w37t011.Alias },
            //    { 2, TestDatabaseManagement.CorrugateNotLoaded }
            //};

            adminConsole.NavigateToPage(NavPages.ChangeCorrugate);

            MachineGroupExt mg = TestDatabaseManagement.Pg17_stc_mg7_c199c2755.MachineGroups[0];
            adminConsole.SelectMachineGroup(mg.SetId());

            ChangeCorrugatePage ccPage = new ChangeCorrugatePage();
            ccPage.ChangeCorrugateForOneMachine(0, mtcList);

            adminConsole.SelectMachineGroup(mg.Id);

            //Start a sim
            var sim2 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm2);
            var sim3 = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm3);

            // TODO: Retry.For(() => Specify.That(opConsole.MachineGroupStatus.InnerText.Contains("Paused")).Should.BeTrue("MG never got out of Initializing"),
            //    TimeSpan.FromSeconds(60));

            var mpPage = MachineProductionPage.GetWindow();

            //Put MG5 in auto
            mpPage.ClickAutoAndPlayButtons();
            mpPage.TriggerJob("2000001:5:5:5:1");

            //verify that the job is not sent to fusion 2 with the 37" corrugate
            SimulatorHelper.VerifySimulatorHasItemsInQueue(sim2, false);

            //complete job on correct sim
            // TODO: CompleteJobOnSim(opConsole, sim3);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void ScanToCreateTriggeredUntilJobSentToMachineQueue()
        {
            var simUtilities = new SimulatorUtilities();
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printerSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
 
            new ScanToCreateRepository().DeleteAll();

            var results = new PerformanceTestResult() { SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate, 
                Description = "STC Triggered Until Job Sent to Queue" };

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            results.Tests.Add("warmup", MeasureTimeBetweenTriggerAndJobSentToQueue("111", adminConsole));
            results.Tests.Add("111", MeasureTimeBetweenTriggerAndJobSentToQueue("111", adminConsole));
            results.Tests.Add("111:1", MeasureTimeBetweenTriggerAndJobSentToQueue("111:1", adminConsole));
            results.Tests.Add("111:10", MeasureTimeBetweenTriggerAndJobSentToQueue("111:10", adminConsole));
            results.Tests.Add("average", new TimeSpan(Convert.ToInt64(results.Tests.Average(t => t.Value.Ticks))));

            new PerformanceTestResultRepository().Create(results);
            File.AppendAllText(@"C:\STC_TriggeredUntilJobSentToQueue_testresults.txt", results.ToString());
        }

        private TimeSpan MeasureTimeBetweenTriggerAndJobSentToQueue(string cartonToTrigger, AdminConsole adminConsole)
        {
            new ScanToCreateRepository().DeleteAll();

            File.Delete(Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log"));

            Thread.Sleep(2000);

            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId());

            var page = MachineProductionPage.GetWindow();
            page.ClickAutoAndPlayButtons();
            page.TriggerJob(cartonToTrigger);

            var stopWatch = Stopwatch.StartNew();
            while (!CheckFileForJobSentToQueueUpdate())
                Thread.Sleep(1);
            stopWatch.Stop();

            return stopWatch.Elapsed;
        }

        public bool CheckFileForJobSentToQueueUpdate()
        {
            string[] lines = { };
            bool readSuccess = false, purgeSuccess = false;
            var logFilePath = Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log");
            var matchStrA = "AddProductionItemToQueue|AddProductionItemToQueue for item Carton ProducibleId:";
            var matchStrB = "QueueLength:";

            while (!readSuccess)
            {
                try
                {
                    lines = File.ReadAllLines(logFilePath);
                    readSuccess = true;
                }
                catch { }
            }
            for (int i = lines.Length - 1; i > 0; i--)
            {
                if (lines[i].ToLower().Contains(matchStrA.ToLower()) || lines[i].ToLower().Contains(matchStrB.ToLower()))
                {
                    while (!purgeSuccess)
                    {
                        try
                        {
                            File.Delete(logFilePath);
                            purgeSuccess = true;
                        }
                        catch { }
                    }
                    return true;
                }
            }
            return false;
        }


    }
}
