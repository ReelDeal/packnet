﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Specificity;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.IODeviceService.DTO.Accessories;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    /// <summary>
    /// Summary description for MachineManagerProductionGroupTests
    /// </summary>
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    public class MachineGroupTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public MachineGroupTests()
            : base(TestSettings.Default.MachineManagerProcessName, Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe), false, true)
        {
        }

        protected override void BeforeTestInitialize()
        {
            TestDatabaseManagement.DropDatabase();
            TestUtilities.RestartPackNetService();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void MachineGroupsWindowOpens()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.MachineGroup);
            var page = new MachineGroupPage();
            page.ClickNewButton();
            page.VerifyEditPageIsOpen();
        }

        /// <summary>
        /// This will create a machine group.
        /// </summary>
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6311")]
        public void CreateNewMachineGroup()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            MachineMacros.Create(TestDatabaseManagement.Fm1, adminConsole);
            MachineGroupMacros.Create(TestDatabaseManagement.Mg3_fm1, adminConsole);
            MachineGroupMacros.VerifyMachineGroup(0, TestDatabaseManagement.Mg3_fm1);
        }

        /// <summary>
        /// This will edit a machine group.
        /// </summary>
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6346")]
        public void EditMachineGroup()
        {
            var mg = TestDatabaseManagement.Mg3_fm1;

            // Start Admin Console before test execution
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            MachineMacros.Create(TestDatabaseManagement.Fm1, adminConsole);
            MachineMacros.Create(TestDatabaseManagement.Fm2, adminConsole);

            var page = MachineGroupPage.GetWindow();

            page.Create(mg, adminConsole);

            page.UpdateInfo(0, mg, false);

            //change from machine 1 to machine 2
            page.SetMachine(TestDatabaseManagement.Fm1.Alias, false);
            page.SetMachine(TestDatabaseManagement.Fm2.Alias);

            //save
            page.ClickSaveButton();

            //verify that the correct alert is shown
            page.VerifyUpdatedSuccessfulMessage(mg.Alias);

            // verify the edit page is closed
            page.VerifyEditPageIsOpen(false);

            MachineGroupMacros.VerifyMachineGroup(0, mg);
        }

        /// <summary>
        /// This will delete a machine group.
        /// </summary>
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6347")]
        public void DeleteMachineGroup()
        {
            var mg = TestDatabaseManagement.Mg1_em6;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = new MachineGroupPage();

            MachineMacros.Create(TestDatabaseManagement.Em6, adminConsole);

            MachineGroupMacros.Create(mg, adminConsole);

            var machineGroupCount = page.MachineGroupTable.RowCount();

            page.PageTableSelectRow(0);

            page.VerifyDeleteButtonIsEnabled();

            page.ClickDeleteButton();

            page.VerifyDeleteSuccessfulMessage(mg.Alias);

            page.MachineGroupTable.VerifyTableHasExpectedNumberOfRecords(machineGroupCount - 1);

            MachineGroupMacros.VerifyMachineGroupExistsInDatabase(mg.Alias, false);
        }

        /// <summary>
        /// Temp test to work on Numeric up down.
        /// </summary>
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6347")]
        public void TestNumericUD()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.MachineGroup);

            var queueLength = 44;
            var page = new MachineGroupPage();
            page.ClickNewButton();
            page.SetName("MG1");
            page.SetQueueLengthWithUpDownButtons(queueLength);
            page.VerifyQueueLength(queueLength);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10278")]
        public void VerifyNoDuplicateMachineGroupName()
        {
            var mg = TestDatabaseManagement.Mg3_fm1;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            MachineMacros.Create(TestDatabaseManagement.Fm1, adminConsole);

            var page = MachineGroupPage.GetWindow();
            page.Create(mg, adminConsole);
            page.Create(mg, adminConsole, false, false);
            page.VerifyDuplicateAliasMessage(mg.Alias);
            page.VerifyEditPageIsOpen();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9977")]
        public void TrackOverlappingMessageWillShow()
        {
            var steps = new TestSteps { new TestStep(TestInserts.VerifyOverlappingTracks) };
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg18_orders_Mg1_c2755c4724, null, steps);
        }
        #endregion

    }
}
