﻿using System;
using System.IO;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Utils;
using PackNet.Data.CartonPropertyGroups;

using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class CartonPropertyGroupsTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public CartonPropertyGroupsTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyCreateCPG()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = CartonPropertyGroupsPage.GetWindow();

            page.Create(TestDatabaseManagement.Cpg1, adminConsole);

            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.Cpg1.Alias);

            CPGMacros.VerifyCPGIsSavedToDatabase(TestDatabaseManagement.Cpg1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyCreateCPGAliasRequired()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.CartonPropertyGroup);
            var page = CartonPropertyGroupsPage.GetWindow();

            page.ClickNewButton();
            page.VerifyEditPageIsOpen();
            page.ClickSaveButton();

            //Verify the error is shown
            page.VerifyNameValidation();

            page.SetName("value");

            //Verify the error goes away
            page.VerifyNameValidation(false);

            page.ClickCancelButton();

            //Verify that the new dialog was closed
            page.VerifyEditPageIsOpen(false);

            //Verify data is not in the table
            page.VerifyTableIsEmpty();

            //verify entry is not in DB
            CPGMacros.VerifyCPGExistsInDatabase(TestDatabaseManagement.Cpg1.Alias, false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10273")]
        public void VerifyDuplicateCPGAlias()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = CartonPropertyGroupsPage.GetWindow();

            page.Create(TestDatabaseManagement.Cpg1, adminConsole);

            //Verify row in table
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.Cpg1.Alias);

            //Create the same entry
            page.Create(TestDatabaseManagement.Cpg1, adminConsole, false);

            page.VerifyDuplicateAliasMessage(TestDatabaseManagement.Cpg1.Alias);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyDeleteCPG()
        {
            //Open admin page, login, navigate to cpg page
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = CartonPropertyGroupsPage.GetWindow();

            page.Create(TestDatabaseManagement.Cpg1, adminConsole);

            //Verify cpg in table
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.Cpg1.Alias);

            //Verify Delete is disabled
            page.VerifyDeleteButtonIsEnabled(false);

            //Select the row
            page.PageTableSelectRow(0);

            //Verify Delete is now enabled
            page.VerifyDeleteButtonIsEnabled();

            //Delete the cpg
            page.ClickDeleteButton();

            // verify the message is correct
            page.VerifyDeleteSuccessfulMessage(TestDatabaseManagement.Cpg1.Alias);

            // verify that the table is empty
            page.CPGTable.RefreshData();
            page.VerifyTableIsEmpty();

            //Verify no entry in db
            CPGMacros.VerifyCPGExistsInDatabase(TestDatabaseManagement.Cpg1.Alias, false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyUpdateCPG()
        {
            //Open admin page, login, navigate to page
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = CartonPropertyGroupsPage.GetWindow();

            page.Create(TestDatabaseManagement.Cpg1, adminConsole);

            var cpg1Guid = CPGMacros.FindByAlias(TestDatabaseManagement.Cpg1.Alias).Id;

            // verify row in table
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.Cpg1.Alias);

            // verify the record in the database
            CPGMacros.VerifyCPGExistsInDatabase(TestDatabaseManagement.Cpg1.Alias);

            page.UpdateInfo(0, TestDatabaseManagement.Cpg2);

            //Verify row update in table
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.Cpg2.Alias);

            //Verify edit in the db
            CPGMacros.VerifyCPGIsSavedToDatabase(TestDatabaseManagement.Cpg2, cpg1Guid);
        }

        #endregion
    }
}

