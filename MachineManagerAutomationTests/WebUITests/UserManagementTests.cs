﻿using System;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using PackNet.Common.Utils;
using PackNet.Common.Interfaces.DTO.Users;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{

    [CodedUITest]
    public class UserManagementTests : ExternalApplicationTestBase
    {
        #region Variables
        public static readonly string OperatorGroup = "operators";
        public static readonly string Operator1Username = "op1";
        public static readonly string Operator2Username = "op2";
        public static readonly string OperatorBlankPassword = "";
        public static readonly string OperatorPassword = "pass";
        #endregion

        #region Constructors and initiators
        public UserManagementTests()
            : base(TestSettings.Default.MachineManagerProcessName, TestUtilities.MachineManagerWindowLocation)
        {
        }

        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
            TestDatabaseManagement.AddUserToDataBase(TestDatabaseManagement.AdminUser);
        }

        [TestCleanup]
        public void CleanUp()
        {
            TestDatabaseManagement.CascadedTruncateOfTableInDatabase(DatabaseTables.Users);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            if (Playback.IsInitialized)
            {
                Playback.Cleanup();
            }
        }
        #endregion

        #region Tests
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("158")]
        public void AdminCanLoginToConsole()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.VerifyDashboardIsAvailable();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void AdminCanCreateAUser()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.User);
            
            var page = UserManagementPage.GetWindow();

            // verify our operator user doesn't exist yet
            page.SearchForUserAndVerifyItExists(TestDatabaseManagement.QaUser.UserName, false);

            // create new operator user
            UserMacros.Create(TestDatabaseManagement.QaUser, adminConsole);

            // verify our operator user was created correctly
            page.SearchForUserAndVerifyItExists(TestDatabaseManagement.QaUser.UserName, true, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("160")]
        public void AutoLoginUserShouldBeLoggedInWithoutPrompt()
        {
            const string password = "SuperPwd";

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var firstUser = TestUtilities.GetTimestamp().Substring(8);
            UserMacros.Create(UserMacros.CreateUserObject(firstUser, password, false, false), adminConsole);

            var secondUser = TestUtilities.GetTimestamp().Substring(8);
            UserMacros.Create(UserMacros.CreateUserObject(secondUser, password, true, false), adminConsole);

            adminConsole.Logout();

            var loginPage = DynamicBrowserWindowWithLogin.GetWindow();
            loginPage.Login(secondUser, password);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("174")]
        public void ShouldNotBeAbleToLogInIntoAPageRequiringPermissionsWithInvalidPermissionsOrCredentials()
        {
            TestDatabaseManagement.AddUserToDataBase(Operator1Username, OperatorPassword);

            //verify operator can log in
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministratorWithLoginData<AdminConsole>(Operator1Username, OperatorPassword);
            Specify.That(adminConsole.Uri.ToString().ToLower().Contains("notauthorized")).Should.BeTrue("Operator user should not have permissions to see Admin Console page");
            
            //log out
            adminConsole.Logout();

            DynamicBrowserWindowWithLogin login = DynamicBrowserWindowWithLogin.GetWindow();

            //non existent user should not be able to log in
            login.Login(Operator2Username, OperatorPassword);

            //invalid user should not be able to log in
            login.SetLoginUsername("usser");
            login.SetLoginPassword("users");
            login.VerifyLoginButtonIsEnabled(true, "Login button is disabled even though username and password boxes are not empty");
            login.ClickLoginButton();
            login.LoginDialogAlerts.VerifyAlert(true, AlertStates.Danger, "Invalid Username and/or Password");

            //invalid user should not be able to log in
            login.SetLoginUsername("op");
            login.SetLoginPassword("users");
            login.ClickLoginButton();
            login.LoginDialogAlerts.VerifyAlert(true, AlertStates.Danger, "Invalid Username and/or Password");

            login.SetLoginUsername(TestSettings.Default.MachineManagerAdminUserName);
            login.SetLoginPassword(TestSettings.Default.MachineManagerAdminPassword);
            login.ClickLoginButton();
        }


        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("993")]
        public void LoginToOperatorConsoleWithBlankPassword()
        {
            TestDatabaseManagement.AddUserToDataBase(TestSettings.Default.MachineManagerOperatorUserName, TestSettings.Default.MachineManagerOperatorPassword);
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministratorWithLoginData<AdminConsole>(Operator1Username, "");
            adminConsole.VerifyMessageWhenLogInWithoutPassword();
        }

        #endregion
    }
}
