﻿//using System;
//using System.Reflection;

//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Microsoft.VisualStudio.TestTools.UITesting;

//using Testing.Specificity;

//using PackNet.Common.Utils;

//using MachineManagerAutomationTests.Helpers;
//using MachineManagerAutomationTests.WebUITests.Repository;

//namespace MachineManagerAutomationTests.WebUITests
//{
//    /// <summary>
//    /// Summary description for SplashScreenExternalApplicationTests
//    /// </summary>
//    [CodedUITest]
//    public class MachinesListTests : ExternalApplicationTestBase
//    {
//        #region Constructor and initializers
//        public MachinesListTests()
//            : base(TestSettings.Default.MachineManagerProcessName, TestUtilities.MachineManagerWindowLocation)
//        {
//        }

//        protected override void BeforeTestInitialize()
//        {
//        }

//        protected override void AfterTestInitialize()
//        {
//        }
//        #endregion

//        //TODO: Admin splash page 1627
//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("2697")]
//        public void MachinesListContainsVersionInformation()
//        {
//            var opConsole = TestUtilities.CreatePageAndLoginAsAdministrator<OperatorConsole>();

//            // goto SelectMachineGroup ok?
//            opConsole.NavigateToPage(OpNavPages.MachineProduction);

//            var machinesListPage = new MachinesListWindow();
//            Retry.For(() => machinesListPage.GetButtonForMachineId(1).Enabled, TimeSpan.FromSeconds(10));
//            Specify.That(machinesListPage.GetButtonForMachineId(1).Enabled).Should.BeTrue();
//            Specify.That(machinesListPage.GetButtonForMachineId(2).Enabled).Should.BeTrue();
//            Specify.That(machinesListPage.GetButtonForMachineId(3).Enabled).Should.BeTrue();
//            Specify.That(machinesListPage.GetButtonForMachineId(1111).Enabled).Should.BeTrue();
//            Specify.That(machinesListPage.GetButtonForMachineId(5).Enabled).Should.BeTrue();

//            var machineManagerWindowAssembly = Assembly.LoadFile(ExePath);

//            VerifyAboutPage(machineManagerWindowAssembly.GetName().Version.ToString());
//        }

//    }
//}
