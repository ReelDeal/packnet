﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using PackNet.Common.Interfaces.Enums;
using PackNet.Data.Corrugates;
using PackNet.Data.ProductionGroups;
using PackNet.Data.UserManagement;
using PackNet.Common.Utils;
using PackNet.Data.CartonPropertyGroups;
using PackNet.Data.Classifications;
using PackNet.Data.PickArea;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    public class ProductionGroupTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public ProductionGroupTests()
            : base(TestSettings.Default.MachineManagerProcessName, Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe), false, true)
        {
        }

        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6340")]
        public void CreateNewProductionGroup()
        {
            CreateNewProductionGroupHelper();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6351")]
        public void DeleteProductionGroup()
        {
            // create
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg1_orders_mg1_c2755, adminConsole); 

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var page = new ProductionGroupPage();
            var pgCount = page.ProductionGroupTable.RowCount();
            page.PageTableSelectRow(0);
            page.ClickDeleteButton();

            //Check the PG row in the UI is gone
            page.VerifyNumberOfRecordsIsAsExpected(pgCount - 1);

            //Check the DB to make sure the PG is gone
            ProductionGroupMacros.VerifyProductionGroupExistsInDatabase(TestDatabaseManagement.Pg1_orders_mg1_c2755.Alias, false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6343")]
        public void EditProductionGroup()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            CreateNewProductionGroupHelper(adminConsole);
            EditProductionGroupHelper(adminConsole);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6393")]
        public void ProductionGroupCrudValidations()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.ProductionGroup);

            var page = new ProductionGroupPage();

            page.ClickNewButton();

            //Verify there are no Validation messages present when first opening
            page.VerifyNameValidationIsDisplayed(false);
            page.VerifyProductionModeValidationIsDisplayed(false);

            //Verify Clicking Save causes validations to appear
            page.ClickSaveButton();
            page.VerifyNameValidationIsDisplayed(true);

            //close and re-open to test individual field triggers
            page.ClickCancelButton();
            page.ClickNewButton();

            //click into each field to trigger validations and verify
            page.ClickNameField();
            page.ClickProductionModeMenu();
            page.ClickDescriptionField();
            page.ClickNameField();

            page.VerifyNameValidationIsDisplayed(true);
            page.VerifyProductionModeValidationIsDisplayed(true);

            page.SetName("PG1");
            page.SetDescription("PG1 desc");
            page.SetProductionModeMenu(ProductionModesFriendlyNames.BoxLast);

            page.VerifyNameValidationExists(false);
            page.VerifyProductionModeValidationExists(false);

            //Save this PG so we can check edit validation
            page.ClickSaveButton();

            page.PageTableSelectRow(0);

            page.ClickEditButton();

            //Verify no validation present when first opening an Edit
            page.ClickNameField();
            page.VerifyNameValidationExists(false);
            page.VerifyProductionModeValidationExists(false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10282")]
        public void VerifyNoDuplicateProductionGroupName()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var pg = TestDatabaseManagement.Pg13_boxlast_mg2_c199c2755;
            var page = ProductionGroupPage.GetWindow();
            page.Create(pg, adminConsole);
            page.Create(pg, adminConsole);
            page.VerifyDuplicateAliasMessage(pg.Alias);
        }

        #endregion

        #region Helper methods

        public static void CreateNewProductionGroupHelper(AdminConsole adminConsole = null)
        {
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg13_boxlast_mg2_c199c2755, adminConsole);
        }

        public static void EditProductionGroupHelper(AdminConsole adminConsole)
        {
            var updatedPg = TestDatabaseManagement.Pg11_boxlast_mg10_c279;

            CorrugateMacros.Create(TestDatabaseManagement.Corrugate_w279t011, adminConsole);

            MachineGroupMacros.Create(TestDatabaseManagement.Mg9_noMachine, adminConsole);

            // these are extra so that the editing of the pg removes the checkboxes in the list and checks the ones in 
            // pg.MachineGroups and pg.Corrugates
            var extraValues = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("MachineGroup:" + TestDatabaseManagement.Mg2_fm1zp1.Alias, "false"),
                new KeyValuePair<string, string>("Corrugate:" + TestDatabaseManagement.Corrugate_w199t011.Alias, "false"),
                new KeyValuePair<string, string>("Corrugate:" + TestDatabaseManagement.Corrugate_w2755t011.Alias, "false")
            };

            var page = ProductionGroupPage.GetWindow();
            page.UpdateProductionGroup(0, updatedPg, adminConsole, extraValues);
            page.VerifyTableOneCorrugateAndMachineGroup(0, updatedPg);

            //Verify the object coming from the DB matches as well
            var pgFromDb = ProductionGroupMacros.VerifyProductionGroupIsSavedToDatabase(updatedPg, updatedPg.Id);

            //assumes that the edited PG only has one corrugate
            ProductionGroupMacros.VerifyOneCorrugateInProductionGroup(pgFromDb, TestDatabaseManagement.Corrugate_w279t011);
        }

        #endregion
    }
}
