﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Specificity;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Data.PrintMachines;
using PackNet.Data.BarcodeScanner;
using PackNet.IODeviceService.DTO.Accessories;
using MachineManagerAutomationTests.Macros;


namespace MachineManagerAutomationTests.WebUITests
{
    /// <summary>
    /// Summary description for MachineManagerProductionGroupTests
    /// </summary>
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    public class MachinesTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public MachinesTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6322")]
        public void AddNewEmMachine()
        {
            MachineMacros.Create(TestDatabaseManagement.Em7);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6488")]
        public void AddNewFusionMachine()
        {
            MachineMacros.Create(TestDatabaseManagement.Fm1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6335")]
        public void AddNewPrinter()
        {
            MachineMacros.Create(TestDatabaseManagement.Zp1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6352")]
        public void AddNewScanner()
        {
            MachineMacros.Create(TestDatabaseManagement.Scanner1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6325")]
        public void EditFusionMachine()
        {
            EditMachine(TestDatabaseManagement.Fm1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6338")]
        public void EditPrinter()
        {
            EditMachine(TestDatabaseManagement.Zp1);
        }
        
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6359")]
        public void EditScanner()
        {
            EditMachine(TestDatabaseManagement.Scanner1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6337")]
        public void DeletePrinter()
        {
            DeleteMachine(TestDatabaseManagement.Zp1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6324")]
        public void DeleteFusionMachine()
        {
            DeleteMachine(TestDatabaseManagement.Fm1);
        }
        
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6358")]
        public void DeleteScanner()
        {
            DeleteMachine(TestDatabaseManagement.Scanner1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10270")]
        public void VerifyNoDuplicateMachineName()
        {
            var em7 = TestDatabaseManagement.Em7;
            var zp1 = TestDatabaseManagement.Zp1;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = MachineManagementPage.GetWindow();

            page.Create(em7, adminConsole, true);
            
            // create a new machine with the same name but with another port so they don't collide
            em7.Port = em7.Port + 1;
            page.Create(em7, adminConsole);

            // verify that an alert appears saying a machine with this name already exists
            page.VerifyDuplicateAliasMessage(em7.Alias, MachineMacros.GetMachineTypeFriendlyName(MachineType.EM));

            // close page
            page.ClickCancelButton();

            // verify duplicated machine name wasn't added since already exists
            page.VerifyTableIsEmpty(MachineMacros.GetMachineTypeFriendlyName(MachineType.EM), 1);

            // verify existing Em machine is still in the table
            page.VerifyMachineTableRowContainsSpecifiedData(0, em7.Alias, MachineMacros.GetMachineTypeFriendlyName(MachineType.EM));

            // in the original test case it was not allowed to save machines of different types 
            // with the same name, but this has changed so now it's allowed

            //// create a printer with the same name. it will trigger a message indicating machine already exists
            zp1.Alias = em7.Alias;
            page.Create(zp1, adminConsole, true);
 
            // create a scanner with the same name. it will trigger a message indicating machine already exists
            var scanner = TestDatabaseManagement.Scanner1;
            scanner.Alias = em7.Alias;
            page.Create(scanner, adminConsole, true);
        }

        public void DeleteMachine(IMachine machine)
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = MachineManagementPage.GetWindow();

            MachineMacros.Create(machine, adminConsole);

            page.VerifyDeleteButtonIsEnabled(false);

            page.PageTableSelectRow(0);

            page.VerifyDeleteButtonIsEnabled();

            var machineCount = page.MachinesTable.RowCount();

            page.ClickDeleteButton();

            page.VerifyDeleteSuccessfulMessage(machine.MachineAlias, MachineMacros.GetMachineTypeFriendlyName(MachineMacros.GetMachineType(machine)));

            page.VerifyTableIsEmpty(MachineMacros.GetMachineTypeFriendlyName(MachineMacros.GetMachineType(machine)));

            MachineMacros.VerifyDataTableIsNotEmpty(MachineType.EM);
        }

        public void EditMachine(IMachine machine)
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.Machine);

            var page = MachineManagementPage.GetWindow();

            page.Create(machine, adminConsole);

            page.PageTableSelectRow(0);

            machine.MachineAlias += "(Edited)";
            machine.MachineDescription += "(Edited)";

            page.UpdateMachineInfo(0, machine, adminConsole);

            page.VerifyMachineTableRowContainsSpecifiedData(0, machine.MachineAlias, null);

            MachineMacros.VerifyMachineIsSavedToDatabase(MachineMacros.GetMachineType(machine), machine.MachineAlias);

        }

        #endregion
    }
}
