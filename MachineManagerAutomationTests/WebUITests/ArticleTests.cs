﻿//using System;
//using System.IO;
//using System.Threading;

//using MachineManagerAutomationTests.Helpers;
//using MachineManagerAutomationTests.WebUITests.Repository;

//using Microsoft.VisualStudio.TestTools.UITesting;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//using PackNet.Common.Interfaces.DTO;
//using PackNet.Common.Interfaces.DTO.Scanning;
//using PackNet.Common.Utils;

//using Testing.Specificity;

//namespace MachineManagerAutomationTests.WebUITests
//{
//    [CodedUITest]
//    public class ArticleTests : ExternalApplicationTestBase
//    {
//        public static string _pageUrl = @"index.html#/Articles";

//        /// <summary>
//        /// The action TestDatabaseManagement.CascadedTruncateAllTablesInDatabase(); truncates database tables prior to the run of each of these tests
//        /// </summary>
//        public ArticleTests()
//            : base(
//                TestSettings.Default.MachineManagerProcessName,
//                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
//                TestSettings.Default.CloseAppBetweenTests)
//        {
//            ForceApplicationCloseAtStartup = true;
//        }

//        protected override void BeforeTestInitialize()
//        {
//            TestDatabaseManagement.DropDatabase();
//            TestUtilities.RestartPackNetService();
//        }

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        public void ShouldbePromptedForUsernameAndPasswordWhenNotLoggedIn()
//        {
//            //var articlesWindow = TestUtilities.CreatePage<ArticlesManagement>(_pageUrl);
//            //Specify.That(articlesWindow).Should.Not.BeNull("Articles windows wasn't opened.");
//            //Specify.That(articlesWindow.LoginButton.Exists).Should.BeTrue("Login didn't appear");
//        }

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        public void AdminShouldbeAbleToLogIn()
//        {
//            //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);
//        }

        

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("184")]
//        public void CreateArticle()
//        {
//            //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);

//            //var article = new Article
//            //              {
//            //                  ArticleId = Guid.NewGuid().ToString(),
//            //                  Description = "Box for my rocks",
//            //                  Length = 10,
//            //                  Width = 10,
//            //                  Height = 10,
//            //                  DesignId = 2000001,
//            //                  DesignName = Designs.IQ02000001INCHES
//            //              };
//            //CreateNewArticle(articlesWindow, article);

//            //VerifyArticleInTable(articlesWindow, 0, article);

//            //EditArticleAt(0, 1, articlesWindow);

//            //CloseEditArticle(articlesWindow);
//        }

       

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("154")]
//        public void ArticleManagementWindowButtonStatesAndSearch()
//        {
//            //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);

//            //Specify.That(articlesWindow.ArticleButtonNew.Enabled).Should.BeTrue("new button is not enabled");
//            //Specify.That(articlesWindow.ArticleButtonCopy.Enabled).Should.BeFalse("copy button is not disabled");
//            //Specify.That(articlesWindow.ArticleButtonEdit.Enabled).Should.BeFalse("edit button is not disabled");
//            //Specify.That(articlesWindow.ArticleButtonDelete.Enabled).Should.BeFalse("delete button is not disabled");
//            //Specify.That(articlesWindow.ArticleButtonProduce.Enabled).Should.BeFalse("produce button is not enabled");
//            //Specify.That(articlesWindow.ArticlesTableDiv.Enabled).Should.BeTrue("Articles table doesn't exist");

//            ////We need at least two articles
//            //var articleToSearchFor = string.Empty;

//            //for (int i = 0; i < 2; i++)
//            //{
//            //    var article = new Article
//            //    {
//            //        ArticleId = Guid.NewGuid().ToString(),
//            //        Description = "Box for my rocks",
//            //        Length = 10,
//            //        Width = 10,
//            //        Height = 10,
//            //        DesignId = 2000001
//            //    };
//            //    CreateNewArticle(articlesWindow, article);
//            //    articleToSearchFor = article.ArticleId;
//            //}
            
//            //articlesWindow.ArticlesTable.SelectRow(0);
            
//            //Specify.That(articlesWindow.ArticleButtonNew.Enabled).Should.BeTrue("new button is not enabled");
//            //Specify.That(articlesWindow.ArticleButtonCopy.Enabled).Should.BeTrue("copy button is not enabled");
//            //Specify.That(articlesWindow.ArticleButtonEdit.Enabled).Should.BeTrue("edit button is not enabled");
//            //Specify.That(articlesWindow.ArticleButtonDelete.Enabled).Should.BeTrue("delete button is not enabled");
//            //Specify.That(articlesWindow.ArticleButtonProduce.Enabled).Should.BeTrue("produce button is not enabled");

//            //articlesWindow.ArticlesTable.SelectRow(1);
            
//            //Specify.That(articlesWindow.ArticleButtonNew.Enabled).Should.BeTrue("new button is not enabled");
//            //Specify.That(articlesWindow.ArticleButtonCopy.Enabled).Should.BeFalse("copy button is not disabled");
//            //Specify.That(articlesWindow.ArticleButtonEdit.Enabled).Should.BeFalse("edit button is not disabled");
//            //Specify.That(articlesWindow.ArticleButtonDelete.Enabled).Should.BeTrue("delete button is not enabled");
//            //Specify.That(articlesWindow.ArticleButtonProduce.Enabled).Should.BeTrue("produce button is not enabled");

//            //articlesWindow.ArticlesTable.SearchToggle.Click();
//            //Specify.That(articlesWindow.ArticlesTable.SearchEdit.Enabled).Should.BeTrue("search did not get enabled");
//            //articlesWindow.ArticlesTable.SearchEdit.SetText(articleToSearchFor);

//            //Specify.That(articlesWindow.ArticlesTable.RowCount() == 1).Should.BeTrue("filter didn't work");
//        }

        

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("157")]
//        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
//        public void ArticleImportExport()
//        {
//            ArticleHelper.Import100Articles();

//            var fileLocation = ExportArticles();
//            Thread.Sleep(5000);
//            var fi = new FileInfo(fileLocation);
//            Thread.Sleep(5000);

//            Retry.For(() => fi.Exists, TimeSpan.FromSeconds(10));

//            Specify.That(fi.Exists).Should.BeTrue(String.Format("Could not find temp export file {0} {1} ", fileLocation, fi));
//            Specify.That(File.ReadAllLines(fileLocation).Length).Should.BeEqualTo(101); // 100 articles + header row

//        }

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("2736")]
//        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
//        public void ArticlesWindowDeleteAllArticles()
//        {
//            //ArticleHelper.Import100Articles();

//            //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);
                        
//            //DeleteAllArticles(articlesWindow);

//            //Specify.That(articlesWindow.ArticlesTable.RowCount() == 0).Should.BeTrue("The articles were not deleted.");
//        }

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("183")]
//        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
//        public void ArticlesWindowDeleteArticle()
//        {
//            //ArticleHelper.Import100Articles();

//            //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);
                        
//            ////select the first item to copy
//            //var originalArticle = ArticleHelper.GetArticle(articlesWindow.ArticlesTable, 0);

//            //Specify.That(articlesWindow.ArticlesTable.FindCell(0, 1).InnerText.Trim()).Should.BeEqualTo(originalArticle.ArticleId);


//            //articlesWindow.ArticlesTable.FindCell(0, 1).Click();
//            //articlesWindow.ArticleButtonDelete.Click();
            
//            //articlesWindow.ArticleDeleteCancel.SafeClick();
//            //Retry.For(() => !articlesWindow.ArticleDeleteConfirm.Exists, TimeSpan.FromSeconds(5));
//            //Specify.That(articlesWindow.ArticleDeleteConfirm.Exists).Should.BeFalse("", "dialog was not closed");
//            //Specify.That(articlesWindow.ArticlesTable.FindCell(0, 1).InnerText.Trim()).Should.BeEqualTo(originalArticle.ArticleId);
//            //articlesWindow.ArticleButtonDelete.Click();
//            //Retry.For(() => articlesWindow.ArticleDeleteConfirm.Exists, TimeSpan.FromSeconds(2));
//            //articlesWindow.ArticleDeleteConfirm.Click();
//            //articlesWindow.DeleteArticleWindowAlert.VerifyAlert(true, AlertStates.Success, "Delete complete, article(s) deleted.");
//            //articlesWindow.ArticleDeleteCancel.Click();
//            //Specify.That(articlesWindow.ArticlesTable.FindCell(0, 1).InnerText.Trim()).Should.Not.BeEqualTo(originalArticle.ArticleId);
//        }

      

      

       

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("181")]
//        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
//        public void ArticleWindowProduceMultipleCartons()
//        {
//            //ArticleHelper.Import100Articles();

//            //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);

//            //articlesWindow.ArticlesTable.SelectRow(0);
//            //articlesWindow.ArticlesTable.SelectRow(1);
//            //articlesWindow.ArticlesTable.SelectRow(2);

//            //var rows = articlesWindow.ArticlesTable.FindRows();
//            //Specify.That(articlesWindow.ProduceArticleTable.FindCell(rows[0], 1).InnerText.Trim()).Should.BeEqualTo("555317", "incorrect article selected");
//            //Specify.That(articlesWindow.ProduceArticleTable.FindCell(rows[1], 1).InnerText.Trim()).Should.BeEqualTo("baboon", "incorrect article selected");
//            //Specify.That(articlesWindow.ProduceArticleTable.FindCell(rows[2], 1).InnerText.Trim()).Should.BeEqualTo("balcony", "incorrect article selected");
//            //Specify.That(articlesWindow.ProduceArticleNudInputText(0).GetText()).Should.BeEqualTo("1", "nud didn't have default value");

//            //articlesWindow.ArticleButtonProduce.Click();

//            ////remove one row
//            //rows = articlesWindow.ProduceArticleTable.FindRows();
//            //articlesWindow.ProduceArticleRemoveButton(0).Click();
//            //Specify.That(articlesWindow.ProduceArticleTable.FindCell(1, 1).InnerText.Trim()).Should.BeEqualTo("balcony", "incorrect article selected");

//            ////verify NUD
//            //articlesWindow.ProduceArticleNudIncrementButton(0).Click();
//            ////articlesWindow.ProduceArticleNudInputText(0).SetText("50");
//            //articlesWindow.ProduceArticleNudIncrementButton(0).Click();
//            //Specify.That(articlesWindow.ProduceArticleNudInputText(0).GetText()).Should.BeEqualTo("3", "nud didn't increment value");
//            //articlesWindow.ProduceArticleNudDecrementButton(0).Click();
//            //Specify.That(articlesWindow.ProduceArticleNudInputText(0).GetText()).Should.BeEqualTo("2", "nud didn't decrement value");

//            ////other fields
//            //articlesWindow.ProduceArticleOrderNumber(0).SetText("order id 1");
//            //articlesWindow.ProduceArticleOrderNumber(0).SetText("order id 2");
//            //articlesWindow.ProduceArticleProductionGroup(0, 0).SelectItem(1);
//            //articlesWindow.ProduceArticleProductionGroup(0, 0).SelectItem(1);

//            ////submit
//            //articlesWindow.ArticleProduceConfirm.Click();
//            //articlesWindow.ProduceArticleWindowAlert.VerifyAlert(true, AlertStates.Success, "3 total cartons created from 2 articles");

//            //articlesWindow.ArticleProduceCancel.Click();
//            //Specify.That(articlesWindow.ArticleProduceCancel.Exists).Should.BeFalse("window didn't go away");

//            //Assert.Fail("TODO: dip into mongo for cartons");
//            ////var cartons = CartonRequestRepository.All().ToArray();
//            ////Specify.That(cartons.Count()).Should.BeEqualTo(3);
//            ////Specify.That(cartons.Count(c => c.OrderId == "order id 1")).Should.BeEqualTo(2);
//            ////Specify.That(cartons.Count(c => c.OrderId == "order id 2")).Should.BeEqualTo(1);
//        }

//        [TestMethod]
//        [TestCategory("UIAutomation")]
//        [TestCategory("284")]
//        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
//        public void ArticleWindowProduceAutoGeneratesSerialNumber()
//        {
//            //ArticleHelper.Import100Articles();

//            //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);

//            //articlesWindow.ArticlesTable.SelectRow(0);
//            //articlesWindow.ArticleButtonProduce.Click();

//            //var prodArticleRow = articlesWindow.ProduceArticleTable.FindRow(0);
//            //articlesWindow.ProduceArticleOrderNumber(0).SetText("order id 1");
//            //articlesWindow.ProduceArticleProductionGroup(0, 0).SelectItem(1);
//            //articlesWindow.ArticleProduceConfirm.Click();
//            //articlesWindow.ProduceArticleWindowAlert.VerifyAlert(true, AlertStates.Success, "1 total cartons created from 1 articles");

//            //var adminWindow = TestUtilities.CreatePage<AdminConsole>(AdminConsoleTests.pageUrl);

//            //var failedRequestsTable = adminWindow.FailedRequestsTable;
//            //var serialNumber = failedRequestsTable.GetCell(1, 0).InnerText;

//            //// Note that all data in database is deleted prior to running this test
//            //Specify.That(serialNumber).Should.Not.BeNull("Carton request was not created from article");

//            //NullReferenceException shouldThrowNullReference = null;
//            //var result = String.Empty;
//            //try
//            //{
//            //    result = failedRequestsTable.GetCell(2, 0).InnerText;
//            //}
//            //catch (NullReferenceException e)
//            //{
//            //    shouldThrowNullReference = e;
//            //}
//            //Specify.That(String.IsNullOrEmpty(result)).Should.BeTrue("The string isn't null.");
//            //Specify.That(shouldThrowNullReference).Should.Not.BeNull("Second carton request should not exist");
//        }

//        public static string ExportArticles()
//        {
//            const string fileName = "Export.txt";
           
//            return ArticleHelper.ExportArticles(fileName);
//        }

//        public static void DeleteAllArticles(ArticlesManagement page)
//        {
//            //while (page.ArticlesTable.RowCount() > 0)
//            //{
//            //    page.ArticlesTable.PageSize.SelectItem(2);
//            //    Thread.Sleep(TimeSpan.FromSeconds(5));
//            //    var checkBox = page.ArticlesTable.SelectAllCheckBox;
//            //    checkBox.Check();
//            //    page.ArticleButtonDelete.Click();
//            //    Retry.For(() => page.ArticleDeleteConfirm.Enabled, TimeSpan.FromSeconds(2));
//            //    page.ArticleDeleteConfirm.Click();
//            //    Retry.For(() => page.ArticleDeleteCancel.Enabled, TimeSpan.FromSeconds(4));
//            //    page.ArticleDeleteCancel.Click();
//            //}
//        }

//        public static void VerifyArticleInTable(ArticlesManagement page, int rowId, Article article)
//        {
//            //var row = page.ArticlesTable.FindRow(rowId);
//            //Specify.That(page.ArticlesTable.FindCell(row, 1).InnerText.Trim()).Should.BeEqualTo(article.ArticleId, "Article id was not as expected");
//            //Specify.That(page.ArticlesTable.FindCell(row, 2).InnerText.Trim()).Should.BeEqualTo(article.Description, "Article Description was not as expected");
//            //Specify.That(page.ArticlesTable.FindCell(row, 3).InnerText.Trim()).Should.BeEqualTo(article.Length.ToString(CultureInfo.InvariantCulture), "Article Length was not as expected");
//            //Specify.That(page.ArticlesTable.FindCell(row, 4).InnerText.Trim()).Should.BeEqualTo(article.Width.ToString(CultureInfo.InvariantCulture), "Article Width was not as expected");
//            //Specify.That(page.ArticlesTable.FindCell(row, 5).InnerText.Trim()).Should.BeEqualTo(article.Height.ToString(CultureInfo.InvariantCulture), "Article Height was not as expected");
//            ////Specify.That(page.ArticlesTable.FindCell(row, 6).InnerText.Trim()).Should.BeEqualTo(article.DesignName, "Article Design Name was not as expected");
//            //if (article.CorrugateQuality.HasValue)
//            //    Specify.That(page.ArticlesTable.FindCell(row, 7).InnerText.Trim()).Should.BeEqualTo(article.CorrugateQuality.Value.ToString(CultureInfo.InvariantCulture), "Article CorrugateQuality was not as expected");            
//        }
                        
//        public static void VerifyCreateFormValidation(ArticlesManagement articlesWindow)
//        {
//            ///*
//            // *  Setting the text to string.Empty or "" will, for some reason, not inactivate the Create button like it does when 
//            // *  you manually remove the text from the ArticleId. Use the Clear extension method to get it to work correctly
//            // */

//            ////Test article id edit
//            //articlesWindow.ArticleIdEdit.SetText(" ");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Article Id must not be empty.");
//            //articlesWindow.ArticleIdEdit.SetText("1");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(false);
//            //articlesWindow.ArticleIdEdit.Clear();
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Article Id must not be empty.");
//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Shouldn't be able to click Create but can (1)");
//            //articlesWindow.ArticleIdEdit.SetText("Rock Box");

//            ////Test length
//            //articlesWindow.LengthEdit.SetText(" ");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Length must be a number.");
//            //articlesWindow.LengthEdit.SetText("10");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(false);
//            //articlesWindow.LengthEdit.Clear();
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Length must be a number.");
//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Shouldn't be able to click Create but can (2)");
//            //articlesWindow.LengthEdit.SetText("10");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(false);
//            //articlesWindow.LengthEdit.SetText("ab10c");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Length must be a number.");
//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Shouldn't be able to click Create but can (2)");
//            //articlesWindow.LengthEdit.SetText("10");

//            ////Test Width
//            //articlesWindow.WidthEdit.SetText(" ");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Width must be a number.");
//            //articlesWindow.WidthEdit.SetText("10");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(false);
//            //articlesWindow.WidthEdit.Clear();
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Width must be a number.");
//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Shouldn't be able to click Create but can (3)");
//            //articlesWindow.WidthEdit.SetText("10");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(false);
//            //articlesWindow.WidthEdit.SetText("ab10c");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Width must be a number.");
//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Shouldn't be able to click Create but can (3)");
//            //articlesWindow.WidthEdit.SetText("10");

//            ////Test height
//            //articlesWindow.HeightEdit.SetText(" ");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Height must be a number.");
//            //articlesWindow.HeightEdit.SetText("10");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(false);
//            //articlesWindow.HeightEdit.Clear();
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Height must be a number.");
//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Shouldn't be able to click Create but can (4)");
//            //articlesWindow.HeightEdit.SetText("10");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(false);
//            //articlesWindow.HeightEdit.SetText("ab10c");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Danger, "Height must be a number.");
//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Shouldn't be able to click Create but can (4)");
//            //articlesWindow.HeightEdit.SetText("10");
//        }
                        
//        public static void CloseEditArticle(ArticlesManagement articlesWindow)
//        {
//            //articlesWindow.ArticleEditClose.Click();

//            //Retry.For(() => !articlesWindow.ArticleEditSave.Exists, TimeSpan.FromSeconds(2));
//            //Specify.That(articlesWindow.ArticleEditSave.Exists).Should.BeFalse("New article dialog should be closed");
//            //Specify.That(articlesWindow.ArticleEditClose.Exists).Should.BeFalse("New article dialog should be closed");
//        }

//        public static void EditArticleAt(int row, int column, ArticlesManagement articlesWindow)
//        {
//            //articlesWindow.ArticlesTable.SelectRow(row);

//            //Retry.For(() => articlesWindow.ArticleButtonEdit.Enabled, TimeSpan.FromSeconds(2));
//            //articlesWindow.ArticleButtonEdit.Click();

//            //Retry.For(() => articlesWindow.ArticleEditSave.Exists, TimeSpan.FromSeconds(2));
//            //Specify.That(articlesWindow.ArticleEditSave.Exists).Should.BeTrue("New article dialog didn't appear.");
//            //Specify.That(articlesWindow.ArticleEditClose.Exists).Should.BeTrue("New article dialog didn't appear.");

//            //Specify.That(articlesWindow.ArticleEditSave.Enabled).Should.BeTrue("Create button should be enabled");
//            //Specify.That(articlesWindow.ArticleEditClose.Enabled).Should.BeTrue("Close button should be enabled");
//        }

//        public static void OpenNewArticleDialog(ArticlesManagement articlesWindow)
//        {
//            //Retry.For(() => articlesWindow.ArticleButtonNew.Click(), TimeSpan.FromSeconds(4));

//            //Retry.For(() => !articlesWindow.ArticleNewSave.Exists, TimeSpan.FromSeconds(2));
//            //Specify.That(articlesWindow.ArticleNewSave.Exists).Should.BeTrue("New article dialog didn't appear.");
//            //Specify.That(articlesWindow.ArticleNewClose.Exists).Should.BeTrue("New article dialog didn't appear.");

//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeFalse("Create button should not be enabled");
//            //Specify.That(articlesWindow.ArticleNewClose.Enabled).Should.BeTrue("Close button should be enabled");

//            //Specify.That(articlesWindow.ArticleIdEdit.Exists).Should.BeTrue("ArticleID input box didn't appear");
//            //Specify.That(articlesWindow.DescriptionEdit.Exists).Should.BeTrue("Description input box didn't appear");
//            //Specify.That(articlesWindow.DesignIdList.Exists).Should.BeTrue("DesignID list box didn't appear");
//            //Specify.That(articlesWindow.LengthEdit.Exists).Should.BeTrue("Length input box didn't appear");
//            //Specify.That(articlesWindow.WidthEdit.Exists).Should.BeTrue("Width input box didn't appear");
//            //Specify.That(articlesWindow.HeightEdit.Exists).Should.BeTrue("Height input box didn't appear");
//            ////Specify.That(articlesWindow.MachineTypeList.Exists).Should.BeTrue("DesignID list box didn't appear");
//            //Specify.That(articlesWindow.CorrugateQualityEdit.Exists).Should.BeTrue("Corrugate Quality input box didn't appear");
//        }

//        public static void CloseNewArticleDialog(ArticlesManagement articlesWindow)
//        {
//            //articlesWindow.ArticleNewClose.Click();

//            //Retry.For(() => articlesWindow.ArticleNewSave.Exists, TimeSpan.FromSeconds(2));

//            //Specify.That(articlesWindow.ArticleNewSave.Exists).Should.BeFalse("New article dialog should be closed");
//            //Specify.That(articlesWindow.ArticleNewClose.Exists).Should.BeFalse("New article dialog should be closed");
//        }

//        //public static void CreateNewArticle(AdminConsole adminConsole, Article article)
//        //{
//        //    //var articlesWindow = TestUtilities.CreatePageAndLoginAsAdministrator<ArticlesManagement>(_pageUrl);
//        //    Retry.For(() => adminConsole.ArticlesLink.Exists, TimeSpan.FromSeconds(10));
//        //    adminConsole.ArticlesLink.Click();
//        //    var articlesWindow = new ArticlesManagement();

//        //    CreateNewArticle(articlesWindow, article);

//        //}

//        //public static void CreateNewArticle(ArticlesManagement articlesWindow, Article article)
//        //{
//        //    OpenNewArticleDialog(articlesWindow);

//        //    InputNewArticle(articlesWindow, article);

//        //    SaveNewArticle(articlesWindow);

//        //    CloseNewArticleDialog(articlesWindow);
//        //}

//        public static void InputNewArticle(ArticlesManagement articlesWindow, Article article)
//        {
//            //articlesWindow.ArticleIdEdit.SetText(article.ArticleId);
//            //articlesWindow.DescriptionEdit.SetText(article.Description);
//            //articlesWindow.LengthEdit.SetText(article.Length.ToString(CultureInfo.InvariantCulture));
//            //articlesWindow.WidthEdit.SetText(article.Width.ToString(CultureInfo.InvariantCulture));
//            //articlesWindow.HeightEdit.SetText(article.Height.ToString(CultureInfo.InvariantCulture));

//            //var items = articlesWindow.DesignIdList.GetChildren().OfType<CUITe_HtmlCustom>().Select(i => i.InnerText).ToList();
//            //Specify.That(items.Any()).Should.BeTrue("No designs found");
//            //if (items.Any(i => i.Contains(article.DesignName.ToString())))
//            //    articlesWindow.DesignIdList.SelectItem(items.First(i => i.Contains(article.DesignName.ToString())));
//            //else
//            //    articlesWindow.DesignIdList.SelectItem(1);

//            //if (article.CorrugateQuality.HasValue)
//            //    articlesWindow.CorrugateQualityEdit.SetText(article.CorrugateQuality.Value.ToString(CultureInfo.InvariantCulture));

//            //Specify.That(articlesWindow.ArticleNewSave.Enabled).Should.BeTrue("Create button did not enable");
//        }

//        public static void SaveNewArticle(ArticlesManagement articlesWindow)
//        {
//            //articlesWindow.ArticleNewSave.Click();

//            //Retry.For(() => articlesWindow.NewArticleWindowAlert.AlertDiv.Exists, TimeSpan.FromSeconds(10));
//            //Specify.That(articlesWindow.NewArticleWindowAlert).Should.Not.BeEqualTo("Article created Successfully");
//            //Specify.That(articlesWindow.ArticleIdEdit.GetText()).Should.BeEqualTo("", "Article Id should be cleared when saved");
//            //Specify.That(articlesWindow.DescriptionEdit.Text).Should.BeEqualTo("", "Description should be cleared when saved");
//            //Specify.That(articlesWindow.DesignIdList.SelectedIndex).Should.BeEqualTo(0, "Design Id should be cleared when saved"); //
//            //Specify.That(articlesWindow.LengthEdit.GetText()).Should.BeEqualTo("", "Length should be cleared when saved");
//            //Specify.That(articlesWindow.WidthEdit.GetText()).Should.BeEqualTo("", "Width should be cleared when saved");
//            //Specify.That(articlesWindow.HeightEdit.GetText()).Should.BeEqualTo("", "Height should be cleared when saved");            
//            //Specify.That(articlesWindow.CorrugateQualityEdit.GetText()).Should.BeEqualTo("", "Corrugate Quality should be cleared when saved");
//            ////Specify.That(articlesWindow.SaveButton.Enabled).Should.BeFalse("Create should not be enabled");
//            ////Specify.That(articlesWindow.CancelButton.Enabled).Should.BeTrue("Close should be enabled");
//            //articlesWindow.NewArticleWindowAlert.VerifyAlert(true, AlertStates.Success, "Article created successfully");
//        }
//    }
//}
