﻿using System;
using System.IO;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Utils;
using PackNet.Data.Classifications;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class ClassificationsTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public ClassificationsTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyCreateClassification()
        {
            var c = TestDatabaseManagement.Classification1;
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var page = ClassificationsPage.GetWindow();

            // create a classification and verify that all entered data is saved correctly
            page.Create(c, adminConsole, true);

            // list should have 2 records: the created one plus Unassigned
            page.ClassificationsTable.VerifyTableHasExpectedNumberOfRecords(2);

            // verify that the first row contains the alias of the classification
            page.VerifyTableRowContainsSpecifiedData(0, c.Alias, c.Number);

            // check in the database
            ClassificationMacros.VerifyClassificationExistsInDatabase(c.Alias, true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10272")]
        [TestCategory("Bug 10296")]
        public void VerifyDuplicateClassificationAlias()
        {
            var c = TestDatabaseManagement.Classification1;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = ClassificationsPage.GetWindow();

            // create first classification
            page.Create(c, adminConsole);

            // verify that the first row contains the alias of the classification
            page.VerifyTableRowContainsSpecifiedData(0, c.Alias, c.Number);

            // create another classification with same name, we use the same object as above
            page.Create(c, adminConsole);

            //Verify duplicate error alert is displayed
            page.VerifyDuplicateAliasMessage(c.Alias);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyDeleteClassification()
        {
            var c = TestDatabaseManagement.Classification1;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = ClassificationsPage.GetWindow();

            // add a classification
            page.Create(c, adminConsole);

            // verify the delete button is disabled
            page.VerifyDeleteButtonIsEnabled(false);

            // select the first classification in the table
            page.PageTableSelectRow(0);

            // verify the delete button is enabled
            page.VerifyDeleteButtonIsEnabled();

            // click delete button
            page.ClickDeleteButton();

            // verify the delete successful message is displayed correctly
            page.VerifyDeleteSuccessfulMessage(c.Alias);

            // verify the classification is deleted from the table, 
            // so just one row remains in the table: Unassigned value.
            page.VerifyTableIsEmpty(1);
 
            // verify classification is deleted in the database
            ClassificationMacros.VerifyClassificationExistsInDatabase(c.Alias, false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyUpdateClassification()
        {
            var c1 = TestDatabaseManagement.Classification1;
            var c2 = TestDatabaseManagement.Classification2;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = ClassificationsPage.GetWindow();

            // create a new classification
            page.Create(c1, adminConsole);

            var c1Guid = ClassificationMacros.FindByAlias(c1.Alias).Id;

            // verify that the edit button is disabled
            page.VerifyEditButtonIsEnabled(false);

            // verify that the classification is displayed correctly in the table
            page.VerifyTableRowContainsSpecifiedData(0, c1.Alias);

            // verify the record is saved correctly to the database
            var repo = new ClassificationRepository();
            ClassificationMacros.VerifyClassificationIsSavedToDatabase(c1, repo);

            // update the record
            page.UpdateInfo(0, c2, true);

            //Verify classification update in table
            page.RefreshTables();

            page.VerifyTableRowContainsSpecifiedData(0, c2.Alias, TestDatabaseManagement.Classification2.Number);

            //Verify edit in the db
            ClassificationMacros.VerifyClassificationIsSavedToDatabase(c2, c1Guid, repo);
        }
        #endregion
    }
}
