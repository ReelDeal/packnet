﻿using System;
using System.IO;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Utils;
using PackNet.Data.PickArea;
using PackNet.Data.Machines;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class CustomJobTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public CustomJobTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
           TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("11314")]
        [TestCategory("11514")]
        public void DesignPreview_ShouldShow()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            MachineGroupMacros.BuildMachineGroup(TestDatabaseManagement.Mg2_fm1zp1, adminConsole);
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.SetId(), true);
            var page = CustomJobPage.GetWindow();
            page.Create(TestDatabaseManagement.CustomJob_h10w10l10, adminConsole, true);
        }
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10545")]
        public void VerifyRotationOnCustomJobPage()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg1_orders_mg1_c2755, adminConsole);
            adminConsole.SelectMachineGroup(TestDatabaseManagement.Mg1_em6.SetId(), true);
            CustomJobPage page = new CustomJobPage();
            page.Create(TestDatabaseManagement.CustomJob_h30w30l30_TAPEJOINT, adminConsole, true);
            page.ClickCancelButton();
            page.Create(TestDatabaseManagement.CustomJob_h30w30l30_TrackOffsetInch, adminConsole, true);
        }
        #endregion

    }
}
