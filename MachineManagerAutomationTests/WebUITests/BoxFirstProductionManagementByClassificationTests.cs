﻿using System;
using System.IO;
using System.Threading;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Specificity;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.FusionSim.ViewModels;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    [DeploymentItem("NLog.config")]
    public class BoxFirstProductionManagementByClassificationTests : ExternalApplicationTestBase
    {
        #region Variables
        SimulatorUtilities simUtilities;
        #endregion

        #region Constructor and initiators
        public BoxFirstProductionManagementByClassificationTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }

        protected override void BeforeTestInitialize()
        {
            base.BeforeTestInitialize();
            simUtilities = new SimulatorUtilities();
        }
        protected override void AfterTestCleanup()
        {
            simUtilities.Close();
            Thread.Sleep(1000);
            base.AfterTestCleanup();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void SetupBoxFirstEnviroment()
        {
            BoxFirstSearchTest.SetupBoxFirstEnvironment();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void LoadBoxFirstPreconfigs()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("2153")]
        [TestCategory("8137")]
        [TestCategory("8133")]
        public void BoxFirst_ExpeditedJobsShouldBeProducedFirst()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // drop file with 4 jobs
            var items = DropFileUtilities.CreateBoxFirstDropFileAlternatingPickZone(4);
            DropFileUtilities.CreateFile(DropFileType.BoxFirst, items);

            // add classification widget to dashboard and set initial values
            var widget = new ClassificationsWidget();
            adminConsole.AddWidgetToDashboard(DashboardWidgets.Classifications, DashboardLayouts.Row1_1Col_Row2_3Cols);
            widget.SetStatus(TestDatabaseManagement.Classification1, ClassificationStatuses.Expedite);

            // verify status and number of requests
            widget.VerifyStatus(TestDatabaseManagement.Classification1, ClassificationStatuses.Expedite, true);
            widget.VerifyStatus(TestDatabaseManagement.Classification2, ClassificationStatuses.Normal, true);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 2);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 2);

            // initialize machines
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var normalPrinterSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var priorityPrinterSim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            // set machine group online
            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { normalPrinterSim, priorityPrinterSim },
                TestDatabaseManagement.Mg5_fm1zp1zp2.SetId(), adminConsole);

            var page = MachineProductionPage.GetWindow(SelectionAlgorithmTypes.BoxFirst,ProductionMode.Auto);

            // verify data before production starts, first to be produced is the first job in classification 1
            // with status Expedite
            page.ItemsInProductionTable.VerifyCellValue(0, 1, "CU_0");

            // produce first job that belongs to classification that has status Expedite
            // jobs within that classification also prints to priority printer
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, priorityPrinterSim);

            page.RefreshTables();
            page.ItemsInProductionTable.VerifyCellValue(0, 1, "CU_2");

            // verify what has happened in classification widget after first production
            adminConsole.NavigateToPage(NavPages.Dashboard);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 1);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 2);

            // produce second job that belongs to classification with status Expedite
            // priority printer is used
            adminConsole.NavigateToPage(NavPages.MachineProduction);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, priorityPrinterSim);
            page.RefreshTables();
            Thread.Sleep(3000);
            adminConsole.NavigateToPage(NavPages.Dashboard);

            // both surge jobs are produced, the remaining jobs belong classification2
            // which has status Normal
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 0);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 2);

            adminConsole.NavigateToPage(NavPages.MachineProduction);
            page.ItemsInProductionTable.VerifyCellValue(0, 1, "CU_1");

            // close simulators
            fusionSim.Dispose();
            normalPrinterSim.Dispose();
            priorityPrinterSim.Dispose();
        }
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8136")]
        public void BoxFirst_StopAClassificationWillSuspendTheJobsInThatClassification()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // drop box first file to create some test data
            DropFileUtilities.DropBoxFirstDropFile(10);

            // initialize machines 
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            // add widget and verify data in the widget
            var widget = new ClassificationsWidget();
            adminConsole.AddWidgetToDashboard(DashboardWidgets.Classifications);
            widget.SetStatus(TestDatabaseManagement.Classification1, ClassificationStatuses.Stop);
            widget.VerifyStatus(TestDatabaseManagement.Classification2, ClassificationStatuses.Normal, true);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 5);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 5);

            // connect to machines
            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            // produce 5 of the jobs
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            // verify number of requests
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 0);

            Playback.Wait(1000);

            // check that there is zero jobs in the itemsInProduction table
            var page = MachineProductionPage.GetWindow();
            page.ItemsInProductionTable.VerifyTableHasExpectedNumberOfRecords(0);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("8138")]
        public void BoxFirst_LastAClassificationWillMakeTheJobsInThatClassificationBeProducedAtLast()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // drop box first file to create some test data
            DropFileUtilities.DropBoxFirstDropFile(10);

            // initialize machines 
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            // add widget to dashboard, set initial values and verify current state
            var widget = new ClassificationsWidget();
            adminConsole.AddWidgetToDashboard(DashboardWidgets.Classifications);
            widget.SetStatus(TestDatabaseManagement.Classification1, ClassificationStatuses.Last);
            widget.VerifyStatus(TestDatabaseManagement.Classification2, ClassificationStatuses.Normal, true);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 5);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 5);

            // connect to machines
            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            // produce 5 of the jobs
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            // check state of widget
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 5);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 5);

            // verify that the table is empty
            var page = MachineProductionPage.GetWindow();
            page.ItemsInProductionTable.VerifyTableHasData(false);

            // produce one more job
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);

            // verify number of requests
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 4);

        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("290")]
        [TestCategory("5596")]
        [TestCategory("3843")]
        [TestCategory("5688")]
        public void BoxFirst_ReproducingWithPriorityLabelAndNormalLabelPrinting()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // drop files with 4+2 jobs
            DropFileUtilities.DropBoxFirstDropFile(4);
            DropFileUtilities.DropBoxFirstDropFile(2);

            // initialize machines 
            var fusionSim = simUtilities.ConnectAndGetFusionSim(TestDatabaseManagement.Fm1);
            var printer1Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp1);
            var printer2Sim = simUtilities.ConnectAndGetPrinterSim(TestDatabaseManagement.Zp2);

            // add widget to dashboard and set initial state
            var widget = new ClassificationsWidget();
            adminConsole.AddWidgetToDashboard(DashboardWidgets.Classifications);
            widget.SetPrinterPriority(TestDatabaseManagement.Classification1.Alias, false);

            // connect to machines
            SimulatorHelper.SetMachineGroupOnline(fusionSim, new List<ZebraPrinterSimulatorViewModel> { printer1Sim, printer2Sim },
                TestDatabaseManagement.Mg2_fm1zp1.SetId(), adminConsole);

            // produce 2 jobs
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer1Sim);
            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim, printer2Sim);

            // verify state in widget
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification1, 0);
            widget.VerifyNumberOfRequests(TestDatabaseManagement.Classification2, 0);

            // verify itemsInProduction has no data
            var page = MachineProductionPage.GetWindow();
            page.ItemsInProductionTable.VerifyTableHasData(false);

            // reproduce from OP history list
            int serialNumber = ReproduceJobFromMachineHistoryList(0);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim,
                serialNumber % 2 == 0 ? printer2Sim : printer1Sim);

            serialNumber = ReproduceJobFromSearchHistory();

            page.ItemsInProductionTable.VerifyCellValue(0, 1, serialNumber);

            SimulatorHelper.CompleteTheJobSentToMachine(fusionSim,
                serialNumber % 2 == 0 ? printer2Sim : printer1Sim);

            page.CompletedItemsTable.VerifyContainsValue(serialNumber.ToString());
        }

        #endregion

        #region Helper methods
        private int ReproduceJobFromSearchHistory()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);

            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);

            page.SetStatus(JobStatus.Completed);

            page.ClickSearchButton();

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(2);

            page.ResultTable.SelectRow(0);

            var serialNumber = page.ResultTable.GetCellIntValue(0, 0);

            page.ClickReproduceButton();

            return serialNumber;
        }

        private int ReproduceJobFromMachineHistoryList(int rowIndex)
        {
            var machineProdPage = MachineProductionPage.GetWindow();
            var serialNumber = machineProdPage.CompletedItemsTable.GetCellValue(rowIndex, 2);
            machineProdPage.CompletedItemsTable.ClickReproduceButton(rowIndex);
            machineProdPage.ClickReproduceOkButton();
            Specify.That(machineProdPage.ItemsInProductionTable.GetCellValue(0, 1)).Should.BeEqualTo(serialNumber);
            return int.Parse(serialNumber);
        }
        #endregion
    }
}