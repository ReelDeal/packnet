﻿using System;
using System.Diagnostics;
using System.Reflection;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITesting;

using Testing.Specificity;

using PackNet.Common.Utils;

using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    /// <summary>
    /// Summary description for MachineManagerAdminConsoleTests
    /// </summary>
    [CodedUITest]
    public class AdminConsoleTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public AdminConsoleTests()
            : base(TestSettings.Default.MachineManagerProcessName, TestUtilities.MachineManagerWindowLocation)
        {
        }

        protected override void TestInitializeBeforeStartProcess()
        {
            TestDatabaseManagement.CascadedTruncateAllTablesInDatabase();
        }
        #endregion

        #region Tests
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("1627")]
        public void AdminAboutPageContainsVersionInformation()
        {
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(ExePath);

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.About);
            var aboutInfo = new AboutPage();
            Specify.That(aboutInfo.PacksizeVersion.InnerText).Should.BeEqualTo("About PackNet.Server - Version: " + fvi.FileVersion);

            Specify.That(aboutInfo.ReleaseNotesButton.Exists).Should.BeTrue("Release notes button should exist.");
            Specify.That(aboutInfo.EulaButton.Exists).Should.BeTrue("EULA button should exist.");

            Specify.That(aboutInfo.ReleaseNotesButton.Enabled).Should.BeFalse("Release notes button should be disabled when About page loads.");
            Specify.That(aboutInfo.EulaButton.Enabled).Should.BeTrue("Eula button should be enabled when About page loads.");
            Specify.That(aboutInfo.IFrame.Enabled).Should.BeTrue("Release Notes content should be displayed");
            Specify.That(aboutInfo.IFrameContents.InnerText.StartsWith(""));

            aboutInfo.EulaButton.Click();
            Specify.That(aboutInfo.ReleaseNotesButton.Enabled).Should.BeTrue("Release notes button should be enabled when other button is clicked.");
            Specify.That(aboutInfo.EulaButton.Enabled).Should.BeFalse("Eula button should be disabled after it is clicked.");
            Specify.That(aboutInfo.IFrame.Enabled).Should.BeTrue("EULA content should be displayed");
            Specify.That(aboutInfo.IFrameContents.InnerText.StartsWith(""));
        }
        #endregion
    }
}
