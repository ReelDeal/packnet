﻿using System;
using System.IO;
using System.Linq;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Utils;
using PackNet.Data.Corrugates;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class CorrugatesTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public CorrugatesTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6024")]
        public void VerifyCreateCorrugate()
        {
            // open admin page, login
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // select corrugates in the menu
            var page = CorrugatesPage.GetWindow();

            // create a corrugate
            page.Create(TestDatabaseManagement.Corrugate_w2755t011, adminConsole);

            // verify the code in the UI and
            page.VerifyCorrugate(TestDatabaseManagement.Corrugate_w2755t011, adminConsole);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10271")]
        public void VerifyDuplicateCorrugateAlias()
        {
            // open admin page, login
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // Select corrugates in the menu
            var page = CorrugatesPage.GetWindow();

            // create a corrugate
            page.Create(TestDatabaseManagement.Corrugate_w2755t011, adminConsole);

            // verify corrugate in the grid
            page.VerifyCorrugate(TestDatabaseManagement.Corrugate_w2755t011, adminConsole);

            // create another corrugate with same name
            page.Create(TestDatabaseManagement.Corrugate_w2755t011, adminConsole);

            //Verify error alert is displayed
            page.VerifyDuplicateAliasMessage(TestDatabaseManagement.Corrugate_w2755t011.Alias);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6024")]
        public void VerifyUpdateCorrugate()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = CorrugatesPage.GetWindow();

            // create a corrugate
            page.Create(TestDatabaseManagement.Corrugate_w2755t011, adminConsole, true);

            // verify the corrugate is displayed correctly in the table
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.Corrugate_w2755t011.Alias);

            // verify the record is saved to the database
            var corrugate1 = CorrugateMacros.VerifyCorrugateExistsInDatabase(TestDatabaseManagement.Corrugate_w2755t011.Alias);

            // edit the first row with data from another corrugate
            page.UpdateInfo(0, TestDatabaseManagement.Corrugate_w199t011);

            // verify corrugate update in table, renew neccessary since data in table has been updated
            page.RefreshTables();

            // verify that the table contains correct alias
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.Corrugate_w199t011.Alias);

            // verify edit in the db, since all info is changed we need to use the guid to find the record in the database
            CorrugateMacros.VerifyCorrugateIsSavedToDatabase(TestDatabaseManagement.Corrugate_w199t011, corrugate1.Id);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("6024")]
        [TestCategory("3.0")]
        public void VerifyDeleteCorrugate()
        {
            // start Admin Console before test execution
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = CorrugatesPage.GetWindow();

            // add the classification through the UI
            page.Create(TestDatabaseManagement.Corrugate_w2755t011, adminConsole);

            // make sure Delete button is disabled
            page.VerifyDeleteButtonIsEnabled(false);

            // select the first classification
            page.PageTableSelectRow(0);

            // click delete
            page.ClickDeleteButton();

            //Make sure the delete successfully banner is displayed
            page.VerifyDeleteSuccessfulMessage(TestDatabaseManagement.Corrugate_w2755t011.Alias);

            //Make sure the row no longer exists in the table , so just 1 row remaining in the table , Unassigned value.
            page.VerifyTableIsEmpty();

            //verify classification deleted in mongo
            CorrugateMacros.VerifyCorrugateExistsInDatabase(TestDatabaseManagement.Corrugate_w2755t011.Alias,false);
        }
        #endregion

    }
}