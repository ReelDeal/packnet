﻿//using System;
//using System.Collections.Generic;
//using System.IO;

//namespace MachineManagerAutomationTests.WebUITests
//{
//    public class DropFileTests
//    {
//        public DropFileTests()
//        {
//            DropFileExtension = ".csv";
//        }

//        public string DropFileExtension { get; set; }

//        /// <summary>
//        /// Drop File In Directory
//        /// </summary>
//        /// <param name="dropFolderPath"></param>
//        /// <param name="dropData"></param>
//        public static string DropFileInDirectory(string dropFolderPath, List<string> dropData)
//        {
//            var dropFileTests = new DropFileTests();
//            var fileName = Path.Combine(dropFolderPath, Guid.NewGuid() + dropFileTests.DropFileExtension);

//            using (var sw = new StreamWriter(fileName))
//            {
//                dropData.ForEach(sw.WriteLine);
//            }
//            return fileName;
//        }

//        /// <summary>
//        /// Drop File In Directory
//        /// </summary>
//        /// <param name="dropData"></param>
//        public static string DropFileInDirectory(List<string> dropData)
//        {
//            return DropFileInDirectory(Path.Combine(TestSettings.Default.InstallLocation, "DropFolder\\"), dropData);
//        }
//    }
//}
