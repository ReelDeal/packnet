﻿using System;
using System.IO;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Utils;
using PackNet.Data.PickArea;

using Testing.Specificity;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("NLog.config")]
    public class PickZonesTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public PickZonesTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyCreatePickZone()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var page = PickZonesPage.GetWindow();
            page.Create(TestDatabaseManagement.PickZone1, adminConsole, true);
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.PickZone1.Alias, TestDatabaseManagement.PickZone1.Description);
            PickZoneMacros.VerifyPickZoneIsSavedToDatabase(TestDatabaseManagement.PickZone1);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyCreatePickZoneAliasRequired()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.PickZone);
            var page = PickZonesPage.GetWindow();

            page.ClickNewButton();
            page.VerifyEditPageIsOpen();
            page.ClickSaveButton();

            //Verify the error is shown
            page.VerifyNameValidationIsDisplayed();

            page.SetName("any value");
            
            //Verify the error goes away
            page.VerifyNameValidationExists(false);

            page.ClickCancelButton();

            //Verify that the new dialog was closed
            page.VerifyEditPageIsOpen(false);

            //Verify pickzone is not in the table
            page.VerifyTableIsEmpty();

            //verify pickzone is not in DB
            PickZoneMacros.VerifyPickZoneExistsInDatabase(TestDatabaseManagement.PickZone1.Alias, false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("10281")]
        [TestCategory("5061")]
        public void VerifyDuplicatePickZoneAlias()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var pz = TestDatabaseManagement.PickZone1;

            var page = PickZonesPage.GetWindow();

            page.Create(pz, adminConsole);

            //Verify pickzone in table
            page.VerifyTableRowContainsSpecifiedData(0, pz.Alias);

            //Create the same pickzone
            page.Create(pz, adminConsole);

            //Verify that the error alert is displayed
            page.VerifyDuplicateAliasMessage(TestDatabaseManagement.PickZone1.Alias);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyDeletePickZone()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = PickZonesPage.GetWindow();

            page.Create(TestDatabaseManagement.PickZone1, adminConsole);

            //Verify pickzone in table
            page.VerifyTableRowContainsSpecifiedData(0, TestDatabaseManagement.PickZone1.Alias);

            //Verify Delete is disabled
            page.VerifyDeleteButtonIsEnabled(false);

            //Select the pickzone in the list
            page.PageTableSelectRow(0);

            //Verify Delete is now enabled
            page.VerifyDeleteButtonIsEnabled(true);

            //Delete the pickzone
            page.ClickDeleteButton();

            page.VerifyDeleteSuccessfulMessage(TestDatabaseManagement.PickZone1.Alias);

            //Verify that the table does not have any rows anymore
            page.PickZoneTable.RefreshData();
            page.VerifyTableIsEmpty();

            //Verify no pickzone in db
            PickZoneMacros.VerifyPickZoneExistsInDatabase(TestDatabaseManagement.PickZone1.Alias, false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyUpdatePickZone()
        {
            var pz1 = TestDatabaseManagement.PickZone1;
            var pz2 = TestDatabaseManagement.PickZone2;

            //Open admin page, login, navigate to pickzone page
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = PickZonesPage.GetWindow();

            // create the pickzone and get guid of created pickzone from db
            page.Create(pz1, adminConsole);
            var pz1Guid = PickZoneMacros.FindByAlias(pz1.Alias).Id;

            // Verify pickzone in table
            page.VerifyTableRowContainsSpecifiedData(0, pz1.Alias);

            // Select the row and update the information
            page.UpdateInfo(0, pz1);

            //Verify the record in the database
            var repo = new PickZoneRepository();
            var pz = PickZoneMacros.VerifyPickZoneExistsInDatabase(pz1.Alias, true, repo);

            //Click edit and change alias
            page.UpdateInfo(0, pz2);

            //Verify pickzone update in table
            page.PickZoneTable.VerifyCellValue(0, 0, pz2.Alias);
            page.PickZoneTable.VerifyCellValue(0, 1, pz2.Description);

            //Verify edit in the db
            PickZoneMacros.VerifyPickZoneIsSavedToDatabase(pz2, pz1Guid, repo);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5058")]
        public void AddRemoveEditPickzonesFromAdminPanel()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.PickZone);

            var page = new PickZonesPage();
            page.VerifyNewEditAndDeleteButtonExist();
        }
   
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("5062")]
        public void AddingOrEditingPickzonesFromAdminUIGivesRealTimeValidation()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.PickZone);

            var page = new PickZonesPage();
            page.ClickNewButton();
            page.ClickNameEditField();
            page.ClickDescriptionEditField();

            //Verify real time validation is present and save is disabled
            page.VerifyNameValidationIsDisplayed(true);

            // check that the save button is enabled
            page.VerifySaveButtonIsEnabled();

            page.SetName("derp");

            //Verify real time validation is hidden and save is enabled
            page.VerifyNameValidationExists(false);

            //this test is worthless since the design today never disables a save button
            //page.VerifySaveButtonIsEnabled(false);

            page.SetName("");

            //Verify real time validation is present and save is disabled
            page.VerifyNameValidationIsDisplayed(true);

            //this test is worthless since the design today never disables a save button
            //page.VerifySaveButtonIsEnabled(false);
        }

        #endregion
    }
}
