﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CUITe.Controls.HtmlControls;
using Testing.Specificity;

using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Data.UserManagement;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    public class OperatorConsoleTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public OperatorConsoleTests()
            : base(TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe), TestSettings.Default.CloseAppBetweenTests)
        {}
        #endregion

        #region Obsoleted tests
        //[Obsolete("3.0 UI Update")]
        //[TestMethod]
        //[TestCategory("UIAutomation")]
        //[TestCategory("???")] //TODO: Create Associated Test
//        public void CanHitOperatorConsolePagesDirectly()
//        {
//            var opPage = DynamicBrowserWindowWithLogin.LoginAsOperatorAndCreateOperatorConsole(3);
////            Specify.That(opPage.MachineAliasDiv.Exists).Should.BeTrue("OperatorConsole Page not found");
//            opPage.Close();

//            //opPage = TestUtilities.LoginAsOperatorAndCreateOperatorConsole(3, "index.html#/OperatorConsole/ChangeCorrugates");

//            //Specify.That(opPage.SaveCorrugateButton.Exists).Should.BeTrue("Corrugates page not found");
//            //opPage.Close();

//            opPage = DynamicBrowserWindowWithLogin.LoginAsOperatorAndCreateOperatorConsole(3, "index.html#/OperatorConsole/ChangeProductionGroup");
//            Specify.That(opPage.Get<CUITe_HtmlDiv>("id=operatorConsoleChangeProductionGroups").Exists).Should.BeTrue("ChangeProductionGroup page not found");
//            opPage.Close();

//            opPage = DynamicBrowserWindowWithLogin.LoginAsOperatorAndCreateOperatorConsole(3, "index.html#/OperatorConsole/MachineCommands");
//            Specify.That(opPage.Get<CUITe_HtmlDiv>("id=divPauseMachine").Exists).Should.BeTrue("MachineControl page not found");
//            opPage.Close();

//            //production
//            //history

//            opPage = DynamicBrowserWindowWithLogin.LoginAsOperatorAndCreateOperatorConsole(3, "index.html#/OperatorConsole/CreateFromArticle");
//            // use new page!            OperatorConsoleArticleDialog articlesPage = new OperatorConsoleArticleDialog(opPage.Title);
//            //Specify.That(articlesPage.Dialog.Exists).Should.BeTrue("Article page not found");
//            //articlesPage.Close();

//            opPage = DynamicBrowserWindowWithLogin.LoginAsOperatorAndCreateOperatorConsole(3, "index.html#/OperatorConsole/CreateCustomJob");
//            Specify.That(opPage.Get<CUITe_HtmlDiv>("id=OperatorConsoleCustomJobDialog").Exists).Should.BeTrue("CreateCustom page not found");
//            opPage.Close();
//        }

        [Obsolete("3.0 UI Update")]
        [TestMethod]
        [TestCategory("UIAutomation")]
        public void CanChangeCorrugate()
        {
            //const int machineId = 3;

            //var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(AdminConsoleTests.pageUrl);
            //ProductionGroupTests.AddMachineToProductionGroup("PG1", machineId);
            //FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8081);
            //AdminConsoleTests.SetMachineInAutoMode( machineId);
            //adminConsole.Close();

            ////Open Fusion OP
            //var operatorConsole = OpenOperatorPageAndLoginAsAdmin(machineId);

            //Retry.For(() => operatorConsole.ChangeCorrugatesLink.Click(), TimeSpan.FromSeconds(60));

            //if (operatorConsole.PauseMachineButton.Enabled) { operatorConsole.PauseMachineButton.Click(); }

            //var originalTrack1Corrugate = GetTrackCorrugate(operatorConsole);
            //const string newTrack1Corrugate = "Corrugate 33.688 0.16 0";

            //Specify.That(GetTrackCorrugate(operatorConsole) == originalTrack1Corrugate)
            //    .Should.BeTrue("Change Corrugate Test fails because the corrugates were identical to bein with.");

            //ChangeMachineCorrugate(operatorConsole, newTrack1Corrugate);

            //Specify.That(GetTrackCorrugate(operatorConsole) == newTrack1Corrugate)
            //    .Should.BeTrue("Change Corrugate did not save properly.");

            //operatorConsole.Close();
        }

        [Obsolete("3.0 UI Update")]
        [TestMethod]
        [TestCategory("2965")]
        [TestCategory("UIAutomation")]
        public void PauseBetweenOrdersOnASingleMachineInOperatorPanel()
        {
            //const int machineId = 3;

            //var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(AdminConsoleTests.pageUrl);

            //// Create new article with article id 5555
            //var article = new Article { ArticleId = "5555", Description = "article 5555", Length = 10, Width = 10, Height = 10, DesignId = 2010001, DesignName = "IQ 0201-0001 INCHES" };

            //ArticleTests.CreateNewArticle(adminConsole, article);
            //ProductionGroupTests.EnablePauseBetweenOrders();
            //ProductionGroupTests.AddMachineToProductionGroup("PG1", machineId);

            //const int qty = 4;
            //var orderIds = new[] { "order9123", "order9456", "order9789" };
            //DropFileTests.DropFileInDirectory(
            //    new List<string>
            //    {
            //            "Type;OrderId;ArticleId;Quantity;PickZone\n" +
            //            "Order;"+orderIds[0]+";"+article.ArticleId+";"+qty+";AZ1",
            //            "Order;"+orderIds[1]+";"+article.ArticleId+";"+qty+";AZ1",
            //            "Order;"+orderIds[2]+";"+article.ArticleId+";"+qty+";AZ1"
            //        });

            //// Unpause the machine then click the Auto button in the Operator Console window
            //var sim = FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8081);
            //AdminConsoleTests.SetMachineInAutoMode( machineId);

            ////Open Fusion OP
            //var machineName = "iQ Fusion Machine(" + machineId + ")";
            //var operatorPanel = OpenOperatorPageAndLoginAsAdmin(machineId);
            //try
            //{

            //    ChangeMachineCorrugate(operatorPanel, "Corrugate 33.688 0.16 0");


            //    var openOrderControl = operatorPanel.OpenOrdersWidget;

            //    if (sim.IsMachinePaused)
            //    {
            //        sim.TogglePauseMachine();
            //    }

            //    //Complete 4 jobs
            //    Enumerable.Range(1, 4).ForEach(i => FusionSimulatorTests.StartAndCompleteJob(sim));

            //    Retry.For(() => operatorPanel.MachineProductionTable.RowCount == 1 /*RowCount includes header*/, TimeSpan.FromSeconds(30));
            //    Specify.That(operatorPanel.MachineProductionTable.RowCount).Should.BeEqualTo(1, "Jobs didn't complete for order 1");

            //    // Wait for machine to finish production of all carton requests in queue
            //    Retry.For(() => openOrderControl.Orders.Count == 2, TimeSpan.FromSeconds(60));
            //    Specify.That(openOrderControl.Orders.Count).Should.BeEqualTo(2, "First order didn't complete");

            //    // Unpause the machine.
            //    Specify.That(operatorPanel.MachineStatusIsPaused).Should.BeTrue("Operator Panel is not in a paused state after first order.");
            //    Specify.That(sim.IsMachinePaused).Should.BeTrue("Machine did not pause after first order.");
            //    sim.TogglePauseMachine();

            //    // Wait for machine to finish production of order9789
            //    Enumerable.Range(1, 4).ForEach(i => FusionSimulatorTests.StartAndCompleteJob(sim));
            //    Retry.For(() => operatorPanel.MachineProductionTable.RowCount == 1 /*RowCount includes header*/, TimeSpan.FromSeconds(30));
            //    Specify.That(operatorPanel.MachineProductionTable.RowCount).Should.BeEqualTo(1, "Jobs didn't complete for order 2");

            //    Retry.For(() => openOrderControl.Orders.Count == 1, TimeSpan.FromSeconds(60));
            //    Specify.That(openOrderControl.Orders.Count).Should.BeEqualTo(1, "Second order didn't complete");

            //    // Unpause the machine
            //    Specify.That(operatorPanel.MachineStatusIsPaused).Should.BeTrue("Operator Panel is not in a paused state after second order.");
            //    Specify.That(sim.IsMachinePaused).Should.BeTrue("Machine did not pause after second order");
            //    sim.TogglePauseMachine();

            //    // Wait for order9123 to finish
            //    Enumerable.Range(1, 4).ForEach(i => FusionSimulatorTests.StartAndCompleteJob(sim));

            //    Retry.For(() => operatorPanel.MachineProductionTable.RowCount == 1 /*RowCount includes header*/, TimeSpan.FromSeconds(30));
            //    Specify.That(operatorPanel.MachineProductionTable.RowCount).Should.BeEqualTo(1, "Jobs didn't complete for order 3");

            //    Retry.For(() => openOrderControl.Orders.Count == 0, TimeSpan.FromSeconds(60));
            //    Specify.That(openOrderControl.Orders.Count).Should.BeEqualTo(0, "Third order didn't complete");

            //    // Unpause the machine
            //    Specify.That(operatorPanel.MachineStatusIsPaused).Should.BeTrue("Operator Panel is not in a paused state after third order.");
            //    Specify.That(sim.IsMachinePaused).Should.BeTrue("Machine did not pause after third order.");
            //}
            //finally
            //{
            //    operatorPanel.Close();
            //}
        }

        [Obsolete("3.0 UI Update")]
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("651")]
        public void ShouldDistributeOrders()
        {
            //// In Admin Console window click Production Group, click the Production Options tab, then check "Allow orders to be distributed across multiple machines" and click Create & Close

            //const int machine1 = 3;
            //const int machine2 = 5;

            //var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(AdminConsoleTests.pageUrl);

            //ProductionGroupTests.EnableShouldDistributeOrders(adminConsole, "PG1");

            //// Create new article with article id 5555
            //var articles = new List<Article>
            //{
            //    new Article
            //    {
            //        ArticleId = "333",
            //        Length = 7,
            //        Width = 7,
            //        Height = 7,
            //        DesignId = 2010001,
            //        Description = "Article 333"
            //    },
            //    new Article
            //    {
            //        ArticleId = "444",
            //        Length = 10,
            //        Width = 10,
            //        Height = 10,
            //        DesignId = 2010001,
            //        Description = "Article 444"
            //    },
            //    new Article
            //    {
            //        ArticleId = "555",
            //        Length = 7,
            //        Width = 7,
            //        Height = 7,
            //        DesignId = 2010001,
            //        Description = "Article 555"
            //    }
            //};

            //ArticleTests.CreateNewArticles(adminConsole, articles);

            //// Drop the file SampleOrderMultiplePickZones.csv

            //const int qty = 75;

            //string[] pickZones = { "AZ1", "AZ2", "AZ3" };
            //string[] orderIds = { "Ford 3466-12", "Tesla 4414-12", "Toyota 8855-12" };


            //DropFileTests.DropFileInDirectory(
            //    new List<string>
            //    {
            //            "1;"+orderIds[0]+";333;"+qty+";"+pickZones[0],
            //            "1;"+orderIds[1]+";444;"+qty+";"+pickZones[0],
            //            "1;"+orderIds[2]+";555;"+qty+";"+pickZones[0],
            //            "1;GM 73322;333;"+qty+";"+pickZones[1],
            //            "1;Ford 4772-12;444;"+qty+";AZ2"+pickZones[1],
            //            "1;Chevy-22;555;"+qty+";AZ2"+pickZones[1],
            //            "1;Offline Dec-32;333;"+qty+";AZ3"+pickZones[2],
            //            "1;Offline Jan-32;444;"+qty+";AZ3"+pickZones[2],
            //            "1;Offline Lg Box-32;555;"+qty+";AZ3"+pickZones[2]
            //        });

            //ProductionGroupTests.AddMachineToProductionGroup("PG1", "iQ Fusion Machine");
            //ProductionGroupTests.AddMachineToProductionGroup("PG1", "iQ Fusion Machine 2");

            //// In Packsize Suite set the EM7 to Play

            //var sim1 = FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8081);
            //var sim2 = FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8082);
            //var sims = new List<SimulatedFusion> { sim1, sim2 };

            //AdminConsoleTests.SetMachineInAutoMode( machine1);
            //AdminConsoleTests.SetMachineInAutoMode( machine2);

            //adminConsole.Close();

            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 10);

            //const string optimalCorrugate = "Corrugate 33.688 0.16 0";

            //// Open the Operator Console for the Fusion and the EM7

            //var machineName1 = "iQ Fusion Machine(" + machine1 + ")";
            //var machineName2 = "iQ Fusion Machine(" + machine2 + ")";
            //var operatorPanel1 = OpenOperatorPageAndLoginAsAdmin(machine1);

            //ChangeMachineCorrugate(operatorPanel1, optimalCorrugate);
            //if (sim1.IsMachinePaused) { sim1.TogglePauseMachine(); }

            //var openOrderControl1 = operatorPanel1.OpenOrdersWidget;

            //// In the Operator Console for the Fusion click the Distribute button on the top order (in this case it is Ford 3466-12)
            //openOrderControl1.Orders.First(o => o.OrderId.Equals(orderIds[0])).Distribute.Click();

            //// The EM7 switches to production on the Fusion order (Ford 3466-12). The EM7 order(Tesla 4412-12) shows as Paused, and no jobs are being produced for that order.
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 4);

            //var fordOrder = openOrderControl1.Orders.First(o => o.OrderId.Equals(orderIds[0]));
            //var teslaOrder = openOrderControl1.Orders.First(o => o.OrderId.Equals(orderIds[1]));

            //Specify.That(teslaOrder.OrderStatus == "Paused").Should.BeTrue(String.Format("{0} is not paused.", orderIds[1]));
            //Specify.That(fordOrder.MachinesThatHaveWorkedOnOrder == 2)
            //    .Should.BeTrue(String.Format("Two machines should have worked on {0}", orderIds[0]));
            //operatorPanel1.Close();

            //// Wait for the distributed order both machines are working on to complete
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 66);

            //// When the distributed order completes. The EM7 returns to the order it was working on and the Fusion goes to the next order (in this case it is Toyota 8855-12)
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 10);

            //// Before the EM7 completes its order click the Distribute button.
            //var operatorPanel2 = OpenOperatorPageAndLoginAsAdmin(machine2);

            //ChangeMachineCorrugate(operatorPanel2, optimalCorrugate);
            //if (sim2.IsMachinePaused) { sim2.TogglePauseMachine(); }

            //var openOrderControl2 = operatorPanel2.OpenOrdersWidget;

            //// The Fusion switches to production on the the EM7 order. The Fusion order shows as In Progress, but no are jobs being produced.
            //openOrderControl2.Orders.First(o => o.OrderId.Equals(orderIds[1])).Distribute.Click();
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 4);

            //teslaOrder = openOrderControl2.Orders.First(o => o.OrderId.Equals(orderIds[1]));
            //var toyotaOrder = openOrderControl2.Orders.First(o => o.OrderId.Equals(orderIds[2]));

            //Specify.That(toyotaOrder.OrderStatus == "Paused").Should.BeTrue(String.Format("{0} is not paused.", orderIds[2]));
            //Specify.That(teslaOrder.MachinesThatHaveWorkedOnOrder == 2)
            //    .Should.BeTrue(String.Format("Two machines should have worked on {0}", orderIds[1]));

            //// Before the distributed order both machines are working on completes click the Single Machine button
            //openOrderControl2.Orders.First(o => o.OrderId.Equals(orderIds[1])).Distribute.Click();
            //// A prompt opens asking to confirm. Click continue. 
            //Specify.That(openOrderControl2.ContinueDistributeRequestButton.Enabled)
            //    .Should.BeTrue("Are you sure dialog did not come up.");
            //openOrderControl2.ContinueDistributeRequestButton.Click();

            ////Hack to let it refresh.
            //Thread.Sleep(2000);

            //teslaOrder = openOrderControl2.Orders.First(o => o.OrderId.Equals(orderIds[1]));
            //toyotaOrder = openOrderControl2.Orders.First(o => o.OrderId.Equals(orderIds[2]));

            //// Both machines are paused. Ensure that both machines are visibly paused in the Web UI and Schedular. All orders show as, "Set a machine to 'Auto' to produce this order"
            //const string pausedOrderStatus = "Connect a machine, run machine and set to 'Auto' to produce this order.";
            //Specify.That(teslaOrder.OrderStatus == pausedOrderStatus).Should.BeTrue(String.Format("{0} is not paused.", orderIds[1]));
            //Specify.That(toyotaOrder.OrderStatus == pausedOrderStatus).Should.BeTrue(String.Format("{0} is not paused.", orderIds[2]));

            ////if (sim1.IsMachinePaused) { sim1.TogglePauseMachine(); }
            ////if (sim2.IsMachinePaused) { sim2.TogglePauseMachine(); }

            ////FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 131);

        }

        [Obsolete("3.0 UI Update")]
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("1054")]
        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
        public void SwitchProductionModeAutoManualWhileMachineIsProducing()
        {

            // const int machine1 = 3;

            // // Setup a production group with Pause Between Orders enabled.
            // File.Copy(".\\AdditionalFiles\\ProductionGroupsPG1OrdersQueue.xml", Path.Combine(TestUtilities.MachineManagerInstallPath(),
            //        TestUtilities.DefaultUninstallDataDir, TestUtilities.ProductGroupXMLFile), true);
            // //setup the first fusion to have the right corrugate
            // File.Copy(".\\AdditionalFiles\\CorrugatesConfiguration33-6.xml", Path.Combine(TestUtilities.MachineManagerInstallPath(),
            //        TestUtilitiesDefaultUninstallDataDir, TestUtilities.CorrugateConfigXMLFile), true);
            // File.Copy(".\\AdditionalFiles\\MachinesConfigurationFusionWCorr4.xml", Path.Combine(TestUtilities.MachineManagerInstallPath(),
            //        TestUtilities.DefaultUninstallDataDir, TestUtilities.MachinesConfigurationXMLFile), true);


            // TestUtilities.RestartPackNetServer();

            // // Create new article with article id 555317
            // var article = new Article { ArticleId = "555317", Description = "article 555317", Length = 7, Width = 7, Height = 7, DesignId = 2010001, DesignName = "IQ 201-0001 INCHES", CorrugateQuality = 1};

            // ArticleTests.CreateNewArticle(article);

            // // Drop the attached file.
            // // 1 order with 4 carton requests is imported.
            //// const string optimalCorrugate = "Corrugate 33.688 0.16 0";
            // const string autoOrderId = "order1054";

            // const int qty = 10;

            // DropFileTests.DropFileInDirectory(
            //     new List<string> {
            //         "Type;OrderId;ArticleId;Quantity;PickZone\n" +
            //         "Order;"+autoOrderId+";"+article.ArticleId+";"+qty+";AZ1"
            //     });

            // var sim = FusionSimulatorTests.ConnectAndGetFusionSim("127.0.0.1", 8081, "127.0.0.1", 9104);


            // //operator console init here
            // var operatorConsole = TestUtilities.LoginAsOperatorAndCreateOperatorConsole(machine1);
            // OpenOrdersWidget openOrderControl = operatorConsole.OpenOrdersWidget;

            // if(operatorConsole.AutoManualSwitch.Checked)
            //     operatorConsole.AutoManualSwitchDiv.Click();


            // // Add a Custom Job with a quantity of 4 from the machines operator console.
            // CreateCustomJob(operatorConsole, "iQ Fusion Machine(3)",
            //     new List<SCustomJob>
            //            {
            //                new SCustomJob
            //                {
            //                    DesignAlias = "IQ 201-0001 INCHES",
            //                    Length = 7,
            //                    Width = 7,
            //                    Height = 7,
            //                    Quantity = qty,
            //                }
            //            });

            // //FusionSimulatorTests.UnPause(operatorConsole, sim);
            // //sim.LButton.Click();

            // // The machine starts producing the manual job.
            // FusionSimulatorTests.ProduceJobs(sim, 1);

            // var manualOrderId = openOrderControl.Orders[0].OrderId;

            // // While the manual job is producing. Switch production mode to Auto
            // operatorConsole.AutoManualSwitchDiv.Click();
            // // Order1054 is displayed in the Open Orders list and the machine PAUSES

            // // Wait for machine to finish up the carton currently in progress and it should also fininsh the one left in queue
            // FusionSimulatorTests.ProduceJobs(sim, 2);

            // // Example: If machine has 2 jobs, both will go to "Jobs sent to Machine" Job 1 is started, the other one is queued,
            // // it will finish all jobs in the "Jobs sent to machine" window before going to a different Production Type.

            // // Machine is paused.
            // Retry.For(() => (operatorConsole.MachineStatus.InnerText == "Machine is Paused"), TimeSpan.FromSeconds(20));
            // Specify.That(operatorConsole.MachineStatus.InnerText == "Machine is Paused").Should.BeTrue(String.Format("Machine {0} should be paused.", machine1));

            // // Unpause the machine.
            // sim.LButton.Click();

            // // The machine starts producing Order1054 
            // Specify.That(openOrderControl.Orders[0].OrderId == autoOrderId)
            //     .Should.BeTrue(String.Format("{0} is the Auto order that should have appeared.", autoOrderId));

            // // While Order1054 is producing. Switch production mode to Manual.
            // FusionSimulatorTests.ProduceJobs(sim, 1);
            // operatorConsole.AutoManualSwitchDiv.Click();

            // // The manual job is displayed in the Open Orders list.

            // // Wait for machine to finish up the carton currently in progress as on step 6.
            // FusionSimulatorTests.ProduceJobs(sim, 1);
            // // Machine is paused
            // Retry.For(() => (operatorConsole.MachineStatus.InnerText == "Machine is Paused"), TimeSpan.FromSeconds(20));
            // Specify.That(operatorConsole.MachineStatus.InnerText == "Machine is Paused").Should.BeTrue(String.Format("Machine {0} should be paused.", machine1));

            // // Unpause the machine.
            // sim.LButton.Click();
            // // The machine continues producing the manual job
            // Specify.That(openOrderControl.Orders[0].OrderId == manualOrderId)
            //     .Should.BeTrue(String.Format("{0} is the Manual order that should have appeared.", manualOrderId));


            // // Wait for the manual job to be completed 
            // FusionSimulatorTests.ProduceJobs(sim, 2);
            // // Machine is paused when done.
            // sim.LButton.Click();

            // // Retry.For(() => (operatorConsole.MachineStatus.InnerText == "Machine is Paused"), TimeSpan.FromSeconds(20));
            // // Specify.That(sim.IsMachinePaused).Should.BeTrue(String.Format("Machine {0} should be paused.", machine1));

            // // Switch machine to Auto mode
            // operatorConsole.AutoManualSwitchDiv.Click();

            // // Machine continues producing Order1054 until done.
            // FusionSimulatorTests.ProduceJobs(sim, 1);
            // Specify.That(openOrderControl.Orders[0].OrderId == autoOrderId)
            //     .Should.BeTrue(String.Format("{0} is the Auto order that should have appeared.", autoOrderId));
            // FusionSimulatorTests.ProduceJobs(sim, 8);
            // operatorConsole.Close();
        }

        [Obsolete("3.0 UI Update")]
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("1055")]
        public void PauseBetweenOrdersKeepsOrderStatusInProgressWhenReordingTheQueueWithDistributedOrders()
        {
            //// Connect .Server and Packsize Suite
            //const int machine1 = 3;
            //const int machine2 = 5;

            //var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(AdminConsoleTests.pageUrl);

            //// Set MaxNumberOfTiles to 1

            //// Setup a production group for two machines, and enable Pause Between Orders
            //ProductionGroupTests.AddMachineToProductionGroup( "PG1", "iQ Fusion Machine");
            //ProductionGroupTests.AddMachineToProductionGroup("PG1", "iQ Fusion Machine 2");
            //ProductionGroupTests.EnablePauseBetweenOrders("PG1");
            //ProductionGroupTests.EnableShouldDistributeOrders(adminConsole, "PG1");

            //// Create new article with article id 555317
            //var article = new Article { ArticleId = "555317", Description = "article 555317", Length = 10, Width = 10, Height = 10, DesignId = 2010001, DesignName = "IQ 0201-0001 INCHES" };

            //ArticleTests.CreateNewArticle(adminConsole, article);

            //adminConsole.Close();

            //// Drop the attached file
            //// 3 order is created. Order1055-1 has a quantity of 50. Order1055-2 and Order1055-3 has a quantity of 2 each.
            //const string optimalCorrugate = "Corrugate 33.688 0.16 0";
            //string[] orderIds = { "order1055-1", "order1055-2", "order1055-3" };

            //const int qty = 50;

            //DropFileTests.DropFileInDirectory(
            //    new List<string> {
            //        "1;"+orderIds[0]+";555317;"+qty+";AZ1",
            //        "1;"+orderIds[1]+";555317;2;AZ1",
            //        "1;"+orderIds[2]+";555317;2;AZ1"
            //    });


            //// Unpause machine1 
            //var sim1 = FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8081);
            //var sim2 = FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8082);
            //var sims = new List<SimulatedFusion> { sim1, sim2 };

            //var operatorPanel1 = OpenOperatorPageAndLoginAsAdminAndSetOptimalCorrugate(machine1, optimalCorrugate);

            //FusionSimulatorTests.UnPause(operatorPanel1, sim1);

            //// then click the Auto button in the Operator Console window
            //operatorPanel1.AutoButton.Click();

            //// Machine1 start producing order1055-1
            //var openOrderControl1 = operatorPanel1.OpenOrdersWidget;
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 10);

            //// Distribute Order1055-1.
            //Specify.That(openOrderControl1.Orders[0].OrderId == orderIds[0])
            //    .Should.BeTrue(String.Format("{0} was not in the first position.", orderIds[0]));

            //openOrderControl1.Orders[0].Distribute.Click();

            //// Unpause machine2 then click the Auto button in the Operator Console window.
            //var operatorPanel2 = OpenOperatorPageAndLoginAsAdminAndSetOptimalCorrugate(machine2, optimalCorrugate);

            //FusionSimulatorTests.UnPause(operatorPanel2, sim2);

            //operatorPanel2.AutoButton.Click();

            //// Machine2 starts producing Order1055-1.
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 10);

            //Specify.That(openOrderControl1.Orders[0].MachinesThatHaveWorkedOnOrder == 2)
            //    .Should.BeTrue(String.Format("Two machines should have worked on {0}", orderIds[0]));
            //Specify.That(openOrderControl1.Orders[1].OrderStatus == "InQueue").Should.BeTrue(String.Format("{0} is not InQueue.", orderIds[1]));

            //// Check the box next to Order1055-2 and click Create Next
            //var openOrderControl2 = operatorPanel2.OpenOrdersWidget;
            ////openOrderControl2.Orders[1].SelectCheckBox.Check();
            //var orderCheckBox = operatorPanel2.Get<CUITe_HtmlCheckBox>("id=cb" + orderIds[1]);
            //orderCheckBox.Check();

            //openOrderControl2.CreateNextButton.Click();

            //// A dialog opens asking to confirm the move. Click Continue to confirm the move. Order1055-2 is moved to the top of the list (this may take a few seconds).
            //openOrderControl2.ContinueCreateNextButton.Click();

            //FusionSimulatorTests.ProduceJobsOnMachineUntilPaused(sims);

            //// Wait for both machine to finish up boxes in queue.
            //// Machine 1 and 2 pauses.
            //const string pausedOrderStatus = "Connect a machine, run machine and set to 'Auto' to produce this order.";
            //Specify.That(sim1.IsMachinePaused && sim2.IsMachinePaused).Should.BeTrue("Both machines should be paused.");
            //Specify.That(openOrderControl2.Orders[0].OrderStatus == pausedOrderStatus).Should.BeTrue(String.Format("{0} is not paused.", orderIds[0]));
            //Specify.That(openOrderControl2.Orders[1].OrderStatus == pausedOrderStatus).Should.BeTrue(String.Format("{0} is not paused.", orderIds[1]));

            //// Unpause machine1
            //FusionSimulatorTests.UnPause(operatorPanel1, sim1);

            //// Machine1 starts producing Order1055-2. Order1055-1 has status in progress.
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sim1, 1);

            //Retry.For(() => openOrderControl1.Orders.Any(), TimeSpan.FromSeconds(5));
            //Specify.That(openOrderControl1.Orders[1].OrderStatus == "InProgress").Should.BeTrue(String.Format("{0} should be InProgress on Machine {1} but was {2}.", orderIds[0], machine1, openOrderControl1.Orders[1].OrderStatus));

            //// Wait for machine1 to complete Order1055-2.
            //FusionSimulatorTests.ProduceJobsOnMachineUntilPaused(sim1);

            //// Machine1 is paused.
            //Specify.That(sim1.IsMachinePaused).Should.BeTrue(String.Format("Machine {0} should be paused.", machine1));

            //// Unpause Machine1.
            //FusionSimulatorTests.UnPause(operatorPanel1, sim1);

            //// Machine1 continues production of Order1055-1.
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sim1, 1);

            //// Wait for Order1055-1 to be completed.
            //FusionSimulatorTests.ProduceJobsOnMachineUntilPaused(sim1);

            //// Machine1 and 2 are paused. 
            //Specify.That(sim1.IsMachinePaused && sim2.IsMachinePaused).Should.BeTrue("Both machines should be paused.");

            //// Unpause machine1
            //FusionSimulatorTests.UnPause(operatorPanel1, sim1);

            //// Machine1 produces Order1055-3
            //FusionSimulatorTests.DistributeAndProduceJobsEvenly(sim1, 2);

            //operatorPanel1.Close();
        }

        [Obsolete("3.0 UI Update")]
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("1050")]
        public void ReOrderOrdersOnMultipleMachinesInOperatorPanel()
        {
            //        // Setup a production group for two machines and set Pause Between Orders to true.
            //        const int machine1 = 3;
            //        const int machine2 = 5;


            //        // Create new article with article id 5555
            //        var article = new Article
            //        {
            //            ArticleId = "555317",
            //            Description = "4099",
            //            Length = 12.13,
            //            Width = 7.71,
            //            Height = 9.6,
            //            DesignId = 2010001,
            //            DesignName = "IQ 0201-0001 INCHES"
            //        };
            //       var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(AdminConsoleTests.pageUrl);

            // //      ArticleTests.CreateNewArticle(adminConsole, article);

            ////       ProductionGroupTests.AddMachineToProductionGroup("PG1", "iQ Fusion Machine");
            ////       ProductionGroupTests.AddMachineToProductionGroup("PG1", "iQ Fusion Machine 2");
            //       ProductionGroupTests.EnablePauseBetweenOrders();

            //        // Drop the attached file.
            //        // 4 orders with 4 carton requests each are imported

            //        string[] orderIds = { "order1050-1", "order1050-2", "order1050-3", "order1050-4" };

            //        const int qty = 25;

            //        DropFileTests.DropFileInDirectory(
            //            new List<string>
            //            {
            //                "1;"+orderIds[0]+";555317;"+qty+";AZ1",
            //                "1;"+orderIds[1]+";555317;"+qty+";AZ1",
            //                "1;"+orderIds[2]+";555317;"+qty+";AZ1",
            //                "1;"+orderIds[3]+";555317;"+qty+";AZ1"
            //            });

            //        // In Packnet.Server open the Operator Console window for machine1 and machine2.
            //        // 4 orders are shown as "Set a machine to 'Auto" to produce this order"

            //        var sim1 = FusionSimulatorTests.ConnectAndGetFusionSim("127.0.0.1", 8081, "127.0.0.1", 9104);

            //        //var sim1 = FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8081);
            //        var sim2 = FusionSimulatorTests.ConnectAndGetFusionSim("127.0.0.1", 8082, "127.0.0.1", 9106);



            //        //var sim2 = FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8082);
            //        var sims = new List<FusionSimulatorWindow> { sim2, sim2 };


            // //       AdminConsoleTests.SetMachineInAutoMode( machine1);
            // //       AdminConsoleTests.SetMachineInAutoMode( machine2);


            //        // Start the first operator panel.
            //        const string optimalCorrugate = "Corrugate 33.688 0.16 0";

            //        var operatorPanel1 = OpenOperatorPageAndLoginAsAdminAndSetOptimalCorrugate(machine1, optimalCorrugate);
            //        var operatorPanel2 = OpenOperatorPageAndLoginAsAdminAndSetOptimalCorrugate(machine2, optimalCorrugate);

            //        sim1.LButton.Click();
            //        sim2.LButton.Click();

            //        // While Order1050-1 and Order1050-2 are producing. Check the box next to order1050-4 then click the button CreateNext

            // //       FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 2);

            //        // While order9123 is producing. Check the box next to order9789 then click the button CreateNext
            //        operatorPanel1.Get<CUITe_HtmlCheckBox>("id=cb" + orderIds[2]).Check();

            //        // since we are creating enough jobs to make the OpenOrders div scroll we have to ensure that we scroll to the end
            //        // so that the entire dom is loaded.  Otherwise you only get the hml loaded for the visible items.
            //        Mouse.Click(new Point(880, 400));// these coords are the bottom of the scroll bar

            //        var openOrderControl = operatorPanel1.OpenOrdersWidget;

            //        Specify.That(openOrderControl.Orders[2].OrderId == orderIds[2])
            //            .Should.BeTrue(String.Format("Order {0} was not in the third position.", orderIds[2]));

            //        Retry.For(() => openOrderControl.CreateNextButton.Click(), TimeSpan.FromSeconds(60));
            //        // A dialog opens asking to confirm the move. Click Continue to confirm the move. Order1050-4 is moved to the top of the list, above Order1050-1 and Order1050-2 (this may take a few seconds)
            //        Retry.For(() => openOrderControl.ContinueCreateNextButton.Enabled, TimeSpan.FromSeconds(2));
            //        Specify.That(openOrderControl.ContinueCreateNextButton.Enabled).Should.BeTrue("Continue click next dialog was not displayed");
            //        openOrderControl.ContinueCreateNextButton.Click();

            //     //   FusionSimulatorTests.DistributeAndProduceJobsEvenly(sims, 2);

            //        // Wait for the UI to change, look in the first and second position since we don't know which sim grabbed it.
            //        Retry.For(() => openOrderControl.Orders[1].OrderId == orderIds[2] || openOrderControl.Orders[0].OrderId == orderIds[2], TimeSpan.FromMinutes(1));

            //        Specify.That(openOrderControl.Orders[1].OrderId == orderIds[2] || openOrderControl.Orders[0].OrderId == orderIds[2])
            //            .Should.BeTrue(String.Format("Order {0} was not in the first or second position.", orderIds[2]));
        }

        [Obsolete("3.0 UI Update")]
        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("631")]
        public void ReOrderOrdersOnASingleMachineInOperatorPanel()
        {
            //const int machineId = 3;

            //var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator<AdminConsole>(AdminConsoleTests.pageUrl);

            //// Create new article with article id 5555
            //var article = new Article
            //{
            //    ArticleId = "5555",
            //    Description = "article 5555",
            //    Length = 10,
            //    Width = 10,
            //    Height = 10,
            //    DesignId = 2010001,
            //    DesignName = "IQ 0201-0001 INCHES"
            //};

            //ArticleTests.CreateNewArticle(adminConsole, article);
            //// ProductionGroupExternalApplicationTests.EnablePauseBetweenOrders(adminConsole, "PG1");
            //ProductionGroupTests.AddMachineToProductionGroup("PG1", machineId);

            //const int qty = 4;
            //string[] orderIds = { "order9123", "order9456", "order9789" };
            //DropFileTests.DropFileInDirectory(
            //    new List<string>
            //    {
            //            "1;"+orderIds[0]+";"+article.ArticleId+";"+qty+";AZ1",
            //            "1;"+orderIds[1]+";"+article.ArticleId+";"+qty+";AZ1",
            //            "1;"+orderIds[2]+";"+article.ArticleId+";"+qty+";AZ1"
            //        });

            //// Unpause the machine then click the Auto button in the Operator Console window
            //FusionSimulatorTests.StartFusionSimulation("127.0.0.1", 8081);
            //AdminConsoleTests.SetMachineInAutoMode( machineId);
            //adminConsole.Close();

            ////Open Fusion OP
            ////AdminConsoleExternalApplicationTests.OpenOperatorPageForMachineNumber(adminConsole, machineId);
            ////var operatorPanel = CUITe_DynamicBrowserWindow.GetBrowserWindow<OperatorConsole>("iQ Fusion Machine(" + machineId + ")");
            //var operatorPanel = OpenOperatorPageAndLoginAsAdmin(machineId);
            //ChangeMachineCorrugate(operatorPanel, "Corrugate 33.688 0.16 0");

            //// While order9123 is producing. Check the box next to order9789 then click the button CreateNext
            //var orderCheckbox = operatorPanel.Get<CUITe_HtmlCheckBox>("id=cb" + orderIds[2]);
            //orderCheckbox.Check();

            //// since we are creating enough jobs to make the OpenOrders div scroll we have to ensure that we scroll to the end
            //// so that the entire dom is loaded.  Otherwise you only get the hml loaded for the visible items.
            //Mouse.Click(new Point(880, 400));// these coords are the bottom of the scroll bar

            //var openOrderControl = operatorPanel.OpenOrdersWidget;

            //Specify.That(openOrderControl.Orders[2].OrderId == orderIds[2])
            //    .Should.BeTrue(String.Format("Order {0} was not in the third position.", orderIds[2]));

            //Retry.For(() => openOrderControl.CreateNextButton.Click(), TimeSpan.FromSeconds(60));
            //if (openOrderControl.ContinueCreateNextButton.Enabled) { openOrderControl.ContinueCreateNextButton.Click(); }

            //// Wait for the UI to change.
            //Retry.For(() => openOrderControl.Orders[1].OrderId == orderIds[2], TimeSpan.FromSeconds(10));

            //Specify.That(openOrderControl.Orders[1].OrderId == orderIds[2])
            //    .Should.BeTrue(String.Format("Order {0} was not in the second position.", orderIds[2]));

            //operatorPanel.Close();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
        [TestCategory("169")]
        public void ProduceManualOrdersFromArticlesInOperatorPanel()
        {
            //we need some test articles...
            //ArticleHelper.Import100Articles();

            //// start sim
            ////TODO: Use new sim

            ////open operator panel for machine
            //var opPage = OpenOperatorPage(3);
            //TestUtilities.Login(opPage, TestSettings.Default.MachineManagerAdminUserName, TestUtilities.MachineManagerAdminPassword);

            //// change pg if necessary
            //if (opPage.AttachMachineWindowAlert.AlertDiv.Exists
            //    && opPage.AttachMachineWindowAlert.AlertText == "Add the machine to a production group to produce jobs")
            //{
            //    ChangeProductionGroupToFirstInList(opPage);
            //}

            //var createFromArticlesDialog = OperatorConsoleArticleDialog.OpenArticlesDialog(opPage);

            ////use this dictionary to keep track of the articles we created so we can use them to verify below.
            //var createdArticlesAndQuantity = new Dictionary<string, int>();

            ////Begin adding quantities to article rows.
            ////Row 2
            //createFromArticlesDialog.InputBox(1).SetText("2");
            //createdArticlesAndQuantity.Add(createFromArticlesDialog.ArticleTable.FindCell(1, 1).InnerText.Trim(), 2);

            ////article in row 1
            //createFromArticlesDialog.IncrementButton(0).Click();
            //createFromArticlesDialog.IncrementButton(0).Click();
            //createdArticlesAndQuantity.Add(createFromArticlesDialog.ArticleTable.FindCell(0, 1).InnerText.Trim(), 2);

            ////Go to Next page
            //createFromArticlesDialog.ArticleTable.NextPageButton.Click();

            ////article in row 3
            //createFromArticlesDialog.IncrementButton(2).Click();
            //createdArticlesAndQuantity.Add(createFromArticlesDialog.ArticleTable.FindCell(2, 1).InnerText.Trim(), 1);

            ////article in row 2
            //createFromArticlesDialog.InputBox(1).SetText("2");
            //createdArticlesAndQuantity.Add(createFromArticlesDialog.ArticleTable.FindCell(1, 1).InnerText.Trim(), 2);

            ////article in row 1
            //createFromArticlesDialog.IncrementButton(0).Click();
            //createFromArticlesDialog.IncrementButton(0).Click();
            //createdArticlesAndQuantity.Add(createFromArticlesDialog.ArticleTable.FindCell(0, 1).InnerText.Trim(), 2);

            ////create the articles now.
            //createFromArticlesDialog.CreateButton.Click();
            ////createFromArticlesDialog.Alert.VerifyAlert(true, AlertStates.Success, "9 total cartons created from 5 articles");
            //createFromArticlesDialog.CloseButton.Click();

            //// since we are creating enough jobs to make the OpenOrders div scroll we have to ensure that we scroll to the end
            //// so that the entire dom is loaded.  Otherwise you only get the hml loaded for the visible items.
            //Mouse.Click(new Point(880, 400));// these coords are the bottom of the scroll bar

            //var openOrderControl = opPage.OpenOrdersWidget;

            ////We should have 5 order created
            ////Specify.That(openOrderControl.Orders.Count).Should.BeEqualTo(5);
            ////ensure that each order is correct
            //foreach (var orderWidget in openOrderControl.Orders)
            //{
            //    Debug.WriteLine(orderWidget.ArticleId + " " + orderWidget.OriginalQuantity);
            //    if (createdArticlesAndQuantity.ContainsKey(orderWidget.ArticleId))
            //        Specify.That(createdArticlesAndQuantity[orderWidget.ArticleId])
            //            .Should.BeEqualTo(orderWidget.OriginalQuantity);
            //    else
            //    {
            //        Assert.Fail(orderWidget.ArticleId + "Order Widget Article ID unexpected");
            //    }

            //}
            //end of test because we don't really need to test that the machine can produce the orders.
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("167")]
        [DeploymentItem("AdditionalFiles", "AdditionalFiles")]
        public void OperatorCanFilterAndPageArticles()
        {
            //TODO: use new sim

            ArticleHelper.Import100Articles();

            Assert.Fail("The following code does not work since machines are identified with guids and not int's");
            //            var opPage = OpenOperatorPage(3);
            var loginPage = DynamicBrowserWindowWithLogin.GetWindow(); // not to create an error this substitues the above line
            loginPage.Login(TestSettings.Default.MachineManagerAdminUserName, TestSettings.Default.MachineManagerAdminPassword);
            //Specify.That(opPage.MachineStatus.InnerText.Trim()).Should.BeEqualTo("Machine is Disconnected");

            // TODO: 2016-06-15
            //if (opPage.AttachMachineWindowAlert.AlertText == "Add the machine to a production group to produce jobs")
            //{
            //    ChangeProductionGroupToFirstInList(opPage);
            //}

            //opPage.ArticleButton.Click();

            // Go to next page in the Article Management list
            //            var articleDialog = OperatorConsoleArticleDialog.OpenArticlesDialog(opPage);

            var articleDialog = new ArticlesManagementPage();
            var articlesTable = articleDialog.ArticlesTable;
            //           articlesTable.PageSize.SelectItem(2);

            //select the first item to copy
            var originalArticle = ArticleHelper.GetArticle(articlesTable, 0);

            // Search for a certain Article ID that exists 
            ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, originalArticle.ArticleId, true);

            // Search for a certain DesignName that exists
            ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, originalArticle.DesignName, true);

            // Search for a certain Description that exists
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, originalArticle.Description, true);

            // Search for a certain Length that exists
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, originalArticle.Length.ToString(CultureInfo.InvariantCulture), true);

            // Search for a certain Width that exists
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, originalArticle.Width.ToString(CultureInfo.InvariantCulture), true);

            // Search for a certain Height that exists
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, originalArticle.Height.ToString(CultureInfo.InvariantCulture), true);

            //  Search for a certain Corrugate Quality that exists
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, originalArticle.CorrugateQuality.ToString(), true);

            // Search for a value that DOES NOT exist
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, "AbC123", false);

            // Go to search field, type baNaNNa and press search button 
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, "baBoOn", true);

            // Go to search field, type aN press search button
            //ArticleHelper.SearchForValueAndVerifyItExists(articlesTable, "BoOn", true);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("783")]
        public void OperatorCanChangeProductionGroup()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService("localization");
//            new LocalizationTests().SetupLocalizationDatabase();

            var opPage = TestUtilities.CreatePageAndLoginAsOperator<OperatorConsole>();
            opPage.SelectMachineGroup(TestDatabaseManagement.Mg_Localization.Alias);

            var cpPage = new SelectProductionGroupPage();
            // the select method includes a verification that the selection went well
            cpPage.Select("TestBL", opPage);
            cpPage.Select("TestOR", opPage);
        }
        #endregion
    }
}
