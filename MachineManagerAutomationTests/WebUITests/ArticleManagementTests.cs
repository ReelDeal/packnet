﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

using MongoDB.Bson.Serialization;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Utils;
using PackNet.Plugin.ArticleService.Data;

using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.Macros;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    public class ArticleManagementTests : ExternalApplicationTestBase
    {
        #region Constructor and initializers
        public ArticleManagementTests()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            TestUtilities.DropDbAndRestart();
        }
        #endregion

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void CreateCartonTest()
        {
            CreateCartonOrLabelTest(TestDatabaseManagement.Carton1_d2000001_l10w10h10, null);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("11505")]
        public void ArticlePreview_ShouldShow_WhenCreateACarton()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var page = ArticlesManagementPage.GetWindow();

            page.CreateArticle(TestDatabaseManagement.Article111, adminConsole);

            var carton = TestDatabaseManagement.Carton1_d2000001_l10w10h10;
            page.ClickNewSubArticleButton();
            page.ClickNewCartonButton();
            page.SetCartonName("Carton 1");
            page.VerifyCartonPreview(carton);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void CreateArticleWithCartonAndLabelTest()
        {
            var article = new ArticleExt("New article", "Article Description");
            var carton = TestDatabaseManagement.Carton1_d2000001_l10w10h10;
            var label = TestDatabaseManagement.Label1;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = new ArticlesManagementPage();

            page.CreateArticle(article, adminConsole);

            page.CreateCarton(carton);

            page.CreateLabel(label);

            //save the article
            page.ClickSaveArticleButton();

            //specify that article exists in grid
            Playback.Wait(1000);
            page.VerifyArticle(article, carton, 1, label, 1);

            //open article and verify items
            page.PageTableSelectRow(0);
            page.ClickEditArticleButton();

            page.RefreshTables();
            page.VerifyCartonInGrid(carton, 1);
            page.VerifyLabelInGrid(label, 1);

            //delete the label inside the article
            page.ArticlesTable.SelectRowWithValue(label.Alias);
            page.ClickDeleteSubArticleButton();

            ArticleMacros.VerifyDatabaseHasExpectedNumberOfArticles(1);
            page.ArticlesTable.VerifyContainsValue(label.Alias, false, string.Format("Label with alias '{0}' was not deleted.", label.Alias));

            //save the kit and close form
            page.ClickSaveArticleButton();

            // delete the kit
            page.RefreshTables();
            page.PageTableSelectRow(0);
            page.ClickDeleteArticleButton();

            page.VerifyTableIsEmpty();
            ArticleMacros.VerifyDbTableIsEmpty();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void EditLabelTest()
        {
            var label = TestDatabaseManagement.Label1;
            var editedLabel = TestDatabaseManagement.Label_Edit;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            var page = new ArticlesManagementPage();

            // create an article and label
            page.CreateArticle(TestDatabaseManagement.Article111, adminConsole);
            page.CreateLabel(label);

            // update the just created label
            page.UpdateLabelInfo(0, editedLabel, 2);

            //verify info in grid after the update
            page.RefreshTables();
            page.VerifyLabelInGrid(editedLabel, 2);

            // save
            page.ClickSaveArticleButton();

            // verify that correct info is saved to the database
            var kit = ArticleMacros.VerifyArticleInDatabase(editedLabel.Alias, editedLabel.CustomerDescription);
            var order = (Order)kit.ItemsToProduce.First();
            ArticleMacros.VerifyLabelInDatabase((Label)order.Producible, editedLabel);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void EditCartonTest()
        {
            var carton = TestDatabaseManagement.Carton1_d2000001_l10w10h10;
            var cartonEdit = TestDatabaseManagement.Carton2_d2000001_l11w11h11;
            cartonEdit.DesignId = DesignHelper.GetDesignIdFromName(Designs.IQ02000001INCHES);
            var design = (PackagingDesign)DesignHelper.LoadPackagingDesignFromDisk(Designs.IQ02000001INCHES);
            cartonEdit.DesignId = design.Id;
            var page = ArticlesManagementPage.GetWindow();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            // create article and carton
            page.CreateArticle(TestDatabaseManagement.Article111, adminConsole);
            page.CreateCarton(carton);

            // update the just created carton
            page.UpdateCartonInfo(0, cartonEdit, 2);

            //verify info in grid after the update
            page.RefreshTables();
            page.VerifyCartonInGrid(cartonEdit, 2);

            // save
            page.ClickSaveArticleButton();

            // verify that correct info is saved to the database
            var kit = ArticleMacros.VerifyArticleInDatabase(cartonEdit.CustomerUniqueId, cartonEdit.CustomerDescription);
            var order = (Order)kit.ItemsToProduce.First();
            ArticleMacros.VerifyCartonInDatabase((Carton)order.Producible, cartonEdit);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void EditArticleWithCartonAndLabelTest()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var page = ArticlesManagementPage.GetWindow();

            var carton = TestDatabaseManagement.Carton1_d2000001_l10w10h10;
            var cartonEdit = TestDatabaseManagement.Carton2_d2000001_l11w11h11;
            var article = new ArticleExt("New article", "Article description");
            var articleEdit = new ArticleExt("Edited article", "Edited description");
            var label = TestDatabaseManagement.Label1;
            var labelEdit = TestDatabaseManagement.Label_Edit;

            // create article with both carton and label
            page.CreateArticle(article, adminConsole);
            page.CreateCarton(carton);
            page.CreateLabel(label);

            //save
            page.ClickSaveArticleButton();

            // verify that article exists in grid
            Playback.Wait(1000);
            page.VerifyArticle(article, carton, carton.Quantity, label, 1);

            // edit the article with new data
            page.UpdateArticleInfo(0, articleEdit, false);

            //carton - assumes carton is first and label is second
            cartonEdit.DesignId = DesignHelper.GetDesignIdFromName(Designs.IQ02000001INCHES);
            page.RefreshTables();
            page.UpdateCartonInfo(0, cartonEdit, 2);

            //edit label
            page.RefreshTables();
            page.UpdateLabelInfo(1, labelEdit, 3);

            //verify info in grid
            page.RefreshTables();
            page.VerifyCartonInGrid(carton, 2);
            page.VerifyLabelInGrid(labelEdit, 3);

            //save article
            page.ClickSaveArticleButton();

            // verify article in the grid
            page.RefreshTables();
            page.VerifyArticle(articleEdit, carton, 2, labelEdit, 2);

            //reopen article and verify items
            page.PageTableSelectRow(0);
            page.ClickEditArticleButton();

            page.RefreshTables();
            page.VerifyCartonInGrid(carton, 1);
            page.VerifyLabelInGrid(labelEdit, 1);

        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void ImportArticleFile()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.Article);
            var articlePage = ArticlesManagementPage.GetWindow();
            articlePage.ClickImportButton();

            var impPage = ArticleImportPage.GetWindow();
            impPage.ClickBrowseFileButton();
            impPage.SelectFile("articles_10kits.csv");
            impPage.ClickProcessFileButton();

            impPage.ArticlesTable.ClickArticleImportIgnore(0);
            impPage.ArticlesTable.ClickArticleImportUpdate(1);
            impPage.ArticlesTable.ClickArticleImportIgnore(3);

            impPage.ClickFinalizeImportButton();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void ExportArticleFile()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.Article);
            var articlePage = ArticlesManagementPage.GetWindow();
            ArticleMacros.BuildArticle(TestDatabaseManagement.Article111, adminConsole);
            articlePage.ClickExportButton();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void DeleteFromProduceArticles()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            ArticleMacros.BuildArticle(TestDatabaseManagement.Article111, adminConsole);
            ArticleMacros.BuildArticle(TestDatabaseManagement.Article222, adminConsole);
            ArticleMacros.BuildArticle(TestDatabaseManagement.Article333, adminConsole);

            var articlePage = ArticlesManagementPage.GetWindow();
            articlePage.ArticlesTable.SelectRows(0, 1, 2);
            articlePage.ClickProduceButton();

            AdminProduceFromArticlePage page = new AdminProduceFromArticlePage();
            page.ArticlesTable.SelectRow(0);
            page.ClickDeleteButton();
            page.ArticlesTable.VerifyTableHasExpectedNumberOfRecords(2);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void ProduceFromProduceArticles()
        {
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.Article);
            var articlePage = ArticlesManagementPage.GetWindow();
            articlePage.ArticlesTable.SelectRows(0, 1);
            articlePage.ClickProduceButton();

            AdminProduceFromArticlePage page = new AdminProduceFromArticlePage();

            // here i am supposed to select a production group and change quantity to 2 but
            // i can't find out how to reach the pg dropdown and quantity field. What's the id??

            page.ClickProduceButton();
        }

        #region Internal methods

        /// <summary>
        ///  The same method is used for both cartons and labels since so many of the steps are the same. Only carton OR
        ///  label can be supplied
        /// </summary>
        /// <param name="carton">The carton to be created. </param>
        /// <param name="label">The label to be created</param>
        public void CreateCartonOrLabelTest(Carton carton, LabelExt label)
        {
            var repo = new ArticleRepository();
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            var article = TestDatabaseManagement.Article111;
            var id = string.Empty;

            var page = ArticlesManagementPage.GetWindow();

            // create new article
            page.CreateArticle(article, adminConsole);

            // create and save carton or label depending on whats supplied into this method
            if (carton != null)
            {
                page.CreateCarton(carton);
                page.VerifyCartonInGrid(carton, 1);
                page.ClickSaveArticleButton();
                id = carton.CustomerUniqueId;
            }
            else if (label != null)
            {
                page.CreateLabel(label);
                page.VerifyLabelInGrid(label, 1);
                page.ClickSaveArticleButton();
                id = label.CustomerUniqueId;
            }

            var kit = ArticleMacros.VerifyArticleInDatabase(article.ArticleId, article.Description);

            // verify that carton or label is saved to database
            var order = (Order)kit.ItemsToProduce.First();
            if (carton != null)
                ArticleMacros.VerifyCartonInDatabase((Carton)order.Producible, carton);
            else if (label != null)
                ArticleMacros.VerifyLabelInDatabase((Label)order.Producible, label);

            // select article to edit it
            page.RefreshTables();
            page.PageTableSelectRow(0);
            page.ClickEditArticleButton();

            page.RefreshTables();
            page.ArticlesTable.VerifyContainsValue(id);
            page.PageTableSelectRow(0);
            page.ClickDeleteSubArticleButton();

            // save article after deleting the carton
            page.ClickSaveArticleButton();

            // it is not possible to save an article without subarticles
            page.VerifySubArticleRequiredMessage(article.ArticleId);

            if (carton != null)
                page.CreateCarton(carton);
            else if (label != null)
                page.CreateLabel(label);

            page.ClickSaveArticleButton();

            //// check in the database if it has been removed
            //kit = (Kit)repo.All().First().Producible;
            //Specify.That(kit.ItemsToProduce.Count).Should.BeEqualTo(0, "Producible was not removed from database");

            // delete the article
            page.RefreshTables();
            page.PageTableSelectRow(0);
            page.ClickDeleteArticleButton();

            // verify the database is empty
            ArticleMacros.VerifyDbTableIsEmpty();
        }

        #endregion
    }
}
