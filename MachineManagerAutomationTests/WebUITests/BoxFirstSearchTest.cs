﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Interfaces.Enums;
using PackNet.Data.Machines;
using PackNet.FusionSim.ViewModels;
using PackNet.Common.Utils;

using MachineManagerAutomationTests.Macros;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

namespace MachineManagerAutomationTests.WebUITests
{
    [CodedUITest]
    [DeploymentItem("AdditionalFiles\\Template.prn", "Data\\Labels")]
    [DeploymentItem("NLog.config")]
    public class BoxFirstSearchTest : ExternalApplicationTestBase
    {
        #region Variables
        SimulatorUtilities simUtilities = null;
        #endregion

        #region Constructor and initiators
        public BoxFirstSearchTest()
            : base(
                TestSettings.Default.MachineManagerProcessName,
                Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe),
                TestSettings.Default.CloseAppBetweenTests)
        { }
        protected override void BeforeTestInitialize()
        {
            simUtilities = new SimulatorUtilities();
        }

        protected override void AfterTestCleanup()
        {
            simUtilities.Close();
            Thread.Sleep(1000);
            base.AfterTestCleanup();
        }
        #endregion

        #region Tests

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void LoadBoxFirstPreconfigs()
        {
            TestDatabaseManagement.RestoreLatestMongoDumpAndRestartService(SelectionAlgorithmTypes.BoxFirst);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("")]
        public void SetupBoxFirstSearchTestingEnvironment()
        {
            TestUtilities.DropDbAndRestart();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            SetupBoxFirstEnvironment(adminConsole);

            DropFileUtilities.DropBoxFirstDropFile(6);

            SetupBoxFirstSearchTestingEnvironmentSub(TestDatabaseManagement.Fm1, TestDatabaseManagement.Zp1, TestDatabaseManagement.Mg2_fm1zp1, adminConsole);
            SetupBoxFirstSearchTestingEnvironmentSub(TestDatabaseManagement.Fm2, TestDatabaseManagement.Zp2, TestDatabaseManagement.Mg6_fm2zp2, adminConsole);

            TestDatabaseManagement.CreateMongoDump(SelectionAlgorithmTypes.BoxFirst);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9029")]
        public void BoxFirstSearchLinkDoesNotExistInMenu_IfThereAreNoPGsInBoxFirst()
        {
            TestUtilities.DropDbAndRestart();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            ProductionGroupMacros.Build(TestDatabaseManagement.Pg24_order_mg3_c22, adminConsole);
            adminConsole.NavigateToPage(NavPages.SearchOrders);
            adminConsole.VerifySearchBoxFirstLinkExists();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9030")]
        public void BoxFirstSearchExistInMenu_IfThereArePGsInBoxFirst()
        {
            LoadBoxFirstPreconfigs();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);
            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);
            page.VerifySearchPageIsLoaded();
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9032")]
        [TestCategory("9034")]
        public void BoxFirstStatusSearch()
        {
            //Loading DB snapshot should be within 24 hours after the DB snapshot was dumped. 
            LoadBoxFirstPreconfigs();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);

            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);

            page.VerifySearchPageIsLoaded();

            Specify.That(page.StatusComboBox.SelectedItem == JobStatus.All).Should.BeTrue("ALL status not selected");

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);

            page.VerifyPageContainsValue("Total Items: 0");

            page.ClickSearchButton();

            page.VerifyPageContainsValue("Total Items: 6");

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(6);

            page.SetStatus(JobStatus.Completed);

            page.ClickSearchButton();

            page.VerifyPageContainsValue("Total Items: 2");

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(1);

            page.ResultTable.VerifyCellValue(0, 2, JobStatus.Completed);

            page.SetStatus(JobStatus.Open);

            page.ClickSearchButton();

            page.RefreshTables();

            page.VerifyPageContainsValue("Total Items: 5");

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(5);

            page.ResultTable.VerifyCellValue(0, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(1, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(2, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(3, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(4, 2, JobStatus.Open);

            page.SetStatus(JobStatus.InProgress);

            page.ClickSearchButton();

            page.VerifyPageContainsValue("Total Items: 0");

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);

            page.SetStatus(JobStatus.Failed);

            page.ClickSearchButton();

            page.VerifyPageContainsValue("Total Items: 0");

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);

            page.SetStatus(JobStatus.NotProducible);

            page.ClickSearchButton();

            page.VerifyPageContainsValue("Total Items: 0");

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9033")]
        public void BoxFirstCustomerIDSearch()
        {
            //Loading DB snapshot should be within 24 hours after the DB snapshot was dumped. 
            LoadBoxFirstPreconfigs();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);

            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);

            page.IsSearchBoxFirstPageLoaded();

            Specify.That(page.CustomerIdTextBox.Exists).Should.BeTrue();

            Specify.That(page.CustomerIdTextBox.InnerText).Should.BeEqualTo("");

            Specify.That(page.ResultTable.RowCount()).Should.BeEqualTo(0);

            page.SetCustomerId("0");

            page.ClickSearchButton();

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(1);

            page.ResultTable.VerifyCellValue(0, 0, 0);

            page.SetCustomerId("11");

            page.ClickSearchButton();

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
        }


        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9033")]
        public void BoxFirstCreatedTimeRangeSearch()
        {
            //Loading DB snapshot should be within 24 hours after the DB snapshot was dumped. 
            LoadBoxFirstPreconfigs();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);

            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);

            page.IsSearchBoxFirstPageLoaded();

            page.ClickSearchButton();

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(6);

            page.ClickOpenStartDatePickerButton();

            Specify.That(page.BeginDateTimePicker.Exists && page.BeginDateTimePicker.Enabled).Should.BeEqualTo(true);

            Assert.Inconclusive("TODO: Find a way to use time picker in automation test!");

            Retry.For(() => page.PickDayAnd12AMInTheTable(page.BeginDateTimePicker, 0), TimeSpan.FromSeconds(6));

            Retry.For(() => page.OpenEndDatePickerButton.SafeClick(), TimeSpan.FromSeconds(6));

            Specify.That(page.EndDateTimePicker.Exists && page.EndDateTimePicker.Enabled).Should.BeEqualTo(true);

            Retry.For(() => page.PickDayAnd12AMInTheTable(page.EndDateTimePicker, 1), TimeSpan.FromSeconds(6));

            page.ClickSearchButton();

            Specify.That(page.ResultTable.RowCount()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9037")]
        public void BoxFirstSearch_SortingBasedOnTheHeader()
        {
            LoadBoxFirstPreconfigs();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);

            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);

            page.VerifySearchPageIsLoaded();

            Specify.That(page.StatusComboBox.SelectedItem == JobStatus.All).Should.BeTrue();

            page.ClickSearchButton();

            page.VerifyPageContainsValue("Total Items: 6");

            Retry.For(() => page.ResultTable.TableDiv.Get<CUITe_HtmlSpan>("InnerText=Customer Id").SafeClick(), TimeSpan.FromSeconds(6));

            page.ResultTable.VerifyCellValue(0, 0, 0);
            page.ResultTable.VerifyCellValue(1, 0, 1);
            page.ResultTable.VerifyCellValue(2, 0, 2);
            page.ResultTable.VerifyCellValue(3, 0, 3);
            page.ResultTable.VerifyCellValue(4, 0, 4);
            page.ResultTable.VerifyCellValue(5, 0, 5);

            Retry.For(() => page.ResultTable.TableDiv.Get<CUITe_HtmlSpan>("InnerText=Customer Id").SafeClick(), TimeSpan.FromSeconds(6));

            page.ResultTable.VerifyCellValue(0, 0, 5);
            page.ResultTable.VerifyCellValue(1, 0, 4);
            page.ResultTable.VerifyCellValue(2, 0, 3);
            page.ResultTable.VerifyCellValue(3, 0, 2);
            page.ResultTable.VerifyCellValue(4, 0, 1);
            page.ResultTable.VerifyCellValue(5, 0, 0);

            Retry.For(() => page.ResultTable.TableDiv.Get<CUITe_HtmlSpan>("InnerText=Status").SafeClick(), TimeSpan.FromSeconds(6));

            page.ResultTable.VerifyCellValue(0, 2, JobStatus.Completed);
            page.ResultTable.VerifyCellValue(1, 2, JobStatus.Completed);
            page.ResultTable.VerifyCellValue(2, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(3, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(4, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(5, 2, JobStatus.Open);

            Retry.For(() => page.ResultTable.TableDiv.Get<CUITe_HtmlSpan>("InnerText=Status").SafeClick(), TimeSpan.FromSeconds(6));

            page.ResultTable.VerifyCellValue(0, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(1, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(2, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(3, 2, JobStatus.Open);
            page.ResultTable.VerifyCellValue(4, 2, JobStatus.Completed);
            page.ResultTable.VerifyCellValue(5, 2, JobStatus.Completed);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9038")]
        public void BoxFirstSearch_ReproduceJobsFromSearchResultList()
        {
            LoadBoxFirstPreconfigs();

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);

            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);

            page.SetStatus(JobStatus.All);

            page.ClickSearchButton();

            Retry.For(() => page.ResultTable.TableDiv.Get<CUITe_HtmlSpan>("InnerText=Status").SafeClick(), TimeSpan.FromSeconds(6));

            if (page.ResultTable.GetCellValue(2, 2) != JobStatus.Open)
                page.ResultTable.TableDiv.Get<CUITe_HtmlSpan>("InnerText=Status").SafeClick();

            var firstRowCheckBox = page.BoxFirst_FindResultGridRowCheckBoxDiv(0);
            var ThirdRowCheckBox = page.BoxFirst_FindResultGridRowCheckBoxDiv(2);
            var headerRowCheckBox = page.BoxFirst_FindResultGridHeaderCheckBoxDiv();

            Specify.That(page.ReproduceButton.Enabled).Should.BeEqualTo(false);
            // Check all rows
            Specify.That(headerRowCheckBox.Exists).Should.BeEqualTo(true);

            Retry.For(() => headerRowCheckBox.SafeClick(), TimeSpan.FromSeconds(6));

            Specify.That(page.ReproduceButton.Enabled).Should.BeEqualTo(false);

            //Uncheck all rows
            Retry.For(() => headerRowCheckBox.SafeClick(), TimeSpan.FromSeconds(6));

            Specify.That(page.ReproduceButton.Enabled).Should.BeEqualTo(false);

            //Check first completed row
            Specify.That(firstRowCheckBox.Exists).Should.BeEqualTo(true);

            Retry.For(() => firstRowCheckBox.SafeClick(), TimeSpan.FromSeconds(6));

            Specify.That(page.ReproduceButton.Enabled).Should.BeEqualTo(true);

            //Check one not completed job row
            Retry.For(() => ThirdRowCheckBox.SafeClick(), TimeSpan.FromSeconds(6));

            Assert.Inconclusive("The Test Is Failing Now, Connected With Bug 9434!");
            Specify.That(page.ReproduceButton.Enabled).Should.BeEqualTo(false);
        }

        [TestMethod]
        [TestCategory("UIAutomation")]
        [TestCategory("9039")]
        public void BoxFirstAdditionalSearch()
        {
            LoadBoxFirstPreconfigs();

            var pz1 = TestDatabaseManagement.PickZone1;
            var pz2 = TestDatabaseManagement.PickZone2;

            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();

            adminConsole.NavigateToPage(NavPages.SearchBoxFirst);

            var page = new SearchPage(SelectionAlgorithmTypes.BoxFirst);

            page.ClickSearchButton();

            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(6);

            page.ClickToggleAdditionalOptionsButton();

            Specify.That(page.MachineGroupComboBox.Exists && page.MachineGroupComboBox.Enabled).Should.BeEqualTo(true);

            Specify.That(page.MachineGroupComboBox.SelectedItem).Should.BeEqualTo("All");

            // machine group additional searching
            page.SetMachineGroup(TestDatabaseManagement.Mg2_fm1zp1.Alias);
            page.ClickSearchButton();

            var printer = TestDatabaseManagement.Zp1;
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);
            page.ResultTable.VerifyCellValue(0, 4, printer.Alias);
            page.ResultTable.VerifyCellValue(1, 4, printer.Alias);
            page.ResultTable.VerifyCellValue(2, 4, printer.Alias);

            // Carton details design searching
            page.SetDesign(Designs.IQ02000001INCHES);
            page.ClickSearchButton();
            Specify.That(page.ResultTable.RowCount()).Should.BeEqualTo(0);

            page.SetDesign(Designs.IQ02030001INCHES);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);

            // Carton details length searching
            page.SetLength(1);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
            page.SetLength(10);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);

            // Carton details width searching
            page.SetWidth(1);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
            page.SetWidth(10);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);

            // Carton details height searching
            page.SetHeight(1);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
            page.SetHeight(5);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);

            // Corrugate details Name searching
            page.SetCorrugateName(TestDatabaseManagement.Corrugate_w22t011.Alias);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);

            page.SetCorrugateName(TestDatabaseManagement.Corrugate_w37t011.Alias);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);

            // BoxFirst Properties PickZone searching
            Retry.For(() => page.SetPickZone(pz1.Alias), TimeSpan.FromSeconds(6));
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
            Retry.For(() => page.SetPickZone(pz2.Alias), TimeSpan.FromSeconds(6));
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);

            // BoxFirst Properties Classification searching
            page.SetClassification(TestDatabaseManagement.Classification1.Alias);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
            page.SetClassification(TestDatabaseManagement.Classification2.Alias);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);

            // BoxFirst Properties CPG searching
            page.SetCartonPropertyGroup(TestDatabaseManagement.Cpg1.Alias);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(0);
            page.SetCartonPropertyGroup(TestDatabaseManagement.Cpg2.Alias);
            page.ClickSearchButton();
            page.ResultTable.VerifyTableHasExpectedNumberOfRecords(3);
        }
        #endregion

        #region Helpers

        private void SetupBoxFirstSearchTestingEnvironmentSub(FusionMachineExt fusion, ZebraPrinterExt printer, MachineGroupExt mg, AdminConsole adminConsole)
        {
            var fmSim = simUtilities.ConnectAndGetFusionSim(fusion);
            var zpSim = simUtilities.ConnectAndGetPrinterSim(printer);

            SimulatorHelper.SetMachineGroupOnline(fmSim, SimulatorHelper.CreatePrinterSimList(zpSim), mg.SetId(), adminConsole);
            SimulatorHelper.CompleteTheJobSentToMachine(fmSim, zpSim);
        }

        public static void SetupBoxFirstEnvironment(AdminConsole adminConsole = null)
        {
            TestUtilities.DropDbAndRestart();

            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            ProductionGroupMacros.Build(TestDatabaseManagement.Pg3_boxfirst_mg2mg6_c199c2755c37, adminConsole);

            adminConsole.AddWidgetToDashboard(DashboardWidgets.Classifications, DashboardLayouts.Row1_1Col_Row2_3Cols);

            var widget = new ClassificationsWidget();
            widget.SetPrinterPriority(TestDatabaseManagement.Classification1, true);

            TestDatabaseManagement.CreateMongoDump(SelectionAlgorithmTypes.BoxFirst);
        }

        #endregion
    }
}