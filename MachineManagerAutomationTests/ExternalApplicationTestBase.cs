﻿using System;
using System.Diagnostics;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UITesting;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;

using MachineManagerAutomationTests.WebUITests.Repository;

using PackNet.Common.Utils;
using PackNet.FusionSim.ViewModels;

using Testing.Specificity;
using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests
{

    /// <summary>
    /// Launches and manages an external process for the duration of the test
    ///  </summary>
    [TestClass]
    public abstract class ExternalApplicationTestBase
    {
        protected ApplicationUnderTest applicationUnderTest;

        protected bool CloseAppBetweenTests;

        private readonly int waitTimeout;

        protected bool ForceApplicationCloseAtStartup;

        protected readonly string ExePath;

        private readonly string testProcessName;

        private Process testProcess;

        private readonly bool shouldStartApplication;

        public DateTime TestStartTime { get; set; }

        public ExternalApplicationTestBase(string testProcessName, string exePath, bool closeAppBetweenTests = true, bool shouldStart = true, int waitTimeout = 2)
        {
            this.testProcessName = testProcessName;
            ExePath = exePath;
            shouldStartApplication = shouldStart;
            CloseAppBetweenTests = closeAppBetweenTests;
            this.waitTimeout = waitTimeout;
        }

        protected virtual void BeforeTestInitialize() { }
        protected virtual void AfterTestInitialize() { }

        protected virtual void TestInitializeBeforeStartProcess() { }

        protected virtual void BeforeTestCleanup() { }
        protected virtual void AfterTestCleanup() { }
        protected virtual void BeforeClassCleanup() { }
        protected virtual void AfterClassCleanup() { }

        [TestCleanup]
        public void TestCleanup()
        {
            BeforeTestCleanup();

            if (CloseAppBetweenTests && applicationUnderTest != null)
                applicationUnderTest.Close();
            if (Playback.IsSessionStarted)
                Playback.Cleanup();

            AfterTestCleanup();
        }

        [ClassCleanup]
        public void Cleanup()
        {
            BeforeClassCleanup();

            if (testProcess != null)
                testProcess.Close();

            if (Playback.IsSessionStarted)
                Playback.StopSession();

            AfterClassCleanup();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            BeforeTestInitialize();
            TestStartTime = DateTime.Now;
            if (!Playback.IsInitialized)
            {
                Playback.Initialize();
            }

            BrowserWindow.CurrentBrowser = TestSettings.Default.CurrentBrowser;

            if (!shouldStartApplication)
                return;

            Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.Disabled;
            var runningProcesses = Process.GetProcessesByName(testProcessName);

            if (ForceApplicationCloseAtStartup && runningProcesses.Length > 0)
            {
                //Specify.Failure("Can't currently stop server");
                foreach (var runningProcess in runningProcesses)
                {
                    if (runningProcess.ProcessName == "PackNet.Server")
                    {
                        //restart service if it exists because we have to clear out the stale data in the DB
                        var services = ServiceController.GetServices();
                        var packnetService = services.FirstOrDefault(s => s.DisplayName.Contains("PackNet.Server"));
                        if (packnetService != null)
                        {
                            packnetService.Stop();
                            packnetService.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(30));
                            packnetService.Start();
                        }
                        else
                        {
                            runningProcess.Kill();
                        }
                    }
                    else
                        runningProcess.Kill();
                }
            }

            if (runningProcesses.Length == 0 || ForceApplicationCloseAtStartup)
            {
                TestInitializeBeforeStartProcess();

                if (ExePath.ToLower().EndsWith(".msi"))
                {
                    testProcess = new Process
                    {
                        StartInfo = { FileName = ExePath }
                    };
                    testProcess.Start();
                }
                else
                {
                    var services = ServiceController.GetServices();
                    //If the service is not installed and it is not .Server execute the command in  MSTest
                    if (!services.Any(s => s.DisplayName.Contains("PackNet.Server")) || !ExePath.Contains("Packnet.Server"))
                    {
                        if (!File.Exists(ExePath))
                        {
                            Trace.WriteLine("Failed to locate exe at path: " + ExePath);
                            Trace.WriteLine("Current directory is  " + Environment.CurrentDirectory);
                        }
                        applicationUnderTest = ApplicationUnderTest.Launch(ExePath, string.Empty,
                            ExePath.Contains("Packnet.Server") ? "--console" : "");
                    }
                    else //ensure service is started
                    {
                        var packnetService = services.First(s => s.DisplayName.Contains("PackNet.Server"));
                        if (packnetService.Status != ServiceControllerStatus.Running)
                        {
                            packnetService.Start();
                        }
                    }
                    //we need to wait for some apps to start (.server)
                    Thread.Sleep(TimeSpan.FromSeconds(waitTimeout));
                }
            }
            //else
            //    applicationUnderTest = ApplicationUnderTest.FromProcess(runningProcesses[0]);

            //if (applicationUnderTest != null)
            //{
            //    applicationUnderTest.CloseOnPlaybackCleanup = CloseAppBetweenTests;
            //}

            AfterTestInitialize();
        }

        public List<String> GetWorkFlowTimingLogs(DateTime startTime, DateTime endTime)
        {
            var logPath = Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log");
            var magicSearchString = "InvokeWorkFlowAndWaitForResult for Workflow";
            var logLines = File.ReadAllLines(logPath);
            var results = new List<string>();
            foreach (var logLine in logLines)
            {
                var logTime = ParseTimeStampFromLogLine(logLine);
                if (!logTime.HasValue)
                    continue;

                if (logTime >= startTime && logTime <= endTime && logLine.Contains(magicSearchString))
                    results.Add(logLine);

            }
            return results;
        }

        private DateTime? ParseTimeStampFromLogLine(string logLine)
        {
            var split = logLine.Split('|');
            DateTime dt;
            if (DateTime.TryParse(split[0], out dt))
                return dt;
            return null;
        }

        public void WriteToOutput(string str)
        {
            Debug.WriteLine(str);
        }

        public void WriteToOutput(List<string> strings)
        {
            strings.ForEach(WriteToOutput);
        }


        //public static void CompleteJobOnSim(FusionSimulatorViewModel sim, int packagingTime = 3)
        //{
        //    SimulatorHelper.VerifySimulatorHasCartonsInQueue(sim);

        //    var machineProdPage = MachineProductionPage.GetWindow(null, ProductionMode.Auto, UserType.Administrator);

        //    machineProdPage.ItemsInProductionTable.VerifyContainsValue("Sent");

        //    //simulate packaging time
        //    sim.StartStopPackaging();
        //    Thread.Sleep(TimeSpan.FromSeconds(packagingTime));

        //    // verify item in table is started
        //    machineProdPage.ItemsInProductionTable.VerifyContainsValue("Started");

        //    // produce one more
        //    sim.StartStopPackaging();

        //}

        #region Performance Testing Helpers

        public bool CheckFileForImportUpdate(string importFileName)
        {
            string[] lines = { };
            bool readSuccess = false;
            try
            {
                while (!readSuccess)
                {
                    lines = File.ReadAllLines(TestUtilities.GetLogPath(LogTypes.Regular));
                    readSuccess = true;
                }
            }
            catch { }

            for (int i = lines.Length - 1; i > 0; i--)
            {
                if (lines[i].Contains(importFileName) && lines[i].ToLower().Contains("done processing file"))
                    return true;
            }
            return false;
        }

        #endregion
    }
}