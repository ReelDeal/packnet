﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.InstallerTests.Repository;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;


namespace MachineManagerAutomationTests.InstallerTests
{
    using PackNet.Common.Utils;

    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    [DeploymentItem("PackNet.Server Prerequisite Installer.exe")]
    public class WixPreServerInstallerTests : ExternalApplicationTestBase
    {

        protected override void BeforeTestInitialize()
        {
            StagingTests.WixPreInstallerCopyFromBuild();
            TestContext.WriteLine("Current working dir: " + Environment.CurrentDirectory);
        }

        public WixPreServerInstallerTests()
            : base(TestSettings.Default.WixPreInstallerProcessName, Path.Combine(TestSettings.Default.LocalTempDir, TestSettings.Default.WixPreInstallerPath))
        {
            ForceApplicationCloseAtStartup = true;
        }

        [TestMethod]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        [TestCategory("5737")]
        //This test is for pre-requisites for .server 3.0. The correct test settings need to be set
        //PreReqInstallerLocation with the correct path to where the pre-requisites installer is located
        public void WixPreInstallerWillInstallOnCleanSystem()
        {
            var buildNumber = GetLatestPrereqBuildNumber();
            var wixPreInstaller = new WixPreSetupWindow(buildNumber);
            wixPreInstaller.InstallButton.Click();
            
            //Installing
            //Close is visible but disabled
            Retry.For(() =>
            {
                bool successExists = wixPreInstaller.SuccessText.Exists;
                bool failExists = wixPreInstaller.FailText.Exists;
                return successExists || failExists;
            }, TimeSpan.FromMinutes(30));
            
            wixPreInstaller.CloseButton.Click();

            //If the installer does fail, notify
            Specify.That(wixPreInstaller.FailText.Exists).Should.BeFalse("The Prerequisite installer failed.");
        }

       
        [TestMethod]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void PreInstallerUninstaller()
        {
            var wixPreInstallerUninstall = new WixPreSetupWindow();
            wixPreInstallerUninstall.UninstallButton.Click();

            Retry.For(() =>
            {
                bool successExists = wixPreInstallerUninstall.SuccessText.Exists;
                bool failExists = wixPreInstallerUninstall.FailText.Exists;
                return successExists || failExists;
            }, TimeSpan.FromMinutes(5));

            wixPreInstallerUninstall.CloseButton.Click();

            //If the installer does fail, notify
            Specify.That(wixPreInstallerUninstall.FailText.Exists).Should.BeFalse("The Prerequisite uninstaller failed.");

        }

        [TestMethod]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void PreInstallerOptions()
        {
            var wixPreInstallerOptions = new WixPreSetupWindow();
            //
            wixPreInstallerOptions.OptionButton.Click();

            wixPreInstallerOptions.CloseButton.Click();

        }


        public string GetLatestPrereqBuildNumber()
        {
            var latestBuildDir = TestUtilities.GetLastWrittenDirectory(TestSettings.Default.PreReqInstallerLocation);
            var exe = latestBuildDir.GetFiles().FirstOrDefault(f => f.Name == "PackNet.Server Prerequisite Installer.exe");
            if (exe != null)
            {
                var info = FileVersionInfo.GetVersionInfo(exe.FullName);
                return info.FileVersion;
            }
            return null;
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;
    }
}
