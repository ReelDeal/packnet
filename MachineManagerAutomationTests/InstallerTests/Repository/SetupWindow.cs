﻿using CUITe.Controls.WinControls;
namespace MachineManagerAutomationTests.InstallerTests.Repository
{
    public class SetupWindow : CUITe_WinWindow
    {
        public SetupWindow() : base("Name=Machine Manager 2.2.0 Setup") { }
        
        //UI Detection Details -- ControlType:ControlType.Button, Automation ID:, Name:Close
        public CUITe_WinButton CloseButton { get { return Get<CUITe_WinWindow>("Name=Close").Get<CUITe_WinButton>(); } }

        //UI Detection Details -- ControlType:ControlType.Button, Automation ID:2, Name:OK
        public CUITe_WinButton OKButton { get { return Get<CUITe_WinWindow>("Name=OK").Get<CUITe_WinButton>(); } }
        public CUITe_WinButton FinishButton { get { return Get<CUITe_WinWindow>("Name=Finish").Get<CUITe_WinButton>(); } }
        public CUITe_WinButton NextButton { get { return Get<CUITe_WinWindow>("Name=Next >").Get<CUITe_WinButton>(); } }
        public CUITe_WinButton InstallButton { get { return Get<CUITe_WinWindow>("Name=Install").Get<CUITe_WinButton>(); } }
        public CUITe_WinButton BrowseButton { get { return Get<CUITe_WinWindow>("Name=Browse...").Get<CUITe_WinButton>(); } }

        public CUITe_WinButton YesButton { get { return Get<CUITe_WinWindow>("Name=Yes").Get<CUITe_WinButton>(); } }
        public CUITe_WinButton NoButton { get { return Get<CUITe_WinWindow>("Name=No").Get<CUITe_WinButton>(); } }

        public CUITe_WinEdit Edit { get { return Get<CUITe_WinWindow>().Get<CUITe_WinEdit>(); } }
        public CUITe_WinText TextMessage { get { return Get<CUITe_WinWindow>().Get<CUITe_WinText>(); } }

        public CUITe_WinRadioButton RebootNowRadioButton { get { return Get<CUITe_WinWindow>("Name=Reboot now").Get<CUITe_WinRadioButton>(); } }
        public CUITe_WinRadioButton RebootLaterRadioButton { get { return Get<CUITe_WinWindow>("Name=I want to manually reboot later").Get<CUITe_WinRadioButton>(); } }
    }


}
