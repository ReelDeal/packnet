﻿using CUITe.Controls.WinControls;
namespace MachineManagerAutomationTests.InstallerTests.Repository
{
    public class UninstallWindow : CUITe_WinWindow
    {
        public UninstallWindow(string version) : base(string.Format("Name=Packsize PackNet.Server {0} Setup", version)) { }
        public CUITe_WinButton NextButton { get { return Get<CUITe_WinButton>("Name=Next"); } }
        public CUITe_WinButton RemoveButton { get { return Get<CUITe_WinButton>("Name=Remove"); } }
        public CUITe_WinButton FinishButton { get { return Get<CUITe_WinButton>("Name=Finish"); } }
        public CUITe_WinButton CancelButton { get { return Get<CUITe_WinButton>("Name=Cancel"); } }

    }


}
