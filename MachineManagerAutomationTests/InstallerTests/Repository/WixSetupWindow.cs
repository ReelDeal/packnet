﻿using CUITe.Controls.HtmlControls;
using CUITe.Controls.WinControls;

namespace MachineManagerAutomationTests.InstallerTests.Repository
{
    public class WixSetupWindow : CUITe_WinWindow
    {
        public WixSetupWindow(string version) : base(string.Format("Name=Packsize PackNet.Server {0} Setup", version)) { }
        public CUITe_WinButton NextButton { get { return Get<CUITe_WinButton>("Name=Next"); } }
        public CUITe_WinCheckBox AcceptLicenseCheckBox { get { return Get<CUITe_WinCheckBox>("Name=I accept the terms in the License Agreement"); } }
        public CUITe_WinButton InstallButton { get { return Get<CUITe_WinButton>("Name=Install"); } }
        public CUITe_WinButton FinishButton { get { return Get<CUITe_WinButton>("Name=Finish"); } }
        public CUITe_WinText SetupFinished { get { return Get<CUITe_WinText>("Name=Click the Finish button to exit the Setup Wizard."); } }
        public CUITe_WinText SetupInterrupted { get { return Get<CUITe_WinText>("Name=Setup Wizard was interrupted"); } }
        public CUITe_HtmlListItem TestList { get { return Get<CUITe_HtmlListItem>("id=test"); } }

    }


}
