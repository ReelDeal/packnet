﻿using CUITe.Controls.WinControls;
namespace MachineManagerAutomationTests.InstallerTests.Repository
{
    public class WixPreSetupWindow : CUITe_WinWindow
    {
        public WixPreSetupWindow(string buildNumber) : base("Name=PackNet.Server Prerequisite Installer " + buildNumber + " Setup") { }
        public WixPreSetupWindow() : base("Name=PackNet.Server Prerequisite Installer Setup") { }
        public CUITe_WinButton InstallButton { get { return Get<CUITe_WinButton>("Name=Install"); } }
        public CUITe_WinButton CloseButton { get { return Get<CUITe_WinButton>("Name=Close"); } }
        public CUITe_WinButton UninstallButton { get { return Get<CUITe_WinButton>("Name=Uninstall"); } }
        public CUITe_WinButton OptionButton { get { return Get<CUITe_WinButton>("Name=Option"); } }
        public CUITe_WinText SuccessText { get { return Get<CUITe_WinText>("Name=Setup Successful"); } }
        public CUITe_WinText FailText { get { return Get<CUITe_WinText>("Name=Setup Failed"); } }
    }


}
