﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

using MachineManagerAutomationTests.InstallerTests.Repository;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;
using System.Diagnostics;

using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests.InstallerTests
{
    using PackNet.Common.Utils;

    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class WixServerInstallerTests : ExternalApplicationTestBase
    {
        public WixServerInstallerTests()
            : base(
                String.Format(TestSettings.Default.WixInstallerProcessName, GetBuildNumber()),
                TestSettings.Default.WixInstallerPath)
        {
            ForceApplicationCloseAtStartup = true;
        }

        public static string GetBuildNumber()
        {
            return Assembly.GetAssembly(typeof(WixServerInstallerTests)).GetName().Version.ToString();
        }

        protected override void BeforeTestInitialize()
        {
        }

        [DllImport("user32.dll")]
        static extern IntPtr SetFocus(IntPtr hWnd);
        public void ReFocusSetupWindow()
        {
            var setupWindow = Process.GetProcessesByName(GetBuildNumber()).FirstOrDefault();

            if (setupWindow != null)
            {
                SetFocus(setupWindow.MainWindowHandle);

            }
            


        }
        
        [TestMethod]
        [DeploymentItem("PackNet.Server Installer.msi")]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void PackNetInstallerWillInstallOnCleanSystem()
        {
            var wixInstaller = new WixSetupWindow(GetBuildNumber());
            
            // Wait for any background/pre-setup stuff to finish
            Retry.For(() => wixInstaller.NextButton.Enabled, TimeSpan.FromSeconds(10));
            
            // Testing this out..something happens in the background and causes the setup window to lose focus
            ReFocusSetupWindow();
            wixInstaller.NextButton.Click();

            // License accept Window
            wixInstaller.AcceptLicenseCheckBox.Checked = true;
            wixInstaller.NextButton.Click();
            // Destination Folder
            wixInstaller.NextButton.Click();

            // Potential timing issue
            Retry.For(() => wixInstaller.InstallButton.Enabled, TimeSpan.FromSeconds(10));

            // Ready to install
            wixInstaller.InstallButton.Click();

            //Installing
            //Next is visible but disabled
            Specify.That(wixInstaller.NextButton.Enabled).Should.BeFalse();

            Retry.For(() => wixInstaller.SetupFinished.Exists, TimeSpan.FromMinutes(10));
            
            wixInstaller.FinishButton.Click();
        }

        [TestMethod]
        [DeploymentItem("PackNet.Server Installer.msi")]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void PackNetServerWillUninstall()
        {
            var wixUninstall = new UninstallWindow(GetBuildNumber());
            
            //Get the install dir before it's removed from ARP
            var postInstall = TestSettings.Default.InstallLocation;

            ReFocusSetupWindow();

            // Wait for the next button to appear after initial launch
            Retry.For(() => wixUninstall.NextButton.Enabled, TimeSpan.FromSeconds(5));

            wixUninstall.NextButton.Click();
            // Remove
            wixUninstall.RemoveButton.Click();
            // Click Remove again
            wixUninstall.RemoveButton.Click();

            // Wait for the finish button to appear
            Retry.For(() => wixUninstall.FinishButton.Enabled, TimeSpan.FromSeconds(120));

            wixUninstall.FinishButton.Click();

            //Check to see if the install directory still exists
            Specify.That(Directory.Exists(postInstall)).Should.BeTrue("The PackNet.Server installation directory did not exist after uninstall.");

            //Check to see if the Data dir exists
            Specify.That(Directory.Exists(Path.Combine(TestSettings.Default.InstallLocation,
                "Data"))).Should.BeTrue("The PackNet.Server Data directory did not exist after uninstall.");

            //Check to see if the Logs dir exists
            Specify.That(Directory.Exists(Path.Combine(TestSettings.Default.InstallLocation,
                "Logs"))).Should.BeTrue("The PackNet.Server Logs directory did not exist after uninstall.");

        }

        [TestMethod]
        [DeploymentItem("PackNet.Server Installer.msi")]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void PackNetServerWillRepair()
        {

            File.Delete(Path.Combine(TestSettings.Default.InstallLocation,
                            TestSettings.Default.RepairFileName));

            var wixRepair = new RepairWindow(GetBuildNumber());
            Retry.For(() => wixRepair.NextButton.Enabled, TimeSpan.FromSeconds(5));

            ReFocusSetupWindow();

            wixRepair.NextButton.Click();
            // Repair
            wixRepair.RepairButton.Click();
            // Click Repair again
            wixRepair.RepairButton.Click();

            // Wait for the finish button to appear
            Retry.For(() => wixRepair.FinishButton.Enabled, TimeSpan.FromSeconds(120));

            wixRepair.FinishButton.Click();

            Specify.That(File.Exists(Path.Combine(TestSettings.Default.InstallLocation,
                TestSettings.Default.RepairFileName))).Should.BeTrue("The file did not get repaired!");

        }

        [TestMethod]
        [DeploymentItem("PackNet.Server Installer.msi")]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void PackNetServerWillRepairWhenPackNetIsRunning()
        {
            //Start an instance of PackNet before repair initiates
            var startPackNet = new PostWixServerInstallerExternalApplicationTests();

            startPackNet.ServerWillStartAfterInstall();

            //Sleep to allow Server to start
            Thread.Sleep(7000);

            var wixRepair = new RepairWindow(GetBuildNumber());
            Retry.For(() => wixRepair.NextButton.Enabled, TimeSpan.FromSeconds(5));

            ReFocusSetupWindow();

            wixRepair.NextButton.Click();
            // Repair
            wixRepair.RepairButton.Click();
            // Click Repair again
            wixRepair.RepairButton.Click();
            //Click Cancel
            wixRepair.CancelButton.Click();
            //Click Yes
            wixRepair.YesButton.Click();

            // Wait for the finish button to appear
            Retry.For(() => wixRepair.FinishButton.Enabled, TimeSpan.FromSeconds(120));

            wixRepair.FinishButton.Click();

        }

        [TestMethod]
        [DeploymentItem("PackNet.Server Installer.msi")]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void PackNetServerWillUninstallWhenPackNetIsRunning()
        {
            //Start an instance of PackNet before repair initiates so default dirs/files are created
            var startPackNet = new PostWixServerInstallerExternalApplicationTests();

            startPackNet.ServerWillStartAfterInstall();

            var uninstallWindow = new UninstallWindow(GetBuildNumber());
            Retry.For(() => uninstallWindow.NextButton.Enabled, TimeSpan.FromSeconds(5));

            ReFocusSetupWindow();
            
            uninstallWindow.NextButton.Click();
            // Remove
            uninstallWindow.RemoveButton.Click();
            // Click Repair again
            uninstallWindow.RemoveButton.Click();
            //Click Cancel
            uninstallWindow.CancelButton.Click();

            // Wait for the finish button to appear
            Retry.For(() => uninstallWindow.FinishButton.Enabled, TimeSpan.FromSeconds(120));

            uninstallWindow.FinishButton.Click();

        }


        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        private TestContext testContextInstance;
    }

    [CodedUITest]

    public class PostWixServerInstallerExternalApplicationTests : ExternalApplicationTestBase
    {

        public PostWixServerInstallerExternalApplicationTests()
            : base(
                String.Format(TestSettings.Default.WixInstallerProcessName, WixServerInstallerTests.GetBuildNumber()),
                TestSettings.Default.WixInstallerPath, false, false)
        {
            ForceApplicationCloseAtStartup = true;
        }

        protected override void BeforeTestInitialize()  {  }

        [TestMethod]
        [TestCategory("InstallerAutomation")]
        [TestCategory("UIAutomation")]
        public void ServerWillStartAfterInstall()
        {
            Specify.That(Process.GetProcessesByName("PackNet.Server").Count()).Should.BeEqualTo(1); 
            var adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator();
            adminConsole.VerifyLoggedInUsernameFieldHasExpectedValue("PacksizeAdmin");
        }
    }
}

