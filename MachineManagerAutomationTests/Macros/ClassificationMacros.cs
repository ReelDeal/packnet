﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Testing.Specificity;

using PackNet.Common.Utils;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Data.Classifications;

using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.Helpers;

namespace MachineManagerAutomationTests.Macros
{
    public static class ClassificationMacros
    {
        public static void Create(Classification classification, AdminConsole adminConsole = null)
        {
            ClassificationsPage page = new ClassificationsPage();
            page.Create(classification, adminConsole);
        }

        /// <summary>
        /// Verify if a classification exist in the database or not
        /// </summary>
        /// <param name="alias">The alias of the classification to look for in the database</param>
        /// <param name="shouldExistInDb">True if the classification should exist in the database, false if it should not exist.</param>
        /// <param name="repo">The repository to use when searching in the database</param>
        /// <returns>The existing classification</returns>
        public static Classification VerifyClassificationExistsInDatabase(string alias, bool shouldExistInDb = true, ClassificationRepository repo = null)
        {
            var cl = FindByAlias(alias, repo);
            TestUtilities.VerifyObjectExistsInDatabase(cl, "Classification", alias, shouldExistInDb);
            return cl;
        }

        public static Classification FindByAlias(string alias, ClassificationRepository repo = null)
        {
            if (repo == null) repo = new ClassificationRepository();
            return repo.Find(alias);
        }

        /// <summary>
        /// Verify that classification is saved to database. If Id is set (=existing classification) we'll search on that value,
        /// if not (=new classification) we search on the alias
        /// </summary>
        /// <param name="o">Classification to check if is saved and that the data is correct</param>
        /// <param name="repo">The repository to use when searching in the database</param>
        /// <returns>The saved object, if it exist</returns>
        public static Classification VerifyClassificationIsSavedToDatabase(Classification o, ClassificationRepository repo = null)
        {
            return VerifyClassificationIsSavedToDatabase(o, Guid.Empty, repo);
        }
        /// <summary>
        /// Verify that classification is saved to database. If Id is set (=existing classification) we'll search on that value,
        /// if not (=new classification) we search on the alias
        /// </summary>
        /// <param name="o"></param>
        /// <param name="idOfExistingObject"></param>
        /// <param name="repo"></param>
        /// <returns>The saved object, if it exist</returns>
        public static Classification VerifyClassificationIsSavedToDatabase(Classification o, Guid idOfExistingObject, ClassificationRepository repo = null)
        {
            if (repo == null) repo = new ClassificationRepository();
            Classification objInDb = null;

            if (idOfExistingObject != Guid.Empty)
            {
                objInDb = repo.Find(idOfExistingObject);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find saved Classification in database with supplied guid.");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, string.Format("Classification alias is not correct in database. Expected: {0} Actual: {1}.", o.Alias, objInDb.Alias));
            }
            else if (o.Id == Guid.Empty)
            {
                objInDb = repo.All().FirstOrDefault(z => z.Alias == o.Alias);
                Specify.That(objInDb).Should.Not.BeNull(string.Format("Failed to find Classification with alias '{0}'.", o.Alias));
            }
            else
            {
                objInDb = repo.Find(o.Id);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find edited Classification in database by record id.");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, string.Format("Classification alias is not correct in database. Expected: {0} Actual: {1}.", o.Alias, objInDb.Alias));
            }

            Specify.That(objInDb.Number).Should.BeEqualTo(o.Number, string.Format("Classification number is not correct in database. Expected: {0} Actual: {1}.", o.Number, objInDb.Number));

            return objInDb;
        }
    }
}
