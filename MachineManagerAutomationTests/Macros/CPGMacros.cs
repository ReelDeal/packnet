﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Testing.Specificity;

using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Data.CartonPropertyGroups;

using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

namespace MachineManagerAutomationTests.Macros
{
    public static class CPGMacros
    {
        public static void Create(List<CartonPropertyGroup> cpgs, AdminConsole adminConsole = null)
        {
            foreach (var cpg in cpgs)
                Create(cpg, adminConsole);
        }
        public static void Create(CartonPropertyGroup cpg, AdminConsole adminConsole = null, bool useValidation = false)
        {
            CartonPropertyGroupsPage page = new CartonPropertyGroupsPage();
            page.Create(cpg, adminConsole, useValidation);
        }
        public static CartonPropertyGroup VerifyCPGIsSavedToDatabase(CartonPropertyGroup o, CartonPropertyGroupRepository repo = null)
        {
            return VerifyCPGIsSavedToDatabase(o, Guid.Empty, repo);
        }
        public static CartonPropertyGroup VerifyCPGIsSavedToDatabase(CartonPropertyGroup o, Guid idOfExistingObject, CartonPropertyGroupRepository repo = null)
        {
            if (repo == null) repo = new CartonPropertyGroupRepository();
            CartonPropertyGroup objInDb = null;

            if (idOfExistingObject != Guid.Empty)
            {
                objInDb = repo.Find(idOfExistingObject);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find saved CPG in database with supplied guid.");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, string.Format("CPG alias is not correct in database. Expected: {0} Actual: {1}.", o.Alias, objInDb.Alias));
            }
            else if (o.Id == Guid.Empty)
            {
                objInDb = repo.All().FirstOrDefault(z => z.Alias == o.Alias);
                Specify.That(objInDb).Should.Not.BeNull(string.Format("Failed to find CPG with alias '{0}'.", o.Alias));
            }
            else
            {
                objInDb = repo.Find(o.Id);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find edited CPG in database by record id.");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, string.Format("CPG alias is not correct in database. Expected: {0} Actual: {1}.", o.Alias, objInDb.Alias));
            }

            return objInDb;
        }
        /// <summary>
        /// Verify if a pickzone exist or not in the database
        /// </summary>
        /// <param name="alias">The alias of the pickzone to look for in the database</param>
        /// <param name="shouldExistInDb">True if the pickzone should exist in the database, false if it should not exist.</param>
        public static CartonPropertyGroup VerifyCPGExistsInDatabase(string alias, bool shouldExistInDb = true, CartonPropertyGroupRepository repo = null)
        {
            var cpg = FindByAlias(alias, repo);
            TestUtilities.VerifyObjectExistsInDatabase(cpg, "Carton Property Group", alias, shouldExistInDb);
            return cpg;
        }
        public static CartonPropertyGroup FindByAlias(string alias, CartonPropertyGroupRepository repo = null)
        {
            if (repo == null) repo = new CartonPropertyGroupRepository();
            return repo.All().FirstOrDefault(z => z.Alias == alias);
        }
    }
}
