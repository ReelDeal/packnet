﻿using MachineManagerAutomationTests.WebUITests.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testing.Specificity;
using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.Helpers;
using PackNet.Common.Utils;
using PackNet.Common.Interfaces.DTO.Users;

namespace MachineManagerAutomationTests.Macros
{
    public static class UserMacros
    {
        public static void Create(User user, AdminConsole adminConsole = null)
        {
            var page = UserManagementPage.GetWindow();
            page.Create(user, adminConsole);
        }
        public static User CreateUserObject(string username, string password, bool isAutoLogin, bool isAdmin)
        {
            User user = new User();
            user.UserName = username;
            user.Password = password;
            user.IsAutoLogin = isAutoLogin;
            user.IsAdmin = isAdmin;
            return user;
        }

    }
}
