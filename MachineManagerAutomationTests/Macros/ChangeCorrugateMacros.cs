﻿using MachineManagerAutomationTests.WebUITests.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineManagerAutomationTests.Macros
{
    public static class ChangeCorrugateMacros
    {
        public static bool ChangeCorrugatesForAllMachineGroups(ProductionGroupExt pg, TestSteps testSteps, AdminConsole adminConsole)
        {
            ChangeCorrugatePage page = new ChangeCorrugatePage();
            return page.ChangeCorrugatesForAllMachineGroups(pg, testSteps, adminConsole);
        }
    }
}
