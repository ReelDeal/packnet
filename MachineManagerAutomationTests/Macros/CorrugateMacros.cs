﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;

using Testing.Specificity;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Utils;
using PackNet.Data.Corrugates;

using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;

namespace MachineManagerAutomationTests.Macros
{
    public static class CorrugateMacros
    {
        public static void Create(List<Corrugate> corrugates, AdminConsole adminConsole = null)
        {
            foreach (var c in corrugates)
                Create(c, adminConsole);
        }

        public static void Create(Corrugate corrugate, AdminConsole adminConsole)
        {
            var page = new CorrugatesPage();
            page.Create(corrugate, adminConsole);
        }

        public static Corrugate FindByAlias(string alias, CorrugateRepository repo = null)
        {
            if (repo == null) repo = new CorrugateRepository();
            return repo.All().FirstOrDefault(z => z.Alias == alias);
        }

        /// <summary>
        /// Verify if a corrugate exist in the database or not
        /// </summary>
        /// <param name="alias">The alias of the corrugate to look for in the database</param>
        /// <param name="shouldExistInDb">True if the corrugate should exist in the database, false if it should not exist.</param>
        /// <param name="repo">The repository to use when searching in the database</param>
        /// <returns>The existing corrugate</returns>
        public static Corrugate VerifyCorrugateExistsInDatabase(string alias, bool shouldExistInDb = true, CorrugateRepository repo = null)
        {
            var corrugate = FindByAlias(alias, repo);
            TestUtilities.VerifyObjectExistsInDatabase(corrugate, "Corrugate", alias, shouldExistInDb);
            return corrugate;
        }

        public static Corrugate VerifyCorrugateIsSavedToDatabase(Corrugate o, CorrugateRepository repo = null)
        {
            return VerifyCorrugateIsSavedToDatabase(o, Guid.Empty, repo);
        }
        
        public static Corrugate VerifyCorrugateIsSavedToDatabase(Corrugate o, Guid idOfExistingObject, CorrugateRepository repo = null)
        {
            if (repo == null) repo = new CorrugateRepository();
            Corrugate objInDb = null;

            if (idOfExistingObject != Guid.Empty)
            {
                objInDb = repo.Find(idOfExistingObject);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find saved corrugate in database with supplied guid");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, string.Format("Corrugate alias is not correct in database. Expected: {0} Actual: {1}.", o.Alias, objInDb.Alias));
            }
            else if (o.Id == Guid.Empty)
            {
                objInDb = repo.All().FirstOrDefault(z => z.Alias == o.Alias);
                Specify.That(objInDb).Should.Not.BeNull(string.Format("Failed to find Corrugate with alias '{0}'.", o.Alias));
            }
            else
            {
                objInDb = repo.Find(o.Id);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find edited corrugate in database by record id");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, "Corrugate alias is not correct in database");
            }

            Specify.That(objInDb.Width).Should.BeEqualTo(o.Width, string.Format("Width is not correct in database. Expected: {0} Actual: {1}.", o. Width, objInDb.Width));
            Specify.That(objInDb.Thickness).Should.BeEqualTo(o.Thickness, string.Format("Thickness is not correct in database. Expected: {0} Actual: {1}.", o.Thickness, objInDb.Thickness));
            Specify.That(objInDb.Quality).Should.BeEqualTo(o.Quality, string.Format("Quality is not correct in database. Expected: {0} Actual: {1}.", o.Quality, objInDb.Quality));

            return objInDb;
        }
    }
}
