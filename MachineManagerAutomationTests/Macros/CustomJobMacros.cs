﻿using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using Microsoft.VisualStudio.TestTools.UITesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testing.Specificity;
using MachineManagerAutomationTests.WebUITests;

namespace MachineManagerAutomationTests.Macros
{
    public static class CustomJobMacros
    {
        public static void Create(CustomJob cj, AdminConsole console = null)
        {
            var page = new CustomJobPage();
            page.Create(cj, console, false);
        }
    }
}
