﻿using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Utils;
using PackNet.Data.PickArea;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testing.Specificity;
using MachineManagerAutomationTests.WebUITests;

namespace MachineManagerAutomationTests.Macros
{
    public static class PickZoneMacros
    {
        public static void Create(List<PickZone> pickzones, AdminConsole adminConsole = null)
        {
            foreach (var pz in pickzones)
                Create(pz, adminConsole);
        }
        public static void Create(PickZone pz, AdminConsole adminConsole = null, bool useValidation = false)
        {
            PickZonesPage page = new PickZonesPage();
            page.Create(pz, adminConsole, useValidation);
        }
        public static PickZone VerifyPickZoneIsSavedToDatabase(PickZone o, PickZoneRepository repo = null)
        {
            return VerifyPickZoneIsSavedToDatabase(o, Guid.Empty, repo);
        }
        public static PickZone VerifyPickZoneIsSavedToDatabase(PickZone o, Guid idOfExistingObject, PickZoneRepository repo = null)
        {
            if (repo == null) repo = new PickZoneRepository();
            PickZone objInDb = null;

            if (idOfExistingObject != Guid.Empty)
            {
                objInDb = repo.Find(idOfExistingObject);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find saved PickZone in database with supplied guid");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, string.Format("Pickzone alias is not correct in database. Expected: {0} Actual: {1}.", o.Alias, objInDb.Alias));
            }
            else if (o.Id == Guid.Empty)
            {
                objInDb = repo.All().FirstOrDefault(z => z.Alias == o.Alias);
                Specify.That(objInDb).Should.Not.BeNull(string.Format("Failed to find PickZone with alias '{0}'.", o.Alias));
            }
            else
            {
                objInDb = repo.Find(o.Id);
                Specify.That(objInDb).Should.Not.BeNull("Failed to find edited PickZone in database by record id");
                Specify.That(objInDb.Alias).Should.BeEqualTo(o.Alias, "PickZone alias is not correct in database");
            }

            Specify.That(objInDb.Description).Should.BeEqualTo(o.Description, string.Format("Description is not correct in database. Expected: {0} Actual: {1}.", o. Description, objInDb.Description));

            return objInDb;
        }

        public static PickZone FindByAlias(string alias, PickZoneRepository repo = null)
        {
            if(repo == null) repo = new PickZoneRepository();
            return repo.FindByAlias(alias);
        }

        /// <summary>
        /// Verify if a pickzone exist in the database or not
        /// </summary>
        /// <param name="alias">The alias of the pickzone to look for in the database</param>
        /// <param name="shouldExistInDb">True if the pickzone should exist in the database, false if it should not exist.</param>
        /// <param name="repo">The repository to use when searching in the database</param>
        /// <returns>The existing pickzone</returns>
        public static PickZone VerifyPickZoneExistsInDatabase(string alias, bool shouldExistInDb = true, PickZoneRepository repo = null)
        {
            var pz = FindByAlias(alias,repo);
            TestUtilities.VerifyObjectExistsInDatabase(pz, "Pick Zone", alias, shouldExistInDb);
            return pz;
        }
    }
}
