﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

using Testing.Specificity;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Data.Machines;
using PackNet.Common.Utils;

using MachineManagerAutomationTests.WebUITests.Repository;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace MachineManagerAutomationTests.Macros
{
    public static class MachineGroupMacros
    {
        public static void Create(MachineGroupExt mg, AdminConsole adminConsole = null, bool checkMachinesAreCreated = false, bool useValidation = true)
        {
            MachineGroupPage page = new MachineGroupPage();
            page.Create(mg, adminConsole,checkMachinesAreCreated, useValidation);
        }
        public static void BuildMachineGroup(MachineGroupExt mg, AdminConsole adminConsole = null)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);
            var page = new MachineManagementPage();

            foreach (var m in mg.Machines)
            {
                m.MachineGroup = mg;
                MachineMacros.Build(m, adminConsole, page);
            }

            MachineGroupMacros.Create(mg, adminConsole, false, false);
        }
        public static void UpdateWorkflow(MachineGroupExt mg, string workflow, AdminConsole adminConsole)
        {
            var mgPage = MachineGroupPage.GetWindow();
            mgPage.UpdateWorkflow(mg, workflow, adminConsole);
        }
        public static MachineGroup FindByAlias(string alias, MachineGroupRepository repo = null)
        {
            if (repo == null) repo = new MachineGroupRepository();
            return repo.All().FirstOrDefault(z => z.Alias == alias);
        }
        public static void VerifyMachineGroup(int rowIndex, MachineGroupExt expectedMachineGroup)
        {
            //verify the table
            var page = MachineGroupPage.GetWindow();
            var tbl = page.MachineGroupTable;
            Specify.That(tbl.GetCellValue(rowIndex, 0)).Should.BeEqualTo(expectedMachineGroup.Alias, "Name is not correct");
            Specify.That(tbl.GetCellValue(rowIndex, 1)).Should.BeEqualTo(expectedMachineGroup.Description, "Description is not correct");
            Specify.That(tbl.GetCellValue(rowIndex, 2)).Should.BeEqualTo(expectedMachineGroup.MaxQueueLength.ToString(), "Queue length is not correct");
            Specify.That(tbl.GetCellValue(rowIndex, 3)).Should.Contain(expectedMachineGroup.WorkflowPath, "Workflow is not correct");

            //verify machine group in database
            MachineGroupRepository repo = new MachineGroupRepository();
            MachineGroup mg = repo.All().First();
            Specify.That(mg.Alias)
                .Should.BeEqualTo(expectedMachineGroup.Alias, "Name is not correct");
            Specify.That(mg.Description)
                .Should.BeEqualTo(expectedMachineGroup.Description, "Description is not correct");
            Specify.That(mg.MaxQueueLength)
                .Should.BeEqualTo(expectedMachineGroup.MaxQueueLength, "Queue length is not correct");
            Specify.That(mg.WorkflowPath)
                .Should.Contain(expectedMachineGroup.WorkflowPath, "Workflow is not correct");
        }
        /// <summary>
        /// Verify if a machine group exist or not in the database
        /// </summary>
        /// <param name="alias">The alias of the  machine group to look for in the database</param>
        /// <param name="shouldExistInDb">True if the  machine group should exist in the database, false if it should not exist.</param>
        public static MachineGroup VerifyMachineGroupExistsInDatabase(string alias, bool shouldExistInDb = true, MachineGroupRepository repo = null)
        {
            var mg = FindByAlias(alias, repo);
            TestUtilities.VerifyObjectExistsInDatabase(mg, "Machine Group", alias, shouldExistInDb);
            return mg;
        }
    }
}
