﻿using System;
using System.Linq;

using Testing.Specificity;

using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Data.CartonPropertyGroups;
using PackNet.Data.Classifications;
using PackNet.Data.Corrugates;
using PackNet.Data.PickArea;
using PackNet.Data.ProductionGroups;
using PackNet.Data.UserManagement;

using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using Microsoft.VisualStudio.TestTools.UITesting;
using PackNet.Common.Interfaces.DTO.Corrugates;

namespace MachineManagerAutomationTests.Macros
{
    public static class ProductionGroupMacros
    {
        public static void Create(ProductionGroupExt pg, AdminConsole adminConsole)
        {
            var page = new ProductionGroupPage();
            page.Create(pg, adminConsole);
        }
        public static void Build(ProductionGroupExt pg, AdminConsole adminConsole = null, TestSteps testSteps = null, bool dropDatabase = false)
        {
            if (dropDatabase)
            {
                TestDatabaseManagement.DropDatabase();
                TestUtilities.RestartPackNetService();
            }

            BuildProductionGroupInner(pg, adminConsole, testSteps, dropDatabase);
        }

        private static void BuildProductionGroupInner(ProductionGroupExt pg, AdminConsole adminConsole = null, TestSteps testSteps = null, bool dropDatabase = true, string mgWorkflow = null)
        {
            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            var existingOp = new UserRepository().All().FirstOrDefault(z => z.UserName == TestDatabaseManagement.QaUser.UserName);
            if (existingOp == null)
            {
                UserMacros.Create(TestDatabaseManagement.QaUser, adminConsole);
            }

            foreach (var c in pg.Corrugates)
            {
                var existingCorr = new CorrugateRepository().All().FirstOrDefault(z => z.Alias == c.Alias);
                if (existingCorr == null)
                {
                    CorrugateMacros.Create(c, adminConsole);
                }
            }

            foreach (var mg in pg.MachineGroups)
            {
                mg.ProductionGroup = pg;
                MachineGroupMacros.BuildMachineGroup(mg, adminConsole);
            }

            foreach (var cpg in pg.CPGs)
            {
                var existingCpg = new CartonPropertyGroupRepository().All().FirstOrDefault(z => z.Alias == cpg.Alias);
                if (existingCpg == null)
                {
                    CPGMacros.Create(cpg, adminConsole);
                }
            }

            foreach (var cl in pg.Classifications)
            {
                var existingClassification = new ClassificationRepository().All().FirstOrDefault(z => z.Alias == cl.Alias);
                if (existingClassification == null)
                {
                    ClassificationMacros.Create(cl, adminConsole);
                }
            }

            foreach (var pz in pg.PickZones)
            {
                var existingPickZone = new PickZoneRepository().All().FirstOrDefault(z => z.Alias == pz.Alias);
                if (existingPickZone == null)
                {
                    PickZoneMacros.Create(pz, adminConsole);
                }
            }

            ProductionGroupMacros.Create(pg, adminConsole);

            ChangeCorrugateMacros.ChangeCorrugatesForAllMachineGroups(pg, testSteps, adminConsole);
        }
        public static ProductionGroup VerifyProductionGroupIsSavedToDatabase(ProductionGroupExt pg, Guid idOfExistingObject)
        {
            ProductionGroup pgInDb = new ProductionGroup();
            var repo = new ProductionGroupRepository();

            if(pg.Id != Guid.Empty)
                pgInDb = repo.Find(idOfExistingObject);
            else
                pgInDb = repo.All().FirstOrDefault(z => z.Alias == pg.Alias);

            if (pg.Id != Guid.Empty)
            {
                Specify.That(pgInDb).Should.Not.BeNull(string.Format("Failed to find the production group with id '{0}'.", idOfExistingObject.ToString()));
                Specify.That(pgInDb.Alias).Should.BeEqualTo(pg.Alias, "Alias is not correct in the database. Actual: {0} Expected: {1}", pgInDb.Alias, pg.Alias);
            }
            else
            {
                Specify.That(pgInDb).Should.Not.BeNull(string.Format("Failed to find the production group with alias '{0}'", pg.Alias));
            }

            Specify.That(pgInDb.Description).Should.BeEqualTo(pg.Description, string.Format("Description is not correct in the database. Actual: {0} Expected: {1}", pgInDb.Description, pg.Description));
            Specify.That(pgInDb.SelectionAlgorithm).Should.BeEqualTo(pg.SelectionAlgorithm, string.Format("Selection algorithm is not correct in the database. Actual: {0} Expected: {1}", pgInDb.SelectionAlgorithm.ToString(), pg.SelectionAlgorithm.ToString()));

            return pgInDb;
        }
        /// <summary>
        /// Verify if a pickzone exist or not in the database
        /// </summary>
        /// <param name="alias">The alias of the pickzone to look for in the database</param>
        /// <param name="shouldExistInDb">True if the pickzone should exist in the database, false if it should not exist.</param>
        public static ProductionGroup VerifyProductionGroupExistsInDatabase(string alias, bool shouldExistInDb, ProductionGroupRepository repo = null)
        {
            var pg = FindByAlias(alias, repo);
            TestUtilities.VerifyObjectExistsInDatabase(pg, "Production Group", alias, shouldExistInDb);
            return pg;
        }
        public static ProductionGroup FindByAlias(string alias, ProductionGroupRepository repo = null)
        {
            if (repo == null) repo = new ProductionGroupRepository();
            return repo.All().FirstOrDefault(z => z.Alias == alias);
        }

        /// <summary>
        /// Verify that the production group has the expected corrugate. This only works if the production group
        /// only has one corrugate
        /// </summary>
        /// <param name="pg"></param>
        /// <param name="expectedCorrugate"></param>
        public static void VerifyOneCorrugateInProductionGroup(ProductionGroup pg, Corrugate expectedCorrugate)
        {
            var allCorrugates = new CorrugateRepository().All();
            var expectedCor = allCorrugates.Single(fromDb => fromDb.Alias == TestDatabaseManagement.Corrugate_w279t011.Alias);
            Specify.That(pg.ConfiguredCorrugates.Contains(expectedCor.Id)).Should.BeTrue();
            Specify.That(pg.ConfiguredMachineGroups.All(c => pg.ConfiguredMachineGroups.Contains(c))).Should.BeTrue();
        }
    }
}
