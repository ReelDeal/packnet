﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Testing.Specificity;

using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Data.PrintMachines;
using PackNet.Data.BarcodeScanner;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.DTO.Machines;

using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests.Repository;
using Microsoft.VisualStudio.TestTools.UITesting;

namespace MachineManagerAutomationTests.Macros
{
    public static class MachineMacros
    {
        public static void Create(List<IMachine> machines, AdminConsole adminConsole = null)
        {
            foreach(var m in machines)
                Create(machines, adminConsole);
        }
        public static void Create(IMachine machine, AdminConsole adminConsole = null)
        {
            MachineManagementPage page = new MachineManagementPage();
            page.Create(machine, adminConsole);
        }
        public static void Build(IMachine m, AdminConsole adminConsole = null, MachineManagementPage page = null)
        {
            if (page == null) page = MachineManagementPage.GetWindow();

            adminConsole = TestUtilities.CreatePageAndLoginAsAdministrator(adminConsole);

            if (m is IPacksizeCutCreaseMachine)
            {
                page.AddRequiredPickZonesForAccessories(((IMachineWIthAccessories)m).Accessories, adminConsole);
                page.Create(m, adminConsole);
                if (((IMachineWIthAccessories)m).Accessories.Count > 0)
                {
                    page.MachinesTable.SelectRowWithValue(m.MachineAlias, 0);
                    page.ClickEditButton();
                    page.CreateAccessoriesForMachine(((IMachineWIthAccessories)m).Accessories);
                    Playback.Wait(1000);
                    page.ClickSaveButton();
                }
            }
            else if (m is ZebraPrinter || m is ScannerExt)
            {
                page.Create(m, adminConsole);
            }
            else
            {
                throw new Exception("Wrong machine type:" + m.GetType().Name);
            }

        }

        public static string GetMachineTypeFriendlyName(MachineType machineType)
        {
            switch (machineType)
            {
                case MachineType.EM:
                    return MachineTypeFriendlyNames.Em;
                case MachineType.Fusion:
                    return MachineTypeFriendlyNames.Fusion;
                case MachineType.Scanner:
                    return MachineTypeFriendlyNames.Scanner;
                case MachineType.ZebraPrinter:
                    return MachineTypeFriendlyNames.ZebraPrinter;
            }

            return null;
        }

        public static bool CheckIfMachinesExistInDatabase(List<IMachine> machines)
        {
            bool allMachinesExist = true;
            if (machines.Count > 0)
            {
                var emList = new EmMachineRepository().All();
                var fmList = new FusionMachineRepository().All();
                var zpList = new ZebraPrinterRepository().All();
                var scList = new BarcodeScannerRepository().All();

                if (emList != null && fmList != null && zpList != null && scList != null)
                {
                    if (emList.Count() == 0 && fmList.Count() == 0 && zpList.Count() == 0 && scList.Count() == 0)
                    {
                        allMachinesExist = false;
                    }
                    else
                    {
                        foreach (var m in machines)
                        {
                            if (m is EmMachine)
                            {
                                var em = emList.FirstOrDefault(z => z.Alias == ((EmMachine)m).Alias);
                                if (em == null)
                                {
                                    allMachinesExist = false;
                                    break;
                                }
                            }
                            if (m is FusionMachine)
                            {
                                var fm = fmList.FirstOrDefault(z => z.Alias == ((FusionMachine)m).Alias);
                                if (fm == null)
                                {
                                    allMachinesExist = false;
                                    break;
                                }
                            }
                            if (m is ZebraPrinter)
                            {
                                var zp = zpList.FirstOrDefault(z => z.Alias == ((ZebraPrinter)m).Alias);
                                if (zp == null)
                                {
                                    allMachinesExist = false;
                                    break;
                                }
                            }
                            if (m is BarcodeScannerMachine)
                            {
                                var sc = scList.FirstOrDefault(z => z.Alias == ((BarcodeScannerMachine)m).Alias);
                                if (sc == null)
                                {
                                    allMachinesExist = false;
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    allMachinesExist = false;
                }
            }
            return allMachinesExist;
        }

        public static IPacksizeCutCreaseMachine FindByAlias(string alias, MachineType machineType)
        {
            IPacksizeCutCreaseMachine m = null;
            if (machineType == MachineType.EM)
            {
                var repo = new EmMachineRepository();
                m = repo.All().FirstOrDefault(z => z.Alias == alias);
            }
            if (machineType == MachineType.Fusion)
            {
                var repo = new FusionMachineRepository();
                m = repo.All().FirstOrDefault(z => z.Alias == alias);
            }
            return m;
        }
        public static string TranslatePhysicalMachineSettingsFilePathToName(string path)
        {
            if (path.IndexOf("xml") > 0)
            {
                path = path.Replace(".xml", "");
                return path.Substring(path.LastIndexOf("\\") + 1);
            }

            return path;
        }
        public static string GetConveyorTypeFriendlyName(ConveyorType type)
        {
            if (type.ToString() == "WasteConveyor")
                return "Waste Conveyor";
            else if (type.ToString() == "CrossConveyor")
                return "Cross Conveyor";
            else
            {
                return string.Empty;
            }
        }
        public static string GetFootPedalPurposeFriendlyName(FootpedalPurpose purpose)
        {
            if (purpose.ToString() == "Trigger")
                return "Trigger";
            else if (purpose.ToString() == "PausePlayMachineGroup")
                return "Pause/Play Machine Group";
            else
            {
                return string.Empty;
            }
        }

        public static void VerifyDataTableIsNotEmpty(MachineType machineType)
        {
            // Verify machine is deleted properly in mongo db
            switch (machineType)
            {
                case MachineType.EM:

                    var repoEm = new EmMachineRepository();
                    var dbRecordEm = repoEm.Find(machineType.ToString());
                    Specify.That(dbRecordEm).Should.BeNull();
                    Specify.That(repoEm.Count()).Should.Not.BeEqualTo(0, "Em machine not deleted from DB");
                    break;
                case MachineType.Fusion:
                    var repoFusion = new FusionMachineRepository();
                    var dbRecordFusion = repoFusion.Find(machineType.ToString());
                    Specify.That(dbRecordFusion).Should.BeNull();
                    Specify.That(repoFusion.Count()).Should.Not.BeEqualTo(0, "Fusion machine not deleted from DB");
                    break;
                case MachineType.ZebraPrinter:
                    var repoZebra = new ZebraPrinterRepository();
                    var dbRecordZebra = repoZebra.Find(machineType.ToString());
                    Specify.That(dbRecordZebra).Should.BeNull();
                    Specify.That(repoZebra.Count()).Should.Not.BeEqualTo(0, "Zebra printer machine not deleted from DB");
                    break;
                case MachineType.Scanner:
                    var repoScanner = new BarcodeScannerRepository();
                    var dbRecordScanner = repoScanner.Find(machineType.ToString());
                    Specify.That(dbRecordScanner).Should.BeNull();
                    Specify.That(repoScanner.Count()).Should.Not.BeEqualTo(0, "Scanner machine not deleted from DB");
                    break;
            }
        }
        public static MachineType GetMachineType(IMachine machine)
        {
            if (machine is EmMachine)
                return MachineType.EM;
            else if (machine is FusionMachine)
                return MachineType.Fusion;
            else if (machine is ZebraPrinter)
                return MachineType.ZebraPrinter;
            else if (machine is BarcodeScannerMachine)
                return MachineType.Scanner;
            else
                return MachineType.None;
        }
        public static bool IsMachine(MachineType machine)
        {
            return (machine == MachineType.Fusion || machine == MachineType.EM);
        }
        public static bool IsPrinter(MachineType machine)
        {
            return (machine == MachineType.ZebraPrinter);
        }


        public static void VerifyMachineIsSavedToDatabase(MachineType type, string alias)
        {
            switch (type)
            {
                case MachineType.EM:

                    var repoEm = new EmMachineRepository();
                    var dbRecordEm = repoEm.Find(alias);
                    Specify.That(dbRecordEm.Alias)
                    .Should.BeEqualTo(alias, "Em Machine's Name is not correct in DB");
                    break;
                case MachineType.Fusion:
                    var repoFusion = new FusionMachineRepository();
                    var dbRecordFusion = repoFusion.Find(alias);
                    Specify.That(dbRecordFusion.Alias)
                    .Should.BeEqualTo(alias, "Fusion Machine's Name is not correct in DB");
                    break;
                case MachineType.ZebraPrinter:
                    var repoZebra = new ZebraPrinterRepository();
                    var dbRecordZebra = repoZebra.Find(alias);
                    Specify.That(dbRecordZebra.Alias)
                  .Should.BeEqualTo(alias, "Zebra Printer's Name is not correct in DB");
                    break;
                case MachineType.Scanner:
                    var repoScanner = new BarcodeScannerRepository();
                    var dbRecordScanner = repoScanner.Find(alias);
                    Specify.That(dbRecordScanner.Alias)
                    .Should.BeEqualTo(alias, "Barcode Scanner's Name is not correct in DB");
                    break;
            }
        }

    }
}
