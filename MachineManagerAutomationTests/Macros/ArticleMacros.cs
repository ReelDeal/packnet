﻿using System;
using System.IO;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

using MongoDB.Bson.Serialization;

using PackNet.Common.Utils;
using PackNet.Common.Interfaces.Enums;
using PackNet.Plugin.ArticleService.Data;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PrintingMachines;

using MachineManagerAutomationTests.Macros;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.WebUITests.Repository;

namespace MachineManagerAutomationTests.Macros
{
    public static class ArticleMacros
    {

        //used to compare the expected carton with the one retrieved from the database
        public static void VerifyCartonInDatabase(Carton cartonFromDatabase, Carton expectedCarton)
        {
            Specify.That(cartonFromDatabase.CustomerDescription == expectedCarton.CustomerDescription);
            Specify.That(cartonFromDatabase.CustomerUniqueId == expectedCarton.CustomerUniqueId);
            Specify.That(cartonFromDatabase.DesignId == expectedCarton.DesignId);
            Specify.That(cartonFromDatabase.Width.Equals(expectedCarton.Width));
            Specify.That(cartonFromDatabase.Height.Equals(expectedCarton.Height));
            Specify.That(cartonFromDatabase.Length.Equals(expectedCarton.Length));
            Specify.That(cartonFromDatabase.CorrugateQuality.Equals(expectedCarton.CorrugateQuality));
        }

        public static void VerifyLabelInDatabase(Label labelFromDatabase, LabelExt label)
        {
            Specify.That(labelFromDatabase.CustomerDescription == label.CustomerDescription);
            Specify.That(labelFromDatabase.CustomerUniqueId == label.Alias);
        }

        public static Kit VerifyArticleInDatabase(string alias, string description)
        {
            Playback.Wait(1000);

            var repo = new ArticleRepository();
            Specify.That(repo.All().Count() > 0).Should.BeTrue("VerifyArticleInDatabase: No articles found in the database");

            var article = (Kit)repo.All().FirstOrDefault().Producible;
            Specify.That(article.CustomerUniqueId == alias);
            Specify.That(article.CustomerDescription == description);

            return article;
        }

        public static void BuildArticles(List<ArticleExt> articles, AdminConsole adminConsole = null)
        {
            foreach (var a in articles)
                BuildArticle(a, adminConsole);
        }

        public static void BuildArticle(ArticleExt art, AdminConsole adminConsole)
        {
            var page = ArticlesManagementPage.GetWindow();

            adminConsole.NavigateToPage(NavPages.Article);

            page.ClickNewArticleButton();

            page.EnterArticleInfo(art);

            foreach (var p in art.SubArticles)
            {
                if (p is ICarton)
                {
                    page.CreateCarton((Carton)p, 1);
                }
                else if (p is Label)
                {
                    page.CreateLabel((LabelExt)p, 1);
                }
            }

            page.ClickSaveArticleButton();
        }

        public static void VerifyDbTableIsEmpty(ArticleRepository repo = null)
        {
            if(repo == null) repo = new ArticleRepository();
            var cnt = repo.All().Count();
            Specify.That(cnt).Should.BeEqualTo(0, "Article collection still contains data.");
        }

        public static ArticleExt AddSubArticlesToArticle(ArticleExt article, List<IProducible> subarticles, bool clearFirst)
        {
            if (clearFirst)
                article.SubArticles.Clear();

            article.SubArticles.AddRange(subarticles);

            return article;
        }
        public static void VerifyDatabaseHasExpectedNumberOfArticles(int expectedNumberOfArticles)
        {
            int cnt = new ArticleRepository().All().Count();
            Specify.That(cnt).Should.BeEqualTo(expectedNumberOfArticles, string.Format("Database have incorrect number of articles. Actual: {0} Expected: {1}", cnt, expectedNumberOfArticles));
        }
    }
}
