﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Testing.Specificity;

namespace MachineManagerAutomationTests.Helpers
{
    [TestClass]
    public class StagingTests
    {
        
        public static void WixPreInstallerCopyFromBuild()
        {
            var latestBuildDir = TestUtilities.GetLastWrittenDirectory(TestSettings.Default.PreReqInstallerLocation);
            
            // Check if local dir exists, if not create it
            if (!Directory.Exists(TestSettings.Default.LocalTempDir))
            {
                Directory.CreateDirectory(TestSettings.Default.LocalTempDir);
            }

            //Copy the pre-req install to the local drive
            File.Copy(Path.Combine(latestBuildDir.FullName, TestSettings.Default.WixPreInstallerPath), Path.Combine(TestSettings.Default.LocalTempDir, TestSettings.Default.WixPreInstallerPath), true);

            Specify.That(latestBuildDir.FullName.Contains(TestSettings.Default.WixPreInstallerPath));
        }
    }
}
