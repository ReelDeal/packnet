﻿using System;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.IO;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace MachineManagerAutomationTests.Helpers
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    /// 
    /// THIS IS FOR CONTINIOUS INTEGRATION AND DEEP FREEZE MACHINES - DO NOT CHANGE THIS CLASS OR METHODS
    [CodedUITest]
    public class RebootTest
    {
        private const string RestartBatchFile = @"C:\Restart.bat";

        /// <summary>
        /// This is used by CI to restart the node and trigger a clean by deepfreeze
        /// </summary>
        [TestMethod]
        [TestCategory("UIAutomation")]
        public void RebootNodeAfterTestingComplete()
        {
            //first back-up log file
            var source = Path.Combine(TestSettings.Default.InstallLocation, "Logs\\PackNet.Server.log");
            var destination = "\\\\rd1\\r&d\\QALOGS\\" + DateTime.Now.ToString("yyyy-M-ddTHH_mm_ss") + ".log";
            try
            {
                File.Copy(source, destination);
            }
            finally { }

            if (File.Exists(RestartBatchFile))
            {
                // This will run restart batch for Automation Test (Not in DEV)
                Process.Start(RestartBatchFile);
            }
        }

    }
}
