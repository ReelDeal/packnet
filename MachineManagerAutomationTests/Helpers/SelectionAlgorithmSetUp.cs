﻿using System;
using System.IO;

using PackNet.Common.Interfaces.Enums;

namespace MachineManagerAutomationTests.Helpers
{
    public class SelectionAlgorithmSetUp
    {
        public static void ChangeImportWorkflow(SelectionAlgorithmTypes algorithm, string wfName)
        {
            if (algorithm == SelectionAlgorithmTypes.BoxFirst)
            {
                File.Copy(Path.Combine(Environment.CurrentDirectory, "TestingWorkflows", wfName), Path.Combine(Environment.GetEnvironmentVariable("PackNetWorkflows"), "ImportPlugins\\BoxFirst\\BoxFirstCartonImport.xaml"), true);
                return;
            }

            if (algorithm == SelectionAlgorithmTypes.BoxLast)
            {
                File.Copy(Path.Combine(Environment.CurrentDirectory, "TestingWorkflows", wfName), Path.Combine(Environment.GetEnvironmentVariable("PackNetWorkflows"), "ImportPlugins\\BoxLast\\BoxLastCartonImport.xaml"), true);
                return;
            }

            if (algorithm == SelectionAlgorithmTypes.Order)
            {
                File.Copy(Path.Combine(Environment.CurrentDirectory, "TestingWorkflows", wfName), Path.Combine(Environment.GetEnvironmentVariable("PackNetWorkflows"), "ImportPlugins\\Order\\OrderImport.xaml"), true);
                return;
            }
        }
    }
}
