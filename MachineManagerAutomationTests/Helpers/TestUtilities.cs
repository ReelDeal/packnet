﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.ServiceProcess;

using CUITe.Controls.HtmlControls;

using MachineManagerAutomationTests.WebUITests;
using MachineManagerAutomationTests.WebUITests.Repository;

using Microsoft.VisualStudio.TestTools.UITesting;

using PackNet.Common.Utils;
using PackNet.Data.ArticleManagement;
using PackNet.Data.UserManagement;

using Testing.Specificity;
using CUITe.Controls;
using PackNet.Common.Interfaces.Enums;
using PackNet.FusionSim.ViewModels;
using System.Collections.Generic;
using System.Collections;

namespace MachineManagerAutomationTests.Helpers
{
    public enum LogTypes
    {
        Error,
        Regular
    }

    public static class TestUtilities
    {

        public static DirectoryInfo GetLastWrittenDirectory(string path)
        {
            path = path.Replace("&amp;", "&");
            return new DirectoryInfo(path).GetDirectories()
                       .OrderByDescending(d => d.LastWriteTimeUtc).First();
        }

        /// <summary>
        /// Machine Manager Window Location
        /// </summary>
        public static string MachineManagerWindowLocation
        {
            get { return Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe); }
        }

        public static string GetTimestamp()
        {
            return
                DateTime.Now.ToString(System.Globalization.CultureInfo.InvariantCulture)
                    .Replace("-", "")
                    .Replace(" ", "")
                    .Replace("/", "")
                    .Replace(":", "") + DateTime.Now.Millisecond.ToString();
        }

        public static void VerifyPrinterHasLabelTemplate(ZebraPrinterSimulatorViewModel printer, string printerName, bool shouldGetLabel = true)
        {
            Thread.Sleep(1000);
            if (string.IsNullOrEmpty(printerName))
                printerName = "Printer";

            if(shouldGetLabel)
                Specify.That(string.IsNullOrEmpty(printer.ZebraPrinter.Template)).Should.BeFalse(printerName + " should get label");
            else
                Specify.That(string.IsNullOrEmpty(printer.ZebraPrinter.Template)).Should.BeTrue(printerName + " should not get label");
        }
        public static void DropDbAndRestart()
        {
            TestDatabaseManagement.DropDatabase();
            RestartPackNetService();
            
        }

        public static void StartServiceIfNotStarted(ServiceController packNetService = null)
        {
            // Set the variable to the process name.
            if(packNetService == null)
                packNetService = new ServiceController { ServiceName = TestSettings.Default.MachineManagerProcessName };

            // Check to see if the PackNet service is running or not.  If is not, then start it.
            if (!packNetService.Status.Equals(ServiceControllerStatus.Running))
            {
                try
                {
                    packNetService.Start();
                    packNetService.WaitForStatus(ServiceControllerStatus.Running);
                    return;
                }
                catch (InvalidOperationException)
                {
                    Console.WriteLine("Could not start the PackNet Service!");
                }
            }
        }

        /// <summary>
        /// Restart PackNet.Server
        /// </summary>
        public static void RestartPackNetService()
        {
            // Set the variable to the process name.
            var packNetService = new ServiceController { ServiceName = TestSettings.Default.MachineManagerProcessName };

            StartServiceIfNotStarted(packNetService);

            try
            {
                // If the service is running, then restart it.
                packNetService.Stop();
                packNetService.WaitForStatus(ServiceControllerStatus.Stopped);
                packNetService.Start();
                packNetService.WaitForStatus(ServiceControllerStatus.Running);
            }
            catch (InvalidOperationException)
            {
                Console.WriteLine("Could not start the PackNet Service!");
            }
        }

        public static void RestartPackNetServerExe(int DelayBeforeStartingAgain = 10)
        {

            //Playback.PlaybackSettings.WaitForReadyLevel = WaitForReadyLevel.Disabled;
            var runningProcesses = Process.GetProcessesByName(TestSettings.Default.MachineManagerProcessName);

            if (runningProcesses.Any())
            {
                foreach (var runningProcess in runningProcesses)
                {
                    runningProcess.Kill();
                }
                //wait for it to die
                Retry.For(
                    () => Process.GetProcessesByName(TestSettings.Default.MachineManagerProcessName).Any(),
                    TimeSpan.FromSeconds(10));

                Thread.Sleep(TimeSpan.FromSeconds(DelayBeforeStartingAgain));
            }

            ApplicationUnderTest.Launch(Path.Combine(TestSettings.Default.InstallLocation, TestSettings.Default.DefaultWixInstallMMExe), string.Empty, "--console");

            //we need to wait for some apps to start (.server)
            Thread.Sleep(TimeSpan.FromSeconds(15));
        }

        /// <summary>
        /// Opens a new page and logs in. If console is null, then new page is created and log in is performed. If not null, the existing object is returned.
        /// </summary>
        /// <param name="console">Existing or not created console</param>
        /// <returns>If console is null, then new page is created and log in is performed. If not null, the existing object is returned.</returns>
        public static AdminConsole CreatePageAndLoginAsAdministrator(AdminConsole console = null)
        {
            if (console != null)
                return console;
            else
                return CreatePageAndLogin<AdminConsole>(@"index.html#/AdminConsole", TestSettings.Default.MachineManagerAdminUserName, TestSettings.Default.MachineManagerAdminPassword, null);
        }

        public static T CreatePageAndLoginAsAdministrator<T>(string pageUrl = @"index.html#/AdminConsole", string language = null) where T : DynamicBrowserWindowWithLogin, new()
        {
            return CreatePageAndLogin<T>(pageUrl, TestSettings.Default.MachineManagerAdminUserName, TestSettings.Default.MachineManagerAdminPassword, language);
        }

        public static T CreatePageAndLoginAsAdministratorWithLoginData<T>(string username, string password) where T : DynamicBrowserWindowWithLogin, new()
        {
            return CreatePageAndLogin<T>(@"index.html#/AdminConsole", username, password, null);
        }

        public static T CreatePageAndLoginAsOperator<T>(string pageUrl = @"index.html#/SelectMachineGroup", string language = null) where T : DynamicBrowserWindowWithLogin, new()
        {
            return CreatePageAndLogin<T>(pageUrl, TestSettings.Default.MachineManagerOperatorUserName, TestSettings.Default.MachineManagerOperatorPassword, language);
        }

        public static T CreatePageAndLogin<T>(string pageUrl, string username, string password, string language = null) where T : DynamicBrowserWindowWithLogin, new()
        {
            var page = CreatePage<T>(pageUrl);
            Retry.For(() => page.Login(username, password, language),
                TimeSpan.FromSeconds(30));

            return page;
        }

        public static T CreatePage<T>(string pageUrl) where T : CUITe_DynamicBrowserWindow, new()
        {
            var mw =
                CUITe_BrowserWindow.Launch<T>(String.Format("{0}/{1}",
                GetMachineManagerURL(), pageUrl));
            mw.Maximized = true;

            return mw;
        }

        public static void SetUpDownValue(CUITe_HtmlEdit textField, CUITe_HtmlButton upButton, CUITe_HtmlButton downButton, int desiredValue)
        {
            int currentValue = desiredValue;
            int.TryParse(textField.GetText().Trim(), out currentValue);

            while (currentValue < desiredValue)
            {
                upButton.Click();
                int.TryParse(textField.GetText().Trim(), out currentValue);
            }

            while (int.Parse(textField.GetText().Trim()) > desiredValue)
            {
                downButton.Click();
                int.TryParse(textField.GetText().Trim(), out currentValue);
            }
        }

        // june 11:  this method is not relevant any more so remove the calls and this method please
        public static string GetMachineManagerURL()
        {
            return String.Format("http://{0}/{1}", Environment.MachineName, TestSettings.Default.PackNetURL);
        }

        public static void VerifyContentInLogFile(string str, LogTypes log)
        {
            Specify.That(File.ReadAllLines(GetLogPath(log)).Any(e => e.Contains(str))).Should.BeTrue();
        }

        public static string GetMachineTypeFriendlyName(MachineType machineType)
        {
            switch (machineType)
            {
                case MachineType.EM:
                    return MachineTypeFriendlyNames.Em;
                case MachineType.Fusion:
                    return MachineTypeFriendlyNames.Fusion;
                case MachineType.Scanner:
                    return MachineTypeFriendlyNames.Scanner;
                case MachineType.ZebraPrinter:
                    return MachineTypeFriendlyNames.ZebraPrinter;
            }

            return null;
        }

        public static void VerifyPackNetServerProcessesAreOnlyOne()
        {
            Specify.That(Process.GetProcessesByName("PackNet.Server").Count()).Should.BeEqualTo(1);
        }

        public static string GetLogPath(LogTypes log)
        {
            var logfileName = log == LogTypes.Error ? "PackNet.Server.Errors.log" : "PackNet.Server.log";
            return Path.Combine(TestSettings.Default.InstallLocation, @"Logs\" + logfileName);
        }
       
        public static string[] GetLogPaths(LogTypes logType)
        {
            string[] files = Directory.GetFiles(Path.Combine(TestSettings.Default.InstallLocation, @"Logs\"));
            if(logType == LogTypes.Error) 
                return files.Where(z => z.Contains("Errors")).ToArray();
            else
                return files.Where(z => !z.Contains("Errors")).ToArray();
        }
        public static string[] FindInLogFile(LogTypes logType, string matchStrA, string matchStrB = null, bool currentLogFileOnly = false)
        {
            var occurances = new List<string>();
            var location = string.Empty;
            string[] lines = null;
            string[] files = { GetLogPath(logType) };
            if (!currentLogFileOnly) 
                files = GetLogPaths(logType);

            foreach(string f in files)
            {
                lines = File.ReadAllLines(f);
                int lineIdx = 0;
                foreach(string l in lines)
                {
                    lineIdx++;
                    bool aHit = true;
                    bool bHit = true;
                    if(!string.IsNullOrEmpty(matchStrA))
                        aHit = l.ToLower().Contains(matchStrA.ToLower());                    
                    if(!string.IsNullOrEmpty(matchStrB))
                        bHit = l.ToLower().Contains(matchStrB.ToLower());                    

                    if (aHit && bHit)
                        occurances.Add(f.Substring(f.LastIndexOf('\\') + 1) + "|" + lineIdx + "|" + l);
                }
            }
            return occurances.ToArray();
        }
        public static void DeleteLogs(LogTypes logType)
        {
            string[] files = Directory.GetFiles(Path.Combine(TestSettings.Default.InstallLocation, @"Logs\"));
            foreach(string f in files)
            {
                if(f.Contains("Errors") && logType == LogTypes.Error)
                    File.Delete(f);
                else if (!f.Contains("Errors") && logType == LogTypes.Regular)
                    File.Delete(f);
            }
        }
        public static void VerifyAlert(AlertWidget alert, string alias, string validateMsg, string errorMsg, bool hasError)
        {
            Retry.For(() => alert.Exists, TimeSpan.FromSeconds(10));
            Specify.That(alert.Exists).Should.BeTrue("The alert does not exist.");
            Specify.That(alert.HasError).Should.BeEqualTo(hasError);
            string alertText = alert.AlertText;
            Specify.That(alertText).Should.BeEqualTo(validateMsg, errorMsg);
        }

        public static bool CheckIfValueExistsInComboBox(CUITe_HtmlComboBox cb, string value)
        {
            var arr = cb.InnerText.Split('\n');
            for (int idx = 0; idx < arr.Length; idx++)
            {
                if (arr[idx].Contains("\r"))
                    arr[idx] = arr[idx].Substring(0, arr[idx].Length - 1);

                if (arr[idx] == value)
                    return true;
            }
            return false;
        }

        public static void DeleteLogFile(LogTypes logType)
        {
            if (logType == LogTypes.Regular)
                File.Delete(Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.log"));
            else if (logType == LogTypes.Error)
                File.Delete(Path.Combine(TestSettings.Default.InstallLocation, @"Logs\PackNet.Server.Errors.log"));
        }

        public static void VerifyObjectExistsInDatabase(object obj, string objectName, string alias, bool shouldExistInDb)
        {
            if (shouldExistInDb)
            {
                Specify.That(obj).Should.Not.BeNull(string.Format("{0} with alias '{1}' should exist in the database.", objectName, alias));
            }
            else
            {
                Specify.That(obj).Should.BeNull(string.Format("{0} with alias '{1}' should NOT exist in the database.", objectName, alias));
            }
        }

        public static void VerifyHtmlElementExists(CUITe_DynamicBrowserWindow win, string id, string errorText = null, bool shouldExist = true )
        {
            if (shouldExist)
            {
                if (string.IsNullOrEmpty(errorText))
                    errorText = string.Format("The element with id {0} does not exist.", id);
                Specify.That(GetUITestControl(win, id).Exists).Should.BeTrue(errorText);
            }
            else
            {
                if (string.IsNullOrEmpty(errorText))
                    errorText = string.Format("The element with id {0} exists but shouldn't.", id);
                Specify.That(GetUITestControl(win, id).Exists).Should.BeFalse(errorText);
            }
        }

        public static void VerifyHtmlElementIsDisplayed(CUITe_DynamicBrowserWindow win, string id, string errorText = null, bool shouldBeDisplayed = true)
        {
            if (shouldBeDisplayed)
            {
                if (string.IsNullOrEmpty(errorText))
                    errorText = string.Format("The element with id {0} is not displayed.", id);
                Specify.That(GetUITestControl(win, id).Enabled).Should.BeTrue(errorText);
            }
            else
            {
                if (string.IsNullOrEmpty(errorText))
                    errorText = string.Format("The element with id {0} is displayed.", id);
                Specify.That(GetUITestControl(win, id).Enabled).Should.BeFalse(errorText);
            }
        }

        public static UITestControl GetUITestControl(UITestControl parentObj, string selectorIdentifier, string selectorType = "Id", int tagInstance = -1)
        {
            try
            {
                UITestControl c = new UITestControl(parentObj);
                c.TechnologyName = "Web";
                if(tagInstance > -1)
                    c.SearchProperties.Add("TagInstance", tagInstance.ToString());
                c.SearchProperties.Add(new PropertyExpression(selectorType, selectorIdentifier));
                return c;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
