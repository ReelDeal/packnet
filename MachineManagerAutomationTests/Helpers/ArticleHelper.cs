﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using Testing.Specificity;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO;
using PackNet.Data.ArticleManagement;

using MachineManagerAutomationTests.WebUITests.Repository;

namespace MachineManagerAutomationTests.Helpers
{
    using PackNet.Common.Utils;

    public static class ArticleHelper
    {
        private static int added;
        private static int total;
        private static int failed;

        public static void Import100Articles()
        {
            var filename = Path.Combine(Environment.CurrentDirectory, "AdditionalFiles", "100Articles.csv");
            ImportArticles(filename);
        }

        public static void SearchForValueAndVerifyItExists(Table articlesTable, string searchString, bool desiredResult)
        {

            articlesTable.SearchWithSearchField(searchString, 0);

            var foundStringInRow = false;
            if (articlesTable.RowCount() > 0)
            {
                var row = articlesTable.FindRow(0);
                if (row.GetChildren().OfType<CUITe.Controls.HtmlControls.CUITe_HtmlDiv>().Any(child => child.InnerText.Trim().ToLower().Contains(searchString.ToLower())))
                {
                    foundStringInRow = true;
                }
            }

            if (desiredResult)
            {
                Specify.That(foundStringInRow)
                    .Should.BeTrue(String.Format("filtering for {0} was not found in the first row", searchString));
            }
            else
            {
                Specify.That(foundStringInRow)
                    .Should.BeFalse(String.Format("filtering for {0} was unexpectedly found in the first row",
                        searchString));
            }
        }

        public static Article GetArticle(Table articlesTable, int rowNumber)
        {
            var row = articlesTable.FindRow(rowNumber);

            var originalArticle = new Article
            {

                ArticleId = articlesTable.FindCell(row, 1).InnerText.Trim(),
                Description = articlesTable.FindCell(row, 2).InnerText.Trim(),
                DesignName = articlesTable.FindCell(row, 6).InnerText.Trim(),
                Length = Double.Parse(articlesTable.FindCell(row, 3).InnerText.Trim()),
                Width = Double.Parse(articlesTable.FindCell(row, 4).InnerText.Trim()),
                Height = Double.Parse(articlesTable.FindCell(row, 5).InnerText.Trim())
            };

            int corrugateQuality;
            originalArticle.CorrugateQuality =
                Int32.TryParse(articlesTable.FindCell(row, 7).InnerText.Trim(), out corrugateQuality)
                    ? corrugateQuality
                    : (int?)null;

            return originalArticle;
        }


        private static void ImportArticles(string importFile)
        {
            var path = "";
            added = 0;
            total = 0;

            if ((path = GetAbsolutePath(importFile)) == String.Empty)
            {
                Debug.WriteLine("Could not find absolute path to '{0}'", importFile);
                return;
            }
            var articleRepo = new ArticleRepository();

            Console.WriteLine("Importing from '{0}'", path);


            using (var file = new StreamReader(path))
            {
                var article = file.ReadLine();
                var headers = article;
                do
                {
                    article = file.ReadLine();
                    if (article == null)
                        continue;

                    try
                    {
                        var parsedArticle = ParseImportDataToArticle(article.Split(';'), headers);
                        if (parsedArticle != null)
                        {
                            articleRepo.Create(parsedArticle);
                            added++;
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e.Message);
                        failed++;
                    }
                    total++;

                } while (!String.IsNullOrWhiteSpace(article) && !file.EndOfStream);

                Debug.WriteLine(string.Format(
                    "\r\n\tImported: {0}\r\n\tFailed: {1}",
                    added,
                    failed));

                file.Close();
            }


        }

        private static Article ParseImportDataToArticle(string[] importData, string headers)
        {
            var realHeaders = headers.Split(';');

            if (realHeaders.Count() == importData.Count() || realHeaders.Count() == (importData.Count() + 1))
            {
                var dict = new Dictionary<string, object>();
                for (var i = 1; i < realHeaders.Count(); i++)
                {
                    dict.Add(realHeaders[i], importData.Count() > i ? importData[i] : "");
                }
                var quality = 1;
                int.TryParse(dict["CorrugateQuality"].ToString(), out quality);

                return new Article
                {
                    ArticleId = dict["ArticleId"].ToString(),
                    Description = dict["Description"].ToString(),
                    Length = Double.Parse(dict["Length"].ToString()),
                    Width = Double.Parse(dict["Width"].ToString()),
                    Height = Double.Parse(dict["Height"].ToString()),
                    DesignId = int.Parse(dict["DesignId"].ToString()),
                    CorrugateQuality = quality
                };
            }
            throw new InvalidDataException(
                string.Format(
                    "The number of headers found {0} does not equal the number of data fields {1}.",
                    realHeaders.Count(),
                    importData.Count()));
        }

        private static string GetAbsolutePath(string path)
        {
            try
            {
                var p = Path.GetFullPath(path);
                return p;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }


        public static string ExportArticles(string fileName)
        {
            var exportFile = new FileInfo(Path.Combine(Environment.CurrentDirectory, fileName));

            try
            {
                var writer = new StreamWriter(exportFile.FullName);
                Debug.WriteLine(string.Format("Exporting to '{0}'", exportFile.FullName));

                var repo = new ArticleRepository();
                var articles = repo.All();

                //write the header

                string[] keys = articles.FirstOrDefault().AsDictionary().Keys.ToArray();
                for (int i = 0; i < keys.Length; i++)
                {
                    writer.Write(keys[i]);
                    if (i < (keys.Length - 1))
                        writer.Write(";");
                    else
                        writer.WriteLine();

                }

                foreach (var article in articles)
                {
                    string stringArticle = "";
                    var values = article.AsDictionary().Values.ToArray();
                    for (int i = 0; i < values.Length; i++)
                    {
                        stringArticle += values[i];
                        if (i < (values.Length - 1))
                            stringArticle += ";";


                    }

                    writer.WriteLine(stringArticle);
                }

                writer.Close();

            }
            catch (Exception e)
            {
                Debug.WriteLine("Could not write to file at '{0}'", exportFile);
                Debug.WriteLine(e.Message);
                Debug.WriteLine("If you get a \"permission denied\" error, please restart this tool as an administrator.");
            }

            return exportFile.FullName;
        }


    }
}


