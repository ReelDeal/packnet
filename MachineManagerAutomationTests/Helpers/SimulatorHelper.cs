﻿using MachineManagerAutomationTests.WebUITests.Repository;
using Microsoft.VisualStudio.TestTools.UITesting;
using PackNet.Common.Utils;
using PackNet.FusionSim.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Testing.Specificity;
using MachineManagerAutomationTests.Helpers;
using MachineManagerAutomationTests.WebUITests;
using System.Threading;
using PackNet.Common.Interfaces.Enums;

namespace MachineManagerAutomationTests.Helpers
{
    public class SimulatorHelper
    {
        public static void SetMachineGroupOnline(FusionSimulatorViewModel fusion, List<ZebraPrinterSimulatorViewModel> printers, Guid mgGuid, AdminConsole console)
        {
            Retry.For(() => fusion.IsSimulationStarted, TimeSpan.FromSeconds(60));
            Retry.For(() => printers.TrueForAll(s => s.IsSimulationStarted), TimeSpan.FromSeconds(60));
            console.NavigateToPage(NavPages.MachineProduction);
            console.SelectMachineGroup(mgGuid);
            var machineProdPage = MachineProductionPage.GetWindow();
            machineProdPage.ClickAutoAndPlayButtons();
        }

        public static void VerifySimulatorHasItemsInQueue(FusionSimulatorViewModel sim, bool shouldHaveItems = true)
        {
            if (shouldHaveItems)
                Retry.For(() => Specify.That(sim.Simulator.Queue.Count > 0).Should.BeTrue("Did not find any cartons in the sim queue"),
                    TimeSpan.FromSeconds(10));
            else
                Retry.For(() => Specify.That(sim.Simulator.Queue.Count == 0).Should.BeTrue("job should not be sent to the sim queue"),
                    TimeSpan.FromSeconds(10));
        }

        public static void CompleteTheJobSentToMachine(FusionSimulatorViewModel fusionSim, ZebraPrinterSimulatorViewModel printerSim, bool verifyGridContent = false)
        {
            ProduceCartonInQueue(fusionSim, verifyGridContent);
            ProduceLabelInQueue(printerSim);
        }

        public static void ProduceLabelInQueue(ZebraPrinterSimulatorViewModel printer, string printerName = "Printer")
        {
            TestUtilities.VerifyPrinterHasLabelTemplate(printer, printerName, true);
            printer.StartCompletePrinting();
            Thread.Sleep(1000);
            Specify.That(printer.ZebraPrinter.HasLabelsInQueue).Should.BeTrue("No label in queue to be removed");
            printer.TakeLabel();
            //printer.ClearQueue(); // why should we clear the queue?? See method StartCompleteVerifyCarton
            Thread.Sleep(1000);
        }

        public static void ProduceCartonInQueue(FusionSimulatorViewModel fusion, bool verifyGridContent = false)
        {
            SimulatorHelper.VerifySimulatorHasCartonsInQueue(fusion);

            var page = MachineProductionPage.GetWindow(null, ProductionMode.Auto, UserType.Administrator);
            if (verifyGridContent)
            {
                Thread.Sleep(1000);
                page.ItemsInProductionTable.VerifyContainsValue(JobStatus.SentToMachine);
            }

            fusion.StartStopPackaging();
            Thread.Sleep(1000);

            if(verifyGridContent)
                page.ItemsInProductionTable.VerifyContainsValue(JobStatus.Started);

            fusion.StartStopPackaging();
        }

        public static List<ZebraPrinterSimulatorViewModel> CreatePrinterSimList(ZebraPrinterSimulatorViewModel printer1, ZebraPrinterSimulatorViewModel printer2 = null)
        {
            var printers = new List<ZebraPrinterSimulatorViewModel>();
            if (printer1 != null) printers.Add(printer1);
            if (printer2 != null) printers.Add(printer2);
            return printers;
        }

        public static void VerifySimulatorHasCartonsInQueue(FusionSimulatorViewModel fusion)
        {
            Retry.For(() => Specify.That(fusion.Simulator.Queue.Count > 0).Should.BeTrue("Did not find any cartons in the sim queue"),
                TimeSpan.FromSeconds(10));
        }
    }
}
