﻿//using Newtonsoft.Json;

//using PackNet.Common.Communication.RabbitMQ;
//using PackNet.Common.Interfaces.DTO.Messaging;
//using PackNet.Common.Interfaces.Logging;

//namespace MachineManagerAutomationTests.Helpers
//{
//    using Moq;

//    using PackNet.Common.Interfaces.DTO.Scanning;
//    using PackNet.Common.Interfaces.DTO.Users;
//    using PackNet.Common.Interfaces.Enums;

//    public static class RabbitHelper
//    {
//        public static void PublishBarcodeData(string barcodeData, string productionGroup)
//        {
//            using (var publisher = new RabbitPublisher("localhost", "mm", new Mock<ILogger>().Object))
//            {
//                var trigger = new CreateCartonTrigger { BarcodeData = barcodeData, ProductionGroupAlias = productionGroup };
//                var msg = new Message<CreateCartonTrigger> { Data = trigger, ReplyTo = "SuperQueue" };
//                publisher.PublishData(msg, new JsonSerializerSettings());
//            }
//        }

//        public static void AddNewUser(string userName, string password, bool isAdmin)
//        {
//            using (var publisher = new RabbitPublisher("localhost", "mm", new Mock<ILogger>().Object))
//            {
//                var user = new User { UserName = userName, Password = password, IsAdmin = isAdmin, };
//                var msg = new Message<User> { Data = user, MessageType = UserMessages.CreateUser, ReplyTo = "SuperQueue" };

//                publisher.PublishData(msg, new JsonSerializerSettings());
//            }
//        }
//    }
//}
