﻿using Moq;
using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using System.Collections.Generic;
using System.Linq;

namespace MachineManagerAutomationTests.Helpers
{
    public static class DesignHelper
    {
        public static string[] GetAllowedRotationsAsStringArray(string name, string propertyName = "Value")
        {
            var rotations = GetAllowedRotationsOfDesign(name);
            return ParseRotationListToStringArray(rotations, propertyName);
        }

        public static string[] GetAllowedRotationsAsStringArray(int designId, string propertyName = "Value")
        {
            var rotations = GetAllowedRotationsOfDesign(designId);
            return ParseRotationListToStringArray(rotations, propertyName);
        }

        private static string[] ParseRotationListToStringArray(List<Rotation> rotations, string propertyName = "Value")
        {
            string[] strArray = new string[rotations.Count];
            for (var i = 0; i < rotations.Count; i++)
            {
                if (propertyName == "Text")
                    strArray[i] = rotations[i].Text;
                else
                    strArray[i] = rotations[i].Value;
            }
            return strArray;

        }

        public static bool DesignExists(string name)
        {
            return LoadPackagingDesignFromDisk(name) != null;
        }

        public static bool DesignExists(int id)
        {
            return LoadPackagingDesignFromDisk(id) != null;
        }
        
        public static int GetDesignIdFromName(string name)
        {
            var design = LoadPackagingDesignFromDisk(name);
            if (design != null)
                return design.Id;
            else
                return -1;
        }

        public static string GetDesignNameFromId(int id)
        {
            return LoadPackagingDesignFromDisk(id).Name;
        }

        public static List<Rotation> GetAllowedRotationsOfDesign(string name)
        {
            return ParseAllowedRotationsInPackagingDesign((PackagingDesign)LoadPackagingDesignFromDisk(name));
        }

        public static List<Rotation> GetAllowedRotationsOfDesign(int designId)
        {
            return ParseAllowedRotationsInPackagingDesign((PackagingDesign)LoadPackagingDesignFromDisk(designId));
        }

        private static List<Rotation> ParseAllowedRotationsInPackagingDesign(PackagingDesign design)
        {
            var lst = new List<Rotation>();
            foreach (var prop in design.AllowedRotations.GetType().GetProperties())
            {
                if (prop.Name.Contains("Degree"))
                {
                    var el = (ConditionalBooleanDesignElement)prop.GetValue(design.AllowedRotations);
                    if (el.Allowed)
                    {
                        lst.Add(new Rotation(prop.Name));
                    }
                }
            }
            return lst;

        }
        internal static IPackagingDesign LoadPackagingDesignFromDisk(string name)
        {
            return LoadPackagingDesignListFromDisk().FirstOrDefault(d => d.Name == name);
        }

        internal static IPackagingDesign LoadPackagingDesignFromDisk(int designId)
        {
            return LoadPackagingDesignListFromDisk().FirstOrDefault(d => d.Id == designId);
        }

        internal static IEnumerable<IPackagingDesign> LoadPackagingDesignListFromDisk()
        {
            string path = TestSettings.Default.InstallLocation + "\\Data\\Packaging Designs";
            var designManager =
                new PackagingDesignManager(path, new Mock<ICachingService>().Object,
                    new Mock<ILogger>().Object);

            designManager.LoadDesigns();
            return designManager.GetDesigns();
        }

        public class Rotation
        {
            public string Value { get; set; }
            public string Text { get; set; }
            public int Degree { get; set; }
            public bool Flipped { get; set; }

            public Rotation(string value)
            {
                Value = value;
                Flipped = value.Contains("Flip");
                Degree = int.Parse(value.Replace("Flip", "").Substring(6));
                Text = Degree.ToString() + " Degrees";
                if (Flipped)
                    Text += " Flipped";
            }

            public override string ToString()
            {
                return Text;
            }
        }
    }
}
