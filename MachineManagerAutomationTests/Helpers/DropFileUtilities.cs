﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MachineManagerAutomationTests.WebUITests;

namespace MachineManagerAutomationTests.Helpers
{
    public class DropFileUtilities
    {
 
        public static string DropFileExtension
        {
            get { return ".csv"; }
        }
        
        public static string DropStaplesBoxFirstDropFile(int numberOfOrders = 3, int startingOrderId = 1, int length = 1000, int width = 1000, int height = 1000, string pickzone = "0153", string lastPickZone = "0370", string designId = "RSC", string chcrtp = "CTN", bool randomizeDimentions = false)
        {
            var lines = new List<string>();
            var random = new Random();
            for (int i = startingOrderId; i < (startingOrderId + numberOfOrders); i++)
            {
                if (randomizeDimentions)
                {
                    length = (int)(Math.Floor((decimal)((random.Next(600) + 700) / 10)) * 10);
                    width = (int)(Math.Floor((decimal)((random.Next(600) + 700) / 10)) * 10);
                    height = (int)(Math.Floor((decimal)((random.Next(600) + 700) / 10)) * 10);
                }
                lines.Add(string.Format("20150806004;{0};001;{1};{2};{3};{4};BB;{5};0;{6};0;{7};0;U10;11;1;", i, designId, pickzone,
                    lastPickZone, chcrtp, length, width, height));
            }
            return DropFileUtilities.DropFileInDirectory(lines);
        }

        public static string DropBoxFirstWithConveyorDropFile(int numberOfJobs, int length = 10, int width = 10, int height = 5, string designId = "2030001", bool showHeaders = true, string classification = "", string pickzone = "", string conveyorPickZone = "")
        {
            var lines = new List<string>();
            if (showHeaders)
                lines.Add("Type;CustomerUniqueId;Name;Length;Width;Height;ClassificationNumber;CorrugateQuality;FirstPickZone;ConveyorPickZone;DesignId;ProductionGroupAlias");
            for (int i = 0; i < numberOfJobs; i++)
            {
                lines.Add(String.Format("BoxFirst;{0};sample{0};{1};{2};{3};{4};1;{5};{6};{7};PG1", i, length, width, height,
                    classification == "" ? ((i % 2 == 0) ? "1" : "2") : classification,
                    pickzone == "" ? ((i % 2 == 0) ? "AZ1" : "AZ2") : pickzone,
                    conveyorPickZone == "" ? ((i % 2 == 0) ? "AZ3" : "AZ4") : conveyorPickZone,
                    designId));
            }

            return DropFileUtilities.DropFileInDirectory(lines);
        }

        public static string DropBoxFirstDropFile(int numberOfJobs, int length = 10, int width = 10, int height = 5, string designId = "2030001", bool showHeaders = true, string classification = "", string pickzone = "")
        {
            var lines = new List<string>();
            if (showHeaders)
                lines.Add("Type;Command;CustomerUniqueId;Name;Length;Width;Height;ClassificationNumber;CorrugateQuality;FirstPickZone;DesignId;ProductionGroupAlias");
            for (int i = 0; i < numberOfJobs; i++)
            {
                lines.Add(String.Format("BoxFirst;1;{0};sample{0};{1};{2};{3};{4};1;{5};{6};PG1", i, length, width, height,
                    classification == "" ? ((i % 2 == 0) ? "1" : "2") : classification,
                    pickzone == "" ? ((i % 2 == 0) ? "AZ1" : "AZ2") : pickzone,
                    designId));
            }

            return DropFileUtilities.DropFileInDirectory(lines);
        }
       
        public static string DropOrdersDropFile(int numberOfOrders = 3, int quantityPerOrder = 1, int startingOrderId = 1, string articleId = "111", string pg = "PG1", bool showHeaders = true)
        {
            var lines = new List<string>();
            if (showHeaders)
                lines.Add("Type;OrderId;ArticleId;Quantity;ProductionGroupName");
            for (int i = startingOrderId; i < (startingOrderId + numberOfOrders); i++)
                lines.Add(string.Format("Order;order{0};{1};{2};{3}", i, articleId, quantityPerOrder, pg));

            return DropFileUtilities.DropFileInDirectory(lines);
        }

        public static DropFileItems CreateBoxFirstDropFileAlternatingPickZone(int numberOfJobs, int length = 10, int width = 10, int height = 5, string designId = "2000001", bool showHeaders = true, int classification = 0, string pickzone = "")
        {
            DropFileItems items = new DropFileItems();
            for (int i = 0; i < numberOfJobs; i++)
            {
                items.Add(DropFileJob.Create("CU_" + i, "J" + i, length, width, height, 
                    classification == 0 ? ((i % 2 == 0) ? 1 : 2) : classification,
                    pickzone == "" ? ((i % 2 == 0) ? "AZ1" : "AZ2") : pickzone, 
                    designId, string.Empty));
            }
            return items;
        }

        public static string DropDefaultBoxLastData(int numCartons = 1, string productionGroupAlias = "PG1", string designId = "2010001", int startingSerialNumber = 1)
        {
            var CartonsToDrop = new List<String>()
            {
                        "Type;SerialNumber;OrderId;Quantity;Name;Length;Width;Height;ClassificationNumber;CorrugateQuality;ProductionGroupAlias;DesignId;PrintInfoField1;PrintInfoField2;PrintInfoField3;PrintInfoField4;PrintInfoField5;PrintInfoField6;PrintInfoField7;PrintInfoField8;PrintInfoField9;PrintInfoField10"
            };

            for (int i = 0; i < numCartons; i++)
                CartonsToDrop.Add(string.Format("BoxLast;{0};;;;5;5;5;1;1;{1};{2};r1;r2;r3;r4;r5;r6;r7;r8;r9;r10", i + startingSerialNumber, productionGroupAlias, designId));

            return DropFileUtilities.DropFileInDirectory(CartonsToDrop);
        }

        public static string DropLabelOnlyBoxLastData(int numLabels, string pickZone = "AZ1", int startingId = 1, bool priorityLabels = false)
        {
            //sample file:
            //Type;CustomerUniqueId;Pickzone;ShipToName;ShipToAddress1;ShipToAddress2;IsPriority;LabelWidth;LabelHeight;Template
            //Label;91;pz1;jon doe;444 your street; apt# 11;false;4;2;DoubleBarcode
            //Label;92;pz1;ana doe;555 your street; apt# 12;False;4;2;Template
            //Label;93;pz2;joe doe;666 your street; apt# 13;true;4;6;Template
            //Label;94;pz2;pat doe;777 your street; apt# 14;FALSE;4;6;Template

            var labelsToDrop = new List<string>()
            {
                "Type;CustomerUniqueId;Pickzone;ShipToName;ShipToAddress1;ShipToAddress2;IsPriority;LabelWidth;LabelHeight;Template"
            };
            for (int i = 0; i < numLabels; i++)
                labelsToDrop.Add(string.Format("Label;{0};{1};jose doe;555 your street; apt# 12;{2};4;2;Template", i + startingId, pickZone, priorityLabels));

            return DropFileUtilities.DropFileInDirectory(labelsToDrop);
        }

        #region Methods generic for all kinds of files

        public static string DropFileInDirectory(string dropFolderPath, List<string> dropData, string dropFileType = null)
        {
            if (dropFileType == null)
                dropFileType = string.Empty;
            else
                dropFileType += "_";

            var fileName = Path.Combine(dropFolderPath, dropFileType + Guid.NewGuid() + DropFileUtilities.DropFileExtension);

            using (var sw = new StreamWriter(fileName))
            {
                dropData.ForEach(sw.WriteLine);
            }
            return fileName;
        }
        public static string DropFileInDirectory(List<string> dropData, string dropFileType = null)
        {
            return DropFileInDirectory(Path.Combine(TestSettings.Default.InstallLocation, "DropFolder\\"), dropData, dropFileType);
        }
        public static List<string> CreateDropFile(List<DropFileColumn> cols, List<IDropFileItem> items)
        {
            List<string> df = new List<string>();
            StringBuilder sb = new StringBuilder();
            foreach (var c in cols)
            {
                if (sb.Length > 0) sb.Append(";");
                sb.Append(c.ColumnName);
            }
            df.Add(sb.ToString());

            foreach (var itm in items)
            {
                sb.Clear();
                foreach (var c in cols)
                {
                    if (sb.Length > 0) sb.Append(";");
                    sb.Append(itm.GetType().GetProperty(c.PropertyName).GetValue(itm));
                }
                df.Add(sb.ToString());
            }
            return df;

        }
        public static void CreateFile(DropFileType t, List<IDropFileItem> content)
        {
            List<DropFileColumn> cols = GetColumns(t);
            foreach (var c in content)
                c.Type = t.ToString();

            List<string> df = CreateDropFile(cols, content);
            DropFileInDirectory(df, t.ToString());
        }
        public static List<DropFileColumn> GetColumns(DropFileType fileType)
        {
            List<DropFileColumn> cols = new List<DropFileColumn>();

            if (fileType == DropFileType.Article)
            {
                cols.Add(new DropFileColumn("Type", "Type"));
                cols.Add(new DropFileColumn("OrderId", "OrderId"));
                cols.Add(new DropFileColumn("ArticleId", "ArticleId"));
                cols.Add(new DropFileColumn("Quantity", "Quantity"));
                cols.Add(new DropFileColumn("ProductionGroupName", "ProductionGroupName"));
            }

            if (fileType == DropFileType.BoxFirst)
            {
                cols.Add(new DropFileColumn("Type", "Type"));
                cols.Add(new DropFileColumn("CustomerUniqueId", "CustomerUniqueId"));
                cols.Add(new DropFileColumn("Name", "Name"));
                cols.Add(new DropFileColumn("Length", "Length"));
                cols.Add(new DropFileColumn("Width", "Width"));
                cols.Add(new DropFileColumn("Height", "Height"));
                cols.Add(new DropFileColumn("ClassificationNumber", "ClassificationNumber"));
                cols.Add(new DropFileColumn("CorrugateQuality", "CorrugateQuality"));
                cols.Add(new DropFileColumn("FirstPickZone", "FirstPickZone"));
                cols.Add(new DropFileColumn("DesignId", "DesignId"));
                cols.Add(new DropFileColumn("ProductionGroupAlias", "ProductionGroupAlias"));
            }


            return cols;
        }

        #endregion

    }

    public interface IDropFileItem
    {
        string Type { get; set; }
    }

    public enum DropFileType
    {
        Article,
        CustomJob,
        BoxFirst,
        BoxLast
    }

    public class DropFileColumn
    {
        public string ColumnName { get; set; }
        public string PropertyName { get; set; }

        public DropFileColumn(string columnName, string property)
        {
            ColumnName = columnName;
            PropertyName = property;
        }

        public override string ToString()
        {
            return ColumnName;
        }
    }

    public class DropFileJob : IDropFileItem
    {
        public string Type { get; set; }
        public string Command { get; set; }
        public string CustomerUniqueId { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int ClassificationNumber { get; set; }
        public int CorrugateQuality { get; set; }
        public string FirstPickZone { get; set; }
        public int DesignId { get; set; }
        public string ProductionGroupAlias { get; set; }

        public static DropFileJob Create(string customerUniqueId, string name, int length, int width, int height,
            int classificationNumber, string firstPickZone, string designId, string pgName, int quantity = 1, int corrugateQuality = 1)
        {
            DropFileJob f = new DropFileJob();
            f.Type = null;
            f.CustomerUniqueId = customerUniqueId;
            f.Name = name;
            f.Length = length;
            f.Width = width;
            f.Height = height;
            f.ClassificationNumber = classificationNumber;
            f.CorrugateQuality = corrugateQuality;
            f.FirstPickZone = firstPickZone;
            f.DesignId = int.Parse(designId);
            f.ProductionGroupAlias = pgName;
            return f;
        }
        public static DropFileItems CreateMany(int numberOfItems, int classificationNumber,
            string firstPickZone, string designId, string pgName, int startNumber = 0, string customerUniqueIdPrefix = "CU_", string namePrefix = "J")
        {
            return CreateMany(numberOfItems, 10, 10, 10, classificationNumber,firstPickZone, designId, pgName, startNumber);
        }
        public static DropFileItems CreateMany(int numberOfItems, int length, int width, int height, int classificationNumber, 
            string firstPickZone, string designId, string pgName, int startNumber = 0, string customerUniqueIdPrefix = "CU_", string namePrefix = "J")
        {
            var items = new DropFileItems();
            for (int i = startNumber; i < numberOfItems + startNumber; i++)
            {
                var f = Create(customerUniqueIdPrefix + (i+1), namePrefix + (i + 100), length, width, height, classificationNumber, firstPickZone, designId, pgName);
                items.Add(f);
            }
            return items;
        }
    }

    public class DropFileItems : List<IDropFileItem>
    {
    }

    public class DropFileArticle : IDropFileItem
    {
        public string Type { get; set; }
        public string ArticleName { get; set; }
        public string Description { get; set; }
        public IEnumerable<IDropFileCartonOrLabel> SubArticles { get; set; }
        public DropFileArticle()
        {
            this.Type = "Article";
        }
    }

    public class DropFileArticleJob : IDropFileItem
    {
        public string Type { get; set; }
        public string OrderId { get; set; }
        public string ArticleId { get; set; }
        public int Quantity { get; set; }
        public string ProductionGroupName { get; set; }
        public void DropFileArticle()
        {
            this.Type = "Order";
        }

        public static DropFileArticleJob CreateJob(string articleId,string orderId,string pgName, int quantity = 1)
        {
            DropFileArticleJob f = new DropFileArticleJob();
            f.ArticleId = articleId;
            f.OrderId = orderId;
            f.ProductionGroupName = pgName;
            f.Quantity = quantity;
            return f;
        }

        public static List<IDropFileItem> CreateMany(int numberOfItems, string pgName, int quantity = 1, string orderIdPrefix = "Order-", string articleIdPrefix = "A")
        {
            var items = new List<IDropFileItem>();
            for (int i = 0; i < numberOfItems; i++)
            {
                var f = CreateJob(articleIdPrefix + (i + 100),orderIdPrefix + (i+1),pgName, quantity);
                items.Add(f);
            }
            return items;
        }
    }

    public class DropFileCustomJob : IDropFileItem
    {
        public string Type { get; set; }
        public string OrderId { get; set; }
        public int Quantity { get; set; }
        public string ProductionGroupName { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int CorrugateQuality { get; set; }
        public int DesignId { get; set; }
        public DropFileCustomJob()
        {
            this.Type = "Order";
        }

        public static DropFileCustomJob CreateJob(string orderId, string pgName, int length, int width, int height, int corrugateQuality, int designId, int quantity = 1)
        {
            DropFileCustomJob f = new DropFileCustomJob();
            f.OrderId = orderId;
            f.Quantity = quantity;
            f.ProductionGroupName = pgName;
            f.Length = length;
            f.Width = width;
            f.Height = height;
            f.CorrugateQuality = corrugateQuality;
            f.DesignId = designId;
            return f;
        }
        public static List<IDropFileItem> CreateMany(int numberOfItems, string pgName, int length, int width, int height, int corrugateQuality, int designId, int quantity = 1, string orderIdPrefix = "Order_")
        {
            var items = new List<IDropFileItem>();
            for (int i = 0; i < numberOfItems; i++)
            {
                var f = CreateJob(orderIdPrefix + (i+1),pgName, length, width, height,corrugateQuality,designId, quantity);
                items.Add(f);
            }
            return items;
        }
    }

    public class DropFileCarton : IDropFileCartonOrLabel
    {
        public string Type { get { return "Carton"; } }
        public string Name { get; set; }
        public int DesignId { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Quantity { get; set; }
        public string Rotation { get; set; }
        public int CorrugateQuality { get; set; }
    }

    public static class DropFileCubeCommand
    {
        public static readonly string New = "New";
    }

    public class DropFileCubeToOrder : IDropFileItem
    {
        public string Type { get; set; }
        public string Command { get; set; }
        public string OrderId { get; set; }
        public int BoxId { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Classification { get; set; }
        public int CorrugateQuality { get; set; }
        public string ProductionGroupName { get; set; }
        public int DesignId { get; set; }
        public string AddInfo1 { get; set; }
        public string AddInfo2 { get; set; }
        public string AddInfo3 { get; set; }
        public string AddInfo4 { get; set; }
        public string AddInfo5 { get; set; }
        public string AddInfo6 { get; set; }
        public string AddInfo7 { get; set; }
        public string AddInfo8 { get; set; }
        public string AddInfo9 { get; set; }
        public string AddInfo10 { get; set; }
        public DropFileCubeToOrder()
        {
            this.Type = "Cube";
        }
    }

    public class DropFileCube : IDropFileItem
    {
        public string Type { get; set; }
        public string CustomerUniqueId { get; set; }
        public string Name { get; set; }
        public string Command { get; set; }
        public string Classification { get; set; }
        public string OrderId { get; set; }
        public int BoxId { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int CorrugateQuality { get; set; }
        public string ProductionGroupName { get; set; }
        public int DesignId { get; set; }
        public string AddInfo1 { get; set; }
        public string AddInfo2 { get; set; }
        public string AddInfo3 { get; set; }
        public string AddInfo4 { get; set; }
        public string AddInfo5 { get; set; }
        public string AddInfo6 { get; set; }
        public string AddInfo7 { get; set; }
        public string AddInfo8 { get; set; }
        public string AddInfo9 { get; set; }
        public string AddInfo10 { get; set; }
        public DropFileCube()
        {
            this.Type = "Cube";
        }
    }

    public class DropFileLabel : IDropFileCartonOrLabel
    {
        public string Type { get { return "Label"; } }
        public string Name { get; set; }
        public string PrintDataCustomerUniqueId { get; set; }
        public string Template { get; set; }

        public string PickZone { get; set; }
        public string ShipToName { get; set; }
        public string ShipToAddress1 { get; set; }
        public string ShipToAddress2 { get; set; }
        public bool IsPriority { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public interface IDropFileCartonOrLabel
    {
        string Name { get; set; }
        string Type { get; }
    }
}
