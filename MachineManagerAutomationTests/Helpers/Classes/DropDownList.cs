﻿using System;
using System.Collections.Generic;
using System.Linq;

using CUITe.Controls.HtmlControls;

using PackNet.Common.Utils;

namespace MachineManagerAutomationTests.Helpers
{
    public class ListItem
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool Selected { get; set; }

        public ListItem(string value, string text)
        {
            Value = value;
            Text = text;
        }

        public ListItem(string text)
        {
            Value = text;
            Text = text;
        }

        public override string ToString()
        {
            return Value + " - " + Text;
        }
    }

    public class DropDownList
    {
        private readonly CUITe_HtmlComboBox _lst = null;
        private List<ListItem> _items = null;

        public DropDownList(CUITe_HtmlComboBox lst)
        {
            _lst = lst;
        }

        public ListItem SelectedItem
        {
            get { return Items.Find(z => z.Text == _lst.SelectedItem); }
        }

        public int SelectedIndex
        {
            get { return _lst.SelectedIndex; }
        }

        public bool SelectItem(string text)
        {
            try
            {
                Retry.For(() => _lst.SelectItem(text), TimeSpan.FromSeconds(5));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SelectItem(int index)
        {
            try
            {
                Retry.For(() => _lst.SelectItem(index), TimeSpan.FromSeconds(5));
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Clone the list items, with parameter removeEmptyItems set to true all items with empty value will be removed from the list
        /// </summary>
        /// <param name="removeEmptyItems"></param>
        /// <returns></returns>
        public List<ListItem> Clone(bool removeEmptyItems = false)
        {
            var newList = new List<ListItem>();
            foreach (var itm in Items)
            {
                if (removeEmptyItems)
                {
                    if (!string.IsNullOrEmpty(itm.Value.Trim()))
                    {
                        newList.Add(itm);
                    }
                }
                else
                {
                    newList.Add(itm);
                }
            }
            return newList;
        }
        /// <summary>
        /// Compare a string array to value and text in the list items to see if they are exactly the same
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="removeEmptyItems"></param>
        /// <returns></returns>
        public bool ListsAreEqual(string[] arr, bool removeEmptyItems = false)
        {
            if (arr == null)
                throw new ArgumentNullException("The parameter 'arr' can't be null.");

            var items = Clone(true);

            if (arr.Length != items.Count)
                return false;

            foreach (var s in arr)
            {
                if (!ItemExists(s, items))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The supplied value exists in the list
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ItemExists(string value)
        {
            return Items.Any(v => v.Text.Equals(value) || v.Value.Equals(value));
        }

        // helper method called from ListsAreEqual
        private bool ItemExists(string value, List<ListItem> items)
        {
            return items.Any(v => v.Text.Equals(value) || v.Value.Equals(value));
        }

        public void Refresh()
        {
            _items = null;
            SetItems();
        }

        public List<ListItem> Items
        {
            get
            {
                SetItems();
                return _items;
            }
        }

        /// <summary>
        /// Since the ItemCount sometimes returns an error wë need to handle the error
        /// so that the count is returned nicely. -1 = count could not be returned
        /// </summary>
        /// <returns></returns>
        private int GetComboBoxItemCount()
        {
            try
            {
                return _lst.ItemCount;
            }
            catch (Exception)
            {
                return -1;
            }

        }

        private void SetItems()
        {
            if (_items != null && (GetComboBoxItemCount() != _items.Count))
                _items = null;

            if (_items == null)
            {
                _items = new List<ListItem>();
                foreach (var child in _lst.GetChildren())
                {
                    var opt = (CUITe_HtmlCustom)child;
                    var li = new ListItem(opt.ValueAttribute, opt.InnerText);
                    li.Selected = _lst.SelectedItem.Equals(li.Text);
                    _items.Add(li);
                }
            }
            else
            {
                // refresh selected item
                foreach (var itm in _items)
                    itm.Selected = false;
                _items[_lst.SelectedIndex].Selected = true;
            }
        }
    }
}