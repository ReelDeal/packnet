using System.Windows.Forms;

using PackNet.Common.Interfaces.Machines;
using PackNet.FusionSim.Models;
using PackNet.FusionSim.ViewModels;
using System;
using System.Threading;

namespace MachineManagerAutomationTests
{
    public class SimulatorUtilities
    {
        private readonly SimController simController;
        private readonly Thread oThread;


        /// <summary>
        /// Connects the and get printer sim.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns></returns>
        public ZebraPrinterSimulatorViewModel ConnectAndGetPrinterSim(INetworkConnectedMachine machine)
        {
            return ConnectAndGet(machine, simController.SimulatorCollection.AddPrinter);
        }

        /// <summary>
        /// Connects the and get fusion sim.
        /// </summary>
        /// <param name="machine">The machine.</param>
        /// <returns></returns>
        public FusionSimulatorViewModel ConnectAndGetFusionSim(INetworkConnectedMachine machine)
        {
            FusionSimulatorViewModel viewModel = ConnectAndGet(machine, simController.SimulatorCollection.AddFusion);
            BaseCutCreaseMachineSimulator baseMachineCommunicationBasedMachine = viewModel.Simulator;
            baseMachineCommunicationBasedMachine.Play = true;
            return viewModel;
        }

        public EmSimulatorViewModel ConnectAndGetEmSim(INetworkConnectedMachine machine)
        {
            return ConnectAndGet(machine, simController.SimulatorCollection.AddEm);
        }

        public SimulatorUtilities()
        {
            simController = new SimController();

            // Create the thread object, passing in the Alpha.Beta method
            // via a ThreadStart delegate. This does not start the thread.
            oThread = new Thread(simController.Start);

            // Start the thread
            oThread.Start();

            // Spin for a while waiting for the started thread to become
            // alive:
            while (!oThread.IsAlive)
            {
            }
            
        }


        ~SimulatorUtilities()
        {
            oThread.Abort();
            while (oThread.IsAlive)
            {
            }
        }

        private static T ConnectAndGet<T>(INetworkConnectedMachine machine, Func<string, string, int, T> addFunction) where T: Simulator
        {
            var sim = addFunction(machine.Alias, machine.IpOrDnsName, machine.Port);

            StartAndCompleteSimulation(sim);
            //sim.StartStopSimulation();
            //Thread.Sleep(20000);
            //sim.StartupComplete();
            //Thread.Sleep(5000);
            
            return sim;
        }

        public void Close()
        {
            simController.Stop();
            simController.Dispose();
        }

        public static void StartAndCompleteSimulation(Simulator sim)
        {
            if (!sim.IsSimulationStarted)
            {
                sim.StartStopSimulation();
                Thread.Sleep(10000);
                sim.StartupComplete();
                Thread.Sleep(5000);
            }
        }
    }
}