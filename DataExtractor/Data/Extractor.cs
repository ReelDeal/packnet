﻿namespace Packsize.MachineManager.DataExtractor.Data
{
    using System;
    using System.Data;
    using System.Globalization;
    using System.IO;
    using System.Text;

    using Npgsql;

    using PackNet.Common.Utils;

    public static class Extractor
    {
        internal static void Extract(string server, string database, string table, string password, string targetPath)
        {
            StreamWriter targetStream = new StreamWriter(GetTargetStream(targetPath), Encoding.Default);
            DataTable data = GetData(server, database, table, password);
            CSVWriter.WriteTableToCSV(targetStream, data, ';', true, false);
            targetStream.Close();
            targetStream.Dispose();
            targetStream = null;
        }

        private static DataTable GetData(string server, string database, string table, string password)
        {
            DataTable t = new DataTable(table);
            t.Locale = CultureInfo.InvariantCulture;
            NpgsqlCommand cmd = GetSqlCommand(server, database, table, password);
            cmd.Connection.Open();
            NpgsqlDataAdapter adapter = new NpgsqlDataAdapter(cmd);
            adapter.Fill(t);

            foreach (DataRow r in t.Rows)
            {
                foreach (DataColumn c in t.Columns)
                {
                    if (c.DataType == Type.GetType("System.DateTime"))
                    {
                        r[c] = TimeZoneInfo.ConvertTimeFromUtc((DateTime)r[c], TimeZoneInfo.Local).ToString("yyyy-MM-dd HH:mm:ss");
                    }
                }
            }

            cmd.Connection.Close();
            cmd.Connection.Dispose();
            cmd.Dispose();
            adapter.Dispose();

            return t;
        }

        private static NpgsqlCommand GetSqlCommand(string server, string database, string table, string password)
        {
            return new NpgsqlCommand("select * from " + table, new NpgsqlConnection("Server=" + server + ";database=" + database + ";User ID=postgres;Password=" + password));
        }

        private static Stream GetTargetStream(string path)
        {
            return new FileStream(path, FileMode.Create, FileAccess.Write, FileShare.None);
        }
    }
}
