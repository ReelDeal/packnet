﻿namespace Packsize.MachineManager.DataExtractor
{
    using Microsoft.Win32;

    public static class DialogService
    {
        public static string SaveFile()
        {
            return SaveFile(string.Empty);
        }

        public static string SaveFile(string caption)
        {
            return SaveFile(caption, null);
        }

        public static string SaveFile(string caption, string defaultFileName)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = caption;
            if (defaultFileName != null)
            {
                sfd.FileName = defaultFileName;
            }

            if (sfd.ShowDialog().Value)
            {
                return sfd.FileName;
            }
            return null;
        }

        public static string OpenFile()
        {
            return OpenFile(string.Empty);
        }

        public static string OpenFile(string caption)
        {
            return OpenFile(caption, null);
        }

        public static string OpenFile(string caption, string defaultFileName)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (!string.IsNullOrEmpty(caption))
            {
                ofd.Title = caption;
            }

            if (!string.IsNullOrEmpty(defaultFileName))
            {
                ofd.FileName = defaultFileName;
            }

            if (ofd.ShowDialog().Value)
            {
                return ofd.FileName;
            }
            return null;
        }
    }
}
