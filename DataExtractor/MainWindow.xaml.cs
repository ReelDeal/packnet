﻿namespace Packsize.MachineManager.DataExtractor
{
    using System.Windows;
    using Packsize.MachineManager.DataExtractor.ViewModels;

    public sealed partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var viewModel = new MainWindowViewModel();
            DataContext = viewModel;
        }
    }
}
