﻿namespace Packsize.MachineManager.DataExtractor.Input
{
    using System.Windows.Input;

    internal class ItemCommand
    {
        public ItemCommand(object item, ICommand command)
        {
            Command = command;
            Item = item;
        }

        public ICommand Command { get; private set; }

        public object Item { get; private set; }
    }
}
