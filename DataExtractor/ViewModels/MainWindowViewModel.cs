﻿namespace Packsize.MachineManager.DataExtractor.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Diagnostics;

    using Packsize.MachineManager.DataExtractor.Data;
    using Packsize.MachineManager.DataExtractor.Input;

    internal class MainWindowViewModel
    {
        private ObservableCollection<ItemCommand> files = new ObservableCollection<ItemCommand>();

        public MainWindowViewModel()
        {
            List<ItemCommand> cmds = new List<ItemCommand>();
            cmds.Add(new ItemCommand("_History data", new RelayCommand(param => ExportHistoryData())));
            Commands = new ObservableCollection<ItemCommand>(cmds);
        }

        public ObservableCollection<ItemCommand> Commands { get; private set; }

        public ObservableCollection<ItemCommand> Files
        {
            get { return files; }
        }

        private void ExtractTable(string server, string database, string table, string password)
        {
            string targetPath = DialogService.SaveFile("Path to " + table + " file", table + ".csv");
            if (!string.IsNullOrEmpty(targetPath))
            {
                Extractor.Extract(server, database, table, password, targetPath);
                ItemCommand cmd = new ItemCommand(targetPath, new RelayCommand(param => Process.Start(targetPath)));
                Files.Add(cmd);
            }
        }

        private void ExportHistoryData()
        {
            ExtractTable("localhost", "history", "cartonproductiondata", "trazan123@");
            ExtractTable("localhost", "history", "cartonrequest", "trazan123@");
        }
    }
}
