﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Templates
{
    public interface ITemplateRepository : IRepository<Template>
    {
    }
}