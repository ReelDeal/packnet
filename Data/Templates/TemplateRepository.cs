﻿
using PackNet.Common.Interfaces.DTO;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.Templates
{
    public class TemplateRepository : MongoDbRepository<Template>, ITemplateRepository
    {
        public TemplateRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Templates")
        {
            
        }
    }
}
