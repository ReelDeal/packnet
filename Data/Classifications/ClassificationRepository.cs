﻿using System;
using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.Classifications
{
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Data.Serializers;

    public class ClassificationRepository : MongoDbRepository<Classification>, IClassificationRepository
    {
        private readonly object syncObject = new Object();

        private readonly bool serializerRegistered;

        public ClassificationRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Classifications")
        {
            EnsureUnassignedClassificationExists();
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<ClassificationStatuses>(new EnumerationSerializer());
                serializerRegistered = true;
            }
        }

        public Classification Find(string name)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.Alias == name);
        }

        public void EnsureUnassignedClassificationExists()
        {
            if (Find("Unassigned") != null) 
                return;

            Create(new Classification
                   {
                       Id = Guid.NewGuid(),
                       Alias = "Unassigned",
                       BuiltInClassification = true
                   });
        }
    }
}
