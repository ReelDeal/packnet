﻿using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Classifications
{
    public interface IClassificationRepository : IRepository<Classification>
    {
        Classification Find(string name);
        void EnsureUnassignedClassificationExists();
    }
}