﻿using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.UserManagement
{
    public interface IUserRepository : IRepository<User>
    {
        User Find(string userName);
        User Find(string userName, string password);
        User FindByToken(string token);
        User FindByAutoLogin();

        void EnsureDefaultUserExists();
    }
}