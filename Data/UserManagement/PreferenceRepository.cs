﻿using System.Linq;
using MongoDB.Driver.Linq;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.UserManagement
{
    public class PreferenceRepository : MongoDbRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Preference")
        {
        }

        public Preference Find(User user)
        {
            return Collection.AsQueryable().FirstOrDefault(u => u.UserId == user.Id);
        }
    }
}