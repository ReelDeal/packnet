﻿using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.UserManagement
{
    public interface IPreferenceRepository : IRepository<Preference>
    {
        Preference Find(User user);
    }
}