﻿using System;
using PackNet.Common.Interfaces.DTO.Users;

namespace PackNet.Data.UserManagement
{
    using System.Linq;

    using MongoDB.Driver.Linq;

    using Repositories.MongoDB;

    public class UserRepository : MongoDbRepository<User>, IUserRepository
    {
        public UserRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Users")
        {
        }

        public User Find(string userName)
        {
            return Collection.AsQueryable().FirstOrDefault(u => u.UserName == userName);
        }

        public User FindByToken(string token)
        {
            return Collection.AsQueryable().FirstOrDefault(u => u.Token == token);
        }

        public User FindByAutoLogin()
        {
            var dbUser = Collection.AsQueryable().FirstOrDefault(u => u.IsAutoLogin);
            // TODO: We should create a second User object
            if (dbUser != null)
            {
                dbUser.Password = string.Empty;
            }
            return dbUser;
        }

        public User Find(string userName, string password)
        {
            return Collection.AsQueryable().FirstOrDefault(u => u.UserName == userName && u.Password == password);
        }

        public void EnsureDefaultUserExists()
        {
            if (Find(Defaults.Default.AdminUserName) != null) 
                return;

            //if a system user exists...delete it
            var existingSystemUser = Collection.AsQueryable().FirstOrDefault(u => u.IsSystem);
            if(existingSystemUser != null)
                Delete(existingSystemUser);

            Create(new User
                   {
                       Id = Guid.NewGuid(),
                       UserName = Defaults.Default.AdminUserName,
                       Password = Defaults.Default.AdminPassword,
                       IsSystem = true,
                       IsAdmin = true
                   });
        }
    }
}