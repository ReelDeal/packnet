﻿using System;
using System.Activities.Tracking;
using System.Linq;

using MongoDB.Driver.Builders;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.WorkflowTracking;
using PackNet.Data.Repositories.MongoDB;

using Label = PackNet.Common.Interfaces.DTO.PrintingMachines.Label;

namespace PackNet.Data.Workflows
{
    public class WorkflowTrackingRepository : MongoDbRepository<WorkflowLifetime>, IWorkflowTrackingRepository
    {
        static WorkflowTrackingRepository()
        {
            MongoDbHelpers.TryRegisterClassMap<TrackingRecord>();
            MongoDbHelpers.TryRegisterClassMap<WorkflowInstanceRecord>();
            MongoDbHelpers.TryRegisterClassMap<WorkflowInstanceAbortedRecord>();
            MongoDbHelpers.TryRegisterClassMap<WorkflowInstanceSuspendedRecord>();
            MongoDbHelpers.TryRegisterClassMap<WorkflowInstanceTerminatedRecord>();
            MongoDbHelpers.TryRegisterClassMap<WorkflowInstanceUnhandledExceptionRecord>();
            MongoDbHelpers.TryRegisterClassMap<WorkflowInstanceUpdatedRecord>();
            MongoDbHelpers.TryRegisterClassMap<ActivityScheduledRecord>();
            MongoDbHelpers.TryRegisterClassMap<ActivityStateRecord>();
            MongoDbHelpers.TryRegisterClassMap<BookmarkResumptionRecord>();
            MongoDbHelpers.TryRegisterClassMap<CancelRequestedRecord>();
            MongoDbHelpers.TryRegisterClassMap<FaultPropagationRecord>();
            MongoDbHelpers.TryRegisterClassMap<CustomTrackingRecord>();
            MongoDbHelpers.TryRegisterClassMap<PacksizeTrackingRecord>();
            MongoDbHelpers.TryRegisterClassMap<JsonTrackingRecord>();
            MongoDbHelpers.TryRegisterClassMap<PacksizeActivityTrackingRecordWrapper>();
            MongoDbHelpers.TryRegisterClassMap<MachineGroup>();
            MongoDbHelpers.TryRegisterClassMap<TiledCarton>();
            MongoDbHelpers.TryRegisterClassMap<Common.Interfaces.DTO.Carton.Carton>();
            MongoDbHelpers.TryRegisterClassMap<Kit>();
            MongoDbHelpers.TryRegisterClassMap<Label>();
            MongoDbHelpers.TryRegisterClassMap<Importable>();
        }

        public WorkflowTrackingRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "WorkflowLifetimes")
        {
            //Set up the TTL to remove records older then a specified number of days
            if (Properties.Settings.Default.DaysToRetainWorkflowInstances > 0)
            {
                var keys = IndexKeys.Ascending("expiry");
                var options = IndexOptions.SetTimeToLive(TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainWorkflowInstances));
                var existing = Collection.GetIndexes().FirstOrDefault(i => i.Name == "expiry_1");
                if (existing != null &&
                    existing.TimeToLive.Ticks != TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainWorkflowInstances).Ticks)
                {
                    Collection.DropIndexByName("expiry_1");
                }
                Collection.EnsureIndex(keys, options);
            }
        }

        public WorkflowTrackingRepository(string conn, string db)
            : base(conn, db, "WorkflowLifetimes")
        {
        }
    }
}