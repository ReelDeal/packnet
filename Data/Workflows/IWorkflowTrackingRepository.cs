﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Workflows
{
    public interface IWorkflowTrackingRepository : IRepository<WorkflowLifetime>
    {
    }
}