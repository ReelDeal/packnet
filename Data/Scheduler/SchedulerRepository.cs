﻿using PackNet.Common.Interfaces.DTO.Scheduler;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.Scheduler
{
    public class SchedulerRepository : MongoDbRepository<SchedulerJob>, ISchedulerRepository
    {
        public SchedulerRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "SchedulerJobs")
        {
        }
    }
}
