using PackNet.Common.Interfaces.DTO.Scheduler;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Scheduler
{
    public interface ISchedulerRepository : IRepository<SchedulerJob>
    {
    }
}