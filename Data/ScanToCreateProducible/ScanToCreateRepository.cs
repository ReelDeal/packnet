﻿using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.ScanToCreateProducible
{
    public class ScanToCreateRepository : MongoDbRepository<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible>, IScanToCreateRepository
    {
        public ScanToCreateRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "ScanToCreateProducible")
        {
            //Set up the TTL to remove records older then a specified number of days
            if (Properties.Settings.Default.DaysToRetainWorkflowInstances > 0)
            {
                var keys = IndexKeys.Ascending("expiry");
                var options = IndexOptions.SetTimeToLive(TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainScanToCreateProducibles));
                var existing = Collection.GetIndexes().FirstOrDefault(i => i.Name == "expiry_1");
                if (existing != null &&
                    existing.TimeToLive.Ticks != TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainScanToCreateProducibles).Ticks)
                {
                    Collection.DropIndexByName("expiry_1");
                }
                Collection.EnsureIndex(keys, options);
            }
        }

        public Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible FindByCustomerUniqueId(string customerUniqueId)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.CustomerUniqueId == customerUniqueId);
        }

        public Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible FindByIdOrSubId(Guid id)
        {
            var result = Collection.AsQueryable().FirstOrDefault(c => c.Id == id);
            if (result == null)
            {
                //TODO: create index to check for subproducibles
                result = All().FirstOrDefault(p => ContainsId(p.Producible, id));
                //result =
                //    Collection.AsQueryable()
                //    .Where(stc => stc.Producible is Kit)
                //    .FirstOrDefault(stc => (stc.Producible as Kit).ItemsToProduce.Any(i => i.Id == id));
                //result = Collection.AsQueryable().FirstOrDefault(p => ContainsId(p.Producible, id));
                //.FirstOrDefault(c => (c.Producible as Kit).ItemsToProduce.Any(i => i.Id == id));
            }
            return result;
        }

        private bool ContainsId(IProducible p, Guid id)
        {
            var kit = p as Kit;
            if (kit != null)
                return kit.ItemsToProduce.Any(i => i.Id == id);
            return false;
        }

        public IEnumerable<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible> FindWithStatus(params ProducibleStatuses[] statuses)
        {
            return Collection.AsQueryable().Where(p => statuses.Contains(p.ProducibleStatus)).ToList();
        }
        public IEnumerable<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible> FindWhereStatusIsNot(params ProducibleStatuses[] statuses)
        {
            //todo: consider making this async for each status then rejoining the results.
            //todo: test with Query.In
            return Collection.AsQueryable().Where(p => !statuses.Contains(p.ProducibleStatus)).ToList();
        }

        public IEnumerable<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible> GetProductionHistory(Guid machineGroupId, int qty = 20)
        {
            var query = Query.And(
                    Query.EQ("ProducedOnMachineGroupId", machineGroupId),
                    Query.EQ("ProducibleStatus", ProducibleStatuses.ProducibleCompleted.DisplayName));

            return Collection.Find(query).SetFields(Fields.Slice("History", -1)).SetSortOrder(new SortByBuilder().Descending("History.OccuredOn")).SetLimit(qty);
        }
    }
}
