﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.ScanToCreateProducible
{
    public interface IScanToCreateRepository : IRepository<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible>
    {
        Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible FindByCustomerUniqueId(string customerUniqueId);

        Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible FindByIdOrSubId(Guid id);

        IEnumerable<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible> FindWithStatus(params ProducibleStatuses[] statuses);

        IEnumerable<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible> FindWhereStatusIsNot(
            params ProducibleStatuses[] statuses);

        IEnumerable<Common.Interfaces.DTO.ScanToCreate.ScanToCreateProducible> GetProductionHistory(Guid machineGroupId, int qty = 20);
    }
}
