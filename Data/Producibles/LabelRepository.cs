﻿using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Data.Repositories.MongoDB;


namespace PackNet.Data.Carton
{
    public class LabelRepository: MongoDbRepository<Label>, ILabelRepository
    {

        public LabelRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Label")
        {
           
        }
    }

    public interface ILabelRepository:IRepository<Label>
    {
    }
}
