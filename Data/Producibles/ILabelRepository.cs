﻿using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Carton
{
    public interface ILabelRepository : IRepository<Label>
    {
    }
}