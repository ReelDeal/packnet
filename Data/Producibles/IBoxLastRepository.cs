﻿
using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Producibles
{
    public interface IBoxLastRepository : IRepository<BoxLastProducible>
    {
        BoxLastProducible FindByCustomerUniqueId(string customerUniqueId);

        IEnumerable<BoxLastProducible> FindWithStatus(params ProducibleStatuses[] statuses);

        IEnumerable<BoxLastProducible> FindWhereStatusIsNot(params ProducibleStatuses[] statuses);

        long DeleteCartonsBetweenDates(DateTime startDateTime, DateTime endDateTime);

        IEnumerable<BoxLastProducible> GetProductionHistoryForMachineGroup(Guid id, int qty = 20);
    }
}
