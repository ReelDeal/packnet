﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Data.Repositories.MongoDB;
using System;
using System.Linq;

namespace PackNet.Data.Producibles
{
    public class BoxLastRepository : MongoDbRepository<BoxLastProducible>, IBoxLastRepository
    {
        static BoxLastRepository()
        {
            try
            {
                BsonClassMap.RegisterClassMap<Common.Interfaces.DTO.Carton.Carton>(cm => cm.AutoMap());
                BsonClassMap.RegisterClassMap<PackagingDesign>(cm => cm.AutoMap());
                BsonClassMap.RegisterClassMap<Label>(cm => cm.AutoMap());
                BsonClassMap.RegisterClassMap<Kit>(cm => cm.AutoMap());
            }
            catch (Exception)
            {
                // we don't care that it's already been registered...
            }
        }

        public BoxLastRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "BoxLastProducible")
        {
            Collection.CreateIndex(new IndexKeysBuilder()
                .Ascending("CustomerUniqueId"), IndexOptions.SetUnique(true));
            //Set up the TTL to remove records older then a specified number of days
            if (Properties.Settings.Default.DaysToRetainWorkflowInstances > 0)
            {
                var keys = IndexKeys.Ascending("expiry");
                var options = IndexOptions.SetTimeToLive(TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainBoxLastProducibles));
                var existing = Collection.GetIndexes().FirstOrDefault(i => i.Name == "expiry_1");
                if (existing != null &&
                    existing.TimeToLive.Ticks != TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainBoxLastProducibles).Ticks)
                {
                    Collection.DropIndexByName("expiry_1");
                }
                Collection.EnsureIndex(keys, options);
            }
        }

        public BoxLastProducible FindByCustomerUniqueId(string customerUniqueId)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.CustomerUniqueId == customerUniqueId);
        }

        public IEnumerable<BoxLastProducible> FindWithStatus(params ProducibleStatuses[] statuses)
        {
            //todo: consider making this async for each status then rejoining the results.
            //todo: test with Query.In
            var lst = Collection.AsQueryable().Where(p => statuses.Contains(p.ProducibleStatus)).ToList();
            return lst;
        }

        public IEnumerable<BoxLastProducible> FindWhereStatusIsNot(params ProducibleStatuses[] statuses)
        {
            //todo: consider making this async for each status then rejoining the results.
            //todo: test with Query.In
            return Collection.AsQueryable().Where(p => !statuses.Contains(p.ProducibleStatus)).ToList();
        }

        public long DeleteCartonsBetweenDates(DateTime startDateTime, DateTime endDateTime)
        {
            var query = Query<BoxLastProducible>.Where(c => 
                c.ProducibleStatus != ProducibleStatuses.ProducibleCompleted && //Don't remove completed
                !(c.ProducibleStatus is InProductionProducibleStatuses) &&  //Don't remove in production
                c.History[0].OccuredOn > startDateTime && c.History[0].OccuredOn < endDateTime);
            return Collection.Remove(query, RemoveFlags.None, WriteConcern.Acknowledged).DocumentsAffected;
        }

        public IEnumerable<BoxLastProducible> GetProductionHistoryForMachineGroup(Guid machineGroupId, int qty = 20)
        {
            var query = Query.And(
                Query.EQ("ProducibleStatus", ProducibleStatuses.ProducibleCompleted.DisplayName),
                Query.EQ("ProducedOnMachineGroupId", machineGroupId)
                );
            
            return Collection.Find(query).SetFields(Fields.Slice("History", -1)).SetSortOrder(new SortByBuilder().Descending("History.OccuredOn")).SetLimit(qty);
        }
    }
}