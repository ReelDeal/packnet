﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data
{
    public class DatabaseTaskRepository : MongoDbRepository<DatabaseTask>, IDatabaseTaskRepository
    {
        public DatabaseTaskRepository() :
            base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "DatabaseTasks")
        {
        }

        public void DropDatabase()
        {
            Database.Drop();
        }
    }
}