﻿using System.Linq;

using MongoDB.Driver.Linq;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;

namespace PackNet.Data.Corrugates
{
    public class CorrugateRepository : MongoDbRepository<Corrugate>, ICorrugateRepository
    {
        private static bool serializerRegistered;
        private static object syncObject = new object();
        public CorrugateRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Corrugates")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<MicroMeter>(new MicroMeterSerializer());
                serializerRegistered = true;
            }
        }
      
        public Corrugate FindByCorrugateAlias(string alias)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.Alias == alias);
        }
    }
}
