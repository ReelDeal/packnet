﻿using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Corrugates
{
    public interface ICorrugateRepository : IRepository<Corrugate>
    {
        Corrugate FindByCorrugateAlias(string alias);
    }
}
