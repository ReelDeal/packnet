﻿using System;

using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Data.Serializers
{
    public class MicroMeterSerializer : IBsonSerializer 
{
        /// <summary>
        /// Deserializes an object from a BsonReader.
        /// </summary>
        /// <param name="bsonReader">The BsonReader.</param><param name="nominalType">The nominal type of the object.</param><param name="options">The serialization options.</param>
        /// <returns>
        /// An object.
        /// </returns>
        public object Deserialize(BsonReader bsonReader, Type nominalType, IBsonSerializationOptions options)
        {
            if (nominalType != typeof(MicroMeter))
                throw new ArgumentException("Cannot serialize anything but MicroMeter");
            var doubleValue = bsonReader.ReadDouble();
            return new MicroMeter(doubleValue);
        }

        /// <summary>
        /// Deserializes an object from a BsonReader.
        /// </summary>
        /// <param name="bsonReader">The BsonReader.</param><param name="nominalType">The nominal type of the object.</param><param name="actualType">The actual type of the object.</param><param name="options">The serialization options.</param>
        /// <returns>
        /// An object.
        /// </returns>
        public object Deserialize(BsonReader bsonReader, Type nominalType, Type actualType, IBsonSerializationOptions options)
        {
            return Deserialize(bsonReader, nominalType, options);
        }

        /// <summary>
        /// Gets the default serialization options for this serializer.
        /// </summary>
        /// <returns>
        /// The default serialization options for this serializer.
        /// </returns>
        public IBsonSerializationOptions GetDefaultSerializationOptions()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Serializes an object to a BsonWriter.
        /// </summary>
        /// <param name="bsonWriter">The BsonWriter.</param><param name="nominalType">The nominal type.</param><param name="value">The object.</param><param name="options">The serialization options.</param>
        public void Serialize(BsonWriter bsonWriter, Type nominalType, object value, IBsonSerializationOptions options)
        {
            if (nominalType != typeof(MicroMeter))
                throw new ArgumentException("Cannot serialize anything but MicroMeter");
            var doubleValue = (double)(MicroMeter)value;
            bsonWriter.WriteDouble(doubleValue);
        }
}
}
