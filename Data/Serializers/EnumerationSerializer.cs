﻿using System;
using System.Linq;

using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.ExtensionMethods;

namespace PackNet.Data.Serializers
{
    
    public class EnumerationSerializer : IBsonSerializer
    {
        private static object syncObject = new object();
       
        /// <summary>
        /// Deserializes an object from a BsonReader.
        /// </summary>
        /// <param name="bsonReader">The BsonReader.</param><param name="nominalType">The nominal type of the object.</param><param name="options">The serialization options.</param>
        /// <returns>
        /// An object.
        /// </returns>
        public object Deserialize(BsonReader bsonReader, Type nominalType, IBsonSerializationOptions options)
        {
            if (!nominalType.IsTypeOrSubclassOf(typeof(Enumeration)))
                throw new ArgumentException("Cannot serialize anything but Enumeration");
            var stringValue = bsonReader.ReadString();
            //Get the exact type
            var returnValue = Enumeration.GetAll().FirstOrDefault(e => e.DisplayName == stringValue && e.GetType() == nominalType);
            if (returnValue == null)
            {
                //Get a derived type
                returnValue = Enumeration.GetAll().FirstOrDefault(e => e.DisplayName == stringValue && (nominalType.IsTypeOrSubclassOf(e.GetType()) || nominalType.IsTypeOrDerivedFrom(e.GetType())));
            }
            return returnValue;
        }

        /// <summary>
        /// Deserializes an object from a BsonReader.
        /// </summary>
        /// <param name="bsonReader">The BsonReader.</param><param name="nominalType">The nominal type of the object.</param><param name="actualType">The actual type of the object.</param><param name="options">The serialization options.</param>
        /// <returns>
        /// An object.
        /// </returns>
        public object Deserialize(BsonReader bsonReader, Type nominalType, Type actualType, IBsonSerializationOptions options)
        {
            return Deserialize(bsonReader, nominalType, options);
        }

        /// <summary>
        /// Gets the default serialization options for this serializer.
        /// </summary>
        /// <returns>
        /// The default serialization options for this serializer.
        /// </returns>
        public IBsonSerializationOptions GetDefaultSerializationOptions()
        {
            return null;
        }

        /// <summary>
        /// Serializes an object to a BsonWriter.
        /// </summary>
        /// <param name="bsonWriter">The BsonWriter.</param><param name="nominalType">The nominal type.</param><param name="value">The object.</param><param name="options">The serialization options.</param>
        public void Serialize(BsonWriter bsonWriter, Type nominalType, object value, IBsonSerializationOptions options)
        {
            if (!value.GetType().IsTypeOrSubclassOf(typeof(Enumeration)))
                throw new ArgumentException("Cannot serialize anything but Enumeration");
            var stringValue = (Enumeration)value;
            bsonWriter.WriteString(stringValue.DisplayName);
        }

    
    }
}
