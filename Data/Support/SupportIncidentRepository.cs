﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.Support
{
    public class SupportIncidentRepository : MongoDbRepository<SupportIncident>, ISupportIncidentRepository
    {
        public SupportIncidentRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "SupportIncident")
        {
        }
    }
}