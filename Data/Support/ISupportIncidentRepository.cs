using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO;

namespace PackNet.Data.Support
{
    public interface ISupportIncidentRepository
    {
        /// <summary>
        /// Serializes a document of type T to the collection specified
        /// </summary>
        /// <param name="objectToCreate"></param>
        void Create(SupportIncident objectToCreate);

        /// <summary>
        /// Creates multiple items
        /// </summary>
        /// <param name="objectsToCreate"></param>
        void Create(IEnumerable<SupportIncident> objectsToCreate);

        /// <summary>
        /// Find all the elements of type T in the database
        /// Returns a cursor of IPersistable objects
        /// </summary>
        /// <returns></returns>
        IEnumerable<SupportIncident> All();

        /// <summary>
        /// Find a single element by the id passed into the method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        SupportIncident Find(Guid id);

        /// <summary>
        /// Update takes an object and performs a collection.save() on that object.
        /// The object will either be updated (if it already exists), or saved (if there is nothing in the id field)
        /// </summary>
        /// <param name="objectToUpdate"></param>
        /// <returns></returns>
        SupportIncident Update(SupportIncident objectToUpdate);

        /// <summary>
        /// Convenience method to update multiple items... NOT efficient (Read foreach) as mongo doesn't provide a bulk update
        /// </summary>
        /// <param name="objectsToUpdate"></param>
        void Update(IEnumerable<SupportIncident> objectsToUpdate);

        /// <summary>
        /// Deletes an IPersistable from the collection.  Note that the Id
        /// field is used to look up the item that needs to be removed.
        /// </summary>
        /// <param name="objectToDelete"></param>
        void Delete(SupportIncident objectToDelete);

        /// <summary>
        /// Delete by id
        /// </summary>
        /// <param name="id"></param>
        void Delete(Guid id);

        /// <summary>
        /// Delete multiple items
        /// </summary>
        /// <param name="objectsToDelete"></param>
        void Delete(IEnumerable<SupportIncident> objectsToDelete);
    }
}