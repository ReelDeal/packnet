﻿using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.PickArea
{
    public interface IPickZoneRepository : IRepository<PickZone>
    {
        PickZone FindByAlias(string alias);
    }
}
