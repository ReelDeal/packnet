﻿using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.PickArea
{
    using System;

    using PackNet.Common.Interfaces.Enums;
    using PackNet.Data.Serializers;

    public class PickZoneRepository : MongoDbRepository<PickZone>, IPickZoneRepository
    {
        private readonly object syncObject = new Object();

        private readonly bool serializerRegistered;

        public PickZoneRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "PickZones")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<PickZoneStatuses>(new EnumerationSerializer());
                serializerRegistered = true;
            }
        }

        public PickZone FindByAlias(string alias)
        {
            return Collection.AsQueryable().FirstOrDefault(pickzone => pickzone.Alias == alias);
        }
    }
}
