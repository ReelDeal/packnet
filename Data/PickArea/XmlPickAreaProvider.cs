﻿namespace PackNet.Data.PickArea
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Common.ComponentModel.Configuration;
    using Common.Interfaces.DTO.PickZones;

    [DataContract(Name = "PickAreaProvider", Namespace = "http://Packsize.MachineManager.com")]
    public class XmlPickAreaProvider : XmlConfiguration<XmlPickAreaProvider>, IPickAreaProvider
    {
        public XmlPickAreaProvider()
        {
            Version = "1.2";
        }

        [DataMember(Name = "PickAreas")]
        public IEnumerable<PickArea> PickAreas { get; private set; }

        [DataMember(Name = "Version")]
        public string Version { get; set; }
    }
}
