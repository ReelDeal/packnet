﻿namespace PackNet.Data.PickArea
{
    using System.Collections.Generic;

    using Common.Interfaces.DTO.PickZones;

    public interface IPickAreaProvider
    {
        IEnumerable<PickArea> PickAreas { get; }
    }
}
