﻿using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.Settings
{
    public class PackNetServerSettingsRepository : MongoDbRepository<PackNetServerSettings>, IPackNetServerSettingsRepository
    {
        public PackNetServerSettingsRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "PackNetServerSettings")
        {
        }
    }
}
