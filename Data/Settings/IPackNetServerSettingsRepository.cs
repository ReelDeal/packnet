using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Settings
{
    public interface IPackNetServerSettingsRepository : IRepository<PackNetServerSettings>
    {
    }
}