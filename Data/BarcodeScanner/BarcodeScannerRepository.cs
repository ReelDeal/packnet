﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Printing;
using PackNet.Data.Repositories.MongoDB;
using System.Linq;
using MongoDB.Driver.Linq;

namespace PackNet.Data.BarcodeScanner
{
    public class BarcodeScannerRepository : MongoDbRepository<BarcodeScannerMachine>, IBarcodeScannerRepository 
    {
        public BarcodeScannerRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "BarcodeScanners")
        {
        }
        public BarcodeScannerMachine Find(string name)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.Alias == name);
        }
    }
}
