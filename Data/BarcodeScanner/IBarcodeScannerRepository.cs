﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Repositories;
namespace PackNet.Data.BarcodeScanner
{
    public interface IBarcodeScannerRepository : IRepository<BarcodeScannerMachine>
    {
         
    }
}