﻿using System.Linq;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.CartonPropertyGroups
{
    using System;

    using PackNet.Common.Interfaces.Enums;
    using PackNet.Data.Serializers;

    public class CartonPropertyGroupRepository : MongoDbRepository<CartonPropertyGroup>, ICartonPropertyGroupRepository
    {
        private readonly object syncObject = new Object();

        private readonly bool serializerRegistered;

        public CartonPropertyGroupRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "CartonPropertyGroups")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<CartonPropertyGroupStatuses>(new EnumerationSerializer());
                serializerRegistered = true;
            }
        }

        public CartonPropertyGroup FindByCpgAlias(string alias)
        {
            return All().FirstOrDefault(cpg => cpg.Alias.Equals(alias));
        }
    }
}
