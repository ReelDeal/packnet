﻿using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.CartonPropertyGroups
{
    public interface ICartonPropertyGroupRepository : IRepository<CartonPropertyGroup>
    {
        CartonPropertyGroup FindByCpgAlias(string alias);
    }
}
