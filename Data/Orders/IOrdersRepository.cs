﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Orders
{
    public interface IOrdersRepository : IRepository<Order>
    {
        IEnumerable<Order> GetOpenOrders(int qty = 20);
        IEnumerable<Order> GetAllNotProducibleOrders();
        IEnumerable<Order> GetCompletedOrders(int qty = 20);
        IEnumerable<Order> GetCompletedOrdersForRestrictionId(Guid mgId, int qty = 20);
        IEnumerable<Order> GetOpenOrdersForRestrictionId(Guid mgId, int qty = 20);

        IEnumerable<Order> GetAllOpenOrders();
        Order Find(string customerUniqueId);
        Order UpdateWithRetries(Order objectToUpdate, int maxTries = 50);
    }
}