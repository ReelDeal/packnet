﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver.Builders;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.Orders
{
    using MongoDB.Driver.Linq;

    public class OrdersRepository : MongoDbRepository<Order>, IOrdersRepository
    {
        private readonly ILogger logger;

        public OrdersRepository()
            : this(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Orders")
        {

        }

        public OrdersRepository(IServiceLocator serviceLocator)
            : this(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Orders")
        {
            logger = serviceLocator.Locate<ILogger>();
        }

        protected OrdersRepository(string connectionString, string dbName, string collection)
            :base(connectionString, dbName, collection)
        {
            var keys = IndexKeys.Ascending("ProductionSequence");
            var options = IndexOptions.SetTimeToLive(TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainOrdersProducibles));
            var existing = Collection.GetIndexes().FirstOrDefault(i => i.Name == "ProductionSequence");
            if (existing != null)
            {
                Collection.DropIndexByName("ProductionSequence");
            }
            Collection.EnsureIndex(keys, options);
          
            //Set up the TTL to remove records older then a specified number of days
            if (Properties.Settings.Default.DaysToRetainWorkflowInstances > 0)
            {
                keys = IndexKeys.Ascending("expiry");
                options = IndexOptions.SetTimeToLive(TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainOrdersProducibles));
                existing = Collection.GetIndexes().FirstOrDefault(i => i.Name == "expiry_1");
                if (existing != null &&
                    existing.TimeToLive.Ticks != TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainOrdersProducibles).Ticks)
                {
                    Collection.DropIndexByName("expiry_1");
                }
                Collection.EnsureIndex(keys, options);
           }

            RegisterClassMaps();
        }

        private void RegisterClassMaps()
        {
            try
            {
                BsonClassMap.RegisterClassMap<TiledCarton>(cm => cm.AutoMap());                
            }
            catch (Exception)
            {
                // we don't care that it's already been registered...
            }
        }

        public IEnumerable<Order> GetOpenOrders(int qty=20)
        {
            var query = Query.NotIn("ProducibleStatus", new List<BsonValue> { 
                ProducibleStatuses.ProducibleRemoved.DisplayName,
                ProducibleStatuses.ProducibleCompleted.DisplayName,
                ErrorProducibleStatuses.NotProducible.DisplayName
            });
            return Collection.Find(query).SetSortOrder(new SortByBuilder().Ascending("ProductionSequence")).SetLimit(qty);
        }

        public IEnumerable<Order> GetOpenOrdersForRestrictionId(Guid mgId, int qty = 20)
        {
            var restrictionIdList = new List<Guid> { mgId };

            var query =  Query.And(Query.NotIn("ProducibleStatus", new List<BsonValue> { 
                ProducibleStatuses.ProducibleRemoved.DisplayName,
                ProducibleStatuses.ProducibleCompleted.DisplayName,
                ErrorProducibleStatuses.NotProducible.DisplayName
            }), Query.In("Restrictions.Value", BsonArray.Create(restrictionIdList)));

            return Collection.Find(query).SetSortOrder(new SortByBuilder().Ascending("ProductionSequence")).SetLimit(qty);
        }

        public IEnumerable<Order> GetAllOpenOrders()
        {
            var query = Query.NotIn("ProducibleStatus", new List<BsonValue> { 
                ProducibleStatuses.ProducibleRemoved.DisplayName,
                ProducibleStatuses.ProducibleCompleted.DisplayName
            });
            return Collection.Find(query).SetSortOrder(new SortByBuilder().Ascending("ProductionSequence"));
        }

        public IEnumerable<Order> GetCompletedOrders(int qty=20)
        {
            var query = Query.In("ProducibleStatus", new List<BsonValue> { 
                ProducibleStatuses.ProducibleRemoved.DisplayName,
                ProducibleStatuses.ProducibleCompleted.DisplayName,
                ErrorProducibleStatuses.NotProducible.DisplayName
            });
            return Collection.Find(query).SetSortOrder(new SortByBuilder().Descending("StatusChangedDateTime")).SetLimit(qty);
        }

        public IEnumerable<Order> GetCompletedOrdersForRestrictionId(Guid mgId, int qty = 20)
        {
            // Get the stuff for machine group
            var restrictionIdList = new List<Guid> { mgId };

            var query = Query.And(
                Query.In(
                    "ProducibleStatus",
                    new List<BsonValue>
                    {
                        ProducibleStatuses.ProducibleRemoved.DisplayName,
                        ProducibleStatuses.ProducibleCompleted.DisplayName,
                        ErrorProducibleStatuses.NotProducible.DisplayName
                    }),
                    Query.In("Restrictions.Value", BsonArray.Create(restrictionIdList)));
            return Collection.Find(query).SetSortOrder(new SortByBuilder().Descending("StatusChangedDateTime")).SetLimit(qty);
        }

        public IEnumerable<Order> GetAllNotProducibleOrders()
        {
            var query = Query.In("ProducibleStatus", new List<BsonValue> {
                ErrorProducibleStatuses.NotProducible.DisplayName
            });
            return Collection.Find(query);
        }

        public Order Find(string customerUniqueId)
        {
            var query = Query<Order>.EQ(e => e.CustomerUniqueId, customerUniqueId);
            Collection.Find(query);
            var item = Collection.Find(query).FirstOrDefault(); //Collection.AsQueryable().FirstOrDefault(i => i.Id == id);
            return item;
        }

        /// <summary>
        /// This method was introduced to partially address TFS 10256. I explicitily _do not_ call Retry because I want to get a log of how long it took and how many times we
        /// had to try. NOTE: If this fails past the maxTries amount of times you will end up getting your original input back as your result.
        /// </summary>
        /// <param name="objectToUpdate"></param>
        /// <param name="maxTries"></param>
        /// <returns></returns>
        public Order UpdateWithRetries(Order objectToUpdate, int maxTries)
        {
            //TODO: Remove this
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool saved = false;
            int tries = 0;
            //NOTE: If this fails past the maxTries amount of times you will end up getting your original input back as your result.
            Order orderSaveResult = objectToUpdate;
            
            //Please do not change this to a Retry
            while (!saved && tries < maxTries)
            {
                try
                {
                    tries++;
                    orderSaveResult = Update(objectToUpdate);
                    saved = true;
                }
                catch (InvalidOperationException)
                {
                    saved = false;
                }
                catch (AggregateException)
                {
                    saved = false;
                }
            }

            string message = string.Format("Took {0} ms and {1} tries for db Update to finally stick for {2}", stopwatch.ElapsedMilliseconds, tries, orderSaveResult);
            Trace.WriteLine(message);
            
            //Hack so that the TestDatabaseManagement class does not break
            if (logger != null)
            {
                logger.Log(LogLevel.Debug, message);
            }

            return orderSaveResult;
        }
    }
}
