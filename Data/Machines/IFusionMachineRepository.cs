﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Machines
{
    public interface IFusionMachineRepository : IRepository<FusionMachine>
    {
    }
}