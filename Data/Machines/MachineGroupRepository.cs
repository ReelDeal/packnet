﻿using System;
using System.Linq;

using MongoDB.Bson.Serialization;
using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.Machines
{
    public class MachineGroupRepository : MongoDbRepository<MachineGroup>, IMachineGroupRepository
    {
        public MachineGroupRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "MachineGroups")
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(AllRequiredRestriction)))
                BsonClassMap.RegisterClassMap<AllRequiredRestriction>();
            if (!BsonClassMap.IsClassMapRegistered(typeof(AnyRequiredRestriction)))
                BsonClassMap.RegisterClassMap<AnyRequiredRestriction>();
            if (!BsonClassMap.IsClassMapRegistered(typeof(AggregateRestriction)))
                BsonClassMap.RegisterClassMap<AggregateRestriction>();
        }

        public MachineGroup FindByMachineId(Guid id)
        {
            return Collection.AsQueryable().FirstOrDefault(mg => mg.ConfiguredMachines.Any(mId => mId == id));
        }
        
        public MachineGroup FindByAlias(string alias)
        {
            return Collection.AsQueryable().FirstOrDefault(mg => mg.Alias.Equals(alias));
   
        }
    }
    public static class Extensions
    {
        public static Guid SetId(this MachineGroup mg)
        {
            var id = new Guid();
            var dbMg = new MachineGroupRepository().FindByAlias(mg.Alias);
            if (dbMg != null)
            {
                id = dbMg.Id;
                mg.Id = id;
            }
            return id;
        }
    }
}
