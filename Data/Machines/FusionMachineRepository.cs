﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using System.Linq;
using MongoDB.Driver.Linq;
namespace PackNet.Data.Machines
{
    public class FusionMachineRepository : MongoDbRepository<FusionMachine>, IFusionMachineRepository
    {

        public FusionMachineRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "FusionMachines")
        {
            MongoDbHelpers.TryRegisterSerializer<MicroMeter>(new MicroMeterSerializer());
        }
        public FusionMachine Find(string name)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.Alias == name);
        }
    }
}
