﻿using System;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Machines
{
    public interface IMachineGroupRepository : IRepository<MachineGroup>
    {
        MachineGroup FindByMachineId(Guid id);

        MachineGroup FindByAlias(string alias);
    }
}