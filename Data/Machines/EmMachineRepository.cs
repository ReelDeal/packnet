﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using System.Linq;
using MongoDB.Driver.Linq;

namespace PackNet.Data.Machines
{
    public class EmMachineRepository : MongoDbRepository<EmMachine>, IEmMachineRepository
    {
        private object syncObject = new object();
        private bool serializerRegistered;

        public EmMachineRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "EmMachines")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<MicroMeter>(new MicroMeterSerializer());
                serializerRegistered = true;
            }
        }
        public EmMachine Find(string name)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.Alias == name);
        }
    }
}
