﻿using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;

namespace PackNet.Data.ComponentsConfigurations
{
    public class ComponentsConfigurationRepository : MongoDbRepository<ComponentsConfiguration>, IComponentsConfigurationRepository
    {
        private static bool serializerRegistered;
        private static readonly object syncObject = new object();

        public ComponentsConfigurationRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "ComponentsConfiguration")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<MicroMeter>(new MicroMeterSerializer());
                serializerRegistered = true;
            }

            if (All().FirstOrDefault() == null)
                AddDefaultSettings();
        }

        private void AddDefaultSettings()
        {
            Create(new ComponentsConfiguration()
            {
                CodeGeneratorSettings = new CodeGeneratorSettings()
                {
                    MinimumLineDistanceToCorrugateEdge = 0.2,
                    PerforationSafetyDistance = 2
                }
            });
        }
    }
}
