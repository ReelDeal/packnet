﻿using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.ComponentsConfigurations
{
    public interface IComponentsConfigurationRepository : IRepository<ComponentsConfiguration>
    {
    }
}
