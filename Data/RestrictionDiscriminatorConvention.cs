﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Conventions;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Data
{
    /// <summary>
    /// 
    /// </summary>
    /// <example>
#pragma warning disable 1570
    /// BsonSerializer.RegisterDiscriminatorConvention(typeof(BasicRestriction<YouTypeHere>), new RestrictionDiscriminatorConvention());//You should test to ensure that you type can be serialized to the repository or if you need to register a 
#pragma warning restore 1570
    /// 
    /// </example>
    public class RestrictionDiscriminatorConvention : IDiscriminatorConvention
    {
        public string ElementName
        {
            get { return "_t"; }
        }

        public Type GetActualType(BsonReader bsonReader, Type nominalType)
        {
            if (nominalType != typeof(IRestriction) && nominalType != typeof(List<IRestriction>))
                throw new Exception("Cannot use RestrictionDiscriminatorConvention for type " + nominalType);

            var ret = nominalType;

            var bookmark = bsonReader.GetBookmark();
            bsonReader.ReadStartDocument();
            if (bsonReader.FindElement(ElementName))
            {
                var value = bsonReader.ReadString();

                ret = Type.GetType(value);

                if (ret == null)
                    throw new Exception("Could not find type from " + value);

                if (!typeof(IRestriction).IsAssignableFrom(ret) && !ret.IsSubclassOf(typeof(IRestriction)))
                    throw new Exception("type is not an IRestriction");
            }

            bsonReader.ReturnToBookmark(bookmark);

            return ret;
        }

        public BsonValue GetDiscriminator(Type nominalType, Type actualType)
        {
            if (nominalType != typeof(IRestriction) && nominalType != typeof(List<IRestriction>))
                throw new Exception("Cannot use RestrictionDiscriminatorConvention for type " + nominalType);

            return actualType.AssemblyQualifiedName;
        }
    }
}
