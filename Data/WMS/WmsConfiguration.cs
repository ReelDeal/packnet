﻿using System.Runtime.Serialization;
using PackNet.Common.ComponentModel.Configuration;

namespace PackNet.Data.WMS
{
    [DataContract(Name = "WmsConfiguration", Namespace = "http://Packsize.MachineManager.com")]
    public class WmsConfiguration : XmlConfiguration<WmsConfiguration>
    {
        public WmsConfiguration()
        {
            Enabled = false;
            ExternalPrint = false;
            PrinterAlertPort = 9101;
            PrinterPollingTimes = 3;
            PrinterPollingTimeout = 500;
            ReportProduced = true;
            SequenceNumberLowerBound = 1;
            SequenceNumberUpperBound = 99999;
            UseSocketCommunication = false;
            Version = "1.2";
        }

        [DataMember(Name = "Enabled")]
        public bool Enabled { get; set; }

        [DataMember(Name = "ExternalPrint")]
        public bool ExternalPrint { get; set; }

        [DataMember(Name = "PrinterAlertPort")]
        public int PrinterAlertPort { get; set; }

        [DataMember(Name = "PrinterPollingTimeout")]
        public int PrinterPollingTimeout { get; set; }
        
        [DataMember(Name = "PrinterPollingTimes")]
        public int PrinterPollingTimes { get; set; }

        [DataMember(Name = "ReportProduced")]
        public bool ReportProduced { get; set; }

        [DataMember(Name = "SequenceNumberLowerBound")]
        public int SequenceNumberLowerBound { get; set; }

        [DataMember(Name = "SequenceNumberUpperBound")]
        public int SequenceNumberUpperBound { get; set; }

        [DataMember(Name = "UseSocketCommunication")]
        public bool UseSocketCommunication { get; set; }

        [DataMember(Name = "Version")]
        public string Version { get; set; }
    }
}
