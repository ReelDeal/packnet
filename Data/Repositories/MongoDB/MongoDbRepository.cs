﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.Repositories.MongoDB
{
    /// <summary>
    /// Implementation of the repository pattern with a mongodb backend
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MongoDbRepository<T> : IRepository<T> where T : class, IPersistable
    {
        protected readonly MongoCollection<T> Collection;
        protected readonly MongoDatabase Database;

        /// <summary>
        /// Provides simple implementations for basic CRUD operations on MongoDB
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="databaseName"></param>
        /// <param name="collectionName"></param>
        public MongoDbRepository(string connectionString, string databaseName, string collectionName)
        {
            // Connections are pooled by the MongoDB driver
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            Database = server.GetDatabase(databaseName);
            Collection = Database.GetCollection<T>(collectionName);
            MongoDbHelpers.RegisterIgnoreIfNullConvention();
            MongoDbHelpers.RegisterEnumerationSerializers();
        }

        /// <summary>
        /// Serializes a document of type T to the collection specified
        /// </summary>
        /// <param name="objectToCreate"></param>
        public virtual void Create(T objectToCreate)
        {
            var retry = Properties.Settings.Default.DBWriteRetry;
            while (retry >= 0)
            {
                try
                {
                    retry--;
                    var sw = Stopwatch.StartNew();
                    if (objectToCreate.Id == Guid.Empty)
                        objectToCreate.Id = Guid.NewGuid();

                    Collection.Save(objectToCreate, WriteConcern.Acknowledged);
                    Trace.WriteLine(String.Format("Took {0} ms for db insert {1}", sw.ElapsedMilliseconds, typeof(T)));
                    break;
                }
                catch (Exception e)
                {
                    Thread.Sleep(10);
                    Trace.WriteLine(String.Format("MONGO INSERT failed on retry {0} {1} FROM {2}", retry, e, Environment.StackTrace));
                    if (retry <= 0)
                        throw;
                }
            }
        }

        /// <summary>
        /// Serializes a document of type T to the collection specified
        /// </summary>
        /// <param name="objectToCreate"></param>
        public virtual void CreateEnsureNoDuplicate(T objectToCreate)
        {
            var retry = Properties.Settings.Default.DBWriteRetry;
            while (retry >= 0)
            {
                try
                {
                    retry--;
                    var sw = Stopwatch.StartNew();
                    if (objectToCreate.Id == Guid.Empty)
                        objectToCreate.Id = Guid.NewGuid();

                    Collection.Insert(objectToCreate, WriteConcern.Acknowledged);
                    Trace.WriteLine(String.Format("Took {0} ms for db insert {1}", sw.ElapsedMilliseconds, typeof(T)));
                    break;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(String.Format("MONGO INSERT failed on retry {0} {1} FROM {2}", retry, e, Environment.StackTrace));
                    Thread.Sleep(10);
                    if (retry <= 0)
                        throw;
                }
            }
        }

        /// <summary>
        /// Creates multiple items
        /// </summary>
        /// <param name="objectsToCreate"></param>
        public virtual void Create(IEnumerable<T> objectsToCreate)
        {
            if (!objectsToCreate.Any())
                return;
            //TODO: Jeff - This is throwing duplicate key exceptions, only part of the collection gets persisted
            //var retry = 25;
            //while (retry >= 0)
            //{
            //    try
            //    {
            //        retry--;
                    var sw = Stopwatch.StartNew();
                    Collection.InsertBatch(objectsToCreate, WriteConcern.Acknowledged);
                    Trace.WriteLine(String.Format("Took {0} ms for batch db insert {1}", sw.ElapsedMilliseconds, typeof(T)));
            //    }
            //    catch (Exception e)
            //    {
            //        Trace.WriteLine(String.Format("MONGO INSERTBATCH failed on retry {0} {1} FROM {2}", retry, e, Environment.StackTrace));
            //        if (retry <= 0)
            //            throw;
            //    }
            //}
        }

        /// <summary>
        /// Find all the elements of type T in the database
        /// Returns a cursor of IPersistable objects
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<T> All()
        {
            var sw = Stopwatch.StartNew();
            var all = Collection.FindAll().ToList();
            Trace.WriteLine(String.Format("Took {0} ms to retrieve all records {1}", sw.ElapsedMilliseconds, typeof(T)));
            return all;
        }

        /// <summary>
        /// return the number of items in the collection
        /// </summary>
        public virtual long Count()
        {
            return Collection.Count();
        }

        /// <summary>
        /// Find a single element by the id passed into the method
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T Find(Guid id)
        {
            var sw = Stopwatch.StartNew();
            var query = Query<T>.EQ(e => e.Id, id);
            var item = Collection.Find(query).FirstOrDefault();
            Trace.WriteLine(String.Format("Took {0} ms for db to find item by id {1}", sw.ElapsedMilliseconds, typeof(T)));
            return item;
        }

        /// <summary>
        /// Find objects in the repository by Ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> Find(IEnumerable<Guid> ids)
        {
            var sw = Stopwatch.StartNew();
            var query = Query<T>.In(e => e.Id, ids);
            var item = Collection.Find(query);
            Trace.WriteLine(String.Format("Took {0} ms for db find multiple items by id {1}", sw.ElapsedMilliseconds, typeof(T)));
            return item;
        }

        /// <summary>
        /// Update takes an object and performs a collection.save() on that object.
        /// The object will either be updated (if it already exists), or saved (if there is nothing in the id field)
        /// </summary>
        /// <param name="objectToUpdate"></param>
        /// <returns></returns>
        public virtual T Update(T objectToUpdate)
        {
            var retry = Properties.Settings.Default.DBWriteRetry;
            while (retry >= 0)
            {
                try
                {
                    retry--;
                    var sw = Stopwatch.StartNew();
                    Collection.Save(objectToUpdate, WriteConcern.Acknowledged);
                    Trace.WriteLine(String.Format("Took {0} ms for db Update {1}", sw.ElapsedMilliseconds, typeof(T)));
                    break;
                }
                catch (Exception e)
                {
                    Trace.WriteLine(String.Format("MONGO SAVE failed on retry {0} {1} FROM {2}", retry, e, Environment.StackTrace));
                    Thread.Sleep(10);
                    if (retry <= 0)
                        throw;
                }
            }
            return objectToUpdate;
        }

        /// <summary>
        /// Convenience method to update multiple items... NOT efficient (Read foreach) as mongo doesn't provide a bulk update
        /// </summary>
        /// <param name="objectsToUpdate"></param>
        public virtual void Update(IEnumerable<T> objectsToUpdate)
        {
            var sw = Stopwatch.StartNew();
            foreach (var objectToUpdate in objectsToUpdate)
            {
                Update(objectToUpdate);
            }
            sw.Stop();
            Trace.WriteLine(String.Format("Took {0} ms for multiple db update {1}", sw.ElapsedMilliseconds, typeof(T)));
        }

        /// <summary>
        /// Deletes ALL
        /// </summary>
        public virtual void DeleteAll()
        {
            var sw = Stopwatch.StartNew();
            Collection.RemoveAll();
            Trace.WriteLine(String.Format("Took {0} ms for db remove all {1}", sw.ElapsedMilliseconds, typeof(T)));
        }

        /// <summary>
        /// Deletes an IPersistable from the collection.  Note that the Id
        /// field is used to look up the item that needs to be removed.
        /// </summary>
        /// <param name="objectToDelete"></param>
        public virtual void Delete(T objectToDelete)
        {
            Delete(objectToDelete.Id);
        }

        /// <summary>
        /// Delete by id
        /// </summary>
        /// <param name="id"></param>
        public virtual void Delete(Guid id)
        {
            var sw = Stopwatch.StartNew();
            var query = Query<T>.EQ(e => e.Id, id);
            Collection.Remove(query, RemoveFlags.Single, WriteConcern.Acknowledged);
            Trace.WriteLine(String.Format("Took {0} ms for db delete {1}", sw.ElapsedMilliseconds, typeof(T)));
        }

        /// <summary>
        /// Delete multiple items
        /// </summary>
        /// <param name="objectsToDelete"></param>
        public virtual void Delete(IEnumerable<T> objectsToDelete)
        {
            var sw = Stopwatch.StartNew();
            var ids = objectsToDelete.Select(i => i.Id);
            var query = Query<T>.Where(i => i.Id.In(ids));
            Collection.Remove(query, WriteConcern.Acknowledged);
            Trace.WriteLine(String.Format("Took {0} ms for multiple db delete {1}", sw.ElapsedMilliseconds, typeof(T)));
        }

        /// <summary>
        /// Delete multiple items
        /// </summary>
        public virtual void Delete(IEnumerable<Guid> idsToDelete)
        {
            var sw = Stopwatch.StartNew();
            var query = Query<T>.Where(i => i.Id.In(idsToDelete));
            Collection.Remove(query, WriteConcern.Acknowledged);
            Trace.WriteLine(String.Format("Took {0} ms for multiple db delete {1}", sw.ElapsedMilliseconds, typeof(T)));
        }

        public virtual IEnumerable<T> Search(string critera, int limit = -1)
        {
            var sw = Stopwatch.StartNew();
            var query = new JSQueryDocument(critera);
            var cursor = Collection.Find(query);
            if (limit > 0)
            {
                cursor.Limit = limit;
            }
            var result = cursor.ToList();
            Trace.WriteLine(String.Format("Took {0} ms for db search {1}", sw.ElapsedMilliseconds, typeof(T)));
            return result;
        }
    }

    public class JSQueryDocument : QueryDocument
    {
        public JSQueryDocument(string query)
            : base(BsonSerializer.Deserialize<BsonDocument>(query))
        {
            // Probably better to do this as a method rather than constructor as it
            // could be hard to debug queries that are not formatted correctly
        }
    }
}