﻿using System;
using System.Linq;

using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Data.Serializers;

namespace PackNet.Data.Repositories.MongoDB
{
    public static class MongoDbHelpers
    {
        private static bool serializerRegistered = false;
        private static bool ignoreIfNullConventionRegistered = false;

        static MongoDbHelpers()
        {
            RegisterIgnoreIfNullConvention();
            RegisterEnumerationSerializers();
        }

        public static void RegisterIgnoreIfNullConvention()
        {
            if (ignoreIfNullConventionRegistered)
                return;
            ignoreIfNullConventionRegistered = true;
            var conventionPack = new ConventionPack
                                 {
                                     new IgnoreIfNullConvention(true)
                                 };

            ConventionRegistry.Register("Custom Conventions", conventionPack, t => true);
        }

        public static void RegisterEnumerationSerializers()
        {
            if (serializerRegistered)
            {
                return;
            }

            serializerRegistered = true;
            //The serialization needs all derived classes primed
            var enumSerializer = new EnumerationSerializer();
            var subclasses =
                from assembly in AppDomain.CurrentDomain.GetAssemblies()
                from type in assembly.GetTypes()
                where type.IsSubclassOf(typeof(Enumeration))
                select type;
            subclasses.ForEach(t => MongoDbHelpers.TryRegisterSerializer(t, enumSerializer));
        }
        /// <summary>
        /// Helper function that will swallow all exceptions and return a bool for success/failure
        /// </summary>
        /// <typeparam name="TSer"></typeparam>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public static bool TryRegisterSerializer<TSer>(IBsonSerializer serializer)
        {
            try
            {
                BsonSerializer.RegisterSerializer(typeof(TSer), serializer);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Helper function that will swallow all exceptions and return a bool for success/failure
        /// </summary>
        /// <param name="type"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public static bool TryRegisterSerializer(Type type, IBsonSerializer serializer)
        {
            try
            {
                BsonSerializer.RegisterSerializer(type, serializer);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Helper function that will swallow Bsons exception that the type has already been registered.
        /// </summary>
        /// <typeparam name="TSer"></typeparam>
        /// <param name="discriminatorConvention"></param>
        /// <returns></returns>
        public static bool TryRegisterDiscriminatorConvention<TSer>(IDiscriminatorConvention discriminatorConvention)
        {
            try
            {
                BsonSerializer.RegisterDiscriminatorConvention(typeof(TSer), discriminatorConvention);
                return true;
            }
            catch (Exception)
            {
                //swallow because we don't care if it's already been registered.
                return false;
            }
        }

        /// <summary>
        /// Register a class map if it is not yet registered 
        /// </summary>
        /// <typeparam name="TSer"></typeparam>
        public static void TryRegisterClassMap<TSer>()
        {
            if (!BsonClassMap.IsClassMapRegistered(typeof(TSer)))
                BsonClassMap.RegisterClassMap<TSer>();
        }

        /// <summary>
        /// Uses BSON to create a deep copy of an IPersistable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToCopy"></param>
        /// <returns></returns>
        public static T DeepCopy<T>(T objectToCopy) where T : IPersistable
        {

            var byteArray = new Byte[0];
            using (var buffer = new BsonBuffer())
            {
                using (var bsonWriter = BsonWriter.Create(buffer, new BsonBinaryWriterSettings()))
                {
                    BsonSerializer.Serialize(bsonWriter, typeof(T), objectToCopy);
                }
                byteArray = buffer.ToByteArray();
            }
            var result = BsonSerializer.Deserialize<T>(byteArray);
            return result;
        }
    }
}