namespace PackNet.Data
{
    public interface IDatabaseTaskRepository
    {
        void DropDatabase();
    }
}