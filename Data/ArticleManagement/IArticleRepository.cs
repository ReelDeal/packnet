using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.ArticleManagement
{
    using Common.Interfaces.DTO;

    public interface IArticleRepository : IRepository<Article>
    {
        Article Find(string articleId);
    }
}