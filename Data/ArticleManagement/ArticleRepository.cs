﻿namespace PackNet.Data.ArticleManagement
{
    using System.Linq;

    using MongoDB.Driver.Linq;                    

    using Common.Interfaces.DTO;
    using Repositories.MongoDB;

    public class ArticleRepository : MongoDbRepository<Article>, IArticleRepository
    {
        public ArticleRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Articles")
        {
        }

        public Article Find(string articleId)
        {
            return Collection.AsQueryable().FirstOrDefault(a => a.ArticleId == articleId);
        }
    }
}
