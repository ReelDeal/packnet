﻿using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.DTO.ExternalSystems;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.ExternalSystems
{
    public class ExternalCustomerSystemRepository : MongoDbRepository<ExternalCustomerSystem>, IExternalCustomerSystemRepository 
    {
        public ExternalCustomerSystemRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "ExternalCustomerSystems")
        {
        }

        public ExternalCustomerSystem Find(string externalCustomerSystemName)
        {
            return Collection.AsQueryable().FirstOrDefault(e => e.Alias == externalCustomerSystemName);
        }
    }
}
