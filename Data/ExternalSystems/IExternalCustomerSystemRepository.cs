﻿using PackNet.Common.Interfaces.DTO.ExternalSystems;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.ExternalSystems
{
    public interface IExternalCustomerSystemRepository : IRepository<ExternalCustomerSystem>
    {
        ExternalCustomerSystem Find(string externalCustomerSystemName);
    }
}