﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.BoxFirstProducible
{
    public class BoxFirstRepository : MongoDbRepository<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible>, IBoxFirstRepository
    {
        public BoxFirstRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "BoxFirstProducible")
        {
            //Set up the TTL to remove records older then a specified number of days
            if (Properties.Settings.Default.DaysToRetainWorkflowInstances > 0)
            {
                var keys = IndexKeys.Ascending("expiry");
                var options = IndexOptions.SetTimeToLive(TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainBoxFirstProducibles));
                var existing = Collection.GetIndexes().FirstOrDefault(i => i.Name == "expiry_1");
                if (existing != null &&
                    existing.TimeToLive.Ticks != TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainBoxFirstProducibles).Ticks)
                {
                    Collection.DropIndexByName("expiry_1");
                }
                Collection.EnsureIndex(keys, options);
        }
        }

        public IEnumerable<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible> GetNonCompletedCartons()
        {
            return Collection.AsQueryable().Where(p => p.ProducibleStatus != ProducibleStatuses.ProducibleCompleted);
        }

        public Common.Interfaces.DTO.BoxFirst.BoxFirstProducible FindByCustomerUniqueId(string customerUniqueId)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.CustomerUniqueId == customerUniqueId);
        }

        /// <summary>
        /// Find objects in the repository by Ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public IEnumerable<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible> FindByCustomerUniqueId(IEnumerable<string> ids)
        {
            var query = Query<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible>.In(e => e.CustomerUniqueId, ids);
            var item = Collection.Find(query);
            return item;
        }

        public IEnumerable<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible> GetProductionHistoryForMachineGroup(Guid machineGroupId, int qty = 20)
        {
            var query = Query.And( 
                Query.EQ("ProducibleStatus", ProducibleStatuses.ProducibleCompleted.DisplayName ),
                Query.EQ("ProducedOnMachineGroupId", machineGroupId)
            );

            return Collection.Find(query).SetFields(Fields.Slice("History", -1)).SetSortOrder(new SortByBuilder().Descending("History.OccuredOn")).SetLimit(qty);
        } 
    }
}
