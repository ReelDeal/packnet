﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.BoxFirstProducible
{
    public interface IBoxFirstRepository : IRepository<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible>
    {
        Common.Interfaces.DTO.BoxFirst.BoxFirstProducible FindByCustomerUniqueId(string customerUniqueId);

        IEnumerable<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible> FindByCustomerUniqueId(IEnumerable<string> ids);

        IEnumerable<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible> GetNonCompletedCartons();

        IEnumerable<Common.Interfaces.DTO.BoxFirst.BoxFirstProducible> GetProductionHistoryForMachineGroup(Guid machineGroupId, int qtry = 20);
    }
}
