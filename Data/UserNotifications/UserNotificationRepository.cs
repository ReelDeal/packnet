﻿using System;
using System.Collections.Generic;
using System.Linq;

using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.Data.UserNotifications
{
	public class UserNotificationRepository : MongoDbRepository<UserNotification>, IUserNotificationRepository
	{
		public UserNotificationRepository()
			: base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "UserNotifications")
		{
            //Set up the TTL to remove records older then a specified number of days
            if (Properties.Settings.Default.DaysToRetainWorkflowInstances > 0)
            {
                var keys = IndexKeys.Ascending("expiry");
                var options = IndexOptions.SetTimeToLive(TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainNotifications));
                var existing = Collection.GetIndexes().FirstOrDefault(i => i.Name == "expiry_1");
                var newTTL = TimeSpan.FromDays(Properties.Settings.Default.DaysToRetainNotifications);
                if (existing != null &&
                    existing.TimeToLive.Ticks != newTTL.Ticks)
                {
                    Collection.DropIndexByName("expiry_1");
                }
                Collection.EnsureIndex(keys, options);
            }
		}

		/// <summary>
		/// Dismiss all notifications of type "System" for the specified user.
		/// </summary>
		/// <param name="userId">User identification.</param>
		public void DismissAllByUser(Guid userId)
		{
			DismissAllByUser(new Guid(), userId);
		}

		/// <summary>
		/// Dismiss all notifications sent to the specified machine group for the specified user.
		/// </summary>
		/// <param name="machineGroupId">Machine Group identification.</param>
		/// <param name="userId">User identification.</param>
		public void DismissAllByUser(Guid machineGroupId, Guid userId)
		{
			var query = Query<UserNotification>
				.Where(u => u.NotifiedUsers.Contains(userId)
					&& !u.DismissedByUsers.Contains(userId)
					&& (u.NotifiedMachineGroups.Count == 0 || u.NotifiedMachineGroups.Contains(machineGroupId)));

			var push = Update<UserNotification>.Push(u => u.DismissedByUsers, userId);

			Collection.Update(query, push, UpdateFlags.Multi);
		}

		/// <summary>
		/// Dismiss the specified user notification.
		/// </summary>
		/// <param name="userNotificationId">User notification identification.</param>
		/// <param name="userId">User identification.</param>
		public void DismissByUser(Guid userNotificationId, Guid userId)
		{
			var query = Query<UserNotification>.Where(u => u.Id == userNotificationId && !u.DismissedByUsers.Contains(userId));
			var push = Update<UserNotification>.Push(u => u.DismissedByUsers, userId);

			Collection.Update(query, push);
		}

		/// <summary>
		/// Find all notifications for the specified user.
		/// </summary>
		/// <param name="userId">User identification.</param>
		/// <returns></returns>
		public IEnumerable<UserNotification> FindByUserId(Guid userId)
		{
			return Collection.AsQueryable()
				.Where(u => u.NotifiedUsers.Contains(userId))
				.OrderBy(u => u.CreatedDate)
				.ToList();
		}

		/// <summary>
		/// Find all active notifications between the 24 hour limit for the specified user.
		/// </summary>
		/// <param name="userId">User identification.</param>
		/// <returns></returns>
		public IEnumerable<UserNotification> FindActiveByUserId(Guid userId)
		{
			var dateTo = DateTime.UtcNow;
			var dateFrom = dateTo.AddDays(-1);
		    var takeLimit = 50;//bug:9371 fix

			return Collection.AsQueryable()
				.Where(u => u.NotifiedUsers.Contains(userId) && !u.DismissedByUsers.Contains(userId) && u.CreatedDate > dateFrom && u.CreatedDate <= dateTo)
                .OrderByDescending(u => u.CreatedDate)
                .Take(takeLimit)
				.ToList();
		}

		/// <summary>
		/// Search for user notifications that matches the supplied criterion
		/// </summary>
		/// <param name="dateFrom">Begin date to start looking for.</param>
		/// <param name="dateTo">End date to stop looking for.</param>
		/// <param name="notificationTypes">Notification types to match.</param>
		/// <param name="notificationSeverities">Notification severities to match.</param>
		/// <returns></returns>
		public IEnumerable<UserNotification> SearchUserNotifications(DateTime dateFrom, DateTime dateTo, IEnumerable<NotificationType> notificationTypes, IEnumerable<NotificationSeverity> notificationSeverities)
		{
			var query = new List<IMongoQuery>();

			query.Add(Query<UserNotification>.GTE(u => u.CreatedDate, dateFrom));
			query.Add(Query<UserNotification>.LTE(u => u.CreatedDate, dateTo));

			if (notificationTypes.Any())
			{
				query.Add(Query<UserNotification>.In(u => u.NotificationType, notificationTypes));
			}

			if (notificationSeverities.Any())
			{
				query.Add(Query<UserNotification>.In(u => u.Severity, notificationSeverities));
			}

		    var cursor = Collection.Find(Query.And(query));

		    cursor.Limit = 1000;
            
			return cursor.ToList();
		}
	}
}