﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.UserNotifications
{
	public interface IUserNotificationRepository : IRepository<UserNotification>
	{
		void DismissAllByUser(Guid userId);
		void DismissAllByUser(Guid machineGroupId, Guid userId);
		void DismissByUser(Guid userNotificationId, Guid userId);
		IEnumerable<UserNotification> FindByUserId(Guid userId);
		IEnumerable<UserNotification> FindActiveByUserId(Guid userId);
		IEnumerable<UserNotification> SearchUserNotifications(DateTime dateFrom, DateTime dateTo, IEnumerable<NotificationType> notificationTypes, IEnumerable<NotificationSeverity> notificationSeverities);
	}
}