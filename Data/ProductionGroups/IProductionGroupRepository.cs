﻿using System;

using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.ProductionGroups
{
    public interface IProductionGroupRepository : IRepository<ProductionGroup>
    {
        ProductionGroup FindByMachineGroup(Guid guid);
    }
}