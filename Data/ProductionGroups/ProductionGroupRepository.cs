﻿using System;
using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;

namespace PackNet.Data.ProductionGroups
{
    public class ProductionGroupRepository : MongoDbRepository<ProductionGroup>, IProductionGroupRepository
    {
        public ProductionGroupRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "ProductionGroups")
        {
        }

        public ProductionGroup FindByMachineGroup(Guid guid)
        {
            return Collection.AsQueryable().FirstOrDefault(pg => pg.ConfiguredMachineGroups.Contains(guid));
        }
    }
}
