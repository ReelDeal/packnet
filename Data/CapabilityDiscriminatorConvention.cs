﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Conventions;
using PackNet.Common.Interfaces.Machines;
using System;
using System.Collections.Generic;

using PackNet.Data.L10N;

namespace PackNet.Data
{
    /// <summary>
    /// 
    /// </summary>
    /// <example>
#pragma warning disable 1570
    /// BsonSerializer.RegisterDiscriminatorConvention(typeof(BasicCapability<YouTypeHere>), new CapabilityDiscriminatorConvention());//You should test to ensure that you type can be serialized to the repository or if you need to register a 
#pragma warning restore 1570
    /// 
    /// </example>
    public class CapabilityDiscriminatorConvention : IDiscriminatorConvention
    {
        public string ElementName
        {
            get { return "_t"; }
        }

        public Type GetActualType(BsonReader bsonReader, Type nominalType)
        {
            if (nominalType != typeof(ICapability) && nominalType != typeof(List<ICapability>))
                throw new Exception(string.Format(Strings.CapabilityDiscriminatorConvention_GetActualType_Cannot_use_CapabilityDiscriminatorConvention_for_type__0_, nominalType));

            var ret = nominalType;

            var bookmark = bsonReader.GetBookmark();
            bsonReader.ReadStartDocument();
            if (bsonReader.FindElement(ElementName))
            {
                var value = bsonReader.ReadString();

                ret = Type.GetType(value);

                if (ret == null)
                    throw new Exception("Could not find type from " + value);

                if (!typeof(ICapability).IsAssignableFrom(ret) && !ret.IsSubclassOf(typeof(ICapability)))
                    throw new Exception("type is not an ICapability");
            }

            bsonReader.ReturnToBookmark(bookmark);

            return ret;
        }

        public BsonValue GetDiscriminator(Type nominalType, Type actualType)
        {
            if (nominalType != typeof(ICapability) && nominalType != typeof(List<ICapability>))
                throw new Exception("Cannot use CapabilityDiscriminatorConvention for type " + nominalType);

            return actualType.AssemblyQualifiedName;
        }
    }
}
