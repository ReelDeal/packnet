﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Data.PrintMachines
{
    public interface IZebraPrinterRepository : IRepository<ZebraPrinter>
    {
    }
}