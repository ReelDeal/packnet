﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Printing;
using PackNet.Data.Repositories.MongoDB;
using System.Linq;
using MongoDB.Driver.Linq;
namespace PackNet.Data.PrintMachines
{
    public class ZebraPrinterRepository : MongoDbRepository<ZebraPrinter>, IZebraPrinterRepository
    {
        public ZebraPrinterRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "ZebraPrinters")
        {
        }
        public ZebraPrinter Find(string name)
        {
            return Collection.AsQueryable().FirstOrDefault(c => c.Alias == name);
        }
    }
}
