﻿using System;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Utils;
using PackNet.ReportingService;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.Repositories;

using Testing.Specificity;

namespace ReportingServiceTests
{
    [TestClass]
    public class TimeSeriesRecorderTests
    {
        private Mock<ILogger> logger;
        private Mock<ITimeSeriesDataRepository> repository;

        private TimeSeriesRecorder recorder;
        private EventAggregator aggregator;

        [TestInitialize]
        public void Setup()
        {
            logger = new Mock<ILogger>();    
            repository = new Mock<ITimeSeriesDataRepository>();
            aggregator = new EventAggregator();

            recorder = new TimeSeriesRecorder(aggregator, repository.Object, logger.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRecordWasteMetrics()
        {
            var corrugateId = Guid.NewGuid();
            var createCalled = false;
            TimeSeriesDataPoint result = null;

            repository.Setup(m => m.Create(It.IsAny<TimeSeriesDataPoint>())).Callback<TimeSeriesDataPoint>(d =>
            {
                result = d;
                createCalled = true;
            });


            aggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine()
                {
                AssignedOperator = "Emil"
                }, new MachineProductionData()
                {
                    CartonOnCorrugate = new CartonOnCorrugate()
                    {
                        Corrugate = new Corrugate()
                        {
                            Id = corrugateId,
                            Width = 1
                        },
                        AreaOnCorrugate = 0.5,
                        CartonLengthOnCorrugate = 1
                    }
                })
            });

            Retry.For(() => createCalled, TimeSpan.FromSeconds(2));

            Specify.That(createCalled).Should.BeTrue();
            Specify.That(result.Data[TimeSeriesMessages.WastePercentage.DisplayName]).Should.BeLogicallyEqualTo(0.5);
            Specify.That(result.Data[TimeSeriesMessages.WasteValue.DisplayName]).Should.BeLogicallyEqualTo(0.5);
                         
            Specify.That(result.Filters[TimeSeriesMessages.Machine]).Should.BeEqualTo(Guid.Empty.ToString());
            Specify.That(result.Filters[TimeSeriesMessages.Operator]).Should.BeLogicallyEqualTo("Emil");
            Specify.That(result.Filters[TimeSeriesMessages.Corrugate]).Should.BeLogicallyEqualTo(corrugateId.ToString());
        }

    }
}
