﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Utils;
using PackNet.ReportingService;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.Repositories;

using Testing.Specificity;

namespace ReportingServiceTests
{
    [TestClass]
    public class TimeSeriesHandlerTests
    {
        private EventAggregator aggregator;
        private TimeSeriesHandler handler;

        private Mock<ILogger> logger;
        private Mock<ITimeSeriesDataRepository> repository;
            
        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();
            logger = new Mock<ILogger>();
            repository = new Mock<ITimeSeriesDataRepository>();

            handler = new TimeSeriesHandler(repository.Object, aggregator, logger.Object);
        }
                      //todo: fix unit test
#if DEBUG  
 
        [TestMethod]
        [TestCategory("Unit")]
        [Ignore]
        public void ShouldCompressTimeSeriesDataCorrectly()
        {
            var endTime = DateTime.Now;
            var startTime = DateTime.Now.AddYears(-3);
            var timeSeriesData = GetTimeSeriesDataBetween(startTime, endTime, 1000, TimeSeriesMessages.Waste);

            var deleteCalled = false;
            var updateCalled = false;
            IEnumerable<TimeSeriesDataPoint> createdData = null;
            IEnumerable<TimeSeriesDataPoint> deletedData = null;
            repository.Setup(m => m.FindByType(TimeSeriesMessages.Waste))
                .Returns(timeSeriesData);
            
            repository.Setup(m => m.Delete(It.IsAny<IEnumerable<TimeSeriesDataPoint>>())).Callback<IEnumerable<TimeSeriesDataPoint>>(data =>
            {
                deletedData = data;
                deleteCalled = true;
            });

            repository.Setup(m => m.Create(It.IsAny<List<TimeSeriesDataPoint>>())).Callback<IEnumerable<TimeSeriesDataPoint>>(data =>
            {
                createdData = data;
                updateCalled = true;
            });

            aggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>()
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData())
            });

            Retry.For(() => deleteCalled, TimeSpan.FromSeconds(2));
            Retry.For(() => updateCalled, TimeSpan.FromSeconds(2));

            Specify.That(deleteCalled).Should.BeTrue();
            Specify.That(timeSeriesData.SequenceEqual(deletedData)).Should.BeTrue();
            
            Specify.That(updateCalled).Should.BeTrue();
            var timeSeriesDataPoints = createdData as TimeSeriesDataPoint[] ?? createdData.ToArray();
            Specify.That(timeSeriesDataPoints.Count() < timeSeriesData.Count).Should.BeTrue();
            Specify.That(timeSeriesDataPoints.Count(d => d.CreationTime - d.CreationTime.AddYears(-2) > TimeSpan.Zero) <= 1).Should.BeTrue();

        }
          
#endif
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAvrageOutDatapointsCorrectly()
        {
            var points = new List<TimeSeriesDataPoint>()
            {
                new TimeSeriesDataPoint()
                {
                    Data = new Dictionary<string, double>()
                    {
                        { "A", 1 },
                        { "B", 8 },
                        { "C", 15 }
                    }
                },
                new TimeSeriesDataPoint()
                {
                    Data = new Dictionary<string, double>()
                    {
                        { "A", 2 },
                        { "B", 9 },
                        { "C", 16 }
                    }
                },
                new TimeSeriesDataPoint()
                {
                    Data = new Dictionary<string, double>()
                    {
                        { "A", 3 },
                        { "C", 17 },
                    }
                },
                new TimeSeriesDataPoint()
                {
                    Data = new Dictionary<string, double>()
                    {

                        { "A", 4 },
                        { "B", 11 },
                        { "C", 18 },
                    }
                },
                new TimeSeriesDataPoint()
                {
                    Data = new Dictionary<string, double>()
                    {
                        { "A", 5 },
                        { "B", 12 },
                        { "C", 19 },
                        { "D", 19 }
                    }
                },
            };

            var op = TimeSeriesHandler.GetAverage(points);

            Specify.That(op["A"]).Should.BeLogicallyEqualTo(3);
            Specify.That(op["B"]).Should.BeLogicallyEqualTo(10);
            Specify.That(op["C"]).Should.BeLogicallyEqualTo(17);
            Specify.That(op["D"]).Should.BeLogicallyEqualTo(19);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotCompressYearlyData()
        {
            var data = new List<TimeSeriesDataPoint>()
            {
                new TimeSeriesDataPoint() { CreationTime = new DateTime(1, 1, 1, 1, 1, 1, 1), Data = new Dictionary<string, double>() },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(2, 12, 31, 23, 59, 59, 999), Data = new Dictionary<string, double>() },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(3, 12, 31, 23, 59, 59, 999), Data = new Dictionary<string, double>() },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(4, 12, 31, 23, 59, 59, 999), Data = new Dictionary<string, double>() },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(5, 12, 31, 23, 59, 59, 999), Data = new Dictionary<string, double>() },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(6, 12, 31, 23, 59, 59, 999), Data = new Dictionary<string, double>() },
            };

            var yearlyGroupings = TimeSeriesHandler.GetYearlyGroupings(data, new DateTime(7, 1, 1));

            Specify.That(yearlyGroupings.Count()).Should.BeEqualTo(6);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShoudMergeDataIntoTheNextStepWhenItsOldEnough()
        {
            var data = new List<TimeSeriesDataPoint>()
            {
                new TimeSeriesDataPoint() { CreationTime = new DateTime(1, 1, 1, 1, 1, 1, 1), Data = new Dictionary<string, double>() },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(1, 12, 31, 23, 59, 59, 999), Data = new Dictionary<string, double>() },
            };

            var yearlyGroupings = TimeSeriesHandler.GetYearlyGroupings(data, new DateTime(3, 1, 1));

            Specify.That(yearlyGroupings.Count()).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGroupDataCorrectly()
        {
            var data = new List<TimeSeriesDataPoint>()
            {
                new TimeSeriesDataPoint() { CreationTime = new DateTime(1, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(2, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(3, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(1, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(2, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(3, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(1, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(2, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(3, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(1, 1, 1) },
                new TimeSeriesDataPoint() { CreationTime = new DateTime(2, 1, 1) },
            };

            var yearlyGroupings = TimeSeriesHandler.GetYearlyGroupings(data, new DateTime(3,1,1));
            //var monthlyGroupings = TimeSeriesHandler.GetMonthlyGroupings(data);
            //var weeklyGroupings = TimeSeriesHandler.GetWeeklyGroupings(data);
            //var dailyGroupings = TimeSeriesHandler.GetDailyGroupings(data);

            Specify.That(yearlyGroupings.Count()).Should.BeEqualTo(3);


        }

        public List<TimeSeriesDataPoint> GetTimeSeriesDataBetween(DateTime starTime, DateTime endTime, int count, TimeSeriesMessages type)
        {
            var random = new Random();
            var data = new List<TimeSeriesDataPoint>();

            for (int i = 0; i < count; i++)
            {
                data.Add(new TimeSeriesDataPoint
                {
                    CreationTime = new DateTime(random.Next(starTime.Year, endTime.Year),
                                                random.Next(starTime.Month, endTime.Month),
                                                random.Next(starTime.Day, endTime.Day),
                                                random.Next(starTime.Hour, endTime.Hour),
                                                random.Next(starTime.Minute, endTime.Minute),
                                                random.Next(starTime.Second, endTime.Second)),
                    DataType = type,
                    Data = new Dictionary<string, double> { { "value", random.NextDouble() } }
                });
                Thread.Sleep(1);
            }

            return data;
        }
    }
}
