﻿using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.ReportingService;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

namespace ReportingServiceTests
{

    [TestClass]
    public class MetricRecorderTests
    {
        private EventAggregator eventAggregator = new EventAggregator();
        private Mock<IUICommunicationService> uiCommunicator;
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IMetricsRepository<string>> metricsRepository;
        private Mock<IMetricsRepository<KeyValuePair<string, string>>> advancedMetricsRepository;
        private Mock<IMetricsRepository<IDictionary<string, string>>> averageMetricsRepository;
        private Mock<ISlidingWindowMetricsRepository> slidingWindowRepository;
        private Mock<ICorrugateService> corrugateService;
        private Mock<IAggregateMachineService> machineService;
        private Mock<IUserService> userService;
        private Mock<ICartonPropertyGroupService> cpgService;
        private Mock<ILogger> logger;
        // ReSharper disable NotAccessedField.Local
        private MetricRecorder metricRecorder;
        // ReSharper restore NotAccessedField.Local
        private Subject<Tuple<MessageTypes, string>> subject;
        private FusionMachine machine = new FusionMachine { Alias = "test" };
        private User user = new User { UserName = "test" };

        [TestInitialize]
        public void Setup()
        {
            subject = new Subject<Tuple<MessageTypes, string>>();
            uiCommunicator = new Mock<IUICommunicationService>();
            uiCommunicator.SetupGet(m => m.UIEventObservable).Returns(subject.AsObservable());
            metricsRepository = new Mock<IMetricsRepository<string>>();
            advancedMetricsRepository = new Mock<IMetricsRepository<KeyValuePair<string, string>>>();
            averageMetricsRepository = new Mock<IMetricsRepository<IDictionary<string, string>>>();
            slidingWindowRepository = new Mock<ISlidingWindowMetricsRepository>();
            corrugateService = new Mock<ICorrugateService>();
            corrugateService.SetupGet(c => c.Corrugates).Returns(new List<Corrugate> { new Corrugate { Alias = "test", Width = 100 } });

            machineService = new Mock<IAggregateMachineService>();
            machineService.SetupGet(m => m.Machines).Returns(new List<IMachine> { machine });

            userService = new Mock<IUserService>();
            userService.SetupGet(m => m.Users).Returns(new List<User> { user });

            cpgService = new Mock<ICartonPropertyGroupService>();
            cpgService.Setup(cpgs => cpgs.Groups)
                .Returns(
                    new List<CartonPropertyGroup>
                    {
                        new CartonPropertyGroup() { Alias = "AZ1",},
                        new CartonPropertyGroup() { Alias = "AZ2" },
                    });

            serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(sl => sl.Locate<IUICommunicationService>()).Returns(uiCommunicator.Object);
            serviceLocatorMock.Setup(sl => sl.Locate<ICorrugateService>()).Returns(corrugateService.Object);
            serviceLocatorMock.Setup(sl => sl.Locate<IAggregateMachineService>()).Returns(machineService.Object);
            serviceLocatorMock.Setup(sl => sl.Locate<IUserService>()).Returns(userService.Object);
            serviceLocatorMock.Setup(sl => sl.Locate<ICartonPropertyGroupService>()).Returns(cpgService.Object);

            logger = new Mock<ILogger>();
            metricRecorder = new MetricRecorder(eventAggregator,
                eventAggregator,
                serviceLocatorMock.Object,
                metricsRepository.Object,
                advancedMetricsRepository.Object,
                averageMetricsRepository.Object,
                slidingWindowRepository.Object,
                logger.Object);
        }

        #region ProducibleImportCount
        [TestMethod]
        public void SendsImportedCountToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleImportCount), It.IsAny<TimeSpan>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForImportedCountMetric()
        {
            metricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.ProducibleImportCount)));
        }

        [TestMethod]
        public void RespondsToRequestsForImportCountsFromUi()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.ProducibleImportCount, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleImportCount), It.IsAny<TimeSpan>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsImportedTheImportCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "1";
            eventAggregator.Publish(new ImportMessage<IEnumerable<IProducible>>
            {
                Data = new List<IProducible> { new Carton() }
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleImportCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleImportCount && metric.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreImportedTheImportCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "3";
            eventAggregator.Publish(new ImportMessage<IEnumerable<IProducible>>
            {
                Data = new List<IProducible> { new Carton(), new Carton(), new Carton() }
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleImportCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleImportCount && metric.Value == expectedValue)));
        }
        #endregion ProducibleImportCount

        #region ProducibleCreatedCount
        [TestMethod]
        public void SendsCreatedCountToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleCreatedCount), It.IsAny<TimeSpan>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForCreatedCountMetric()
        {
            metricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.ProducibleCreatedCount)));
        }

        [TestMethod]
        public void RespondsToRequestsForCreatedCounts()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.ProducibleCreatedCount, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleCreatedCount), It.IsAny<TimeSpan>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheCreatedCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 1 })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleCreatedCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleCreatedCount && metric.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheCreatedCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 1 })
            });
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 2 })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleCreatedCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(3));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleCreatedCount && metric.Value == expectedValue)));
        }
        #endregion ProducibleCreatedCount

        #region ProducibleRotatedCount
        [TestMethod]
        public void SendsRotatedCountToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleRotatedCount), It.IsAny<TimeSpan>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForRotatedCountMetric()
        {
            metricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.ProducibleRotatedCount)));
        }

        [TestMethod]
        public void RespondsToRequestsForRotatedCounts()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.ProducibleRotatedCount, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleRotatedCount), It.IsAny<TimeSpan>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheRotatedCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    CartonOnCorrugate = new CartonOnCorrugate { Rotation = OrientationEnum.Degree90 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleRotatedCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleRotatedCount && metric.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheRotatedCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    CartonOnCorrugate = new CartonOnCorrugate { Rotation = OrientationEnum.Degree90 }
                })
            });
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    CartonOnCorrugate = new CartonOnCorrugate { Rotation = OrientationEnum.Degree90 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleRotatedCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(3));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleRotatedCount && metric.Value == expectedValue)));
        }
        #endregion ProducibleRotatedCount

        #region ProducibleFailedCount

        [TestMethod]
        public void SendsFailedCountToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleFailedCount), It.IsAny<TimeSpan>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForFailedCountMetric()
        {
            metricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.ProducibleFailedCount)));
        }

        [TestMethod]
        public void RespondsToRequestsForFailedCounts()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.ProducibleFailedCount, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleFailedCount), It.IsAny<TimeSpan>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedAndFailingTheFailedCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 1, Failure = true })
            });
            Thread.Sleep(500);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleFailedCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleFailedCount && metric.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedAndFailingTheFailedCountMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 1, Failure = true })
            });
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 2, Failure = true })
            });
            Thread.Sleep(500);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<string>>>(msg => msg.MessageType == MetricType.ProducibleFailedCount && msg.Data.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(3));
            metricsRepository.Verify(m => m.Update(It.Is<Metric<string>>(metric => metric.MetricType == MetricType.ProducibleFailedCount && metric.Value == expectedValue)));
        }

        #endregion ProducibleFailedCount

        #region AverageProductionTime
        [TestMethod]
        public void SendsAveProductionTimeToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageProductionTime), It.IsAny<TimeSpan>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForAveProductionTimeMetric()
        {
            averageMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.AverageProductionTime)));
        }

        [TestMethod]
        public void RespondsToRequestsForAveProductionTime()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.AverageProductionTime, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageProductionTime), It.IsAny<TimeSpan>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheMetricIsIncrementedAndPersisted()
        {
            var expectedValue = TimeSpan.FromSeconds(10).ToString();
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 1, ProductionTime = 10000D })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageProductionTime && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageProductionTime && metric.Value[MetricRecorder.AverageValue] == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheMetricIsIncrementedAndPersisted()
        {
            var expectedValue = TimeSpan.FromSeconds(0).ToString("hh\\:mm\\:ss");
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageProductionTime && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = TimeSpan.FromSeconds(10).ToString();
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    ProductionTime = 10000D,
                    Corrugate = new Corrugate { Width = 30D }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageProductionTime && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(1));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageProductionTime && metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
            averageMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = TimeSpan.FromSeconds(13).ToString("hh\\:mm\\:ss");
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData { ProducibleCount = 2, ProductionTime = 30000D })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageProductionTime && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(1));
            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageProductionTime && metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
        }
        #endregion AverageProductionTime

        #region AverageWastePercentage
        [TestMethod]
        public void SendsAverageWastePercentageToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentage), It.IsAny<TimeSpan>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForAverageWastePercentageMetric()
        {
            averageMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.AverageWastePercentage)));
        }

        [TestMethod]
        public void RespondsToRequestsForAverageWastePercentage()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.AverageWastePercentage, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentage), It.IsAny<TimeSpan>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenATiledCartonIsCompletedTheAverageWastePercentageMetricShouldCalculatedWasteAccordingToTileCound()
        {
            var expectedValue = "13.2";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 13.02, TileCount = 2 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentage && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentage && metric.Value[MetricRecorder.AverageValue] == expectedValue)));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheAverageWastePercentageMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "13.3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentage && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentage && metric.Value[MetricRecorder.AverageValue] == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheAverageWastePercentageMetricIsIncrementedAndPersisted()
        {
            var expectedValue = "0";
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentage && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = "13.3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    Corrugate = new Corrugate { Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentage && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(1));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentage && metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
            averageMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = "8.87";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    Corrugate = new Corrugate { Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentage && msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(1));
            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentage && metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
        }
        #endregion AverageWastePercentage

        #region AverageWastePercentageByMachine
        [TestMethod]
        public void SendsAverageWastePercentageByMachineToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByMachine), false, It.IsAny<bool>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForAverageWastePercentageByMachineMetric()
        {
            averageMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.AverageWastePercentageByMachine), It.IsAny<Func<Metric<IDictionary<string, string>>, bool>>()));
        }

        [TestMethod]
        public void RespondsToRequestsForAverageWastePercentageByMachine()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.AverageWastePercentageByMachine, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByMachine), false, It.IsAny<bool>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheAverageWastePercentageByMachineMetricIsIncrementedAndPersisted()
        {
            var machine = new FusionMachine { Alias = "test" };
            var expectedKey = "test";
            var expectedValue = "13.3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machine, new MachineProductionData
                {
                    ProducibleCount = 1,
                    Corrugate = new Corrugate { Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByMachine &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(2));
            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentageByMachine &&
                 metric.Value[MetricRecorder.AverageKey] == expectedKey &&
                 metric.Value[MetricRecorder.AverageValue] == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheAverageWastePercentageByMachineMetricIsIncrementedAndPersisted()
        {
            var machine = new FusionMachine() { Alias = "test" };
            var expectedKey = "test";
            var expectedValue = "0";
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByMachine &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = "13.3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machine, new MachineProductionData
                {
                    ProducibleCount = 1,
                    Corrugate = new Corrugate { Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByMachine &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentageByMachine &&
            metric.Value[MetricRecorder.AverageKey] == expectedKey &&
            metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
            averageMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = "8.87";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    Corrugate = new Corrugate { Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByMachine &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));

            averageMetricsRepository.Verify(
                m =>
                    m.Update(
                        It.Is<Metric<IDictionary<string, string>>>(
                            metric => metric.MetricType == MetricType.AverageWastePercentageByMachine &&

                                      metric.Value[MetricRecorder.AverageKey] == expectedKey &&
                                      metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
        }
        #endregion AverageWastePercentageByMachine

        #region AverageWastePercentageByMachine
        [TestMethod]
        public void SendsAverageWastePercentageByCorrugateToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByCorrugate), false, It.IsAny<bool>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForAverageWastePercentageByCorrugateMetric()
        {
            averageMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.AverageWastePercentageByCorrugate), It.IsAny<Func<Metric<IDictionary<string, string>>, bool>>()));
        }

        [TestMethod]
        public void RespondsToRequestsForAverageWastePercentageByCorrugate()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.AverageWastePercentageByCorrugate, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByCorrugate), false, It.IsAny<bool>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheAverageWastePercentageByCorrugateMetricIsIncrementedAndPersisted()
        {
            var machine = new FusionMachine() { Alias = "test" };
            var expectedKey = "test";
            var expectedValue = "13.3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machine, new MachineProductionData
                {
                    ProducibleCount = 1,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 },
                    Machine = machine,
                    UserName = expectedKey,
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByCorrugate &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(2));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentageByCorrugate &&
                metric.Value[MetricRecorder.AverageKey] == expectedKey &&
                metric.Value[MetricRecorder.AverageValue] == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheAverageWastePercentageByCorrugateMetricIsIncrementedAndPersisted()
        {
            var machine = new FusionMachine() { Alias = "test" };
            var expectedKey = "test";
            var expectedValue = "0";
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByCorrugate &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = "13.3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machine, new MachineProductionData
                {
                    ProducibleCount = 1,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByCorrugate &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentageByCorrugate &&
                metric.Value[MetricRecorder.AverageKey] == expectedKey &&
                metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
            averageMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = "8.87";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { CartonWidthOnCorrugate = 26.01, TileCount = 1 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<IDictionary<string, string>>>>(msg => msg.MessageType == MetricType.AverageWastePercentageByCorrugate &&
                msg.Data.Value[MetricRecorder.AverageKey] == expectedKey &&
                msg.Data.Value[MetricRecorder.AverageValue] == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));

            averageMetricsRepository.Verify(m => m.Update(It.Is<Metric<IDictionary<string, string>>>(metric => metric.MetricType == MetricType.AverageWastePercentageByCorrugate &&
            metric.Value[MetricRecorder.AverageKey] == expectedKey &&
                metric.Value[MetricRecorder.AverageValue] == expectedValue)), Times.Exactly(1));
        }
        #endregion AverageWastePercentageByCorrugate

        #region CorrugateConsumption
        [TestMethod]
        public void SendsCorrugateConsumptionToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CorrugateConsumption), It.IsAny<bool>(), It.IsAny<bool>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForCorrugateConsumption()
        {
            advancedMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.CorrugateConsumption),
                It.IsAny<Func<Metric<KeyValuePair<string, string>>, bool>>()));
        }

        [TestMethod]
        public void RespondsToRequestsForCorrugateConsumption()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.CorrugateConsumption, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CorrugateConsumption), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheCorrugateConsumptionMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "10";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator
                .Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CorrugateConsumption &&
                    msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(2));
            advancedMetricsRepository
                .Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CorrugateConsumption &&
                    metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheCorrugateConsumptionMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "0";
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CorrugateConsumption &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = "10";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CorrugateConsumption &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CorrugateConsumption &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
            advancedMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = "22";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    FedLength = 12D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CorrugateConsumption &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CorrugateConsumption &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
        }
        #endregion CorrugateConsumption

        #region CartonCountByCorrugate
        [TestMethod]
        public void SendsCartonCountByCorrugateToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCorrugate), false, It.IsAny<bool>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForCartonCountByCorrugate()
        {
            advancedMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.CartonCountByCorrugate),
                It.IsAny<Func<Metric<KeyValuePair<string, string>>, bool>>()));
        }

        [TestMethod]
        public void RespondsToRequestsForCartonCountByCorrugate()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.CartonCountByCorrugate, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCorrugate), false, It.IsAny<bool>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheCartonCountByCorrugateMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator
                .Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCorrugate &&
                    msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(2));
            advancedMetricsRepository
                .Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByCorrugate &&
                    metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheCartonCountByCorrugateMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "0";
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCorrugate &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCorrugate &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByCorrugate &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
            advancedMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = "3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    FedLength = 12D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 }
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCorrugate &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByCorrugate &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
        }
        #endregion CartonCountByCorrugate

        #region CartonCountByMachine
        [TestMethod]
        public void SendsCartonCountByMachineToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByMachine), false, It.IsAny<bool>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForCartonCountByMachine()
        {
            advancedMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.CartonCountByMachine),
                It.IsAny<Func<Metric<KeyValuePair<string, string>>, bool>>()));
        }

        [TestMethod]
        public void RespondsToRequestsForCartonCountByMachine()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.CartonCountByMachine, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByMachine), false, It.IsAny<bool>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheCartonCountByMachineMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator
                .Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByMachine &&
                    msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(2));
            advancedMetricsRepository
                .Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByMachine &&
                    metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheCartonCountByMachineMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "0";
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByMachine &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByMachine &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByMachine &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
            advancedMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = "3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    FedLength = 12D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 },
                    Machine = machine
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByMachine &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByMachine &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
        }
        #endregion CartonCountByMachine

        #region CartonCountByUser
        [TestMethod]
        public void SendsCartonCountByUserToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByUser), false, It.IsAny<bool>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForCartonCountByUser()
        {
            advancedMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.CartonCountByUser),
                It.IsAny<Func<Metric<KeyValuePair<string, string>>, bool>>()));
        }

        [TestMethod]
        public void RespondsToRequestsForCartonCountByUser()
        {
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.CartonCountByUser, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByUser), false, It.IsAny<bool>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheCartonCountByUserMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 },
                    UserName = user.UserName
                })
            });
            Thread.Sleep(100);
            uiCommunicator
                .Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByUser &&
                    msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(2));
            advancedMetricsRepository
                .Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByUser &&
                    metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)));
        }

        [TestMethod]
        public void WhenCartonsAreCompletedTheCartonCountByUserMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "test";
            var expectedValue = "0";
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByUser &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            uiCommunicator.ResetCalls();

            expectedValue = "1";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 1,
                    FedLength = 10D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 },
                    UserName = user.UserName
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByUser &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByUser &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
            advancedMetricsRepository.ResetCalls();
            uiCommunicator.ResetCalls();

            expectedValue = "3";
            eventAggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(new FusionMachine(), new MachineProductionData
                {
                    ProducibleCount = 2,
                    FedLength = 12D,
                    Corrugate = new Corrugate { Alias = "test", Width = 30D },
                    CartonOnCorrugate = new CartonOnCorrugate { AreaOnCorrugate = 259.99 },
                    UserName = user.UserName
                })
            });
            Thread.Sleep(100);
            uiCommunicator.Verify(m => m.SendMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByUser &&
                msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), false, It.IsAny<bool>()), Times.Exactly(1));
            advancedMetricsRepository.Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByUser &&
                metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)), Times.Exactly(1));
        }
        #endregion CartonCountByUser

        #region CartonCountByCpg
        [TestMethod]
        public void SendsCartonCountByCPGToUiWhenConstructed()
        {
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCPG), It.IsAny<TimeSpan>()));
        }

        [TestMethod]
        public void QueriesDatabaseAtStartupForCartonCountByCPG()
        {
            advancedMetricsRepository.Verify(m => m.FindByType(It.Is<MetricType>(mt => mt == MetricType.CartonCountByCPG),
                It.IsAny<Func<Metric<KeyValuePair<string, string>>, bool>>()));
        }

        [TestMethod]
        public void RespondsToRequestsForCartonCountByCPG()
        {
            uiCommunicator.ResetCalls();
            //Simulate the UI asking for the counts
            subject.OnNext(new Tuple<MessageTypes, string>(MetricType.CartonCountByCPG, ""));
            //Make sure the counts were sent twice... once at startup and once when requested
            uiCommunicator.Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCPG), It.IsAny<TimeSpan>()), Times.Exactly(2));
        }

        [TestMethod]
        public void WhenACartonIsCompletedTheCartonCountByCPGMetricIsIncrementedAndPersisted()
        {
            var expectedKey = "AZ1";
            var expectedValue = "1";
            eventAggregator.Publish(new Message<string>
            {
                MessageType = CartonPropertyGroupMessages.ProducibleCompleted,
                Data = expectedKey
            });
            Thread.Sleep(100);
            uiCommunicator
                .Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCPG &&
                    msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));
            advancedMetricsRepository
                .Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByCPG &&
                    metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)));
        }

        [TestMethod]
        public void UpdatingACPG_ShouldResetTheMetric()
        {
            var expectedKey = "AZ1";
            var expectedValue = "1";
            eventAggregator.Publish(new Message<string>
            {
                MessageType = CartonPropertyGroupMessages.ProducibleCompleted,
                Data = expectedKey
            });
            Thread.Sleep(100);
            uiCommunicator
                .Verify(m => m.SendThrottledMessageToUI(It.Is<Message<Metric<KeyValuePair<string, string>>>>(msg => msg.MessageType == MetricType.CartonCountByCPG &&
                    msg.Data.Value.Key == expectedKey &&
                msg.Data.Value.Value == expectedValue), It.IsAny<TimeSpan>()), Times.Exactly(2));

            advancedMetricsRepository
                .Verify(m => m.Update(It.Is<Metric<KeyValuePair<string, string>>>(metric => metric.MetricType == MetricType.CartonCountByCPG &&
                    metric.Value.Key == expectedKey &&
                metric.Value.Value == expectedValue)));

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.CartonPropertyGroupUpdated,
                Data = new CartonPropertyGroup() { Alias = expectedKey }
            });

            expectedValue = "0";
            uiCommunicator.Verify(
                m =>
                    m.SendThrottledMessageToUI(
                        It.Is<Message<Metric<KeyValuePair<string, string>>>>(
                            msg =>
                                msg.MessageType == MetricType.CartonCountByCPG && msg.Data.Value.Key == expectedKey
                                && msg.Data.Value.Value == expectedValue),
                        It.IsAny<TimeSpan>()),
                Times.Exactly(3));
            advancedMetricsRepository.Verify(
                m =>
                    m.Update(
                        It.Is<Metric<KeyValuePair<string, string>>>(
                            metric =>
                                metric.MetricType == MetricType.CartonCountByCPG && metric.Value.Key == expectedKey
                                && metric.Value.Value == expectedValue)));
        }

        [TestMethod]
        public void EntriesAddedToASlidingWindowMetric_ShouldNotBeAdded_IfDateIsOutsideWindow()
        {
            eventAggregator = new EventAggregator();
            var testScheduler = new TestScheduler();
            testScheduler.AdvanceTo(DateTime.Now.Subtract(TimeSpan.FromHours(2)).Ticks);
            eventAggregator.Scheduler = testScheduler;

            metricRecorder = new MetricRecorder(eventAggregator,
                eventAggregator,
                serviceLocatorMock.Object,
                metricsRepository.Object,
                advancedMetricsRepository.Object,
                averageMetricsRepository.Object,
                slidingWindowRepository.Object,
                logger.Object);

            var expectedKey = "AZ1";
            var expectedValue = 0;
            var msg = new Message<string> { MessageType = CartonPropertyGroupMessages.ProducibleCompleted, Data = expectedKey };

            for (int i = 0; i < 5; i++)
            {
                slidingWindowRepository.ResetCalls();
                msg.Created = testScheduler.Now.DateTime;
                eventAggregator.Publish(msg);
                Thread.Sleep(20);
                testScheduler.AdvanceBy(TimeSpan.FromMinutes(1).Ticks);

                slidingWindowRepository.Verify(
                    m =>
                        m.Update(
                            It.Is<SlidingWindowMetric>(
                                metric =>
                                    metric.MetricType == MetricType.CartonCountByCPGBySlidingHour
                                    && metric.Value.Key == expectedKey && metric.Value.Value == expectedValue.ToString())),
                    Times.Exactly(1));

            }
            slidingWindowRepository.ResetCalls();
            testScheduler.AdvanceTo(DateTime.Now.Ticks);
            msg.Created = testScheduler.Now.DateTime;
            eventAggregator.Publish(msg);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5).Ticks);

            expectedValue = 1;
            slidingWindowRepository.Verify(
                    m =>
                        m.Update(
                            It.Is<SlidingWindowMetric>(
                                metric =>
                                    metric.MetricType == MetricType.CartonCountByCPGBySlidingHour
                                    && metric.Value.Key == expectedKey && metric.Value.Value == expectedValue.ToString())),
                    Times.Between(1, 2, Range.Inclusive));
        }

        [TestMethod]
        public void EntriesInASlidingWindowMetric_ShouldBePurged_WhenGoingOutsideTheWindowSize()
        {
            eventAggregator = new EventAggregator();
            var testScheduler = new TestScheduler();
            testScheduler.AdvanceTo(DateTime.Now.Subtract(TimeSpan.FromHours(2)).Ticks);
            eventAggregator.Scheduler = testScheduler;

            metricRecorder = new MetricRecorder(eventAggregator,
                eventAggregator,
                serviceLocatorMock.Object,
                metricsRepository.Object,
                advancedMetricsRepository.Object,
                averageMetricsRepository.Object,
                slidingWindowRepository.Object,
                logger.Object);

            var expectedKey = "AZ1";
            var expectedValue = 0;

            var msg = new Message<string> { MessageType = CartonPropertyGroupMessages.ProducibleCompleted, Data = expectedKey };

            // 5 entries created 2 hours ago will be added to the sliding window metric
            // They will not count towards the metric.Value because they are out of scope
            for (int i = 0; i < 5; i++)
            {
                slidingWindowRepository.ResetCalls();
                msg.Created = testScheduler.Now.DateTime;
                eventAggregator.Publish(msg);
                testScheduler.AdvanceBy(TimeSpan.FromMinutes(1).Ticks);
                expectedValue++;
                Thread.Sleep(20);
                slidingWindowRepository.Verify(
                        m => m.Update(It.Is<SlidingWindowMetric>(
                                    metric =>
                                        metric.MetricType == MetricType.CartonCountByCPGBySlidingHour
                                        && metric.Value.Key == expectedKey
                                        && metric.Entries.Count == expectedValue)), Times.Exactly(1));
            }

            // Reset the calls and advance the scheduler time to DateTime.Now
            // Since the purgeInterval is set to 30min (and we haven't completed anyting for the past two hours) the next event till trigger a purge
            slidingWindowRepository.ResetCalls();
            testScheduler.AdvanceTo(DateTime.Now.Ticks);
            msg.Created = testScheduler.Now.DateTime;
            eventAggregator.Publish(msg);
            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(5).Ticks);

            // The entries added 2 hours ago should now be gone from metric.Entries and only the one we just added should be present
            expectedValue = 1;
            slidingWindowRepository.Verify(
                    m =>
                        m.Update(
                            It.Is<SlidingWindowMetric>(
                                metric =>
                                    metric.MetricType == MetricType.CartonCountByCPGBySlidingHour
                                    && metric.Value.Key == expectedKey && metric.Entries.Count == expectedValue && metric.Entries.ElementAt(0) == msg.Created)),
                    Times.Between(1, 2, Range.Inclusive));
        }

        #endregion
    }
}
