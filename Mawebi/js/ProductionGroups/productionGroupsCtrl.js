function ProductionGroupsCtrl($location, $log, $scope, $state, $timeout, alertService, introJsService, messagingService, uiGridConstants, translationService) {
    var awaitingUpdate = false;

    var selectionAlgorithms = {
        BoxFirst: {
            Enum: "BoxFirst",
            Label: translationService.translate('SelectionAlgorithm.Label.BoxFirst'),
            Path: "Box First"
        },
        BoxLast: {
            Enum: "BoxLast",
            Label: translationService.translate('SelectionAlgorithm.Label.BoxLast'),
            Path: "Box Last"
        },
        Orders: {
            Enum: "Order",
            Label: translationService.translate('SelectionAlgorithm.Label.Order'),
            Path: "Orders"
        },
        ScanToCreate: {
            Enum: "ScanToCreate",
            Label: translationService.translate('SelectionAlgorithm.Label.ScanToCreate'),
            Path: "Scan To Create"
        }
    }

    $timeout = $timeout.getInstance($scope);

    $scope.assignedMachinesGroups = [];
    $scope.availableMachineGroups = [];
    $scope.unavailableMachineGroups = [];
    $scope.productionGroups = [];
    $scope.selectedProductionGroup = {};
    $scope.selectedProductionGroup.IsNewProductionGroup = true;
    $scope.labelUnavailableMachineGroups = '';
    $scope.showUnavailableMachineGroups = false;

    $scope.availableSelectionAlgorithms = [
        selectionAlgorithms.BoxFirst.Label,
        selectionAlgorithms.BoxLast.Label,
        selectionAlgorithms.Orders.Label,
        selectionAlgorithms.ScanToCreate.Label
    ];

    $scope.$on('$locationChangeSuccess', function () {
        if ($scope.selectedProductionGroup && $scope.selectedProductionGroup.IsNewProductionGroup == false && $location.path() == '/ProductionGroups') {
            $scope.cancelNewEditProductionGroup();
        }
    });

    /**
* Compares two arrays. 
* @param {array} originalCollection
* @param {array} newCollection
* @param {string} key: object key to compare
* @param {string} compareProperty: if we need to compare to a different property beside the key.
* @returns {object} Object with two properties: shouldUpdate and collection.
*/
    var checkIfOriginalCollectionShouldBeUpdate = function (originalCollection, newCollection, key, compareProperty) {
        // Make sure the original collection has data and the size of both arrays are the same.
        if (originalCollection != null &&
            originalCollection.length == newCollection.length) {

            // Both Collections have the same size, lets check if they are the same items.
            var originalEnum = Enumerable.From(originalCollection);
            var newCollectionEnum = Enumerable.From(newCollection);

            // Make sure all items from one collection exist in the other one
            if (originalEnum.All(function (originalItem) {
                // Get the item to compare by key
                var itemToCompare = newCollectionEnum.FirstOrDefault(null, function (i) { return originalItem[key] == i[key]; });

                // If value doesn't exist on the other collection, the collections are not the same.
                if (itemToCompare == null) {
                    return false;
            }

                // if compareProperty is undefined, we only care for the key.
                if (compareProperty == undefined) {
                    return true;
            }

                // Compare the property
                return originalItem[compareProperty] == itemToCompare[compareProperty];
            })) {
                // As both are the same we don't need to do anything, if we need to update some value we should do it here.
                return { shouldUpdate: false, collection: null };
            }
        }

        // Items are not the same, we should update
        return { shouldUpdate: true, collection: newCollection };
    };

    function convertSelectionAlgorithmFriendlyNameToEnum(displayValue) {
        if (displayValue === selectionAlgorithms.BoxFirst.Label) {
            return selectionAlgorithms.BoxFirst.Enum;
        }
        else if (displayValue === selectionAlgorithms.BoxLast.Label) {
            return selectionAlgorithms.BoxLast.Enum;
        }
        else if (displayValue === selectionAlgorithms.Orders.Label) {
            return selectionAlgorithms.Orders.Enum;
        }
        else if (displayValue === selectionAlgorithms.ScanToCreate.Label) {
            return selectionAlgorithms.ScanToCreate.Enum;
        }
        return displayValue;
    }

    function convertSelectionAlgorithmFriendlyNameToPath(displayValue) {
        if (displayValue === selectionAlgorithms.BoxFirst.Label) {
            return selectionAlgorithms.BoxFirst.Path;
        }
        else if (displayValue === selectionAlgorithms.BoxLast.Label) {
            return selectionAlgorithms.BoxLast.Path;
        }
        else if (displayValue === selectionAlgorithms.Orders.Label) {
            return selectionAlgorithms.Orders.Path;
        }
        else if (displayValue === selectionAlgorithms.ScanToCreate.Label) {
            return selectionAlgorithms.ScanToCreate.Path;
        }
        return displayValue;
    }

    function convertSelectionAlgorithmToFriendlyName(selectionAlgorithm) {
        if (selectionAlgorithm === selectionAlgorithms.BoxFirst.Enum) {
            return selectionAlgorithms.BoxFirst.Label;
        }
        else if (selectionAlgorithm === selectionAlgorithms.BoxLast.Enum) {
            return selectionAlgorithms.BoxLast.Label;
        }
        else if (selectionAlgorithm === selectionAlgorithms.Orders.Enum) {
            return selectionAlgorithms.Orders.Label;
        }
        else if (selectionAlgorithm === selectionAlgorithms.ScanToCreate.Enum) {
            return selectionAlgorithms.ScanToCreate.Label;
        }
        return translationService.translate('Common.Label.Unassigned');
    }

    function convertCorrugatesToFriendlyFormat(corrugates) {
        if (corrugates == undefined || $scope.corrugates == undefined) return "";


        var aliases = Enumerable.From($scope.corrugates)
            .Where(function (c) { return corrugates.indexOf(c.Id) > -1; })
            .Select(function (c) { return c.Alias; })
            .ToArray();

        var friendlyMessage = "";
        for (var i = 0; i < aliases.length; i++) {
            friendlyMessage = friendlyMessage + aliases[i];
            if (i != aliases.length - 1) friendlyMessage = friendlyMessage + ", ";
        }

        return friendlyMessage;
    }

    function convertMachineGroupsToFriendlyFormat(machineGroups) {
        var aliases = Enumerable.From($scope.machineGroups)
            .Where(function (mg) { return machineGroups.indexOf(mg.Id) > -1; })
            .Select(function (mg) { return mg.Alias; })
            .ToArray();

        var friendlyMessage = "";
        for (var i = 0; i < aliases.length; i++) {
            friendlyMessage = friendlyMessage + aliases[i];
            if (i != aliases.length - 1) friendlyMessage = friendlyMessage + ", ";
        }

        return friendlyMessage;
    }

    $scope.$watch('showUnavailableMachineGroups', function (newValue) {
        delete $scope.labelUnavailableMachineGroups;

        if (newValue) {
            $scope.labelUnavailableMachineGroups = translationService.translate('ProductionGroup.Label.HideUnavailableMachineGroups');
        }
        else {
            $scope.labelUnavailableMachineGroups = translationService.translate('ProductionGroup.Label.ShowUnavailableMachineGroups');
        }
    });

    messagingService.productionGroupsObservable.autoSubscribe(function (msg) {
        $timeout(function () {
        //Punt here since we have to set machine groups before setting production groups (TFS 9785)
        if ($scope.machineGroups == null || $scope.machineGroups == undefined) {
            return;
        }

            var productionGroups = checkIfOriginalCollectionShouldBeUpdate($scope.productionGroups, Enumerable.From(msg.Data).Select(function (pg) {
            pg.FriendlySelectionAlgorithm = convertSelectionAlgorithmToFriendlyName(pg.SelectionAlgorithm);
            pg.FriendlyCorrugates = convertCorrugatesToFriendlyFormat(pg.ConfiguredCorrugates);
            pg.FriendlyMachineGroups = convertMachineGroupsToFriendlyFormat(pg.ConfiguredMachineGroups);
            return pg;
            }).ToArray(), 'Id');

            if (productionGroups.shouldUpdate) {
                $timeout(function () {
                    
                    delete $scope.productionGroups;
                    $scope.productionGroups = productionGroups.collection;

                    
                    delete $scope.assignedMachinesGroups;
                    $scope.assignedMachinesGroups = Enumerable.From(msg.Data).SelectMany('$.ConfiguredMachineGroups',
            // ($) - the parent object
            // ($$) - the selected object
            '{ MachineGroupId: $$, ProductionGroupId: $.Id, ProductionGroupAlias: $.Alias}'
        ).ToArray();
                }, 0, true);
            }
        }, 0);
    }, $scope, 'ProductionGroups');

    messagingService.productionGroupCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('ProductionGroup.Label.NameSingular'), $scope.productionGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('ProductionGroup.Label.NameSingular'), $scope.productionGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('ProductionGroup.Label.NameSingular'), $scope.productionGroupBeingSaved.Alias), 5000);
        }
    }, $scope, 'ProductionGroup Created');

    messagingService.productionGroupUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('ProductionGroup.Label.NameSingular'), $scope.productionGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('ProductionGroup.Label.NameSingular'), $scope.productionGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('ProductionGroup.Label.NameSingular'), $scope.productionGroupBeingSaved.Alias), 5000);
        }
    }, $scope, 'ProductionGroup Updated');

    messagingService.productionGroupDeletedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.DeleteFailed', translationService.translate('ProductionGroup.Label.NameSingular'), msg.Data.Alias));
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', translationService.translate('ProductionGroup.Label.NameSingular'), msg.Data.Alias), 5000);
            
            delete $scope.productionGroupBeingSaved;
            $scope.productionGroupBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedProductionGroup;
            $scope.selectedProductionGroup = {};
            $scope.selectedProductionGroup.IsNewProductionGroup = true;
        }
        requestData();
    }, $scope, 'ProductionGroup Deleted');

    messagingService.machineGroupsObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            messagingService.publish("GetProductionGroups");

        
        delete $scope.machineGroups;
        $scope.machineGroups = Enumerable
                .From(msg.Data)
            .Select('{Id: $.Id, Alias: $.Alias}')
            .ToArray();

            var unavailableMachineGroups = checkIfOriginalCollectionShouldBeUpdate($scope.unavailableMachineGroups, Enumerable
            .From($scope.assignedMachinesGroups)
            .Except(Enumerable.From($scope.selectedProductionGroup.ConfiguredMachineGroups).Select('{MachineGroupId: $}'), '$.MachineGroupId')
            .Select(function (item) {
            return {
                        MachineGroupId: item.MachineGroupId,
                        MachineGroupAlias: Enumerable.From($scope.machineGroups).Where(function (mg) { return mg.Id == item.MachineGroupId; }).Select('$.Alias').FirstOrDefault(),
                        ProductionGroupId: item.ProductionGroupId,
                        ProductionGroupAlias: item.ProductionGroupAlias
                };
            })
                .ToArray(), 'MachineGroupId', 'ProductionGroupId');
            if (unavailableMachineGroups.shouldUpdate) {
                
                delete $scope.unavailableMachineGroups;
                $scope.unavailableMachineGroups = unavailableMachineGroups.collection;
            }

            var availableMachineGroups = checkIfOriginalCollectionShouldBeUpdate($scope.availableMachineGroups, Enumerable
            .From($scope.machineGroups)
            .Except(Enumerable.From($scope.unavailableMachineGroups).Select('{Id: $.MachineGroupId}'), '$.Id')
            .Select(function (item) {
                return {
                        Id: item.Id,
                        Alias: item.Alias,
                        Included: Enumerable
                        .From($scope.selectedProductionGroup.ConfiguredMachineGroups)
                    .Any(function (configured) {
                            return configured == item.Id;
                    })
                };
            })
                .ToArray(), 'Id');
            if (availableMachineGroups.shouldUpdate) {
                
                delete $scope.availableMachineGroups;
                $scope.availableMachineGroups = availableMachineGroups.collection;
            }
        }, 0);
    }, $scope, 'MachineGroups');

    messagingService.corrugatesObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.corrugates;
            $scope.corrugates = Enumerable.From(msg.Data).Select(function (corrugate) {
            return {
                Id: corrugate.Id,
                Alias: corrugate.Alias,
                Included: Enumerable.From($scope.selectedProductionGroup.ConfiguredCorrugates)
                    .Any(function (configured) {
                        return corrugate.Id == configured;
                    })
            }
        }).ToArray();
        }, 0);
    }, $scope, 'Corrugates');

    $scope.toggleUnavailableMachineGroups = function () {
        $scope.showUnavailableMachineGroups = !$scope.showUnavailableMachineGroups;
    }

    var requestData = function () {
        messagingService.publish("GetCorrugates");
        messagingService.publish("GetMachineGroups");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);
    
    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'productionGroups',
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                width: 200,
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                field: 'Description',
                name: translationService.translate('Common.Label.Description'),
                width: 150
            },
            {
                field: 'FriendlySelectionAlgorithm',
                name: translationService.translate('ProductionGroup.Label.ProductionMode'),
                width: 150
            },
            {
                field: 'FriendlyCorrugates',
                name: translationService.translate('Corrugate.Label.NamePlural'),
                width: '*',
                minWidth: 50
            },
            {
                field: 'FriendlyMachineGroups',
                name: translationService.translate('MachineGroup.Label.NamePlural'),
                width: '*',
                minWidth: 50
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
            //gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            //    $scope.unavailableMachineGroups = null;
            //});
        }
    };

    $scope.getProductionOptionsPath = function () {
        if ($scope.selectedProductionGroup.FriendlySelectionAlgorithm != undefined)
            return "partials\\SelectionAlgorithm\\" + convertSelectionAlgorithmFriendlyNameToPath($scope.selectedProductionGroup.FriendlySelectionAlgorithm) + "\\productionGroupOptions.html";
    }

    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    //In order to overwrite the function def this must be attached to an object
    $scope.OptionsHelper = {};
    $scope.OptionsHelper.saveOptions = function () {
        //Assign this in any child controller to handle custom logic saving Options
        return true;
    }

    $scope.saveProductionGroup = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        $timeout(function () { }, 0, true);
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        
        delete $scope.productionGroupBeingSaved;
        $scope.productionGroupBeingSaved = {
            Id: $scope.selectedProductionGroup.Id,
            Alias: $scope.selectedProductionGroup.Alias,
            Description: $scope.selectedProductionGroup.Description,
            SelectionAlgorithm: convertSelectionAlgorithmFriendlyNameToEnum($scope.selectedProductionGroup.FriendlySelectionAlgorithm),
            ConfiguredCorrugates: Enumerable.From($scope.corrugates).Where("c => c.Included").Select("c => c.Id").ToArray(),
            ConfiguredMachineGroups: Enumerable.From($scope.availableMachineGroups).Where("mg => mg.Included").Select("mg => mg.Id").ToArray(),
            IsNewProductionGroup: $scope.selectedProductionGroup.IsNewProductionGroup,
            Options: $scope.selectedProductionGroup.Options
        };

        var action = "CreateProductionGroup";
        if (!$scope.productionGroupBeingSaved.IsNewProductionGroup)
            action = "UpdateProductionGroup";

        //Hook for custom options code
        if (!$scope.OptionsHelper.saveOptions()) {
            return;
        }

        messagingService.publish(action, $scope.productionGroupBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('ProductionGroup.Label.NameSingular'), $scope.productionGroupBeingSaved.Alias));
                awaitingUpdate = false;
                
                delete $scope.productionGroupBeingSaved;
                $scope.productionGroupBeingSaved = {};
                
                delete $scope.selectedProductionGroup;
                $scope.selectedProductionGroup = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditProductionGroup = function () {
        $state.go('^.list', null, { reload: true });
    }

    $scope.deleteProductionGroup = function () {
        messagingService.publish("DeleteProductionGroup", $scope.gridApi.selection.getSelectedRows());
        $scope.gridApi.selection.clearSelectedRows();
    }

    $scope.editProductionGroup = function () {
        
        delete $scope.selectedProductionGroup;
        $scope.selectedProductionGroup = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedProductionGroup.IsNewProductionGroup = false;
        
        delete $scope.unavailableMachineGroups;
        $scope.unavailableMachineGroups = null;
        
        delete $scope.availableMachineGroups;
        $scope.availableMachineGroups = null;
        $state.go('^.new');
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        if ($state.current.name === "ProductionGroups.new") {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('ProductionGroup.Help.Form.Overview')
            });

            steps.push({
                element: "#newProductionGroupAlias",
                intro: translationService.translate('ProductionGroup.Help.Form.Name')
            });

            steps.push({
                element: "#newProductionGroupDescription",
                intro: translationService.translate('ProductionGroup.Help.Form.Description')
            });

            steps.push({
                element: $scope.selectedProductionGroup.IsNewProductionGroup ? "select#newProductionGroupSelectionAlgorithm" : "input#newProductionGroupSelectionAlgorithm",
                intro: translationService.translate('ProductionGroup.Help.Form.ProductionMode')
            });

            steps.push({
                element: "#machineGroupsDiv",
                intro: translationService.translate('ProductionGroup.Help.Form.MachineGroups')
            });

            steps.push({
                element: "#corrugatesDiv",
                intro: translationService.translate('ProductionGroup.Help.Form.Corrugates')
            });

            steps.push({
                element: "#additionalOptionsDiv",
                intro: translationService.translate('ProductionGroup.Help.Form.AdditionalOptions')
            });

            steps.push({
                element: "#newProductionGroupSaveButton",
                intro: translationService.translate('ProductionGroup.Help.Form.Save')
            });

            steps.push({
                element: "#newProductionGroupCancelButton",
                intro: translationService.translate('ProductionGroup.Help.Form.Cancel')
            });
        }
        else {
            var columnDefs = $("[ui-grid-header-cell]");

            steps.push({
                element: "#notFound",
                intro: translationService.translate('ProductionGroup.Help.List.Overview')
            });

            steps.push({
                element: "#newProductionGroup",
                intro: translationService.translate('ProductionGroup.Help.List.New')
            });

            steps.push({
                element: "#deleteProductionGroup",
                intro: translationService.translate('ProductionGroup.Help.List.Delete')
            });

            steps.push({
                element: "#editProductionGroup",
                intro: translationService.translate('ProductionGroup.Help.List.Edit')
            });

            steps.push({
                element: '#productionGroupListTable',
                intro: translationService.translate('ProductionGroup.Help.List.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('ProductionGroup.Help.List.Columns.Name')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('ProductionGroup.Help.List.Columns.Description')
            });

            steps.push({
                element: columnDefs[3],
                intro: translationService.translate('ProductionGroup.Help.List.Columns.ProductionMode')
            });

            steps.push({
                element: columnDefs[4],
                intro: translationService.translate('ProductionGroup.Help.List.Columns.Corrugates')
            });

            steps.push({
                element: columnDefs[5],
                intro: translationService.translate('ProductionGroup.Help.List.Columns.MachineGroups')
            });
        }

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};
