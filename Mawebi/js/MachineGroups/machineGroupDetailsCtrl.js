function MachineGroupDetailsCtrl($scope, $state, $timeout, alertService, introJsService, machineGroupService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    //This allows us to search for this controller within heap snapshots in chrome. It's good for finding leaks.
    function MachineDetailsCtrlTag() { }
    
    $scope.__tag = new MachineDetailsCtrlTag();

    $scope.currentMachineGroup = machineGroupService.getCurrentMachineGroup();
    $scope.machineGroups = machineGroupService.getMachineGroups();
    
    // Not sure if this is the way to do this, getting desperate
    $scope.$watch($scope.isAuto, function () {
        messagingService.publish("GetOrdersHistory", "");
    });

    var exchangeArgumentCodesForReadableErrors = function (error, args) {
        for (var i = 0; i < args.length; i++) {
            try {
                //translate will throw an exception if a translation isn't available. We have localized the most common arguments so we need to only replace the argument if a translation is available.
                args[i] = translationService.translate('MachineError.Message.' + error + ".ErrorCode." + args[i]);
            }
            catch (e) {
            }
        }
    }

    var getReadableErrorCode = function (error) {
        if (!error) {
            return "";
        }

        var displayText = translationService.translate('MachineError.Message.' + error.Error + ".DisplayText");
        var stepsToResolve = translationService.translate('MachineError.Message.' + error.Error + ".StepsToResolve");

        if (error.Arguments && error.Arguments.length > 0 && Enumerable.From(error.Arguments).Any(function (arg) { return arg != "0"; })) {

            exchangeArgumentCodesForReadableErrors(error.Error, error.Arguments);
            var additionalInfo = translationService.translate('MachineError.Message.' + error.Error + ".AdditionalInfo", error.Arguments);

            return displayText + " - " + (additionalInfo !== "" ? additionalInfo + " - " : "") + stepsToResolve;
        }
        return displayText + " - " + stepsToResolve;
    }

    if ($scope.currentMachineGroup == null) {
        $scope.isAuto = false;
        $scope.isPaused = false;
        $scope.isPausing = false;
    }
    else {
        if ($scope.currentMachineGroup.ProductionGroup == undefined) {
            alertService.addError(translationService.translate('MachineProduction.Message.MachineGroupNotAssignedToProductionGroup'));
            $state.go('OperatorConsole.ChangeProductionGroup');
            return;
        }

        $scope.isAuto = $scope.currentMachineGroup.ProductionMode === "AutoProductionMode";
        $scope.isPaused = $scope.currentMachineGroup.CurrentStatus === "MachineGroupPaused" ||
                          $scope.currentMachineGroup.CurrentStatus === "ProduceCurrentAndPause" ||
                          $scope.currentMachineGroup.CurrentStatus === "ProduceQueueAndPause";
        $scope.isPausing = $scope.currentMachineGroup.CurrentStatus === "ProduceCurrentAndPause" ||
                           $scope.currentMachineGroup.CurrentStatus === "ProduceQueueAndPause";

        
        delete $scope.currentMachineGroup.AllMachineCapabilitiesClean;
        $scope.currentMachineGroup.AllMachineCapabilitiesClean = Enumerable.From($scope.currentMachineGroup.AllMachineCapabilities)
                .Where('c => c.DisplayValue.match(/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/) == null')
                .Select('c => c.DisplayValue').Distinct().ToArray();

        // Doing this because for some reason is not reading from $scope
        Enumerable.From($scope.currentMachineGroup.Machines).ForEach(function (m) {
            m.getReadableErrorCode = getReadableErrorCode;
        });
    }


    var fixStoopidGridDrawring = function () {
        $timeout(function () {
            $(window).resize();
        }, 0, true);
    }

    function canChangeMachineStatusPausePlay() {
        if (!$scope.currentMachineGroup) {
            return false;
        }
        if (!$scope.currentMachineGroup.ProductionGroup) {
            return false;
        }

        var allMachinesOnline = Enumerable.From($scope.currentMachineGroup.Machines).All("c => c.Data.CurrentStatus == 'MachineOnline'");
        if (allMachinesOnline) {
            return $scope.currentMachineGroup.CurrentStatus === 'MachineGroupPaused' ||
                   $scope.currentMachineGroup.CurrentStatus === 'MachineGroupOnline' ||
                   $scope.currentMachineGroup.CurrentStatus === 'ProduceCurrentAndPause' ||
                   $scope.currentMachineGroup.CurrentStatus === "ProduceQueueAndPause";
        }
        return false;
    };

    function canChangeMachineStatus() {
        if (!$scope.currentMachineGroup) {
            return false;
        }

        if (!$scope.currentMachineGroup.ProductionGroup) {
            return false;
        }

        var allMachinesOnlineOrPaused = Enumerable.From($scope.currentMachineGroup.Machines).All("c => c.Data.CurrentStatus == 'MachineOnline'  || c.Data.CurrentStatus == 'MachinePaused'");
        if (allMachinesOnlineOrPaused) {
            return $scope.currentMachineGroup.CurrentStatus === 'MachineGroupPaused' ||
                   $scope.currentMachineGroup.CurrentStatus === 'MachineGroupOnline' ||
                   $scope.currentMachineGroup.CurrentStatus === 'ProduceCurrentAndPause' ||
                   $scope.currentMachineGroup.CurrentStatus === "ProduceQueueAndPause";
        }
        return false;
    };
    
    machineGroupService.currentMachineGroupUpdatedObservable
        .autoSubscribe(function (machine) {
            delete $scope.currentMachineGroup;
            $scope.currentMachineGroup = machine;
            $timeout(function () {

                    if ($scope.currentMachineGroup) {
                        $scope.isAuto = $scope.currentMachineGroup.ProductionMode === "AutoProductionMode";
                        $scope.isPaused = $scope.currentMachineGroup.CurrentStatus === "MachineGroupPaused" ||
                            $scope.currentMachineGroup.CurrentStatus === "ProduceCurrentAndPause" ||
                            $scope.currentMachineGroup.CurrentStatus === "ProduceQueueAndPause";
                        $scope.isPausing = $scope.currentMachineGroup.CurrentStatus === "ProduceCurrentAndPause" ||
                           $scope.currentMachineGroup.CurrentStatus === "ProduceQueueAndPause";
                        $scope.CanChangeMachineStatus = canChangeMachineStatus();
                        $scope.CanChangeMachineStatusPausePlay = canChangeMachineStatusPausePlay();

                        delete $scope.currentMachineGroup.AllMachineCapabilitiesClean;
                        $scope.currentMachineGroup.AllMachineCapabilitiesClean = Enumerable
                            .From($scope.currentMachineGroup.AllMachineCapabilities)
                            .Where('c => c.DisplayValue.match(/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/) == null')
                            .Select('c => c.DisplayValue')
                            .Distinct()
                            .ToArray();

                        // Doing this because for some reason is not reading from $scope
                        Enumerable.From($scope.currentMachineGroup.Machines)
                            .ForEach(function (m) {
                                m.getReadableErrorCode = getReadableErrorCode;
                            });
                    }
                },
                0,
                true
            );

        }, $scope, 'Current Machine updated');

    machineGroupService.machineGroupsUpdatedObservable
        .autoSubscribe(function (machineGroups) {
            $timeout(function () {
                delete $scope.machineGroups;
                $scope.machineGroups = machineGroups;
                $scope.CanChangeMachineStatus = canChangeMachineStatus();
                $scope.CanChangeMachineStatusPausePlay = canChangeMachineStatusPausePlay();
            }, 0, true);
        }, $scope, 'Machines Updated');

    $scope.CanChangeMachineStatus = canChangeMachineStatus();
    $scope.CanChangeMachineStatusPausePlay = canChangeMachineStatusPausePlay();

    $scope.changeMachineProductionStatus = function (id, state) {
        messagingService.publish("ChangeMachineProductionStatus", "{'Id': '" + id + "', 'Status': '" + state + "'}");
    };

    $scope.$watch('isPaused', function (value) {
        if (!canChangeMachineStatus() || !$scope.currentMachineGroup) {
            return;
        }

        var newStatus = value ? 'MachineGroupPaused' : 'MachineGroupOnline';

        if ($scope.currentMachineGroup.CurrentStatus === newStatus) {
            return;
        }

        if ($scope.currentMachineGroup.CurrentStatus !== 'MachineGroupOnline' && $scope.currentMachineGroup.CurrentStatus !== 'MachineGroupPaused') {
            return;
        }

        if ($scope.currentMachineGroup.ProductionGroup != undefined ) {
            
            delete $scope.currentMachineGroup.ProductionGroup.MachineGroups;
            $scope.currentMachineGroup.ProductionGroup.MachineGroups = [];
        }

        messagingService.publish("ChangeMachineGroupStatus", $scope.currentMachineGroup);
    });

    $scope.$watch('isAuto', function (value) {
        if (!canChangeMachineStatus()) {
            return;
        }

        var newStatus;
        if (value) {
            newStatus = 'AutoProductionMode';
        }
        else {
            newStatus = 'ManualProductionMode';
        }

        if ($scope.currentMachineGroup.ProductionMode === newStatus) {
            return;
        }
        
        delete $scope.currentMachineGroup.ProductionMode;
        $scope.currentMachineGroup.ProductionMode = newStatus;

        var simplifiedObject = { // Getting a circular reference
            Id: $scope.currentMachineGroup.Id,
            Alias: $scope.currentMachineGroup.Alias,
            ProductionMode: $scope.currentMachineGroup.ProductionMode
    } 
        messagingService.publish("ChangeMachineGroupProductionMode", simplifiedObject);

        $timeout(function () {
            messagingService.publish("GetOrders");
            messagingService.publish("GetOrdersHistory");
        }, 500);

        //UI-grid is not resizing correctly either :-). Old NG grid issue: https://github.com/angular-ui/ng-grid/issues/50
        fixStoopidGridDrawring();
    });

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        steps.push({
            element: "#notFound",
            intro: translationService.translate('MachineProduction.Help.Overview')
        });

        steps.push({
            element: "#currentMachineGroupAlias",
            intro: translationService.translate('MachineProduction.Help.MachineGroup')
        });

        steps.push({
            element: "#currentMachineGroupAssignedOperator",
            intro: translationService.translate('MachineProduction.Help.User')
        });

        steps.push({
            element: "#currentMachineGroupStatus",
            intro: translationService.translate('MachineProduction.Help.Status')
        });

        if (canChangeMachineStatus()) {
            steps.push({
                element: "#playPauseSwitch",
                intro: translationService.translate('MachineProduction.Help.PausePlay')
            });

            steps.push({
                element: "#autoManualSwitch",
                intro: translationService.translate('MachineProduction.Help.AutoManual')
            });
        }

        if ($scope.shouldShowWaveView()) {
            if ($scope.shouldShowTriggerInterface()) {
                steps.push({
                    element: "[name='triggeredSerialNo']",
                    intro: translationService.translate('MachineProduction.Wave.Help.BarcodeData')
                });

                steps.push({
                    element: "#triggerInterfaceSubmitBtn",
                    intro: translationService.translate('MachineProduction.Wave.Help.TriggerData')
                });
            }

            steps.push({
                element: "#autoOperatorPanelJobsSent",
                intro: translationService.translate('MachineProduction.Help.SentJob')
            });

            steps.push({
                element: "#autoOperatorPanelCompletedJobs",
                intro: translationService.translate('MachineProduction.Help.CompletedJob')
            });
        }

        if ($scope.shouldShowOrderView()) {
            steps.push({
                element: "#operatorOpenOrders",
                intro: translationService.translate('MachineProduction.Order.Help.OpenOrder.Overview')
            });

            steps.push({
                element: "#openOrdersWidget #orderFilter",
                intro: translationService.translate('MachineProduction.Order.Help.OpenOrder.Filter')
            });

            steps.push({
                element: "#moveOrdersToNext",
                intro: translationService.translate('MachineProduction.Order.Help.OpenOrder.CreateNext')
            });

            steps.push({
                element: "#removeOrders",
                intro: translationService.translate('MachineProduction.Order.Help.OpenOrder.Cancel')
            });

            steps.push({
                element: "#manualOperatorPanelJobsSent",
                intro: translationService.translate('MachineProduction.Help.SentJob')
            });

            steps.push({
                element: "#manualOperatorPanelCompletedJobs",
                intro: translationService.translate('MachineProduction.Order.Help.ClosedOrder.Overview')
            });

            steps.push({
                element: "#ordersHistory #orderFilter",
                intro: translationService.translate('MachineProduction.Order.Help.ClosedOrder.Filter')
            });

            steps.push({
                element: "#ordersHistorySortyBy",
                intro: translationService.translate('MachineProduction.Order.Help.ClosedOrder.SortBy')
            });
        }

        if ($scope.shouldShowMachineGroupDetails()) {
            steps.push({
                element: "#productionGroupDetails",
                intro: translationService.translate('MachineProduction.Help.ProductionGroup')
            });

            steps.push({
                element: "#configuredMachinesDetails",
                intro: translationService.translate('MachineProduction.Help.ConfiguredMachines')
            });

            steps.push({
                element: "#machineGroupCapabilitiesDetails",
                intro: translationService.translate('MachineProduction.Help.Capabilities')
            });
        }

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
}

app.directive('machineDetails', function () {
    return {
        restrict: 'E',

        scope: {
            updateTabIcon: "&"
        },
        templateUrl: 'partials/Operator/machineDetails.html'
    };
});