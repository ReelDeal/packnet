function MachineGroupsCtrl($location, $log, $scope, $state, $timeout, alertService, introJsService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);
   
    $scope.assignedMachines = [];
    $scope.availableMachines = [];
    $scope.unavailableMachines = [];
    $scope.machineGroups = [];
    $scope.machineGroupWorkflowPaths = [];
    $scope.selectedMachineGroup = {};
    $scope.selectedMachineGroup.IsNewMachineGroup = true;
    $scope.labelUnavailableMachines = '';
    $scope.showUnavailableMachines = false;

    var awaitingUpdate = false;

    $scope.$on('$locationChangeSuccess', function () {
        if ($scope.selectedMachineGroup && $scope.selectedMachineGroup.IsNewMachineGroup == false && $location.path() == '/MachineGroups') {
            $scope.cancelNewEditMachineGroup();
        }
    });

    $scope.$watch('selectedMachineGroup', function () {
        if ($scope.currentMachineGroup) {
        
        delete $scope.selectedMachineGroup.AllMachineCapabilitiesClean;
        $scope.selectedMachineGroup.AllMachineCapabilitiesClean = Enumerable.From($scope.selectedMachineGroup.AllMachineCapabilities)
            .Where('c => c.DisplayValue.match(/^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/) == null')
            .Select('c => c.DisplayValue').Distinct().ToArray();
        }
    });

    $scope.$watch('showUnavailableMachines', function (newValue) {
        if (newValue) {
            delete $scope.labelUnavailableMachines;
            $scope.labelUnavailableMachines = translationService.translate('MachineGroup.Label.HideUnavailableMachines');
        }
        else {
            delete $scope.labelUnavailableMachines;
            $scope.labelUnavailableMachines = translationService.translate('MachineGroup.Label.ShowUnavailableMachines');
        }
    });

    /**
* Compares two arrays. 
* @param {array} originalCollection
* @param {array} newCollection
* @param {string} key: object key to compare
* @param {string} compareProperty: if we need to compare to a different property beside the key.
* @returns {object} Object with two properties: shouldUpdate and collection.
*/
    var checkIfOriginalCollectionShouldBeUpdate = function (originalCollection, newCollection, key, compareProperty, compareAllProperties) {
        // Make sure the original collection has data and the size of both arrays are the same.
        if (compareAllProperties) {
            var item1String = angular.toJson(originalCollection);
            var item2String = angular.toJson(newCollection);
            return { shouldUpdate: item1String !== item2String, collection: newCollection };
        }

        if (originalCollection != null && originalCollection.length == newCollection.length) {

            // Both Collections have the same size, lets check if they are the same items.
            var originalEnum = Enumerable.From(originalCollection);
            var newCollectionEnum = Enumerable.From(newCollection);

            // Make sure all items from one collection exist in the other one
            if (originalEnum.All(function (originalItem) {
                // Get the item to compare by key
                var itemToCompare = newCollectionEnum.FirstOrDefault(null, function (i) { return originalItem[key] == i[key]; });

                // If value doesn't exist on the other collection, the collections are not the same.
                if (itemToCompare == null) {
                    return false;
            }

                // if compareProperty is undefined, we only care for the key.
                if (compareProperty == undefined) {
                    return true;
            }

                // Compare the property
                return originalItem[compareProperty] == itemToCompare[compareProperty];
            }
                )) {
                // As both are the same we don't need to do anything, if we need to update some value we should do it here.
                return { shouldUpdate: false, collection: null };
            }
        }

        // Items are not the same, we should update
        return { shouldUpdate: true, collection: newCollection };
    };

    messagingService.machineGroupsObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            var machineGroups = checkIfOriginalCollectionShouldBeUpdate($scope.machineGroups, msg.Data, 'Id', undefined, false);
            if (machineGroups.shouldUpdate) {
                delete $scope.machineGroups;
                $scope.machineGroups = msg.Data;

                $scope.machineGroups.forEach(function (machineGroup) {
                    machineGroup.WorkflowPathToDisplay = machineGroup.WorkflowPath.replace(/^.*[\\\/]/, '').replace('.xaml', '');
                            machineGroup.FriendlyMachines = convertMachinesToFriendlyFormat(machineGroup.ConfiguredMachines);
                });

                
                delete $scope.assignedMachines;
                $scope.assignedMachines = Enumerable.From(msg.Data).SelectMany('$.ConfiguredMachines',
                    // ($) - the parent object
                    // ($$) - the selected object
                    '{ MachineId: $$, MachineGroupId: $.Id, MachineGroupAlias: $.Alias}'
                ).ToArray();
            }
        }, 100);
    }, $scope, 'MachineGroups');

    messagingService.productionGroupsObservable.autoSubscribe(function (msg) {

        messagingService.publish("GetMachines");
        if (!$scope.selectedMachineGroup) {
            return;
        }

        delete $scope.productionGroup;
        $scope.productionGroup = Enumerable.From(msg.Data).FirstOrDefault(null, function (pg) {
            return Enumerable.From(pg.ConfiguredMachineGroups).Any(function (mgId) {
                return mgId === $scope.selectedMachineGroup.Id;
            });
        });
    }, $scope, 'ProductionGroups');

    messagingService.machineGroupCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('MachineGroup.Label.NameSingular'), $scope.machineGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('MachineGroup.Label.NameSingular'), $scope.machineGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('MachineGroup.Label.NameSingular'), $scope.machineGroupBeingSaved.Alias), 5000);
        }
    }, $scope, 'MachineGroup Created');

    messagingService.machineGroupUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            var additionalInfo = msg.Message != null ? msg.Message.split(",") : [""];

            if (additionalInfo[0] === "MachineGroupInProduction")
                alertService.addError(translationService.translate('MachineGroup.Message.UpdateInProduction', $scope.machineGroupBeingSaved.Alias));
            else
                alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('MachineGroup.Label.NameSingular'), $scope.machineGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('MachineGroup.Label.NameSingular'), $scope.machineGroupBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('MachineGroup.Label.NameSingular'), $scope.machineGroupBeingSaved.Alias), 5000);
        }
    }, $scope, 'MachineGroup Updated');

    messagingService.machineGroupDeletedObservable.autoSubscribe(function (msg) {
        if (msg.Result === "Fail") {
            var additionalInfo = msg.Message != null ? msg.Message.split(",") : [""];

            if (additionalInfo[0] === "MachineGroupInProduction")
                alertService.addError(translationService.translate('MachineGroup.Message.DeleteInProduction', msg.Data.Alias));
            else
                alertService.addError(translationService.translate('Common.Message.DeleteFailed', translationService.translate('MachineGroup.Label.NameSingular'), msg.Data.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('MachineGroup.Message.DeleteRelationshipExists', msg.Data.Alias));
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', translationService.translate('MachineGroup.Label.NameSingular'), msg.Data.Alias), 5000);
            
            delete $scope.machineGroupBeingSaved;
            $scope.machineGroupBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedMachineGroup;
            $scope.selectedMachineGroup = {};
            $scope.selectedMachineGroup.IsNewMachineGroup = true;
        }
        requestData();
    }, $scope, 'Machine Group Deleted');

    function convertMachinesToFriendlyFormat(machines) {
        if (machines == undefined || $scope.machines == undefined) return "";

        var aliases = Enumerable.From($scope.machines)
            .Where(function (m) { return machines.indexOf(m.Id) > -1; })
            .Select(function (m) { return m.Alias; })
            .ToArray();

        var friendlyMessage = "";
        for (var i = 0; i < aliases.length; i++) {
            friendlyMessage = friendlyMessage + aliases[i];
            if (i != aliases.length - 1) friendlyMessage = friendlyMessage + ", ";
        }

        return friendlyMessage;
    }

    messagingService.machinesObservable.autoSubscribe(function (msg) {
        if (!$scope.selectedMachineGroup) {
            $state.go("SelectMachineGroup");
            return;
        }
        $timeout(function () {
            var machines = checkIfOriginalCollectionShouldBeUpdate($scope.machines,
                Enumerable.From(msg.Data)
                .Select(function (machine) {
                    if ($scope.selectedMachineGroup) {
                        machine.Data.Included = Enumerable.From($scope.selectedMachineGroup.ConfiguredMachines)
                            .Any(function (configured) {
                                return machine.Id === configured;
                            });
                    }
                    return machine.Data;
                })
                .ToArray(),
                'Id',
                true,
                true);

            if (machines.shouldUpdate) {
                
                delete $scope.machines;
                $scope.machines = machines.collection;
            }

            var unavailableMachines = checkIfOriginalCollectionShouldBeUpdate($scope.unavailableMachines,
                Enumerable
                .From($scope.assignedMachines)
                .Except(Enumerable.From($scope.selectedMachineGroup.ConfiguredMachines).Select('{MachineId: $}'),
                    '$.MachineId')
                .Select(function (item) {
                    return {
                        MachineId : item.MachineId,
                        MachineAlias : Enumerable.From($scope.machines)
                            .Where(function (m) { return m.Id == item.MachineId; })
                            .Select('$.Alias')
                            .FirstOrDefault(),
                        MachineType : Enumerable.From($scope.machines)
                            .Where(function (m) { return m.Id == item.MachineId; })
                            .Select('$.MachineType')
                            .FirstOrDefault(),
                        MachineGroupId : item.MachineGroupId,
                        MachineGroupAlias : item.MachineGroupAlias
                    };
                })
                .ToArray(),
                'MachineId',
                'MachineGroupId'); // Should I add MachineId check?

            if (unavailableMachines.shouldUpdate) {
                
                delete $scope.unavailableMachines;
                $scope.unavailableMachines = unavailableMachines.collection;
        }

            var availableMachines = checkIfOriginalCollectionShouldBeUpdate($scope.availableMachines, Enumerable
            .From($scope.machines)
            .Where(function (m) {
                return m.MachineType != 'ExternalCustomerSystem';
            })
            .Except(Enumerable.From($scope.unavailableMachines).Select('{Id: $.MachineId}'), '$.Id')
            .Select(function (item) {
                return {
                        Id : item.Id,
                        Alias : item.Alias,
                        MachineType : item.MachineType,
                        CurrentCapabilities : item.CurrentCapabilities,
                        Included : item.Included
                };
            })
                .ToArray(), 'Id');
            if (availableMachines.shouldUpdate) {
                delete $scope.availableMachines;
                $scope.availableMachines = availableMachines.collection;
            }

            $scope.machineGroups.forEach(function (machineGroup) {
                machineGroup.FriendlyMachines = convertMachinesToFriendlyFormat(machineGroup.ConfiguredMachines);
            });
        }, 100);
    }, $scope, 'Machines');

    messagingService.machineGroupWorkflowPathsObservable.autoSubscribe(function (msg) {
        delete $scope.machineGroupWorkflowPaths;
        $scope.machineGroupWorkflowPaths = msg.Data;
    }, $scope, 'Machine Group Workflow Path');

    $scope.toggleUnavailableMachines = function () {
        $scope.showUnavailableMachines = !$scope.showUnavailableMachines;
    }

    var requestData = function () {
        messagingService.publish("GetMachineGroups");
        messagingService.publish("GetProductionGroups");
        messagingService.publish("GetMachineGroupWorkflowPaths");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'machineGroups',
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                width: 200
            },
            {
                field: 'Description',
                name: translationService.translate('Common.Label.Description'),
                width: 150
            },
            {
                field: 'MaxQueueLength',
                name: translationService.translate('MachineGroup.Label.QueueLength'),
                width: 150
            },
            {
                field: 'WorkflowPathToDisplay',
                name: translationService.translate('MachineGroup.Label.WorkflowPath'),
                width: 300
            },
            {
                field: 'FriendlyMachines',
                name: translationService.translate('MachineGroup.Label.ConfiguredMachines'),
                width: '*',
                minWidth: 50
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
        }
    };

    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.effectiveCapabilities = function () {
        return Enumerable.From($scope.availableMachines).Where("m => m.Included").SelectMany("m => m.CurrentCapabilities").ToArray();
    }

    $scope.saveMachineGroup = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        delete $scope.machineGroupBeingSaved;
        $scope.machineGroupBeingSaved = {
            Id: $scope.selectedMachineGroup.Id,
            Alias: $scope.selectedMachineGroup.Alias,
            Description: $scope.selectedMachineGroup.Description,
            WorkflowPath: $scope.selectedMachineGroup.WorkflowPath,
            MaxQueueLength: $scope.selectedMachineGroup.MaxQueueLength,
            ConfiguredMachines: Enumerable.From($scope.availableMachines).Where("m => m.Included").Select("m => m.Id").ToArray(),
            IsNewMachineGroup: $scope.selectedMachineGroup.IsNewMachineGroup
        };

        var action = "CreateMachineGroup";
        if (!$scope.machineGroupBeingSaved.IsNewMachineGroup)
            action = "UpdateMachineGroup";
        messagingService.publish(action, $scope.machineGroupBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('MachineGroup.Label.NameSingular'), $scope.machineGroupBeingSaved.Alias));
                awaitingUpdate = false;
                delete $scope.machineGroupBeingSaved;
                $scope.machineGroupBeingSaved = {};
                delete $scope.selectedMachineGroup;
                $scope.selectedMachineGroup = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditMachineGroup = function () {
        $state.go('^.list', null, { reload: true });
    }

    $scope.deleteMachineGroup = function () {
        messagingService.publish("DeleteMachineGroup", $scope.gridApi.selection.getSelectedRows());
        $scope.gridApi.selection.clearSelectedRows();
    }

    $scope.editMachineGroup = function (){
        delete $scope.selectedMachineGroup;
        $scope.selectedMachineGroup = $scope.gridApi.selection.getSelectedRows()[0];
        delete $scope.machines;
        $scope.machines = null;
        delete $scope.availableMachines;
        $scope.availableMachines = null;
        delete $scope.unavailableMachines;
        $scope.unavailableMachines = null;
        $scope.selectedMachineGroup.IsNewMachineGroup = false;
        $state.go('^.new');
    }

    var exchangeArgumentCodesForReadableErrors = function (error, args) {
        for (var i = 0; i < args.length; i++) {
            try {
                //translate will throw an exception if a translation isn't available. We have localized the most common arguments so we need to only replace the argument if a translation is available.
                args[i] = translationService.translate('MachineError.Message.' + error + ".ErrorCode." + args[i]);
            }
            catch (e) {
            } 
        }
    }

    $scope.GetReadableErrorCode = function (error) {
        if (!error) {
            return "";
            }

        var displayText = translationService.translate('MachineError.Message.' + error.Error + ".DisplayText");
        var stepsToResolve = translationService.translate('MachineError.Message.' + error.Error + ".StepsToResolve");

        if (error.Arguments && error.Arguments.length > 0 && Enumerable.From(error.Arguments).Any(function (arg) { return arg != "0"; })) {

            exchangeArgumentCodesForReadableErrors(error.Error, error.Arguments);
            var additionalInfo = translationService.translate('MachineError.Message.' + error.Error + ".AdditionalInfo", error.Arguments);

            return displayText + " - " + (additionalInfo !== "" ? additionalInfo + " - " : "") + stepsToResolve;
        }
        return displayText + " - " + stepsToResolve;
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        var columnDefs = $("[ui-grid-header-cell]");

        if ($state.current.name === 'MachineGroups.new') {
            steps.push({
                element: '#doesntExist',
                intro: translationService.translate('MachineGroup.Help.Form.Overview')
            });

            steps.push({
                element: '#newMachineGroupAlias',
                intro: translationService.translate('MachineGroup.Help.Form.Name')
            });

            steps.push({
                element: '#newMachineDescription',
                intro: translationService.translate('MachineGroup.Help.Form.Description')
            });

            steps.push({
                element: '#newMachineGroupWorkflowPath',
                intro: translationService.translate('MachineGroup.Help.Form.WorkflowPath')
            });

            steps.push({
                element: '#nudHorizontalTable',
                intro: translationService.translate('MachineGroup.Help.Form.QueueLength')
            });

            steps.push({
                element: '#newMachineGroupMachinesList',
                intro: translationService.translate('MachineGroup.Help.Form.Machines')
            });

            steps.push({
                element: '#newMachineGroupSaveButton',
                intro: translationService.translate('MachineGroup.Help.Form.Save')
            });

            steps.push({
                element: '#newMachineGroupCancelButton',
                intro: translationService.translate('MachineGroup.Help.Form.Cancel')
            });

            steps.push({
                element: '#newEffectiveCapabilities',
                intro: translationService.translate('MachineGroup.Help.Form.EffectiveCapabilities')
            });
        }
        else {
            steps.push({
                element: '#doesntExist',
                intro: translationService.translate('MachineGroup.Help.List.Overview')
            });

            steps.push({
                element: '#newMachineGroup',
                intro: translationService.translate('MachineGroup.Help.List.New')
            });

            steps.push({
                element: '#deleteMachineGroup',
                intro: translationService.translate('MachineGroup.Help.List.Delete')
            });

            steps.push({
                element: '#editMachineGroup',
                intro: translationService.translate('MachineGroup.Help.List.Edit')
            });

            steps.push({
                element: '#machineGroupListTable',
                intro: translationService.translate('MachineGroup.Help.List.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('MachineGroup.Help.List.Columns.MachineGroup')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('MachineGroup.Help.List.Columns.Description')
            });

            steps.push({
                element: columnDefs[3],
                intro: translationService.translate('MachineGroup.Help.List.Columns.QueueLength')
            });

            steps.push({
                element: columnDefs[4],
                intro: translationService.translate('MachineGroup.Help.List.Columns.WorkflowPath')
            });

            steps.push({
                element: columnDefs[5],
                intro: translationService.translate('MachineGroup.Help.List.Columns.ConfiguredMachines')
            });
        }

        return steps;
    }

    // This controller is also used in other pages besides the administration ones
    if ($state.current.name === 'MachineGroups.list' || $state.current.name === 'MachineGroups.new') {
        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage, true);	
    }
};