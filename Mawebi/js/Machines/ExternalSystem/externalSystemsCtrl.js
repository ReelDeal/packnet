function ExternalSystemsCtrl($scope, $state, $timeout, alertService, introJsService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    var awaitingUpdate = false;
    $scope.workflowPaths = [];

    var externalCustomerSystemUpdatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ExternalCustomerSystemUpdated"));

    var externalCustomerSystemCreatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ExternalCustomerSystemCreated"));

    var externalCustomerSystemWorkflowsObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ExternalCustomerSystemWorkflows"));

    var externalCustomerSystemsObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ExternalCustomerSystems"));

    externalCustomerSystemWorkflowsObservable.autoSubscribe(function (msg) {
        
        delete $scope.workflowPaths;
        $scope.workflowPaths = msg.Data;
    }, $scope, 'External System Workflows');

    externalCustomerSystemCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('Machine.ExternalSystem.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.ExternalSystem.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('Machine.ExternalSystem.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'External System Created');

    externalCustomerSystemUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Machine.ExternalSystem.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.ExternalSystem.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Machine.ExternalSystem.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'Machine Updated');

    $scope.saveExternalSystem = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        
        delete $scope.machineBeingSaved;
        $scope.machineBeingSaved = {
            Id: $scope.selectedMachine.Id,
            Alias: $scope.selectedMachine.Alias,
            Description: $scope.selectedMachine.Description,
            IpOrDnsName: $scope.selectedMachine.Data.IpOrDnsName,
            Port: $scope.selectedMachine.Data.Port,
            WorkflowPath: $scope.selectedMachine.Data.WorkflowPath,
            IsNewMachine: $scope.selectedMachine.IsNewMachine
        };

        var action = "CreateExternalCustomerSystem";
        if (!$scope.machineBeingSaved.IsNewMachine)
            action = "UpdateExternalCustomerSystem";
        messagingService.publish(action, $scope.machineBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.ExternalSystem.Label.NameSingular'), $scope.machineBeingSaved.Alias));
                awaitingUpdate = false;
                
                delete $scope.machineBeingSaved;
                $scope.machineBeingSaved = {};
                
                delete $scope.selectedMachine;
                $scope.selectedMachine = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditExternalSystem = function () {
    	$state.go('^.list', null, { reload: true });
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        steps.push({
            element: "#doesntExist",
            intro: translationService.translate('Machine.ExternalSystem.Help.Form.Overview')
        });

        steps.push({
            element: "#newExternalSystemAlias",
            intro: translationService.translate('Machine.ExternalSystem.Help.Form.Name')
        });

        steps.push({
        	element: "#newExternalSystemDescription",
        	intro: translationService.translate('Machine.Help.Form.Description')
        });

        steps.push({
        	element: "#newExternalSystemIPAddress",
        	intro: translationService.translate('Machine.Help.Form.IpOrDnsName')
        });

        steps.push({
        	element: "#nudHorizontalTable",
        	intro: translationService.translate('Machine.Help.Form.Port')
        });

        steps.push({
        	element: "#newExternalSystemWorkflowPath",
        	intro: translationService.translate('Machine.ExternalSystem.Help.Form.Workflow')
        });

        steps.push({
            element: "#newExternalSystemSaveButton",
            intro: translationService.translate('Machine.Help.Form.Save')
        });

        steps.push({
            element: "#newExternalSystemCancelButton",
            intro: translationService.translate('Machine.Help.Form.Cancel')
        });

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
    messagingService.publish("GetExternalCustomerSystemWorkflows");
};