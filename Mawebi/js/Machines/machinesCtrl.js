function networkMachineResponseHandler($scope, alertService, translationService, machine, response) {
    // TODO: This should be isolated in a Helper, not as a global function
    if (!machine) {
        return;
    }

    if (response === "IpAddressAlreadyInUse") {
        if (machine.IpOrDnsName != undefined) {
            alertService.addError(translationService.translate('Machine.Message.IPAddressAndPortAlreadyAssigned', machine.IpOrDnsName, machine.Port));
        }
        else {
            alertService.addError(translationService.translate('Machine.Message.PortAlreadyAssigned', machine.Port));
        }
    }

    if (response === "InvalidNetworkIpOrDnsValue") {
        alertService.addError(translationService.translate('Machine.Message.InvalidIPAddress', machine.IpOrDnsName));
    }

    if (response === "PortOutOfRange") {
        alertService.addError(translationService.translate('Machine.Message.PortOutOfRange', machine.Port));
    }
}

function MachinesCtrl($location, $filter, $rootScope, $scope, $state, $timeout, alertService, introJsService, messagingService, translationService, uiGridConstants) {
    $timeout = $timeout.getInstance($scope);

    $scope.machine = [];
    $scope.selectedMachine = {};
    $scope.selectedMachine.IsNewMachine = true;

    $scope.$on('$locationChangeSuccess', function () {
        if ($scope.selectedMachine && $scope.selectedMachine.IsNewMachine == false && $location.path() == '/Machines') {
            $state.go('^.list', null, { reload: true });
        }
    });

    messagingService.machinesObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            if ($scope.machine && $scope.machine.length === msg.Data.length) {
                var machines = Enumerable.From($scope.machine).Select('$.Id');
                var dataIds = Enumerable.From(msg.Data).Select('$.Id');

                if (dataIds.All(function (c) { return machines.Contains(c); })) {
                    var ms = Enumerable.From(msg.Data);
                    $scope.machine.forEach(function (machine) {
                        var m = ms.Single(function (s) { return s.Id === machine.Id; });
                        machine.Data.CurrentStatus = m.Data.CurrentStatus;
                        machine.Data.CurrentProductionStatus = m.Data.CurrentProductionStatus;
                    });
                    return; //no machines added or removed
                }
            }
            
            delete $scope.machine;
            $scope.machine = msg.Data;
        }, 0, true);

    }, $scope, 'Machine');

    $scope.requestData = function () {
        messagingService.publish("GetMachines");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, $scope.requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'machine',
        columnDefs: [
            {
                displayName: translationService.translate('Common.Label.Name'),
                field: 'Alias',
                width: '200',
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                displayName: translationService.translate('Machine.Label.Type'),
                field: 'MachineType',
                cellFilter: 'machineType',
                width: '150'
            },
            {
                displayName: translationService.translate('Common.Label.Description'),
                field: 'Description',
                width: '*'
            },
            {
                displayName: translationService.translate('Common.Label.Status'),
                field: 'Data.CurrentStatus',
                cellFilter: 'machineStatus',
                width: '200'
            },
            {
                displayName: translationService.translate('Common.Label.ProductionStatus'),
                field: 'Data.CurrentProductionStatus',
                cellFilter: 'machineProductionStatus',
                width: '200'
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            delete $scope.gridApi;
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
        }
    };

    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    messagingService.machineDeletedObservable.autoSubscribe(function (msg) {
        var machineType = $filter("machineType")(msg.Data.MachineType);

        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.DeleteFailed', machineType, msg.Data.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Machine.Message.DeleteRelationshipExists', msg.Data.Alias));
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', machineType, msg.Data.Alias), 5000);
            delete $scope.machineBeingSaved;
            delete $scope.selectedMachine;
            
            delete $scope.machineBeingSaved;
            $scope.machineBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedMachine;
            $scope.selectedMachine = {};
            $scope.selectedMachine.IsNewMachine = true;
        }
        $scope.requestData();
    }, $scope, 'Machine Deleted');

    $scope.deleteMachines = function () {
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (machine) {
            //MachineType is set based on MachineTypes and PrintMachineTypes Enumeration
            if (machine.MachineType === "Fusion") {
                messagingService.publish("DeleteFusionMachine", machine);
            }
            else if (machine.MachineType === "Em") {
                messagingService.publish("DeleteEmMachine", machine);
            }
            else if (machine.MachineType === "ZebraPrinter") {
                messagingService.publish("DeleteZebraPrinter", machine);
            }
            else if (machine.MachineType === "WifiBarcodeScanner") {
                messagingService.publish("DeleteNetworkScanner", machine);
            }
            else if (machine.MachineType === "ExternalCustomerSystem") {
                messagingService.publish("DeleteExternalCustomerSystem", machine);
            }
        });

        $scope.gridApi.selection.clearSelectedRows();
    }

    $scope.editMachine = function (){
        delete $scope.selectedMachine;
        
        delete $scope.selectedMachine;
        $scope.selectedMachine = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedMachine.IsNewMachine = false;

        //MachineType is set based on MachineTypes and PrintMachineTypes Enumeration
        if ($scope.selectedMachine.MachineType === "Fusion") {
            $state.go('^.newFusion');
        }
        else if ($scope.selectedMachine.MachineType === "Em") {
            $state.go('^.newEm');
        }
        else if ($scope.selectedMachine.MachineType === "ZebraPrinter") {
            $state.go('^.newZebra');
        }
        else if ($scope.selectedMachine.MachineType === "WifiBarcodeScanner") {
            $state.go('^.newBarcodeScanner');
        }
        else if ($scope.selectedMachine.MachineType === "ExternalCustomerSystem") {
            $state.go('^.newExternalSystem');
        }
    }

    $scope.defaultOptions = function () {
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        steps.push({
            element: "#notFound",
            intro: translationService.translate('Machine.Help.List.Overview')
        });

        steps.push({
            element: "#newMachineButton",
            intro: translationService.translate('Machine.Help.List.New')
        });

        steps.push({
            element: "#deleteMachines",
            intro: translationService.translate('Machine.Help.List.Delete')
        });

        steps.push({
            element: "#editMachine",
            intro: translationService.translate('Machine.Help.List.Edit')
        });

        steps.push({
            element: '#machinesListTable',
            intro: translationService.translate('Machine.Help.List.Columns.Overview')
        });

        steps.push({
            element: columnDefs[1],
            intro: translationService.translate('Machine.Help.List.Columns.MachineName')
        });

        steps.push({
            element: columnDefs[2],
            intro: translationService.translate('Machine.Help.List.Columns.MachineType')
        });

        steps.push({
            element: columnDefs[3],
            intro: translationService.translate('Machine.Help.List.Columns.Description')
        });

        steps.push({
            element: columnDefs[4],
            intro: translationService.translate('Machine.Help.List.Columns.Status')
        });

        steps.push({
            element: columnDefs[5],
            intro: translationService.translate('Machine.Help.List.Columns.ProductionStatus')
        });

        return steps;
    }

    $scope.tellMeAboutThisPage = function () {

        var steps = [];

        if (angular.lowercase($state.current.name) === 'machines.list') {
            steps = $scope.defaultOptions();
        }

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};

function DeleteMachineOpenDialogCtrl($modal, $scope, alertService){

    $scope.opts = {
        backdrop: true,
        keyboard: true,
        templateUrl: 'partials/Machines/deleteMachineDialog.html',
        controller: 'DeleteMachineDialogCtrl',
        windowClass: "smallDialogWindow"
    };

    $scope.openDialog = function () {
        alertService.clearAll();
        $modal.open($scope.opts).result.then(function () {
            $scope.deleteMachines();
        });
    };
}

function DeleteMachineDialogCtrl($modalInstance, $scope){

    $scope.closeWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.confirmDelete = function () {
        $modalInstance.close();
    }
};

app.directive('deleteMachineButton', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/Machines/deleteMachineButton.html'
    };
});