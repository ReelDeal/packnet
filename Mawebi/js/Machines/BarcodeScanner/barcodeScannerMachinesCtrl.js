function BarcodeScannerMachinesCtrl($scope, $state, $timeout, alertService, introJsService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.showAdvanceSettings = false;
    $scope.labelAdvanceSettings = '';

    $scope.toggleAdvanceSettings = function () {
        $timeout(function () {
            $scope.showAdvanceSettings = !$scope.showAdvanceSettings;
        }, 0);
    }

    $scope.$watch('showAdvanceSettings', function (newValue) {
        delete $scope.labelAdvanceSettings;

        if (newValue) {
            $scope.labelAdvanceSettings = translationService.translate('Machine.Label.HideAdvancedSettings');
        }
        else {
            $scope.labelAdvanceSettings = translationService.translate('Machine.Label.ShowAdvancedSettings');
        }
    });

    if (!$scope.selectedMachine.IsNewMachine) {
        $scope.notifyBarcodeToUIOnManualMode = $scope.$parent.selectedMachine.Data.NotifyBarcodeToUIOnManualMode > 0 ? true : false;
    }

    var awaitingUpdate = false;

    messagingService.machineCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('Machine.NetworkScanner.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.NetworkScanner.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('Machine.NetworkScanner.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'Machine Created');

    messagingService.machineUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Machine.NetworkScanner.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.NetworkScanner.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Machine.NetworkScanner.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'Machine Updated');

    $scope.saveMachine = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        
        delete $scope.machineBeingSaved;
        $scope.machineBeingSaved = {
            Id: $scope.selectedMachine.Id,
            Alias: $scope.selectedMachine.Alias,
            Description: $scope.selectedMachine.Description,
            IpOrDnsName: $scope.selectedMachine.Data.IpOrDnsName,
            Port: $scope.selectedMachine.Data.Port,
            NotifyBarcodeToUIOnManualMode: $scope.notifyBarcodeToUIOnManualMode ? 1 : 0,
            IsNewMachine: $scope.selectedMachine.IsNewMachine
        };

        var action = "CreateNetworkScanner";
        if (!$scope.machineBeingSaved.IsNewMachine)
            action = "UpdateNetworkScanner";
        messagingService.publish(action, $scope.machineBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.NetworkScanner.Label.NameSingular'), $scope.machineBeingSaved.Alias));
                awaitingUpdate = false;
                
                delete $scope.machineBeingSaved;
                $scope.machineBeingSaved = {};
                
                delete $scope.selectedMachine;
                $scope.selectedMachine = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditMachine = function () {
    	$state.go('^.list', null, { reload: true });
    }

    var showingDetailsBeforeHelp = $scope.showAdvanceSettings;

    $scope.tellMeAboutThisPage = function () {
        showingDetailsBeforeHelp = $scope.showAdvanceSettings;

        var steps = [];

        steps.push({
            element: "#doesntExist",
            intro: translationService.translate('Machine.NetworkScanner.Help.Form.Overview')
        });

        steps.push({
            element: "#newMachineAlias",
            intro: translationService.translate('Machine.NetworkScanner.Help.Form.Name')
        });

        steps.push({
            element: "#newMachineDescription",
            intro: translationService.translate('Machine.Help.Form.Description')
        });

        steps.push({
            element: "#nudHorizontalTable",
            intro: translationService.translate('Machine.Help.Form.Port')
        });

        steps.push({
            element: "#newMachineAutoTabBetweenFields",
            intro: translationService.translate('Machine.NetworkScanner.Help.Form.AutoTabBetweenFields')
        });

        steps.push({
            element: "#newMachineSaveButton",
            intro: translationService.translate('Machine.Help.Form.Save')
        });

        steps.push({
            element: "#newMachineCancelButton",
            intro: translationService.translate('Machine.Help.Form.Cancel')
        });

        return steps;
    }

    var afterIntro = function () {
        if (showingDetailsBeforeHelp !== $scope.showAdvanceSettings)
            $scope.toggleAdvanceSettings();
    }

    introJsService.onbeforechange(function () {
        if (!$scope.showAdvanceSettings) {
            $scope.toggleAdvanceSettings();
        }
    });

    introJsService.oncomplete(afterIntro);

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};