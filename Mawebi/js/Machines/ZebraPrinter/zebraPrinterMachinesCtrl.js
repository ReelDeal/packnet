function ZebraPrinterMachinesCtrl($log, $scope, $state, $timeout, alertService, introJsService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.isPeelOff = true;
    $scope.showAdvanceSettings = false;
    $scope.labelAdvanceSettings = '';

    $scope.toggleAdvanceSettings = function () {
        $timeout(function () {
            $scope.showAdvanceSettings = !$scope.showAdvanceSettings;
        }, 0);
    }

    $scope.$watch('showAdvanceSettings', function (newValue) {
        if (newValue) {
            
            delete $scope.labelAdvanceSettings;
            $scope.labelAdvanceSettings = translationService.translate('Machine.Label.HideAdvancedSettings');
        }
        else {
            
            delete $scope.labelAdvanceSettings;
            $scope.labelAdvanceSettings = translationService.translate('Machine.Label.ShowAdvancedSettings');
        }
    });

    $scope.Models = [
        {
            Key: "140Xi4",
            Value: "140Xi4",
            PollInterval: 800,
            RetryCount: 25,
            LabelSecondCheckWaitTime: 500
        },
        {
            Key: "GX420t",
            Value: "GX420t",
            PollInterval: 800,
            RetryCount: 25,
            LabelSecondCheckWaitTime: 500
        }
    ];

    $scope.changeDefaultsForModel = function () {
        
        delete $scope.selectedMachine.Data.PollInterval;
        $scope.selectedMachine.Data.PollInterval = $scope.selectedMachine.model.PollInterval;
        
        delete $scope.selectedMachine.Data.RetryCount;
        $scope.selectedMachine.Data.RetryCount = $scope.selectedMachine.model.RetryCount;
        
        delete $scope.selectedMachine.Data.LabelSecondCheckWaitTime;
        $scope.selectedMachine.Data.LabelSecondCheckWaitTime = $scope.selectedMachine.model.LabelSecondCheckWaitTime;
        
        delete $scope.selectedMachine.Data.LabelInQueueRetryWaitTime;
        $scope.selectedMachine.Data.LabelInQueueRetryWaitTime = $scope.selectedMachine.model.LabelInQueueRetryWaitTime;
        
        delete $scope.selectedMachine.Data.SocketSendTimeout;
        $scope.selectedMachine.Data.SocketSendTimeout = $scope.selectedMachine.model.SocketSendTimeout;
        
        delete $scope.selectedMachine.Data.SocketReceiveTimeout;
        $scope.selectedMachine.Data.SocketReceiveTimeout = $scope.selectedMachine.model.SocketReceiveTimeout;
        
        delete $scope.selectedMachine.Data.SocketHeartbeatWaitTime;
        $scope.selectedMachine.Data.SocketHeartbeatWaitTime = $scope.selectedMachine.model.SocketHeartbeatWaitTime;

        
        delete $scope.selectedMachine.Description;
        $scope.selectedMachine.Description = $scope.selectedMachine.Description ? $scope.selectedMachine.Description = $scope.selectedMachine.model.Value : $scope.selectedMachine.model.Value;
    }

    if (!$scope.selectedMachine.IsNewMachine) {
        $scope.isPeelOff = $scope.$parent.selectedMachine.Data.IsPeelOff;
        $scope.isPriorityPrinter = $scope.$parent.selectedMachine.Data.IsPriorityPrinter > 0 ? true : false;
        var mod = $scope.selectedMachine.Data.PrinterMode;
        $scope.Models.forEach(function (item) {
            if (item.Key == mod) {
                
                delete $scope.selectedMachine.model;
                $scope.selectedMachine.model = item;
            }
        });
    }
    else {
        
        delete $scope.selectedMachine.model;
        $scope.selectedMachine.model = $scope.Models[0];
        
        delete $scope.selectedMachine.Data;
        $scope.selectedMachine.Data = {};
        $scope.changeDefaultsForModel();
    }

    var awaitingUpdate = false;

    messagingService.machineCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('Machine.ZebraPrinter.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.ZebraPrinter.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('Machine.ZebraPrinter.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'Machine Created');

    messagingService.machineUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Machine.ZebraPrinter.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.ZebraPrinter.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Machine.ZebraPrinter.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'Machine Updated');

    $scope.saveMachine = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        
        delete $scope.machineBeingSaved;
        $scope.machineBeingSaved = {
            Id: $scope.selectedMachine.Id,
            Alias: $scope.selectedMachine.Alias,
            Description: $scope.selectedMachine.Description,
            IpOrDnsName: $scope.selectedMachine.Data.IpOrDnsName,
            Port: $scope.selectedMachine.Data.Port,
            PollInterval: $scope.selectedMachine.Data.PollInterval,
            RetryCount: $scope.selectedMachine.Data.RetryCount,
            LabelSecondCheckWaitTime: $scope.selectedMachine.Data.LabelSecondCheckWaitTime,
            IsPeelOff: $scope.isPeelOff ? 1 : 0,
            IsPriorityPrinter: $scope.isPriorityPrinter ? 1 : 0,
            IsNewMachine: $scope.selectedMachine.IsNewMachine,
            PrinterMode: $scope.selectedMachine.model.Key
        };

        var action = "CreateZebraPrinter";
        if (!$scope.machineBeingSaved.IsNewMachine) action = "UpdateZebraPrinter";
        messagingService.publish(action, $scope.machineBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.ZebraPrinter.Label.NameSingular'), $scope.machineBeingSaved.Alias));
                awaitingUpdate = false;
                
                delete $scope.machineBeingSaved;
                $scope.machineBeingSaved = {};
                
                delete $scope.selectedMachine;
                $scope.selectedMachine = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditMachine = function () {
        $state.go('^.list', null, { reload: true });
    }

    var showAdvancedSettingsState = $scope.showAdvanceSettings;
    $scope.tellMeAboutThisPage = function () {
        showAdvancedSettingsState = $scope.showAdvanceSettings;
        var steps = [];
        var tables = document.querySelectorAll("#nudHorizontalTable");

        steps.push({
            element: "#doesntExist",
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.Overview')
        });

        steps.push({
            element: "#newMachineModel",
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.Model')
        });

        steps.push({
            element: "#newMachineAlias",
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.Name')
        });

        steps.push({
            element: "#newMachineDescription",
            intro: translationService.translate('Machine.Help.Form.Description')
        });

        steps.push({
            element: "#newMachineIpOrDnsName",
            intro: translationService.translate('Machine.Help.Form.IpOrDnsName')
        });

        steps.push({
            element: tables[0],
            intro: translationService.translate('Machine.Help.Form.Port')
        });

        steps.push({
            element: "#printerModeOptions",
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.PrinterMode')
        });

        steps.push({
            element: "#machinePriorityPrinterContainer",
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.PriorityPrinter')
        });

        steps.push({
            element: tables[1],
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.RetryCount')
        });

        steps.push({
            element: tables[2],
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.PollInterval')
        });

        steps.push({
            element: tables[3],
            intro: translationService.translate('Machine.ZebraPrinter.Help.Form.LabelSecondCheck')
        });

        steps.push({
            element: "#newMachineSaveButton",
            intro: translationService.translate('Machine.Help.Form.Save')
        });

        steps.push({
            element: "#newMachineCancelButton",
            intro: translationService.translate('Machine.Help.Form.Cancel')
        });

        return steps;
    }

    var afterIntro = function () {
        if (!showAdvancedSettingsState)
            $scope.toggleAdvanceSettings();
    }
    introJsService.onbeforechange(function (targetElement) {
        if (!$scope.showAdvanceSettings) {
            $scope.toggleAdvanceSettings();
        }
    });

    introJsService.onexit(afterIntro);
    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage, true);
};