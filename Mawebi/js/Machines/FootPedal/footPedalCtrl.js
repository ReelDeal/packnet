function FootPedalCtrl($log, $scope, $state, $stateParams, $timeout, alertService, introJsService, messagingService, stateChangeService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.displayAddForm = false;
    $scope.selectedDevice = {};
    $scope.selectedDevice.IsNewDevice = true;
    
    $scope.footPedalPurposes = [
        {
            DisplayName: translationService.translate('Common.Label.Trigger'),
            Value : "trigger"
        },
        {
            DisplayName: translationService.translate('Machine.Accessory.FootPedal.Label.PausePlayMachineGroup'),
            Value : "mgpauseplay"
        }
    ];
    
    $scope.selectedDevice.Purpose = $scope.footPedalPurposes[0].Value;

    var ioAccessoryCreatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("IOAccessoryCreated"));

    var ioAccessoryUpdatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("IOAccessoryUpdated"));

    if ($stateParams && $stateParams.selectedDevice) {
        
        delete $scope.selectedDevice;
        $scope.selectedDevice = $stateParams.selectedDevice;

        if ($scope.selectedDevice.MachineGroupPausePlay != undefined && $scope.selectedDevice.MachineGroupPausePlay != {}) {
            
            delete $scope.selectedDevice.Purpose;
            $scope.selectedDevice.Purpose = $scope.footPedalPurposes[1].Value;
            
            delete $scope.selectedDevice.Port;
            $scope.selectedDevice.Port = $scope.selectedDevice.MachineGroupPausePlay.Port;
            
            delete $scope.selectedDevice.Module;
            $scope.selectedDevice.Module = $scope.selectedDevice.MachineGroupPausePlay.Module;
        }
        else {
            
            delete $scope.selectedDevice.Purpose;
            $scope.selectedDevice.Purpose = $scope.footPedalPurposes[0].Value;
            
            delete $scope.selectedDevice.Port;
            $scope.selectedDevice.Port = $scope.selectedDevice.FootPedalTrigger.Port;
            
            delete $scope.selectedDevice.Module;
            $scope.selectedDevice.Module = $scope.selectedDevice.FootPedalTrigger.Module;
        }
    }

    $scope.cancelNewEditFootPedal = function (){
        $state.go(stateChangeService.previousState(), null, { reload: false });
    }

    ioAccessoryCreatedObservable.subscribe(handleReply, $scope, 'AccessoryCreated');
    ioAccessoryUpdatedObservable.subscribe(handleReply, $scope, 'AccessoryUpdated');

    function handleReply(msg) {
        $scope.awaitingUpdate = false;

        if (msg.Result === "Success") {
            $state.go(stateChangeService.previousState(), null, { reload: false });
        }

        if (msg.Result === "Exists") {
            if (msg.Message === "Alias") {
                alertService.addError(translationService.translate('Machine.Accessory.Message.NameExists', translationService.translate('Machine.Accessory.FootPedal.Label.NameSingular'), $scope.footPedalBeingSaved.Alias));
            } else if (msg.Message === "Id") {
                alertService.addError(translationService.translate('Machine.Accessory.Message.IdExists', translationService.translate('Machine.Accessory.FootPedal.Label.NameSingular')));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'));
                $log.info(msg);
            }
        }

        if (msg.Result === "Fail") {
            if (msg.Message === "AccessoryModuleAndPortInUse") {
                alertService.addError(translationService.translate('Machine.Accessory.Message.ModuleAndPortInUse'));
            } else if (msg.Message === "NotExists") {
                alertService.addError(translationService.translate('Machine.Accessory.Message.NotExists', translationService.translate('Machine.Accessory.FootPedal.Label.NameSingular')));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'));
                $log.info(msg);
            }
        }
    }

    $scope.saveFootPedal = function (form) {
        $scope.$broadcast('show-errors-check-validity');

        if (!form.$valid) {
            return;
        }

        alertService.clearAll();


        
        delete $scope.footPedalBeingSaved;
        $scope.footPedalBeingSaved = {
            Id: $scope.selectedDevice.Id,
            Alias: $scope.selectedDevice.Alias,
            Description: $scope.selectedDevice.Description,
            MachineId: $scope.selectedMachine.Id
        };

        if ($scope.selectedDevice.Purpose == 'mgpauseplay') {
            
            delete $scope.footPedalBeingSaved.MachineGroupPausePlay;
            $scope.footPedalBeingSaved.MachineGroupPausePlay = {
                Module: $scope.selectedDevice.Module,
                Port: $scope.selectedDevice.Port
            }
        }
        else {
            
            delete $scope.footPedalBeingSaved.FootPedalTrigger;
            $scope.footPedalBeingSaved.FootPedalTrigger = {
                Module: $scope.selectedDevice.Module,
                Port: $scope.selectedDevice.Port
            }
        }

        $scope.awaitingUpdate = true;
        var action = "CreateFootPedalAccessory";

        if (!$scope.selectedDevice.IsNewDevice) {
            action = "UpdateFootPedalAccessory";
        }

        messagingService.publish(action, $scope.footPedalBeingSaved);

        $timeout(function () {
            if ($scope.awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.Accessory.FootPedal.Label.NameSingular'), $scope.footPedalBeingSaved.Alias));
                $scope.awaitingUpdate = false;
                
                delete $scope.footPedalBeingSaved;
                $scope.footPedalBeingSaved = {};
                
                delete $scope.selectedDevice;
                $scope.selectedDevice = {};
            }
        }, 5000);
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        var tables = document.querySelectorAll("#nudHorizontalTable");

        steps.push({
            element: "#doesntExist",
            intro: translationService.translate('Machine.Accessory.FootPedal.Help.Form.Overview')
        });

        steps.push({
            element: "#newFootPedalName",
            intro: translationService.translate('Machine.Accessory.FootPedal.Help.Form.Name')
        });

        steps.push({
            element: "#newFootPedalDescription",
            intro: translationService.translate('Machine.Accessory.FootPedal.Help.Form.Description')
        });

        steps.push({
            element: "#newFootPedalPurpose",
            intro: translationService.translate('Machine.Accessory.FootPedal.Help.Form.Purpose')
        });

        steps.push({
            element: tables[0],
            intro: translationService.translate('Machine.Accessory.Help.Form.Module')
        });

        steps.push({
            element: tables[1],
            intro: translationService.translate('Machine.Accessory.Help.Form.Port')
        });
        
        steps.push({
            element: "#newFootPedalSaveButton",
            intro: translationService.translate('Machine.Accessory.Help.Form.Save')
        });

        steps.push({
            element: "#newFootPedalCancelButton",
            intro: translationService.translate('Machine.Accessory.Help.Form.Cancel')
        });

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};