function BrMachineDiscoveryController($interval, $scope, $state, $stateParams, $timeout, alertService, introJsService, machineGroupService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.discoveredMachines = [];
    $scope.selectedItems = [];
    $scope.searching = false;

    $scope.showProgressBar = function (){
        delete $scope.progressType;
        $scope.progressType = 'success';
        $scope.progress = 25;
        $interval(function () {
            if ($scope.progress < 100) {
                $scope.progress += 2;
            }
        }, 300);
    }

    var notConfiguredLabel = translationService.translate('Common.Label.NotConfigured');

    var brDiscoveredDevicesObservable = messagingService.messageSubject
        .where(messagingService.filterObs("DiscoveredBrDevices"))
        .select(messagingService.dataFunction);

    //wire up
    brDiscoveredDevicesObservable.autoSubscribe(function (data) {
        $scope.searching = false;

        if (!data || data.length === 0) {
            alertService.addWarning(translationService.translate('Machine.Discover.Message.NoResults'));
            return;
        }

        alertService.addSuccess(translationService.translate('Machine.Discover.Message.ResultsFound', data.length));
        var allMachines = Enumerable.From(machineGroupService.getMachines());
        //TODO: Refactor this nastiness
        var discoveredMachines = Enumerable.From(data).Select(function (discoveredmachine) {
            var configuredmachine = allMachines.FirstOrDefault
            (
                null,
                function (existingMachine) {
                    return existingMachine.Data.IpOrDnsName === discoveredmachine.IpV4Address;
                }
            );

            discoveredmachine.ConfiguredAs = (configuredmachine != null)
                ? discoveredmachine.ConfiguredAs = configuredmachine.Alias
                : discoveredmachine.ConfiguredAs = notConfiguredLabel;

            return discoveredMachines;
        }).ToArray();

        //TODO: Disable the checkboxes for rows that don't say "Not configured" 
        delete $scope.discoveredMachines;
        $scope.discoveredMachines = data;
    }, $scope, 'BR Discovered Machine');

    //request callback
    var requestData = function () {
        $scope.showProgressBar();
        $scope.searching = true;
        messagingService.publish("DiscoverBrDevices");
        $timeout(function () {
            if ($scope.searching) {
                $scope.searching = false;
            }
        }, 20000, true);
    }

    //request
    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'discoveredMachines',
        columnDefs: [
            {
                displayName: translationService.translate('Machine.Label.MACAddress'),
                field: 'MacAddress',
                width: "*"
            },
            {
                displayName: translationService.translate('Machine.Label.IPAddress'),
                field: 'IpV4Address',
                width: "*"
            },
            {
                displayName: translationService.translate('Machine.Label.SubnetMask'),
                field: 'IpV4SubnetMask',
                width: "*"
            },
            {
                displayName: translationService.translate('Machine.Discover.Label.PLCModule'),
                field: 'PlcModule',
                width: "*"
            },
            {
                displayName: translationService.translate('Machine.Discover.Label.PLCVersion'),
                field: 'PlcVersion',
                width: "*"
            },
            {
                displayName: translationService.translate('Machine.Discover.Label.Version'),
                field: 'RuntimeVersion',
                width: "*"
            },
            {
                displayName: translationService.translate('Machine.Discover.Label.ConfiguredAs'),
                field: 'ConfiguredAs',
                width: "*"
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: false,
        multiSelect: false,
        enableSorting: false,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            //allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
        }
    };

    $scope.discover = function () {
        requestData();
    }

    $scope.cancelDiscovery = function () {
        $state.go('^.list', null, { reload: true });
    }

    $scope.saveDiscovery = function () {
        //Go to the new machine dialog and populate info there for adding
        var selectedItems = $scope.gridApi.selection.getSelectedRows();

        if (selectedItems.length > 0) {
            var selectedMachine = selectedItems[0];

            if (selectedMachine.ConfiguredAs === notConfiguredLabel) {
                var machineType = "Fusion";
                var routeBits = $state.current.name.split('_');

                if (routeBits.length > 1) {
                    machineType = routeBits[routeBits.length - 1];
                }

                var nextRoute = 'Machines.new' + machineType;
                $state.go(nextRoute, { selectedMachine: selectedMachine }, { reload: true });
            }
        }
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        steps.push({
            element: "#notFound",
            intro: translationService.translate('Machine.Discover.Help.Overview')
        });

        steps.push({
            element: "#newSaveMachineButton",
            intro: translationService.translate('Machine.Discover.Help.SaveButton')
        });

        steps.push({
            element: "#discoverCancelButton",
            intro: translationService.translate('Machine.Discover.Help.CancelButton')
        });

        steps.push({
            element: "#discoverBrMachinesButton",
            intro: translationService.translate('Machine.Discover.Help.RefreshButton')
        });

        steps.push({
            element: '#damnCheckboxCannotBeHighlighted',
            intro: translationService.translate('Machine.Discover.Help.Columns.Overview')
        });

        steps.push({
            element: columnDefs[1],
            intro: translationService.translate('Machine.Discover.Help.Columns.MacAddress')
        });

        steps.push({
            element: columnDefs[2],
            intro: translationService.translate('Machine.Discover.Help.Columns.IpAddress')
        });

        steps.push({
            element: columnDefs[3],
            intro: translationService.translate('Machine.Discover.Help.Columns.SubNetMask')
        });

        steps.push({
            element: columnDefs[4],
            intro: translationService.translate('Machine.Discover.Help.Columns.PlcModel')
        });

        steps.push({
            element: columnDefs[5],
            intro: translationService.translate('Machine.Discover.Help.Columns.PlcVersion')
        });

        steps.push({
            element: columnDefs[6],
            intro: translationService.translate('Machine.Discover.Help.Columns.RuntimeVersion')
        });

        steps.push({
            element: columnDefs[7],
            intro: translationService.translate('Machine.Discover.Help.Columns.ConfiguredAs')
        });

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};