function EmMachinesCtrl($interval, $log, $scope, $state, $stateParams, $timeout, alertService, introJsService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.timeoutInMilliseconds = 10000;
    $scope.awaitingUpdate = false;

    var emMachineSettingsFileNamesObservable = messagingService.messageSubject
        .where(messagingService.filterObs("EmMachineSettingsFileNames"));

    var ipVersion4InfoSetResponseObservable = messagingService.messageSubject
        .where(messagingService.filterObs("IpVersion4InfoSetResponse"));

    if ($scope.selectedMachine.IsNewMachine === true) {
        
        delete $scope.selectedMachine.Data;
        $scope.selectedMachine.Data = {};

        //Assign values from discovery
        if ($stateParams.selectedMachine) {
            
            delete $scope.selectedMachine.Data.IpOrDnsName;
            $scope.selectedMachine.Data.IpOrDnsName = $stateParams.selectedMachine.IpV4Address;
            
            delete $scope.selectedMachine.Data.MacAddress;
            $scope.selectedMachine.Data.MacAddress = $stateParams.selectedMachine.MacAddress;
            
            delete $scope.selectedMachine.Data.PlcModule;
            $scope.selectedMachine.Data.PlcModule = $stateParams.selectedMachine.PlcModule;
            
            delete $scope.selectedMachine.Data.IpV4SubnetMask;
            $scope.selectedMachine.Data.IpV4SubnetMask = $stateParams.selectedMachine.IpV4SubnetMask;
            
            delete $scope.selectedMachine.Data.RuntimeVersion;
            $scope.selectedMachine.Data.RuntimeVersion = $stateParams.selectedMachine.RuntimeVersion;
            
            delete $scope.selectedMachine.Description;
            $scope.selectedMachine.Description = translationService.translate("Machine.Message.Discovered", $stateParams.selectedMachine.PlcModule, $stateParams.selectedMachine.MacAddress, $stateParams.selectedMachine.RuntimeVersion);
        }
    }

    $scope.originalData = angular.copy($scope.selectedMachine.Data);

    messagingService.machineCreatedObservable.autoSubscribe(function (msg) {
        $scope.awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('Machine.Em.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.Em.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('Machine.Em.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'Machine Created');

    messagingService.machineUpdatedObservable.autoSubscribe(function (msg) {
        $scope.awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Machine.Em.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Machine.Em.Label.NameSingular'), $scope.machineBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Machine.Em.Label.NameSingular'), $scope.machineBeingSaved.Alias), 5000);
        }
        else {
            networkMachineResponseHandler($scope, alertService, translationService, $scope.machineBeingSaved, msg.Result);
        }
    }, $scope, 'Machine Updated');

    emMachineSettingsFileNamesObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            delete $scope.emMachineSettingsFileNames;
            $scope.emMachineSettingsFileNames = msg.Data;
        }, 0);
    }, $scope, 'Em Machine Settings File');

    function PublishCreateOrUpdateMessage(isUpdate, partialSuccessMessage) {
        $scope.awaitingUpdate = true;
        $scope.timeoutInMilliseconds = 65000;
        $scope.showProgressBar();
        var action = "CreateEmMachine";

        if (isUpdate) {
            action = "UpdateEmMachine";
        }

        if (partialSuccessMessage) {
            alertService.addWarning(partialSuccessMessage, 5000);
            //This timeout emulates the normal wait time we would get from the previous message so we don't publish right away and cover up the partial success message
            $timeout(function () {
                if ($scope.machineBeingSaved) {
                    console.log("PublishCreateOrUpdateMessage");
                    messagingService.publish(action, $scope.machineBeingSaved);
                }
            }, 5000);
        }
        else {
            //No timeout delay necessary as there was no partial success message
            messagingService.publish(action, $scope.machineBeingSaved);
        }
        
        $timeout(function () {
            if ($scope.awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.Em.Label.NameSingular'), $scope.machineBeingSaved.Alias));
                $scope.awaitingUpdate = false;
                
                delete $scope.machineBeingSaved;
                $scope.machineBeingSaved = {};
                
                delete $scope.selectedMachine;
                $scope.selectedMachine = {};
            }
        }, $scope.timeoutInMilliseconds);
    }

    ipVersion4InfoSetResponseObservable.autoSubscribe(function (msg) {
        $scope.awaitingUpdate = false;
        var isUpdate = !$scope.machineBeingSaved.IsNewMachine;

        if (msg.Result === "Success") {
            PublishCreateOrUpdateMessage(isUpdate, null);
        }
        else if (msg.Result === "PartialSuccess") {
            PublishCreateOrUpdateMessage(isUpdate, msg.Message);
        }
        else if (msg.Result === "Fail") {
            var additionalInfo = msg.Message != null ? msg.Message.split(",") : [""];
            var messageDetails;

            if (additionalInfo[0] === "PLCError") {
                messageDetails = translationService.translate('Machine.Message.PLCError');
                alertService.addError(translationService.translate('Machine.Message.IPInformationFailed', messageDetails));
            }
            else if (additionalInfo[0] === "SNMPError") {
                messageDetails = translationService.translate('Machine.Message.SNMPError', additionalInfo[1]);
                alertService.addError(translationService.translate('Machine.Message.IPInformationFailed', messageDetails));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'));
                $log.info(msg);
            }
        }
    }, $scope, 'ipVersion4InfoSetResponseObservable');

    $scope.requestFileNames = function () {
        messagingService.publish("GetEmMachineSettingsFileNames");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, $scope.requestFileNames);

    $scope.showProgressBar = function () {
        
        delete $scope.progressType;
        $scope.progressType = 'success';
        $scope.progress = 25;
        $interval(function () {
            if ($scope.progress < 100) {
                $scope.progress += 2;
            }
        }, 12);
    }

    $scope.saveMachine = function (form) {
        $scope.$broadcast('show-errors-check-validity');

        if (!form.$valid) {
            return;
        }

        alertService.clearAll();

        
        delete $scope.machineBeingSaved;
        $scope.machineBeingSaved = {
            Id: $scope.selectedMachine.Id,
            Alias: $scope.selectedMachine.Alias,
            Description: $scope.selectedMachine.Description,
            IpOrDnsName: $scope.selectedMachine.Data.IpOrDnsName,
            Port: $scope.selectedMachine.Data.Port,
            NumTracks: $scope.selectedMachine.Data.NumTracks,
            PhysicalSettingsFilePath: $scope.selectedMachine.Data.PhysicalSettingsFilePath,
            IsNewMachine: $scope.selectedMachine.IsNewMachine,
            MacAddress: $scope.selectedMachine.Data.MacAddress,
            IpV4SubnetMask: $scope.selectedMachine.Data.IpV4SubnetMask,
            IpV4DefaultGateway: $scope.selectedMachine.Data.IpV4DefaultGateway
        };

        var discoveredMachineWithInvalidIpAddress = $scope.machineBeingSaved.MacAddress && $scope.selectedMachine.Data.IpOrDnsName === "127.0.0.1";
        var discoveredMachineWithInvalidDefaultGateway = $scope.machineBeingSaved.MacAddress && $scope.selectedMachine.Data.IpV4DefaultGateway === "127.0.0.1";

        if (discoveredMachineWithInvalidIpAddress) {
            $timeout(function () {
                alertService.addError(translationService.translate('Machine.Discover.Message.InvalidIPAddress'));
            }, 500);
        }
        else if (discoveredMachineWithInvalidDefaultGateway) {
            $timeout(function () {
                alertService.addError(translationService.translate('Machine.Discover.Message.InvalidDefaultGateway'));
            }, 500);
        }
        else {
            var isUpdate = false;

            if (!$scope.machineBeingSaved.IsNewMachine) {
                isUpdate = true;
            }

            var isIpv4InfoDirty = $scope.machineBeingSaved.IpOrDnsName !== $scope.originalData.IpOrDnsName
                || $scope.machineBeingSaved.IpV4SubnetMask !== $scope.originalData.IpV4SubnetMask
                || $scope.machineBeingSaved.IpV4DefaultGateway !== $scope.originalData.IpV4DefaultGateway;

            //Only set IPV4 when changed
            //Discovery will handle persisting the machine on the callback for IpVersion4InfoSetRequest
            if ($scope.machineBeingSaved.MacAddress && isIpv4InfoDirty) {
                
                delete $scope.machineBeingSaved.MacAddress;
                $scope.machineBeingSaved.MacAddress = $scope.selectedMachine.Data.MacAddress;
                
                delete $scope.machineBeingSaved.IpV4Address;
                $scope.machineBeingSaved.IpV4Address = $scope.selectedMachine.Data.IpOrDnsName;
                
                delete $scope.machineBeingSaved.IpV4DefaultGateway;
                $scope.machineBeingSaved.IpV4DefaultGateway = $scope.selectedMachine.Data.IpV4DefaultGateway;
                $scope.awaitingUpdate = true;
                $scope.timeoutInMilliseconds = 60000;
                $scope.showProgressBar();
                messagingService.publish("IpVersion4InfoSetRequest", $scope.machineBeingSaved);

                $timeout(function () {
                    if ($scope.awaitingUpdate && $scope.machineBeingSaved && $scope.machineBeingSaved.Alias) {
                        alertService.addError(translationService.translate('Machine.Message.IPInformationTimeout', $scope.machineBeingSaved.Alias));
                        $scope.awaitingUpdate = false;
                    }
                }, $scope.timeoutInMilliseconds);
            }
            else {
                PublishCreateOrUpdateMessage(isUpdate, null);
            }
        }
    }

    $scope.cancelNewEditMachine = function () {
        $state.go('^.list', null, { reload: true });
    }

    $scope.discoverBrMachines = function () {
        $state.go('^.discoverBrMachines_Em', null, { reload: true });
    }

    $scope.editPhysicalMachineSettings = function () {
        $state.go('^.emPhysicalMachineSettings');
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        var tables = document.querySelectorAll("#nudHorizontalTable");

        steps.push({
            element: "#doesntExist",
            intro: translationService.translate('Machine.Em.Help.Form.Overview')
        });

        steps.push({
            element: "#newMachineAlias",
            intro: translationService.translate('Machine.Em.Help.Form.Name')
        });

        steps.push({
            element: "#newMachineDescription",
            intro: translationService.translate('Machine.Help.Form.Description')
        });

        steps.push({
            element: "#newMachineIpOrDnsName",
            intro: translationService.translate('Machine.Help.Form.IpOrDnsNameWithDiscover')
        });

        steps.push({
            element: tables[0],
            intro: translationService.translate('Machine.Help.Form.Port')
        });

        steps.push({
            element: tables[1],
            intro: translationService.translate('Machine.Em.Help.Form.NumberOfTracks')
        });

        steps.push({
            element: "#newMachinePhysicalSettings",
            intro: translationService.translate('Machine.Help.Form.PhysicalMachineSettings')
        });

        steps.push({
            element: "#AccessoriesGridDiv",
            intro: translationService.translate('Machine.Accessory.Help.List.Overview')
        });

        steps.push({
            element: "#newAccessoryButton",
            intro: translationService.translate('Machine.Accessory.Help.List.New')
        });

        steps.push({
            element: "#deleteAccessory",
            intro: translationService.translate('Machine.Accessory.Help.List.Delete')
        });

        steps.push({
            element: "#editAccessory",
            intro: translationService.translate('Machine.Accessory.Help.List.Edit')
        });

        steps.push({
            element: "#newMachineSaveButton",
            intro: translationService.translate('Machine.Help.Form.Save')
        });

        steps.push({
            element: "#newMachineCancelButton",
            intro: translationService.translate('Machine.Help.Form.Cancel')
        });

        steps.push({
            element: "#discoverBrMachinesButton",
            intro: translationService.translate('Machine.Help.Form.Discover')
        });

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};