function IoDeviceCtrl($scope, $state, $timeout, messagingService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.selectedDevice = {};
    $scope.selectedDevice.IsNewDevice = true;

    var machineIoAccessoriesObservable = messagingService.messageSubject
        .where(messagingService.filterObs("MachineIoAccessories"));

    machineIoAccessoriesObservable.autoSubscribe(populateIoDevice, $scope, 'Machine Created');

    function populateIoDevice(msg){
        $timeout(function () {
            
            delete $scope.ioDevices;
            $scope.ioDevices = msg.Data;
        }, 0, true);
    };

    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.editDevice = function () {
        
        delete $scope.selectedDevice;
        $scope.selectedDevice = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedDevice.IsNewDevice = false;

        if ($scope.selectedDevice.AccessoryType === "FootPedal") {
            $state.go('^.newFootPedal', { selectedDevice: $scope.selectedDevice });
        }
        else if ($scope.selectedDevice.AccessoryType === "Conveyor") {
            $state.go('^.newConveyor', { selectedDevice: $scope.selectedDevice });
        }
    }

    $scope.deleteDevices = function (){
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (device) {
            //MachineType is set based on MachineTypes and PrintMachineTypes Enumeration
            if (device.AccessoryType === "FootPedal") {
                messagingService.publish("DeleteFootPedalAccessory", device);
            }
            else if (device.AccessoryType === "Conveyor") {
                messagingService.publish("DeleteConveyorAccessory", device);
            }
        });
    }

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'ioDevices',
        columnDefs: [
            {
                displayName: translationService.translate('Common.Label.Name'),
                field: 'Alias',
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                displayName: translationService.translate('Common.Label.Type'),
                field: 'AccessoryType',
                width: 150
            },
            {
                displayName: translationService.translate('Common.Label.Description'),
                field: 'Description'
            }
        ],
        headerRowHeight: 36,
        minRowsToShow: 5,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
        }
    };

    messagingService.publish("GetIoAccessoriesForMachine", $scope.selectedMachine.Id);
};