function ConveyorCtrl($log, $scope, $state, $stateParams, $timeout, alertService, introJsService, messagingService, stateChangeService, translationService) {

    $timeout = $timeout.getInstance($scope);
    $scope.displayAddForm = false;
    
    $scope.selectedDevice = {
        IsNewDevice: true,
        SecondMoveCapability: {},
        FirstMoveCapability: {}
    };

    $scope.conveyorTypes = ["CrossConveyor", "WasteConveyor"];
    
    $scope.pickZones = [];

    var ioAccessoryCreatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("IOAccessoryCreated"));

    var ioAccessoryUpdatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("IOAccessoryUpdated"));

    if ($stateParams && $stateParams.selectedDevice) {
        
        delete $scope.selectedDevice;
        $scope.selectedDevice = $stateParams.selectedDevice;

        var enumerable = Enumerable.From($scope.selectedDevice.MoveCapabilities);
        var first = enumerable.ElementAtOrDefault(0, undefined);
        var second = enumerable.ElementAtOrDefault(1, undefined);

        if (first) {
            $scope.selectedDevice.FirstMoveCapability =
            {
                Enabled : true,
                Port : first.Value.Port,
                Module: first.Value.Module,
                PickZone: first.PickZone
            }
        }
        
        if (second) {
            $scope.selectedDevice.SecondMoveCapability =
            {
                Enabled : true,
                Port: second.Value.Port,
                Module: second.Value.Module,
                PickZone: second.PickZone
            }
        }
    }

    $scope.cancelNewEditConveyor = function (){
        $state.go(stateChangeService.previousState(), null, { reload : false });
    }

    ioAccessoryCreatedObservable.subscribe(handleReply, $scope, 'AccessoryCreated');
    ioAccessoryUpdatedObservable.subscribe(handleReply, $scope, 'AccessoryUpdated');
    messagingService.pickZonesObservable.subscribe(handlePickZoneReply, $scope, 'ConveyorGotPickzones');

    function handleReply(msg)
    {
        $scope.awaitingUpdate = false;


        if (msg.Result === "Success") {
            $state.go(stateChangeService.previousState(), null, { reload: false });
        }

        if (msg.Result === "Exists") {
            if (msg.Message === "Alias")
            {
                if ($scope.conveyorBeingSaved.Alias != undefined) {
                    alertService.addError(translationService.translate('Machine.Accessory.Message.NameExists', translationService.translate('Machine.Accessory.Conveyor.Label.NameSingular'), $scope.conveyorBeingSaved.Alias));
                }
            }
            else if (msg.Message === "Id")
            {
                alertService.addError(translationService.translate('Machine.Accessory.Message.IdExists', translationService.translate('Machine.Accessory.Conveyor.Label.NameSingular')));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'));
                $log.info(msg);
            }
        }

        if (msg.Result === "Fail") {
            if (msg.Message === "AccessoryModuleAndPortInUse") {
                alertService.addError(translationService.translate('Machine.Accessory.Message.ModuleAndPortInUse'));
            } else if (msg.Message === "NotExists") {
                alertService.addError(translationService.translate('Machine.Accessory.Message.NotExists', translationService.translate('Machine.Accessory.Conveyor.Label.NameSingular')));
            }
            else if (msg.Message === "PickZoneAlreadyAssigned") {
                alertService.addError(translationService.translate('Machine.Accessory.Conveyor.Message.PickZoneAlreadyAssigned'));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'));
                $log.info(msg);
            }
        }
        
        delete $scope.conveyorBeingSaved;
        $scope.conveyorBeingSaved = {};
    }

    function handlePickZoneReply(msg){
        $timeout(function () {
            
            delete $scope.pickZones;
            $scope.pickZones = msg.Data;
        }, 0, true);
    };

    $scope.oneDirectionIsEnabled = function() {
        return ($scope.selectedDevice.SecondMoveCapability && $scope.selectedDevice.SecondMoveCapability.Enabled === true) || ($scope.selectedDevice.FirstMoveCapability && $scope.selectedDevice.FirstMoveCapability.Enabled === true);
    };

    $scope.enabledDirectionsHasPickzone = function(){
        var secondHas = true;
        var firstHas = true;

        if ($scope.selectedDevice.FirstMoveCapability && $scope.selectedDevice.FirstMoveCapability.Enabled === true)
            firstHas = $scope.selectedDevice.FirstMoveCapability.PickZone != undefined;

        if ($scope.selectedDevice.SecondMoveCapability && $scope.selectedDevice.SecondMoveCapability.Enabled === true)
            secondHas = $scope.selectedDevice.SecondMoveCapability.PickZone != undefined;

        return firstHas && secondHas;
    }

    $scope.showNoPickzoneError = function (){
        return $scope.selectedDevice.ConveyorType == "CrossConveyor" && $scope.pickZones.length == 0;
    }

    $scope.isPickzonesDifferent = function() {
        if ($scope.selectedDevice.SecondMoveCapability &&
            $scope.selectedDevice.FirstMoveCapability &&
            $scope.selectedDevice.SecondMoveCapability.PickZone &&
            $scope.selectedDevice.FirstMoveCapability.PickZone &&
            $scope.selectedDevice.FirstMoveCapability.Enabled === true &&
            $scope.selectedDevice.SecondMoveCapability.Enabled === true) {
            return $scope.selectedDevice.SecondMoveCapability.PickZone.Id !==
                $scope.selectedDevice.FirstMoveCapability.PickZone.Id;
        }
        return true;
    };

    $scope.arePortsDifferent = function (){
        // Ports should not be the same
        if ($scope.selectedDevice.FirstMoveCapability
            && $scope.selectedDevice.SecondMoveCapability
            && $scope.selectedDevice.SecondMoveCapability.PickZone
            && $scope.selectedDevice.FirstMoveCapability.PickZone
            && $scope.selectedDevice.SecondMoveCapability.Enabled === true
            && $scope.selectedDevice.FirstMoveCapability.Enabled === true) {
            return $scope.selectedDevice.FirstMoveCapability.Port !==
                $scope.selectedDevice.SecondMoveCapability.Port;
        }
        return true;
    }

    $scope.saveDisabled = function (form){
        if ($scope.selectedDevice.ConveyorType == "CrossConveyor")
        return !(form.$valid && $scope.oneDirectionIsEnabled() && $scope.isPickzonesDifferent() && $scope.enabledDirectionsHasPickzone());
        return !form.$valid;
    };

    $scope.saveConveyor = function (form) {
        $scope.$broadcast('show-errors-check-validity');

        if ($scope.saveDisabled(form)) {
            return;
        }

        alertService.clearAll();

        
        delete $scope.conveyorBeingSaved;
        $scope.conveyorBeingSaved = {
            Id: $scope.selectedDevice.Id,
            ConveyorType: $scope.selectedDevice.ConveyorType,
            Alias: $scope.selectedDevice.Alias,
            Description: $scope.selectedDevice.Description,
            MachineId: $scope.selectedMachine.Id,
            MoveCapabilities: []
        };

        if ($scope.selectedDevice.ConveyorType === "CrossConveyor") {
            if ($scope.selectedDevice.FirstMoveCapability.Enabled === true) {
                $scope.conveyorBeingSaved.MoveCapabilities.push(
            {
                    "$type": "PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities.ConveyorPickzoneCapability, PackNet.IODeviceService",
                    PickZone: $scope.selectedDevice.FirstMoveCapability.PickZone,
                Value: {
                        Module : $scope.selectedDevice.FirstMoveCapability.Module,
                        Port : $scope.selectedDevice.FirstMoveCapability.Port
            }
                });
        }

            if ($scope.selectedDevice.SecondMoveCapability.Enabled === true) {
                $scope.conveyorBeingSaved.MoveCapabilities.push(
                {
                    "$type": "PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities.ConveyorPickzoneCapability, PackNet.IODeviceService",
                    PickZone: $scope.selectedDevice.SecondMoveCapability.PickZone,
                Value: {
                        Module : $scope.selectedDevice.SecondMoveCapability.Module,
                        Port : $scope.selectedDevice.SecondMoveCapability.Port
                    }
                });
            }
                }
        else if ($scope.selectedDevice.ConveyorType === "WasteConveyor") {
            $scope.conveyorBeingSaved.MoveCapabilities.push(
            {
                "$type": "PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities.ConveyorMoveCapability, PackNet.IODeviceService",
                Value:
                {
                    Module: $scope.selectedDevice.FirstMoveCapability.Module,
                    Port: $scope.selectedDevice.FirstMoveCapability.Port
            }
            });
        }

        $scope.awaitingUpdate = true;
        var action = "CreateConveyorAccessory";

        if ($scope.selectedDevice.IsNewDevice != undefined && $scope.selectedDevice.IsNewDevice == false) {
            action = "UpdateConveyorAccessory";
        }

        messagingService.publish(action, $scope.conveyorBeingSaved);

        $timeout(function () {
            if ($scope.awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.Accessory.Conveyor.Label.NameSingular'), $scope.conveyorBeingSaved.Alias));
                $scope.awaitingUpdate = false;
                
                delete $scope.conveyorBeingSaved;
                $scope.conveyorBeingSaved = {};
            }
        }, 5000);
    }

    messagingService.publish("GetPickZones");

    $scope.updateIntro = function (){
        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        var tables = document.querySelectorAll("#nudHorizontalTable");

        steps.push({
            element : "#doesntExist",
            intro : translationService.translate('Machine.Accessory.Conveyor.Help.Form.Overview')
        });

        steps.push({
            element : "#newConveyorName",
            intro : translationService.translate('Machine.Accessory.Conveyor.Help.Form.Name')
        });

        steps.push({
            element : "#newConveyorDescription",
            intro : translationService.translate('Machine.Accessory.Conveyor.Help.Form.Description')
        });

        steps.push({
            element : "#conveyorTypeSelection",
            intro : translationService.translate('Machine.Accessory.Conveyor.Help.Form.TypeSelection')
        });

        if ($scope.selectedDevice.ConveyorType == "CrossConveyor") {
        steps.push({
                element: "#crossConveyorFirstFeedEnabled",
                intro: translationService.translate('Machine.Accessory.Conveyor.Help.Form.CrossConveyorFirstFeedEnabled')
        });

        steps.push({
            element: tables[0],
            intro: translationService.translate('Machine.Accessory.Help.Form.Module')
        });

        steps.push({
            element: tables[1],
            intro: translationService.translate('Machine.Accessory.Help.Form.Port')
        });

        steps.push({
                element: "#crossConveyorFirstPickZoneSelection",
            intro: translationService.translate('Machine.Accessory.Conveyor.Help.Form.PickZone')
        });

        steps.push({
                element: "#crossConveyorSecondFeedEnabled",
                intro: translationService.translate('Machine.Accessory.Conveyor.Help.Form.CrossConveyorSecondFeedEnabled')
        });

        steps.push({
            element: tables[2],
            intro: translationService.translate('Machine.Accessory.Help.Form.Module')
        });

        steps.push({
            element: tables[3],
            intro: translationService.translate('Machine.Accessory.Help.Form.Port')
        });

        steps.push({
                element: "#crossConveyorSecondPickZoneSelection",
            intro: translationService.translate('Machine.Accessory.Conveyor.Help.Form.PickZone')
        });
       
        } else if ($scope.selectedDevice.ConveyorType == "WasteConveyor") {
            steps.push({
                element: tables[0],
                intro : translationService.translate('Machine.Accessory.Conveyor.Help.Form.WasteConveyorModule')
            });
            steps.push({
                element: tables[1],
                intro : translationService.translate('Machine.Accessory.Conveyor.Help.Form.WasteConveyorPort')
            });
        }

        steps.push({
            element: "#newConveyorSaveButton",
            intro: translationService.translate('Machine.Accessory.Help.Form.Save')
        });

        steps.push({
            element: "#newConveyorCancelButton",
            intro: translationService.translate('Machine.Accessory.Help.Form.Cancel')
        });

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};