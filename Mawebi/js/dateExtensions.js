﻿
Date.prototype.startOfDay = function () {

    var date = new Date();
    date.setHours(0, 0, 0, 0);

    return date;
}

Date.prototype.endOfDay = function () {

    var endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999);

    return endOfDay;
}

Date.prototype.IsBetween = function (from, to){

    var fromDate = from instanceof String ? new Date(from) : from;
    var toDate = to instanceof String ? new Date(to) : to;

    return this >= fromDate && this <= toDate;
};