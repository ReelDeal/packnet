function PreferencesCtrl($scope, $log, $timeout, messagingService, alertService, Loglevel_Constants, Configured_Languages, $state, $translate, $rootScope) {
    $timeout = $timeout.getInstance($scope);
    $scope.logLevelIsOpen = false;
    
    $scope.preferences_new = angular.copy($scope.preferences);
    $scope.logLevels = Loglevel_Constants;
    $scope.languages = Configured_Languages;
    $scope.$log = $log.getInstance('Preferences');


    $scope.saveChanges = function () {
        $translate.use($scope.preferences_new.PreferredLocale);
        $rootScope.currentLanguage = $scope.preferences_new.PreferredLocale;
        messagingService.publish('UpdateUserPreferences', $scope.preferences_new);
    }

    $scope.selectLogLevel = function (level) {
        
        delete $scope.preferences_new.LogLevel;
        $scope.preferences_new.LogLevel = level;
        $scope.logLevelIsOpen = false;
    }

    $scope.selectLanguage = function (language) {
        
        delete $scope.preferences_new.PreferredLocale;
        $scope.preferences_new.PreferredLocale = language;
        $scope.languageIsOpen = false;

        $translate.preferredLanguage(language);
        $translate.use(language);
        $rootScope.currentLanguage = language;
    }

    $scope.userPreferencesLoaded = function (data) {
        if (data && data.Data) {
            
            delete $scope.preferences_new;
            $scope.preferences_new = angular.copy(data.Data);
        }
    }
    $scope.userPreferencesUpdated = function (data) {
        if (data) {
            if (data.Result === 'Success') {
                alertService.addSuccess('Saved preferences', 5000);
                return;
            }
        }
        alertService.addError('Failed to save preferences', 5000);
    }

    $scope.cancel = function (){
        //TODO
    }

    messagingService.userPreferencesObservable.autoSubscribe($scope.userPreferencesLoaded, $scope, 'User Preferences');
    messagingService.userPreferencesUpdatedObservable.autoSubscribe($scope.userPreferencesUpdated, $scope, 'User Preferences Updated');
}
