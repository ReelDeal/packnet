function UserCtrl($location, $scope, $log, $state, $timeout, messagingService, alertService, authenticationService, introJsService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.users = [];
    $scope.selectedUser = {};
    $scope.selectedUser.IsNewUser = true;

    var awaitingUpdate = false;

    $scope.$on('$locationChangeSuccess', function () {
        if ($scope.selectedUser && $scope.selectedUser.IsNewUser == false && $location.path() == '/Users') {
            $scope.cancelNewEditUser();
        }
    });

    var clearCheckedUsers = function () {
        $scope.gridApi.selection.clearSelectedRows();
    }

    messagingService.usersObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.users;
            $scope.users = msg.Data;
            $scope.users.forEach(function (user) {
                user.IsAdmin = user.IsAdmin === 1;
                user.IsSystem = user.IsSystem === 1;
                user.IsAutoLogin = user.IsAutoLogin === 1;
                user.DisableAutoLogin = function () {
                    return user.IsSystem || user.IsAdmin;
                };
                user.DisableAdmin = function () {
                    return user.IsSystem || user.IsAutoLogin;
                };
                user.FriendlyType = (user.IsAutoLogin ? translationService.translate('User.Label.AutoLogin') + " - " : "") + (user.IsAdmin ? translationService.translate('Common.Label.Administrator') : translationService.translate('Common.Label.Operator'));
            });
        }, 0, true);
    }, $scope, 'Users');

    messagingService.userCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName), 5000);
        }
    }, $scope, 'User Created');

    messagingService.userUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName), 5000);
        }
    }, $scope, 'User Updated');

    messagingService.userDeletedObservable.autoSubscribe(function (msg) {
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.DeleteFailed', translationService.translate('User.Label.NameSingular'), msg.Data.UserName));
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', translationService.translate('User.Label.NameSingular'), msg.Data.UserName), 5000);
            
            delete $scope.userBeingSaved;
            $scope.userBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedUser;
            $scope.selectedUser = {};
            $scope.selectedUser.IsNewUser = true;
        }
    }, $scope, 'User Updated');

    var requestData = function () {
        messagingService.publish("GetUsers");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'users',
        columnDefs: [
            {
                field: 'UserName',
                name: translationService.translate('User.Label.Username'),
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                field: 'FriendlyType',
                name: translationService.translate('Common.Label.Type')
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol" && newRowCol.col.field !== "autoLoginUser" && newRowCol.col.field !== "administrator")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
        }
    };


    //This happens on click.. so the checked value is the opposite because the model has not been applied
    //Have to do this because ng-checked causes the event to get fired way too many times
    $scope.setAdminUser = function (user) {
        
        delete $scope.userBeingSaved;
        $scope.userBeingSaved = user;
        $scope.userBeingSaved.IsAdmin = !user.IsAdmin;
        messagingService.publish("UpdateUser", $scope.userBeingSaved);
        awaitingUpdate = true;
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName));
            }
        }, 5000);

    }

    //This happens on click.. so the checked value is the opposite because the model has not been applied
    //Have to do this because ng-checked causes the event to get fired way too many times
    $scope.setAutoLogin = function (user) {
        awaitingUpdate = true;
        
        delete $scope.userBeingSaved;
        $scope.userBeingSaved = user;
        $scope.userBeingSaved.IsAutoLogin = !user.IsAutoLogin;
        messagingService.publish("UpdateUser", $scope.userBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName));
                awaitingUpdate = false;
                
                delete $scope.userBeingSaved;
                $scope.userBeingSaved = {};
            }
        }, 5000);
    }

    $scope.saveUser = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        if ($scope.selectedUser.Password === $scope.selectedUser.UserName) {
            
            delete $scope.selectedUser.Password;
            $scope.selectedUser.Password = "";
            
            delete $scope.selectedUser.confirmPassword;
            $scope.selectedUser.confirmPassword = "";
            $scope.$broadcast('show-errors-reset');
            alertService.addError(translationService.translate('User.Validation.PasswordCannotMatchUsername'), 5000);
            return;
        }
        if ($scope.selectedUser.Password !== $scope.selectedUser.confirmPassword) {
            
            delete $scope.selectedUser.Password;
            $scope.selectedUser.Password = "";
            
            delete $scope.selectedUser.confirmPassword;
            $scope.selectedUser.confirmPassword = "";
            $scope.$broadcast('show-errors-reset');
            alertService.addError(translationService.translate('User.Validation.PasswordsMustMatch'), 5000);
            return;
        }
        if ($scope.selectedUser.IsAdmin && $scope.selectedUser.IsAutoLogin) {
            $scope.$broadcast('show-errors-reset');
            alertService.addError(translationService.translate('User.Validation.AdminCannotAutoLogin'), 5000);
            return;
        }

        awaitingUpdate = true;
        
        delete $scope.userBeingSaved;
        $scope.userBeingSaved = {
            Id: $scope.selectedUser.Id,
            UserName: $scope.selectedUser.UserName,
            Password: $scope.selectedUser.Password,
            IsNewUser: $scope.selectedUser.IsNewUser,
            IsAdmin: $scope.selectedUser.IsAdmin ? 1 : 0,
            IsAutoLogin: $scope.selectedUser.IsAutoLogin ? 1 : 0
        };
        var action = "CreateUser";
        if (!$scope.userBeingSaved.IsNewUser)
            action = "UpdateUser";
        messagingService.publish(action, $scope.userBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('User.Label.NameSingular'), $scope.userBeingSaved.UserName));
                awaitingUpdate = false;
                
                delete $scope.userBeingSaved;
                $scope.userBeingSaved = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditUser = function () {
        $state.go('^.list', null, { reload: true });
    }

    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.deleteUser = function () {
        var forceLogOut = Enumerable.From($scope.gridApi.selection.getSelectedRows()).Any(function (user) { return $scope.currentUser.Id == user.Id && user.UserName != 'PacksizeAdmin'; }) === true;

        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (user) {
            messagingService.publish("DeleteUser", user);
        });

        $timeout(function () {
            $scope.gridApi.selection.clearSelectedRows();
        }, 0, true);

        if (forceLogOut)
            authenticationService.logout();
    }

    $scope.editUser = function () {
        
        delete $scope.selectedUser;
        $scope.selectedUser = $scope.gridApi.selection.getSelectedRows()[0];
        
        delete $scope.selectedUser.Password;
        $scope.selectedUser.Password = "";
        $scope.selectedUser.IsNewUser = false;
        $state.go('^.new');
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        if (angular.lowercase($state.current.name) === 'users.new') {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('User.Help.Form.Overview')
            });

            steps.push({
                element: "#newUserUserName",
                intro: translationService.translate('User.Help.Form.Username')
            });

            steps.push({
                element: "#newUserPassword",
                intro: translationService.translate('User.Help.Form.Password')
            });

            steps.push({
                element: "#newUserPasswordConfirm",
                intro: translationService.translate('User.Help.Form.PasswordSecondCheck')
            });

            steps.push({
                element: "#newUserAutoLoginLabelContainer",
                intro: translationService.translate('User.Help.Form.AutoLoginUser')
            });

            steps.push({
                element: "#newUserAdminContainer",
                intro: translationService.translate('User.Help.Form.Administrator')
            });

            steps.push({
                element: "#newUserSaveButton",
                intro: translationService.translate('User.Help.Form.Save')
            });

            steps.push({
                element: "#newUserCancelButton",
                intro: translationService.translate('User.Help.Form.Cancel')
            });
        }
        else {
            var columnDefs = $("[ui-grid-header-cell]");

            steps.push({
                element: "#notFound",
                intro: translationService.translate('User.Help.List.Overview')
            });

            steps.push({
                element: "#newUser",
                intro: translationService.translate('User.Help.List.New')
            });

            steps.push({
                element: "#deleteUser",
                intro: translationService.translate('User.Help.List.Delete')
            });

            steps.push({
                element: "#editUser",
                intro: translationService.translate('User.Help.List.Edit')
            });

            steps.push({
                element: '#userListTable',
                intro: translationService.translate('User.Help.List.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('User.Help.List.Columns.User')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('User.Help.List.Columns.Type')
            });
        }

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};

function DeleteUserOpenDialogCtrl($modal, $scope, alertService) {
    
    $scope.opts = {
        backdrop: true,
        keyboard: true,
        templateUrl: 'partials/UserManagement/deleteUserDialog.html',
        controller: 'DeleteUserDialogCtrl',
        windowClass: "smallDialogWindow"
    };

    $scope.openDialog = function () {
        alertService.clearAll();
        $modal.open($scope.opts).result.then(function () {
            $scope.deleteUser();
        });
    };
}

function DeleteUserDialogCtrl($modalInstance, $scope) {
    $scope.closeWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.deleteUser = function () {
        $modalInstance.close();
    }
};

app.directive('deleteUserButton', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/UserManagement/deleteUserButton.html'
    };
});