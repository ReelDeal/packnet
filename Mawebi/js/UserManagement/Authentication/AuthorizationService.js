﻿var authorizationModule = angular.module('authorization', []);

authenticationModule.factory('authorizationService', function ($rootScope, $timeout, $log, messagingService, authenticationService, stateChangeService, Session, AUTH_EVENTS, USER_ROLES) {
    $timeout = $timeout.getInstance($rootScope);
    var authorizationService = {};

    authorizationService.$log = $log.getInstance("authorizationService");
//    authorizationService.$log.enableLogging(false);
    authorizationService.$rootScope = $rootScope;

    authorizationService.start = function () {
        $rootScope.$on('$stateChangeStart', authorizationService.stateChangeStart);
    };

    authorizationService.isAuthorized = function (authorizedRoles) {
        if (!angular.isArray(authorizedRoles)) {
            authorizedRoles = [authorizedRoles];
        }

        var isAuthenticated = authenticationService.isAuthenticated();
        var isAuthorized = authorizedRoles.indexOf(Session.userRole) !== -1;
        return isAuthenticated && isAuthorized;
    };

    authorizationService.isCurrentUserAdministrator = function (){
        return authorizationService.isAuthorized(USER_ROLES.admin);
    };

    authorizationService.stateChangeStart = function (event, toState, toParams, fromState, fromParams) {
        stateChangeService.$log.debug("stateChangeStart: " + JSON.stringify(toState));
        //TODO: Fix exception here on reload of browser (data is null)
        var authorizedRoles = toState.data.authorizedRoles;

        if (authorizedRoles) {
            stateChangeService.$log.debug("Roles are specified");
            var containsAll = Enumerable.From(authorizedRoles).FirstOrDefault(null, function (role) {
                return role == USER_ROLES.all;
            });
            stateChangeService.$log.debug("Contains All: " + containsAll);
            if (containsAll) {
                return;
            }
        }

        if (!authorizedRoles || !authorizationService.isAuthorized(authorizedRoles)) {
            stateChangeService.$log.debug("Preventing state change");
            event.preventDefault();
            if (authenticationService.isAuthenticated()) {
                stateChangeService.$log.info("User is not authorized");
                // user is not allowed
                $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
                stateChangeService.notAuthorized();

            } else {
                stateChangeService.$log.debug("User is not authenticated");
                stateChangeService.persistIntendedState(toState);
                $rootScope.$broadcast(AUTH_EVENTS.notAuthenticated, toState);
            }
        }
    };

    return authorizationService;
});