﻿//https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

var authenticationModule = angular.module('authentication', []);


authenticationModule.service('Session', function () {
    this.create = function (sessionId, userId, userRole) {
        this.id = sessionId;
        this.userId = userId;
        this.userRole = userRole;
    };
    this.destroy = function () {
        this.id = null;
        this.userId = null;
        this.userRole = null;
    };
    return this;
});

authenticationModule.factory('authenticationService', function ($rootScope, $timeout, $log, $translate, messagingService, Session, AUTH_EVENTS, USER_ROLES, localStorageService, tmhDynamicLocale) {
    $timeout = $timeout.getInstance($rootScope);
    var authenticationService = {};
    authenticationService.queuedRequests = [];
    authenticationService.localStorageService = localStorageService;
    authenticationService.userStorageKey = "USER_977AD149722C41E4B36C9940BFC4A7C8";
    authenticationService.languageKey = "LOCALIZED_LANGUAGE";

    messagingService.signalRConnectedObservable.autoSubscribe(function () {
        for (var i = 0; i < authenticationService.queuedRequests.length; i++) {
            authenticationService.queuedRequests[i]();
        }
    }, $rootScope, "Authentication Service on connected");

    authenticationService.userUpdateFunction = function () { };
    authenticationService.userPreferencesUpdateFunction = function () { };
    authenticationService.$log = $log.getInstance("authenticationService");
    //    authenticationService.$log.enableLogging(false);
    authenticationService.$rootScope = $rootScope;
    authenticationService.login = function (userName, password) {
        authenticationService.$log.debug("Requesting login of user: " + userName);
        messagingService.publish("LoginUser", { UserName: userName, Password: password });
    };

    authenticationService.start = function (){
        var user = authenticationService.localStorageService.get(authenticationService.userStorageKey);
        if (!!user && user.Token) {
            authenticationService.loginByToken(user.Token);
        }

        //load stored localized language
        var language = authenticationService.localStorageService.get(authenticationService.languageKey);
        if (language) {
            $rootScope.currentLanguage = language;

            //load angular localization files
            var lang = language.replace('_', '-');
            tmhDynamicLocale.set(lang);

            $translate.preferredLanguage(language);
            $translate.use(language);
            $rootScope.currentLanguage = language;
        }

        return;
    }

    authenticationService.isAuthenticated = function () {
        return !!Session.userId;
    };

    authenticationService.updateCurrentUser = function (user){
        user.Password = '';
        authenticationService.localStorageService.set(authenticationService.userStorageKey, user);
        authenticationService.$log.debug("Updating current user to: " + JSON.stringify(user));
        authenticationService.$rootScope.currentUser = user;

        //store localized language
        var selectedLanguage = $rootScope.currentLanguage;
        var localLanguage = authenticationService.localStorageService.get(authenticationService.languageKey);
        if (selectedLanguage && (selectedLanguage !== localLanguage)) {
            authenticationService.localStorageService.set(authenticationService.languageKey, selectedLanguage);
        }
    };

    authenticationService.logout = function () {
        authenticationService.$log.info("Logging out");
        Session.destroy();
        authenticationService.$rootScope.currentUser = null;
        authenticationService.localStorageService.set(authenticationService.userStorageKey, null);
        authenticationService.$log.debug("Broadcasting logout message");
        $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
    }

    authenticationService.loginByToken = function (token) {
        authenticationService.$log.debug("Requesting login of user by token: " + token);
        if (messagingService.IsSignalRConnected()) {
            messagingService.publish("LoginUserByToken", { UserName: "", Password: "", Token: token });
        } else {
            authenticationService.queuedRequests.push(function () {
                messagingService.publish("LoginUserByToken", { UserName: "", Password: "", Token: token });
            });
        }
    }

    authenticationService.requestAutoLoginUser = function (event, destination) {
        authenticationService.$log.debug("Request autologin user");

        if (messagingService.IsSignalRConnected()) {
            messagingService.publish("AutoLogin");
        } else {
            authenticationService.queuedRequests.push(function () {
                messagingService.publish("AutoLogin");
            });
        }
    };

    authenticationService.loginResponse = function (data) {
        authenticationService.$log.debug("Login response: " + JSON.stringify(data));
        if (data && data.Result === "Success") {
            var user = data.Data;
            Session.create(user.Id, user.Id, user.IsAdmin ? USER_ROLES.admin : USER_ROLES.operator);
            authenticationService.updateCurrentUser(user);
            authenticationService.$log.debug("(LoginResponse) Broadcasting login success");
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        } else {
            authenticationService.$log.debug("(LoginResponse) Broadcasting login failure");
            $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
        }
    };

    authenticationService.autoLoginResponse = function (data) {
        if (authenticationService.isAuthenticated())
            return;
        authenticationService.$log.debug("Autologin response: " + JSON.stringify(data));
        if (data && data.IsAutoLogin) {
            var user = data;
            //TODO: Remove hard coded admin
            Session.create(user.Id, user.Id, user.IsAdmin ? USER_ROLES.admin : USER_ROLES.operator);
            authenticationService.updateCurrentUser(user);
            authenticationService.$log.debug("(AutoLoginUser) Broadcasting login success");
            $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
        } else {
            authenticationService.$log.debug("(AutoLoginUser) Broadcasting authentication requested");
            $rootScope.$broadcast(AUTH_EVENTS.authenticationRequested);
        }
    };

    authenticationService.userPreferences = function (data) {
        if (data && data.Data) {
            authenticationService.userPreferencesUpdateFunction(data.Data);
        }
    };

    authenticationService.userPreferencesUpdated = function (data) {
        if (data && data.Data) {
            authenticationService.userPreferencesUpdateFunction(data.Data);
        }
    };

    $rootScope.$on(AUTH_EVENTS.notAuthenticated, authenticationService.requestAutoLoginUser);

    var autoLoggedInObservable = messagingService.messageSubject
        .where(messagingService.filterObs("AutoLoggedIn"));

    var userLoggedInObservable = messagingService.messageSubject
        .where(messagingService.filterObs("UserLoggedIn"));

    var userPreferencesObservable = messagingService.messageSubject
        .where(messagingService.filterObs("UserPreferences"));

    var userPreferencesUpdatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("UserPreferencesUpdated"));

    autoLoggedInObservable.autoSubscribe(authenticationService.autoLoginResponse, $rootScope, "autoLoggedInSubscription");
    userLoggedInObservable.autoSubscribe(authenticationService.loginResponse, $rootScope, "userLoggedIn");
    userPreferencesObservable.autoSubscribe(authenticationService.userPreferences, $rootScope, "userPreferences");
    userPreferencesUpdatedObservable.autoSubscribe(authenticationService.userPreferencesUpdated, $rootScope, "userPreferencesUpdated");

    return authenticationService;
});

authenticationModule.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function (response) {
            $rootScope.$broadcast({
                401: AUTH_EVENTS.notAuthenticated,
                403: AUTH_EVENTS.notAuthorized,
                419: AUTH_EVENTS.sessionTimeout,
                440: AUTH_EVENTS.sessionTimeout
            }[response.status], response);
            return $q.reject(response);
        }
    };
});

authenticationModule.config(function ($httpProvider) {
    $httpProvider.interceptors.push([
        '$injector',
        function ($injector) {
            return $injector.get('AuthInterceptor');
        }
    ]);
});