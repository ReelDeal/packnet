﻿var stateChangeModule = angular.module('stateChange', []);



stateChangeModule.factory('stateChangeService', function ($rootScope, $timeout, $state, $location, $log, messagingService, persistentStorageService, machineGroupService, authenticationService, Session, stateChangeConstants, AUTH_EVENTS, Machines_Constants, USER_ROLES) {
    $timeout = $timeout.getInstance($rootScope);
    var stateChangeService = {};
    stateChangeService.$log = $log.getInstance("stateChangeService");
//    stateChangeService.$log.enableLogging(false);

    persistentStorageService.addUrlParam('machineId', function (newValue) {
        stateChangeService.$log.info("MachineId change registered. Updating value to " + newValue);
        machineGroupService.updateCurrentMachineGroup(newValue);
    });

    persistentStorageService.addUrlParam('userToken', function (newValue) {
        stateChangeService.$log.info("UserId change registered. Updating value to " + newValue);
        authenticationService.loginByToken(newValue);
    });

    stateChangeService.getIntendedState = function () {
        return persistentStorageService.get(stateChangeConstants.intendedState);
    }

    stateChangeService.persistIntendedState = function (toState) {
        if (toState.data) {
            if (toState.data.directlyAccessible == undefined) {
                stateChangeService.$log.debug("Persisting state. Directly Accessible not set. State is:" + JSON.stringify(toState));
                persistentStorageService.add(stateChangeConstants.intendedState, toState);
            } else if (toState.data.directlyAccessible) {
                stateChangeService.$log.debug("Persisting state. Directly Accessible is true. State is:" + JSON.stringify(toState));
                persistentStorageService.add(stateChangeConstants.intendedState, toState);
            } else if (toState.data.redirectTo) {
                stateChangeService.$log.debug("Persisting state. Redirect to specified. Redirecting to: " + toState.data.redirectTo + " State is:" + JSON.stringify(toState));
                var redirectObj = {};
                redirectObj.name = toState.data.redirectTo;
                persistentStorageService.add(stateChangeConstants.intendedState, redirectObj);
            }
        } else {
            stateChangeService.$log.debug("Persisting state. Data not specified. State is:" + JSON.stringify(toState));
            persistentStorageService.add(stateChangeConstants.intendedState, toState);
        }
    }

    stateChangeService.goToIntendedStateOrDefault = function () {
        stateChangeService.$log.debug("Go to intended state or default");
        var nextState = persistentStorageService.get(stateChangeConstants.intendedState);

        if (!nextState) {
            stateChangeService.$log.debug("Next state not specified, setting to Dashboard");
            nextState = {};
            nextState.value = {};
            nextState.value.name = $rootScope.currentUser.IsAdmin ? "Dashboard" : "SelectMachineGroup";
        }
        stateChangeService.$log.info("Transitioning to state: " + JSON.stringify(nextState));
        $state.transitionTo(nextState.value.name);
    }

    stateChangeService.notAuthorized = function () {
        stateChangeService.$log.info("Redirecting to not authorized page");
        $state.transitionTo("NotAuthorized");
    };

    stateChangeService.stateChangeStart = function (event, toState, toParams, fromState, fromParams) {
        stateChangeService.$log.debug("State change beginning");
        if (!messagingService.IsSignalRConnected() && toState.name !== "Connecting") {
            stateChangeService.$log.debug("SignalR not connected, redirecting to connect");
            event.preventDefault();

            stateChangeService.persistIntendedState(toState);

            $state.transitionTo("Connecting");
        }
    };

    stateChangeService.previousState = function (){
        return $rootScope.previousState;
    }

    stateChangeService.stateChangeSuccess = function (event, toState, toParams, fromState, fromParams) {
        $rootScope.previousState = fromState.name;
        $rootScope.currentState = toState.name;
    }

    stateChangeService.start = function () {
        $rootScope.$on('$viewContentLoading', function () {
            if (!$state.$current.locals)
                $state.$current.locals = {};
        });
        $rootScope.$on('$stateChangeStart', stateChangeService.stateChangeStart);
        $rootScope.$on('$stateChangeSuccess', stateChangeService.stateChangeSuccess);
        $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
            if (messagingService.IsSignalRConnected()) {
                messagingService.publish('FindUserPreferences', $rootScope.currentUser);
            } else {
                messagingService.signalRConnectedObservable.subscribe(function () {
                    messagingService.publish('FindUserPreferences', $rootScope.currentUser);
                }, $rootScope, 'SignalR Connected');
            }
            stateChangeService.goToIntendedStateOrDefault();
        });

        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
            stateChangeService.$log.debug("Logout auth event received");
            $state.go("Login");
        });

        $rootScope.$on(AUTH_EVENTS.authenticationRequested, function () {
            stateChangeService.$log.debug("Authentication requested auth event received");
            $state.go("Login");
        });

        $rootScope.$on(Machines_Constants.machineGroupNotSelected, function (options, args) {
            stateChangeService.$log.debug("Machine Group not selected");
            var intendedMachineId = persistentStorageService.getUrlParam('machineId');
            if (!intendedMachineId) {
                stateChangeService.$log.debug("No machine specified, redirecting to select one");
                $state.go("SelectMachineGroup", { endState: args.endState });
            } else {
                stateChangeService.$log.debug("Machine specified. Updating machine service");
                machineGroupService.updateCurrentMachineGroup(intendedMachineId);
            }
        });
    };

    return stateChangeService;
});
