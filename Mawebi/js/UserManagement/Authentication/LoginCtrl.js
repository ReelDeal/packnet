function LoginCtrl($rootScope, $scope, $timeout, $state, $translate, messagingService, alertService, AUTH_EVENTS, Configured_Languages, tmhDynamicLocale, translationService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.userName = '';
    $scope.password = '';
    $scope.languages = $.map(Configured_Languages, function (name, code) {
        return {
            "code": code,
            "name": name
        }
    });

    delete $scope.language;
    if (!$rootScope.currentLanguage) {
        $scope.language = 'en_US';
    }
    else {
        $scope.language = $rootScope.currentLanguage;
    }   

    $scope.selectLanguage = function (language) {
        
        delete $scope.language;
        $scope.language = language;
        $scope.languageIsOpen = false;

        //load angular localization files
        var lang = language.replace('_', '-');
        tmhDynamicLocale.set(lang);

        $translate.preferredLanguage(language);
        $translate.use(language);
        $rootScope.currentLanguage = language;
    }

    $scope.selectedLanguageName = function (){
        return Configured_Languages[$scope.language];
    }

    $scope.login = function (userName, password) {
        var obj = {};
        obj.UserName = userName;
        obj.Password = password;
        messagingService.publish("LoginUser", obj);
    }

    $scope.$on(AUTH_EVENTS.loginFailed, function () {
        var msg = translationService.translate("Login.Validation.InvalidLogin");
        alertService.addError(msg, 5000);
    });
}