function PickZonesCtrl($location, $scope, $state, $timeout, messagingService, alertService, uiGridConstants, introJsService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.isEditMode = false;
    $scope.pickZones = [];
    $scope.selectedPickZone = {};
    $scope.selectedPickZone.IsNewPickZone = true;

    var awaitingUpdate = false;

    $scope.$on('$locationChangeSuccess', function () {
        if ($scope.selectedPickZone && $scope.selectedPickZone.IsNewPickZone == false && $location.path() == '/PickZones') {
            $scope.cancelNewEditPickZone();
        }
    });

    messagingService.pickZonesObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            if ($scope.gridApi.selection.getSelectedRows().length === 0) {
                
                delete $scope.pickZones;
                $scope.pickZones = msg.Data;
            }
        }, 0, true);
    }, $scope, 'PickZones');

    messagingService.pickZoneCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('PickZone.Label.NameSingular'), $scope.pickZoneBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('PickZone.Label.NameSingular'), $scope.pickZoneBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('PickZone.Label.NameSingular'), $scope.pickZoneBeingSaved.Alias), 5000);
        }
    }, $scope, 'PickZone Created');

    messagingService.pickZoneUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            if (msg.Message === "PickZoneInProduction") {
                alertService.addError(translationService.translate('PickZone.Message.UpdateInProduction', msg.Data.Alias));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('PickZone.Label.NameSingular'), $scope.pickZoneBeingSaved.Alias));
            }
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('PickZone.Label.NameSingular'), $scope.pickZoneBeingSaved.Alias));
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('PickZone.Label.NameSingular'), $scope.pickZoneBeingSaved.Alias), 5000);
        }
    }, $scope, 'PickZone Updated');

    messagingService.pickZoneDeletedObservable.autoSubscribe(function (msg) {
        if (msg.Result === "Fail") {
            if (msg.Message === "PickZoneInProduction") {
                alertService.addError(translationService.translate('PickZone.Message.DeleteInProduction', msg.Data.Alias));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.DeleteFailed', translationService.translate('PickZone.Label.NameSingular'), msg.Data.Alias));
            }
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', translationService.translate('PickZone.Label.NameSingular'), msg.Data.Alias), 5000);
            
            delete $scope.pickZoneBeingSaved;
            $scope.pickZoneBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedPickZone;
            $scope.selectedPickZone = {};
            $scope.selectedPickZone.IsNewPickZone = true;
        }
        requestData();
    }, $scope, 'PickZone Deleted');

    var requestData = function () {
        messagingService.publish("GetPickZones");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'pickZones',
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                width: 150,
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                field: 'Description',
                name: translationService.translate('Common.Label.Description')
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
       }
    };


    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.savePickZone = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        
        delete $scope.pickZoneBeingSaved;
        $scope.pickZoneBeingSaved = {
            Id: $scope.selectedPickZone.Id,
            Alias: $scope.selectedPickZone.Alias,
            Description: $scope.selectedPickZone.Description,
            IsNewPickZone: $scope.selectedPickZone.IsNewPickZone,
        };
        var action = "CreatePickZone";
        if (!$scope.pickZoneBeingSaved.IsNewPickZone)
            action = "UpdatePickZone";
        messagingService.publish(action, $scope.pickZoneBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('PickZone.Label.NameSingular'), $scope.pickZoneBeingSaved.Alias));
                awaitingUpdate = false;
                
                delete $scope.pickZoneBeingSaved;
                $scope.pickZoneBeingSaved = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditPickZone = function () {
    	$state.go('^.list', null, { reload: true });
    }

    $scope.deletePickZones = function () {
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (pickZone) {
            messagingService.publish("DeletePickZone", pickZone);
        });

        $scope.gridApi.selection.clearSelectedRows();
    }

    $scope.editPickZone = function () {
        
        delete $scope.selectedPickZone;
        $scope.selectedPickZone = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedPickZone.IsNewPickZone = false;
        $scope.isEditMode = true;
        $state.go('^.new');
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        if (angular.lowercase($state.current.name) === "pickzones.new") {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('PickZone.Help.Form.Overview')
            });

            steps.push({
                element: "#newPickZonePickZoneName",
                intro: translationService.translate('PickZone.Help.Form.Name')
            });

            steps.push({
                element: "#newMachineDescription",
                intro: translationService.translate('PickZone.Help.Form.Description')
            });

            steps.push({
                element: "#newPickZoneSaveButton",
                intro: translationService.translate('PickZone.Help.Form.Save')
            });

            steps.push({
                element: "#newPickZoneCancelButton",
                intro: translationService.translate('PickZone.Help.Form.Cancel')
            });
        }
        else {
            var columnDefs = $("[ui-grid-header-cell]");

            steps.push({
                element: "#notFound",
                intro: translationService.translate('PickZone.Help.List.Overview')
            });

            steps.push({
                element: "#newPickZone",
                intro: translationService.translate('PickZone.Help.List.New')
            });

            steps.push({
                element: "#deletePickZone",
                intro: translationService.translate('PickZone.Help.List.Delete')
            });

            steps.push({
                element: "#editPickZone",
                intro: translationService.translate('PickZone.Help.List.Edit')
            });

            steps.push({
                element: '#pickZoneListTable',
                intro: translationService.translate('PickZone.Help.List.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('PickZone.Help.List.Columns.Name')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('PickZone.Help.List.Columns.Description')
            });
        }

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};