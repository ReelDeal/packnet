﻿app.factory('notificationService', function ($rootScope, messagingService, machineGroupService, AUTH_EVENTS, NOTIFICATIONS_EVENTS) {
    var self = {};

    var currentMachineGroup = null;
    var notifications = [];

    var activeUserNotificationsByUserIdObservable = messagingService.messageSubject
            .where(messagingService.filterObs("ActiveUserNotificationsByUserId"));

    var dismissedAllUserNotificationsObservable = messagingService.messageSubject
        .where(messagingService.filterObs("DismissedAllUserNotifications"));

    var dismissedUserNotificationObservable = messagingService.messageSubject
        .where(messagingService.filterObs("DismissedUserNotification"));

    var publishedUserNotificationObservable = messagingService.messageSubject
            .where(messagingService.filterObs("PublishedUserNotification"));

    function clearNotifications() {
        notifications = [];

        $rootScope.$broadcast(NOTIFICATIONS_EVENTS.changed);
    }

    function isResponseValidForCurrentUser(response) {
        return response.Result === 'Success' && response.UserName === $rootScope.currentUser.UserName;
    }

    function loadNotifications() {
        messagingService.publish('GetActiveUserNotificationsByUserId', $rootScope.currentUser.Id);
    }

    self.dismiss = function (userNotification) {
        var payload = {
            Item1: userNotification.Id,
            Item2: $rootScope.currentUser.Id
        };

        messagingService.publish('DismissUserNotification', payload);
    }

    self.dismissAll = function () {

        if (currentMachineGroup === null)
            messagingService.publish('DismissAllUserNotificationsForMg', $rootScope.currentUser.Id);
        else
            messagingService.publish('DismissAllUserNotifications', [currentMachineGroup.Id, $rootScope.currentUser.Id]);

    }

    self.getNotifications = function () {
        var result = [];

        // Filter notifications to only return "System" ones and also the ones related to the current "Machine Group"
        // The filtering is done client side to reduce the calls to the server
        result = Enumerable
            .From(notifications)
            .Where(function (n) {
                return n.NotifiedMachineGroups &&
                    (n.NotifiedMachineGroups.length === 0 ||
                    (currentMachineGroup != null && n.NotifiedMachineGroups.indexOf(currentMachineGroup.Id) > -1));
            })
            .ToArray();


        //Compress user notifications -> AAAAABBBAA -> A5B3A2
        var compressedResults = [];

        if (result.length > 0) {
            compressedResults.push(result[0]);

            for (var i = 1; i < result.length; i++) {
                if (result[i].Message !== result[i - 1].Message) {
                    compressedResults.push(result[i]);
                }
                else {
                    compressedResults[compressedResults.length - 1].MessageCount++;
                }
            }
        }

        return compressedResults;
    }

    self.notificationsContainErrors = function (notificationsArray) {
        return Enumerable.From(notificationsArray).Any(function (n) {
            return n.Severity === 'Error';
        });
    }

    self.notificationsContainWarnings = function (notificationsArray) {
        return Enumerable.From(notificationsArray).Any(function (n) {
            return n.Severity === 'Warning';
        });
    }

    self.start = function () {
        // $broadcast listeners
        $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
            loadNotifications();
        });

        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
            clearNotifications();
        });
    }

    // Listeners
    machineGroupService.currentMachineGroupUpdatedObservable.autoSubscribe(function (newMachineGroup) {
        var currentMachineGroupId = currentMachineGroup !== null ? currentMachineGroup.Id : null;
        var newMachineGroupId = newMachineGroup !== null ? newMachineGroup.Id : null;

        // This observable gets triggered several times in a row
        if (currentMachineGroupId !== newMachineGroupId) {
            currentMachineGroup = newMachineGroup;

            $rootScope.$broadcast(NOTIFICATIONS_EVENTS.changed);
        }
    }, $rootScope, 'Refresh Notifications due to a Machine Group change');

    activeUserNotificationsByUserIdObservable.autoSubscribe(function (msg) {
        notifications = msg.Data;

        $rootScope.$broadcast(NOTIFICATIONS_EVENTS.changed);
    }, $rootScope, 'Active Notifications by User');

    dismissedAllUserNotificationsObservable.autoSubscribe(function (msg) {
        if (isResponseValidForCurrentUser(msg)) {
            loadNotifications();
        }
    }, $rootScope, "Dismissed All User Notifications");

    dismissedUserNotificationObservable.autoSubscribe(function (msg) {
        if (isResponseValidForCurrentUser(msg)) {
            for (var i = 0; i < notifications.length; i++) {
                if (notifications[i].Id === msg.Data) {
                    notifications.splice(i, 1);

                    $rootScope.$broadcast(NOTIFICATIONS_EVENTS.changed);
                    break;
                }
            }
        }
    }, $rootScope, "Dismissed User Notification");

    publishedUserNotificationObservable.autoSubscribe(function (msg) {
        notifications.splice(49, 1);
        notifications.splice(0, 0, msg.Data);
        $rootScope.$broadcast(NOTIFICATIONS_EVENTS.changed);
    }, $rootScope, 'Published Notification');

    return self;
});