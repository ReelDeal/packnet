app.controller('NotifierCtrl', ['$scope', '$timeout', 'notificationService', 'NOTIFICATIONS_EVENTS',
	function ($scope, $timeout, notificationService, NOTIFICATIONS_EVENTS) {
	    $timeout = $timeout.getInstance($scope);
	    
	    delete $scope.notifications;
	    $scope.notifications = [];
	    $scope.notificationsContainErrors = false;
	    $scope.notificationsContainWarnings = false;

	    $scope.dismiss = function (notification, event) {
	        if ($scope.notifications.length > 1) {
	            // Prevent the dropdown from closing if there is more than one item
	            event.stopPropagation();
	        }

	        notificationService.dismiss(notification);
	    }

	    $scope.dismissAll = function () {
	        notificationService.dismissAll();
	    }

	    // $broadcast listeners
	    $scope.$on(NOTIFICATIONS_EVENTS.changed, function () {
	        $timeout(function () {
	            
	            delete $scope.notifications;
	            $scope.notifications = notificationService.getNotifications();
	            
	            $scope.notificationsContainErrors = notificationService.notificationsContainErrors($scope.notifications);
	            $scope.notificationsContainWarnings = notificationService.notificationsContainWarnings($scope.notifications);
	        }, 0, true);
	    });
	}
]);