function UPCManagementCtrl($modal, $scope, $state, $stateParams, $timeout, alertService, authorizationService, csvService, introJsService, packagingDesignService, messagingService, stateChangeService, translationService) {
    $timeout = $timeout.getInstance($scope, 'UPCManagementCtrl');
    
    $scope.upcs = [];
    $scope.unfilteredUpcs = [];
    $scope.showFilters = false;
    $scope.filterEdited = false;
    $scope.filterCreated = false;
    $scope.toggleFilterMessage = '';
    $scope.searchOptions = {
        UPC: "",
        Length: "",
        Width: "",
        Height: "",
        Weight: "",
        createdDateRange: {
            from: new Date().startOfDay(),
            to: new Date().endOfDay(),
        },
        editedDateRange: {
            from: new Date().startOfDay(),
            to: new Date().endOfDay(),
        }
    };

    $scope.packagingDesigns = packagingDesignService.getAllDesigns();

    var awaitingUpdate = false;

    $scope.isAdmin = function () {
        return authorizationService.isCurrentUserAdministrator();
    }

    $scope.shouldShowCloseButton = function () {
        return authorizationService.isCurrentUserAdministrator() == false || $stateParams.fromOperatorPanel == true;
    }

    $scope.gridOptions = {
        enableGridMenu: true,
        data : 'upcs',
        enablePaging : true,
        showFilter : false,
        showSelectionCheckbox : true,
        enableColumnResizing : true,
        enableHeaderRowSelection : true,
        enableSelectAll : $scope.isAdmin(),
        multiSelect : $scope.isAdmin(),
        headerRowHeight : 36,
        showGridFooter : true,
        columnDefs : [
            {
                field : 'UPC',
                displayName : translationService.translate('UPC.Label.NameSingular'),
                sortingAlgorithm : function (a, b){

                    if (isNaN(a) && isNaN(b)) {
                        return a === b ? 0 : (a < b ? -1 : 1);
                    }

                    if (!isNaN(a) && !isNaN(b)) {
                        var idA = parseFloat(a);
                        var idB = parseFloat(b);

                        return idA === idB ? 0 : (idA < idB ? -1 : 1);
                    }

                    return 0;
                },
            },
            {
                field: 'Description',
                displayName: translationService.translate('Common.Label.Description')
            },
            {
                field: 'Length',
                displayName: translationService.translate('Common.Label.Length'), width: 90
            },
            {
                field: 'Width',
                displayName: translationService.translate('Common.Label.Width'), width: 90
            },
            {
                field: 'Height',
                displayName: translationService.translate('Common.Label.Height'), width: 90
            },
            {
                field: 'Weight',
                displayName: translationService.translate('Common.Label.Weight'), width: 90
            },
            {
                field : 'DesignId',
                displayName: translationService.translate('Common.Label.Design'),
                cellFilter : 'designNameFromId'
            },
            {
                field : 'DesignId',
                displayName: translationService.translate('Common.Label.DesignId')
            },
            {
                field : 'CreateDate',
                displayName: translationService.translate('Common.Label.CreatedDate'),
                sortingAlgorithm : function (a, b){
                    var timeA = typeof (a) === "string" ? new Date(a).getTime() : a.getTime(),
                        timeB = typeof (b) === "string" ? new Date(b).getTime() : b.getTime();
                    return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                },
                cellFilter : 'datemedium'
            },
            {
                field : 'LastUpdatedDate',
                displayName : translationService.translate('Common.Label.LastUpdatedDate'),
                sortingAlgorithm : function (a, b){
                    var timeA = typeof (a) === "string" ? new Date(a).getTime() : a.getTime(),
                        timeB = typeof (b) === "string" ? new Date(b).getTime() : b.getTime();
                    return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                },
                cellFilter : 'datemedium'
            }
        ],
        onRegisterApi : function (gridApi){
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;
        }
    };

    $scope.filter = function () {
        var enumerable = Enumerable.From($scope.unfilteredUpcs);

        if ($scope.showFilters) {
            enumerable = enumerable.Where(function (upc) { return upc.UPC.indexOf($scope.searchOptions.UPC) != -1; });
            enumerable = enumerable.Where(function (upc) { return upc.Length.toString().indexOf($scope.searchOptions.Length) != -1; });
            enumerable = enumerable.Where(function (upc) { return upc.Width.toString().indexOf($scope.searchOptions.Width) != -1; });
            enumerable = enumerable.Where(function (upc) { return upc.Height.toString().indexOf($scope.searchOptions.Height) != -1; });
            enumerable = enumerable.Where(function (upc) { return upc.Weight.toString().indexOf($scope.searchOptions.Weight) != -1; });
            enumerable = enumerable.Where(function (upc) { return upc.DesignId == $scope.searchOptions.DesignId || $scope.searchOptions.DesignId == null; });

            if ($scope.filterCreated) {
                enumerable = enumerable.Where(function (upc) {
                    return new Date(upc.CreateDate).IsBetween($scope.searchOptions.createdDateRange.from, $scope.searchOptions.createdDateRange.to);
                });
            }

            if ($scope.filterEdited) {
                enumerable = enumerable.Where(function (upc) {
                    return new Date(upc.LastUpdatedDate).IsBetween($scope.searchOptions.editedDateRange.from, $scope.searchOptions.editedDateRange.to);
                });
            }
        }

        
        delete $scope.upcs;
        $scope.upcs = enumerable.ToArray();

        //this $apply is needed when we have very big sets of UPCS
        //I noticed that the data comes in to late for angular to notice it. And since the page is kept pristine unless you touch it a digest cycle is never run and therefore an update to the grid is never done.
        //Please don't remove or find a better way to force an update when you have very big data sets
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }

    $scope.toggleFilters = function () {
        $scope.showFilters = !$scope.showFilters;

        if ($scope.showFilters == false)
            $scope.filter();
    }

    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.exportUPCs = function () {
        awaitingUpdate = true;
        messagingService.publish("ExportUPCs");
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('UPC.Message.ExportFailed'));
                awaitingUpdate = false;
            }
        }, 5000);
    }

    $scope.editUPC = function () {
        
        delete $scope.selectedUPC;
        $scope.selectedUPC = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedUPC.IsNewUPC = false;
        $scope.isEditMode = true;
        $state.go('^.new', { selectedUPC: $scope.selectedUPC }, { reload: false });
    }

    $scope.deleteUPC = function () {

        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SLD/UPCDeleteConfirmationDialog.html',
            controller: 'UPCDeleteCtrl', //defined end of this file
            resolve: {
                itemsToDelete: function () {
                    return $scope.gridApi.selection.getSelectedRows();
                }
            }
        };

        $modal.open(opts).result.then(function () {
            awaitingUpdate = true;
            $timeout(function () {
                if (awaitingUpdate) {
                    alertService.addError(translationService.translate('UPC.Message.DeleteFailed'));
                    awaitingUpdate = false;
                }
            }, 5000);
        });
    }

    $scope.close = function () {
        $state.go(stateChangeService.previousState(), null, { reload: false });
    }

    $scope.$watch('showFilters', function (newValue) {
        
        delete $scope.toggleFilterMessage;
        $scope.toggleFilterMessage = newValue ? translationService.translate('UPC.Label.HideFilters') : translationService.translate('UPC.Label.ShowFilters');
    });

    messagingService.messageSubject
        .where(messagingService.filterObs("AllUPCs"))
        .select(messagingService.dataFunction).autoSubscribe(function (data) {
            
            delete $scope.unfilteredUpcs;
            $scope.unfilteredUpcs = data;
            $scope.filter();
        }, $scope, 'All UPCs');

    messagingService.messageSubject
        .where(messagingService.filterObs("ExportUPCsResponse"))
        .select(messagingService.responseDataFunction).autoSubscribe(function (data) {
            if (data.Result === "Success") {
                alertService.addSuccess(translationService.translate('UPC.Message.ExportSuccess'), 5000);
                awaitingUpdate = false;
                csvService.saveCsVtoFile(data.Data, "SLDUPCs");
            }
        }, $scope, 'All UPCs');

    messagingService.messageSubject
        .where(messagingService.filterObs("UPCsDeleted"))
        .select(messagingService.responseDataFunction).autoSubscribe(function (data) {
            if (data.Result === "Success") {
                $scope.gridApi.selection.clearSelectedRows();
                alertService.addSuccess(translationService.translate('UPC.Message.DeleteSuccess'), 5000);
                awaitingUpdate = false;
            }
        }, $scope, 'All UPCs');


    //Begin Intro JS Stuff
    var showFiltersBeforeHelp = $scope.showFilters;

    $scope.tellMeAboutThisPage = function () {
        showFiltersBeforeHelp = $scope.showFilters;

        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        steps.push({
            element: "#notFound",
            intro: translationService.translate('UPC.Help.List.Overview')
        });

        steps.push({
            element: "#newUPC",
            intro: translationService.translate('UPC.Help.List.New')
        });

        steps.push({
            element: "#deleteUPC",
            intro: translationService.translate('UPC.Help.List.Delete')
        });

        steps.push({
            element: "#editUPC",
            intro: translationService.translate('UPC.Help.List.Edit')
        });

        if ($scope.isAdmin()) {
            steps.push({
                element: "#importUPCs",
                intro: translationService.translate('UPC.Help.List.Import')
            });

            steps.push({
                element: "#exportUPCs",
                intro: translationService.translate('UPC.Help.List.Export')
            });
        }

        steps.push({
            element: "#UpcFilterInput",
            intro: translationService.translate('UPC.Help.List.UPC')
        });

        steps.push({
            element: "#enableCreatedDateFilteringCheckBox",
            intro: translationService.translate('UPC.Help.List.CreatedDateRangeToggle')
        });

        steps.push({
            element: "#UPCCreatedFromDateWrapper",
            intro: translationService.translate('UPC.Help.List.CreatedDateRangeFromDate')
        });

        steps.push({
            element: "#UPCCreatedToDateWrapper",
            intro: translationService.translate('UPC.Help.List.CreatedDateRangeToDate')
        });

        steps.push({
            element: "#enableEditedDateFilteringCheckBox",
            intro: translationService.translate('UPC.Help.List.EditedDateRangeToggle')
        });

        steps.push({
            element: "#UPCEditedFromDateWrapper",
            intro: translationService.translate('UPC.Help.List.EditedDateRangeFromDate')
        });

        steps.push({
            element: "#UPCEditedToDateWrapper",
            intro: translationService.translate('UPC.Help.List.EditedDateRangeToDate')
        });

        steps.push({
            element: "#DesignSelectionDropDown",
            intro: translationService.translate('UPC.Help.List.Design')
        });

        steps.push({
            element: "#LengthFilterInput",
            intro: translationService.translate('UPC.Help.List.Length')
        });

        steps.push({
            element: "#WidthFilterInput",
            intro: translationService.translate('UPC.Help.List.Width')
        });

        steps.push({
            element: "#HeightFilterInput",
            intro: translationService.translate('UPC.Help.List.Height')
        });

        steps.push({
            element: "#WeightFilterInput",
            intro: translationService.translate('UPC.Help.List.Weight')
        });

        steps.push({
            element: "#FilterButton",
            intro: translationService.translate('UPC.Help.List.Filter')
        });

        steps.push({
            element: "#UpcGrid",
            intro: translationService.translate('UPC.Help.List.Columns.Overview')
        });

        steps.push({
            element: columnDefs[1],
            intro: translationService.translate('UPC.Help.List.Columns.UPC')
        });

        steps.push({
            element: columnDefs[2],
            intro: translationService.translate('UPC.Help.List.Columns.Description')
        });

        steps.push({
            element: columnDefs[3],
            intro: translationService.translate('UPC.Help.List.Columns.Length')
        });

        steps.push({
            element: columnDefs[4],
            intro: translationService.translate('UPC.Help.List.Columns.Width')
        });

        steps.push({
            element: columnDefs[5],
            intro: translationService.translate('UPC.Help.List.Columns.Height')
        });

        steps.push({
            element: columnDefs[6],
            intro: translationService.translate('UPC.Help.List.Columns.Weight')
        });

        steps.push({
            element: columnDefs[7],
            intro: translationService.translate('UPC.Help.List.Columns.Design')
        });

        steps.push({
            element: columnDefs[8],
            intro: translationService.translate('UPC.Help.List.Columns.DesignId')
        });

        steps.push({
            element: columnDefs[9],
            intro: translationService.translate('UPC.Help.List.Columns.CreatedDate')
        });

        steps.push({
            element: columnDefs[10],
            intro: translationService.translate('UPC.Help.List.Columns.LastUpdatedDate')
        });

        if ($scope.shouldShowCloseButton()) {
            steps.push({
                element: "#upcManagementCloseButton",
                intro: translationService.translate('UPC.Help.List.Close')
            });
        }

        return steps;
    };

    var afterIntro = function () {
        if (showFiltersBeforeHelp !== $scope.showFilters)
            $scope.toggleFilters();
    }

    introJsService.onbeforechange(function () {
        if (!$scope.showFilters) {
            $scope.toggleFilters();
        }
    });

    introJsService.oncomplete(afterIntro);

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
    //End Intro JS Stuff

    messagingService.publish("GetUPCs");
}

function UPCDeleteCtrl($scope, $modalInstance, messagingService, itemsToDelete) {
    
    $scope.itemsToDelete = itemsToDelete;
    $scope.deleteOk = function () {
        messagingService.publish("DeleteUPCs", itemsToDelete);
        $modalInstance.close();
    }

    $scope.deleteCancel = function () {
        $modalInstance.dismiss();
    }
}