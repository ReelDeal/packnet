function ImportUPCCtrl($interval, $scope, $state, $timeout, alertService, introJsService, messagingService, stateChangeService, translationService) {
    $timeout = $timeout.getInstance($scope);
    $interval = $interval.getInstance($scope);

    
    $scope.uploadme = {};
    $scope.uploadme.src = "";
    $scope.useHeadersInFile = true;
    $scope.headerFields = [];
    $scope.importCollection = {};
    $scope.importCollection.Items = [];
    $scope.configuration = {};

    $scope.awaitingUpdate = false;
    $scope.progress = 1;
    
    $scope.importGridOptions = {
        data: 'importCollection.Items',
        headerRowHeight: 36,
        enableSorting: false,
        columnDefs: [
            {
                field: 'UPC',
                displayName: translationService.translate('UPC.Label.NameSingular'),
                enableHiding: false
            },
            {
                field: 'Description',
                displayName: translationService.translate('Common.Label.Description'),
                enableHiding: false
            },
            {
                field: 'Length',
                displayName: translationService.translate('Common.Label.Length'),
                width: 90,
                enableHiding: false
            },
            {
                field: 'Width',
                displayName: translationService.translate('Common.Label.Width'),
                width: 90,
                enableHiding: false
            },
            {
                field: 'Height',
                displayName: translationService.translate('Common.Label.Height'),
                width: 90,
                enableHiding: false
            },
            {
                field: 'Weight',
                displayName: translationService.translate('Common.Label.Weight'),
                width: 90,
                enableHiding: false
            },
            {
                field: 'DesignId',
                displayName: translationService.translate('Common.Label.DesignId'),
                enableHiding: false
            },
            {
                field: 'CreateDate',
                displayName: translationService.translate('Common.Label.CreatedDate'),
                cellFilter: 'datemedium',
                enableHiding: false
            },
            {
                field: 'LastUpdatedDate',
                displayName: translationService.translate('Common.Label.LastUpdatedDate'),
                cellFilter: 'datemedium',
                enableHiding: false
            },
            {
                field: 'ImportAction',
                displayName: translationService.translate('Common.Label.Action'),
                enableHiding: false,
                menuItems: [
                    {
                        title: translationService.translate('Common.Import.Label.ImportUpdateAll'),
                        action: function () {
                            if (!$scope.importCollection || !$scope.importCollection.Items) return;

                            $scope.importCollection.Items.forEach(function (item) {
                                if (item.originalAction != 'Import') {
                                    item.ImportAction = "Update";
                                }
                                else {
                                    item.ImportAction = "Import";
                                }
                            });
                        }
                    },
                    {
                        title: translationService.translate('Common.Import.Label.IgnoreAllConflicted'),
                        action: function () {
                            if (!$scope.importCollection || !$scope.importCollection.Items) return;

                            $scope.importCollection.Items.forEach(function (item) {
                                if (item.originalAction != 'Import') item.ImportAction = "Ignore";
                            });
                        }
                    }
                ],
                cellTemplate: 'partials/SLD/importActionTemplate.html',
                cellClass: 'ui-grid-cell-contents-no-padding'
            }
        ]
    };

    function SetupProgressBarHandling(maxTime) {
        $scope.progress = 1;
        var tick = maxTime / 99;

        $scope.currentProgressBarPromise = $interval(function () { $scope.progress++; }, tick, 99);
    }

    function TearDownProgressBarHandling() {
        if ($scope.currentProgressBarPromise) {
            $interval.cancel($scope.currentProgressBarPromise);
            
            delete $scope.currentProgressBarPromise;
        }
    }

    function SetupAwaitingUpdate(delay, errorMessage){
        $scope.awaitingUpdate = true;

        var waitTime = Math.max(delay, 5000);

        SetupProgressBarHandling(waitTime);
        $timeout(function () {
            if ($scope.awaitingUpdate) {
                alertService.addError(errorMessage);
                $scope.awaitingUpdate = false;
            }
        }, waitTime);
    }

    $scope.importActionCanImport = function (item) {
        return item.originalAction == 'Import';
    };

    $scope.importActionCanUpdate = function (item) {
        return item.originalAction != 'Import';
    };

    $scope.cantFinalize = function () {
        return $scope.importCollection.Items.length == 0 || $scope.awaitingUpdate;
    }

    $scope.cantProcess = function () {
        return $scope.uploadme.src == '' || $scope.awaitingUpdate;
    }

    $scope.cantBrowse = function () {
        return $scope.configuration.ImportFileDelimeter == '' || $scope.configuration.ImportFileCommentPrefix == '' || ($scope.useHeadersInFile == false && $scope.headerFields.lenght == 0) || $scope.awaitingUpdate;
    }

    $scope.clearSelectedFile = function () {
        
        delete $scope.uploadme.src;
        $scope.uploadme.src = "";
        alertService.clearAll();
    }

    $scope.processFile = function () {
        messagingService.publish("ImportUPCs", {
            "Contents": $scope.uploadme.src,
            "Delimiter": $scope.configuration.ImportFileDelimeter,
            "CommentPrefix": $scope.configuration.ImportFileCommentPrefix,
            "HeadersInFile": $scope.useHeadersInFile,
            "HeaderFields": $scope.useHeadersInFile ? [] : Enumerable.From($scope.headerFields).Select(function (field) { return field.text; }).ToArray()
        });
        
        SetupAwaitingUpdate($scope.uploadme.src.length, translationService.translate("Common.Import.Message.ProcessFailed"));

        
        delete $scope.uploadme;
        $scope.uploadme = {};
        
        delete $scope.uploadme.src;
        $scope.uploadme.src = "";
    }

    $scope.finalizeImport = function () {

        var numItems = $scope.importCollection.Items.length;

        var items = [];
        $scope.importCollection.Items.forEach(function (item) {
            items.push({
                ImportAction: item.ImportAction,
                UPC: item.UPC,
                Description: item.Description,
                Length: item.Length,
                Width: item.Width,
                Height: item.Height,
                DesignId: item.DesignId,
                CreateDate: item.CreateDate
            });
        });
        
        delete $scope.importCollection.Items;
        $scope.importCollection.Items = items;

        messagingService.publish("FinalizeUPCImport", $scope.importCollection);
        
        delete $scope.importCollection;
        $scope.importCollection = {};
        $scope.importCollection.Items = [];

        SetupAwaitingUpdate(numItems * 10, translationService.translate("Common.Import.Message.FinalizeFailed"));
    }

    $scope.cancelButtonClick = function () {
        $scope.clearSelectedFile();
        $state.go(stateChangeService.previousState(), null, { reload: false });
    }

    messagingService.messageSubject
        .where(messagingService.filterObs("UPCsUnderImport"))
        .select(messagingService.responseDataFunction).autoSubscribe(function (data) {
            if (data.Result == 'Success') {
                
                delete $scope.importCollection;
                $scope.importCollection = data.Data;

                $scope.importCollection.Items.forEach(function (item) {
                    item.originalAction = item.ImportAction;
                });

                $scope.awaitingUpdate = false;
                alertService.addSuccess(translationService.translate("Common.Import.Message.ProcessSuccess"));

                //this $apply is needed when we have very big sets of UPCS
                //I noticed that the data comes in to late for angular to notice it. And since the page is kept pristine unless you touch it a digest cycle is never run and therefore an update to the grid is never done.
                //Please don't remove or find a better way to force an update when you have very big data sets
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            }

            TearDownProgressBarHandling();
        }, $scope, 'All UPCs');

    messagingService.messageSubject
        .where(messagingService.filterObs("UPCsImported"))
        .select(messagingService.responseDataFunction).autoSubscribe(function (data) {
            if (data.Result == 'Success') {
                $scope.awaitingUpdate = false;
                $state.go(stateChangeService.previousState(), null, { reload: false });
            }

            TearDownProgressBarHandling();
        }, $scope, 'All UPCs');

    messagingService.messageSubject
        .where(messagingService.filterObs("GetCurrentSldConfigurationResponse"))
        .select(messagingService.responseDataFunction)
        .autoSubscribe(function (msg) {
            if (msg.Result == 'Success') {
                
                delete $scope.configuration;
                $scope.configuration = msg.Data;
            }
        }, $scope, "SLD Configuration subscriber");

    $scope.resetImport = function () {
        
        delete $scope.uploadme;
        $scope.uploadme = {};
        
        delete $scope.uploadme.src;
        $scope.uploadme.src = "";

        
        delete $scope.importCollection;
        $scope.importCollection = {};
        
        delete $scope.importCollection.Items;
        $scope.importCollection.Items = [];
    };

    function SetupIntroJs() {
        //Begin Intro JS Stuff
        $scope.tellMeAboutThisPage = function () {
            var steps = [];
            var columnDefs = $("[ui-grid-header-cell]");

            steps.push({
                element: "#notFound",
                intro: translationService.translate('UPC.Import.Help.Overview')
            });

            steps.push({
                element: "#CurrentUPCItemsList",
                intro: translationService.translate('UPC.Import.Help.Columns.Overview')
            });

            steps.push({
                element: columnDefs[0],
                intro: translationService.translate('UPC.Help.List.Columns.UPC')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('UPC.Help.List.Columns.Description')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('UPC.Help.List.Columns.Length')
            });

            steps.push({
                element: columnDefs[3],
                intro: translationService.translate('UPC.Help.List.Columns.Width')
            });

            steps.push({
                element: columnDefs[4],
                intro: translationService.translate('UPC.Help.List.Columns.Height')
            });

            steps.push({
                element: columnDefs[5],
                intro: translationService.translate('UPC.Help.List.Columns.Weight')
            });

            steps.push({
                element: columnDefs[6],
                intro: translationService.translate('UPC.Help.List.Columns.DesignId')
            });

            steps.push({
                element: columnDefs[7],
                intro: translationService.translate('UPC.Help.List.Columns.CreatedDate')
            });

            steps.push({
                element: columnDefs[8],
                intro: translationService.translate('UPC.Help.List.Columns.LastUpdatedDate')
            });

            steps.push({
                element: "#importHeadersInFileSelectionBox",
                intro: translationService.translate('Common.Import.Help.HeadersInFile')
            });

            if ($scope.useHeadersInFile == false) {
                steps.push({
                    element: "#headerFields",
                    intro: translationService.translate('Common.Import.Help.HeadersInputField')
                });
            }

            steps.push({
                element: "#UPCImportDelimiter",
                intro: translationService.translate('Common.Import.Help.Delimiter')
            });

            steps.push({
                element: "#UPCImportCommentPrefix",
                intro: translationService.translate('Common.Import.Help.CommentPrefix')
            });

            steps.push({
                element: "#UPCImportBrowseFile",
                intro: translationService.translate('Common.Import.Help.BrowseFile')
            });

            steps.push({
                element: "#UPCImportProcessFile",
                intro: translationService.translate('UPC.Import.Help.ProcessFile')
            });

            steps.push({
                element: "#UPCImportFinalizeImport",
                intro: translationService.translate('UPC.Import.Help.FinalizeImport')
            });

            steps.push({
                element: "#UPCImportCancelImport",
                intro: translationService.translate('Common.Import.Help.Cancel')
            });

            return steps;
        };

        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
        //End Intro JS Stuff
    }

    SetupIntroJs();
    $scope.$watch('useHeadersInFile', function (newValue, oldValue) {
        SetupIntroJs();
    });

    messagingService.publish("GetCurrentSldConfiguration");
}