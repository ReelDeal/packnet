function UPCCtrl($q, $scope, $state, $timeout, alertService, introJsService, machineGroupService, messagingService, packagingDesignService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope, 'UPC Control');
    var awaitingUpdate = false;
    $scope.isScanningMode = false;
    $scope.UPCs = [];
    $scope.searchUPC = {};
    $scope.currentMachineGroup = machineGroupService.getCurrentMachineGroup();
    $scope.labelMachineGroup = translationService.translate('MachineGroup.Label.NameSingular');
    $scope.labelStatus = translationService.translate('Common.Label.Status');
    $scope.designList = packagingDesignService.getAllDesigns(); // this should be a Id/Name pair
    packagingDesignService.allDesignsObservable.autoSubscribe(function (designs) {
        $timeout(function () {

            
            delete $scope.designList;
            $scope.designList = designs;
        }, 0, true);
    }, $scope, "all designs");

    var UPCLocatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("UPCLocated"))
        .select(messagingService.responseDataFunction);

    var UPCsProcessedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("UPCsProcessed"))
        .select(messagingService.responseDataFunction);

    var addOrUpdateQty = function (item, quantity) {
        var existingItem = Enumerable.From($scope.UPCs).FirstOrDefault(null, function (existing) {
            return existing.UPC === item.UPC && existing.DesignId === item.DesignId;
        });

        if (existingItem) {
            existingItem.Quantity += quantity;
        }
        else {
            if (!item.Quantity) {
                item.Quantity = quantity;
            }
            $scope.UPCs.push(item);
        }
    };

    UPCLocatedObservable.autoSubscribe(function (message) {
        if (message.Result === "Fail") {
            $state.go('^.new', {
                selectedUPC: {
                    UPC: $scope.searchUPC.UPC,
                    DesignId: $scope.searchUPC.defaultDesignId,
                    IsNewUPC: true,
                    Quantity: $scope.searchUPC.Quantity
                }
            },
            {
                reload: false
            });
        }
        else if (message.Result === "Success") {
            alertService.addSuccess(translationService.translate('SLD.Message.Located', message.Data.UPC), 5000);

            if ($scope.searchUPC.defaultDesignId && $scope.searchUPC.defaultDesignId != 0) {
                message.Data.DesignId = $scope.searchUPC.defaultDesignId;
            }

            addOrUpdateQty(message.Data, $scope.searchUPC.Quantity);
            $scope.isScanningMode = false;
        }

        
        delete $scope.searchUPC.UPC;
        $scope.searchUPC.UPC = "";
    }, $scope, 'UPC Located');

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'UPCs',
        columnDefs: [
            {
                field: 'UPC',
                displayName: translationService.translate('UPC.Label.NameSingular'),
                width: 150,
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                field: 'Description',
                displayName: translationService.translate('Common.Label.Description')
            },
            {
                field: 'Length',
                displayName: translationService.translate('Common.Label.Length')
            },
            {
                field: 'Width',
                displayName: translationService.translate('Common.Label.Width')
            },
            {
                field: 'Height',
                displayName: translationService.translate('Common.Label.Height')
            },
            {
                field: 'Quantity',
                displayName: translationService.translate('Common.Label.Quantity')
            },
            {
                field: 'DesignId',
                displayName: translationService.translate('Common.Label.DesignId')
            },
            {
                field: 'remove',
                displayName: '',
                cellTemplate: "partials/SLD/UPCRemoveEditTemplate.html",
                cellClass: 'ui-grid-cell-contents-no-padding'
            }
        ],
        rowHeight: 37,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;
        }
    };

    $scope.removeRow = function (row) {
        var index = $scope.UPCs.indexOf(row.entity);
        $scope.UPCs.splice(index, 1);
    };

    $scope.editRow = function (row) {
        var updatePromise = $q.defer();

        updatePromise.promise.then(function (updatedRow) {
            row.entity.Description = updatedRow.Description;
            row.entity.Length = updatedRow.Length;
            row.entity.Height = updatedRow.Height;
            row.entity.Width = updatedRow.Width;
            row.entity.Weight = updatedRow.Weight;
            row.entity.DesignId = updatedRow.DesignId;
            row.entity.Quantity = updatedRow.Quantity;
        });

        row.entity.IsNewUPC = false;
        $state.go('^.new', {
            selectedUPC: row.entity,
            rowEditPromise: updatePromise,
            editingScannedRow: true
        },
        { reload: false });
    };

    $scope.goToManagementPage = function () {
        $state.go('^.management', { fromOperatorPanel: true }, { reload: false });
    };

    $scope.$watch('searchUPC.UPC', function (tmpStr) {
        if (!tmpStr || tmpStr.length == 0) {
            return;
        }

        $timeout(function () {
            // if searchStr is still the same after timeout...
            // go ahead perform the action
            if (tmpStr === $scope.searchUPC.UPC) {
                if ($scope.searchUPC.UPC.toLowerCase() == "send") {
                    $scope.send();
                }
                else {
                    $scope.isScanningMode = true;
                    messagingService.publish("LocateUPC", $scope.searchUPC.UPC);
                }
            }
        }, 1000, true);
    });

    UPCsProcessedObservable.autoSubscribe(function (message) {
        if (message.Result == 'Success') {
            awaitingUpdate = false;
            $scope.UPCs.length = 0;
            $scope.isScanningMode = false;
            
            delete $scope.searchUPC.UPC;
            $scope.searchUPC.UPC = "";
            alertService.addSuccess(translationService.translate('SLD.Message.ImportSuccess'), 5000);
        }
        else {
            alertService.addError(translationService.translate('SLD.Message.ImportFailed'), 5000);
        }
    }, $scope, "upcs processed");

    $scope.send = function () {
        messagingService.publish("ProcessUPCs", $scope.UPCs);

        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('SLD.Message.ProcessFailed'), 5000);
                awaitingUpdate = false;
            }
        }, 5000, true);
    };

    $scope.tellMeAboutThisPage = function () {
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        steps.push({
            element: "#notFound",
            intro: translationService.translate('SLD.Help.Overview')
        });

        steps.push({
            element: "#UPCInput",
            intro: translationService.translate('SLD.Help.UPC')
        });

        steps.push({
            element: "#SendUPCButton",
            intro: translationService.translate('SLD.Help.Send')
        });

        steps.push({
            element: "#DesignDropDown",
            intro: translationService.translate('SLD.Help.Design')
        });

        steps.push({
            element: "#UPCQuantity",
            intro: translationService.translate('SLD.Help.Quantity')
        });

        steps.push({
            element: "#ScannedUPCsGrid",
            intro: translationService.translate('SLD.Help.Columns.Overview')
        });

        steps.push({
            element: columnDefs[0],
            intro: translationService.translate('UPC.Help.List.Columns.UPC')
        });

        steps.push({
            element: columnDefs[1],
            intro: translationService.translate('UPC.Help.List.Columns.Description')
        });

        steps.push({
            element: columnDefs[2],
            intro: translationService.translate('UPC.Help.List.Columns.Length')
        });

        steps.push({
            element: columnDefs[3],
            intro: translationService.translate('UPC.Help.List.Columns.Width')
        });

        steps.push({
            element: columnDefs[4],
            intro: translationService.translate('UPC.Help.List.Columns.Height')
        });

        steps.push({
            element: columnDefs[5],
            intro: translationService.translate('SLD.Help.Columns.Quantity')
        });

        steps.push({
            element: columnDefs[6],
            intro: translationService.translate('UPC.Help.List.Columns.DesignId')
        });

        steps.push({
            element: "#ManageUPCsButton",
            intro: translationService.translate('SLD.Help.ManageUPCs')
        });

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};