function SLDSettingsCtrl($rootScope, $scope, messagingService, translationService, NOTIFICATIONS_EVENTS) {
    
    $scope.notEmptyPattern = /^.+$/;
    $scope.configurationOptions = {
        ConflictResolutionMethod: "",
        ImportWorkflow: {},
        UPCImportWorkflow: {},
        UPCExportWorkflow: {},
        ImportFileDelimeter: "",
        ImportFileCommentPrefix: ""
    };

    $scope.isValid = function (form) {
        console.log(form);
    }

    $rootScope.$on(NOTIFICATIONS_EVENTS.settingsSaved, function () {
        console.log('Save called in SLDSettingsCtrl');

        if (!$scope.configurationOptions.ImportWorkflow.WorkflowPath ||
            !$scope.configurationOptions.UPCImportWorkflow.WorkflowPath ||
            !$scope.configurationOptions.UPCExportWorkflow.WorkflowPath ||
            $scope.configurationOptions.ConflictResolutionMethod == "" ||
            $scope.configurationOptions.ImportFileDelimeter == "" ||
            $scope.configurationOptions.ImportFileCommentPrefix == "") {
            console.log("Unable to save SLD settings since the provided data is invalid");
            return;
        }

        var settings = {
            ImportWorkflowPath: $scope.configurationOptions.ImportWorkflow.WorkflowPath,
            UPCImportWorkflowPath: $scope.configurationOptions.UPCImportWorkflow.WorkflowPath,
            UPCExportWorkflowPath: $scope.configurationOptions.UPCExportWorkflow.WorkflowPath,
            ConflictResolutionMethod: $scope.configurationOptions.ConflictResolutionMethod,
            ImportFileDelimeter: $scope.configurationOptions.ImportFileDelimeter,
            ImportFileCommentPrefix: $scope.configurationOptions.ImportFileCommentPrefix
        };

        messagingService.publish("SaveSLDConfiguration", settings);
    });

    messagingService.messageSubject
        .where(messagingService.filterObs("GetWorkflowsResponse"))
        .select(messagingService.responseDataFunction)
        .autoSubscribe(function (msg) {
            if (msg.Result == 'Success') {
                
                delete $scope.workflows;
                $scope.workflows = msg.Data;

                //We don't want to retreive the current config before the workflows have been populated
                messagingService.publish("GetCurrentSldConfiguration");
            }
        }, $scope, "SLD Workflow Subsriber");

    messagingService.messageSubject
       .where(messagingService.filterObs("SaveSLDConfigurationResponse"))
       .select(messagingService.responseDataFunction)
       .autoSubscribe(function (msg) {
           if (msg.Result == 'Success') {
               console.log("Succesfully updated SLD configuration");
           }
           else {
               console.log("Failed when updating SLD configuration");
           }
       }, $scope, "SLD Configuration update subscriber");


    messagingService.messageSubject
    .where(messagingService.filterObs("GetCurrentSldConfigurationResponse"))
    .select(messagingService.responseDataFunction)
    .autoSubscribe(function (msg) {
        if (msg.Result == 'Success') {
            var configuration = msg.Data;

            
            delete $scope.configurationOptions.ConflictResolutionMethod;
            $scope.configurationOptions.ConflictResolutionMethod = configuration.ConflictResolutionMethod;
            
            delete $scope.configurationOptions.ImportFileCommentPrefix;
            $scope.configurationOptions.ImportFileCommentPrefix = configuration.ImportFileCommentPrefix;
            
            delete $scope.configurationOptions.ImportFileDelimeter;
            $scope.configurationOptions.ImportFileDelimeter = configuration.ImportFileDelimeter;

            
            delete $scope.configurationOptions.ImportWorkflow;
            $scope.configurationOptions.ImportWorkflow = Enumerable.From($scope.workflows.ImportWorkflows).FirstOrDefault({}, function (entity) {
                return entity.WorkflowPath == configuration.ImportWorkflowPath;
            });
            
            delete $scope.configurationOptions.UPCImportWorkflow;
            $scope.configurationOptions.UPCImportWorkflow = Enumerable.From($scope.workflows.UPCImportWorkflows).FirstOrDefault({}, function (entity) {
                return entity.WorkflowPath == configuration.UPCImportWorkflowPath;
            });
            
            delete $scope.configurationOptions.UPCExportWorkflow;
            $scope.configurationOptions.UPCExportWorkflow = Enumerable.From($scope.workflows.UPCExportWorkflows).FirstOrDefault({}, function (entity) {
                return entity.WorkflowPath == configuration.UPCExportWorkflowPath;
            });
        }
    }, $scope, "SLD Configuration subscriber");

    messagingService.publish("GetWorkflows");
};


app.directive('sldSettings', function () {
    return {
        controller: "SLDSettingsCtrl",
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/SLD/SLDSettings.html'
    };
});