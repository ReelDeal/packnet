function UPCNewEditCtrl($scope, $state, $stateParams, $timeout, alertService, introJsService, messagingService, stateChangeService, translationService) {
    $timeout = $timeout.getInstance($scope, 'UPCNewEditCtrl');
    var awaitingUpdate = false;

    $scope.editingScannedRow = $stateParams.editingScannedRow;
    $scope.floatingPointRegex = /^(\d+\.?\d*|\.\d+)$/;
    $scope.intRegex = /^\d+$/;
    $scope.selectedUPC = {};
    $scope.selectedUPC.IsNewUPC = true;

    if ($stateParams.selectedUPC) {
        
        delete $scope.selectedUPC;
        $scope.selectedUPC = $stateParams.selectedUPC;
    }

    var UPCCreatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("UPCCreated"))
        .select(messagingService.responseDataFunction);

    var UPCUpdatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("UPCUpdated"))
        .select(messagingService.responseDataFunction);

    UPCCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('UPC.Label.NameSingular'), $scope.UPCBeingSaved.UPC));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('UPC.Label.NameSingular'), $scope.UPCBeingSaved.UPC));
        }
        else if (msg.Result === "Success") {
            $state.go(stateChangeService.previousState(), null, { reload: false });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('UPC.Label.NameSingular'), $scope.UPCBeingSaved.UPC), 5000);

            if ($scope.isScanningMode) { //add the new item if the user created it on the fly
                msg.Data.Quantity = $scope.searchUPC.Quantity;
                addOrUpdateQty(msg.Data);
            }
        }
    }, $scope, 'UPC Created');

    UPCUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('UPC.Label.NameSingular'), $scope.UPCBeingSaved.UPC));
        }
        else if (msg.Result === "Success") {
            $state.go(stateChangeService.previousState(), null, { reload: false });
            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('UPC.Label.NameSingular'), $scope.UPCBeingSaved.UPC), 5000);
        }
    }, $scope, 'UPC Updated');

    var addOrUpdateQty = function (item) {
        var existingItem = Enumerable.From($scope.UPCs).FirstOrDefault(null, function (existing) {
            return existing.UPC === item.UPC && existing.DesignId === item.DesignId;
        });
        if (existingItem) {
            existingItem.Quantity += item.Quantity;
        }
        else {
            if (!item.Quantity) {
                item.Quantity = 1;
            }
            $scope.UPCs.push(item);
        }
    };

    function updateUpc(form) {
        awaitingUpdate = true;

        var action = "CreateUPC";
        if (!$scope.UPCBeingSaved.IsNewUPC) {
            action = "UpdateUPC";
        }

        messagingService.publish(action, $scope.UPCBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('UPC.Label.NameSingular'), $scope.UPCBeingSaved.UPC));
                awaitingUpdate = false;
                
                delete $scope.UPCBeingSaved;
                $scope.UPCBeingSaved = {};
            }
        }, 5000);
    }

    function updateScannedRow(){
        
        delete $scope.UPCBeingSaved.Quantity;
        $scope.UPCBeingSaved.Quantity = $scope.selectedUPC.Quantity;
        $stateParams.rowEditPromise.resolve($scope.UPCBeingSaved);
        $state.go(stateChangeService.previousState(), { updatedUpcRow: $scope.UPCBeingSaved }, { reload: false });
    }

    $scope.saveUPC = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }
        
        alertService.clearAll();

        
        delete $scope.UPCBeingSaved;
        $scope.UPCBeingSaved = {
            Id: $scope.selectedUPC.Id,
            UPC: $scope.selectedUPC.UPC,
            Description: $scope.selectedUPC.Description,
            Length: $scope.selectedUPC.Length,
            Width: $scope.selectedUPC.Width,
            Height: $scope.selectedUPC.Height,
            Weight: $scope.selectedUPC.Weight,
            DesignId: $scope.selectedUPC.DesignId,
            IsNewUPC: $scope.selectedUPC.IsNewUPC
        };

        if ($scope.editingScannedRow) {
            updateScannedRow();
        }
        else {
            updateUpc(form);
        }
    }

    $scope.$watch('selectedUPC.Parsed', function (tmpStr) {
        if (!tmpStr || tmpStr.length == 0) return;
        $timeout(function () {

            // if searchStr is still the same after timeout...
            // go ahead perform the action
            if (tmpStr === $scope.selectedUPC.Parsed) {
                var parts = $scope.selectedUPC.Parsed.split(" ");
                if (!parts || parts.length != 3) {
                    alertService.addError(translationService.translate('UPC.Message.ParseFailed', $scope.selectedUPC.Parsed), 5000);
                    
                    delete $scope.selectedUPC.Parsed;
                    $scope.selectedUPC.Parsed = "";
                    return false;
                }

                var length = parseFloat(parts[0]);
                if (isNaN(length)) {
                    alertService.addError(translationService.translate('UPC.Message.ParseFailedLength', $scope.selectedUPC.Parsed), 5000);
                    
                    delete $scope.selectedUPC.Parsed;
                    $scope.selectedUPC.Parsed = "";
                    return false;
                }
                var width = parseFloat(parts[1]);
                if (isNaN(width)) {
                    alertService.addError(translationService.translate('UPC.Message.ParseFailedWidth', $scope.selectedUPC.Parsed), 5000);
                    
                    delete $scope.selectedUPC.Parsed;
                    $scope.selectedUPC.Parsed = "";
                    return false;
                }

                var height = parseFloat(parts[2]);
                if (isNaN(height)) {
                    alertService.addError(translationService.translate('UPC.Message.ParseFailedHeight', $scope.selectedUPC.Parsed), 5000);
                    
                    delete $scope.selectedUPC.Parsed;
                    $scope.selectedUPC.Parsed = "";
                    return false;
                }

                
                delete $scope.selectedUPC.Length;
                $scope.selectedUPC.Length = length;
                
                delete $scope.selectedUPC.Width;
                $scope.selectedUPC.Width = width;
                
                delete $scope.selectedUPC.Height;
                $scope.selectedUPC.Height = height;
                alertService.addSuccess(translationService.translate('UPC.Message.ParseSuccess', length, width, height), 5000);
                
                delete $scope.selectedUPC.Parsed;
                $scope.selectedUPC.Parsed = "";
            }
        }, 1000, true);
    }, true);

    $scope.cancelNewEditUPC = function () {
        $state.go(stateChangeService.previousState(), null, { reload: false });
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        if ($scope.editingScannedRow) {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('UPC.Help.Form.Overview.EditScannedRow')
            });
        }
        else {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('UPC.Help.Form.Overview.CreateEdit')
            });
        }
        
        steps.push({
            element: "#UPCIdentifierInput",
            intro: translationService.translate('UPC.Help.Form.UPC')
        });

        steps.push({
            element: "#UPCDescriptionInput",
            intro: translationService.translate('UPC.Help.Form.Description')
        });

        steps.push({
            element: "#ParseInput",
            intro: translationService.translate('UPC.Help.Form.Parse')
        });

        steps.push({
            element: "#UPCLengthInput",
            intro: translationService.translate('UPC.Help.Form.Length')
        });

        steps.push({
            element: "#UPCWidthInput",
            intro: translationService.translate('UPC.Help.Form.Width')
        });

        steps.push({
            element: "#UPCHeightInput",
            intro: translationService.translate('UPC.Help.Form.Height')
        });

        steps.push({
            element: "#UPCWeightInput",
            intro: translationService.translate('UPC.Help.Form.Weight')
        });

        steps.push({
            element: "#UPCDesignDropDown",
            intro: translationService.translate('UPC.Help.Form.Design')
        });

        if ($scope.editingScannedRow) {
            steps.push({
                element: "#UPCQuantityInput",
                intro: translationService.translate('UPC.Help.Form.Quantity')
            });

            steps.push({
                element: "#NewUPCSaveButton",
                intro: translationService.translate('UPC.Help.Form.Edit')
            });
        }
        else {
            steps.push({
                element: "#NewUPCSaveButton",
                intro: translationService.translate('UPC.Help.Form.Save')
            });
        }

        steps.push({
            element: "#NewUPCCancelButton",
            intro: translationService.translate('UPC.Help.Form.Cancel')
        });

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
}
