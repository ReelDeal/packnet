﻿function csvService() {

    function saveCsVtoFile(CSVData, fileName) {

        var date = new Date();
        var name = fileName + "_" + date.getFullYear() + date.getMonth() + date.getDay() + date.getHours() + date.getSeconds() + date.getMilliseconds();
        //var uri = 'data:text/csv;charset=utf-8,' + escape(CSV.join(""));

        var file = new Blob([CSVData], { encoding: "UTF-8", type: 'text/csv;charset=UTF-8' });
        var url = window.URL.createObjectURL(file);

        var link = document.createElement("a");
        link.href = url;

        link.style = "visibility:hidden";
        link.download = name + ".csv";

        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };


    return { saveCsVtoFile: saveCsVtoFile };
}
app.factory('csvService', csvService);
