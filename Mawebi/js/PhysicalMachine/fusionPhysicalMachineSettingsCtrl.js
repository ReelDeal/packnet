﻿(function () {
    'use strict';

    app.controller('FusionPhysicalMachineSettingsCtrl', FusionPhysicalMachineSettingsCtrl);

    FusionPhysicalMachineSettingsCtrl.$inject = ['$scope', '$state', '$timeout', 'alertService', 'messagingService', 'physicalMachineSettingsService', 'stateChangeService', 'translationService'];

    function FusionPhysicalMachineSettingsCtrl($scope, $state, $timeout, alertService, messagingService, physicalMachineSettingsService, stateChangeService, translationService) {
        var awaitingUpdate = false;

        $scope.cancel = cancel;
        $scope.fusionPhysicalMachineSettings = null;
        $scope.fusionPhysicalMachineSettingsFileName = '';
        $scope.save = save;

        // Listeners
        physicalMachineSettingsService.physicalMachineSettingsObservable.autoSubscribe(function (msg) {
            $timeout(function () {
                $scope.fusionPhysicalMachineSettings = msg.Data;

                //Converts the Type to string so that the dropdown selection works
                angular.forEach($scope.fusionPhysicalMachineSettings.LongHeadParameters.LongHeads, function (data, index) {
                    data.Type = String(data.Type);
                });
            }, 0, true);
        }, $scope, 'PhysicalMachineSettings');

        physicalMachineSettingsService.physicalMachineSettingsUpdatedObservable.autoSubscribe(function (msg) {
            awaitingUpdate = false;

            if (msg.Result === "Fail") {
                alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Machine.PhysicalMachineSetting.Label.NameSingular'), $scope.fusionPhysicalMachineSettingsFileName));
            }
            else {
                cancel();

                alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Machine.PhysicalMachineSetting.Label.NameSingular'), $scope.fusionPhysicalMachineSettingsFileName), 5000);
            }
        }, $scope, 'PhysicalMachineSettingsUpdated');

        // Functions
        function cancel() {
            $state.go(stateChangeService.previousState(), null, { reload: false });
        }

        function init() {
            var pathParts = $scope.selectedMachine.Data.PhysicalSettingsFilePath.split('\\');

            $scope.fusionPhysicalMachineSettingsFileName = pathParts[pathParts.length - 1];

            requestData();
        }

        function requestData() {
            messagingService.publish("GetFusionPhysicalMachineSettings", $scope.selectedMachine.Data.PhysicalSettingsFilePath);
        }

        function save(form) {
            $scope.$broadcast('show-errors-check-validity');

            if (!form.$valid) {
                return;
            }

            // Exclude EventNotifierIp (there's no json serializer and is not needed)
            $scope.fusionPhysicalMachineSettings.EventNotifierIp = null;

            messagingService.publish("UpdateFusionPhysicalMachineSettings", $scope.fusionPhysicalMachineSettings);

            $timeout(function () {
                if (awaitingUpdate) {
                    alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.PhysicalMachineSetting.Label.NameSingular'), $scope.fusionPhysicalMachineSettingsFileName));
                    awaitingUpdate = false;
                }
            }, 5000);
        }

        // Initialize
        init();
    }
})();