﻿(function () {
    'use strict';

    app.factory('physicalMachineSettingsService', physicalMachineSettingsService);

    physicalMachineSettingsService.$inject = ['messagingService'];

    function physicalMachineSettingsService(messagingService) {
        var physicalMachineSettingsObservable = messagingService.messageSubject
            .where(messagingService.filterObs("PhysicalMachineSettings"));

        var physicalMachineSettingsUpdatedObservable = messagingService.messageSubject
            .where(messagingService.filterObs("PhysicalMachineSettingsUpdated"));

        var service = {
            physicalMachineSettingsObservable: physicalMachineSettingsObservable,
            physicalMachineSettingsUpdatedObservable: physicalMachineSettingsUpdatedObservable
        }

        return service;
    }
})();