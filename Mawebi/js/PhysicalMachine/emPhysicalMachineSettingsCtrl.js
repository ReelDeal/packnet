﻿(function () {
    'use strict';

    app.controller('EmPhysicalMachineSettingsCtrl', EmPhysicalMachineSettingsCtrl);

    EmPhysicalMachineSettingsCtrl.$inject = ['$scope', '$state', '$timeout', 'alertService', 'messagingService', 'physicalMachineSettingsService', 'stateChangeService', 'translationService'];

    function EmPhysicalMachineSettingsCtrl($scope, $state, $timeout, alertService, messagingService, physicalMachineSettingsService, stateChangeService, translationService) {
        var awaitingUpdate = false;

        $scope.cancel = cancel;
        $scope.emPhysicalMachineSettings = null;
        $scope.emPhysicalMachineSettingsFileName = '';
        $scope.save = save;

        // Listeners
        physicalMachineSettingsService.physicalMachineSettingsObservable.autoSubscribe(function (msg) {
            $timeout(function () {
                $scope.emPhysicalMachineSettings = msg.Data;
            }, 0, true);
        }, $scope, 'PhysicalMachineSettings');

        physicalMachineSettingsService.physicalMachineSettingsUpdatedObservable.autoSubscribe(function (msg) {
            awaitingUpdate = false;

            if (msg.Result === "Fail") {
                alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Machine.PhysicalMachineSetting.Label.NameSingular'), $scope.emPhysicalMachineSettingsFileName));
            }
            else {
                cancel();

                alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Machine.PhysicalMachineSetting.Label.NameSingular'), $scope.emPhysicalMachineSettingsFileName), 5000);
            }
        }, $scope, 'PhysicalMachineSettingsUpdated');

        // Functions
        function cancel() {
            $state.go(stateChangeService.previousState(), null, { reload: false });
        }

        function init(){
            var pathParts = $scope.selectedMachine.Data.PhysicalSettingsFilePath.split('\\');

            $scope.emPhysicalMachineSettingsFileName = pathParts[pathParts.length - 1];

            requestData();
        }

        function requestData(){
            messagingService.publish("GetEmPhysicalMachineSettings", $scope.selectedMachine.Data.PhysicalSettingsFilePath);
        }

        function save(form) {
            $scope.$broadcast('show-errors-check-validity');

            if (!form.$valid) {
                return;
            }
            
            // Exclude EventNotifierIp (there's no json serializer and is not needed)
            $scope.emPhysicalMachineSettings.EventNotifierIp = null;

            messagingService.publish("UpdateEmPhysicalMachineSettings", $scope.emPhysicalMachineSettings);

            $timeout(function () {
                if (awaitingUpdate) {
                    alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Machine.PhysicalMachineSetting.Label.NameSingular'), $scope.emPhysicalMachineSettingsFileName));
                    awaitingUpdate = false;
                }
            }, 5000);
        }

        // Initialize
        init();
    }
})();