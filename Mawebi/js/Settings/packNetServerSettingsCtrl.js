app.controller('PackNetServerSettingsCtrl', ['$rootScope', '$scope', '$state', '$timeout', 'alertService', 'introJsService', 'messagingService', 'NOTIFICATIONS_EVENTS', 'translationService',
    function ($rootScope, $scope, $state, $timeout, alertService, introJsService, messagingService, NOTIFICATIONS_EVENTS, translationService) {
        $timeout = $timeout.getInstance($scope);
        // Locals
        var awaitingResponse = false;

        var requestData = function () {
            messagingService.publish('GetSettings');
        }

        var serverSettingsUpdatedObservable = messagingService.messageSubject
            .where(messagingService.filterObs("ServerSettingsUpdated"));

        var serverSettingsObservable = messagingService.messageSubject
            .where(messagingService.filterObs("ServerSettings"));

        // Bindings
        $scope.data = null;
        $scope.deleteImportedFiles = false;
        $scope.headerFields = [];
        $scope.tabs = {};
        $scope.tabs.generalTabActive = true;
        $scope.tabs.fileImportTabActive = false;

        // TODO: These values could be inside a Service or something similar, more globally
        
        $scope.importTypes = [
            {
                label: translationService.translate('SelectionAlgorithm.Label.BoxFirst'),
                value: 'BoxFirst'
            },
            {
                label: translationService.translate('SelectionAlgorithm.Label.BoxLast'),
                value: 'BoxLast'
            },
            {
                label: translationService.translate('SelectionAlgorithm.Label.Order'),
                value: 'Order'
            }
        ];

        $scope.cancel = function () {
            $state.go('Dashboard');
        }

        $scope.save = function (form) {
            $scope.$broadcast('show-errors-check-validity');

            if (!form.$valid) {
                alertService.addError(translationService.translate('Setting.Message.InvalidOrMissingInformation'), 5000);
                return;
            }

            alertService.clearAll();

            awaitingResponse = true;

            //TODO: Find workaround to avoid this... The angular ng-true-value and ng-false-value directives wasn't working well.
            $scope.data.FileImportSettings.DeleteImportedFiles = $scope.deleteImportedFiles ? 1 : 0;

            //Convert the array of objects of the tags-input to a simple array
            
            delete $scope.data.FileImportSettings.HeaderFields;
            $scope.data.FileImportSettings.HeaderFields = $.map($scope.headerFields, function (val, i) {
                return val.text;
            });

            messagingService.publish('UpdateSettings', $scope.data);

            //Broadcast so plugin settings controllers can send their save message
            $rootScope.$broadcast(NOTIFICATIONS_EVENTS.settingsSaved);
            /*
            $timeout(function () {
                if (awaitingResponse) {
                    alertService.addError('Save failed, please try again.');
                    awaitingResponse = false;
                }
            }, 5000);
            */
        }

        // Listeners
        serverSettingsObservable.autoSubscribe(function (msg) {
            $timeout(function () {
                
                delete $scope.data;
                $scope.data = msg.Data;

                if ($scope.data) {
                    //TODO: Find workaround to avoid this... The angular ng-true-value and ng-false-value directives wasn't working well.
                    //When the database returned a 1, the checkbox was not rendered checked
                    $scope.deleteImportedFiles = $scope.data.FileImportSettings.DeleteImportedFiles === 1;

                    //Convert the simple array to an array of objects, this so it can be used with the tags-input
                    
                    delete $scope.headerFields;
                    $scope.headerFields = $.map($scope.data.FileImportSettings.HeaderFields, function (val, i) {
                        return { text: val };
                    });
                }
            }, 0, true);
        }, $scope, 'Server Settings');

        serverSettingsUpdatedObservable.autoSubscribe(function (msg) {
            if (msg.Result === 'Fail') {
                alertService.addError(translationService.translate('Setting.Message.UpdateFailed'));
            }
            else if (msg.Result === 'Success') {
                alertService.addSuccess(translationService.translate('Setting.Message.UpdateSuccess'), 5000);
            }
        }, $scope, 'Server Settings Updated');

        // Initialize
        $scope.utilities.initialDataRequest($scope, messagingService, requestData);

        $scope.tellMeAboutThisPage = function () {
            var steps = [];
            var tables = document.querySelectorAll("#nudHorizontalTable");

            if ($scope.tabs.generalTabActive === true) {

                steps.push({
                    element: "#notFound",
                    intro: translationService.translate('Setting.Help.Form.General.Overview')
                });

                steps.push({
                    element: "#generalTab",
                    intro: translationService.translate('Setting.Help.Form.General.Tab')
                });

                steps.push({
                    element: "#fileImportTab",
                    intro: translationService.translate('Setting.Help.Form.FileImport.Tab')
                });

                if ($rootScope.SLDServiceAvaliable) {
                    steps.push({
                        element: "#sldSettingsTab",
                        intro: translationService.translate('Setting.Help.Form.SLD.Tab')
                    });
                }

                if ($rootScope.ArticleServiceAvaliable) {
                    steps.push({
                        element : "#articleImportSettingsTab",
                        intro : translationService.translate('Setting.Help.Form.Article.Tab')
                    });
                }

                steps.push({
                    element: "#callbackIPAddress",
                    intro: translationService.translate('Setting.Help.Form.General.CallbackIpOrDnsName')
                });

                steps.push({
                    element: tables[0],
                    intro: translationService.translate('Setting.Help.Form.General.CallbackPort')
                });

                steps.push({
                    element: "#workflowTrackingLevel",
                    intro: translationService.translate('Setting.Help.Form.General.WorkflowTrackingLevel')
                });
            }
            else if ($scope.tabs.fileImportTabActive === true) {
                steps.push({
                    element: "#defaultImportType",
                    intro: translationService.translate('Setting.Help.Form.FileImport.DefaultImportType')
                });

                steps.push({
                    element: "#monitoredFolderPath",
                    intro: translationService.translate('Setting.Help.Form.FileImport.MonitoredFolderPath')
                });

                steps.push({
                    element: "#fileExtension",
                    intro: translationService.translate('Setting.Help.Form.FileImport.FileExtension')
                });

                steps.push({
                    element: tables[1],
                    intro: translationService.translate('Setting.Help.Form.FileImport.TimesToRetryAccessFilePath')
                });

                steps.push({
                    element: tables[2],
                    intro: translationService.translate('Setting.Help.Form.FileImport.SecondsToRetryToAccessMonitoredFilePath')
                });

                steps.push({
                    element: tables[3],
                    intro: translationService.translate('Setting.Help.Form.FileImport.TimesToRetryLockedFile')
                });

                steps.push({
                    element: tables[4],
                    intro: translationService.translate('Setting.Help.Form.FileImport.SecondsToRetryLockedFile')
                });

                steps.push({
                    element: "#fieldDelimiter",
                    intro: translationService.translate('Setting.Help.Form.FileImport.DefaultDelimiter')
                });

                steps.push({
                    element: "#commentIndicator",
                    intro: translationService.translate('Setting.Help.Form.FileImport.DefaultCommentIndicator')
                });

                steps.push({
                    element: "#headerFields > div > div",
                    intro: translationService.translate('Setting.Help.Form.FileImport.HeaderFields')
                });

                steps.push({
                    element: "#deleteImportedFiles",
                    intro: translationService.translate('Setting.Help.Form.FileImport.DeleteImportedFiles')
                });
            }
            else if ($scope.tabs.sldSettingsTabActive === true) {
                steps.push({
                    element: "#importWorkflowDropdown",
                    intro: translationService.translate('Setting.Help.Form.SLD.ImportWorkflow')
                });

                steps.push({
                    element: "#upcImportWorkflowDropdown",
                    intro: translationService.translate('Setting.Help.Form.SLD.UPCImportWorkflow')
                });

                steps.push({
                    element: "#upcExportWorkflowDropdown",
                    intro: translationService.translate('Setting.Help.Form.SLD.UPCExportWorkflow')
                });

                steps.push({
                    element: "#conflictResolutionMethodDropDown",
                    intro: translationService.translate('Setting.Help.Form.SLD.ConflictResolutionMethod')
                });

                steps.push({
                    element: "#defaultDelimiterInput",
                    intro: translationService.translate('Setting.Help.Form.SLD.DefaultDelimiter')
                });

                steps.push({
                    element: "#defaultCommentPrefixInput",
                    intro: translationService.translate('Setting.Help.Form.SLD.DefaultCommentIndicator')
                });
            }
            else if ($scope.tabs.articleImportSettingsTabActive === true) {
                steps.push({
                    element: "#defaultArticleImportDelimiterInput",
                    intro: translationService.translate('Setting.Help.Form.Article.DefaultDelimiter')
                });

                steps.push({
                    element: "#defaultArticleImportCommentPrefixInput",
                    intro: translationService.translate('Setting.Help.Form.Article.DefaultCommentIndicator')
                });
            }

            steps.push({
                element: "#packNetServerSettingsSaveButton",
                intro: translationService.translate('Setting.Help.Form.Save')
            });

            steps.push({
                element: "#packNetServerSettingsCancelButton",
                intro: translationService.translate('Setting.Help.Form.Cancel')
            });

            return steps;
        };

        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
    }]);