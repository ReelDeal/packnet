app.controller("articleNewCtrl", ["$modal", "$scope", "$state", "$timeout", "alertService", "articleStateService", "csvService", "designPreviewService", "helperService", "introJsService", "messagingService", "packagingDesignService", "uiGridConstants", "dateValidationService", "translationService",
    function ($modal, $scope, $state, $timeout, alertService, articleStateService, csvService, designPreviewService, helperService, introJsService, messagingService, packagingDesignService, uiGridConstants, dateValidationService, translationService) {
        $timeout = $timeout.getInstance($scope);

        //Locals
        var awaitingResponse = false;
        var editingKitItem = false;
        var editingKitItemIndex = null;
        var expectedDeleteResponseCount = 0;
        var deleteResponseCount = 0;

        var templateRestrictionType = "PackNet.Common.Interfaces.RestrictionsAndCapabilities.BasicRestriction`1[[PackNet.Common.Interfaces.DTO.Template, PackNet.Common.Interfaces]], PackNet.Common.Interfaces";

        var templatesObservable = messagingService.messageSubject
            .where(messagingService.filterObs("Templates"));

        var loadingMessage = translationService.translate("Common.Message.Loading");

        var requestData = function () {
            if ($scope.articlesGridOptions != undefined && $scope.articlesGridOptions.data != undefined) {
                alertService.addInfo(loadingMessage, 3000);
            }

            messagingService.publish("ArticleSearchNew");
            messagingService.publish("GetTemplates");
            messagingService.publish("GetProductionGroups", "");
        }
        
        function getProducibleTypeFullName(producibleTypeName){
            var result = "";

            switch (producibleTypeName) {
                case "Carton":
                    result = "PackNet.Common.Interfaces.DTO.Carton.Carton, PackNet.Common.Interfaces";
                    break;
                case "Kit":
                    result = "PackNet.Common.Interfaces.DTO.Carton.Kit, PackNet.Common.Interfaces";
                    break;
                case "Label":
                    result = "PackNet.Common.Interfaces.DTO.PrintingMachines.Label, PackNet.Common.Interfaces";
                    break;
                case "Order":
                    result = "PackNet.Common.Interfaces.DTO.Orders.Order, PackNet.Common.Interfaces";
                    break;
            }

            return result;
        }

        function getAllowedRotations(designId){
            var design = helperService.linq.getItemById($scope.packagingDesigns, designId);
            return design.AllowedRotations;
        }

        function getPackagingDesignParameters(designId) {
            var design = helperService.linq.getItemById($scope.packagingDesigns, designId);
            var result = [];

            if (design) {
                //Design Parameters
                result = Enumerable
                    .From(design.DesignParameters)
                    .Select(function (d) { return { Alias: d.Alias, Value: 0, Name: d.Name } })
                    .ToArray();
            }

            return result;
        }

        function waitForResponse(errorMessage) {
            awaitingResponse = true;

            $timeout(function () {
                if (awaitingResponse) {
                    awaitingResponse = false;
                    alertService.clearAll();
                    alertService.addError(errorMessage);
                }
            }, 5000);
        }

        function bindKitGrid(data) {
            
            delete $scope.kitGridOptions.data;
            $scope.kitGridOptions.data = data;
        }

        function buildArticle(order, kit) {
            var result = {
                Alias: order.alias,
                Description: order.description,
                Producible: kit
            };

            if (order.id) {
                result.Id = order.id;
            }

            return result;
        }

        function buildOrder(viewModel) {
            return {
                "$type": getProducibleTypeFullName("Order"),
                ProducibleType: "Order",
                OriginalQuantity: viewModel.quantity,
                Producible: {
                    "$type": getProducibleTypeFullName(viewModel.type),
                    ProducibleType: viewModel.type,
                    CustomerUniqueId: viewModel.alias,
                    CustomerDescription: viewModel.description
                }
            };
        }

        function buildCarton(viewModel) {
            var result = buildOrder(viewModel);

            //XValues
            var xValues = {
                "$type": "System.Collections.Generic.Dictionary`2[[System.String],[System.Double]]"
            };

            for (var i = 0; i < viewModel.xValues.length; i++) {
                var value = viewModel.xValues[i].Value;

                if (!isNaN(value) && value !== "")
                    xValues[viewModel.xValues[i].Name] = value;
            }

            //Specific producible properties
            result.Producible.DesignId = viewModel.design;
            result.Producible.Length = isNaN(viewModel.length) ? 0 : parseFloat(viewModel.length);
            result.Producible.Height = isNaN(viewModel.height) ? 0 : parseFloat(viewModel.height);
            result.Producible.Width = isNaN(viewModel.width) ? 0 : parseFloat(viewModel.width);
            result.Producible.Restrictions = [
                {
                    "$type": "PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce.MustProduceOnCorrugateWithPropertiesRestriction, PackNet.Common.Interfaces",
                    "Width": isNaN(viewModel.corrugateWidth) ? null : parseFloat(viewModel.corrugateWidth),
                    "Thickness": isNaN(viewModel.corrugateThickness) ? null : parseFloat(viewModel.corrugateThickness),
                    "Quality": viewModel.corrugateQuality
                }
            ];
            
            result.Producible.rotation = viewModel.rotation;
                       
            if (viewModel.xValues.length > 0) {
                result.Producible.XValues = xValues;
            }

            return result;
        }

        function buildCartonViewModel(id, order){
            var result = {
                type : order.Producible.ProducibleType,
                id : id,
                alias : order.Producible.CustomerUniqueId,
                description : order.Producible.CustomerDescription,
                quantity : order.OriginalQuantity,
                design : order.Producible.DesignId,
                length : order.Producible.Length,
                height : order.Producible.Height,
                width : order.Producible.Width,
                rotation: order.Producible.Rotation,
                allowedRotations: getAllowedRotations(order.Producible.DesignId)
            };

            if (result.rotation == null) {
                 result.rotation = "";
            };
            
            //Restrictions
            var corrugatePropertyRestriction = Enumerable.From(order.Producible.Restrictions).Where("r => r.$type.contains('MustProduceOnCorrugateWithPropertiesRestriction')").FirstOrDefault();

            if (corrugatePropertyRestriction != null) {
                result.corrugateQuality = corrugatePropertyRestriction.Quality;
                result.corrugateWidth = corrugatePropertyRestriction.Width;
                result.corrugateThickness = corrugatePropertyRestriction.Thickness;
            }

            //TODO: handle other restrictions

            //XValues
            result.xValues = getPackagingDesignParameters(result.design);

            for (var i = 0; i < result.xValues.length; i++) {
                //Explicitly checking "!= null" will let 0's through, but not null or undefined
                if (order.Producible.XValues[result.xValues[i].Name] != null) {
                    result.xValues[i].Value = order.Producible.XValues[result.xValues[i].Name];
                }
            }

            return result;
        }

        function buildLabel(viewModel) {
            var result = buildOrder(viewModel);

            //If the template has a CustomerUniqueId value, it should be the same as the label alias
            Enumerable.From(viewModel.templateVariables)
                .Where(function (v) { return v.Key.toLowerCase() === "customeruniqueid"; })
                .ForEach(function (value, Index) { value.Value = viewModel.alias; });

            //Restrictions
            var template = helperService.linq.getItemByField($scope.templates, viewModel.template, "Name");

            var restrictions = [
                {
                    "$type": templateRestrictionType,
                    "Value": {
                        "Id": template.Id,
                        "Name": template.Name
                    }
                }
            ];

            //PrintData
            var printData = {
                "$type": "System.Collections.Generic.Dictionary`2[[System.String],[System.String]]"
            };

            for (var i = 0; i < viewModel.templateVariables.length; i++) {
                printData[viewModel.templateVariables[i].Key] = viewModel.templateVariables[i].Value;
            }
            
            //Specific producible properties
            result.Producible.Restrictions = restrictions;
            result.Producible.PrintData = printData;

            return result;
        }

        function buildLabelViewModel(id, order) {
            var result = {
                type: order.Producible.ProducibleType,
                id: id,
                alias: order.Producible.CustomerUniqueId,
                description: order.Producible.CustomerDescription,
                quantity: order.OriginalQuantity,
                template: order.Producible.Restrictions.length > 0 ? order.Producible.Restrictions[0].Value.Name : null,
                templateVariables: []
            };

            for (var property in order.Producible.PrintData) {
                if (property !== "$type" && order.Producible.PrintData.hasOwnProperty(property)) {
                    result.templateVariables.push({ Key: property, Value: order.Producible.PrintData[property] });
                }
            }

            return result;
        }

        function buildKit(viewModel) {

            var result = {
                "$type": getProducibleTypeFullName("Kit"),
                Id: viewModel.Id,
                ItemsToProduce: []
            }

            angular.forEach(viewModel.itemsToProduce, function (item) {
                switch (item.type) {
                    case "Carton":
                        var order = buildCarton(item);
                        order.Producible.ArticleId = viewModel.alias;
                        result.ItemsToProduce.push(order);

                        break;
                    case "Label":
                        result.ItemsToProduce.push(buildLabel(item));
                        break;
                }
            });

            return result;
        }

        function buildKitViewModel(id, article) {
            var result = {
                type: article.Producible.ProducibleType,
                id: id,
                alias: article.Alias,
                description: article.Description,
                //quantity: article.Producible.OriginalQuantity,
                itemsToProduce: Enumerable
                    .From(article.Producible.ItemsToProduce)
                    .Select(function (item) {
                        var viewModel = {};

                        switch (item.Producible.ProducibleType) {
                            case "Carton":
                                viewModel = buildCartonViewModel(null, item);
                                break;
                            case "Label":
                                viewModel = buildLabelViewModel(null, item);
                                break;
                            default:
                                break;
                        }

                        return viewModel;
                    })
                    .ToArray()
            };

            return result;
        }

        function emptyArticle() {
            return { id: null, alias: "", description: "", quantity: 1 };
        }

        function emptyCarton() {
            var result = emptyArticle();

            result.type = "Carton";
            result.design = null;
            result.length = null;
            result.height = null;
            result.width = null;
            result.corrugateWidth = null;
            result.corrugateThickness = null;
            result.corrugateQuality = 1;

            return result;
        }

        function emptyLabel() {
            var result = emptyArticle();

            result.type = "Label";
            result.template = null;
            result.templateVariables = [];

            return result;
        }

        function emptyKit() {
            var result = emptyArticle();

            result.type = "Kit";
            result.itemsToProduce = [];

            return result;
        }

        function isKitEdition() {
            return $scope.selectedKit != null;
        }
        
        function saveArticle(article){
            messagingService.publish(!article.hasOwnProperty("Id") ? "ArticleCreateNew" : "ArticleUpdateNew", article, true);

            waitForResponse(translationService.translate("Common.Message.SaveTimeout", translationService.translate("Article.Label.NameSingular"), article.Alias));
        }

        //Bindings
        $scope.floatingPointRegex = /^(\d+\.?\d*|\.\d+)$/;
        $scope.intRegex = /^\d+$/;
        $scope.exportAllText = translationService.translate("Common.Label.Export");
        $scope.packagingDesigns = [];
        $scope.templates = [];
        $scope.selectedCarton = null;
        $scope.selectedLabel = null;
        $scope.selectedKit = null;
        $scope.showDesignPreview = true;
        $scope.showDesignPreviewContainer = true;

        
        $scope.articlesGridOptions = {
            columnDefs: [
                {
                    name: translationService.translate("Common.Label.Name"),
                    field: "Alias",
                    filter: {
                        condition: uiGridConstants.filter.CONTAINS,
                        placeholder: translationService.translate("Common.Label.Contains").toLowerCase()
                    }
                },
                {
                    name: translationService.translate("Common.Label.Description"),
                    field: "Description",
                    filter: {
                        condition: uiGridConstants.filter.CONTAINS,
                        placeholder: translationService.translate("Common.Label.Contains").toLowerCase()
                    }
                }
            ],
            enableColumnResizing: true,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableSorting: true,
            expandableRowHeight: 150,
            expandableRowTemplate: "partials/Articles/articlesListExpandableRow.html",
            showGridFooter: true,
            onRegisterApi: function (gridApi) {
                //Set gridApi on scope
                delete $scope.articlesGridApi;
                $scope.articlesGridApi = gridApi;                
                //Allow the click of a row to select the row
                gridApi.cellNav.on.navigate($scope, function (newRowCol) {
                    if (newRowCol.col.field !== "selectionRowHeaderCol")
                        $scope.articlesGridApi.selection.toggleRowSelection(newRowCol.row.entity);
                });

                //Builds sub-grid information when expanding the row
                gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                    if (row.isExpanded && row.entity.Producible.ProducibleType === "Kit" && !row.entity.subGridOptions) {
                        row.entity.subGridOptions = {
                            enableColumnResizing: true,
                            columnDefs: [
                                { name: translationService.translate("Common.Label.Name"), field: "Producible.CustomerUniqueId" },
                                { name: translationService.translate("Common.Label.Description"), field: "Producible.CustomerDescription" },
                                { name: translationService.translate("Common.Label.Quantity"), field: "OriginalQuantity", width: "125" },
                                { name: translationService.translate("Common.Label.Type"), field: "Producible.ProducibleType", cellFilter: "producibleType", width: "125" }
                            ],
                            data: row.entity.Producible.ItemsToProduce
                        };
                    }
                });
            }
        };

        $scope.kitGridOptions = {
            columnDefs: [
                { name: translationService.translate("Common.Label.Name"), field: "alias" },
                { name: translationService.translate("Common.Label.Description"), field: "description" },
                { name: translationService.translate("Common.Label.Quantity"), field: "quantity" },
                { name: translationService.translate("Common.Label.Type"), field: "type", cellFilter: "producibleType" }
            ],
            enableColumnResizing: true,
            enableFiltering: false,
            enableRowSelection: true,
            enableSelectAll: true,
            enableSorting: false,
            showGridFooter: true,
            onRegisterApi: function (gridApi) {
                //Set gridApi on scope
                delete $scope.kitItemsGridApi;
                $scope.kitItemsGridApi = gridApi;

                //Allow the click of a row to select the row
                gridApi.cellNav.on.navigate($scope, function (newRowCol) {
                    if (newRowCol.col.field !== "selectionRowHeaderCol")
                        $scope.kitItemsGridApi.selection.toggleRowSelection(newRowCol.row.entity);
                });
            }
        };

        //--- Common
        $scope.canEdit = function (gridApi) {
            return gridApi.selection.getSelectedRows().length === 1;
        }
        
        $scope.canDelete = function (gridApi) {
            return gridApi.selection.getSelectedRows().length > 0;
        }

        $scope.canMove = function (gridApi, isDirectionUp) {
            var result = false;

            if (gridApi.selection.getSelectedRows().length === 1) {
                if (isDirectionUp) {
                    result = gridApi.grid.options.data.lastIndexOf(gridApi.selection.getSelectedRows()[0]) > 0;
                }
                else {
                    result = gridApi.grid.options.data.lastIndexOf(gridApi.selection.getSelectedRows()[0]) < gridApi.grid.options.data.length - 1;
                }
            }

            return result;
        }

        //--- Article
        $scope.deleteArticles = function (){
            var itemsToDelete = $scope.articlesGridApi.selection.getSelectedRows();
            var x, y, tempArray, chunk = 100;
                expectedDeleteResponseCount = Math.ceil(itemsToDelete.length / chunk);
                for (x = 0, y = itemsToDelete.length; x < y; x += chunk) {
                    tempArray = itemsToDelete.slice(x, x +chunk);
                    messagingService.publish("ArticleDeleteNew", tempArray, true);
                }

                waitForResponse(translationService.translate("Article.Message.DeleteTimeout"));
        }

        $scope.editArticle = function () {
            var article = $scope.articlesGridApi.selection.getSelectedRows()[0];
            delete $scope.selectedKit;
            $scope.selectedKit = buildKitViewModel(article.Id, article);

            bindKitGrid($scope.selectedKit.itemsToProduce);

            $state.go('^.kit', null, { reload: false });
        }

        $scope.canExportArticles = function (gridApi) {
            return gridApi.core.getVisibleRows(gridApi.grid).length > 0;
        }

        $scope.exportArticles = function () {
            $scope.awaitingArticles = true;
            delete $scope.exportAllText;
            $scope.exportAllText = translationService.translate("Article.Message.Exporting");
            messagingService.publish("ArticleExportNew");
            alertService.addInfo(translationService.translate("Article.Message.ExportStarted"), 5000);
        }

        $scope.produceArticles = function () {
            articleStateService.selectedArticles = $scope.articlesGridApi.selection.getSelectedRows();
            $state.go('ArticlesProductionNew.list', { administratorView: true });
        }

        //--- Carton
        $scope.cancelCarton = function (){
            delete $scope.selectedCarton;
            $scope.selectedCarton = null;

            if (isKitEdition())
                $state.go('^.kit');
            else
                $state.go('^.list', null, { reload: true });
        }

        $scope.newCarton = function (){
            delete $scope.selectedCarton;
            $scope.selectedCarton = emptyCarton();

            if (isKitEdition()) editingKitItem = false;
            
            $state.go('^.carton');
        }

        $scope.$watch('selectedCarton', function () {

            if (!$scope.isCartonDataValid($scope.selectedCarton)) {
                $scope.showDesignPreview = false;
                return;
            }

            $scope.showDesignPreview = true;

            var carton = $scope.getCartonData();

            if (carton.Height == undefined) carton.Height = 0;
            if (carton.Length == undefined) carton.Length = 0;
            if (carton.Width == undefined) carton.Width = 0;
            
            var coc = {
                "Rotation": $scope.selectedCarton.rotation,
                "OriginalCarton": carton
            }

            if ($scope.selectedCarton.rotation === "") {
                coc.Rotation = Enumerable.From($scope.selectedCarton.allowedRotations).FirstOrDefault([], function (d) { return d.Value.Allowed == 1; }).Key;
            }

            messagingService.publish("GetPackagingDesignWithAppliedValues", coc);
        }, true);

        $scope.isCartonDataValid = function (cartonData) {
            var floatingPointRegex = /^(\d+\.?\d*|\.\d+)$/;

            if (cartonData &&
                (cartonData.length == undefined || (cartonData.length != undefined && floatingPointRegex.test(cartonData.length) && parseFloat(cartonData.length) >= 0)) &&
                (cartonData.width == undefined || (cartonData.width != undefined && floatingPointRegex.test(cartonData.width) && parseFloat(cartonData.width) >= 0)) &&
                (cartonData.height == undefined || (cartonData.height != undefined && floatingPointRegex.test(cartonData.height) && parseFloat(cartonData.height) >= 0)) &&
                cartonData.design != undefined) {
                return true;
            }
            else {
                return false;
            }
        }

        $scope.getCartonData = function (){

            var carton = {
                ArticleId: $scope.selectedCarton.alias,
                DesignId: $scope.selectedCarton.design,
                Height: $scope.selectedCarton.height,
                Id: null,
                Length: $scope.selectedCarton.length,
                Width: $scope.selectedCarton.width
            };

            carton.XValues = {};
            
            $.map($scope.selectedCarton.xValues, function (dp) {
                carton.XValues[dp.Name] = dp.Value;
            });

            var record = angular.fromJson(angular.toJson(carton));
            return record;
        }

        $scope.saveCarton = function (form) {
            $scope.$broadcast('show-errors-check-validity');

            if (!form.$valid) return;

            //Assign default values in LWH if they are not there
            if (!$scope.selectedCarton.length) $scope.selectedCarton.length = 0;
            if (!$scope.selectedCarton.height) $scope.selectedCarton.height = 0;
            if (!$scope.selectedCarton.width) $scope.selectedCarton.width = 0;

            //Make sure xValues are not blank/empty
            if ($scope.selectedCarton.xValues) {
                for (var xValue in $scope.selectedCarton.xValues) {
                    if (!xValue.Value) xValue.Value = 0;
                }
            }
            //Save
            if (isKitEdition()) {
                if (!editingKitItem)
                    $scope.selectedKit.itemsToProduce.push($scope.selectedCarton);
                else
                    $scope.selectedKit.itemsToProduce[editingKitItemIndex] = $scope.selectedCarton;

                delete $scope.selectedCarton;
                $scope.selectedCarton = null;

                $state.go('^.kit');
            }
            else {
                var article = buildArticle($scope.selectedCarton.id, buildCarton($scope.selectedCarton));

                saveArticle(article);
            }
        }

        $scope.cartonDesignChange = function (){
            delete $scope.selectedCarton.allowedRotations;
            $scope.selectedCarton.allowedRotations = getAllowedRotations($scope.selectedCarton.design);

            delete $scope.selectedCarton.firstAllowedRotation;
            $scope.selectedCarton.firstAllowedRotation = Enumerable.From($scope.selectedCarton.allowedRotations).FirstOrDefault([], function (d) { return d.Value.Allowed == 1; }).Key;

            delete $scope.selectedCarton.rotation;
            $scope.selectedCarton.rotation = "";

            delete $scope.selectedCarton.xValues;
            $scope.selectedCarton.xValues = getPackagingDesignParameters($scope.selectedCarton.design);
        }

        $scope.toggleDesignPreviewContainer = function (){
            $scope.showDesignPreviewContainer = !$scope.showDesignPreviewContainer;
        }

        $scope.$watch('showDesignPreviewContainer', function (newValue) {
            delete $scope.labelPackageDesignPreview;
            $scope.labelPackageDesignPreview = newValue ? translationService.translate("Article.CartonProducible.Label.HidePreview") : translationService.translate("Article.CartonProducible.Label.ShowPreview");
        });

        //--- Label
        $scope.cancelLabel = function (){
            delete $scope.selectedLabel;
            $scope.selectedLabel = null;

            if (isKitEdition())
                $state.go('^.kit');
            else
                $state.go('^.list', null, { reload: true });
        }

        $scope.newLabel = function (){
            delete $scope.selectedLabel;
            $scope.selectedLabel = emptyLabel();

            if (isKitEdition()) editingKitItem = false;

            $state.go('^.label');
        }

        $scope.saveLabel = function (form) {
            $scope.$broadcast('show-errors-check-validity');

            if (!form.$valid) return;

            //Save
            if (isKitEdition()) {
                if (!editingKitItem)
                    $scope.selectedKit.itemsToProduce.push($scope.selectedLabel);
                else
                    $scope.selectedKit.itemsToProduce[editingKitItemIndex] = $scope.selectedLabel;

                delete $scope.selectedLabel;
                $scope.selectedLabel = null;

                $state.go('^.kit');
            }
            else {
                var article = buildArticle($scope.selectedLabel.id, buildLabel($scope.selectedLabel));

                saveArticle(article);
            }
        }

        $scope.labelTemplateChange = function () {
            var template = helperService.linq.getItemByField($scope.templates, $scope.selectedLabel.template, "Name");

            if (template) {
                //Template Variables
                var variables = helperService.regEx.getValuesBetweenCurlyBrackets(template.Content);

                
                delete $scope.selectedLabel.templateVariables;
                $scope.selectedLabel.templateVariables = Enumerable
                    .From(variables)
                    .Select(function (v) { return { Key: v, Value: "" } })
                    .ToArray();
            }
            else {
                
                delete $scope.selectedLabel.templateVariables;
                $scope.selectedLabel.templateVariables = [];
            }
        }

        //--- Kit
        $scope.cancelKit = function () {
            //Reload will set all selectedProducible variables to null
            $state.go('^.list', null, { reload: true });
        }

        $scope.deleteKitItems = function () {
            angular.forEach($scope.kitItemsGridApi.selection.getSelectedRows(), function (data, index) {
                $scope.kitGridOptions.data.splice($scope.kitGridOptions.data.lastIndexOf(data), 1);
            });
        }

        $scope.editKitItem = function () {
            editingKitItem = true;
            editingKitItemIndex = $scope.selectedKit.itemsToProduce.indexOf($scope.kitItemsGridApi.selection.getSelectedRows()[0]);

            var viewModel = angular.copy($scope.selectedKit.itemsToProduce[editingKitItemIndex]);

            switch (viewModel.type) {
                case "Carton":
                    delete $scope.selectedCarton;
                    $scope.selectedCarton = viewModel;
                    $state.go('^.carton', null, { reload: false });
                    break;
                case "Label":
                    delete $scope.selectedLabel;
                    $scope.selectedLabel = viewModel;
                    $state.go('^.label', null, { reload: false });
                    break;
            }
        }

        $scope.moveKitItem = function (isDirectionUp) {
            var kitItemToMove = $scope.kitItemsGridApi.selection.getSelectedRows()[0];
            var originalIndex = $scope.kitGridOptions.data.lastIndexOf(kitItemToMove);

            //Pop item
            $scope.kitGridOptions.data.splice(originalIndex, 1);
            //Push item
            $scope.kitGridOptions.data.splice(originalIndex + (isDirectionUp ? -1 : 1), 0, kitItemToMove);
        }

        $scope.newKit = function (){
            delete $scope.selectedKit;
            $scope.selectedKit = emptyKit();

            bindKitGrid($scope.selectedKit.itemsToProduce);

            $state.go('^.kit');
        }

        $scope.copyArticle = function () {

            var article = $scope.articlesGridApi.selection.getSelectedRows()[0];
            var clone = $.extend(true, {}, article);
            clone.Alias = "";
            clone.Id = null;
            delete $scope.selectedKit;
            $scope.selectedKit = buildKitViewModel(clone.Id, clone);

            bindKitGrid($scope.selectedKit.itemsToProduce);

            $state.go('^.kit', null, { reload : false });
        }

        $scope.saveKit = function (form) {
            $scope.$broadcast('show-errors-check-validity');

            if (!form.$valid) return;

            //TODO: Find a way to trigger this error as the other ones
            if ($scope.selectedKit.itemsToProduce.length === 0) {
                alertService.addError(translationService.translate("Article.KitProducible.Validation.MustContainItem"), 5000);
                return;
            }

            //Save
            var article = buildArticle($scope.selectedKit, buildKit($scope.selectedKit));

            saveArticle(article);
        }

        //Listeners
        messagingService.articlesExportedNewObservable.autoSubscribe(function (msg) {
            if ($scope.awaitingArticles == true) {
                if (msg.Data)
                    csvService.saveCsVtoFile(msg.Data, "articles");
                else {
                    alertService.addError(translationService.translate("Article.Message.ExportNoResults"));
                }
                $scope.awaitingArticles = false;
                delete $scope.exportAllText;
                $scope.exportAllText = translationService.translate("Common.Label.Export");
            }

        }, $scope, "Export all");

        messagingService.messageSubject.where(messagingService.filterObs("ArticleSearchResponseNew"))
            .select(messagingService.responseDataFunction).autoSubscribe(function (msg) {
                alertService.closeByMessage(loadingMessage);
                
                delete $scope.articlesGridOptions.data;
                $scope.articlesGridOptions.data = msg.Data;

                //this $apply is needed when we have very big sets of Articles
                //I noticed that the data comes in too late for angular to notice it. And since the page is kept pristine unless you touch it a digest cycle is never run and therefore an update to the grid is never done.
                //Please don't remove or find a better way to force an update when you have very big data sets
                if (!$scope.$$phase) {
                    $scope.$apply();
                }

            }, $scope, "Articles List");

        messagingService.articleNewCreatedObservable.autoSubscribe(function (msg) {
            if (awaitingResponse) {
                awaitingResponse = false;

                var alias;

                if ($scope.selectedCarton)
                    alias = $scope.selectedCarton.alias;
                else if ($scope.selectedLabel)
                    alias = $scope.selectedLabel.alias;
                else
                    alias = $scope.selectedKit.alias;

                if (msg.Result === "Fail") {
                    alertService.addError(translationService.translate("Common.Message.CreateFailed", translationService.translate("Article.Label.NameSingular"), alias));
                }
                else if (msg.Result === "Exists" || msg.Result === "ArticleAliasAlreadyInUse") {
                    alertService.addError(translationService.translate("Common.Message.NameExists", translationService.translate("Article.Label.NameSingular"), alias));
                }
                else if (msg.Result === "Success") {
                    alertService.addSuccess(translationService.translate("Common.Message.CreateSuccess", translationService.translate("Article.Label.NameSingular"), alias), 5000);

                    //Reload will set all selectedXYZ variables to null
                    $state.go('^.list', null, { reload: true });

                }
            }
        }, $scope, "Article Created");

        messagingService.articleNewUpdatedObservable.autoSubscribe(function (msg) {
            if (awaitingResponse) {
                awaitingResponse = false;

                if (msg.Result === "Fail") {
                    alertService.addError(translationService.translate("Common.Message.UpdateFailed", translationService.translate("Article.Label.NameSingular"), msg.Data.Alias));
                }
                else if (msg.Result === "Exists" || msg.Result === "ArticleAliasAlreadyInUse") {
                    alertService.addError(translationService.translate("Common.Message.NameExists", translationService.translate("Article.Label.NameSingular"), msg.Data.Alias));
                                    }
                else if (msg.Result === "Success") {
                    alertService.addSuccess(translationService.translate("Common.Message.UpdateSuccess", translationService.translate("Article.Label.NameSingular"), msg.Data.Alias), 5000);

                    //Reload will set all selectedXYZ variables to null
                    $state.go('^.list', null, { reload: true });

                }
            }
        }, $scope, "Article Updated");
        
        messagingService.articleNewDeletedObservable.autoSubscribe(function (msg) {
            if (awaitingResponse) {
                deleteResponseCount++;
                if (deleteResponseCount === expectedDeleteResponseCount) {
                    awaitingResponse = false;
                    expectedDeleteResponseCount = 0;
                    deleteResponseCount = 0;
                    $scope.articlesGridApi.selection.clearSelectedRows();
                }
                var articles = $scope.articlesGridApi.selection.getSelectedRows();

                if (msg.Result === "Fail") {
                    if (articles.length === 1) {
                        alertService.addError(translationService.translate("Common.Message.DeleteFailed", translationService.translate("Article.Label.NameSingular"), articles[0].Alias));
                    }
                    else {
                        alertService.addError(translationService.translate("Article.Message.DeleteFailed"));
                    }
                }
                else if (msg.Result === "Success") {          
                    if (articles.length === 1) {
                        alertService.addSuccess(translationService.translate("Common.Message.DeleteSuccess", translationService.translate("Article.Label.NameSingular"), articles[0].Alias), 5000);
                    }
                    else {
                        alertService.addSuccess(translationService.translate("Article.Message.DeleteSuccess"), 5000);
                    }
                }
                //Already on list page refresh data
                requestData();
            }
        }, $scope, "Article Deleted");

        delete $scope.packagingDesigns;
        $scope.packagingDesigns = packagingDesignService.getAllDesigns();
        packagingDesignService.allDesignsObservable.autoSubscribe(function () {
            delete $scope.packagingDesigns;
            $scope.packagingDesigns = packagingDesignService.getAllDesigns();
        }, $scope, 'Get Designs');

        messagingService.packagingDesignWithAppliedValuesObservable.autoSubscribe(function (msg) {
            var design = helperService.linq.getItemById($scope.packagingDesigns, $scope.selectedCarton.design);

            angular.forEach(msg.Data.Lines, function (line, index) {
                if (design.Lines[index].Label != null) {
                    line.Label = design.Lines[index].Label;
                }
            });

            designPreviewService.draw.drawPreview(msg.Data, $("#previewContainer"), $("#restrictionsContainer").width(), 200);
        }, $scope, "Packaging Design Preview");

        templatesObservable.autoSubscribe(function (msg) {
            delete $scope.templates;
            $scope.templates = msg.Data;
        }, $scope, 'Templates');

        $scope.openDeleteDialog = function (){
            var opts = {
                backdrop : true,
                keyboard : true,
                templateUrl : 'partials/ArticleManagement/deleteArticleDialog.html',
                controller : 'DeleteArticleDialogCtrl',
                windowClass : "smallDialogWindow",
                resolve : {
                    selectedArticlesCount : function (){
                        return $scope.articlesGridApi.selection.getSelectedRows().length;
                    }
                }
            };

            alertService.clearAll();
            $modal.open(opts).result.then(function () {
                $scope.deleteArticles();
            });
        }

        //IntroJS
        $scope.tellMeAboutThisPage = function (){
            var columnDefs;
            var steps = [];
            var tables = document.querySelectorAll("#nudHorizontalTable");

            if ($state.current.name === "ArticlesNew.list") {
                columnDefs = $("[ui-grid-header-cell]");

                steps.push({
                    element: "#notFound",
                    intro: translationService.translate("Article.Help.List.Overview")
                });

                steps.push({
                    element: "#newArticle",
                    intro: translationService.translate("Article.Help.List.New")
                });

                steps.push({
                    element: "#deleteArticles",
                    intro: translationService.translate("Article.Help.List.Delete")
                });

                steps.push({
                    element: "#editArticle",
                    intro: translationService.translate("Article.Help.List.Edit")
                });

                steps.push({
                    element: "#copyArticle",
                    intro: translationService.translate("Article.Help.List.Copy")
                });

                steps.push({
                    element: "#produceArticle",
                    intro: translationService.translate("Article.Help.List.Produce")
                });

                steps.push({
                    element: "#importArticles",
                    intro: translationService.translate("Article.Help.List.Import")
                });

                steps.push({
                    element: "#exportArticles",
                    intro: translationService.translate("Article.Help.List.Export")
                });

                steps.push({
                    element: '#articlesList',
                    intro: translationService.translate("Article.Help.List.Columns.Overview")
                });

                steps.push({
                    element: columnDefs[2],
                    intro: translationService.translate("Article.Help.List.Columns.Name")
                });

                steps.push({
                    element: columnDefs[3],
                    intro: translationService.translate("Article.Help.List.Columns.Description")
                });
            }
            else if ($state.current.name === "ArticlesNew.carton") {
                steps.push({
                    element: "#notFound",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Overview")
                });

                steps.push({
                    element: "#alias",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Name")
                });

                steps.push({
                    element: "#description",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Description")
                });

                steps.push({
                    element: tables[0],
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Quantity")
                });

                steps.push({
                    element: "#design",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Design")
                });

                steps.push({
                    element: "#Length",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Length")
                });

                steps.push({
                    element: "#Width",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Width")
                });

                steps.push({
                    element: "#Height",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Height")
                });

                steps.push({
                    element: tables[1],
                    intro: translationService.translate("Article.CartonProducible.Help.Form.CorrugateQuality")
                });

                steps.push({
                    element: "#CorrugateWidth",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.CorrugateWidth")
                });

                steps.push({
                    element: "#CorrugateThickness",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.CorrugateThickness")
                });

                steps.push({
                    element: "#rotation",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Rotation")
                });

                steps.push({
                    element: "#previewAndToggleContainer",
                    intro: translationService.translate('Article.CartonProducible.Help.Form.Preview')
                });

                steps.push({
                    element: "#cartonSaveButton",
                    intro: translationService.translate("Article.Help.Form.SaveChild")
                });

                steps.push({
                    element: "#cartonCancelButton",
                    intro: translationService.translate("Article.Help.Form.CancelChild")
                });
            }
            else if ($state.current.name === "ArticlesNew.label") {
                steps.push({
                    element: "#notFound",
                    intro: translationService.translate("Article.LabelProducible.Help.Form.Overview")
                });

                steps.push({
                    element: "#labelAlias",
                    intro: translationService.translate("Article.LabelProducible.Help.Form.Name")
                });

                steps.push({
                    element: "#labelDescription",
                    intro: translationService.translate("Article.LabelProducible.Help.Form.Description")
                });

                steps.push({
                    element: tables[0],
                    intro: translationService.translate("Article.LabelProducible.Help.Form.Quantity")
                });

                steps.push({
                    element: "#labelTemplate",
                    intro: translationService.translate("Article.LabelProducible.Help.Form.Template")
                });

                steps.push({
                    element: "#labelSaveButton",
                    intro: translationService.translate("Article.Help.Form.SaveChild")
                });

                steps.push({
                    element: "#labelCancelButton",
                    intro: translationService.translate("Article.Help.Form.CancelChild")
                });
            }
            else if ($state.current.name === "ArticlesNew.kit") {
                columnDefs = $("[ui-grid-header-cell]");

                steps.push({
                    element: "#notFound",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Overview")
                });

                steps.push({
                    element: "#kitAlias",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Name")
                });

                steps.push({
                    element: "#kitDescription",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Description")
                });

                steps.push({
                    element: "#articlesList",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Items.Overview")
                });

                steps.push({
                    element: "#newArticleDropDown",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Items.New")
                });

                steps.push({
                    element: "#deleteArticles",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Items.Delete")
                });

                steps.push({
                    element: "#editArticle",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Items.Edit")
                });

                steps.push({
                    element: "#moveUp",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Items.MoveUp")
                });

                steps.push({
                    element: "#moveDown",
                    intro: translationService.translate("Article.KitProducible.Help.Form.Items.MoveDown")
                });

                steps.push({
                    element: "#kitSaveButton",
                    intro: translationService.translate("Article.Help.Form.Save")
                });

                steps.push({
                    element: "#kitCancelButton",
                    intro: translationService.translate("Article.Help.Form.Cancel")
                });
            }

            return steps;
        };

        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);

        //Initialize
        $scope.utilities.initialDataRequest($scope, messagingService, requestData);
    }
]);

function DeleteArticleDialogCtrl($modalInstance, $scope, selectedArticlesCount){

    $scope.selectedArticlesCount = selectedArticlesCount;

    $scope.closeWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.confirmDelete = function () {
        $modalInstance.close();
    }
};