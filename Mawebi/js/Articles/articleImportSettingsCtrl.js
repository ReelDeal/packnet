﻿function articleServiceSettingsCtrl($rootScope, $scope, messagingService, translationService, NOTIFICATIONS_EVENTS){
    $scope.notEmptyPattern = /^.+$/;
    $scope.configurationOptions = {
        ArticleImportFileDelimeter: "",
        ArticleImportFileCommentPrefix: ""
    };

    $scope.isValid = function (form) {
        console.log(form);
    }

    $rootScope.$on(NOTIFICATIONS_EVENTS.settingsSaved, function () {
        console.log('Save called in articleImportSettingsCtrl');

        if ($scope.configurationOptions.ArticleImportFileDelimeter == "" ||
            $scope.configurationOptions.ArticleImportFileCommentPrefix == "") {
            console.log("Unable to save Article Import settings since the provided data is invalid");
            return;
        }

        var settings = {
            ArticleImportFileDelimeter: $scope.configurationOptions.ArticleImportFileDelimeter,
            ArticleImportFileCommentPrefix: $scope.configurationOptions.ArticleImportFileCommentPrefix
        };

        messagingService.publish("SaveArticleServiceConfiguration", settings);
    });


    messagingService.messageSubject
       .where(messagingService.filterObs("SaveArticleServiceConfigurationResponse"))
       .select(messagingService.responseDataFunction)
       .autoSubscribe(function (msg) {
           if (msg.Result == 'Success') {
               console.log("Succesfully updated article configuration");
           }
           else {
               console.log("Failed when updating article configuration");
           }
       }, $scope, "Article configuration update subscriber");


    messagingService.messageSubject
    .where(messagingService.filterObs("GetCurrentArticleServiceConfigurationResponse"))
    .select(messagingService.responseDataFunction)
    .autoSubscribe(function (msg) {
        if (msg.Result == 'Success') {
            var configuration = msg.Data;
            delete $scope.configurationOptions.ArticleImportFileCommentPrefix;
            $scope.configurationOptions.ArticleImportFileCommentPrefix = configuration.ArticleImportFileCommentPrefix;
            delete $scope.configurationOptions.ArticleImportFileDelimeter;
            $scope.configurationOptions.ArticleImportFileDelimeter = configuration.ArticleImportFileDelimeter;
        }
    }, $scope, "Article configuration subscriber");

    messagingService.publish("GetCurrentArticleServiceConfiguration");
};

app.directive('articleServiceSettings', function () {
    return {
        controller : "articleServiceSettingsCtrl",
        restrict : 'E',
        scope : true,
        templateUrl : 'partials/Articles/articleServiceSettings.html'
    };
});