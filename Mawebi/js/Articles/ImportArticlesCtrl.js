function importArticlesCtrl($interval, $scope, $state, $timeout, alertService, introJsService, messagingService, stateChangeService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope);
    $interval = $interval.getInstance($scope);

    
    $scope.uploadme = {};
    $scope.uploadme.src = "";
    $scope.useHeadersInFile = true;
    $scope.headerFields = [];
    $scope.importCollection = {};
    $scope.importCollection.Items = [];
    $scope.configuration = {};

    $scope.awaitingUpdate = false;
    $scope.progress = 1;

    var expectedImportSuccessCount = 0;
    var importSuccessCount = 0;

    $scope.articlesGridOptions = {
        data: 'importCollection.Items',
        columnDefs: [
            {
                name: translationService.translate("Common.Label.Name"),
                field: "Alias",
                filter: {
                    condition: uiGridConstants.filter.CONTAINS,
                    placeholder: translationService.translate("Common.Label.Contains").toLowerCase()
                },
                cellTemplate: '<div class="ui-grid-cell-contents ng-binding ng-scope" required>{{row.entity.Alias}}</div>'
            },
            {
                name: translationService.translate("Common.Label.Description"),
                field: "Description",
                filter: {
                    condition: uiGridConstants.filter.CONTAINS,
                    placeholder: translationService.translate("Common.Label.Contains").toLowerCase()
                }
            },
            {
                field: 'ImportAction',
                displayName: translationService.translate("Common.Label.Action"),
                enableHiding: false,
                enableFiltering: false,
                menuItems: [
                    {
                        title: translationService.translate('Common.Import.Label.ImportUpdateAll'),
                        action: function () {
                            if (!$scope.importCollection || !$scope.importCollection.Items) return;

                            $scope.importCollection.Items.forEach(function (item) {
                                if (item.originalAction != 'ArticleActionImport') {
                                    item.ImportAction = "ArticleActionUpdate";
                                }
                                else {
                                    item.ImportAction = "ArticleActionImport";
                                }
                            });
                        }
                    },
                    {
                        title: translationService.translate('Common.Import.Label.IgnoreAllConflicted'),
                        action: function () {
                            if (!$scope.importCollection || !$scope.importCollection.Items) return;

                            $scope.importCollection.Items.forEach(function (item) {
                                if (item.originalAction != 'ArticleActionImport') item.ImportAction = "ArticleActionIgnore";
                            });
                        }
                    }
                ],
                cellTemplate: 'partials/Articles/articleImportActionTemplate.html',
                cellClass: 'ui-grid-cell-contents-no-padding'
            }
        ],
        enableColumnResizing: true,
        enableFiltering: true,
        enableRowSelection: true,
        enableSelectAll: true,
        enableSorting: true,
        expandableRowHeight: 150,
        expandableRowTemplate: "partials/Articles/articlesListExpandableRow.html",
        showGridFooter: true,
        onRegisterApi: function (gridApi) {
            //Set gridApi on scope
            delete $scope.articlesGridApi;
            $scope.articlesGridApi = gridApi;

            //Allow the click of a row to select the row
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.articlesGridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });

            //Builds sub-grid information when expanding the row
            gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                if (row.isExpanded && row.entity.Producible.ProducibleType === "Kit" && !row.entity.subGridOptions) {
                    row.entity.subGridOptions = {
                        enableColumnResizing: true,
                        columnDefs: [
                            { name: translationService.translate("Common.Label.Name"), field: "Producible.CustomerUniqueId" },
                            { name: translationService.translate("Common.Label.Description"), field: "Producible.CustomerDescription" },
                            { name: translationService.translate("Common.Label.Quantity"), field: "OriginalQuantity" },
                            { name: translationService.translate("Common.Label.Type"), field: "Producible.ProducibleType", cellFilter: "producibleType" }
                        ],
                        data: row.entity.Producible.ItemsToProduce
                    };
                }
            });

        }
    };

    function SetupProgressBarHandling(maxTime) {
        $scope.progress = 1;
        var tick = maxTime / 99;
        $scope.currentProgressBarPromise = $interval(function () { $scope.progress++; }, tick, 99);
    }

    function TearDownProgressBarHandling() {
        if ($scope.currentProgressBarPromise) {
            $interval.cancel($scope.currentProgressBarPromise);
            
            delete $scope.currentProgressBarPromise;
        }
    }

    function SetupAwaitingUpdate(delay, errorMessage) {
        $scope.awaitingUpdate = true;

        var waitTime = Math.max(delay, 5000);

        SetupProgressBarHandling(waitTime);
        $timeout(function () {
            if ($scope.awaitingUpdate) {
                alertService.addError(errorMessage);
                $scope.awaitingUpdate = false;
            }
        }, waitTime);
    }

    $scope.importActionCanImport = function (item) {
        return item.originalAction == 'ArticleActionImport';
    };

    $scope.importActionCanUpdate = function (item) {
        return item.originalAction != 'ArticleActionImport';
    };

    $scope.cantFinalize = function (){
        return $scope.importCollection.Items.length == 0 || $scope.awaitingUpdate;
    }
    
    //This helper should have a line for each validation alert for imports as to clear all import validation errors.
    function clearImportValidationAlerts(){
        alertService.closeByMessage(translationService.translate("Article.Import.Validation.NameRequired"));
    }

    $scope.cantProcess = function () {
        return $scope.uploadme.src == '' || $scope.awaitingUpdate;
    }

    $scope.cantBrowse = function () {
        return $scope.configuration.ImportFileDelimeter == '' || $scope.configuration.ImportFileCommentPrefix == '' || ($scope.useHeadersInFile == false && $scope.headerFields.lenght == 0) || $scope.awaitingUpdate;
    }

    $scope.clearSelectedFile = function () {
        
        delete $scope.uploadme.src;
        $scope.uploadme.src = "";
        alertService.clearAll();
    }

    $scope.processFile = function () {
        messagingService.publish("ArticleImportNew", {
            "Contents": $scope.uploadme.src,
            "Delimiter": $scope.configuration.ArticleImportFileDelimeter,
            "CommentPrefix": $scope.configuration.ArticleImportFileCommentPrefix,
            "HeadersInFile": $scope.useHeadersInFile,
            "HeaderFields": $scope.useHeadersInFile ? [] : Enumerable.From($scope.headerFields).Select(function (field) { return field.text; }).ToArray()
        }, true);

        SetupAwaitingUpdate($scope.uploadme.src.length, translationService.translate("Common.Import.Message.ProcessFailed"));

        
        delete $scope.uploadme;
        $scope.uploadme = {};
        
        delete $scope.uploadme.src;
        $scope.uploadme.src = "";
    }

    $scope.finalizeImport = function (){

        clearImportValidationAlerts();

        var numItems = $scope.importCollection.Items.length;
        var items = [];

        for (var i = 0; i < numItems; i++) {

            var record = $scope.importCollection.Items[i];
            if (record.ImportAction !== "ArticleActionIgnore") {
                delete record.originalAction;
                delete record.ImportAction;
                
                if (record.Alias) {
                    items.push(record);
                }
                else {
                    alertService.addError(translationService.translate("Article.Import.Validation.NameRequired"));
                    return;
                }
            }
        }

        var x, y, tempArray, chunk = 100;
        expectedImportSuccessCount = Math.ceil(items.length / chunk);

        for (x = 0, y = items.length; x < y; x += chunk) {
            tempArray = items.slice(x, x + chunk);
            messagingService.publish("FinalizeArticleImport", tempArray, true);
        }

        delete $scope.importCollection;
        $scope.importCollection = {};
        
        delete $scope.importCollection.Items;
        $scope.importCollection.Items = [];

        SetupAwaitingUpdate(numItems * 10, translationService.translate("Common.Import.Message.FinalizeFailed"));
    }

    $scope.cancelButtonClick = function () {
        $scope.clearSelectedFile();
        $state.go(stateChangeService.previousState(), null, { reload: false });
    }

    messagingService.messageSubject
        .where(messagingService.filterObs("ArticlesReadyForImport"))
        .select(messagingService.responseDataFunction).autoSubscribe(function (data) {
            if (data.Result == 'Success') {
                
                delete $scope.importCollection;
                $scope.importCollection = data.Data;

                $scope.importCollection.Items.forEach(function (item) {
                    item.originalAction = item.ImportAction;
                });

                $scope.awaitingUpdate = false;
                alertService.addSuccess(translationService.translate("Common.Import.Message.ProcessSuccess"));

                //this $apply is needed when we have very big sets of Articles
                //I noticed that the data comes in too late for angular to notice it. And since the page is kept pristine unless you touch it a digest cycle is never run and therefore an update to the grid is never done.
                //Please don't remove or find a better way to force an update when you have very big data sets
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            }

            TearDownProgressBarHandling();
        }, $scope, 'All Articles');

    messagingService.messageSubject
        .where(messagingService.filterObs("ArticleImportedNew"))
        .select(messagingService.responseDataFunction).autoSubscribe(function (data) {
            if (data.Result == 'Success' || data.Result == 'PartialSuccess') {
                importSuccessCount++;
                if (importSuccessCount === expectedImportSuccessCount) {
                    importSuccessCount = 0;
                    expectedImportSuccessCount = 0;
                    $scope.awaitingUpdate = false;
                    $state.go(stateChangeService.previousState(), null, { reload: true });
                }
            }

            TearDownProgressBarHandling();
        }, $scope, 'All Articles');

    messagingService.messageSubject
        .where(messagingService.filterObs("GetCurrentArticleServiceConfigurationResponse"))
        .select(messagingService.responseDataFunction)
        .autoSubscribe(function (msg) {
            if (msg.Result == 'Success') {
                
                delete $scope.configuration;
                $scope.configuration = msg.Data;
            }
        }, $scope, "Articles Configuration subscriber");

    $scope.resetImport = function () {
        
        delete $scope.uploadme;
        $scope.uploadme = {};
        
        delete $scope.uploadme.src;
        $scope.uploadme.src = "";

        delete $scope.importCollection;
        $scope.importCollection = {};
        
        delete $scope.importCollection.Items;
        $scope.importCollection.Items = [];
    };

    function SetupIntroJs() {
        //Begin Intro JS Stuff
        $scope.tellMeAboutThisPage = function () {
            var steps = [];
            var columnDefs = $("[ui-grid-header-cell]");

            steps.push({
                element: "#notFound",
                intro: translationService.translate('Article.Import.Help.Overview')
            });

            steps.push({
                element: "#CurrentArticleItemsList",
                intro: translationService.translate('Article.Import.Help.Columns.Overview')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate("Article.Help.List.Columns.Name")
            });

            steps.push({
                element: columnDefs[3],
                intro: translationService.translate("Article.Help.List.Columns.Description")
            });

            steps.push({
                element: "#articleImportHeadersInFileSelectionBox",
                intro: translationService.translate('Common.Import.Help.HeadersInFile')
            });

            if ($scope.useHeadersInFile == false) {
                steps.push({
                    element: "#articleHeaderFields",
                    intro: translationService.translate('Common.Import.Help.HeadersInputField')
                });
            }

            steps.push({
                element: "#ArticlesImportDelimiter",
                intro: translationService.translate('Common.Import.Help.Delimiter')
            });

            steps.push({
                element: "#ArticlesImportCommentPrefix",
                intro: translationService.translate('Common.Import.Help.CommentPrefix')
            });

            steps.push({
                element: "#ArticlesImportBrowseFile",
                intro: translationService.translate('Common.Import.Help.BrowseFile')
            });

            steps.push({
                element: "#ArticleImportProcessFile",
                intro: translationService.translate('Article.Import.Help.ProcessFile')
            });

            steps.push({
                element: "#ArticleImportFinalizeImport",
                intro: translationService.translate('Article.Import.Help.FinalizeImport')
            });

            steps.push({
                element: "#ArticleImportCancelImport",
                intro: translationService.translate('Common.Import.Help.Cancel')
            });

            return steps;
        };

        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
        //End Intro JS Stuff
    }

    SetupIntroJs();
    $scope.$watch('useHeadersInFile', function (newValue, oldValue) {
        SetupIntroJs();
    });

    messagingService.publish("GetCurrentArticleServiceConfiguration");
}
