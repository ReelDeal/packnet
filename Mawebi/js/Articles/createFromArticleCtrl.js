app.controller("createFromArticleNewCtrl", ["$scope", "$state", "$timeout", "alertService", "articleStateService", "designPreviewService", "helperService", "introJsService", "machineGroupService", "messagingService", "packagingDesignService", "uiGridConstants", "translationService",
    function ($scope, $state, $timeout, alertService, articleStateService, designPreviewService, helperService, introJsService, machineGroupService, messagingService, packagingDesignService, uiGridConstants, translationService) {
        var loadingMessage = translationService.translate("Common.Message.Loading");

        //Locals
        var requestData = function () {
            if ($scope.gridOptions != undefined && $scope.gridOptions.data != undefined) {
                alertService.addInfo(loadingMessage, 3000);
            }

            messagingService.publish("ArticleSearchNew");
            messagingService.publish("GetTemplates");
        }
        var awaitingResponse = false;
        var corrugateRestrictionType = "PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce.MustProduceOnCorrugateWithPropertiesRestriction, PackNet.Common.Interfaces";
        var currentMachine = machineGroupService.getCurrentMachineGroup();
        
        //ui-grid - v3.0.0-rc.21-efd3798's saveState functionality doesn't support expanded rows and the scroll position is not accurate
        var gridExpandedRows = [];
        var gridRestoreState = false;
        var gridScrollTopPosition = 0;
        var gridViewPortSelector = "#articlesList div[container-id='\\'body\\'']:first > .ui-grid-viewport";

        //Bindings
        $scope.packagingDesigns = packagingDesignService.getAllDesigns();
        $scope.templates = [];
        $scope.corrugateRestrictions = null;
        $scope.readOnly = true;
        $scope.selectedArticle = null;
        $scope.selectedPackageDesign = null;
        $scope.selectedTemplate = null;
        $scope.showDesignPreview = true;
        $scope.showDesignPreviewContainer = true;
        $scope.gridOptions = {
            enableGridMenu: true,
            columnDefs: [
                {
                    name: translationService.translate("Common.Label.Name"),
                    field: "Alias",
                    enableCellEdit: false,
                    filter: {
                        condition: uiGridConstants.filter.CONTAINS,
                        placeholder: translationService.translate("Common.Label.Contains").toLowerCase()
                    }
                },
                {
                    name: translationService.translate("Common.Label.Description"),
                    field: "Description",
                    enableCellEdit: false,
                    filter: {
                        condition: uiGridConstants.filter.CONTAINS,
                        placeholder: translationService.translate("Common.Label.Contains").toLowerCase()
                    }
                },
                {
                    name: translationService.translate("Common.Label.Quantity"),
                    field: "Quantity",
                    enableFiltering: false,
                    enableCellEdit: true,
                    type: "number",
                    width: "125"
                },
                {
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        //Couldn't find a better method to use, but it works xD

                        //Anything that is not a "Kit" should NOT be expandable
                        helperService.uiGrid.hideExpandableIcon("articlesList", rowRenderIndex, row.entity.Producible.ProducibleType !== "Kit");

                        return '';
                    },
                    enableCellEdit: false,
                    name: translationService.translate("Common.Label.Type"),
                    field: "Producible.ProducibleType",
                    enableFiltering: false,
                    cellFilter: "producibleType",
                    width: "125"
                }
            ],
            enableColumnResizing: true,
            enableFiltering: true,
            enableRowSelection: true,
            enableSelectAll: true,
            enableSorting: true,
            expandableRowHeight: 150,
            expandableRowScope: {
                editArticle: function (rowEntity) {
                    return $scope.editArticle(rowEntity);
                }
            },
            expandableRowTemplate: "partials/Articles/Production/articlesListExpandableRow.html",
            showGridFooter: true,
            onRegisterApi: function (gridApi) {
                //Set gridApi on scope
                delete $scope.gridApi;
                $scope.gridApi = gridApi;

                // Restores grid state (if any)
                gridApi.core.on.renderingComplete($scope, function (grid) {
                    if (gridRestoreState) {
                        //toggleRowExpansion only works when the grid is visible on screen
                        $timeout(function () {
                            for (var i = 0; i < gridExpandedRows.length; i++) {
                                $scope.gridApi.expandable.toggleRowExpansion(gridExpandedRows[i]);
                            }

                            //toggleRowExpansion has a bit of delay when increasing the viewport's height
                            $timeout(function () {
                                $(gridViewPortSelector).scrollTop(gridScrollTopPosition);
                            }, 50);

                            gridRestoreState = false;
                        }, 50);
                    }
                });

                //Allow the click of a row to select the row
                gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                    if (newRowCol.col.field !== "selectionRowHeaderCol")
                        $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
                });
                
                gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                    //Store the rows that are expanded (the ui-grid saveState functionality doesn't store this information)
                    if (row.isExpanded) {
                        if (gridExpandedRows.indexOf(row.entity) === -1) gridExpandedRows.push(row.entity);
                    }
                    else {
                        gridExpandedRows.pop(row.entity);
                    }


                    //Builds sub-grid information when expanding the row
                    if (row.isExpanded && row.entity.Producible.ProducibleType === "Kit" && !row.entity.subGridOptions) {
                        row.entity.subGridOptions = {
                            enableColumnResizing: true,
                            columnDefs: [
                                { name: translationService.translate("Common.Label.Name"), field: "Producible", cellTemplate: "partials/Articles/Production/CellTemplates/alias.html" },
                                { name: translationService.translate("Common.Label.Description"), field: "Producible.CustomerDescription" },
                                { name: translationService.translate("Common.Label.Quantity"), field: "OriginalQuantity", width: "125" },
                                { name: translationService.translate("Common.Label.Type"), field: "Producible.ProducibleType", cellFilter: "producibleType", width: "125" }
                            ],
                            data: row.entity.Producible.ItemsToProduce
                        };
                    }
                });
            }
        };

        var showCrossConveyorPickzone = false;

        var conveyorPiczoneCapability = "PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities.ConveyorPickzoneCapability, PackNet.IODeviceService";
        
        $scope.pickZones = [];
        if (Enumerable.From(currentMachine.AllMachineCapabilities).Any(function (c) { return c.$type == conveyorPiczoneCapability; })) {
            showCrossConveyorPickzone = true;
            delete $scope.pickZones;
            $scope.pickZones = Enumerable.From(currentMachine.AllMachineCapabilities).Where(function (c) { return c.$type == conveyorPiczoneCapability; }).Select('c=>c.PickZone').ToArray();
            
            $scope.gridOptions.columnDefs.push(
                {
                    name: translationService.translate("Article.Production.Label.PickZoneSelection"),
                    field: "pickzoneLabel",
                    enableCellEdit: true,
                    enableFiltering: false,
                    editDropdownOptionsArray: $scope.pickZones,
                    editableCellTemplate: "ui-grid/dropdownEditor",
                    editDropdownValueLabel: "Alias",
                    editDropdownIdLabel: "Alias", //Value that is going to be assigned to the cell, for displaying purposes, we need the Alias. The Id would be manually assigned on afterCellEdit.
                });
        }

        $scope.canProduce = function (){
            var pickzoneComplete = true;
            if (showCrossConveyorPickzone) {
                $scope.gridApi.selection.getSelectedRows().forEach(function (article) {
                    if (article.pickzoneLabel == undefined) {
                        pickzoneComplete = false;
                    }
                });
            }
            return $scope.gridApi.selection.getSelectedRows().length > 0 && pickzoneComplete;
        };

        $scope.editArticle = function (rowEntity) {
            //Saves the grid's viewport scroll position
            gridScrollTopPosition = $(gridViewPortSelector).scrollTop();
            gridRestoreState = true;

            delete $scope.selectedArticle;
            $scope.selectedArticle = rowEntity;

            switch ($scope.selectedArticle.Producible.ProducibleType) {
                case "Carton" :
                    
                    delete $scope.corrugateRestrictions;
                    $scope.corrugateRestrictions = Enumerable.From($scope.selectedArticle.Producible.Restrictions).Where("r => r.$type.contains('" + corrugateRestrictionType + "')").FirstOrDefault();
                    $scope.cartonDesignChange();

                    $state.go("^.carton");
                    break;
                case "Label" :
                    $scope.labelTemplateChange();

                    $state.go("^.label");
                    break;
            }
        };

        $scope.produceArticles = function (){
            articleStateService.selectedArticles = $scope.gridApi.selection.getSelectedRows();
            $scope.gridApi.selection.clearSelectedRows();

            var articleProductionRequests = Enumerable
                .From(articleStateService.selectedArticles)
                .Select(function (item) {
                    if (showCrossConveyorPickzone) {
                        var selectedPickZone = Enumerable.From($scope.pickZones).Single("p => p.Alias == '" + item.pickzoneLabel + "'");
                        var restriction = { "$type": 'PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions.ConveyorPickzoneRestriction, PackNet.IODeviceService', "Value": selectedPickZone };
                        item.Producible.ItemsToProduce.forEach(function (order){
                                if(order.Producible.$type.contains('Carton')){
                                        order.Producible.Restrictions.push(restriction);
                                }
                        });
                        delete item.pickzoneLabel;
                    }

                    var articleProductionRequest = {
                        "$type" : "PackNet.Plugin.ArticleService.ArticleProductionRequest, PackNet.Plugin.ArticleService",
                        CustomerUniqueId : item.Alias,
                        Article: item,
                        Quantity : item.Quantity
                    }

                    if (item.productionGroupId)
                        articleProductionRequest["ProductionGroupId"] = item.productionGroupId;
                    else
                        articleProductionRequest["MachineGroupId"] = currentMachine.Id;

                    return articleProductionRequest;
                })
                .ToArray();

            $scope.readOnly = true;

            messagingService.publish("ArticleProduceNew", articleProductionRequests);

            // As we remove the selection we added it again when the production has been confirmed 
            if ($scope.pickZones.length == 1) {
                $scope.gridOptions.data.forEach(function (item) {
                    item.pickzoneLabel = $scope.pickZones[0].Alias;
                });
            }

            waitForResponse(translationService.translate("Article.Production.Message.ProduceTimeout"));
        };

        function waitForResponse(errorMessage) {
            awaitingResponse = true;

            $timeout(function () {
                if (awaitingResponse) {
                    awaitingResponse = false;
                    alertService.addError(errorMessage, 5000);
                }
            }, 5000);
        };

        //--- Edit
        $scope.cartonDesignChange = function (){
            delete $scope.selectedPackageDesign;
            $scope.selectedPackageDesign = helperService.linq.getItemById($scope.packagingDesigns, $scope.selectedArticle.Producible.DesignId);
        };

        $scope.closeArticle = function (){
            delete $scope.selectedArticle;
            $scope.selectedArticle = null;

            $state.go("^.list");
        };

        $scope.labelTemplateChange = function (){
            delete $scope.selectedTemplate;
            $scope.selectedTemplate = helperService.linq.getItemById($scope.templates, $scope.selectedArticle.Producible.Restrictions[0].Value.Id);
        };

        $scope.toggleDesignPreviewContainer = function (){
            $scope.showDesignPreviewContainer = !$scope.showDesignPreviewContainer;
        }

        $scope.$watch("selectedArticle", function () {
            delete $scope.showDesignPreviewContainer;
            if (!$scope.selectedArticle || $scope.selectedArticle.Producible.ProducibleType !== "Carton") {
                return;
            }

            var container = $("#previewContainer");

            if (container.length > 0) container[0].innerHTML = "";

            var producible = $scope.selectedArticle.Producible;

            if (!(parseFloat(producible.Length) >= 0 && parseFloat(producible.Width) >= 0 && parseFloat(producible.Height) >= 0)) {
                return;
            }
            var payload = {
                "OriginalCarton": {
                    ArticleId: producible.CustomerUniqueId,
                    DesignId: producible.DesignId,
                    Height: producible.Height,
                    Id: null,
                    Length: producible.Length,
                    Width: producible.Width,
                    XValues: {}
                },
                "Rotation": producible.Rotation ? producible.Rotation : Enumerable.From($scope.selectedPackageDesign.AllowedRotations).FirstOrDefault([], function (d) { return d.Value.Allowed === 1; }).Key
            }

            angular.forEach($scope.selectedPackageDesign.DesignParameters, function (designParameter) {
                payload.OriginalCarton.XValues[designParameter.Name] = producible.XValues[designParameter.Name] != undefined ? producible.XValues[designParameter.Name] : 0;
            });

            messagingService.publish("GetPackagingDesignWithAppliedValues", payload);
        }, true);

        $scope.$watch("showDesignPreviewContainer", function (newValue) {
            delete $scope.labelPackageDesignPreview;
            $scope.labelPackageDesignPreview = newValue ? translationService.translate("Article.CartonProducible.Label.HidePreview") : translationService.translate("Article.CartonProducible.Label.ShowPreview");
        });

        //Listeners
        messagingService.messageSubject.where(messagingService.filterObs("ArticleSearchResponseNew"))
            .select(messagingService.responseDataFunction).autoSubscribe(function (data) {

                data.Data.map(function (item) {
                    item.Quantity = 1;

                    if ($scope.pickZones.length == 1) {
                        item.pickzoneLabel = $scope.pickZones[0].Alias;
                    }
                });

                delete $scope.gridOptions.data;
                $scope.gridOptions.data = data.Data;
                alertService.closeByMessage(loadingMessage);
                if (!$scope.$$phase) {
                    $scope.$apply();
                }
            }, $scope, "Articles List");

        messagingService.articleNewProducedObservable.autoSubscribe(function (data) {
            if (awaitingResponse) {
                awaitingResponse = false;

                if (data.Result === "Fail") {
                    alertService.addError(translationService.translate("Article.Production.Message.ProduceFailed"));
                }
                else if (data.Result === "Success") {
                    alertService.addSuccess(translationService.translate("Article.Production.Message.ProduceSuccess"), 5000);
                }
            }
        }, $scope, "Article Updated");

        messagingService.packagingDesignWithAppliedValuesObservable.autoSubscribe(function (msg) {
            var data = msg.Data;
            angular.forEach(data.Lines, function (line, index) {
                if ($scope.selectedPackageDesign.Lines[index].Label != null) {
                    line.Label = $scope.selectedPackageDesign.Lines[index].Label;
                }
            });

            designPreviewService.draw.drawPreview(data, $("#previewContainer"), $("#restrictionsContainer").width(), 200);
        }, $scope, "Packaging Design Preview");

        packagingDesignService.allDesignsObservable.autoSubscribe(function () {
            delete $scope.packagingDesigns;
            $scope.packagingDesigns = packagingDesignService.getAllDesigns();
        }, $scope, 'Get Designs');

        var templatesObservable = messagingService.messageSubject
            .where(messagingService.filterObs("Templates"));

        templatesObservable.autoSubscribe(function (msg) {
            var data = msg.Data;
            delete $scope.templates;
            $scope.templates = Enumerable
                .From(data)
                .Select(function (item) {
                    item.Variables = helperService.regEx.getValuesBetweenCurlyBrackets(item.Content);;

                    return item;
                })
                .ToArray();
        }, $scope, "Templates");

        //IntroJS

        $scope.tellMeAboutThisPage = function () {
            var steps = [];

            if ($state.current.name === "OperatorConsole.CreateFromArticleNew.list") {
                var columnDefs = $("[ui-grid-header-cell]");

                steps.push({
                    element: "#notFound",
                    intro: translationService.translate("CreateFromArticle.Help.Overview")
                });

                steps.push({
                    element: "#produceArticle",
                    intro: translationService.translate("Article.Help.List.Produce")
                });

                steps.push({
                    element: '#articlesList',
                    intro: translationService.translate("Article.Help.List.Columns.Overview")
                });

                steps.push({
                    element: columnDefs[2],
                    intro: translationService.translate("CreateFromArticle.Help.List.Columns.Name")
                });

                steps.push({
                    element: columnDefs[3],
                    intro: translationService.translate("Article.Help.List.Columns.Description")
                });

                steps.push({
                    element: columnDefs[4],
                    intro: translationService.translate("Article.Production.Help.List.Columns.Quantity")
                });

                steps.push({
                    element: columnDefs[5],
                    intro: translationService.translate("Article.Help.List.Columns.Type")
                });
                if (showCrossConveyorPickzone) {
                    steps.push({
                        element: columnDefs[6],
                        intro: translationService.translate("Article.Help.List.Columns.PickZoneSelection") 
                    });
                }
            } else if ($state.current.name === "OperatorConsole.CreateFromArticleNew.carton") {
                steps.push({
                    element: "#previewAndToggleContainer",
                    intro: translationService.translate('Article.CartonProducible.Help.Form.Preview')
                });

                steps.push({
                    element: "#closeButton",
                    intro: translationService.translate("CreateFromArticle.Help.Form.Close")
                });
            } else if ($state.current.name === "OperatorConsole.CreateFromArticleNew.label") {
                steps.push({
                    element: "#closeButton",
                    intro: translationService.translate("CreateFromArticle.Help.Form.Close")
                });
            }
            
            return steps;
        };

        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);

        //Initialize
        $scope.utilities.initialDataRequest($scope, messagingService, requestData);
    }
]);