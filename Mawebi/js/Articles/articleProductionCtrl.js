app.controller("articleNewProductionCtrl", ["$scope", "$state", "$stateParams", "$timeout", "alertService", "articleStateService", "designPreviewService", "helperService", "introJsService", "machineGroupService", "messagingService", "packagingDesignService", "translationService",
    function ($scope, $state, $stateParams, $timeout, alertService, articleStateService, designPreviewService, helperService, introJsService, machineGroupService, messagingService, packagingDesignService, translationService) {
        $timeout = $timeout.getInstance($scope);

        //Locals
        var awaitingResponse = false;
        var corrugateRestrictionType = "PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce.MustProduceOnCorrugateWithPropertiesRestriction, PackNet.Common.Interfaces";

        //Store state parameters to avoid loosing the values when transitioning between the different views 
        var administratorView = $stateParams.administratorView;

        //ui-grid - v3.0.0-rc.21-efd3798's saveState functionality doesn't support expanded rows and the scroll position is not accurate
        var gridExpandedRows = [];
        var gridRestoreState = false;
        var gridScrollTopPosition = 0;
        var gridViewPortSelector = "#articlesList div[container-id='\\'body\\'']:first > .ui-grid-viewport";

        var templatesObservable = messagingService.messageSubject
            .where(messagingService.filterObs("Templates"));

        var requestData = function () {
            messagingService.publish("GetTemplates");
        }

        function isCartonInformationComplete(producible) {
            var result = true;

            result = parseFloat(producible.Length) >= 0 && parseFloat(producible.Width) >= 0 && parseFloat(producible.Height) >= 0;

            if (result) {
                for (var property in producible.XValues) {
                    if (producible.XValues.hasOwnProperty(property)) {
                        result = (producible.XValues[property] != null && parseFloat(producible.XValues[property]) >= 0);
                        if (!result) break;
                    }
                }
            }

            return result;
        }

        function isLabelInformationComplete(producible) {
            var result = true;

            for (var property in producible.PrintData) {
                if (property !== "$type" && producible.PrintData.hasOwnProperty(property)) {
                    result = (producible.PrintData[property] && producible.PrintData[property] !== "");
                    if (!result) break;
                }
            }

            return result;
        }

        function isArticleInformationComplete(producible) {
            var result = true;

            switch (producible.ProducibleType) {
                case "Carton":
                    result = isCartonInformationComplete(producible);
                    break;
                case "Label":
                    result = isLabelInformationComplete(producible);
                    break;
                case "Kit":
                    for (var i = 0; i < producible.ItemsToProduce.length; i++) {
                        result = isArticleInformationComplete(producible.ItemsToProduce[i].Producible);
                        if (!result) break;
                    }

                    break;
            }

            return result;
        }

        function isRowEditable(rowEntity) {
            return !$scope.readOnly;
        }

        function loadArticlesToProduce(){
            delete $scope.articlesToProduce;
            $scope.articlesToProduce = Enumerable.From(articleStateService.selectedArticles).Select(function (article) {
                return {
                    article: article,
                    order: null,
                    ordersCreated: null,
                    productionGroupId: null,
                    productionGroupLabel: "",
                    quantity: 1, //TODO: set value from grid/production form
                    isInformationComplete: function () {
                        var isComplete;

                        //Article Production Request Information
                        isComplete = (!administratorView || (administratorView && this.productionGroupId)) && (this.quantity && !isNaN(this.quantity) && parseFloat(this.quantity) > 0);

                        if (!isComplete) return false;

                        //Article Information
                        isComplete = isArticleInformationComplete(this.article.Producible);

                        if (!isComplete) return false;

                        return true;
                    },
                    status: function () {
                        var result;

                        if (this.ordersCreated) {
                            result = this.ordersCreated.length > 0 ? translationService.translate("Common.Label.Produced") : translationService.translate("Common.Label.Failed");
                        }
                        else {
                            result = this.isInformationComplete() ? translationService.translate("Common.Label.Ready") : translationService.translate("Common.Label.Incomplete");
                        }

                        return result;
                    }
                };
            }).ToArray();

            //When there's just one Production Group to choose from, assigns it as default
            if (administratorView && $scope.productionGroups.length === 1) {
                angular.forEach($scope.articlesToProduce, function (item) {
                    item.productionGroupId = $scope.productionGroups[0].Id;
                    item.productionGroupLabel = $scope.productionGroups[0].Alias;
                });
            }

            
            delete $scope.gridOptions.data;
            $scope.gridOptions.data = $scope.articlesToProduce;
        }

        function waitForResponse(errorMessage) {
            awaitingResponse = true;

            $timeout(function () {
                if (awaitingResponse) {
                    awaitingResponse = false;
                    alertService.addError(errorMessage, 5000);
                }
            }, 5000);
        }

        //Bindings
        $scope.articlesToProduce = [];
        $scope.packagingDesigns = [];
        $scope.productionGroups = Enumerable.From(machineGroupService.getProductionGroups()).Where("pg => pg.SelectionAlgorithm == 'Order'").ToArray();
        $scope.templates = [];
        $scope.pickZones = [];
        $scope.corrugateRestrictions = null;
        $scope.readOnly = false;
        $scope.selectedArticle = null;
        $scope.selectedPackageDesign = null;
        $scope.selectedTemplate = null;
        $scope.showDesignPreview = true;
        $scope.showDesignPreviewContainer = true;

        $scope.gridOptions = {
            enableGridMenu: true,
            columnDefs: [
                {
                    name: translationService.translate("Common.Label.Name"),
                    field: "article.Alias",
                    enableCellEdit: false
                },
                {
                    name: translationService.translate("Common.Label.Quantity"),
                    field: "quantity",
                    enableCellEdit: true,
                    cellEditableCondition: function ($scope) {
                        return isRowEditable($scope.row.entity);
                    },
                    type: "number",
                    width: "125"
                },
                {
                    name: translationService.translate("ProductionGroup.Label.NameSingular"),
                    field: "productionGroupLabel",
                    enableCellEdit: true,
                    cellEditableCondition: function ($scope) {
                        return isRowEditable($scope.row.entity);
                    },
                    editDropdownOptionsArray: $scope.productionGroups,
                    editableCellTemplate: "ui-grid/dropdownEditor",
                    editDropdownValueLabel: "Alias",
                    editDropdownIdLabel: "Alias", //Value that is going to be assigned to the cell, for displaying purposes, we need the Alias. The Id would be manually assigned on afterCellEdit.
                    visible: administratorView,
                    width: "300"
                },
                {
                    cellClass: function (grid, row, col, rowRenderIndex, colRenderIndex) {
                        //Couldn't find a better method to use, but it works xD

                        //Anything that is not a "Kit" should NOT be expandable
                        helperService.uiGrid.hideExpandableIcon("articlesList", rowRenderIndex, row.entity.article.Producible.ProducibleType !== "Kit");

                        return '';
                    },
                    name: translationService.translate("Common.Label.Status"),
                    field: "status()",
                    enableCellEdit: false,
                    width: "125"
                }
            ],
            enableCellEditOnFocus: true,
            enableColumnResizing: true,
            enableFiltering: false,
            enableRowSelection: true,
            enableSelectAll: true,
            enableSorting: true,
            expandableRowHeight: 150,
            expandableRowScope: {
                editArticle: function (rowEntity) {
                    return $scope.editArticle(rowEntity);
                }
            },
            expandableRowTemplate: "partials/Articles/Production/articlesListExpandableRow.html",
            saveScroll: true,
            showGridFooter: true,
            onRegisterApi: function (gridApi) {
                //Set gridApi on scope
                delete $scope.gridApi;
                $scope.gridApi = gridApi;

                // Restores grid state (if any)
                gridApi.core.on.renderingComplete($scope, function (grid) {
                    if (gridRestoreState) {
                        //toggleRowExpansion only works when the grid is visible on screen
                        $timeout(function () {
                            for (var i = 0; i < gridExpandedRows.length; i++) {
                                $scope.gridApi.expandable.toggleRowExpansion(gridExpandedRows[i]);
                            }

                            //toggleRowExpansion has a bit of delay when increasing the viewport's height
                            $timeout(function () {
                                $(gridViewPortSelector).scrollTop(gridScrollTopPosition);
                            }, 50);

                            gridRestoreState = false;
                        }, 50);
                    }
                });

                // Input validations
                gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    alertService.clearAll();

                    switch (colDef.field) {
                        case "order":
                            if (newValue) {
                                if (!helperService.regEx.isAlphanumeric(newValue)) {
                                    alertService.addError(translationService.translate("Article.Production.Message.OrderMustBeAlphanumeric", rowEntity.article.Alias));
                                    break;
                                }

                                //Validate uniqueness client side
                                if (Enumerable
                                    .From($scope.articlesToProduce)
                                    .Where(function (item) {
                                        return item.article.Id !== rowEntity.article.Id && item.order === newValue;
                                })
                                    .Any()) {
                                    rowEntity.order = "";
                                    alertService.addError(translationService.translate("Article.Production.Message.OrderMustBeUnique", newValue, rowEntity.article.Alias));
                                    break;
                                }
                            }

                            break;
                        case "productionGroupLabel":
                            //Manually assigning the Id to the rowEntity property. This should be "accurate" because Production Group names are unique.
                            if (newValue) {
                                rowEntity.productionGroupId = Enumerable
                                    .From(colDef.editDropdownOptionsArray)
                                    .Where(function (item) {
                                        return item.Alias === newValue;
                                    })
                                    .Select(function (item) {
                                        return item.Id;
                                    })
                                    .FirstOrDefault();

                                if ($scope.showCrossConveyorPickzone) {
                                    var pgCapabilities = Enumerable.From(machineGroupService.getMachineGroups()).Where("mg => mg.ProductionGroup != undefined && mg.ProductionGroup.Alias == '" + newValue + "'").SelectMany("mg => mg.AllMachineCapabilities");
                                    if (pgCapabilities.Any(function (c) { return c.$type == conveyorPiczoneCapability; })) {
                                        rowEntity.pickzoneOptions = pgCapabilities.Where(function (c) { return c.$type == conveyorPiczoneCapability; }).Select('c=>c.PickZone').Distinct().ToArray();
                                    }
                                    else {
                                        rowEntity.pickzoneOptions = [];
                                    }
                                }
                            }
                            else {
                                rowEntity.productionGroupId = null;
                                rowEntity.pickzoneOptions = [];
                            }

                            break;
                        case "quantity":
                            if (!newValue || isNaN(newValue) || parseFloat(newValue) <= 0) {
                                alertService.addError(translationService.translate("Article.Production.Message.QuantityMustBeGreaterThanZero", rowEntity.article.Alias));
                            }

                            break;
                    }
                });

                gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                    //Store the rows that are expanded (the ui-grid saveState functionality doesn't store this information)
                    if (row.isExpanded) {
                        if (gridExpandedRows.indexOf(row.entity) === -1) gridExpandedRows.push(row.entity);
                    }
                    else {
                        gridExpandedRows.pop(row.entity);
                    }

                    //Builds sub-grid information when expanding the row
                    if (row.isExpanded && row.entity.article.Producible.ProducibleType === "Kit" && !row.entity.subGridOptions) {
                        angular.forEach(row.entity.article.Producible.ItemsToProduce, function (item) {
                            item.status = function () {
                                return isArticleInformationComplete(item.Producible) ? translationService.translate("Common.Label.Ready") : translationService.translate("Common.Label.Incomplete");
                            }
                        });

                        row.entity.subGridOptions = {
                            enableColumnResizing: true,
                            columnDefs: [
                                { name: translationService.translate("Common.Label.Name"), field: "Producible", cellTemplate: "partials/Articles/Production/CellTemplates/alias.html" },
                                { name: translationService.translate("Common.Label.Quantity"), field: "OriginalQuantity", width: "125" },
                                { name: translationService.translate("Common.Label.Status"), field: "status()", width: "125" }
                            ],
                            data: row.entity.article.Producible.ItemsToProduce
                        };
                    }
                });
            }
        };

        // We need to check all machine groups associated with the Orders PGs to see if we have there a Machine with a X Conveyor
        var conveyorPiczoneCapability = "PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities.ConveyorPickzoneCapability, PackNet.IODeviceService";
        $scope.showCrossConveyorPickzone = false;
        var allCapabilities = Enumerable.From(machineGroupService.getMachineGroups()).Where("mg => mg.ProductionGroup != undefined && mg.ProductionGroup.SelectionAlgorithm == 'Order'").SelectMany("mg => mg.AllMachineCapabilities"); 
        if (allCapabilities.Any(function (c) { return c.$type == conveyorPiczoneCapability; })) {
            $scope.showCrossConveyorPickzone = true;
            delete $scope.pickZones;
            $scope.pickZones = allCapabilities.Where(function (c) { return c.$type == conveyorPiczoneCapability; }).Select('c=>c.PickZone').Distinct().ToArray();

            $scope.gridOptions.columnDefs.splice(3, 0, {
                name: translationService.translate("Article.Production.Label.PickZoneSelection"),
                field : "pickzoneLabel",
                enableCellEdit : true,
                enableFiltering : false,
                editDropdownRowEntityOptionsArrayPath: 'pickzoneOptions',
                editableCellTemplate : "ui-grid/dropdownEditor",
                editDropdownValueLabel : "Alias",
                editDropdownIdLabel : "Alias", //Value that is going to be assigned to the cell, for displaying purposes, we need the Alias. The Id would be manually assigned on afterCellEdit.
            });
        }

        $scope.canProduce = function () {
            var result = false;

            if ($scope.articlesToProduce.length > 0) {
                result = Enumerable
                    .From($scope.articlesToProduce)
                    .Where(function (item) {
                        return !item.ordersCreated && item.isInformationComplete();
                    })
                    .Count() === $scope.articlesToProduce.length;
            }

            return result;
        }

        $scope.canRemove = function () {
            return $scope.gridApi.selection.getSelectedRows().length > 0;
        }

        $scope.editArticle = function (rowEntity) {
            //Saves the grid's viewport scroll position
            gridScrollTopPosition = $(gridViewPortSelector).scrollTop();
            gridRestoreState = true;

            //Assigning the selected article depending on the grid or sub-grid structure
            delete $scope.selectedArticle;
            $scope.selectedArticle = (rowEntity.article) ? rowEntity.article.Producible : rowEntity;

            switch ($scope.selectedArticle.Producible.ProducibleType) {
                case "Carton":
                    //Points the Order Restrictions so that any change on the Carton Restrictions gets reflected
                    delete $scope.selectedArticle.Restrictions;
                    $scope.selectedArticle.Restrictions = $scope.selectedArticle.Producible.Restrictions;

                    delete $scope.corrugateRestrictions;
                    $scope.corrugateRestrictions = Enumerable.From($scope.selectedArticle.Producible.Restrictions).Where("r => r.$type.contains('" + corrugateRestrictionType + "')").FirstOrDefault();
                    $scope.cartonDesignChange(false);
                    $state.go("^.carton");
                    break;
                case "Label":
                    $scope.labelTemplateChange();

                    $state.go("^.label");
                    break;
            }
        }

        $scope.produceArticles = function () {
            var currentMachine = messagingService.currentMachine();

            var articleProductionRequests = Enumerable
                .From($scope.articlesToProduce)
                .Select(function (item) {
                    if ($scope.showCrossConveyorPickzone && item.pickzoneLabel != undefined) {
                        var selectedPickZone = Enumerable.From($scope.pickZones).Single("p => p.Alias == '" + item.pickzoneLabel + "'");
                        var restriction = { "$type": 'PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions.ConveyorPickzoneRestriction, PackNet.IODeviceService', "Value": selectedPickZone };
                        item.article.Producible.ItemsToProduce.forEach(function (order) {
                            if (order.Producible.$type.contains('Carton')) {
                                order.Producible.Restrictions.push(restriction);
                            }
                        });
                        delete item.pickzoneLabel;
                    }
                    delete item.pickzoneOptions;
                    
                    var articleProductionRequest = {
                        "$type" : "PackNet.Plugin.ArticleService.ArticleProductionRequest, PackNet.Plugin.ArticleService",
                        CustomerUniqueId : item.article.Alias,
                        Article: item.article,
                        Quantity: item.quantity
                    }

                    if (item.productionGroupId)
                        articleProductionRequest["ProductionGroupId"] = item.productionGroupId;
                    else
                        articleProductionRequest["MachineGroupId"] = currentMachine.Id;

                    return articleProductionRequest;
                })
                .ToArray();

            $scope.readOnly = true;

            messagingService.publish("ArticleProduceNew", articleProductionRequests);
            waitForResponse(translationService.translate("Article.Production.Message.ProduceTimeout"));
            $timeout(function () {
                $state.go('ArticlesNew.list', { administratorView: true });
            }, 3000);
        }

        $scope.removeArticles = function () {
            angular.forEach($scope.gridApi.selection.getSelectedRows(), function (data, index) {
                $scope.gridOptions.data.splice($scope.gridOptions.data.lastIndexOf(data), 1);
            });

            $scope.gridApi.selection.clearSelectedRows();

            if ($scope.gridOptions.data.length === 0) {
                $state.go('ArticlesNew.list', { administratorView: true });
            }
        }

        //--- Edit

        $scope.closeArticle = function () {
            
            delete $scope.selectedArticle;
            $scope.selectedArticle = null;

            $state.go("^.list");
        }

        $scope.cartonDesignChange = function (clearXValues) {
            if (clearXValues)
                
                delete $scope.selectedArticle.Producible.XValues;
                $scope.selectedArticle.Producible.XValues = {};

            delete $scope.selectedPackageDesign;
            $scope.selectedPackageDesign = helperService.linq.getItemById($scope.packagingDesigns, $scope.selectedArticle.Producible.DesignId);
        }

        $scope.labelTemplateChange = function (){
            delete $scope.selectedTemplate;
            $scope.selectedTemplate = helperService.linq.getItemById($scope.templates, $scope.selectedArticle.Producible.Restrictions[0].Value.Id);
        }

        $scope.toggleDesignPreviewContainer = function () {
            $scope.showDesignPreviewContainer = !$scope.showDesignPreviewContainer;
        }

        $scope.$watch("selectedArticle", function () {
            if (!$scope.selectedArticle || $scope.selectedArticle.Producible.ProducibleType !== "Carton") {
                return;
            }

            var container = $("#previewContainer");

            if (container.length > 0) container[0].innerHTML = "";

            var producible = $scope.selectedArticle.Producible;

            if (!(parseFloat(producible.Length) >= 0 && parseFloat(producible.Width) >= 0 && parseFloat(producible.Height) >= 0)) {
                return;
            }

            var payload = {
                "OriginalCarton": {
                    ArticleId: producible.CustomerUniqueId,
                    DesignId: producible.DesignId,
                    Height: producible.Height,
                    Id: null,
                    Length: producible.Length,
                    Width: producible.Width,
                    XValues: {}
                },
                "Rotation": producible.Rotation ? producible.Rotation: Enumerable.From($scope.selectedPackageDesign.AllowedRotations).FirstOrDefault([], function (d) { return d.Value.Allowed === 1; }).Key
                }

            //TODO: Clarify, XValues are stored using the "Alias", however the method to draw the preview is expecting the "Name"
            angular.forEach($scope.selectedPackageDesign.DesignParameters, function (designParameter) {
                payload.OriginalCarton.XValues[designParameter.Name] = producible.XValues[designParameter.Alias] != undefined ? producible.XValues[designParameter.Alias] : null;
            });

            messagingService.publish("GetPackagingDesignWithAppliedValues", payload);
        }, true);

        $scope.$watch("showDesignPreviewContainer", function (newValue) {
            delete $scope.labelPackageDesignPreview;
            $scope.labelPackageDesignPreview = newValue ? translationService.translate("Article.CartonProducible.Label.HidePreview") : translationService.translate("Article.CartonProducible.Label.ShowPreview");
        });

        //Listeners
        messagingService.articleNewProducedObservable.autoSubscribe(function (data) {
            if (awaitingResponse) {
                awaitingResponse = false;

                if (data.Result === "Fail") {
                    alertService.addError(translationService.translate("Article.Production.Message.ProduceFailed"));
                }
                else if (data.Result === "Success") {
                    angular.forEach($scope.articlesToProduce, function (articleToProduce, index) {
                        articleToProduce.ordersCreated = data.Data[articleToProduce.order];
                    });

                    alertService.addSuccess(translationService.translate("Article.Production.Message.ProduceSuccess"), 5000);
                }
            }
        }, $scope, "Article Updated");

        messagingService.packagingDesignWithAppliedValuesObservable.autoSubscribe(function (msg) {
            var data = msg.Data;
            angular.forEach(data.Lines, function (line, index) {
                if ($scope.selectedPackageDesign.Lines[index].Label != null) {
                    line.Label = $scope.selectedPackageDesign.Lines[index].Label;
                }
            });

            designPreviewService.draw.drawPreview(data, $("#previewContainer"), $("#restrictionsContainer").width(), 200);
        }, $scope, "Packaging Design Preview");

        $scope.packagingDesigns = packagingDesignService.getAllDesigns();
        packagingDesignService.allDesignsObservable.autoSubscribe(function () {
            delete $scope.packagingDesigns;
            $scope.packagingDesigns = packagingDesignService.getAllDesigns();
        }, $scope, 'Get Designs');

        templatesObservable.autoSubscribe(function (msg) {
            var data = msg.Data;
            delete $scope.templates;
            $scope.templates = Enumerable
                .From(data)
                .Select(function (item) {
                    item.Variables = helperService.regEx.getValuesBetweenCurlyBrackets(item.Content);;

                    return item;
                })
                .ToArray();
        }, $scope, "Templates");

        //IntroJS
        $scope.tellMeAboutThisPage = function () {
            var steps = [];

            if ($state.current.name === "ArticlesProductionNew.list") {
                var columnDefs = $("[ui-grid-header-cell]");

                steps.push({
                    element: "#notFound",
                    intro: translationService.translate("Article.Production.Help.List.Overview")
                });

                steps.push({
                    element: "#removeArticles",
                    intro: translationService.translate("Article.Help.List.Delete")
                });

                steps.push({
                    element: "#produceArticles",
                    intro: translationService.translate("Article.Production.Help.List.Produce")
                });

                steps.push({
                    element: '#articlesList',
                    intro: translationService.translate("Article.Production.Help.List.Columns.Overview")
                });

                steps.push({
                    element: columnDefs[2],
                    intro: translationService.translate("Article.Production.Help.List.Columns.Name")
                });

                steps.push({
                    element: columnDefs[3],
                    intro: translationService.translate("Article.Production.Help.List.Columns.Quantity")
                });

                if (administratorView) {
                    steps.push({
                        element: columnDefs[4],
                        intro: translationService.translate("Article.Production.Help.List.Columns.ProductionGroup")
                    });
                }

                if (administratorView && $scope.showCrossConveyorPickzone) {
                    steps.push({
                        element: columnDefs[5],
                        intro: translationService.translate("Article.Production.Help.List.Columns.PickZoneSelection")
                    });
                }

                steps.push({
                    element: columnDefs[administratorView ? ($scope.showCrossConveyorPickzone ? 6 : 5) : 4],
                    intro: translationService.translate("Article.Production.Help.List.Columns.Status")
                });
            }
            else if ($state.current.name === "ArticlesProductionNew.carton") {
                steps.push({
                    element: "#design",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Design")
                });

                steps.push({
                    element: "#length",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Length")
                });

                steps.push({
                    element: "#width",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Width")
                });
                steps.push({
                    element: "#height",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Height")
                });

                steps.push({
                    element: "#corrugateQuality",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.CorrugateQuality")
                });

                steps.push({
                    element: "#corrugateWidth",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.CorrugateWidth")
                });

                steps.push({
                    element: "#corrugateThickness",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.CorrugateThickness")
                });

                steps.push({
                    element: "#rotation",
                    intro: translationService.translate("Article.CartonProducible.Help.Form.Rotation")
                });

                steps.push({
                    element: "#previewAndToggleContainer",
                    intro: translationService.translate('Article.CartonProducible.Help.Form.Preview')
                });

                steps.push({
                    element: "#closeButton",
                    intro: translationService.translate("Article.Production.Help.Form.Close")
                });
            }
            else if ($state.current.name === "ArticlesProductionNew.label") {
                steps.push({
                    element: "#closeButton",
                    intro: translationService.translate("Article.Production.Help.Form.Close")
                });
            }

            return steps;
        };

        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);

        //Initialize
        loadArticlesToProduce();

        $scope.utilities.initialDataRequest($scope, messagingService, requestData);
    }
]);