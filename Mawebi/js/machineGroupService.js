function machineGroupService(messagingService, $rootScope, $location, AUTH_EVENTS) {
    var currentMachineGroupId = 0;
    if ($location.search().machineGroupId != undefined) {
        currentMachineGroupId = $location.search().machineGroupId;
    }
    var currentMachineGroup = currentMachineGroup || null;
    var machineGroups = null;
    var machines = null;
    var productionGroups = null;

    var currentMachineGroupSubject = new Rx.Subject();
    var allMachineGroupsSubject = new Rx.Subject();
    currentMachineGroupSubject.onNext(currentMachineGroup);

    // TODO: Figure out why this is happening before login.
    var fetchData = function () {
        messagingService.publish("GetMachineGroups", "");
        messagingService.publish("GetProductionGroups", "");
        messagingService.publish("GetMachines", "");
    };

    function clearCurrentMachineGroup() {
        if (currentMachineGroup) {
            currentMachineGroup.AssignedOperator = null;

            messagingService.publish("ClearAssignedOperator");
        }

        currentMachineGroupId = 0;
        currentMachineGroup = null;

        messagingService.setCurrentMachineGroup(currentMachineGroup);
        currentMachineGroupSubject.onNext(currentMachineGroup);
    }

    function start (){
        // $broadcast listeners
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function () {
            clearCurrentMachineGroup();
        });
    }

    messagingService.machineGroupCreatedObservable.autoSubscribe(function (msg){
        fetchData();
    }, 
    $rootScope, "Machine Service - MachineGroups");
    
    messagingService.machineGroupUpdatedObservable.autoSubscribe(function (msg){
        fetchData();
    }, 
    $rootScope, "Machine Service - MachineGroups");

    messagingService.machineGroupDeletedObservable.autoSubscribe(function (msg){
        fetchData();
    }, 
    $rootScope, "Machine Service - MachineGroups");

    messagingService.machineGroupsObservable.autoSubscribe(function (msg) {
        if (machineGroups != null) {
            //Keep Production group if it has been assigned and doesn't exist on the new data
            var existingMachineGroups = Enumerable.From(machineGroups);
            machineGroups = Enumerable.From(msg.Data).Select(function (mg) {
                mg.ProductionGroup = Enumerable.From(existingMachineGroups)
                    .Where(function (existingGroup) {
                         return existingGroup.Id == mg.Id && !mg.ProductionGroup;
                    })
                    .Select("mg=>mg.ProductionGroup")
                    .FirstOrDefault(null);
                return mg;
            }).ToArray();
        }
        else {
            machineGroups = Enumerable.From(msg.Data).ToArray();

            if (productionGroups != null) {
                Enumerable.From(machineGroups).ForEach(function (machineGroup) {
                    machineGroup.ProductionGroup = Enumerable
                        .From(productionGroups)
                        .Where(function (productionGroup) {
                            return productionGroup.ConfiguredMachineGroups.indexOf(machineGroup.Id) > -1;
                        })
                        .Select().FirstOrDefault();
                });
            }
        }

        updateCurrentMachineGroup(currentMachineGroupId);
        allMachineGroupsSubject.onNext(machineGroups);
    }, $rootScope, "Machine Service - MachineGroups");

    messagingService.productionGroupsObservable.autoSubscribe(function (msg) {
        productionGroups = msg.Data;

        // Refresh machine groups previous associations
        Enumerable.From(machineGroups).ForEach(function (machineGroup) {
            machineGroup.ProductionGroup = Enumerable
                .From(productionGroups)
                .Where(function (productionGroup) {
                    return productionGroup.ConfiguredMachineGroups.indexOf(machineGroup.Id) > -1;
                })
                .Select().FirstOrDefault();
        });

        var configuredSelectionAlgorithms = Enumerable.From(msg.Data).Select(function (pg) { return pg.SelectionAlgorithm; }).Distinct().ToArray();

        $rootScope.IsBoxLastConfigured = Enumerable.From(configuredSelectionAlgorithms).Contains('BoxLast');
        $rootScope.IsOrderConfigured = configuredSelectionAlgorithms.length > 0; 
        $rootScope.IsBoxFirstConfigured = Enumerable.From(configuredSelectionAlgorithms).Contains('BoxFirst');
        $rootScope.IsScanToCreateConfigured = Enumerable.From(configuredSelectionAlgorithms).Contains('ScanToCreate');
        $rootScope.IsSearchAvailable = configuredSelectionAlgorithms.length > 0;
    }, $rootScope, 'Machine Service - ProductionGroups');

    messagingService.machinesObservable.autoSubscribe(function (msg) {
        machines = msg.Data;
        Enumerable.From(machineGroups).ForEach(function (mg) {
            mg.Machines = Enumerable.From(machines).Where(function (machine) {
                return Enumerable.From(mg.ConfiguredMachines).Contains(machine.Id);
            }).ToArray();
            updateCurrentMachineGroup(currentMachineGroupId);
        });
    }, $rootScope, 'Machines');

    messagingService.signalRConnectedObservable.autoSubscribe(function () {
        fetchData();

        // Clear Machine Group on reconnect in order to avoid having jobs without an assigned operator.
        clearCurrentMachineGroup();
    }, $rootScope, "Machine Group Service on connected");

    function updateCurrentMachineGroup(newId, currentUser) {
        var newMachineGroup = findMachineGroup(newId);
        if (newMachineGroup) {
            
            currentMachineGroupId = newId;
            currentMachineGroup = newMachineGroup;
            messagingService.setCurrentMachineGroup(currentMachineGroup);
            if (currentUser) {
                //Message will include machine group info and user info
                messagingService.publish("SetAssignedOperator");
                currentMachineGroup.AssignedOperator = currentUser.UserName;
            }
            currentMachineGroup.Machines = Enumerable.From(machines).Where(function (machine) {
                return Enumerable.From(currentMachineGroup.ConfiguredMachines).Contains(machine.Id);
            }).ToArray();

            currentMachineGroupSubject.onNext(currentMachineGroup);
        }
    }

    function findMachineGroup(machineGroupId) {
        return Enumerable.From(machineGroups).FirstOrDefault(null, function (machineGroup) {
            return machineGroup.Id == machineGroupId;
        });
    }

    var currentMachineGroupObservable = Rx.Observable.create(function (observer) {
        var disposable = currentMachineGroupSubject.subscribe(function (currentMachine) {
            observer.onNext(currentMachine);
        });

        return disposable;
    });

    var allMachineGroupsObservable = Rx.Observable.create(function (observer) {
        var disposable = allMachineGroupsSubject.subscribe(function (allMachineGroups) {
            observer.onNext(allMachineGroups);
        });

        return disposable;
    });

    function hasCapabilityWithId(corrugateId){
        var result = false;

        if (!currentMachineGroup)
            return true;

        var corrugatesOnMachineIdCapas = Enumerable.From(currentMachineGroup.AllMachineCapabilities).Where(function (c) {
            return c.RestrictionType.indexOf("CorrugatesOnMachineId") > -1;
        }).ToArray();

        if (corrugatesOnMachineIdCapas)
             result = Enumerable.From(corrugatesOnMachineIdCapas).Any(function (cap) {
                 return Enumerable.From(cap.Value.Corrugates).Any(function (corr) {
                     return corr.Id === corrugateId;
                 });
             });

        return result;
    }

    function hasMachineType(machineType){
        if (!currentMachineGroup) return false;

        if (machineType == "") return true;

        var result = Enumerable.From(currentMachineGroup.Machines).FirstOrDefault(false, function (m) {
            return m.MachineType == machineType;
        });
        return result;
    }

    return {
        getCurrentMachineGroup: function (allowedToBypassAssignedOperator) {

            if (currentMachineGroup != null && $rootScope.currentUser != null && (currentMachineGroup.AssignedOperator == $rootScope.currentUser.UserName || ($rootScope.currentUser.IsAdmin === 1 && allowedToBypassAssignedOperator === true) || ($rootScope.currentUser.IsAdmin === 1 && allowedToBypassAssignedOperator == undefined))) {
                return currentMachineGroup;
            }

            return null;
        },
        getMachineGroups: function () {
            if (machineGroups) {
                return machineGroups.slice();
            }
            return [];
        },
        getMachines: function (){
            return machines;
        },
        getProductionGroups: function (){ //TODO: production group service
            return productionGroups;
        },
        updateCurrentMachineGroup: updateCurrentMachineGroup,
        hasCapabilityWithId: hasCapabilityWithId,
        hasMachineType: hasMachineType,
        currentMachineGroupUpdatedObservable: currentMachineGroupObservable,
        machineGroupsUpdatedObservable: allMachineGroupsObservable,
        start: start
    };
}
app.factory('machineGroupService', machineGroupService);
