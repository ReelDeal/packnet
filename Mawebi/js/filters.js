﻿app.filter('datemedium', function ($filter) {
    return function (input, entity) {
        return $filter('date')(input, 'medium');
    };
});

app.filter('designNameFromId', function (packagingDesignService) {
    return function (input, attribute) {
        var design = packagingDesignService.getDesign(input);
        if (design) {
            return design.Name;
        }
        return "Unknown Design Id: " + input;
    }
});

app.filter("format", function () {
    return function (input) {
        var args = arguments;
        return input.replace(/\{(\d+)\}/g, function (match, capture) {
            return args[1 * capture + 1];
        });
    };
});

app.filter('machineGroupProductionMode', ['translationService', function (translationService) {
    return function (input) {
        return translationService.translate('MachineGroupProductionMode.Label.' + input);
    };
}]);

app.filter('machineGroupStatus', ['translationService', function (translationService) {
    return function (input) {
        return translationService.translate('MachineGroupStatus.Label.' + input);
    };
}]);

app.filter('machineProductionStatus', ['translationService', function (translationService) {
    return function (input) {
        var result;

        if (input === null) {
            result = '';
        }
        else {
            result = translationService.translate('MachineProductionStatus.Label.' + input);
        }

        return result;
    };
}]);

app.filter('machineStatus', ['translationService', function (translationService) {
    return function (input){
        var result;

        if (input === null) {
            result = '';
        }
        else {
            result = translationService.translate('MachineStatus.Label.' + input);
        }

        return result;
    };
}]);

app.filter('machineType', ['translationService', function (translationService) {
    return function (input) {
        var result;

        switch (input) {
            case 'Em':
                result = translationService.translate('Machine.Em.Label.NameSingular');
                break;
            case 'ExternalCustomerSystem':
                result = translationService.translate('Machine.ExternalSystem.Label.NameSingular');
                break;
            case 'Fusion':
                result = translationService.translate('Machine.Fusion.Label.NameSingular');
                break;
            case 'WifiBarcodeScanner':
                result = translationService.translate('Machine.NetworkScanner.Label.NameSingular');
                break;
            case 'ZebraPrinter':
                result = translationService.translate('Machine.ZebraPrinter.Label.NameSingular');
                break;
            default:
                result = translationService.translate('Machine.Label.NameSingular');
        }

        return result;
    };
}]);

app.filter('orderByPropertyTranslated', ['$filter', 'translationService', function ($filter, translationService) {
    return function (array, property) {
        return Enumerable.From(array).OrderBy(function (item) {
            return translationService.translate(item[property]);
        }).ToArray();
    };
}]);

app.filter('orderObjectBy', function () {
    return function (input, attribute) {
        if (!angular.isObject(input)) return input;

        var array = [];
        for (var objectKey in input) {
            array.push(input[objectKey]);
        }

        array.sort(function (a, b) {
            a = parseInt(a[attribute]);
            b = parseInt(b[attribute]);
            return a - b;
        });
        return array;
    };
});

app.filter('producibleStatus', ['translationService', function (translationService) {
    return function (input) {
        return translationService.translate('ProducibleStatus.Label.' + input);
    };
}]);

app.filter('producibleType', ['translationService', function (translationService) {
    return function (input){
        return translationService.translate('ProducibleType.Label.' + input);
    };
}]);

app.filter('rotation', ['translationService', function (translationService) {
    return function (input) {
        var result;

        if (input === null) {
            result = translationService.translate('Rotation.Label.Optimal');
        }
        else {
            result = translationService.translate('Rotation.Label.' + input);
        }

        return result;
    };
}]);