var currentScope;

function InitKeyboardHandler(scope)
{
	document.onkeypress = KeyboardHandler;
	currentScope = scope;
}	
	
function KeyboardHandler(e)
{	
	if(e.which == 13)
	{
		var currentElement = document.activeElement;
		if(currentElement.id == 'historySearchText')
		{
			var searchbutton =	document.getElementById('historySearchButton');
			searchbutton.click();
			
			document.getElementById('reproduceRequestsButton').focus();
		}	
		else if(currentElement.id == 'prodSearchText')
		{
			var prodsearchbutton = document.getElementById('prodSearchButton');
			prodsearchbutton.click();
			
			document.getElementById('reproduceRequestsButton').focus();
		}
		//else if(currentElement.id == 'AdminSearchText')
		//{
		//	currentElement.blur();
		//	var adminsearchbutton = document.getElementById('AdminSearchButton');
		//	adminsearchbutton.click(true);
			
		//	document.getElementById('reproduceRequestsButton').focus();
		//}
		else if (currentScope.history.length > 0 || currentElement.id == 'reproduceRequestsButton')
		{
			var reproducebutton = document.getElementById('reproduceRequestsButton');
			reproducebutton.click();
		}
	}

}

