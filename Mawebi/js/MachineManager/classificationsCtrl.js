function ClassificationsCtrl(translationService, $scope, $log, $timeout, messagingService, uiGridConstants) {
    $timeout = $timeout.getInstance($scope);
    $scope.$log = $log;

    messagingService.classificationsObservable
        .autoSubscribe(function (msg) {
            $timeout(function () {
                
                delete $scope.classifications;
                $scope.classifications = msg.Data;
            }, 0, true);
        }, $scope, 'Classifications widget');

    var requestData = function () {
        messagingService.publish("GetClassifications", "");
    };

    if (messagingService.IsSignalRConnected()) {
        requestData();
    }

    messagingService.signalRConnectedObservable.autoSubscribe(requestData, $scope, 'SignalR Connected');

    $scope.updateStatus = function (state, classification) {
        messagingService.publish("UpdateClassificationStatus", {
            Id: classification.Id,
            Status: state,
            IsPriorityLabel: classification.IsPriorityLabel,
        });
    }

    $scope.updateLabelPriority = function (isPriorityLabel, classification) {
        messagingService.publish("UpdateClassificationStatus", {
            Id: classification.Id,
            Status: classification.Status,
            IsPriorityLabel: isPriorityLabel == 0 ? 1 : 0,
        });
    }

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'classifications',
        headerRowHeight: 26,
        rowHeight: 26,
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                //Sort on the name by default
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 0
                },
                minWidth: 150
            },
            {
                field: 'NumberOfRequests',
                name: translationService.translate('Common.Label.Requests'),
                width: '15%',
                cellClass: 'center-text',
                cellTemplate: '<label id="classifications-NumberOfRequests-{{row.entity.Alias}}" ng-bind="row.entity.NumberOfRequests"></label>',
                type: 'number'
            },
            {
                field: 'PriorityLabel',
                name: translationService.translate('Common.Label.Priority'),
                width: '15%',
                cellClass: 'center-text',
                cellTemplate: '<input type="checkbox" id="classifications-priority-checkbox-{{row.entity.Alias}}" ng-checked="row.entity.IsPriorityLabel" ng-click="$parent.grid.api.updateLabelPriority(row.entity.IsPriorityLabel, row.entity)" title="Set as Priority Label">'
            },
            {
                field: 'Status',
                name: translationService.translate('Common.Label.Status'),
                cellTemplate: 'partials/MachineManager/CellTemplates/ClassificationStatus.html',
                cellClass: 'ui-grid-cell-contents',
                width: 400

            }
        ],
        onRegisterApi: function (gridApi) {
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;
            gridApi.updateStatus = $scope.updateStatus;
            gridApi.updateLabelPriority = $scope.updateLabelPriority;
        }
    };
};
