function PickZonesWidgetCtrl(translationService, $scope, $log, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.$log = $log;
    
    $scope.pickzones = [];

    messagingService.pickZonesObservable
        .autoSubscribe(function (msg) {
            $timeout(function () {
                
                delete $scope.pickzones;
                $scope.pickzones = msg.Data;
            }, 0, true);
        }, $scope, 'Pick Zones');

    var requestData = function () {
        messagingService.publish("GetPickZones", "");
    };

    if (messagingService.IsSignalRConnected()) {
        requestData();
    }

    messagingService.signalRConnectedObservable.autoSubscribe(requestData, $scope, 'SignalR Connected');

    $scope.updateStatus = function (state, pickzone) {
        pickzone.Status = state;
        messagingService.publish("UpdatePickZoneStatus", pickzone);
    };

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'pickzones',
        headerRowHeight: 26,
        rowHeight: 26,
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                width: '15%',
                sort: {
                    direction: 'asc',
                    priority: 0
                }
            },
            {
                field: 'Description',
                name: translationService.translate('Common.Label.Description'),
                width: '15%'
            },
            {
                field: 'NumberOfRequests',
                name: translationService.translate('Common.Label.Requests'),
                width: '15%',
                type: 'number'
            },
            {
                field: 'Status',
                name: translationService.translate('Common.Label.Status'),
                cellTemplate: 'partials/MachineManager/CellTemplates/PickzoneStatus.html',
                cellClass: 'ui-grid-cell-contents'
            }
        ],
        onRegisterApi: function (gridApi) {
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;
            gridApi.updateStatus = $scope.updateStatus;
        }
    };
}
