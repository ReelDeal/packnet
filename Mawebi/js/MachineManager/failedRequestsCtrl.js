function FailedRequestsCtrl($scope, $log, messagingService) {
    $scope.$log = $log;
    
    $scope.failedRequests = [];
    messagingService.failedRequestsObservable.autoSubscribe(function (msg) {
        
        delete $scope.failedRequests;
        $scope.failedRequests = msg.Data;
    },  $scope, 'Failed Requests');

    var requestData = function () {
        messagingService.publish("GetFailedProducibles", "");
    };

    if (messagingService.IsSignalRConnected()) {
        requestData();
    }

    messagingService.signalRConnectedObservable.autoSubscribe(requestData, $scope, 'SignalR Connected');

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'failedRequests',
        headerRowHeight: 26,
        rowHeight: 26,
        columnDefs: [
            //TODO.. popover or another way to view messages http://plnkr.co/edit/RfGt0C3aRB0WWePqEKPe?p=preview
            { field: 'SerialNumber', width: 75, displayName: 'Dashboard.Widgets.General.Top20InvalidRequests.Headers.SerialNumber', headerCellFilter: 'translate' },
            //{ field: 'Name', displayName: 'Name', width: 75 },

            { field: 'ValidationMessage', width: '*', displayName: 'Dashboard.Widgets.General.Top20InvalidRequests.Headers.ValidationMessage', headerCellFilter: 'translate' }
            //{ field: 'Length', displayName: 'L', width: 45 },
            //{ field: 'Width', displayName: 'W', width: 45 },
            //{ field: 'Height', displayName: 'H', width: 45 },
            //{ field: 'ClassificationNumber', displayName: 'Class', width: 25 }
        ]
    };
}
