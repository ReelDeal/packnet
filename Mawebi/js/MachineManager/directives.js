app.directive('designParametersForm', function () {
    return {
        restrict: 'E',
        scope: {
            designparameters: '=',
            pattern: '=',
            tabbingIndex: '=',
            sizeClass: '='
        },
        templateUrl: 'partials/designParametersForm.html'
    }
});