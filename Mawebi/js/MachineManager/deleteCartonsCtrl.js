
function deleteCartonsCtrl($filter, translationService, $scope, $timeout, messagingService, dateValidationService) {
    $timeout = $timeout.getInstance($scope);
    $scope.isDateRangeInvalid = false;

    var beginDate = new Date();
    beginDate.setDate(beginDate.getDate() - 7);

    var endDate = new Date();
    endDate.setDate(endDate.getDate() - 2);

    $scope.beginDate = beginDate;
    $scope.endDate = endDate;

    var cartonRemovedCountObservable = messagingService.messageSubject
        .where(messagingService.filterObs("CartonRemovedCount"));

    $scope.isDisabled = function () {
        return angular.isUndefined($scope.beginDate) || angular.isUndefined($scope.endDate) || $scope.beginDate === null || $scope.endDate === null;
    }

    $scope.requestCartonDelete = function (){
        
        $scope.isDateRangeInvalid = !dateValidationService.validateSearchDates($scope.beginDate, $scope.endDate);

        if ($scope.isDateRangeInvalid) {
            return;
        }

        var dates = {
            Item1: $filter("date")($scope.beginDate, "medium"),
            Item2: $filter("date")($scope.endDate, "medium")
        };

        var result = confirm(translationService.translate('Dashboard.Widgets.DeleteBoxLastCartons.Message.DeleteConfirm', dates.Item1.toString(), dates.Item2.toString()));

        if (result) {
            messagingService.publish("CartonRemovalBetweenDate", dates);
        }
    }

    cartonRemovedCountObservable.autoSubscribe(function (data) {
        $timeout(function () {
            
            delete $scope.cartonRemovalMessage;
            $scope.cartonRemovalMessage = translationService.translate('Dashboard.Widgets.DeleteBoxLastCartons.Message.DeleteCount', data.Data);
        }, 0, true);
    }, $scope, 'Carton Removed Count');

}