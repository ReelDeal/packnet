function CartonPropertyGroupsCtrl(translationService, $scope, $log, messagingService, uiGridConstants) {
    var cartonPropertyGroupsCountersUpdatedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("CartonPropertyGroupsCountersUpdated"));

    var requestData = function () {
        messagingService.publish("GetCartonPropertyGroups", "");
    };

    $scope.$log = $log;
    
    $scope.cartonPropertyGroups = [];
    var setupCPGStats = function (msg){
        
        delete $scope.cartonPropertyGroups;
        $scope.cartonPropertyGroups = msg.Data;
        
        delete $scope.totals;
        $scope.totals = Enumerable.From($scope.cartonPropertyGroups)
            .GroupBy(function (f) { return 1; }, null, function (key, g) {
                return {
                    Expedited: g.Sum("$.NumberOfExpeditedRequests | 0"),
                    Stopped: g.Sum("$.NumberOfStoppedRequests | 0"),
                    Normal: g.Sum("$.NumberOfNormalRequests | 0"),
                    Last: g.Sum("$.NumberOfLastRequests | 0"),
                    Total: g.Sum("$.NumberOfRequests | 0")
                }
            }, function (f) { return 1 }).FirstOrDefault();
    }

    //Take the first message so we don't get a half second lag
    messagingService.cartonPropertyGroupsObservable
        .first()
        .autoSubscribe(function (msg) {
            setupCPGStats(msg);
        }, $scope, 'Carton Property Groups');

    //Limit the times we update the UI (only once every .5 seconds)
    messagingService.cartonPropertyGroupsObservable
        .skip(1)
        .sample(500)
        .autoSubscribe(function (msg) {
            setupCPGStats(msg);
        }, $scope, 'Carton Property Groups');

    cartonPropertyGroupsCountersUpdatedObservable.autoSubscribe(function (msg) {
        requestData();
    }, $scope, 'Carton Property Groups Counters Updated');

    if (messagingService.IsSignalRConnected()) {
        requestData();
    }

    messagingService.signalRConnectedObservable.autoSubscribe(requestData, $scope, 'SignalR Connected');

    $scope.updateStatus = function (state, cpg) {

        messagingService.publish("UpdateCartonPropertyGroupStatus", {
            Alias: cpg.Alias,
            Status: state
        });

    };

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'cartonPropertyGroups',
        headerRowHeight: 26,
        rowHeight: 26,
        enableFiltering: false,
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                width: "10%",
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 0
                }
            },
            {
                field: 'NumberOfRequests',
                name: translationService.translate('Common.Label.Requests'),
                width: "15%",
                cellClass: 'center-text',
                type: 'number',
                cellTemplate: '<label id="cpg-NumberOfRequests-{{row.entity.Alias}}" ng-bind="row.entity.NumberOfRequests"></label>'
            },
            {
                field: 'NumberOfExpeditedRequests',
                name: translationService.translate('Common.Label.Expedite'),
                width: "15%",
                cellClass: 'center-text',
                type: 'number',
                cellTemplate: '<label id="cpg-NumberOfExpeditedRequests-{{row.entity.Alias}}" ng-bind="row.entity.NumberOfExpeditedRequests"></label>'
            },
            {
                field: 'NumberOfStoppedRequests',
                name: translationService.translate('Common.Label.Stopped'),
                width: "15%",
                cellClass: 'center-text',
                type: 'number',
                cellTemplate: '<label id="cpg-NumberOfStoppedRequests-{{row.entity.Alias}}" ng-bind="row.entity.NumberOfStoppedRequests"></label>'
            },
            {
                field: 'NumberOfLastRequests',
                name: translationService.translate('Common.Label.Last'),
                width: "15%",
                cellClass: 'center-text',
                type: 'number',
                cellTemplate: '<label id="cpg-NumberOfLastRequests-{{row.entity.Alias}}" ng-bind="row.entity.NumberOfLastRequests"></label>'
            },
            {
                field: 'Status',
                name: translationService.translate('Common.Label.Status'),
                minWidth: 355,
                cellClass: 'ui-grid-cell-contents',
                cellTemplate: 'partials/MachineManager/CellTemplates/CartonPropertyGroupsStatus.html'
            }
        ],
        onRegisterApi: function (gridApi) {
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            gridApi.updateStatus = $scope.updateStatus;
        }
    };

};