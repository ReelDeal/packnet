﻿(function (){
    'use strict';

    app.controller('OnlineIndicatorCtrl', OnlineIndicatorCtrl);

    OnlineIndicatorCtrl.$inject = ['$scope', 'machineGroupService'];

    function OnlineIndicatorCtrl($scope, machineGroupService){
        $scope.currentMachineGroup = machineGroupService.getCurrentMachineGroup();

        machineGroupService.currentMachineGroupUpdatedObservable.autoSubscribe(function (machine) {
            $scope.currentMachineGroup = machine;
        }, $scope, 'Current Machine Group Updated');
    }
})();