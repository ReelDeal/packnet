/*
TODO: CorrugatesCtrl should be renamed because it really is a list of all corrugates loaded on a machine.
*/
function CorrugatesCtrl($scope, $log, messagingService) {
    $scope.$log = $log;

    var setupCorrugates = function (msg){
        msg.Data.forEach(function (item) {
            item.statusIcon = item.Producible ? 'img/GreenDot.png' : 'img/red_icon.png';
            item.statusMessage = item.Producible ? 'Producible' : 'Not Producible';
        });
        
        delete $scope.corrugates;
        $scope.corrugates = msg.Data;
    }

    //Take the first message so we don't get a half second lag
    messagingService.corrugatesOnMachinesObservable
        .first()
        .autoSubscribe(function (msg) {
            setupCorrugates(msg);
        }, $scope,'Corrugates On Machines First');
    
    //Limit the times we update the UI (only once every .5 seconds)
    messagingService.corrugatesOnMachinesObservable
        .skip(1)
        .sample(500)
        .autoSubscribe(function (msg) {
            setupCorrugates(msg);
        }, $scope,'Corrugates On Machines Sample');

    //Ask for initial set of data
    var requestData = function () {
        messagingService.publish("GetCorrugatesLoadedOnMachines", {});
    };

    if (messagingService.IsSignalRConnected()) {
        requestData();
    }

    messagingService.signalRConnectedObservable.autoSubscribe(requestData, $scope,'SignalR Connected');

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'corrugates',
        headerRowHeight: 26,
        rowHeight: 26,
        columnDefs: [
            { field: 'Alias', name: 'Name', displayName: 'Dashboard.Widgets.General.Corrugates.Headers.Name', headerCellFilter: 'translate' },
            { field: 'Width', name: 'Width', width: 75, displayName: 'Dashboard.Widgets.General.Corrugates.Headers.Width', headerCellFilter: 'translate' },
            { field: 'MachineName', name: 'Machine', width: 200, displayName: 'Dashboard.Widgets.General.Corrugates.Headers.Machine', headerCellFilter: 'translate' },
            { field: 'Status', name: '', cellTemplate: 'partials/MachineManager/CellTemplates/StatusIcon.html', width: 30 }
        ]
    };
}
