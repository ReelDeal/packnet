app.controller("machineStatusCtrl", function (translationService, $scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.machineStatus = [];
    $scope.machines = [];

    var setupMachineData = function(msg) {
        $timeout(function () {
            var machinesEnumerable = Enumerable.From(msg.Data)
                .Where(function (m) {
                    return m.MachineType !== "ExternalCustomerSystem";
                })
                .Select("m => m.Data");
            
            
            delete $scope.machines;
            $scope.machines = machinesEnumerable.ToArray();

            $scope.machineStatus =
            [
                {
                    value: machinesEnumerable.Count("m => m.CurrentStatus == 'MachineInitializing'"),
                    color: "#85A000",
                    status: translationService.translate('MachineStatus.Label.MachineInitializing')
                },
                {
                    value: machinesEnumerable.Count("m => m.CurrentStatus == 'MachineOffline'"),
                    color: "#A69700",
                    status: translationService.translate('MachineStatus.Label.MachineOffline')
                },
                {
                    value: machinesEnumerable.Count("m => m.CurrentStatus == 'MachinePaused'"),
                    color: "#FFE800",
                    status: translationService.translate('MachineStatus.Label.MachinePaused')
                },
                {
                    value: machinesEnumerable.Count("m => m.CurrentStatus == 'MachineOnline'"),
                    color: "#67E300",
                    status: translationService.translate('MachineStatus.Label.MachineOnline')
                },
                {
                    value: machinesEnumerable.Count("m => m.CurrentStatus == 'MachineError' || " +
                                                    "m.CurrentStatus == 'MachineEmergencyStop' || " +
                                                    "m.CurrentStatus == 'UnexpectedLabelInPrinterQueue' || " +
                                                    "m.CurrentStatus == 'UnexpectedLabelOnPrinter' || " +
                                                    "m.CurrentStatus == 'OutOfRibbon' || " +
                                                    "m.CurrentStatus == 'HeadOpen' || " +
                                                    "m.CurrentStatus == 'OutOfLabels' ||" +
                                                    "m.CurrentStatus == 'InvalidPrintData'"),
                    color: "#FF0000",
                    status: translationService.translate('MachineStatus.Label.MachineError')
                }
            ];
        });
    };

    //Take the first message so we don't get a 5 second lag
    messagingService.machinesObservable
        .first()
        .autoSubscribe(setupMachineData, $scope, 'Machines Observable (1)');

    //Limit the times we update the UI (only once every 5 seconds)
    messagingService.machinesObservable
        .skip(1)
        .sample(5000)
        .autoSubscribe(setupMachineData, $scope, 'Machines Observable (2)');

    $scope.options = {

        segmentShowStroke: false,
        animationEasing: "easeOutElastic",
        responsive: true,
        maintainAspectRatio: false,
    }

    messagingService.signalRConnectedObservable.autoSubscribe(function (msg) {
         messagingService.publish("GetMachines", "");
    }, $scope, 'SignalR Connected');

    if (messagingService.IsSignalRConnected()) {
        messagingService.publish("GetMachines", "");
    }

    $scope.gridOptions = {
        enableGridMenu: true,
        data : 'machines',
        enableRowSelection : false,
        headerRowHeight : 26,
        rowHeight : 26,
        minRowsToShow : 5, //Only show 5 rows at a time
        columnDefs : [
            {
                name: translationService.translate('Machine.Label.NameSingular'),
                field: 'Alias'
            },
            {
                name: translationService.translate('Machine.Label.Type'),
                field: 'MachineType',
                cellFilter: 'machineType'
            },
            {
                name: translationService.translate('Common.Label.CurrentState'),
                field: 'CurrentStatus',
                cellFilter: 'machineStatus'
            }
        ]
    };
})