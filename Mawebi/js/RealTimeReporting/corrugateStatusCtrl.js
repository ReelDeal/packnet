app.controller("CorrugateStatusCtrl", function (translationService, $scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.corrugateStatus = [];
    $scope.corrugatesToDisplay = [];
    
    var machines = Enumerable.From([]);
    var corrugates = [];
    var productionGroups = [];

    var loadedCorrugate = [];

    var status = {
        Loaded: translationService.translate('Common.Label.Loaded'),
        NotAssigned: translationService.translate('Common.Label.NotAssigned'),
        NotLoaded: translationService.translate('Common.Label.NotLoaded')
    }

    var setupCorrugateData = function () {
        $timeout(function () {
            var tracks = machines.SelectMany("m => m.Tracks");
            loadedCorrugate = tracks.Select("m => m.LoadedCorrugate").Where(function (c) {
                return c != null && c.Alias != status.NotLoaded;
            }).Distinct("$.Alias");
            var inProductionGroups = Enumerable.From(productionGroups).SelectMany("m => m.ConfiguredCorrugates").Distinct();
            var totalCount = Enumerable.From(corrugates).Distinct("$.Alias").Count();
            var loadedCount = loadedCorrugate.Count();
            var notAssignedCount = totalCount - inProductionGroups.Count();
            var notLoadedCount = totalCount - loadedCount - notAssignedCount;

            
            delete $scope.corrugatesToDisplay;
            $scope.corrugatesToDisplay = loadedCorrugate.Select(function (c) {
                return { Alias: c.Alias, State: status.Loaded };
            }).Distinct("$.Alias").ToArray();

            var notAssigned = Enumerable.From(corrugates)
                .Where(function (c) {
                    return !inProductionGroups.Any(function (x) {
                        return x === c.Id;
                    });
                })
                .Select(function (c) {
                    return { Alias: c.Alias, State: status.NotAssigned };
                }).ToArray();

            var notLoaded = Enumerable.From(corrugates)
                .Except(loadedCorrugate, function (x) { return x.Id; })
                .Where(function (f) {
                    return !Enumerable.From(notAssigned).Any(function (x) {
                        return x.Alias === f.Alias;
                    });
                })
                .Select(function (c) {
                    return { Alias: c.Alias, State: status.NotLoaded };
                }).ToArray();

            //Without this the not assigned corrugates are not displayed in the list
            $scope.corrugatesToDisplay = $scope.corrugatesToDisplay.concat(notLoaded);
            $scope.corrugatesToDisplay = $scope.corrugatesToDisplay.concat(notAssigned);
            
            $scope.corrugateStatus =
            [
                {
                    value: loadedCount,
                    color: "#67E300",
                    status: status.Loaded
                },
                {
                    value: notLoadedCount,
                    color: "#FFE800",
                    status: status.NotLoaded
                },
                {
                    value: notAssignedCount,
                    color: "#FF0000",
                    status: status.NotAssigned
                }
            ];
        }, 0, true);
    };

    var setCorrugates = function (msg) {
        corrugates = msg.Data;
        setupCorrugateData();
    };

    var setProductionGroups = function (msg) {
        productionGroups = msg.Data;
        setupCorrugateData();
    };

    var setMachines = function (msg){
        machines = Enumerable.From(msg.Data).Select("m => m.Data");
        setupCorrugateData();
    }

    //Take the first message so we don't get a 5 second lag
    messagingService.machinesObservable
        .first()
        .autoSubscribe(setMachines, $scope, 'Machines Observable (1)');

    //Limit the times we update the UI (only once every 5 seconds)
    messagingService.machinesObservable
        .skip(1)
        .sample(5000)
        .autoSubscribe(setMachines, $scope, 'Machines Observable (2)');

    //Take the first message so we don't get a 5 second lag
    messagingService.corrugatesObservable
        .first()
        .autoSubscribe(setCorrugates, $scope, 'Corrugates Observable (1)');

    //Limit the times we update the UI (only once every 5 seconds)
    messagingService.corrugatesObservable
        .skip(1)
        .sample(5000)
        .autoSubscribe(setCorrugates, $scope, 'Corrugates Observable (2)');

    //Take the first message so we don't get a 5 second lag
    messagingService.productionGroupsObservable
        .first()
        .autoSubscribe(setProductionGroups, $scope, 'Production Groups Observable (1)');

    //Limit the times we update the UI (only once every .5 seconds)
    messagingService.productionGroupsObservable
        .skip(1)
        .sample(5000)
        .autoSubscribe(setProductionGroups, $scope, 'Production Groups Observable (2)');

    
    $scope.options = {
        segmentShowStroke: false,
        animationEasing: "easeOutElastic",
        responsive: true,
        maintainAspectRatio: false,
    }

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'corrugatesToDisplay',
        enableRowSelection: false,
        headerRowHeight: 26,
        rowHeight: 26,
        minRowsToShow: 5, //Only show 5 rows at a time
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Corrugate.Label.NameSingular')
            },
            {
                field: 'State',
                name: translationService.translate('Common.Label.CurrentState')
            }
        ]
    };

    messagingService.signalRConnectedObservable.autoSubscribe(function (msg) {
        messagingService.publish("GetCorrugates");
        messagingService.publish("GetMachines");
        messagingService.publish("GetProductionGroups");
    }, $scope, 'SignalR Connected');

    if (messagingService.IsSignalRConnected()) {
        messagingService.publish("GetCorrugates");
        messagingService.publish("GetMachines");
        messagingService.publish("GetProductionGroups");
    }

});