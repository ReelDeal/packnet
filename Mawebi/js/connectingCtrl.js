function ConnectingCtrl($scope, $interval, $timeout, $state, messagingService, alertService, AUTH_EVENTS) {
    $timeout = $timeout.getInstance($scope);
    $scope.secondsToWait = 10;
    $scope.progress = 33;
    
    $scope.progressType = 'success';
    $scope.connectedToSignalR = false;
    $scope.connectedToServer = false;
    $scope.currentlyConnectingTo = 'SignalR';
    $scope.waitingForServerResponse = true;
    $scope.waitingForSignalRResponse = true;
    $scope.connectionFailed = false;
    $scope.canReconnect = false;
    $scope.signalRConnectionFailed = false;
    $scope.sidebarOptions.connectingPage = true;

    $scope.connectToServer = function () {
        $scope.progress = 66;
        alertService.clearAll();
        $scope.canReconnect = false;
        $scope.connectionFailed = false;
        messagingService.publish("ConnectionAlive", "");
        $scope.counter = $scope.secondsToWait;
        var cancelServerPromise = $interval(function () {
            if (!$scope.waitingForServerResponse) {
                // received a response from server
                if (angular.isDefined(cancelServerPromise)) {
                    $interval.cancel(cancelServerPromise);
                    cancelServerPromise = undefined;
                }
                $timeout(function () {
                    $scope.progress = 100;
                });
                if ($scope.authenticationService.destination) {
                    var dest = $scope.authenticationService.destination;
                    
                    delete $scope.authenticationService.destination;
                    $scope.authenticationService.destination = '';
                    $scope.sidebarOptions.connectingPage = false;
                    $state.go(dest);
                } else if ($scope.authenticationService.isAuthenticated()) {
                    $scope.sidebarOptions.connectingPage = false;
                    var state = $scope.currentUser.IsAdmin ? "Dashboard" : "SelectMachineGroup";
                    $state.go(state);
                }
                else{
                    $scope.authenticationService.requestAutoLoginUser();
                }
            }
            $scope.counter--;

            if ($scope.counter == 0) {
                if ($scope.waitingForServerResponse) {
                    $scope.canReconnect = true;
                    $scope.progress = 0;
                    alertService.addError("Failed to contact PackNET Server");
                    $scope.connectionFailed = true;
                }
            }
        }, 1000, $scope.secondsToWait);
    };

    //Connect to messageService (signalR)
    messagingService.signalRConnectedObservable.autoSubscribe(function (msg) {
        connectedToSignalR();
   }, $scope,'SignalR Connected');

    messagingService.signalRConnectionFailedObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            $scope.canReconnect = false;
            $scope.connectionFailed = true;
            $scope.signalRConnectionFailed = true;
            alertService.addError("Failed to connect to 'SignalR' messagingService. Please contact support");
        }, 10, true);
    }, $scope,'SignalR Connection Failed');

    messagingService.connectionAliveObservable.autoSubscribe(function (msg) {
        $scope.waitingForServerResponse = false;
    }, $scope,'Connection Alive');

    var connectedToSignalR = function () {
        $timeout(function () {
            $scope.connectedToSignalR = true;
            
            $scope.currentlyConnectingTo = 'PackNet.Server';
        }, 10, true);
        $scope.connectToServer();
    }

    if (messagingService.IsSignalRConnected()) {
        connectedToSignalR();
    }
}