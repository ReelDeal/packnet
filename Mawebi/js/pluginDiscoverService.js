﻿function pluginDiscoveryService(messagingService, $rootScope, AUTH_EVENTS) {
 
    function discoverServices() {
        messagingService.publish("DiscoverSLDService");
        messagingService.publish("DiscoverArticleService");
    }

    function start(){
        $rootScope.$on(AUTH_EVENTS.loginSuccess, function () {
            discoverServices();
        });
    }

    messagingService.messageSubject
        .where(messagingService.filterObs("DiscoverSLDServiceResponse"))
        .select(messagingService.dataFunction).autoSubscribe(function (data) {
        $rootScope.SLDServiceAvaliable = true;
    }, $rootScope, 'Discover SLD Service Response');

    messagingService.messageSubject
        .where(messagingService.filterObs("DiscoverArticleServiceResponse"))
        .select(messagingService.dataFunction).autoSubscribe(function (data) {
            $rootScope.ArticleServiceAvaliable = true;
        }, $rootScope, 'Discover Article Service Response');

    return { start : start };
}
app.factory('pluginDiscoveryService', pluginDiscoveryService);
