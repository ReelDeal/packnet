function ChangeProductionGroupCtrl($log, $scope, $timeout, alertService, introJsService, machineGroupService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.productionGroups = {};
    $scope.currentMachineGroup = {};
    $scope.selectedProductionGroupModel = {};
    var awaitingUpdate = false;

    var requestData = function () {
        messagingService.publish("GetProductionGroups");
    };

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    machineGroupService.currentMachineGroupUpdatedObservable.autoSubscribe(function (currentMachineGroup) {
        
        delete $scope.currentMachineGroup;
        $scope.currentMachineGroup = currentMachineGroup;
    }, $scope,'Current Machine Updated');

    $scope.currentMachineGroup = machineGroupService.getCurrentMachineGroup();

    var updateGroups = function (msg){
        $timeout(function () {
                var currentlySelectedProductionGroup = $scope.selectedProductionGroup;
                var machineGroups = Enumerable.From(machineGroupService.getMachineGroups());
                var productionGroupsEnumerable = Enumerable.From(msg.Data)
                    .Select(function (pg) {
                        pg.MachineGroups = machineGroups.Where(function (mg) {
                                return pg.ConfiguredMachineGroups.indexOf(mg.Id) != -1;
                            })
                            .ToArray();
                        return pg;
                    });

                delete $scope.productionGroups;
                $scope.productionGroups = productionGroupsEnumerable.ToArray();

                if (currentlySelectedProductionGroup) {

                    delete $scope.selectedProductionGroup;
                    $scope.selectedProductionGroup = Enumerable.From($scope.productionGroups)
                        .FirstOrDefault(undefined,
                            function (pg) {
                                return pg.Id === currentlySelectedProductionGroup.Id;
                            });
                }
                else if ($scope.currentMachineGroup &&
                    $scope.currentMachineGroup.ProductionGroup &&
                    $scope.currentMachineGroup.ProductionGroup.Alias) {

                    delete $scope.selectedProductionGroup;
                    $scope.selectedProductionGroup = Enumerable.From($scope.productionGroups)
                        .FirstOrDefault(undefined,
                            function (pg) {
                                return pg.Id === $scope.currentMachineGroup.ProductionGroup.Id;
                            });
                }
                if ($scope.selectedProductionGroup && $scope.selectedProductionGroupModel) {
                    $scope.selectedProductionGroupModel.selectedProductionGroupId = $scope.selectedProductionGroup.Id;
                }
            },
            0);
    };

    //Take the first message so we don't get a 2 second lag
    messagingService.productionGroupsObservable
        .first()
        .autoSubscribe(updateGroups,$scope, 'Production Groups (1)');

    //Limit the times we update the UI (only once every 2 seconds)
    messagingService.productionGroupsObservable
        .skip(1)
        .sample(2000)
        .autoSubscribe(updateGroups, $scope, 'Production Groups (2)');

    $scope.saveProductionGroup = function () {
        $log.info('Saving Production Group... ');

        Enumerable.From($scope.productionGroups).ForEach(function (pg) {
            if ($scope.selectedProductionGroup != null && pg.Id === $scope.selectedProductionGroup.Id) {
                pg.ConfiguredMachineGroups = Enumerable.From(pg.ConfiguredMachineGroups).Where(function (mg) {
                    return $scope.currentMachineGroup.Id !== mg;
                }).ToArray();
            }
            else if (pg.Id === $scope.selectedProductionGroupModel.selectedProductionGroupId && (pg.ConfiguredMachineGroups.indexOf($scope.currentMachineGroup.Id) < 0)) {
                pg.ConfiguredMachineGroups.push($scope.currentMachineGroup.Id);
            }
            delete pg.MachineGroups;
        });
        
        messagingService.publish("UpdateProductionGroups", $scope.productionGroups);
        delete $scope.selectedProductionGroup;
        awaitingUpdate = true;
        $timeout(function () {
            if (awaitingUpdate) {
                $log.info('ProductionGroupsCtrl.save timeout expired... save failed');
                alertService.addError(translationService.translate('ChangeProductionGroup.Message.SaveTimeout'));
            }
        }, 5000);
    };

    messagingService.productionGroupUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;

        if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('ChangeProductionGroup.Message.SaveSuccess'), 5000);
            messagingService.publish("GetMachineGroups");
        }
        else {
            alertService.addError(translationService.translate('ChangeProductionGroup.Message.SaveFailed'));
        }
    }, $scope, 'ProductionGroup Updated');

    $scope.canSave = function (){
        return ($scope.selectedProductionGroup == null && $scope.selectedProductionGroupModel.selectedProductionGroupId != null) ||
            ($scope.selectedProductionGroup != null && $scope.selectedProductionGroup.Id != $scope.selectedProductionGroupModel.selectedProductionGroupId);
    };

    //filters
    $scope.onlyIncluded = function (item) {
        if (item.Included === true) {
            return true;
        }
        return false;
    };

    $scope.tellMeAboutThisPage = function () {
    	var steps = [];

    	steps.push({
    		element: "#notFound",
    		intro: translationService.translate('ChangeProductionGroup.Help.Overview')
    	});

    	steps.push({
    		element: "#productionGroupsWrapper",
    		intro: translationService.translate('ChangeProductionGroup.Help.ProductionGroups')
    	});

    	steps.push({
    		element: "#ProductionGroupsSaveButton",
    		intro: translationService.translate('ChangeProductionGroup.Help.Save')
    	});

    	steps.push({
    		element: "#ProductionGroupsCloseButton",
    		intro: translationService.translate('ChangeProductionGroup.Help.Cancel')
    	});

    	return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
}