function MachineProductionListCtrl($scope, $timeout, machineGroupService, messagingService, producibleHelperService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.productionList = [];

    var machineProductionObservable = messagingService.messageSubject
        .where(messagingService.filterObs("MachineProduction"));

    // this should only be updated when the page is loaded or a mg has changed.
    machineProductionObservable.autoSubscribe(handleMachineProducible, $scope, 'machineProductionObservable Status Changed');

    //handle individual job updates after initial load of mg queue.
    messagingService.producibleStatusChangedObservable
        .autoSubscribe(handleProducibleStatusChanged, $scope, 'Producible Status Changed');

    machineGroupService.currentMachineGroupUpdatedObservable.autoSubscribe(function (updatedMachineGroup) {
        $timeout(function () {
            if (!updatedMachineGroup || (updatedMachineGroup.CurrentStatus !== "MachineGroupOnline" &&
                                         updatedMachineGroup.CurrentStatus !== "MachineGroupPaused" &&
                                         updatedMachineGroup.CurrentStatus !== "ProduceCurrentAndPause" &&
                                         updatedMachineGroup.CurrentStatus !== "ProduceQueueAndPause")) {
                $scope.productionList.length = 0;
            }
        }, 0, true);
    }, $scope, 'current machine group updated');

    //Ask for initial set of data
    messagingService.publish("GetMachineGroupProductionQueue", "");

    function completeInfo(job) {
        job.UserFriendlyProducibleStatus = translationService.translate('ProducibleStatus.Label.' + job.ProducibleStatus);
        job.UserFriendlyProducibleType = translationService.translate('ProducibleType.Label.' + job.ProducibleType);
        if (job.ProducibleType === "Carton") {
            job.AdditionalInfo = job.XValues;
        }
    }

    function handleMachineProducible(msg) {
        //this should only happen when the control is loaded.  
        $timeout(function () {
                if ($scope.currentMachineGroup) {
                    var tempList = Enumerable.From(msg.Data)
                        .Where(function (job) {
                            return job.ProducedOnMachineGroupId === $scope.currentMachineGroup.Id;
                        })
                        .Select(function (p) {
                            completeInfo(p);
                            return p;
                        })
                        .ToArray();
                    var list = [];
                    for (var i = 0; i < tempList.length; i++) {
                        var producible = tempList[i];
                        if (producible.ItemsToProduce) {
                            for (var j = 0; j < producible.ItemsToProduce.length; j++) {
                                var innerProducible = producible.ItemsToProduce[j];
                                if (innerProducible.ProducibleStatus !== "ProducibleCompleted") {
                                    list.push(innerProducible);
                                }
                            }
                        }
                        else {
                            list.push(producible);
                        }
                    }

                    delete $scope.productionList;
                    $scope.productionList = updateDisplayListWithNewProducibleStatuses(list);
                    while ($scope.productionList
                        .length <
                        ($scope.currentMachineGroup
                            .MaxQueueLength +
                            1)) { // One item aded for the item currently in production)
                        $scope.productionList.push({});
                    }
                }
            },
            400,
            true);
    }

    // This code is based on the assumption that one job is dispatched to a machine at a time
    function handleProducibleStatusChanged(msg) {
        $timeout(function () {
            // User was logout.
            if ($scope.currentMachineGroup == null) {
                return;
            }
            var newListWithJobUpdated = [];

            if (msg.Data.ProducedOnMachineGroupId !== $scope.currentMachineGroup.Id) {
                // Check if there was an error and the producible was taken by another MG.
                if (Enumerable.From($scope.productionList).Any(function (producible) { return (producible.Id === msg.Data.Id); })) {
                    // Remove the producible from the list.
                    newListWithJobUpdated = Enumerable.From($scope.productionList).Where(function (producible) {
                        return (producible.Id !== msg.Data.Id);
                    });
                }
                else {
                    return; // ignore data not for my machine group
                }
            }
            else {
                //job should no longer be displayed in production -- add to out production history list
                if (msg.Data.ProducibleStatus === "ProducibleCompleted"
                    || msg.Data.ProducibleStatus === "ProducibleRemoved"
                    || msg.Data.ProducibleStatus === "ProducibleFailed"
                    || msg.Data.ProducibleStatus === "NotProducible") {

                    if (msg.Data.ProducibleStatus != "ProducibleRemoved") {
                        if (msg.Data.CustomerUniqueId.indexOf('+') > -1) {
                            var listOfProducibles = msg.Data.CustomerUniqueId.split('+');
                            Enumerable.From(listOfProducibles).ForEach(function (p) {
                                var untiledJob = JSON.parse(JSON.stringify(msg.Data));
                                untiledJob.CustomerUniqueId = p.trim();
                                producibleHelperService.addProducible(untiledJob);
                            });
                        }
                        else {
                            producibleHelperService.addProducible(msg.Data);
                        }
                    }

                    var removeIds = [];
                    if (msg.Data.ItemsToProduce) {
                        // Remove the completed order and items
                        removeIds = Enumerable
                            .From(msg.Data.ItemsToProduce)
                            .Select(function (producible) {
                                return producible.Id;
                            })
                            .ToArray();
                    }

                    removeIds.push(msg.Data.Id);
                    newListWithJobUpdated = Enumerable
                        .From($scope.productionList)
                        .Where(function (producible) {
                            return ($.inArray(producible.Id, removeIds) < 0);
                        })
                        .ToArray();
                }
                else { // job not completed update statuses 
                    if (msg.Data.ItemsToProduce) {
                        // Update the production list
                        Enumerable
                            .From(msg.Data.ItemsToProduce)
                            .Where(function (producible) {
                                return (producible.ProducibleStatus !== "ProducibleCompleted");
                            })
                            .ForEach(function (producible) {
                                newListWithJobUpdated = updateOrAddListWithNewProducibleStatus(producible);
                                
                                delete $scope.productionList;
                                $scope.productionList = newListWithJobUpdated;
                            });

                        // Remove the completed items
                        var completedIds = Enumerable
                            .From(msg.Data.ItemsToProduce)
                            .Where(function (producible) {
                                return (producible.ProducibleStatus === "ProducibleCompleted");
                            })
                            .Select(function (producible) {
                                return producible.Id;
                            })
                            .ToArray();

                        newListWithJobUpdated = Enumerable
                            .From($scope.productionList)
                            .Where(function (producible) {
                                return ($.inArray(producible.Id, completedIds) < 0);
                            })
                            .ToArray();
                    }
                    else {
                        newListWithJobUpdated = updateOrAddListWithNewProducibleStatus(msg.Data);
                    }
                }
            }

            

            var currentList = updateDisplayListWithNewProducibleStatuses(newListWithJobUpdated);
            //Trying to fix the Items In Production getting out of sync
            //TODO: I put this in to mess with Garcia
            if ($scope.currentMachineGroup &&
                $scope.currentMachineGroup.ProductionGroup &&
                $scope.currentMachineGroup.ProductionGroup.SelectionAlgorithm === "Order" &&
                currentList.length > $scope.currentMachineGroup.MaxQueueLength) {
                messagingService.publish("GetMachineGroupProductionQueue", "");
            }
            else {
                delete $scope.productionList;
                $scope.productionList = currentList;
            }

            while ($scope.productionList.length < ($scope.currentMachineGroup.MaxQueueLength + 1)) { // One item added for the item currently in production)
                $scope.productionList.push({});
            }
        }, 1000, true);
    }

    function updateDisplayListWithNewProducibleStatuses(newUpdatedList) {
        return Enumerable.From(newUpdatedList)
                    .Where(function (p) { return p.Id; })
                   .Select(function (p) {
                       completeInfo(p);
                       return p;
                   })
                    .ToArray();
    }

    function updateOrAddListWithNewProducibleStatus(producible) {
        var jobFound = false;

        var newList = [];

        newList = Enumerable.From($scope.productionList).Select(function (prod) {
            if (prod.Id === producible.Id) {
                jobFound = true;
                return producible;
            }
            return prod;
        }).ToArray();

        // if this is a new item add it to the list
        if (!jobFound) {
            newList.push(producible);
        }

        return newList;
    }


    var gridRowHeight = 26;
    var gridHeaderHeight = 26;
    var maxRows = 0;
    function updateGridRowsShown(rows) {
        if (maxRows < rows) {
            maxRows = rows;
            
            delete $scope.gridStyle;
            $scope.gridStyle = {
                'height': 5 + gridHeaderHeight + gridRowHeight * rows + 'px'
            }
        }
    }

    //set initial grid height.
    if ($scope.currentMachineGroup) {
        updateGridRowsShown($scope.currentMachineGroup.MaxQueueLength + 1); // One item aded for the item currently in production
    }

    $scope.gridOptions = {
        data: 'productionList',
        enableHorizontalScrollbar: uiGridConstants.scrollbars.NEVER,
        enableVerticalScrollbar: uiGridConstants.scrollbars.NEVER,
        headerRowHeight: 26,
        rowHeight: 26,
        virtualizationThreshold: 3,
        columnDefs: [
            {
                field: 'UserFriendlyProducibleStatus',
                displayName: translationService.translate('Common.Label.Status'),
                width: 500
            },
            {
                field: 'CustomerUniqueId',
                displayName: translationService.translate('Common.Label.CustomerId'),
                width: 375
            },
            {
                field: 'UserFriendlyProducibleType',
                displayName: translationService.translate('Common.Label.Type'),
                width: 150
            },
            {
                field: 'Length',
                displayName: translationService.translate('Common.Label.LengthAcronym'),
                width: 60
            },
            {
                field: 'Width',
                displayName: translationService.translate('Common.Label.WidthAcronym'),
                width: 60
            },
            {
                field: 'Height',
                displayName: translationService.translate('Common.Label.HeightAcronym'),
                width: 60
            },
            {
                field: 'DesignId',
                displayName: translationService.translate('Common.Label.DesignId'),
                width: 100
            },
            {
                field: 'TrackNumber',
                displayName: translationService.translate('Machine.Label.Track'),
                width: 80
            },
            {
                field: 'AdditionalInfo',
                displayName: translationService.translate('Machine.Label.AditionalInfo'),
                width: '*'
            }
        ]
    };

    //UI-grid is not resizing correctly either :-). Old NG grid issue: https://github.com/angular-ui/ng-grid/issues/50
    $scope.$watch('productionList', function () {
        fixStoopidGridDrawring();
    });

    function fixStoopidGridDrawring() {
        $timeout(function () {
            $(window).resize();
        }, 0, true);
    }
}

app.directive('machineProduction', function () {
    {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'partials/Operator/machineProductionList.html'
        };
    }
});