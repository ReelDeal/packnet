function SelectMachineGroupCtrl($rootScope, $scope, $state, $stateParams, $timeout, alertService, introJsService, machineGroupService, translationService){
    var redirectAfterSelection = ($stateParams.endState !== undefined);

    $scope.machineGroups = [];
    $scope.showInterface = false;

    $timeout(function () {
        $scope.machineGroups = machineGroupService.getMachineGroups();

        if (redirectAfterSelection) {
            if ($scope.machineGroups.length === 1) {
                //Auto-select the only Machine Group configured
                $scope.openEndState($scope.machineGroups[0].Id);
            }
            else {
                //User intended to access a page without previously selecting a Machine Group
                $scope.showInterface = true;

                alertService.addError(translationService.translate('ChangeMachineGroup.Validation.MustBeSelected'), 5000);
            }
        }
        else {
            $scope.showInterface = true;
        }
    }, 0);

    machineGroupService.machineGroupsUpdatedObservable.autoSubscribe(function () {
        $timeout(function () {
            $scope.machineGroups = machineGroupService.getMachineGroups();
        }, 0);
    }, $scope, 'SelectMachineGroupCtrl');

    $scope.openEndState = function (machineGroupId) {
        var options = { machineGroupId: machineGroupId };
        machineGroupService.updateCurrentMachineGroup(machineGroupId, $rootScope.currentUser);

        var endState = $stateParams.endState ? $stateParams.endState : 'OperatorConsole.Console';

        $state.go(endState, options);
    };

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        steps.push({
            element: '#notFound',
            intro: translationService.translate('ChangeMachineGroup.Help.Overview')
        });

        steps.push({
            element: '#divMachineSelectionList',
            intro: translationService.translate('ChangeMachineGroup.Help.List')
        });

        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
}