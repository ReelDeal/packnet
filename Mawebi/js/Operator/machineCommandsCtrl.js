function MachineCommandsCtrl($log, $scope, $timeout, alertService, introJsService, machineGroupService, messagingService, translationService) {
    var master = machineGroupService.getCurrentMachineGroup();
    var awaitingPause = false;
    var awaitingSendResponseId = "";

    $scope.data = [];
    $scope.machinesPerformingCommands = [];
    $scope.breaksEngaged = false;
    $scope.currentMachineGroup = master;

    var machineServiceResponseObservable = messagingService.messageSubject
        .where(messagingService.filterObs("MachineServiceResponse"));

    $scope.currentMachineGroupUpdatedObsermachineGroupObservable = machineGroupService.currentMachineGroupUpdatedObservable.autoSubscribe(handleMachinesUpdated, $scope, 'Current Machine Updated');

    var getMachineCommandsObservable = messagingService.messageSubject
        .where(messagingService.filterObs("MachineCommandResponse"));

    var machineCommandDoneObservable = messagingService.messageSubject
        .where(messagingService.filterObs("MachineCommandDoneResponse"));

    getMachineCommandsObservable.autoSubscribe(function (data) {
        if (data.Result == "Success") {
            handleMachinesCommandResponse(data.Data);
        }
    }, $scope, 'Get Machine Commands');

    machineServiceResponseObservable.autoSubscribe(function (data) {
        if (data.Result == "Success") {
            handleMachinesCommandResponse(data.Data);
        }
    }, $scope, "Machine service response");

    machineCommandDoneObservable.autoSubscribe(function (data) {
            handleMachineCommandDoneResponse(data.Data, data.Result);
    }, $scope, 'Machine command done');

    $log.info("MachineCommandsCtrl.");

    $scope.isMachinePacksizeTrackBasedMachine = function (machine) {
        return machine.MachineType == "Fusion" || machine.MachineType == "Em";
    };

    $scope.hasCorrugateLoaded = function (track){
        return track.LoadedCorrugate != undefined && track.LoadedCorrugate != null && track.TrackOffset > 0.0;
    }

    $scope.allowMachineCommands = function (machine) {
        if (machine.Data.CurrentStatus == 'MachinePaused' || machine.Data.CurrentStatus == 'MachineInError')
            return true;

        awaitingPause = true;
        return false;
    };

    $scope.allowNewCommand = function (machineId){
        return $.inArray(machineId, $scope.machinesPerformingCommands) == -1;
    };

    $scope.sendCommand = function (machineId, trackNumber, command){
        $log.info('MachineCommandsCtrl.Sending... ');

        alertService.clearAll();
        $scope.machinesPerformingCommands.push(machineId);

        awaitingSendResponseId = machineId;

        messagingService.publish(command, { "MachineId" : machineId, "TrackNumber" : trackNumber });
        $timeout(function () { MachineCommandCtrlSendTimeout(machineId); }, 5000);
    };

    $scope.pauseMachine = function (machine) {
        $log.info('MachineCommandsCtrl.PauseMachine... ');
        messagingService.publish("ServicePerformCommand",
        {
            MachineId : machine.Id,
            Type : "Pause"
        });

        awaitingSendResponseId = machine.Id;

        $timeout(function () { MachineCommandCtrlSendTimeout(machine.Id); }, 2000);
    };

    function handleMachinesCommandResponse(machineId) {
        if (machineId == awaitingSendResponseId) {
            alertService.addSuccess(translationService.translate('MachineCommand.Message.SendSuccess'), 5000);
            awaitingSendResponseId = "";
        }
    };

    function handleMachineCommandDoneResponse(machineId, result){
        if ($.inArray(machineId, $scope.machinesPerformingCommands) != -1) {
            $scope.machinesPerformingCommands.splice($.inArray(machineId, $scope.machinesPerformingCommands), 1);
            if (result == "Success") {
                alertService.addSuccess(translationService.translate('MachineCommand.Message.PerformSuccess'), 5000);
            }
            else {
                alertService.addError(translationService.translate('MachineCommand.Message.PerformFailed'), 5000);
            }
        }
    };

    function handleMachinesUpdated(currentMachineGroup) {
        if (awaitingPause) {
            awaitingPause = false;
        }
        master = currentMachineGroup;
        
        delete $scope.currentMachineGroup;
        $scope.currentMachineGroup = master;
    };

    function MachineCommandCtrlSendTimeout(machineId) {
        if (awaitingSendResponseId != "") {
            $log.info('MachineCommandCtrl.send timeout expired save failed');
            alertService.addError(translationService.translate('MachineCommand.Message.SendTimeout'), 5000);
            awaitingSendResponseId = "";
            $scope.machinesPerformingCommands.splice($.inArray(machineId, $scope.machinesPerformingCommands), 1);
        }
    }
    
    $scope.tellMeAboutThisPage = function () {
    	var steps = [];
    	var element;

    	steps.push({
    		element: "#notFound",
    		intro: translationService.translate('MachineCommand.Help.Overview')
    	});

    	element = $("div[id^='pauseMachineWrapper']:not(.ng-hide):first > button[id^='pauseMachine']");

    	if (element.length > 0) {
    		steps.push({
    			element: "#" + element[0].id,
    			intro: translationService.translate('MachineCommand.Help.PausePlay')
    		});
    	}

    	element = $("div[id^='machineTracksWrapper']:not(.ng-hide):first button[id^='forwardFeedTrack']:first");

    	if (element.length > 0) {
    		steps.push({
    			element: "#" + element[0].id,
    			intro: translationService.translate('MachineCommand.Help.RollForward')
    		});
    	}

    	element = $("div[id^='machineTracksWrapper']:not(.ng-hide):first button[id^='backwardFeedTrack']:first");

    	if (element.length > 0) {
    		steps.push({
    			element: "#" + element[0].id,
    			intro: translationService.translate('MachineCommand.Help.RollBackward')
    		});
    	}

    	element = $("div[id^='machineTracksWrapper']:not(.ng-hide):first button[id^='cleanCutTrack']:first");

    	if (element.length > 0) {
    		steps.push({
    			element: "#" + element[0].id,
    			intro: translationService.translate('MachineCommand.Help.CleanCut')
    		});
    	}

    	return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
}