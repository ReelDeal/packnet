/* This controller is responsible for:
    A - Determining the state of the machines in a machine group and whether they are in suitable state 
        for corrugate change.
    B - Allowing the user to change the corrugates on a machine using a corrugate from the production group.
    C - Sending the data to the machines about the corrugate change
    D - Handling edge cases related to Fusion vs. Em differences

Expected Behaviors:
    1 - The pause button is displayed only when the cut-crease machine is truly online
    2 - A machine in any error state should have the corrugate dropdowns available for the user to change corrugate 
        (do not dead-end the user)
    3 - If the pause button for the machine is clicked and the pause command is executed successfully 
        (machine gets paused) then the pause button should not be displayed and the corrugate dropdowns should be enabled
    4 - This controller deals with only _1_ machine group at a time. As the UI constantly needs data from the MG, 
        that MG should be on scope or at least important states should be.
    5 - The cancel button should take the user back to the operator panel for the MG
    6 - The save button should send the new corrugate info to the machine when it is in an acceptable state.
    7 - If the machine is in error it is allowable to send new corrugate data to it, 
        but we should not use a timeout in this case.

Current limitations:
    1 - Currently this controller does not limit corrugates to the production group in an 'official' way. 
        We should still determine if the corrugates from the PG are a snapshot - that is will 
        they still work when an admin goes and changes the corrugates out from under the PG.
    2 - Pause commands sent are not really tracked individually in terms of us flagging 
        whether we are waiting for an update
    3 - No timeout is used for a save corrugate operation unless _all_ machines in the MG are error free.
*/
function ChangeCorrugatesCtrl($log, $scope, $timeout, $state, alertService, helperService, introJsService, machineGroupService, machineService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.currentMachineGroup = machineGroupService.getCurrentMachineGroup();
    $scope.currentMachineGroup.Machines = Enumerable.From($scope.currentMachineGroup.Machines).OrderBy("$.Alias").ToArray();
    $scope.canSave = false;
    $scope.displayPauseButton = false;
    $scope.enablePauseButton = false;
    var awaitingUpdate = false;
    var pauseCommandsSentDictionary = new Array();
    var timeoutInMilliseconds = 60000;
    var emMachineType = "Em";
    var fusionMachineType = "Fusion";
    var noCorrugatesLoadedStatus = "NoCorrugatesLoaded";
    var machineOfflineStatus = "MachineOffline";
    var machineInitializingStatus = "MachineInitializing";
    var machineErrorStatus = "MachineError";
    var machineEmergencyStop = "MachineEmergencyStop";
    var machineInOutOfCorrugateStatus = "MachineInOutOfCorrugate";
    var machineChangingCorrugateStatus = "MachineChangingCorrugate";
    var machinePausedStatus = "MachinePaused";
    var machineOnlineStatus = "MachineOnline";
    var notLoadedLabel = translationService.translate('Common.Label.NotLoaded');
    
    $log.info("current MG is: " + $scope.currentMachineGroup.Alias);
    $log.info("ChangeCorrugatesCtrl.changeCorrugate");

    if ($scope.currentMachineGroup.ProductionGroup == undefined) {
        $state.go('OperatorConsole.ChangeProductionGroup');
        alertService.addError(translationService.translate('MachineProduction.Message.ProductionGroupNotAssigned'));
    }

    $scope.offsetChange = function (){
        $scope.canSave = true;
    }

    function handlePauseStatusChangeForMachineGroup(currentMachineGroup) {
        //Do this for only this machine group
        if ($scope.currentMachineGroup.Id === currentMachineGroup.Id) {
            //Get all the machines from the inbound machine group message
            handleMachineUpdates(currentMachineGroup.Machines);
        }
    }

    function updateTracksChangingCorrugateState(tracksFromScope, tracksFromMessage){
        Enumerable.From(tracksFromScope).ForEach(function (trackFromScope) {
            var trackFromMessage = helperService.linq.getItemByField(tracksFromMessage, trackFromScope.TrackNumber, "TrackNumber");
            if (trackFromMessage) {
                trackFromScope.IsInChangingCorrugateState = trackFromMessage.IsInChangingCorrugateState;
            }
        });
    }

    function handleMachineUpdates(machinesFromMessage) {
        $timeout(function () {
            if ($scope.currentMachineGroup && $scope.currentMachineGroup.Machines) {
                //Look at the currentStatus on each machine in the incoming message and assign that status to the correct machine in the $scope.currentMachineGroup
                Enumerable.From($scope.currentMachineGroup.Machines)
                    .ForEach(function (machineFromScope) {
                        var machineFromMessage = helperService.linq.getItemById(machinesFromMessage, machineFromScope.Id);
                        if (machineFromMessage) {
                            machineFromScope.Data.CurrentStatus = machineFromMessage.Data.CurrentStatus;
                            machineFromScope.Data.CurrentProductionStatus = machineFromMessage.Data.CurrentProductionStatus;
                            // Only update the field "IsInChangingCorrugateState" on the tracks
                            updateTracksChangingCorrugateState(machineFromScope.Data.Tracks, machineFromMessage.Data.Tracks);
                        }
                    });
            }
        }, 0, true);
    }

    function handleMessagingServiceMachineUpdate(msg) {
        handleMachineUpdates(msg.Data);
    }

    function handleNoCorrugatesLoadedError() {
        //Warn the user that there are no corrugates loaded if that's the case in our machine group
        if ($scope.currentMachineGroup && $scope.currentMachineGroup.Errors) {
            for (var x = 0; x < $scope.currentMachineGroup.Errors.length; x++) {
                if ($scope.currentMachineGroup.Errors[x] === noCorrugatesLoadedStatus) {
                    alertService.addError(translationService.translate('ChangeCorrugate.Message.NoCorrugateLoadedOnMachine'));
                }
            }
        }
    }

    function handleMoMachinesError()
    {
        if (!Enumerable.From($scope.currentMachineGroup.Machines)
        .Any(function (machine) {
            return $scope.isMachinePacksizeTrackBasedMachine(machine.Data);
        })) {
            alertService.addError(translationService.translate('ChangeCorrugate.Message.NoMachinesWithTracks', $scope.currentMachineGroup.Alias));
        }
    }

    function handleMachineUpdatesForCurrentMachineGroup(currentMachineGroup){
        $log.info("currentMachineGroup: " + currentMachineGroup.Alias);
        $log.info("$scope.currentMachineGroup: " + $scope.currentMachineGroup.Alias);
        handleMoMachinesError();
        handleNoCorrugatesLoadedError();
        handlePauseStatusChangeForMachineGroup(currentMachineGroup);
    }

    function shouldAllowToPauseMachine(machine, track) {

        if (machine.Data.MachineType === emMachineType) {
            return machine.Data.CurrentStatus === machinePausedStatus || (track != null && !track.IsInChangingCorrugateState && machine.Data.CurrentStatus === machineChangingCorrugateStatus);
        }

        return machine.Data.CurrentStatus === machineOnlineStatus;
    }

    $scope.shouldMachineDisplayPauseButton = function (machine, track) {
        //Given a machine determine if it should _display_ a pause button in the UI
        if (machine == null) {
            return false;
        }
            //If the machine is a PS cut-crease machines and truly online then we should show the pause button
            var isMachinePacksizeTrackBasedMachine = $scope.isMachinePacksizeTrackBasedMachine(machine);
            var allowMachinePause = shouldAllowToPauseMachine(machine, track);

            return isMachinePacksizeTrackBasedMachine && allowMachinePause && (track == null ? true : !track.IsInChangingCorrugateState);
    }

    $scope.shouldMachineDisablePauseButton = function (machine) {
        //Given a machine determine if it should _disable_ the pause button in the UI
        if (machine == null) {
            return false;
        }

        //At risk of some duplicated logic, leave nothing to coincidence here
        //It must be a PS track-based machine and be in offline or initializing state to justify disabling the pause button
        return $scope.shouldMachineDisplayPauseButton(machine) && ($scope.isMachineOffline(machine) || $scope.isMachineInitializing(machine));
    }

    $scope.isMachinePaused = function (machine) {
        var paused = false;

        if ($scope.isMachinePacksizeTrackBasedMachine(machine)) {

            //handle the case for brake release with the Em machines
            if (machine.MachineType === emMachineType) {
                paused = machine.Data.CurrentStatus !== machineChangingCorrugateStatus;
            }
            else if (machine.MachineType === fusionMachineType) {
                paused = machine.Data.CurrentStatus !== machinePausedStatus;
            }
        }

        return paused;
    }

    $scope.isMachineInitializing = function (machine) {
        return $scope.isMachinePacksizeTrackBasedMachine(machine) && machine.Data.CurrentStatus === machineInitializingStatus;
    }

    $scope.isMachineOffline = function (machine) {
        return $scope.isMachinePacksizeTrackBasedMachine(machine) && machine.Data.CurrentStatus === machineOfflineStatus;
    }

    $scope.shouldMachineDisableChangeCorrugateButton = function (machine)
    {
        var disabled = $scope.shouldMachineDisablePauseButton(machine)
            || machine.Data.CurrentProductionStatus === "ProductionInProgress";
        return disabled;
    }

    machineService.allMachinesObservable.autoSubscribe(handleMachineUpdates, $scope, 'Current Machine Updated');
    messagingService.machinesObservable.autoSubscribe(handleMessagingServiceMachineUpdate, $scope, 'Current Machines Updated');
    machineGroupService.currentMachineGroupUpdatedObservable.autoSubscribe(handleMachineUpdatesForCurrentMachineGroup, $scope, 'Current Machine Group Updated');

    function fixOffsetsIfNeeded(machine) {
        //When a track offset has nothing 
        for (var i = 0; i < machine.Tracks.length; i++) {
            var curTrack = machine.Tracks[i];
            if (curTrack.TrackOffset === '') {
                curTrack.TrackOffset = 0;
            }
        }
    }

    function changeCorrugatesCtrlSaveTimeout() {
        if (awaitingUpdate) {
            $log.info('ChangeCorrugatesCtrl.save timeout expired save failed');
            alertService.addError(translationService.translate('ChangeCorrugate.Message.SaveTimeout'));
            $scope.canSave = true;
        }
    }

    function pauseMachineTimeout(machine) {
        if (machine in pauseCommandsSentDictionary && awaitingUpdate) {
            $log.info('ChangeCorrugatesCtrl.PauseMachine timeout expired for machine: ' + machine);
            delete pauseCommandsSentDictionary[machine];
            alertService.addError(translationService.translate('ChangeCorrugate.Message.PauseTimeout'));
        }
    }

    $scope.isMachinePacksizeTrackBasedMachine = function (machine) {
        return machine.MachineType === fusionMachineType || machine.MachineType === emMachineType;
    };

    $scope.assignCorrugateToTrack = function (machine, track, corrugate) {
        track.LoadedCorrugate = corrugate;
        $scope.canSave = true;

        // validate that we should let them save and do not allow same corrugate loaded on two tracks.
        var tracks = Enumerable.From(machine.Data.Tracks);

        var nonNullCorrugates = tracks
            .Where("$.LoadedCorrugate != null && $.LoadedCorrugate.Alias != '" + notLoadedLabel + "'")
            .Count();

        var loadedCorrugates = tracks
            .Where("$.LoadedCorrugate != null && $.LoadedCorrugate.Alias != '" + notLoadedLabel + "'")
            .Select("$.LoadedCorrugate.Id")
            .Distinct()
            .Count();

        alertService.clearAll();

        if (nonNullCorrugates !== loadedCorrugates) {
            alertService.addError(translationService.translate('ChangeCorrugate.Validation.SelectionMustBeDifferent', machine.Alias));
            $scope.canSave = false;
            return;
        }
    };

    $scope.showDropDown = function () {
        return $scope.corrugates && $scope.corrugates.length > 0;
    }

    messagingService.corrugatesObservable.autoSubscribe(function (msg) {
        if ($scope.currentMachineGroup.ProductionGroup != undefined) {
        var configuredCorrugates = Enumerable.From($scope.currentMachineGroup.ProductionGroup.ConfiguredCorrugates);
            $timeout(function () {

                    //delete $scope.corrugates;
                    $scope.corrugates = Enumerable.From(msg.Data)
                        .Where(function (c) {
                            return configuredCorrugates.Any(function (cc) { return cc === c.Id; });
                        })
                        .ToArray();

                    if ($scope.corrugates.length === 0) {
                        alertService.addError(translationService.translate('ChangeCorrugate.Message.NoCorrugateInProductionGroup',
                            $scope.currentMachineGroup.ProductionGroup.Alias));
                    }
                    $scope.corrugates.unshift({ Alias : notLoadedLabel });
                },
                0);

        }
    }, $scope, 'Available Corrugates');

    messagingService.assignCorrugatesResponseObservable.autoSubscribe(function (msg) {
        if (msg.MachineGroupId === $scope.currentMachineGroup.Id) {
            awaitingUpdate = false;

            if (msg.Result === "Success") {
                alertService.addSuccess(translationService.translate('ChangeCorrugate.Message.SaveSuccess'), 3000);
                return;
            }

            var errors = msg.Data;
            var messages = [];

            Enumerable.From(errors).ForEach(function (error) {
                messages.push(translationService.translate('ChangeCorrugate.Message.' + error.ErrorMessageType, error.MachineAlias, error.TrackNumbers.toString()));
            });

            if (msg.Result === "PartialSuccess") {
                alertService.addWarning(Enumerable.From(messages).Aggregate(function (acc, mess) {
                    //TODO: Unable to understand what message needs to be localized
                    return acc + " and " + mess;
                }));
                return;
            }

            if (msg.Result === "Fail") {
                alertService.addError(Enumerable.From(messages).Aggregate(function (acc, mess) {
                    //TODO: Unable to understand what message needs to be localized
                    return acc + " and " + mess;
                }));
            }
        }
    }, $scope, 'Assign corrugates response');

    messagingService.publish("GetCorrugates", "");

    $scope.allowChangeCorrugate = function (machine, track) {

        if (machine.CurrentStatus === machineOfflineStatus ||
            machine.CurrentStatus === machineErrorStatus ||
            machine.CurrentStatus === machineEmergencyStop ||
            machine.CurrentStatus === machineInOutOfCorrugateStatus) {
            return true;
        }

        if (machine.MachineType === emMachineType) {
            return machine.CurrentStatus === machineChangingCorrugateStatus && track.IsInChangingCorrugateState;
        }

        if (machine.MachineType === fusionMachineType) {
            return machine.CurrentStatus === machinePausedStatus;
        }
        
        return false;
    };

    function isMachineInError(machine) {
        //Is the machine in an error of any kind?
        return machine != null
            && $scope.isMachinePacksizeTrackBasedMachine(machine)
            && machine.Data.CurrentStatus.toLowerCase().contains("error");
    }

    function areAllMachinesErrorFree() {
        for (var i = 0; i < $scope.currentMachineGroup.Machines.length; i++) {
            if (isMachineInError($scope.currentMachineGroup.Machines[i])) {
                return false;
            }
        }

        return true;
    }

    $scope.saveCorrugates = function (machines) {
        var machinesWithTracks = Enumerable.From(machines)
            .Where(function (machine) {
                return $scope.isMachinePacksizeTrackBasedMachine(machine.Data);
            }).Select("m=>m.Data");
        machinesWithTracks.ForEach(fixOffsetsIfNeeded);
        $log.info('ChangeCorrugatesCtrl.Saving... ');
        awaitingUpdate = true;
        $scope.canSave = false;
        messagingService.publish("AssignCorrugates", machinesWithTracks.ToArray());

        //We only want to use a timeout check for saving corrugate when all machines are NOT in error. Reason: When a machine is in error corrugate data can be written to it
        //but the user must clear the error to get up and running again and that could be 3 seconds or 3 hours. As we are relying on another message coming back to confirm the
        //the change so don't bother with it and run the risk of throwing up a superfluous timeout that causes confusion.
        if (areAllMachinesErrorFree()) {
            $timeout(function () {
                changeCorrugatesCtrlSaveTimeout();
        }, timeoutInMilliseconds);
        }
    };

    $scope.leaveChangeCorrugate = function (machines) {
        //Deal with an em-specific machine scenario - I believe this has to do with brake release/engage
        var emMachines = Enumerable.From(machines)
            .Where(function (machine) {
                return machine.Data.MachineType === emMachineType && machine.Data.CurrentStatus === machineChangingCorrugateStatus;
            }).Select("m=>m.Id").ToArray();

        messagingService.publish("LeaveChangeCorrugate", emMachines);
    };

    $scope.pauseMachine = function (machine) {
        messagingService.publish("EnterChangeCorrugate", machine.Id);

        //Track in a dictionary what machines we have sent pause commands to
        if (!(machine.Id in pauseCommandsSentDictionary)) {
            pauseCommandsSentDictionary.push(machine.Id);
        }

        awaitingUpdate = true;
        $timeout(function () {
            pauseMachineTimeout(machine.Id);
        }, timeoutInMilliseconds);
    };

    $scope.pauseMachineTrack = function (machine, track){
        messagingService.publish("EnterChangeCorrugateTrack", { "Item1": machine.Id, "Item2": track.TrackNumber });

        var key = machine.Id + '_' + track.TrackNumber;

        //Track in a dictionary what machines we have sent pause commands to
        if (!(key in pauseCommandsSentDictionary)) {
            pauseCommandsSentDictionary.push(key);
        }
        awaitingUpdate = true;
        $timeout(function () {
            pauseMachineTimeout(key);
        }, timeoutInMilliseconds);

    }

    //BEGIN User help stuff
    $scope.tellMeAboutThisPage = function () {
        var steps = [];
        var element;

        steps.push({
            element: "#notFound",
            intro: translationService.translate('ChangeCorrugate.Help.Overview')
        });

        steps.push({
            element: "#machines",
            intro: translationService.translate('ChangeCorrugate.Help.Machines')
        });

        element = $("[id^='machineName']:first");

        if (element.length > 0) {
            steps.push({
                element: "#" + element[0].id,
                intro: translationService.translate('ChangeCorrugate.Help.Machine')
            });
        }

        element = $("[id^='pauseMachine']:not(.ng-hide):first");

        if (element.length > 0) {
            steps.push({
                element: "#" + element[0].id,
                intro: translationService.translate('ChangeCorrugate.Help.PausePlay')
            });
        }

        element = $("button[id^='machineTracks']:first");

        if (element.length > 0) {
            steps.push({
                element: "#" + element[0].id,
                intro: translationService.translate('ChangeCorrugate.Help.Track')
            });
        }

        steps.push({
            element: "#SaveCorrugateButton",
            intro: translationService.translate('ChangeCorrugate.Help.Save')
        });

        steps.push({
            element: "#CloseCorrugateButton",
            intro: translationService.translate('ChangeCorrugate.Help.Cancel')
        });

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
    //END User help stuff
}