function ReproduceCartonDialogCtrl($modalInstance, $scope, currentCarton, reproducer, translationService) {
    
    $scope.currentCarton = currentCarton;

    $scope.ok = function () {
        reproducer($scope.currentCarton);
        $modalInstance.close();
    }

    $scope.cancel = function () {
        $modalInstance.close();
    }
}

function MachineGroupProductionHistoryListCtrl($log, $modal, $scope, $timeout, messagingService, producibleHelperService, NOTIFICATIONS_EVENTS, translationService) {
    var numberOfRowsInGrid = 10;

    $scope.history = Enumerable.Range(1, numberOfRowsInGrid).Select(function () { return {}; }).ToArray();

    function handleMachineHistory(data) {
        var myProducibles = Enumerable.From(data).Where(function (d) { return d.ProducedOnMachineGroupId == $scope.currentMachineGroup.Id }).ToArray();

        producibleHelperService.setProduciblesCompletedList(myProducibles);

        
        delete $scope.history;
        $scope.history = Enumerable.From(myProducibles)
            .Select(function (c) {
                c.ProductionStatusTime = moment.utc(c.ProductionStatusTime, 'YYYY-MM-DDTHH:mm:ss.SSS Z').local().format('YYYY-MM-DD HH:mm:ss');

                if (c.Producible) c.ProducibleType = c.Producible.ProducibleType;

                c.UserFriendlyProducibleStatus = translationService.translate('ProducibleStatus.Label.' + c.ProducibleStatus);
                c.UserFriendlyProducibleType = translationService.translate('ProducibleType.Label.' + c.ProducibleType);

                var carton = producibleHelperService.findFirstCarton(c);

                if (carton) {
                    c.Length = carton.Length;
                    c.Width = carton.Width;
                    c.Height = carton.Height;
                    c.DesignId = carton.DesignId;
                }
                return c;
            })
            .Take(20) // Show only 20
            .ToArray();
    }

    function handleProducibleStatusChanged(msg) {
        $timeout(function () {

            // User was logout. 
            if ($scope.currentMachineGroup) {
                if (msg.Data.ProducedOnMachineGroupId !== $scope.currentMachineGroup.Id) {
                    // When an item is reproduced it is left in the history unless another machine starts working on it, if that happens we will remove it from the list
                    var tmp = Enumerable.From($scope.history)
                        .Where(function (producible) {
                            return producible.Id && (producible.Id !== msg.Data.Id);
                        })
                        .ToArray();
                    if (tmp.length > 0) {
                        // Remove the producible from the list.

                        delete $scope.history;
                        $scope.history = tmp;
                    }
                }
            }
        }, 1000, true);
    }
    
    messagingService.producibleStatusChangedObservable
        .autoSubscribe(handleProducibleStatusChanged, $scope, 'Producible Status Changed');

    $scope.$on(NOTIFICATIONS_EVENTS.producibleCompleted, function () {
        $timeout(function () {
            handleMachineHistory(producibleHelperService.getProduciblesCompleted());
        }, 0, true);
    });
    
    var fixStoopidGridDrawring = function () {
        $timeout(function () {
        $(window).resize();
        }, 0, true);
    }

    //UI-grid is not resizing correctly either :-). Old NG grid issue: https://github.com/angular-ui/ng-grid/issues/50
    $scope.$watch('history', function () {
        fixStoopidGridDrawring();
    });

    messagingService.machineProductionHistoryObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            handleMachineHistory(msg.Data);
        }, 0, true);
    }, $scope, 'Machine History');

    //Ask for initial set of data
    messagingService.publish("GetProductionHistory", "");

    // User specified broken box
    $scope.completeBrokenBox = function (producible) {
        $log.info("Flagging Broken: ", producible);
        producible.Broken = 1;
        messagingService.publish("ReproduceCarton", { CustomerUniqueId: producible.CustomerUniqueId, ProducedOnMachineGroupId: $scope.currentMachineGroup.Id });
    };

    $scope.showBrokenBoxButton = function (producible) {
        //Broken property set in completeBrokenBox() above
        return producible.CustomerUniqueId !== undefined && !producible.Broken == 1;
    };

    $scope.openReproduceDialog = function (producible) {
        
        delete $scope.opts;
        $scope.opts = {
            backdrop: true,
            keyboard: true,
            templateUrl: 'partials/Operator/reproduceCartonDialog.html',
            controller: 'ReproduceCartonDialogCtrl',
            windowClass: "smallDialogWindow",
            resolve: {
                currentCarton: function () {
                    return producible;
                },
                reproducer: function () {
                    return $scope.completeBrokenBox;
                }
            }
        };

        $modal.open($scope.opts);
    };

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'history',
        headerRowHeight: 26,
        rowHeight: 38,
        columnDefs: [
            {
                field: 'nothing',
                displayName: translationService.translate('Common.Label.Action'),
                enableColumnResizing: false,
                cellTemplate: '<button id="completedJobReproduceButton" class="btn btn-default center-block" style="margin-top:2px" ng-if="grid.appScope.showBrokenBoxButton(row.entity)" ng-click="grid.appScope.openReproduceDialog(row.entity)" translate>Common.Label.Reproduce</button>',
                width: 110
            },
            {
                field: 'UserFriendlyProducibleStatus',
                displayName: translationService.translate('Common.Label.Status'),
                width: 200
            },
            {
                field: 'CustomerUniqueId',
                displayName: translationService.translate('Common.Label.CustomerId'),
                sortingAlgorithm: function (a, b) {

                    if (isNaN(a) && isNaN(b)) {
                        return a === b ? 0 : (a < b ? -1 : 1);
                    }

                    if (!isNaN(a) && !isNaN(b)) {
                        var idA = parseFloat(a);
                        var idB = parseFloat(b);

                        return idA === idB ? 0 : (idA < idB ? -1 : 1);
                    }

                    return 0;
                }
            },
            {
                field: 'UserFriendlyProducibleType',
                displayName: translationService.translate('Common.Label.Type'),
                width: 150
            },
            {
                field: 'Length',
                displayName: translationService.translate('Common.Label.LengthAcronym'),
                width: 60
            },
            {
                field: 'Width',
                displayName: translationService.translate('Common.Label.WidthAcronym'),
                width: 60
            },
            {
                field: 'Height',
                displayName: translationService.translate('Common.Label.HeightAcronym'),
                width: 60
            },
            {
                field: 'DesignId',
                displayName: translationService.translate('Common.Label.DesignId'),
                width: 100
            }
        ]
    };
}

app.directive('machineHistory', function () {
    {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'partials/Operator/machineGroupHistoryList.html'
        };
    }
});