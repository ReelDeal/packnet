function OperatorPanelCtrl($scope, $state, $timeout, alertService, machineGroupService, messagingService, translationService) {

    $timeout = $timeout.getInstance($scope);

    var shouldBypassAssignedOperator = ($state.params && ($state.params.bypassOperator == "true" || $state.params.bypassOperator == true));
    
    $scope.currentMachineGroup = machineGroupService.getCurrentMachineGroup(shouldBypassAssignedOperator);

    var machinesMissingCorrugate = [];

    var noCorrugateAvailableObservable = messagingService.messageSubject
        .where(messagingService.filterObs("NoCorrugateAvailable"));

    //leave this function definition here or the page will have severe issues rendering or trying to call it in our check
    //before we setup anything else
    var areAllTracksEmptyOnAnyMachine = function (){
        if (!$scope.currentMachineGroup || !$scope.currentMachineGroup.Machines) {
            return false;
        }

        if (Enumerable.From($scope.currentMachineGroup.Machines).Any(function (m){
                if (m.Data.NumTracks) {
                    var allEmptyTracks = Enumerable.From(m.Data.Tracks).All(
                        function (track)
                        {
                            return track.LoadedCorrugate == null;
                        }
                    );
                    if (allEmptyTracks) {
                        machinesMissingCorrugate.push(m);
                    }

                    return allEmptyTracks;
                }
                return false;
            }
        ))
        {
            return true;
        }

        return false;
    }

    //We have our current machine group so check _now_ if we need to have them load corrugate
    if (areAllTracksEmptyOnAnyMachine()) {
        $state.go('OperatorConsole.ChangeCorrugates');
        //Let's be intentionally defensive here so we don't get 'undefined' stuff in the UI
        var machine = machinesMissingCorrugate[machinesMissingCorrugate.length - 1];
        if (machine != null) {
            alertService.addError(translationService.translate('MachineProduction.Message.MachineNameHasNoCorrugate', machine.Alias));
        }
        else {
            alertService.addError(translationService.translate('MachineProduction.Message.MachineHasNoCorrugate'));
        }
        //machinesMissingCorrugate.length = 0;
    }

    $scope.showMachineGroupDetails = false;
    $scope.TriggerData = { serialNumber: '' };

    //Todo: Use current Machine group observable here
    machineGroupService.machineGroupsUpdatedObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            //delete $scope.currentMachineGroup;
            $scope.currentMachineGroup = machineGroupService.getCurrentMachineGroup(shouldBypassAssignedOperator);
            if ($scope.currentMachineGroup &&
                $scope.currentMachineGroup.CurrentStatus === 'MachineGroupOffline' &&
                $scope.currentMachineGroup.CurrentStatus !== 'MachineGroupOffline' &&
                $scope.currentMachineGroup.CurrentStatus !== 'MachineGroupInitializing')
                alertService.addWarning(translationService.translate('MachineProduction.Message.AttachMachineToWork'));
            else if ($scope.currentMachineGroup && $scope.currentMachineGroup.ProductionGroupAlias === '')
                alertService.addWarning(translationService.translate('MachineProduction.Message.AddMachineGroupToProductionGroupToWork'));

            $scope.shouldShowTriggerInterface();
        }, 10, true);
    }, $scope, 'Machines Updated');

    noCorrugateAvailableObservable.autoSubscribe(function (data) {
        if ($scope.currentMachineGroup && $scope.currentMachineGroup.Id == data.ProducedOnMachineGroupId) {
            alertService.addError(translationService.translate('MachineProduction.Message.UnavailableCorrugateToProduce', data.CustomerUniqueId));
        }
    }, $scope, "No Corrugate Available");

    messagingService.scanTriggerErrorObservable.autoSubscribe(function (msg) {
        if ($scope.currentMachineGroup.ProductionGroup && $scope.currentMachineGroup.ProductionGroup.Alias == msg.Data.ProductionGroup) {
            alertService.addError(msg.Data.Message, 5000);
        }
    }, $scope, 'Scan Trigger Error');

    messagingService.scanTriggerSuccessObservable.autoSubscribe(function (msg) {
        if ($scope.currentMachineGroup.ProductionGroup && $scope.currentMachineGroup.ProductionGroup.Alias == msg.Data.ProductionGroup) {
            alertService.addSuccess(msg.Data.Message, 5000);
        }
    }, $scope, 'Scan Trigger Success');

    var producibleStatusChangedObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ProducibleStatusChanged"));

    producibleStatusChangedObservable.autoSubscribe(handleProducibleStatusChanged, $scope, 'Producible Status Changed');

    function handleProducibleStatusChanged(job) {
        if (job.ProducibleStatus == "ProducibleFailed") {
            alertService.addError(translationService.translate('MachineProduction.Message.ProducibleFailed', job.CustomerUniqueId));
        }
    }

    var selectionAlgorithmIsOrder = function(){

        return $scope.currentMachineGroup.ProductionGroup.SelectionAlgorithm === 'Order' || $scope.currentMachineGroup.ProductionGroup.SelectionAlgorithm === 'ScanToCreate';
    }

    $scope.shouldShowTriggerInterface = function () {
        if (!$scope.currentMachineGroup || !$scope.currentMachineGroup.ProductionGroup) return false;

        return $scope.currentMachineGroup.ProductionGroup.Options.ShowTriggerInterface === "True";
    }

    $scope.shouldShowWaveView = function () {
        if ($scope.showMachineGroupDetails ||
            !$scope.currentMachineGroup ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupOffline' ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupNotConfigured' ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupInitializing' ||
            isInProductionGroupEmpty() ||
            areAllTracksEmptyOnAnyMachine() ||
            areAnyMachineErrors()
            )
            return false;

        var shouldShowWave = selectionAlgorithmIsOrder() === false && $scope.currentMachineGroup.ProductionMode === 'AutoProductionMode';
        return shouldShowWave;
    };
    $scope.shouldShowOrderView = function () {
        if ($scope.showMachineGroupDetails ||
            !$scope.currentMachineGroup ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupNotConfigured' ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupOffline' ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupInitializing' ||
            isInProductionGroupEmpty() ||
            areAllTracksEmptyOnAnyMachine() ||
            areAnyMachineErrors()
            )
            return false;

        var shouldShowOrders = selectionAlgorithmIsOrder() || $scope.currentMachineGroup.ProductionMode === 'ManualProductionMode';
        shouldShowOrders = shouldShowOrders && $scope.currentMachineGroup.ProductionGroup != '';

        return shouldShowOrders;
    };

    var areAnyMachineErrors = function () {
        return (Enumerable.From($scope.currentMachineGroup.Machines).Any(function (machine) {
            var result = machine.Data.Errors && machine.Data.Errors.length > 0;
            return result;
        }));
    }

    var isInProductionGroupEmpty = function (){
        var alertMessage = translationService.translate('MachineProduction.Message.MachineGroupNotAssignedToProductionGroup');
        if (!$scope.currentMachineGroup.ProductionGroup) {
            alertService.addError(alertMessage);
            $state.go('OperatorConsole.ChangeProductionGroup');
            return true;
        }
        else {
            alertService.closeByMessage(alertMessage);
            return false;
        }
    }

    $scope.canShowMachineGroupDetails = function () {
        if (!$scope.currentMachineGroup ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupNotConfigured' ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupOffline' ||
            $scope.currentMachineGroup.CurrentStatus === 'MachineGroupInitializing' ||
            isInProductionGroupEmpty() ||
            areAllTracksEmptyOnAnyMachine() ||
            areAnyMachineErrors()
            ) {
            return false;
        }
        return true;
    }

    $scope.shouldShowMachineGroupDetails = function () {
        if ($scope.showMachineGroupDetails ||
            !$scope.canShowMachineGroupDetails()) {
            return true;
        }
        return false;
    };

    //TODO: move this to it's own controller for triggering
    $scope.sendBarcodeData = function () {
        if (!$scope.TriggerData.serialNumber || $scope.TriggerData.serialNumber.length == 0) {
            alertService.addWarning(translationService.translate('MachineProduction.Wave.Validation.BarcodeDataRequired'), 5000);
            return;
        }

        var now = new Date();
        messagingService.publish("CreateCartonTrigger", {
            'BarcodeData': $scope.TriggerData.serialNumber,
            'ProductionGroupAlias': $scope.currentMachineGroup.ProductionGroup.Alias,
            'TriggeredOn': now
        });
        
        delete $scope.TriggerData.serialNumber;
        $scope.TriggerData.serialNumber = '';
        event.preventDefault();
    };

    /*Bug 12967: Begin of dirty fix */
    $scope.refreshProductionList = function () {
        messagingService.publish("GetMachineGroupProductionQueue", "");
    }

    $scope.refreshProductionHistoryList = function () {
        messagingService.publish("GetProductionHistory", "");
    }

    $scope.refreshOrdersList = function () {
        messagingService.publish("GetOrders", "");
}

    $scope.refreshOrdersHistoryList = function () {
        messagingService.publish("GetOrdersHistory", "");
    }
    /*Bug 12967: End of dirty fix */
}

app.directive('openOrdersView', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/SelectionAlgorithm/Orders/ordersList.html'
    };
});

app.directive('waveOperatorView', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/Operator/Wave/waveOperatorView.html'
    };
});

app.directive('ordersOperatorView', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/Operator/Orders/operatorView.html'
    };
});


app.directive('boxLastOperatorTriggerInterface', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/MachineManager/boxLastOperatorTriggerInterface.html',
    };
});