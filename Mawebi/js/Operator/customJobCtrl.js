function CustomJobCtrl($scope, $timeout, alertService, designPreviewService, helperService, introJsService, machineGroupService, messagingService, packagingDesignService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.floatingPointRegex = /^(\d+\.?\d*|\.\d+)$/;
    $scope.intRegex = /^\d+$/;
    $scope.model = { Custom: true, Length: null, Width: null, Height: null, DesignId: null, Quantity: 1, MachineId: 0, CorrugateQuality: null };

    $scope.minValue = 1;
    $scope.maxValue = 2000;
    
    $scope.currentMachine = machineGroupService.getCurrentMachineGroup();

    $scope.showDesignPreview = true;
    $scope.showPreviewContainer = false;
    $scope.firstAllowedRotation = "";
    $scope.model.selectedRotation = "";

    $scope.machines = null;
    $scope.awaitingUpdate = false;
    $scope.useKeyboardWedgeScanner = false;
    $scope.showCrossConveyorPickzone = false;

    var conveyorPiczoneCapability = "PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities.ConveyorPickzoneCapability, PackNet.IODeviceService";
    if (Enumerable.From($scope.currentMachine.AllMachineCapabilities).Any(function (c) { return c.$type == conveyorPiczoneCapability; })) {
        $scope.showCrossConveyorPickzone = true;
        
        delete $scope.pickZones;
        $scope.pickZones = Enumerable.From($scope.currentMachine.AllMachineCapabilities)
            .Where(function (c) { return c.$type == conveyorPiczoneCapability; })
            .Select('c => c.PickZone')
            .GroupBy('s => s.Alias')
            .Select('d => d.First()')
            .ToArray();

        if ($scope.pickZones.length == 1) {
            
            delete $scope.selectedPickZone;
            $scope.selectedPickZone = $scope.pickZones[0];
        }
    }

    messagingService.customCartonCreatedObservable.autoSubscribe(handleResponse, $scope, 'Custom Carton Created');
    messagingService.packagingDesignWithAppliedValuesObservable.autoSubscribe(handlePreviewResponse, $scope, 'Custom Carton Created');
    var executeProduce = function (){
        if ($scope.model.Length != null && $scope.model.Width != null && $scope.model.Height != null && !$scope.isProduceDisabled()) {
            $scope.produce($scope.customJobForm);
        }
    }

    $scope.designList = packagingDesignService.getAllDesigns();
    packagingDesignService.allDesignsObservable.autoSubscribe(function () {
        
        delete $scope.designList;
        $scope.designList = packagingDesignService.getAllDesigns();
    }, $scope, 'Get Designs');

    var scannerObservable = messagingService
        .messageSubject
        .where(messagingService.filterObs("ScanTriggerSuccess"))
        .select(messagingService.dataFunction)
        .autoSubscribe(function (data) {
            if (isNaN(data.BarcodeData)) {
                return;
            }
            var barcodeData = parseInt(data.BarcodeData);

            if ($scope.model.Length == null) {
                
                delete $scope.model.Length;
                $scope.model.Length = barcodeData;
            }
            else if ($scope.model.Width == null) {
                
                delete $scope.model.Width;
                $scope.model.Width = barcodeData;
            }
            else {
                
                delete $scope.model.Height;
                $scope.model.Height = barcodeData;
            }
            
            $scope.$apply();

        executeProduce();
    }, $scope, 'Custom Carton Created');
    
    $scope.onFieldKeyup = function (currentField, nextFieldName, event){
        if ($scope.useKeyboardWedgeScanner && event.keyCode == 13) {

            if (nextFieldName == 'Produce') {
                executeProduce();
            }
            else {
                if ($scope.model[currentField] != null) {
                    angular.element('#' + nextFieldName).trigger('focus');
                }
            }
        }
    }

    var isScannerConfigured = function () {
        if ($scope.useKeyboardWedgeScanner) {
            return true;
        }

        var scanner = Enumerable.From($scope.currentMachine.Machines).Where('c=>c.MachineType == "WifiBarcodeScanner"').FirstOrDefault();
        return (scanner != undefined && scanner.Data.NotifyBarcodeToUIOnManualMode == 1);
    }

    if (isScannerConfigured() && $scope.currentMachine.ProductionMode != "ManualProductionMode") {
        alertService.addWarning(translationService.translate('CustomJob.Message.ScannerOnAutoProductionMode'));
    }

    var currentValuesKept = false;

    var setModelToDefault = function(keepCurrentValues){
        $timeout(function () {
            if (isScannerConfigured()) {
                delete $scope.model.CustomerUniqueId;
                $scope.model.CustomerUniqueId = null;

                delete $scope.customerUniqueId;
                $scope.customerUniqueId = null;

                delete $scope.model.Length;
                $scope.model.Length = null;

                delete $scope.model.Width;
                $scope.model.Width = null;

                delete $scope.model.Height;
                $scope.model.Height = null;

                angular.element('#customJobLengthInputBox').trigger('focus');
            }
            else {
                if (keepCurrentValues) {
                    currentValuesKept = true;

                    delete $scope.model.CustomerUniqueId;
                    $scope.model.CustomerUniqueId = null;

                    delete $scope.customerUniqueId;
                    $scope.customerUniqueId = null;

                    angular.element('#CustomJob').trigger('focus');
                }
                else {
                    currentValuesKept = false;

                    $scope.model.Quantity = 1;

                    delete $scope.model.Length;
                    $scope.model.Length = null;

                    delete $scope.model.Width;
                    $scope.model.Width = null;

                    delete $scope.model.Height;
                    $scope.model.Height = null;

                    delete $scope.model.selectedRotation;
                    $scope.model.selectedRotation = "";

                    delete $scope.model.CorrugateQuality;
                    $scope.model.CorrugateQuality = null;

                    delete $scope.selectedPickZone;
                    $scope.selectedPickZone = null;

                    angular.element('#quantityInputBox').trigger('focus');
                }
            }

            $scope.customJobForm.$setPristine();
            $scope.$broadcast('show-errors-reset');
        }, 100, true);
    }

    function handlePreviewResponse(data){

        var design = helperService.linq.getItemById($scope.designList, $scope.selectedDesignId);

        var labels = [];

        design.Lines.map(function (line, index) { labels[index] = line.Label; });

        data.Data.Lines.map(function (line, index) { if (labels[index] != null) { line.Label = labels[index]; } });

        var container = $('#previewContainer');
        var width = $('#previewWrapper').width();
        var height = $('#customJobForm').height();
                
        designPreviewService.draw.drawPreview(data.Data, container, width, height);
    }

    function getCartonData(){
        
        delete $scope.model.MachineId;
        $scope.model.MachineId = $scope.currentMachine.Id;
        
        delete $scope.model.CustomerUniqueId;
        $scope.model.CustomerUniqueId = $scope.customerUniqueId;
        
        delete $scope.model.XValues;
        $scope.model.XValues = {};

        if ($scope.model.DesignParameters) {
            $.map($scope.model.DesignParameters, function (dp) {
                $scope.model.XValues[dp.Name] = dp.Value === "" ? 0 : dp.Value;
            });
        }

        var record = angular.fromJson(angular.toJson($scope.model));
        record.UserName = $scope.currentUser.UserName;
        delete record.Description;
        delete record.MachineType;
        delete record.LastUpdated;
        delete record.OptimalMachines;

        if (record.Length == null) {
            record.Length = 0;
        }

        if (record.Width == null) {
            record.Width = 0;
        }

        if (record.Height == null) {
            record.Height = 0;
        }

        return record;
    }

    function handleResponse(data) {
        if ($scope.awaitingUpdate) {
            $scope.awaitingUpdate = false;
            if (data.Result == "Success") {
                alertService.addSuccess(translationService.translate('CustomJob.Message.ProduceSuccess'), 5000);
                setModelToDefault(true);
            }
            else {
                alertService.addError(translationService.translate('CustomJob.Message.ProduceFailed'));
            }
        }
    };

    $scope.hidePreview = function (){
        $scope.showDesignPreview = !$scope.showDesignPreview;
        return $scope.showDesignPreview;
    }

    $scope.$watch('showDesignPreview', function (newValue) {
        
        delete $scope.labelPackageDesignPreview;
        $scope.labelPackageDesignPreview = newValue ? translationService.translate("Article.CartonProducible.Label.HidePreview") : translationService.translate("Article.CartonProducible.Label.ShowPreview");
    });

    $scope.updateSelection = function (designId) {
        
        delete $scope.model.DesignId;
        $scope.model.DesignId = designId;
        var designData = Enumerable.From($scope.designList).FirstOrDefault([], function (d) { return d.Id === designId; });

        
        delete $scope.model.AllowedRotations;
        $scope.model.AllowedRotations = designData.AllowedRotations;

        
        delete $scope.firstAllowedRotation;
        $scope.firstAllowedRotation = Enumerable.From(designData.AllowedRotations).FirstOrDefault([], function (d) { return d.Value.Allowed == 1; }).Key;

        
        delete $scope.model.selectedRotation;
        $scope.model.selectedRotation = "";

        
        delete $scope.model.DesignParameters;
        $scope.model.DesignParameters = $.map(designData.DesignParameters, function (dp) {
            return {
                Name: dp.Name,
                Alias: dp.Alias,
                Value: ""
            }
        });

        if (currentValuesKept) {
            setModelToDefault(false);
        }
    };

    $scope.$watch('model', function () {
        //only do this when design has been selected.
        if ($scope.model.DesignId == null) {
            $scope.showPreviewContainer = false;
            return;
        }

        $scope.showPreviewContainer = true;

        var carton = getCartonData();
        //Create cartonOnCorrugate from carton
        var coc = {
            "Rotation": $scope.model.selectedRotation,
            "OriginalCarton": carton
        }

        if ($scope.model.selectedRotation === "") {
            coc.Rotation = $scope.firstAllowedRotation;            
        }
        
        messagingService.publish("GetPackagingDesignWithAppliedValues", coc);
    }, true);


    $scope.produce = function (form) {
        alertService.clearAll();
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        $scope.awaitingUpdate = true;
        
        delete $scope.model.MachineId;
        $scope.model.MachineId = $scope.currentMachine.Id;
        
        delete $scope.model.CustomerUniqueId;
        $scope.model.CustomerUniqueId = $scope.customerUniqueId;
        
        delete $scope.model.XValues;
        $scope.model.XValues = {};

        $.map($scope.model.DesignParameters, function (dp) {
            $scope.model.XValues[dp.Name] = dp.Value;
        });

        var record = angular.fromJson(angular.toJson($scope.model));        
        record.UserName = $scope.currentUser.UserName;
        record.Rotation = $scope.model.selectedRotation;
        delete record.Description;
        delete record.MachineType;
        delete record.LastUpdated;
        delete record.OptimalMachines;

        if ($scope.selectedPickZone != null) {
            record.Restrictions = [{ "$type": 'PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions.ConveyorPickzoneRestriction, PackNet.IODeviceService', "Value": $scope.selectedPickZone }];
        }        

        var container = $('#previewContainer');
        container.empty();

        messagingService.publish("CreateCustomCarton", record); // has to be sent as an array
        $timeout(function () {
            if ($scope.awaitingUpdate) {
                alertService.addError(translationService.translate('CustomJob.Message.ProduceTimeout'), 5000);
                $scope.awaitingUpdate = false;
            }
        }, 10000);
    };

    $scope.isProduceDisabled = function () {
        return $scope.customJobForm.$invalid || $scope.model.Length == null;
    };

    $scope.isCancelDisabled = function () {
        return $scope.awaitingUpdate || angular.equals($scope.master, $scope.model);
    };

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        steps.push({
            element: "#notFound",
            intro: translationService.translate('CustomJob.Help.Overview')
        });

        steps.push({
            element: "#quantityInputBox",
            intro: translationService.translate('CustomJob.Help.Quantity')
        });

        steps.push({
            element: "#CustomJob",
            intro: translationService.translate('CustomJob.Help.OrderNumber')
        });

        steps.push({
            element: "#CustomJobDesignIdButton",
            intro: translationService.translate('CustomJob.Help.Design')
        });

        steps.push({
            element: "#customJobLengthInputBox",
            intro: translationService.translate('CustomJob.Help.CartonLength')
        });

        steps.push({
            element: "#customJobWidthInputBox",
            intro: translationService.translate('CustomJob.Help.CartonWidth')
        });

        steps.push({
            element: "#customJobHeightInputBox",
            intro: translationService.translate('CustomJob.Help.CartonHeight')
        });

        if ($scope.model && $scope.model.AllowedRotations != null) {
            steps.push({
                element: "#AllowedRotationsDropDown",
                intro: translationService.translate('CustomJob.Help.Rotation')
            });
        }

        steps.push({
            element: "#customJobCorrugateQualityInputBox",
            intro: translationService.translate('CustomJob.Help.Quality')
        });

        if ($scope.showCrossConveyorPickzone) {
            steps.push({
                element: "#crossConveyorPickZone",
                intro: translationService.translate('CustomJob.Help.CrossConveyorPickZone')
            });
        }

        steps.push({
            element: "#UseKeyboardWedgeScanner",
            intro: translationService.translate('CustomJob.Help.UseScanner')
        });

        steps.push({
            element: "#produce",
            intro: translationService.translate('CustomJob.Help.Produce')
        });

        steps.push({
            element: "#cancel",
            intro: translationService.translate('CustomJob.Help.Cancel')
        });

        if ($scope.showPreviewContainer) {
            steps.push({
                element: "#preview",
                intro: translationService.translate('Article.CartonProducible.Help.Form.Preview')
            });   
        }

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
}// end of Edit controller