var app = angular.module('PackNetApp', [
    'numericUpDown', 'ui.directives', 'ui.bootstrap', 'ui.event', 'ui.sortable',
    'angles', 'adf', 'LocalStorageModule', 'structures',
    'ngGrid', 'ui.grid', 'ui.grid.selection', 'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.rowEdit', 'ui.grid.expandable', 'ui.grid.pinning', 'ui.grid.resizeColumns', 'ui.grid.moveColumns', 'ui.grid.autoResize',
    'ngSanitize', 'ngCsv','ngKeypad','ngDraggable',
    'translationModule',
    'tmh.dynamicLocale',
    'dashboardMgmt',
    'dashboard.widgets.machineStatus',
    'dashboard.widgets.machineGroupsWidget',
    'dashboard.widgets.corrugateStatus',
    'dashboard.widgets.pickzones',
    'dashboard.widgets.cartonpropertygroups',
    'dashboard.widgets.classifications',
    'dashboard.widgets.produciblesImportedMetric',
    'dashboard.widgets.produciblesImportedMetricByDay',
    'dashboard.widgets.produciblesImportedMetricByWeek',
    'dashboard.widgets.produciblesCreatedMetric',
    'dashboard.widgets.produciblesCreatedMetricByDay',
    'dashboard.widgets.produciblesCreatedMetricByWeek',
    'dashboard.widgets.produciblesFailedMetric',
    'dashboard.widgets.produciblesFailedMetricByDay',
    'dashboard.widgets.produciblesFailedMetricByWeek',
    'dashboard.widgets.produciblesRotatedMetric',
    'dashboard.widgets.produciblesRotatedMetricByDay',
    'dashboard.widgets.produciblesRotatedMetricByWeek',
    'dashboard.widgets.averageProductionTimeMetric',
    'dashboard.widgets.averageProductionTimeMetricByDay',
    'dashboard.widgets.averageProductionTimeMetricByWeek',
    'dashboard.widgets.averageWastePercentageMetric',
    'dashboard.widgets.averageWastePercentageMetricByDay',
    'dashboard.widgets.averageWastePercentageMetricByWeek',
    'dashboard.widgets.averageWastePercentageByMachineMetric',
    'dashboard.widgets.averageWastePercentageByMachineMetricByDay',
    'dashboard.widgets.averageWastePercentageByMachineMetricByWeek',
    'dashboard.widgets.averageWastePercentageByCorrugateMetric',
    'dashboard.widgets.averageWastePercentageByCorrugateMetricByDay',
    'dashboard.widgets.averageWastePercentageByCorrugateMetricByWeek',
    'dashboard.widgets.corrugateConsumptionMetric',
    'dashboard.widgets.corrugateConsumptionMetricByDay',
    'dashboard.widgets.corrugateConsumptionMetricByWeek',
    'dashboard.widgets.cartonCountByCorrugateMetric',
    'dashboard.widgets.cartonCountByCorrugateMetricByDay',
    'dashboard.widgets.cartonCountByCorrugateMetricByWeek',
    'dashboard.widgets.cartonCountByMachineMetric',
    'dashboard.widgets.cartonCountByMachineByDayMetric',
    'dashboard.widgets.cartonCountByMachineByWeekMetric',
    'dashboard.widgets.cartonCountByUserMetric',
    'dashboard.widgets.cartonCountByUserMetricByDay',
    'dashboard.widgets.cartonCountByUserMetricByWeek',
    'dashboard.widgets.cartonCountByCPGMetric',
    'dashboard.widgets.deleteCartonsFromRange',
    'dashboard.widgets.orders',
    'dashboard.widgets.searchUserNotification',
    'ui.router',
    'packNetEnhancedLogging',
    'packNetEnhancedTimeout',
    'packNetEnhancedObservables',
    'authentication',
    'authorization',
    'stateChange',
    'persistentSettings',
    'packsizeConstants',
    'frapontillo.bootstrap-switch',
    'ui.bootstrap.datetimepicker',
    'ui.bootstrap.showErrors',
    'minicolors',
    'angular.filter',
    'ui-rangeSlider',
    'pascalprecht.translate',
    'angular-intro',
    'ngTagsInput',
    'packsizeIntroJsModule',
    'designPreviewModule',
    'helperModule',
    'ngClickCopy'
]);

app.factory('utilities', function () {
    return {
        subscribeAndDispose: function ($scope, observable, callback) {
            observable.autoSubscribe(callback, $scope, "subscribeAndDispose");
        },
        initialDataRequest: function ($scope, messagingService, requestData) {

            if (!messagingService) {
                throw "messaging service was not supplied";
            }
            if (messagingService.IsSignalRConnected()) {
                requestData();
            }

            this.subscribeAndDispose($scope, messagingService.signalRConnectedObservable, requestData);
        }
    };
});

app.config(function (tmhDynamicLocaleProvider) {
    tmhDynamicLocaleProvider.localeLocationPattern('Scripts/i18n/angular-locale_{{locale}}.js');
});

app.config(function (localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('adf');
});

app.config(function (logEnhancerProvider) {
    logEnhancerProvider.loggingPattern = '%s::[%s]> ';
});

app.config(['$controllerProvider', function ($controllerProvider) {
    // Allow the definition of global controllers
    $controllerProvider.allowGlobals();
}]);

app.config(['$provide', function ($provide) {
    $provide.decorator('$controller', [
        '$delegate',
        function ($delegate) {
            return function (constructor, locals, later, indent) {
                if (typeof constructor == "string" && !locals.$scope.controllerName) {
                    locals.$scope.controllerName = constructor;
                }
                return $delegate(constructor, locals, later, indent);
            }
        }]);
}]);

app.run(['$log', '$rootScope', 'logEnhancer', '$timeout', 'timeoutEnhancer', 'observableEnhancer', 'Loglevel_Constants', function ($log, $rootScope, logEnhancer, $timeout, timeoutEnhancer, observableEnhancer, Loglevel_Constants) {
    $rootScope.introJs = {};

    $rootScope.sidebarOptions = {};
    $rootScope.sidebarOptions.connectingPage = false;
    $rootScope.preferences = null;
    $rootScope.browserFingerPrint = new Fingerprint().get();
    logEnhancer.enhanceAngularLog($log, $rootScope, Loglevel_Constants);
    timeoutEnhancer.enhanceAngularTimeout($timeout);
    observableEnhancer.enhanceRxObservables($log, $timeout);

}]);

app.run(function ($rootScope, $state, $location, messagingService, persistentStorageService, machineGroupService, pluginDiscoveryService, utilities, authenticationService, authorizationService, AUTH_EVENTS, USER_ROLES, stateChangeService, notificationService, $translate, translationService) {

    machineGroupService.start();
    notificationService.start();
    persistentStorageService.start();
    authorizationService.start();
    authenticationService.start();
    stateChangeService.start();
    pluginDiscoveryService.start();

    $rootScope.utilities = utilities;
    $rootScope.currentUser = null;

    $rootScope.userRoles = USER_ROLES;
    $rootScope.isAuthorized = authenticationService.isAuthorized;
    $rootScope.authenticationService = authenticationService;
    $rootScope.date = new Date();

    authenticationService.userUpdateFunction = function (user) {
        $rootScope.currentUser = user;
    };

    function setLanguage(locale){
        $translate.use(locale);
    }

    authenticationService.userPreferencesUpdateFunction = function (preferences) {
        $rootScope.preferences = preferences;
        var current = $translate.use();

        if ($rootScope.currentLanguage) { //Set by login
            if ($rootScope.currentLanguage !== current) {
                setLanguage($rootScope.currentLanguage);
            }
            return;
        }

        if (preferences.PreferredLocale && preferences.PreferredLocale !== $translate.use()) {
            setLanguage(preferences.PreferredLocale);
        }
    };
});

app.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.on("change", function (changeEvent) {

                if (changeEvent.target.value == '') return;

                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                        changeEvent.target.value = ''; //If the same file is selected again due to a change within the file in question this event wont fire unless the value is reset after the file has been read
                    });
                }
                reader.readAsText(changeEvent.target.files[0]);
            });
        }
    }
}]);

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

function MainCtrl($rootScope, $scope, $log, $modal, messagingService, authorizationService, alertService, introJsService, USER_ROLES){
    $scope.$log = $log;
    
    $scope.shortcutIconUri = "img/red_icon.png";
    $scope.siteSettings = { "ShowPickZones": false };
    $scope.sideBarExpanded = true;
    $scope.version = { major: '3.1' };
    $scope.userRoles = USER_ROLES;
    $scope.authorizationService = authorizationService;
    $scope.showOperatorPanel = false;

    var componentErrorsObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ComponentErrors"));

    $scope.BroadcastPageOverviewRequested = function (){
        introJsService.invokeTellMeAboutThisPage();
    }

    $scope.toggleOperatorPanel = function () {
        $scope.showOperatorPanel = !$scope.showOperatorPanel;
    };

    $scope.updateTabIcon = function (url) {
        
        delete $scope.shortcutIconUri;
        $scope.shortcutIconUri = url;
    };

    $scope.toggleSidebar = function () {
        $scope.sideBarExpanded = !$scope.sideBarExpanded;
    };

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.sidebarOptions.connectingPage = false;
        $('ul.options li a').removeClass('highlight');
        $('ul.options li:has(a[href$="#' + toState.url + '"]) a').addClass('highlight');

        if (toState.name != fromState.name)
            alertService.clearAll();
    });
}