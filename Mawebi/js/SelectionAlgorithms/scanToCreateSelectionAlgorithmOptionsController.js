function ScanToCreateSelectionAlgorithmOptionsController($scope, $log, $state, $timeout, messagingService, alertService) {
    $timeout = $timeout.getInstance($scope);
    $scope.$log = $log;

    $scope.barcodeParsingWorkflowPaths = [];

    messagingService.scanDataWorkflowsObservable.autoSubscribe(function (message) {
        $timeout(function (){
            
            delete $scope.barcodeParsingWorkflowPaths;
            $scope.barcodeParsingWorkflowPaths = message.Data;
        }, 0);
    }, $scope, 'ScanDataWorkflows');

    var requestData = function () {
        messagingService.publish("GetScanDataWorkflows");
    }

    $scope.$parent.OptionsHelper.saveOptions = function (){
        return true;
    }
    $scope.utilities.initialDataRequest($scope, messagingService, requestData);
};
