function BoxFirstSelectionAlgorithmOptionsController($scope, $timeout, alertService, machineGroupService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.cpgRatio = [];
    $scope.showGraph = false;
    $scope.ratioMessage = "";
    $scope.options = {
        animation: false,
        maintainAspectRatio: false,
        responsive: true,
        segmentShowStroke: false
    }

    if (!$scope.selectedProductionGroup.Options) {
        
        delete $scope.selectedProductionGroup.Options;
        $scope.selectedProductionGroup.Options = {};
    }

    if (!$scope.selectedProductionGroup.Options.SurgeCount) {
        $scope.surgeCount = 10;
        $scope.selectedProductionGroup.Options.SurgeCount = 10;
    }
    else {
        $scope.surgeCount = $scope.selectedProductionGroup.Options.SurgeCount;
    }

    $scope.$watch('cartonPropertyGroups', function () {
        if ($scope.cartonPropertyGroups && $scope.cartonPropertyGroups.length > 0) {
            $scope.selectedProductionGroup.Options.ConfiguredCartonPropertyGroups =
                angular.toJson(Enumerable.From($scope.cartonPropertyGroups)
                    .Where("cpg=>cpg.Included").ToArray());
        }

        setupCPGOptions($scope.cartonPropertyGroups);
    }, true);

    messagingService.cartonPropertyGroupsObservable.autoSubscribe(function (data) {
        $timeout(function () {
            if ($scope.productionGroups) {
                var otherBoxFirstPGs = Enumerable.From($scope.productionGroups)
                    .Where(function (pg) {
                        return pg.SelectionAlgorithm === "BoxFirst" && pg.Id !== $scope.selectedProductionGroup.Id;
                    })
                    .ToArray();

                var availableCPGs = Enumerable.From(data.Data)
                    .Where(function (cpg) {
                        var filter = "$.Id === '" + cpg.Id + "'";

                        return !Enumerable.From(otherBoxFirstPGs)
                            .Where(function (pg) {
                                return Enumerable.From(angular.fromJson(pg.Options.ConfiguredCartonPropertyGroups))
                                    .Any(filter);
                            })
                            .Any();
                    })
                    .ToArray();

                setupCPGOptions(availableCPGs);
            }
        }, 0, true);
    }, $scope, 'CartonPropertyGroups');

    var getColorByHue = function (hue) {
        var result = "hsl(" + hue + ", 80%, 50%)";

        return result;
    }

    var setupCPGOptions = function (availableCPGs) {
        if (!$scope.selectedProductionGroup.Options) {
            
            delete $scope.selectedProductionGroup.Options;
            $scope.selectedProductionGroup.Options = {};
        }
        if (!$scope.selectedProductionGroup.Options.ConfiguredCartonPropertyGroups) {
            
            delete $scope.selectedProductionGroup.Options.ConfiguredCartonPropertyGroups;
            $scope.selectedProductionGroup.Options.ConfiguredCartonPropertyGroups = "[]";
        }

        var selectedCPGEnumerable = Enumerable.From($.parseJSON($scope.selectedProductionGroup.Options.ConfiguredCartonPropertyGroups));
        var cpgs = Enumerable.From(availableCPGs).Select(function (cpg) {
            var includedCPG = selectedCPGEnumerable.FirstOrDefault(undefined, function (included) {
                return included.Id == cpg.Id;
            });
            if (includedCPG) {
                return includedCPG;
            }
            cpg.Included = false;
            return cpg;
        });
        
        delete $scope.cartonPropertyGroups;
        $scope.cartonPropertyGroups = cpgs.ToArray();
        $scope.cpgRatio.length = 0;
        
        delete $scope.ratioMessage;
        $scope.ratioMessage = "";
        $scope.totalMixCount = 0;
        var includedCpg =  selectedCPGEnumerable.Where("cpg=>cpg.Included");
        var gap = 360 / includedCpg.Count();

        includedCpg.ForEach(function (cpg, index) {
                $scope.totalMixCount = $scope.totalMixCount + cpg.MixQuantity;
                $scope.cpgRatio.push({
                    value: cpg.MixQuantity,
                    color: getColorByHue(index * gap), 
                    label: cpg.Alias
                });
                $scope.ratioMessage = $scope.ratioMessage + cpg.Alias + ": " + cpg.MixQuantity + " / ";
            });
        
        delete $scope.showGraph;
        $scope.showGraph = selectedCPGEnumerable.Any("cpg=>cpg.Included");

        if ($scope.ratioMessage.length > 0) {
            $scope.ratioMessage = $scope.ratioMessage.substr(0, $scope.ratioMessage.length - 2);
        }
        setPriority();
    }
    var requestData = function () {
        messagingService.publish("GetCartonPropertyGroups");
    }

    var setPriority = function () {
        var cpgsOrderedByPriority = Enumerable.From($scope.cartonPropertyGroups).Where(function (c) {
            return c.Included > 0;
        }).OrderByDescending("c=>c.Priority");
        var count = cpgsOrderedByPriority.Count();
        $scope.selectedCount = count;
        cpgsOrderedByPriority.ForEach(function (cpg) {
            cpg.Priority = count--;
        });
    }

    $scope.checkedChanged = function (cpg) {
        if (!cpg.Included) {
            cpg.Priority = 0;
        }
        setPriority();
    }

    $scope.increasePriority = function (cpg) {
        var higherPriority = Enumerable.From($scope.cartonPropertyGroups).FirstOrDefault(null, function (c) {
            return c.Priority == cpg.Priority + 1;
        });
        higherPriority.Priority = cpg.Priority;
        cpg.Priority = cpg.Priority + 1;
        setPriority();
    }

    $scope.decreasePriority = function (cpg) {
        var lowerPriority = Enumerable.From($scope.cartonPropertyGroups).FirstOrDefault(null, function (c) {
            return c.Priority == cpg.Priority - 1;
        });
        lowerPriority.Priority = cpg.Priority;

        cpg.Priority = cpg.Priority - 1;
        if (cpg.Priority < 0)
            cpg.Priority = 0;
        setPriority();
    }
    $scope.$parent.OptionsHelper.saveOptions = function () {
        var includedCPG = Enumerable.From($scope.cartonPropertyGroups)
            .Where("cpg => cpg.Included");

        if (!includedCPG.Any())
            return true;

        var boxFirstPgs = Enumerable.From(machineGroupService.getProductionGroups())
            .Where(function (pg) {
                return pg.SelectionAlgorithm == "BoxFirst";
            }).ToArray();
        var cpgsAlreadyInAProductionGroup = Enumerable.From(boxFirstPgs)
            .Where(function (pg) {
                return pg.SelectionAlgorithm == "BoxFirst" && pg.Id != $scope.productionGroupBeingSaved.Id;
            })
            .SelectMany(function (pg) {
                return Enumerable.From(angular.fromJson(pg.Options.ConfiguredCartonPropertyGroups))
                .Select("cpg=>cpg.Id");
            });

        var duplicateCpgs = includedCPG.Select("cpg=>cpg.Id").Intersect(cpgsAlreadyInAProductionGroup);
        if (duplicateCpgs.Any()) {
            var duplicateNames = includedCPG.Where(function (cpg) {
                return duplicateCpgs.Any(function (id) { return cpg.Id == id; });
            }).Select("cpg=>cpg.Alias").ToArray().join(", ");
            alertService.addError(translationService.translate('ProductionGroup.Message.BoxFirst.CartonPropertyGroupAlreadyAssigned', duplicateNames));
            return false;
        }

        
        delete $scope.productionGroupBeingSaved.Options;
        $scope.productionGroupBeingSaved.Options = {};
        
        delete $scope.productionGroupBeingSaved.Options.ConfiguredCartonPropertyGroups;
        $scope.productionGroupBeingSaved.Options.ConfiguredCartonPropertyGroups = angular.toJson(includedCPG.ToArray());
        $scope.productionGroupBeingSaved.Options.SurgeCount = $scope.surgeCount;
        return true;
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);
};