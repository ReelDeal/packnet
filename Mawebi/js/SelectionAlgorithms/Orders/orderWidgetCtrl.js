function CorrugateInfoDialogCtrl($modalInstance, $scope, machineGroupService, producibleHelperService, currentMachine, selectedOrder, translationService) {

    $scope.cartonsInOrder = producibleHelperService.getAllOrders(selectedOrder);

    $scope.currentMachine = currentMachine;

    $scope.closeWindow = function () {
        $modalInstance.close();
    };

    $scope.getAlert = function (type, message) {
        var alert = { type: type, msg: message };
        return alert;
    };


    var machineHasOptimalCorrugateLoaded = function (m, carton) {
        return Enumerable.From(m.Data.Tracks).Any(function (t) {
            if (!carton.CartonOnCorrugate) return false;

            var c = carton.CartonOnCorrugate.Corrugate;
            if (!c) return false;
            else return t.LoadedCorrugate && t.LoadedCorrugate.Id === c.Id;
        });
    }

    var machineCanProduceCartonOnCorrugate = function (m, order) {
        if (order.CartonOnCorrugate === null) return false;

        return Enumerable.From(order.CartonOnCorrugate.ProducibleMachines).Any(function (producibleMachine) {
            return producibleMachine.Key === m.Id;
        });
    }

    var machineCanProduceOptimalCorrugate = function (m, order) {
        var loaded = machineHasOptimalCorrugateLoaded(m, order);
        var canProduce = machineCanProduceCartonOnCorrugate(m, order);

        return loaded && canProduce;
    }

    var getMachinesWithCorrugateLoaded = function (order) {
        var machinesWithCorrugateLoadedEnum = Enumerable.From(machineGroupService.getMachineGroups()).Where(function (mg) {
            return Enumerable.From(mg.Machines).Any(function (machine) {
                return machineCanProduceOptimalCorrugate(machine, order);
            });
        });
        return machinesWithCorrugateLoadedEnum.ToArray();
    };



    var getProductionGroupForOrder = function (order) {
        var ret = Enumerable.From(machineGroupService.getProductionGroups()).FirstOrDefault(undefined, function (pg) {
            return Enumerable.From(order.Restrictions).Any(function (restriction) {
                return restriction.Value == pg.Id;
            });
        });
        return ret;
    }

    var getAbleMachinesForMachineGroup = function (coc, mg) {
        if (mg == null) return [];
        var ableMachines = Enumerable.From(mg.Machines).Where(function (machine) {
            return coc != null && coc.ProducibleMachines[machine.Id] != null;
        }).Select(function (m) { return m.Alias; });
        return ableMachines;
    }


    $scope.getMachinesWithOptimalCorrugate = function () {
        if ($scope.order) {
            var machinesWithCorrugateLoaded = getMachinesWithCorrugateLoaded($scope.order);
            if (machinesWithCorrugateLoaded.length == 0) {
                return translationService.translate("MachineProduction.Order.Message.NoMachines");
            }
            var val = Enumerable.From(machinesWithCorrugateLoaded)
                .Select(function (m) {
                    return m.Alias;
                })
                .Aggregate("a,b=>a + ', ' + b");
            return val;
        }

        return translationService.translate("MachineProduction.Order.Message.NoMachines");
    };

    $scope.getMachineThatAreAbleToProduceCartonOnCorrugate = function (coc) {
        var machineGroups = machineGroupService.getMachineGroups();
        var ableMachines = [];
        if (currentMachine)
            ableMachines = getAbleMachinesForMachineGroup(coc, currentMachine);
        else {
            ableMachines = Enumerable.From(machineGroups).SelectMany(function (mg) {
                return getAbleMachinesForMachineGroup(coc, mg);
            });
        }

        if (ableMachines.Count() > 0) {
            return ableMachines.Aggregate("a,b=>a + ', ' + b");
        }

        return translationService.translate("MachineProduction.Order.Message.NoMachines");
    }



    $scope.getAlertMessage = function (order) {
        if ($scope.currentMachine == undefined || order.Status == "Cancelled" || order.Status === "Completed")
            return;

        delete $scope.alert;

        if (Enumerable.From(getMachinesWithCorrugateLoaded(order)).Any(function (machine) {
            return $scope.currentMachine.Alias == machine.Alias;
        })) {
            var pg = getProductionGroupForOrder(order);

            if (pg && $scope.currentMachine.ProductionMode != "AutoProductionMode") {
                $scope.alert = $scope.getAlert('danger',
                    translationService.translate("MachineProduction.Order.Message.ChangeToAuto"));
            }
            else {
                $scope.alert = $scope.getAlert('success',
                    translationService.translate("MachineProduction.Order.Message.AlreadyLoaded",
                        producibleHelperService.findFirstOptimalCorrugate(order).Alias));
            }
        } else {
            var optimalCorrugate = producibleHelperService.findFirstOptimalCorrugate(order);

            if (!optimalCorrugate) {
                $scope.alert = $scope.getAlert('danger', translationService.translate("MachineProduction.Order.Message.CannotProduce"));
            }
            else {
                $scope.alert = $scope.getAlert('warning', translationService.translate("MachineProduction.Order.Message.LoadCorrugate", optimalCorrugate.Alias));

            }
        }
    }
}

function OrderErrorInfoDialogCtrl($scope, $modalInstance, selectedOrder, currentMachine) {

    $scope.order = selectedOrder;
    $scope.currentMachine = currentMachine;

    $scope.closeWindow = function () {
        $modalInstance.close();
    };
}

function OrderReproduceDialogCtrl($log, $modalInstance, $scope, messagingService, selectedOrder) {

    $scope.order = selectedOrder;
    $scope.model = {};
    $scope.model.reproduceQuantity = 1;
    $scope.maxReproduce = selectedOrder.TotalQty - selectedOrder.RemainingQty;
    $scope.intRegex = /^\d+$/;

    $scope.closeBreakOrdersDialogWindow = function () {
        $modalInstance.close();
    };

    $scope.reproduce = function () {
        $scope.order.RemainingQuantity += $scope.order.InProgressQuantity + $scope.model.reproduceQuantity;
        $log.info('Saving broken box request Group... ');
        messagingService.publish("ReproduceCartonForOrder", { Id: selectedOrder.Id, ReproducedQuantity: $scope.model.reproduceQuantity });
        $scope.closeBreakOrdersDialogWindow();
    };
}

function OrderDistributeDialogCtrl($log, $modalInstance, $scope, messagingService, selectedOrder) {

    $scope.order = selectedOrder;

    $scope.closeDistributeDialogWindow = function () {
        $modalInstance.close();
    };

    $scope.distribute = function (result) {
        $log.info('Distributing Order... ');
        $log.info(result);
        messagingService.publish("DistributeOrders", selectedOrder.Id);
        $scope.closeDistributeDialogWindow();
    };
}

function OrderWidgetCtrl($log, $modal, translationService, $scope, machineGroupService, messagingService, producibleHelperService) {
    if (typeof $scope.order.BestCorrugate !== 'object') {
        $scope.order.BestCorrugate = eval("(" + $scope.order.BestCorrugate + ")");
    }

    $scope.progressPercent = 100 - ((($scope.order.RemainingQuantity + $scope.order.InProgressQuantity) / $scope.order.OriginalQuantity) * 100);

    if ($scope.progressPercent < 0) {
        $scope.progressPercent = 0;
    }

    $scope.failedPercent = ($scope.order.FailedQuantity / $scope.order.OriginalQuantity) * 100;

    if ($scope.failedPercent < 0) {
        $scope.failedPercent = 0;
    }

    $scope.progressPercent = $scope.progressPercent - $scope.failedPercent;

    $scope.distribute = function () {

        if ($scope.order.Distribute && $scope.order.InProgressQuantity > 0) {
            var opts = {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'partials/MachineManager/orderDistributeDialog.html',
                controller: 'OrderDistributeDialogCtrl',
                size: 'lg',
                resolve: {
                    selectedOrder: function () {
                        return $scope.order;
                    }
                }
            };
            $modal.open(opts);
        } else {
            messagingService.publish("DistributeOrders", $scope.order.Id);
        }
    };

    $scope.getProducibleInfo = function (order) {
        var result;

        if (order.Producible.ProducibleType == "Carton")
            result = translationService.translate('Article.CartonProducible.Label.NameSingular') + ' ' +
                translationService.translate('Common.Label.LengthAcronym') + ':' + order.Producible.Length + ' ' +
                translationService.translate('Common.Label.WidthAcronym') + ':' + order.Producible.Width + ' ' +
                translationService.translate('Common.Label.HeightAcronym') + ':' + order.Producible.Height + ' ' +
                translationService.translate('Common.Label.DesignAcronym') + ':' + order.Producible.DesignId;
        else
            result = translationService.translate('Article.Label.NameSingular');

        return result;
    }

    $scope.getCorrugateAliases = function (order) {
        var aliases = "";
        var coc = null;
        var containsCartons = false;
        if (order.Producible.ProducibleType == "Kit") {
            for (var i = 0; i < order.Producible.ItemsToProduce.length; i++) {
                if (order.Producible.ItemsToProduce[i].ProducibleType == "Carton" ||
                    order.Producible.ItemsToProduce[i].ProducibleType == "TiledCarton") {
                    coc = order.Producible.ItemsToProduce[i].CartonOnCorrugate;
                    containsCartons = true;
                    if (coc) aliases += coc.Corrugate.Alias + " ";

                }
                    // Articles
                else if (order.Producible.ItemsToProduce[i].ProducibleType == "Order") {
                    var prod = order.Producible.ItemsToProduce[i].Producible;
                    if (prod.ProducibleType == "Carton" || prod.ProducibleType == "TiledCarton") {
                        coc = order.Producible.ItemsToProduce[i].Producible.CartonOnCorrugate;
                        containsCartons = true;
                        if (coc) aliases += coc.Corrugate.Alias + " ";
                    }
                }
            }
        }
        else if (order.Producible.ProducibleType == "Carton") {
            coc = order.Producible.CartonOnCorrugate;
            containsCartons = true;
            if (coc) aliases = coc.Corrugate.Alias;
        }
        if (aliases === "" && containsCartons) aliases = translationService.translate("MachineProduction.Order.Message.CorrugateError");
        return aliases;
    }

    $scope.getMachinesThatHaveWorkedOnOrderString = function () {
        if ($scope.order && $scope.order.MachinesThatHaveWorkedOnOrder && $scope.order.MachinesThatHaveWorkedOnOrder.length > 0) {

            if ($scope.order.MachinesThatHaveWorkedOnOrder.length == 1)
                return $scope.order.MachinesThatHaveWorkedOnOrder[0].Alias;

            var val = Enumerable.From($scope.order.MachinesThatHaveWorkedOnOrder).Select(function (machine) {
                return machine.Alias;
            }).Aggregate("a,b=>a + ',' + b");
            return val;
        }
        return translationService.translate("MachineProduction.Order.Message.NoMachines");
    };

    var getProductionGroupForOrder = function (order) {
        var productionGroup = Enumerable.From(machineGroupService.getProductionGroups()).FirstOrDefault(undefined, function (pg) {
            return Enumerable.From(order.Restrictions).Any(function (restriction) {
                return restriction.Value == pg.Id;
            });
        });
        return productionGroup;
    }

    $scope.getAvailableMachines = function () {
        var currentMachineGroup = machineGroupService.getCurrentMachineGroup();
        if (currentMachineGroup) {
            if (currentMachineGroup.ProductionMode == "AutoProductionMode" && currentMachineGroup.ProductionGroup != undefined) {
                return currentMachineGroup.ProductionGroup.ConfiguredMachineGroups.length;
            }
            else {
                return 1; //Machine specific
            }
        }

        var productionGroup = getProductionGroupForOrder($scope.order);
        if (productionGroup) {
            return productionGroup.ConfiguredMachineGroups.length;
        }
        return 1; //Machine specific
    };

    $scope.reproduce = function () {
        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SelectionAlgorithm/Orders/orderReproduceDialog.html',
            controller: 'OrderReproduceDialogCtrl',
            resolve: {
                selectedOrder: function () {
                    return $scope.order;
                }
            }
        };
        $modal.open(opts);
    };

    $scope.corrugateInfo = function () {
        var opts = {
            backdrop: true,
            keyboard: true,
            templateUrl: 'partials/SelectionAlgorithm/Orders/orderCorrugateInfoDialog.html',
            controller: 'CorrugateInfoDialogCtrl',
            resolve: {
                currentMachine: function () {
                    return machineGroupService.getCurrentMachineGroup();
                },
                selectedOrder: function () {
                    return $scope.order;
                }
            }
        };
        $modal.open(opts);
    };

    $scope.errorInfo = function () {
        var opts = {
            backdrop: true,
            keyboard: true,
            templateUrl: 'partials/SelectionAlgorithm/Orders/orderErrorInfoDialog.html',
            controller: 'OrderErrorInfoDialogCtrl',
            size: 'lg',
            resolve: {
                selectedOrder: function () {
                    return $scope.order;
                },
                currentMachine: function () {
                    return machineGroupService.getCurrentMachineGroup();
                }
            }
        };
        $modal.open(opts);
    };

    var shouldDisplayError = function (order) {
        return order.ProducibleStatus !== "ProducibleCompleted" && order.ProducibleStatus !== "ProducibleRemoved";
    };


    $scope.isBestCorrugateNull = function () {
        if (!shouldDisplayError($scope.order))
            return false;
        return !producibleHelperService.findFirstOptimalCorrugate($scope.order);
    };

    $scope.containsCarton = function () {
        var carton = producibleHelperService.findFirstCarton($scope.order);
        return carton != undefined;
    }

    $scope.isBestCorrugateNotLoadedOnMachine = function () {
        if (!shouldDisplayError($scope.order))
            return false;
        if (!machineGroupService.getCurrentMachineGroup())
            return false;

        var optimalCorrugates = producibleHelperService.findAllOptimalCorrugates(producibleHelperService.findAllCartons($scope.order), []);
        if (!optimalCorrugates) {
            return false;
        }

        var result = Enumerable.From(optimalCorrugates).Any(function (optimalCorr) {
            return !machineGroupService.hasCapabilityWithId(optimalCorr.Id);
        });

        return result;
    };

    $scope.canMachineProduceOptimally = function () {
        if (!shouldDisplayError($scope.order) || !$scope.currentMachineGroup) return true;
        var result = producibleHelperService.machineGroupCanProduceAllCartons($scope.order, $scope.currentMachineGroup);
        return result;
    };

    $scope.getBestCorrugateTitle = function () {
        if ($scope.order.ProducibleStatus === "ProducibleCompleted")
            return translationService.translate("MachineProduction.Order.Message.CompletedOn", $scope.order.StatusChangedDateTime);

        if ($scope.order.ProducibleStatus === "ProducibleRemoved")
            return translationService.translate("MachineProduction.Order.Message.CanceledOn", $scope.order.StatusChangedDateTime);

        if (!producibleHelperService.findFirstOptimalCorrugate($scope.order)) {
            return translationService.translate("MachineProduction.Order.Message.CannotProduce");
        }

        var currentMachineGroup = machineGroupService.getCurrentMachineGroup();
        if (currentMachineGroup) {

            if (!Enumerable.From(producibleHelperService.findFirstCartonOnCorrugate($scope.order).ProducibleMachines).Any(
                    function (a) {
                        return Enumerable.From(currentMachineGroup.ConfiguredMachines).Any(function (b) { return b == a.Key; });
            })
            ) {
                return translationService.translate("MachineProduction.Order.Message.CannotProduce");
            }
            var loaded = Enumerable.From(currentMachineGroup.CurrentCapabilities).Any(function (c) {
                if (c.RestrictionType.contains('CorrugatesOnMachineId') && Enumerable.From(c.Value.Corrugates).Any(function (corr) {
                    return corr.Alias === producibleHelperService.findFirstOptimalCorrugate($scope.order).Alias;
                }));
                return false;
            });
            var alias = producibleHelperService.findFirstOptimalCorrugate($scope.order).Alias;
            if (loaded) {
                return alias;
            }

            return translationService.translate("MachineProduction.Order.Message.LoadCorrugate", alias);
        }
        else {
            return producibleHelperService.findFirstOptimalCorrugate($scope.order).Alias;
        }
    };

    $scope.orderQuantityTitle = translationService.translate('MachineProduction.Order.Message.QuantitySummary', ($scope.order.RemainingQuantity + $scope.order.InProgressQuantity), $scope.order.OriginalQuantity, $scope.order.FailedQuantity);
    $scope.orderMachinesCountsTitle = translationService.translate('MachineProduction.Order.Message.MachinesThatProducedCarton', $scope.getMachinesThatHaveWorkedOnOrderString());

    $scope.$on('$destroy',
        function () {
            delete $scope.order;
            console.log("watchers count = " + $scope.$$watchers.length);
        });

}

app.directive('order', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/SelectionAlgorithm/Orders/orderWidget.html',
        link: {
            pre: function postLink(scope, element, attrs) {
                if (attrs.searching != undefined)
                    scope.searching = true;
            }
        }
    };
});