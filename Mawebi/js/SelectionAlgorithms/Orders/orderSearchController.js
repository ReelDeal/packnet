function OrderSearchController($filter,
    $log,
    $modal,
    $scope,
    localStorageService,
    $timeout,
    alertService,
    introJsService,
    messagingService,
    machineGroupService,
    producibleHelperService,
    packagingDesignService,
    searchConfigurationService,
    uiGridConstants,
    dateValidationService, translationService) {
    $timeout = $timeout.getInstance($scope);
    var awaitingResponse = true;
    $scope.Restrictions = {};
    $scope.showDetails = false;
    $scope.filtersEnabled = false;
    $scope.labelDetails = '';
    $scope.filteringLabel = '';
    $scope.disableReproduce = true;
    $scope.disableCancel = true;
    $scope.disableCreateNext = true;
    $scope.status = '';

    var producibleStatus = {
        Closed: translationService.translate('ProducibleStatus.Label.Generic.Closed'),
        Failed: translationService.translate('ProducibleStatus.Label.Generic.Failed'),
        InProgress: translationService.translate('ProducibleStatus.Label.Generic.InProgress'),
        Open: translationService.translate('ProducibleStatus.Label.Generic.Open'),
        Removed: translationService.translate('ProducibleStatus.Label.Generic.Removed'),
        Unknown: translationService.translate('ProducibleStatus.Label.Generic.Unknown')
    };

    var date = new Date();
    date.setDate(date.getDate() - 1);
    var endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999);
    //setup initial criteria
    $scope.criteria = {
        Created: {
            $gte: date,
            $lt: endOfDay
        },
        CustomerUniqueId: ''
    };

    $scope.data = {};
    $scope.producible = {};
    $scope.corrugate = {};
    //Added to fix TFS 9460
    $scope.distinctAndSortedCorrugateQualityOptions = [];
    $scope.machine = {};
    $scope.production = {};
    $scope.OpenStatusList = ['ProducibleImported', 'ProducibleStaged'];
    $scope.InProgressStatusList = [
        'ProducibleSentToMachineGroup', 'ProducibleSentToMachine', 'ProducibleProductionStarted',
        'ProducibleFlaggedForReproduction', 'ProducibleSelected', 'AddedToMachineGroupQueue'
    ];
    $scope.ClosedStatusList = ['ProducibleCompleted', 'ProducibleRemoved'];
    $scope.FailedStatusList = ['NotProducible', 'ProducibleFailed'];
    $scope.RemovedStatusList = [];
    $scope.productionGroups = Enumerable.From(machineGroupService.getProductionGroups()).Where("pg => pg.SelectionAlgorithm == 'Order'").ToArray();

    $scope.data = localStorageService.get("OrderSavedSettings");
    if ($scope.data != undefined) {
        $scope.producible.CustomerUniqueId = $scope.data.ArticleId;
        $scope.corrugate.Alias = $scope.data.Alias;
        $scope.corrugate.Quality = $scope.data.Quality;
        $scope.machine = $scope.data.Machine;
        $scope.Restrictions.Value = $scope.data.ProductionGroup;
        $scope.producible.DesignId = $scope.data.DesignId;
        $scope.producible.Height = $scope.data.Height;
        $scope.producible.Length = $scope.data.Length;
        $scope.producible.Width = $scope.data.Width;
        $scope.class = "btn-success";
    }
    else {
        $scope.class = "btn-default";
    }

    $scope.toggleDetails = function () {
        $timeout(function () {
            $scope.showDetails = !$scope.showDetails;
        },
            0);
    };
    $scope.$watch('showDetails',
        function (newValue) {

            delete $scope.labelDetails;
            $scope.labelDetails = newValue
                ? translationService.translate('Search.Common.Label.HideOptions')
                : translationService.translate('Search.Common.Label.ShowOptions');
        });

    $scope.selectableStatuses = [
    {
        text: translationService.translate('ProducibleStatus.Label.Generic.Open'),
        value: [{ 'ProducibleStatus': 'ProducibleImported' }, { 'ProducibleStatus': 'ProducibleStaged' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.InProgress'),
        value: [
            { 'ProducibleStatus': 'ProducibleSelected' }, { 'ProducibleStatus': 'ProducibleFlaggedForReproduction' },
            { 'ProducibleStatus': 'ProducibleProductionStarted' },
            { 'ProducibleStatus': 'ProducibleSentToMachineGroup' },
            { 'ProducibleStatus': 'ProducibleSentToMachine' }, { 'ProducibleStatus': 'AddedToMachineGroupQueue' }
        ]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Closed'),
        value: [{ 'ProducibleStatus': 'ProducibleCompleted' }, { 'ProducibleStatus': 'ProducibleRemoved' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Failed'),
        value: [{ 'ProducibleStatus': 'NotProducible' }, { 'ProducibleStatus': 'ProducibleFailed' }]
    }]; //todo add archived option

    $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    packagingDesignService.allDesignsObservable.autoSubscribe(function () {

        delete $scope.selectableDesigns;
        $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    }, $scope, 'Get Designs');

    messagingService.corrugatesObservable.autoSubscribe(function (msg) {
        //I really shouldn't have to be saying this, but sort the list for display

        delete $scope.allCorrugates;
        $scope.allCorrugates = Enumerable.From(msg.Data).OrderBy(function (m) { return m.Alias; }).ToArray();
        //Added to fix TFS 9460

        delete $scope.distinctAndSortedCorrugateQualityOptions;
        $scope.distinctAndSortedCorrugateQualityOptions = Enumerable.From(msg.Data).Select(function (m) { return m.Quality; }).Distinct().OrderBy(function (i) { return i; }).ToArray();
    }, $scope, 'Corrugates');

    messagingService.machinesObservable.autoSubscribe(function (msg) {
        delete $scope.allMachines;
        $scope.allMachines = msg.Data;

    }, $scope, 'Machines');

    $scope.toggleFiltering = function () {
        $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
    };

    var enableReproduce = function () {
        $scope.disableReproduce = getItemsToReproduce().length == 0;
        $scope.disableCancel = getItemsToCancel().length == 0;
        $scope.disableCreateNext = getItemToCreateNext().length == 0;
    };
    var getItemsToReproduce = function () {
        var items = [];
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (selectedRow) {
            if (selectedRow.uiReproduceQuantity > 0) {
                selectedRow.newTotal = selectedRow.RemainingQuantity + selectedRow.uiReproduceQuantity;
                items.push(selectedRow);
            }
        });

        return items;
    };
    var getItemsToCancel = function () {
        var items = [];
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (selectedRow) {
            if (selectedRow.UserFriendlyStatus === producibleStatus.InProgress || selectedRow.UserFriendlyStatus === producibleStatus.Open) {
                items.push(selectedRow);
            }
        });
        return items;
    };

    var getItemToCreateNext = function () {
        var items = [];
        Enumerable.From($scope.gridApi.core.getVisibleRows()).ForEach(function (selectedRow) {
            if ((selectedRow.entity.UserFriendlyStatus === producibleStatus.Open) && (selectedRow.isSelected === true)) {
                items.push(selectedRow.entity);
            }
        });

        if (items.length >= 1) {
            return items;
        }

        return [];
    };

    var nudTemplate = '<div ng-show="row.isSelected">' +
        '<numeric-up-down id="newMachinePortNUD" ng-model="row.entity.uiReproduceQuantity" default-value="1" min-value="1" max-value="65535" horizontal="true" on-changed="$parent.grid.api.enableReproduce()"></numeric-up-down>' +
        '</div>';

    $scope.gridOptions = {
        enableGridMenu: true,
        enableColumnResizing: true,
        showGridFooter: true,
        // This template works as editable cell template because only one column is editable
        editableCellTemplate: nudTemplate,
        columnDefs: [
            {
                name: translationService.translate('Common.Label.Reproduce'),
                field: 'uiReproduceQuantity',
                type: 'number',
                enableFiltering: false,
                minWidth: 125,
                enableCellEdit: true,
                enableCellEditOnFocus: true,
                cellTemplate: nudTemplate
            },
            {
                displayName: translationService.translate('Common.Label.CustomerId'),
                field: 'CustomerUniqueId',
                enableCellEdit: false,
                sortingAlgorithm: function (a, b) {

                    if (isNaN(a) && isNaN(b)) {
                        return a === b ? 0 : (a < b ? -1 : 1);
                    }

                    if (!isNaN(a) && !isNaN(b)) {
                        var idA = parseFloat(a);
                        var idB = parseFloat(b);

                        return idA === idB ? 0 : (idA < idB ? -1 : 1);
                    }

                    return 0;
                }
            },
            {
                displayName: translationService.translate('Article.Label.Id'),
                field: 'ArticleId',
                enableCellEdit: false,
                sortingAlgorithm: function (a, b) {

                    if (isNaN(a) && isNaN(b)) {
                        return a === b ? 0 : (a < b ? -1 : 1);
                    }

                    if (!isNaN(a) && !isNaN(b)) {
                        var idA = parseFloat(a);
                        var idB = parseFloat(b);

                        return idA === idB ? 0 : (idA < idB ? -1 : 1);
                    }

                    return 0;
                }
            },
            {
                name: translationService.translate('Search.Order.Label.RemainingOriginal'),
                enableCellEdit: false,
                field: 'QuantityInfo'
            },
            {
                name: translationService.translate('Common.Label.Created'),
                field: 'Created',
                type: 'date',
                cellFilter: 'date:\'medium\'',
                enableFiltering: false,
                enableCellEdit: false,
                sortingAlgorithm: function (a, b) {
                    var timeA = typeof (a) === "string" ? new Date(a).getTime() : a.getTime(),
                        timeB = typeof (b) === "string" ? new Date(b).getTime() : b.getTime();

                    return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                }
            }, // having issues with this filter, better disable it
            {
                name: translationService.translate('Common.Label.Status'),
                enableCellEdit: false,
                field: 'UserFriendlyStatus'
            },
            {
                name: translationService.translate('Common.Label.Type'),
                field: 'ProducibleType',
                enableCellEdit: false,
                cellFilter: 'producibleType'
            },
            {
                name: translationService.translate('Common.Label.Dimensions'),
                enableCellEdit: false,
                field: 'MeasureInfo'
            },
            {
                name: translationService.translate('Common.Label.Design'),
                enableCellEdit: false,
                field: 'DesignName'
            },
            {
                name: translationService.translate('Corrugate.Label.NameSingular'),
                enableCellEdit: false,
                field: 'CorrugateInfo'
            },
            {
                name: translationService.translate('Machine.Label.NameSingular'),
                enableCellEdit: false,
                field: 'ProducedOn'
            },
            {
                name: translationService.translate('Search.Common.Label.TimesCompleted'),
                field: 'CompletedHistoryCount',
                enableCellEdit: false,
                type: 'number'
            }
        ],

        enableFiltering: false,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,

        expandableRowHeight: 150,
        expandableRowTemplate: "partials/SelectionAlgorithm/expandableProducibleTemplate.html",

        multiSelect: true,
        onRegisterApi: function (gridApi) {

            delete $scope.gridApi;
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                enableReproduce();
                row.entity.enableReproduce = row.isSelected;
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {
                enableReproduce();
                rows.forEach(function (row) {
                    row.entity.enableReproduce = row.isSelected;
                });
            });

            gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                if (row.isExpanded && row.entity.Producible.ProducibleType === "Kit" && !row.entity.subGridOptions) {
                    row.entity.subGridOptions = {
                        enableColumnResizing: true,
                        columnDefs: [
                            { name: translationService.translate("Common.Label.Name"), field: "CustomerUniqueId" },
                            { name: translationService.translate("Common.Label.Description"), field: "CustomerDescription" },
                            { name: translationService.translate("Common.Label.Length"), field: "Length", width: 80 },
                            { name: translationService.translate("Common.Label.Width"), field: "Width", width: 80 },
                            { name: translationService.translate("Common.Label.Height"), field: "Height", width: 80 },
                            { name: translationService.translate("Common.Label.Quantity"), field: "Quantity", width: 95 },
                            { name: translationService.translate("Common.Label.Type"), field: "ProducibleType", cellFilter: "producibleType", width: 150 },
                            { name: translationService.translate("Search.ScanToCreate.Label.ProducibleInfo"), field: "ProducibleInfo", width: 250 }
                        ],
                        data: row.entity.Producible.ItemsToProduce
                    };
                }
            });

            gridApi.enableReproduce = enableReproduce;
        }
    };

    $scope.$watch('gridOptions.enableFiltering', function (newValue) {

        delete $scope.filteringLabel;
        $scope.filteringLabel = newValue ? translationService.translate('Common.Label.DisableResultsFilter') : translationService.translate('Common.Label.EnableResultsFilter');
    });

    var convertToLike = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        secondPart = secondPart.replace('"', '/').replace('"', '/');
        if (secondPart.startsWith('//')) {
            index = firstPart.indexOf(field) - 2;
            firstPart = firstPart.substring(0, index);
            secondPart = secondPart.substring(2, secondPart.length);
        }
        return firstPart + secondPart;
    };
    var convertToISODate = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        secondPart = secondPart.replace('"', 'ISODate(\'').replace('"', '\')');

        return firstPart + secondPart;
    };

    var getNestedCriteria = function (prefix, obj) {
        // prefix can be a comma separated list of prefixes, when supplying multiple values they are enclosed inside an $or instruction

        var result = '';
        var prefixes = prefix.split(',');

        for (var key in obj) {
            if (obj.hasOwnProperty(key) && obj[key] != null && obj[key] !== '' && obj[key] !== 0) {
                if (result !== '')
                    result += ", ";

                if (prefixes.length > 1) {
                    result += '{ $or: [';
                }

                for (var i = 0; i < prefixes.length; i++) {
                    if (i > 0)
                        result += ', ';

                    result += '{"' + prefixes[i].trim() + '.' + key + '": ';

                    if (Enumerable.From(['Alias']).Contains(key)) {
                        result += '"' + obj[key] + '"';
                    }
                    else if (Enumerable.From(['CustomerUniqueId']).Contains(key)) {
                        result += '/' + obj[key] + '/i';
                    }
                    else if (Enumerable.From(['_id']).Contains(key)) {
                        result += 'CSUUID("' + obj[key] + '")';
                    }
                    else if (Enumerable.From(['Value']).Contains(key)) {
                        result += 'CSUUID("' + obj[key] + '")';
                    }
                    else {
                        result += obj[key];
                    }

                    result += "}";
                }

                if (prefixes.length > 1) {
                    result += '] }';
                }
            }
        }

        return result;
    };

    var removeNull = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        if (secondPart.startsWith('null')) {
            index = firstPart.indexOf(field) - 2;
            firstPart = firstPart.substring(0, index);
            secondPart = secondPart.substring(4, secondPart.length);
        }
        return firstPart + secondPart;
    };

    $scope.search = function () {
        $scope.gridApi.selection.clearSelectedRows();

        $scope.isDateRangeInvalid = !dateValidationService.validateSearchDates($scope.criteria.Created.$gte, $scope.criteria.Created.$lt);
        if ($scope.isDateRangeInvalid) {
            return;
        }

        delete $scope.status;

        $scope.status = translationService.translate('Search.Common.Message.Searching');

        $scope.criteria.CustomerUniqueId = $scope.criteria.CustomerUniqueId.replace(/"/g, '');

        var criteria = JSON.stringify($scope.criteria);
        criteria = convertToLike(criteria, "CustomerUniqueId");
        criteria = convertToISODate(criteria, '$gte');
        criteria = convertToISODate(criteria, '$lt');
        criteria = removeNull(criteria, '$or');

        //Begin Fixes for TFS 9460 - BoxFirst (and last) Search Page: In additional search options, when corrugate quality is equal to 0, the search result list is wrong.
        /*
        1.We were providing a bunch of options arbitrarily with numeric up-down which is not right. We need to get a list of the unique corrugate qualities
        in the system and provide those in a numerically sorted list with All at the top of it. This is accomplished using $scope.distinctAndSortedCorrugateQualityOptions.
        2.Changed numeric up/down input to a select (dropdown) because we didn't correctly deal with the nullable integer. Added an "All" option.
        3.Used a primitive array of numbers to bind to the select element while keeping id for automated testing.
        4.Search behavior jacked up: 0 does not mean "All." 0 means use 0 in the search. All means don't apply the filter. See above items about nullable int.
        5.We didn't use the right string filter for criteria and corrugate quality or it got changed out at some point so mongo query was wrong. Added a check for
        a useable numeric value coming in and fixed the filter string.
        */

        var additionalCriteria = '';
        var nestedCriteria = getNestedCriteria('Producible, Producible.ItemsToProduce', $scope.producible);

        if (nestedCriteria !== '')
            additionalCriteria += nestedCriteria;

        nestedCriteria = getNestedCriteria('Producible.CartonOnCorrugate.Corrugate, Producible.ItemsToProduce.CartonOnCorrugate.Corrugate', $scope.corrugate);

        if (nestedCriteria !== '')
            additionalCriteria += (additionalCriteria !== '' ? ', ' : '') + nestedCriteria;

        nestedCriteria = getNestedCriteria('MachinesThatHaveWorkedOnOrder', $scope.machine);

        if (nestedCriteria !== '')
            additionalCriteria += (additionalCriteria !== '' ? ', ' : '') + nestedCriteria;
        //End Fixes for TFS 9460

        for (var key in $scope.Restrictions) {
            if (!$scope.Restrictions[key]) {
                delete $scope.Restrictions[key];
            }
        }

        $scope.filtersEnabled = (additionalCriteria !== '');

        if (additionalCriteria !== '') {
            var position = criteria.length - 1;

            criteria = [criteria.slice(0, position), ', $and: [' + additionalCriteria + ']', criteria.slice(position)].join('');
        }

        messagingService.publish("SearchForProduciblesWithRestrictions", { SelectionAlgorithmTypes: "Order", Criteria: criteria, Restrictions: $scope.Restrictions });
    };

    $scope.save = function () {
        $scope.data = {
            ArticleId: $scope.producible.CustomerUniqueId,
            Alias: $scope.corrugate.Alias,
            Quality: $scope.corrugate.Quality,
            Machine: $scope.machine,
            ProductionGroup: $scope.Restrictions.Value,
            DesignId: $scope.producible.DesignId,
            Height: $scope.producible.Height,
            Width: $scope.producible.Width,
            Length: $scope.producible.Length
        };

        localStorageService.set("OrderSavedSettings", $scope.data);
        alertService.addSuccess(translationService.translate('Common.Message.SavedSearch'));
        $scope.class = "btn-success";
    };

    $scope.reset = function () {
        localStorageService.remove("OrderSavedSettings");
        $scope.producible.CustomerUniqueId = "";
        $scope.corrugate.Alias = "";
        $scope.corrugate.Quality = "";
        $scope.machine._id = "";
        $scope.Restrictions.Value = "";
        $scope.producible.DesignId = "";
        $scope.producible.Height = "";
        $scope.producible.Width = "";
        $scope.producible.Length = "";
        $scope.class = "btn-default";
    }

    $scope.reproduceCancel = function (isCancel) {
        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SelectionAlgorithm/Orders/searchOrderConfirmationDialog.html',
            controller: 'SearchOrderConfirmationDialogCtrl', //defined end of this file
            resolve: {
                selectedOrders: function () {
                    if (isCancel) {
                        return getItemsToCancel();
                    }
                    return getItemsToReproduce();
                },
                cancelling: function () {
                    return isCancel;
                },
                grid: function () {
                    return $scope.gridApi;
                }
            }
        };

        $modal.open(opts).result.then(function () {
            $timeout(function () { $scope.search(); }, 1000);
        });
    };

    $scope.createNext = function () {
        if (getItemToCreateNext().length > 0) {

            var opts = {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'partials/MachineManager/createNextDialog.html',
                controller: 'CreateNextSearchDialogCtrl',
                resolve: {
                    selectedOrders: getItemToCreateNext
                }
            };
            $modal.open(opts);
            clearSelected();
        }
    };

    var clearSelected = function () {
        $scope.gridApi.selection.clearSelectedRows();
    };

    var translateStatusToUserFriendlyNames = function (status) {
        if (Enumerable.From($scope.OpenStatusList).Contains(status)) {
            return producibleStatus.Open;
        }
        else if (Enumerable.From($scope.InProgressStatusList).Contains(status)) {
            return producibleStatus.InProgress;
        }
        else if (Enumerable.From($scope.ClosedStatusList).Contains(status)) {
            return producibleStatus.Closed;
        }
        else if (Enumerable.From($scope.RemovedStatusList).Contains(status)) {
            return producibleStatus.Removed;
        }
        else if (Enumerable.From($scope.FailedStatusList).Contains(status)) {
            return producibleStatus.Failed;
        }
        else {
            return producibleStatus.Unknown;
        }
    };

    messagingService.searchOrdersResponseObservable
       .autoSubscribe(function (msg) {
           $timeout(function () {

               delete $scope.status;
               $scope.status = translationService.translate('Search.Common.Message.ResultFound');
               if ($scope.debug) {
                   $log.info([moment().format("HH:mm:ss.SSS"), data]);
               }
               awaitingResponse = false;
               Enumerable.From(msg.Data).ForEach(function (producible) {
                   var corrugateInfo = null;

                   producible.CompletedHistoryCount = Enumerable.From(producible.History).Where(function (h) { return h.NewStatus === "ProducibleCompleted"; }).Count();
                   producible.ProducibleType = producible.Producible.ProducibleType;
                   producible.ArticleId = "";
                   producible.ProducedOn = Enumerable.From(producible.MachinesThatHaveWorkedOnOrder).Select(function (m) { return m.Alias; }).ToArray().toString();
                   producible.uiReproduceQuantity = 0;
                   producible.QuantityInfo = producible.RemainingQuantity + '/' + producible.OriginalQuantity;

                   if (producible.Producible.CartonOnCorrugate != null && producible.Producible.CartonOnCorrugate.Corrugate != null) {
                       corrugateInfo = producible.Producible.CartonOnCorrugate.Corrugate;
                       producible.CorrugateInfo = corrugateInfo.Alias + ' - W: ' + corrugateInfo.Width + ' - T: ' + corrugateInfo.Thickness + ' - Q: ' + corrugateInfo.Quality;
                   }

                   if (producible.ProducibleType !== "Kit") {
                       producible.MeasureInfo = "L:" + producible.Producible.Length + " W:" + producible.Producible.Width + " H:" + producible.Producible.Height;
                   }
                   else {
                       producible.ArticleId = producible.Producible.CustomerUniqueId;
                   }

                   if ($scope.selectableDesigns) {
                       producible.DesignName =
                           Enumerable.From($scope.selectableDesigns)
                           .Where(function (pg) { return pg.Id == producible.Producible.DesignId; })
                           .Select(function (c) { return c.Name; })
                           .ToArray()
                           .toString();
                   }

                   producible.UserFriendlyStatus = translateStatusToUserFriendlyNames(producible.ProducibleStatus);

                   if (producible.ProducibleType === 'Carton') {
                       //Faking the structure used on Box First and Box Last Search to display the details of a Carton

                       producible.Producible.ItemsToProduce = [];

                       var xvalues = '';
                       if (producible.Producible.XValues) {
                           xvalues = ' ' + Enumerable.From(producible.Producible.XValues).Select(function (x) { return x.Key + ':' + x.Value }).ToArray().join(' ');
                       }
                       producible.Producible.ItemsToProduce.push({
                           ProducibleType: producible.Producible.ProducibleType,
                           Header: $filter('producibleType')(producible.Producible.ProducibleType) + ' - ' + producible.UserFriendlyStatus,
                           Line1: translationService.translate('Common.Label.LengthAcronym') + ': ' + producible.Producible.Length + ' ' +
                               translationService.translate('Common.Label.WidthAcronym') + ': ' + producible.Producible.Width + ' ' +
                               translationService.translate('Common.Label.HeightAcronym') + ': ' + producible.Producible.Height + xvalues,
                           Line2: translationService.translate('Common.Label.Design') + ': ' + producible.DesignName
                       });

                       if (corrugateInfo) {
                           producible.Producible.ItemsToProduce.push({
                               ProducibleType: 'Corrugate',
                               Header: translationService.translate('Search.Common.Label.ProducedOnCorrugate'),
                               Line1: translationService.translate('Common.Label.Name') + ': ' + corrugateInfo.Alias,
                               Line2: translationService.translate('Common.Label.WidthAcronym') + ': ' + corrugateInfo.Width + ' - ' +
                                   translationService.translate('Common.Label.ThicknessAcronym') + ': ' + corrugateInfo.Thickness + ' - ' +
                                   translationService.translate('Common.Label.QualityAcronym') + ': ' + corrugateInfo.Quality
                           });
                       }
                   }

                   if (producible.ProducibleType === 'Kit') {
                       Enumerable.From(producible.Producible.ItemsToProduce).ForEach(function (item) {
                           item.ProducibleInfo = producibleHelperService.getDiplayValue(item);
                       });
                   }
               });


               delete $scope.additionalDetails;
               $scope.additionalDetails = Enumerable.From(msg.Data).Count() >= searchConfigurationService.getMaxOrdersSearchResults() ? translationService.translate('Common.Message.ShowingFirstResults', searchConfigurationService.getMaxOrdersSearchResults()) : '';


               delete $scope.gridOptions.data;
               $scope.gridOptions.data = msg.Data;

               delete $scope.status;
               $scope.status = '';
           }, 0, true);
       }, $scope, 'OrderSearch');

    var requestData = function () {
        messagingService.publish("GetCorrugates");
        messagingService.publish("GetProductionGroups");
        messagingService.publish("GetMachines");

    };

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    var showingDetailsBeforeHelp = $scope.showDetails;
    $scope.tellMeAboutThisPage = function () {
        showingDetailsBeforeHelp = $scope.showDetails;
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        steps.push({
            element: '#status',
            intro: translationService.translate('Search.Common.Help.Status')
        });

        steps.push({
            element: "#customerUniqueId",
            intro: translationService.translate('Search.Common.Help.CustomerId')
        });

        steps.push({
            element: "#beginDateWrapper",
            intro: translationService.translate('Search.Common.Help.DateBegin')
        });

        steps.push({
            element: "#endDateWrapper",
            intro: translationService.translate('Search.Common.Help.DateEnd')
        });

        steps.push({
            element: "#articleId",
            intro: translationService.translate('Search.Common.Help.ArticleId')
        });

        steps.push({
            element: "#designId",
            intro: translationService.translate('Search.Common.Help.Design')
        });

        steps.push({
            element: "#length",
            intro: translationService.translate('Search.Common.Help.CartonLength')
        });

        steps.push({
            element: "#width",
            intro: translationService.translate('Search.Common.Help.CartonWidth')
        });

        steps.push({
            element: "#height",
            intro: translationService.translate('Search.Common.Help.CartonHeight')
        });

        steps.push({
            element: "#corrugateAlias",
            intro: translationService.translate('Search.Common.Help.Corrugate')
        });

        steps.push({
            element: "#corrugateQualityWrapper",
            intro: translationService.translate('Search.Common.Help.CorrugateQuality')
        });

        steps.push({
            element: "#machine",
            intro: translationService.translate('Search.Common.Help.Machine')
        });

        steps.push({
            element: "#toggleDetails",
            intro: translationService.translate('Search.Common.Help.AdditionalOptions')
        });

        steps.push({
            element: "#searchButton",
            intro: translationService.translate('Search.Common.Help.Search')
        });

        steps.push({
            element: "#reproduceButton",
            intro: translationService.translate('Search.Common.Help.Reproduce')
        });

        steps.push({
            element: "#cancelButton",
            intro: translationService.translate('Search.Common.Help.Cancel')
        });

        steps.push({
            element: "#createNextButton",
            intro: translationService.translate('Search.Common.Help.CreateNext')
        });

        steps.push({
            element: "#toggleFiltering",
            intro: translationService.translate('Search.Common.Help.ResultsFilter')
        });

        steps.push({
            element: "#resultGrid",
            intro: translationService.translate('Search.Common.Help.Columns.Overview')
        });

        steps.push({
            element: columnDefs[2],
            intro: translationService.translate('Search.Common.Help.Columns.Reproduce')
        });

        steps.push({
            element: columnDefs[3],
            intro: translationService.translate('Search.Common.Help.Columns.CustomerUniqueId')
        });

        steps.push({
            element: columnDefs[4],
            intro: translationService.translate('Search.Common.Help.Columns.ArticleId')
        });

        steps.push({
            element: columnDefs[5],
            intro: translationService.translate('Search.Order.Help.Columns.Quantities')
        });

        steps.push({
            element: columnDefs[6],
            intro: translationService.translate('Search.Common.Help.Columns.CreatedDate')
        });

        steps.push({
            element: columnDefs[7],
            intro: translationService.translate('Search.Common.Help.Columns.Status')
        });

        steps.push({
            element: columnDefs[8],
            intro: translationService.translate('Search.Common.Help.Columns.Type')
        });

        steps.push({
            element: columnDefs[9],
            intro: translationService.translate('Search.Order.Help.Columns.Dimensions')
        });
        steps.push({
            element: columnDefs[10],
            intro: translationService.translate('Search.Order.Help.Columns.Design')
        });

        steps.push({
            element: columnDefs[11],
            intro: translationService.translate('Search.Common.Help.Columns.Corrugate')
        });

        steps.push({
            element: columnDefs[12],
            intro: translationService.translate('Search.Order.Help.Columns.ProducedOn')
        });

        steps.push({
            element: columnDefs[13],
            intro: translationService.translate('Search.Common.Help.Columns.TimesProduced')
        });

        return steps;
    };

    var afterIntro = function () {
        if (showingDetailsBeforeHelp !== $scope.showDetails) $scope.toggleDetails();
    };

    introJsService.onbeforechange(function () {
        if (!$scope.showDetails) {
            $scope.toggleDetails();
        }
    });

    introJsService.onexit(afterIntro);
    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage, true);
}

function SearchOrderConfirmationDialogCtrl($log, $modalInstance, $scope, messagingService, translationService, selectedOrders, cancelling, grid) {
    selectedOrders.forEach(function (order) {
        if (cancelling) {
            order.message = translationService.translate('Search.Order.CancelDialog.Message.Body', order.CustomerUniqueId, order.RemainingQuantity);
        }
        else {
            if (!order.CustomerUniqueId) order.CustomerUniqueId = "sub article";
            order.message = translationService.translate('Search.Order.ReproduceDialog.Message.Body', order.CustomerUniqueId, order.RemainingQuantity, order.uiReproduceQuantity, order.newTotal);
        }
    });

    $scope.cancelling = cancelling;
    $scope.selectedOrders = selectedOrders;
    $scope.cancelOrReproduceTitle = cancelling ? translationService.translate('Search.Order.CancelDialog.Label.Header') : translationService.translate('Search.Order.ReproduceDialog.Label.Header');

    $scope.closeBreakOrdersDialogWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.reproduce = function (result) {
        if ($scope.cancelling) {
            var orderUpdatePayload = Enumerable.From($scope.selectedOrders).Select("o=>o.Id").ToArray();
            messagingService.publish("CancelOrders", orderUpdatePayload);
            grid.selection.clearSelectedRows();
        }
        else {
            var ordersToReproduce = Enumerable.From($scope.selectedOrders).Select(function (selectedOrder) {
                return {
                    Id: selectedOrder.Id,
                    CustomerUniqueId: selectedOrder.CustomerUniqueId,
                    ReproducedQuantity: selectedOrder.uiReproduceQuantity
                };
            }).ToArray();

            $log.info('Saving broken box request Group... ');
            messagingService.publish("ReproduceOrders", ordersToReproduce);
        }

        $modalInstance.close();
    };
}

function CreateNextSearchDialogCtrl($scope, $log, messagingService, $modalInstance, selectedOrders) {

    $scope.selectedOrders = selectedOrders;

    $scope.closeDialogWindow = function () {
        $modalInstance.close();
    };

    $scope.getOrdersList = function () {
        var val = Enumerable.From($scope.selectedOrders).Select("o=>o.OrderId").Aggregate("a,b=>a + ',' + b");
        return val;
    };

    $scope.confirmNext = function () {
        var orderUpdatePayload = Enumerable.From($scope.selectedOrders).Select("o=>o.Id").ToArray();
        messagingService.publish("UpdateOrdersPriority", orderUpdatePayload);
        $scope.closeDialogWindow();
    };
}