﻿function SearchOrdersHistoryCtrl($log, $scope, messagingService) {

    $scope.searchDate = moment().subtract('days', 1).format('MM/DD/YYYY hh:mm a');
    $scope.orders = [];
    $scope.searching = false;

    messagingService.ordersSearchObservable
        .autoSubscribe(function (msg) {
            delete $scope.orders;
            $scope.orders = msg.Data;
            $scope.searching = false;
            if ($scope.debug) {
                $log.info([moment().format("HH:mm:ss.SSS"), msg.Data]);
            }
        }, $scope, 'Orders Search');

    $scope.searchOrders = function () {
        $scope.searching = true;
        messagingService.requestData("OrdersSearch", { Type: 'OrderId', SearchText: $scope.searchText, SinceDate: $scope.searchDate });
    };
}

function OrdersHistoryCtrl($log, $scope, $timeout, machineGroupService, messagingService, translationService, producibleHelperService, NOTIFICATIONS_EVENTS) {
    $timeout = $timeout.getInstance($scope);

    $scope.sortOptions = [
        {
            UserFriendlyText: translationService.translate('MachineProduction.Order.Label.LastChangedDate'),
            sortValue: '-StatusChangedDateTime'     // negative here will sort by descending
        },
        {
            UserFriendlyText: translationService.translate('Order.Label.Id'),
            sortValue: 'OrderId'
        }
    ];

    $scope.selectedSortOption = $scope.sortOptions[0];

    $scope.changeSort = function (newSort) {
        $timeout(function () {
            delete $scope.selectedSortOption;
            $scope.selectedSortOption = newSort;
        }, 0, true);
    };

    function updateOrders(msg) {
        delete $scope.orders;
        $scope.orders = msg.Data;
        // why are we doing this?
        //var machineGroup = machineGroupService.getCurrentMachineGroup();
        //if (machineGroup) {
        //    var machineGroupId = machineGroup.Id; // show all jobs sent to my machine group
        //    if (machineGroup.ProductionMode === "AutoProductionMode") // show all jobs sent to my production group
        //    {
        //        if (machineGroup.ProductionGroup == undefined)
        //            return;

        //        machineGroupId = machineGroup.ProductionGroup.Id;
        //    }

        //    $scope.orders = Enumerable.From(msg.Data).Where(function (item) {
        //        return Enumerable.From(item.Restrictions).Any(function (r) {
        //            return r.Value == machineGroupId;
        //        });
        //    }).ToArray();
        //} else {
        //    $scope.orders = msg.Data;
        //}
    }

    function updateOrderInCompletedOrderList(msg) {
        var machineGroup = machineGroupService.getCurrentMachineGroup();
        if (machineGroup) {
            var machineGroupId = machineGroup.Id; // show all jobs sent to my machine group
            if (machineGroup.ProductionMode === "AutoProductionMode") // show all jobs sent to my production group
            {
                if (machineGroup.ProductionGroup == undefined)
                    return [];

                machineGroupId = machineGroup.ProductionGroup.Id;
            }

            var thisOrderIsForThisMachineGroup = Enumerable.From(msg.Data.Restrictions).Any(function (r) {
                return r.Value === machineGroupId;
            });

            if (thisOrderIsForThisMachineGroup) {
                if (msg.Data.RemainingQuantity == 0 && msg.Data.InProgressQuantity === 0) {
                    if (Enumerable.From($scope.orders)
                        .Any(function (o) {
                            return o.Id === msg.Data.Id; //TODO: This is never called when running!!! The list grows to infinity and kills the browser
                    })) {
                        $scope.orders.pop(msg.Data);
                    } //Don't remove the item and then add it again ಠ_ಠ
                    else if ($scope && $scope.orders) {
                        $scope.orders.unshift(msg.Data);
                        if ($scope.orders.length > 20) { //TODO: remove this when you figure out how to maintain this list correctly
                            $scope.orders.splice(19, $scope.orders.length - 20);
                        }
                    }
                }
            }
			if($scope.orders)
			{
				console.log("ordersHistory $scope.orders length: " + $scope.orders.length);
			}
			else{
				console.log("ordersHistory $scope.orders is undefined");
			}
        }
    }

    $scope.$on(NOTIFICATIONS_EVENTS.producibleCompleted, function () {
        //We don't use the helper for the orders view but it is tracking every carton. Set the list to empty to avoid a major leak
        producibleHelperService.setProduciblesCompletedList([]);
    });
    $scope.$on('$destroy',
        function () {
            if (!$scope.orders) {
                return;
            }

            $scope.orders.forEach(function (o) {
                delete o;
                o = undefined;
            });

            delete $scope.orders;
        });
    //Ask for initial set of data
    messagingService.publish("GetOrdersHistory", "");

    //Take the first message so we don't get a 2 second lag
    messagingService.ordersHistoryObservable
        .first()
        .autoSubscribe(updateOrders, $scope, 'Orders History (1)');

    //Limit the times we update the UI (only once every 2 seconds)
    messagingService.ordersHistoryObservable
        .skip(1)
        .sample(2000)
        .autoSubscribe(updateOrders, $scope, 'Orders History (2)');

    messagingService.orderChangedObservable
        .autoSubscribe(updateOrderInCompletedOrderList, $scope, 'Order changed');
}

app.directive('ordersHistory', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/SelectionAlgorithm/Orders/ordersHistory.html',
        link: {
            pre: function postLink(scope, element, attrs) {
                if (scope.searchText != undefined && scope.searchText.length > 0) {
                    scope.orderFilter = { OrderId: scope.searchText };
                }

                if (attrs.searching != undefined)
                    scope.searching = true;
            }
        }
    };
});