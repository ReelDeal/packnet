﻿function CancelOrdersDialogCtrl($log, $modalInstance, $scope, messagingService, machineGroupService, selectedOrders, alertService, translationService) {
    var awaitingUpdate = false;
    var ordersCancellationConfirmed = false;
    $scope.machineGroup = machineGroupService.getCurrentMachineGroup();

    $scope.selectedOrders = selectedOrders;

    messagingService.ordersObservable
        .autoSubscribe(function (msg) {
            if ($scope.debug) {
                $log.info([moment().format("HH:mm:ss.SSS"), msg.Data]);
            }

            if (ordersCancellationConfirmed)

                setTimeout(function () {
                    messagingService.publish("GetMachineGroupProductionQueue", "");
                }, 1000);

            $scope.closeOrdersDialogWindow();
            awaitingUpdate = false;
        }, $scope, 'Orders');

    $scope.closeOrdersDialogWindow = function () {
        setTimeout(function () {
            $modalInstance.close();
        }, 300);
    };

    $scope.confirmCancelOrders = function (result){
        if (awaitingUpdate) {
            alertService.addError(translationService
                .translate('MachineProduction.Order.CancelDialog.Message.CancelationInProgress'));
            return;
        }
        awaitingUpdate = true;
        ordersCancellationConfirmed = true;
        $log.info('Cancelling orders... ');
        $log.info(result);


        //Cancellation for STC orders
        if ($scope.machineGroup &&
            $scope.machineGroup.ProductionMode === "AutoProductionMode" &&
            $scope.machineGroup.ProductionGroup &&
            $scope.machineGroup.ProductionGroup.SelectionAlgorithm === "ScanToCreate") {
            messagingService.publish("RemoveScanToCreateCartons", $scope.selectedOrders);
        }
        //Cancellation for non-STC orders (usually orders)
        else {
            var orderUpdatePayload = Enumerable.From($scope.selectedOrders).Select("o=>o.Id").ToArray();
            messagingService.publish("CancelOrders", orderUpdatePayload);
        }

        setTimeout(function () {
                if (awaitingUpdate) {
                    alertService.addError(translationService
                        .translate('MachineProduction.Order.CancelDialog.Message.CancelationFailed'));
                    $scope.closeOrdersDialogWindow();
                    awaitingUpdate = false;
                }
            },
            22000);
    };

    $scope.canConfirmCancel = function () {
        return awaitingUpdate;
    }
}

function CreateNextDialogCtrl($modalInstance, $scope, machineGroupService, messagingService, selectedOrders) {

    $scope.selectedOrders = selectedOrders;

    $scope.closeDialogWindow = function () {
        $modalInstance.close();
    };

    $scope.getOrdersList = function () {
        var val = Enumerable.From($scope.selectedOrders).Select("o=>o.OrderId").Aggregate("a,b=>a + ',' + b");
        return val;
    };

    $scope.createNext = function () {

        var orderUpdatePayload = Enumerable.From($scope.selectedOrders).Select("o=>o.Id").ToArray();
        messagingService.publish("UpdateOrdersPriority", orderUpdatePayload);
        $scope.closeDialogWindow();
    };
}

function OrdersListCtrl($scope, $modal, messagingService, machineGroupService, $timeout, $filter) {
    $timeout = $timeout.getInstance($scope);
    var allOrders = [];
    var filterByMachineOrPgSubject = new Rx.Subject();

    if (!$scope.orderFilter) {

        delete $scope.orderFilter;
        $scope.orderFilter = { OrderId: '' };
    }
    $scope.isAttached = false;
    $scope.awaitingUpdate = false;

    $scope.selectedOrders = function () {
        return $filter('filter')($scope.orders, { selected: true });
    };

    $scope.machineGroup = machineGroupService.getCurrentMachineGroup();

    //This property can be set through a directive attribute
    if ($scope.displayAllOrders == undefined) {
        $scope.displayAllOrders = true;
    }

    var setPriority = function () {
        //Take all the orders, put 'In progress' at the top, 'In Queue' next, then errors, and finally 'Completed' at the bottom 
        var selectedOrdersEnumerable = Enumerable.From($scope.selectedOrders());

        var inQueueSelectedOrders = selectedOrdersEnumerable
            .Select(function (o) {
                o.Priority = 0;
                return o;
            })
            .OrderByDescending(function (o) { return o.ProductionSequence; })
            .ThenBy(function (o) { return o.Created; });

        var ordersEnumerable = Enumerable.From($scope.orders)
            .Except(inQueueSelectedOrders);

        var reprioritizedOrders = ordersEnumerable
            .OrderBy(function (o) { return o.ProducibleStatus === "ProducibleProductionStarted" ? 0 : 1; })
            .ThenBy(function (o) {
                return o.ProducibleStatus === "ProducibleStaged" ? 0 : 1;
            })
            .ThenBy(function (o) { return o.ProductionSequence; });

        var currentPriority = 1;

        selectedOrdersEnumerable.ForEach(function (o) {
            o.Priority = currentPriority++;
        });

        reprioritizedOrders.ForEach(function (o) {
            o.Priority = currentPriority++;
        });
    };

    var setOrders = function (data) {
        $timeout(function () {
            $scope.awaitingUpdate = false;
            var selectedOrderIds = Enumerable.From($scope.orders).Where("o=>o.selected").Select("o=>o.Id");

            delete $scope.orders;
            if (selectedOrderIds.Any()) {
                var newOrders = Enumerable.From(data).Select(function (order) {
                    order.selected = selectedOrderIds.Any(function (id) {
                        return id === order.Id;
                    });
                    return order;
                });
                $scope.orders = newOrders.Take(20).ToArray();
            } else {
                var enumerable = Enumerable.From(data);
                $scope.totalOrderCount = enumerable.Count();
                $scope.orders = enumerable.Take(20).ToArray();
            }

            setPriority();
            $scope.orderIsSelected = $scope.selectedOrders().length > 0;
        }, 10, true);

    };

    var filterOrdersByPG = function (productionGroupId) {
        var filteredByPg = Enumerable.From(allOrders)
            .Where(function (order) {
                return Enumerable.From(order.Restrictions).Any(function (r) {
                    return r.Value === productionGroupId;
                });
            });

        setOrders(filteredByPg.ToArray());
    };

    var filterOrdersByMachine = function (machineId) {
        var filteredByMachine = Enumerable.From(allOrders)
            .Where(function (order) {
                return Enumerable.From(order.Restrictions).Any(function (r) {
                    return r.Value === machineId;
                });
            });

        setOrders(filteredByMachine.ToArray());
    };

    function clearSelected() {
        $timeout(function () {
            var tmp = $scope.selectedOrders();
            for (var i = 0; i < tmp.length; i++) {
                tmp[i].selected = false;
            }
            $scope.orderIsSelected = false;
        }, 10, true);

    };

    $scope.removeSelectedOrders = function () {
        var orders = $scope.selectedOrders();
        if (orders.length > 0) {

            var opts = {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'partials/MachineManager/cancelOrdersDialog.html',
                controller: 'CancelOrdersDialogCtrl',
                resolve: {
                    selectedOrders: function () {
                        return orders;
                    }
                }
            };
            $modal.open(opts);
        }
    };

    $scope.moveSelectedToNext = function () {
        if ($scope.selectedOrders().length > 0) {

            var opts = {
                backdrop: 'static',
                keyboard: false,
                templateUrl: 'partials/MachineManager/createNextDialog.html',
                controller: 'CreateNextDialogCtrl',
                resolve: {
                    selectedOrders: function () {
                        return $scope.selectedOrders();
                    }
                }
            };
            $modal.open(opts);
            clearSelected();
        }
    };

    $scope.selectOrder = function (order) {
        $timeout(function () {
            order.selected = !order.selected;
            $scope.orderIsSelected = $scope.selectedOrders().length > 0;
        }, 10, true);

    };

    //The filter is getting pounded, it processes the entire list
    // Put on an observable so it can be throttled
    filterByMachineOrPgSubject
        .sample(1000)
        .autoSubscribe(
            function () {
                if ($scope.displayAllOrders) {
                    setOrders(allOrders);
                } else if ($scope.$parent.selectedProductionGroup) {
                    $scope.filterOrdersByPG($scope.$parent.selectedProductionGroup.Alias);
                } else {
                    if ($scope.machineGroup) {
                        if ($scope.machineGroup.ProductionMode === "AutoProductionMode" && $scope.machineGroup.ProductionGroup)
                            filterOrdersByPG($scope.machineGroup.ProductionGroup.Id);
                        else
                            filterOrdersByMachine($scope.machineGroup.Id);
                    }
                    else {
                        setOrders(allOrders);
                    }
                }

            }, $scope, 'filterByMachineOrPgSubject');

    var statusWatcher = $scope.$watch('currentMachine.Status', function () {
        if ($scope.currentMachine == undefined)
            return;
        if ($scope.currentMachine.Status === 'Detached' || $scope.currentMachine.Status === 'Attached')
            $scope.isAttached = $scope.currentMachine.Status === 'Attached';

        filterByMachineOrPgSubject.onNext();
    });

    var selectedProductionGroupWatcher = $scope.$parent.$watch('$parent.selectedProductionGroup', function () {
        if ($scope.$parent.selectedProductionGroup) {
            filterByMachineOrPgSubject.onNext();
        }
    });

    machineGroupService.currentMachineGroupUpdatedObservable
        .autoSubscribe(function (machineGroup) {
            delete $scope.machineGroup;
            $scope.machineGroup = machineGroup;
        }, $scope, "current machine group updated");

    //Take the first message so we don't get a 2 second lag
    messagingService.ordersObservable
        .first()
        .autoSubscribe(function (msg) {
            allOrders = msg.Data;
            filterByMachineOrPgSubject.onNext();
        }, $scope, 'Orders First');

    ////Limit the times we update the UI (only once every 2 seconds)
    messagingService.ordersObservable
        .skip(1)
        .autoSubscribe(function (msg) {
            allOrders = msg.Data;
            filterByMachineOrPgSubject.onNext();
        }, $scope, 'Orders Sampled');

    function orderExistsInOpenOrdersList(order) {
        return allOrders.filter(function (ord) { return ord.Id === order.Id; }).length > 0;
    };

    function orderIsCompleted(order) {
        return order.ProducibleStatus == "ProducibleCompleted" || order.ProducibleStatus == "ProducibleRemoved";
    };

    function removeOrderFromOpenOrdersList(order) {
        allOrders = allOrders.filter(function (ord) {
            return ord.Id != order.Id;
        });
    };

    function updateOrderInOpenOrdersList(order) {
        allOrders = allOrders.map(function (ord) {
            if (ord.Id === order.Id) {
                return order;
            }
            else {
                return ord;
            }
        });
    };

    messagingService.orderChangedObservable
        .autoSubscribe(function (msg) {
            if (!orderExistsInOpenOrdersList(msg.Data) && !orderIsCompleted(msg.Data)) {
                allOrders.push(msg.Data);
            };

            if (orderIsCompleted(msg.Data)) {
                removeOrderFromOpenOrdersList(msg.Data);
            };

            updateOrderInOpenOrdersList(msg.Data);
            console.log("ordersListCtrl allOrders length:" + allOrders.length);
            filterByMachineOrPgSubject.onNext();
        }, $scope, 'Order Changed');

    $scope.$on('$destroy',
            function () {
                statusWatcher();
                selectedProductionGroupWatcher();

                allOrders.forEach(function (o) {
                    delete o;
                    o = undefined;
                });
                delete allOrders;
                allOrders = undefined;
            });
    //Ask for initial set of data
    messagingService.publish("GetOrders", "");

    setPriority();
}

app.directive('ordersList', function () {
    return {
        restrict: 'E',
        scope: true,
        templateUrl: 'partials/SelectionAlgorithm/Orders/ordersList.html',
        link: {
            pre: function postLink(scope, element, attrs) {

                scope.displayAllOrders = attrs.displayAllOrders != undefined;

                if (scope.searchText != undefined && scope.searchText.length > 0) {
                    scope.orderFilter = { OrderId: scope.searchText };
                }

                scope.totalOrderCount = 0;

                if (attrs.searching != undefined)
                    scope.searching = true;

                scope.operatorView = attrs.operatorView != undefined;
            }
        }
    };
});