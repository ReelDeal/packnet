function BoxFirstSearchController($filter, $log, $modal, $scope, localStorageService, $timeout, alertService, introJsService, messagingService, packagingDesignService, searchConfigurationService, uiGridConstants, dateValidationService, translationService) {
    $scope.$timeout = $timeout.getInstance($scope, 'Search Box First Control');
    var awaitingResponse = true;
    $scope.showDetails = false;
    $scope.filtersEnabled = false;
    $scope.labelDetails = '';
    $scope.filteringLabel = '';
    $scope.disableReproduce = true;
    $scope.disableRemove = true;
    $scope.status = '';

    var date = new Date();
    date.setDate(date.getDate() - 1);

    var endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999);

    //setup initial criteria
    $scope.criteria = {
        Created: {
            $gte: date,
            $lt: endOfDay
        },
        CustomerUniqueId: ''
    };

    $scope.data = {};
    $scope.producible = {};
    $scope.producibleRestrictions = {};
    $scope.corrugate = {};
    $scope.machine = {};
    $scope.OpenStatusList = ['ProducibleImported', 'ProducibleStaged', 'ProducibleSentToMachineGroup', 'ProducibleFlaggedForReproduction', 'ProducibleSelected', 'ProducibleRemoved'];
    $scope.InProgressStatusList = ['ProducibleSentToMachine', 'ProducibleProductionStarted', 'LabelWaitingToBePeeledOff', 'ProducibleWaitingForTrigger'];
    $scope.ClosedStatusList = ['ProducibleCompleted'];
    $scope.FailedStatusList = ['ProducibleFailed'];
    $scope.NotProducibleStatusList = ['NotProducible'];
    $scope.RemovedStatusList = [];

    //Added to fix TFS 9460
    $scope.distinctAndSortedCorrugateQualityOptions = [];

    $scope.toggleDetails = function (args){
        $timeout(function () {
            if (typeof args !== 'undefined' && (args === true || args === false)) {
                $scope.showDetails = args;
            }
            else {
                $scope.showDetails = !$scope.showDetails;
            }
        }, 0);
    };

    $scope.data = localStorageService.get("BoxFirstSavedSettings");
    if ($scope.data != undefined) {
        $scope.producible.CustomerUniqueId = $scope.data.ArticleId;
        $scope.corrugate.Alias = $scope.data.Alias;
        $scope.corrugate.Quality = $scope.data.Quality;
        $scope.producible.DesignId = $scope.data.DesignId;
        $scope.producible.Height = $scope.data.Height;
        $scope.producible.Length = $scope.data.Length;
        $scope.producible.Width = $scope.data.Width;
        $scope.producibleRestrictions.PickZone = $scope.data.PickZone;
        $scope.producibleRestrictions.Classification = $scope.data.Classification;
        $scope.producibleRestrictions.CartonPropertyGroup = $scope.data.CartonPropertyGroup;
        $scope.machine.ProducedOnMachineGroupId = $scope.data.ProducedOnMachineGroupId;
        $scope.class = "btn-success";
    }
    else {
        $scope.class = "btn-default";
    }

    $scope.$watch('showDetails', function (newValue) {
        $scope.labelDetails = newValue ? translationService.translate('Search.Common.Label.HideOptions') : translationService.translate('Search.Common.Label.ShowOptions');
    });

    $scope.selectableStatuses = [
    {
        text: translationService.translate('ProducibleStatus.Label.Generic.Open'),
        value: [{ 'ProducibleStatus': 'ProducibleImported' }, { 'ProducibleStatus': 'ProducibleStaged' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.InProgress'),
        value: [
            { 'ProducibleStatus': 'ProducibleSelected' }, { 'ProducibleStatus': 'ProducibleFlaggedForReproduction' },
            { 'ProducibleStatus': 'ProducibleProductionStarted' },
            { 'ProducibleStatus': 'ProducibleSentToMachineGroup' },
            { 'ProducibleStatus': 'ProducibleSentToMachine' }, { 'ProducibleStatus': 'AddedToMachineGroupQueue' }
        ]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Closed'),
        value: [{ 'ProducibleStatus': 'ProducibleCompleted' }, { 'ProducibleStatus': 'ProducibleRemoved' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Failed'),
        value: [{ 'ProducibleStatus': 'NotProducible' }, { 'ProducibleStatus': 'ProducibleFailed' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.NotProducible'),
        value: [{ 'ProducibleStatus': 'NotProducible' }]
    }];

    var compareOnProperty = function (a, b, propertyName){
        if (a[propertyName] < b[propertyName]) {
            return -1;
        }

        if (a[propertyName] > b[propertyName]) return 1;

        return 0;
    };

    var compareOnAlias = function (a, b){
        return compareOnProperty(a, b, 'Alias');
    };

    $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    packagingDesignService.allDesignsObservable.autoSubscribe(function () {
        
        delete $scope.selectableDesigns;
        $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    }, $scope, 'Get Designs');

    messagingService.corrugatesObservable.autoSubscribe(function (msg) {
        
        delete $scope.allCorrugates;
        $scope.allCorrugates = msg.Data.sort(compareOnAlias);
        //Added to fix TFS 9460
        
        delete $scope.distinctAndSortedCorrugateQualityOptions;
        $scope.distinctAndSortedCorrugateQualityOptions = Enumerable.From(msg.Data).Select(function (m) { return m.Quality; }).Distinct().OrderBy(function (i) { return i; }).ToArray();
    }, $scope, 'Corrugates');

    messagingService.machineGroupsObservable.autoSubscribe(function (msg) {
        
        delete $scope.allMachineGroups;
        $scope.allMachineGroups = msg.Data.sort(compareOnAlias);
    }, $scope, 'Machine Groups');

    messagingService.cartonPropertyGroupsObservable.autoSubscribe(function (msg) {
        
        delete $scope.allCartonPropertyGroups;
        $scope.allCartonPropertyGroups = msg.Data.sort(compareOnAlias);
    }, $scope, 'Carton Property Groups');

    messagingService.classificationsObservable.autoSubscribe(function (msg) {
        
        delete $scope.allClassifications;
        $scope.allClassifications = msg.Data.sort(compareOnAlias);
    }, $scope, 'Classifications');

    messagingService.pickZonesObservable.autoSubscribe(function (msg) {
        
        delete $scope.allPickZones;
        $scope.allPickZones = msg.Data.sort(compareOnAlias);
    }, $scope, 'Pick Zones');

    $scope.toggleFiltering = function () {
        $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
    };

    $scope.clearAll = function () {
        $scope.gridApi.selection.clearSelectedRows();
    };

    var enableReproduce = function (){
        $scope.disableReproduce = getItemsToReproduce().length == 0;
    };

    var enableRemove = function (){
        
        delete $scope.disableRemove;
        $scope.disableRemove = $scope.gridApi.selection.getSelectedRows().length == 0;
    };

    var getItemsToReproduce = function () {
        var items = [];
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (selectedRow) {
            if (selectedRow.ProducibleStatus == 'ProducibleCompleted') {
                items.push(selectedRow);
            }
        });

        return items;
    };

    var copyToClipboard = function (text) {
        var textArea = document.createElement("textarea");

        //
        // *** This styling is an extra step which is likely not required. ***
        //
        // Why is it here? To ensure:
        // 1. the element is able to have focus and selection.
        // 2. if element was to flash render it has minimal visual impact.
        // 3. less flakyness with selection and copying which **might** occur if
        //    the textarea element is not visible.
        //
        // The likelihood is the element won't even render, not even a flash,
        // so some of these are just precautions. However in IE the element
        // is visible whilst the popup box asking the user for permission for
        // the web page to copy to the clipboard.
        //

        // Place in top-left corner of screen regardless of scroll position.
        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;

        // Ensure it has a small width and height. Setting to 1px / 1em
        // doesn't work as this gives a negative w/h on some browsers.
        textArea.style.width = '2em';
        textArea.style.height = '2em';

        // We don't need padding, reducing the size if it does flash render.
        textArea.style.padding = 0;

        // Clean up any borders.
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';

        // Avoid flash of white box if rendered for any reason.
        textArea.style.background = 'transparent';


        textArea.value = text;

        document.body.appendChild(textArea);

        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
            if (!successful) {
                window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
            }
        } catch (err) {
            console.log('Oops, unable to copy');
        }

        document.body.removeChild(textArea);
    };

    $scope.gridOptions = {
        enableGridMenu: true,
        enableFiltering: false,
        headerRowHeight: 36,
        rowHeight: 36,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        enableColumnResizing: true, 
        expandableRowTemplate: 'partials/SelectionAlgorithm/expandableKitsTemplate.html',
        expandableRowHeight: 150,
        showGridFooter: true,
        columnDefs: [
            {
                displayName: translationService.translate('Common.Label.CustomerId'),
                field: 'CustomerUniqueId',
                cellTemplate: '<div class="center ui-grid-cell-contents">&nbsp;{{row.entity.CustomerUniqueId}} <button id="copyToClipboard-{{row.entity.CustomerUniqueId}}" ng-click-copy="{{row.entity.CustomerUniqueId}}" class="btn btn-default pull-right" style="height: 26px; padding-top: 3px;"><i class="fa fa-clipboard"></i></button></div>',
                sortingAlgorithm: function (a, b) {

                    if (isNaN(a) && isNaN(b)) {
                        return a === b ? 0 : (a < b ? -1 : 1);
                    }

                    if (!isNaN(a) && !isNaN(b)) {
                        var idA = parseFloat(a);
                        var idB = parseFloat(b);

                        return idA === idB ? 0 : (idA < idB ? -1 : 1);
                    }

                    return 0;
                }
            },
            {
                name: translationService.translate('Search.BoxFirst.Label.Imported'),
                field : 'Created',
                type : 'date',
                cellFilter : 'date:\'medium\'',
                enableFiltering : false,
                sortingAlgorithm : function (a, b){
                    var timeA = typeof (a) === "string" ? new Date(a).getTime() : a.getTime(),
                        timeB = typeof (b) === "string" ? new Date(b).getTime() : b.getTime();
                    return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                }
            }, // having issues with this filter, better disable it
            {
                name: translationService.translate('Common.Label.Status'),
                field: 'ProducibleStatus',
                cellFilter: 'producibleStatus'
            },
            {
                name: translationService.translate('Common.Label.Type'),
                field: 'ProducibleType',
                cellFilter: 'producibleType'
            },
            {
                name: translationService.translate('Common.Label.ProducedOn'),
                field: 'ProducedOn'
            },
            {
                name: translationService.translate('Search.BoxFirst.Label.Completed'),
                field: 'Completed',
                type: 'date',
                cellFilter : 'date:\'medium\'',
                enableFiltering : false,
                sortingAlgorithm : function (a, b){
                    var timeA = a != null ? new Date(a).getTime() : 0,
                        timeB = b != null ? new Date(b).getTime(): 0;
                    return timeA === timeB ? 0 :( timeA < timeB ? -1: 1);
                }
            },
            // having issues with this filter, better disable it
            {
                name: translationService.translate('Search.Common.Label.TimesCompleted'),
                field: 'CompletedHistoryCount',
                type: 'number'
            }
        ],
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
            
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                enableReproduce();
                enableRemove();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (row) {
                enableReproduce();
                enableRemove();
            });
            gridApi.copyToClipboard = copyToClipboard;
        }
    };

    $scope.$watch('gridOptions.enableFiltering', function (newValue) {
        
        delete $scope.filteringLabel;
        $scope.filteringLabel = newValue ? translationService.translate('Common.Label.DisableResultsFilter') : translationService.translate('Common.Label.EnableResultsFilter');
    });

    var convertToLike = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        secondPart = secondPart.replace('"', '/').replace('"', '/');
        if (secondPart.startsWith('//')) {
            index = firstPart.indexOf(field) - 2;
            firstPart = firstPart.substring(0, index);
            secondPart = secondPart.substring(2, secondPart.length);
        }
        return firstPart + secondPart;
    };
    var convertToISODate = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        secondPart = secondPart.replace('"', 'ISODate(\'').replace('"', '\')');

        return firstPart + secondPart;
    };

    var getNestedCriteria = function (base, obj) {
        var result = '';
        for (var key in obj) {
            if (obj[key] != null && obj[key] != '' && obj[key] != 0) {
                result += ',"' + base + '.' + key + '":';
                if (Enumerable.From(['ArticleId', 'Alias']).Contains(key)) {
                    result += '"' + obj[key] + '"';
                }
                else if (Enumerable.From([ 'ProducedOnMachineGroupId']).Contains(key)) {
                    result += 'CSUUID("' + obj[key] + '")';
                }
                else {
                    result += obj[key];
                }
            }
        }

        return result;
    };

    var removeNull = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        if (secondPart.startsWith('null')) {
            index = firstPart.indexOf(field) - 2;
            firstPart = firstPart.substring(0, index);
            secondPart = secondPart.substring(4, secondPart.length);
        }
        return firstPart + secondPart;
    };

    $scope.getTranslation = function (args){
        var x = translationService.translate(args);
        return x;
    };

    $scope.save = function () {
        $scope.data = {
            ArticleId: $scope.producible.CustomerUniqueId,
            Alias: $scope.corrugate.Alias,
            Quality: $scope.corrugate.Quality,
            DesignId: $scope.producible.DesignId,
            Height: $scope.producible.Height,
            Width: $scope.producible.Width,
            Length: $scope.producible.Length,
            PickZone: $scope.producibleRestrictions.PickZone,
            Classification: $scope.producibleRestrictions.Classification,
            CartonPropertyGroup: $scope.producibleRestrictions.CartonPropertyGroup,
            ProducedOnMachineGroupId: $scope.machine.ProducedOnMachineGroupId
        };

        localStorageService.set("BoxFirstSavedSettings", $scope.data);
        alertService.addSuccess(translationService.translate('Common.Message.SavedSearch'));
        $scope.class = "btn-success";
    };

    $scope.reset = function () {
        localStorageService.remove("BoxFirstSavedSettings");
        $scope.producible.CustomerUniqueId = "";
        $scope.corrugate.Alias = "";
        $scope.corrugate.Quality = "";
        $scope.machine._id = "";
        $scope.producible.DesignId = "";
        $scope.producible.Height = "";
        $scope.producible.Width = "";
        $scope.producible.Length = "";
        $scope.producibleRestrictions.PickZone = "";
        $scope.producibleRestrictions.Classification = "";
        $scope.producibleRestrictions.CartonPropertyGroup = "";
        $scope.machine.ProducedOnMachineGroupId = "";
        $scope.class = "btn-default";
    };

    $scope.search = function () {
        $scope.gridApi.selection.clearSelectedRows();

        $scope.isDateRangeInvalid = !dateValidationService.validateSearchDates($scope.criteria.Created.$gte, $scope.criteria.Created.$lt);
        if ($scope.isDateRangeInvalid) {
            return;
        }

        
        delete $scope.status;
        $scope.status = translationService.translate('Search.Common.Message.Searching');
        
        $scope.criteria.CustomerUniqueId = $scope.criteria.CustomerUniqueId.replace(/"/g, '');
        //Starting a new search, clear the old results
        
        delete $scope.gridOptions.data;
        $scope.gridOptions.data = [];

        $scope.clearAll();

        var criteria = JSON.stringify($scope.criteria);
        criteria = convertToLike(criteria, "CustomerUniqueId");
        criteria = convertToISODate(criteria, '$gte');
        criteria = convertToISODate(criteria, '$lt');
        criteria = removeNull(criteria, '$or');
        criteria = removeNull(criteria, 'ProducedOnMachineGroupId');

        //Begin Fixes for TFS 9460 - BoxFirst (and last) Search Page: In additional search options, when corrugate quality is equal to 0, the search result list is wrong.
        /*
        1.We were providing a bunch of options arbitrarily with numeric up-down which is not right. We need to get a list of the unique corrugate qualities
        in the system and provide those in a numerically sorted list with All at the top of it. This is accomplished using $scope.distinctAndSortedCorrugateQualityOptions.
        2.Changed numeric up/down input to a select (dropdown) because we didn't correctly deal with the nullable integer. Added an "All" option.
        3.Used a primitive array of numbers to bind to the select element while keeping id for automated testing.
        4.Search behavior jacked up: 0 does not mean "All." 0 means use 0 in the search. All means don't apply the filter. See above items about nullable int.
        5.We didn't use the right string filter for criteria and corrugate quality or it got changed out at some point so mongo query was wrong. Added a check for
        a useable numeric value coming in and fixed the filter string.
        */
        var nestedCriteria = getNestedCriteria('Producible.ItemsToProduce', $scope.producible);

        if ($scope.corrugate.Quality !== null && !isNaN($scope.corrugate.Quality)) {
            //The getNestedCriteria does not work well with primitive arrays so just concatenate it in here and clean up below with the slicing
            nestedCriteria += ',"Producible.ItemsToProduce.CorrugateQuality":' + $scope.corrugate.Quality;
        }

        //Check if the alias is present and assemble search string for it
        if ($scope.corrugate.Alias !== null && typeof ($scope.corrugate.Alias) !== "undefined") {
            //The getNestedCriteria does not work well with primitive arrays so just concatenate it in here and clean up below with the slicing
            nestedCriteria += ',"Producible.ItemsToProduce.CartonOnCorrugate.Corrugate.Alias":' + "\"" + $scope.corrugate.Alias + "\"";
        }

        nestedCriteria += getNestedCriteria('Producible', $scope.machine);
        //End Fixes for TFS 9460

        
        delete $scope.filtersEnabled;
        $scope.filtersEnabled = (nestedCriteria != "");

        var position = criteria.length - 1;
        criteria = [criteria.slice(0, position), nestedCriteria, criteria.slice(position)].join('');

        for (var key in $scope.producibleRestrictions) {
            if (!$scope.producibleRestrictions[key]) {
                delete $scope.producibleRestrictions[key];
            }
            else {
                $scope.filtersEnabled = true;
            }
        }

        //This criteria remove is part of fix for TFS Bug 10570*:BF Search - Failed w/ no CUID yields zero results. 
        //We cannot send the search in for failed items because the child producibles may be failed and the parent is not - thus no results are returned
        //We need to filter that out in the final grid for display to the user and also doctor up the ViewModel so that any child producible in the view model
        //means the view model shows the parent as failed. Good times... noodle salad.
        //{"Created":{"$gte":ISODate('2015-08-12T20:24:39.302Z'),"$lt":ISODate('2015-08-14T05:59:59.999Z')},"$or":[{"ProducibleStatus":"ProducibleFailed"}]}
        if (criteria.contains(',"$or":[{"ProducibleStatus":"ProducibleFailed"}]}')) {
            criteria = criteria.replace(',"$or":[{"ProducibleStatus":"ProducibleFailed"}]', '');
        }

        messagingService.publish("SearchForProduciblesWithRestrictions", { SelectionAlgorithmTypes : "BoxFirst", Criteria : criteria, Restrictions : $scope.producibleRestrictions });
    };

    $scope.reproduceCancel = function (which) {

        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SelectionAlgorithm/Box First/searchConfirmationDialog.html',
            controller: 'SearchBoxFirstConfirmationDialogCtrl', //defined end of this file
            resolve: {
                selectedOrders: function () {
                    return getItemsToReproduce();
                },
                cancelling: which
            }
        };

        $modal.open(opts).result.then(function () {
            $timeout(function () { $scope.search(); }, 1000);
        });
    };

    $scope.remove = function () {

        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SelectionAlgorithm/Box First/deleteConfirmationDialog.html',
            controller: 'DeleteBoxFirstConfirmationDialogCtrl', //defined end of this file
            resolve: {
                selectedOrders: function () {
                    return $scope.gridApi.selection.getSelectedRows();
                },
            }
        };

        $modal.open(opts).result.then(function () {
            $timeout(function () { $scope.search(); }, 1000);
        });
    };

    messagingService.searchBoxFirstResponseObservable
       .autoSubscribe(function (msg) {
            $timeout(function () {

                    delete $scope.status;
                    $scope.status = translationService.translate('Search.Common.Message.ResultFound');
                    if ($scope.debug) {
                        $log.info([moment().format("HH:mm:ss.SSS"), msg.Data]);
                    }
                    var starttime = moment();
                    console.log(starttime
                        .format("HH:mm:ss.SSS") +
                        " Search result received: " +
                        msg.Data.length +
                        "results");
                    awaitingResponse = false;
                    Enumerable.From(msg.Data)
                        .ForEach(function (producible) {
                            producible.ProducibleType = producible.ProducibleType;
                            producible
                                .Producible = {};
                            //todo: update and remove nested producible when we update other searches to use viewModel
                            producible.Producible.ItemsToProduce = producible.ItemsToProduce;
                            producible.CompletedHistoryCount = 0;
                            producible.ProducedOn = "";
                            //set produced on
                            Enumerable.From(producible.ProductionInfo)
                                .Where(function (h) { return h.Status === "ProducibleCompleted"; })
                                .ForEach(function (h) {
                                    producible.CompletedHistoryCount += 1;
                                    if (h.MachineGroupId) {
                                        //Fix for Bug 10592:BF Search - Deleted MGs show up as Undefined
                                        var matchingMg = Enumerable.From($scope.allMachineGroups)
                                            .FirstOrDefault(h.MachineGroupId,
                                                function (mg) { return mg.Id === h.MachineGroupId; });
                                        var matchingMgAlias = matchingMg.Alias;

                                        if (typeof (matchingMgAlias) == "undefined") {
                                            matchingMgAlias = "";
                                        }
                                        producible.ProducedOn += producible.ProducedOn.length === 0
                                            ? matchingMgAlias
                                            : " ," + matchingMgAlias;
                                    }
                                });

                            producible.uiReproduceQuantity = 0;

                            Enumerable.From(producible.Producible.ItemsToProduce)
                                .ForEach(function (childProducible) {
                                    if (childProducible.ProducibleType === 'Carton') {
                                        childProducible
                                            .Header = $filter('producibleType')(childProducible.ProducibleType) +
                                            ' - ' +
                                            $filter('producibleStatus')(childProducible.ProducibleStatus);
                                        childProducible.Line1 = translationService.translate('Common.Label.LengthAcronym') +
                                            ': ' +
                                            childProducible.Length +
                                            ' ' +
                                            translationService.translate('Common.Label.WidthAcronym') +
                                            ': ' +
                                            childProducible.Width +
                                            ' ' +
                                            translationService.translate('Common.Label.HeightAcronym') +
                                            ': ' +
                                            childProducible.Height;
                                        childProducible.Line2 = translationService.translate('Common.Label.Design') +
                                            ': ' +
                                            Enumerable.From($scope.selectableDesigns)
                                            .Where(function (pg) { return pg.Id === childProducible.DesignId; })
                                            .Select(function (c) { return c.Name; })
                                            .ToArray()
                                            .toString();

                                        if (childProducible.OptimalCorrugate) {
                                            producible.Producible.ItemsToProduce.push({
                                                ProducibleType : 'Corrugate',
                                                Header : translationService.translate('Search.Common.Label.ProducedOnCorrugate'),
                                                Line1 : translationService.translate('Common.Label.Name') +
                                                    ': ' +
                                                    childProducible.OptimalCorrugate.Alias,
                                                Line2 : translationService.translate('Common.Label.WidthAcronym') +
                                                    ': ' +
                                                    childProducible.OptimalCorrugate.Width +
                                                    ' - ' +
                                                    translationService.translate('Common.Label.ThicknessAcronym') +
                                                    ': ' +
                                                    childProducible.OptimalCorrugate.Thickness +
                                                    ' - ' +
                                                    translationService.translate('Common.Label.QualityAcronym') +
                                                    ': ' +
                                                    childProducible.OptimalCorrugate.Quality
                                            });
                                        }
                                    }
                                    if (childProducible.ProducibleType === 'Label') {
                                        childProducible
                                            .Header = $filter('producibleType')(childProducible.ProducibleType) +
                                            ' - ' +
                                            $filter('producibleStatus')(childProducible.ProducibleStatus);
                                        childProducible.Line1 = translationService.translate('Common.Label.Priority') +
                                            ': ' +
                                            childProducible.IsPriority;
                                        childProducible.Line2 = translationService.translate('Common.Label.Template') +
                                            ': ' +
                                            childProducible.Template;
                                    }
                                });
                        });

                    //Part of fix for Bug 10570*:BF Search - Failed w/ no CUID yields zero results. 
                    //If UI search was initiated for failed items We need to return only those items with a ProducibleFailed status
                    if ($scope.criteria &&
                        $scope.criteria.$or &&
                        $scope.criteria.$or[0] &&
                        $scope.criteria.$or[0].ProducibleStatus) {
                        if ($scope.criteria.$or[0].ProducibleStatus.contains("ProducibleFailed")) {
                            msg.Data = Enumerable.From(msg.Data)
                                .Where(function (parentProducible) {
                                    return parentProducible.ProducibleStatus === "ProducibleFailed";
                                })
                                .ToArray();
                        }
                        //If UI search was initatied for open items we need to return The open status items but NOT failed items
                        else if ($scope.criteria.$or[0].ProducibleStatus &&
                        ($scope.criteria.$or[0].ProducibleStatus === 'ProducibleImported' ||
                            $scope.criteria.$or[0].ProducibleStatus === 'ProducibleStaged' ||
                            $scope.criteria.$or[0].ProducibleStatus === 'ProducibleSentToMachineGroup' ||
                            $scope.criteria.$or[0].ProducibleStatus === 'ProducibleFlaggedForReproduction' ||
                            $scope.criteria.$or[0].ProducibleStatus === 'ProducibleSelected' ||
                            $scope.criteria.$or[0].ProducibleStatus === 'ProducibleRemoved')) {
                            msg.Data = Enumerable.From(msg.Data)
                                .Where(function (parentProducible) {
                                    return parentProducible.ProducibleStatus !== "ProducibleFailed";
                                })
                                .ToArray();
                        }
                    }

                    delete $scope.additionalDetails;
                    $scope.additionalDetails = Enumerable.From(msg.Data).Count() >=
                        searchConfigurationService.getMaxBoxFirstSearchResults()
                        ? translationService.translate('Common.Message.ShowingFirstResults',
                            searchConfigurationService.getMaxBoxFirstSearchResults())
                        : '';

                    delete $scope.gridOptions.data;
                    $scope.gridOptions.data = msg.Data;

                    delete $scope.status;
                    $scope.status = '';
                    var endtime = moment();
                    var duration = moment.duration(endtime.diff(starttime));
                    console.log(endtime.format("HH:mm:ss.SSS") + " Search result processed " + duration);
                },
                0,
                true);
        }, $scope, 'BoxFirstSearch');

    var requestData = function (){
        messagingService.publish("GetMachineGroups");
        messagingService.publish("GetCorrugates");

        messagingService.publish("GetCartonPropertyGroups");
        messagingService.publish("GetClassifications");
        messagingService.publish("GetPickZones");
    };

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    var showingDetailsBeforeHelp = $scope.showDetails;
    $scope.tellMeAboutThisPage = function (){
        showingDetailsBeforeHelp = $scope.showDetails;
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");
        steps.push({
            element : '#status',
            intro : translationService.translate('Search.Common.Help.Status')
        });

        steps.push({
            element : "#customerUniqueId",
            intro : translationService.translate('Search.Common.Help.CustomerId')
        });

        steps.push({
            element : "#beginDateWrapper",
            intro : translationService.translate('Search.Common.Help.DateBegin')
        });

        steps.push({
            element : "#endDateWrapper",
            intro : translationService.translate('Search.Common.Help.DateEnd')
        });

        steps.push({
            element : "#designId",
            intro : translationService.translate('Search.Common.Help.Design')
        });

        steps.push({
            element : "#length",
            intro : translationService.translate('Search.Common.Help.CartonLength')
        });

        steps.push({
            element : "#width",
            intro : translationService.translate('Search.Common.Help.CartonWidth')
        });

        steps.push({
            element : "#height",
            intro : translationService.translate('Search.Common.Help.CartonHeight')
        });

        steps.push({
            element : "#corrugateAlias",
            intro : translationService.translate('Search.Common.Help.Corrugate')
        });

        steps.push({
            element : "#corrugateQualityWrapper",
            intro : translationService.translate('Search.Common.Help.CorrugateQuality')
        });

        steps.push({
            element : "#machineGroup",
            intro : translationService.translate('Search.Common.Help.MachineGroup')
        });

        steps.push({
            element : "#searchPickZone",
            intro : translationService.translate('Search.BoxFirst.Help.PickZone')
        });

        steps.push({
            element : "#searchClassification",
            intro : translationService.translate('Search.BoxFirst.Help.Classification')
        });

        steps.push({
            element : "#searchCartonPropertyGroup",
            intro : translationService.translate('Search.BoxFirst.Help.CartonPropertyGroup')
        });

        steps.push({
            element : "#toggleDetails",
            intro : translationService.translate('Search.Common.Help.AdditionalOptions')
        });

        steps.push({
            element : "#searchButton",
            intro : translationService.translate('Search.Common.Help.Search')
        });

        steps.push({
            element : "#reproduceButton",
            intro : translationService.translate('Search.Common.Help.Reproduce')
        });

        steps.push({
            element : "#removeButton",
            intro : translationService.translate('Search.Common.Help.Remove')
        });

        steps.push({
            element : "#toggleFiltering",
            intro : translationService.translate('Search.Common.Help.ResultsFilter')
        });

        steps.push({
            element : "#resultGrid",
            intro : translationService.translate('Search.Common.Help.Columns.Overview')
        });

        steps.push({
            element : columnDefs[2],
            intro : translationService.translate('Search.Common.Help.Columns.CustomerUniqueId')
        });

        steps.push({
            element : columnDefs[3],
            intro : translationService.translate('Search.Common.Help.Columns.ImportedDate')
        });

        steps.push({
            element : columnDefs[4],
            intro : translationService.translate('Search.Common.Help.Columns.Status')
        });

        steps.push({
            element : columnDefs[5],
            intro : translationService.translate('Search.Common.Help.Columns.Type')
        });

        steps.push({
            element : columnDefs[6],
            intro : translationService.translate('Search.Common.Help.Columns.ProducedOn')
        });

        steps.push({
            element : columnDefs[7],
            intro : translationService.translate('Search.Common.Help.Columns.Completed')
        });

        steps.push({
            element : columnDefs[8],
            intro : translationService.translate('Search.Common.Help.Columns.TimesProduced')
        });

        return steps;
    };

    var afterIntro = function (){
        if (showingDetailsBeforeHelp !== $scope.showDetails) $scope.toggleDetails();
    };

    introJsService.onbeforechange(function () {
        if (!$scope.showDetails) {
            $scope.toggleDetails();
        }
    });
   
    introJsService.onexit(afterIntro);
    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage, true);
}

function SearchBoxFirstConfirmationDialogCtrl($log, $modalInstance, $scope, messagingService, translationService, selectedOrders, cancelling) {
    selectedOrders.forEach(function (order) {
        order.message = translationService.translate('Search.Common.ReproduceDialog.Message.Body', order.CustomerUniqueId);
    });

    $scope.selectedProducibles = selectedOrders;
    $scope.cancelOrReproduceTitle = cancelling ? translationService.translate('Search.BoxFirst.CancelDialog.Label.Header') : translationService.translate('Search.BoxFirst.ReproduceDialog.Label.Header');
    $scope.cancelling = cancelling;

    $scope.closeBreakOrdersDialogWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.reproduce = function (result) {
        var producibles = Enumerable.From($scope.selectedProducibles).Select(function (selectedProducible) {
            return {
                Id: selectedProducible.Id,
                CustomerUniqueId: selectedProducible.CustomerUniqueId
            };
        }).ToArray();
        $log.info('Saving broken box request Group... ');
        //TFS 13115 - Bug 13115:3.0 Produced items imported by day recording negative value when reproducing and completed job:
        //I don't know why this plural message is in the system. Is it too hard to use one messsage and if
        //needed pass a collection to it? Last time I checked you could always pass a collection
        //with only one item in it or even make a loop in caller and deal individuallyt in callee. Take a stance.
        //I'm removing the janky way which jacks up metrics and just making a looping call to something that
        //I know works.
        //messagingService.publish("ReproduceCartons", producibles);
        Enumerable.From(producibles).ForEach(function (p) {
            messagingService.publish("ReproduceCarton", p);
        });
        $modalInstance.close();
    };
}

function DeleteBoxFirstConfirmationDialogCtrl($log, $modalInstance, $scope, messagingService, translationService, selectedOrders) {
    
    $scope.message = translationService.translate('Search.BoxFirst.RemoveDialog.Message.Body', selectedOrders.length);

    $scope.closeBreakOrdersDialogWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.remove = function () {
        var producibles = Enumerable.From(selectedOrders)
        .Where(function (p) {
            return p.ProducibleStatus != "ProducibleCompleted";
        })
        .Select(function (selectedProducible) {
            return {
                Id: selectedProducible.Id,
                CustomerUniqueId: selectedProducible.CustomerUniqueId
            };
        }).ToArray();
        if (producibles.length > 0) {
            messagingService.publish("RemoveBoxFirstCartons", producibles);
        }

        $modalInstance.close();
    };
}