'use strict';
function ScanToCreateSearchController($filter, $log, $modal, $scope, localStorageService, $timeout, alertService, introJsService, messagingService, producibleHelperService, packagingDesignService, searchConfigurationService, uiGridConstants, dateValidationService, translationService) {
    $timeout = $timeout.getInstance($scope);
    var awaitingResponse = true;
    $scope.showDetails = false;
    $scope.filtersEnabled = false;
    $scope.labelDetails = '';
    $scope.filteringLabel = '';
    $scope.disableReproduce = true;
    $scope.disableRemove = true;
    $scope.status = '';
    $scope.selectedItems = [];

    var date = new Date();
    date.setDate(date.getDate() - 1);
    var endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999);
    //setup initial criteria

    $scope.criteria = {
        Created: {
            $gte: date,
            $lt: endOfDay
        },
        CustomerUniqueId: ''
    };

    var enableRemove = function () {
        var allowedRemoveStatusList = ['ProducibleImported', 'ProducibleStaged', 'ProducibleFlaggedForReproduction', 'ProducibleSelected', 'NotProducible'];        
        
        delete $scope.disableRemove;
        $scope.disableRemove = $scope.gridApi.selection.getSelectedRows().length == 0 ||
            $scope.gridApi.selection.getSelectedRows().filter(function (item) { return allowedRemoveStatusList.indexOf(item.ProducibleStatus) > -1; }).length
            !== $scope.gridApi.selection.getSelectedRows().length;
    }

    $scope.data = {};
    $scope.producible = {};
    $scope.corrugate = {};
    $scope.distinctAndSortedCorrugateQualityOptions = [];
    $scope.machine = {};
    $scope.OpenStatusList = ['ProducibleImported', 'ProducibleStaged', 'ProducibleSentToMachineGroup', 'ProducibleFlaggedForReproduction', 'ProducibleSelected'];
    $scope.InProgressStatusList = ['ProducibleSentToMachine', 'ProducibleProductionStarted', 'LabelWaitingToBePeeledOff', 'AddedToMachineGroupQueue'];
    $scope.ClosedStatusList = ['ProducibleCompleted', 'ProducibleRemoved'];
    $scope.FailedStatusList = ['NotProducible', 'ProducibleFailed'];
    $scope.RemovedStatusList = [];

    $scope.toggleDetails = function () {
        $timeout(function () {
            $scope.showDetails = !$scope.showDetails;
        }, 0);
    }

    $scope.data = localStorageService.get("ScanToCreateSavedSettings");
    if ($scope.data != undefined) {
        $scope.producible.CustomerUniqueId = $scope.data.ArticleId;
        $scope.corrugate.Alias = $scope.data.Alias;
        $scope.corrugate.Quality = $scope.data.Quality;
        $scope.producible.DesignId = $scope.data.DesignId;
        $scope.producible.Height = $scope.data.Height;
        $scope.producible.Length = $scope.data.Length;
        $scope.producible.Width = $scope.data.Width;
        $scope.machine.ProducedOnMachineGroupId = $scope.data.ProducedOnMachineGroupId;
        $scope.class = "btn-success";
    }
    else {
        $scope.class = "btn-default";
    }

    $scope.$watch('showDetails', function (newValue) {
        
        delete $scope.labelDetails;
        $scope.labelDetails = newValue ? translationService.translate('Search.Common.Label.HideOptions') : translationService.translate('Search.Common.Label.ShowOptions');
    });

    $scope.selectableStatuses = [
    {
        text: translationService.translate('ProducibleStatus.Label.Generic.Open'),
        value: [{ 'ProducibleStatus': 'ProducibleImported' }, { 'ProducibleStatus': 'ProducibleStaged' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.InProgress'),
        value: [
            { 'ProducibleStatus': 'ProducibleSelected' }, { 'ProducibleStatus': 'ProducibleFlaggedForReproduction' },
            { 'ProducibleStatus': 'ProducibleProductionStarted' },
            { 'ProducibleStatus': 'ProducibleSentToMachineGroup' },
            { 'ProducibleStatus': 'ProducibleSentToMachine' }, { 'ProducibleStatus': 'AddedToMachineGroupQueue' }
        ]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Closed'),
        value: [{ 'ProducibleStatus': 'ProducibleCompleted' }, { 'ProducibleStatus': 'ProducibleRemoved' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Failed'),
        value: [{ 'ProducibleStatus': 'NotProducible' }, { 'ProducibleStatus': 'ProducibleFailed' }]
    }]; 
    
    $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    packagingDesignService.allDesignsObservable.autoSubscribe(function () {
        
        delete $scope.selectableDesigns;
        $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    }, $scope, 'Get Designs');

    messagingService.corrugatesObservable.autoSubscribe(function (msg) {
        
        delete $scope.allCorrugates;
        $scope.allCorrugates = msg.Data;
        
        delete $scope.distinctAndSortedCorrugateQualityOptions;
        $scope.distinctAndSortedCorrugateQualityOptions = Enumerable.From(msg.Data).Select(function (m) { return m.Quality; }).Distinct().OrderBy(function (i) { return i; }).ToArray();
    }, $scope, 'Scan to create Corrugates');

    messagingService.machineGroupsObservable.autoSubscribe(function (msg) {
        
        delete $scope.allMachineGroups;
        $scope.allMachineGroups = msg.Data;
    }, $scope, 'Scan to create MachineGroups');

    $scope.toggleFiltering = function () {
        $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
    };

    var addOrRemoveRow = function (row){
        if (row.isSelected) {
            if (row.entity.CustomerUniqueId == null) row.entity.CustomerUniqueId = row.entity.Producible.CustomerUniqueId;
            $scope.selectedItems.push(row.entity);
        }

        else {
            var i = $scope.selectedItems.indexOf(row.entity);
            $scope.selectedItems.splice(i, 1);
        }
    }

    var enableReproduce = function () {
        
        delete $scope.disableReproduce;
        $scope.disableReproduce = $scope.selectedItems.length == 0 || Enumerable.From($scope.selectedItems).Any(function (i) {
            return i.ProducibleStatus != "ProducibleCompleted" && i.ProducibleStatus != 'ProducibleFailed';
        });
    }

    var getItemsToReproduce = function () {
        var items = [];

        Enumerable.From($scope.selectedItems).ForEach(function (selectedRow) {
            if (selectedRow.ProducibleStatus == 'ProducibleCompleted' || selectedRow.ProducibleStatus == 'ProducibleFailed') {
                selectedRow.newTotal = selectedRow.uiReproduceQuantity;
                items.push(selectedRow);
            }
        });

        return items;
    };

    var nudTemplate = '<div ng-show="row.isSelected">' +
        '<numeric-up-down id="newMachinePortNUD" ng-model="row.entity.uiReproduceQuantity" default-value="1" min-value="1" max-value="65535" horizontal="true" on-changed="$parent.grid.api.enableReproduce()"></numeric-up-down>' +
        '</div>';

    $scope.gridOptions = {
        selectedItems:[],
        enableGridMenu: true,
        enableColumnResizing: true,
        expandableRowTemplate: "partials/SelectionAlgorithm/expandableProducibleTemplate.html",
        expandableRowHeight: 150,
        showGridFooter: true,
        // This template works as editable cell template because only one column is editable
        editableCellTemplate: nudTemplate,
        columnDefs: [
            {
                name: translationService.translate('Common.Label.Reproduce'),
                field: 'uiReproduceQuantity',
                type: 'number',
                enableFiltering: false,
                minWidth: 125,
                enableCellEdit: true,
                enableCellEditOnFocus: true,
                cellTemplate: nudTemplate
            },
            {
                name: translationService.translate('Common.Label.Barcode'),
                enableCellEdit: false,
                field: 'BarcodeData', width: 120
            },
            {
                displayName: translationService.translate('Article.Label.Id'),
                field: 'CustomerUniqueId',
                width: 320,
                enableCellEdit: false,
                sortingAlgorithm: function (a, b) {

                    if (isNaN(a) && isNaN(b)) {
                        return a === b ? 0 : (a < b ? -1 : 1);
                    }

                    if (!isNaN(a) && !isNaN(b)) {
                        var idA = parseFloat(a);
                        var idB = parseFloat(b);

                        return idA === idB ? 0 : (idA < idB ? -1 : 1);
                    }

                    return 0;
                }
            },
            {
                name: translationService.translate('Common.Label.Created'),
                field: 'Created',
                type: 'date',
                cellFilter: 'date:\'medium\'',
                enableFiltering: false,
                sortingAlgorithm: function (a, b) {
                    var timeA = typeof (a) === "string" ? new Date(a).getTime() : a.getTime(),
                        timeB = typeof (b) === "string" ? new Date(b).getTime() : b.getTime();

                    return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                },
                enableCellEdit: false,
                width: 200
            }, // having issues with this filter, better disable it
            {
                name: translationService.translate('Common.Label.Status'),
                field: 'ProducibleStatus',
                cellFilter: 'producibleStatus',
                enableCellEdit: false,
                width: 120
            },
            {
                name: translationService.translate('Common.Label.Type'),
                field: 'ProducibleType',
                cellFilter: 'producibleType',
                enableCellEdit: false,
                width: 150
            },
            {
                name: translationService.translate('Search.ScanToCreate.Label.ProducibleInfo'),
                field: 'ProducibleInfo',
                enableCellEdit: false,
                width: 150
            },
            {
                name: translationService.translate('Search.ScanToCreate.Label.OptimalCorrugate'),
                enableCellEdit: false,
                field: 'CorrugateInfo'
            },
            {
                name: translationService.translate('Common.Label.ProducedOn'),
                enableCellEdit: false,
                field: 'ProducedOn'
            },
            {
                name: translationService.translate('Search.Common.Label.TimesCompleted'),
                field: 'CompletedHistoryCount',
                enableCellEdit: false,
                type: 'number'
            }
        ],
        enableFiltering: false,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                addOrRemoveRow(row);
                enableReproduce();
                enableRemove();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (row) {
                addOrRemoveRow(row);
                enableReproduce();
                enableRemove();
            });
            //Builds sub-grid information when expanding the row
            gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {
                if (row.isExpanded && row.entity.Producible.ProducibleType === "Kit" && !row.entity.subGridOptions) {
                    row.entity.subGridOptions = {
                        enableColumnResizing: true,
                        enableRowSelection: true,
                        enableSelectAll: false,
                        multiSelect: true,
                        onRegisterApi: function (subGridApi){
                            subGridApi.selection.on.rowSelectionChanged($scope, function (row) {
                                addOrRemoveRow(row);
                                enableReproduce();
                                //enableRemove();
                            });
                            subGridApi.selection.on.rowSelectionChangedBatch($scope, function (row) {
                                addOrRemoveRow(row);
                                enableReproduce();
                                //enableRemove();
                            });
                        },
                        columnDefs: [
                            { name: translationService.translate('Common.Label.Reproduce'),
                                field: 'uiReproduceQuantity',
                                type: 'number',
                                enableColumnResizing: false,
                                enableFiltering: false,
                                shown: function () {//todo: not working currently should hide reproduce column if nothing has been selected.
                                    return $scope.gridApi.selection.getSelectedRows() > 0;
                                },
                                cellTemplate: '<div ng-show="row.isSelected">' +
                                '<numeric-up-down id="newMachinePortNUD" ng-model="row.entity.uiReproduceQuantity" default-value="1" min-value="1" max-value="65535" horizontal="true" on-changed="$parent.grid.api.enableReproduce()"></numeric-up-down>' +
                                '</div>'
                            },
                            { name: translationService.translate("Common.Label.Name"), field: "Producible.CustomerUniqueId" },
                            { name: translationService.translate("Common.Label.Description"), field: "Producible.CustomerDescription" },
                            { name: translationService.translate("Common.Label.Quantity"), field: "OriginalQuantity" },
                            { name: translationService.translate("Common.Label.Type"), field: "Producible.ProducibleType", cellFilter: "producibleType" },
                            { name: translationService.translate("Search.ScanToCreate.Label.ProducibleInfo"), field: "ProducibleInfo", width: 250 }
                        ],
                        data: row.entity.Producible.ItemsToProduce
                    };
                }
            });
        },
    };

    $scope.$watch('gridOptions.enableFiltering', function (newValue) {
        
        delete $scope.filteringLabel;
        $scope.filteringLabel = newValue ? translationService.translate('Common.Label.DisableResultsFilter') : translationService.translate('Common.Label.EnableResultsFilter');
    });

    var convertToLike = function (rawCriteria, field) {
        if (rawCriteria.indexOf(field) > -1) {
            var index = rawCriteria.indexOf(field) + field.length + 2;
            var firstPart = rawCriteria.substring(0, index);
            var secondPart = rawCriteria.substring(index, rawCriteria.length);
            secondPart = secondPart.replace('"', '/').replace('"', '/');
            if (secondPart.startsWith('//')) {
                index = firstPart.indexOf(field) - 2;
                firstPart = firstPart.substring(0, index);
                secondPart = secondPart.substring(2, secondPart.length);
            }
            return firstPart + secondPart;
        }
        else {
            return rawCriteria;
        }
    };
    var convertToISODate = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        secondPart = secondPart.replace('"', 'ISODate(\'').replace('"', '\')');

        return firstPart + secondPart;
    };

    var getNestedCriteria = function (prefix, obj) {
        // prefix can be a comma separated list of prefixes, when supplying multiple values they are enclosed inside an $or instruction

        var result = '';
        var prefixes = prefix.split(',');

        for (var key in obj) {
            if (obj.hasOwnProperty(key) && obj[key] != null && obj[key] !== '' && obj[key] !== 0) {
                if (result !== '')
                    result += ", ";

                if (prefixes.length > 1) {
                    result += '{ $or: [';
                }

                for (var i = 0; i < prefixes.length; i++) {
                    if (i > 0)
                        result += ', ';

                    result += '{"' + prefixes[i].trim() + '.' + key + '": ';

                    if (Enumerable.From(['ArticleId', 'Alias']).Contains(key)) {
                        result += '"' + obj[key] + '"';
                    }
                    else if (Enumerable.From(['ProducedOnMachineGroupId']).Contains(key)) {
                        result += 'CSUUID("' + obj[key] + '")';
                    }
                    else {
                        result += obj[key];
                    }

                    result += "}";
                }

                if (prefixes.length > 1) {
                    result += '] }';
                }
            }
        }

        return result;
    };

    var removeNull = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        if (secondPart.startsWith('null')) {
            index = firstPart.indexOf(field) - 2;
            firstPart = firstPart.substring(0, index);
            secondPart = secondPart.substring(4, secondPart.length);
        }
        return firstPart + secondPart;
    };

    $scope.save = function () {
        $scope.data = {
            ArticleId: $scope.producible.CustomerUniqueId,
            Alias: $scope.corrugate.Alias,
            Quality: $scope.corrugate.Quality,
            DesignId: $scope.producible.DesignId,
            Height: $scope.producible.Height,
            Width: $scope.producible.Width,
            Length: $scope.producible.Length,
            ProducedOnMachineGroupId: $scope.machine.ProducedOnMachineGroupId
        };

        localStorageService.set("ScanToCreateSavedSettings", $scope.data);
        alertService.addSuccess(translationService.translate('Common.Message.SavedSearch'));
        $scope.class = "btn-success";
    };

    $scope.reset = function () {
        localStorageService.remove("ScanToCreateSavedSettings");
        $scope.producible.CustomerUniqueId = "";
        $scope.corrugate.Alias = "";
        $scope.corrugate.Quality = "";
        $scope.machine._id = "";
        $scope.producible.DesignId = "";
        $scope.producible.Height = "";
        $scope.producible.Width = "";
        $scope.producible.Length = "";
        $scope.machine.ProducedOnMachineGroupId = "";
        $scope.class = "btn-default";
    };

    $scope.search = function (){
        $scope.gridApi.selection.clearSelectedRows();
        enableReproduce();
        enableRemove();
        
        delete $scope.isDateRangeInvalid;
        $scope.isDateRangeInvalid = !dateValidationService.validateSearchDates($scope.criteria.Created.$gte, $scope.criteria.Created.$lt);
        if ($scope.isDateRangeInvalid) {
            return;
        }
        
        delete $scope.status;
        $scope.status = translationService.translate('Search.Common.Message.Searching');
        
        $scope.criteria.CustomerUniqueId = $scope.criteria.CustomerUniqueId.replace(/"/g, '');
        if ($scope.criteria.BarcodeData) {
            
            $scope.criteria.BarcodeData = $scope.criteria.BarcodeData.replace(/"/g, '');
        }
        else {
            
            delete $scope.criteria.BarcodeData;
            $scope.criteria.BarcodeData = '';
        }
        var criteria = JSON.stringify($scope.criteria);
        criteria = convertToLike(criteria, "CustomerUniqueId");
        criteria = convertToISODate(criteria, '$gte');
        criteria = convertToISODate(criteria, '$lt');
        criteria = convertToLike(criteria, "BarcodeData");
        criteria = removeNull(criteria, '$or');
        criteria = removeNull(criteria, 'ProducedOnMachineGroupId');

        var additionalCriteria = '';
        var nestedCriteria = getNestedCriteria('Producible, Producible.ItemsToProduce.Producible', $scope.producible);

        if (nestedCriteria !== '')
            additionalCriteria += nestedCriteria;

        nestedCriteria = getNestedCriteria('Producible.CartonOnCorrugate.Corrugate, Producible.ItemsToProduce.Producible.CartonOnCorrugate.Corrugate', $scope.corrugate);

        if (nestedCriteria !== '')
            additionalCriteria += (additionalCriteria !== '' ? ', ' : '') + nestedCriteria;

        nestedCriteria = getNestedCriteria('Producible, Producible.ItemsToProduce.Producible', $scope.machine);

        if (nestedCriteria !== '')
            additionalCriteria += (additionalCriteria !== '' ? ', ' : '') + nestedCriteria;

        if (additionalCriteria !== '') {
            var position = criteria.length - 1;

            criteria = [criteria.slice(0, position), ', $and: [' + additionalCriteria + ']', criteria.slice(position)].join('');
        }

        $scope.filtersEnabled = (additionalCriteria !== '');
        
        messagingService.publish("SearchForProducibles", { SelectionAlgorithmTypes: "ScanToCreate", Criteria: criteria });
    }

    $scope.remove = function () {

        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SelectionAlgorithm/Scan To Create/deleteConfirmationDialog.html',
            controller: 'DeleteScanToCreateConfirmationDialogCtrl', //defined end of this file
            resolve: {
                selectedOrders: function () {
                    return $scope.gridApi.selection.getSelectedRows();
                }
            }
        };

        $modal.open(opts).result.then(function () {
            $timeout(function () {
                $scope.gridApi.selection.clearSelectedRows();
                enableRemove();
                $scope.search();
            }, 1000);
        });
    };

    $scope.reproduceCancel = function (which) {

        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SelectionAlgorithm/Orders/searchOrderConfirmationDialog.html',
            controller: 'SearchOrderConfirmationDialogCtrl', //defined end of this file
            resolve: {
                selectedOrders: function () {
                    return getItemsToReproduce();
                },
                cancelling: which,
                grid: $scope.gridApi
            }
        };

        $modal.open(opts).result.then(function () {
            $timeout(function () { $scope.search(); }, 1000);
        });
    };

    messagingService.searchScanToCreateResponseObservable
       .autoSubscribe(function (msg) {
           $timeout(function () {
               
               delete $scope.status;
               $scope.status = translationService.translate('Search.Common.Message.ResultFound');
               if ($scope.debug) {
                   $log.info([moment().format("HH:mm:ss.SSS"), msg.Data]);
               }
               awaitingResponse = false;
               Enumerable.From(msg.Data).ForEach(function (producible) {
                   producible.CompletedHistoryCount = Enumerable.From(producible.History).Where(function (h) { return h.NewStatus === "ProducibleCompleted"; }).Count();
                   producible.ProducibleType = producible.Producible.ProducibleType;
                   producible.ProducedOn = Enumerable.From($scope.allMachineGroups).Where(function (mg) { return mg.Id == producible.ProducedOnMachineGroupId; }).Select(function (m) { return m.Alias; }).ToArray().toString();
                   producible.uiReproduceQuantity = 0;
                   producible.CorrugateInfo = producibleHelperService.getOptimalCorrugateDisplayValue(producible);
                   var carton = producibleHelperService.findFirstCarton(producible);
                   if (carton && carton.ArticleId) producible.ArticleId = carton.ArticleId;

                   if (producible.ProducibleType === 'Carton') {
                       //Faking the structure used on Box First and Box Last Search to display the details of a Carton
                       producible.ProducibleInfo = producibleHelperService.getDiplayValue(producible);
                       producible.Producible.ItemsToProduce = [];

                       producible.Producible.ItemsToProduce.push({
                           ProducibleType: producible.Producible.ProducibleType,
                           Header: $filter('producibleType')(producible.Producible.ProducibleType) + ' - ' + $filter('producibleStatus')(producible.Producible.ProducibleStatus),
                           Line1: translationService.translate('Common.Label.LengthAcronym') + ': ' + producible.Producible.Length + ' ' +
                               translationService.translate('Common.Label.WidthAcronym') + ': ' + producible.Producible.Width + ' ' +
                               translationService.translate('Common.Label.HeightAcronym') + ': ' + producible.Producible.Height,
                           Line2: translationService.translate('Common.Label.Design') + ': ' +
                               Enumerable.From($scope.selectableDesigns)
                               .Where(function (pg) { return pg.Id === producible.Producible.DesignId; })
                               .Select(function (c) { return c.Name; })
                               .ToArray().toString()
                       });

                       var optimalCorrugate = producibleHelperService.findFirstOptimalCorrugate(producible);

                       if (optimalCorrugate) {
                           producible.Producible.ItemsToProduce.push({
                               ProducibleType: 'Corrugate',
                               Header: translationService.translate('Search.Common.Label.ProducedOnCorrugate'),
                               Line1: translationService.translate('Common.Label.Name') + ': ' + optimalCorrugate.Alias,
                               Line2: translationService.translate('Common.Label.WidthAcronym') + ': ' + optimalCorrugate.Width + ' - ' +
                                   translationService.translate('Common.Label.ThicknessAcronym') + ': ' + optimalCorrugate.Thickness + ' - ' +
                                   translationService.translate('Common.Label.QualityAcronym') + ': ' + optimalCorrugate.Quality
                           });
                       }
                   }

                   if (producible.ProducibleType === 'Kit') {
                       Enumerable.From(producible.Producible.ItemsToProduce).ForEach(function (item) {
                           item.ProducibleInfo = producibleHelperService.getDiplayValue(item);
                       });
                   }
               });

               
               delete $scope.additionalDetails;
               $scope.additionalDetails = Enumerable.From(msg.Data).Count() >= searchConfigurationService.getMaxScanToCreateSearchResults() ? translationService.translate('Common.Message.ShowingFirstResults', searchConfigurationService.getMaxScanToCreateSearchResults()) : '';

               
               delete $scope.gridOptions.data;
               $scope.gridOptions.data = msg.Data;
               
               delete $scope.status;
               $scope.status = '';
           }, 0, true);
       }, $scope, 'ScanToCreateSearch');

    var requestData = function (){
        messagingService.publish("GetMachineGroups");
        messagingService.publish("GetCorrugates");
    };

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    var showingDetailsBeforeHelp = $scope.showDetails;
    $scope.tellMeAboutThisPage = function (){
        showingDetailsBeforeHelp = $scope.showDetails;
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        steps.push({
            element : '#status',
            intro : translationService.translate('Search.Common.Help.Status')
        });

        steps.push({
            element : "#customerUniqueId",
            intro : translationService.translate('Search.Common.Help.ArticleId')
        });

        steps.push({
            element : "#barcode",
            intro : translationService.translate('Search.ScanToCreate.Help.Barcode')
        });

        steps.push({
            element : "#beginDateWrapper",
            intro : translationService.translate('Search.Common.Help.DateBegin')
        });

        steps.push({
            element : "#endDateWrapper",
            intro : translationService.translate('Search.Common.Help.DateEnd')
        });
        steps.push({
            element : "#designId",
            intro : translationService.translate('Search.Common.Help.Design')
        });

        steps.push({
            element : "#length",
            intro : translationService.translate('Search.Common.Help.CartonLength')
        });

        steps.push({
            element : "#width",
            intro : translationService.translate('Search.Common.Help.CartonWidth')
        });

        steps.push({
            element : "#height",
            intro : translationService.translate('Search.Common.Help.CartonHeight')
        });

        steps.push({
            element : "#corrugateAlias",
            intro : translationService.translate('Search.Common.Help.Corrugate')
        });

        steps.push({
            element : "#corrugateQualityWrapper",
            intro : translationService.translate('Search.Common.Help.CorrugateQuality')
        });

        steps.push({
            element : "#machineGroup",
            intro : translationService.translate('Search.Common.Help.MachineGroup')
        });

        steps.push({
            element : "#toggleDetails",
            intro : translationService.translate('Search.Common.Help.AdditionalOptions')
        });

        steps.push({
            element : "#searchButton",
            intro : translationService.translate('Search.Common.Help.Search')
        });

        steps.push({
            element : "#reproduceButton",
            intro : translationService.translate('Search.Common.Help.Reproduce')
        });

        steps.push({
            element : "#removeButton",
            intro : translationService.translate('Search.Common.Help.Remove')
        });

        steps.push({
            element : "#toggleFiltering",
            intro : translationService.translate('Search.Common.Help.ResultsFilter')
        });

        steps.push({
            element : "#resultGrid",
            intro : translationService.translate('Search.Common.Help.Columns.Overview')
        });

        steps.push({
            element: columnDefs[2],
            intro : translationService.translate('Search.Common.Help.Columns.Reproduce')
        });

        steps.push({
            element : columnDefs[3],
            intro : translationService.translate('Search.ScanToCreate.Help.Columns.Barcode')
        });

        steps.push({
            element : columnDefs[4],
            intro : translationService.translate('Search.Common.Help.Columns.ArticleId')
        });

        steps.push({
            element : columnDefs[5],
            intro : translationService.translate('Search.Common.Help.Columns.CreatedDate')
        });

        steps.push({
            element : columnDefs[6],
            intro : translationService.translate('Search.Common.Help.Columns.Status')
        });

        steps.push({
            element : columnDefs[7],
            intro : translationService.translate('Search.Common.Help.Columns.Type')
        });

        steps.push({
            element : columnDefs[8],
            intro : translationService.translate('Search.Common.Help.Columns.ProducibleInfo')
        });

        steps.push({
            element : columnDefs[9],
            intro : translationService.translate('Search.Common.Help.Columns.Corrugate')
        });

        steps.push({
            element : columnDefs[10],
            intro : translationService.translate('Search.Common.Help.Columns.ProducedOn')
        });

        steps.push({
            element: columnDefs[11],
            intro: translationService.translate('Search.Common.Help.Columns.TimesProduced')
        });

        return steps;
    };

    var afterIntro = function (){
        if (showingDetailsBeforeHelp !== $scope.showDetails) $scope.toggleDetails();
    };

    introJsService.onbeforechange(function (targetElement) {
        if (!$scope.showDetails) {
            $scope.toggleDetails();
        }
    });

    introJsService.onexit(afterIntro);
    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage, true);
};

function DeleteScanToCreateConfirmationDialogCtrl($log, $modalInstance, $scope, messagingService, translationService, selectedOrders) {

    $scope.message = translationService.translate('Search.ScanToCreate.RemoveDialog.Message.Body', selectedOrders.length);

    $scope.close = function () {
        $modalInstance.dismiss();
    };

    $scope.delete = function (){
        $log.info('Sending Remove Cartons message');
        messagingService.publish("RemoveScanToCreateCartons", selectedOrders);
        $modalInstance.close();
    };
}

function SearchScanToCreateConfirmationDialogCtrl($log, $modalInstance, $scope, messagingService, translationService, selectedOrders, cancelling) {
    selectedOrders.forEach(function (order) {
        order.message = translationService.translate('Search.Common.ReproduceDialog.Message.Body', order.CustomerUniqueId);
    });

    $scope.selectedProducibles = selectedOrders;
    $scope.cancelOrReproduceTitle = cancelling ? translationService.translate('Search.ScanToCreate.CancelDialog.Label.Header') : translationService.translate('Search.ScanToCreate.ReproduceDialog.Label.Header');
    
    $scope.cancelling = cancelling;

    $scope.closeBreakOrdersDialogWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.reproduce = function (result) {
        var producibles = Enumerable.From($scope.selectedProducibles).Select(function (selectedProducible) {
            return {
                Id: selectedProducible.Id,
                CustomerUniqueId: selectedProducible.CustomerUniqueId
            };
        }).ToArray();

        $log.info('Saving broken box request Group... ');
        messagingService.publish("ReproduceCartons", producibles);
        $modalInstance.close();
    };
}