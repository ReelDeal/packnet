function BoxLastSearchController($filter, $log, $modal, $scope, localStorageService, $timeout, alertService, introJsService, messagingService, packagingDesignService, searchConfigurationService, uiGridConstants, dateValidationService, translationService) {
    $timeout = $timeout.getInstance($scope);
    var awaitingResponse = true;
    $scope.showDetails = false;
    $scope.filtersEnabled = false;
    $scope.labelDetails = '';
    $scope.filteringLabel = '';
    $scope.disableReproduce = true;
    $scope.status = '';

    var date = new Date();
    date.setDate(date.getDate() - 1);
    var endOfDay = new Date();
    endOfDay.setHours(23, 59, 59, 999);
    //setup initial criteria
    $scope.criteria = {
        Created: {
            $gte: date,
            $lt: endOfDay
        },
        CustomerUniqueId: ''
    };

    $scope.data = {};
    $scope.producible = {};
    $scope.corrugate = {};
    //Added to fix TFS 9460
    $scope.distinctAndSortedCorrugateQualityOptions = [];
    $scope.machine = {};
    $scope.OpenStatusList = ['ProducibleImported', 'ProducibleStaged', 'ProducibleSentToMachineGroup', 'ProducibleFlaggedForReproduction', 'ProducibleSelected'];
    $scope.InProgressStatusList = ['ProducibleSentToMachine', 'ProducibleProductionStarted', 'LabelWaitingToBePeeledOff', 'AddedToMachineGroupQueue'];
    $scope.ClosedStatusList = ['ProducibleCompleted', 'ProducibleRemoved'];
    $scope.FailedStatusList = ['NotProducible', 'ProducibleFailed'];
    $scope.RemovedStatusList = [];
    
    $scope.toggleDetails = function () {
        $timeout(function () {
            $scope.showDetails = !$scope.showDetails;
        }, 0);
    }

    $scope.data = localStorageService.get("BoxLastSavedSettings");
    if ($scope.data != undefined) {
        $scope.producible.CustomerUniqueId = $scope.data.ArticleId;
        $scope.corrugate.Alias = $scope.data.Alias;
        $scope.corrugate.Quality = $scope.data.Quality;
        $scope.producible.DesignId = $scope.data.DesignId;
        $scope.producible.Height = $scope.data.Height;
        $scope.producible.Length = $scope.data.Length;
        $scope.producible.Width = $scope.data.Width;
        $scope.machine.ProducedOnMachineGroupId = $scope.data.ProducedOnMachineGroupId;
        $scope.class = "btn-success";
    }
    else {
        $scope.class = "btn-default";
    }

    $scope.$watch('showDetails', function (newValue) {
        
        delete $scope.labelDetails;
        $scope.labelDetails = newValue ? translationService.translate('Search.Common.Label.HideOptions') : translationService.translate('Search.Common.Label.ShowOptions');
    });

    $scope.selectableStatuses = [
    {
        text: translationService.translate('ProducibleStatus.Label.Generic.Open'),
        value: [{ 'ProducibleStatus': 'ProducibleImported' }, { 'ProducibleStatus': 'ProducibleStaged' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.InProgress'),
        value: [
            { 'ProducibleStatus': 'ProducibleSelected' }, { 'ProducibleStatus': 'ProducibleFlaggedForReproduction' },
            { 'ProducibleStatus': 'ProducibleProductionStarted' },
            { 'ProducibleStatus': 'ProducibleSentToMachineGroup' },
            { 'ProducibleStatus': 'ProducibleSentToMachine' }, { 'ProducibleStatus': 'AddedToMachineGroupQueue' }
        ]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Closed'),
        value: [{ 'ProducibleStatus': 'ProducibleCompleted' }, { 'ProducibleStatus': 'ProducibleRemoved' }]
    }, {
        text: translationService.translate('ProducibleStatus.Label.Generic.Failed'),
        value: [{ 'ProducibleStatus': 'NotProducible' }, { 'ProducibleStatus': 'ProducibleFailed' }]
    }]; 

    $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    packagingDesignService.allDesignsObservable.autoSubscribe(function () {
        
        delete $scope.selectableDesigns;
        $scope.selectableDesigns = packagingDesignService.getAllDesigns();
    }, $scope, 'Get Designs');

    messagingService.corrugatesObservable.autoSubscribe(function (msg) {
        //I really shouldn't have to be saying this, but sort the list for display
        
        delete $scope.allCorrugates;
        $scope.allCorrugates = Enumerable.From(msg.Data).OrderBy(function (m) { return m.Alias; }).ToArray();
        //Added to fix TFS 9460
        
        delete $scope.distinctAndSortedCorrugateQualityOptions;
        $scope.distinctAndSortedCorrugateQualityOptions = Enumerable.From(msg.Data).Select(function (m) { return m.Quality; }).Distinct().OrderBy(function (i) { return i; }).ToArray();
    }, $scope, 'Corrugates');

    messagingService.machineGroupsObservable.autoSubscribe(function (msg) {
        
        delete $scope.allMachineGroups;
        $scope.allMachineGroups = msg.Data;
    }, $scope, 'MachineGroups');

    $scope.toggleFiltering = function () {
        $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
    };

    var enableReproduce = function () {
        $scope.disableReproduce = getItemsToReproduce().length == 0;
    }
    var getItemsToReproduce = function () {
        var items = [];
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (selectedRow) {
            if (selectedRow.ProducibleStatus == 'ProducibleCompleted') {
                items.push(selectedRow);
            }
        });

        return items;
    };

    $scope.gridOptions = {
        enableGridMenu: true,
        enableColumnResizing: true,
        expandableRowTemplate: 'partials/SelectionAlgorithm/expandableKitsTemplate.html',
        expandableRowHeight: 150,
        showGridFooter: true,
        columnDefs: [
            {
                displayName: translationService.translate('Common.Label.CustomerId'),
                field: 'CustomerUniqueId',
                sortingAlgorithm: function (a, b) {

                    if (isNaN(a) && isNaN(b)) {
                        return a === b ? 0 : (a < b ? -1 : 1);
                    }

                    if (!isNaN(a) && !isNaN(b)) {
                        var idA = parseFloat(a);
                        var idB = parseFloat(b);

                        return idA === idB ? 0 : (idA < idB ? -1 : 1);
                    }

                    return 0;
                }
            },
            {
                name: translationService.translate('Common.Label.Created'),
                field: 'Created',
                type: 'date',
                cellFilter: 'date:\'medium\'',
                enableFiltering: false,
                sortingAlgorithm: function (a, b) {
                    var timeA = typeof (a) === "string" ? new Date(a).getTime() : a.getTime(),
                        timeB = typeof (b) === "string" ? new Date(b).getTime() : b.getTime();

                    return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                }
            }, // having issues with this filter, better disable it
            {
                name: translationService.translate('Common.Label.Status'),
                field: 'ProducibleStatus',
                cellFilter: 'producibleStatus'
            },
            {
                name: translationService.translate('Common.Label.Type'),
                field: 'ProducibleType',
                cellFilter: 'producibleType'
            },
            {
                name: translationService.translate('Common.Label.ProducedOn'),
                field: 'ProducedOn'
            },
            {
                name: translationService.translate('ProductionGroup.Label.NameSingular'),
                field: 'ProductionGroupAlias'
            },
            {
                name: translationService.translate('Common.Label.Triggered'),
                field: 'TimeTriggered',
                type: 'date',
                cellFilter: 'date:\'medium\'',
                enableFiltering: false,
                sortingAlgorithm: function (a, b) {
                    var timeA = typeof (a) === "string" ? new Date(a).getTime() : a.getTime(),
                        timeB = typeof (b) === "string" ? new Date(b).getTime() : b.getTime();

                    return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                }
            },
            {
                name: translationService.translate('Search.Common.Label.TimesCompleted'),
                field: 'CompletedHistoryCount',
                type: 'number'
            }
        ],
        enableFiltering: false,
        enableRowSelection: true,
        enableSelectAll: true,
        selectionRowHeaderWidth: 35,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;
            gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                enableReproduce();
            });
            gridApi.selection.on.rowSelectionChangedBatch($scope, function (row) {
                enableReproduce();
            });
        },
    };

    $scope.$watch('gridOptions.enableFiltering', function (newValue) {
        
        delete $scope.filteringLabel;
        $scope.filteringLabel = newValue ? translationService.translate('Common.Label.DisableResultsFilter') : translationService.translate('Common.Label.EnableResultsFilter');
    });

    var convertToLike = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        secondPart = secondPart.replace('"', '/').replace('"', '/');
        if (secondPart.startsWith('//')) {
            index = firstPart.indexOf(field) - 2;
            firstPart = firstPart.substring(0, index);
            secondPart = secondPart.substring(2, secondPart.length);
        }
        return firstPart + secondPart;
    };
    var convertToISODate = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        secondPart = secondPart.replace('"', 'ISODate(\'').replace('"', '\')');

        return firstPart + secondPart;
    };

    var getNestedCriteria = function (base, obj) {
        var result = '';
        for (var key in obj) {
            if (obj[key] != null && obj[key] != '' && obj[key] != 0) {
                result += ',"' + base + '.' + key + '":';
                if (Enumerable.From(['ArticleId', 'Alias']).Contains(key)) {
                    result += '"' + obj[key] + '"';
                }
                else if (Enumerable.From(['ProducedOnMachineGroupId']).Contains(key)) {
                    result += 'CSUUID("' + obj[key] + '")';
                }
                else {
                    result += obj[key];
                }
            }
        }

        return result;
    };

    var removeNull = function (rawCriteria, field) {
        var index = rawCriteria.indexOf(field) + field.length + 2;
        var firstPart = rawCriteria.substring(0, index);
        var secondPart = rawCriteria.substring(index, rawCriteria.length);
        if (secondPart.startsWith('null')) {
            index = firstPart.indexOf(field) - 2;
            firstPart = firstPart.substring(0, index);
            secondPart = secondPart.substring(4, secondPart.length);
        }
        return firstPart + secondPart;
    };

    $scope.save = function () {
        $scope.data = {
            ArticleId: $scope.producible.CustomerUniqueId,
            Alias: $scope.corrugate.Alias,
            Quality: $scope.corrugate.Quality,
            DesignId: $scope.producible.DesignId,
            Height: $scope.producible.Height,
            Width: $scope.producible.Width,
            Length: $scope.producible.Length,
            ProducedOnMachineGroupId: $scope.machine.ProducedOnMachineGroupId
        };

        localStorageService.set("BoxLastSavedSettings", $scope.data);
        alertService.addSuccess(translationService.translate('Common.Message.SavedSearch'));
        $scope.class = "btn-success";
    };

    $scope.reset = function () {
        localStorageService.remove("BoxLastSavedSettings");
        $scope.producible.CustomerUniqueId = "";
        $scope.corrugate.Alias = "";
        $scope.corrugate.Quality = "";
        $scope.machine._id = "";
        $scope.producible.DesignId = "";
        $scope.producible.Height = "";
        $scope.producible.Width = "";
        $scope.producible.Length = "";
        $scope.machine.ProducedOnMachineGroupId = "";
        $scope.class = "btn-default";
    };

    $scope.search = function () {
        $scope.gridApi.selection.clearSelectedRows();

        $scope.isDateRangeInvalid = !dateValidationService.validateSearchDates($scope.criteria.Created.$gte, $scope.criteria.Created.$lt);
        if ($scope.isDateRangeInvalid) {
            return;
        }
        
        delete $scope.status;
        $scope.status = translationService.translate('Search.Common.Message.Searching');
        
        $scope.criteria.CustomerUniqueId = $scope.criteria.CustomerUniqueId.replace(/"/g, '');
        var criteria = JSON.stringify($scope.criteria);
        criteria = convertToLike(criteria, "CustomerUniqueId");
        criteria = convertToISODate(criteria, '$gte');
        criteria = convertToISODate(criteria, '$lt');
        criteria = removeNull(criteria, '$or');
        criteria = removeNull(criteria, 'ProducedOnMachineGroupId');

        //Begin Fixes for TFS 9460 - BoxFirst (and last) Search Page: In additional search options, when corrugate quality is equal to 0, the search result list is wrong.
        /*
        1.We were providing a bunch of options arbitrarily with numeric up-down which is not right. We need to get a list of the unique corrugate qualities
        in the system and provide those in a numerically sorted list with All at the top of it. This is accomplished using $scope.distinctAndSortedCorrugateQualityOptions.
        2.Changed numeric up/down input to a select (dropdown) because we didn't correctly deal with the nullable integer. Added an "All" option.
        3.Used a primitive array of numbers to bind to the select element while keeping id for automated testing.
        4.Search behavior jacked up: 0 does not mean "All." 0 means use 0 in the search. All means don't apply the filter. See above items about nullable int.
        5.We didn't use the right string filter for criteria and corrugate quality or it got changed out at some point so mongo query was wrong. Added a check for
        a useable numeric value coming in and fixed the filter string.
        */
        var nestedCriteria = getNestedCriteria('Producible.ItemsToProduce', $scope.producible);

        if ($scope.corrugate.Quality !== null && !isNaN($scope.corrugate.Quality))
        {
            //The getNestedCriteria does not work well with primitive arrays so just concatenate it in here and clean up below with the slicing
            nestedCriteria += ',"Producible.ItemsToProduce.CorrugateQuality":' + $scope.corrugate.Quality;
        }
        
        //Check if the alias is present and assemble search string for it
        if ($scope.corrugate.Alias !== null && typeof ($scope.corrugate.Alias) !== "undefined") {
            //The getNestedCriteria does not work well with primitive arrays so just concatenate it in here and clean up below with the slicing
            nestedCriteria += ',"Producible.ItemsToProduce.CartonOnCorrugate.Corrugate.Alias":' + "\"" + $scope.corrugate.Alias + "\"";
        }

        nestedCriteria += getNestedCriteria('Producible', $scope.machine);
        //End Fixes for TFS 9460

        $scope.filtersEnabled = (nestedCriteria != "");

        var position = criteria.length - 1;
        criteria = [criteria.slice(0, position), nestedCriteria, criteria.slice(position)].join('');
        messagingService.publish("SearchForProducibles", { SelectionAlgorithmTypes: "BoxLast", Criteria: criteria });
    }

    $scope.reproduceCancel = function (which) {

        var opts = {
            backdrop: 'static',
            keyboard: false,
            templateUrl: 'partials/SelectionAlgorithm/Box Last/searchConfirmationDialog.html',
            controller: 'SearchBoxLastConfirmationDialogCtrl', //defined end of this file
            resolve: {
                selectedOrders: function () {
                    return getItemsToReproduce();
                },
                cancelling: which
            }
        };

        $modal.open(opts).result.then(function () {
            $timeout(function () { $scope.search(); }, 1000);
        });
    };


    messagingService.searchBoxLastResponseObservable
       .autoSubscribe(function (msg) {
           $timeout(function () {
               
               delete $scope.status;
               $scope.status = translationService.translate('Search.Common.Message.ResultFound');
               if ($scope.debug) {
                   $log.info([moment().format("HH:mm:ss.SSS"), msg.Data]);
               }
               awaitingResponse = false;
               Enumerable.From(msg.Data).ForEach(function (producible) {
                   producible.CompletedHistoryCount = Enumerable.From(producible.History).Where(function (h) { return h.NewStatus === "ProducibleCompleted"; }).Count();
                   producible.ProducibleType = producible.Producible.ProducibleType;
                   producible.ProducedOn = Enumerable.From($scope.allMachineGroups).Where(function (mg) { return mg.Id == producible.ProducedOnMachineGroupId; }).Select(function (m) { return m.Alias; }).ToArray().toString();
                   producible.uiReproduceQuantity = 0;

                   if (producible.Producible.ItemsToProduce === undefined) {
                       producible.Producible.ItemsToProduce = [];
                       producible.Producible.ItemsToProduce.push(angular.copy(producible.Producible));
                   }

                       Enumerable.From(producible.Producible.ItemsToProduce).ForEach(function (childProducible) {
                           if (childProducible.ProducibleType == 'Carton') {
                               childProducible.Header = $filter('producibleType')(childProducible.ProducibleType) + ' - ' + $filter('producibleStatus')(childProducible.ProducibleStatus);
                               childProducible.Line1 = translationService.translate('Common.Label.LengthAcronym') + ': ' + childProducible.Length + ' ' +
                                   translationService.translate('Common.Label.WidthAcronym') + ': ' + childProducible.Width + ' ' +
                                   translationService.translate('Common.Label.HeightAcronym') + ': ' + childProducible.Height;
                               childProducible.Line2 = translationService.translate('Common.Label.Design') + ': ' +
                                   Enumerable.From($scope.selectableDesigns)
                                   .Where(function (pg) { return pg.Id === childProducible.DesignId; })
                                   .Select(function (c) { return c.Name; })
                                   .ToArray().toString();

                               if (childProducible.CartonOnCorrugate != null) {
                                   producible.Producible.ItemsToProduce.push({
                                       ProducibleType : 'Corrugate',
                                       Header : translationService.translate('Search.Common.Label.ProducedOnCorrugate'),
                                       Line1 : translationService.translate('Common.Label.Name') + ': ' + childProducible.CartonOnCorrugate.Corrugate.Alias,
                                       Line2 : translationService.translate('Common.Label.WidthAcronym') + ': ' + childProducible.CartonOnCorrugate.Corrugate.Width + ' - ' +
                                           translationService.translate('Common.Label.ThicknessAcronym') + ': ' + childProducible.CartonOnCorrugate.Corrugate.Thickness + ' - ' +
                                           translationService.translate('Common.Label.QualityAcronym') + ': ' + childProducible.CartonOnCorrugate.Corrugate.Quality
                                   });
                               }
                           }
                           if (childProducible.ProducibleType == 'Label') {
                               childProducible.Header = $filter('producibleType')(childProducible.ProducibleType) + ' - ' + $filter('producibleStatus')(childProducible.ProducibleStatus);

                               Enumerable.From(childProducible.Restrictions).ForEach(function (restriction) {
                                   if (typeof (restriction.Value) == 'string') {
                                       childProducible.Line1 = translationService.translate('Common.Label.Priority') + ': ' + restriction.Value;
                                   }
                                   else if (restriction.Value.Name != null) {
                                       childProducible.Line2 = translationService.translate('Common.Label.Template') + ': ' + restriction.Value.Name;
                                   }
                               });
                           }
                       });
               });

               
               delete $scope.additionalDetails;
               $scope.additionalDetails = Enumerable.From(msg.Data).Count() >= searchConfigurationService.getMaxBoxLastSearchResults() ? translationService.translate('Common.Message.ShowingFirstResults', searchConfigurationService.getMaxBoxLastSearchResults()) : '';

               
               delete $scope.gridOptions.data;
               $scope.gridOptions.data = msg.Data;
               
               delete $scope.status;
               $scope.status = '';
           }, 0, true);
       }, $scope, 'BoxLastSearch');

    var requestData = function () {
        messagingService.publish("GetMachineGroups");
        messagingService.publish("GetCorrugates");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    var showingDetailsBeforeHelp = $scope.showDetails;
    $scope.tellMeAboutThisPage = function () {
        showingDetailsBeforeHelp = $scope.showDetails;
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        steps.push({
            element: '#status',
            intro: translationService.translate('Search.Common.Help.Status')
        });

        steps.push({
            element: "#customerUniqueId",
            intro: translationService.translate('Search.Common.Help.CustomerId')
        });

        steps.push({
            element: "#beginDateWrapper",
            intro: translationService.translate('Search.Common.Help.DateBegin')
        });

        steps.push({
            element: "#endDateWrapper",
            intro: translationService.translate('Search.Common.Help.DateEnd')
        });

        steps.push({
            element: "#designId",
            intro: translationService.translate('Search.Common.Help.Design')
        });

        steps.push({
            element: "#length",
            intro: translationService.translate('Search.Common.Help.CartonLength')
        });

        steps.push({
            element: "#width",
            intro: translationService.translate('Search.Common.Help.CartonWidth')
        });

        steps.push({
            element: "#height",
            intro: translationService.translate('Search.Common.Help.CartonHeight')
        });

        steps.push({
            element: "#corrugateAlias",
            intro: translationService.translate('Search.Common.Help.Corrugate')
        });

        steps.push({
            element: "#corrugateQualityWrapper",
            intro: translationService.translate('Search.Common.Help.CorrugateQuality')
        });

        steps.push({
            element: "#machineGroup",
            intro: translationService.translate('Search.Common.Help.MachineGroup')
        });

        steps.push({
            element: "#toggleDetails",
            intro: translationService.translate('Search.Common.Help.AdditionalOptions')
        });

        steps.push({
            element: "#searchButton",
            intro: translationService.translate('Search.Common.Help.Search')
        });

        steps.push({
            element: "#reproduceButton",
            intro: translationService.translate('Search.Common.Help.Reproduce')
        });

        steps.push({
            element: "#toggleFiltering",
            intro: translationService.translate('Search.Common.Help.ResultsFilter')
        });

        steps.push({
            element: "#resultGrid",
            intro: translationService.translate('Search.Common.Help.Columns.Overview')
        });

        steps.push({
            element: columnDefs[2],
            intro: translationService.translate('Search.Common.Help.Columns.CustomerUniqueId')
        });

        steps.push({
            element: columnDefs[3],
            intro: translationService.translate('Search.Common.Help.Columns.CreatedDate')
        });

        steps.push({
            element: columnDefs[4],
            intro: translationService.translate('Search.Common.Help.Columns.Status')
        });

        steps.push({
            element: columnDefs[5],
            intro: translationService.translate('Search.Common.Help.Columns.Type')
        });

        steps.push({
            element: columnDefs[6],
            intro: translationService.translate('Search.Common.Help.Columns.ProducedOn')
        });
        steps.push({
            element: columnDefs[7],
            intro: translationService.translate('Search.Common.Help.Columns.ProductionGroup')
        });

        steps.push({
            element: columnDefs[8],
            intro: translationService.translate('Search.Common.Help.Columns.Trigger')
        });

        steps.push({
            element: columnDefs[9],
            intro: translationService.translate('Search.Common.Help.Columns.TimesProduced')
        });

        return steps;
    }

    var afterIntro = function () {
        if (showingDetailsBeforeHelp !== $scope.showDetails)
            $scope.toggleDetails();
    }

    introJsService.onbeforechange(function (targetElement) {
        if (!$scope.showDetails) {
            $scope.toggleDetails();
        }
    });

    introJsService.onexit(afterIntro);
    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage, true);
};

function SearchBoxLastConfirmationDialogCtrl($log, $modalInstance, $scope, messagingService, translationService, selectedOrders) {
    selectedOrders.forEach(function (order) {
        order.message = translationService.translate('Search.Common.ReproduceDialog.Message.Body', order.CustomerUniqueId);
    });

    $scope.selectedProducibles = selectedOrders;
    $scope.cancelOrReproduceTitle = translationService.translate('Search.BoxLast.ReproduceDialog.Label.Header');

    $scope.closeBreakOrdersDialogWindow = function () {
        $modalInstance.dismiss();
    };

    $scope.reproduce = function (result) {
        var producibles = Enumerable.From($scope.selectedProducibles).Select(function (selectedProducible) {
            return {
                Id: selectedProducible.Id,
                CustomerUniqueId: selectedProducible.CustomerUniqueId
            };
        }).ToArray();

        $log.info('Saving broken box request Group... ');
        //TFS 13115 - Bug 13115:3.0 Produced items imported by day recording negative value when reproducing and completed job:
        //I don't know why this plural message is in the system. Is it too hard to use one messsage and if
        //needed pass a collection to it? Last time I checked you could always pass a collection
        //with only one item in it or even make a loop in caller and deal individuallyt in callee. Take a stance.
        //I'm removing the janky way which jacks up metrics and just making a looping call to something that
        //I know works.
        //messagingService.publish("ReproduceCartons", producibles);
        Enumerable.From(producibles).ForEach(function (p) {
            messagingService.publish("ReproduceCarton", p);
        });
        $modalInstance.close();
    };
}