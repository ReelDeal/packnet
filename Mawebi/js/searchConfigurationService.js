﻿function searchConfigurationService(messagingService) {
    var myScope = { $id: "searchConfigurationService", $on: function (a, b) { } }// spoof a scope so we can subscribe.
    var allSettings;

    var resultObservable = messagingService.messageSubject
        .where(messagingService.filterObs("SearchConfiguration"))
        .select(messagingService.dataFunction);

    resultObservable.autoSubscribe(function (data) {
        allSettings = data; 
    }, myScope, 'Search Configuration');

    //request designs
    messagingService.publish("GetSearchConfiguration", "");

    return {
        getMaxBoxFirstSearchResults : function (){
            return allSettings.MaxBoxFirstSearchResults;
        },

        getMaxBoxLastSearchResults : function (){
            return allSettings.MaxBoxLastSearchResults;
        },
        getMaxOrdersSearchResults : function (){
            return allSettings.MaxOrdersSearchResults;
        },
        getMaxScanToCreateSearchResults : function (){
            return allSettings.MaxScanToCreateSearchResults;
        }
    }
}

app.factory('searchConfigurationService', searchConfigurationService);