/*
messagingService object is used to create a web socket communication between the browser and 
the web server.  
this object is created once in the angular application and added as a service using the app factory
*/
function messagingService($log, $rootScope, $state) {
    console.debug('Loading messagingService');
    var currentMachineGroup = undefined;

    var rabbitMqHub = $.connection.rabbitMqHub;
    var connectionState = "disconnected";
    var connectionId = "";

    var startCommunication = function (subscriber) {
        // Start the connection.
        $.connection.hub.start()
            .done(function () {
                console.debug("Server connection started");
                rabbitMqHub.client.connected();
            })
            .fail(function () {
                console.error('Could not Connect to signalR!');
                connectionState = "disconnected";
                subscriber.onNext({ MessageType: "ConnectionFailed", Data: '' });
            });
    };

    var rawObservable = Rx.Observable.create(function (subscriber) {

        startCommunication(subscriber);

        rabbitMqHub.client.broadcastMessage = function (socketMessage) {
            var msg = JSON.parse(socketMessage);
            if (msg.MessageType != 'ConnectionAlive') {
                if (msg.Data && !msg.Compressed) {
                    console.debug('Message received: ' + msg.MessageType + " " + JSON.stringify(msg));
                }
                console.debug(subscriber.observer.observers.length + " observers listening");
                
            }
            console.log(msg.MessageType + " message size " + formatSizeUnits(byteCount(socketMessage)));

            if (msg.Data && msg.Compressed == true) {
                //decode base 64 encoded string
                console.time("Decompressing data took");
                var compressedData = atob(msg.Data);

                // Convert binary string to character-number array
                var charData = compressedData.split('').map(function (x) { return x.charCodeAt(0); });

                // Turn number array into byte-array
                var binData = new Uint8Array(charData);

                // Pako magic
                var data = pako.inflate(binData, {to:'string'});
                console.timeEnd("Decompressing data took");

                msg.Data = angular.fromJson(data);
                msg.Compressed = false;
                subscriber.onNext(msg);
            }
            else{
                subscriber.onNext(msg);
            }
        };


        $.connection.hub.connected = function () {
            rabbitMqHub.client.connected();
        };

        rabbitMqHub.client.connected = function () {
            connectionId = $.connection.hub.id;
            console.debug("Connected callback - connection ID: " + connectionId);
            connectionState = "connected";
            subscriber.onNext({ MessageType: "Connected", Data: '' });
        };

        $.connection.hub.disconnected = function () {
            rabbitMqHub.client.disconnected();
        };

        rabbitMqHub.client.disconnected = function () {
            if ($state.current.name == "Connecting") {
                return;
            }
            $state.go('Connecting');
            console.error("Server connection disconnected -- ID: " + connectionId);
            connectionId = "";
            connectionState = "disconnected";
            subscriber.onNext({ MessageType: "Disconnected", Data: '' });
            setTimeout(function () {
                //Give the connect page 1 second to load and get ready for the reconnect ry
                startCommunication();
            }, 1000);
        };

        $.connection.hub.error = function () {
            rabbitMqHub.client.error();
        };

        rabbitMqHub.client.error = function (error) {
            console.error("Server connection error -- ID: " + $.connection.hub.id + " Error: " + error);
            subscriber.onNext({ MessageType: "ConnectionError", Data: error });
        };

        $.connection.hub.connectionSlow = function () {
            rabbitMqHub.client.connectionSlow();
        };

        rabbitMqHub.client.connectionSlow = function () {
            console.info("Server connection slow -- ID: " + $.connection.hub.id);
            subscriber.onNext({ MessageType: "ConnectionSlow", Data: '' });
        };

        $.connection.hub.reconnecting = function () {
            rabbitMqHub.client.reconnecting();
        };

        rabbitMqHub.client.reconnecting = function () {
            console.info("Server connection reconnecting -- ID: " + $.connection.hub.id);
            connectionId = $.connection.hub.id;
            connectionState = "reconnecting";
            subscriber.onNext({ MessageType: "ConnectionReconnecting", Data: '' });
        };

        $.connection.hub.reconnected = function () {
            rabbitMqHub.client.reconnected();
        };

        rabbitMqHub.client.reconnected = function () {
            console.info("Server connection reconnected -- ID: " + $.connection.hub.id);
            connectionId = $.connection.hub.id;
            connectionState = "connected";
            subscriber.onNext({ MessageType: "ConnectionReconnected", Data: '' });
        };

    });

    //Subject allows multiple subscribers
    this.messageSubject = new Rx.Subject();
    rawObservable.subscribe(this.messageSubject);

    //Return the function that will filter messages
    var filterObs = function (filter) {
        return function (data) {
            return data.MessageType === filter;
        };
    };

    function formatSizeUnits(bytes) {
        if ((bytes >> 30) & 0x3FF)
            bytes = (bytes >>> 30) + '.' + (bytes & (3 * 0x3FF)) + 'GB';
        else if ((bytes >> 20) & 0x3FF)
            bytes = (bytes >>> 20) + '.' + (bytes & (2 * 0x3FF)) + 'MB';
        else if ((bytes >> 10) & 0x3FF)
            bytes = (bytes >>> 10) + '.' + (bytes & (0x3FF)) + 'KB';
        else if ((bytes >> 1) & 0x3FF)
            bytes = (bytes >>> 1) + 'Bytes';
        else
            bytes = bytes + 'Byte';
        return bytes;
    }

    var dataFunction = function (msg) {
        return msg.Data;
    };

    var responseDataFunction = function (msg){
        return {
            Data: msg.Data,
            Result: msg.Result
        }
    }

    function byteCount(s) {
        return encodeURI(s).split(/%..|./).length - 1;
    }

    var searchOrdersResponseObservable = this.messageSubject
        .where(filterObs("SearchOrdersResponse"));

    var searchBoxFirstResponseObservable = this.messageSubject
        .where(filterObs("SearchBoxFirstResponse"));

    var searchBoxLastResponseObservable = this.messageSubject
        .where(filterObs("SearchBoxLastResponse"));

        var searchScanToCreateResponseObservable = this.messageSubject
            .where(filterObs("SearchScanToCreateResponse"));

    var connectionAliveObservable = this.messageSubject
        .where(filterObs("ConnectionAlive"));

    var machinesObservable = this.messageSubject
        .where(filterObs("Machines"))
        .sample(50); //Machines are sent to the UI all the time, don't respond to every message

    var machineUpdatedObservable = this.messageSubject
        .where(filterObs("MachineUpdated"));

    var machineCreatedObservable = this.messageSubject
        .where(filterObs("MachineCreated"));

    var machineDeletedObservable = this.messageSubject
        .where(filterObs("MachineDeleted"));

    var machineProductionHistoryObservable = this.messageSubject
        .where(filterObs("MachineProductionHistory"));

    var fusionMachineSettingsFileNamesObservable = this.messageSubject
        .where(filterObs("FusionMachineSettingsFileNames"));

    var emMachineSettingsFileNamesObservable = this.messageSubject
        .where(filterObs("EmMachineSettingsFileNames"));

    var machineIoAccessoriesObservable = this.messageSubject
        .where(filterObs("MachineIoAccessories"));

    var ioAccessoryCreatedObservable = this.messageSubject
        .where(filterObs("IOAccessoryCreated"));
    
    var ioAccessoryUpdatedObservable = this.messageSubject
        .where(filterObs("IOAccessoryUpdated"));

    var userLoggedInObservable = this.messageSubject
        .where(filterObs("UserLoggedIn"));

    var productionSearchResultsObservable = this.messageSubject
        .where(filterObs("ProductionSearchResults"));

    var producibleStatusChangedObservable = this.messageSubject
        .where(filterObs("ProducibleStatusChanged"));

    var historySearchResultsObservable = this.messageSubject
        .where(filterObs("HistorySearchResults"));

    var signalRConnectedObservable = this.messageSubject
        .where(filterObs("Connected"));

    var autoLoggedInObservable = this.messageSubject
        .where(filterObs("AutoLoggedIn"));

    var siteSettingsObservable = this.messageSubject
        .where(filterObs("SiteSettings"));

    var cartonRequestsObservable = this.messageSubject
        .where(filterObs("CartonRequests"));

    var articlesObservable = this.messageSubject
        .where(filterObs("Articles"));

    var articleCreatedObservable = this.messageSubject
        .where(filterObs("ArticleCreated"));

    var articleDeletedObservable = this.messageSubject
        .where(filterObs("ArticleDeleted"));

    var articleImportStartedObservable = this.messageSubject
        .where(filterObs("ArticleImportStarted"));

    var articlesExportedObservable = this.messageSubject
        .where(filterObs("ArticlesExported"));

    var articlesExportedNewObservable = this.messageSubject
        .where(filterObs("ArticleExportedNew"));

    var articlesReadyForImportObservable = this.messageSubject
        .where(filterObs("ArticlesReadyForImport"));

    var articlesFinalizeImportObservable = this.messageSubject
        .where(filterObs("FinalizeArticleImport"));

    var articleUpdatedObservable = this.messageSubject
        .where(filterObs("ArticleUpdated"));

    //ArticlesNew
    //var articleNewSearchObservable = this.messageSubject
    //    .where(filterObs("ArticleSearchResponseNew"));

    var articleNewCreatedObservable = this.messageSubject
            .where(filterObs("ArticleCreatedNew"));

    var articleNewUpdatedObservable = this.messageSubject
        .where(filterObs("ArticleUpdatedNew"));

    var articleNewProducedObservable = this.messageSubject
        .where(filterObs("ArticleProducedNew"));

    var articleNewDeletedObservable = this.messageSubject
        .where(filterObs("ArticleDeletedNew"));

    var packagingDesignWithAppliedValuesObservable = this.messageSubject
        .where(filterObs("PackagingDesignWithAppliedValues"));

    var productionGroupsObservable = this.messageSubject
        .where(filterObs("ProductionGroups"));

    var productionGroupCreatedObservable = this.messageSubject
        .where(filterObs("ProductionGroupCreated"));

    var productionGroupUpdatedObservable = this.messageSubject
        .where(filterObs("ProductionGroupUpdated"));

    var productionGroupDeletedObservable = this.messageSubject
        .where(filterObs("ProductionGroupDeleted"));

    var versionInfoObservable = this.messageSubject
        .where(filterObs("VersionInfo"));

    var assignCorrugatesResponseObservable = this.messageSubject
        .where(filterObs("AssignCorrugatesResponse"));

    var failedRequestsObservable = this.messageSubject
        .where(filterObs("FailedRequests"));

    var logOffRequestObservable = this.messageSubject
        .where(filterObs("LogOffRequest"));

    var machineSynchronizedObservable = this.messageSubject
        .where(filterObs("MachineSynchronized"));

    var machineGroupsObservable = this.messageSubject
        .where(filterObs("MachineGroups"));

    var machineGroupCreatedObservable = this.messageSubject
        .where(filterObs("MachineGroupCreated"));

    var machineGroupUpdatedObservable = this.messageSubject
        .where(filterObs("MachineGroupUpdated"));

    var machineGroupDeletedObservable = this.messageSubject
        .where(filterObs("MachineGroupDeleted"));

    var usersObservable = this.messageSubject
        .where(filterObs("Users"));

    var userCreatedObservable = this.messageSubject
        .where(filterObs("UserCreated"));

    var userUpdatedObservable = this.messageSubject
        .where(filterObs("UserUpdated"));

    var userDeletedObservable = this.messageSubject
        .where(filterObs("UserDeleted"));

    var userPermissionsObservable = this.messageSubject
        .where(filterObs("UserPermissions"));

    var cartonPropertyGroupsObservable = this.messageSubject
        .where(filterObs("CartonPropertyGroups"));

    var cartonPropertyGroupCreatedObservable = this.messageSubject
        .where(filterObs("CartonPropertyGroupCreated"));

    var cartonPropertyGroupUpdatedObservable = this.messageSubject
        .where(filterObs("CartonPropertyGroupUpdated"));

    var cartonPropertyGroupDeletedObservable = this.messageSubject
        .where(filterObs("CartonPropertyGroupDeleted"));

    var corrugatesOnMachinesObservable = this.messageSubject
        .where(filterObs("CorrugatesOnMachines"));

    var ordersObservable = this.messageSubject
        .where(filterObs("Orders"));
    
    var orderChangedObservable = this.messageSubject
        .where(filterObs("OrderChanged"));

    var ordersHistoryObservable = this.messageSubject
        .where(filterObs("OrdersHistory"));

    var pickZonesObservable = this.messageSubject
        .where(filterObs("PickZones"));

    var pickZoneCreatedObservable = this.messageSubject
        .where(filterObs("PickZoneCreated"));

    var pickZoneUpdatedObservable = this.messageSubject
        .where(filterObs("PickZoneUpdated"));

    var pickZoneDeletedObservable = this.messageSubject
        .where(filterObs("PickZoneDeleted"));

    var cartonReproductionResultsObservable = this.messageSubject
        .where(filterObs("TODO"));

    var cartonRemovalResultsObservable = this.messageSubject
        .where(filterObs("CartonRemovalResponse"));

    var getProduciblesObservable = this.messageSubject
        .where(filterObs("SearchedProducibles"));

    var getMachineCommandsObservable = this.messageSubject
        .where(filterObs("MachineCommandResponse"));

    var machineCommandDoneObservable = this.messageSubject
        .where(filterObs("MachineCommandDoneResponse"));

    var scanTriggerErrorObservable = this.messageSubject
        .where(filterObs("ScanTriggerError"));

    var scanTriggerSuccessObservable = this.messageSubject
        .where(filterObs("ScanTriggerSuccess"));

    var signalRConnectionFailedObservable = this.messageSubject
        .where(filterObs("ConnectionFailed"));

    var allUnsentSupportIncidents = this.messageSubject
        .where(filterObs("AllUnsentSupportIncidents"));

    var supportIncidentCreatedObservable = this.messageSubject
        .where(filterObs("SupportIncidentCreated"));

    var corrugatesObservable = this.messageSubject
        .where(filterObs("Corrugates"));

    var corrugateCreatedObservable = this.messageSubject
        .where(filterObs("CorrugateCreated"));

    var corrugateUpdatedObservable = this.messageSubject
        .where(filterObs("CorrugateUpdated"));

    var corrugateDeletedObservable = this.messageSubject
        .where(filterObs("CorrugateDeleted"));

    var classificationsObservable = this.messageSubject
        .where(filterObs("Classifications"));

    var classificationCreatedObservable = this.messageSubject
        .where(filterObs("ClassificationCreated"));

    var classificationUpdatedObservable = this.messageSubject
        .where(filterObs("ClassificationUpdated"));

    var classificationDeletedObservable = this.messageSubject
        .where(filterObs("ClassificationDeleted"));

    var produciblesImportedObservable = this.messageSubject
        .where(filterObs("ProducibleImportCount"));

    var produciblesImportedByDayObservable = this.messageSubject
        .where(filterObs("ProducibleImportCountByDay"));

    var produciblesImportedByWeekObservable = this.messageSubject
        .where(filterObs("ProducibleImportCountByWeek"));

    var produciblesCreatedObservable = this.messageSubject
        .where(filterObs("ProducibleCreatedCount"));

    var produciblesCreatedByDayObservable = this.messageSubject
        .where(filterObs("ProducibleCreatedCountByDay"));

    var produciblesCreatedByWeekObservable = this.messageSubject
        .where(filterObs("ProducibleCreatedCountByWeek"));

    var produciblesFailedObservable = this.messageSubject
        .where(filterObs("ProducibleFailedCount"));

    var produciblesFailedByDayObservable = this.messageSubject
        .where(filterObs("ProducibleFailedCountByDay"));

    var produciblesFailedByWeekObservable = this.messageSubject
        .where(filterObs("ProducibleFailedCountByWeek"));

    var averageProductionTimeObservable = this.messageSubject
        .where(filterObs("AverageProductionTime"));

    var averageProductionTimeByDayObservable = this.messageSubject
        .where(filterObs("AverageProductionTimeByDay"));

    var averageProductionTimeByWeekObservable = this.messageSubject
        .where(filterObs("AverageProductionTimeByWeek"));

    var averageWastePercentageObservable = this.messageSubject
        .where(filterObs("AverageWastePercentage"));

    var averageWastePercentageByDayObservable = this.messageSubject
        .where(filterObs("AverageWastePercentageByDay"));

    var averageWastePercentageByWeekObservable = this.messageSubject
        .where(filterObs("AverageWastePercentageByWeek"));

    var averageWastePercentageByCorrugateObservable = this.messageSubject
        .where(filterObs("AverageWastePercentageByCorrugate"));

    var averageWastePercentageByCorrugateByDayObservable = this.messageSubject
        .where(filterObs("AverageWastePercentageByCorrugateByDay"));

    var averageWastePercentageByCorrugateByWeekObservable = this.messageSubject
        .where(filterObs("AverageWastePercentageByCorrugateByWeek"));

    var corrugateConsumptionObservable = this.messageSubject
        .where(filterObs("CorrugateConsumption"));

    var corrugateConsumptionByDayObservable = this.messageSubject
        .where(filterObs("CorrugateConsumptionByDay"));

    var corrugateConsumptionByWeekObservable = this.messageSubject
        .where(filterObs("CorrugateConsumptionByWeek"));

    var cartonCountByMachineObservable = this.messageSubject
        .where(filterObs("CartonCountByMachine"));

    var cartonCountByMachineByDayObservable = this.messageSubject
        .where(filterObs("CartonCountByMachineByDay"));

    var cartonCountByMachineByWeekObservable = this.messageSubject
        .where(filterObs("CartonCountByMachineByWeek"));

    var cartonCountByUserObservable = this.messageSubject
        .where(filterObs("CartonCountByUser"));

    var cartonCountByUserByDayObservable = this.messageSubject
        .where(filterObs("CartonCountByUserByDay"));

    var cartonCountByUserByWeekObservable = this.messageSubject
        .where(filterObs("CartonCountByUserByWeek"));

    var cartonCountByCPGObservable = this.messageSubject
        .where(filterObs("CartonCountByCPG"));

    var cartonCountByCPGByHourObservable = this.messageSubject
        .where(filterObs("CartonCountByCPGByHour"));

    var cartonCountByCPGBySlidingHourObservable = this.messageSubject
        .where(filterObs("CartonCountByCPGBySlidingHour"));

    var cartonCountByCPGByDayObservable = this.messageSubject
        .where(filterObs("CartonCountByCPGByDay"));

    var cartonCountByCPGByWeekObservable = this.messageSubject
        .where(filterObs("CartonCountByCPGByWeek"));

    var cartonCountByCPGByMonthObservable = this.messageSubject
        .where(filterObs("CartonCountByCPGByMonth"));

    var produciblesRotatedObservable = this.messageSubject
        .where(filterObs("ProducibleRotatedCount"));

    var produciblesRotatedByDayObservable = this.messageSubject
        .where(filterObs("ProducibleRotatedCountByDay"));

    var produciblesRotatedByWeekObservable = this.messageSubject
        .where(filterObs("ProducibleRotatedCountByWeek"));

    var wasteReportResponse = this.messageSubject
        .where(filterObs("WasteReportResponse"));

    var configurationRequestResponse = this.messageSubject
        .where(filterObs("ConfigurationRequestResponse"));

    var configurationUpdateResponse = this.messageSubject
        .where(filterObs("ConfigurationUpdateResponse"));

    var customCartonCreatedObservable = this.messageSubject
        .where(filterObs("CustomCartonCreated"));

    var machineGroupWorkflowPathsObservable = this.messageSubject
        .where(filterObs("MachineGroupWorkflowPaths"));

    var cartonRemovedCountObservable = this.messageSubject
        .where(filterObs("CartonRemovedCount"));

    var scanDataWorkflowsObservable = this.messageSubject
        .where(filterObs("ScanDataWorkflows"));

    //helper function that formats request to json
    function createMessage(type, data) {
        var replyTo = connectionId;

        var stringData = JSON.stringify({
            MessageType: type,
            Data: data,
            ReplyTo: replyTo,
            BrowserFingerPrint: $rootScope.browserFingerPrint,
            MachineGroupName: currentMachineGroup ? currentMachineGroup.Alias : "",
            MachineGroupId: currentMachineGroup ? currentMachineGroup.Id : null,
            UserName: $rootScope.currentUser ? $rootScope.currentUser.UserName : ""
        });
        return stringData;
    }

    return {
        publish : function (type, data, shortLogging){
            var message = createMessage(type, data);
            if (rabbitMqHub.connection.state == $.signalR.connectionState.connected) {
                rabbitMqHub.server.send(type, message);
                if (shortLogging && message.length > 1000)
                    console.debug('Message sent: ' + message.substring(0, 1000) + '... (Message was shortened by ' + (message.length - 1000) + ' characters)');
                else
                    console.debug('Message sent: ' + message);
            }
            else {
                rabbitMqHub.client.disconnected();
            }
        },
        setCurrentMachineGroup : function (machineGroup){
            currentMachineGroup = machineGroup;
        },
        currentMachine : function (){
            return currentMachineGroup;
        },
        IsSignalRConnected : function (){
            return connectionState == "connected";
        },
        //Expose components to allow controllers to setup their own observables
        filterObs: filterObs,
        messageSubject: this.messageSubject,
        dataFunction: dataFunction,
        responseDataFunction : responseDataFunction,

        //Expose the filtered observables
        signalRConnectedObservable : signalRConnectedObservable, //Connected to signalr
        signalRConnectionFailedObservable : signalRConnectionFailedObservable,
        connectionAliveObservable : connectionAliveObservable, //Connected to .server
        autoLoggedInObservable : autoLoggedInObservable,
        productionSearchResultsObservable : productionSearchResultsObservable,
        historySearchResultsObservable : historySearchResultsObservable,
        machineProductionHistoryObservable : machineProductionHistoryObservable,
        fusionMachineSettingsFileNamesObservable : fusionMachineSettingsFileNamesObservable,
        emMachineSettingsFileNamesObservable : emMachineSettingsFileNamesObservable,
        machineIoAccessoriesObservable: machineIoAccessoriesObservable,
        ioAccessoryCreatedObservable: ioAccessoryCreatedObservable,
        ioAccessoryUpdatedObservable: ioAccessoryUpdatedObservable,
        userLoggedInObservable : userLoggedInObservable,
        producibleStatusChangedObservable : producibleStatusChangedObservable,
        siteSettingsObservable : siteSettingsObservable,
        cartonRequestsObservable : cartonRequestsObservable,
        getProduciblesObservable : getProduciblesObservable,
        getMachineCommandsObservable : getMachineCommandsObservable,
        machineCommandDoneObservable : machineCommandDoneObservable,
        cartonRemovedCountObservable : cartonRemovedCountObservable,
        customCartonCreatedObservable : customCartonCreatedObservable,

        //Article messages
        //articlesNewSearchObservable: articleNewSearchObservable,
        articleNewCreatedObservable : articleNewCreatedObservable,
        articleNewUpdatedObservable : articleNewUpdatedObservable,
        articleNewDeletedObservable: articleNewDeletedObservable,
        articleNewProducedObservable: articleNewProducedObservable,
        articlesObservable : articlesObservable,
        articleCreatedObservable : articleCreatedObservable,
        articleDeletedObservable : articleDeletedObservable,
        articleUpdatedObservable : articleUpdatedObservable,
        articleImportStartedObservable : articleImportStartedObservable,
        articlesExportedObservable: articlesExportedObservable,
        articlesExportedNewObservable: articlesExportedNewObservable,

        articlesFinalizeImportObservable: articlesFinalizeImportObservable,
        articlesReadyForImportObservable: articlesReadyForImportObservable,

        allUnsentSupportIncidents : allUnsentSupportIncidents,
        supportIncidentCreatedObservable : supportIncidentCreatedObservable,
        packagingDesignWithAppliedValuesObservable: packagingDesignWithAppliedValuesObservable,

        //Production Groups
        productionGroupsObservable : productionGroupsObservable,
        productionGroupCreatedObservable : productionGroupCreatedObservable,
        productionGroupUpdatedObservable : productionGroupUpdatedObservable,
        productionGroupDeletedObservable : productionGroupDeletedObservable,

        versionInfoObservable : versionInfoObservable,
        assignCorrugatesResponseObservable : assignCorrugatesResponseObservable,
        failedRequestsObservable : failedRequestsObservable,
        logOffRequestObservable : logOffRequestObservable,
        machineSynchronizedObservable : machineSynchronizedObservable,

        scanDataWorkflowsObservable : scanDataWorkflowsObservable,

        /* User messages */
        usersObservable : usersObservable,
        userUpdatedObservable: userUpdatedObservable,
        userDeletedObservable: userDeletedObservable,
        userCreatedObservable : userCreatedObservable,
        userPermissionsObservable : userPermissionsObservable,

        /* MachineGroup messages */
        machineGroupsObservable : machineGroupsObservable,
        machineGroupUpdatedObservable : machineGroupUpdatedObservable,
        machineGroupCreatedObservable : machineGroupCreatedObservable,
        machineGroupDeletedObservable : machineGroupDeletedObservable,
        machineGroupWorkflowPathsObservable : machineGroupWorkflowPathsObservable,

        /* Machine messages */
        machinesObservable : machinesObservable,
        machineCreatedObservable : machineCreatedObservable,
        machineUpdatedObservable : machineUpdatedObservable,
        machineDeletedObservable : machineDeletedObservable,

        corrugatesOnMachinesObservable : corrugatesOnMachinesObservable,

        ordersObservable: ordersObservable,
        orderChangedObservable: orderChangedObservable,
        searchOrdersResponseObservable : searchOrdersResponseObservable,
        searchBoxFirstResponseObservable : searchBoxFirstResponseObservable,
        searchBoxLastResponseObservable : searchBoxLastResponseObservable,
        searchScanToCreateResponseObservable : searchScanToCreateResponseObservable,
        ordersHistoryObservable : ordersHistoryObservable,

        /* PickZone Messages */
        pickZonesObservable : pickZonesObservable,
        pickZoneCreatedObservable : pickZoneCreatedObservable,
        pickZoneUpdatedObservable : pickZoneUpdatedObservable,
        pickZoneDeletedObservable : pickZoneDeletedObservable,

        cartonReproductionResultsObservable : cartonReproductionResultsObservable,
        cartonRemovalResultsObservable : cartonRemovalResultsObservable,
        scanTriggerErrorObservable : scanTriggerErrorObservable,
        scanTriggerSuccessObservable : scanTriggerSuccessObservable,

        /* Corrugate messages */
        corrugatesObservable : corrugatesObservable,
        corrugateCreatedObservable : corrugateCreatedObservable,
        corrugateUpdatedObservable : corrugateUpdatedObservable,
        corrugateDeletedObservable : corrugateDeletedObservable,

        /* Classifications messages */
        classificationsObservable : classificationsObservable,
        classificationCreatedObservable : classificationCreatedObservable,
        classificationUpdatedObservable : classificationUpdatedObservable,
        classificationDeletedObservable : classificationDeletedObservable,

        /* Carton Property Group Messages */
        cartonPropertyGroupsObservable : cartonPropertyGroupsObservable,
        cartonPropertyGroupCreatedObservable : cartonPropertyGroupCreatedObservable,
        cartonPropertyGroupUpdatedObservable : cartonPropertyGroupUpdatedObservable,
        cartonPropertyGroupDeletedObservable : cartonPropertyGroupDeletedObservable,

        /* Metrics */
        produciblesImportedObservable : produciblesImportedObservable,
        produciblesImportedByDayObservable: produciblesImportedByDayObservable,
        produciblesImportedByWeekObservable : produciblesImportedByWeekObservable,

        produciblesCreatedObservable : produciblesCreatedObservable,
        produciblesCreatedByDayObservable : produciblesCreatedByDayObservable,
        produciblesCreatedByWeekObservable : produciblesCreatedByWeekObservable,

        produciblesFailedObservable : produciblesFailedObservable,
        produciblesFailedByDayObservable : produciblesFailedByDayObservable,
        produciblesFailedByWeekObservable : produciblesFailedByWeekObservable,

        averageProductionTimeObservable : averageProductionTimeObservable,
        averageProductionTimeByDayObservable : averageProductionTimeByDayObservable,
        averageProductionTimeByWeekObservable : averageProductionTimeByWeekObservable,

        averageWastePercentageObservable : averageWastePercentageObservable,
        averageWastePercentageByDayObservable : averageWastePercentageByDayObservable,
        averageWastePercentageByWeekObservable : averageWastePercentageByWeekObservable,

        produciblesRotatedObservable : produciblesRotatedObservable,
        produciblesRotatedByDayObservable : produciblesRotatedByDayObservable,
        produciblesRotatedByWeekObservable : produciblesRotatedByWeekObservable,

        cartonCountByMachineObservable : cartonCountByMachineObservable,
        cartonCountByMachineByDayObservable : cartonCountByMachineByDayObservable,
        cartonCountByMachineByWeekObservable : cartonCountByMachineByWeekObservable,

        wasteReportResponse : wasteReportResponse,
        configurationUpdateResponse : configurationUpdateResponse,
        configurationRequestResponse : configurationRequestResponse,

        cartonCountByUserObservable : cartonCountByUserObservable,
        cartonCountByUserByDayObservable : cartonCountByUserByDayObservable,
        cartonCountByUserByWeekObservable : cartonCountByUserByWeekObservable,

        cartonCountByCPGObservable : cartonCountByCPGObservable,
        cartonCountByCPGByHourObservable : cartonCountByCPGByHourObservable,
        cartonCountByCPGBySlidingHourObservable : cartonCountByCPGBySlidingHourObservable,
        cartonCountByCPGByDayObservable : cartonCountByCPGByDayObservable,
        cartonCountByCPGByWeekObservable : cartonCountByCPGByWeekObservable,
        cartonCountByCPGByMonthObservable : cartonCountByCPGByMonthObservable,

        corrugateConsumptionObservable : corrugateConsumptionObservable,
        corrugateConsumptionByDayObservable : corrugateConsumptionByDayObservable,
        corrugateConsumptionByWeekObservable : corrugateConsumptionByWeekObservable,

        averageWastePercentageByCorrugateObservable : averageWastePercentageByCorrugateObservable,
        averageWastePercentageByCorrugateByDayObservable : averageWastePercentageByCorrugateByDayObservable,
        averageWastePercentageByCorrugateByWeekObservable : averageWastePercentageByCorrugateByWeekObservable

    };
}
app.factory('messagingService', messagingService);