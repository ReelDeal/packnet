function CartonPropertyGroupsController($location, $log, $scope, $state, $timeout, alertService, introJsService, messagingService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.floatingPointRegex = /^(\d+\.?\d*|\.\d+)$/;

    $scope.cartonPropertyGroups = [];
    $scope.selectedCartonPropertyGroup = {};
    $scope.selectedCartonPropertyGroup.IsNewCartonPropertyGroup = true;

    var awaitingUpdate = false;

    $scope.$on('$locationChangeSuccess', function () {
        if ($scope.selectedCartonPropertyGroup && $scope.selectedCartonPropertyGroup.IsNewCartonPropertyGroup == false && $location.path() == '/CartonPropertyGroups') {
            $scope.cancelNewEditCartonPropertyGroup();
        }
    });

    messagingService.cartonPropertyGroupsObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.cartonPropertyGroups;
            $scope.cartonPropertyGroups = msg.Data;
        }, 0, true);
    }, $scope, 'CartonPropertyGroups');

    messagingService.cartonPropertyGroupCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias));
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias));
        }
        else if (msg.Result === "Success") {
        	$state.go('^.list', null, { reload: true });

        	alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias), 5000);
        }
    }, $scope, 'CartonPropertyGroup Created');

    messagingService.cartonPropertyGroupUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            if (msg.Message === "CartonPropertyGroupInProduction") {
                alertService.addError(translationService.translate('CartonPropertyGroup.Message.UpdateInProduction', msg.Data.Alias));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias));
            }
        }
        else if (msg.Result === "Exists") {
            alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias));
        }
        else if (msg.Result === "Success") {
        	$state.go('^.list', null, { reload: true });

        	alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias), 5000);
        }
    }, $scope, 'CartonPropertyGroup Updated');

    messagingService.cartonPropertyGroupDeletedObservable.autoSubscribe(function (msg) {
        if (msg.Result === "Fail") {
            if (msg.Message === "CartonPropertyGroupInProduction") {
                alertService.addError(translationService.translate('CartonPropertyGroup.Message.DeleteInProduction', msg.Data.Alias));
            } else if (msg.Message === "CartonPropertyGroupRelationshipExists") {
                alertService.addError(translationService.translate('CartonPropertyGroup.Message.DeleteRelationshipExists', msg.Data.Alias));
            }
            else {
                alertService.addError(translationService.translate('Common.Message.DeleteFailed', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias));
            }
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', translationService.translate('CartonPropertyGroup.Label.NameSingular'), msg.Data.Alias), 5000);
            
            delete $scope.cartonPropertyGroupBeingSaved;
            $scope.cartonPropertyGroupBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedCartonPropertyGroup;
            $scope.selectedCartonPropertyGroup = {};
            $scope.selectedCartonPropertyGroup.IsNewCartonPropertyGroup = true;
        }
        requestData();
    }, $scope, 'Machine Group Deleted');

    var requestData = function () {
        messagingService.publish("GetCartonPropertyGroups");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);
    
    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.saveCartonPropertyGroup = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        delete $scope.cartonPropertyGroupBeingSaved;
        $scope.cartonPropertyGroupBeingSaved = {
            Id: $scope.selectedCartonPropertyGroup.Id,
            Alias: $scope.selectedCartonPropertyGroup.Alias,
            IsNewCartonPropertyGroup: $scope.selectedCartonPropertyGroup.IsNewCartonPropertyGroup
        };
        var action = "CreateCartonPropertyGroup";
        if (!$scope.cartonPropertyGroupBeingSaved.IsNewCartonPropertyGroup)
            action = "UpdateCartonPropertyGroup";
        messagingService.publish(action, $scope.cartonPropertyGroupBeingSaved);

        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('CartonPropertyGroup.Label.NameSingular'), $scope.cartonPropertyGroupBeingSaved.Alias));
                awaitingUpdate = false;
                
                delete $scope.cartonPropertyGroupBeingSaved;
                $scope.cartonPropertyGroupBeingSaved = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditCartonPropertyGroup = function () {
    	$state.go('^.list', null, { reload: true });
    }

    $scope.deleteCartonPropertyGroups = function () {
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (cartonPropertyGroup) {
            messagingService.publish("DeleteCartonPropertyGroup", cartonPropertyGroup);
        });
        $timeout(function () {
            $scope.gridApi.selection.clearSelectedRows();
        }, 0, true);
    }

    
    $scope.editCartonPropertyGroup = function (){
        delete $scope.selectedCartonPropertyGroup;
        $scope.selectedCartonPropertyGroup = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedCartonPropertyGroup.IsNewCartonPropertyGroup = false;
        $state.go('^.new');
    }

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'cartonPropertyGroups',
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                sort: { direction: uiGridConstants.ASC }
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
        }
    };

    $scope.tellMeAboutThisPage = function () {
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        if (angular.lowercase($state.current.name) === "cartonpropertygroups.new") {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('CartonPropertyGroup.Help.Form.Overview')
            });

            steps.push({
                element: "#cartonPropertyGroupAliasInputBox",
                intro: translationService.translate('CartonPropertyGroup.Help.Form.Name')
            });

            steps.push({
                element: "#newCartonPropertyGroupSaveButton",
                intro: translationService.translate('CartonPropertyGroup.Help.Form.Save')
            });

            steps.push({
                element: "#newCartonPropertyGroupCancelButton",
                intro: translationService.translate('CartonPropertyGroup.Help.Form.Cancel')
            });

        }
        else {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('CartonPropertyGroup.Help.List.Overview')
            });

            steps.push({
                element: "#newCartonPropertyGroup",
                intro: translationService.translate('CartonPropertyGroup.Help.List.New')
            });

            steps.push({
                element: "#deleteCartonPropertyGroup",
                intro: translationService.translate('CartonPropertyGroup.Help.List.Delete')
            });

            steps.push({
                element: "#editCartonPropertyGroup",
                intro: translationService.translate('CartonPropertyGroup.Help.List.Edit')
            });

            steps.push({
                element: '#cartonPropertyGroupListTable',
                intro: translationService.translate('CartonPropertyGroup.Help.List.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('CartonPropertyGroup.Help.List.Columns.Alias')
            });
        }

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};