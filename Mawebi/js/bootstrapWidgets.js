
angular.module('numericUpDown', ['ui.bootstrap.tpls', "template/numericupdown/numericupdown.html"])
    .directive('loaderWidget', function () {
        return {
            restrict: 'E',
            scope: { loadingMessage: '@' },
            template: '<span id="loaderWidget"><img alt="loading image" src="img/ajax-loader.gif"/> {{loadingMessage}}</span>'
        };
    })
    .directive('selectOnClick', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('click', function () {
                    if (this.type === 'number' || this.selectionStart === this.selectionEnd) {
                        this.select();
                    }
                });
            }
        };
    })
    .directive('focusElement', function ($timeout) {
        return {
            link: function (scope, element, attrs, model) {
                if ($timeout.getInstance) {
                    $timeout = $timeout.getInstance(scope);
                }
                $timeout(function () {
                    element[0].focus();
                });
            }
        };
    })
    .directive('numericUpDown', ['$log', function ($log) {
        /*Example
        <numeric-up-down id="produceArticleNUD" ng-model="someScopeObject" default-value="0" min-value="0" max-value="9999" horizontal="true" on-changed="someFunction(someScopeObject)"></numeric-up-down>'
        */
        return {
            restrict: 'EA',
            require: '^ngModel',
            replace: true,
            scope: {
                onChanged: "&",
                ngModel: '=',
                id: '@'
                /* These attributes are available for use with 
                this directive but we wanted default values so the code to implement 
                is in the link: function defined below.
                , defaultValue: "@" //default is 0
                , minValue: "@" // default is Number.MIN_VALUE
                , maxValue: "@"// default is Number.MAX_VALUE
                , vertical: "@" // default is false
                , horizontal: "@" //default is false
                , displayText: "@" // default is ""
                */
            },
            templateUrl: 'template/numericupdown/numericupdown.html',
            link: function (scope, element, attrs, ngModel) {
                if (!ngModel) {
                    return; // do nothing if no ng-model
                }
                scope.onChanged = (angular.isDefined(attrs.onChanged)) ? scope.onChanged : function () { };
                scope.defaultValue = (angular.isDefined(attrs.defaultValue)) ? scope.$eval(attrs.defaultValue) : 0;
                scope.minValue = (angular.isDefined(attrs.minValue)) ? scope.$eval(attrs.minValue) : 0;
                scope.maxValue = (angular.isDefined(attrs.maxValue)) ? scope.$eval(attrs.maxValue) : Number.MAX_VALUE;
                scope.vertical = (angular.isDefined(attrs.vertical)) ? scope.$eval(attrs.vertical) : false;
                scope.horizontal = (angular.isDefined(attrs.horizontal)) ? scope.$eval(attrs.horizontal) : false;
                scope.displayText = (angular.isDefined(attrs.displayText)) ? scope.$eval(attrs.displayText) : "";
                scope.id = (angular.isDefined(attrs.id)) ? scope.$eval(attrs.id) : "";

                if (scope.defaultValue < scope.minValue)
                    throw new Error('defaultValue is less than minValue defined in usage of directive:numericUpDown');
                if (scope.defaultValue > scope.maxValue)
                    throw new Error('defaultValue is greater than maxValue defined in usage of directive:numericUpDown');

                if (scope.vertical && scope.horizontal)
                    scope.vertical = false;

                if (!scope.vertical && !scope.horizontal)
                    scope.horizontal = true;

                if (scope.ngModel == undefined) {
                    scope.ngModel = scope.value = scope.defaultValue;
                }
                else {
                    if (scope.ngModel < scope.minValue || scope.ngModel > scope.maxValue) {
                        scope.value = scope.defaultValue;
                    }
                    else {
                        scope.value = scope.ngModel;
                    }
                }
                // Respond on mousewheel spin
                var mousewheel = (angular.isDefined(attrs.mousewheel)) ? scope.$eval(attrs.mousewheel) : false;
                if (mousewheel) {

                    var isScrollingUp = function (e) {
                        if (e.originalEvent) {
                            e = e.originalEvent;
                        }
                        //pick correct delta variable depending on event
                        var delta = (e.wheelDelta) ? e.wheelDelta : -e.deltaY;
                        return (e.detail || delta > 0);
                    };

                    value.bind('mousewheel wheel', function (e) {
                        scope.$apply((isScrollingUp(e)) ? scope.increment() : scope.decrement());
                        e.preventDefault();
                    });
                }

                scope.incrementDisabled = function () {
                    return scope.value >= scope.maxValue;
                };
                scope.decrementDisabled = function () {
                    return scope.value <= scope.minValue;
                };

                scope.$watch('ngModel', function () {
                    if (scope.ngModel == undefined || scope.ngModel < scope.minValue || scope.ngModel > scope.maxValue)
                        scope.value = scope.ngModel = scope.defaultValue;
                    if (typeof scope.ngModel === 'string') {
                        if (isNaN(scope.ngModel)) {
                            scope.value = scope.ngModel = scope.defaultValue;
                        }
                        else {
                            scope.value = scope.ngModel = parseInt(scope.ngModel);
                        }
                    }
                    if(scope.value != scope.ngModel)
                        scope.value = scope.ngModel;
                    scope.onChanged();
                });
               
                scope.increment = function () {
                    if (scope.incrementDisabled())
                        return;
                    scope.value++;
                    scope.ngModel = scope.value;
                };
                scope.decrement = function () {
                    if (scope.decrementDisabled())
                        return;
                    scope.value--;
                    scope.ngModel = scope.value;
                };
                scope.keydown = function (value) {
                    if (isNaN(value)){
                        scope.value = value.replace(/\D/g, '').replace(/ /g, '');
                    }
                    
                    if (scope.value == '')
                        scope.value = scope.minValue;
                    if (scope.value != '' && scope.value < scope.minValue)
                        scope.value = scope.minValue;
                    if (scope.value > scope.maxValue)
                        scope.value = scope.maxValue;
                    scope.ngModel = scope.value;
                };
                
            }
        };
    }]);

angular.module("template/numericupdown/numericupdown.html", [])
    .run(["$templateCache", function ($templateCache) {
        $templateCache.put("template/numericupdown/numericupdown.html",
          "<span>\n" +
            "<table id='nudVerticalTable' class=\"numericUpDownTable\" style=\"width: auto\" ng-show='vertical'>\n" +
              "<tr>\n" +
                "<td>\n" +
                "<button id='nudIncrementButtonVertical' type='button' ng-click='increment()' ng-disabled='incrementDisabled()' class=\"btn btn-default\" style=\"width: 100%;margin: 0;\">\n" +
                  "<i class=\"fa fa-chevron-up\"></i>\n" +
                "</button></td>\n" +
              "</tr>\n" +
              "<tr>\n" +
                "<td ng-class=\"{'error': invalid}\">\n" +
                  "<span id='nudDisplayTextVertical' class=\"numericUpDownLabel\">{{displayText}} </span><input id='nudInputTextVertical' type=\"text\" pattern=\"[0-9]*\" style=\"margin: 0;text-align: center;\" ng-model=\"value\" class=\"span1 form-control\" ng-mousewheel=\"increment()\" ng-blur=\"keydown(this.value)\" ng-readonly=\"readonlyInput\" maxlength=\"maxLength\" />\n" +
                "</td>\n" +
              "</tr>\n" +
              "<tr>\n" +
                "<td>\n" +
                  "<button id='nudDecrementButtonVertical' type='button' ng-click='decrement()' ng-disabled='decrementDisabled()'class=\"btn btn-default\" style=\"width: 100%;margin: 0;\">\n" +
                    "<i class=\"fa fa-chevron-down\"></i>\n" +
                  "</button>\n" +
                "</td>\n" +
              "</tr>\n" +
            "</table>" +
            "<table id='nudHorizontalTable' class=\"numericUpDownTable\" style=\"width: auto\" ng-show='horizontal'>\n" +
              "<tr>\n" +
                "<td ng-class=\"{'error': invalid}\">\n" +
                  "<span id='nudDisplayText' class=\"numericUpDownLabel\">{{displayText}} </span><input id='{{id}}' type=\"text\" pattern=\"[0-9]*\" style=\"margin: 0;text-align: center;\" ng-model='value' class=\"span1 form-control\" ng-mousewheel='increment()' ng-blur='keydown(value)' ng-readonly='readonlyInput' maxlength='maxLength' />\n" +
                "</td>\n" +
                "<td>\n" +
                "<button id='nudIncrementButton' type='button' ng-click='increment()' ng-disabled='incrementDisabled()' class=\"btn btn-default\" style=\"width: 100%;margin: 0;\">\n" +
                  "<i class=\"fa fa-chevron-up\"></i>\n" +
                "</button></td>\n" +
                "<td>\n" +
                  "<button id='nudDecrementButton' type='button' ng-click='decrement()' ng-disabled='decrementDisabled()' class=\"btn btn-default\" style=\"width: 100%;margin: 0;\">\n" +
                    "<i class=\"fa fa-chevron-down\"></i>\n" +
                  "</button>\n" +
                "</td>\n" +
              "</tr>\n" +
            "</table>\n" +
          "</span>");
    }]);





