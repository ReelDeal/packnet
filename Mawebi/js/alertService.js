﻿app.factory('alertService', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
    $timeout = $timeout.getInstance($rootScope);
    var alertService;
    var closeAlertsPromises = [];
    $rootScope.alerts = [];
    
  
    var hasAlert = function (message) {
        return Enumerable.From($rootScope.alerts).Any(function (a) {
            return a.msg == message;
        });
    };

    var hasErrorAlert = function () {
        return Enumerable.From($rootScope.alerts).Any(function (a) {
            return a.type == 'danger';
        });
    };

    return alertService = {
        addActive: function (msg, timeout) {
            return this.append('active', msg, timeout);
        },
        addSuccess: function (msg, timeout) {
            return this.append('success', msg, timeout);
        },
        addWarning: function (msg, timeout) {
            return this.append('warning', msg, timeout);
        },
        addError: function (msg, timeout) {
            return this.append('danger', msg, timeout);
        },
        addInfo: function (msg, timeout) {
            return this.append('info', msg, timeout);
        },
        hasErrorAlert: hasErrorAlert,
        hasAlert: hasAlert,
        add: function (type, msg, timeout) {
            if (!hasAlert(msg)) {
                closeAlertsPromises.forEach(function (promise) {
                    $timeout.cancel(promise);
                });
                closeAlertsPromises.length = 0;

                $rootScope.alerts.length = 0;
                this.append(type, msg, timeout);
            }
        },
        append: function (type, msg, timeout){
            $timeout(function () {
                if (hasAlert(msg)) {
                    return;
                }

                if($rootScope && $rootScope.alerts)
                {
                    $rootScope.alerts.unshift({
                        type : type,
                        msg : msg,
                        close : function (){
                            return alertService.closeAlert(this);
                        }
                    });
                }

            }, 0, true);

            if (timeout) {
                closeAlertsPromises.push($timeout(function () {
                    alertService.closeAlert(this);
                }, timeout));
            }
        },
        closeByMessage: function (message) {
            var alert = Enumerable.From($rootScope.alerts).FirstOrDefault(null, function (a) {
                return a.msg == message;
            });

            if (alert != null) {
                this.closeAlert(alert);
            }
        },
        closeAlert: function (alert) {
            return this.closeAlertIdx($rootScope.alerts.indexOf(alert));
        },
        closeAlertIdx: function (index) {
            return $rootScope.alerts.splice(index, 1);
        },
        clearAll: function () {
            $rootScope.alerts.length = 0;
        }

    };
}]);