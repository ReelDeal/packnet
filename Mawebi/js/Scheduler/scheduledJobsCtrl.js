function ScheduledJobsCtrl($scope, $log, $state, $timeout, messagingService, alertService, uiGridConstants, introJsService, translationService) {
    $scope.$log = $log;
    
    $scope.messagingService = messagingService;
    $scope.isEditMode = false;
    $scope.scheduledJobs = [];
    $scope.selectedScheduledJob = {};
    $scope.selectedScheduledJob.IsNewScheduledJob = true;
    $scope.$timeout = $timeout.getInstance($scope, 'ScheduledJob Control');

    var awaitingUpdate = false;

    $scope.messagingService.scheduledJobsObservable.autoSubscribe(function (data) {
        
        delete $scope.scheduledJobs;
        $scope.scheduledJobs = data;
    }, $scope, 'ScheduledJobs');

    $scope.messagingService.scheduledJobCreatedObservable.autoSubscribe(function (data) {
        awaitingUpdate = false;
        if (data.Result === "Fail") {
            alertService.addError("Creating Pick Zone '" + $scope.scheduledJobBeingSaved.Alias + "' failed");
        }
        else if (data.Result === "Exists") {
            alertService.addError("Pick Zone '" + $scope.scheduledJobBeingSaved.Alias + "' already exists");
        }
        else if (data.Result === "Success") {
        	$state.go('^.list', null, { reload: true });

        	alertService.addSuccess("Pick Zone '" + $scope.scheduledJobBeingSaved.Alias + "' created successfully", 5000);
        }
    }, $scope, 'ScheduledJob Created');

    $scope.messagingService.scheduledJobUpdatedObservable.autoSubscribe(function (data) {
        awaitingUpdate = false;
        if (data.Result === "Fail") {
            alertService.addError("Pick Zone could not be updated... Try again");
        }
        else if (data.Result === "Success") {
        	$state.go('^.list', null, { reload: true });

        	alertService.addSuccess("Pick Zone updated successfully", 5000);
        }
    }, $scope, 'ScheduledJob Updated');

    $scope.messagingService.scheduledJobDeletedObservable.autoSubscribe(function (message) {
        if (message.Result === "Fail") {
            alertService.addError("Deleting Pick Zone '" + message.Data.Alias + "' failed");
        }
        else if (message.Result === "Success") {
            alertService.addSuccess("Pick Zone '" + message.Data.Alias + "' deleted successfully", 5000);
            
            delete $scope.scheduledJobBeingSaved;
            $scope.scheduledJobBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedScheduledJob;
            $scope.selectedScheduledJob = {};
            $scope.selectedScheduledJob.IsNewScheduledJob = true;
        }
        requestData();
    }, $scope, 'Machine Group Deleted');

    var requestData = function () {
        $scope.messagingService.publish("GetScheduledJobs");
    }

    $scope.utilities.initialDataRequest($scope, $scope.messagingService, requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'scheduledJobs',
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Pickzones.Headers.Name'),
                width: 150,
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                field: 'Description',
                name: translationService.translate('Pickzones.Headers.Description'),
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
       }
    };


    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.saveScheduledJob = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        
        delete $scope.scheduledJobBeingSaved;
        $scope.scheduledJobBeingSaved = {
            Id: $scope.selectedScheduledJob.Id,
            Alias: $scope.selectedScheduledJob.Alias,
            Description: $scope.selectedScheduledJob.Description,
            IsNewScheduledJob: $scope.selectedScheduledJob.IsNewScheduledJob,
        };
        var action = "CreateScheduledJob";
        if (!$scope.scheduledJobBeingSaved.IsNewScheduledJob)
            action = "UpdateScheduledJob";
        $scope.messagingService.publish(action, $scope.scheduledJobBeingSaved);
        $scope.$timeout(function () {
            if (awaitingUpdate && $scope.scheduledJobBeingSaved && $scope.schedule.Alias) {
                alertService.addError("Save failed for scheduledJob '" + $scope.scheduledJobBeingSaved.Alias + "', please try again.");
                awaitingUpdate = false;
                
                delete $scope.scheduledJobBeingSaved;
                $scope.scheduledJobBeingSaved = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditScheduledJob = function () {
    	$state.go('^.list', null, { reload: true });
    }

    $scope.deleteScheduledJobs = function () {
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (scheduledJob) {
            $scope.messagingService.publish("DeleteScheduledJob", scheduledJob);
        });

        $scope.gridApi.selection.clearSelectedRows();
    }

    $scope.editScheduledJob = function () {
        
        delete $scope.selectedScheduledJob;
        $scope.selectedScheduledJob = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedScheduledJob.IsNewScheduledJob = false;
        $scope.isEditMode = true;
        $state.go('^.new');
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];

        if (angular.lowercase($state.current.name) === "pickzones.new") {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('Pickzones.Intro.Edit.Overview')
            });

            steps.push({
                element: "#newScheduledJobScheduledJobName",
                intro: translationService.translate('Pickzones.Intro.Edit.Name')
            });

            steps.push({
                element: "#newMachineDescription",
                intro: translationService.translate('Pickzones.Intro.Edit.Description')
            });

            steps.push({
                element: "#newScheduledJobSaveButton",
                intro: translationService.translate('Pickzones.Intro.Edit.SaveButton')
            });

            steps.push({
                element: "#newScheduledJobCancelButton",
                intro: translationService.translate('Pickzones.Intro.Edit.CancelButton')
            });
            
        }
        else {

            var columnDefs = $("[ui-grid-header-cell]");

            steps.push({
                element: "#notFound",
                intro: translationService.translate('Pickzones.Intro.Overview')
            });

            steps.push({
                element: "#newScheduledJob",
                intro: translationService.translate('Pickzones.Intro.NewButton')
            });

            steps.push({
                element: "#deleteScheduledJob",
                intro: translationService.translate('Pickzones.Intro.DeleteButton')
            });

            steps.push({
                element: "#editScheduledJob",
                intro: translationService.translate('Pickzones.Intro.EditButton')
            });

            steps.push({
                element: '#damnCheckboxCannotBeHighlighted',
                intro: translationService.translate('Pickzones.Intro.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('Pickzones.Intro.Columns.Name')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('Pickzones.Intro.Columns.Description')
            });
        }

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};