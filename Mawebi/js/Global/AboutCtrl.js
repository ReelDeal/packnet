function AboutCtrl($scope, $timeout, $log, $window, messagingService){
    $timeout = $timeout.getInstance($scope);
    $scope.showingReleaseNotes = true;
    $scope.showingEula = false;
    $scope.showingSupportInfo = false;
    $scope.$log = $log.getInstance('About');
    $scope.userAgent = $window.navigator.userAgent;

    messagingService.signalRConnectedObservable.autoSubscribe(function () {
        messagingService.publish("GetVersionInfo", "");
    }, $scope, 'SignalR Connected');

    messagingService.versionInfoObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.version;
            $scope.version = msg.Data;
        }, 0);
    }, $scope, 'Version Info');

    $scope.showReleaseNotes = function (){
        $scope.showingReleaseNotes = true;
        $scope.showingEula = false;
        $scope.showingSupportInfo = false;
    }

    $scope.showSupportInfo = function (){
        $scope.showingSupportInfo = true;
        $scope.showingReleaseNotes = false;
        $scope.showingEula = false;

        
        delete $scope.logMessages;
        $scope.logMessages = window.logMessages.join('\r\n');
    }

    $scope.showEULA = function (){
        $scope.showingReleaseNotes = false;
        $scope.showingEula = true;
        $scope.showingSupportInfo = false;
    }

    if (messagingService.IsSignalRConnected()) {
        messagingService.publish("GetVersionInfo", "");
    }
    else {
        messagingService.signalRConnectedObservable.autoSubscribe(function () {
            messagingService.publish("GetVersionInfo", "");
        }, $scope, 'SignalR connected');
    }

  
}