﻿var constants = angular.module('packsizeConstants', []);

constants.constant('Machines_Constants', {
    machineGroupNotSelected: 'machine-group-not-selected'
});

constants.constant('USER_ROLES', {
    all: '*',
    admin: 'admin',
    operator: 'operator',
    notset: 'notset'
});

constants.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',/*Broadcasted when user is authenticated*/
    loginFailed: 'auth-login-failed',/*Broadcasted when login fails. Only broadcast when a user fails to enter username or password correctly*/
    logoutSuccess: 'auth-logout-success',/*Broadcasted when logout is successful*/
    sessionTimeout: 'auth-session-timeout',/*Broadcasted when authentication session expires*/
    notAuthenticated: 'auth-not-authenticated',/*Broadcasted when user is not authenticated.  Does not trigger login form, instead we ask .Server for an auto-login user*/
    authenticationRequested: 'auth-requested',/*Broadcasted when user is not authenticated AND no auto-login user exists.  This will trigger the login form*/
    notAuthorized: 'auth-not-authorized'/*Broadcasted when user is authenticated, but not authorized for the route*/
});

constants.constant('stateChangeConstants', {
    intendedState: 'stateChangeNextState'
});

constants.constant('Configured_Languages', {
    da_DK: "Dansk",
    de_DE: "Deutsch",
    de_AT: "Deutsch Österreich ",
    de_CH: "Schwiizerdütsch",
    en_US: "English",
    es_ES: "Español Castellano",
    es_MX: "Español México",
    fr_CA: "Français Canadien",
    fr_FR: "Français",
    it_IT: "Italiano",
    hu_HU: "Magyar",
    nl_NL: "Nederlands",
    nl_BE: "Flemish",
    pl_PL: "Polski",
    ro_RO: "Română",
    sv_SE: "Svenska"
});

constants.constant('INTROJS_EVENTS', {
    tellMeAboutThisPage: 'introjs-tell-me-about-this-page'
});

constants.constant('Loglevel_Constants', {
    off: 'Off',
    info: 'Info',
    warn: 'Warning',
    debug: 'Debug',
    error: 'Error'
});

constants.constant('NOTIFICATION_SEVERITY', {
	error: 'Error',
	info: 'Info',
	success: 'Success',
	warning: 'Warning'
});

constants.constant('NOTIFICATION_TYPE', {
	machineGroup: 'MG',
	productionGroup: 'PG',
	system: 'System'
});

constants.constant('NOTIFICATIONS_EVENTS', {
    changed: 'notifications-changed',		/* Any time the notifications array changes */
    producibleCompleted: 'producible-Completed',
    settingsSaved: 'settings-saved'
});