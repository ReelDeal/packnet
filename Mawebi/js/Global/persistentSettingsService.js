﻿var persistentSettingsModule = angular.module('persistentSettings', []);

persistentSettingsModule.factory('persistentStorageService', function ($rootScope, $location, settingsStorage, AUTH_EVENTS) {
    var persistentStorageService = {};
    persistentStorageService.$rootScope = $rootScope;
    persistentStorageService.$location = $location;
    persistentStorageService.settingsStorage = settingsStorage;
    persistentStorageService.autoUpdateFromUrlStorageKey = '8A1B864445904838A500F0EC1BBFAC77_autoUpdatingFromUrl';
    persistentStorageService.variableStorage = '8A1B864445904838A500F0EC1BBFAC77_normalStorage';

    persistentStorageService.updateFromUrl = function () {
        var queryString = $location.search();
        var settingsToUpdate = settingsStorage.getStorageContainer(persistentStorageService.autoUpdateFromUrlStorageKey);

        for (var i = 0; i < settingsToUpdate.length; i++) {
            var curSetting = settingsToUpdate[i];
            var queryStringValue = queryString[curSetting.key];
            if (queryStringValue && queryStringValue !== curSetting.currentValue) {
                curSetting.currentValue = queryStringValue;
                curSetting.updateFunction(curSetting.currentValue);

                settingsToUpdate[i] = curSetting;
            }
        }

        settingsStorage.putStorageContainer(persistentStorageService.autoUpdateFromUrlStorageKey, settingsToUpdate);
    }

    persistentStorageService.stateChangeStart = function (event, toState, toParams, fromState, fromParams) {
        persistentStorageService.updateFromUrl();
    };

    persistentStorageService.stateChangeSuccess = function (event, toState, toParams, fromState, fromParams) {
    };

    persistentStorageService.start = function () {
        $rootScope.$on('$stateChangeStart', persistentStorageService.stateChangeStart);
        $rootScope.$on('$stateChangeSuccess', persistentStorageService.stateChangeSuccess);
        $rootScope.$on(AUTH_EVENTS.logoutSuccess, function() {
            settingsStorage.purge();
        });
    }

    persistentStorageService.add = function (key, value) {
        var storageObj = settingsStorage.getStorageObject(persistentStorageService.variableStorage, key, value, null);
        settingsStorage.addToContainer(storageObj);
    }

    persistentStorageService.get = function (key) {
        return settingsStorage.getValueFromStorageContainer(persistentStorageService.variableStorage, key);
    }

    persistentStorageService.addUrlParam = function (urlParamName, updateFunction) {
        var storageObj = settingsStorage.getStorageObject(persistentStorageService.autoUpdateFromUrlStorageKey, urlParamName, $location.search()[urlParamName], updateFunction);
        settingsStorage.addToContainer(storageObj);
    }

    persistentStorageService.getUrlParam = function (urlParamName) {
        var storageObj = settingsStorage.getValueFromStorageContainer(persistentStorageService.autoUpdateFromUrlStorageKey, urlParamName);
        return storageObj ? storageObj.value : undefined;
    }

    return persistentStorageService;
});

persistentSettingsModule.service('settingsStorage', function () {
    var me = this;
    me.backingStorage = [];

    me.purge = function() {
        me.backingStorage = [];
    }

    me.getStorageObject = function (storageName, key, value, updateFunction) {
        var obj = {};
        obj.storageName = storageName;
        obj.key = key;
        obj.value = value;
        obj.updateFunction = updateFunction;
        return obj;
    }

    me.getValueFromStorageContainer = function (storageName, keyName) {
        var storage = me.getStorageContainerAsEnumerable(storageName);
        return storage.FirstOrDefault(undefined, function (arrayItem) {
            return arrayItem.key == keyName;
        });
    }

    me.getStorageContainer = function (storageName) {
        var container = me.backingStorage[storageName];
        return container ? container.ToArray() : [];
    };

    me.getStorageContainerAsEnumerable = function (storageName) {
        var container = me.backingStorage[storageName];
        return container ? container : Enumerable.From([]);
    }

    me.putStorageContainer = function (storageName, container) {
        if (!container.GetEnumerator) {
            container = Enumerable.From(container);
        }
        me.backingStorage[storageName] = container;
    }

    me.addToContainer = function (value) {
        var storage = me.getStorageContainerAsEnumerable(value.storageName);
        var array = storage.ToArray();
        var indexesToRemove = [];
        if (storage.Any(function (storageItem) { return storageItem.key === value.key; })) {
            for (var i = 0; i < array.length; i++) {
                if (!array[i] || array[i].key === value.key) {
                    indexesToRemove.push(i);
                }
            }

            for (var j = 0; j < indexesToRemove.length; j++) {
                array = array.splice(indexesToRemove[j], 1);
            }
        }

        array.push(value);
        storage = Enumerable.From(array);

        me.backingStorage[value.storageName] = storage;
    }

    return me;
});