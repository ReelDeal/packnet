﻿var helperModule = angular.module('helperModule', []);

helperModule.factory('helperService', function () {
    //LINQ
    function getItemByField(enumerable, value, field) {
        return Enumerable
            .From(enumerable)
            .Where(function (item) {
                return item[field] === value;
            })
            .FirstOrDefault();
    };

    function getItemById(enumerable, value) {
        return this.getItemByField(enumerable, value, "Id");
    };

    //RegEx
    function getAllMatchingValues(string, regEx) {
        //Important! The regEx must have the global modifier active
        var result = [];
        var match;

        do {
            match = regEx.exec(string);

            if (match) {
                result.push(match[2]);
            }
        } while (match);

        return result;
    };

    function getValuesBetweenCurlyBrackets(string) {
        var regEx = /([{])((?:(?=(\\?))\3.)*?)([}])/g;

        return getAllMatchingValues(string, regEx);
    };

    function getValuesBetweenDoubleQuotes(string) {
        var regEx = /(["])((?:(?=(\\?))\3.)*?)(["])/g;

        return getAllMatchingValues(string, regEx);
    };

    function isAlphanumeric(string) {
        var regEx = /^[a-zA-Z0-9]*$/;

        return regEx.exec(string);
    }

    //UI-Grid
    function hideExpandableIcon(gridId, rowIndex, hide) {
        var sideContainerSelector = "#" + gridId + " [container-id='side'] .ui-grid-canvas .ui-grid-row";
        var bodyContainerSelector = "#" + gridId + " [container-id*='body'] .ui-grid-canvas .ui-grid-row";

        //When removing rows, the previous class assignments are kept, clearing them out
        $(sideContainerSelector)[rowIndex].className = $(sideContainerSelector)[rowIndex].className.replace(" ui-grid-hide-expandable", "");
        $(bodyContainerSelector)[rowIndex].className = $(bodyContainerSelector)[rowIndex].className.replace(" ui-grid-hide-expandable", "");

        //Applies the custom CSS class to hide the expandable icon
        if (hide) {
            $(sideContainerSelector)[rowIndex].className += " ui-grid-hide-expandable";
            $(bodyContainerSelector)[rowIndex].className += " ui-grid-hide-expandable";
        }
    }

    //Public
    return {
        linq: {
            getItemByField: getItemByField,
            getItemById: getItemById
        },
        regEx: {
            getValuesBetweenCurlyBrackets: getValuesBetweenCurlyBrackets,
            getValuesBetweenDoubleQuotes: getValuesBetweenDoubleQuotes,
            isAlphanumeric: isAlphanumeric
        },
        uiGrid: {
            hideExpandableIcon: hideExpandableIcon
        }
    };
});