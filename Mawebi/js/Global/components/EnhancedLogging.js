﻿angular.module('packNetEnhancedLogging', []).provider('logEnhancer', function () {
    this.loggingPattern = '%s - %s: ';

    this.$get = function () {
        var loggingPattern = this.loggingPattern;
        return {
            enhanceAngularLog: function ($log, $rootScope, Loglevel_Constants) {
                $log.enabledContexts = [];
                window.logMessages = [];
                $log.whiteListedLoggers = [];
                $log.getInstance = function (context) {
                    var retObj = {
                        log: enhanceLogging($log.log, 'log', context, loggingPattern),
                        info: enhanceLogging($log.info, 'info', context, loggingPattern),
                        warn: enhanceLogging($log.warn, 'warn', context, loggingPattern),
                        debug: enhanceLogging($log.debug, 'debug', context, loggingPattern),
                        error: enhanceLogging($log.error, 'error', context, loggingPattern)
                    };

                    window.addWhiteListedLogger = function (loggerName) {
                        $log.whiteListedLoggers[loggerName] = true;
                        $log.whiteListedLoggers.push(true);
                    };

                    window.clearWhiteListedLogger = function (loggerName) {
                        $log.whiteListedLoggers = [];
                    };

                    return retObj;
                };
                var enabled = true;
                function isEnabledForLogging(logLevel, context) {
                    //Why does this need to be so complicated? 
                    //No comments on how to use this thing. Console logs never get shown in the debugger window.
                    //Just log to the damn console window..
                    //Also, why would we ever hide console errors?

                    return enabled;

                    if ($log.whiteListedLoggers.length > 0) {
                        return $log.whiteListedLoggers[context] ? $log.whiteListedLoggers[context] : false;
                    }

                    var selectedLogLevel = $rootScope.preferences ? $rootScope.preferences.LogLevel : Loglevel_Constants.info;
                    if (selectedLogLevel) {
                        if (selectedLogLevel == Loglevel_Constants.off) {
                            return false;
                        }

                        if (selectedLogLevel == Loglevel_Constants.debug) {
                            return true;
                        }

                        if (selectedLogLevel == Loglevel_Constants.info) {
                            if (logLevel == Loglevel_Constants.info || logLevel == Loglevel_Constants.warn || logLevel == Loglevel_Constants.error) {
                                return true;
                            }
                        }

                        if (selectedLogLevel == Loglevel_Constants.warn) {
                            if (logLevel == Loglevel_Constants.warn || logLevel == Loglevel_Constants.error) {
                                return true;
                            }
                        }

                        if (selectedLogLevel == Loglevel_Constants.error) {
                            if (logLevel == Loglevel_Constants.error) {
                                return true;
                            }
                        }
                    }

                    return false;
                };

                function recordMessage(logMessage) {
                    var numMessages = $rootScope.preferences ? $rootScope.preferences.MaximumNumberOfLogMessagesToRecord : 4096;
                    if (window.logMessages.length > numMessages) {
                        window.logMessages = window.logMessages.splice(0, 1);
                    }
                    window.logMessages.push(logMessage);
                }

                function enhanceLogging(loggingFunc, logLevel, context, loggingPattern) {
                    return function () {
                        var modifiedArguments = [].slice.call(arguments);
                        //modifiedArguments[0] = [sprintf(loggingPattern, moment().format(), context)] + modifiedArguments[0];
                        if (enabled)
                            console.log(modifiedArguments[0]);

                        //recordMessage(modifiedArguments[0]);
                        ////if (contextEnabled && contextEnabled[logLevel]) {
                        //if (isEnabledForLogging(logLevel, context)) {
                        //    loggingFunc.apply(null, modifiedArguments);
                        //}
                    };
                }
            }
        };
    };
});