﻿var designPreviewModule = angular.module('designPreviewModule', []);

designPreviewModule.factory('designPreviewService', function () {

    function drawPreview(previewData, container, containerWidth, containerHeight) {
       drawHtmlCanvasPreview(previewData, container, containerWidth, containerHeight);
        //if (!Detector.webgl) {
        //    drawHtmlCanvasPreview(previewData, container, containerWidth, containerHeight);
        //}
        //else {
        //    drawWebGLPreview(previewData, container, containerWidth, containerHeight);
        //}
    };

    function drawWebGLPreview(previewData, container, containerWidth, containerHeight) {

        container.empty();

        var scene = new THREE.Scene();

        var fov = 45;
        var previewWidth = 0;
        var previewHeight = 0;

        Enumerable.From(previewData.Lines).ForEach(function (line) {

            if (line.EndCoordinate.X > previewWidth) {
                previewWidth = line.EndCoordinate.X;
            }
            if (line.EndCoordinate.Y > previewHeight) {
                previewHeight = line.EndCoordinate.Y;
            }
        });

        // find the largest distance given the camera fov and the preview dimensions, used to set the camera distance and scale the dashed lines
        var distH = previewHeight * 2 / 2 / Math.tan(fov / 2);
        var distW = previewWidth * 2 / 2 / Math.tan(fov / 2);

        var dist = Math.max(distH, distW);

        var dashLineScale = dist / 200;

        var lineMaterial = new THREE.LineBasicMaterial({
            color: 0x53534b
        });
        var creaseLineMaterial = new THREE.LineDashedMaterial({
            color: 0x0000FF,
            dashSize: dashLineScale,
            gapSize: dashLineScale
        });
        var perforationLineMaterial = new THREE.LineDashedMaterial({
            color: 0xed15ea,
            dashSize: dashLineScale,
            gapSize: dashLineScale
        });

        Enumerable.From(previewData.Lines).ForEach(function (line) {
            var geometry = new THREE.Geometry();

            geometry.vertices.push(new THREE.Vector3(line.StartCoordinate.X, line.StartCoordinate.Y, 0));
            geometry.vertices.push(new THREE.Vector3(line.EndCoordinate.X, line.EndCoordinate.Y, 0));

            var material = lineMaterial;

            if (line.Type == 1) {
                material = lineMaterial;
            }
            else if (line.Type == 0) {
                material = creaseLineMaterial;
            }
            else if (line.Type == 2) {
                material = perforationLineMaterial;
            }            

            var threeLine = new THREE.Line(geometry, material);
            geometry.computeLineDistances();

            scene.add(threeLine);

            var tscale = 2500;

            var options = { font: 'arial', weight: 'normal', style: 'normal', size: 48, curveSegments: 300 };

            if (line.Label != null) {

                if (line.StartCoordinate.X != line.EndCoordinate.X) {
                    var xLineLength = line.EndCoordinate.X - line.StartCoordinate.X;
                    var textShapes = THREE.FontUtils.generateShapes(line.Label, options);
                    var text = new THREE.ShapeGeometry(textShapes);
                    var textMesh = new THREE.Mesh(text, new THREE.MeshBasicMaterial({ color: 0x000000 }));
                    textMesh.scale.set(dist / tscale, dist / tscale, dist / tscale);
                    var box = new THREE.Box3().setFromObject(textMesh);
                    var mid = (box.max.x / 2);                    
                    if (box.max.x < xLineLength) {
                        textMesh.position.set(line.StartCoordinate.X + ((xLineLength / 2) - mid), line.StartCoordinate.Y + dist / 250, 0);
                        scene.add(textMesh);
                    }
                }

                if (line.StartCoordinate.Y != line.EndCoordinate.Y) {
                    var yLineLength = line.EndCoordinate.Y - line.StartCoordinate.Y;
                    textShapes = THREE.FontUtils.generateShapes(line.Label, options);
                    text = new THREE.ShapeGeometry(textShapes);
                    textMesh = new THREE.Mesh(text, new THREE.MeshBasicMaterial({ color: 0x000000 }));
                    textMesh.scale.set(dist / tscale, dist / tscale, dist / tscale);
                    box = new THREE.Box3().setFromObject(textMesh);
                    mid = (box.max.x / 2);                    
                    if (box.max.x < yLineLength) {
                        textMesh.position.set(line.StartCoordinate.X + dist / 250, line.EndCoordinate.Y - ((yLineLength / 2) - mid), 0);
                        textMesh.rotateZ(Math.PI / -2);
                        scene.add(textMesh);
                    }
                }
            }
        });

        // set the scene size
        var WIDTH = containerWidth;
        var HEIGHT = containerHeight;

        // set some camera attributes
        var VIEW_ANGLE = fov,
          ASPECT = WIDTH / HEIGHT,
          NEAR = 0.1,
          FAR = 100000;

        var camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT, NEAR, FAR);

        camera.fov = fov;
        camera.position.set(previewWidth / 2, previewHeight / 2, dist);
        camera.updateProjectionMatrix();

        var renderer = new THREE.WebGLRenderer({ alpha: true });
        //renderer.setClearColor(0x111111);
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(WIDTH, HEIGHT);

        container.append(renderer.domElement);
        renderer.render(scene, camera);
    };

    function drawHtmlCanvasPreview(previewData, container, containerWidth, containerHeight) {

        var canvas = document.createElement('canvas');

        canvas.width = containerWidth;
        canvas.height = containerHeight;

        var ctx = canvas.getContext("2d");

        ctx.clearRect(0, 0, canvas.width, canvas.height);

        var previewWidth = 0;
        var previewHeight = 0;

        Enumerable.From(previewData.Lines).ForEach(function (line) {
            if (line.EndCoordinate.X > previewWidth) {
                previewWidth = line.EndCoordinate.X;
            }
            if (line.EndCoordinate.Y > previewHeight) {
                previewHeight = line.EndCoordinate.Y;
            }
        });

        var scaleX = canvas.width / previewWidth;
        var scaleY = canvas.height / previewHeight;

        var min = Math.min(scaleX, scaleY);

        ctx.lineWidth = 1 / min;
        ctx.scale(min, min);

        ctx.beginPath();

        // remove these comments to re-enable ugly background color
        //ctx.fillStyle = '#cccccc';
        //ctx.fillRect(0, 0, previewWidth, previewHeight);

        Enumerable.From(previewData.Lines).ForEach(function (line) {

            ctx.beginPath();

            if (line.Type == 1) {
                ctx.strokeStyle = '#000000';
                ctx.setLineDash([1, 0]);
            }
            else if (line.Type == 0) {
                ctx.strokeStyle = '#0000ff';
                ctx.setLineDash([ctx.lineWidth]);
            }
            else if (line.Type == 2) {
                ctx.strokeStyle = '#ed15ea';
                ctx.setLineDash([4 * ctx.lineWidth, ctx.lineWidth]);
            }
            
            if (line.Label != null) {
                ctx.font = "4px arial";
                ctx.textBaseLine = "hanging";
                ctx.textAlign = "center";

                if (line.StartCoordinate.X != line.EndCoordinate.X) {
                    ctx.save();
                    var xLineLength = line.EndCoordinate.X - line.StartCoordinate.X;
                    var xText = ctx.measureText(line.Label);
                    ctx.translate(line.StartCoordinate.X + (xLineLength / 2), line.StartCoordinate.Y - 3 / min);
                    ctx.scale(3 / min, 3 / min);

                    if (xText.width * (3 / min) < xLineLength) {
                        ctx.fillText(line.Label, 0, 0);
                    }
                    ctx.restore();
                }

                if (line.StartCoordinate.Y != line.EndCoordinate.Y) {
                    ctx.save();
                    var yLineLength = line.EndCoordinate.Y - line.StartCoordinate.Y;
                    ctx.translate(line.StartCoordinate.X + 3 / min, line.StartCoordinate.Y + (yLineLength / 2));
                    ctx.scale(3 / min, 3 / min);
                    ctx.rotate(Math.PI / 2);
                    var yText = ctx.measureText(line.Label);
                    if (yText.width * (3 / min) < yLineLength) {
                        ctx.fillText(line.Label, 0, 0);
                    }

                    ctx.restore();
                }
            }
            ctx.moveTo(line.StartCoordinate.X, line.StartCoordinate.Y);
            ctx.lineTo(line.EndCoordinate.X, line.EndCoordinate.Y);
            ctx.stroke();
        });
        container.empty();
        container.append(canvas);
    };

    //Public
    return {
        draw: {
            drawPreview: drawPreview
        }
    };
});