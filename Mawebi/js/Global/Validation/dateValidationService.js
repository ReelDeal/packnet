﻿function dateValidationService() {

    this.validateSearchDates = function (startDate, endDate){

        var now = new Date();
        return (startDate < endDate) && (startDate <= now);
    };

}

app.service('dateValidationService', dateValidationService);
