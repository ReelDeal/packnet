﻿var introJsModule = angular.module('packsizeIntroJsModule', []);

introJsModule.service('introJsService', function ($q) {
    var me = this;

    this.storage = {};
    this.storage.logger = {};// $log.getInstance('IntroJS Service');}
    this.storage.oncompleteCallback = undefined;
    this.storage.onexitCallback = undefined;
    this.storage.onChangeCallback = undefined;
    this.storage.onBeforeChangeCallback = undefined;
    this.storage.onAfterChangeCallback = undefined;
    this.storage.onChangeCallback = undefined;
    this.storage.tellMeAboutThisPageCallback = undefined;

    this.storage.guide = {};

    this.wireupIntroEvents = function () {

        me.storage.guide.oncomplete(function () {
            if (me.storage.oncompleteCallback) {
                try {
                    me.storage.oncompleteCallback();
                }
                catch (err) {
                    me.storage.logger.error(err);
                }
            }
        });

        me.storage.guide.onexit(function () {
            if (me.storage.onexitCallback) {

                try {
                    me.storage.onexitCallback();
                }
                catch (err) {
                    me.storage.logger.error(err);
                }
            }
        });

        me.storage.guide.onchange(function (targetElement) {
            if (me.storage.onChangeCallback) {

                try {
                    me.storage.onChangeCallback(targetElement);
                }
                catch (err) {
                    me.storage.logger.error(err);
                }
            }
        });

        me.storage.guide.onbeforechange(function (targetElement) {
            if (me.storage.onBeforeChangeCallback) {

                try {
                    me.storage.onBeforeChangeCallback(targetElement);
                }
                catch (err) {
                    me.storage.logger.error(err);
                }
            }
        });

        me.storage.guide.onafterchange(function (targetElement) {
            if (me.storage.onAfterChangeCallback) {

                try {
                    me.storage.onAfterChangeCallback(targetElement);
                }
                catch (err) {
                    me.storage.logger.error(err);
                }
            }

        });
    };

    this.create = function (targetElement) {
        if (targetElement) {
            me.storage.guide = new introJs(targetElement);
        }
        else {
            me.storage.guide = new introJs();
        }
    }

    this.start = function (steps) {
        me.wireupIntroEvents();
        me.setOptions(steps);
        me.storage.guide.start();
    };

    this.setOptions = function (steps) {
        var options = me.getOptions(steps);
        me.storage.guide.setOptions(options);
    }

    this.getOptions = function (steps) {
        var local = me.storage.IntroOptions;
        local.steps = steps;

        return local;
    }

    this.goToStep = function (step) {
        me.storage.guide.goToStep(step);
    }

    this.nextStep = function () {
        me.storage.guide.nextStep();
    }

    this.previousStep = function () {
        me.storage.guide.previousStep();
    }

    this.exit = function () {
        me.storage.guide.exit();
    }

    this.refresh = function () {
        me.storage.guide.refresh();
    }

    this.oncomplete = function (callback) {
        me.storage.oncompleteCallback = callback;
    }

    this.onexit = function (callback) {
        me.storage.onexitCallback = callback;
    }

    this.onchange = function (callback) {
        me.storage.onChangeCallback = callback;
    }

    this.onbeforechange = function (callback) {
        me.storage.onBeforeChangeCallback = callback;
    }

    this.onafterchange = function (callback) {
        me.storage.onAfterChangeCallback = callback;
    }

    this.storage.IntroOptions = {
        'nextLabel': 'Next',
        'prevLabel': 'Previous',
        'skipLabel': 'Close',
        'doneLabel': 'Done',
        'tooltipPosition': 'right',
        'exitOnEsc': true,
        'exitOnOverlayClick': true,
        'showStepNumbers': true,
        'keyboardNavigation': true,
        'showButtons': true,
        'showBullets': true,
        'showProgress': true,
        'scrollToElement': true,
        'overlayOpacity': 1,
        'disableInteraction': true
    }

    this.tellMeAboutThisPage = function (scope, callback, autoStart) {

        if (!scope || !scope.$on) {
            throw "Scope is required";
        }

        autoStart = typeof autoStart !== 'undefined' ? autoStart : true;

        var obj = {
            method: callback,
            autoStart: autoStart
        };

        me.storage.tellMeAboutThisPageCallbacks = obj;

        scope.$on("$destroy", function () {
            me.storage.tellMeAboutThisPageCallbacks = undefined;
            me.storage.onAfterChangeCallback = undefined;
            me.storage.onBeforeChangeCallback = undefined;
            me.storage.onChangeCallback = undefined;
            me.storage.onexitCallback = undefined;
            me.storage.oncompleteCallback = undefined;
        });
    }

    this.invokeTellMeAboutThisPage = function () {
        if (me.storage.tellMeAboutThisPageCallbacks) {
            try {

                var callback = me.storage.tellMeAboutThisPageCallbacks;

                // Use $q.when when the method called may or may not be a promise
                $q.when(callback.method(), function (steps) {
                    if (callback.autoStart === true) {
                        if (steps && angular.isArray(steps)) {
                            me.start(steps);
                        }
                        else {
                            me.start();
                        }
                    }
                });
            }
            catch (err) {
                console.error(err);
            }
        }
    }

    this.destroy = function () {
        me.storage = {};
        me.storage = undefined;
    };

    //Create the first instance
    this.create();

    return this;
});