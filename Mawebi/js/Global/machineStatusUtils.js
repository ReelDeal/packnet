﻿function getIconForMachineStatus(status) {
    if (!status)
        return "";

    switch (status) {
        case 'Disconnected':
            return ["img/red_icon.png", "Disconnected"];
        case 'Attached':
            return ["img/connect.png", "Attached"];
        case 'Detached':
            return ["img/disconnect.png", "Detached"];
        case 'Paused':
            return ["img/GreenDot.png", "Paused"];
        case 'Initializing':
            return ["img/recycle.png", "Initializing"];
        default:
            return ["", status];

    }
}