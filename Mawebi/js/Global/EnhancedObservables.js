﻿angular.module('packNetEnhancedObservables', []).provider('observableEnhancer', function () {
    this.$get = function () {
        return {
            enhanceRxObservables: function ($log) {
                var observablesCollection = [];
                var log = $log.getInstance('Enhanced Observable');

                Rx.Observable.prototype.autoSubscribe = function (observerOnNext, scope, subscriptionName, onError, onCompleted){

                    if (!scope) {
                        console.trace();
                        throw "Invalid usage: scope is required to autoSubscribe";
                    }

                    if (!subscriptionName || subscriptionName == "") {
                        console.trace();
                        throw "Invalid usage: subscriptionName is required to autoSubscribe";
                    }

                    var oldObserverNext = observerOnNext;
                    var name = scope.controllerName;

                    observerOnNext = function (data) {
                        try {
                            if (data) {
                                log.debug('observable onNext called for controller ' + name + " message type " + data.MessageType);
                            }
                            else {
                                log.debug('observable onNext called for controller ' + name);
                            }
                            console.time('observable callback');
                            oldObserverNext(data);
                            console.timeEnd('observable callback');
                        }
                        catch (e) {
                            log.error("The following exception bubbled out of $timeout");
                            log.error(e);
                            throw e;
                        }
                    };

                    var observable = this.subscribe(observerOnNext, onError, onCompleted);

                    if (scope.$id) {

                        if (!observablesCollection[scope.$id]) {
                            observablesCollection[scope.$id] = [];

                            scope.$on('$destroy', function () {
                                log.debug('Scope Destroy called for ' + name);

                                for (var i = 0; i < observablesCollection[scope.$id].length; i++) {
                                        log.debug('Destroying observable ' + (subscriptionName ? "'" + subscriptionName + "' Observable" : i) + " for " + name);
                                    var obs = observablesCollection[scope.$id][i];
                                    obs.dispose();
                                }

                                observablesCollection[scope.$id] = undefined;
                            });
                        }

                        observablesCollection[scope.$id].push(observable);
                    }

                    return observable;
                };
            }
        };
    };
});

