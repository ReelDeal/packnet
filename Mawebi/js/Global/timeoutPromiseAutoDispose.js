﻿angular.module('packNetEnhancedTimeout', ['packNetEnhancedLogging']).provider('timeoutEnhancer', function () {

    this.$get = function ($log, $timeout, $interval) {
        var timeoutPromises = [];
        var intervalPromises = [];
      

        return {
            enhanceAngularTimeout: function () {
                function setupPromiseDisposer(scope, promiseContainer, service) {
                    if (!promiseContainer[scope.$id]) {
                        var myLog = $log.getInstance('timeoutEnhancer');
                        promiseContainer[scope.$id] = [];

                        scope.$on('$destroy', function () {
                            console.debug("timeoutEnhancer: Cancelling " + promiseContainer[scope.$id].length + " promises for " + scope.controllerName);
                            for (var i = 0; i < promiseContainer[scope.$id].length; i++) {
                                var cancelled = service.cancel(promiseContainer[scope.$id][i]);
                                console.debug("cancelled status " + cancelled);
                                delete promiseContainer[scope.$id][i];
                                promiseContainer[scope.$id][i] = undefined;
                            }

                            promiseContainer[scope.$id] = undefined;
                        });
                    }
                }
                
                $interval.getInstance = function (scope){
                    var instance = function (fn, tick, times, invokeApply) {
                        var promise = $interval(fn, tick, times, invokeApply);

                        setupPromiseDisposer(scope, intervalPromises, $interval);

                        intervalPromises[scope.$id].push(promise);

                        return promise;
                    };

                    instance.cancel = $interval.cancel;

                    return instance;
                }

                $timeout.getInstance = function (scope){
                    var instance = function (fn, delay, invokeApply) {
                        var promise = $timeout(fn, delay, invokeApply);

                        setupPromiseDisposer(scope, timeoutPromises, $timeout);

                        timeoutPromises[scope.$id].push(promise);
                        promise.finally(function () {
                            var index = timeoutPromises[scope.$id].indexOf(promise);
                            timeoutPromises[scope.$id].splice(index, 1);
                            console.log("$timeout finally: promise count for scope " + scope.$id + " = " + timeoutPromises[scope.$id].length);
                        });
                        return promise;
                    };

                    instance.cancel = $timeout.cancel;
                    console.log("$timeout totals: " + Enumerable.From(timeoutPromises).Sum(function (a) { 
						if(!a)
						{
							return 0; //TODO: This is not good! The timeouts in the array are not being removed, just set to undefined ಠ_ಠ
						}
						return a.length; }));
                    return instance;
                }
            }
        };
    };
});