﻿function machineService($log, $rootScope, $state, messagingService) {
    $rootScope.machines = [];
    $rootScope.currentMachine = {};
    $rootScope.currentMachineId = "";

    var serviceGetMachineInfoObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ServiceGetMachineInfoResponse"));

    var serviceGetMachinesObservable = messagingService.messageSubject
        .where(messagingService.filterObs("ServiceGetMachinesResponse"));

    var allMachinesSubject = new Rx.Subject();
    var allMachinesObservable = Rx.Observable.create(function (observer) {
        var disposable = allMachinesSubject.subscribe(function (allMachines) {
            observer.onNext(allMachines);
        });

        return disposable;
    });

    var machineInfoSubject = new Rx.Subject();
    var machineInfoObservable = Rx.Observable.create(function (observer) {
        var disposable = machineInfoSubject.subscribe(function (machine) {
            observer.onNext(machine);
        });

        return disposable;
    });

    serviceGetMachinesObservable.autoSubscribe(function (msg) {
            $rootScope.machines = msg.Data;
               allMachinesSubject.onNext($rootScope.machines);
           }, $rootScope, 'Machine Service Machines');

    serviceGetMachineInfoObservable.autoSubscribe(function (msg) {
            $rootScope.currentMachine = msg.Data;
            machineInfoSubject.onNext($rootScope.currentMachine);
        }, $rootScope, 'Machine Service Machine Info');

    function requestData() {
        messagingService.publish("ServiceGetMachines", "");
    };

    function requestInfo(machineId) {
        messagingService.publish("ServiceGetMachineInfo", machineId);
    }

    function getLocalMachine(machineId) {
        return Enumerable.From($rootScope.machines).FirstOrDefault({}, function (m) { return m.Id == machineId; });
    }

    function setCurrentMachine(machineId) {
        var localMachine = getLocalMachine(machineId);
        if (localMachine) {
            $rootScope.currentMachineId = machineId;
            requestInfo($rootScope.currentMachineId);
        }
    };

    function getCurrentMachine() {
        if ($rootScope.currentMachine) {
            return $rootScope.currentMachine;
        }
        return {
            NumberOfLongHeads: 3
        };
    }

    if (messagingService.IsSignalRConnected()) {
        requestData();
    }

    messagingService.signalRConnectedObservable.autoSubscribe(requestData, $rootScope, 'SignalR Connected');

    return {
        getMachines: function () {
            if ($rootScope.machines) {
                    return $rootScope.machines.slice();
            }
            return [];
        },
        getCurrentMachineId: function () {
            return $rootScope.currentMachineId;
        },
        setCurrentMachine: setCurrentMachine,
        getCurrentMachine: getCurrentMachine,
        requestMachineInfo: requestInfo,
        requestData: requestData,
        allMachinesObservable: allMachinesObservable,
        machineInfoObservable: machineInfoObservable,
    }
}
app.factory('machineService', machineService);