function MachineServiceCtrl($log, $rootScope, $scope, $timeout, alertService, machineService, messagingService, translationService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.machine = machineService.getCurrentMachine();
    $scope.inServiceMode = false;
    $scope.awaitingUpdate = false;

    var machineServiceResponse = messagingService.messageSubject
        .where(messagingService.filterObs("MachineServiceResponse"));

    machineService.machineInfoObservable
        .autoSubscribe(function (data) {
            if (data.Id != machineService.getCurrentMachineId()) {
                return;
            }

            $timeout(function () {
                
                delete $scope.machine;
                $scope.machine = data;
            }, 0, true);
        }, $scope, 'Machine Service Machine Info');

    machineServiceResponse
        .autoSubscribe(function (msg) {
            if (msg.Data != $scope.machine.Id) {
                return;
            }

            switch (msg.Result) {
                case 'Success':
                    switch (msg.Message) {
                        case 'CalibrationCompleted':
                            alertService.addSuccess(translationService.translate('MachineService.Message.MachineCalibrationCompleted'), 5000);
                            break;
                        case 'CalibrationStarted':
                            alertService.addSuccess(translationService.translate('MachineService.Message.MachineCalibrationStarted'), 5000);
                            break;
                        case 'CommandSend':
                            alertService.addSuccess(translationService.translate('MachineCommand.Message.SendSuccess'), 5000);
                            break;
                        case 'PauseCommandSend':
                            alertService.addSuccess(translationService.translate('MachineService.Message.PauseCommandSendSuccess'), 5000);
                            break;
                        case 'ServiceCommandSend':
                            alertService.addSuccess(translationService.translate('MachineService.Message.ServiceCommandSendSuccess'), 5000);
                            break;
                    }

                    break;
                case 'Fail':
                    var additionalInfo = msg.Message != null ? msg.Message.split(",") : [""];

                    switch (additionalInfo[0]) {
                        case 'Calibration':
                            alertService.addError(translationService.translate('MachineService.Message.MachineCalibrationFailed'), 5000);
                            break;
                        case 'CommandSend':
                            alertService.addError(translationService.translate('MachineService.Message.CommandFailed'), 5000);
                            break;
                        case 'MachineIdNotExists':
                            alertService.addError(translationService.translate('MachineService.Message.MachineIdNotExists', additionalInfo[1]), 5000);
                            break;
                        default:
                            alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'), 5000);
                            $log.info(msg);
                    }

                    break;
            }

            $scope.awaitingUpdate = false;

        }, $scope, 'Machine Service Perform Command');

    $scope.publishCommand = function (type, part, number, toolState) {
        if ($scope.machine) {
            messagingService.publish(
                "ServicePerformCommand",
                {
                    ToolState: toolState,
                    MachineId: $scope.machine.Id,
                    Number: number,
                    Type: type,
                    Part: part,
                });
            $scope.awaitingUpdate = true;
        }
    }

    window.onbeforeunload = function () {
        if ($scope.machine.MachineStatus == 'MachineService') {
            return translationService.translate("MachineService.Message.StillInService");
        }
        return $scope.undefined;
    };
    
    var locationChange = $scope.$on('$locationChangeStart', function (event) {
        if ($scope.machine.MachineStatus == 'MachineService') {
            if (!confirm(translationService.translate("MachineService.Message.StillInServiceConfirm"))) {
                event.preventDefault();
                return;
            }

            window.onbeforeunload = $scope.undefined;
            locationChange();
        }
    });

    var stateChange = $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            if ($scope.machine.MachineStatus == 'MachineService') {
                if (!confirm(translationService.translate("MachineService.Message.StillInServiceConfirm"))) {
                    event.preventDefault();
                    return;
                }

                window.onbeforeunload = $scope.undefined;
                stateChange();
            }
        });

    $scope.$on('$destroy', function () {
        locationChange();
        stateChange();
    });
};