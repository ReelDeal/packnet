function MachineServiceMainCtrl($scope, $state, $timeout, machineService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.machines = machineService.getMachines();

    machineService.allMachinesObservable
           .autoSubscribe(function (data) {
        $timeout(function () {
            
            delete $scope.machines;
            $scope.machines = data;
        }, 0, true);
    }, $scope, 'Machine Service Main Machines');
    
    $scope.openServiceConsole = function (machine){
        machineService.setCurrentMachine(machine.Id);
        machineService.requestMachineInfo(machine.Id);

        if (machine.MachineType == "Em") {
            $state.go("EmMachineService", { machineId: machine.Id });
        } else if (machine.MachineType == "Fusion") {
            $state.go("FusionMachineService", { machineId: machine.Id });
        }
    };

    machineService.requestData();
};

//Directives
app.directive('machineServiceMachineStateManager', function () {
    {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            templateUrl: 'partials/MachineService/MachineServiceMachineStateHandler.html'
        };
    }
});
