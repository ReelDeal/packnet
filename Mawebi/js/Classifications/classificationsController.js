function ClassificationsController($location, $log, $scope, $state, $timeout, alertService, introJsService, messagingService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.floatingPointRegex = /^(\d+\.?\d*|\.\d+)$/;
    $scope.classifications = [];
    $scope.selectedClassification = {};
    $scope.selectedClassification.IsNewClassification = true;

    var awaitingUpdate = false;

    $scope.$on('$locationChangeSuccess', function () {
        if($scope.selectedClassification && $scope.selectedClassification.IsNewClassification == false && $location.path() == '/Classifications') {
            $scope.cancelNewEditClassification();
        }
    });

    messagingService.classificationsObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            if ($scope.gridApi.selection.getSelectedRows().length === 0) {
                delete $scope.classifications;
                $scope.classifications = msg.Data;
            }
        }, 0, true);
    }, $scope, 'Classifications');

    messagingService.classificationCreatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('Classification.Label.NameSingular'), $scope.classificationBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            if (msg.Message === "Alias")
                alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Classification.Label.NameSingular'), $scope.classificationBeingSaved.Alias));
            else if(msg.Message === "Number")
                alertService.addError(translationService.translate('Classification.Message.NumberExists', $scope.classificationBeingSaved.Number));
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'));
                $log.debug(msg);
            }
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('Classification.Label.NameSingular'), $scope.classificationBeingSaved.Alias), 5000);
        }
    }, $scope, 'Classification Created');

    messagingService.classificationUpdatedObservable.autoSubscribe(function (msg) {
        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Classification.Label.NameSingular'), $scope.classificationBeingSaved.Alias));
        }
        else if (msg.Result === "Exists") {
            if (msg.Message === "Alias")
                alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Classification.Label.NameSingular'), $scope.classificationBeingSaved.Alias));
            else if (msg.Message === "Number")
                alertService.addError(translationService.translate('Classification.Message.NumberExists', $scope.classificationBeingSaved.Number));
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'));
                $log.debug(msg);
            }
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Classification.Label.NameSingular'), $scope.classificationBeingSaved.Alias), 5000);
        }
    }, $scope, 'Classification Updated');

    messagingService.classificationDeletedObservable.autoSubscribe(function (msg) {
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.DeleteFailed', translationService.translate('Classification.Label.NameSingular'), msg.Data.Alias));
        }
        else if (msg.Result === "ClassificationCurrentlyLoadedOnMachine") {
            alertService.addError(translationService.translate('Common.Message.DeleteCurrentlyLoaded', translationService.translate('Classification.Label.NameSingular'), msg.Data.Alias));
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', translationService.translate('Classification.Label.NameSingular'), msg.Data.Alias), 5000);
            delete $scope.classificationBeingSaved;
            $scope.classificationBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            delete $scope.selectedClassification;
            $scope.selectedClassification = {};
            $scope.selectedClassification.IsNewClassification = true;
        }
        requestData();
    }, $scope, 'Machine Group Deleted');

    var requestData = function () {
        messagingService.publish("GetClassifications");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'classifications',
        columnDefs: [
            {
                displayName: translationService.translate('Common.Label.Name'),
                field: 'Alias',
                width: 350,
                sort: { direction: uiGridConstants.ASC }
            },
            {
                displayName: translationService.translate('Common.Label.Number'),
                field: 'Number',
                type: 'number'
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });

            gridApi.selection.on.rowSelectionChanged($scope, function () {
                if (Enumerable.From($scope.gridApi.selection.getSelectedRows()).Any(function (c) { return c.BuiltInClassification; })) {
                    var msg = translationService.translate('Classification.Message.CannotRemoveEditUnassigned');
                    if (!alertService.hasAlert(msg)) alertService.addWarning(msg, 5000);
                }
            });
        }
    };

    $scope.canEdit = function () {
        if (Enumerable.From($scope.gridApi.selection.getSelectedRows()).Any(function (c) {
                return c.BuiltInClassification;
        })) {
            return false;
        }
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        if (Enumerable.From($scope.gridApi.selection.getSelectedRows()).Any(function (c) {
                return c.BuiltInClassification;
        })) {
            return false;
        }
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.saveClassification = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        delete $scope.classificationBeingSaved;
        $scope.classificationBeingSaved = {
            Id: $scope.selectedClassification.Id,
            Alias: $scope.selectedClassification.Alias,
            Number: $scope.selectedClassification.Number,
            IsNewClassification: $scope.selectedClassification.IsNewClassification,
        };
        var action = "CreateClassification";
        if (!$scope.classificationBeingSaved.IsNewClassification)
            action = "UpdateClassification";
        messagingService.publish(action, $scope.classificationBeingSaved);

        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Classification.Label.NameSingular'), $scope.classificationBeingSaved.Alias));
                awaitingUpdate = false;
                $scope.classificationBeingSaved = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditClassification = function () {
        $state.go('^.list', null, { reload: true });
    }

    $scope.deleteClassifications = function () {
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (classification) {
            messagingService.publish("DeleteClassification", classification);
        });
        $timeout(function () {
            $scope.gridApi.selection.clearSelectedRows();
        }, 0, true);
    }

    $scope.editClassification = function (){
        delete $scope.selectedClassification;
        $scope.selectedClassification = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedClassification.IsNewClassification = false;
        $state.go('^.new');
    }

    $scope.tellMeAboutThisPage = function () {
        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        if (angular.lowercase($state.current.name) === "classifications.new") {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('Classification.Help.Form.Overview')
            });

            steps.push({
                element: "#newClassificationClassificationName",
                intro: translationService.translate('Classification.Help.Form.Name')
            });

            steps.push({
                element: "#nudHorizontalTable",
                intro: translationService.translate('Classification.Help.Form.Number')
            });

            steps.push({
                element: "#newClassificationSaveButton",
                intro: translationService.translate('Classification.Help.Form.Save')
            });
            steps.push({
                element: "#newClassificationCancelButton",
                intro: translationService.translate('Classification.Help.Form.Cancel')
            });
        }
        else {
            steps.push({
                element: "#notFound",
                intro: translationService.translate('Classification.Help.List.Overview')
            });

            steps.push({
                element: "#newClassification",
                intro: translationService.translate('Classification.Help.List.New')
            });

            steps.push({
                element: "#deleteClassification",
                intro: translationService.translate('Classification.Help.List.Delete')
            });

            steps.push({
                element: "#editClassification",
                intro: translationService.translate('Classification.Help.List.Edit')
            });

            steps.push({
                element: '#classificationListTable',
                intro: translationService.translate('Classification.Help.List.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('Classification.Help.List.Columns.Name')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('Classification.Help.List.Columns.Number')
            });
        }

        return steps;
    };

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};