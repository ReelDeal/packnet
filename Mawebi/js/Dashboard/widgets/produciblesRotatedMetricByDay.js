'use strict';
angular.module('dashboard.widgets.produciblesRotatedMetricByDay', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Daily.ProducibleItemsRotatedByDay';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsRotatedByDay.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsRotatedByDay.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByDay',
                controller: 'ProduciblesRotatedByDayCtrl',
                templateUrl: 'partials/Widgets/produciblesRotatedMetricsByDay.html',
                reload: true
            });
    });

app.controller("ProduciblesRotatedByDayCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsRotated = 0;

    messagingService.produciblesRotatedByDayObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsRotated;
            $scope.itemsRotated = msg.Data.Value;
        }, 100);
    }, $scope, 'Producibles Rotated By Day');

    messagingService.publish("ProducibleRotatedCountByDay", "");
});