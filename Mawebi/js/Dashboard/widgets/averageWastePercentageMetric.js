'use strict';
angular.module('dashboard.widgets.averageWastePercentageMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.AverageWastePercentage';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageWastePercentage.Label.Title',
                description: 'Dashboard.Widgets.AverageWastePercentage.Label.Description',
                dashboard: 'dashboardMgmt',
                category : 'Metrics',
                controller: 'AverageWastePercentageCtrl',
                templateUrl: 'partials/Widgets/averageWastePercentageMetric.html',
                reload : true
            });
    });

app.controller("AverageWastePercentageCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.averageWastePercentage = 0;

    messagingService.averageWastePercentageObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            delete $scope.averageWastePercentage;
            $scope.averageWastePercentage = msg.Data.Value.Value;
        },100);
    }, $scope, "Average Waste Percentage Metric");

    messagingService.publish("AverageWastePercentage", "");
})