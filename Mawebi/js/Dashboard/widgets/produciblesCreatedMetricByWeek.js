'use strict';
angular.module('dashboard.widgets.produciblesCreatedMetricByWeek', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Weekly.ProducibleItemsCreatedByWeek';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsCreatedByWeek.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsCreatedByWeek.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByWeek',
                controller: 'ProduciblesCreatedByWeekCtrl',
                templateUrl: 'partials/Widgets/produciblesCreatedMetricsByWeek.html',
                reload: true
            });
    });

app.controller("ProduciblesCreatedByWeekCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsCreated = 0;

    messagingService.produciblesCreatedByWeekObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsCreated;
            $scope.itemsCreated = msg.Data.Value;
        },100);
    }, $scope, 'Producibles Created By Week');

    messagingService.publish("ProducibleCreatedCountByWeek", "");
})