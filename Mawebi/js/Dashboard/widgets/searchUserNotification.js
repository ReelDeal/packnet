'use strict';
angular.module('dashboard.widgets.searchUserNotification', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.SearchUserNotifications';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.SearchUserNotifications.Label.Title',
                description: 'Dashboard.Widgets.SearchUserNotifications.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'searchUserNotificationCtrl',
                templateUrl: 'partials/Widgets/searchUserNotification.html',
                reload: true
            });
    });

app.controller('searchUserNotificationCtrl', ['translationService', '$scope', '$timeout', 'alertService', 'messagingService', 'uiGridConstants', 'NOTIFICATION_SEVERITY', 'NOTIFICATION_TYPE', 'dateValidationService',
    function (translationService, $scope, $timeout, alertService, messagingService, uiGridConstants, NOTIFICATION_SEVERITY, NOTIFICATION_TYPE, dateValidationService) {
        $timeout = $timeout.getInstance($scope);
        // Locals
        var users;
        var machineGroups;

        var searchedUserNotificationsObservable = messagingService.messageSubject
            .where(messagingService.filterObs("SearchedUserNotifications"));

        function joinData(userNotifications){
            var result = Enumerable.From(userNotifications)
                .Where(function (userNotification) {
                    // Iterates through each User Notification

                    // Obtain the Machine Group information
                    userNotification.NotifiedMachineGroups = Enumerable.From(userNotification.NotifiedMachineGroups)
                        .Join(machineGroups, '$', '$.Id', function (id, machineGroup) {
                            return machineGroup;
                        })
                        .ToArray();

                    // Obtain the User information
                    userNotification.DismissedByUsers = Enumerable.From(userNotification.DismissedByUsers)
                        .Join(users, '$', '$.Id', function (id, user) {
                            return user;
                        })
                        .ToArray();

                    return userNotification;
                })
                .ToArray();

            return result;
        }

        // Bindings
        $scope.gridOptions = {
            enableGridMenu: true,
            columnDefs: [
                {
                    name: translationService.translate('Common.Label.CreatedDate'),
                    field: 'CreatedDate',
                    type: 'date',
                    cellFilter: 'date:\'medium\'',
                    enableFiltering: false,				// filtering for dates is buggy
                    sort: {
                        direction: uiGridConstants.DESC,
                        priority: 0
                    },
                    sortingAlgorithm: function (a, b) {
                        var timeA = typeof(a) === "string" ? new Date(a).getTime() : a.getTime(),
                            timeB = typeof(b) === "string" ? new Date(b).getTime() : b.getTime();

                        return timeA === timeB ? 0 : (timeA < timeB ? -1 : 1);
                    }
                },
                {
                    name: translationService.translate('Common.Label.Message'),
                    field: 'Message'
                },
                {
                    name: translationService.translate('Common.Label.Type'),
                    field: 'NotificationType'
                },
                {
                    name: translationService.translate('Common.Label.Severity'),
                    field: 'TranslatedSeverity'
                }
            ],
            enableColumnResizing: true,
            enableFiltering: false,
            enableRowSelection: false,
            enableSelectAll: false,
            enableSorting: true,
            expandableRowHeight: 200,
            expandableRowTemplate: 'partials/Widgets/searchUserNotificationExpandableRow.html',
            showGridFooter: true
        };

        $scope.dateFrom = moment(new Date().setHours(0, 0, 0, 0)).add(-1, 'days').toDate();
        $scope.dateTo = moment(new Date().setHours(0, 0, 0, 0)).add(1, 'days').toDate();
        $scope.isDateRangeInvalid = false;

        $scope.notificationSeverityInfo = false;
        $scope.notificationSeveritySuccess = false;
        $scope.notificationSeverityWarning = false;
        $scope.notificationSeverityError = false;

        $scope.notificationTypeMachineGroup = false;
        $scope.notificationTypeProductionGroup = false;
        $scope.notificationTypeSystem = false;

        $scope.status = '';

        $scope.selectAllNotificationSeverities = function () {
            $scope.notificationSeverityInfo = true;
            $scope.notificationSeveritySuccess = true;
            $scope.notificationSeverityWarning = true;
            $scope.notificationSeverityError = true;
        };

        $scope.selectAllNotificationTypes = function () {
            $scope.notificationTypeMachineGroup = true;
            $scope.notificationTypeProductionGroup = true;
            $scope.notificationTypeSystem = true;
        };

        $scope.search = function () {
            //validate search criteria
            $scope.isDateRangeInvalid = !dateValidationService.validateSearchDates($scope.dateFrom, $scope.dateTo);

            if ($scope.isDateRangeInvalid) {
                return;
            }
            //end validate

            
            delete $scope.status;
            $scope.status = translationService.translate('Search.Common.Message.Searching');
            var payload = {
                Item1 : $scope.dateFrom,
                Item2 : $scope.dateTo,
                Item3 : [],
                Item4 : []
            };

            // Notification Types
            if ($scope.notificationTypeMachineGroup) payload.Item3.push(NOTIFICATION_TYPE.machineGroup);
            if ($scope.notificationTypeProductionGroup) payload.Item3.push(NOTIFICATION_TYPE.productionGroup);
            if ($scope.notificationTypeSystem) payload.Item3.push(NOTIFICATION_TYPE.system);

            // Notification Severities
            if ($scope.notificationSeverityError) payload.Item4.push(NOTIFICATION_SEVERITY.error);
            if ($scope.notificationSeverityInfo) payload.Item4.push(NOTIFICATION_SEVERITY.info);
            if ($scope.notificationSeveritySuccess) payload.Item4.push(NOTIFICATION_SEVERITY.success);
            if ($scope.notificationSeverityWarning) payload.Item4.push(NOTIFICATION_SEVERITY.warning);

            messagingService.publish('SearchUserNotifications', payload);
        };

        // Listeners
        messagingService.machineGroupsObservable.autoSubscribe(function (msg) {
            machineGroups = Enumerable.From(msg.Data).Select(function (i) {
                 return { Id: i.Id, Alias: i.Alias }
            }).ToArray();
        }, $scope, 'Machine Groups');

        searchedUserNotificationsObservable.autoSubscribe(function (msg) {
            $timeout(function () {
                
                delete $scope.status;
                $scope.status = translationService.translate('Search.Common.Message.ResultFound');
                for (var index = 0; index < msg.Data.length; index++) {
                    var row = msg.Data[index];
                    if (row['Severity'].toLowerCase() in NOTIFICATION_SEVERITY) {
                        row['TranslatedSeverity'] = translationService.translate('Common.Label.' + row['Severity']);
                    }
                    else {
                        row['TranslatedSeverity'] = row['Severity'];
                    }
                }
                
                delete $scope.gridOptions.data;
                $scope.gridOptions.data = joinData(msg.Data);
                
                delete $scope.status;
                $scope.status = '';
            }, 100);
        }, $scope, 'Searched User Notifications');

        messagingService.usersObservable.autoSubscribe(function (msg) {
            users = Enumerable.From(msg.Data).Select(function (i) {
                return { Id: i.Id, UserName: i.UserName }
            }).ToArray();
        }, $scope, 'Users');

        // Initialize
        messagingService.publish('GetMachineGroups');
        messagingService.publish('GetUsers');
    }
]);