'use strict';
angular.module('dashboard.widgets.produciblesImportedMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.ProducibleItemsImported';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsImported.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsImported.Label.Description',
                dashboard: 'usageMgmt',
                category: 'Metrics',
                controller: 'ProduciblesImportedCtrl',
                templateUrl: 'partials/Widgets/produciblesImportedMetrics.html',
                reload: true
            });
    });

app.controller("ProduciblesImportedCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsImported = 0;
    messagingService.produciblesImportedObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsImported;
            $scope.itemsImported = msg.Data.Value;
        },100);
    }, $scope, 'Producibles Imported');

    messagingService.publish("ProducibleImportCount", "");
})