'use strict';
angular.module('dashboard.widgets.averageWastePercentageByCorrugateMetricByDay', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Daily.AverageWastePercentageByCorrugateByDay';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageWastePercentageByCorrugateByDay.Label.Title',
                description: 'Dashboard.Widgets.AverageWastePercentageByCorrugateByDay.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'MetricsByDay',
                controller: 'AverageWastePercentageByCorrugateByDayCtrl',
                templateUrl: 'partials/Widgets/averageWastePercentageByCorrugateMetric.html',
                edit: {
                    controller : 'AverageWastePercentageByCorrugateByDayCtrl',
                    templateUrl: 'partials/Widgets/graphicEditDialogMetric.html'
                },
                resolve: {
                    data: function (config) {
                        if (!config.widgetHeight) {
                            config.widgetHeight = 300; //Default widget height before the user configures it.
                        }
                    }
                },
                reload: true
            });
    });

app.controller("AverageWastePercentageByCorrugateByDayCtrl", function ($scope, messagingService, $timeout, config) {
    $timeout = $timeout.getInstance($scope);

    $scope.colorPickerSettings = {
        opacity : true
    };

    $scope.applyDefaults = function (){
        config.widgetHeight = 300;
        config.graphFillColor = "#97bbcd";
        config.graphFillColorOpacity = "0.5";
        config.graphFillColorRGBA = "rgba(151,187,205,0.5)";
        config.graphStrokeColor = "#97bbcd";
        config.graphStrokeColorOpacity = "0.8";
        config.graphStrokeColorRGBA = "rgba(151,187,205,0.8)";
        config.graphHighlightFillColor = "#97bbcd";
        config.graphHighlightFillColorOpacity = "0.75";
        config.graphHighlightFillColorRGBA = "rgba(151,187,205,0.75)";
        config.graphHighlightStrokeColor = "#97bbcd";
        config.graphHighlightStrokeColorOpacity = "1";
        config.graphHighlightStrokeColorRGBA = "rgba(151,187,205,1)";
    }

    if (!config.graphFillColor) {
        $scope.applyDefaults();
    }
    $scope.averageWastePercentageByCorrugateData = {};
    $scope.averageWastePercentageByCorrugate = {
        labels : [],
        datasets : [
            {
                fillColor : config.graphFillColorRGBA,
                strokeColor : config.graphStrokeColorRGBA,
                highlightFill : config.graphHighlightFillColorRGBA,
                highlightStroke : config.graphHighlightStrokeColorRGBA,
                data : []
            }
        ]
    };

    $scope.options = {
        segmentShowStroke : false,
        animation: false,
        responsive : true,
        maintainAspectRatio : true,
    }

    messagingService.averageWastePercentageByCorrugateByDayObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            var index = $scope.averageWastePercentageByCorrugate.labels.indexOf(msg.Data.Value.Key);
            if (index == -1) {
                //Item doesn't exists
                $scope.averageWastePercentageByCorrugate.labels.push(msg.Data.Value.Key);
                $scope.averageWastePercentageByCorrugate.datasets[0].data.push(msg.Data.Value.Value);
            }
            else {
                $scope.averageWastePercentageByCorrugate.datasets[0].data[index] = msg.Data.Value.Value;
            }
        }, 100);
    }, $scope, "Average Waste Percentage By Corrugate Metric By Day");

    messagingService.publish("AverageWastePercentageByCorrugateByDay", "");
});