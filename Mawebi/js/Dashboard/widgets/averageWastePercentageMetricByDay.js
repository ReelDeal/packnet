'use strict';
angular.module('dashboard.widgets.averageWastePercentageMetricByDay', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Daily.AverageWastePercentageByDay';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageWastePercentageByDay.Label.Title',
                description: 'Dashboard.Widgets.AverageWastePercentageByDay.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'MetricsByDay',
                controller: 'AverageWastePercentageByDayCtrl',
                templateUrl: 'partials/Widgets/averageWastePercentageMetricByDay.html',
                reload : true
            });
    });

app.controller("AverageWastePercentageByDayCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.averageWastePercentage = 0;

    messagingService.averageWastePercentageByDayObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            delete $scope.averageWastePercentage;
            $scope.averageWastePercentage = msg.Data.Value.Value;
        },100);
    }, $scope, "Average Waste Percentage Metric By Day");

    messagingService.publish("AverageWastePercentageByDay", "");
})