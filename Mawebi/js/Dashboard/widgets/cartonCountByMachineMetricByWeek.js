'use strict';
angular.module('dashboard.widgets.cartonCountByMachineByWeekMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Weekly.CartonCountByMachineByWeek';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.CartonCountByMachineByWeek.Label.Title',
                description: 'Dashboard.Widgets.CartonCountByMachineByWeek.Label.Description',
                dashboard: 'monitoringMgmt',
                category: 'MetricsByWeek',
                controller: 'CartonCountByMachineByWeekCtrl',
                templateUrl: 'partials/Widgets/cartonCountByMachineMetric.html',
                reload: true
            });
    });

app.controller("CartonCountByMachineByWeekCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.cartonCountByMachineData = {};
    $scope.cartonCountByMachine = {
        labels: [],
        datasets: [
            {
                //light gray
                //fillColor: "rgba(220,220,220,0.5)",
                //strokeColor: "rgba(220,220,220,0.8)",
                //highlightFill: "rgba(220,220,220,0.75)",
                //highlightStroke: "rgba(220,220,220,1)",

                //light blue
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: []
            }
        ]
    };
    
    $scope.options = {
        segmentShowStroke: false,
        animation: false,
        responsive: true,
        maintainAspectRatio: true,
    }

    messagingService.cartonCountByMachineByWeekObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            var index = $scope.cartonCountByMachine.labels.indexOf(msg.Data.Value.Key);
            if (index == -1) {
                //Item doesn't exists
                $scope.cartonCountByMachine.labels.push(msg.Data.Value.Key);
                $scope.cartonCountByMachine.datasets[0].data.push(msg.Data.Value.Value);
            }
            else {
                $scope.cartonCountByMachine.datasets[0].data[index] = msg.Data.Value.Value;
            }
        },100);
    }, $scope, 'Carton Count By Machine By Week');

    messagingService.publish("CartonCountByMachineByWeek", "");
})