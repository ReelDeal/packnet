﻿'use strict';
angular.module('dashboard.widgets.orders', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.Orders';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.Orders.Label.Title',
                description: 'Dashboard.Widgets.Orders.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'OrdersListCtrl',
                templateUrl: 'partials/SelectionAlgorithm/Orders/ordersList.html',
                reload: true
            });
    });