'use strict';
angular.module('dashboard.widgets.produciblesRotatedMetricByWeek', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Weekly.ProducibleItemsRotatedByWeek';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsRotatedByWeek.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsRotatedByWeek.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByWeek',
                controller: 'ProduciblesRotatedByWeekCtrl',
                templateUrl: 'partials/Widgets/produciblesRotatedMetricsByWeek.html',
                reload: true
            });
    });

app.controller("ProduciblesRotatedByWeekCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsRotated = 0;

    messagingService.produciblesRotatedByWeekObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsRotated;
            $scope.itemsRotated = msg.Data.Value;
        }, 100);
    }, $scope, 'Producibles Rotated By Week');

    messagingService.publish("ProducibleRotatedCountByWeek", "");
});