angular.module('dashboard.widgets.machineStatus', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.MachineStatus';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.MachineStatus.Label.Title',
                description: 'Dashboard.Widgets.MachineStatus.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'machineStatusCtrl',
                templateUrl: 'partials/Widgets/machineStatus.html',
                reload: true
            });
    });
