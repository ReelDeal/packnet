'use strict';
angular.module('dashboard.widgets.produciblesRotatedMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.ProducibleItemsRotated';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsRotated.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsRotated.Label.Description',
                dashboard: 'usageMgmt',
                category: 'Metrics',
                controller: 'ProduciblesRotatedCtrl',
                templateUrl: 'partials/Widgets/produciblesRotatedMetrics.html',
                reload: true
            });
    });

app.controller("ProduciblesRotatedCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsRotated = 0;

    messagingService.produciblesRotatedObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsRotated;
            $scope.itemsRotated = msg.Data.Value;
        }, 100);
    }, $scope, 'Producibles Rotated');

    messagingService.publish("ProducibleRotatedCount", "");
});