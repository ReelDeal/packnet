'use strict';
angular.module('dashboard.widgets.produciblesFailedMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.ProducibleItemsFailed';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsFailed.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsFailed.Label.Description',
                dashboard: 'usageMgmt',
                category: 'Metrics',
                controller: 'ProduciblesFailedCtrl',
                templateUrl: 'partials/Widgets/produciblesFailedMetrics.html',
                reload: true
            });
    });

app.controller("ProduciblesFailedCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsFailed = 0;

    messagingService.produciblesFailedObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsFailed;
            $scope.itemsFailed = msg.Data.Value;
        }, 100);
    }, $scope, 'Producibles Failed');

    messagingService.publish("ProducibleFailedCount", "");
});