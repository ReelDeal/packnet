﻿'use strict';
angular.module('dashboard.widgets.cartonpropertygroups', ['adf.provider'])
    .config(function (dashboardProvider) {
    var id = 'Dashboard.Widgets.General.CartonPropertyGroups';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.CartonPropertyGroups.Label.Title',
                description: 'Dashboard.Widgets.CartonPropertyGroups.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'CartonPropertyGroupsCtrl',
                templateUrl: 'partials/Widgets/cartonPropertyGroups.html',
                reload: true
            });
    });