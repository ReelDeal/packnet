﻿angular.module('dashboard.widgets.deleteCartonsFromRange', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.DeleteBoxLastCartons';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.DeleteBoxLastCartons.Label.Title',
                description: 'Dashboard.Widgets.DeleteBoxLastCartons.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'deleteCartonsCtrl',
                templateUrl: 'partials/Widgets/deleteCartons.html',
                reload: true
            });
    });