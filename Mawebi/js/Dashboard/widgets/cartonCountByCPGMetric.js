'use strict';
angular.module('dashboard.widgets.cartonCountByCPGMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.CartonCountByCPG';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.CartonCountByCPG.Label.Title',
                description: 'Dashboard.Widgets.CartonCountByCPG.Label.Description',
                dashboard: 'monitoringMgmt',
                category: 'Metrics',
                controller: 'CartonCountByCPGCtrl',
                templateUrl: 'partials/Widgets/cartonCountByCPGMetric.html',
                edit: {
                    controller: 'CartonCountByCPGCtrl',
                    templateUrl: 'partials/Widgets/graphicEditDialogMetric.html'
                },
                resolve: {
                    data: function (config) {
                        config.widgetHeight = 200; //Default widget height before the user configures it.
                    }
                },
                reload: true
            });
    });

app.controller("CartonCountByCPGCtrl", function ($scope, $timeout, config, messagingService) {
    $timeout = $timeout.getInstance($scope);

    $scope.options = {
        animation: false,
        responsive: true,
        showTooltips: true
    }

    $scope.availableMetrics =
    [
        {
            MetricName: "CartonCountByCPG",
            LocalizationKey: "Common.Label.All"
        },
        {
            MetricName: "CartonCountByCPGByHour",
            LocalizationKey: "Common.Label.ThisHour"
        },
        {
            MetricName: "CartonCountByCPGBySlidingHour",
            LocalizationKey: "Common.Label.PastHour"
        },
        {
            MetricName: "CartonCountByCPGByDay",
            LocalizationKey: "Common.Label.Today"
        },
        {
            MetricName: "CartonCountByCPGByWeek",
            LocalizationKey: "Common.Label.ThisWeek"
        },
        {
            MetricName: "CartonCountByCPGByMonth",
            LocalizationKey: "Common.Label.ThisMonth"
        }
    ];

    $scope.applyDefaults = function () {
        config.widgetHeight = 200;
        config.graphFillColor = "#97bbcd";
        config.graphFillColorOpacity = "0.5";
        config.graphFillColorRGBA = "rgba(151,187,205,0.5)";
        config.graphStrokeColor = "#97bbcd";
        config.graphStrokeColorOpacity = "0.8";
        config.graphStrokeColorRGBA = "rgba(151,187,205,0.8)";
        config.graphHighlightFillColor = "#97bbcd";
        config.graphHighlightFillColorOpacity = "0.75";
        config.graphHighlightFillColorRGBA = "rgba(151,187,205,0.75)";
        config.graphHighlightStrokeColor = "#97bbcd";
        config.graphHighlightStrokeColorOpacity = "1";
        config.graphHighlightStrokeColorRGBA = "rgba(151,187,205,1)";
        config.options = $scope.options;
    }

    if (!config.graphFillColor) {
        $scope.applyDefaults();
    }

    
    $scope.metrics = [];
    $scope.cartonPropertyGroups = [];
    $scope.targetCartonCountByCPG = [];
    $scope.actualCartonCountByCPG = [];
    $scope.selectedProductionGroup = {};
    $scope.selectedMetric = $scope.availableMetrics[0];
    $scope.pieColors = [];
    $scope.totalMix = 0;
    $scope.totalProducedForMetric = 0;
    $scope.showDetails = false;


    var setupCPGTargetChart = function (cpgs){
        delete $scope.cartonPropertyGroups;
        $scope.cartonPropertyGroups = cpgs.ToArray();

        if ($scope.cartonPropertyGroups.length > 0) {
            delete $scope.totalMix;
            $scope.totalMix = $scope.cartonPropertyGroups.reduce(function (c, n) { return c + n.MixQuantity; }, 0);
        }
        else $scope.totalMix = 0;

        delete $scope.pieColors;
            $scope.pieColors = [];

        delete $scope.targetCartonCountByCPG;
        $scope.targetCartonCountByCPG = [];
        if ($scope.totalMix > 0) {          
            cpgs.ForEach(function (cpg) {
                $scope.targetCartonCountByCPG.push({
                    value: Math.round((cpg.MixQuantity / $scope.totalMix) * 100),
                    color: getColorForCpg(cpg.Alias), //Creates a random color for graph
                    label : cpg.Alias
                });
            });
        }
        else {
            $scope.targetCartonCountByCPG.push({
                value: 100,
                color: '#D7D7D7', //Creates a random color for graph
                label: "No Data Available"
            });
        }
    };

    var setupCPGActualChart = function () {

        if ($scope.metrics.length > 0) {
            delete $scope.totalProducedForMetric;
            $scope.totalProducedForMetric = $scope.metrics
                .reduce(function (c, n) { return parseInt(c + parseInt(n.Value.Value)); }, 0);
        }
        else $scope.totalProducedForMetric = 0;

        var metrics = Enumerable.From($scope.metrics);
        delete $scope.actualCartonCountByCPG;
        $scope.actualCartonCountByCPG = [];
        if ($scope.totalProducedForMetric > 0) {
            metrics.ForEach(function (metric) {

                if (metric.Value.Value > 0) {
                    $scope.actualCartonCountByCPG.push({
                        value: Math.round((metric.Value.Value / $scope.totalProducedForMetric) * 100),
                        color: getColorForCpg(metric.Value.Key), //Creates a random color for graph
                        label : metric.Value.Key
                    });
                }
            });
        }
        else {
            $scope.actualCartonCountByCPG.push({
                value: 100,
                color: '#D7D7D7', //Creates a random color for graph
                label: "No Data Available"
            });
        }
    };

    $scope.getCPGsForSelectedPG = function () {
        setupCPGTargetChart(Enumerable.From($.parseJSON($scope.selectedProductionGroup.Options.ConfiguredCartonPropertyGroups)));
        
        delete $scope.actualCartonCountByCPG;
        $scope.actualCartonCountByCPG = [];
        
        delete $scope.metrics;
        $scope.metrics = [];
        messagingService.publish($scope.selectedMetric.MetricName, "");
    };

    $scope.toggleDetails = function () {
        $scope.showDetails = !$scope.showDetails;
    };

    $scope.getSelectedMetric = function () {
        
        delete $scope.metrics;
        $scope.metrics = [];
        messagingService.publish($scope.selectedMetric.MetricName, "");
    };

    var getColorForCpg = function (cpgAlias){

        var color = '';

        if (!$scope.pieColors || $scope.pieColors.length === 0)
            
            delete $scope.pieColors;
            $scope.pieColors = [];


        var colrIndex = $scope.pieColors.map(function (x) { return x.cpgAlias; }).indexOf(cpgAlias);

        if (colrIndex > -1) color = $scope.pieColors[colrIndex].color;
        else {
            color = getRandomColor();
            $scope.pieColors.push({ cpgAlias: cpgAlias, color: color });
        }

        return color;
    }

    var getRandomColor = function (){
        return '#' + Math.floor(Math.random() * 16777215).toString(16);
    };

    var getAvailableCpgAliases = function (){
        return $scope.cartonPropertyGroups.map(function (x) { return x.Alias; });
    };

    messagingService.productionGroupsObservable
    .autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.productionGroups;
            $scope.productionGroups = Enumerable.From(msg.Data).Where("pg=>pg.Options.ConfiguredCartonPropertyGroups").ToArray();
            
            delete $scope.selectedProductionGroup;
            $scope.selectedProductionGroup = $scope.productionGroups[0];

            if ($scope.selectedProductionGroup != undefined) {
                setupCPGTargetChart(Enumerable.From($.parseJSON($scope.selectedProductionGroup.Options.ConfiguredCartonPropertyGroups)));
            }
            messagingService.publish($scope.selectedMetric.MetricName, "");
        }, 0, true);
       
    }, $scope, 'Production Groups');

    messagingService.cartonCountByCPGObservable
        .autoSubscribe(function (msg) {
            updateMetrics(msg.Data);
        }, $scope, 'Carton Count By CPGs');

    messagingService.cartonCountByCPGByHourObservable
        .autoSubscribe(function (msg) {
            updateMetrics(msg.Data);
        }, $scope, 'Carton Count By CPGs By Hour');

    messagingService.cartonCountByCPGBySlidingHourObservable
        .autoSubscribe(function (msg) {
            updateMetrics(msg.Data);
        }, $scope, 'Carton Count By CPGs By SLiding Hour');

    messagingService.cartonCountByCPGByDayObservable
    .autoSubscribe(function (msg) {
        updateMetrics(msg.Data);
    }, $scope, 'Carton Count By CPGs By Day');

    messagingService.cartonCountByCPGByWeekObservable
    .autoSubscribe(function (msg) {
        updateMetrics(msg.Data);
    }, $scope, 'Carton Count By CPGs By Week');

    messagingService.cartonCountByCPGByMonthObservable
    .autoSubscribe(function (msg) {
        updateMetrics(msg.Data);
    }, $scope, 'Carton Count By CPGs By Month');

    var updateMetrics = function (metric) {
        if ($scope.selectedMetric.MetricName === metric.MetricType && getAvailableCpgAliases().indexOf(metric.Value.Key) > -1) {
            var mtr = $scope.metrics.filter(function (m) { return m.Value.Key === metric.Value.Key; });
            if (mtr.length > 0) {
                var idx = $scope.metrics.indexOf(mtr[0]);
                if (idx != -1) {
                    $scope.metrics[idx] = metric;
                }
            }
            else
                $scope.metrics.push(metric);
        }
        setupCPGActualChart();
    };

    messagingService.signalRConnectedObservable.autoSubscribe(function () {
        messagingService.publish("GetProductionGroups", "");
    }, $scope, 'SignalR Connected');

    if (messagingService.IsSignalRConnected()) {
        messagingService.publish("GetProductionGroups", "");
    }
});