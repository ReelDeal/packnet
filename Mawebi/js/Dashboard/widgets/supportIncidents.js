﻿angular.module('dashboard.widgets.supportIncidents', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.SupportIncidents';
        dashboardProvider
            .widget(id, {
                id: id,
                title: id + '.Title',
                description: id + '.Description',
                category: 'Metrics',
                dashboard: 'dashboardMgmt',
                controller: 'SupportIncidentCtrl',
                reload: true,
                templateUrl: 'partials/Widgets/supportIncidents.html',
            });
    });