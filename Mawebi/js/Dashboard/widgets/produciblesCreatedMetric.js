'use strict';
angular.module('dashboard.widgets.produciblesCreatedMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.ProducibleItemsCreated';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsCreated.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsCreated.Label.Description',
                dashboard: 'usageMgmt',
                category: 'Metrics',
                controller: 'ProduciblesCreatedCtrl',
                templateUrl: 'partials/Widgets/produciblesCreatedMetrics.html',
                reload: true
            });
    });

app.controller("ProduciblesCreatedCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsCreated = 0;

    messagingService.produciblesCreatedObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsCreated;
            $scope.itemsCreated = msg.Data.Value;
        },100);
    }, $scope, 'Producibles Created');

    messagingService.publish("ProducibleCreatedCount", "");
})