'use strict';
angular.module('dashboard.widgets.averageProductionTimeMetricByWeek', ['adf.provider', 'translationModule'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Weekly.AverageProductionTimeByWeek';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageProductionTimeByWeek.Label.Title',
                description: 'Dashboard.Widgets.AverageProductionTimeByWeek.Label.Description',
                dashboard: 'dashboardMgmt',
                category : 'MetricsByWeek',
                controller: 'AverageProductionTimeByWeekCtrl',
                templateUrl: 'partials/Widgets/averageProductionTimeMetricByWeek.html',
                reload : true
            });
    });

app.controller("AverageProductionTimeByWeekCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsCreated = 0;

    messagingService.averageProductionTimeByWeekObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            delete $scope.averageProductionTime;
            $scope.averageProductionTime = msg.Data.Value.Value;
        }, 100);
    }, $scope, "Average Production Time Metric By Week");

    messagingService.publish("AverageProductionTimeByWeek", "");
});