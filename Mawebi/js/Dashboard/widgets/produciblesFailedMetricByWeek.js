'use strict';
angular.module('dashboard.widgets.produciblesFailedMetricByWeek', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Weekly.ProducibleItemsFailedByWeek';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsFailedByWeek.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsFailedByWeek.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByWeek',
                controller: 'ProduciblesFailedByWeekCtrl',
                templateUrl: 'partials/Widgets/produciblesFailedMetricsByWeek.html',
                reload: true
            });
    });

app.controller("ProduciblesFailedByWeekCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsFailed = 0;

    messagingService.produciblesFailedByWeekObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsFailed;
            $scope.itemsFailed = msg.Data.Value;
        }, 100);
    }, $scope, 'Producibles Failed By Week');

    messagingService.publish("ProducibleFailedCountByWeek", "");
});