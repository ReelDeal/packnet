'use strict';
angular.module('dashboard.widgets.corrugateEfficiency', ['adf.provider'])
    .service('guidHandler', function () {
        var guidHandler = {};

        guidHandler.getGuid = function (){
            function s4(){
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }

            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        return guidHandler;
    })
    .service('timeDateConverter', function () {

        var timeDateConverter = {};

        //required for plotting data over time
        timeDateConverter.getDateMilliseconds = function (year, month, day, hours, minutes, seconds){
            return new Date(year, month - 1, day, hours, minutes, seconds).getTime();
        };

        //get date string - for nice tooltips
        timeDateConverter.getDateString = function (milliseconds){
            return new Date(milliseconds).toDateString();
        }

        return timeDateConverter;
    })
    .service('drawGraph', function () {
        var drawingService = {};

        drawingService.Init = function (graph) {
            $(window).resize(function () {
                drawingService.drawGraph(graph);
            });

            $(document).ready(function () {
                drawingService.drawGraph(graph);
            });

            drawingService.bindEvents(graph);
            drawingService.drawGraph(graph);
        }

        drawingService.drawGraph = function (graph){
            try {
                drawingService.clearTooltips();
                $.plot($('#efficiency-div-' + graph.Configuration.Id), graph.DataSet, graph.Options);
            }
            catch (err) {
                
            }
        }

        drawingService.toolTipHTML = function (stat, series){
            var html = '';
            html += '<div class="tooltip">';
            html += '<div>';
            if (series) html += '<span class="series">' + series + '</span>';
            html += '<span class="stats">' + stat + '</span>';
            html += '</div>';
            html += '</div>';
            return html;
        }

        var clickTips, hoverTip;
        drawingService.clearTooltips = function (){
            if (hoverTip) hoverTip.remove();
            if (clickTips) $.each(clickTips, function (i, t) { t.remove(); });
            hoverTip = null;
            clickTips = null;
        }

        drawingService.bindEvents = function (graph){

            //clear event handlers for the graph so we dont get duplicates
            $('#efficiency-div-' + graph.Configuration.Id)
                .off('.graph');

            $('#efficiency-div-' + graph.Configuration.Id)
                .on('plothover.graph', function (event, pos, item) {
                var ofs = { height : 0, width : 0 },
                    fmtd;
                if (clickTips) {
                    return;
                }
                    drawingService.clearTooltips();
                if (item) {
                    fmtd = item.series.data[item.dataIndex][1];
                    hoverTip = $(drawingService.toolTipHTML(fmtd, item.series.label));
                    $('#wrapper').append(hoverTip);
                    ofs.height = hoverTip.outerHeight();
                    ofs.width = hoverTip.outerWidth();
                    hoverTip.offset({ left : item.pageX - ofs.width / 2, top : item.pageY - ofs.height - 15 });
                }
            });

            $('#efficiency-div-' + graph.Configuration.Id)
                .on('plotclick.graph', function (event, pos, item) {
                    for (var i = 0; graph.DataSet.length - 1; i++) {
                        if (graph.DataSet[i].label === item.series.label) {
                            var serieToMove = graph.DataSet[i];
                            graph.DataSet.splice(i, 1);
                            graph.DataSet.push(serieToMove);

                        break;
                    }
                }
                drawingService.drawGraph(graph);
            });
        }

        return drawingService;

    })
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.CorrugateEfficiency';
    dashboardProvider
        .widget(id, {
            id : id,
            title : id+'.Title',
                category: 'Metrics',
                dashboard: 'dashboardMgmt',
                description : id+'.Description',
                controller : 'corrugateEfficiencyCtrl',
                templateUrl : 'partials/Widgets/corrugateEfficiency.html',
                scope : {},
                edit : {
                    templateUrl : 'partials/Widgets/corrugateEfficiencyEdit.html',
                    controller : 'corrugateEfficiencyEditCtrl'
                }
            });
    });


app.controller("corrugateEfficiencyCtrl", function ($scope, messagingService, $timeout, $filter, guidHandler, timeDateConverter, drawGraph) {
    $timeout = $timeout.getInstance($scope);

    //Setup messaging
    messagingService.configurationRequestResponse.autoSubscribe(function (msg) {
        if ($scope.config.graph.Configuration.Id != undefined && $scope.config.graph.Configuration.Id != msg.Data.Id) {
            return;
        }

        if ($scope.config.graph.Configuration.Id === undefined) {
            $('#efficiency-div').attr('id', 'efficiency-div-' + msg.Data.Id);
        }

        
        delete $scope.config.graph.Configuration;
        $scope.config.graph.Configuration = msg.Data;

        messagingService.publish("WasteReport", $scope.config.graph.Configuration.Id);
    },$scope,'Configuration Request');

    messagingService.configurationUpdateResponse.autoSubscribe(function (msg) {
        $scope.drawStuff(msg.Data);
    }, $scope, 'Configuration updated');

    messagingService.wasteReportResponse.autoSubscribe(function (msg) {
        $scope.drawStuff(msg.Data);
    },$scope,'Waste report');


    //Change the id of the control so its accessible
    try {
        $('#efficiency-div').attr('id', 'efficiency-div-' + $scope.config.graph.Configuration.Id);
    }
    catch (err) {
    }


    //Setup graph
    if ($scope.config.graph === undefined) {

        
        delete $scope.config.graph;
        $scope.config.graph = {
            DataTypes : [
                {
                    key : "WasteValue",
                    LocalizationKey : 'Dashboard.Widgets.General.CorrugateEfficiencyEdit.DataTypes.WasteValue'
                },
                {
                    key : "WastePercentage",
                    LocalizationKey : 'Dashboard.Widgets.General.CorrugateEfficiencyEdit.DataTypes.WasteByPercentage'
                },
                {
                    key : "ProductionTime",
                    LocalizationKey: 'Dashboard.Widgets.General.CorrugateEfficiencyEdit.DataTypes.ProductionTime'
                }
            ],
            FilterTypes : ["Operator", "Machine", "TiledBox", "RotatedBox", "FailedBox"],
            Configuration : {},
            Colors : ['#1337EB', '#RA5182', '#360NOS', '#058DC7', '#50B432', '#ED561B', '#666666'],
            DataSet : [],
            Options : {
                grid : {
                    hoverable : true,
                    clickable : true,
                    autoHighlight : true
                },
                xaxis : {
                    mode : "time"
                },
                yaxis : { min : 0 },
                lines : {
                    show : true,
                    fill : true
                },
                series : {
                    points : {
                        show : true,
                        radius : 1
                    }
                }
            }
        };
        messagingService.publish("NewConfiguration", guidHandler.getGuid());
    }
    else {
        messagingService.publish("WasteReport", $scope.config.graph.Configuration.Id);
    }

    $scope.graph = $scope.config.graph;

   

    //Graph drawing stuff

    $scope.drawStuff = function (data) {
        $timeout(function () {
            if ($scope.config.graph.Configuration.Id != data.GraphId) {
                return;
            }

            processIncomingData(data);
            drawGraph.drawGraph($scope.config.graph);
        });
    };

    function processIncomingData(data) {
        var dataPointGroups = Enumerable.From(data.DataPoints).GroupBy(function (dataPoint) { return dataPoint.Line; }).ToArray();

        
        delete $scope.config.graph.DataSet;
        $scope.config.graph.DataSet =[];
        
        for (var j = 0; j < dataPointGroups.length; j++) {
            $scope.config.graph.DataSet.push({
        label: dataPointGroups[j].source[0].Line, data: []}) ;

            for (var i = 0; i < dataPointGroups[j].source.length; i++) {

                var year = dataPointGroups[j].source[i].CreationTime.substr(0, 4);
                var month = dataPointGroups[j].source[i].CreationTime.substr(5, 2);
                var day = dataPointGroups[j].source[i].CreationTime.substr(8, 2);
                var hour = dataPointGroups[j].source[i].CreationTime.substr(11, 2);
                var minute = dataPointGroups[j].source[i].CreationTime.substr(14, 2);
                var second = dataPointGroups[j].source[i].CreationTime.substr(17, 2);

                $scope.config.graph.DataSet[j].data.push([timeDateConverter.getDateMilliseconds(year, month, day, hour, minute, second), dataPointGroups[j].source[i].Data]);
            }

            $scope.config.graph.DataSet[j].color = $scope.config.graph.Colors[j];
        }
    }

    drawGraph.Init($scope.config.graph);
});

app.controller("corrugateEfficiencyEditCtrl", function ($scope, messagingService) {
  
    $scope.saveFiltersClick = function () {
        messagingService.publish("ConfigurationUpdate", $scope.config.graph.Configuration);
    };

    $scope.addLineClick = function (){
        $scope.config.graph.Configuration.Lines.push({ LineName: "Line " + ($scope.config.graph.Configuration.Lines.length + 1), Filters: [] });
    };
    
    $scope.removeLineClick = function (){
        $scope.config.graph.Configuration.Lines.pop();
    };

    $scope.addFilter = function (currentLine){
        currentLine.Filters.push({ Type: "", Value: "" });
    };

    $scope.deleteFilter = function (filterToRemove, currentLine) { 
        var index = currentLine.Filters.indexOf(filterToRemove);
        if (index > -1) {
            currentLine.Filters.splice(index, 1);
        }
    };
});
