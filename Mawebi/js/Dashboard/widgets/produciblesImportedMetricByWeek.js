'use strict';
angular.module('dashboard.widgets.produciblesImportedMetricByWeek', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Weekly.ProducibleItemsImportedByWeek';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsImportedByWeek.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsImportedByWeek.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByWeek',
                controller: 'ProduciblesImportedByWeekCtrl',
                templateUrl: 'partials/Widgets/produciblesImportedMetricsByWeek.html',
                reload: true
            });
    });

app.controller("ProduciblesImportedByWeekCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsImported = 0;

    messagingService.produciblesImportedByWeekObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsImported;
            $scope.itemsImported = msg.Data.Value;
        },100);
    }, $scope, 'Producibles Imported By Week');

    messagingService.publish("ProducibleImportCountByWeek", "");
})