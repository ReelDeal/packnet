'use strict';
angular.module('dashboard.widgets.averageWastePercentageMetricByWeek', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Weekly.AverageWastePercentageByWeek';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageWastePercentageByWeek.Label.Title',
                description: 'Dashboard.Widgets.AverageWastePercentageByWeek.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'MetricsByWeek',
                controller: 'AverageWastePercentageByWeekCtrl',
                templateUrl: 'partials/Widgets/averageWastePercentageMetricByWeek.html',
                reload: true
            });
    });

app.controller("AverageWastePercentageByWeekCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);

    $scope.averageWastePercentage = 0;

    messagingService.averageWastePercentageByWeekObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            delete $scope.averageWastePercentage;
            $scope.averageWastePercentage = msg.Data.Value.Value;
        },100);
    }, $scope, "Average Waste Percentage Metric By Week");

    messagingService.publish("AverageWastePercentageByWeek", "");
})