'use strict';
angular.module('dashboard.widgets.cartonCountByMachineMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.CartonCountByMachine';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.CartonCountByMachine.Label.Title',
                description: 'Dashboard.Widgets.CartonCountByMachine.Label.Description',
                dashboard: 'monitoringMgmtCtrl',
                category: 'Metrics',
                controller: 'CartonCountByMachineCtrl',
                templateUrl: 'partials/Widgets/cartonCountByMachineMetric.html',
                reload: true
            });
    });

app.controller("CartonCountByMachineCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);

    $scope.cartonCountByMachineData = {};
    $scope.cartonCountByMachine = {
        labels: [],
        datasets: [
            {
                //light blue
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: []
            }
        ]
    };
    
    $scope.options = {
        segmentShowStroke: false,
        animation: false,
        responsive: true,
        maintainAspectRatio: true,
    }

    messagingService.cartonCountByMachineObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            var index = $scope.cartonCountByMachine.labels.indexOf(msg.Data.Value.Key);
            if (index == -1) {
                //Item doesn't exists
                $scope.cartonCountByMachine.labels.push(msg.Data.Value.Key);
                $scope.cartonCountByMachine.datasets[0].data.push(msg.Data.Value.Value);
            }
            else {
                $scope.cartonCountByMachine.datasets[0].data[index] = msg.Data.Value.Value;
            }
        },100);
    }, $scope, 'Carton Count By Machine');

    messagingService.publish("CartonCountByMachine", "");
})