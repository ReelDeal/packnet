'use strict';
angular.module('dashboard.widgets.machineGroupsWidget', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.MachineGroups';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.MachineGroups.Label.Title',
                description: 'Dashboard.Widgets.MachineGroups.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'MachineGroupsWidgetCtrl',
                templateUrl: 'partials/MachineManager/dashboardWidgetTableTemplate.html',
                reload: true
            });
    });

function MachineGroupsWidgetCtrl(translationService, $scope, $state, $window, $timeout, machineGroupService, messagingService) {
    $timeout = $timeout.getInstance($scope);

    function MachineGroupsWidgetCtrlTag() { };
    
    $scope.__tag = new MachineGroupsWidgetCtrlTag();

    $scope.machineGroups = machineGroupService.getMachineGroups();

    machineGroupService.machineGroupsUpdatedObservable.autoSubscribe(function (machineGroups) {
        $timeout(function () {
            
            delete $scope.machineGroups;
            $scope.machineGroups = machineGroups;
        }, 0, true);
    }, $scope, 'Machines Updated');

    // Production Group can be updated and if that happens the list must be updated to be reflected on the widget. 
    // Bug 11216
    messagingService.productionGroupUpdatedObservable.autoSubscribe(function (msg) {
        if (msg.Result === "Success") {
            messagingService.publish("GetMachineGroups");
            messagingService.publish("GetProductionGroups", "");
            $timeout(function () {
                
                delete $scope.machineGroups;
                $scope.machineGroups = machineGroupService.getMachineGroups();
            }, 0, true);
        }
    }, $scope, 'ProductionGroup Updated');

    $scope.openOpConsole = function (machineGroupId){
        var options = { machineGroupId: machineGroupId };
        if (!$state.params) {
            $state.params = {};
        }
        $state.params.bypassOperator = true;
        options.bypassOperator = true;
        machineGroupService.updateCurrentMachineGroup(machineGroupId);

        $state.go("OperatorConsole.Console", options);
    };

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'machineGroups',
        enableRowSelection: false,
        headerRowHeight: 26,
        rowHeight: 26,
        columnDefs: [
            {
                field: 'Alias',
                name: translationService.translate('Common.Label.Name'),
                width: 160
            },
            {
                field: 'CurrentStatus',
                name: translationService.translate('Common.Label.CurrentStatus'),
                cellFilter: 'machineGroupStatus'
            },
            {

                field: 'CurrentProductionStatus',
                name: translationService.translate('Common.Label.ProductionStatus'),
                cellFilter: 'machineProductionStatus'
            },
            {
                field: 'AssignedOperator',
                name: translationService.translate('Common.Label.Operator')
            },
            {
                field: 'ProductionMode',
                name: translationService.translate('Common.Label.ProductionMode'),
                cellFilter: 'machineGroupProductionMode'
            },
            {
                field: 'ProductionGroup.Alias',
                name: translationService.translate('ProductionGroup.Label.NameSingular')
            },
            {
                name: translationService.translate('Common.Label.Action'),
                displayName: '',
                cellTemplate: 'partials/MachineManager/CellTemplates/ConsoleIcon.html',
                width: 40
            }
        ],
        onRegisterApi: function (gridApi) {
            gridApi.openOpConsole = $scope.openOpConsole;
        }
    };
}