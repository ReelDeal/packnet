'use strict';
angular.module('dashboard.widgets.produciblesImportedMetricByDay', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Daily.ProducibleItemsImportedByDay';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsImportedByDay.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsImportedByDay.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByDay',
                controller: 'ProduciblesImportedByDayCtrl',
                templateUrl: 'partials/Widgets/produciblesImportedMetricsByDay.html',
                reload: true
            });
    });

app.controller("ProduciblesImportedByDayCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsImported = 0;

    messagingService.produciblesImportedByDayObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsImported;
            $scope.itemsImported = msg.Data.Value;
        }, 100);
    }, $scope, 'Producibles Imported By Day');

    messagingService.publish("ProducibleImportCountByDay", "");
});