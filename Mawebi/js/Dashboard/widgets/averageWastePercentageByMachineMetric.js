'use strict';
angular.module('dashboard.widgets.averageWastePercentageByMachineMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.AverageWastePercentageByMachine';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageWastePercentageByMachine.Label.Title',
                description: 'Dashboard.Widgets.AverageWastePercentageByMachine.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'AverageWastePercentageByMachineCtrl',
                templateUrl: 'partials/Widgets/averageWastePercentageByMachineMetric.html',
                edit: {
                    controller: 'AverageWastePercentageByMachineCtrl',
                    templateUrl: 'partials/Widgets/graphicEditDialogMetric.html'
                },
                resolve: {
                    data: function (config) {
                        if (!config.widgetHeight) {
                            config.widgetHeight = 300; //Default widget height before the user configures it.
                        }
                    }
                },
                reload: false
            });
    });

app.controller("AverageWastePercentageByMachineCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.averageWastePercentageByMachineData = {};
    $scope.averageWastePercentageByMachine = {
        labels : [],
        datasets : [
            {
                //light blue
                fillColor : "rgba(151,187,205,0.5)",
                strokeColor : "rgba(151,187,205,0.8)",
                highlightFill : "rgba(151,187,205,0.75)",
                highlightStroke : "rgba(151,187,205,1)",
                data : []
            }
        ],
    };
    $scope.options = {
        segmentShowStroke : false,
        animation: false,
        responsive : true,
        maintainAspectRatio : true,
    }
    var averageWastePercentageByMachineObservable = messagingService.messageSubject
        .where(messagingService.filterObs("AverageWastePercentageByMachine"));

    averageWastePercentageByMachineObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            var index = $scope.averageWastePercentageByMachine.labels.indexOf(msg.Data.Value.Key);
            if (index == -1) {
                //Item doesn't exists
                $scope.averageWastePercentageByMachine.labels.push(msg.Data.Value.Key);
                $scope.averageWastePercentageByMachine.datasets[0].data.push(msg.Data.Value.Value);
            }
            else {
                $scope.averageWastePercentageByMachine.datasets[0].data[index] = msg.Data.Value.Value;
            }
        }, 100);
    }, $scope, "Average Waste Percentage By Machine Metric");

    messagingService.publish("AverageWastePercentageByMachine", "");
});