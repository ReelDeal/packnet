'use strict';
angular.module('dashboard.widgets.potentialCartonsByCorrugate', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.PotentialCartonsByCorrugate';
        dashboardProvider
            .widget(id, {
                id: id,
                title: id+'.Title',
                dashboard: 'monitoringMgmtCtrl',
                category: 'Metrics',
                description: id+'.Description',
                controller: 'PotentialCartonsByCorrugateCtrl',
                reload: true,
                templateUrl: 'partials/Widgets/potentialCartonsByCorrugate.html',
            });
    });

app.controller("PotentialCartonsByCorrugateCtrl", function ($scope, messagingService, $timeout) {
    $timeout = $timeout.getInstance($scope);
    
    //$scope.cartonCountByCorrugate = {};
    $scope.cartonCountByCorrugate = {
        labels: [],
        datasets: [
            {
                //light blue
                fillColor: "rgba(151,187,205,0.5)",
                strokeColor: "rgba(151,187,205,0.8)",
                highlightFill: "rgba(151,187,205,0.75)",
                highlightStroke: "rgba(151,187,205,1)",
                data: []
            }
        ],
    };
    
    $scope.options = {
        segmentShowStroke: false,
        animation: false,
        responsive: true,
        maintainAspectRatio: true,
    }

    var potentialCartonCountByCorrugateObservable = messagingService.messageSubject
        .where(messagingService.filterObs("CartonsThatCanBeProducedOnCorrugates"));

    var updateUI = function (msg){
        $timeout(function () {
            Enumerable.From(msg.Data).ForEach(function (item) {
                var index = $scope.cartonCountByCorrugate.labels.indexOf(item.Key);
                if (index === -1) {
                    //Item doesn't exists
                    $scope.cartonCountByCorrugate.labels.push(item.Key);
                    $scope.cartonCountByCorrugate.datasets[0].data.push(item.Value);
                }
                else {
                    $scope.cartonCountByCorrugate.datasets[0].data[index] = item.Value;
                }
            });
        }, 0);
    }
    
    //Take the first message so we don't get a 2 second lag
    potentialCartonCountByCorrugateObservable
            .first()
            .autoSubscribe(function (msg) {
                updateUI(msg);
            }, $scope, "potentialCartonCountByCorrugateObservable");

    //Limit the times we update the UI (only once every 2 seconds)
    potentialCartonCountByCorrugateObservable
        .skip(1)
        .sample(2000)
        .autoSubscribe(function (msg) {
            updateUI(msg);
        }, $scope, "potentialCartonCountByCorrugateObservable");

    messagingService.publish("GetCartonsThatCanBeProducedOnCorrugates");
})