﻿'use strict';
angular.module('dashboard.widgets.classifications', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.Classifications';
        dashboardProvider
            .widget(id, {
                id:id,
                title: 'Dashboard.Widgets.Classifications.Label.Title',
                description: 'Dashboard.Widgets.Classifications.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'ClassificationsCtrl',
                templateUrl: 'partials/MachineManager/dashboardWidgetTableTemplate.html',
                reload: true
            });
    });