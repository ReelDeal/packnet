﻿'use strict';
angular.module('dashboard.widgets.failedrequests', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.Top20InvalidRequests';
        dashboardProvider
            .widget(id, {
                id: id,
                title: id+'.Title',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                description: id+'.Description',
                controller: 'FailedRequestsCtrl',
                reload: true,
                templateUrl: 'partials/MachineManager/dashboardWidgetTableTemplate.html',
            });
    });