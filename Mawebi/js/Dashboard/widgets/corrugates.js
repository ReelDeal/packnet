﻿'use strict';
angular.module('dashboard.widgets.corrugates', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.Corrugates';
        dashboardProvider
            .widget(id, {
                id: id,
                title: id+'.Title',
                category: 'Metrics',
                dashboard: 'dashboardMgmt',
                description: id+'.Description',
                controller: 'CorrugatesCtrl',
                reload: true,
                templateUrl: 'partials/MachineManager/dashboardWidgetTableTemplate.html',
            });
    });