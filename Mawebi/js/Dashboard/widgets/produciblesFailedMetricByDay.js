'use strict';
angular.module('dashboard.widgets.produciblesFailedMetricByDay', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Daily.ProducibleItemsFailedByDay';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsFailedByDay.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsFailedByDay.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByDay',
                controller: 'ProduciblesFailedByDayCtrl',
                templateUrl: 'partials/Widgets/produciblesFailedMetricsByDay.html',
                reload: true
            });
    });

app.controller("ProduciblesFailedByDayCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsFailed = 0;

    messagingService.produciblesFailedByDayObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsFailed;
            $scope.itemsFailed = msg.Data.Value;
        }, 100);
    }, $scope, 'Producibles Failed By Day');

    messagingService.publish("ProducibleFailedCountByDay", "");
});