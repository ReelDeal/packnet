angular.module('dashboard.widgets.corrugateStatus', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.CorrugateStatus';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.CorrugateStatus.Label.Title',
                description: 'Dashboard.Widgets.CorrugateStatus.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'CorrugateStatusCtrl',
                templateUrl: 'partials/Widgets/corrugateStatus.html',
                reload: true
            });
    });