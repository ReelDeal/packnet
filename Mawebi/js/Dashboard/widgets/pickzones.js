﻿'use strict';
angular.module('dashboard.widgets.pickzones', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id ='Dashboard.Widgets.General.PickZones';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.PickZones.Label.Title',
                description: 'Dashboard.Widgets.PickZones.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'Metrics',
                controller: 'PickZonesWidgetCtrl',
                templateUrl: 'partials/MachineManager/dashboardWidgetTableTemplate.html',
                reload: true
            });
    });