'use strict';
angular.module('dashboard.widgets.produciblesCreatedMetricByDay', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Daily.ProducibleItemsCreatedByDay';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.ProducibleItemsCreatedByDay.Label.Title',
                description: 'Dashboard.Widgets.ProducibleItemsCreatedByDay.Label.Description',
                dashboard: 'usageMgmt',
                category: 'MetricsByDay',
                controller: 'ProduciblesCreatedByDayCtrl',
                templateUrl: 'partials/Widgets/produciblesCreatedMetricsByDay.html',
                reload: true
            });
    });

app.controller("ProduciblesCreatedByDayCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsCreated = 0;

    messagingService.produciblesCreatedByDayObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.itemsCreated;
            $scope.itemsCreated = msg.Data.Value;
        },100);
    }, $scope, 'Producibles Created By Day');

    messagingService.publish("ProducibleCreatedCountByDay", "");
})