'use strict';
angular.module('dashboard.widgets.produciblesFailedOverTime', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Indefinite.ProduciblesFailedOverTime';
        dashboardProvider
            .widget(id, {
                id: id,
                title: id+'.Title',
                dashboard: 'usageMgmt',
                description: id+'.Description',
                controller: 'produciblesFailedOverTimeCtrl',
                reload: true,
                templateUrl: 'partials/Widgets/produciblesFailedOverTime.html',
            });
    });


app.controller("produciblesFailedOverTimeCtrl", function ($scope, messagingService, $timeout) {
    $timeout = $timeout.getInstance($scope);
    var clickTips, hoverTip, plot;
    var failData = [[getDateMilliseconds(new Date().getFullYear(), (new Date().getMonth()), (new Date().getDate() - 2)), 2], [getDateMilliseconds(new Date().getFullYear(), (new Date().getMonth()), (new Date().getDate() - 1)), 1]];

    messagingService.produciblesFailedObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            var d = new Date();
            addFailure(getDateMilliseconds(d.getFullYear(), d.getMonth(), d.getDate()), msg.Data.Value);
        }, 100);
    }, $scope, 'Producibles Failed Over Time');

    messagingService.publish("ProducibleFailedCount", "");

    var dataset = [
        {
            label: "YOU HAVE FAILED",
            data: failData
        }
    ];

    var options = {
        grid: {
            hoverable: true,
            clickable: true,
            autoHighlight: true
        },
        xaxis: {
            mode: "time"
        },
        yaxis: { min: 0 },
        series: {
            lines: {
                show: true,
                fill: true
            },
            points: {
                radius: 5,
                show: true
            }
        }
    };

    $(window).resize(function () {
        drawGraph();
    });

    $(document).ready(function () {
        drawGraph();
    });

    function drawGraph() {
        clearTooltips();
        plot = $.plot($('#produciblesFailedOverTime-div'), dataset, options);
        bindEvents(plot);
    }

    //plotting time requires the data represented as milliseconds
    function getDateMilliseconds(year, month, day) {
        return new Date(year, month , day).getTime();
    }

    function addFailure(date, fails) {
        var dateExisted = false;
        for (var i = 0; i < failData.length; i++) {
            if (failData[i][0] == date && !dateExisted) {
                dateExisted = true;
                failData[i][1] = parseInt(fails);
            }
        }
        if (!dateExisted) failData.push([date, parseInt(fails)]);
        drawGraph();
    }

    function toolTipHTML(stat, series) {
        var html = '';
        html += '<div class="tooltip">';
        html += '<div>';
        if (series) html += '<span class="series">' + series + '</span>';
        html += '<span class="stats">' + stat + '</span>';
        html += '</div>';
        html += '</div>';
        return html;
    }

    function clearTooltips() {
        if (hoverTip) hoverTip.remove();
        if (clickTips) $.each(clickTips, function (i, t) { t.remove(); });
        hoverTip = null;
        clickTips = null;
    }

    function bindEvents(plot) {
        $('#produciblesFailedOverTime-div').on('plothover', function (event, pos, item) {
            var ofs = { height: 0, width: 0 },
                fmtd;
            if (clickTips) return;
            clearTooltips();
            if (item) {
                fmtd = item.series.data[item.dataIndex][1];
                hoverTip = $(toolTipHTML(fmtd, item.series.label));
                $('#wrapper').append(hoverTip);
                ofs.height = hoverTip.outerHeight();
                ofs.width = hoverTip.outerWidth();
                hoverTip.offset({ left: item.pageX - ofs.width / 2, top: item.pageY - ofs.height - 15 });
            }
        });

        $('#produciblesFailedOverTime-div').on('plotclick', function (event, pos, item) {
            var x = 0,
                ttip,
                fmtd,
                dp,
                pz,
                tmp,
                xtickl,
                ofs = { pageX: 0, pageY: 0, height: 0, width: 0, plotX: 0, plotY: 0 },
                axis = plot.getAxes(),
                series = plot.getData(),
                xcnt = series[0].data.length;
            plot.unhighlight();
            clearTooltips();
            if (item) {
                clickTips = [];
                tmp = $('#produciblesFailedOverTime-div').offset();
                ofs.plotX = tmp.left;
                ofs.plotY = tmp.top;
                tmp = plot.getPlotOffset();
                ofs.plotX += tmp.left;
                ofs.plotY += tmp.top;
                xtickl = Math.ceil(xcnt / 12);
                for (; x < xcnt; x += xtickl) {
                    plot.highlight(item.seriesIndex, x);
                    pz = series[item.seriesIndex].datapoints.pointsize;
                    dp = [series[item.seriesIndex].datapoints.points[x * pz], series[item.seriesIndex].datapoints.points[x * pz + 1]];
                    fmtd = series[item.seriesIndex].data[x][1] + " <br/> " + (new Date(series[item.seriesIndex].data[x][0]).toDateString());
                    ofs.pageX = parseInt(axis.xaxis.p2c(dp[0]) + ofs.plotX);
                    ofs.pageY = parseInt(axis.yaxis.p2c(dp[1]) + ofs.plotY);
                    ttip = $(toolTipHTML(fmtd, (item.dataIndex == x ? item.series.label : null)));
                    $('#wrapper').append(ttip);
                    ofs.width = ttip.outerWidth();
                    ofs.height = ttip.outerHeight();
                    ttip.offset({ left: ofs.pageX - ofs.width / 2, top: ofs.pageY - ofs.height - 15 });
                    clickTips[x] = ttip;
                }
            }
        });


    }
});
