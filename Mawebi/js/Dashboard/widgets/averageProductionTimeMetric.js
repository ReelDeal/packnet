'use strict';
angular.module('dashboard.widgets.averageProductionTimeMetric', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.General.AverageProductionTime';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageProductionTime.Label.Title',
                description: 'Dashboard.Widgets.AverageProductionTime.Label.Description',
                dashboard: 'dashboardMgmt',
                category : 'Metrics',
                controller: 'AverageProductionTimeCtrl',
                templateUrl: 'partials/Widgets/averageProductionTimeMetric.html',
                reload : true
            });
    });

app.controller("AverageProductionTimeCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsCreated = 0;

    messagingService.averageProductionTimeObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.averageProductionTime;
            $scope.averageProductionTime = msg.Data.Value.Value;
        },100);
    }, $scope, "Average Production Time Metric");

    messagingService.publish("AverageProductionTime", "");
})