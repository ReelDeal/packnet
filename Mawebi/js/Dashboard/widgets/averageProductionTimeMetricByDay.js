'use strict';
angular.module('dashboard.widgets.averageProductionTimeMetricByDay', ['adf.provider'])
    .config(function (dashboardProvider) {
        var id = 'Dashboard.Widgets.Daily.AverageProductionTimeByDay';
        dashboardProvider
            .widget(id, {
                id: id,
                title: 'Dashboard.Widgets.AverageProductionTimeByDay.Label.Title',
                description: 'Dashboard.Widgets.AverageProductionTimeByDay.Label.Description',
                dashboard: 'dashboardMgmt',
                category: 'MetricsByDay',
                controller: 'AverageProductionTimeByDayCtrl',
                templateUrl: 'partials/Widgets/averageProductionTimeMetricByDay.html',
                reload : true
            });
    });

app.controller("AverageProductionTimeByDayCtrl", function ($scope, $timeout, messagingService) {
    $timeout = $timeout.getInstance($scope);
    $scope.itemsCreated = 0;

    messagingService.averageProductionTimeByDayObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            delete $scope.averageProductionTime;
            $scope.averageProductionTime = msg.Data.Value.Value;
        }, 100);
    }, $scope, "Average Production Time Metric By Day");

    messagingService.publish("AverageProductionTimeByDay", "");
});