function SupportIncidentCtrl($scope, $log, $window, messagingService) {
    
    $scope.customerInformation = '';
    $scope.$log = $log.getInstance('Support Incident Ctrl');
    $scope.userAgent = '';
    $scope.cookiesEnabled = '';
    $scope.language = '';
    $scope.languages = [];
    $scope.platform = '';
    $scope.browserPlugins = [];
    $scope.vendor = '';
    $scope.product = '';

    $scope.saveReport = function () {
        var obj = {};
        obj.UserAgent = $scope.userAgent;
        obj.cookiesEnabled = $scope.cookiesEnabled;
        obj.Language = $scope.language;
        obj.Languages = $scope.languages;
        obj.Platform = $scope.platform;
        obj.browserPlugins = $scope.browserPlugins;
        obj.Vendor = $scope.vendor;
        obj.Product = $scope.product;
        //obj.UserMessage = $scope.customerInformation;
        obj.ReportingUser = $scope.currentUser.UserName;

        messagingService.publish('CreateSupportIncident', obj);
    }

    $scope.viewMessage = function (incidentId) {
        var unSentEnumerable = Enumerable.From($scope.unSentIncidents);

        var unsentItem = unSentEnumerable.FirstOrDefault(undefined, function (incident) {
            return incident.Id == incidentId;
        });

        var messageToSend = angular.toJson(unsentItem, true);
        
        delete $scope.messageContents;
        $scope.messageContents = messageToSend;
    }

    $scope.getUnsentIncidents = function () {
        messagingService.publish('GetAllUnsentSupportIncidents');
    };

    $scope.unSentIncidentsResult = function (data) {
        
        delete $scope.unSentIncidents;
        $scope.unSentIncidents = data.Data;
    }

    $scope.supportIncidentCreated = function (data) {
        if (!data.Data)
            return;

        var updateObj = {};
        updateObj.UiLogMessages = [];
        updateObj.IncidentToUpdate = data.Data;
        updateObj.UserMessage = "";

        for (var i = 0; i < window.logMessages.length; i++) {
            updateObj.UiLogMessages.push(window.logMessages[i]);

            if (i !== 0 && i % 500 === 0) {
                messagingService.publish('SupportIncidentUpdateLogEntry', updateObj);
                updateObj.UiLogMessages = [];
            }
        }

        if (updateObj.UiLogMessages.length > 0) {
            messagingService.publish('SupportIncidentUpdateLogEntry', updateObj);
            updateObj.UiLogMessages = [];
        }

        for (var j = 0; j < $scope.customerInformation.length; j++) {
            updateObj.UserMessage += $scope.customerInformation[j];

            if (j !== 0 && j % 500 === 0) {
                messagingService.publish('SupportIncidentUpdateUserMessage', updateObj);
                updateObj.UserMessage = "";
            }
        }

        if (updateObj.UserMessage) {
            messagingService.publish('SupportIncidentUpdateUserMessage', updateObj);
            updateObj.UserMessage = "";
        }

        messagingService.publish('SupportIncidentFinished', updateObj);
    }

    if (messagingService.IsSignalRConnected()) {
        $scope.getUnsentIncidents();
    } else {
        $scope.signalRConnectedObservable.autoSubscribe($scope.getUnsentIncidents, $scope, 'SignalR Connected');
    }

    messagingService.supportIncidentCreatedObservable.autoSubscribe($scope.supportIncidentCreated, $scope, 'Support Incident Created');
    messagingService.allUnsentSupportIncidents.autoSubscribe($scope.unSentIncidentsResult, $scope, 'All unsent support incidents');

    var navigator = $window.navigator;
    
    $scope.userAgent = navigator.userAgent;
    $scope.cookiesEnabled = navigator.cookieEnabled;
    $scope.language = navigator.language;
    $scope.languages = navigator.languages;
    $scope.platform = navigator.platform;
    $scope.browserPlugins = Enumerable.From(navigator.plugins).Select(function (p) { return p.name; }).Distinct().ToArray();
    $scope.vendor = navigator.vendor;
    $scope.product = navigator.product;
}