function SupportCtrl($scope, $timeout, messagingService, authenticationService, authorizationService, USER_ROLES) {
    $timeout = $timeout.getInstance($scope);
    
    $scope.messages = [];
    $scope.backupMessagesHeader = "";
    $scope.restoreMessagesHeader = "";
    $scope.showLogs = true;
    $scope.backingUp = false;
    $scope.restoring = false;
    $scope.databaseRestoreFiles = [];
    $scope.selectedFile = "";
    $scope.fileDropDownToggled = false;

    $scope.showDatabaseBackup = (authenticationService.isAuthenticated() && authorizationService.isAuthorized([USER_ROLES.admin]));

    var databaseRestoreFilesResponseObservable = messagingService.messageSubject
        .where(messagingService.filterObs("DatabaseRestoreFilesResponse"));

    var databaseActionInProgressObservable = messagingService.messageSubject
        .where(messagingService.filterObs("DatabaseActionInProgress"));

    var databaseActionCompleteObservable = messagingService.messageSubject
        .where(messagingService.filterObs("DatabaseActionComplete"));

    databaseActionInProgressObservable.autoSubscribe(function (data) {
        $scope.messages.unshift(data.Message);
    }, $scope, "Database Action InProgress");

    databaseActionCompleteObservable.autoSubscribe(function (data) {
        $scope.messages.unshift(data.Message);
        if ($scope.restoring) {
            
            delete $scope.restoreMessagesHeader;
            $scope.restoreMessagesHeader = "Restore Complete.";
            $scope.restoring = false;
        }
        else {
            
            delete $scope.backupMessagesHeader;
            $scope.backupMessagesHeader = "Backup Complete. File: " + msg.Data.Path;
            $scope.backingUp = false;
        }
    }, $scope, "Database Action Complete");

    databaseRestoreFilesResponseObservable.autoSubscribe(function (data) {
        
        delete $scope.databaseRestoreFiles;
        $scope.databaseRestoreFiles = data;
        
        delete $scope.selectedFile;
        $scope.selectedFile = data[0];
    }, $scope, "Database restore files");

    $scope.selectFile = function (fileItem) {
        
        delete $scope.selectedFile;
        $scope.selectedFile = fileItem;
    }

    $scope.requestBackup = function () {
        if ($scope.backingUp) return;

        $scope.backingUp = true;
        $scope.showLogs = true;
        
        delete $scope.messages;
        $scope.messages = [];
        messagingService.publish("DatabaseBackupRequested");
        $timeout(function () { }, 0, true);
    }

    $scope.restoreDatabase = function () {
        if ($scope.restoring) return;

        $scope.restoring = true;
        var data = {};
        data.Item1 = $scope.selectedFile.Item2;
        data.Item2 = true;

        messagingService.publish("RestoreDatabase", data);
    }

    messagingService.publish("DatabaseRestoreFilesRequested");
}
