﻿app.config(function ($stateProvider, $urlRouterProvider, USER_ROLES, Machines_Constants) {
    $urlRouterProvider.otherwise("/Connecting");
    $stateProvider
        .state('Connecting', {
            url : "/Connecting",
            templateUrl : 'partials/connecting.html',
            controller : 'ConnectingCtrl',
            onEnter : ['$rootScope', function ($rootScope){ clearAlerts($rootScope); }],
            data : {
                authorizedRoles : [USER_ROLES.all]
            }
        })
        .state('LogOut', {
            url: "/LogOut",
            templateUrl: 'partials/Operator/selectMachineGroup.html',
            controller: 'SelectMachineGroupCtrl',
            onEnter: [
                '$rootScope', 'authenticationService', function ($rootScope, authenticationService) {
                    clearAlerts($rootScope);
                    authenticationService.logout();
                }
            ],
            data : {
                authorizedRoles : [USER_ROLES.all]
            }
        })
        .state('Preferences', {
            url : "/Preferences",
            templateUrl : 'partials/UserManagement/preferences.html',
            controller : 'PreferencesCtrl',
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ],
            data : {
                authorizedRoles : [USER_ROLES.operator, USER_ROLES.admin]
            }
        })

        // ##########
        // Operator Console
        // ##########
        .state('OperatorConsole', {
            abstract: true,
            url: "/OperatorConsole",
            template: '<ui-view/>',
            data : {
                authorizedRoles : [USER_ROLES.operator, USER_ROLES.admin]
            }
        })
        .state('OperatorConsole.Console', {
            url : "/Console?machineGroupId&userToken&bypassOperator",
            templateUrl : 'partials/Operator/operatorConsole.html',
            controller : 'OperatorPanelCtrl',
            data : {
                directlyAccessible : true
            },
            onEnter : [
                '$rootScope', '$state', '$timeout', 'machineGroupService', 'alertService', 'translationService', function ($rootScope, $state, $timeout, machineGroupService, alertService, translationService) {
                    clearAlerts($rootScope);
                    ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, "OperatorConsole.Console", $timeout, alertService, ($state.params && $state.params.bypassOperator === true));
                }
            ]
        })
		.state('OperatorConsole.ChangeProductionGroup', {
		    url: "/ChangeProductionGroup",
		    templateUrl: 'partials/Operator/changeProductionGroup.html',
		    controller: 'ChangeProductionGroupCtrl',
		    onEnter: [
		        '$rootScope', '$state', '$timeout', 'machineGroupService', 'alertService', 'translationService', function ($rootScope, $state, $timeout, machineGroupService, alertService, translationService) {
		            clearAlerts($rootScope);
		            ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, 'OperatorConsole.ChangeProductionGroup', $timeout, alertService, false);
		        }]
		})
		.state('OperatorConsole.ChangeCorrugates', {
		    url: "/ChangeCorrugates",
		    templateUrl: 'partials/Operator/changeCorrugates.html',
		    controller: 'ChangeCorrugatesCtrl',
		    onEnter: [
		        '$rootScope', '$state', '$timeout', 'machineGroupService', 'alertService', 'translationService', function ($rootScope, $state, $timeout, machineGroupService, alertService, translationService) {
		            clearAlerts($rootScope);
		            ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, 'OperatorConsole.ChangeCorrugates', $timeout, alertService, false);
		        }],
		    data: {
		        directlyAccessible: true
		    }
		})
        .state('OperatorConsole.CreateFromArticleNew', {
            abstract: true,
            url: "/CreateFromArticleNew",
            templateUrl: 'partials/Operator/CartonCreation/Article/createFromArticleNew.html'
        })
        .state('OperatorConsole.CreateFromArticleNew.list', {
            url: "",
            templateUrl: 'partials/Operator/CartonCreation/Article/createFromArticleList.html',
            onEnter: [
                '$rootScope', '$state', '$timeout', 'machineGroupService', 'alertService', 'translationService', function ($rootScope, $state, $timeout, machineGroupService, alertService, translationService) {
                    ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, 'OperatorConsole.CreateFromArticleNew.list', $timeout, alertService, false);
                }
            ],
            data: {
                directlyAccessible: true
            }
        })
        .state('OperatorConsole.CreateFromArticleNew.carton', {
            url: "/Carton",
            templateUrl: 'partials/Articles/Production/cartonForm.html',
            onEnter: [
                '$rootScope', function ($rootScope) {
                    clearAlerts($rootScope);
                }
            ],
            data: {
                directlyAccessible: false
            }
        })
        .state('OperatorConsole.CreateFromArticleNew.label', {
            url: "/Label",
            templateUrl: 'partials/Articles/Production/labelForm.html',
            onEnter: [
                '$rootScope', function ($rootScope) {
                    clearAlerts($rootScope);
                }
            ],
            data: {
                directlyAccessible: false
            }
        })
		.state('OperatorConsole.CreateCustomJob', {
		    url: "/CreateCustomJob",
		    templateUrl: 'partials/Operator/CartonCreation/CustomJob/createFromDimentions.html',
		    controller: 'CustomJobCtrl',
		    onEnter: [
                '$rootScope', '$state', '$timeout', 'machineGroupService', 'alertService', 'translationService', function ($rootScope, $state, $timeout, machineGroupService, alertService, translationService) {
                    clearAlerts($rootScope);
                    ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, 'OperatorConsole.CreateCustomJob', $timeout, alertService, false);
                }],
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('OperatorConsole.MachineCommands', {
		    url: "/MachineCommands",
		    templateUrl: 'partials/Operator/machineCommands.html',
		    controller: 'MachineCommandsCtrl',
		    onEnter: [
                '$rootScope', '$state', '$timeout', 'machineGroupService', 'alertService', 'translationService', function ($rootScope, $state, $timeout, machineGroupService, alertService, translationService) {
                    clearAlerts($rootScope);
                    ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, 'OperatorConsole.MachineCommands', $timeout, alertService, false);
                }]
		})
		.state('SelectMachineGroup', {
		    url: "/SelectMachineGroup",
		    templateUrl: 'partials/Operator/selectMachineGroup.html',
		    controller: 'SelectMachineGroupCtrl',
		    onEnter: ['$rootScope', function ($rootScope) {
		        clearAlerts($rootScope);
		    }],
		    params: {
		        endState: "OperatorConsole.Console"
		    },
		    data: {
		        authorizedRoles: [USER_ROLES.operator, USER_ROLES.admin],
		        directlyAccessible: true
		    }
		})

		// ############
		// MachineService
        // ############
        .state('MachineService', {
            url : "/Service",
            templateUrl : 'partials/MachineService/MachineServiceMain.html',
            controller : 'MachineServiceMainCtrl',
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ],
            data : {
                authorizedRoles : [USER_ROLES.admin],
                directlyAccessible : true
            }
        })
        .state('FusionMachineService', {
            url : "/FusionService?machineId",
            templateUrl : 'partials/MachineService/FusionMachineService.html',
            controller : 'MachineServiceCtrl',
            data : {
                directlyAccessible : false,
                authorizedRoles : [USER_ROLES.admin]
            },
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ]
        })
        .state('EmMachineService', {
            url : "/EmService?machineId",
            templateUrl : 'partials/MachineService/EmMachineService.html',
            controller : 'MachineServiceCtrl',
            params : ['machine'],
            data : {
                directlyAccessible : false,
                authorizedRoles : [USER_ROLES.admin]
            },
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ]
        })

		// ############
		// Admin Console
		// ############
		.state('AdminConsole', {
		    url: "/AdminConsole",
		    templateUrl: 'partials/Dashboard/dashboard.html',
		    controller: 'dashboardMgmtCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('Dashboard', {
		    url: "/Dashboard",
		    templateUrl: 'partials/Dashboard/dashboard.html',
		    controller: 'dashboardMgmtCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('Monitoring', {
		    url: "/Monitoring",
		    templateUrl: 'partials/Dashboard/dashboard.html',
		    controller: 'monitoringMgmtCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.operator, USER_ROLES.admin]
		    }
		})
		.state('Usage', {
		    url: "/Usage",
		    templateUrl: 'partials/Dashboard/dashboard.html',
		    controller: 'usageMgmtCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.operator, USER_ROLES.admin]
		    }
		})
		.state('Setup', {
		    url: "/Setup",
		    templateUrl: 'partials/Setup/Setup.html',
		    controller: 'SetupCtrl',
		    onEnter: [
			  '$rootScope', function ($rootScope) {
			      clearAlerts($rootScope);
			  }],
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
        .state('SearchBoxFirst', {
            url: "/SearchBoxFirst",
            templateUrl: 'partials/SelectionAlgorithm/Box First/search.html',
            controller: 'BoxFirstSearchController',
            onEnter: [
			  '$rootScope', function ($rootScope) {
			      clearAlerts($rootScope);
			  }],
            data: {
                authorizedRoles: [USER_ROLES.operator, USER_ROLES.admin]
            }
        })
		.state('SearchOrders', {
		    url: "/SearchOrders",
		    templateUrl: 'partials/SelectionAlgorithm/Orders/search.html',
		    controller: 'OrderSearchController',
		    onEnter: [
			  '$rootScope', function ($rootScope) {
			      clearAlerts($rootScope);
			  }],
		    data: {
		        authorizedRoles: [USER_ROLES.operator, USER_ROLES.admin]
		    }
		})
		.state('SearchBoxLast', {
		    url: "/SearchBoxLast",
		    templateUrl: 'partials/SelectionAlgorithm/Box Last/search.html',
		    controller: 'BoxLastSearchController',
		    onEnter: [
			  '$rootScope', function ($rootScope) {
			      clearAlerts($rootScope);
			  }],
		    data: {
		        authorizedRoles: [USER_ROLES.operator, USER_ROLES.admin]
		    }
		})//
		.state('SearchScanToCreate', {
		    url: "/SearchScanToCreate",
		    templateUrl: 'partials/SelectionAlgorithm/Scan To Create/search.html',
		    controller: 'ScanToCreateSearchController',
		    onEnter: [
			  '$rootScope', function ($rootScope) {
			      clearAlerts($rootScope);
			  }],
		    data: {
		        authorizedRoles: [USER_ROLES.operator, USER_ROLES.admin]
		    }
		})
		.state('Settings', {
		    url: "/Settings",
		    templateUrl: 'partials/Settings/packNetServerSettings.html',
		    controller: 'PackNetServerSettingsCtrl',
		    onEnter: [
			  '$rootScope', function ($rootScope) {
			      clearAlerts($rootScope);
			  }],
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})

		// ##########
        // Articles Page
        // ##########
        .state('ArticlesNew', {
            abstract : true,
            url : "/ArticlesNew",
            templateUrl : 'partials/Articles/articles.html',
            data : {
                authorizedRoles : [USER_ROLES.admin]
            }
        })
        .state('ArticlesNew.list', {
            url : "",
            templateUrl : 'partials/Articles/articlesList.html',
            data : {
                directlyAccessible : true
            }
        })
        .state('ArticlesNew.carton', {
            url : "/Carton",
            templateUrl : 'partials/Articles/cartonForm.html',
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ],
            data : {
                directlyAccessible : false
            }
        })
        .state('ArticlesNew.label', {
            url : "/Label",
            templateUrl : 'partials/Articles/labelForm.html',
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ],
            data : {
                directlyAccessible : false
            }
        })
        .state('ArticlesNew.kit', {
            url : "/Kit",
            templateUrl : 'partials/Articles/kitForm.html',
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ],
            data : {
                directlyAccessible : false
            }
        })
        .state('ArticlesNew.import', {
            url: "/Import",
            templateUrl: 'partials/Articles/articleImport.html',
            onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
            data: {
                directlyAccessible: false
            }
        })

        // ##########
        // Article Production
        // ##########
        .state('ArticlesProductionNew', {
            abstract : true,
            url : "/ArticlesProductionNew",
            templateUrl : 'partials/Articles/Production/articles.html',
            data : {
                authorizedRoles : [USER_ROLES.admin]
            }
        })
        .state('ArticlesProductionNew.list', {
            url : "",
            templateUrl : 'partials/Articles/Production/articlesList.html',
            data : {
                directlyAccessible : false
            },
            params : {
                administratorView : null
            }
        })
        .state('ArticlesProductionNew.carton', {
            url : "/Carton",
            templateUrl : 'partials/Articles/Production/cartonForm.html',
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ],
            data : {
                directlyAccessible : false
            }
        })
        .state('ArticlesProductionNew.label', {
            url : "/Label",
            templateUrl : 'partials/Articles/Production/labelForm.html',
            onEnter : [
                '$rootScope', function ($rootScope){
                    clearAlerts($rootScope);
                }
            ],
            data : {
                directlyAccessible : false
            }
        })

        // ##########
		// Carton Property Groups Page
		// ##########
		.state('CartonPropertyGroups', {
		    abstract: true,
		    url: "/CartonPropertyGroups",
		    templateUrl: 'partials/CartonPropertyGroups/cartonPropertyGroups.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('CartonPropertyGroups.list', {
		    url: "",
		    templateUrl: 'partials/CartonPropertyGroups/cartonPropertyGroupsList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('CartonPropertyGroups.new', {
		    url: "New",
		    templateUrl: 'partials/CartonPropertyGroups/newCartonPropertyGroup.html',
		    onEnter: ['$rootScope', function ($rootScope) {
		        clearAlerts($rootScope);
		    }],
		    data: {
		        directlyAccessible: false
		    }
		})

		// ##########
		// Production Groups Page
		// ##########
		.state('ProductionGroups', {
		    abstract: true,
		    url: "/ProductionGroups",
		    templateUrl: 'partials/ProductionGroups/productionGroups.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('ProductionGroups.list', {
		    url: "",
		    templateUrl: 'partials/ProductionGroups/productionGroupsList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('ProductionGroups.new', {
		    url: "New",
		    templateUrl: 'partials/ProductionGroups/newProductionGroup.html',
		    onEnter: ['$rootScope', 'messagingService', function ($rootScope, messagingService) {
		        clearAlerts($rootScope);
		        messagingService.publish("GetCorrugates");
		        messagingService.publish("GetMachineGroups");
		    }],
		    data: {
		        directlyAccessible: false
		    }
		})

		// ##########
		// Machine Groups Page
		// ##########
		.state('MachineGroups', {
		    abstract: true,
		    url: "/MachineGroups",
		    templateUrl: 'partials/MachineGroups/machineGroups.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('MachineGroups.list', {
		    url: "",
		    templateUrl: 'partials/MachineGroups/machineGroupsList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('MachineGroups.new', {
		    url: "New",
		    templateUrl: 'partials/MachineGroups/newMachineGroup.html',
		    onEnter: ['$rootScope', 'messagingService', function ($rootScope, messagingService) {
		        clearAlerts($rootScope);
		        messagingService.publish("GetMachines");
		    }],
		    data: {
		        directlyAccessible: false
		    }
		})

		// ##########
		// Machines Page
		// ##########
		.state('Machines', {
		    abstract: true,
		    url: "/Machines",
		    templateUrl: 'partials/Machines/machines.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('Machines.list', {
		    url: "",
		    templateUrl: 'partials/Machines/machinesList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
        .state('Machines.discoverBrMachines_Fusion', {
            url : "DiscoverBrMachines_Fusion",
            templateUrl : 'partials/Machines/machinesDiscoverBrMachines.html',
            data : {
                directlyAccessible : false
            }
        })
        .state('Machines.discoverBrMachines_Em', {
            url : "DiscoverBrMachines_Em",
            templateUrl : 'partials/Machines/machinesDiscoverBrMachines.html',
            data : {
                directlyAccessible : true
            }
        })
		.state('Machines.newFusion', {
		    url: "/NewFusion",
		    params: { selectedMachine: null },
		    templateUrl: 'partials/Machines/Fusion/newFusionMachine.html',
		    controller: 'FusionMachinesCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})
        .state('Machines.newFootPedal', {
            url : "NewFootPedal",
            params : {
                selectedDevice : null,
                hiddenParam : 'YES'
            },
            templateUrl : 'partials/Machines/FootPedal/newFootPedal.html',
            controller : 'FootPedalCtrl',
            onEnter : ['$rootScope', function ($rootScope){ clearAlerts($rootScope); }],
            data : {
                directlyAccessible : false
            }
        })
        .state('Machines.newConveyor', {
            url : "NewConveyor",
            params : {
                selectedDevice : null,
                hiddenParam : 'YES'
            },
            templateUrl : 'partials/Machines/Conveyor/newConveyor.html',
            controller: 'ConveyorCtrl',
            onEnter : ['$rootScope', function ($rootScope){ clearAlerts($rootScope); }],
            data : {
                directlyAccessible : true
            }
        })
		.state('Machines.newEm', {
		    url: "NewEm",
		    params: { selectedMachine: null },
		    templateUrl: 'partials/Machines/Em/newEmMachine.html',
		    controller: 'EmMachinesCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})
		.state('Machines.newZebra', {
		    url: "NewZebra",
		    templateUrl: 'partials/Machines/ZebraPrinter/newZebraPrinterMachine.html',
		    controller: 'ZebraPrinterMachinesCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})
		.state('Machines.newBarcodeScanner', {
		    url: "NewBarcodeScanner",
		    templateUrl: 'partials/Machines/BarcodeScanner/newBarcodeScannerMachine.html',
		    controller: 'BarcodeScannerMachinesCtrl',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})
        .state('Machines.newExternalSystem', {
            url : "NewExternalSystem",
            templateUrl : 'partials/Machines/ExternalSystem/newExternalSystem.html',
            controller : 'ExternalSystemsCtrl',
            onEnter : ['$rootScope', function ($rootScope){ clearAlerts($rootScope); }],
            data : {
                directlyAccessible : false
            }
        })
        .state('Machines.emPhysicalMachineSettings', {
            url: "/EmPhysicalMachineSettings",
            templateUrl: 'partials/PhysicalMachine/EmPhysicalMachineSettings.html',
            controller: 'EmPhysicalMachineSettingsCtrl',
            onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
            data: {
                directlyAccessible: false
            }
        })
        .state('Machines.fusionPhysicalMachineSettings', {
            url: "/FusionPhysicalMachineSettings",
            templateUrl: 'partials/PhysicalMachine/FusionPhysicalMachineSettings.html',
            controller: 'FusionPhysicalMachineSettingsCtrl',
            onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
            data: {
                directlyAccessible: false
            }
        })

		// ##########
		// Users page
		// ##########
		.state('Users', {
		    abstract: true,
		    url: "/Users",
		    templateUrl: 'partials/UserManagement/userManagement.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('Users.list', {
		    url: "",
		    templateUrl: 'partials/UserManagement/userList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('Users.new', {
		    url: "New",
		    templateUrl: 'partials/UserManagement/newUser.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})

		// ##########
		// PickZones page
		// ##########
		.state('PickZones', {
		    abstract: true,
		    url: "/PickZones",
		    templateUrl: 'partials/PickZones/pickZones.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('PickZones.list', {
		    url: "",
		    templateUrl: 'partials/PickZones/pickZonesList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('PickZones.new', {
		    url: "New",
		    templateUrl: 'partials/PickZones/newPickZone.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})

		// ##########
		// Corrugates page
		// ##########
		.state('Corrugates', {
		    abstract: true,
		    url: "/Corrugates",
		    templateUrl: 'partials/Corrugates/corrugates.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('Corrugates.list', {
		    url: "",
		    templateUrl: 'partials/Corrugates/corrugatesList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('Corrugates.new', {
		    url: "/New",
		    templateUrl: 'partials/Corrugates/newCorrugate.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})

		// ##########
		// Classifications page
		// ##########
		.state('Classifications', {
		    abstract: true,
		    url: "/Classifications",
		    templateUrl: 'partials/Classifications/classifications.html',
		    data: {
		        authorizedRoles: [USER_ROLES.admin]
		    }
		})
		.state('Classifications.list', {
		    url: "",
		    templateUrl: 'partials/Classifications/classificationsList.html',
		    data: {
		        directlyAccessible: true
		    }
		})
		.state('Classifications.new', {
		    url: "New",
		    templateUrl: 'partials/Classifications/newClassification.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        directlyAccessible: false
		    }
		})

        // ##########
        // Scheduler page
        // ##########
        .state('ScheduledJobs', {
            abstract : true,
            url : "/ScheduledJobs",
            templateUrl : 'partials/Scheduler/scheduledJobs.html',
            data : {
                authorizedRoles : [USER_ROLES.admin]
            }
        })
        .state('ScheduledJobs.list', {
            url : "",
            templateUrl : 'partials/Scheduler/scheduledJobsList.html',
            data : {
                directlyAccessible : true
            }
        })
        .state('ScheduledJobs.new', {
            url : "New",
            templateUrl : 'partials/Scheduler/newScheduledJob.html',
            onEnter : ['$rootScope', function ($rootScope){ clearAlerts($rootScope); }],
            data : {
                directlyAccessible : false
            }
        })

        // ##########
        // SLD page
        // ##########
        .state('SLD', {
            abstract : true,
            url : "/UPCItems",
            templateUrl: 'partials/SLD/UPCItems.html',
            data : {
                authorizedRoles : [USER_ROLES.all]
            }
        })
        .state('SLD.management', {
            url : "Management",
            templateUrl: 'partials/SLD/UPCManagementPage.html',
            params: { fromOperatorPanel: false},
            onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
            data : {
                directlyAccessible : true
            }
        })
        .state('SLD.list', {
            url : "List",
            templateUrl: 'partials/SLD/UPCItemsList.html',
            onEnter: [
                '$rootScope', '$state', '$timeout', 'machineGroupService', 'alertService', 'translationService', function ($rootScope, $state, $timeout, machineGroupService, alertService, translationService) {
                     clearAlerts($rootScope);
                     ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, "SLD.list",  $timeout, alertService, false);
                 }],
            params: {
                updatedUpcRow: null
            },
            data : {
                directlyAccessible : true
            }
        })
        .state('SLD.new', {
            url : "New",
            templateUrl : 'partials/SLD/newUPCItem.html',
            params : {
                selectedUPC: null,
                rowEditPromise: null,
                editingScannedRow: false
            },
            onEnter : ['$rootScope', function ($rootScope){ clearAlerts($rootScope); }],
            data : {
                directlyAccessible : false
            }
        })
        .state('SLD.import', {
            url: "Export",
            templateUrl: 'partials/SLD/UPCImport.html',
            onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
            data: {
                directlyAccessible: false
            }
        })

		.state('Login', {
		    url: "/Login",
		    controller: 'LoginCtrl',
		    templateUrl: 'partials/UserManagement/Login.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.all],
		        directlyAccessible: false,
		        redirectTo: 'Dashboard'
		    }
		})
		 .state('About', {
		     url: "/About",
		     controller: 'AboutCtrl',
		     templateUrl: 'partials/Global/About.html',
		     onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		     data: {
		         authorizedRoles: [USER_ROLES.all]
		     }
		 })
		.state('Support', {
		    url: "/Support",
		    controller: 'SupportCtrl',
		    templateUrl: 'partials/Support/Support.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.all]
		    }
		})
		.state('CreateSupportIncident', {
		    url: "/CreateSupportIncident",
		    controller: 'SupportIncidentCtrl',
		    templateUrl: 'partials/Support/SupportIncident.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.all]
		    }
		})
		.state('NotAuthorized', {
		    url: "/NotAuthorized",
		    templateUrl: 'partials/NotAuthorized.html',
		    onEnter: ['$rootScope', function ($rootScope) { clearAlerts($rootScope); }],
		    data: {
		        authorizedRoles: [USER_ROLES.all]
		    }
		});

    function clearAlerts($rootScope) {
        if (!$rootScope.alerts) {
            $rootScope.alerts = [];
        }
        $rootScope.alerts.length = 0;
    };

    function ensureMachineGroupIsSelected(machineGroupService, $rootScope, translationService, endState, $timeout, alertService, bypassOperator) {
        if($timeout.getInstance)
            $timeout = $timeout.getInstance($rootScope);

        var redirectRequired = false;
        try {
            var currentMachineGroup = machineGroupService.getCurrentMachineGroup(bypassOperator);
            if (!currentMachineGroup) {
                redirectRequired = true;
            }
        }
        catch (err) {
            console.error(err);
            redirectRequired = true;
        }

        if (redirectRequired) {
            $rootScope.$broadcast(Machines_Constants.machineGroupNotSelected, { endState: endState });
        }
    };
})