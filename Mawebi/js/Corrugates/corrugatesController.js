function CorrugatesController($location, $log, $scope, $state, $timeout, alertService, introJsService, messagingService, uiGridConstants, translationService) {
    $timeout = $timeout.getInstance($scope);

    $scope.floatingPointRegex = /^(\d+\.?\d*|\.\d+)$/;
    $scope.intRegex = /^\d+$/;
    $scope.corrugates = [];
    $scope.selectedCorrugate = {};
    $scope.selectedCorrugate.IsNewCorrugate = true;
    $scope.selectedCorrugate.Quality = 1;

    var awaitingUpdate = false;

    $scope.$on('$locationChangeSuccess', function () {
        if ($scope.selectedCorrugate && $scope.selectedCorrugate.IsNewCorrugate == false && $location.path() == '/Corrugates') {
            $scope.cancelNewEditCorrugate();
        }
    });

    messagingService.corrugatesObservable.autoSubscribe(function (msg) {
        $timeout(function () {
            
            delete $scope.corrugates;
            $scope.corrugates = msg.Data;
        }, 0, true);
    }, $scope, 'Corrugates');

    messagingService.corrugateCreatedObservable.autoSubscribe(function (msg) {
        var additionalInfo;

        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.CreateFailed', translationService.translate('Corrugate.Label.NameSingular'), $scope.corrugateBeingSaved.Alias), 5000);
        }
        else if (msg.Result === "Exists") {
            additionalInfo = msg.Message != null ? msg.Message.split(",") : [""];

            if (additionalInfo[0] === "Alias")
                alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Corrugate.Label.NameSingular'), $scope.corrugateBeingSaved.Alias), 5000);
            else if (additionalInfo[0] === "Properties")
                alertService.addError(translationService.translate('Corrugate.Message.Duplicate', additionalInfo[1]), 5000);
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'), 5000);
                $log.info(msg);
            }
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.CreateSuccess', translationService.translate('Corrugate.Label.NameSingular'), $scope.corrugateBeingSaved.Alias), 5000);
        }
    }, $scope, 'Corrugate Created');

    messagingService.corrugateUpdatedObservable.autoSubscribe(function (msg) {
        var additionalInfo;

        awaitingUpdate = false;
        if (msg.Result === "Fail") {
            if (msg.Message === "CurrentlyLoaded")
                alertService.addError(translationService.translate('Common.Message.UpdateCurrentlyLoaded', translationService.translate('Corrugate.Label.NameSingular'), $scope.corrugateBeingSaved.Alias), 5000);
            else
                alertService.addError(translationService.translate('Common.Message.UpdateFailed', translationService.translate('Corrugate.Label.NameSingular'), $scope.corrugateBeingSaved.Alias), 5000);
        }
        else if (msg.Result === "Exists") {
            additionalInfo = msg.Message != null ? msg.Message.split(",") : [""];

            if (additionalInfo[0] === "Alias")
                alertService.addError(translationService.translate('Common.Message.NameExists', translationService.translate('Corrugate.Label.NameSingular'), $scope.corrugateBeingSaved.Alias), 5000);
            else if (additionalInfo[0] === "Properties")
                alertService.addError(translationService.translate('Corrugate.Message.Duplicate', additionalInfo[1]), 5000);
            else {
                alertService.addError(translationService.translate('Common.Message.UnexpectedMessageReceived'), 5000);
                $log.info(msg);
            }
        }
        else if (msg.Result === "Success") {
            $state.go('^.list', null, { reload: true });

            alertService.addSuccess(translationService.translate('Common.Message.UpdateSuccess', translationService.translate('Corrugate.Label.NameSingular'), msg.Data.Alias), 5000);
        }
    }, $scope, 'Corrugate Updated');

    messagingService.corrugateDeletedObservable.autoSubscribe(function (msg) {
        if (msg.Result === "Fail") {
            alertService.addError(translationService.translate('Common.Message.DeleteFailed', translationService.translate('Corrugate.Label.NameSingular'), msg.Data.Alias), 5000);
        }
        else if (msg.Result === "CorrugateCurrentlyLoadedOnMachine") {
            alertService.addError(translationService.translate('Common.Message.DeleteCurrentlyLoaded', translationService.translate('Corrugate.Label.NameSingular'), msg.Data.Alias), 5000);
        }
        else if (msg.Result === "Success") {
            alertService.addSuccess(translationService.translate('Common.Message.DeleteSuccess', translationService.translate('Corrugate.Label.NameSingular'), msg.Data.Alias), 5000);
            
            delete $scope.corrugateBeingSaved;
            $scope.corrugateBeingSaved = {};
            $scope.gridApi.selection.clearSelectedRows();
            
            delete $scope.selectedCorrugate;
            $scope.selectedCorrugate = {};
            $scope.selectedCorrugate.IsNewCorrugate = true;
        }
        requestData();
    }, $scope, 'Machine Group Deleted');

    var requestData = function () {
        messagingService.publish("GetCorrugates");
    }

    $scope.utilities.initialDataRequest($scope, messagingService, requestData);

    $scope.gridOptions = {
        enableGridMenu: true,
        data: 'corrugates',
        columnDefs: [
            {
                displayName: translationService.translate('Common.Label.Name'),
                field: 'Alias',
                width: 350,
                sort: {
                    direction: uiGridConstants.ASC,
                    priority: 1
                }
            },
            {
                displayName: translationService.translate('Common.Label.Width'),
                field: 'Width',
                type: 'number',
                width: 100
            },
            {
                displayName: translationService.translate('Common.Label.Thickness'),
                field: 'Thickness',
                type: 'number',
                width: 100
            },
            {
                displayName: translationService.translate('Common.Label.Quality'),
                field: 'Quality',
                type: 'number'
            }
        ],
        headerRowHeight: 36,
        enableColumnResizing: true,
        enableHeaderRowSelection: true,
        enableSelectAll: true,
        multiSelect: true,
        onRegisterApi: function (gridApi) {
            //set gridApi on scope
            delete $scope.gridApi;
            $scope.gridApi = gridApi;

            // allow the click of a row to select the row.
            gridApi.cellNav.on.navigate($scope, function (newRowCol, oldRowCol) {
                if (newRowCol.col.field !== "selectionRowHeaderCol")
                    $scope.gridApi.selection.toggleRowSelection(newRowCol.row.entity);
            });
        }
    };

    $scope.canEdit = function () {
        return $scope.gridApi.selection.getSelectedRows().length === 1;
    }

    $scope.canDelete = function () {
        return $scope.gridApi.selection.getSelectedRows().length > 0;
    }

    $scope.saveCorrugate = function (form) {
        $scope.$broadcast('show-errors-check-validity');
        if (!form.$valid) {
            return;
        }

        alertService.clearAll();
        awaitingUpdate = true;
        delete $scope.corrugateBeingSaved;
        $scope.corrugateBeingSaved = {
            Id: $scope.selectedCorrugate.Id,
            Alias: $scope.selectedCorrugate.Alias,
            Width: $scope.selectedCorrugate.Width,
            Thickness: $scope.selectedCorrugate.Thickness,
            Quality: $scope.selectedCorrugate.Quality,
            IsNewCorrugate: $scope.selectedCorrugate.IsNewCorrugate,
        };
        var action = "CreateCorrugate";
        if (!$scope.corrugateBeingSaved.IsNewCorrugate)
            action = "UpdateCorrugate";
        messagingService.publish(action, $scope.corrugateBeingSaved);
        $timeout(function () {
            if (awaitingUpdate) {
                alertService.addError(translationService.translate('Common.Message.SaveTimeout', translationService.translate('Corrugate.Label.NameSingular'), $scope.corrugateBeingSaved.Alias), 5000);
                awaitingUpdate = false;
                
                delete $scope.corrugateBeingSaved;
                $scope.corrugateBeingSaved = {};
            }
        }, 5000);
    }

    $scope.cancelNewEditCorrugate = function () {
        $state.go('^.list', null, { reload: true });
    }

    $scope.deleteCorrugates = function () {
        Enumerable.From($scope.gridApi.selection.getSelectedRows()).ForEach(function (corrugate) {
            messagingService.publish("DeleteCorrugate", corrugate);
        });
        $timeout(function () {
            $scope.gridApi.selection.clearSelectedRows();
        }, 0, true);
    }

    $scope.editCorrugate = function (){
        delete $scope.selectedCorrugate;
        $scope.selectedCorrugate = $scope.gridApi.selection.getSelectedRows()[0];
        $scope.selectedCorrugate.IsNewCorrugate = false;
        $state.go('^.new');
    }

    $scope.tellMeAboutThisPage = function () {

        var steps = [];
        var columnDefs = $("[ui-grid-header-cell]");

        if (angular.lowercase($state.current.name) === "corrugates.new") {
            steps.push({
                element: "#doesntExist",
                intro: translationService.translate('Corrugate.Help.Form.Overview')
            });

            steps.push({
                element: "#newCorrugateCorrugateName",
                intro: translationService.translate('Corrugate.Help.Form.Name')
            });

            steps.push({
                element: "#newCorrugateWidth",
                intro: translationService.translate('Corrugate.Help.Form.Width')
            });

            steps.push({
                element: "#newCorrugateThickness",
                intro: translationService.translate('Corrugate.Help.Form.Thickness')
            });

            steps.push({
                element: "#nudHorizontalTable",
                intro: translationService.translate('Corrugate.Help.Form.Quality')
            });

            steps.push({
                element: "#newCorrugateSaveButton",
                intro: translationService.translate('Corrugate.Help.Form.Save')
            });

            steps.push({
                element: "#newCorrugateCancelButton",
                intro: translationService.translate('Corrugate.Help.Form.Cancel')
            });
        }
        else {
            //TODO: This is the best selector for column headers, however ui-grid doesn't behave well when intro.js highlights the object
            steps.push({
                element: "#doesntExist",
                intro: translationService.translate('Corrugate.Help.List.Overview')
            });

            steps.push({
                element: "#newCorrugate",
                intro: translationService.translate('Corrugate.Help.List.New')
            });

            steps.push({
                element: "#deleteCorrugate",
                intro: translationService.translate('Corrugate.Help.List.Delete')
            });

            steps.push({
                element: "#editCorrugate",
                intro: translationService.translate('Corrugate.Help.List.Edit')
            });

            steps.push({
                element: '#corrugateListTable',
                intro: translationService.translate('Corrugate.Help.List.Columns.Overview')
            });

            steps.push({
                element: columnDefs[1],
                intro: translationService.translate('Corrugate.Help.List.Columns.Name')
            });

            steps.push({
                element: columnDefs[2],
                intro: translationService.translate('Corrugate.Help.List.Columns.Width')
            });

            steps.push({
                element: columnDefs[3],
                intro: translationService.translate('Corrugate.Help.List.Columns.Thickness')
            });

            steps.push({
                element: columnDefs[4],
                intro: translationService.translate('Corrugate.Help.List.Columns.Quality')
            });
        }
        return steps;
    }

    introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage);
};