﻿function producibleHelperService($rootScope, machineGroupService, packagingDesignService, NOTIFICATIONS_EVENTS) {

    function findFirstCarton(producibleX) {
        var producible = {};
        if (producibleX.Producible) {
            //handle wrappers
            producible = producibleX.Producible;
            return findFirstCarton(producible);
        }
        else producible = producibleX;

        if (producible.ProducibleType == "Kit") {
            for (var i = 0; i < producible.ItemsToProduce.length; i++) {
                var carton = findFirstCarton(producible.ItemsToProduce[i]);
                if (carton) return carton;
            }
        }
        if (producible.ProducibleType == "Carton" || producible.ProducibleType == "TiledCarton") return producible;

        return null;
    }

    function findAllCartons(producibleX){
        var producible = {};
        var cartons = [];
        if (producibleX.Producible) {
            //handle wrappers
            producible = producibleX.Producible;
        }
        else producible = producibleX;

        if (producible.ProducibleType == "Kit") {
            for (var i = 0; i < producible.ItemsToProduce.length; i++) {
                var carton = findFirstCarton(producible.ItemsToProduce[i]);
                if (carton) cartons.push(carton);
            }
        }
        if (producible.ProducibleType == "Carton" || producible.ProducibleType == "TiledCarton") cartons.push(producible);

        return cartons;
    }

    function findFirstOptimalCorrugate(producibleX) {
        var carton = findFirstCarton(producibleX);
        //find first carton
        if (carton) {
            if (carton.CartonOnCorrugate != null && carton.CartonOnCorrugate.Corrugate != null) {
                return carton.CartonOnCorrugate.Corrugate;
            }
        }
        return null;
    }

    function findAllOptimalCorrugates(producibles, corrugates){
        if (!producibles || producibles.length == 0) return corrugates;

        var corr = findFirstOptimalCorrugate(Enumerable.From(producibles).ElementAt(0));
        if (corr) corrugates.push(corr);

        return findAllOptimalCorrugates(Enumerable.From(producibles).Skip(1).ToArray(), corrugates);
    }

    function findFirstCartonOnCorrugate(producibleX) {
        var carton = findFirstCarton(producibleX);
        //find first carton
        if (carton) {
            if (carton.CartonOnCorrugate != null ) {
                return carton.CartonOnCorrugate;
            }
        }
        return null;
    }

    function machineGroupCanProduceAllCartons(order, mg) {
        var cartons = findAllCartons(order, []);
        if (!cartons || cartons.length == 0) return true;
        var optimalCorrugates = [];
        for (var i = 0; i < cartons.length; i++) optimalCorrugates.push(cartons[i].CartonOnCorrugate);
        if (mg == null) return false;
        if (optimalCorrugates && optimalCorrugates.length > 0) {
            return Enumerable.From(optimalCorrugates).All(function (c) {
                if (c == null) {
                    return false;
                }
                return Enumerable.From(mg.Machines).Any(function (m) {
                    return c.ProducibleMachines[m.Id] != null && machineHasOptimalCorrugateLoaded(m, c);
                });
            });
        }
        return false;
    }

    function machineHasOptimalCorrugateLoaded(m, optimalCorrugate) {
        return Enumerable.From(m.Data.Tracks).Any(function (t) {
            if (!optimalCorrugate) return false;
            else return t.LoadedCorrugate && t.LoadedCorrugate.Id === optimalCorrugate.Corrugate.Id;
        });
    }

    //TODO: this is really horrible. If the user doesn't reset the length of the producibleList it is an instant memory leak (like with orders)
    //Refactor this!!!
    var producibleList = [];

    var addProducible = function (newObj){
        var found = Enumerable.From(producibleList).FirstOrDefault(null, function (p) { return p.CustomerUniqueId === newObj.CustomerUniqueId; });
        if (found == null) {
            producibleList.unshift(newObj);
        }
        else {
            var index = producibleList.indexOf(found);
            producibleList[index] = newObj;
        }
        console.log("ProducibleHelperService: producibleList length " + producibleList.length);
        $rootScope.$broadcast(NOTIFICATIONS_EVENTS.producibleCompleted);
    };

    var getProduciblesCompleted = function () {
        return Enumerable.From(producibleList).Distinct().ToArray();
    };

    var setProduciblesCompletedList = function (list){
        producibleList = list;
    };

    var getAllOrders = function (order) {
        var cartons = [];
        if (order.Producible.ProducibleType == "Kit") {
            for (var i = 0; i < order.Producible.ItemsToProduce.length; i++) {
                if (order.Producible.ItemsToProduce[i].ProducibleType == "Order") {
                    var producibleType = order.Producible.ItemsToProduce[i].Producible.ProducibleType;
                    if (producibleType == "Carton" || producibleType == "TiledCarton")
                        cartons.push(order.Producible.ItemsToProduce[i].Producible);
                }
                else {
                    var producibleType = order.Producible.ItemsToProduce[i].ProducibleType;
                    if (producibleType == "Carton" || producibleType == "TiledCarton")
                        cartons.push(order.Producible.ItemsToProduce[i]);
                }
            }
        }
        else if (order.Producible.ProducibleType == "Carton" || order.Producible.ProducibleType == "TiledCarton") {
            cartons.push(order.Producible);
        }
        return cartons;
    }

    return {
        //find the first carton or tiled carton in the producible chain
        findFirstCarton : findFirstCarton,
        findAllCartons : findAllCartons,
        findFirstOptimalCorrugate: findFirstOptimalCorrugate,
        findAllOptimalCorrugates: findAllOptimalCorrugates,
        machineGroupCanProduceAllCartons: machineGroupCanProduceAllCartons,
        machineHasOptimalCorrugateLoaded: machineHasOptimalCorrugateLoaded,
        //findNeededMachineType : findNeededMachineType,
        getOptimalCorrugateDisplayValue: function (passedInProducible) {
            var corrugateInfo = findFirstOptimalCorrugate(passedInProducible);
            return corrugateInfo ? corrugateInfo.Alias + ' - W: ' + corrugateInfo.Width + ' - T: ' + corrugateInfo.Thickness + ' - Q: ' + corrugateInfo.Quality : "OptimalCorrugateNotFound";
        },
        findFirstCartonOnCorrugate: findFirstCartonOnCorrugate,
        getDiplayValue: function (passedInProducible) {
            var producible = findFirstCarton(passedInProducible);
            var result = "";
            if (producible) {
                var designName = packagingDesignService.getDesign(producible.DesignId).Name;
                result += "D:" + designName + " L:" + producible.Length + " W:" + producible.Width + " H:" + producible.Height;
                if (producible.XValues) {
                    result += ' ' + Enumerable.From(producible.XValues).Select(function (x) { return x.Key + ':' + x.Value }).ToArray().join(' ');
                }
            }
            return result;
        },
        getProduceOnTargetDisplayValue: function (producible) {//get PG or MG that should produce on
            var restriction = Enumerable.From(producible.Restrictions).FirstOrDefault(null, "$.$type.includes('ProductionGroupSpecificRestriction')");
            var obj;
            if (restriction) {
                obj = Enumerable.From(machineGroupService.getProductionGroups()).FirstOrDefault(null, "$.Id == '" + restriction.Value + "'");
                return obj ? obj.Alias : "PG";
            }
            restriction = Enumerable.From(producible.Restrictions).FirstOrDefault(null, "$.$type.includes('MachineGroupSpecificRestriction')");
            obj = Enumerable.From(machineGroupService.getMachineGroups()).FirstOrDefault(null, "$.Id == '" + restriction.Value + "'");
            return obj ? obj.Alias : "PG";
        },
        addProducible: addProducible,
        setProduciblesCompletedList: setProduciblesCompletedList,
        getProduciblesCompleted: getProduciblesCompleted,
        getAllOrders: getAllOrders
    }
}
app.factory('producibleHelperService', producibleHelperService);