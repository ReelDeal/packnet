﻿function packagingDesignService(messagingService) {
    var myScope = { $id: "packagingDesignService" ,$on: function (a, b){} }// spoof a scope so we can subscribe.
    var allDesigns;
    var allDesignsSubject = new Rx.Subject();

    var allDesignsObservable = Rx.Observable.create(function (observer) {
        var disposable = allDesignsSubject.subscribe(function (designs) {
            observer.onNext(designs);
        });

        return disposable;
    });

    var designListObservable = messagingService.messageSubject
        .where(messagingService.filterObs("PackagingDesigns"))
        .select(messagingService.dataFunction);

    designListObservable.autoSubscribe(function (data) {
        allDesigns = data; // return the list of designs without details id and name only
        allDesignsSubject.onNext(allDesigns);
    }, myScope, 'Design List');

    //request designs
    messagingService.publish("GetPackagingDesigns", "");

    return {
        getAllDesigns: function () {
            return allDesigns;
        },
        getDesign: function (designId){
            return Enumerable.From(allDesigns).FirstOrDefault(null, "$.Id == " + designId);
        },
        allDesignsObservable: allDesignsObservable
    }
}

app.factory('packagingDesignService', packagingDesignService);