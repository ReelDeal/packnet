﻿var translationModule = angular.module('translationModule', [
    'pascalprecht.translate', 'angular.filter', function ($translateProvider) {
        $translateProvider.useStaticFilesLoader({
            prefix : 'Resources/Languages/Strings_',
            suffix : '.json'
        });

        var prefLang = 'en_US';

        $translateProvider.fallbackLanguage('en_US');
        $translateProvider.use(prefLang);
    }
]).provider('translationService', function () {

    var format = function () {
        var s = arguments[0];

        var args;

        if (typeof arguments[1] == 'object') {
            args = arguments[1];
        }
        else {
            args = Array.prototype.slice.call(arguments, 1);
        }

        for (var i = 0; i < args.length; i++) {
            var reg = new RegExp("\\{" + i + "\\}", "gm");
            s = s.replace(reg, args[i]);
        }
        return s;
    };

    return {
        $get : function ($filter){
            return {
                translate: function (){
                    var origArgs = arguments[0];
                    if (!arguments || !arguments[0]) {
                        return "Invalid argument passed to translate (undefined)";
                    }
                    arguments[0] = $filter('translate')(arguments[0]);
                    if (origArgs === arguments[0]) {
                        throw "Failed to translate " + origArgs;
                    }
                    if(arguments.length !== 1)
                        return format.apply(this, arguments);
                    return arguments[0];
                },
            }
        }
    }
});