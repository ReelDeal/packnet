﻿'use strict';

angular.module('minicolors', []);

angular.module('minicolors').provider('minicolors', function () {
    this.defaults = {
        theme: 'bootstrap',
        position: 'top left',
        defaultValue: '',
        animationSpeed: 5,
        animationEasing: 'swing',
        change: null,
        changeDelay: 0,
        control: 'hue',
        hide: null,
        hideSpeed: 100,
        inline: false,
        letterCase: 'lowercase',
        opacity: false,
        show: null,
        showSpeed: 100
    };

    this.$get = function () {
        return this;
    };

});

angular.module('minicolors').directive('minicolors', ['minicolors', '$timeout', function (minicolors, $timeout) {
    return {
        require: '?ngModel',
        restrict: 'A',
        scope: {
            minicolors: '=',
            rgbaString: '=',
            alpha: '='
        },
        priority: 1, //since we bind on an input element, we have to set a higher priority than angular-default input
        link: function (scope, element, attrs, ngModel) {

            var inititalized = false;

            //gets the settings object
            var getSettings = function (){
                console.log("getSettings called");
                var config = angular.extend({}, minicolors.defaults, scope.minicolors);
                return config;
            };

            if (scope.alpha) {
                element.attr('data-opacity', scope.alpha);
            }
            //what to do if the value changed
            ngModel.$render = function () {

                //we are in digest or apply, and therefore call a timeout function
                $timeout(function () {
                    var color = ngModel.$viewValue;
                    element.minicolors('value', color);
                    var opacity = element.minicolors('opacity');
                    if (scope.alpha && scope.alpha != opacity) {
                        element.minicolors('opacity', scope.alpha);
                    }
                }, 0, true);
            };

            //init method
            var initMinicolors = function () {

                if (!ngModel) {
                    return;
                }
                var settings = getSettings();
                settings.change = function (hex) {
                    $timeout(function () {
                        var rgbaString = element.minicolors('rgbaString');
                        var rgbObject = element.minicolors('rgbObject');
                        scope.rgbaString = rgbaString;
                        scope.alpha = rgbObject.a;
                        ngModel.$setViewValue(hex);
                    }, 0, true);
                };

                var opacity = element.minicolors('opacity');
                if (ngModel.$viewValue && ngModel.$viewValue != element.minicolors('value')) {
                    element.minicolors('value', ngModel.$viewValue);
                }
                if (scope.alpha && scope.alpha != opacity) {
                    element.minicolors('opacity', scope.alpha);
                }

                // If we don't destroy the old one it doesn't update properly when the config changes
                element.minicolors('destroy');

                // Create the new minicolors widget
                element.minicolors(settings);

                // are we inititalized yet ?
                //needs to be wrapped in $timeout, to prevent $apply / $digest errors
                //$scope.$apply will be called by $timeout, so we don't have to handle that case
                if (!inititalized) {
                    ngModel.$render();
                    inititalized = true;
                    return;
                }
            };

            initMinicolors();
        }
    };
}]);