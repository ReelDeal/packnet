/* *
 * The MIT License
 * 
 * Copyright (c) 2014, Sebastian Sdorra
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
'use strict';

angular.module('dashboardMgmt', ['adf', 'LocalStorageModule'])
.service('dashboardService', function () {
    var me = this;

        this.areAnyWidgetsConfigured = function (model){
        if ($.isArray(model.rows)) {
            for (var i = 0; i < model.rows.length; i++) {
                if ($.isArray(model.rows[i].columns)) {
                    for (var j = 0; j < model.rows[i].columns.length; j++) {
                        if ($.isArray(model.rows[i].columns[j].widgets) && model.rows[i].columns[j].widgets.length > 0) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
})
    .controller('dashboardMgmtCtrl', function ($scope, $timeout, localStorageService, introJsService, dashboardService, translationService) {
        $timeout = $timeout.getInstance($scope);

        var name = 'dashboardMgmt' + $scope.currentUser.UserName;
        var model = localStorageService.get(name);

        if (!model) {
            // set default model for demo purposes
            model = {
                displayTitle: true,
                structure: "4-8",
                rows: [
                {
                    columns: [
                    {
                        styleClass: "col-md-4",
                        widgets: []
                    },
                    {
                        styleClass: "col-md-8",
                        widgets: []
                    }
                    ]
                }
                ]
            };
        }

        $scope.name = name;
        $scope.model = model;
        $scope.collapsible = false;
        $scope.displayWelcomeMessage = !dashboardService.areAnyWidgetsConfigured(model);

        $scope.$on('adfDashboardChanged', function (event, name, model) {

            $scope.displayWelcomeMessage = !dashboardService.areAnyWidgetsConfigured(model);

            localStorageService.set(name, model);
        });

        var editMode = false;

        $scope.tellMeAboutThisPage = function () {
            return $timeout(function () {
                editMode = $("#editDashboard").scope().editMode;

                if (!editMode) {
                    $("#editDashboard").click();
                }

                var steps = [];

                steps.push({
                    element: '#doesntExist',
                    intro: translationService.translate('Dashboard.Main.Help.Overview')
                });

                steps.push({
                    element: "[ui-view]",
                    intro: translationService.translate('Dashboard.Main.Help.WidgetDock')
                });

                steps.push({
                    element: "#editDashboard i",
                    intro: translationService.translate('Dashboard.Main.Help.EditDashboard'),
                    position: 'left'
                });

                steps.push({
                    element: "#addWidget i",
                    intro: translationService.translate('Dashboard.Main.Help.AddWidget'),
                    position: 'left'
                });

                steps.push({
                    element: "#editStructure i",
                    intro: translationService.translate('Dashboard.Main.Help.EditDashboardStructure'),
                    position: 'left'
                });

                steps.push({
                    element: "#editDashboard i",
                    intro: translationService.translate('Dashboard.Main.Help.EditDashboardToSave'),
                    position: 'left'
                });

                return steps;
            }, 0);
        }

        var afterIntro = function () {
            if (!editMode) {
                $("#editDashboard").click();
            }
        }

        introJsService.onexit(afterIntro);
        introJsService.oncomplete(afterIntro);

        introJsService.tellMeAboutThisPage($scope, $scope.tellMeAboutThisPage, true);
    });