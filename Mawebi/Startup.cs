﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;

using Owin;

using PackNet;

[assembly: OwinStartup(typeof(Startup))]
namespace PackNet
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalHost.Configuration.MaxIncomingWebSocketMessageSize = null;
            // Minimum handle by Signlr is 32, lets give it double JIC
            GlobalHost.Configuration.DefaultMessageBufferSize = 64;
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();

        }
    }
}