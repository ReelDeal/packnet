﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;

using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Messaging;

using PackNet.Common.Communication.RabbitMQ;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;
using PackNet.Properties;

namespace PackNet
{
    public class RabbitMqHub : Hub
    {
        private static ConcurrentDictionary<string, Tuple<IDisposable, Action>> subscriptions = new ConcurrentDictionary<string, Tuple<IDisposable, Action>>();
        private static TimeSpan pingTime = TimeSpan.FromSeconds(3);
        private static Timer timerPing;
        private static DateTime lastResponse = DateTime.UtcNow;
        private static RabbitConnectionManager rabbitConnectionManager = null;
        private static object observableCreateLock = new object();

        static RabbitMqHub()
        {
            rabbitConnectionManager = new RabbitConnectionManager();
            timerPing = new Timer(VerifyServerCommunication, pingTime);
            timerPing.Start();
        }

        /// <summary>
        /// If no communication has been recorded, send a message to .server and hope for a response. If no response after 15 seconds, disconnect clients
        /// </summary>
        private static void VerifyServerCommunication()
        {
            if (subscriptions.IsEmpty)
                return; //Don't care cause no one is listening

            var elapsedLastResponseSeconds = DateTime.UtcNow.Subtract(lastResponse).TotalSeconds;
            if (elapsedLastResponseSeconds > Settings.Default.WaitTimeBeforeServerPing && elapsedLastResponseSeconds < Settings.Default.WaitTimeBeforeClientDisconnect)
            {
                //Send a ping
                WebLogging.Logger.Log(LogLevel.Warning, Settings.Default.WaitTimeBeforeServerPing + " second connect timer elapsed... Sent a ping to .Server");
                rabbitConnectionManager.SendToServer(new PackNet.Common.Interfaces.DTO.Messaging.Message { MessageType = MessageTypes.ConnectionAlive });
            }
            if (elapsedLastResponseSeconds > Settings.Default.WaitTimeBeforeClientDisconnect)
            {
                //.Server is not responding disconnect hub clients.
                WebLogging.Logger.Log(LogLevel.Error, Settings.Default.WaitTimeBeforeClientDisconnect + " second connect timer elapsed... disconnected clients");
                var context = GlobalHost.ConnectionManager.GetHubContext<RabbitMqHub>();
                context.Clients.All.disconnected();
                lastResponse = DateTime.UtcNow;
            }
        }

        public void Send(string messageType, string message)
        {
            Trace.WriteLine(Context.ConnectionId + " Send message type " + messageType + ": " + message);

            Tuple<IDisposable, Action> subscription = null;
            if (!subscriptions.TryGetValue(Context.ConnectionId, out subscription))
            {
                lock (observableCreateLock)
                {
                    if (subscriptions.TryGetValue(Context.ConnectionId, out subscription))
                    {
                        return;
                    }
                    Tuple<IObservable<string>, Action> observable = rabbitConnectionManager.GetMessageObservable(Context.ConnectionId);
                    subscriptions[Context.ConnectionId] = new Tuple<IDisposable, Action>(observable.Item1
                        .Subscribe(s =>
                        {
                            //Callback for messages
                            lastResponse = DateTime.UtcNow;
                            Trace.WriteLine("Received a server response, resetting timer");
                            Clients.Client(Context.ConnectionId).broadcastMessage(s);
                            WebLogging.Logger.Log(LogLevel.Trace, "Message for: " + Context.ConnectionId + ": message received");
                        }), observable.Item2);
                }
            }
            rabbitConnectionManager.SendToServer(messageType, message);
        }

        public override Task OnConnected()
        {
            WebLogging.Logger.Log(LogLevel.Info, "OnConnected " + Context.ConnectionId);
            return Clients.Client(Context.ConnectionId).connected(Context.ConnectionId, DateTime.UtcNow.ToString());
        }

        public override Task OnReconnected()
        {
            WebLogging.Logger.Log(LogLevel.Info, "OnReconnected " + Context.ConnectionId);
            return OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            Tuple<IDisposable, Action> subscriber;
            subscriptions.TryRemove(Context.ConnectionId, out subscriber);
            if (subscriber != null)
            {
                Trace.WriteLine("Disconnecting client " + Context.ConnectionId);
                WebLogging.Logger.Log(LogLevel.Info, "Disconnecting client " + Context.ConnectionId);
                subscriber.Item2();
                subscriber.Item1.Dispose();
            }

            return Clients.Client(Context.ConnectionId).disconnected(Context.ConnectionId, DateTime.UtcNow.ToString());
        }
    }
}