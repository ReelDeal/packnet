﻿using System.IO;
using System.Web;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Common.Logging.NLogBackend;

namespace PackNet
{
    public static class WebLogging
    {
        static WebLogging()
        {
            ILoggingBackend loggingBackend = new NLogBackend("PackNet Web Site", Path.Combine(HttpRuntime.AppDomainAppPath, "Nlog.config"));
            LogManager.LoggingBackend = loggingBackend;
            Logger = LogManager.GetLogForApplication();
        }

        public static ILogger Logger { get; private set; }
    }
}