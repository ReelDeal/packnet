﻿using System;
using System.ComponentModel.Composition;
using System.Reactive.Linq;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;

namespace MyFirstPlugin
{
    [Export(typeof(IService))]
    public class MyFirstPlugin : IService
    {
        public string Name { get { return "MyFirstPlugin"; } }

        /// <summary>
        /// ILogger allows plugins access to the logging infrastructure
        /// </summary>
        public ILogger logger;

        /// <summary>
        /// PackNet.Server is an event based system. All events are published to the rest of the system through the IEventAggregatorPublisher
        /// </summary>
        public IEventAggregatorPublisher eventPublisher;

        /// <summary>
        /// All events are subscribed to through the IEventAggregatorPublisher
        /// </summary>
        public IEventAggregatorSubscriber eventSubscriber;

        /// <summary>
        /// All messages bound for the UI are passed through the IUICommunicationService
        /// </summary>
        public IUICommunicationService uiCommunicationService;

        /// <summary>
        /// User notification service allows services to easily notify the UI when events occur
        /// </summary>
        public IUserNotificationService userNotificationService;

        /// <summary>
        /// This is the constructor that will be used when PackNet creates an instance of the plugin. The plugin will be a singleton.
        /// </summary>
        /// <param name="serviceLocator">Service Locator is used to get a handle of other services in the system in a decoupled manner.
        ///  The service locator will already have an instance of the service to locate or a service can be notified when the service is loaded</param>
        [ImportingConstructor]
        public MyFirstPlugin(IServiceLocator serviceLocator)
        {
            //Get the logger
            logger = serviceLocator.Locate<ILogger>(); //Service already exists in the service locator
            serviceLocator.ServiceAddedObservable //deferred loading of services (useful when you have two plugins that depend on each other) 
                .Where(addedService => addedService is ILogger)
                .Subscribe(addedService => logger = addedService as ILogger);

            //Get the event publisher
            eventPublisher = serviceLocator.Locate<IEventAggregatorPublisher>();

            //Get the event subscriber
            eventSubscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();

            //get the UI communication Service
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();

            //All events are sent in a "Message" envelope. THe message has a property "Data" that caries an optional payload. The next line subscribes to the event that fires when a package is completed on a packsize cut crease machine
            eventSubscriber
                .GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Subscribe(msg =>
                {
                    //Here we will do something useful when the event is fired
                    var cutCreaseMachine = msg.Data.Item1;
                    var productionData = msg.Data.Item2;

                    logger.Log(LogLevel.Info, "MyFirstPlugin: machine {0} produced item {1}", cutCreaseMachine.Alias, productionData.Carton.CustomerUniqueId);
                });

            serviceLocator.RegisterAsService(this);
            logger.Log(LogLevel.Info, "Service {0} has been registered as a service with the service locator", Name);
            
            //send a message to the UI through a notification when the plugin is loaded
            userNotificationService.SendNotificationToSystem(NotificationSeverity.Info, String.Format("Service {0} has been registered as a service with the service locator", Name), "");
        }

        public void Dispose()
        {
        }
    }
}
