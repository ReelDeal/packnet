﻿using System.Collections.Generic;

namespace PackNet.Plugin.ArticleService
{
    public class ArticleImportCollection
    {
         public ArticleImportCollection()
        {
            Items = new List<ArticleImportItem>();
            FailedImportRows = new List<int>();
        }

        public List<ArticleImportItem> Items { get; set; }

        public int SuccessfulImports
        {
            get { return Items.Count; }
        }

        public int FailedImports
        {
            get { return FailedImportRows.Count; }
        }

        public List<int> FailedImportRows { get; set; } 
    }
}
