﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Plugin.ArticleService
{

    /// <summary>
    /// Represents events and functions used to communicate with the article service
    /// </summary>
    public interface IArticleService : IService
    {
        /// <summary>
        /// Creates an article
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        ResultTypes Create(Article<IProducible> article);

        /// <summary>
        /// Updates an article
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        ResultTypes Update(Article<IProducible> article);

        /// <summary>
        /// Deletes an article
        /// </summary>
        /// <param name="articlesToUpdate"></param>
        /// <returns></returns>
        ResultTypes Delete(Article<IProducible> articlesToUpdate);

        /// <summary>
        /// Deletes articles
        /// </summary>
        /// <param name="articlesToUpdate"></param>
        /// <returns></returns>
        ResultTypes Delete(IEnumerable<Article<IProducible>> articlesToUpdate);

        /// <summary>
        /// Retrieves an article by article Id
        /// </summary>
        /// <param name="alias">Id of the article to retrieve</param>
        /// <returns></returns>
        Article<IProducible> FindByAlias(string alias);


        /// <summary>
        /// Export all articles
        /// </summary>
        /// <param name="replyTo">Where to send the exported articles</param>
        /// <returns></returns>
        void Export(string replyTo);

        /// <summary>
        /// Retrieves all articles matching the given search pattern
        /// </summary>
        /// <param name="articleSearchPattern"></param>
        /// <returns></returns>
        IEnumerable<Article<IProducible>> Search(string articleSearchPattern);
    }
}
