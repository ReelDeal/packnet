﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Plugin.ArticleService.Enums
{
    public class ArticleWorkflowTypes : WorkflowTypes
    {
        public static readonly ArticleWorkflowTypes CreateProducibleFromArticle = new ArticleWorkflowTypes("CreateProducibleFromArticle");

        protected ArticleWorkflowTypes(string name)
            : base(0, name)
        {
        }

        protected ArticleWorkflowTypes(int value, string name)
            : base(value, name)
        {
        }
    }
}