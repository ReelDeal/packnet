﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Plugin.ArticleService.Enums
{
    public class ArticleImportTypes : ImportTypes
    {
        public static readonly ArticleImportTypes CreatedFromArticle = new ArticleImportTypes("CreatedFromArticle");

        protected ArticleImportTypes(string name)
            : base(name)
        {
        }

    }
}