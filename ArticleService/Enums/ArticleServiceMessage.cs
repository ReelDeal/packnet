﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Plugin.ArticleService.Enums
{
    public class ArticleServiceMessage : MessageTypes
        
    {
        public static ArticleServiceMessage DiscoverArticleService = new ArticleServiceMessage("DiscoverArticleService");
        public static readonly ArticleServiceMessage DiscoverArticleServiceResponse = new ArticleServiceMessage("DiscoverArticleServiceResponse");

        public static readonly ArticleServiceMessage ArticleSearchNew = new ArticleServiceMessage("ArticleSearchNew");
        public static readonly ArticleServiceMessage ArticleCreateNew = new ArticleServiceMessage("ArticleCreateNew");
        public static readonly ArticleServiceMessage ArticleUpdateNew = new ArticleServiceMessage("ArticleUpdateNew");
        public static readonly ArticleServiceMessage ArticleDeleteNew = new ArticleServiceMessage("ArticleDeleteNew");
        public static readonly ArticleServiceMessage ArticleProduceNew = new ArticleServiceMessage("ArticleProduceNew");
        public static readonly ArticleServiceMessage ArticleImportNew = new ArticleServiceMessage("ArticleImportNew");
        public static readonly ArticleServiceMessage FinalizeArticleImport = new ArticleServiceMessage("FinalizeArticleImport");
        public static readonly ArticleServiceMessage ArticleExportNew = new ArticleServiceMessage("ArticleExportNew");
        public static readonly ArticleServiceMessage SaveArticleServiceConfiguration = new ArticleServiceMessage("SaveArticleServiceConfiguration");
        public static readonly ArticleServiceMessage GetCurrentArticleServiceConfiguration = new ArticleServiceMessage("GetCurrentArticleServiceConfiguration");


        // Responses
        public static readonly ArticleServiceMessage ArticleSearchResponseNew = new ArticleServiceMessage("ArticleSearchResponseNew");
        public static readonly ArticleServiceMessage ArticleCreatedNew = new ArticleServiceMessage("ArticleCreatedNew");
        public static readonly ArticleServiceMessage ArticleUpdatedNew = new ArticleServiceMessage("ArticleUpdatedNew");
        public static readonly ArticleServiceMessage ArticleDeletedNew = new ArticleServiceMessage("ArticleDeletedNew");
        public static readonly ArticleServiceMessage ArticleImportedNew = new ArticleServiceMessage("ArticleImportedNew");
        public static readonly ArticleServiceMessage ArticleExportedNew = new ArticleServiceMessage("ArticleExportedNew");
        public static readonly ArticleServiceMessage ArticleProducedNew = new ArticleServiceMessage("ArticleProducedNew");
        public static readonly ArticleServiceMessage ArticlesReadyForImport = new ArticleServiceMessage("ArticlesReadyForImport");
        public static readonly ArticleServiceMessage SaveArticleServiceConfigurationResponse = new ArticleServiceMessage("SaveArticleServiceConfigurationResponse");
        public static readonly ArticleServiceMessage GetCurrentArticleServiceConfigurationResponse = new ArticleServiceMessage("GetCurrentArticleServiceConfigurationResponse");

        protected ArticleServiceMessage(string name)
            : base(name)
        {
        }

        

        public static void Touch()
        {
#pragma warning disable 1717
            DiscoverArticleService = DiscoverArticleService;
#pragma warning restore 1717
        }
    }

    public class ArticleServiceResultMessage : ResultTypes
    {
        public static readonly ArticleServiceResultMessage ArticleCreated = new ArticleServiceResultMessage("ArticleCreated");

        private ArticleServiceResultMessage(string name) : base(name) { }
    }
}
