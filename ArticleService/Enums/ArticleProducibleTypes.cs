﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Plugin.ArticleService.Enums
{
    public class ArticleProducibleTypes : ProducibleTypes
    {
        public static readonly ArticleProducibleTypes Article = new ArticleProducibleTypes("Article");

        public ArticleProducibleTypes(string name) : base(name)
        {            
        }        
    }
}
