﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Plugin.ArticleService.Enums
{
    public class ArticleImportAction : Enumeration
    {
        private static readonly Dictionary<string, ArticleImportAction> instance = new Dictionary<string, ArticleImportAction>();

        public static ArticleImportAction Import = new ArticleImportAction("ArticleActionImport");
        public static ArticleImportAction Ignore = new ArticleImportAction("ArticleActionIgnore");
        public static ArticleImportAction Update = new ArticleImportAction("ArticleActionUpdate");

        public static void Touch()
        {
#pragma warning disable 1717
            Ignore = Ignore;
            Update = Update;
            Import = Import;
#pragma warning restore 1717
        }

        protected ArticleImportAction(string name)
            : base(0, name)
        {
            instance[name] = this;
        }

        /* This method is to support explicit conversion */
        public static explicit operator ArticleImportAction(string value)
        {
            ArticleImportAction result;

            if (!instance.TryGetValue(value, out result))
                throw new InvalidCastException();

            return result;
        }

    }
}
