﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Plugin.ArticleService.Enums
{
    public sealed class ArticleServiceResultTypes : ResultTypes
    {
        public static readonly ArticleServiceResultTypes ArticleAliasAlreadyInUse = new ArticleServiceResultTypes("ArticleAliasAlreadyInUse");

        private ArticleServiceResultTypes(string name) : base(name) { }
    }
}
