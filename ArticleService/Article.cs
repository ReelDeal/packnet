﻿using System;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Plugin.ArticleService.Enums;

namespace PackNet.Plugin.ArticleService
{
    public class Article<T> : IPersistable where T : IProducible, IPersistable
    {
        public Article()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// The producible item representing the article
        /// </summary>
        public T Producible { get; set; }

        public Guid Id { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }

        public ArticleProducibleTypes ProducibleType
        {
            get { return ArticleProducibleTypes.Article; } 
        }
    }
}
