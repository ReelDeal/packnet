﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Plugin.ArticleService
{
    public class ArticleProductionRequest
    {
        /// <summary>
        /// Id to be used when creating the new producible
        /// </summary>
        public string CustomerUniqueId { get; set; }
        /// <summary>
        /// The article you want to use to create the producible
        /// </summary>
        public Article<IProducible> Article { get; set; }
        /// <summary>
        /// What production group you want the new producible to be sent to.
        /// </summary>
        public Guid ProductionGroupId { get; set; }
        /// <summary>
        /// What machine group you want the new producible to be sent to.
        /// </summary>
        public Guid MachineGroupId { get; set; }
        /// <summary>
        /// How many of this producible to you want?
        /// </summary>
        public int Quantity { get; set; }
    }
}
