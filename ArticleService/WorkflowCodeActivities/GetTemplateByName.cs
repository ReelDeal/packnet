﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Plugin.ArticleService.WorkflowCodeActivities
{

    public sealed class GetTemplateByName : CodeActivity<Template>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<string> Name { get; set; }

        protected override Template Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);
            var templateService = serviceLocator.Locate<ITemplateService>();
            var name = context.GetValue(Name);
            var result = templateService.FindByName(name);
            return result;
        }
    }
}