﻿using System.Activities;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Plugin.ArticleService.WorkflowCodeActivities
{    
    public sealed class CreateOrUpdateArticleWithKit : CodeActivity
    {
        [RequiredArgument]
        public InArgument<Article<IProducible>> Article { get; set; }

        [RequiredArgument]
        public InArgument<IArticleService> ArticleService { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var article = context.GetValue(Article);
            var articleService = context.GetValue(ArticleService);


            if (articleService.FindByAlias(article.Alias) == null)
                articleService.Create(article);
            else
                articleService.Update(article);
        }
    }
}
