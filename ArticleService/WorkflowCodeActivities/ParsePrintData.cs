﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Activities;
using PackNet.Common.Interfaces;

namespace PackNet.Plugin.ArticleService.WorkflowCodeActivities
{
    public sealed class ParsePrintData : CodeActivity
    {        
        [RequiredArgument]
        public InArgument<IEnumerable<KeyValuePair<string, string>>> ImportedData { get; set; }
        public OutArgument<object> PrintData { get; set; }

        protected override void Execute(CodeActivityContext context)
        {            
            var importedData = context.GetValue(this.ImportedData);

            if (importedData == null || importedData.Any() == false)
                return;

            if (!importedData.All(k => k.Key.StartsWith("PrintData[") && k.Key.EndsWith("]")))
            {
                throw new ArgumentException("Error converting PrintData field - needs to have format 'PrintData[<ValueToPrint>]'");
            }
                
            var returnData = new Dictionary<string, string>();

            importedData.ForEach(kv => returnData.Add(kv.Key.Substring(10, kv.Key.Length - 11), kv.Value));

            context.SetValue(PrintData, returnData);
        }
    }
}
