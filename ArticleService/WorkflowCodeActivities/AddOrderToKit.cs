﻿using System.Activities;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;

namespace PackNet.Plugin.ArticleService.WorkflowCodeActivities
{

    public sealed class AddOrderToKit : CodeActivity
    {
        public InArgument<Kit> kit { get; set; }
        public InArgument<Order> order { get; set; }
        
        protected override void Execute(CodeActivityContext context)
        {
            var theKit = context.GetValue(kit);
            var theOrder = context.GetValue(order);
            
            theKit.AddProducible(theOrder);
        }
    }
}
