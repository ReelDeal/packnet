﻿using System.Linq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Data.Repositories.MongoDB;
using System;
using System.Activities;

using PackNet.Common.Interfaces;

namespace PackNet.Plugin.ArticleService.WorkflowCodeActivities
{

    public sealed class CreateOrderFromArticleProductionRequest : CodeActivity<Order>
    {

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }


        // Define an activity input argument of type string
        [RequiredArgument]
        public InArgument<ArticleProductionRequest> Request { get; set; }

        /// <summary>
        /// If the Order(s) we are going to create should be routed via a specific Production Group supply this value.
        /// </summary>
        public InArgument<Guid> SendToSpecificProductionGroup { get; set; }

        /// <summary>
        /// If the order(s) created are to be sent to a specific Machine Group supply this value.
        /// </summary>
        public InArgument<Guid> SendToSpecificMachineGroup { get; set; }


        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override Order Execute(CodeActivityContext context)
        {
            var logger = context.GetValue(ServiceLocator).Locate<ILogger>();
            var request = context.GetValue(this.Request);
            var producible = MongoDbHelpers.DeepCopy(request.Article.Producible) as Kit;
            if (producible == null)
            {
                logger.Log(LogLevel.Error, "Expected type of Kit stored in Article but it was not.  {0}", request.Article);
                throw new ApplicationException(
                    string.Format("Article.Producible should have been a Kit but was a {1}. Article.Alias={0}",
                        request.Article.Alias, request.Article.Producible.GetType()));
            }

            var kit = new Kit { CustomerUniqueId = request.CustomerUniqueId };

            producible.ItemsToProduce.ForEach(p =>
            {
                var articleOrder = p as Order;
                kit.AddProducible(articleOrder);
            });

            var order = new Order(kit, request.Quantity);
            order.CustomerUniqueId = request.CustomerUniqueId;
            order.RecuseAndSetPropertiesAsNew();

            //add the restriction for PG or MG
            var pgGuid = context.GetValue(SendToSpecificProductionGroup);
            var mgGuid = context.GetValue(SendToSpecificMachineGroup);
            IRestriction restriction;
            if (pgGuid != Guid.Empty)
                restriction = new ProductionGroupSpecificRestriction(pgGuid);
            else
                restriction = new MachineGroupSpecificRestriction(mgGuid);

            order.Restrictions.Add(restriction);
            ((Kit)order.Producible).ItemsToProduce.ForEach(o => o.Restrictions.Add(restriction));


            logger.Log(LogLevel.Trace, "Creating Orders for Article {0} production request.  Orders:{1}", request.Article.Alias,
                string.Join("; ", kit.ItemsToProduce.OfType<Order>().Select(o => string.Concat("OrderId:", o.OrderId, " Qty:", o.OriginalQuantity, " ProducibleType:", o.Producible.GetType().Name))));
            return order;

        }
    }
}
