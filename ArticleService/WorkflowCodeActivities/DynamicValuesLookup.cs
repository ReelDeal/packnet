﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Activities;
using PackNet.Common.Interfaces;

namespace PackNet.Plugin.ArticleService.WorkflowCodeActivities
{
    public sealed class DynamicValuesLookup : CodeActivity
    {
        [RequiredArgument]
        public OutArgument<Dictionary<string, string>> OutputValue { get; set; }
        [RequiredArgument]
        public InArgument<Object> Dynamic { get; set; }
        [RequiredArgument]
        public InArgument<string> HeaderStartsWith { get; set; }
        [RequiredArgument]
        public InArgument<string> HeaderEndsWith { get; set; }

        protected override void Execute(CodeActivityContext context)
        {

            var headerStartsWith = HeaderStartsWith.Get(context);
            var headerEndsWith = HeaderEndsWith.Get(context);
            var obj = Dynamic.Get<object>(context);

            var dynamicItem = obj as ExpandoObject;
            if (obj == null)
                throw new ArgumentException("'Dynamic' must be a ExpandoObject");

            var dictionary = dynamicItem as IDictionary<string, Object>;

            var printinfo = dictionary.Where(kv => kv.Key.StartsWith(headerStartsWith) && kv.Key.EndsWith(headerEndsWith));
            if (!printinfo.Any())
                throw new ArgumentException("dynamic object didn't contain a property that starts with: '" + headerStartsWith + "' and ends with: '" + headerEndsWith + "'");

            var printinfoData = new Dictionary<string, string>();

            printinfo.ForEach(
                kv =>
                {
                    var key = kv.Key.Substring(headerStartsWith.Length,
                        kv.Key.Length - headerStartsWith.Length - headerEndsWith.Length);

                    if (key != "")
                    {
                        printinfoData.Add(
                        kv.Key.Substring(headerStartsWith.Length, kv.Key.Length - headerStartsWith.Length - headerEndsWith.Length),
                        kv.Value as string);
                    }
                });

            OutputValue.Set(context, printinfoData);
        }
    }
}