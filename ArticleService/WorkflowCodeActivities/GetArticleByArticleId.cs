﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Plugin.ArticleService.WorkflowCodeActivities
{

    public sealed class GetArticleByArticleId : CodeActivity<Article<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<string> ArticleId { get; set; }
        
        protected override Article<IProducible> Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);
            var articleService = serviceLocator.Locate<IArticleService>();
            var articleId = context.GetValue(ArticleId);
            var foundArticle = articleService.FindByAlias(articleId);
            return foundArticle;
        }
    }
}
