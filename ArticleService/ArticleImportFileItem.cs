﻿namespace PackNet.Plugin.ArticleService
{
    public class ArticleImportFileItem
    {
        public string Contents { get; set; }
        public char Delimiter { get; set; }
        public char CommentPrefix { get; set; }
        public bool HeadersInFile { get; set; }
        public string[] HeaderFields { get; set; }
    }
}
