﻿using System;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.FileHandling.DynamicImporter;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Plugin.ArticleService.BusinessLayer;
using PackNet.Plugin.ArticleService.Data;
using PackNet.Plugin.ArticleService.Enums;
using Newtonsoft.Json;


using IMessage = PackNet.Common.Interfaces.DTO.Messaging.IMessage;

namespace PackNet.Plugin.ArticleService
{
    using System.Reactive;

    using PackNet.Common.Interfaces;
    using PackNet.Data.Repositories.MongoDB;
    using PackNet.Data.Serializers;

    [Export(typeof(IService))]
    public class ArticleService : PackNet.Plugin.ArticleService.IArticleService, IService
    {
        private readonly Subject<IResponseMessage<string>> exportedSubject = new Subject<IResponseMessage<string>>();
        private readonly Subject<IResponseMessage<string>> importedSubject = new Subject<IResponseMessage<string>>();
        private readonly IUserNotificationService userNotificationService;
        private IArticleServiceBusinessLayer articleServiceBusinessLayer;
        private IUICommunicationService uiCommunicationService;
        private IEventAggregatorSubscriber subscriber;
        private ILogger logger;
        private IDisposable createArticleDisposable;
        //private IDisposable finalizeArticleImportDisposable;
        private IDisposable updateArticleDisposable;
        private IDisposable importArticlesDisposable;
        private IDisposable deleteArticlesDisposable;
        private IDisposable exportArticlesDisposable;
        private IDisposable uiExportArticlesDisposable;
        private IDisposable uiImportArticlesDisposable;
        private bool disposed;

        private IServiceLocator serviceLocator;

        public ArticleService(IServiceLocator serviceLocator, ILogger logger, IArticleServiceBusinessLayer bo)
        {
            ArticleServiceMessage.Touch();
            ArticleImportAction.Touch();

            this.serviceLocator = serviceLocator;
            articleServiceBusinessLayer = bo;
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            this.logger = logger;
            
            //SerializationSettings.AddConverter(new ArticleIdentifiersConverter());
            MongoDbHelpers.TryRegisterSerializer<ArticleServiceMessage>(new EnumerationSerializer());
            WireupToMessages();
            //do this last
            serviceLocator.RegisterAsService(this);
        }


        [ImportingConstructor]
        public ArticleService(IServiceLocator serviceLocator, ILogger logger)
            : this(serviceLocator, logger, new ArticleServiceBusinessLayer(new ArticleRepository(), serviceLocator, new ArticleConfigurationRepository()))
        {
        }


        private void WireupToMessages()
        {
            var eventAggregatorSubscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ArticleServiceMessage.DiscoverArticleService);
            ArticleServiceMessage.DiscoverArticleService.OnMessage(eventAggregatorSubscriber, logger,
                msg => uiCommunicationService.SendMessageToUI(new Message()
                {
                    MessageType = ArticleServiceMessage.DiscoverArticleServiceResponse
                }));

            //Search
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<string>(ArticleServiceMessage.ArticleSearchNew);
            subscriber.GetEvent<IMessage<string>>()
                .Where(m => m.MessageType == ArticleServiceMessage.ArticleSearchNew)
                .ObserveOn(Scheduler.Default)
                .DurableSubscribe(
                    m => uiCommunicationService.SendMessageToUI(new ResponseMessage<List<Article<IProducible>>>
                    {
                        MessageType = ArticleServiceMessage.ArticleSearchResponseNew,
                        Result = ResultTypes.Success,
                        ReplyTo = m.ReplyTo,
                        Data = articleServiceBusinessLayer.Search(m.Data).ToList(),
                        /*
                            Criteria should look like:
                            *    {
                                    Alias: '',
                                    Description: ''
                                 }
                            */
                    }));

            //Finalize Article Import
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<Article<IProducible>>>(ArticleServiceMessage.FinalizeArticleImport);

            ArticleServiceMessage.FinalizeArticleImport.OnMessage<IEnumerable<Article<IProducible>>>(subscriber, logger,
                m =>
                {
                    var resultList = new List<ResultTypes>();
                    m.Data.ForEach(
                        item =>
                        {
                            if (item == null)
                            {
                                return;
                            }

                            var result = articleServiceBusinessLayer.ImportOrUpdate(item);
                            resultList.Add(result);
                            this.logger.Log(LogLevel.Info, "Import updated item {0} : {1}", item.Alias, result.DisplayName);

                        });


                    var finalResult = resultList.All(r => r == ResultTypes.Success)
                        ? ResultTypes.Success
                        : resultList.Any(r => r == ResultTypes.Success) 
                            ? ResultTypes.PartialSuccess 
                            : ResultTypes.Fail;

                    uiCommunicationService.SendMessageToUI(
                        new ResponseMessage
                        {
                            MessageType = ArticleServiceMessage.ArticleImportedNew,
                            ReplyTo = m.ReplyTo,
                            Result = finalResult    
                        });
                });
            
            //Create
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Article<IProducible>>(
                ArticleServiceMessage.ArticleCreateNew);
            createArticleDisposable = subscriber.GetEvent<IMessage<Article<IProducible>>>()
                .Where(m => m.MessageType == ArticleServiceMessage.ArticleCreateNew)
                .ObserveOn(Scheduler.Default)
                .DurableSubscribe(
                    m =>
                    {
                        var result = Create(m.Data);
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<Article<IProducible>>
                        {
                            MessageType = ArticleServiceMessage.ArticleCreatedNew,
                            Result = result,
                            ReplyTo = m.ReplyTo,
                        });
                        if (result != ResultTypes.Success)
                        {
                            logger.Log(LogLevel.Warning, "UI Article request failed.  Request:{0}", m);    
                        }
                        else
                        {
                            logger.Log(LogLevel.Info, "Article : " + m.Data.Alias +  " was created by : " + m.UserName);
                        }
                    }, logger);

            //Update
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Article<IProducible>>(
                ArticleServiceMessage.ArticleUpdateNew);
            updateArticleDisposable = subscriber.GetEvent<IMessage<Article<IProducible>>>()
                .Where(x => x.MessageType == ArticleServiceMessage.ArticleUpdateNew)
                .DurableSubscribe(
                    m =>
                    {
                        var result = Update(m.Data);

                        //send back the Alias reguardless of success.
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<Article<IProducible>>
                        {
                            MessageType = ArticleServiceMessage.ArticleUpdatedNew,
                            Result = result,
                            ReplyTo = m.ReplyTo,
                            Data =
                                result == ResultTypes.Success ? articleServiceBusinessLayer.FindByAlias(m.Data.Alias) : m.Data
                        });
                        if (result != ResultTypes.Success)
                        {
                            logger.Log(LogLevel.Warning, "UI Article request failed.  Request:{0}", m);
                        }
                        else
                        {
                            logger.Log(LogLevel.Info, "Article : " + m.Data.Alias + " was updated by : " + m.UserName);
                        }
                    }, logger);

            //Delete multiple
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<Article<IProducible>>>(
                ArticleServiceMessage.ArticleDeleteNew);
            deleteArticlesDisposable = subscriber.GetEvent<IMessage<IEnumerable<Article<IProducible>>>>()
                .Where(x => x.MessageType == ArticleServiceMessage.ArticleDeleteNew)
                .DurableSubscribe(
                    m =>
                    {
                        var result = Delete(m.Data);

                        //send back the Alias reguardless of success.
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<List<string>>
                        {
                            MessageType = ArticleServiceMessage.ArticleDeletedNew,
                            Result = result,
                            ReplyTo = m.ReplyTo,
                        });

                        if (result != ResultTypes.Success)
                        {
                            logger.Log(LogLevel.Warning, "UI Article request failed.  Request:{0}", m);
                        }
                        else
                        {
                            var deletedarticles = m.Data.Select(a => a.Alias).Aggregate((current, article) => current + ", " + article);
                            logger.Log(LogLevel.Info, "Articles : " + deletedarticles + " was deleted by : " + m.UserName);
                        }
                    }, logger);

            //Produce multiple
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<ArticleProductionRequest>>(
                ArticleServiceMessage.ArticleProduceNew);
            deleteArticlesDisposable = subscriber.GetEvent<IMessage<IEnumerable<ArticleProductionRequest>>>()
                .Where(x => x.MessageType == ArticleServiceMessage.ArticleProduceNew)
                .DurableSubscribe(
                    m =>
                    {
                        var x = m.Data.Select(
                            s =>
                                JsonConvert.SerializeObject(new { s.Article.Alias, s.ProductionGroupId, s.Quantity, s.CustomerUniqueId }))
                            .ToList();

                        logger.Log(LogLevel.Info, "Create article requested by {0} Request:{1}", m.UserName, string.Join(", ", x));

                        //Send message out to SA and wait for response.
                        var result = this.articleServiceBusinessLayer.CreateProducibleFromArticlesRequests(m.Data);

                        //On client side, the key value of a Dictionary cannot be an object
                        //Converting the IEnumerable to a List so it's translated directly into an Array
                        var responseMessageData = result.Item2.ToDictionary(i => i.Key.CustomerUniqueId, i => i.Value);

                        uiCommunicationService.SendMessageToUI(new ResponseMessage<Dictionary<string, Order>>
                        {
                            MessageType = ArticleServiceMessage.ArticleProducedNew,
                            Result = result.Item1,
                            ReplyTo = m.ReplyTo,
                            Data = responseMessageData
                        });

                        if (result.Item1 != ResultTypes.Success)
                            logger.Log(LogLevel.Warning, "Faild to create producibles from Article.  Request:{0}", m);
                    }, logger);

            // Import articles
            uiImportArticlesDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ArticleImportFileItem>(ArticleServiceMessage.ArticleImportNew);
            importArticlesDisposable = subscriber.GetEvent<IMessage<ArticleImportFileItem>>()
                .Where(m => m.MessageType == ArticleServiceMessage.ArticleImportNew)
                .DurableSubscribe(
                    m =>
                    {
                        var msg = new ResponseMessage<string>() { MessageType = MessageTypes.AdminNotification, Data = "Articles.ImportStarted" };
                        importedSubject.OnNext(msg);
                        var data = ConvertRawDataToDynamic(m.Data.Contents, m.Data.Delimiter, m.Data.CommentPrefix, m.Data.HeadersInFile ? null : m.Data.HeaderFields);
                        Import(data, m.ReplyTo);
                    }, logger);

            //Export articles
            uiExportArticlesDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<string>(
                ArticleServiceMessage.ArticleExportNew);
            exportArticlesDisposable = subscriber.GetEvent<IMessage>()
                .Where(x => x.MessageType == ArticleServiceMessage.ArticleExportNew)
                .DurableSubscribe(
                    m =>
                    {
                        var userNotificationService = serviceLocator.Locate<IUserNotificationService>();
                        userNotificationService.SendNotificationToSystem(NotificationSeverity.Info, "Export of articles started. This can take a while.", "");

                        this.Export(m.ReplyTo);
                    }, logger);


            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ArticleServiceConfiguration>(ArticleServiceMessage.SaveArticleServiceConfiguration);
            ArticleServiceMessage.SaveArticleServiceConfiguration.OnMessage<ArticleServiceConfiguration>(eventAggregatorSubscriber, logger, UpdateArticleServiceConfiguration);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ArticleServiceMessage.GetCurrentArticleServiceConfiguration);
            ArticleServiceMessage.GetCurrentArticleServiceConfiguration.OnMessage(eventAggregatorSubscriber, logger, PublishCurrentArticleServiceConfigurationToUi);
        }

        private IEnumerable<object> ConvertRawDataToDynamic(string contents, char delimiter, char commentPrefix, string[] strings)
        {
            return new FileEnumerator(logger, contents, userNotificationService, delimiter, commentPrefix, strings).ToList();
        }

        public ResultTypes Create(Article<IProducible> article)
        {
            //ensure that article id is not guid.empty
            if (article.Id == Guid.Empty)
                article.Id = Guid.NewGuid();

            return articleServiceBusinessLayer.Create(article);
        }

        public ResultTypes Update(Article<IProducible> article)
        {
            return articleServiceBusinessLayer.Update(article);
        }

        public ResultTypes Delete(Article<IProducible> article)
        {
            return articleServiceBusinessLayer.Delete(article);
        }

        public ResultTypes Delete(IEnumerable<Article<IProducible>> articles)
        {
            return articleServiceBusinessLayer.Delete(articles);
        }

        public IEnumerable<Article<IProducible>> Search(string articleSearchPattern)
        {
            //todo: make this work correct like.
            return articleServiceBusinessLayer.Search(articleSearchPattern);

        }

        /// <summary>
        /// Find by Article Alias.  Exact match expected, use Search for partial match.
        /// </summary>
        /// <param name="alias">Exact match for Article.Alias property.</param>
        /// <returns>The article</returns>
        public Article<IProducible> FindByAlias(string alias)
        {
            return articleServiceBusinessLayer.FindByAlias(alias);
        }

        /// <summary>
        /// Get an article by Id.  Exact match only
        /// </summary>
        /// <param name="articleGuid"></param>
        /// <returns></returns>
        public Article<IProducible> Find(Guid articleGuid)
        {
            return articleServiceBusinessLayer.Find(articleGuid);
        }

        /// <summary>
        /// Export all articles
        /// </summary>
        /// <param name="replyTo">Where to send the exported articles</param>
        /// <returns></returns>
        public void Export(string replyTo)
        {
            articleServiceBusinessLayer.DispatchExportArticleWorkflow(replyTo);
        }

        /// <summary>
        /// Import all articles
        /// </summary>
        /// <param name="replyTo">Where to send the imported articles</param>
        /// <param name="articlesToImport">The raw article data to import</param>
        /// <returns></returns>
        public void Import(IEnumerable<object> articlesToImport, string replyTo)
        {
            articleServiceBusinessLayer.DispatchImportArticleWorkflow(articlesToImport, replyTo);
        }

        /// <summary>
        /// Get a collection of Articles based on the passed in Article.Id's  Exact match only.
        /// </summary>
        /// <param name="articleGuids"></param>
        /// <returns></returns>
        public IEnumerable<Article<IProducible>> Find(IEnumerable<Guid> articleGuids)
        {
            return articleServiceBusinessLayer.Find(articleGuids);
        }

        public string Name
        {
            get
            {
                return "ArticleServiceNew";
            }
        }

        private void PublishCurrentArticleServiceConfigurationToUi(IMessage msg)
        {
            uiCommunicationService.SendMessageToUI(new ResponseMessage<ArticleServiceConfiguration>
            {
                MessageType = ArticleServiceMessage.GetCurrentArticleServiceConfigurationResponse,
                ReplyTo = msg.ReplyTo,
                Result = ResultTypes.Success,
                Data = articleServiceBusinessLayer.CurrentArticleServiceConfiguration
            });
        }

        private void UpdateArticleServiceConfiguration(IMessage<ArticleServiceConfiguration> msg)
        {
            if (msg.Data == null)
                return;

            articleServiceBusinessLayer.UpdateArticleServiceConfiguration(msg.Data);

            uiCommunicationService.SendMessageToUI(new ResponseMessage()
            {
                MessageType = ArticleServiceMessage.SaveArticleServiceConfigurationResponse,
                ReplyTo = msg.ReplyTo
            });
        }

        ~ArticleService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (createArticleDisposable != null)
                    createArticleDisposable.Dispose();
                if (updateArticleDisposable != null)
                    updateArticleDisposable.Dispose();
                if (deleteArticlesDisposable != null)
                    deleteArticlesDisposable.Dispose();
                if (uiExportArticlesDisposable != null)
                    uiExportArticlesDisposable.Dispose();
                if (uiImportArticlesDisposable != null)
                    uiImportArticlesDisposable.Dispose();
                if (exportArticlesDisposable != null)
                    exportArticlesDisposable.Dispose();
                if (importArticlesDisposable != null)
                    importArticlesDisposable.Dispose();
                //if (finalizeArticleImportDisposable != null)
                //    finalizeArticleImportDisposable.Dispose();                
            }

            disposed = true;
        }
    }
}
