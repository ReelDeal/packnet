﻿namespace PackNet.Plugin.ArticleService
{
    using MongoDB.Bson.Serialization.Attributes;
    using PackNet.Common.Interfaces.Producible;
    using PackNet.Plugin.ArticleService.Enums;
    using PackNet.Common.Interfaces.DTO.Carton;

    public class ArticleImportItem : Article<IProducible>
    {
        [BsonIgnore]
        public ArticleImportAction ImportAction { get; set; }        
    }
}
