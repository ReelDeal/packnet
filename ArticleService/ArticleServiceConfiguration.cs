﻿using System;
using PackNet.Common.Interfaces.Repositories;
namespace PackNet.Plugin.ArticleService
{
    public class ArticleServiceConfiguration : IPersistable
    {
        public Guid Id { get; set; }

        public string ArticleImportFileDelimeter { get; set; }

        public string ArticleImportFileCommentPrefix { get; set; }
    }
}
