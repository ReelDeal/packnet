﻿namespace PackNet.Plugin.ArticleService.BusinessLayer
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using PackNet.Common;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Orders;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Interfaces.Producible;
    using PackNet.Common.Interfaces.Services;
    using PackNet.Plugin.ArticleService.Data;
    using PackNet.Plugin.ArticleService.Enums;

    /// <summary>
    /// In the database we use the class ArticleWrapper.  All of the method calls are exptecting to use 
    /// </summary>
    public class ArticleServiceBusinessLayer : IArticleServiceBusinessLayer
    {
        private readonly IArticleRepository articleRepository;
        private readonly IServiceLocator serviceLocator;
        private readonly IArticleConfigurationRepository configurationRepository;

        private readonly ILogger logger;

        bool disposed;
        private IWorkflowTrackingService workflowTrackingService;
        private IUserNotificationService userNotificationService;

        public ArticleServiceBusinessLayer(IArticleRepository repo, IServiceLocator serviceLocator, IArticleConfigurationRepository configRepo)
        {
            articleRepository = repo;
            configurationRepository = configRepo;

            this.serviceLocator = serviceLocator;

            logger = serviceLocator.Locate<ILogger>();
            RequestWorkflowTackingService(serviceLocator);
            RequestUserNotificationService(serviceLocator);
            logger.Log(LogLevel.Info, "ArticleServiceNew loaded");
        }

        private void RequestWorkflowTackingService(IServiceLocator serviceLocator)
        {
            try
            {
                this.workflowTrackingService = serviceLocator.Locate<IWorkflowTrackingService>();
            }
            catch (Exception)
            {
                IDisposable subscription = null;
                subscription = serviceLocator.ServiceAddedObservable.Subscribe(s =>
                {
                    if (s is IWorkflowTrackingService)
                    {
                        this.workflowTrackingService = s as IWorkflowTrackingService;
                        subscription.Dispose();
                    }
                });
            }
        }

        private void RequestUserNotificationService(IServiceLocator serviceLocator)
        {
            try
            {
                this.userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            }
            catch (Exception)
            {
                IDisposable subscription = null;
                subscription = serviceLocator.ServiceAddedObservable.Subscribe(s =>
                {
                    if (s is IUserNotificationService)
                    {
                        this.userNotificationService = s as IUserNotificationService;
                        subscription.Dispose();
                    }
                });
            }
        }

        public ResultTypes Create(Article<IProducible> article)
        {
            try
            {
                articleRepository.CreateEnsureNoDuplicate(article);

                return ResultTypes.Success;
            }
            catch (MongoDB.Driver.MongoDuplicateKeyException e)
            {
                logger.Log(LogLevel.Info, "Article With Alias {0} already exists.  Duplicates not allowed. {1}", article.Alias, e.ToString());
                return ArticleServiceResultTypes.ArticleAliasAlreadyInUse;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Warning, "Failed to delete article {0}", e, article.Alias);
            }
            return ResultTypes.Fail;

        }

        public ResultTypes ImportOrUpdate(Article<IProducible> article)
        {
            ResultTypes result;
            var foundArticle = this.articleRepository.FindByAlias(article.Alias);
            if (foundArticle == null)
            {
                result = this.Create(article);
            }
            else
            {
                article.Id = foundArticle.Id;
                result = this.Update(article);
            }
            return result;
        }

        public ArticleServiceConfiguration CurrentArticleServiceConfiguration
        {
            get { return configurationRepository.CurrentServiceConfiguration; }
        }

        public void UpdateArticleServiceConfiguration(ArticleServiceConfiguration newConfiguration)
        {
            configurationRepository.UpdateSldConfiguration(newConfiguration);
        }

        public Article<IProducible> Find(Guid articleGuid)
        {
            return articleRepository.Find(articleGuid);
        }

        public IEnumerable<Article<IProducible>> Find(IEnumerable<Guid> articleGuids)
        {
            return articleRepository.Find(articleGuids);
        }

        public ResultTypes Update(Article<IProducible> article)
        {
            try
            {
                articleRepository.Update(article);

                return ResultTypes.Success;
            }
            catch (MongoDB.Driver.MongoDuplicateKeyException e)
            {
                logger.Log(LogLevel.Info, "Article With Alias {0} already exists.  Duplicates not allowed. {1}", article.Alias, e.ToString());
                return ArticleServiceResultTypes.ArticleAliasAlreadyInUse;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Warning, "Failed to update article {0}", e, article.Alias);
            }
            return ResultTypes.Fail;

        }

        public ResultTypes Delete(Article<IProducible> article)
        {
            if (article == null)
                return ResultTypes.Fail;
            try
            {
                articleRepository.Delete(article);
                return ResultTypes.Success;
                //publisher.Publish(new Message<Article<IProducible>>() { MessageType = ArticleServiceMessage.ArticleDeletedNew, Data = article });
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Warning, "Failed to delete article {0}", e, article.Alias);
            }
            return ResultTypes.Fail;
        }

        public ResultTypes Delete(IEnumerable<Article<IProducible>> articles)
        {
            try
            {
                articleRepository.Delete(articles);
                return ResultTypes.Success;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Warning, "Failed to delete articles", e);
            }
            return ResultTypes.Fail;
        }

        public Tuple<ResultTypes, Dictionary<ArticleProductionRequest, Order>> CreateProducibleFromArticlesRequests(IEnumerable<ArticleProductionRequest> requests)
        {
            //todo: consider changing workflow to handle only one request at a time and in this method we Task off the creation of each request then join them at the end.
            ResultTypes result;
            Dictionary<ArticleProductionRequest, Order> produciblesCreated = null;
            try
            {
                var workflowResult = CreateFromArticleUsingWorkflow(requests);
                produciblesCreated = workflowResult["ProduciblesCreated"] as Dictionary<ArticleProductionRequest, Order>;
                result = ResultTypes.Success;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Warning, "Unhandled exception", e);
                result = ResultTypes.Fail;
            }
            return new Tuple<ResultTypes, Dictionary<ArticleProductionRequest, Order>>(result, produciblesCreated);
        }

        private IDictionary<string, object> CreateFromArticleUsingWorkflow(IEnumerable<ArticleProductionRequest> m)
        {
            //get the full location of the assembly with DaoTests in it
            string fullPath = System.Reflection.Assembly.GetAssembly(typeof(ArticleService)).Location;
            var workflowPath = Path.Combine(Path.GetDirectoryName(fullPath), @"Workflows\CreateProducibleFromArticle.xaml");
            var assembly = this.GetType().Assembly;
            var inArgs = new Dictionary<string, object>
            {
                { "ArticlesToCreate", m },
                { "ServiceLocator", serviceLocator }
            };
            WorkflowLifetime lt = null;
            if (this.workflowTrackingService != null)
            {
                var additionalInfo = new Dictionary<string, object>();                
                lt = workflowTrackingService.Factory(ArticleWorkflowTypes.CreateProducibleFromArticle, workflowPath, additionalInfo);
            }
            return WorkflowHelper.InvokeWorkFlowAndWaitForResult(workflowPath, assembly, inArgs, userNotificationService, logger, lt);
        }

        public Article<IProducible> FindByAlias(string alias)
        {
            return articleRepository.FindByAlias(alias);
        }

        public IEnumerable<Article<IProducible>> Search(string articleSearchPattern)
        {
            if (string.IsNullOrWhiteSpace(articleSearchPattern))
                return articleRepository.All();

            return articleRepository.Search(articleSearchPattern);
        }

        public void DispatchExportArticleWorkflow(string replyTo)
        {
            var fullPath = System.Reflection.Assembly.GetAssembly(typeof(ArticleService)).Location;
            var workflowPath = Path.Combine(Path.GetDirectoryName(fullPath), @"Workflows\ExportArticles.xaml");
            var assembly = GetType().Assembly;

            var inArgs = new Dictionary<string, object>()
                         {
                             { "ArticlesToExport", articleRepository.All() },
                             { "ReplyTo", replyTo},
                             { "ServiceLocator", serviceLocator}
                         };
            WorkflowLifetime lt = null;
            if (workflowTrackingService != null)
            {
                var additionalInfo = new Dictionary<string, object>();
                lt = workflowTrackingService.Factory(ArticleWorkflowTypes.CreateProducibleFromArticle, workflowPath, additionalInfo);
            }

            WorkflowHelper.InvokeWorkFlow(logger, workflowPath, assembly, inArgs, userNotificationService, lt);
        }

        public void DispatchImportArticleWorkflow(IEnumerable<object> articles, string replyTo)
        {
            var fullPath = System.Reflection.Assembly.GetAssembly(typeof(ArticleService)).Location;
            var workflowPath = Path.Combine(Path.GetDirectoryName(fullPath), @"Workflows\ImportArticles.xaml");
            var assembly = GetType().Assembly;

            var inArgs = new Dictionary<string, object>
                             {
                                 { "ArticlesToImport", articles },
                                 { "ServiceLocator", serviceLocator},
                                 { "ReplyTo", replyTo},
                             };
            WorkflowLifetime lt = null;
            if (workflowTrackingService != null)
            {
                var additionalInfo = new Dictionary<string, object>();
                lt = workflowTrackingService.Factory(ArticleWorkflowTypes.CreateProducibleFromArticle, workflowPath, additionalInfo);
            }

            WorkflowHelper.InvokeWorkFlow(logger, workflowPath, assembly, inArgs, userNotificationService, lt);
        }
        
        ~ArticleServiceBusinessLayer()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {

            }

            disposed = true;
        }
    }
}
