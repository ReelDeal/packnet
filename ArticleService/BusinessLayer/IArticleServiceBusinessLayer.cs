using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Plugin.ArticleService.BusinessLayer
{
    public interface IArticleServiceBusinessLayer
    {        
        Article<IProducible> Find(Guid articleGuid);

        IEnumerable<Article<IProducible>> Find(IEnumerable<Guid> articleGuids);

        ResultTypes Create(Article<IProducible> article);
        
        ResultTypes Update(Article<IProducible> article);

        ResultTypes ImportOrUpdate(Article<IProducible> article);

        ResultTypes Delete(Article<IProducible> article);

        ResultTypes Delete(IEnumerable<Article<IProducible>> articles);

        Article<IProducible> FindByAlias(string alias);

        IEnumerable<Article<IProducible>> Search(string articleSearchPattern);

        Tuple<ResultTypes, Dictionary<ArticleProductionRequest, Order>> CreateProducibleFromArticlesRequests(IEnumerable<ArticleProductionRequest> data);

        void DispatchExportArticleWorkflow(string replyTo);

        void DispatchImportArticleWorkflow(IEnumerable<object> articles, string replyTo);

        ArticleServiceConfiguration CurrentArticleServiceConfiguration { get; }

        void UpdateArticleServiceConfiguration(ArticleServiceConfiguration data);
    }
}