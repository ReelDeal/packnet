using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.WorkflowActivities;

namespace PackNet.Plugin.ArticleService
{

    public class ArticleConverter : JsonConverter
    {
        public override bool CanWrite
        {
            get { return false; }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            //canwrite is false so use the defualt serializer.
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // Load JObject from stream
            var jObject = JObject.Load(reader);

            var article = new Article<IProducible>();
            article.Alias = jObject["Alias"].Value<string>();

            JToken property;
            if (jObject.TryGetValue("Description", out property))
                article.Description = property.Value<string>();

            if (jObject.TryGetValue("Id", out property))
            {
                var g = Guid.Empty;
                article.Id = Guid.TryParse(property.Value<string>(), out g) ? g : Guid.NewGuid();
            }
            else
                article.Id = Guid.NewGuid();

            article.Producible = KitConverter.GetProducible(jObject.GetValue("Producible"), serializer);

            return article;

        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsTypeOrSubclassOf(typeof(Article<IProducible>));
        }
    }
}