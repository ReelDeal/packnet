﻿using System;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Plugin.ArticleService
{
    public class ArticleIdentifiersConverter : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var item = value as Article<IProducible>;
            serializer.Serialize(writer, new { Article = item.Alias,  item.Id });
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsTypeOrSubclassOf(typeof(Article<IProducible>));
        }
    }
}
