﻿using PackNet.Plugin.ArticleService;

namespace PackNet.Plugin.ArticleService.Data
{
    public interface IArticleConfigurationRepository 
    {
        void UpdateSldConfiguration(ArticleServiceConfiguration newServiceConfiguration);

        ArticleServiceConfiguration CurrentServiceConfiguration { get; }
    }
}
