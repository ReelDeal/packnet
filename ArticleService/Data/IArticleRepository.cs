﻿using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Plugin.ArticleService.Data
{
    public interface IArticleRepository : IRepository<Article<IProducible>>
    {
        Article<IProducible> FindByAlias(string alias);
    }
}
