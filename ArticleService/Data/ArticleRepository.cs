﻿using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using System.Linq;


namespace PackNet.Plugin.ArticleService.Data
{
    using MongoDB.Bson.Serialization;

    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Orders;
    using PackNet.Common.Interfaces.DTO.PrintingMachines;

    public class ArticleRepository : MongoDbRepository<Article<IProducible>>, IArticleRepository 
    {
        public ArticleRepository()
            : this(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "ArticlesNew")
        {
        }

        public ArticleRepository(string connectionString, string databaseName, string collectionName)
            : base(connectionString, databaseName, collectionName)
        {
            Collection.CreateIndex(new IndexKeysBuilder()
                .Ascending("Alias"), IndexOptions.SetUnique(true));

            if (!BsonClassMap.IsClassMapRegistered(typeof(Label)))
                BsonClassMap.RegisterClassMap<Label>();
            if (!BsonClassMap.IsClassMapRegistered(typeof(Order)))
                BsonClassMap.RegisterClassMap<Order>();
            if (!BsonClassMap.IsClassMapRegistered(typeof(Carton)))
                BsonClassMap.RegisterClassMap<Carton>();
            if (!BsonClassMap.IsClassMapRegistered(typeof(Kit)))
                BsonClassMap.RegisterClassMap<Kit>();


        }

        public Article<IProducible> FindByAlias(string alias)
        {
            return Collection.AsQueryable().FirstOrDefault(a => a.Alias == alias);
        }
    }
}
