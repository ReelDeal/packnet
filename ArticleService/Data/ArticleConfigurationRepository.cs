﻿using System.Linq;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;

namespace PackNet.Plugin.ArticleService.Data
{
    using PackNet.Plugin.ArticleService.Enums;

    public class ArticleConfigurationRepository  : MongoDbRepository<ArticleServiceConfiguration>, IArticleConfigurationRepository 
    {
        private readonly object syncObject = new object();
        private readonly bool serializerRegistered;

        public ArticleConfigurationRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "ArticleConfiguration")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                MongoDbHelpers.TryRegisterSerializer<ArticleImportAction>(new EnumerationSerializer());

                serializerRegistered = true;
            }

            if (All().Any() == false)
            {
                Create(new ArticleServiceConfiguration()
                {
                    ArticleImportFileCommentPrefix = "~",
                    ArticleImportFileDelimeter = ";"
                });
            }
        }

        public ArticleServiceConfiguration CurrentServiceConfiguration
        {
            get { return All().First(); }
        }

        public void UpdateSldConfiguration(ArticleServiceConfiguration newServiceConfiguration)
        {
            newServiceConfiguration.Id = this.CurrentServiceConfiguration.Id;

            Update(newServiceConfiguration);
        }

    }
}
