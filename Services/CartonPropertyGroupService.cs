﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Heaps;

using PackNet.Business.CartonPropertyGroup;
using PackNet.Business.ExtensionMethods;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.WorkflowTracking;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;

using IMessage = PackNet.Common.Interfaces.DTO.Messaging.IMessage;

namespace PackNet.Services
{
    public class CartonPropertyGroupService : ICartonPropertyGroupService
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ICartonPropertyGroups cartonPropertyGroups;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IServiceLocator serviceLocator;

        private readonly ILogger logger;

        private IDisposable createObserver;
        private IDisposable editObserver;
        private IDisposable deleteObserver;
        private IDisposable updateCartonPropertyGroupObserver;
        private readonly ConcurrentDictionary<Guid, IEnumerator<CartonPropertyGroup>> enumerators = new ConcurrentDictionary<Guid, IEnumerator<CartonPropertyGroup>>();
        private readonly ConcurrentDictionary<Guid, IList<Guid>> skipLists = new ConcurrentDictionary<Guid, IList<Guid>>();
        private readonly object enumeratorLock = new object();
        private IClassificationService classificationService;
        private IPickZoneService pickZoneService;

        public IClassificationService ClassificationService
        {
            get
            {
                return classificationService ?? (classificationService = serviceLocator.Locate<IClassificationService>());
            }
        }

        public IPickZoneService PickZoneService
        {
            get
            {
                return pickZoneService ?? (pickZoneService = serviceLocator.Locate<IPickZoneService>());
            }
        }

        public CartonPropertyGroupService(IEventAggregatorPublisher publisher, IEventAggregatorSubscriber subscriber, ICartonPropertyGroups cartonPropertyGroups, IUICommunicationService uiCommunicationService, IServiceLocator serviceLocator, ILogger logger)
        {
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.cartonPropertyGroups = cartonPropertyGroups;
            this.uiCommunicationService = uiCommunicationService;
            this.serviceLocator = serviceLocator;
            this.logger = logger;

            serviceLocator.RegisterAsService(this);
            SetupListeners();
        }

        public string Name
        {
            get { return "CartonPropertyGroupService"; }
        }

        public IEnumerable<CartonPropertyGroup> Groups { get { return cartonPropertyGroups.GetCartonPropertyGroups(); } }

        public CartonPropertyGroup GetCartonPropertyGroupByAlias(string alias)
        {
            return Groups.FirstOrDefault(g => g.Alias == alias);
        }

        public CartonPropertyGroup GetMatchingPropertyGroupForCarton(BoxFirstProducible producible)
        {
            // Return null CPG if this is a machine specific request
            return Groups.FirstOrDefault(cpg => cpg.IsCartonRequestInGroup(producible.Producible as ICarton));
        }

        public CartonPropertyGroup GetNextCPGToDispatchWorkTo(Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup,
            IEnumerable<IProducible> produciblesToGetCpgFor, IEnumerable<IProducible> availableProducibles)
        {
            if (!productionGroup.BoxFirstConfiguration().ConfiguredCartonPropertyGroups.Any())
                return null;
            if (produciblesToGetCpgFor == null || produciblesToGetCpgFor.Any() == false)
                return null;
            if (availableProducibles == null || availableProducibles.Any() == false)
                return null;

            lock (enumeratorLock)
            {
                var skipList = skipLists.GetOrAdd(productionGroup.Id, guid => new List<Guid>());
                var group = GetNextCpgFromSkipList(skipList, produciblesToGetCpgFor);
                if (group != null)
                    return group;

                return GetNextCpgFromMix(skipList, productionGroup, produciblesToGetCpgFor, availableProducibles);
            }
        }

        private CartonPropertyGroup GetNextCpgFromSkipList(IList<Guid> skipList, IEnumerable<IProducible> produciblesToGetCpgFor)
        {
            var distinctSkipList = skipList.Distinct();

            foreach(var cpgId in distinctSkipList)
            {
                var group = Groups.First(g => g.Id == cpgId);
                if (group != null && produciblesToGetCpgFor.Any(group.ProducibleIsAssignedToCpg))
                {
                    skipList.Remove(cpgId);
                    return group;
                }
            }
            return null;
        }

        private CartonPropertyGroup GetNextCpgFromMix(ICollection<Guid> skipList, Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup,
            IEnumerable<IProducible> produciblesToGetCpgFor, IEnumerable<IProducible> availableProducibles)
        {
            var tempSkip = new List<Guid>();

            var pgEnumerator = enumerators.GetOrAdd(productionGroup.Id, guid => new CartonPropertyGroupMixEnumerable(productionGroup, logger).GetEnumerator());
            CartonPropertyGroup cpg = null;
            do
            {
                pgEnumerator.MoveNext();
                if (produciblesToGetCpgFor.Any(pgEnumerator.Current.ProducibleIsAssignedToCpg))
                {
                    cpg = pgEnumerator.Current;
                }
                else if (availableProducibles.Any(pgEnumerator.Current.ProducibleIsAssignedToCpg))
                {
                    tempSkip.Add(pgEnumerator.Current.Id);
                }
            }
            while (cpg == null);

            var persistedCpg = cartonPropertyGroups.GetCartonPropertyGroupByAlias(cpg.Alias);
            if (persistedCpg != null && persistedCpg.Status != CartonPropertyGroupStatuses.Surge)
                tempSkip.ForEach(skipList.Add);

            logger.Log(LogLevel.Debug, "SkipList for Production Group {0} count is {1}", productionGroup.Alias, skipList.Count);

            return cpg;
        }

        public void DecreaseSurgeCounter(string cpgAlias)
        {
            var cpg = cartonPropertyGroups.GetCartonPropertyGroupByAlias(cpgAlias);
            if (cpg != null)
            {
                if (cpg.Status == CartonPropertyGroupStatuses.Surge)
                {
                    cpg.RemainingProduciblesToSurge--;
                    cartonPropertyGroups.Update(cpg);
                    if (cpg.RemainingProduciblesToSurge == 0)
                    {
                        cartonPropertyGroups.SetCartonPropertyGroupStatus(cpg.Alias, CartonPropertyGroupStatuses.Normal);
                    }
                }
            }
            else
            {
                logger.Log(LogLevel.Error, "Failed to lookup Carton Property Group {0}", cpgAlias);
            }
        }

        /// <summary>
        /// Add cpg to be skipped from the sequence one time.
        /// This is needed to get the correct mix when a cpg that was not next in mix was produced 
        /// due to tiling of next cpg was not a tiling option.
        /// <param name="groupToSkip"></param>
        /// </summary>
        public void AddCpgToSkipList(Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup, CartonPropertyGroup groupToSkip)
        {
            skipLists.AddOrUpdate(productionGroup.Id, new List<Guid>() { groupToSkip.Id }, (guid, skipList) =>
            {
                skipList.Add(groupToSkip.Id);
                return skipList;
            });
        }

        public void SetSurgeCount(string cpgAlias, int surgeCount)
        {
            var cpg = cartonPropertyGroups.GetCartonPropertyGroupByAlias(cpgAlias);
            if (cpg != null)
            {
                cpg.SurgeCount = surgeCount;
                cartonPropertyGroups.Update(cpg);
            }

        }

        public IEnumerable<CartonPropertyGroup> GetUnstoppedCartonPropertyGroups()
        {
            return Groups.Where(g => g.Status != CartonPropertyGroupStatuses.Stop).ToList();
        }

        public IEnumerable<CartonPropertyGroup> GetCartonPropertyGroupsByStatus(CartonPropertyGroupStatuses status)
        {
            return Groups.Where(g => g.Status == status).ToList();
        }

        private void SetupListeners()
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(CartonPropertyGroupMessages.GetCartonPropertyGroups);
            CartonPropertyGroupMessages.GetCartonPropertyGroups
                .OnMessage(subscriber, logger,
                    GetCartonPropertyGroups
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CartonPropertyGroup>(CartonPropertyGroupMessages.CreateCartonPropertyGroup);
            createObserver = subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(x => x.MessageType == CartonPropertyGroupMessages.CreateCartonPropertyGroup)
                .DurableSubscribe(msg =>
                {
                    var result = ResultTypes.Success;
                    Exception exception = null;

                    try
                    {
                        CreateCartonPropertyGroup(msg.Data);
                    }
                    catch (AlreadyPersistedException e)
                    {
                        result = ResultTypes.Exists;
                        exception = e;
                    }
                    catch (Exception e)
                    {
                        result = ResultTypes.Fail;
                        exception = e;
                    }
                    finally
                    {
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<CartonPropertyGroup>
                        {
                            MessageType = CartonPropertyGroupMessages.CartonPropertyGroupCreated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = msg.Data
                        });
                    }

                    LogResult(result,
                        "Carton Property Group with Alias: {0} was Created by User: {1}",
                        "Unable to create Carton Property Group with Alias: {0}, message: {2}",
                        string.Empty,
                        "Carton Property Group with Alias: {0} already exists in the database, message: {2}",
                        msg, exception);

                    if (result == ResultTypes.Success)
                    {
                        publisher.Publish(new Message { MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups });
                    }
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CartonPropertyGroup>(CartonPropertyGroupMessages.DeleteCartonPropertyGroup);
            deleteObserver = subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(msg => msg.MessageType == CartonPropertyGroupMessages.DeleteCartonPropertyGroup)
                .DurableSubscribe(msg =>
                {
                    var result = ResultTypes.Success;
                    Exception exception = null;
                    var message = string.Empty;
                    try
                    {
                        DeleteCartonPropertyGroup(msg.Data);
                    }
                    catch (Exception e)
                    {
                        exception = e;
                        result = ResultTypes.Fail;
                        if (e is InUseException)
                        {
                            message = "CartonPropertyGroupInProduction";
                        }
                        else if (e is RelationshipExistsException)
                        {
                            message = "CartonPropertyGroupRelationshipExists";
                        }
                    }
                    finally
                    {
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<CartonPropertyGroup>
                        {
                            MessageType = CartonPropertyGroupMessages.CartonPropertyGroupDeleted,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = msg.Data,
                            Message = message
                        });
                    }

                    LogResult(result,
                        "Carton Property Group with Alias: {0} was Deleted by User: {1}",
                        "Unable to delete Carton Property Group with Alias: {0}, message: {2}",
                        string.Empty,
                        string.Empty,
                        msg, exception);

                    if (result == ResultTypes.Success)
                    {
                        publisher.Publish(new Message { MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups });
                    }
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CartonPropertyGroup>(CartonPropertyGroupMessages.UpdateCartonPropertyGroup);
            editObserver = subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(msg => msg.MessageType == CartonPropertyGroupMessages.UpdateCartonPropertyGroup)
                .DurableSubscribe(msg =>
                {
                    var result = ResultTypes.Success;
                    Exception exception = null;
                    var message = string.Empty;
                    try
                    {
                        UpdateCartonPropertyGroup(msg.Data);
                    }
                    catch (AlreadyPersistedException e)
                    {
                        result = ResultTypes.Exists;
                        exception = e;
                    }
                    catch (Exception e)
                    {
                        result = ResultTypes.Fail;
                        exception = e;
                        if (e is InUseException)
                            message = "CartonPropertyGroupInProduction";
                    }
                    finally
                    {
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<CartonPropertyGroup>
                        {
                            MessageType = CartonPropertyGroupMessages.CartonPropertyGroupUpdated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = msg.Data,
                            Message = message
                        });
                    }

                    LogResult(result,
                        "Carton Property Group with Alias: {0} was Edited by User: {1}",
                        "Unable to edit Carton Property Group with Alias: {0}, message: {2}",
                        string.Empty,
                        string.Empty,
                        msg, exception);

                    if (result == ResultTypes.Success)
                    {
                        publisher.Publish(new Message { MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups });
                    }
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CartonPropertyGroup>(CartonPropertyGroupMessages.UpdateCartonPropertyGroupStatus);
            updateCartonPropertyGroupObserver = subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(msg => msg.MessageType == CartonPropertyGroupMessages.UpdateCartonPropertyGroupStatus)
                .DurableSubscribe(message =>
                {
                    var cartonPropertyGroup = message.Data;
                    SetCartonPropertyGroupStatus(cartonPropertyGroup);
                    publisher.Publish(new Message()
                    {
                        MessageType = SelectionAlgorithmMessages.SelectionEnvironmentChanged
                    });
                }, logger);

            subscriber.GetEvent<Message<IEnumerable<BoxFirstProducible>>>()
                .Where(msg => msg.MessageType == StagingMessages.ProduciblesStaged)
                .DurableSubscribe(UpdateProducibleCount, logger);

            subscriber.GetEvent<Message<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>()
                .Where(msg => msg.MessageType == ProductionGroupMessages.ProductionGroupChanged || msg.MessageType == ProductionGroupMessages.ProductionGroupDeleted)
                .DurableSubscribe(ProductionGroupUpdated, logger);
        }

        public CartonPropertyGroup CreateCartonPropertyGroup(CartonPropertyGroup cartonPropertyGroup)
        {
            return cartonPropertyGroups.Create(cartonPropertyGroup);
        }

        public void DeleteCartonPropertyGroup(CartonPropertyGroup cartonPropertyGroup)
        {
            cartonPropertyGroups.Delete(cartonPropertyGroup);
        }

        public CartonPropertyGroup UpdateCartonPropertyGroup(CartonPropertyGroup cartonPropertyGroup)
        {
            return cartonPropertyGroups.Update(cartonPropertyGroup);
        }

        public void SetCartonPropertyGroupStatus(CartonPropertyGroup cartonPropertyGroup)
        {
            cartonPropertyGroups.SetCartonPropertyGroupStatus(cartonPropertyGroup.Alias, cartonPropertyGroup.Status);
        }

        private void UpdateProducibleCount(Message<IEnumerable<BoxFirstProducible>> msg)
        {

            var classificationsGroupedByStatus = ClassificationService.Classifications.GroupBy(c => c.Status);
            var pickzonesGroupedByStatus = PickZoneService.PickZones.GroupBy(c => c.Status);

            if (msg.Data == null)
            {
                return;
            }

            if (msg.Data.Any() == false)
            {
                ResetCpgNumberOfRequestsCounters(msg);
                return;
            }

            var groups = msg.Data.GroupBy(p =>
            {
                var restriction = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>();
                return restriction != null ? restriction.Id : Guid.Empty;
            }).Select(g => new Tuple<Guid, int, List<BoxFirstProducible>>(g.Key, g.Count(), g.ToList()));


            foreach (var cpg in Groups)
            {
                var grp = groups.FirstOrDefault(g => g.Item1 == cpg.Id);
                if (grp != null)
                {
                    cpg.NumberOfRequests = grp.Item2;
                    cpg.NumberOfExpeditedRequests = grp.Item3.Count(p =>
                    {
                        //TODO: Optimize this code, it will be slow
                        var classification = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();
                        if (classification == null)
                            return false;

                        var expedited = classificationsGroupedByStatus.FirstOrDefault(c => c.Key == ClassificationStatuses.Expedite);
                        if (expedited == null)
                            return false;
                        return expedited.Any(e => e.Alias == classification.Alias);
                    });
                    cpg.NumberOfLastRequests = grp.Item3.Count(p =>
                    {
                        //TODO: Optimize this code, it will be slow
                        var classification = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();
                        if (classification == null)
                            return false;

                        var lastClassifications = classificationsGroupedByStatus.FirstOrDefault(c => c.Key == ClassificationStatuses.Last);
                        if (lastClassifications == null)
                            return false;
                        return lastClassifications.Any(e => e.Alias == classification.Alias);
                    });
                    cpg.NumberOfStoppedRequests = grp.Item3.Count(p =>
                    {
                        if (cpg.Status == CartonPropertyGroupStatuses.Stop)
                            return true;

                        //TODO: Optimize this code, it will be slow
                        var classification = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();
                        var pickZone = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<PickZone>();
                        if (classification == null && pickZone == null)
                            return false;

                        var stoppedClassifications = classificationsGroupedByStatus.FirstOrDefault(c => c.Key == ClassificationStatuses.Stop);
                        var stoppedPickzones = pickzonesGroupedByStatus.FirstOrDefault(c => c.Key == PickZoneStatuses.Stop);

                        if (stoppedClassifications == null && stoppedPickzones == null)
                            return false;

                        var hasStoppedClassification = stoppedClassifications != null &&
                            classification != null &&
                            stoppedClassifications.Any(e => e.Alias == classification.Alias);
                        var hasStoppedPickZone = stoppedPickzones != null && 
                            pickZone != null && 
                            stoppedPickzones.Any(e => e.Id == pickZone.Id);
                        return hasStoppedClassification || hasStoppedPickZone;
                    });
                    cpg.NumberOfNormalRequests = grp.Item3.Count(p =>
                    {
                        //TODO: Optimize this code, it will be slow
                        var classification = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();
                        var pickZone = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<PickZone>();
                        if (classification == null && pickZone == null)
                            return false;

                        var normalClassifications = classificationsGroupedByStatus.FirstOrDefault(c => c.Key == ClassificationStatuses.Normal);
                        var normalPickzones = pickzonesGroupedByStatus.FirstOrDefault(c => c.Key == PickZoneStatuses.Normal);

                        if (normalClassifications == null && normalPickzones == null)
                            return false;

                        var hasNormalClassification = normalClassifications != null &&
                            classification != null &&
                            normalClassifications.Any(e => e.Alias == classification.Alias);
                        var hasNormalPickZone = normalPickzones != null &&
                            pickZone != null &&
                            normalPickzones.Any(e => e.Id == pickZone.Id);
                        return hasNormalClassification || hasNormalPickZone;
                    });
                }
                else
                {
                    cpg.NumberOfRequests = 0;
                    cpg.NumberOfExpeditedRequests = 0;
                    cpg.NumberOfStoppedRequests = 0;
                    cpg.NumberOfNormalRequests = 0;
                    cpg.NumberOfLastRequests = 0;
                }
                cartonPropertyGroups.Update(cpg);
            }

            uiCommunicationService.SendMessageToUI(new Message()
            {
                MessageType = CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated,
                ReplyTo = msg.ReplyTo
            });
        }

        private void ResetCpgNumberOfRequestsCounters(IMessage msg)
        {
            foreach (var cpg in Groups)
            {
                cpg.NumberOfRequests = 0;
                cpg.NumberOfExpeditedRequests = 0;
                cpg.NumberOfLastRequests = 0;
                cpg.NumberOfNormalRequests = 0;
                cpg.NumberOfStoppedRequests = 0;
                cartonPropertyGroups.Update(cpg);
            }

            uiCommunicationService.SendMessageToUI(new Message()
            {
                MessageType = CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated,
                ReplyTo = msg.ReplyTo
            });
        }

        private void GetCartonPropertyGroups(IMessage msg)
        {
            ResultTypes result;
            var cpgs = new ConcurrentList<CartonPropertyGroup>();
            Exception exception = null;

            try
            {
                cpgs = GetCartonPropertyGroups();
                result = ResultTypes.Success;
            }
            catch (Exception e)
            {
                result = ResultTypes.Fail;
                exception = e;
            }

            uiCommunicationService.SendMessageToUI(new Message<IEnumerable<CartonPropertyGroup>>()
            {
                Data = cpgs,
                ReplyTo = msg.ReplyTo,
                MessageType = CartonPropertyGroupMessages.CartonPropertyGroups,
            }, false, true);

            LogResult(result, string.Empty,
                "Unable to retreive Carton Property Groups with message: {2}",
                string.Empty,
                string.Empty,
                new Message<CartonPropertyGroup>(), exception);
        }

        public ConcurrentList<CartonPropertyGroup> GetCartonPropertyGroups()
        {
            return new ConcurrentList<CartonPropertyGroup>(cartonPropertyGroups.GetCartonPropertyGroups().ToList());
        }

        private void ProductionGroupUpdated(Message<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> productionGroupMessage)
        {
            ResetEnumeratorAndSkipListForProductionGroup(productionGroupMessage.Data);
        }

        private void ResetEnumeratorAndSkipListForProductionGroup(Common.Interfaces.DTO.ProductionGroups.ProductionGroup pg)
        {
            lock (enumeratorLock)
            {
                logger.Log(LogLevel.Debug, String.Format("Resetting mix enumerator and skip list for production group '{0}'", pg.Alias));

                IEnumerator<CartonPropertyGroup> outValue;
                if (!enumerators.TryRemove(pg.Id, out outValue))
                {
                    logger.Log(LogLevel.Warning, string.Format("Failed to reset mix enumerator for production group '{0}'.", pg.Alias));
                }

                IList<Guid> outSkip;
                if(!skipLists.TryRemove(pg.Id, out outSkip))
                {
                    logger.Log(LogLevel.Warning, string.Format("Failed to reset skip list for production group '{0}'.", pg.Alias));
                }
            }
        }

        private
             void LogResult(Enumeration result, string successMessage, string failMessage, string partialSuccessMessage, string existsMessage, IMessage<CartonPropertyGroup> msg, Exception exception = null)
        {
            switch (result.DisplayName)
            {
                case "Success":
                    if (successMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Info,
                        string.Format(successMessage, msg.Data.Alias,
                            msg.UserName));
                    break;
                case "Fail":
                    if (failMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Error,
                        string.Format(failMessage, msg.Data.Alias, msg.UserName, exception));
                    break;
                case "PartialSuccess":
                    if (partialSuccessMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Info,
                        string.Format(partialSuccessMessage, msg.Data.Alias,
                            msg.UserName, exception));
                    break;
                case "Exists":
                    if (existsMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Error,
                        string.Format(existsMessage, msg.Data.Alias,
                            msg.UserName, exception));
                    break;
            }
        }

        public void Dispose()
        {
            if (createObserver != null)
                createObserver.Dispose();
            if (editObserver != null)
                editObserver.Dispose();
            if (deleteObserver != null)
                deleteObserver.Dispose();
            if (updateCartonPropertyGroupObserver != null)
                updateCartonPropertyGroupObserver.Dispose();
        }


    }

    public class HeapItem : IComparable<HeapItem>
    {
        public int ItemIndex { get; private set; }
        public int Count { get; set; }
        public double Frequency { get; private set; }
        public double Priority { get; set; }

        public HeapItem(int itemIndex, int count, int totalItems)
        {
            ItemIndex = itemIndex;
            Count = count;
            Frequency = (double)totalItems / Count;
            Priority = Frequency;
        }

        public int CompareTo(HeapItem other)
        {
            if (other == null) return 1;
            var rslt = Priority.CompareTo(other.Priority);
            return rslt;
        }
    }

    /// <summary>
    /// Generator that will iterate over the Mix quantity infinitely until disposed
    /// </summary>
    public class CartonPropertyGroupMixEnumerable : IEnumerable<CartonPropertyGroup>, IDisposable
    {
        private bool running = true;
        private readonly List<CartonPropertyGroup> productionList = new List<CartonPropertyGroup>();

        public CartonPropertyGroupMixEnumerable(Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup, ILogger logger)
        {
            var boxFirstConfig = productionGroup.BoxFirstConfiguration();

            var cpgCounts = boxFirstConfig.ConfiguredCartonPropertyGroups.Select(c => Convert.ToInt32(c.MixQuantity)).ToArray();

            var result = "";
            foreach (var index in OrderItems(cpgCounts))
            {
                result = result + boxFirstConfig.ConfiguredCartonPropertyGroups[index].Alias + ", ";
                productionList.Add(boxFirstConfig.ConfiguredCartonPropertyGroups[index]);
            }
            logger.Log(LogLevel.Info, "CartonPropertyGroup distribution: " + result);
        }

        public IEnumerator<CartonPropertyGroup> GetEnumerator()
        {
            var productionListEnumerator = productionList.GetEnumerator();
            while (running)
            {
                if (!productionListEnumerator.MoveNext())
                {
                    productionListEnumerator = productionList.GetEnumerator();
                    productionListEnumerator.MoveNext();
                }
                yield return productionListEnumerator.Current;
            }
        }

        //Evenly distributing items in a list http://blog.mischel.com/2015/03/26/evenly-distributing-items-in-a-list/
        public IEnumerable<int> OrderItems(int[] counts)
        {
            var totalItems = counts.Sum();

            // Create a heap of work items
            var workItems = counts
                .Select((count, i) => new HeapItem(i, count, totalItems));
            var workHeap = new MinDHeap<HeapItem>(2, workItems);

            while (workHeap.Count > 0)
            {
                var item = workHeap.RemoveRoot();
                yield return item.ItemIndex;
                if (--item.Count == 0)
                {
                    // all done with this item
                    continue;
                }
                // adjust the priority and put it back on the heap
                item.Priority += item.Frequency;
                workHeap.Insert(item);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        static double GreatestCommonDenominator(double a, double b)
        {
            return b == 0 ? a : GreatestCommonDenominator(b, a % b);
        }

        public void Dispose()
        {
            running = false;
        }
    }
}
