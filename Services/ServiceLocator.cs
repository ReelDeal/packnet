﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Plugins;

namespace PackNet.Services
{
    [Export(typeof(IServiceLocator))]
    public class ServiceLocator : IServiceLocator
    {
        private readonly string servicesDirectory;
        private readonly ILogger logger;
        private List<IService> staticServices;
        private Subject<IService> serviceAddedSubject = new Subject<IService>();

        public ServiceLocator(string servicesDirectory, IEnumerable<IService> staticServices, ILogger logger)
        {

            this.servicesDirectory = servicesDirectory;
            this.logger = logger;
            this.staticServices = staticServices.ToList();
            //Prime the list of static services so imports can access them in their constructors... namely eventing services
            //ILogger for example
            Services = staticServices; 
        }

        private void AddServices(List<IService> staticServicesList)
        {
            //Add static (non composed) services back into the list because mef removed them
            staticServices.AddRange(staticServicesList);
            var composedServices = Services.ToList();
            composedServices.AddRange(staticServicesList);
            Services = composedServices;
        }

        public void LoadPlugins()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(assembly));
            var batch = new CompositionBatch();
            batch.AddExportedValue(logger);
            batch.AddExportedValue(this);
            batch.AddPart(this);
            MefComposer.Compose(servicesDirectory, batch, catalog);
            Services.ForEach(s => logger.Log(LogLevel.Info, "Loaded IService plug-in '{0}'", s.Name));
            AddServices(staticServices);

            Enumeration.Primed = false;
        }

        [BsonIgnore]
        [JsonIgnore]
        public IObservable<IService> ServiceAddedObservable { get { return serviceAddedSubject.AsObservable(); }}

        [ImportMany(typeof(IService))]
        [BsonIgnore]
        [JsonIgnore]
        public IEnumerable<IService> Services { get; set; }

        /// <summary>
        /// This is meant for work flows only and plugins only.
        /// If a service is not available and exception will be raised.  In the case where you need a service that has not yet been loaded.  Catch the exception and wire
        /// up to the ServiceAddedObservable to get access to it when it is brought in. 
        /// </summary>
        /// <typeparam name="T">The type of service to look for</typeparam>
        /// <returns>The first service that implements the type passed</returns>
        /// <exception cref="System.Exception"></exception>
        public T Locate<T>() where T : IService
        {
            var service = (T)Services.FirstOrDefault(x => x is T);
            if(service == null)
                throw new Exception(String.Format("Failed to locate service of type '{0}'. Make sure the service has been made available to the service locator", typeof(T)));
            return service;
        }

        public bool TryLocate<T>(out T service) where T : IService
        {
            try
            {
                service = Locate<T>();
            }
            catch
            {
                service = default(T);
                return false;
            }
            return true;
        }

        public void RegisterAsService<T>(T service) where T : IService
        {
            T foundService;
            if (!TryLocate<T>(out foundService))
            {
                AddServices(new List<IService> { service });
                serviceAddedSubject.OnNext(service);
            }
        }

        /// <summary>
        /// This is meant for work flows only.
        /// </summary>
        /// <typeparam name="T">The type of service to look for</typeparam>
        /// <returns>Any service that implements the type passed</returns>
        public IEnumerable<T> LocateMany<T>() where T : IService
        {
            return Services.Where(x => x is T).Cast<T>().ToList();
        }

        private static void TryDispose(IService service)
        {
            try
            {
                service.Dispose();
            }
            catch
            {
            }
        }
        public void Dispose()
        {
            Services.ForEach(TryDispose);
        }
    }
}