﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommonLogging = Common.Logging;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

using Quartz;
using Quartz.Impl;

namespace PackNet.Services.Scheduler
{
    public class SchedulerService : ISchedulerService
    {
        private IEventAggregatorPublisher publisher;
        private IEventAggregatorSubscriber subscriber;
        private IServiceLocator serviceLocator;
        private IUICommunicationService uiCommunicationService;
        private ILogger logger;
        private IScheduler scheduler;

        public SchedulerService(IUICommunicationService uiCommunicationService, 
            IEventAggregatorPublisher publisher, 
            IEventAggregatorSubscriber subscriber, 
            IServiceLocator serviceLocator, 
            ILogger logger)
        {
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.serviceLocator = serviceLocator;
            this.uiCommunicationService = uiCommunicationService;
            this.logger = logger;

            CommonLogging.LogManager.Adapter = new CommonLogging.Simple.ConsoleOutLoggerFactoryAdapter { Level = CommonLogging.LogLevel.Info };

            this.scheduler = StdSchedulerFactory.GetDefaultScheduler();
            this.scheduler.StartDelayed(TimeSpan.FromSeconds(60)); //Start after a minute

            serviceLocator.RegisterAsService(this);
        }

        public void Dispose()
        {
            scheduler.Shutdown();
        }

        public string Name { get { return "Scheduler Service"; } }
    }
}
