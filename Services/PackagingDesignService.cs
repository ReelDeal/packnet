﻿using System.Diagnostics;

using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PackNet.Services
{
    public class PackagingDesignService : IPackagingDesignService
    {
        private readonly IPackagingDesignManager packagingDesignManager;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IServiceLocator serviceLocator;
        private readonly ILogger logger;
        private bool disposed;
        private readonly IDisposable uiGetPackagingDesignsDisposable;
        private readonly IDisposable getPackagingDesignsDisposable;

        public string Name { get { return "DesignService"; } }

        public PackagingDesignService(IPackagingDesignManager packagingDesignManager, 
            IUICommunicationService uiCommunicationService, 
            IEventAggregatorSubscriber subscriber, 
            IServiceLocator serviceLocator, ILogger logger)
        {
            this.packagingDesignManager = packagingDesignManager;
            this.uiCommunicationService = uiCommunicationService;
            this.subscriber = subscriber;
            this.serviceLocator = serviceLocator;
            this.logger = logger;

            uiGetPackagingDesignsDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator(PackagingDesignMessages.GetPackagingDesigns);
            getPackagingDesignsDisposable =
                PackagingDesignMessages.GetPackagingDesigns
                    .OnMessage(subscriber, logger,
                        msg => uiCommunicationService.SendMessageToUI(new Message<IList<IPackagingDesign>>
                        {
                            MessageType = PackagingDesignMessages.PackagingDesigns,
                            ReplyTo = msg.ReplyTo,
                            Data = packagingDesignManager.GetDesigns().ToList()
                        }, false, true));

            uiGetPackagingDesignsDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CartonOnCorrugate>(PackagingDesignMessages.GetPackagingDesignWithAppliedValues);
            getPackagingDesignsDisposable =
                PackagingDesignMessages.GetPackagingDesignWithAppliedValues
                    .OnMessage<CartonOnCorrugate>(subscriber, logger,
                        msg =>
                        {
                            var corrugate = new Corrugate
                            {
                                Alias = "Garbage",
                                Thickness = 0.16,
                                Width = 500
                            };

                            var design = packagingDesignManager.GetPhyscialDesignForCarton(GetCarton(msg.Data), corrugate, msg.Data.Rotation);

                            if (design != null)
                            {
                                uiCommunicationService.SendMessageToUI(new Message<PhysicalDesign>
                                {
                                    MessageType = PackagingDesignMessages.PackagingDesignWithAppliedValues,
                                    ReplyTo = msg.ReplyTo,
                                    Data = design
                                });
                            }
                        });
            
            serviceLocator.RegisterAsService(this);
        }

        private ICarton GetCarton(CartonOnCorrugate cartonOnCorrugate)
        {
            var result = cartonOnCorrugate.OriginalCarton;
            //Ugly Hack: Begin
            //It was requested to use the value 5 on LWH when the values are empty in the UI, however OriginalCarton doesn't support nullable values
            //and it converts negative values to 0, so, the value 5 is going to be used only when LWH are all in zero.
            if (result.Height.Equals(0) &&
                result.Length.Equals(0) &&
                result.Width.Equals(0))
            {
                result.Height = 5;
                result.Length = 5;
                result.Width = 5;

                //Set up the x values if the design has one
                var design = packagingDesignManager.GetDesignFromId(cartonOnCorrugate.OriginalCarton.DesignId);
                result.XValues = design.DesignParameters.ToDictionary<DesignParameter, string, double>(designParameter => designParameter.Name, designParameter => 3);
            }

            //UglyHack: End
            return result;
        }

        public PhysicalDesign GetDesignForCarton(ICarton carton, Corrugate corrugate, OrientationEnum rotation)
        {
            return packagingDesignManager.GetPhyscialDesignForCarton(carton, corrugate, rotation);
        }

        public PackagingDesign GetDesignFromId(int designId)
        {
            return packagingDesignManager.GetDesignFromId(designId);
        }

        public string GetDesignName(int designId)
        {
            return packagingDesignManager.GetDesignName(designId);
        }

        public bool HasDesignWithId(int designId)
        {
            return packagingDesignManager.HasDesignWithId(designId);
        }
        
        ~PackagingDesignService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (uiGetPackagingDesignsDisposable != null)
                    uiGetPackagingDesignsDisposable.Dispose();
                if (getPackagingDesignsDisposable != null)
                    getPackagingDesignsDisposable.Dispose();
            }

            disposed = true;
        }
    }
}
