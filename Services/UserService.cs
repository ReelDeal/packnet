﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Reactive.Linq;
using PackNet.Business.UserManagement;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services
{
    using System.Linq;

    using Common.Interfaces.Exceptions;

    public class UserService : IUserService
    {
        private readonly IUsers users;
        private readonly ILogger logger;
        private readonly IPreferences preferences;
        private readonly IPackNetServerSettingsService packNetServerSettingsService;
        private readonly IServiceLocator serviceLocator;
        public readonly Dictionary<string, DateTime> LoggedInUsers = new Dictionary<string, DateTime>();

        public UserService(IUsers users, 
            IEventAggregatorPublisher publisher, 
            IEventAggregatorSubscriber subscriber, 
            IUICommunicationService uiCommunicationService, 
            IPreferences preferences, 
            IPackNetServerSettingsService packNetServerSettingsService, 
            IServiceLocator serviceLocator, 
            ILogger logger)
        {
            this.users = users;
            this.preferences = preferences;
            this.packNetServerSettingsService = packNetServerSettingsService;
            this.serviceLocator = serviceLocator;
            this.logger = logger;

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<User>(UserMessages.LoginUser);
            subscriber.GetEvent<IMessage<User>>()
                .Where(m => m.MessageType == UserMessages.LoginUser)
                .DurableSubscribe(
                    m =>
                    {
                        var user = Login(m.Data);
                        var response = new ResponseMessage<User>
                        {
                            MessageType = UserMessages.UserLoggedIn,
                            Result = user == null ? ResultTypes.Fail : ResultTypes.Success,
                            Data = user,
                            ReplyTo = m.ReplyTo
                        };
                        uiCommunicationService.SendMessageToUI(response);
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<User>(UserMessages.LoginUserByToken);
            subscriber.GetEvent<IMessage<User>>()
              .Where(m => m.MessageType == UserMessages.LoginUserByToken)
              .DurableSubscribe(
                  m =>
                  {
                      var user = ValidateToken(m.Data);
                      var response = new ResponseMessage<User>
                      {
                          MessageType = UserMessages.UserLoggedIn,
                          Result = user == null ? ResultTypes.Fail : ResultTypes.Success,
                          Data = user,
                          ReplyTo = m.ReplyTo
                      };
                      uiCommunicationService.SendMessageToUI(response);
                  }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(UserMessages.AutoLogin);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == UserMessages.AutoLogin)
                .DurableSubscribe(m => GetAutoLoginUser(), logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(UserMessages.GetUsers);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == UserMessages.GetUsers)
                .DurableSubscribe(
                    m =>
                    {
                        var allUsers = All().ToList();
                        uiCommunicationService.SendMessageToUI(new Message<List<User>>
                                               {
                                                   MessageType = UserMessages.Users,
                                                   Data = allUsers,
                                                   ReplyTo = m.ReplyTo
                                               });
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<User>(UserMessages.CreateUser);
            subscriber.GetEvent<IMessage<User>>()
                .Where(m => m.MessageType == UserMessages.CreateUser)
                .DurableSubscribe(
                    m =>
                    {
                        var result = ResultTypes.Success;
                        User user = null;
                        try
                        {
                            user = Create(m.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }

                        var response = new ResponseMessage<User>
                        {
                            MessageType = UserMessages.UserCreated,
                            Result = result,
                            Data = user,
                            ReplyTo = m.ReplyTo
                        };

                        uiCommunicationService.SendMessageToUI(response);
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<User>(UserMessages.UpdateUser);
            subscriber.GetEvent<IMessage<User>>()
                .Where(m => m.MessageType == UserMessages.UpdateUser)
                .DurableSubscribe(
                    m =>
                    {
                        var result = ResultTypes.Success;
                        User user = null;
                        try
                        {
                            user = Update(m.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }

                        var response = new ResponseMessage<User>
                        {
                            MessageType = UserMessages.UserUpdated,
                            Result = result,
                            Data = user,
                            ReplyTo = m.ReplyTo
                        };

                        uiCommunicationService.SendMessageToUI(response);
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<User>(UserMessages.DeleteUser);
            subscriber.GetEvent<IMessage<User>>()
                .Where(m => m.MessageType == UserMessages.DeleteUser)
                .DurableSubscribe(
                    m =>
                    {
                        var result = ResultTypes.Success;
                        try
                        {
                            Delete(m.Data);
                        }
                        catch (Exception)
                        {
                            result = ResultTypes.Fail;
                        }

                        var response = new ResponseMessage<User>()
                        {
                            MessageType = UserMessages.UserDeleted,
                            Data = m.Data,
                            Result = result,
                            ReplyTo = m.ReplyTo
                        };

                        publisher.Publish(response);
                        uiCommunicationService.SendMessageToUI(response);
                        publisher.Publish(new Message() { MessageType = UserMessages.GetUsers, UserName = m.UserName });
                    }, logger);

            subscriber.GetEvent<Message>()
                .Where(m => m.MessageType == UserMessages.AutoLogin)
                .DurableSubscribe(
                    m =>
                    {
                        var user = GetAutoLoginUser();
                        uiCommunicationService.SendMessageToUI(new Message<User>
                        {
                            MessageType = UserMessages.AutoLoggedIn,
                            Data = user,
                            ReplyTo = m.ReplyTo
                        });
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Preference>(UserMessages.UpdateUserPreferences);
            subscriber.GetEvent<IMessage<Preference>>()
                .Where(m => m.MessageType == UserMessages.UpdateUserPreferences)
                .DurableSubscribe(m =>
                {
                    Preference p = null;
                    var updateResult = ResultTypes.Success;

                    try
                    {
                        p = UpdatePreferences(m.Data);
                    }
                    catch
                    {
                        updateResult = ResultTypes.Fail;
                    }

                    var response = new ResponseMessage<Preference>
                    {
                        MessageType = UserMessages.UserPreferencesUpdated,
                        Result = updateResult,
                        Data = p,
                        ReplyTo = m.ReplyTo
                    };

                    uiCommunicationService.SendMessageToUI(response);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<User>(UserMessages.FindUserPreferences);
            subscriber.GetEvent<IMessage<User>>()
                .Where(m => m.MessageType == UserMessages.FindUserPreferences)
                .DurableSubscribe(m =>
                {
                    Preference p = null;
                    var findResult = ResultTypes.Success;

                    try
                    {
                        p = FindPreferences(m.Data);
                    }
                    catch
                    {
                        findResult = ResultTypes.Fail;
                    }

                    var response = new ResponseMessage<Preference>
                    {
                        MessageType = UserMessages.UserPreferences,
                        Result = findResult,
                        Data = p,
                        ReplyTo = m.ReplyTo
                    };

                    uiCommunicationService.SendMessageToUI(response);
                }, logger);

            //TODO: how is logoff working?
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<User>(UserMessages.LogoffUser);

            serviceLocator.RegisterAsService(this);
        }

        public Preference FindPreferences(User user)
        {
            Contract.Requires(user != null, "User cannot be null");
            return preferences.Find(user);
        }

        public Preference UpdatePreferences(Preference preference)
        {
            Contract.Requires(preferences != null, "Preferences cannot be null");

            return preferences.Update(preference);
        }

        public IEnumerable<User> Users { get { return users.All(); } }

        public User Login(User user)
        {
            var dbUser = users.Login(user.UserName, user.Password);
            if (dbUser != null)
            {
                if (LoggedInUsers.ContainsKey(dbUser.Token))
                {
                    LoggedInUsers[dbUser.Token] = DateTime.UtcNow;
                }
                else
                {
                    LoggedInUsers.Add(dbUser.Token, DateTime.UtcNow);
                }
            }

            return dbUser;
        }

        public User ValidateToken(User user)
        {
            var foundUser = users.ValidateToken(user.Token);

            if (foundUser != null)
            {
                var loggedInBefore = LoggedInUsers.ContainsKey(foundUser.Token);
                if (loggedInBefore)
                {
                    var timeLoggedIn = LoggedInUsers[foundUser.Token];
                    var packNetSettings = packNetServerSettingsService.GetSettings();

                    if (timeLoggedIn.AddSeconds(packNetSettings.LoginExpiresSeconds) >= DateTime.UtcNow)
                    {
                        LoggedInUsers[foundUser.Token] = DateTime.UtcNow;

                        return foundUser;
                    }
                }
            }

            return null;
        }

        public User GetAutoLoginUser()
        {
            return users.FindAutoLoginUser();
        }

        public User Create(User user)
        {
            Contract.Requires(user != null, "User cannot be null");

            try
            {
                return users.Create(user);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to create user " + user.UserName, e);
                throw;
            }
        }

        public User Update(User user)
        {
            Contract.Requires(user != null, "User cannot be null");

            try
            {
                return users.Update(user);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to update user " + user.UserName, e);
                throw;
            }
        }

        public void Delete(User user)
        {
            Contract.Requires(user != null, "User cannot be null");

            try
            {
                if (user.UserName == "PacksizeAdmin")
                {
                    throw new Exception("Default user can not be deleted.");
                }
                users.Delete(user);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to delete user " + user.UserName, e);
                throw;
            }
        }

        public IEnumerable<User> All()
        {
            try
            {
                return users.All();
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Error when Finding all users", e);
            }

            return new List<User>();
        }

        public User Find(string userName)
        {
            Contract.Requires(!String.IsNullOrEmpty(userName), "Username cannot be null or empty");

            try
            {
                return users.Find(userName);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, String.Format("Error when finding user by username: {0}", userName), e);
            }
            return null;
        }

        public User Find(Guid id)
        {
            Contract.Requires(id != Guid.Empty, "You must specify a guid that is not empty");

            try
            {
                return users.Find(id);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, String.Format("Error when finding user by id: {0}", id.ToString("D")), e);
            }

            return null;
        }

        public void Dispose()
        {
        }

        public string Name { get { return "UserService"; } }
    }
}