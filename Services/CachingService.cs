﻿using PackNet.Common.Interfaces.Caching;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services
{
    public class CachingService : ICachingService
    {
        private readonly IMemoryCache memoryCache;

        public CachingService(IMemoryCache memoryCache)
        {
            this.memoryCache = memoryCache;
        }

        public bool TryGet<T>(string cacheKey, out T item)
        {
            return memoryCache.TryGet(cacheKey, out item);
        }

        public T GetItem<T>(string cacheKey)
        {
            return memoryCache.GetItem<T>(cacheKey);
        }

        public void CacheItem(string cacheKey, object obj)
        {
            memoryCache.CacheItem(cacheKey, obj);
        }
    }
}