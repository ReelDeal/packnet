﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Business.ProductionGroups;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;

namespace PackNet.Services.ProductionGroup
{
    public class ProductionGroupService : IProductionGroupService
    {
        private readonly IProductionGroup productionGroups;
        private readonly ICartonPropertyGroupService cartonPropertyGroupService;

        public string Name
        {
            get { return "Production Group Service"; }
        }

        public IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> ProductionGroups
        {
            get { return productionGroups.GetProductionGroups(); }
        }

        public ProductionGroupService(IProductionGroup productionGroups,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            ICartonPropertyGroupService cartonPropertyGroupService,
            ILogger logger)
        {
            this.productionGroups = productionGroups;
            this.cartonPropertyGroupService = cartonPropertyGroupService;

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ProductionGroupMessages.GetProductionGroups);
            ProductionGroupMessages.GetProductionGroups
                .OnMessage(subscriber, logger,
                    m => uiCommunicationService.SendMessageToUI(
                        new Message<IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>
                        {
                            MessageType = ProductionGroupMessages.ProductionGroups,
                            ReplyTo = m.ReplyTo,
                            Data = productionGroups.GetProductionGroups()
                        })
                );

            uiCommunicationService
                .RegisterUIEventWithInternalEventAggregator<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>(
                    ProductionGroupMessages.CreateProductionGroup);
            ProductionGroupMessages.CreateProductionGroup
                .OnMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;

                        Common.Interfaces.DTO.ProductionGroups.ProductionGroup createdProductionGroup = null;
                        try
                        {
                            createdProductionGroup = Create(msg.Data);
                            UpdateCpgSurgeCount(createdProductionGroup);
                        }
                        catch (Exception e)
                        {
							result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage
                            <Common.Interfaces.DTO.ProductionGroups.ProductionGroup>
                        {
                            MessageType = ProductionGroupMessages.ProductionGroupCreated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = createdProductionGroup
                        });
                    }
                );

            uiCommunicationService
                .RegisterUIEventWithInternalEventAggregator<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>(
                    ProductionGroupMessages.UpdateProductionGroup);
            ProductionGroupMessages.UpdateProductionGroup
                .OnMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;

                        Common.Interfaces.DTO.ProductionGroups.ProductionGroup updatedProductionGroup = null;
                        try
                        {
                            updatedProductionGroup = Update(msg.Data);
                            UpdateCpgSurgeCount(updatedProductionGroup);
                        }
						catch (Exception e)
						{
							result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage
                            <Common.Interfaces.DTO.ProductionGroups.ProductionGroup>
                        {
                            MessageType = ProductionGroupMessages.ProductionGroupUpdated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = updatedProductionGroup
                        });
                    }
                );

            uiCommunicationService
                .RegisterUIEventWithInternalEventAggregator<List<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>(
                    ProductionGroupMessages.DeleteProductionGroup);
            ProductionGroupMessages.DeleteProductionGroup
                .OnMessage<List<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>(subscriber, logger,
                    msg =>
                    {
                        foreach (var pg in msg.Data)
                        {
                            var result = ResultTypes.Success;

                            try
                            {
                                Delete(pg);
                            }
                            catch
                            {
                                result = ResultTypes.Fail;
                            }
                            finally
                            {
                                uiCommunicationService.SendMessageToUI(new ResponseMessage
                                    <Common.Interfaces.DTO.ProductionGroups.ProductionGroup>
                                {
                                    MessageType = ProductionGroupMessages.ProductionGroupDeleted,
                                    Result = result,
                                    ReplyTo = msg.ReplyTo,
                                    Data = pg
                                });
                            }
                        }
                    }
                );

            uiCommunicationService
                .RegisterUIEventWithInternalEventAggregator<List<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>(
                    ProductionGroupMessages.UpdateProductionGroups);
            ProductionGroupMessages.UpdateProductionGroups
                .OnMessage<List<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>(subscriber, logger,
                    msg =>
                    {
                        var result = true;
                        foreach (var updatedPg in msg.Data.Select(productionGroups.Update))
                        {
                            try
                            {
                                UpdateCpgSurgeCount(updatedPg);
                            }
                            catch (Exception)
                            {
                                result = false;
                            }
                        }

                        uiCommunicationService.SendMessageToUI(new ResponseMessage
                        {
                            MessageType = ProductionGroupMessages.ProductionGroupUpdated,
                            Result = result ? ResultTypes.Success : ResultTypes.Fail
                        }
                            );

                        uiCommunicationService.SendMessageToUI(
                            new Message<IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>
                            {
                                MessageType = ProductionGroupMessages.ProductionGroups,
                                ReplyTo = msg.ReplyTo,
                                Data = productionGroups.GetProductionGroups()
                            });
                    }
                );

            serviceLocator.RegisterAsService(this);
        }

        private void UpdateCpgSurgeCount(Common.Interfaces.DTO.ProductionGroups.ProductionGroup data)
        {
            var boxFirstConfig = data.BoxFirstConfiguration();
            if (boxFirstConfig != null)
            {
                boxFirstConfig.ConfiguredCartonPropertyGroups.ForEach(
                    cpg => cartonPropertyGroupService.SetSurgeCount(cpg.Alias, boxFirstConfig.SurgeCount));
            }
        }

        public void Dispose()
        {
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup Create(
            Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            return productionGroups.Create(productionGroup);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup Update(
            Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            return productionGroups.Update(productionGroup);
        }

        public void Delete(Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            productionGroups.Delete(productionGroup);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupId(Guid id)
        {
            return productionGroups.FindByCartonPropertyGroupId(id);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupId(IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> productionGroupsList, Guid id)
        {
            return productionGroups.FindByCartonPropertyGroupId(productionGroupsList, id);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupAlias(string alias)
        {
            return productionGroups.FindByCartonPropertyGroupAlias(alias);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByCartonPropertyGroupAlias(IEnumerable<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> productionGroupsList, string alias)
        {
            return productionGroups.FindByCartonPropertyGroupAlias(productionGroupsList, alias);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup Find(Guid id)
        {
            return productionGroups.Find(id);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup FindByAlias(string alias)
        {
            return productionGroups.FindByAlias(alias);
        }

        public Common.Interfaces.DTO.ProductionGroups.ProductionGroup GetProductionGroupForMachineGroup(Guid machineGroupId)
        {
            return productionGroups.GetProductionGroupForMachineGroup(machineGroupId);
        }

        public ConcurrentList<Common.Interfaces.DTO.ProductionGroups.ProductionGroup> GetProductionGroupsForCorrugate(Guid corrugateId)
        {
            return productionGroups.GetProductionGroupsForCorrugate(corrugateId);
        }

        public void RemoveCorrugateFromProductionGroups(Corrugate corrugate)
        {
            productionGroups.RemoveCorrugateFromProductionGroups(corrugate);
        }

        public void RemoveMachineGroupFromProductionGroup(MachineGroup machineGroup)
        {
            productionGroups.RemoveMachineGroupFromProductionGroup(machineGroup);
        }
    }
}