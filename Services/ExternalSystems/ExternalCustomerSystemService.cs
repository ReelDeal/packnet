﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;

using Newtonsoft.Json;

using PackNet.Business.ExternalSystems;
using PackNet.Common;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.ExternalSystems;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.ExternalSystems;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Utils;

namespace PackNet.Services.ExternalSystems
{
    public class ExternalCustomerSystemService : IMachineService<ExternalCustomerSystem>, IExternalCustomerSystemService
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly IExternalCustomerSystems externalCustomerSystems;
        private readonly IUserNotificationService userNotificationService;
        private readonly IServiceLocator serviceLocator;
        private readonly ILogger logger;
        private IAggregateMachineService machineService;
        private const string workflowPath = "%PackNetWorkflows%\\ExternalSystems";

        public ExternalCustomerSystemService(IEventAggregatorSubscriber subscriber, 
                                                IEventAggregatorPublisher publisher, 
                                                IExternalCustomerSystems externalCustomerSystems, 
                                                IServiceLocator serviceLocator, 
                                                IUICommunicationService uiCommunicationService,
                                                ILogger logger)
        {
            this.publisher = publisher;
            this.externalCustomerSystems = externalCustomerSystems;
            this.serviceLocator = serviceLocator;
            this.logger = logger;

            this.userNotificationService = serviceLocator.Locate<UserNotificationService>();

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ExternalCustomerSystemMessages.GetExternalCustomerSystemWorkflows);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == ExternalCustomerSystemMessages.GetExternalCustomerSystemWorkflows)
                .DurableSubscribe(msg =>
                {
                    var path = DirectoryHelpers.ReplaceEnvironmentVariables(workflowPath);
                    logger.Log(LogLevel.Trace, "Loading ExternalSystems workflows from path {0}", path);
                    var payload = Directory.EnumerateFiles(path, "*.xaml")
                        .Select(f =>
                                new KeyValuePair<string, string>(Path.GetFileNameWithoutExtension(f),
                                    Path.Combine(workflowPath, Path.GetFileName(f)))).ToList();

                    uiCommunicationService.SendMessageToUI(new ResponseMessage<List<KeyValuePair<string, string>>>
                    {
                        MessageType = ExternalCustomerSystemMessages.ExternalCustomerSystemWorkflows,
                        ReplyTo = msg.ReplyTo,
                        Data = payload
                    });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ExternalCustomerSystemMessages.GetExternalCustomerSystems);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == ExternalCustomerSystemMessages.GetExternalCustomerSystems)
                .DurableSubscribe(
                    m =>
                    {
                        var all = externalCustomerSystems.All().ToList();
                        uiCommunicationService.SendMessageToUI(new Message<List<ExternalCustomerSystem>>
                        {
                            MessageType = ExternalCustomerSystemMessages.ExternalCustomerSystems,
                            Data = all,
                            ReplyTo = m.ReplyTo
                        });
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ExternalCustomerSystem>(ExternalCustomerSystemMessages.CreateExternalCustomerSystem);
            subscriber.GetEvent<IMessage<ExternalCustomerSystem>>()
                .Where(m => m.MessageType == ExternalCustomerSystemMessages.CreateExternalCustomerSystem)
                .DurableSubscribe(
                    m =>
                    {
                        var result = ResultTypes.Success;
                        ExternalCustomerSystem externalCustomerSystem = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachinePortConfiguration(m.Data);

                            if (result == ResultTypes.Success)
                                externalCustomerSystem = externalCustomerSystems.Create(m.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }

                        var response = new ResponseMessage<ExternalCustomerSystem>
                        {
                            MessageType = ExternalCustomerSystemMessages.ExternalCustomerSystemCreated,
                            Result = result,
                            Data = externalCustomerSystem,
                            ReplyTo = m.ReplyTo
                        };

                        uiCommunicationService.SendMessageToUI(response);
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ExternalCustomerSystem>(ExternalCustomerSystemMessages.UpdateExternalCustomerSystem);
            subscriber.GetEvent<IMessage<ExternalCustomerSystem>>()
                .Where(m => m.MessageType == ExternalCustomerSystemMessages.UpdateExternalCustomerSystem)
                .DurableSubscribe(
                    m =>
                    {
                        var result = ResultTypes.Success;
                        ExternalCustomerSystem externalCustomerSystem = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachinePortConfiguration(m.Data);

                            if (result == ResultTypes.Success)
                                externalCustomerSystem = externalCustomerSystems.Update(m.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }

                        var response = new ResponseMessage<ExternalCustomerSystem>
                        {
                            MessageType = ExternalCustomerSystemMessages.ExternalCustomerSystemUpdated,
                            Result = result,
                            Data = externalCustomerSystem,
                            ReplyTo = m.ReplyTo
                        };

                        uiCommunicationService.SendMessageToUI(response);
                    }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ExternalCustomerSystem>(ExternalCustomerSystemMessages.DeleteExternalCustomerSystem);
            subscriber.GetEvent<IMessage<ExternalCustomerSystem>>()
                .Where(m => m.MessageType == ExternalCustomerSystemMessages.DeleteExternalCustomerSystem)
                .DurableSubscribe(
                    m =>
                    {
                        var result = ResultTypes.Success;
                        try
                        {
                            externalCustomerSystems.Delete(m.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is RelationshipExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<ExternalCustomerSystem>
                        {
                            MessageType = MachineMessages.MachineDeleted,
                            Result = result,
                            ReplyTo = m.ReplyTo,
                            Data = m.Data
                        });
                        publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
                    }, logger);

            serviceLocator.RegisterAsService(this);
            machineService = serviceLocator.Locate<IAggregateMachineService>();
            machineService.RegisterMachineService(this);
        }

        public void SendMessage<T>(ExternalCustomerSystem customerSystem, IMessage<T> message)
        {
            var inArgs = new Dictionary<string, object>()
            {
                {"Publisher", publisher},
                {"ServiceLocator", serviceLocator},
                {"ExternalCustomerSystem", customerSystem},
                {"Message", message }
            };

            logger.Log(LogLevel.Debug, "Starting external customer system workflow for {0}", customerSystem.Alias);
            WorkflowHelper.InvokeWorkFlow(logger, customerSystem.WorkflowPath, Assembly.GetExecutingAssembly(), inArgs, userNotificationService);
        }

        public void SendMessage<T>(Guid customerSystemId, IMessage<T> message)
        {
            var externalSystem = externalCustomerSystems.Find(customerSystemId);
            if (externalSystem == null)
            {
                logger.Log(LogLevel.Error, "Failed to lookup external customer system by ID {0}", customerSystemId);
            }
            else
                SendMessage(externalSystem, message);
        }

        public ExternalCustomerSystem Find(string name)
        {
            return externalCustomerSystems.Find(name);
        }

        public ExternalCustomerSystem Find(Guid id)
        {
            return externalCustomerSystems.Find(id);
        }

        public void Dispose()
        {
        }

        public string Name { get { return "ExternalCustomerSystemService";  } }
        public IEnumerable<ExternalCustomerSystem> Machines { get { return externalCustomerSystems.All().ToList(); } }

        public void Produce(Guid machine, IProducible producible)
        {
            throw new NotImplementedException();
        }

        public IObservable<ExternalCustomerSystem> MachineErrorOccuredObservable { get; private set; }

        public bool CanProduce(Guid machineId, IProducible producible)
        {
            throw new NotImplementedException();
        }
    }
}
