﻿using PackNet.Common.Interfaces.Services;

namespace PackNet.Services
{
    public interface IPackNetServerSettingsService : IService
    {
        Common.Interfaces.DTO.Settings.PackNetServerSettings UpdateSettings(Common.Interfaces.DTO.Settings.PackNetServerSettings packNetServerSettings);
        Common.Interfaces.DTO.Settings.PackNetServerSettings GetSettings();

        Common.Interfaces.DTO.SelectionAlgorithm.SearchConfiguration GetSearchConfiguration();
    }
}