﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using PackNet.Business.Classifications;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Services.L10N;

namespace PackNet.Services
{
    public class ClassificationsService : IClassificationService
    {
        private readonly IClassifications classifications;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ILogger logger;
        private IDisposable createObserver;

        public string Name { get { return "Classifications Service"; } }

        public IEnumerable<Classification> Classifications
        {
            get { return classifications.GetAll(); }
        }

        public ClassificationsService(IClassifications classifications,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            ILogger logger)
        {
            this.classifications = classifications;
            this.uiCommunicationService = uiCommunicationService;
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.logger = logger;

            SetupSubscribers();
            serviceLocator.RegisterAsService(this);
        }

        public IEnumerable<Classification> GetClassificationsByStatus(ClassificationStatuses status)
        {
            if(Classifications != null)
                return Classifications.Where(c => c.Status == status).ToList();
            return new List<Classification>();
        }

        private void SetupSubscribers()
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Classification>(ClassificationMessages.CreateClassification);
            createObserver = subscriber.GetEvent<IMessage<Classification>>()
                .Where(m => m.MessageType == ClassificationMessages.CreateClassification)
                .DurableSubscribe(message =>
                {
                    var result = ResultTypes.Success;
                    Exception exception = null;

                    try
                    {
                        CreateClassification(message.Data);
                    }
                    catch (AlreadyPersistedException e)
                    {
                        exception = e;
                        result = ResultTypes.Exists;
                    }
                    catch (Exception e)
                    {
                        exception = e;
                        result = ResultTypes.Fail;
                    }

                    var response = new ResponseMessage<string>
                    {
                        MessageType = ClassificationMessages.ClassificationCreated,
                        Result = result,
                        Message = result == ResultTypes.Success ? Strings.ClassificationsService_SetupSubscribers_Successfully_created_classification : exception.Message,
                        ReplyTo = message.ReplyTo
                    };

                    publisher.Publish(response);
                    uiCommunicationService.SendMessageToUI(response);
                    publisher.Publish(new Message { MessageType = ClassificationMessages.GetClassifications, ReplyTo = message.ReplyTo });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Classification>(ClassificationMessages.UpdateClassification);
            createObserver = subscriber.GetEvent<IMessage<Classification>>()
                .Where(m => m.MessageType == ClassificationMessages.UpdateClassification)
                .DurableSubscribe(message =>
                {
                    var result = ResultTypes.Success;

                    Classification updatedClassification = null;
                    Exception exception = null;

                    try
                    {
                        updatedClassification = UpdateClassification(message.Data);
                    }
                    catch (AlreadyPersistedException e)
                    {
                        exception = e;
                        result = ResultTypes.Exists;
                    }
                    catch (Exception e)
                    {
                        exception = e;
                        result = ResultTypes.Fail;
                    }

                    var response = new ResponseMessage<Classification>
                    {
                        MessageType = ClassificationMessages.ClassificationUpdated,
                        Result = result,
                        Data = updatedClassification,
                        ReplyTo = message.ReplyTo,
                        Message = exception != null ? exception.Message : string.Empty
                    };

                    publisher.Publish(response);
                    uiCommunicationService.SendMessageToUI(response);
                    publisher.Publish(new Message { MessageType = ClassificationMessages.GetClassifications, UserName = message.UserName });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Classification>(ClassificationMessages.DeleteClassification);
            createObserver = subscriber.GetEvent<IMessage<Classification>>()
                .Where(m => m.MessageType == ClassificationMessages.DeleteClassification)
                .DurableSubscribe(message =>
                {
                    var result = ResultTypes.Success;
                    try
                    {
                        DeleteClassification(message.Data);
                    }
                    catch (Exception)
                    {
                        result = ResultTypes.Fail;
                    }

                    var response = new ResponseMessage<Classification>()
                    {
                        MessageType = ClassificationMessages.ClassificationDeleted,
                        Data = message.Data,
                        Result = result,
                        ReplyTo = message.ReplyTo
                    };

                    publisher.Publish(response);
                    uiCommunicationService.SendMessageToUI(response);
                    publisher.Publish(new Message() { MessageType = ClassificationMessages.GetClassifications, UserName = message.UserName });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Classification>(ClassificationMessages.GetClassifications);
            createObserver = subscriber.GetEvent<IMessage<Classification>>()
                .Where(m => m.MessageType == ClassificationMessages.GetClassifications)
                .DurableSubscribe(message =>
                {
                    var classifications1 = new ConcurrentList<Classification>();

                    try
                    {
                        classifications1 = GetClassifications();
                    }
                    catch (Exception e)
                    {
                        logger.LogException(LogLevel.Error, "Failed to get Classifications", e);
                    }

                    var response = new Message<IEnumerable<Classification>>
                    {
                        MessageType = ClassificationMessages.Classifications,
                        Data = classifications1,
                        ReplyTo = message.ReplyTo,
                    };

                    publisher.Publish(response);
                    uiCommunicationService.SendMessageToUI(response, false, true);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Classification>(ClassificationMessages.UpdateClassificationStatus);
            createObserver = subscriber.GetEvent<IMessage<Classification>>()
                .Where(m => m.MessageType == ClassificationMessages.UpdateClassificationStatus)
                .DurableSubscribe(message =>
                {
                    var classificationToUpdate = classifications.Get(message.Data.Id);
                    classificationToUpdate.Status = message.Data.Status;
                    classificationToUpdate.IsPriorityLabel = message.Data.IsPriorityLabel;
                    classifications.Update(classificationToUpdate);

                    publisher.Publish(new Message()
                    {
                        MessageType = SelectionAlgorithmMessages.SelectionEnvironmentChanged
                    });
                }, logger);

            subscriber.GetEvent<Message<IEnumerable<BoxFirstProducible>>>()
                .Where(msg => msg.MessageType == StagingMessages.ProduciblesStaged)
                .DurableSubscribe(UpdateProducibleCount, logger);
        }

        public void DeleteClassification(Classification classification)
        {
            classifications.Delete(classification);
        }

        public ConcurrentList<Classification> GetClassifications()
        {
            return new ConcurrentList<Classification>(classifications.GetAll().ToList());
        }

        public Classification UpdateClassification(Classification classification)
        {
            Classification result;
            
            var existingClassification = Classifications.FirstOrDefault(c => c.Id != classification.Id && (c.Alias == classification.Alias || c.Number == classification.Number));

            if (existingClassification == null)
                result = classifications.Update(classification);
            else
                throw new AlreadyPersistedException(existingClassification.Alias == classification.Alias ? "Alias" : "Number");

            return result;
        }

        public void CreateClassification(Classification classification)
        {
            var existingClassification = Classifications.FirstOrDefault(c => c.Alias == classification.Alias || c.Number == classification.Number);
            
            if(existingClassification == null)
                classifications.Create(classification);
            else
                throw new AlreadyPersistedException(existingClassification.Alias == classification.Alias ? "Alias" : "Number");
        }

        public void UpdateProducibleCount(Message<IEnumerable<BoxFirstProducible>> msg)
        {
            if (msg.Data == null)
            {
                return;
            }

            if (msg.Data.Any() == false)
            {
                ResetClassificationNumberOfRequestsCounters(msg);
                return;
            }

            var groups = msg.Data.GroupBy(p =>
            {
                var restriction = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();
                if (restriction != null)
                    return restriction.Id;
                return Guid.Empty;
            }).Select(p => new Tuple<Guid, int>(p.Key, p.Count()));


            foreach (var cpg in Classifications)
            {
                var grp = groups.FirstOrDefault(g => g.Item1 == cpg.Id);
                cpg.NumberOfRequests = grp != null ? grp.Item2 : 0;
                classifications.Update(cpg);
            }

            var classifications1 = new List<Classification>();

            try
            {
                classifications1 = classifications.GetAll().ToList();
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to get Classifications", e);
            }

            var response = new Message<IEnumerable<Classification>>
            {
                MessageType = ClassificationMessages.Classifications,
                Data = classifications1,
                ReplyTo = ((IMessage)msg).ReplyTo,
            };

            publisher.Publish(response);
            uiCommunicationService.SendMessageToUI(response, false, true);
        }

        private void ResetClassificationNumberOfRequestsCounters(IMessage msg)
        {
            foreach (var cpg in Classifications)
            {
                cpg.NumberOfRequests = 0;
                classifications.Update(cpg);
            }

            var classifications1 = new List<Classification>();

            try
            {
                classifications1 = classifications.GetAll().ToList();
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to get Classifications", e);
            }

            var response = new Message<IEnumerable<Classification>>
            {
                MessageType = ClassificationMessages.Classifications,
                Data = classifications1,
                ReplyTo = msg.ReplyTo,
            };

            publisher.Publish(response);
            uiCommunicationService.SendMessageToUI(response, false, true);
        }

        public void Dispose()
        {
            createObserver.Dispose();
        }
    }
}
