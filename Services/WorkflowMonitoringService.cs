﻿using System;
using System.Activities.Tracking;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;

using PackNet.Common;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Logging.NLogBackend;
using PackNet.Common.WorkflowTracking;

namespace PackNet.Services
{
    public class WorkflowMonitoringService : IWorkflowMonitoringService
    {
        private FileSystemWatcher workflowMonitor;
        private IDisposable changedObservable;
        private IDisposable createdObservable;
        private IDisposable renamedObservable;
        private IDisposable deletedObservable;

        public WorkflowMonitoringService(IServiceLocator sl)
        {
            //todo: consider using an env variable for our install path?
            workflowMonitor = new FileSystemWatcher(Directory.GetCurrentDirectory(), "*.xaml");

            changedObservable = Observable.FromEventPattern<FileSystemEventArgs>(workflowMonitor, "Changed").DurableSubscribe(x => WorkflowHelper.PurgeActivityCache());
            createdObservable = Observable.FromEventPattern<FileSystemEventArgs>(workflowMonitor, "Created").DurableSubscribe(x => WorkflowHelper.PurgeActivityCache());
            renamedObservable = Observable.FromEventPattern<FileSystemEventArgs>(workflowMonitor, "Renamed").DurableSubscribe(x => WorkflowHelper.PurgeActivityCache());
            deletedObservable = Observable.FromEventPattern<FileSystemEventArgs>(workflowMonitor, "Deleted").DurableSubscribe(x => WorkflowHelper.PurgeActivityCache());

            workflowMonitor.IncludeSubdirectories = true;
            workflowMonitor.EnableRaisingEvents = true;
            sl.RegisterAsService(this);
        }

        public void Dispose()
        {
            changedObservable.Dispose();
            createdObservable.Dispose();
            renamedObservable.Dispose();
            deletedObservable.Dispose();
            workflowMonitor.Dispose();
        }

        public string Name { get { return "Workflow monitoring service"; } }

       
    }
}