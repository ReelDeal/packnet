﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.IO;
using System.Reactive;
using System.Reactive.Linq;

using Newtonsoft.Json;

using PackNet.Business.Workflows;
using PackNet.Common;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Logging.NLogBackend;
using PackNet.Common.WorkflowTracking;
using PackNet.Data.Workflows;

namespace PackNet.Services
{
    public class WorkflowTrackingService : IWorkflowTrackingService
    {
        private readonly ITrackingParticipantBL bl;
        private readonly ILogger logger;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IPackNetServerSettingsService settings;

        public WorkflowTrackingService( ITrackingParticipantBL bl,ILogger logger,IServiceLocator sl,IEventAggregatorSubscriber subscriber,IPackNetServerSettingsService settings )
        {
            this.bl = bl;
            this.logger = logger;
            this.subscriber = subscriber;
            this.settings = settings;
            
            subscriber.GetEvent<IMessage>().Where(msg=>msg.MessageType == PackNetServerSettingsMessages.WorkflowTrackingLevelChanged).DurableSubscribe(
                msg=>bl.SetTrackingLevel(settings.GetSettings().WorkflowTrackingLevel),logger);
            
            bl.SetTrackingLevel(settings.GetSettings().WorkflowTrackingLevel);
            sl.RegisterAsService(this);
        }

        public void Dispose()
        {

        }

        public string Name { get { return "Workflow tracking service"; } }

        public WorkflowLifetime Factory(WorkflowTypes type, string workflow, Dictionary<string, object> additionalInfo = null)
        {
            return bl.Factory(type, workflow, additionalInfo);
        }
    }

}