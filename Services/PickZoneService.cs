﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using PackNet.Business.PickZone;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services
{
    public class PickZoneService : IPickZoneService
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IServiceLocator serviceLocator;
        private readonly IPickZones pickZones;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly ILogger logger;

        private IDisposable createObserver;
        private IDisposable editObserver;
        private IDisposable deleteObserver;
        private IDisposable getPickzonesObserver;
        private IDisposable updatePickZoneStatusObserver;

        public IEnumerable<PickZone> PickZones
        {
            get { return pickZones.GetPickZones(); }
        }

        public PickZoneService(IPickZones pickZones, 
            IUICommunicationService uiCommunicationService, 
            IEventAggregatorPublisher publisher, 
            IEventAggregatorSubscriber subscriber, 
            IServiceLocator serviceLocator, 
            ILogger logger)
        {
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.serviceLocator = serviceLocator;
            this.pickZones = pickZones;
            this.uiCommunicationService = uiCommunicationService;
            this.logger = logger;

            SetupListeners();
            serviceLocator.RegisterAsService(this);
        }

        public string Name
        {
            get { return "PickZoneService"; }
        }

        public IEnumerable<PickZone> GetPickZonesByStatus(PickZoneStatuses status)
        {
            return pickZones.GetPickZonesByStatus(status);
        }

        private void SetupListeners()
        {
            subscriber.GetEvent<Message<PickZone>>()
                .Where(m => m.MessageType == PickZoneMessages.StatusChanged)
                .DurableSubscribe(m =>
                {
                    var pickZonePresentationModels = pickZones.GetPickZones()
                        .Select(p =>
                        {
                            p.NumberOfRequests = 10; //TODO: track cartons by pickzonecartonRequestManager.NumberOfRequestsByPickzone(p.PickZoneId)
                            return p;
                        });

                    uiCommunicationService.SendMessageToUI(new Message<List<PickZone>>
                    {
                        MessageType = PickZoneMessages.PickZones,
                        ReplyTo = m.ReplyTo,
                        Data = pickZonePresentationModels.ToList()
                    }, false, true);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<PickZone>(PickZoneMessages.CreatePickZone);
            createObserver = subscriber.GetEvent<Message<PickZone>>()
                .Where(x => x.MessageType == PickZoneMessages.CreatePickZone)
                .DurableSubscribe(msg =>
                {
                    var result = ResultTypes.Success;
                    Exception exception = null;
                    PickZone createdPickZone = null;
                    try
                    {
                        var pickZoneToCreate = msg.Data;
                        createdPickZone = CreatePickZone(pickZoneToCreate);
                    }
                    catch (AlreadyPersistedException e)
                    {
                        result = ResultTypes.Exists;
                        exception = e;
                    }
                    catch (Exception e)
                    {
                        result = ResultTypes.Fail;
                        exception = e;
                    }

                    var message = new ResponseMessage<PickZone>()
                    {
                        Data = createdPickZone,
                        Result = result,
                        ReplyTo = msg.ReplyTo,
                        MessageType = PickZoneMessages.PickZoneCreated,
                    };
                    publisher.Publish(message);
                    uiCommunicationService.SendMessageToUI(message);

                    LogResult(result,
                        "Pickzone with Id: {0} and Alias: {1} was Created by User: {2}",
                        "Unable to create Pickzone with Id: {0} and Alias: {1}, message: {3}",
                        string.Empty,
                        "Pickzone with Id: {0} and Alias: {1} already exists in the database, message: {3}",
                        msg, exception);

                    if (result == ResultTypes.Success)
                    {
                        publisher.Publish(new Message { MessageType = PickZoneMessages.GetPickZones });
                    }
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<PickZone>(PickZoneMessages.DeletePickZone);
            deleteObserver = subscriber.GetEvent<Message<PickZone>>()
                .Where(msg => msg.MessageType == PickZoneMessages.DeletePickZone)
                .DurableSubscribe(msg =>
                {
                    Exception exception = null;
                    var result = ResultTypes.Success;
                    var message = string.Empty;
                    try
                    {
                        var pickZoneToDelete = msg.Data;
                        DeletePickZone(pickZoneToDelete);
                    }
                    catch (Exception e)
                    {
                        exception = e;
                        result = ResultTypes.Fail;
                        if (e is InUseException)
                            message = "PickZoneInProduction";
                    }
                    uiCommunicationService.SendMessageToUI(new ResponseMessage<PickZone>
                    {
                        MessageType = PickZoneMessages.PickZoneDeleted,
                        Result = result,
                        ReplyTo = msg.ReplyTo,
                        Data = msg.Data,
                        Message = message
                    });
                    publisher.Publish(new Message { MessageType = PickZoneMessages.GetPickZones });

                    LogResult(result,
                        "Pickzone with Id: {0} and Alias: {1} was Deleted by User: {2}",
                        "Unable to delete Pickzone with Id: {0} and Alias: {1}, message: {3}",
                        string.Empty,
                        string.Empty,
                        msg, exception);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<PickZone>(PickZoneMessages.UpdatePickZone);
            editObserver = subscriber.GetEvent<Message<PickZone>>()
                .Where(msg => msg.MessageType == PickZoneMessages.UpdatePickZone)
                .DurableSubscribe(msg =>
                {
                    ResultTypes result;
                    Exception exception = null;
                    var responseMessage = string.Empty;
                    PickZone updatedPickZone = null;
                    try
                    {
                        var pickZoneToUpdate = msg.Data;
                        updatedPickZone = UpdatePickZone(pickZoneToUpdate);

                        publisher.Publish(new Message<PickZone> { MessageType = PickZoneMessages.StatusChanged, Data = msg.Data });

                        result = ResultTypes.Success;
                    }
                    catch (UnableToLocateByPacksizeIdException e)
                    {
                        result = ResultTypes.Fail;
                        exception = e;
                    }
                    catch (ItemExistsException e)
                    {
                        result = ResultTypes.Exists;
                        exception = e;
                    }
                    catch (Exception e)
                    {
                        result = ResultTypes.Fail;
                        exception = e;
                        if (e is InUseException)
                            responseMessage = "PickZoneInProduction";
                    }
                    

                    var message = new ResponseMessage<PickZone>()
                    {
                        Data = msg.Data,
                        Result = result,
                        ReplyTo = msg.ReplyTo,
                        MessageType = PickZoneMessages.PickZoneUpdated,
                        Message = responseMessage
                    };
                    publisher.Publish(message);
                    uiCommunicationService.SendMessageToUI(message);

                    LogResult(result,
                        "Pickzone with Id: {0} and Alias: {1} was Edited by User: {2}",
                        "Unable to edit Pickzone with Id: {0} and Alias: {1}, message: {3}",
                        string.Empty,
                        string.Empty,
                        msg, exception);

                    if (result == ResultTypes.Success)
                    {
                        publisher.Publish(new Message { MessageType = PickZoneMessages.GetPickZones });
                    }
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(PickZoneMessages.GetPickZones);
            getPickzonesObserver = subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == PickZoneMessages.GetPickZones)
                .DurableSubscribe(msg =>
                {
                    ResultTypes result;
                    var pickZones1 = new ConcurrentList<PickZone>();
                    Exception exception = null;

                    try
                    {
                        pickZones1 = GetPickZones();
                        result = ResultTypes.Success;
                    }
                    catch (Exception e)
                    {
                        result = ResultTypes.Fail;
                        exception = e;
                    }

                    var message = new Message<IEnumerable<PickZone>>()
                    {
                        Data = pickZones1,
                        ReplyTo = ((IMessage)msg).ReplyTo,
                        MessageType = PickZoneMessages.PickZones,
                    };
                    publisher.Publish(message);
                    uiCommunicationService.SendMessageToUI(message, false, true);

                    LogResult(result, string.Empty,
                        "Unable to retreive pickzones with message: {3}",
                        string.Empty,
                        string.Empty,
                        new Message<PickZone>(), exception);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<PickZone>(PickZoneMessages.UpdatePickZoneStatus);
            updatePickZoneStatusObserver = subscriber.GetEvent<Message<PickZone>>()
                .Where(msg => msg.MessageType == PickZoneMessages.UpdatePickZoneStatus)
                .DurableSubscribe(msg =>
                {
                    var pickZone = msg.Data;
                    SetPickZoneStatus(pickZone);
                    publisher.Publish(new Message()
                    {
                        MessageType = SelectionAlgorithmMessages.SelectionEnvironmentChanged
                    });
                }, logger);
           
            subscriber.GetEvent<Message<IEnumerable<BoxFirstProducible>>>()
                .Where(msg => msg.MessageType == StagingMessages.ProduciblesStaged)
                .DurableSubscribe(UpdateProducibleCount, logger);
        }

        public void SetPickZoneStatus(PickZone pickZone)
        {
            pickZones.SetPickZoneStatus(pickZone.Id, pickZone.Status);
        }

        public ConcurrentList<PickZone> GetPickZones()
        {
            return new ConcurrentList<PickZone>(pickZones.GetPickZones().ToList());
        }

        public PickZone UpdatePickZone(PickZone pickZoneToUpdate)
        {
            return pickZones.Update(pickZoneToUpdate);
        }

        public void DeletePickZone(PickZone pickZoneToDelete)
        {
            pickZones.Delete(pickZoneToDelete);
        }

        public PickZone CreatePickZone(PickZone pickZoneToCreate)
        {
            return pickZones.Create(pickZoneToCreate);
        }

        private void UpdateProducibleCount(Message<IEnumerable<BoxFirstProducible>> msg)
        {
            if (msg.Data == null)
            {
                return;
            }

            if (msg.Data.Any() == false)
            {
                ResetPickZoneNumberOfRequestsCounters(msg);
                return;
            }

            var groups = msg.Data.GroupBy(p =>
            {
                var restriction = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<PickZone>();
                return restriction != null ? restriction.Id : Guid.Empty;
            }).Select(p => new Tuple<Guid, int>(p.Key, p.Count()));

            foreach (var pickZone in PickZones)
            {
                var grp = groups.FirstOrDefault(g => g.Item1 == pickZone.Id);
                pickZone.NumberOfRequests = grp != null ? grp.Item2 : 0;
                pickZones.Update(pickZone);
            }

            ResultTypes result;
            var pickZones1 = new List<PickZone>();
            Exception exception = null;

            try
            {
                pickZones1 = this.pickZones.GetPickZones().ToList();
                result = ResultTypes.Success;
            }
            catch (Exception e)
            {
                result = ResultTypes.Fail;
                exception = e;
            }

            var message = new Message<IEnumerable<PickZone>>()
            {
                Data = pickZones1,
                ReplyTo = ((IMessage)msg).ReplyTo,
                MessageType = PickZoneMessages.PickZones,
            };
            publisher.Publish(message);
            uiCommunicationService.SendMessageToUI(message, false, true);

            LogResult(result, string.Empty,
                "Unable to retreive pickzones with message: {3}",
                string.Empty,
                string.Empty,
                new Message<PickZone>(), exception);
        }

        private void ResetPickZoneNumberOfRequestsCounters(IMessage msg)
        {
            foreach (var cpg in PickZones)
            {
                cpg.NumberOfRequests = 0;
                pickZones.Update(cpg);
            }

            ResultTypes result;
            var pickZones1 = new List<PickZone>();
            Exception exception = null;

            try
            {
                pickZones1 = this.pickZones.GetPickZones().ToList();
                result = ResultTypes.Success;
            }
            catch (Exception e)
            {
                result = ResultTypes.Fail;
                exception = e;
            }

            var message = new Message<IEnumerable<PickZone>>()
            {
                Data = pickZones1,
                ReplyTo = msg.ReplyTo,
                MessageType = PickZoneMessages.PickZones,
            };
            publisher.Publish(message);
            uiCommunicationService.SendMessageToUI(message, false, true);

            LogResult(result, string.Empty,
                "Unable to retreive pickzones with message: {3}",
                string.Empty,
                string.Empty,
                new Message<PickZone>(), exception);
        }

        private
            void LogResult(Enumeration result, string successMessage, string failMessage, string partialSuccessMessage,
                string existsMessage, IMessage<IEnumerable<PickZone>> msg, Exception exception = null)
        {
            foreach (var pickZone in msg.Data)
            {
                LogResult(result, successMessage, failMessage, partialSuccessMessage, existsMessage, new Message<PickZone>()
                {
                    Created = msg.Created,
                    Data = pickZone,
                    MachineGroupName = msg.MachineGroupName,
                    MessageType = msg.MessageType,
                    ReplyTo = msg.ReplyTo,
                    UserName = msg.UserName
                }, exception);
            }
        }

        private
             void LogResult(Enumeration result, string successMessage, string failMessage, string partialSuccessMessage, string existsMessage, IMessage<PickZone> msg, Exception exception = null)
        {
            switch (result.DisplayName)
            {
                case "Success":
                    if (successMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Info,
                        string.Format(successMessage, msg.Data.Alias,
                            msg.Data.Alias,
                            msg.UserName));
                    break;
                case "Fail":
                    if (failMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Error,
                        string.Format(failMessage, msg.Data.Alias,
                            msg.Data.Alias, msg.UserName, exception.Message));
                    break;
                case "PartialSuccess":
                    if (partialSuccessMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Info,
                        string.Format(partialSuccessMessage, msg.Data.Alias,
                            msg.Data.Alias,
                            msg.UserName, exception.Message));
                    break;
                case "Exists":
                    if (existsMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Error,
                        string.Format(existsMessage, msg.Data.Alias,
                            msg.Data.Alias,
                            msg.UserName, exception.Message));
                    break;
                default:
                    break;
            }
        }

        public void Dispose()
        {
            if (createObserver != null)
                createObserver.Dispose();
            if (editObserver != null)
                editObserver.Dispose();
            if (deleteObserver != null)
                deleteObserver.Dispose();
            if (getPickzonesObserver != null)
                getPickzonesObserver.Dispose();
            if(updatePickZoneStatusObserver != null)
                updatePickZoneStatusObserver.Dispose();
        }

    }
}
