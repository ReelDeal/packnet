﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Plugins;

namespace PackNet.Services.MachineServices
{
    public class MachineServicesImporter : IMachineServicesImporter
    {
        [ImportMany]
        public IEnumerable<IMachineService<IMachine>> MachineServices { get; private set; }

        public MachineServicesImporter(string pluginDirectory, IServiceLocator locator,ILogger logger)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(assembly));
            var batch = new CompositionBatch();
            batch.AddExportedValue(logger);
            batch.AddExportedValue(locator);
            batch.AddPart(this);
            MefComposer.Compose(Path.Combine(Directory.GetCurrentDirectory(), pluginDirectory), batch, catalog);
        }

        public void Dispose()
        {
            MachineServices.ForEach(m => m.Dispose());
        }
    }
}
