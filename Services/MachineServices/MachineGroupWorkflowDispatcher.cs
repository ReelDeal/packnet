﻿using System;
using System.Activities.Expressions;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.IO;

using PackNet.Business.Machines;
using PackNet.Common;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services.MachineServices
{
    public class 
        MachineGroupWorkflowDispatcher : IMachineGroupWorkflowDispatcher
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IUserNotificationService userNotificationService;
        private readonly ILogger logger;
        private IWorkflowTrackingService workflowTrackingService;

        public MachineGroupWorkflowDispatcher(
            IServiceLocator serviceLocator, 
            IEventAggregatorPublisher publisher,
            ILogger logger)
        {
            this.serviceLocator = serviceLocator;
            this.publisher = publisher;
            this.logger = logger;

            this.userNotificationService = serviceLocator.Locate<IUserNotificationService>();

            try
            {
                this.workflowTrackingService = serviceLocator.Locate<IWorkflowTrackingService>();
            }
            catch (Exception)
            {
                IDisposable subscription = null;
                subscription = serviceLocator.ServiceAddedObservable.Subscribe(s =>
                {
                    if (s is IWorkflowTrackingService)
                    {
                        this.workflowTrackingService = s as IWorkflowTrackingService;
                        subscription.Dispose();
                    }
                });
            }
        }

        /// <summary>
        /// Dispatches the machine group workflow and waits for the completion
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <param name="producible"></param>
        public void DispatchCreateProducibleWorkflow(MachineGroup machineGroup, IProducible producible)
        {
            var workflowPath = machineGroup.WorkflowPath;
            var assembly = this.GetType().Assembly;
            var inArgs = new Dictionary<string, object> 
            { 
                { "MachineGroup", machineGroup},
                { "Producible", producible},
                { "ServiceLocator", serviceLocator}
            };
            WorkflowLifetime wflt= null;
            if (workflowTrackingService != null)
            {
                var additionalInfo = new Dictionary<string, object>
                {
                    {"MachineGroup",machineGroup},
                    {"Producible",producible}
                };
                wflt = this.workflowTrackingService.Factory(WorkflowTypes.MachineGroupWorkflow, workflowPath, additionalInfo);
            }
            WorkflowHelper.InvokeWorkFlowAndWaitForResult(workflowPath, assembly, inArgs, userNotificationService, logger, wflt);
        }
    }
}
