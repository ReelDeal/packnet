﻿using Newtonsoft.Json.Linq;
using PackNet.Business.Machines;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Communication.Communicators;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using CommonMachines = PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.Services.MachineServices
{
    using PackNet.Common.Interfaces.Exceptions;

    public class MachineGroupService : IMachineGroupService
    {
        private readonly IMachineGroups machineGroups;
        private readonly IAggregateMachineService machineService;
        private readonly IProductionGroupService productionGroupService;
        private readonly ISelectionAlgorithmDispatchService selectionAlgorithmDispatchService;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IUserNotificationService userNotificationService;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private readonly Dictionary<Guid, Tuple<IDisposable, IDisposable, IDisposable>> observers = new Dictionary<Guid, Tuple<IDisposable, IDisposable, IDisposable>>();
        /// <summary>
        /// We have to keep a seperate list of producibles sent to machine here because we can not rely on the queue in the MGBL because items are removed
        /// from that queue well before they have been fully completed or removed.  The reason is that way workflows start and end.  We are not blocking the end of 
        /// a workflow but instead sending the job and dropping out.
        /// </summary>
        private readonly ConcurrentDictionary<Guid/* maching group id*/, ConcurrentList<IProducible>/*producibles in MG.*/> machineGroupProduciblesInQueue = new ConcurrentDictionary<Guid, ConcurrentList<IProducible>>();
        private const string machineGroupWorkflowPath = "%PackNetWorkflows%\\MachineGroups";
        private object addToQueueLock = new object();

        IDisposable plcWarningDisposable;
        private IViewModelService viewModelService;

        /// <summary>
        /// Contains the instances of machine groups that are setup to monitor the machines in the group
        /// </summary>
        public string Name { get { return "MachineGroupService"; } }
        public IEnumerable<CommonMachines.MachineGroup> MachineGroups { get { return machineGroups.GetMachineGroups(); } }

        public MachineGroupService(IMachineGroups machineGroups,
            IAggregateMachineService machineService,
            IProductionGroupService productionGroupService,
            ISelectionAlgorithmDispatchService selectionAlgorithmDispatchService,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            ILogger logger)
        {
            this.machineGroups = machineGroups;
            this.machineService = machineService;
            this.productionGroupService = productionGroupService;
            this.selectionAlgorithmDispatchService = selectionAlgorithmDispatchService;
            this.uiCommunicationService = uiCommunicationService;
            this.publisher = publisher;
            this.logger = logger;
            this.userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            try
            {
                viewModelService = serviceLocator.Locate<IViewModelService>();
            }
            catch (Exception)
            {
                serviceLocator.ServiceAddedObservable.Subscribe((s) =>
                {
                    if (s is IViewModelService)
                        viewModelService = s as IViewModelService;
                });

            }
            WireupToMessages(subscriber);

            machineGroups.GetMachineGroups().ForEach(SetupMachineGroupUIEvents);
            serviceLocator.RegisterAsService(this);
        }

        private void WireupToMessages(IEventAggregatorSubscriber subscriber)
        {
            //todo: should we be more specific here to only try to fill work for the mgs that are part of the PG?  Or should we call get work for pg's that use the selection algorithm?
            subscriber.GetEvent<Message>()
                .Where(m => m.MessageType == SelectionAlgorithmMessages.SelectionEnvironmentChanged)
                .DurableSubscribe(m => MachineGroups
                    .Where(mg => mg != null && mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline)
                    //Fill idle machines first
                    .OrderBy(mg => mg.CurrentProductionStatus == MachineProductionStatuses.ProductionIdle || mg.CurrentProductionStatus == MachineProductionStatuses.ProductionCompleted ? 0 : 1)
                    //Fill machines with less items in queue second
                    .ThenBy(mg => machineGroups.GetQueueCount(mg))
                    .ForEach(SelectItemAndAddToMachineGroupQueue), logger);

            subscriber.GetEvent<Message>()
                .Where(m => m.MessageType == MachineGroupMessages.GetWorkForMachineGroup)
                .DurableSubscribe(msg =>
                {
                    //Dispatch to single machine group
                    var machineGroup = machineGroups.FindByMachineGroupId(msg.MachineGroupId.GetValueOrDefault());
                    if (machineGroup != null) // a chance that the mg has been removed.
                    {
                        SelectItemAndAddToMachineGroupQueue(machineGroup);
                    }
                }, logger);

            subscriber.GetEvent<Message<Guid>>()
                .Where(m => m.MessageType == ProductionGroupMessages.GetWorkForProductionGroup)
                .DurableSubscribe(msg =>
                {
                    var productionGroup = productionGroupService.ProductionGroups.Single(pg => pg.Id == msg.Data);
                    var configuredMachineGroups = productionGroup.ConfiguredMachineGroups
                        .Select(machineGroups.FindByMachineGroupId)
                        .Where(mg => mg != null && mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline)
                        //Fill idle machines first
                        .OrderBy(mg => mg.CurrentProductionStatus == MachineProductionStatuses.ProductionIdle || mg.CurrentProductionStatus == MachineProductionStatuses.ProductionCompleted ? 0 : 1)
                        //Fill machines with less items in queue second
                        .ThenBy(mg => machineGroups.GetQueueCount(mg));
                    configuredMachineGroups.ForEach(SelectItemAndAddToMachineGroupQueue);
                }, logger);

            //This event handler is key to allowing the staging workflow to communicate with the select workflow 
            subscriber.GetEvent<Message<SelectionAlgorithmTypes>>()
                .Where(m => m.MessageType == SelectionAlgorithmMessages.StagingComplete)
                .DurableSubscribe(msg =>
                {
                    var productionGroups = productionGroupService.ProductionGroups.Where(pg => pg.SelectionAlgorithm == msg.Data);
                    var configuredMachineGroups = productionGroups
                        .SelectMany(pg => pg.ConfiguredMachineGroups)
                        .Select(machineGroups.FindByMachineGroupId)
                        .Where(mg => mg != null && mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline
                         && mg.ProductionMode == MachineGroupProductionModes.AutoProductionMode)
                        //Fill idle machines first
                        .OrderBy(mg => mg.CurrentProductionStatus == MachineProductionStatuses.ProductionIdle || mg.CurrentProductionStatus == MachineProductionStatuses.ProductionCompleted ? 0 : 1)
                        //Fill machines with less items in queue second
                        .ThenBy(mg => machineGroups.GetQueueCount(mg));
                    configuredMachineGroups.ForEach(SelectItemAndAddToMachineGroupQueue);
                }, logger);

            subscriber.GetEvent<Message<List<Guid>>>()
                .Where(m => m.MessageType == MachineGroupMessages.CompleteQueueAndPauseMachineGroup)
                .DurableSubscribe(msg => msg.Data.ForEach(CompleteQueueAndPause), logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineGroupMessages.GetMachineGroups);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == MachineGroupMessages.GetMachineGroups)
                .DurableSubscribe(m => uiCommunicationService.SendMessageToUI(
                    new Message<IEnumerable<CommonMachines.MachineGroup>>
                    {
                        MessageType = MachineGroupMessages.MachineGroups,
                        ReplyTo = m.ReplyTo,
                        Data = MachineGroups
                    }, false, true), logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CommonMachines.MachineGroup>(
                MachineGroupMessages.CreateMachineGroup);
            subscriber.GetEvent<IMessage<CommonMachines.MachineGroup>>()
                .Where(m => m.MessageType == MachineGroupMessages.CreateMachineGroup)
                .DurableSubscribe(m =>
                {
                    var result = ResultTypes.Success;
                    CommonMachines.MachineGroup createdMachineGroup = null;
                    try
                    {
                        createdMachineGroup = Create(m.Data);
                    }
                    catch (Exception e)
                    {
                        createdMachineGroup = m.Data;
                        result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                    }

                    uiCommunicationService.SendMessageToUI(new ResponseMessage<CommonMachines.MachineGroup>
                    {
                        MessageType = MachineGroupMessages.MachineGroupCreated,
                        Result = result,
                        Data = createdMachineGroup
                    });
                }, logger);


            plcWarningDisposable =
                subscriber.GetEvent<IMessage<PlcMachineWarning>>()
                    .Where(m => m.Data != null && this.machineService.FindById(m.Data.MachineId) != null && m.MessageType == MachineMessages.PlcWarning)
                    .DurableSubscribe(
                        msg =>
                        {
                            var machine = this.machineService.FindById(msg.Data.MachineId);
                            var machineGroup = machineGroups.FindByMachineId(msg.Data.MachineId);
                            if (machine != null && machineGroup != null)
                            {
                                this.userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, string.Format("Machine '{0}' reported a PLC warning: {1}. Please contact the Packsize Support", machine.Alias, msg.Data.WarningType), string.Empty, machineGroup.Id);
                            }

                        });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<CommonMachines.MachineGroup>>(
                MachineGroupMessages.DeleteMachineGroup);
            subscriber.GetEvent<Message<List<CommonMachines.MachineGroup>>>()
                .Where(m => m.MessageType == MachineGroupMessages.DeleteMachineGroup)
                .DurableSubscribe(m =>
                {
                    foreach (var mg in m.Data)
                    {
                        var message = string.Empty;
                        var result = ResultTypes.Success;

                        try
                        {
                            Delete(mg);
                        }
                        catch (MachineGroupException e)
                        {
                            message = e.ToString();
                            result = ResultTypes.Fail;
                        }
                        catch (Exception e)
                        {
                            result = (e is RelationshipExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        finally
                        {
                            uiCommunicationService.SendMessageToUI(
                                new ResponseMessage<CommonMachines.MachineGroup>
                                {
                                    MessageType = MachineGroupMessages.MachineGroupDeleted,
                                    Result = result,
                                    Data = mg,
                                    Message = message
                                });
                        }
                    }
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CommonMachines.MachineGroup>(
                MachineGroupMessages.UpdateMachineGroup);
            subscriber.GetEvent<IMessage<CommonMachines.MachineGroup>>()
                .Where(m => m.MessageType == MachineGroupMessages.UpdateMachineGroup)
                .DurableSubscribe(m =>
                {

                    var message = string.Empty;
                    var result = ResultTypes.Success;

                    CommonMachines.MachineGroup updatedMachineGroup = null;

                    try
                    {
                        updatedMachineGroup = Update(m.Data);
                    }
                    catch (MachineGroupException e)
                    {
                        logger.LogException(LogLevel.Warning, "Update failed for {0}.  ReplyTo:{1}", e, m.Data, m.ReplyTo);
                        message = e.ToString();
                        result = ResultTypes.Fail;
                        updatedMachineGroup = m.Data; 
                    }
                    catch (Exception e)
                    {
                        logger.LogException(LogLevel.Warning, "Update failed for {0}.  ReplyTo:{1}", e, m.Data, m.ReplyTo);
                        result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        updatedMachineGroup = m.Data;
                    }

                    uiCommunicationService.SendMessageToUI(new ResponseMessage<CommonMachines.MachineGroup>
                    {
                        MessageType = MachineGroupMessages.MachineGroupUpdated,
                        Result = result,
                        ReplyTo = m.ReplyTo,
                        Data = updatedMachineGroup,
                        Message = message
                    });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CommonMachines.MachineGroup>(
                MachineGroupMessages.ChangeMachineGroupProductionMode);
            subscriber.GetEvent<IMessage<CommonMachines.MachineGroup>>()
                .Where(m => m.MessageType == MachineGroupMessages.ChangeMachineGroupProductionMode)
                .DurableSubscribe(m =>
                {
                    UpdateProductionMode(m.Data);
                    CompleteQueueAndPause(m.Data.Id);
                    publisher.Publish(new Message { MessageType = MachineGroupMessages.GetMachineGroups });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CommonMachines.MachineGroup>(
                MachineGroupMessages.ChangeMachineGroupStatus);
            subscriber.GetEvent<IMessage<CommonMachines.MachineGroup>>()
                .Where(m => m.MessageType == MachineGroupMessages.ChangeMachineGroupStatus)
                .DurableSubscribe(m => ToggleMachineGroupPaused(m.Data.Id), logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineGroupMessages.GetMachineGroupWorkflowPaths);

            MachineGroupMessages.GetMachineGroupWorkflowPaths.OnMessage(subscriber, logger, m =>
            {
                uiCommunicationService.SendMessageToUI(new ResponseMessage<List<KeyValuePair<string, string>>>
                {
                    MessageType = MachineGroupMessages.MachineGroupWorkflowPaths,
                    ReplyTo = m.ReplyTo,
                    Data = Directory.EnumerateFiles(DirectoryHelpers.ReplaceEnvironmentVariables(machineGroupWorkflowPath), "*.xaml")
                        .Select(
                            f =>
                                new KeyValuePair<string, string>(Path.GetFileNameWithoutExtension(f),
                                    Path.Combine(machineGroupWorkflowPath, Path.GetFileName(f)))).ToList()
                });
            });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CommonMachines.MachineGroup>(
                MachineGroupMessages.ClearAssignedOperator);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == MachineGroupMessages.ClearAssignedOperator)
                .DurableSubscribe(m =>
                {
                    SetAssignedOperator(m.MachineGroupId.Value, null);
                    publisher.Publish(new Message { MessageType = MachineGroupMessages.GetMachineGroups });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CommonMachines.MachineGroup>(
                MachineGroupMessages.SetAssignedOperator);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == MachineGroupMessages.SetAssignedOperator)
                .DurableSubscribe(m =>
                {
                    SetAssignedOperator(m.MachineGroupId.Value, m.UserName);
                    publisher.Publish(new Message { MessageType = MachineGroupMessages.GetMachineGroups });
                }, logger);

            //respond to the first time the OP loads
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineGroupMessages.GetMachineGroupProductionQueue);
            MachineGroupMessages.GetMachineGroupProductionQueue.OnMessage(subscriber, logger,
                msg =>
                {
                    var result = new JArray();
                    ConcurrentList<IProducible> q;

                    // Clear History to make the payload smaller
                    if (machineGroupProduciblesInQueue.TryGetValue(msg.MachineGroupId.Value, out q) && q != null)
                    {
                        result = viewModelService.CreateSearchResult(q.ToList());
                    }

                    uiCommunicationService.SendMessageToUI(new Message<JArray>
                    {
                        MessageType = MachineMessages.MachineProduction,
                        ReplyTo = msg.ReplyTo,
                        Data = result
                    });
                });
        }

        private void SelectItemAndAddToMachineGroupQueue(CommonMachines.MachineGroup machineGroup)
        {
            var canAdd = machineGroups.CanAddItemToQueue(machineGroup);
            while (canAdd) //loop until the machine queue is full
            {
                //There is logging done down the chain, none needed here.
                IProducible producible = selectionAlgorithmDispatchService.DispatchSelectProducibleWorkflow(machineGroup);
                if (producible != null)
                {
                    //There is logging done down the chain, none needed here.
                    //try to add it to the mg queue if we are successful then we can continue.
                    lock (addToQueueLock)
                    {
                        while (producible.ProducibleStatus != NotInProductionProducibleStatuses.ProducibleSelected)
                        {
                            logger.Log(LogLevel.Warning, "Producible {0} status was not set to ProducibleSelected when sent to machine group {1}, selecting a new producible", producible, machineGroup);
                            producible = selectionAlgorithmDispatchService.DispatchSelectProducibleWorkflow(machineGroup);
                            if (producible == null)
                            {
                                return; //Couldn't select work so we are done
                            }
                        }
                        if (machineGroups.AddItemToQueue(machineGroup, producible))
                        {
                            var list = machineGroupProduciblesInQueue.GetOrAdd(machineGroup.Id,
                                (id) => new ConcurrentList<IProducible>());

                            list.Add(producible);
                            uiCommunicationService.SendMessageToUI(new Message<JObject>
                            {
                                MachineGroupId = machineGroup.Id,
                                MessageType = MessageTypes.ProducibleStatusChanged,
                                Data = viewModelService.Convert(producible).Item2
                            }, true);
                            IDisposable disposable = null;
                            disposable = producible.ProducibleStatusObservable.DurableSubscribe(ps =>
                            {

                                //Send the full object so the OP can display the producible.
                                uiCommunicationService.SendMessageToUI(new Message<JObject>
                                {
                                    MachineGroupId = machineGroup.Id,
                                    MessageType = MessageTypes.ProducibleStatusChanged,
                                    Data = viewModelService.Convert(producible).Item2
                                }, true);


                                //When complete unwire
                                if (ps == ProducibleStatuses.ProducibleCompleted
                                    || ps == ErrorProducibleStatuses.ProducibleFailed
                                    || ps == ProducibleStatuses.ProducibleRemoved
                                    || ps == ErrorProducibleStatuses.NotProducible)
                                {
                                    list.Remove(producible);
                                    disposable.Dispose();
                                }
                            }, logger);
                            canAdd = machineGroups.CanAddItemToQueue(machineGroup);
                        }
                        else
                        {
                            //revert status of producible
                            logger.Log(LogLevel.Info, "AddItemToQueue for machineGroup {0} returned false when trying to add producible {1} -- reverting producible status to staged", machineGroup.Alias, producible);
                            producible.DoStatusTransition(NotInProductionProducibleStatuses.ProducibleSelected,
                                ProducibleStatuses.ProducibleRemoved);
                            producible.DoStatusTransition(ProducibleStatuses.ProducibleRemoved, 
                                NotInProductionProducibleStatuses.ProducibleStaged);
                            canAdd = false;
                        }
                    }
                }
                else
                {
                    //There is logging done down the chain, none needed here.
                    canAdd = false;
                }
            }
        }

        public CommonMachines.MachineGroup Create(CommonMachines.MachineGroup machineGroupToCreate)
        {
            var machineGroup = machineGroups.Create(machineGroupToCreate);
            SetupMachineGroupUIEvents(machineGroup);
            return machineGroup;
        }

        /// <summary>
        /// Can throw exception if update fails.
        /// </summary>
        /// <param name="machineGroupToUpdate"></param>
        /// <returns></returns>
        public CommonMachines.MachineGroup Update(CommonMachines.MachineGroup machineGroupToUpdate)
        {

            var machineGroup = machineGroups.Update(machineGroupToUpdate);

            publisher.Publish(new ResponseMessage<CommonMachines.MachineGroup>
            {
                MessageType = MachineGroupMessages.MachineGroupUpdated,
                Result = ResultTypes.Success,
                Data = machineGroup
            });
            publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
            return machineGroup;
        }

        public void Delete(CommonMachines.MachineGroup machineGroupToDelete)
        {

            var assignedToPg = productionGroupService.GetProductionGroupForMachineGroup(machineGroupToDelete.Id);
            if (assignedToPg == null)
            {
                machineGroups.Delete(machineGroupToDelete);
                if (observers.ContainsKey(machineGroupToDelete.Id))
                {
                    observers[machineGroupToDelete.Id].Item1.Dispose();
                    observers[machineGroupToDelete.Id].Item2.Dispose();
                }
                publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
            }
            else
            {
                throw new RelationshipExistsException(string.Format("Unable to delete, MachineGroup '{0}' is related to the ProductionGroup '{1}'", machineGroupToDelete.Alias, assignedToPg.Alias));
            }

        }

        public CommonMachines.MachineGroup FindByMachineId(Guid id)
        {
            return machineGroups.FindByMachineId(id);
        }

        public CommonMachines.MachineGroup FindByMachineGroupId(Guid id)
        {
            return machineGroups.Find(id);
        }

        public IEnumerable<IMachine> GetMachinesInGroup(Guid id)
        {
            var mg = FindByMachineGroupId(id);
            if (mg != null)
                return mg.ConfiguredMachines.Select(configuredMachineId => machineService.FindById(configuredMachineId));
            return new List<IMachine>();
        }

        /// <summary>
        /// Finish the items in the machine groups queue and pause the machine group
        /// </summary>
        /// <param name="machineGroupId"></param>
        public void CompleteQueueAndPause(Guid machineGroupId)
        {
            machineGroups.CompleteQueueAndPause(machineGroupId);
        }

        public IProducible GetProducibleFirstInQueue(CommonMachines.MachineGroup machineGroup)
        {
            var list = machineGroupProduciblesInQueue.GetOrAdd(machineGroup.Id, (id) => new ConcurrentList<IProducible>());
            return list.FirstOrDefault();
        }


        private void SetAssignedOperator(Guid machineGroupId, string userName)
        {
            machineGroups.SetAssignedOperator(machineGroupId, userName);
        }

        private void UpdateProductionMode(CommonMachines.MachineGroup machineGroupToUpdate)
        {
            machineGroups.UpdateProductionMode(machineGroupToUpdate);
        }


        private void ToggleMachineGroupPaused(Guid machineGroup)
        {
            machineGroups.ToggleMachineGroupPaused(machineGroup);
        }

        /// <summary>
        /// When the machine group goes online and is in Manual mode, ask for work
        /// </summary>
        /// <param name="machineGroup"></param>
        private void SetupMachineGroupUIEvents(CommonMachines.MachineGroup machineGroup)
        {
            var status = machineGroup.CurrentStatusChangedObservable
                .DurableSubscribe(ms =>
                {
                    if (ms is MachineGroupUnavailableStatuses)
                    {
                        machineGroups.ClearQueue(machineGroup.Id);
                        ConcurrentList<IProducible> q;
                        machineGroupProduciblesInQueue.TryRemove(machineGroup.Id, out q);
                        publisher.Publish(new Message() { MessageType = SelectionAlgorithmMessages.SelectionEnvironmentChanged });
                    }

                    UpdateMachinesInUi();

                }, logger);

            var prodStatus = machineGroup.CurrentProductionStatusObservable.DurableSubscribe(ms => UpdateMachinesInUi());

            var caps = machineGroup.CurrentCapabilitiesChangedObservable.DurableSubscribe(mg => UpdateMachinesInUi(), logger);


            observers.Add(machineGroup.Id, new Tuple<IDisposable, IDisposable, IDisposable>(status, prodStatus, caps));
        }

        private void UpdateMachinesInUi()
        {
            //todo: better way to send updates to UI for individual machine not all. User Story 10729
            publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
            publisher.Publish(new Message { MessageType = MachineGroupMessages.GetMachineGroups });
        }

        public void Dispose()
        {
            observers.ForEach(x =>
            {
                x.Value.Item1.Dispose();
                x.Value.Item2.Dispose();
                x.Value.Item3.Dispose();
            });
            if (plcWarningDisposable != null)
                plcWarningDisposable.Dispose();
        }

    }
}
