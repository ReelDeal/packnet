﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using PackNet.Common.Interfaces;
using PackNet.Business.Machines;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services.MachineServices.CutCreaseMachines
{
    public class EmMachineService :  PacksizeCutCreaseMachineServiceBase<EmMachine>, IEmMachineService
    {
        private readonly IAggregateMachineService machineService;

        public string Name { get { return "EmMachineService"; } }

        public EmMachineService(IEmMachines emMachines, 
            IServiceLocator serviceLocator, 
            IUICommunicationService uiCommunicationService, 
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber, 
            ILogger logger) : base(emMachines, serviceLocator)
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(EmMachineMessages.GetEmMachineSettingsFileNames);
            EmMachineMessages.GetEmMachineSettingsFileNames
                .OnMessage(subscriber, logger,
                    msg =>
                    {
                        uiCommunicationService.SendMessageToUI(new Message<List<KeyValuePair<string, string>>>
                        {
                            MessageType = EmMachineMessages.EmMachineSettingsFileNames,
                            ReplyTo = msg.ReplyTo,
                            Data = Directory.EnumerateFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, physicalMachineSettingsDir), "*.xml").Where(p => EmPhysicalMachineSettings.IsEmPhysicalMachineSettingsFilePath(p, logger))
                                    .Select(f => new KeyValuePair<string, string>(Path.GetFileNameWithoutExtension(f), Path.Combine(physicalMachineSettingsDir, Path.GetFileName(f)))).ToList()
                        });
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<EmMachine>(EmMachineMessages.CreateEmMachine);
            EmMachineMessages.CreateEmMachine
                .OnMessage<EmMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        EmMachine createdEmMachine = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachineConfiguration(msg.Data);
                            
                            if (result== ResultTypes.Success)
                                createdEmMachine = Create(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        var uiMessage = new ResponseMessage<EmMachine>
                        {
                            MessageType = MachineMessages.MachineCreated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = createdEmMachine
                        };
                        uiCommunicationService.SendMessageToUI(uiMessage, true);
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<EmMachine>(EmMachineMessages.DeleteEmMachine);
            EmMachineMessages.DeleteEmMachine
                .OnMessage<EmMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        try
                        {
                            Delete(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is RelationshipExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<EmMachine>
                        {
                            MessageType = MachineMessages.MachineDeleted,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = msg.Data
                        }, true);
                        publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<EmMachine>(EmMachineMessages.UpdateEmMachine);
            EmMachineMessages.UpdateEmMachine
                .OnMessage<EmMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        EmMachine updatedEmMachine = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachineConfiguration(msg.Data);
                            
                            if (result == ResultTypes.Success)
                                updatedEmMachine = Update(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<EmMachine>
                        {
                            MessageType = MachineMessages.MachineUpdated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = updatedEmMachine
                        });
                    }
                );


            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IMachine>(MachineMessages.SynchronizeMachineSettings);
            MachineMessages.SynchronizeMachineSettings
                .OnMessage<IMachine>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data.Id);
                        if (machine == null)
                            return;

                        try
                        {
                            UpdateMachineSettings(machine);
                        }
                        catch (MachineConfigurationException)
                        {
                            //TODO: how is this different then MachineMessages.MachineSettingsUpdated
                            uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                            {
                                MessageType = MachineMessages.MachineSynchronized,
                                Data = machine,
                                Result = ResultTypes.Fail
                            });
                            return;
                        }
                        //TODO: how is this different then MachineMessages.MachineSettingsUpdated
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                        {
                            MessageType = MachineMessages.MachineSynchronized,
                            Data = machine,
                            Result = ResultTypes.Success
                        });
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(MachineMessages.UpdateMachineSettings);
            MachineMessages.UpdateMachineSettings
                .OnMessage<Guid>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data);
                        if (machine == null)
                            return;

                        try
                        {
                            UpdateMachineSettings(machine);
                        }
                        catch (MachineConfigurationException)
                        {
                            //TODO: how is this different then
                            uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                            {
                                MessageType = MachineMessages.MachineSynchronized,
                                Data = machine,
                                Result = ResultTypes.Fail
                            });
                            return;
                        }

                        uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                        {
                            MessageType = MachineMessages.MachineSynchronized,
                            Data = machine,
                            Result = ResultTypes.Success
                        });
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(MachineMessages.PauseMachine);
            MachineMessages.PauseMachine
                .OnMessage<Guid>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data);
                        if (machine == null)
                            return;

                        emMachines.SetMachineStatus(machine, MachinePausedStatuses.MachinePaused);
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Tuple<Guid,int>>(EmMachineMessages.EnterChangeCorrugateTrack);
            EmMachineMessages.EnterChangeCorrugateTrack
                .OnMessage<Tuple<Guid,int>>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data.Item1);
                        if (machine == null)
                            return;

                        emMachines.EnterChangeCorrugate(machine, msg.Data.Item2);
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<Guid>>(
                MachineMessages.LeaveChangeCorrugate);
            MachineMessages.LeaveChangeCorrugate.OnMessage<IEnumerable<Guid>>(subscriber, logger, msg => msg.Data.ForEach(id =>
            {
                var machine = Machines.SingleOrDefault(m => m.Id == id);
                if (machine == null)
                    return;

                emMachines.LeaveChangeCorrugate(machine);
            }));

            machineService = serviceLocator.Locate<IAggregateMachineService>();
            machineService.RegisterMachineService(this);
        }
        
        public void Produce(EmMachine machine, IProducible producible)
        {
            base.Produce(machine.Id, producible);
        }

        public void Dispose()
        {
        }

        public void ConfigureTracks(ITrackBasedCutCreaseMachine machine)
        {
            machines.ConfigureTracks(machine);
        }

        public override bool IsCorrugatesUsableOnMachine(ITrackBasedCutCreaseMachine machine)
        {
            var localMachine = machines.GetMachines().Single(mach => mach.Id == machine.Id);
            
            if (!base.IsCorrugatesUsableOnMachine(machine))
            {
                return false;
            }

            var trackInfo = machine.Tracks.Where(t => t.LoadedCorrugate != null && t.LoadedCorrugate.Id != Guid.Empty).Select(t =>
                new
                {
                    Number = t.TrackNumber,
                    Offset = t.TrackOffset,
                    CorrugateWidth = t.LoadedCorrugate.Width,
                    CorrugateId = t.LoadedCorrugate.Id
                }).ToList();
            
            //verify tracks inside machine min
            var trackOutsideOfMachineMin = trackInfo.FirstOrDefault(t => t.Offset < 0);
            if (trackOutsideOfMachineMin != null)
            {
                throw new MachineConfigurationException()
                {
                    ErrorMessageType = CorrugateConfigurationMessages.TrackOutsideOfMachine,
                    TrackNumbers = new ConcurrentList<int> { trackOutsideOfMachineMin.Number },
                    MachineAlias = machine.Alias
                };
            }

            //verify tracks inside machine max
            var physicalMachineSettings = localMachine.PhysicalMachineSettings as EmPhysicalMachineSettings;
            var trackOutsideMachineMax = trackInfo.FirstOrDefault(t => t.Offset + t.CorrugateWidth > physicalMachineSettings.CrossHeadParameters.MaxHeadPosition);
            if (trackOutsideMachineMax != null)
            {
                throw new MachineConfigurationException()
                {
                    ErrorMessageType = CorrugateConfigurationMessages.TrackOutsideOfMachine,
                    TrackNumbers = new ConcurrentList<int> { trackOutsideMachineMax.Number },
                    MachineAlias = machine.Alias

                };
            }

            //verify that no track is overlapping a rubber roller that is used by its track
            for (var i = 0; i < trackInfo.Count - 1; i++)
            {
                var affectedRubberRollers =
                    physicalMachineSettings.FeedRollerParameters.RubberRollerStartingPositions.Where(
                        r =>
                            r.Position < trackInfo.ElementAt(i).Offset + trackInfo.ElementAt(i).CorrugateWidth &&
                            r.Position > trackInfo.ElementAt(i).Offset &&
                            r.TrackNumber != trackInfo.ElementAt(i).Number);

                if (affectedRubberRollers.Any(r => trackInfo.Any(ti => ti.Number == r.TrackNumber)))
                    throw new MachineConfigurationException
                    {
                        ErrorMessageType = CorrugateConfigurationMessages.OverlappingRubberrollerWithCorrugateLoaded,
                        TrackNumbers =
                            new ConcurrentList<int> { trackInfo.ElementAt(i).Number },
                        MachineAlias = machine.Alias
                    };
            }

            //Verify that no corrugate are overlapping track offsets
            for (var i = 0; i < trackInfo.Count - 1; i++)
            {
                if (trackInfo.ElementAt(i).Offset + trackInfo.ElementAt(i).CorrugateWidth >
                    trackInfo.ElementAt(i + 1).Offset)
                    throw new MachineConfigurationException()
                    {
                        ErrorMessageType = CorrugateConfigurationMessages.OverlappingTracks,
                        TrackNumbers =
                            new ConcurrentList<int> { trackInfo.ElementAt(i).Number, trackInfo.ElementAt(i + 1).Number },
                        MachineAlias = machine.Alias
                    };
            }

            return true;
        }
    }
}
