﻿using System;
using System.Net;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

using PackNet.Common.Interfaces.Services.Machines;

namespace PackNet.Services.MachineServices.CutCreaseMachines
{
    public class PhysicalMachineSettingsService: IPhysicalMachineSettingsService
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ILogger logger;
        private readonly IServiceLocator serviceLocator;
        private readonly IUICommunicationService uiCommunicationService;

        public string Name { get { return "PhysicalMachineSettingsService"; } }

        public PhysicalMachineSettingsService(
            IUICommunicationService uiCommunicationService, 
            IEventAggregatorPublisher publisher, 
            IEventAggregatorSubscriber subscriber, 
            IServiceLocator serviceLocator, 
            ILogger logger)
        {
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.serviceLocator = serviceLocator;
            this.uiCommunicationService = uiCommunicationService;
            this.logger = logger;

            // GetEmPhysicalMachineSettings
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<string>(PhysicalMachineSettingsMessages.GetEmPhysicalMachineSettings);
            
            subscriber.GetEvent<IMessage<string>>()
                .Where(m => m.MessageType == PhysicalMachineSettingsMessages.GetEmPhysicalMachineSettings)
                .DurableSubscribe(
                    m =>
                    {
                        var physicalMachineSettings = GetEmPhysicalMachineSettings(m.Data);

                        uiCommunicationService.SendMessageToUI(new Message<EmPhysicalMachineSettings>
                        {
                            MessageType = PhysicalMachineSettingsMessages.PhysicalMachineSettings,
                            Data = physicalMachineSettings,
                            ReplyTo = m.ReplyTo
                        });
                    }, logger);

            // GetFusionPhysicalMachineSettings
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<string>(PhysicalMachineSettingsMessages.GetFusionPhysicalMachineSettings);

            subscriber.GetEvent<IMessage<string>>()
                .Where(m => m.MessageType == PhysicalMachineSettingsMessages.GetFusionPhysicalMachineSettings)
                .DurableSubscribe(
                    m =>
                    {
                        var physicalMachineSettings = GetFusionPhysicalMachineSettings(m.Data);

                        uiCommunicationService.SendMessageToUI(new Message<FusionPhysicalMachineSettings>
                        {
                            MessageType = PhysicalMachineSettingsMessages.PhysicalMachineSettings,
                            Data = physicalMachineSettings,
                            ReplyTo = m.ReplyTo
                        });
                    }, logger);

            // UpdateEmPhysicalMachineSettings
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<EmPhysicalMachineSettings>(PhysicalMachineSettingsMessages.UpdateEmPhysicalMachineSettings);
            subscriber.GetEvent<IMessage<EmPhysicalMachineSettings>>()
                .Where(m => m.MessageType == PhysicalMachineSettingsMessages.UpdateEmPhysicalMachineSettings)
                .DurableSubscribe(message =>
                {
                    var result = ResultTypes.Success;

                    Exception exception = null;

                    try
                    {
                        UpdatePhysicalMachineSettings(message.Data);
                    }
                    catch (Exception e)
                    {
                        exception = e;
                        result = ResultTypes.Fail;

                        if (message.Data != null)
                        {
                            logger.Log(LogLevel.Error, "Failed to update Physical Machine Settings file: {0} due to exception: {1}.", message.Data.FileName, e);
                        }
                        else
                        {
                            logger.Log(LogLevel.Error, "Failed to update Physical Machine Settings file due to exception: {0}.", e);
                        }
                    }

                    var response = new ResponseMessage
                    {
                        MessageType = PhysicalMachineSettingsMessages.PhysicalMachineSettingsUpdated,
                        Result = result,
                        ReplyTo = message.ReplyTo,
                        Message = exception != null ? exception.Message : string.Empty
                    };

                    uiCommunicationService.SendMessageToUI(response);
                }, logger);

            // UpdateFusionPhysicalMachineSettings
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<FusionPhysicalMachineSettings>(PhysicalMachineSettingsMessages.UpdateFusionPhysicalMachineSettings);
            subscriber.GetEvent<IMessage<FusionPhysicalMachineSettings>>()
                .Where(m => m.MessageType == PhysicalMachineSettingsMessages.UpdateFusionPhysicalMachineSettings)
                .DurableSubscribe(message =>
                {
                    var result = ResultTypes.Success;

                    Exception exception = null;

                    try
                    {
                        UpdatePhysicalMachineSettings(message.Data);
                    }
                    catch (Exception e)
                    {
                        exception = e;
                        result = ResultTypes.Fail;

                        if (message.Data != null)
                        {
                            logger.Log(LogLevel.Error, "Failed to update Physical Machine Settings file: {0} due to exception: {1}.", message.Data.FileName, e);
                        }
                        else
                        {
                            logger.Log(LogLevel.Error, "Failed to update Physical Machine Settings file due to exception: {0}.", e);
                        }
                    }

                    var response = new ResponseMessage
                    {
                        MessageType = PhysicalMachineSettingsMessages.PhysicalMachineSettingsUpdated,
                        Result = result,
                        ReplyTo = message.ReplyTo,
                        Message = exception != null ? exception.Message : string.Empty
                    };

                    uiCommunicationService.SendMessageToUI(response);
                }, logger);

            serviceLocator.RegisterAsService(this);
        }

        public void Dispose()
        {
        }

        public EmPhysicalMachineSettings GetEmPhysicalMachineSettings(string fileName)
        {
            return EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(0, 0), fileName);
        }

        public FusionPhysicalMachineSettings GetFusionPhysicalMachineSettings(string fileName)
        {
            return FusionPhysicalMachineSettings.CreateFromFile(new IPEndPoint(0, 0), fileName);
        }

        public void UpdatePhysicalMachineSettings(PhysicalMachineSettings physicalMachineSettings)
        {
            physicalMachineSettings.UpdateToFile();
        }
    }
}