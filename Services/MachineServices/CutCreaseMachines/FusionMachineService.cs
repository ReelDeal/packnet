﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using PackNet.Business.Machines;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services.MachineServices.CutCreaseMachines
{
    public class FusionMachineService : PacksizeCutCreaseMachineServiceBase<FusionMachine>, IFusionMachineService
    {
        private readonly IFusionMachines fusionMachines;
        private readonly IAggregateMachineService machineService;

        public string Name { get { return "FusionMachineService"; } }

        public FusionMachineService(IFusionMachines fusionMachines,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            ILogger logger) : base(fusionMachines, serviceLocator)
        {
            this.fusionMachines = fusionMachines;
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(FusionMachineMessages.GetFusionMachineSettingsFileNames);
            FusionMachineMessages.GetFusionMachineSettingsFileNames
                .OnMessage(subscriber, logger,
                    msg =>
                    {
                        uiCommunicationService.SendMessageToUI(new Message<List<KeyValuePair<string, string>>>
                        {
                            MessageType = FusionMachineMessages.FusionMachineSettingsFileNames,
                            ReplyTo = msg.ReplyTo,
                            Data = Directory.EnumerateFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, physicalMachineSettingsDir), "*.xml").Where(p => FusionPhysicalMachineSettings.IsFusionPhysicalMachineSettingsFilePath(p, logger))
                                    .Select(f => new KeyValuePair<string, string>(Path.GetFileNameWithoutExtension(f), Path.Combine(physicalMachineSettingsDir, Path.GetFileName(f)))).ToList()
                        });
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<FusionMachine>(FusionMachineMessages.CreateFusionMachine);
            FusionMachineMessages.CreateFusionMachine
                .OnMessage<FusionMachine>(subscriber, logger,
                    msg =>
                    {
                        logger.Log(LogLevel.Trace, "CreateFusionMachine called in FusionMachineService");
                        var result = ResultTypes.Success;
                        FusionMachine createdFusionMachine = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachineConfiguration(msg.Data);

                            if (result == ResultTypes.Success)
                                createdFusionMachine = Create(msg.Data);
                        }
                        catch(Exception e)
                        {
                            logger.Log(LogLevel.Error, "CreateFusionMachine threw an exception", e.ToString());
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }

                        var uiMessage = new ResponseMessage<FusionMachine>
                        {
                            MessageType = MachineMessages.MachineCreated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = createdFusionMachine
                        };

                        uiCommunicationService.SendMessageToUI(uiMessage, true);
                        logger.Log(LogLevel.Debug, string.Format("CreateFusionMachine sent message to the UI: {0}", uiMessage.ToString()));
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<FusionMachine>(FusionMachineMessages.DeleteFusionMachine);
            FusionMachineMessages.DeleteFusionMachine
                .OnMessage<FusionMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        try
                        {
                            Delete(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is RelationshipExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<FusionMachine>
                        {
                            MessageType = MachineMessages.MachineDeleted,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = msg.Data
                        }, true);
                        publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<FusionMachine>(FusionMachineMessages.UpdateFusionMachine);
            FusionMachineMessages.UpdateFusionMachine
                .OnMessage<FusionMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        FusionMachine updatedFusionMachine = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachineConfiguration(msg.Data);
                            
                            if (result == ResultTypes.Success)
                                updatedFusionMachine = Update(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<FusionMachine>
                        {
                            MessageType = MachineMessages.MachineUpdated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = updatedFusionMachine
                        });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IMachine>(MachineMessages.SynchronizeMachineSettings);
            MachineMessages.SynchronizeMachineSettings
                .OnMessage<IMachine>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data.Id);
                        if (machine == null)
                            return;

                        try
                        {
                            UpdateMachineSettings(machine);
                        }
                        catch (MachineConfigurationException)
                        {
                            uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                            {
                                MessageType = MachineMessages.MachineSynchronized,
                                Data = machine,
                                Result = ResultTypes.Fail
                            });
                            return;
                        }

                        //TODO: how is this different then MachineMessages.MachineSettingsUpdated
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                        {
                            MessageType = MachineMessages.MachineSynchronized,
                            Data = machine,
                            Result = ResultTypes.Success
                        });
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(MachineMessages.UpdateMachineSettings);
            MachineMessages.UpdateMachineSettings
                .OnMessage<Guid>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data);
                        if (machine == null)
                            return;

                        try
                        {
                            UpdateMachineSettings(machine);
                        }
                        catch (MachineConfigurationException)
                        {
                            uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                            {
                                MessageType = MachineMessages.MachineSynchronized,
                                Data = machine,
                                Result = ResultTypes.Fail
                            });
                        }

                        //TODO: how is this different then 
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<IMachine>
                        {
                            MessageType = MachineMessages.MachineSynchronized,
                            Data = machine,
                            Result = ResultTypes.Success
                        });
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(MachineMessages.PauseMachine);
            MachineMessages.PauseMachine
                .OnMessage<Guid>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data);
                        if (machine == null)
                            return;
                        fusionMachines.PauseMachine(machine);
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(MachineMessages.EnterChangeCorrugate);
            MachineMessages.EnterChangeCorrugate
                .OnMessage<Guid>(subscriber, logger,
                    msg =>
                    {
                        var machine = Machines.SingleOrDefault(m => m.Id == msg.Data);
                        if (machine == null)
                            return;
                        fusionMachines.PauseMachine(machine);
                        
                    });

            machineService = serviceLocator.Locate<IAggregateMachineService>();
            machineService.RegisterMachineService(this);
            serviceLocator.RegisterAsService(this);
        }

        public void Produce(FusionMachine machine, IProducible producible)
        {
            base.Produce(machine.Id, producible);
        }

        public void Dispose()
        {
        }
        
        public void ConfigureTracks(ITrackBasedCutCreaseMachine machine)
        {
            fusionMachines.ConfigureTracks(machine);
        }

        public override bool IsCorrugatesUsableOnMachine(ITrackBasedCutCreaseMachine machine)
        {
            var localMachine = machines.GetMachines().Single(mach => mach.Id == machine.Id);

            if (base.IsCorrugatesUsableOnMachine(machine))
            {
                var trackInfo =
                    machine.Tracks.Where(t => t.LoadedCorrugate != null && t.LoadedCorrugate.Id != Guid.Empty).Select(t =>
                        new
                        {
                            Number = t.TrackNumber,
                            Offset = t.TrackOffset,
                            CorrugateWidth = t.LoadedCorrugate.Width,
                            CorrugateId = t.LoadedCorrugate.Id
                        }).ToList();

                if (trackInfo.Sum(t => t.CorrugateWidth) >
                    (localMachine.PhysicalMachineSettings as FusionPhysicalMachineSettings).CrossHeadParameters.MaximumPosition)
                {
                    throw new MachineConfigurationException()
                    {
                        ErrorMessageType = CorrugateConfigurationMessages.CorrugateTooWideForMachine,
                        MachineAlias = machine.Alias,
                        TrackNumbers = new ConcurrentList<int>()
                    };
                }
            }

            return false;
        }
    }
}
