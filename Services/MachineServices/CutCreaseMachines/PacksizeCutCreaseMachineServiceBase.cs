﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.Business.Machines;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services.MachineServices.CutCreaseMachines
{
    using CodeGeneration;

    public abstract class PacksizeCutCreaseMachineServiceBase<T> where T : IPacksizeCutCreaseMachine
    {
        protected readonly IPacksizeCutCreaseMachines<T> machines;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private readonly IServiceLocator serviceLocator;
        private CodeGenerationService codeGenService;

        protected Subject<T> machineErrorOccuredObservable = new Subject<T>();

        protected const string physicalMachineSettingsDir = @"Data\PhysicalMachineSettings";

        protected PacksizeCutCreaseMachineServiceBase(IPacksizeCutCreaseMachines<T> machines, IServiceLocator serviceLocator)
        {
            this.machines = machines;
            this.serviceLocator = serviceLocator;
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            logger = serviceLocator.Locate<ILogger>();
            machineErrorOccuredObservable = new Subject<T>();
            machines.GetMachines().ForEach(WireupListeners);
        }

        private void WireupListeners(T machine)
        {
            // machine.CurrentStatusChangedObservable.DurableSubscribe(s=> s., logger);
        }

        public virtual bool IsCorrugatesUsableOnMachine(ITrackBasedCutCreaseMachine machine)
        {
            var trackInfo = machine.Tracks.Where(t => t.LoadedCorrugate != null && t.LoadedCorrugate.Id != Guid.Empty).Select(t =>
                new
                {
                    Number = t.TrackNumber,
                    Offset = t.TrackOffset,
                    CorrugateWidth = t.LoadedCorrugate.Width,
                    CorrugateId = t.LoadedCorrugate.Id
                }).ToList();

            var identicalCorrugateGrouping = trackInfo.GroupBy(t => t.CorrugateId).FirstOrDefault(g => g.Count() > 1);
            if (identicalCorrugateGrouping != null)
            {
                throw new MachineConfigurationException()
                {
                    ErrorMessageType = CorrugateConfigurationMessages.IdenticalCorrugate,
                    TrackNumbers = new ConcurrentList<int>(identicalCorrugateGrouping.Select(t => t.Number).ToList()),
                    MachineAlias = machine.Alias
                };
            }

            return true;
        }

        private CodeGenerationService CodeGenService
        {
            get
            {
                return codeGenService ?? (codeGenService = serviceLocator.Locate<CodeGenerationService>());
            }
        }

        public void Delete(T machine)
        {
            machines.Delete(machine);
        }

        public T Update(T machine)
        {
            return machines.Update(machine);
        }

        public bool CanProduce(Guid machineId, IProducible producible)
        {
            var machine = Machines.SingleOrDefault(m => m.Id == machineId);
            if (machine == null)
                return false;

            return machines.CanProduce(machine, producible);
        }

        public IEnumerable<T> Machines { get { return machines.GetMachines(); } }


        public T Create(T machine)
        {
            machine = machines.Create(machine);
            //wireup ui messages for machine status changes
            machine.CurrentStatusChangedObservable.Subscribe(status => SetMachineStatus(machine, status));
            return machine;
        }

        [BsonIgnore]
        [JsonIgnore]
        public IObservable<T> MachineErrorOccuredObservable { get { return machineErrorOccuredObservable.AsObservable(); } }

        protected void UpdateMachineSettings(T machine)
        {
            machines.UpdateMachineSettings(machine);

            var message = new Message()
            {
                MessageType = MachineMessages.MachineSettingsUpdated
            };
            publisher.Publish(message);
        }

        public T Find(Guid machineId)
        {
            return machines.Find(machineId);
        }

        public void SetMachineStatus(T machine, MachineStatuses newStatus)
        {

            if (machine.CurrentStatus == newStatus)
                return;

            if (newStatus == MachineStatuses.MachineOnline && machine.Errors.Any())
                return;

            if (newStatus == MachinePausedStatuses.MachinePaused)
            {
                 machine.CurrentStatus = newStatus;
                //When a machine is in a invalid state and coming out of pause, send it to manual mode
                //if (invalidUnPauseStatuses.Contains(machine.CurrentMachineStatus)) 
                //    machine.CurrentProductionStatus = MachineProductionStatuses.ManualProductionMode;
            }

            if (newStatus == MachineStatuses.MachineOnline)
            {
                //if (invalidUnPauseStatuses.Contains(machine.CurrentStatus))
                //    machine.CurrentProductionStatus = MachineProductionStatuses.ManualProductionMode;
                machine.CurrentStatus = newStatus;
            }
            else
                machine.CurrentStatus = newStatus;

            var message = new Message<IPacksizeCutCreaseMachine>
            {
                MessageType = MachineMessages.ChangeMachineState,
                Data = machine
            };
            publisher.Publish(message);
        }


        /// <summary>
        /// Called via workflow
        /// </summary>
        /// <param name="machineId"></param>
        /// <param name="producible"></param>
        public void Produce(Guid machineId, IProducible producible)
        {
            Produce(machines.Find(machineId), producible);
        }

        private void Produce(T machine, IProducible producible)
        {
            try
            {
                SetTrackNumberIfNotSet(producible, machine);
                var sw = new Stopwatch();
                sw.Start();
                var instructions = CodeGenService.GenerateCode(machine.Id, producible);
                sw.Stop();
                logger.Log(LogLevel.Trace, "Code Gen took {0}ms on {1} for {2}",sw.ElapsedMilliseconds, machine, producible);

                if (instructions != null)
                    machines.Produce(machine.Id, producible, instructions);
                else
                {
                    //This really should never happen because Optimal CorrCalc should have done the code gen
                    producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
                    logger.Log(LogLevel.Error, string.Format("Code generation for producible with ID {0} failed.", producible.Id));
                }
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Warning, "Produce on packsizeCutCreaseMachine threw exception. {0} working on {1}",e,machine, producible);
                throw;
            }
            
        }

        private void SetTrackNumberIfNotSet(IProducible producible, T machine)
        {
            var packsizeCarton = producible as IPacksizeCarton;
            if (packsizeCarton == null)
                throw new Exception(String.Format("Producible Item '{0}' is not an IPacksizeCarton and can't be created on machine '{1}'", producible.Id, machine.Alias));

            if (packsizeCarton.CartonOnCorrugate == null || packsizeCarton.CartonOnCorrugate.Corrugate == null)
            {
                throw new Exception(String.Format("Producible Item '{0}' did not have an optimal corrugate assigned when sent to machine '{1}'", producible.Id, machine.Alias));
            }

            var trackWithRequiredCorrugate = machine.Tracks.FirstOrDefault(t => t.LoadedCorrugate != null && t.LoadedCorrugate.Id == packsizeCarton.CartonOnCorrugate.Corrugate.Id);
                
            if (trackWithRequiredCorrugate == null)
            {
                throw new Exception(String.Format("Producible Item '{0}' requires corrugate '{1}' but machine '{2}' does not have that corrugate loaded", producible.Id, packsizeCarton.CartonOnCorrugate.Corrugate.Alias, machine.Alias));
            }

            packsizeCarton.TrackNumber = trackWithRequiredCorrugate.TrackNumber;
        }
    }
}
