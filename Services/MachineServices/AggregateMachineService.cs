﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Services.MachineServices.CutCreaseMachines;

namespace PackNet.Services.MachineServices
{
    /// <summary>
    /// Refactor to roll up all machine services into a single interface
    /// Register this with Service locator. The first thing a machine service should do when loaded is register itself with this class
    /// </summary>
    public class AggregateMachineService : IAggregateMachineService
    {
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;

        private readonly ILogger logger;

        private readonly ConcurrentList<IMachineService<IMachine>> services = new ConcurrentList<IMachineService<IMachine>>();

        public AggregateMachineService(
            IUICommunicationService uiCommunicationService,
            IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher,
            IMachineServicesImporter machineServicesImporter,
            IServiceLocator serviceLocator,
            ILogger logger)
        {
            this.uiCommunicationService = uiCommunicationService;
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.logger = logger;

            serviceLocator.RegisterAsService(this);

            services.AddRange(machineServicesImporter.MachineServices);

            SetupSubscribers();
        }

        public void Dispose()
        {
            services.ForEach(m => m.Dispose());
        }

        public string Name
        {
            get { return "Aggregate Machine Service"; }
        }

        public IEnumerable<IMachine> Machines
        {
            get { return services.SelectMany(s => s.Machines.ToList()); }
        }

        public IMachine FindById(Guid machineId)
        {
            return services.SelectMany(s => s.Machines).SingleOrDefault(m => m.Id == machineId);
        }

        public IMachine FindByAlias(string alias)
        {
            return services.SelectMany(s => s.Machines).FirstOrDefault(m => m.Alias == alias);
        }

        public ResultTypes ValidateNetworkMachinePortConfiguration(INetworkConnectedMachine machine)
        {
            var networkConnectedMachines = Machines.Where(m => m is INetworkConnectedMachine)
            .Cast<INetworkConnectedMachine>();

            //validate port
            if (machine.Port < 1 || machine.Port > 65535)
                return ValidationOfNetworkConfigurationResultTypes.PortOutOfRange_1to65535;

            //Check for usage - the MAC address check was related to discovery
            if (
                networkConnectedMachines.Any(
                    m => ( //!string.IsNullOrWhiteSpace(m.MacAddress) && m.MacAddress == machine.MacAddress) &&
                                                  m.IpOrDnsName == machine.IpOrDnsName &&
                                                  m.Port == machine.Port &&
                                                  m.Id != machine.Id)))
                return ValidationOfNetworkConfigurationResultTypes.IpAddressAlreadyInUse;

            return ResultTypes.Success;
        }

        /// <summary>
        /// Determines if a network connected machine already exists in the system with the specified IP/Port combination.
        /// Checks if the IP is valid
        /// Checks for valid port number.
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
        public ResultTypes ValidateNetworkMachineConfiguration(INetworkConnectedMachine machine)
        {
            var result = ResultTypes.Success;
            var networkConnectedMachines = Machines.Where(m => m is INetworkConnectedMachine)
                .Cast<INetworkConnectedMachine>();


            //validate port
            if (machine.Port < 1 || machine.Port > 65535)
                result = ValidationOfNetworkConfigurationResultTypes.PortOutOfRange_1to65535;
            //validate is an address
            try
            {
                if (Networking.FindIpEndPoint(machine.IpOrDnsName, machine.Port, true) == null)
                    result = ValidationOfNetworkConfigurationResultTypes.InvalidNetworkIpOrDnsValue;
            }
            catch (ArgumentNullException argumentNullException)
            {
                logger.Log(LogLevel.Error, argumentNullException.ToString());
                result = ValidationOfNetworkConfigurationResultTypes.InvalidNetworkIpOrDnsValue;
            }

            //Check for usage - the MAC address check was related to discovery
            if (networkConnectedMachines.Any(
                    m => //(!string.IsNullOrWhiteSpace(m.MacAddress) && m.MacAddress == machine.MacAddress) &&
                        m.IpOrDnsName == machine.IpOrDnsName &&
                        m.Port == machine.Port &&
                        m.Id != machine.Id))
            {
                result = ValidationOfNetworkConfigurationResultTypes.IpAddressAlreadyInUse;
            }

            return result;
        }

        public void RegisterMachineService(IMachineService<IMachine> machineServiceToRegister)
        {
            if (services.All(s => s.GetType() != machineServiceToRegister.GetType()))
                services.Add(machineServiceToRegister);
        }

        /// <summary>
        /// returns true if any machine has the corrugate currently loaded
        /// </summary>
        /// <param name="corrugate"></param>
        /// <returns></returns>
        public bool IsCorrugateLoadedOnAnyMachine(Corrugate corrugate)
        {
            return Machines
                .Where(m => m is ITrackBasedCutCreaseMachine)
                .Cast<ITrackBasedCutCreaseMachine>()
                .Where(m => m.Tracks != null)
                .SelectMany(m => m.Tracks)
                .Where(t => t.LoadedCorrugate != null)
                .Any(t => t.LoadedCorrugate.Id == corrugate.Id);
        }

        private void SetupSubscribers()
        {
            //Setup serialization pass through for ui 
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineMessages.GetMachines);
            MachineMessages.GetMachines
                .OnMessage(subscriber, logger, message =>
                {
                    var machines = services.SelectMany(s => s.Machines);
                    uiCommunicationService.SendMessageToUI(new Message<IEnumerable<MachinePresentationModel<IMachine>>>
                    {
                        MessageType = MachineMessages.Machines,
                        Data = machines.Select(m => new MachinePresentationModel<IMachine>
                        {
                            Id = m.Id,
                            MachineType = m.MachineType,
                            Alias = m.Alias,
                            Description = m.Description,
                            Data = m
                        }).ToList(),
                        ReplyTo = message.ReplyTo
                    }, false, true);
                });

            //Send machine status changes to the UI
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IMachine>(MachineMessages.MachineStatusChanged);
            //send machine status changes as a get machines call
            //todo: not sure we should be doing this here.  Should the UI ask for all machine status
            MachineMessages.MachineStatusChanged
                .OnMessage<IMachine>(subscriber, logger,
                    msg => publisher.Publish(new Message { MessageType = MachineMessages.GetMachines }));


            //todo: is this the right place for this call?  Should our generic machine service know about corrugate machines or IPacksizeMachines?
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineMessages.GetCorrugatesLoadedOnMachines);
            MachineMessages.GetCorrugatesLoadedOnMachines
                .OnMessage(subscriber, logger,
                    msg =>
                    {
                        var machines = from machine in Machines
                                       where machine is IPacksizeCutCreaseMachine
                                       select machine as IPacksizeCutCreaseMachine;

                        var corrugates = (from machine in machines
                                          where machine.Tracks.Any(t => t.LoadedCorrugate != null)
                                          from corrugate in machine.Tracks.Where(t => t.LoadedCorrugate != null).Select(t => t.LoadedCorrugate)
                                          select corrugate).ToList();

                        uiCommunicationService.SendMessageToUI(new Message<List<Corrugate>>
                        {
                            MessageType = MachineMessages.CorrugatesOnMachines,
                            ReplyTo = msg.ReplyTo,
                            Data = corrugates
                        });
                    }
                );



            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<ITrackBasedCutCreaseMachine>>(
                MachineMessages.AssignCorrugates);
            subscriber.GetEvent<Message<List<ITrackBasedCutCreaseMachine>>>()
                .Where(m => m.MessageType == MachineMessages.AssignCorrugates)
                .ObserveOn(System.Reactive.Concurrency.Scheduler.Default)
                .DurableSubscribe(HandleCorrugateAssignment, logger);

            CorrugateMessages.UpdateCorrugate
                .OnMessage<Corrugate>(subscriber, logger, GetCorrugatesLoadedOnMachines);
        }

        private void HandleCorrugateAssignment(Message<List<ITrackBasedCutCreaseMachine>> m)
        {
            var validationErrors = ValidateCorrugatesForMachines(m.Data);

            if (validationErrors.Any() == false)
            {
                var assignmentErrors = AssignCorrugatesToMachines(m.Data);

                if (assignmentErrors.Any() == false)
                {
                    publisher.Publish(new Message { MessageType = MachineGroupMessages.GetMachineGroups });
                    var responseMessage = new ResponseMessage
                    {
                        MessageType = MachineMessages.AssignCorrugatesResponse,
                        MachineGroupId = m.MachineGroupId,
                        Result = ResultTypes.Success
                    };
                    uiCommunicationService.SendMessageToUI(responseMessage);
                    publisher.Publish(responseMessage);
                }
                else
                {
                    uiCommunicationService.SendMessageToUI(new ResponseMessage
                        <IEnumerable<MachineConfigurationErrorPresentationModel>>
                    {
                        MessageType = MachineMessages.AssignCorrugatesResponse,
                        MachineGroupId = m.MachineGroupId,
                        Result = ResultTypes.PartialSuccess,
                        Data = assignmentErrors
                    });
                }
            }
            else
            {
                uiCommunicationService.SendMessageToUI(new ResponseMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>
                {
                    MessageType = MachineMessages.AssignCorrugatesResponse,
                    MachineGroupId = m.MachineGroupId,
                    Data = validationErrors,
                    Result = ResultTypes.Fail
                });
            }
        }

        private List<MachineConfigurationErrorPresentationModel> ValidateCorrugatesForMachines(IEnumerable<ITrackBasedCutCreaseMachine> machines)
        {
            var validationErrors = new List<MachineConfigurationErrorPresentationModel>();

            var emMachineService = services.Single(s => s.GetType() == typeof(EmMachineService)) as IEmMachineService;
            var fusionMachineService = services.Single(s => s.GetType() == typeof(FusionMachineService)) as IFusionMachineService;

            machines.AsParallel().ForAll(machine =>
            {
                try
                {
                    if (machine.MachineType == MachineTypes.Em && emMachineService != null)
                    {
                        emMachineService.IsCorrugatesUsableOnMachine(machine);
                    }

                    if (machine.MachineType == MachineTypes.Fusion && fusionMachineService != null)
                    {
                        fusionMachineService.IsCorrugatesUsableOnMachine(machine);
                    }
                }
                catch (Exception e)
                {
                    if (e is MachineConfigurationException)
                    {
                        validationErrors.Add(new MachineConfigurationErrorPresentationModel(e as MachineConfigurationException));
                    }
                    else
                    {
                        logger.Log(LogLevel.Error,
                            string.Format("Unexpected exception thrown when assigning corrugates: {0}", e));
                    }
                }
            });

            return validationErrors;
        }

        private List<MachineConfigurationErrorPresentationModel> AssignCorrugatesToMachines(
            IEnumerable<ITrackBasedCutCreaseMachine> machines)
        {
            var assignmentErrors = new List<MachineConfigurationErrorPresentationModel>();
            var emMachineService = services.Single(s => s.GetType() == typeof(EmMachineService)) as IEmMachineService;
            var fusionMachineService = services.Single(s => s.GetType() == typeof(FusionMachineService)) as IFusionMachineService;

            machines.AsParallel().ForAll(machine =>
            {
                try
                {
                    var m = FindById(machine.Id);
                    if (m.CurrentProductionStatus == MachineProductionInProgressStatuses.ProductionInProgress)
                    {
                        throw new Exception(string.Format("Corrugate cannot be changed in this moment, the machine {0} is in production", machine.Alias));
                    }
                    if (machine.MachineType == MachineTypes.Em && emMachineService != null)
                    {
                        emMachineService.ConfigureTracks(machine);
                    }

                    if (machine.MachineType == MachineTypes.Fusion && fusionMachineService != null)
                    {
                        fusionMachineService.ConfigureTracks(machine);
                    }
                }
                catch (Exception e)
                {
                    if (e is MachineConfigurationException)
                    {
                        assignmentErrors.Add(new MachineConfigurationErrorPresentationModel(e as MachineConfigurationException));
                    }
                    else
                    {
                        logger.Log(LogLevel.Error,
                            string.Format("Unexpected exception thrown when assigning corrugates: {0}", e));
                    }
                }

            });

            return assignmentErrors;
        }

        private void GetCorrugatesLoadedOnMachines(IMessage<Corrugate> message)
        {
            var corrugatesOnMachines = Machines
                .Where(m => m is IPacksizeCutCreaseMachine)
                .Cast<IPacksizeCutCreaseMachine>()
                .SelectMany(m => m.Tracks)
                    .Where(t => t.LoadedCorrugate != null)
                    .Select(t => t.LoadedCorrugate).Distinct();

            var response = new ResponseMessage<IEnumerable<Corrugate>>
            {
                MessageType = MachineMessages.CorrugatesLoadedOnMachines,
                Result = ResultTypes.Success,
                Data = corrugatesOnMachines,
                ReplyTo = message.ReplyTo,
                UserName = message.UserName,
            };

            publisher.Publish(response);
        }

        public void Produce(Guid machineId, IProducible producible)
        {
            if (producible.ProducibleStatus == ProducibleStatuses.ProducibleRemoved)
            {
                logger.Log(LogLevel.Warning, "Producible {0} not sent to Machine Group beacue it has been removed");
                return;
            }

            var machineService = services.FirstOrDefault(s => s.Machines.Any(m => m.Id == machineId));

            if (machineService != null)
            {
                producible.ProduceOnMachineId = machineId;
                machineService.Produce(machineId, producible);
            }
            else
            {
                throw new ArgumentException(string.Format("Machine with MachineId:{0}, does not exists.", machineId));
            }
        }

        public bool CanProduce(Guid machineId, IProducible producible)
        {
            var machineService = services.FirstOrDefault(s => s.Machines.Any(m => m.Id == machineId));

            if (machineService != null)
            {
                try
                {
                    return machineService.CanProduce(machineId, producible);
                }
                catch (Exception ex)
                {
                    logger.Log(LogLevel.Error, ex.ToString());
                    return false;
                }
            }

            throw new ArgumentException(string.Format("Machine with MachineId:{0}, does not exists.", machineId));
        }
    }

    public class MachineConfigurationErrorPresentationModel
    {
        public MachineConfigurationErrorPresentationModel(MachineConfigurationException exception)
        {
            ErrorMessageType = exception.ErrorMessageType;
            TrackNumbers = exception.TrackNumbers;
            MachineAlias = exception.MachineAlias;
        }

        public Enumeration ErrorMessageType { get; set; }

        public ConcurrentList<int> TrackNumbers { get; set; }

        public string MachineAlias { get; set; }
    }

}