﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

using PackNet.Common.Interfaces.Machines;

namespace PackNet.Services.MachineServices
{
    public interface IMachineServicesImporter : IDisposable
    {
        [ImportMany]
        IEnumerable<IMachineService<IMachine>> MachineServices { get; }
    }
}