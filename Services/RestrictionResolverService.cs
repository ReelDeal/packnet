﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ExternalSystems;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;

namespace PackNet.Services
{
    public class RestrictionResolverService : IRestrictionResolverService
    {
        public RestrictionResolverService()
        {
            MongoDbHelpers.TryRegisterSerializer<MicroMeter>(new MicroMeterSerializer());

            //Setup known resolvers
            Resolvers = new Dictionary<Type, IRestrictionResolver>();
            AddResolver(new BasicRestrictionResolver());
            AddResolver(new CanProduceWithCorrugateResolver());
            AddResolver(new CanProduceWithTileCountResolver());
            AddResolver(new CanProduceWithRotationResolver());
            AddResolver(new ExternalPrintRestrictionResolver());
            AddResolver(new MustProduceOnCorrugateWithPropertiesResolver());
            AddResolver(new MustProduceWithOptimalCorrugateResolver());
            AddResolver(new LabelForCartonRestrictionResolver());
            AddResolver(new ProductionGroupSpecificResolver());
            AddResolver(new MustProducueWithSpecifiedRotationResolver());
            AddResolver(new MachineSpecificResolver());         
            AddResolver(new MachineGroupSpecificRestrictionResolver());
            AddResolver(new BasicAggregateResolver(this));

            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<CartonPropertyGroup>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Classification>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<PickZone>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<string>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<int>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Guid>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<double>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Int64>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<float>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Corrugate>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Template>>(
                    new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<MachineSpecificRestriction>(
                new RestrictionDiscriminatorConvention());            
            MongoDbHelpers.TryRegisterDiscriminatorConvention<MachineGroupSpecificRestriction>(
                new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<ProductionGroupSpecificRestriction>(
                new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<LabelForCartonRestriction>(
                new RestrictionDiscriminatorConvention());

            MongoDbHelpers.TryRegisterDiscriminatorConvention<CanProduceWithRotationRestriction>(
                new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<CanProduceWithCorrugateRestriction>(
                new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<CanProduceWithTileCountRestriction>(
                new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<MustProduceWithOptimalCorrugateRestriction>(
                new RestrictionDiscriminatorConvention());

            MongoDbHelpers.TryRegisterDiscriminatorConvention<MustProduceOnCorrugateWithPropertiesRestriction>(
                new RestrictionDiscriminatorConvention());

            MongoDbHelpers.TryRegisterDiscriminatorConvention<ExternalPrintRestriction>(
                new RestrictionDiscriminatorConvention());

            MongoDbHelpers.TryRegisterDiscriminatorConvention<MustProduceWithSpecifiedRotationRestriction>(
                new RestrictionDiscriminatorConvention());

            //Capabilities
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicCapability<string>>(
                    new CapabilityDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicCapability<int>>(
                    new CapabilityDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicCapability<int>>(
                    new CapabilityDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicCapability<double>>(
                    new CapabilityDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicCapability<Corrugate>>(
                    new CapabilityDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<MachineSpecificCapability>(
                new CapabilityDiscriminatorConvention());            
            MongoDbHelpers.TryRegisterDiscriminatorConvention<MachineGroupSpecificCapability>(
                new CapabilityDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<ProductionGroupSpecificCapability>(
                new CapabilityDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicCapability<Template>>(
                new CapabilityDiscriminatorConvention());
        }

        public Dictionary<Type, IRestrictionResolver> Resolvers { get; set; }

        public string Name { get { return "Resolver Service"; } }

        public IEnumerable<Type> HandlesRestrictions { get { return Resolvers.Keys; } }

        /// <summary>
        /// Iterate over all resolvers registered in the service to resolve the restrictions to the machine group.
        /// </summary>
        /// <param name="restriction"></param>
        /// <param name="capabilities"></param>
        /// <returns></returns>
        public bool Resolve(IRestriction restriction, IEnumerable<ICapability> capabilities)
        {
            var tp = restriction.GetType();
            if (Resolvers.ContainsKey(tp))
                return Resolvers[tp].Resolve(restriction, capabilities);

            return false;
        }

        /// <summary>
        /// Add a new resolver to the service
        /// </summary>
        /// <param name="resolver"></param>
        /// <exception cref="System.ArgumentException">If another resolver has already registered a type in HandlesRestrictions that you also specified to handle.</exception>
        public void AddResolver(IRestrictionResolver resolver)
        {
            resolver.HandlesRestrictions.ForEach(h =>
            {
                if (Resolvers.ContainsKey(h))
                    throw new ArgumentException(string.Format("Failed to add Resolver {2} for type {0}. That type is already registered to {1}", h.ToString(),
                        Resolvers[h].GetType().ToString(), resolver.GetType().ToString()));
                Resolvers.Add(h, resolver);
            });
        }

        public void Dispose()
        {
        }

    }
}
