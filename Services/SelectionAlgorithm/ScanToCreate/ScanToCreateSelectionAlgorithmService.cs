﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.ScanToCreate;
using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.ScanToCreateProducible;
using PackNet.Services.Properties;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PackNet.Services.SelectionAlgorithm.ScanToCreate
{
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;

    public class ScanToCreateSelectionAlgorithmService : IScanToCreateSelectionAlgorithmService
    {
        private readonly IRestrictionResolverService restrictionResolverService;
        private readonly IPackagingDesignService packagingDesignService;
        private readonly IScanToCreateRepository scanToCreateRepository;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ICorrugateService corrugateService;
        private readonly IProductionGroupService productionGroupService;
        private readonly IMachineGroupService machineGroupService;
        private readonly IUserNotificationService userNotificationService;
        private readonly ILogger logger;
        private readonly IUICommunicationService uiCommunicationService;

        private IDisposable reproduceDisposable;
        private IDisposable removeCartonsDisposable;
        private IDisposable reproduceCartonsDisposable;
        private IDisposable searchDisposable;
        private IDisposable productionGroupChangedDisposible;
        private object reproduceLock = new object();

        private readonly ConcurrentDictionary<Guid /*ProductionGroup Id*/, ConcurrentList<ScanToCreateProducible>> ProductionGroupQueues = new ConcurrentDictionary<Guid, ConcurrentList<ScanToCreateProducible>>();

        private readonly ConcurrentDictionary<Guid /*ProducibleId*/, IDisposable> producibleStatusesDisposables = new ConcurrentDictionary<Guid, IDisposable>();

        public string Name { get { return "ScanToCreateSelectionAlgorithmService"; } }
        private ConcurrentDictionary<Guid /*OrderId*/, HashSet<Guid>> machineGroupsWorkingOnOrder = new ConcurrentDictionary<Guid, HashSet<Guid>>();

        public ScanToCreateSelectionAlgorithmService(
            IRestrictionResolverService restrictionResolverService,
            IPackagingDesignService packagingDesignService,
            IScanToCreateRepository scanToCreateRepository,
            IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher,
            ICorrugateService corrugateService,
            IProductionGroupService productionGroupService,
            IMachineGroupService machineGroupService,
            IUserNotificationService userNotificationService,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            ILogger logger)
        {
            this.restrictionResolverService = restrictionResolverService;
            this.packagingDesignService = packagingDesignService;
            this.scanToCreateRepository = scanToCreateRepository;
            this.publisher = publisher;
            this.corrugateService = corrugateService;
            this.productionGroupService = productionGroupService;
            this.userNotificationService = userNotificationService;
            this.logger = logger;
            this.machineGroupService = machineGroupService;
            this.uiCommunicationService = uiCommunicationService;
            serviceLocator.RegisterAsService<IScanToCreateSelectionAlgorithmService>(this);

            Wireups(scanToCreateRepository, subscriber, publisher, productionGroupService, machineGroupService, uiCommunicationService, logger);

            var task = Task.Factory.StartNew(LoadFromDB);
            task.ContinueWith((t) =>
            {
                if (t.IsFaulted)
                    logger.Log(LogLevel.Error, "Error: Failed to load items from DB " + t.Exception.Message);
            });
        }

        private void Wireups(IScanToCreateRepository scanToCreateRepository, IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher, IProductionGroupService productionGroupService, IMachineGroupService machineGroupService,
            IUICommunicationService uiCommunicationService, ILogger logger)
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Order>(OrderMessages.ReproduceCartonForOrder);
            OrderMessages.ReproduceCartonForOrder
                .OnMessage<Order>(subscriber, logger,
                    msg =>
                    {
                        if(SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(msg.MachineGroupId, productionGroupService, machineGroupService, SelectionAlgorithmTypes.ScanToCreate))
                            Reproduce(msg.Data);
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<Order>>(OrderMessages.ReproduceOrders);
            OrderMessages.ReproduceOrders
                .OnMessage<List<Order>>(subscriber, logger,
                    msg => msg.Data.ForEach(order =>
                    {
                        if (SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(msg.MachineGroupId, productionGroupService, machineGroupService, SelectionAlgorithmTypes.ScanToCreate))
                            Reproduce(order);
                    }));

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<Producible>>(CartonMessages.RemoveScanToCreateCartons);
            removeCartonsDisposable = subscriber.GetEvent<IMessage<IEnumerable<Producible>>>()
                .Where(msg => msg.MessageType == CartonMessages.RemoveScanToCreateCartons)
                .DurableSubscribe(m =>
                {
                    RemoveProducibles(m.Data, m.UserName);
                    SendOrders(m);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SearchProducibles>(
                SelectionAlgorithmMessages.SearchForProducibles);
            searchDisposable = subscriber.GetEvent<IMessage<SearchProducibles>>()
                .Where(m => m.Data.SelectionAlgorithmTypes == SelectionAlgorithmTypes.ScanToCreate)
                .DurableSubscribe(m =>
                {
                    var searchResult = scanToCreateRepository.Search(m.Data.Criteria, Settings.Default.MaxScanToCreateSearchResults);

                    uiCommunicationService.SendMessageToUI(new Message<IEnumerable<ScanToCreateProducible>>
                    {
                        MessageType = CartonMessages.SearchScanToCreateResponse,
                        ReplyTo = m.ReplyTo,
                        Data = searchResult
                    });
                }, logger);

            productionGroupChangedDisposible =
                ProductionGroupMessages.ProductionGroupChanged.OnMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>(
                    subscriber,
                    logger,
                    msg =>
                    {
                        if (!ProductionGroupQueues.ContainsKey(msg.Data.Id))
                            return;
                        ReStageAllProduciblesInPg(msg.Data);
                    });

            OrderMessages.GetOrdersHistory
                .OnMessage(subscriber, logger,
                    SendOrdersHistory
                );

            OrderMessages.GetOrders
                .OnMessage(subscriber, logger, 
                    SendOrders
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineMessages.GetProductionHistory);
            MachineMessages.GetProductionHistory
                .OnMessage(subscriber, logger,
                    msg =>
                    {
                        if (!msg.MachineGroupId.HasValue)
                        {
                            logger.Log(LogLevel.Warning,
                                "request for getproductionhistory does not have a machinegroupid set");
                            return;
                        }

                        var machineGroup = machineGroupService.FindByMachineGroupId(msg.MachineGroupId.Value);
                        if (machineGroup == null)
                        {
                            return;
                        }

                        var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(msg.MachineGroupId.Value);
                        if (productionGroup == null)
                        {
                            return;
                        }

                        if (productionGroup.SelectionAlgorithm != SelectionAlgorithmTypes.ScanToCreate)
                        {
                            return;
                        }

                        var history = scanToCreateRepository.GetProductionHistory(machineGroup.Id);
                        uiCommunicationService.SendMessageToUI(new Message<IEnumerable<IProducible>>
                        {
                            MessageType = MachineMessages.MachineProductionHistory,
                            ReplyTo = msg.ReplyTo,
                            Data = history.ToList()
                        });
                    });
        }

        private void SendOrders(IMessage msg)
        {
            if (SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(msg.MachineGroupId, productionGroupService, machineGroupService, SelectionAlgorithmTypes.ScanToCreate))
            {
                var pg = productionGroupService.GetProductionGroupForMachineGroup(msg.MachineGroupId.Value);
                SendOrders(pg.Id, msg.ReplyTo);
            }
        }

        private void SendOrders(Guid pgId, string replyTo = null)
        {
            var queue = ProductionGroupQueues.GetOrAdd(pgId, guid => new ConcurrentList<ScanToCreateProducible>());

            //Get all orders that are not in an error state, and are not completed.
            var data = queue.Where(p => !(p.ProducibleStatus is ErrorProducibleStatuses) &&
                    (p.ProducibleStatus != ProducibleStatuses.ProducibleCompleted) &&
                    (p.ProducibleStatus != ProducibleStatuses.ProducibleRemoved)).ToList();

            uiCommunicationService.SendMessageToUI(new Message<IEnumerable<ScanToCreateProducible>>
            {
                MessageType = OrderMessages.Orders,
                ReplyTo = replyTo,
                Data = data
            });
        }

        private void SendOrdersHistory(IMessage msg)
        {
            if (SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(msg.MachineGroupId, productionGroupService,
                machineGroupService, SelectionAlgorithmTypes.ScanToCreate))
            {
                if (!msg.MachineGroupId.HasValue)
                {
                    logger.Log(LogLevel.Warning,
                        "request for getproductionhistory does not have a machinegroupid set");
                    return;
                }

                var machineGroup = machineGroupService.FindByMachineGroupId(msg.MachineGroupId.Value);
                if (machineGroup == null)
                {
                    return;
                }

                var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(msg.MachineGroupId.Value);
                if (productionGroup == null)
                {
                    return;
                }

                if (productionGroup.SelectionAlgorithm != SelectionAlgorithmTypes.ScanToCreate)
                {
                    return;
                }

                var history = scanToCreateRepository.GetProductionHistory(machineGroup.Id);
                uiCommunicationService.SendMessageToUI(new Message<IEnumerable<IProducible>>
                {
                    MessageType = OrderMessages.OrdersHistory,
                    ReplyTo = msg.ReplyTo,
                    Data = history.ToList()
                });
            }
        }

        private void Reproduce(Order order)
        {
            //This lock is needed to make sure that two sub articles that should be reproduced does not overwrite the reproduce quantity for each other
            lock (reproduceLock)
            {
                var dbOrder = scanToCreateRepository.FindByIdOrSubId(order.Id);
                var pgId = FindMachineGroupOrProductionGroupForOrder(dbOrder.Id);
                var queue = ProductionGroupQueues.GetOrAdd(pgId, guid => new ConcurrentList<ScanToCreateProducible>());
            
                var producible = queue.FirstOrDefault(p => ContainsId(p, order.Id));
                if (producible == null)
                    producible = dbOrder;

                if (producible == null)
                {
                    return;
                }

                producible.Producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction;
                producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction;

                UpdateReproduceQuantityForOrderWithId(producible, order.Id, order.ReproducedQuantity);

                AddToProductionGroupQueue(producible);

                var productionGroupRestriction =
                    producible.Restrictions.FirstOrDefault(r => r is ProductionGroupSpecificRestriction);

                if (productionGroupRestriction != null)
                {
                    //Tell machines to get work
                    publisher.Publish(new Message<Guid>
                    {
                        MessageType = ProductionGroupMessages.GetWorkForProductionGroup,
                        Data = ((ProductionGroupSpecificRestriction)productionGroupRestriction).Value
                    });
                }
            }
        }

        private bool ContainsId(ScanToCreateProducible p, Guid id)
        {
            if (p.Id == id)
                return true;
            if (p.Producible.Id == id)
                return true;
            var kit = p.Producible as Kit;
            return kit != null && kit.ItemsToProduce.Any(i => i.Id == id);
        }

        private void UpdateReproduceQuantityForOrderWithId(ScanToCreateProducible order, Guid id, int quantity)
        {
            var kit = order.Producible as Kit;
            if (order.Id == id)
            {
                order.ReproducedQuantity += quantity;
                order.CurrentReproduceQuantity = quantity;
                if (kit != null)
                {
                    foreach (var o in kit.ItemsToProduce.OfType<Order>())
                    {
                        o.ReproducedQuantity += o.OriginalQuantity;
                        o.CurrentReproduceQuantity = o.OriginalQuantity;
                    }
                }
            }
            else if(kit != null)
            {
                var orderInKit = kit.ItemsToProduce.FirstOrDefault(i => i.Id == id) as Order;
                orderInKit.ReproducedQuantity += quantity;
                orderInKit.CurrentReproduceQuantity = quantity;
            }

            Persist(order);
        }

        private Guid FindMachineGroupOrProductionGroupForOrder(Guid orderId)
        {
            foreach (var orderQueue in ProductionGroupQueues)
            {
                if (orderQueue.Value.Any(o => o.Id == orderId))
                    return orderQueue.Key;
            }

            return Guid.Empty;
        }

        private void LoadFromDB()
        {
            var produciblesToReload = scanToCreateRepository.FindWhereStatusIsNot(
                ProducibleStatuses.ProducibleCompleted,
                ErrorProducibleStatuses.ProducibleFailed,
                ProducibleStatuses.ProducibleRemoved).ToList();
            var dbItems = new List<ScanToCreateProducible>();
            produciblesToReload.ForEach(c =>
            {
                c.ProducibleStatus = c.Producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported;
                dbItems.Add(c);
            });
            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                Data = dbItems,
                SelectionAlgorithmType = SelectionAlgorithmTypes.ScanToCreate,
                MessageType = MessageTypes.Restaged
            };

            publisher.Publish(message);
        }

        public IProducible GetNextJobForMachineGroup(MachineGroup machineGroup)
        {
            //Get PG for machine
            var machinePg = productionGroupService.GetProductionGroupForMachineGroup(machineGroup.Id);

            var queue = ProductionGroupQueues.GetOrAdd(machinePg.Id, guid => new ConcurrentList<ScanToCreateProducible>());
            var job = GetNextJobForMachineGroup(machineGroup, queue, 0);
            
            if (job == null)
                return null;

            if (job.RemainingQuantity > 0 || GetRemainingQuantityForOrder(job) > 0)
            {
                var kit = job.Producible as Kit;
                if (kit != null)
                {
                    kit.ItemsToProduce.ForEach(i => UpdateProducedOnMachineGroup(i, machineGroup.Id));
                    return GetNextProducibleFromKit(job, machineGroup);
                }

                job.ProducedOnMachineGroupId = machineGroup.Id;
                var producible = ReturnProducibleAndSubscribe(job, job, machineGroup);
                return producible;
            }

            return null;
        }

        private void UpdateProducedOnMachineGroup(IProducible producible, Guid machineGroupId)
        {
            producible.ProducedOnMachineGroupId = machineGroupId;
            var wrapper = producible as IProducibleWrapper;
            if(wrapper != null)
                UpdateProducedOnMachineGroup(wrapper.Producible, machineGroupId);
        }

        private IProducible GetNextProducibleFromKit(ScanToCreateProducible job, MachineGroup machineGroup)
        {
            var kit = job.Producible as Kit;
            var firstOrder = kit.ItemsToProduce.FirstOrDefault(i => i is Order && (i as Order).RemainingQuantity > 0) as Order;
            if (firstOrder != null)
                return ReturnProducibleAndSubscribe(job, firstOrder, machineGroup);
            
            return null;
        }

        private void ReStageAllProduciblesInPg(Common.Interfaces.DTO.ProductionGroups.ProductionGroup pg)
        {
            ConcurrentList<ScanToCreateProducible> stcProduciblesToReload;
            ProductionGroupQueues.TryGetValue(pg.Id, out stcProduciblesToReload);
            if (stcProduciblesToReload == null || stcProduciblesToReload.Count == 0)
                return;

            stcProduciblesToReload.ForEach(RemoveOptimalCorrugateRestrictions);

            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                MessageType = MessageTypes.Restaged,
                Data = stcProduciblesToReload.ToList(),
                SelectionAlgorithmType = SelectionAlgorithmTypes.ScanToCreate
            };

            // Carton will need to be restaged so that its optimal corrugate may be recalculatecd
            publisher.Publish(message);
        }


        private void RemoveOptimalCorrugateRestrictions(IProducible producible)
        {
            if (producible is ScanToCreateProducible)
            {
                RemoveOptimalCorrugateRestrictions(((ScanToCreateProducible)producible).Producible);              
            }
            else if (producible is Order)
            {
                RemoveOptimalCorrugateRestrictions(((Order)producible).Producible);
            }
            else if (producible is ICarton)
            {
                producible.Restrictions.RemoveAll(r => r is MustProduceWithOptimalCorrugateRestriction);
            }
        }

        private IProducible ReturnProducibleAndSubscribe(ScanToCreateProducible scanToCreateProducible, Order firstOrder,MachineGroup machineGroup)
        {
            var producible = MongoDbHelpers.DeepCopy(firstOrder.Producible);
            var carton = producible as ICarton;
            var baseQuantity = firstOrder.CurrentReproduceQuantity > 0
                ? firstOrder.CurrentReproduceQuantity
                : firstOrder.OriginalQuantity;
            if (carton != null)
            {
                var tileCountToUse = Math.Min(firstOrder.RemainingQuantity, carton.CartonOnCorrugate.TileCount);
                if (tileCountToUse > 1)
                {
                    var tiledCarton = new TiledCarton();
                    tiledCarton.CustomerUniqueId = producible.CustomerUniqueId;
                    var tiles = new List<ICarton>();
                    for (int i = 0; i < tileCountToUse; i++)
                    {
                        var tmpProducible = MongoDbHelpers.DeepCopy(firstOrder.Producible) as ICarton;
                        tmpProducible.Id = Guid.NewGuid();
                        tmpProducible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
                        tiledCarton.CustomerUniqueId += " " + (baseQuantity - (firstOrder.RemainingQuantity - 1));
                        tiles.Add(tmpProducible);
                        SetupSubscription(scanToCreateProducible, firstOrder, machineGroup, tmpProducible);
                        if (i < tileCountToUse - 1)
                            tiledCarton.CustomerUniqueId += " +";
                    }
                    tiledCarton.AddTiles(tiles);

                    if (producible is ICarton)
                        producible = tiledCarton;
                    else
                        carton = tiledCarton;
                }
                else
                {
                    if (scanToCreateProducible.CustomerUniqueId != firstOrder.Producible.CustomerUniqueId)
                    {
                        producible.CustomerUniqueId = scanToCreateProducible.CustomerUniqueId + ":" +
                                                      firstOrder.Producible.CustomerUniqueId;
                        var number = (baseQuantity - (firstOrder.RemainingQuantity - 1));
                        if (number > 0)
                            producible.CustomerUniqueId += ":" + number;
                    }
                    SetupSubscription(scanToCreateProducible, firstOrder, machineGroup, producible);
                }
            }
            else
                SetupSubscription(scanToCreateProducible, firstOrder, machineGroup, producible);

            producible.Id = Guid.NewGuid();
            producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            UpdateProducedOnMachineGroup(producible, machineGroup.Id);
            return producible;
        }

        private void SetupSubscription(ScanToCreateProducible scanToCreateProducible, Order firstOrder, MachineGroup machineGroup, IProducible producible)
        {
            producibleStatusesDisposables.GetOrAdd(
                scanToCreateProducible.Id,
                scanToCreateProducible.ProducibleStatusObservable.DurableSubscribe(
                    ps =>
                    {
                        if (ps == ProducibleStatuses.ProducibleRemoved)
                        {  
                            producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                        }
                        if (ps == ProducibleStatuses.ProducibleCompleted || ps == ProducibleStatuses.ProducibleRemoved
                            || ps == ErrorProducibleStatuses.ProducibleFailed
                            || scanToCreateProducible.ProducibleStatus == ProducibleStatuses.ProducibleRemoved)
                        {
                            IDisposable subscription;
                            producibleStatusesDisposables.TryRemove(scanToCreateProducible.Id, out subscription);
                            if (subscription != null)
                            {
                                subscription.Dispose();
                            }
                        }

                    }));

            firstOrder.ProducibleInProgress.Add(producible);
            producibleStatusesDisposables.GetOrAdd(producible.Id,
                producible.ProducibleStatusObservable.DurableSubscribe(ps =>
                {
                    if (ps == ProducibleStatuses.ProducibleCompleted || ps == ProducibleStatuses.ProducibleRemoved || ps == ErrorProducibleStatuses.ProducibleFailed || scanToCreateProducible.ProducibleStatus == ProducibleStatuses.ProducibleRemoved)
                    {
                        IDisposable subscription;
                        producibleStatusesDisposables.TryRemove(producible.Id, out subscription);
                        if (subscription != null)
                        {
                            logger.Log(LogLevel.Debug, "Scan To Create: Disposed subscription for producible {0}", scanToCreateProducible.Id);
                            subscription.Dispose();
                        }
                    }

                    if (ps is InProductionProducibleStatuses)
                    {
                        firstOrder.ProducibleStatus = ps;
                        scanToCreateProducible.ProducibleStatus = ps;
                    }
                    if (ps == ErrorProducibleStatuses.ProducibleFailed)
                    {
                        logger.Log(LogLevel.Debug, "Scan To Create: {0} failed on {1} and we will not be trying to produce it again.", scanToCreateProducible, machineGroup);
                        firstOrder.ProducibleInProgress.Remove(producible);
                        if (!firstOrder.Failed.Contains(producible.Id))
                        firstOrder.Failed.Add(producible.Id);
                        firstOrder.ProducibleStatus = ps;
                        scanToCreateProducible.ProducibleStatus = ps;
                        if (scanToCreateProducible.RemainingQuantity > 0 && !scanToCreateProducible.Failed.Contains(scanToCreateProducible.Producible.Id))
                        {
                            scanToCreateProducible.Failed.Add(scanToCreateProducible.Producible.Id);
                        }
                        var kit = scanToCreateProducible.Producible as Kit;
                        if (kit != null)
                        {
                            kit.ItemsToProduce.OfType<Order>().ForEach(o =>
                            {
                                while (o.RemainingQuantity > 0)
                                {
                                    o.Failed.Add(new Guid());
                                }
                            });
                        }

                        // Add the remaining work as failed
                        while (firstOrder.RemainingQuantity > 0)
                        {
                            firstOrder.Failed.Add(new Guid());
                        }
                        userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Warning, string.Format("Failed to produce {0}.  We will NOT try again.", scanToCreateProducible.CustomerUniqueId), "", machineGroup.Id);
                    }
                    if (ps == ProducibleStatuses.ProducibleRemoved)
                    {
                        logger.Log(LogLevel.Debug, "Scan To Create: {0} producible was removed on {1}. Will try to produce it again.", scanToCreateProducible, machineGroup);
                        firstOrder.ProducibleInProgress.Remove(producible);
                    }
                    if (ps == ProducibleStatuses.ProducibleCompleted)
                    {
                        firstOrder.ProducibleInProgress.Remove(producible);
                        firstOrder.Produced.Add(producible.Id);
                        var kit = scanToCreateProducible.Producible as Kit;
                        if (kit != null &&
                            kit.ItemsToProduce.OfType<Order>().All(o => o.RemainingQuantity == 0 && o.InProgressQuantity == 0) && scanToCreateProducible.RemainingQuantity > 0)
                        {
                            scanToCreateProducible.Produced.Add(kit.Id);
                            if (scanToCreateProducible.RemainingQuantity > 0)
                            {
                                foreach (var o in kit.ItemsToProduce.OfType<Order>())
                                {
                                    o.ReproducedQuantity += o.OriginalQuantity;
                                }
                            }
                        }
                        
                        if (firstOrder.RemainingQuantity == 0 && firstOrder.InProgressQuantity == 0)
                            firstOrder.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                        if (scanToCreateProducible.RemainingQuantity == 0 && scanToCreateProducible.InProgressQuantity == 0 && GetRemainingQuantityForOrder(scanToCreateProducible) == 0)
                        {
                            scanToCreateProducible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                            var machinePg = productionGroupService.GetProductionGroupForMachineGroup(machineGroup.Id);
                            var queue = ProductionGroupQueues.GetOrAdd(machinePg.Id, guid => new ConcurrentList<ScanToCreateProducible>());
                            queue.Remove(scanToCreateProducible);
                        }
                    }

                    Persist(scanToCreateProducible);
                    UpdateUIWithOrderChange(scanToCreateProducible);
                }, logger));
        }

        private int GetRemainingQuantityForOrder(Order order)
        {
            var remaining = 0;
            var kit = order.Producible as Kit;
            if (kit != null)
            {
                // Current kit remaining quantity
                remaining = GetRemainingQuantityForProducibles(kit.ItemsToProduce);
                if (order.RemainingQuantity > 1)
                {
                    // Complete kits remaining quantity
                    remaining += (order.RemainingQuantity - 1) * GetOriginalQuantityForProducibles(kit.ItemsToProduce);
                }
            }
            else
            {
                // No kit, just regular order remaining quantity
                remaining = order.RemainingQuantity;
            }
            return remaining;
        }

        private int GetRemainingQuantityForProducibles(IEnumerable<IProducible> producibles)
        {
            var orders = producibles.OfType<Order>();
            return orders.Any() ? orders.Sum(o => o.RemainingQuantity) : 0;
        }

        private int GetOriginalQuantityForProducibles(IEnumerable<IProducible> producibles)
        {
            var orders = producibles.OfType<Order>();
            return orders.Any() ? orders.Sum(o => o.OriginalQuantity) : 0;
        }

        private void UpdateUIWithOrderChange(Order order, string replyTo = null)
        {
            uiCommunicationService.SendMessageToUI(new Message<Order>
            {
                MessageType = OrderMessages.OrderChanged,
                ReplyTo = replyTo,
                Data = order
            });
        }

        //Recurse into queue and find producible that the machine group can create
        //private IProducible GetNextJobForMachineGroup(MachineGroup machineGroup, IEnumerable<IProducible> queue, int skipNum, List<IRestriction> failedRestr = null)
        private ScanToCreateProducible GetNextJobForMachineGroup(MachineGroup machineGroup, IEnumerable<IProducible> queue, int skipNum, List<IRestriction> failedRestr = null)
        {
            var queueList = queue.OfType<ScanToCreateProducible>().ToList();
            var scanToCreateProducible = queueList
                .Where(c => 
                    (c.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged ||
                    c.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction ||
                    c.ProducibleStatus is InProductionProducibleStatuses))
                .Skip(skipNum).FirstOrDefault();

            if (scanToCreateProducible == null)
            {
                if (failedRestr != null && failedRestr.Any())
                {
                    userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Warning, string.Format("Failed to produce {0} producibles because of failed restrictions: {1}.", skipNum, String.Join(", ", failedRestr)), string.Empty, machineGroup.Id);
                }
                return null;
            }
            else if (scanToCreateProducible.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction)
            {
                scanToCreateProducible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            }

            var failedRestrictions =
                scanToCreateProducible.Producible.Restrictions.Where(r => !restrictionResolverService.Resolve(r, machineGroup.AllMachineCapabilities)).ToList();

            if (failedRestrictions.Any())
            {
                logger.Log(LogLevel.Info, string.Format("Scan To Create Selection skipped Producible:{0}({1}) because Restrictions ({2}) were not satisfied by MachineGroup",
                        scanToCreateProducible.CustomerUniqueId, scanToCreateProducible.Id, String.Join(", ", failedRestrictions)));

                return GetNextJobForMachineGroup(machineGroup, queueList, skipNum + 1, failedRestr == null ? failedRestrictions : failedRestr.Concat(failedRestrictions).DistinctBy(r => r.GetType()).ToList());
            }

            var machineGroupsAssignedToOrder = machineGroupsWorkingOnOrder.GetOrAdd(scanToCreateProducible.Id, new HashSet<Guid>());
            lock (machineGroupsAssignedToOrder) //Only one machine can add itself to the machines working orders hashset at a time
            {
                //make sure the order is still valid
                if (GetRemainingQuantityForOrder(scanToCreateProducible) == 0)
                {
                    return GetNextJobForMachineGroup(machineGroup,queueList,skipNum + 1,failedRestr == null? failedRestrictions: failedRestr.Concat(failedRestrictions).DistinctBy(r => r.GetType()).ToList());
                }

                //Producible must be assigned to my machine group, or not assigned to a machine group
                if (machineGroupsAssignedToOrder.Any(mgId => mgId == machineGroup.Id) //Is my machine group already working on this order?
                         || !machineGroupsAssignedToOrder.Any()
                         || (machineGroupsAssignedToOrder.All( //No machine is working on this order
                             mgId => machineGroupService.FindByMachineGroupId(mgId) != null
                                        && machineGroupService.FindByMachineGroupId(mgId).CurrentStatus == MachineGroupUnavailableStatuses.MachineGroupOffline)))
                {
                    machineGroupsAssignedToOrder.Add(machineGroup.Id);
                    return scanToCreateProducible;
                }
            }
            return GetNextJobForMachineGroup(machineGroup, queueList, skipNum + 1, failedRestr == null ? failedRestrictions : failedRestr.Concat(failedRestrictions).DistinctBy(r => r.GetType()).ToList());
        }

        public void AddToProductionGroupQueue(ScanToCreateProducible producible)
        {
            if (String.IsNullOrWhiteSpace(producible.CustomerUniqueId))
                producible.CustomerUniqueId = producible.Id.ToString();

            var productionGroupRestriction = producible.Restrictions.FirstOrDefault(r => r is ProductionGroupSpecificRestriction);
            if (productionGroupRestriction == null)
            {
                logger.Log(LogLevel.Error, "Producible item {0} did not contain a ProductionGroupSpecificRestriction and can not be routed", producible.Id);
                return;
            }

            //Locate production group and queue
            var pgId = ((ProductionGroupSpecificRestriction)productionGroupRestriction).Value;
            var queue = ProductionGroupQueues.GetOrAdd(pgId, guid => new ConcurrentList<ScanToCreateProducible>());

            Persist(producible);
            
            if (queue.Any(p => p.Id == producible.Id) == false)
                queue.Add(producible);

            SendOrders(pgId);
        }

        public void Persist(ScanToCreateProducible producible)
        {
            try
            {
                scanToCreateRepository.Create(producible);
            }
            catch
            {
                scanToCreateRepository.Update(producible);
            }
        }

        public void Dispose()
        {
            if (reproduceDisposable != null)
            {
                reproduceDisposable.Dispose();
                reproduceDisposable = null;
            }

            if (searchDisposable != null)
            {
                searchDisposable.Dispose();
                searchDisposable = null;
            }

            if (reproduceCartonsDisposable != null)
            {
                reproduceCartonsDisposable.Dispose();
                reproduceCartonsDisposable = null;
            }

            if (productionGroupChangedDisposible != null)
            {
                productionGroupChangedDisposible.Dispose();
                productionGroupChangedDisposible = null;
            }
        }

        public void RemoveProducibles(IEnumerable<IProducible> produciblesToRemove, string user)
        {
            if (produciblesToRemove.Any() == false)
                return;

            foreach (var producible in produciblesToRemove)
            {
                if (ProductionGroupQueues.IsEmpty)
                {
                    logger.Log(LogLevel.Debug, "User {0} removed producible item {1} -- no pg queues found producible status set to removed", user, producible);
                    var stcProducible = scanToCreateRepository.FindByIdOrSubId(producible.Id);
                    if (stcProducible != null)
                    {
                        stcProducible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                        stcProducible.Producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                        scanToCreateRepository.Update(stcProducible); 
                    }
                    continue;
                }

                if (producible.ProducibleStatus == ProducibleStatuses.ProducibleCompleted)
                {
                    logger.Log(LogLevel.Info, "Trying to remove producible item {0} -- removing jobs that are completed is not allowed", producible);
                    continue;
                }

                ConcurrentList<ScanToCreateProducible> prodQueue = null;
                producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved; 
                ProductionGroupQueues.ForEach(kv =>
                {
                    if (kv.Value.Any(p => p.Id == producible.Id))
                    {
                        prodQueue = kv.Value;
                    }                        
                });

                if (prodQueue != null)
                {

                    var item = prodQueue.FirstOrDefault(p => p.Id == producible.Id);
                    if (item != null)
                    {
                        item.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                        item.ProducibleInProgress.ForEach(p => p.ProducibleStatus = ProducibleStatuses.ProducibleRemoved);
                        scanToCreateRepository.Update(item); 
                        prodQueue.Remove(item);
                        logger.Log(LogLevel.Info, "User {0} removed carton {1}", user, producible.CustomerUniqueId);
                    }
                }
                else
                {
                    var scanToCreateProducible = scanToCreateRepository.FindByIdOrSubId(producible.Id);
                    scanToCreateProducible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                    scanToCreateProducible.ProducibleInProgress.ForEach(p => p.ProducibleStatus = ProducibleStatuses.ProducibleRemoved);
                    scanToCreateRepository.Update(scanToCreateProducible); 

                    logger.Log(
                        LogLevel.Info,
                        "User {0} removing carton {1} which was not found in any ProductionGroup queue, updating status to removed",
                        user,
                        scanToCreateProducible.CustomerUniqueId);

                }
            }
        }

        public IReadOnlyCollection<IProducible> GetProducibles(ProducibleStatuses status = null)
        {
            throw new NotImplementedException();
        }
    }
}