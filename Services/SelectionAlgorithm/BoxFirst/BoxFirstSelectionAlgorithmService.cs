﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

using MongoDB.Driver;

using Newtonsoft.Json.Linq;

using PackNet.Business.ProductionGroups;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.Utils;
using PackNet.Data.BoxFirstProducible;
using PackNet.Services.Properties;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;

namespace PackNet.Services.SelectionAlgorithm.BoxFirst
{
    using Common.Interfaces.DTO.PickZones;
    using Common.Interfaces.DTO.ProductionGroups;
    using Common.Interfaces.DTO.SelectionAlgorithm;
    using Common.Interfaces.RestrictionsAndCapabilities;

    public class BoxFirstSelectionAlgorithmService : IBoxFirstSelectionAlgorithmService
    {
        private readonly ILogger logger;
        private readonly IBoxFirstRepository boxFirstRepository;
        private readonly IProductionGroupService productionGroupService;
        private readonly IAggregateMachineService machineService;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IUserNotificationService userNotificationService;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ICorrugateService corrugateService;
        private readonly IMachineGroupService machineGroupService;
        private readonly ICartonPropertyGroupService cartonPropertyGroupService;

        private readonly IClassificationService classificationService;
        private IDisposable searchDisposable;

        private readonly ConcurrentDictionary<Guid, BoxFirstProducible> producibles;

        private IDisposable productionGroupChangedDisposible;
        private IDisposable removeCartonsDisposable;
        private IDisposable importRemoveCartonsDisposable;
        private IViewModelService viewModelService;

        public BoxFirstSelectionAlgorithmService(IServiceLocator serviceLocator, IBoxFirstRepository repository)
        {
            boxFirstRepository = repository;
            logger = serviceLocator.Locate<ILogger>();
            productionGroupService = serviceLocator.Locate<IProductionGroupService>();
            machineService = serviceLocator.Locate<IAggregateMachineService>();
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            corrugateService = serviceLocator.Locate<ICorrugateService>();
            machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            cartonPropertyGroupService = serviceLocator.Locate<ICartonPropertyGroupService>();
            classificationService = serviceLocator.Locate<IClassificationService>();
            producibles = new ConcurrentDictionary<Guid, BoxFirstProducible>();
            boxFirstRepository.GetNonCompletedCartons().ForEach(b => producibles.TryAdd(b.Id, b));
            serviceLocator.RegisterAsService<IBoxFirstSelectionAlgorithmService>(this);
            try
            {
                viewModelService = serviceLocator.Locate<IViewModelService>();
            }
            catch (Exception)
            {
                serviceLocator.ServiceAddedObservable.Subscribe((s) =>
                {
                    if (s is IViewModelService)
                        viewModelService = s as IViewModelService;
                });

            }
            WireupToEvents();
            var task = Task.Factory.StartNew(() =>
            {
                //TODO: Wire up to the services we need (external print for example)
                //Give the rest of the services time to start
                Thread.Sleep(TimeSpan.FromSeconds(5));
                var sw = Stopwatch.StartNew();
                ResetInvalidStatuses();
                TriggerStagingWorkflow();
                Console.WriteLine("BoxFirstSelectionAlgorithmService loaded and ready... took " + sw.ElapsedMilliseconds + "(ms)");
            });
            task.ContinueWith(t =>
            {
                if (t.IsFaulted)
                {
                    logger.Log(LogLevel.Error, t.Exception.ToString);
                }
            });
        }

        private void ResetInvalidStatuses()
        {
            var unCompletedProducibles = GetAllProduciblesWithoutStatuses(new List<ProducibleStatuses>() { ProducibleStatuses.ProducibleCompleted }).Cast<BoxFirstProducible>().ToList();
            var stagedOrImportedProducibles = unCompletedProducibles.Where(p => p.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged || p.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleImported).ToList();
            foreach (var bf in unCompletedProducibles.Except(stagedOrImportedProducibles))
            {
                CheckForLabelSentToPrinterAndCompleteIfSo(bf, ErrorProducibleStatuses.ProducibleFailed);
            }
        }

        private void TriggerStagingWorkflow()
        {
            // don't get to publish any producibles 
            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                SelectionAlgorithmType = SelectionAlgorithmTypes.BoxFirst,
                Data = new List<IProducible>(),
                MessageType = MessageTypes.Restaged
            };
            publisher.Publish(message);
        }

        private void WireupToEvents()
        {
            //TODO: If this is registred all reproduce carton and GetProductionHistory will
            // trigger twice since it is registred at BoxLastSAS. How will we handle this?
            //uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineMessages.GetProductionHistory);
            MachineMessages.GetProductionHistory.OnMessage(subscriber, logger, GetProductionHistory);

            CartonMessages.ReproduceCarton.OnMessage<Producible>(subscriber, logger, m => ReproduceIfFoundInRepository(m.Data));
            CartonMessages.ReproduceCartons.OnMessage<IEnumerable<Producible>>(subscriber, logger, m => ReproduceRangeIfFoundInRepository(m.Data));

            subscriber.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == MessageTypes.ProducibleStatusChanged)
                .Sample(TimeSpan.FromSeconds(5), subscriber.Scheduler)
                .DurableSubscribe(PublishProducibleCountForEachCartonPropertyGroup, logger);

            subscriber.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == SelectionAlgorithmMessages.StagingComplete)
                .DurableSubscribe(PublishProducibleCountForEachCartonPropertyGroup, logger);

            subscriber.GetEvent<Message<PickZone>>()
                .Where(msg => msg.MessageType == PickZoneMessages.UpdatePickZoneStatus)
                .DurableSubscribe(PublishProducibleCountForEachCartonPropertyGroup, logger);

            subscriber.GetEvent<IMessage<Classification>>()
                .Where(m => m.MessageType == ClassificationMessages.UpdateClassificationStatus)
                .DurableSubscribe(PublishProducibleCountForEachCartonPropertyGroup, logger);

            subscriber.GetEvent<IMessage<CartonPropertyGroup>>()
                .Where(msg => msg.MessageType == CartonPropertyGroupMessages.UpdateCartonPropertyGroupStatus)
                .DurableSubscribe(PublishProducibleCountForEachCartonPropertyGroup, logger);

            //handle production group corrugates and machine groups changed.
            productionGroupChangedDisposible =
                ProductionGroupMessages.ProductionGroupChanged
                    .OnMessage<ProductionGroup>(subscriber, logger,
                        msg =>
                        {
                            if (msg.Data.SelectionAlgorithm != SelectionAlgorithmTypes.BoxFirst)
                                return;
                            TriggerStagingWorkflow();
                        }
                    );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<Producible>>(CartonMessages.RemoveBoxFirstCartons);
            removeCartonsDisposable = subscriber.GetEvent<IMessage<IEnumerable<Producible>>>()
                .Where(msg => msg.MessageType == CartonMessages.RemoveBoxFirstCartons)
                .DurableSubscribe(m => RemoveBoxFirstProducibles(m.Data, m.UserName), logger);

            importRemoveCartonsDisposable = subscriber.GetEvent<IMessage<IEnumerable<string>>>()
                .Where(msg => msg.MessageType == CartonMessages.RemoveBoxFirstCartons)
                .DurableSubscribe(m =>
                {
                    var prods =
                        producibles.Values.Where(
                            p =>
                                (p.ProducibleStatus is InProductionProducibleStatuses == false &&
                                 p.ProducibleStatus != NotInProductionProducibleStatuses.ProducibleSelected &&
                                 p.ProducibleStatus != ProducibleStatuses.ProducibleCompleted) &&
                                m.Data.Contains(p.CustomerUniqueId)).ToList();

                    RemoveBoxFirstProducibles(prods, m.UserName);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SearchProduciblesWithRestrictions>(SelectionAlgorithmMessages.SearchForProduciblesWithRestrictions);
            searchDisposable = subscriber.GetEvent<IMessage<SearchProduciblesWithRestrictions>>()
                .Where(m => m.Data.SelectionAlgorithmTypes == SelectionAlgorithmTypes.BoxFirst)
                .DurableSubscribe(m =>
                {
                    if (viewModelService == null)
                    {
                        logger.Log(LogLevel.Error, "Missing view model service in BoxFirstSAS");
                        return;
                    }
                    var searchResult = boxFirstRepository.Search(m.Data.Criteria, Settings.Default.MaxBoxFirstSearchResults);

                    searchResult = FilterSearchResultOnRestrictions(searchResult, m.Data.Restrictions);

                    var viewModelResults = viewModelService.CreateSearchResult(searchResult);

                    uiCommunicationService.SendMessageToUI(new Message<JArray>
                    {
                        MessageType = CartonMessages.SearchBoxFirstResponse,
                        ReplyTo = m.ReplyTo,
                        Data = viewModelResults
                    }, false, true);
                }, logger);
        }

        private void RemoveBoxFirstProducibles(IEnumerable<IProducible> produciblesToRemove, string user)
        {
            if (produciblesToRemove.Any() == false)
                return;

            var ids = produciblesToRemove.Select(p => p.Id);
            produciblesToRemove.Select(p => p.CustomerUniqueId)
                .ForEach(l => logger.Log(LogLevel.Info, "User {0} removed carton {1}", user, l));

            //TODO should really the items be removed from the repository? in 2.3 we archived them.
            boxFirstRepository.Delete(ids);

            var remove = this.producibles.Values
                .Where(p => ids.Contains(p.Id)).ToList();

            remove.ForEach(bfp =>
            {
                BoxFirstProducible output = null;
                this.producibles.TryRemove(bfp.Id, out output);
            });

            TriggerStagingWorkflow();
        }

        private IEnumerable<BoxFirstProducible> FilterSearchResultOnRestrictions(
            IEnumerable<BoxFirstProducible> searchResult,
            Dictionary<string, Guid> restrictions)
        {
            if (restrictions == null || !restrictions.Keys.Any())
                return searchResult;

            searchResult = restrictions.ContainsKey("Classification")
                ? searchResult.Where(p => p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>().Id == restrictions["Classification"])
                : searchResult;

            searchResult = restrictions.ContainsKey("PickZone")
                ? searchResult.Where(p => p.Restrictions.GetRestrictionOfTypeInBasicRestriction<PickZone>().Id == restrictions["PickZone"])
                : searchResult;

            // TODO: change cpg.Id for cpg.Alias
            searchResult = restrictions.ContainsKey("CartonPropertyGroup")
                ? searchResult.Where(p => p.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>().Id == restrictions["CartonPropertyGroup"])
                : searchResult;

            ProductionGroup pg = null;
            searchResult = restrictions.ContainsKey("ProductionGroup")
                ? searchResult.Where(p => ((pg = productionGroupService.GetProductionGroupForMachineGroup(p.ProducedOnMachineGroupId)) != null && pg.Id == restrictions["ProductionGroup"]))
                : searchResult;

            return searchResult;
        }

        private void GetProductionHistory(IMessage msg)
        {
            if (!msg.MachineGroupId.HasValue)
                logger.Log(LogLevel.Error, "GetProductionHistory must pass in a Machine Group Id.");

            var machineGroup = machineGroupService.FindByMachineGroupId(msg.MachineGroupId.Value);
            if (machineGroup == null)
                return;

            var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(msg.MachineGroupId.Value);
            if (productionGroup == null)
                return;

            if (productionGroup.SelectionAlgorithm != SelectionAlgorithmTypes.BoxFirst)
                return;

            //!!! if you change this away from a database call then you need to ensure that RemoveHistory below does not change in momory producibles.
            var completedJobs = boxFirstRepository.GetProductionHistoryForMachineGroup(msg.MachineGroupId.Value, 20);
            if (completedJobs.Any())
            {
                // get the last twenty jobs, then remove the history and restrictions.
                var viewModelResults = viewModelService.CreateSearchResult(completedJobs);

                var message = new Message<JArray>
                    {
                        MessageType = MachineMessages.MachineProductionHistory,
                        ReplyTo = msg.ReplyTo,
                        Data = viewModelResults
                    };

                uiCommunicationService.SendMessageToUI(message);
            }
        }

        private void ReproduceRangeIfFoundInRepository(IEnumerable<IProducible> produciblesToReproduce)
        {
            var existingProduciblesToReproduce = new List<BoxFirstProducible>();
            produciblesToReproduce.ForEach(
                producible =>
                {
                    var localProducible = producibles.Values.FirstOrDefault(p => p.CustomerUniqueId == producible.CustomerUniqueId)
                                          ?? boxFirstRepository.FindByCustomerUniqueId(producible.CustomerUniqueId);
                    if (localProducible != null)
                    {
                        UpdateProducibleStatus(localProducible, NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction);
                        existingProduciblesToReproduce.Add(localProducible);
                    }

                });

            if (existingProduciblesToReproduce.Any())
            {
                var message = new ImportMessage<IEnumerable<IProducible>>
                {
                    Data = existingProduciblesToReproduce,
                    SelectionAlgorithmType = SelectionAlgorithmTypes.BoxFirst,
                    MessageType = MessageTypes.Restaged //TODO: Is this correct? It will increment metrics
                };

                publisher.Publish(message);
            }
        }

        private void ReproduceIfFoundInRepository(IProducible producible)
        {
            //First see if we can find it in memory
            var localProducible = producibles.Values.FirstOrDefault(p => p.CustomerUniqueId == producible.CustomerUniqueId) ??
                                  boxFirstRepository.FindByCustomerUniqueId(producible.CustomerUniqueId);
            if (localProducible == null)
                return;


            Reproduce(localProducible);
        }

        private void Reproduce(BoxFirstProducible producible)
        {
            UpdateProducibleStatus(producible, NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction);
            UpdateProducibleStatus(producible, NotInProductionProducibleStatuses.ProducibleImported);

            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                Data = new List<IProducible> { producible },
                SelectionAlgorithmType = SelectionAlgorithmTypes.BoxFirst,
                MessageType = MessageTypes.DataImported //TODO: Is this correct? It will increment metrics
            };

            // Carton will need to be restaged so that its optimal corrugate may be recalculatecd
            publisher.Publish(message);
        }

        public void Dispose()
        {
            if (searchDisposable != null)
                searchDisposable.Dispose();
            if (productionGroupChangedDisposible != null)
                productionGroupChangedDisposible.Dispose();

            if (removeCartonsDisposable != null)
                removeCartonsDisposable.Dispose();

            if (importRemoveCartonsDisposable != null)
                importRemoveCartonsDisposable.Dispose();
        }

        public string Name { get; private set; }

        public IEnumerable<IProducible> GetAllProduciblesWithStatuses(IEnumerable<ProducibleStatuses> statuses)
        {
            return producibles.Values.Where(p => statuses.Contains(p.ProducibleStatus)).ToList();
        }

        public IEnumerable<IProducible> GetAllProduciblesWithoutStatuses(IEnumerable<ProducibleStatuses> statuses)
        {
            return producibles.Values.Where(p => !statuses.Contains(p.ProducibleStatus)).ToList();
        }

        public void UpdateProducibleStatuses(IEnumerable<BoxFirstProducible> produciblesToUpdate, ProducibleStatuses status)
        {
            var produciblesToPersist = new List<BoxFirstProducible>();

            produciblesToUpdate.ForEach(p =>
            {
                if (p.ProducibleStatus == NotInProductionProducibleStatuses.AddedToMachineGroupQueue &&
                    status == NotInProductionProducibleStatuses.ProducibleStaged)
                {
                    logger.Log(LogLevel.Warning, "Going to try and change to NotInProductionProducibleStatuses.ProducibleStaged from AddedToMachineGroupQueue , ProducibleId : " + p.Id + " ('" + p.CustomerUniqueId + "')  is in status : " + p.ProducibleStatus + " this should not set it to staged");
                }
                else
                {
                    var localProducible = producibles.Values.FirstOrDefault(lp => lp.CustomerUniqueId == p.CustomerUniqueId)
                                      ?? boxFirstRepository.FindByCustomerUniqueId(p.CustomerUniqueId);
                    if (localProducible != null)
                    {
                        p.ProducibleStatus = p.Producible.ProducibleStatus = status;
                        produciblesToPersist.Add(p);
                    }
                }
            });

            Persist(produciblesToPersist);

        }

        //todo: remove this method and override the producible status in the BoxFirstProducible to do the update of it's status and its .Producible.ProducibleStatus.
        public void UpdateProducibleStatus(BoxFirstProducible producible, ProducibleStatuses status)
        {

            var localProducible = producibles.Values.FirstOrDefault(p => p.CustomerUniqueId == producible.CustomerUniqueId)
                                  ?? boxFirstRepository.FindByCustomerUniqueId(producible.CustomerUniqueId);
            if (localProducible != null)
            {
                producible.ProducibleStatus = producible.Producible.ProducibleStatus = status;
                Persist(producible);
            }

        }

        public IEnumerable<IProducible> AssignOptimalCorrugateAndTilingPartnersForProducibles(
            IEnumerable<BoxFirstProducible> produciblesToUpdate)
        {
            if (produciblesToUpdate == null)
                return new List<IProducible>();
            var tilableComparer = new BoxFirstCartonTilingEqualityComparer();
            var produciblesGroupedByTilingUniqueness = produciblesToUpdate.GroupBy(p => p.Producible, tilableComparer).ToList();

            //BEWARE: Huge performance issues with the code if you do it wrong!!!! Test with 30+ mg and 25000 cartons after changing 
            var machines = machineService.Machines.ToList();
            var productionGroups = productionGroupService.ProductionGroups.ToList();
            var allCorrugates = corrugateService.Corrugates.ToList();


            foreach (var tilableProducibles in produciblesGroupedByTilingUniqueness.ToList())
            {

                //If this is a restage we need to clear the cache for the first calculation. Since we can have different machine, corrugates and cpg in our production group
                var shouldClearCachedCorrugateCalculation = true;
                foreach (var producible in tilableProducibles.ToList())
                {
                    try
                    {
                        var carton = producible.Producible as ICarton;
                        var kit = producible.Producible as Kit;
                        if (kit != null)
                        {
                            var cartonsInKit = kit.ItemsToProduce.OfType<ICarton>().ToList();
                            if (cartonsInKit.Count() > 1) // We will not tile kits with multiple cartons
                                continue;
                            carton = cartonsInKit.FirstOrDefault();
                        }
                        if (carton == null) //This producible was a label only
                            continue;

                        var cartonPropertyGroupForProducible =
                            producible.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>();
                        if (cartonPropertyGroupForProducible != null)
                        {
                            var productionGroupForMachine = productionGroupService.FindByCartonPropertyGroupAlias(
                                productionGroups, cartonPropertyGroupForProducible.Alias);

                            if (productionGroupForMachine == null)
                            {
                                producible.CartonOnCorrugates = null;
                                producible.PossibleTilingPartners = new List<BoxFirstProducible>();
                                continue;
                            }

                            var corrugateIds = productionGroupForMachine.ConfiguredCorrugates;
                            var corrugates = allCorrugates.Where(c => corrugateIds.Contains(c.Id));
                            var machineGroupIdsInProductionGroup = productionGroupForMachine.ConfiguredMachineGroups;
                            var machineGroupsInProducitonGroup =
                                machineGroupService.MachineGroups.Where(mg => machineGroupIdsInProductionGroup.Contains(mg.Id));
                            var machineIdsInMachineGroups =
                                machineGroupsInProducitonGroup.SelectMany(mg => mg.ConfiguredMachines);
                            var cutCreaseMachinesInProductionGroup =
                                machines.Where(m => machineIdsInMachineGroups.Contains(m.Id) && m is IPacksizeCutCreaseMachine)
                                    .Cast<IPacksizeCutCreaseMachine>()
                                    .ToList();
                            var cartonOnCorrugates = corrugateService.GetOptimalCorrugates(corrugates,
                                cutCreaseMachinesInProductionGroup, carton, shouldClearCachedCorrugateCalculation);
                            shouldClearCachedCorrugateCalculation = false;
                            
                            producible.CartonOnCorrugates =
                                cartonOnCorrugates.OrderByDescending(c => c.Yield).ThenByDescending(c => c.TileCount).ToList();
                            producible.PossibleTilingPartners = tilableProducibles.Where(tp => producible.Id != tp.Id)
                                .OrderBy(c => c.Created)
                                .ToList();
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Log(LogLevel.Warning, e.Message);
                        userNotificationService.SendNotificationToSystem(NotificationSeverity.Warning, e.Message, "");
                    }
                }
            }

            return produciblesGroupedByTilingUniqueness.SelectMany(p => p);
        }

        private void PublishProducibleCountForEachCartonPropertyGroup(IMessage msg)
        {
            publisher.Publish(new Message<IEnumerable<BoxFirstProducible>>
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data =
                    producibles.Values.Where(
                        p =>
                            p.ProducibleStatus != ProducibleStatuses.ProducibleCompleted &&
                            p.ProducibleStatus != ProducibleStatuses.ProducibleRemoved &&
                            p.ProducibleStatus != ErrorProducibleStatuses.ProducibleFailed).ToList()
            });

        }

        public IEnumerable<BoxFirstProducible> GetAllStagedJobsForMachineGroup(Guid machineGroupId)
        {
            return Retry.Do(() => //This function continually throws System.InvalidOperationException: Collection was modified; enumeration operation may not execute. Retrying for now
            {
                var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(machineGroupId);
                var config = productionGroup.BoxFirstConfiguration();
                var cpgAliases = config.ConfiguredCartonPropertyGroups.Select(c => c.Alias);
                var produciblesForProductionGroup =
                    producibles.Values.Where(
                        p => BelongsToCpgs(p, cpgAliases) && p.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged)
                        .ToList();
                return produciblesForProductionGroup;
            }, TimeSpan.FromMilliseconds(50), 5);
        }

        private bool BelongsToCpgs(BoxFirstProducible p, IEnumerable<string> cpgAliases)
        {
            var cpg = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>();

            if (cpg == null)
                return false;

            return cpgAliases.Contains(cpg.Alias);
        }

        public IProducible PrepareNextJobForMachineGroup(MachineGroup machineGroup, IEnumerable<IProducible> produciblesToPrepare)
        {
            if (machineGroup.CurrentProductionStatus == MachineGroupAvailableStatuses.MachineGroupPaused
                || machineGroup.CurrentProductionStatus == MachineGroupAvailableStatuses.ProduceCurrentAndPause)
                return null;

            if (!produciblesToPrepare.Any())
                return null;

            if (produciblesToPrepare.Any(p => p.ProducibleStatus != NotInProductionProducibleStatuses.ProducibleStaged))
            {
                //TFS 9596 - Attempted fix
                string invalidIds = string.Join(",", produciblesToPrepare.Select(p => p));
                logger.Log(LogLevel.Trace, "Selection Algorithm selected item was already selected for production. Trying again. The items were: {0}", invalidIds);
                return null;
            }

            var produciblesToProduce = produciblesToPrepare.Cast<BoxFirstProducible>();
            var machinesInMachineGroup = machineGroup.ConfiguredMachines.Select(id => machineService.FindById(id)).Where(m => m is IPacksizeCutCreaseMachine).Cast<IPacksizeCutCreaseMachine>();
            var availableCorrugateIds = machinesInMachineGroup.SelectMany(m => m.Tracks.Select(t => t.LoadedCorrugate)).Where(c => c != null).Select(c => c.Id);

            SetCartonOnCorrugateForCartons(produciblesToProduce, availableCorrugateIds);

            produciblesToProduce.ForEach(
                p => p.Restrictions.Add(new CanProduceWithTileCountRestriction(produciblesToProduce.Count())));

            DecreaseSurgeCounterForGroups(produciblesToProduce);

            return AddProducibleToMachineQueueAndSubscribe(machineGroup.Id, produciblesToProduce);
        }

        /// <summary>
        /// Given the list of producibles find out if we have a printer of the proper type for the producibles assigned classification.  Then evaluate the classifications priority status.
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <param name="produciblesToFilter">List of producibles to evaluate.</param>
        /// <returns>Items that can be produced.</returns>
        public IEnumerable<IProducible> FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(MachineGroup machineGroup, IEnumerable<BoxFirstProducible> produciblesToFilter)
        {
            var printersInMachineGroup = machineGroup.ConfiguredMachines.Select(id => machineService.FindById(id)).Where(m => m is ILabelPrintingMachine).Cast<ILabelPrintingMachine>();
            var priority = printersInMachineGroup.Any(p => p.IsPriorityPrinter);
            var nonPriority = printersInMachineGroup.Any(p => !p.IsPriorityPrinter);
            var classifications = classificationService.Classifications.ToList();
            return produciblesToFilter.ToList().Where(p => CanProduceProducibleWithLabel(classifications, p, priority, nonPriority)).ToList();
        }

        private bool CanProduceProducibleWithLabel(List<Classification> classifications, BoxFirstProducible p, bool priority, bool nonPriority)
        {
            var result = false;
            Retry.Do(() => //This function continually throws System.InvalidOperationException: Collection was modified; enumeration operation may not execute. Retrying for now
            {
                UpdateLabelPriorityOnBoxFirstProducible(classifications, p);

                var labels = GetAllProducibleItems(p).OfType<Label>().ToList();
                if (!labels.Any())
                    result = true; // no labels to produce
                else
                {
                    var printRestriction =
                        labels.First()
                            .Restrictions.OfType<BasicRestriction<string>>()
                            .FirstOrDefault(r => r.Value == "PriorityLabel" || r.Value == "StandardLabel")
                            .Value;

                    if (string.IsNullOrEmpty(printRestriction))
                        result = priority || nonPriority; // no restrictions. print on any printer

                    result = printRestriction == "PriorityLabel" ? priority : nonPriority;
                }
            }, TimeSpan.FromMilliseconds(50), 5);
            return result;
        }

        public IEnumerable<IProducible> GetProduciblesWhereMachineGroupContainsMostOptimalCorrugate(MachineGroup machineGroup, IEnumerable<BoxFirstProducible> produciblesToFilter)
        {
            var machinesInMachineGroup = machineGroup.ConfiguredMachines.Select(id => machineService.FindById(id)).Where(m => m is IPacksizeCutCreaseMachine).Cast<IPacksizeCutCreaseMachine>().ToList();
            var availableCorrugateIds = machinesInMachineGroup.SelectMany(m => m.Tracks.Select(t => t.LoadedCorrugate)).Where(c => c != null).Select(c => c.Id).ToList();
            var tilableComparer = new BoxFirstCartonTilingEqualityComparer();
            var produciblesGroupedByTilingUniqueness = produciblesToFilter.GroupBy(p => p.Producible, tilableComparer).ToList();
            var filteredProducibles = new List<BoxFirstProducible>();
            foreach (var producibleGrouping in produciblesGroupedByTilingUniqueness)
            {
                if (
                    CanProduceMostOptimalCartonOnCorrugate(
                        produciblesToFilter.First(p => p.CustomerUniqueId == producibleGrouping.Key.CustomerUniqueId),
                        machinesInMachineGroup, availableCorrugateIds, producibleGrouping))
                    filteredProducibles.AddRange(producibleGrouping);
            }
            return filteredProducibles;
        }

        private bool CanProduceMostOptimalCartonOnCorrugate(BoxFirstProducible p, IEnumerable<IPacksizeCutCreaseMachine> availableMachines, IEnumerable<Guid> availableCorrugateIds, IEnumerable<IProducible> availableProducibles)
        {
            return GetTileCountToUse(p, availableMachines, availableCorrugateIds, availableProducibles) != 0;
        }

        public int GetTileCountToUse(Guid machineGroupId, BoxFirstProducible producible, IEnumerable<IProducible> availableProducibles)
        {
            var machineGroup = machineGroupService.FindByMachineGroupId(machineGroupId);
            var machinesInMachineGroup = machineGroup.ConfiguredMachines.Select(id => machineService.FindById(id)).Where(m => m is IPacksizeCutCreaseMachine).Cast<IPacksizeCutCreaseMachine>();
            var availableCorrugateIds = machinesInMachineGroup.SelectMany(m => m.Tracks.Select(t => t.LoadedCorrugate)).Where(c => c != null).Select(c => c.Id);
            return GetTileCountToUse(producible, machinesInMachineGroup, availableCorrugateIds, availableProducibles);
        }

        private int GetTileCountToUse(BoxFirstProducible p, IEnumerable<IPacksizeCutCreaseMachine> availableMachines, IEnumerable<Guid> availableCorrugateIds, IEnumerable<IProducible> availableProducibles)
        {
            var tilingPartners = p.PossibleTilingPartners.GetByStatus(NotInProductionProducibleStatuses.ProducibleStaged).ToList();
            var maxTileCount = p.CartonOnCorrugates.Any() ? p.CartonOnCorrugates.Select(coc => coc.TileCount).Max() : 0;
            var numberOfAvailableTiles = GetNumberOfAvailableTilingPartnersForProducible(availableProducibles, tilingPartners, maxTileCount);
            var mostOptimal = p.CartonOnCorrugates.FirstOrDefault(coc => coc.TileCount <= numberOfAvailableTiles);
            if (mostOptimal != null && CanProduceCartonOnCorrugate(mostOptimal, availableMachines, availableCorrugateIds))
                return mostOptimal.TileCount;

            if (mostOptimal != null)
            {
                var numerOfOddTiles = tilingPartners.Count % maxTileCount;
                var oddOptimal = p.CartonOnCorrugates.FirstOrDefault(coc => coc.TileCount <= numerOfOddTiles);
                if (oddOptimal != null && CanProduceCartonOnCorrugate(oddOptimal, availableMachines, availableCorrugateIds))
                    return oddOptimal.TileCount;
            }

            return 0;
        }

        private int GetNumberOfAvailableTilingPartnersForProducible(IEnumerable<IProducible> availableProducibles, IEnumerable<IProducible> tilingPartners, int maxTileCount)
        {
            var availableStagedCount = 1;
            foreach (var partner in tilingPartners)
            {
                if (availableProducibles.Contains(partner))
                {
                    availableStagedCount++;
                }
                if (availableStagedCount == maxTileCount)
                    break;
            }
            return availableStagedCount;
        }

        private bool CanProduceCartonOnCorrugate(CartonOnCorrugate coc, IEnumerable<IPacksizeCutCreaseMachine> availableMachines, IEnumerable<Guid> availableCorrugateIds)
        {
            if (coc != null)
                return availableMachines.Any(m => coc.ProducibleMachines.ContainsKey(m.Id) && m.Tracks.Any(t => t.LoadedCorrugate != null && t.LoadedCorrugate.Id == coc.Corrugate.Id));
            return false;
        }

        private void DecreaseSurgeCounterForGroups(IEnumerable<BoxFirstProducible> produciblesToUse)
        {
            foreach (var restriction in produciblesToUse.Select(producible => producible.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>()).Where(restriction => restriction != null))
            {
                cartonPropertyGroupService.DecreaseSurgeCounter(restriction.Alias);
            }
        }

        private void SetCartonOnCorrugateForCartons(IEnumerable<BoxFirstProducible> produciblesToUse, IEnumerable<Guid> availableCorrugateIds)
        {
            var tileCount = produciblesToUse.Count();
            produciblesToUse.ForEach(p => SetCartonOnCorrugateForCarton(p, availableCorrugateIds, tileCount));
        }

        private void SetCartonOnCorrugateForCarton(BoxFirstProducible producible, IEnumerable<Guid> availableCorrugateIds, int tileCount)
        {
            var cartons = GetAllProducibleItems(new[] { producible }).OfType<ICarton>();

            foreach (var carton in cartons)
            {
                var cartonOnCorrugate = producible.CartonOnCorrugates.FirstOrDefault(c => availableCorrugateIds.Contains(c.Corrugate.Id) && c.TileCount == tileCount);
                carton.CartonOnCorrugate = cartonOnCorrugate;
            }
        }

        protected virtual IProducible AddProducibleToMachineQueueAndSubscribe(Guid machineGroupId, IEnumerable<BoxFirstProducible> boxFirstProducibles)
        {
            var itemToProduce = new Kit();
            var produciblesForKit = GetAllProducibleItems(boxFirstProducibles);
            itemToProduce.ProducedOnMachineGroupId = machineGroupId;
            produciblesForKit.ForEach(p => p.ProducedOnMachineGroupId = machineGroupId);
            var tiledCarton = CreateTiledCarton(produciblesForKit.OfType<ICarton>());

            itemToProduce.AddProducible(tiledCarton);
            produciblesForKit.Where(p => p is ICarton == false).ForEach(itemToProduce.AddProducible);
            itemToProduce.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            itemToProduce.CustomerUniqueId = tiledCarton.CustomerUniqueId;
            boxFirstProducibles.ForEach(bf =>
            {
                bf.ProducedOnMachineGroupId = machineGroupId;
                bf.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleSelected;
            });

            IDisposable sentToProductionObs = null;
            sentToProductionObs = itemToProduce.ProducibleStatusObservable.DurableSubscribe(p =>
            {
                boxFirstProducibles.ForEach(bf =>
                {
                    logger.Log(LogLevel.Debug, "BFSA Status Watch {0} for {1}", p, string.Join(", ", produciblesForKit.Select((i) => i)));
                });

                if (p == ProducibleStatuses.ProducibleCompleted)
                {
                    sentToProductionObs.Dispose();
                    boxFirstProducibles.ForEach(bf =>
                    {
                        PublishItemProducedOnCPG(bf);
                        bf.ProducibleStatus = p;

                        logger.Log(LogLevel.Info, "Production of {0} has completed.", bf);
                        Persist(bf);
                    });
                }
                else if (p == ErrorProducibleStatuses.ProducibleFailed)
                {

                    sentToProductionObs.Dispose();
                    //update jobs for the label
                    boxFirstProducibles.ForEach(bf =>
                    {
                        CheckForLabelSentToPrinterAndCompleteIfSo(bf, ErrorProducibleStatuses.ProducibleFailed);
                        logger.Log(LogLevel.Info, "Production of {0} has failed.", bf);
                        Persist(bf);
                    });
                }
                else if (p == ProducibleStatuses.ProducibleRemoved)
                {
                    sentToProductionObs.Dispose();
                    boxFirstProducibles.ForEach(bf =>
                    {
                        CheckForLabelSentToPrinterAndCompleteIfSo(bf, ProducibleStatuses.ProducibleRemoved);

                        logger.Log(LogLevel.Info, "Production of {0} has been removed.", bf);
                        Persist(bf);
                    });
                }
                else if (p == ErrorProducibleStatuses.NotProducible)
                {
                    sentToProductionObs.Dispose();
                    boxFirstProducibles.ForEach(bf =>
                    {
                        bf.ProducibleStatus = p;
                        Persist(bf);
                    });
                }
                else//otherwise save the status.
                {
                    boxFirstProducibles.ForEach(bf =>
                    {
                        bf.ProducibleStatus = p;
                        Persist(bf);
                    });
                }

            }, logger);

            return itemToProduce;
        }

        private void CheckForLabelSentToPrinterAndCompleteIfSo(BoxFirstProducible bf, ProducibleStatuses notProducibleStatuses)
        {
            if (bf.ProducibleStatus == CartonPropertyGroupMessages.ProducibleCompleted)
                return;// we don't need to update the producible again to completed

            var labels = GetAllProducibleItems(bf).OfType<Label>().ToList();
            var labelPrinted = false;
            if (labels.Any())
            {
                labelPrinted = labels.All(
                    l =>
                        l.ProducibleStatus == ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff ||
                        l.ProducibleStatus == ZebraInProductionProducibleStatuses.ProducibleCompleted ||
                        (l.ProducibleStatus == notProducibleStatuses &&
                         RequireManualReproduction(l.History.Last().PreviousStatus)));
            }

            UpdateProducibleStatus(bf, labelPrinted ? ProducibleStatuses.ProducibleCompleted : notProducibleStatuses);
            if (labelPrinted)
            {
                logger.Log(LogLevel.Info, string.Format("Label for producible {0} was printed. Setting status to completed.", bf.CustomerUniqueId));
            }

            if (!labelPrinted)
            {
                bf.ProducedOnMachineGroupId = Guid.Empty;
                logger.Log(LogLevel.Info, string.Format("Label for producible {0} was not printed. Setting status to {1}.", bf.CustomerUniqueId, notProducibleStatuses));
                UpdateProducibleStatus(bf, NotInProductionProducibleStatuses.ProducibleStaged);
            }
        }

        private static bool RequireManualReproduction(ProducibleStatuses previousStatus)
        {
            return previousStatus == ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff ||
                   previousStatus == ZebraErrorProducibleStatus.FailedToStart ||
                   previousStatus == InProductionProducibleStatuses.ProducibleProductionStarted;
        }

        /// <summary>
        /// Change the labels restriction for priority to match that of the classification.
        /// </summary>
        /// <param name="allClassifications"></param>>
        /// <param name="p"></param>
        private void UpdateLabelPriorityOnBoxFirstProducible(List<Classification> allClassifications, BoxFirstProducible p)
        {
            var classification = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();
            if (classification == null)
                return;

            var currentClassificationStatus = allClassifications.FirstOrDefault(c => c.Alias == classification.Alias);
            if (currentClassificationStatus == null)
                return;

            var labelPriority = currentClassificationStatus.IsPriorityLabel ? "PriorityLabel" : "StandardLabel";
            var labels = GetAllProducibleItems(p).OfType<Label>();
            foreach (var label in labels.ToList())
            {
                var labelRestrictions = label.Restrictions
                    .OfType<BasicRestriction<string>>()
                    .Where(r => r.Value == "PriorityLabel" || r.Value == "StandardLabel");

                foreach (var labelRestriction in labelRestrictions.ToList())
                {
                    label.Restrictions.Remove(labelRestriction);
                }
                label.Restrictions.Add(new BasicRestriction<string>(labelPriority));
            }
        }

        private void PublishItemProducedOnCPG(BoxFirstProducible boxFirstProducible)
        {
            var cpgRestriction = boxFirstProducible.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>();
            if (cpgRestriction != null)
            {
                publisher.Publish(
                    new Message<string>()
                    {
                        MessageType = CartonPropertyGroupMessages.ProducibleCompleted,
                        Data = cpgRestriction.Alias
                    });
            }
        }

        private IEnumerable<IProducible> GetAllProducibleItems(IEnumerable<IProducible> selectedProducibles)
        {
            var producibleItems = new List<IProducible>();
            foreach (var producible in selectedProducibles)
            {
                var prod = producible;
                var wrapper = producible as IProducibleWrapper;
                if (wrapper != null)
                    prod = wrapper.Producible;

                var kit = prod as Kit;
                if (kit != null)
                    producibleItems.AddRange(GetAllProducibleItems(kit.ItemsToProduce));
                else
                    producibleItems.Add(prod);
            }

            return producibleItems;
        }

        private IEnumerable<IProducible> GetAllProducibleItems(IProducible producible)
        {
            return GetAllProducibleItems(new List<IProducible>() { producible });
        }

        private IProducible CreateTiledCarton(IEnumerable<ICarton> cartons)
        {
            if (cartons.Count() == 1)
                return cartons.First();

            var tiled = new TiledCarton();
            tiled.AddTiles(cartons);

            var cartonWithProducedOn = cartons.FirstOrDefault(c => c.ProducedOnMachineGroupId != Guid.Empty);
            tiled.ProducedOnMachineGroupId = cartonWithProducedOn != null ? cartonWithProducedOn.ProducedOnMachineGroupId : Guid.Empty;

            var customerId = string.Empty;
            tiled.Tiles.ForEach(t => customerId += t.CustomerUniqueId + " + ");
            tiled.CustomerUniqueId = customerId.ReplaceLast(" + ", string.Empty);
            return tiled;
        }

        public void AddProducibles(IEnumerable<IProducible> produciblesToAdd)
        {
            if (produciblesToAdd == null || !produciblesToAdd.Any())
                return;

            var boxFirstProducibles = produciblesToAdd.Cast<BoxFirstProducible>();
            Enqueue(boxFirstProducibles);
            Persist(boxFirstProducibles);
        }

        public void AddProduciblesButEnforceUniqueness(IEnumerable<IProducible> produciblesToAdd)
        {
            var sw = Stopwatch.StartNew();
            if (produciblesToAdd == null || !produciblesToAdd.Any())
                return;

            //handle reproduce
            var sw2 = Stopwatch.StartNew();
            var existingProducibleThatAreNotCompleteById = boxFirstRepository.Find(produciblesToAdd.Select(p => p.Id)).ToList(); //Handle update on reproduce call
            var reproduceProduciblesToCreate = produciblesToAdd.Except(existingProducibleThatAreNotCompleteById, new IdComparer<IProducible>()).ToList();
            boxFirstRepository.Update(existingProducibleThatAreNotCompleteById);
            Enqueue(existingProducibleThatAreNotCompleteById);
            logger.Log(LogLevel.Debug, "Took {0} ms to check for reproduce", sw2.ElapsedMilliseconds);
            sw2.Restart();

            //handle duplicate customer unique id
            var produciblesExistingByCustId = boxFirstRepository.FindByCustomerUniqueId(reproduceProduciblesToCreate.Select(p => p.CustomerUniqueId)).ToList();
            var existingCompletedByCustUniqueId = produciblesExistingByCustId.Where(p => p.ProducibleStatus == ProducibleStatuses.ProducibleCompleted).ToList();
            var existingByCustIdToUpdate = produciblesExistingByCustId.Except(existingCompletedByCustUniqueId, new CustomerUniqueIdComparer<BoxFirstProducible>()).ToList();
            boxFirstRepository.Update(existingByCustIdToUpdate);
            Enqueue(existingByCustIdToUpdate);
            logger.Log(LogLevel.Debug, "Took {0} ms to check for duplicate customer id", sw2.ElapsedMilliseconds);
            sw2.Restart();

            var produciblesToCreate = reproduceProduciblesToCreate.Except(produciblesExistingByCustId, new CustomerUniqueIdComparer<IProducible>()).Cast<BoxFirstProducible>().ToList();
            boxFirstRepository.Create(produciblesToCreate);
            Enqueue(produciblesToCreate);
            logger.Log(LogLevel.Debug, "Took {0} ms to create cartons", sw2.ElapsedMilliseconds);
            logger.Log(LogLevel.Debug, "Took {0} ms to add cartons and check uniqueness", sw.ElapsedMilliseconds);

            // Adjust the Producible Items Imported metric
            var produciblesOverwrittenOrSkipped = produciblesToAdd.Except(produciblesToCreate, new CustomerUniqueIdComparer<IProducible>()).ToList();

            if (produciblesOverwrittenOrSkipped.Count > 0)
            {
                var message = new ImportMessage<IEnumerable<IProducible>>
                {
                    MessageType = MessageTypes.DataOverwrittenOrSkipped,
                    Data = produciblesOverwrittenOrSkipped,
                    SelectionAlgorithmType = SelectionAlgorithmTypes.BoxLast
                };

                publisher.Publish(message);
            }
        }

        private void Enqueue(IEnumerable<BoxFirstProducible> produciblesToEnqueue)
        {
            var toRemove = producibles.Values.Where(p => p.ProducibleStatus == ProducibleStatuses.ProducibleCompleted).ToList();
            foreach (var boxFirstProducible in toRemove)
            {
                BoxFirstProducible output = null;
                producibles.TryRemove(boxFirstProducible.Id, out output);
            }
            logger.Log(LogLevel.Info, "BoxFirst Producibles removed: " + toRemove.Count() + " completed producibles");
            produciblesToEnqueue.ForEach(p =>
            {
                if (p.ProducibleStatus != NotInProductionProducibleStatuses.ProducibleImported)
                {
                    logger.Log(LogLevel.Debug, "Producible not in ProducibleImported but was sent to be enqueued: " + p);
                }
                if (!producibles.Values.Any(e => e.Id == p.Id || e.CustomerUniqueId == p.CustomerUniqueId))
                {
                    producibles.TryAdd(p.Id, p);
                }
                else
                {
                    logger.Log(LogLevel.Trace, "Producible already exists in the list but was sent to be enqueued: " + p);
                }
            });
            logger.Log(LogLevel.Info, "BoxFirst Producibles count: " + producibles.Count());
        }

        private void Persist(IEnumerable<BoxFirstProducible> produciblesToPersist)
        {
            var existingProducibles = boxFirstRepository.Find(produciblesToPersist.Select(p => p.Id));

            var produciblesToCreate = produciblesToPersist.Except(existingProducibles, new IdComparer<BoxFirstProducible>());
            var produciblesToUpdate = produciblesToPersist.Except(produciblesToCreate, new IdComparer<BoxFirstProducible>());

            if (produciblesToCreate.Any())
                boxFirstRepository.Create(produciblesToCreate);
            boxFirstRepository.Update(produciblesToUpdate);
        }

        private void Persist(BoxFirstProducible producible)
        {
            try
            {
                boxFirstRepository.Create(producible);
            }
            catch (MongoDuplicateKeyException)
            {
                boxFirstRepository.Update(producible);
            }
        }

        public IReadOnlyCollection<IProducible> GetProducibles(ProducibleStatuses status = null)
        {
            return producibles.Values.ToList();
        }
    }
}