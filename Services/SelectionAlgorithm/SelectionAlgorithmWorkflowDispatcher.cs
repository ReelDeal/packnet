﻿using System;
using System.Activities.Tracking;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

using PackNet.Common;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.SelectionAlgorithms;
using PackNet.Common.Interfaces.Services;



namespace PackNet.Services.SelectionAlgorithm
{
    /// <summary>
    /// Responsible for loading selection algorithm plugins and invoking the associated workflows
    /// </summary>
    public class SelectionAlgorithmWorkflowDispatcher
    {
        private readonly string pluginPath;
        private readonly IServiceLocator serviceLocator;
        private readonly IEnumerable<ISelectionAlgorithm> loadedSelectionAlgorithms;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private readonly IProductionGroupService productionGroupService;
        private readonly IUserNotificationService userNotificationService;
        private IWorkflowTrackingService workflowTrackingService;

        private readonly ConcurrentDictionary<string, Tuple<object/*lock*/ , ConcurrentQueue<IProducible>/*stage again after current run?*/>> stagingWorkflowsRunning = new ConcurrentDictionary<string, Tuple<object, ConcurrentQueue<IProducible>>>();
        public SelectionAlgorithmWorkflowDispatcher(string pluginPath,
            IServiceLocator serviceLocator,
            IProductionGroupService pgs,
            IEnumerable<ISelectionAlgorithm> loadedSelectionAlgorithms,
            IEventAggregatorPublisher publisher,
            ILogger logger)
        {
            productionGroupService = pgs;
            this.pluginPath = pluginPath;
            this.serviceLocator = serviceLocator;
            this.loadedSelectionAlgorithms = loadedSelectionAlgorithms;
            this.publisher = publisher;
            this.logger = logger;
            
            userNotificationService = serviceLocator.Locate<IUserNotificationService>();

            try
            {
                this.workflowTrackingService = serviceLocator.Locate<IWorkflowTrackingService>();
            }
            catch (Exception)
            {
                IDisposable subscription = null;
                subscription = serviceLocator.ServiceAddedObservable.Subscribe(s =>
                {
                    if (s is IWorkflowTrackingService)
                    {
                        this.workflowTrackingService = s as IWorkflowTrackingService;
                        subscription.Dispose();
                    }
                });
            }
        }

        public IEnumerable<ISelectionAlgorithm> SelectionAlgorithmStagingWorkFlows { get { return loadedSelectionAlgorithms; } }

        /// <summary>
        /// Dispatches the Importable to the corresponding workflow that will publish the output on the event aggregator.
        /// Only one staging workflow per selectionAlgorithm type will be running.  All others will be queued up.
        /// </summary>
        /// <param name="newProducibles"></param>
        /// <param name="selectionAlgorithmTypes"></param>
        /// <exception cref="Exception">Throws an Exception if the corresponding workflow for this Importable.ImportType cannot be found</exception>
        /// <remarks>
        /// The corresponding workflow is found by a case-insensitive match of the IWorkflowImport.Name to the importable.ImportType 
        /// </remarks>
        public void DispatchStagingWorkflow(IEnumerable<IProducible> newProducibles, SelectionAlgorithmTypes selectionAlgorithmTypes)
        {
            var locker = stagingWorkflowsRunning.GetOrAdd(selectionAlgorithmTypes.DisplayName, new Tuple<object, ConcurrentQueue<IProducible>>(new object(), new ConcurrentQueue<IProducible>()));

            if (Monitor.TryEnter(locker.Item1, 1))
            {
                try
                {
                    logger.Log(LogLevel.Debug, "Entering staging workflow lock for " + selectionAlgorithmTypes);

                    var algorithm =
                        loadedSelectionAlgorithms.SingleOrDefault(i => i.SelectionAlgorithmType == selectionAlgorithmTypes);
                    if (algorithm == null)
                    {
                        logger.Log(LogLevel.Error,
                            "Staging workflow for producible type '{0}' not found, cannot dispatch producibles, verify that the correct plugins are loaded",
                            selectionAlgorithmTypes);
                        return;
                    }
                    var workflowPath = Path.Combine(pluginPath, algorithm.StagingWorkflowName);
                    var assembly = algorithm.GetType().Assembly;
                    var inArgs = new Dictionary<string, object>
                    {
                        { "Producibles", newProducibles },
                        { "UniqueId", algorithm.UniqueId },
                        { "ServiceLocator", serviceLocator },
                        { "Publisher", publisher }
                    };

                    WorkflowLifetime lt = null;
                    if (this.workflowTrackingService != null)
                    {
                        var additionalInfo = new Dictionary<string, object>
                        {
                            { "ItemCount", newProducibles.Count() },
                            { "SelectionAlgorithmTypes", selectionAlgorithmTypes.DisplayName }
                        };
                        lt = this.workflowTrackingService.Factory(WorkflowTypes.StagingWorkflow, workflowPath, additionalInfo);
                    }
                    //invoke and wait otherwise locking will not work!
                    WorkflowHelper.InvokeWorkFlowAndWaitForResult(workflowPath, assembly, inArgs, userNotificationService, logger, lt);
                }
                finally
                {
                    logger.Log(LogLevel.Debug, "Exiting staging workflow lock for " + selectionAlgorithmTypes.DisplayName);
                    Monitor.Exit(locker.Item1);
                    if (locker.Item2.Any())
                    {
                        var queuedProducibles = new List<IProducible>();
                        IProducible p;
                        while (locker.Item2.TryDequeue(out p))
                            queuedProducibles.Add(p);

                        logger.Log(LogLevel.Debug, "{0} staging finished with one waiting to run with producible count {1}", selectionAlgorithmTypes.DisplayName, queuedProducibles.Count());
                        DispatchStagingWorkflow(queuedProducibles, selectionAlgorithmTypes);
                    }
                }

            }
            else
            {
                logger.Log(LogLevel.Debug, "{0} staging already running.  Queueing up producible count {1}",selectionAlgorithmTypes,newProducibles.Count());
                newProducibles.ForEach(locker.Item2.Enqueue);
                //staging is already running for this selection algorithm.  
            }

        }

        /// <summary>
        /// Dispatches the "Select Producible" workflow for a given machine group and producible type. Returns the producible that the machine group will create.
        /// </summary>
        /// <param name="machineGroup">machine group making the request</param>
        /// <param name="producibles"></param>
        /// <returns></returns>
        public IProducible DispatchSelectProducibleWorkflow(MachineGroup machineGroup, IEnumerable<IProducible> producibles = null)
        {
            var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(machineGroup.Id);
            if (productionGroup == null)
            {
                logger.Log(LogLevel.Warning,
                    "Production Group not found for MachineGroupId:{0}.  Please add machine to a Production Group.",
                    machineGroup.Id);
                return null;
            }

            var sa = (machineGroup.ProductionMode == MachineGroupProductionModes.ManualProductionMode
                ? SelectionAlgorithmTypes.Order
                : productionGroup.SelectionAlgorithm);
            var selectProducibleWorkFlow = loadedSelectionAlgorithms.FirstOrDefault(i => i.SelectionAlgorithmType == sa);
            if (selectProducibleWorkFlow == null)
            {
                logger.Log(LogLevel.Error,
                    "Select producible workflow for type {1} for MachineGroupId:{0} not found.  We cannot dispatch producibles, verify that the correct plugins are loaded.",
                    machineGroup.Id, sa);
                return null;
            }
            var workflowPath = Path.Combine(pluginPath, selectProducibleWorkFlow.SelectionWorkflowName);
            var importAssembly = selectProducibleWorkFlow.GetType().Assembly;
            var inArgs = new Dictionary<string, object> 
            { 
                { "MachineGroupId", machineGroup.Id},
                { "ServiceLocator", serviceLocator},
                { "Publisher", publisher }
            };
            logger.Log(LogLevel.Trace,
                    "Select producible workflow starting for type '{1}' for MachineGroupId:{0}.  Select producible workflow '{2}'",
                    machineGroup.Id, sa, workflowPath);

            WorkflowLifetime lt = null;
            if (this.workflowTrackingService != null)
            {
                var additionalInfo = new Dictionary<string, object>
                {
                    {"MachineGroup",machineGroup}
                };
                lt = this.workflowTrackingService.Factory(WorkflowTypes.SelectionWorkflow, workflowPath, additionalInfo);
            }

            var outputs = WorkflowHelper.InvokeWorkFlowAndWaitForResult(workflowPath, importAssembly, inArgs, userNotificationService, logger, lt);

            object selectedProducible = null;
            if (!outputs.TryGetValue("selectedProducible", out selectedProducible))
            {
                logger.Log(LogLevel.Info,
                    "Selection algorithm type '{0}' in ProductionGroupId:{3} ran but did not find a producible. Using workflow:'{1}' for MachineGroupId:{2}",
                    sa, workflowPath, machineGroup.Id, productionGroup.Id);
            }

            //Check carton state to make sure nothing else selected the carton
            var itemToProduce = selectedProducible as IProducible;
            if (itemToProduce == null)
            {
                logger.Log(LogLevel.Info,
                    "Selection algorithm type '{0}' in ProductionGroupId:{3} was null when returned from select producible workflow '{1}' for MachineGroupId:{2}",
                    sa, workflowPath, machineGroup.Id, productionGroup.Id);
                return null;
            }

            //Set the state of the producible so it won't get selected by another machine group
            var itemSelected = itemToProduce.DoStatusTransition(NotInProductionProducibleStatuses.ProducibleStaged,
                NotInProductionProducibleStatuses.ProducibleSelected);

            //Item was selected while in workflow, get a new item
            if (!itemSelected)
            {
                logger.Log(LogLevel.Info,
                    "Selection algorithm type '{0}' in ProductionGroupId:{4} returned {1} but the item was not in staged state. select producible workflow '{2}' for MachineGroupId:{3}",
                    sa, itemToProduce, workflowPath, machineGroup.Id, productionGroup.Id);
                return DispatchSelectProducibleWorkflow(machineGroup);
            }
            logger.Log(LogLevel.Info,
                    "Selection algorithm type '{0}' in ProductionGroupId:{4} returned {1} from select producible workflow '{2}' for MachineGroupId:{3}",
                    sa, itemToProduce, workflowPath, machineGroup.Id, productionGroup.Id);
            return itemToProduce;
        }
    }
}
