﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using MongoDB.Driver.Linq;

using Newtonsoft.Json.Linq;

using PackNet.Business.Orders;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Services.Properties;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;

namespace PackNet.Services.SelectionAlgorithm.Orders
{
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;

    /// <summary>
    /// Manages state for the Orders selection algorithm
    /// </summary>
    public class OrdersSelectionAlgorithmService : IOrdersSelectionAlgorithmService
    {
        private readonly IOrders orders;
        private readonly ICorrugateService corrugateService;
        private readonly IProductionGroupService productionGroupService;
        private readonly IMachineGroupService machineGroupService;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IUserNotificationService userNotificationService;
        private readonly IRestrictionResolverService restrictionResolverService;
        private readonly ILogger logger;
        private readonly ConcurrentDictionary<Guid /*Production group Id*/, ConcurrentList<Order>> orderQueues = new ConcurrentDictionary<Guid, ConcurrentList<Order>>();

        private readonly Subject<string> uiUpdateSubject = new Subject<string>();
        private IDisposable searchDisposable;
        private IViewModelService viewModelService;

        /// <summary>
        /// Keeps track of the last N orders that have been sent to the machine
        /// </summary>
        protected ConcurrentDictionary<Guid /*MachineGroupId*/, ConcurrentList<IProducible>> ordersSentToMachineGroup = new ConcurrentDictionary<Guid, ConcurrentList<IProducible>>();
        protected ConcurrentDictionary<Guid /*OrderId*/, Order> lastOrderSentToMachine = new ConcurrentDictionary<Guid, Order>();
        protected ConcurrentDictionary<Guid /*disposible*/, IDisposable> completingQueueMachineGroups = new ConcurrentDictionary<Guid, IDisposable>();
        private ConcurrentDictionary<Guid /*ProducibleId*/, IDisposable> producibleStatusesDisposables = new ConcurrentDictionary<Guid, IDisposable>();
        private ConcurrentDictionary<Guid /*mgId*/, HashSet<Guid> /*order id**/> mgAskedForJobAndFailed = new ConcurrentDictionary<Guid, HashSet<Guid>>();
        /// <summary>
        /// List of machineGroupIds that are working on an order
        /// </summary>
        protected ConcurrentDictionary<Guid /*OrderId*/, HashSet<Guid>> machineGroupsWorkingOnOrder = new ConcurrentDictionary<Guid, HashSet<Guid>>();

        private readonly List<IDisposable> observersToDispose = new List<IDisposable>();

        public string Name { get { return "OrdersSelectionAlgorithmService"; } }

        public OrdersSelectionAlgorithmService(
            IOrders orders,
            IProductionGroupService productionGroupService,
            IMachineGroupService machineGroupService,
            IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            IUserNotificationService userNotificationService,
            ICorrugateService corrugateService,
            ILogger logger)
        {
            this.orders = orders;
            this.corrugateService = corrugateService;
            this.productionGroupService = productionGroupService;
            this.machineGroupService = machineGroupService;
            this.publisher = publisher;
            this.uiCommunicationService = uiCommunicationService;
            this.userNotificationService = userNotificationService;
            this.logger = logger;
            this.restrictionResolverService = serviceLocator.Locate<IRestrictionResolverService>();
            serviceLocator.RegisterAsService<IOrdersSelectionAlgorithmService>(this);
            
            try
            {
                viewModelService = serviceLocator.Locate<IViewModelService>();
            }
            catch (Exception)
            {
                serviceLocator.ServiceAddedObservable.Subscribe((s) =>
                {
                    if (s is IViewModelService)
                        viewModelService = s as IViewModelService;
                });

            }

            observersToDispose.Add(
            ProductionGroupMessages.ProductionGroupChanged
                .OnMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>(subscriber, logger,
                    msg =>
                    {
                        logger.Log(LogLevel.Debug, "PG changed updating all orders {0}({1})", msg.Data.Alias, msg.Data.Id);
                        UpdateProductionGroup(msg.Data);
                    }));

            observersToDispose.Add(
                MachineGroupMessages.CapabilitiesChanged.OnMessage(subscriber, logger, msg =>
                {
                    logger.Log(LogLevel.Debug, "Capabilities changed for machine group: {0}({1}). Recalculate manual orders.", msg.MachineGroupName, msg.MachineGroupId);
                    UpdateOptimalCorrugateForOrdersInQueue(msg.MachineGroupId.Value, true);
                }));

            observersToDispose.Add(
                MachineMessages.AssignCorrugatesResponse.OnMessage(subscriber, logger, msg =>
                {
                    logger.Log(LogLevel.Debug, "Corrugates changed on machine group: {0}({1}). Recalculate orders for production group.", msg, msg.MachineGroupId);
                    var pg = productionGroupService.GetProductionGroupForMachineGroup(msg.MachineGroupId.Value);
                    UpdateProductionGroup(pg);
                }));

            //reproduce from operator panel
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Order>(OrderMessages.ReproduceCartonForOrder);
            OrderMessages.ReproduceCartonForOrder
                .OnMessage<Order>(subscriber, logger,
                    msg =>
                    {
                        if (SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(msg.MachineGroupId, productionGroupService, machineGroupService, SelectionAlgorithmTypes.Order))
                        {
                            logger.Log(LogLevel.Info, "{0} flagged to reproduce quanity {1} items for ", msg.Data,
                                msg.Data.ReproducedQuantity);

                            HandleReproduceOrderRequest(msg.Data);
                            CreateNext(new Message<List<Guid>>
                            {
                                Data = new List<Guid> { msg.Data.Id },
                                ReplyTo = msg.ReplyTo
                            });
                            UpdateUI(msg.ReplyTo);
                        }
                    });

            //Reproduce from search
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<Order>>(OrderMessages.ReproduceOrders);
            OrderMessages.ReproduceOrders
                .OnMessage<List<Order>>(subscriber, logger,
                    msg =>
                    {
                        if (SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(msg.MachineGroupId,
                            productionGroupService, machineGroupService, SelectionAlgorithmTypes.Order))
                        {
                            msg.Data.ForEach(order =>
                            {
                                logger.Log(LogLevel.Info, "{0} flagged to reproduce quanity {1} items for ", order, order.ReproducedQuantity);
                                HandleReproduceOrderRequest(order);
                                CreateNext(new Message<List<Guid>>
                                {
                                    Data = new List<Guid> { order.Id },
                                    ReplyTo = msg.ReplyTo
                                });
                            });
                            UpdateUI(msg.ReplyTo);
                        }
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(OrderMessages.DistributeOrders);
            OrderMessages.DistributeOrders
                .OnMessage<Guid>(subscriber, logger,
                    msg =>
                    {
                        var orderToDistribute = msg.Data;
                        var queueGuid = FindMachineGroupOrProductionGroupForOrder(orderToDistribute);
                        if (queueGuid == Guid.Empty)
                            return;

                        var orderQueueOrders = orderQueues[queueGuid];
                        var order = orderQueueOrders.FirstOrDefault(o => o.Id == orderToDistribute);
                        if (order == null)
                            return;

                        order.IsDistributable = !order.IsDistributable;
                        if (order.IsDistributable == false)
                        {
                            //Remove machines from list... probably need to pause machines as well
                            var machineGroupsAssignedToOrder = machineGroupsWorkingOnOrder.GetOrAdd(order.Id, new HashSet<Guid>());
                            foreach (var machineGroupId in machineGroupsAssignedToOrder)
                            {
                                machineGroupService.CompleteQueueAndPause(machineGroupId);
                            }
                            machineGroupsAssignedToOrder.Clear();
                        }
                        logger.Log(LogLevel.Info, "User '{0}' {2} {1}'", msg.UserName, order, order.IsDistributable ? "un-distributed" : "distributed");
                        orders.Update(order);
                        //orders.UpdateWithRetries(order); //partial fix for TFS 10256
                        SendOrders(msg.ReplyTo);
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<Guid>>(OrderMessages.UpdateOrdersPriority);
            OrderMessages.UpdateOrdersPriority
                .OnMessage<List<Guid>>(subscriber, logger, CreateNext);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<Guid>>(OrderMessages.CancelOrders);
            OrderMessages.CancelOrders
                .OnMessage<List<Guid>>(subscriber, logger,
                    msg =>
                    {
                        var ordersAndQueues = new Dictionary<Guid, List<Order>>();
                        //Find the queues that contain the orders
                        foreach (var orderQueue in orderQueues)
                        {
                            var ordersToDelete = orderQueue.Value.Where(o => msg.Data.Contains(o.Id));
                            if (ordersToDelete.Any())
                            {
                                ordersAndQueues.Add(orderQueue.Key, ordersToDelete.ToList());
                            }
                        }

                        //Update orders and remove from queue
                        foreach (var ordersAndQueue in ordersAndQueues)
                        {
                            var queueToDeleteFrom = orderQueues[ordersAndQueue.Key];
                            ordersAndQueue.Value.ForEach(order =>
                            {
                                order.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

                                order.ProducibleInProgress.ForEach(p => p.ProducibleStatus = ProducibleStatuses.ProducibleRemoved);

                                orders.Update(order);
                                //orders.UpdateWithRetries(order); //partial fix for TFS 10256
                                logger.Log(LogLevel.Info, "User '{0}' removed {1}", msg.UserName, order);
                                queueToDeleteFrom.Remove(order);
                            });
                        }

                        UpdateUI(msg.ReplyTo);
                    });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(OrderMessages.GetOrdersHistory);
            OrderMessages.GetOrdersHistory
                .OnMessage(subscriber, logger,
                //msg => SendOrdersHistory(msg.ReplyTo)
                    SendOrdersHistory
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(OrderMessages.GetOrders);
            OrderMessages.GetOrders
                .OnMessage(subscriber, logger,
                    SendOrders
                );

            //Manual carton creation from operator panel
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Carton>(CartonMessages.CreateCustomCarton);
            subscriber.GetEvent<IMessage<Carton>>()
                    .Where(m => m.MessageType == CartonMessages.CreateCustomCarton)
                    .ObserveOn(System.Reactive.Concurrency.Scheduler.Default)
                    .DurableSubscribe(
                        m =>
                        {
                            var machineGroupId = m.MachineGroupId.Value;

                            var order = new Order(m.Data, m.Data.Quantity)
                            {
                                OrderId = String.IsNullOrWhiteSpace(m.Data.CustomerUniqueId) ? Guid.NewGuid().ToString() : m.Data.CustomerUniqueId,
                            };
                            if (!order.Restrictions.Any(r => r is MachineGroupSpecificRestriction && ((MachineGroupSpecificRestriction)r).Value == machineGroupId))
                                order.Restrictions.Add(new MachineGroupSpecificRestriction(machineGroupId));

                            logger.Log(LogLevel.Info, "User '{0}' created {1} on machine group '{2}'", m.UserName, order, m.MachineGroupId);
                            //This will send the order through the staging workflow.
                            publisher.Publish(new ImportMessage<IEnumerable<IProducible>>
                            {
                                ImportType = ImportTypes.Order,
                                SelectionAlgorithmType = SelectionAlgorithmTypes.Order,
                                MachineGroupId = machineGroupId,
                                Data = new List<IProducible> { order }
                            });

                            uiCommunicationService.SendMessageToUI(new ResponseMessage
                            {
                                MessageType = CartonMessages.CustomCartonCreated,
                                Result = ResultTypes.Success,
                                ReplyTo = m.ReplyTo,
                            });
                        }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SearchProduciblesWithRestrictions>(SelectionAlgorithmMessages.SearchForProduciblesWithRestrictions);
            searchDisposable = subscriber.GetEvent<IMessage<SearchProduciblesWithRestrictions>>()
                .Where(m => m.Data.SelectionAlgorithmTypes == SelectionAlgorithmTypes.Order)
                .DurableSubscribe(m =>
                {
                    IEnumerable<Order> searchResult = new List<Order>();

                    try
                    {
                        if (m.Data.Restrictions.Any())
                        {
                            searchResult = orders.Search(m.Data.Criteria);
                            searchResult = FilterSearchResultOnRestrictions(searchResult, m.Data.Restrictions);
                        }
                        else
                        {
                            searchResult = orders.Search(m.Data.Criteria, Settings.Default.MaxOrdersSearchResults);
                        }
                    }
                    catch (Exception ex)
                    {

                        logger.Log(LogLevel.Error, ex.Message);
                    }
           
                    uiCommunicationService.SendMessageToUI(new Message<IEnumerable<Order>>
                    {
                        MessageType = OrderMessages.SearchOrdersResponse,
                        ReplyTo = m.ReplyTo,
                        Data = searchResult
                    });
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SearchProducibles>(SelectionAlgorithmMessages.SearchForProducibles);
            observersToDispose.Add(subscriber.GetEvent<IMessage<SearchProducibles>>()
                .Where(m => m.Data.SelectionAlgorithmTypes == SelectionAlgorithmTypes.Order)
                .DurableSubscribe(m =>
                {
                    IEnumerable<Order> searchResult = new List<Order>();

                    try
                    {
                        searchResult = orders.Search(m.Data.Criteria, Settings.Default.MaxOrdersSearchResults);
                    }
                    catch (Exception ex)
                    {
                        logger.Log(LogLevel.Error, ex.Message);
                    }

                    uiCommunicationService.SendMessageToUI(new Message<IEnumerable<Order>>
                    {
                        MessageType = OrderMessages.SearchOrdersResponse,
                        ReplyTo = m.ReplyTo,
                        Data = searchResult
                    });
                }, logger));

            //Read data from db            
            orders.GetAllOpenOrders().ForEach(
                o =>
                {
                    if (o.ProducibleInProgress.Any())
                    {
                        o.ProducibleInProgress.ToArray().ForEach(p => o.ProducibleInProgress.Remove(p));
                        orders.Update(o);
                        //orders.UpdateWithRetries(o); //partial fix for TFS 10256
                    }


                    var restriction =
                        o.Restrictions.Where(r => r is ProductionGroupSpecificRestriction || r is MachineGroupSpecificRestriction)
                            .Cast<BasicRestriction<Guid>>()
                            .FirstOrDefault();
                    if (restriction != null)
                    {
                        var id = restriction.Value;
                        if (o.ProducibleStatus == ErrorProducibleStatuses.NotProducible)
                            ResetOrderForStaging(o);

                        AddOptimalCorrugateIfContainsCarton(o, restriction.Value, o.Restrictions.Any(r => r is ProductionGroupSpecificRestriction) == false);
                        AddOrderToQueue(id, o);
                    }

                    //TODO: invalid order
                });

            uiUpdateSubject.Sample(TimeSpan.FromMilliseconds(500)).Subscribe(replyTo =>
            {
                SendOrders(replyTo);
                SendOrdersHistory(replyTo);
            });
            serviceLocator.RegisterAsService<IOrdersSelectionAlgorithmService>(this);
        }

        private void ResetOrderForStaging(Order order)
        {
            order.Failed.Clear();
            order.DoStatusTransition(order.ProducibleStatus, NotInProductionProducibleStatuses.ProducibleStaged);
            this.orders.Update(order);
        }

        private void UpdateProductionGroup(ProductionGroup pgToUpdate)
        {
            var notProducibleOrders = this.orders.GetAllNotProducibleOrders().ToList();
            foreach (var order in notProducibleOrders)
            {
                ResetOrderForStaging(order);
            }

            //update production group orders
            UpdateOptimalCorrugateForOrdersInQueue(pgToUpdate.Id, false);

            //update machine group orders that are part of this production group.
            var machineGroupIdsInPg = productionGroupService.ProductionGroups.First(pg => pg.Id == pgToUpdate.Id).ConfiguredMachineGroups;
            machineGroupIdsInPg.ForEach(mg => UpdateOptimalCorrugateForOrdersInQueue(mg, true));

            if (notProducibleOrders.Any())
            {
                logger.Log(LogLevel.Debug, "PG changed restaging every Not Producible order");
                var message = new ImportMessage<IEnumerable<IProducible>>()
                {
                    SelectionAlgorithmType = SelectionAlgorithmTypes.Order,
                    Data = notProducibleOrders,
                    MessageType = MessageTypes.Restaged
                };
                publisher.Publish(message);
            }
            else // if there were any nonproducible orders that should be restaged then the ui will be updated when they are added to queue
            {
                UpdateUI();
            }
        }

        private IEnumerable<Order> FilterSearchResultOnRestrictions(
        IEnumerable<Order> searchResult,
        Dictionary<string, Guid> restrictions)
        {
            if (restrictions == null || !restrictions.Keys.Any())
                return searchResult;

            if (restrictions.ContainsKey("Value"))
            {
                searchResult =
                    searchResult.Where(
                        p =>

                            p.Restrictions.GetRestrictionOfType<ProductionGroupSpecificRestriction>().Value ==
                            restrictions["Value"]).Take(Settings.Default.MaxOrdersSearchResults).ToList();
            }
           
            return searchResult;
        }

        private void UpdateOptimalCorrugateForOrdersInQueue(Guid queueId, bool isMachineGroupSpecific)
        {
            if (mgAskedForJobAndFailed.ContainsKey(queueId))
            {
                lock (mgAskedForJobAndFailed[queueId])
                {
                    var removedItems = new HashSet<Guid>();
                    mgAskedForJobAndFailed.TryRemove(queueId, out removedItems);
                }
            }

            var mgQ = this.GetQueue(queueId);
            mgQ.ForEach(
                o =>
                {
                    this.AddOptimalCorrugateIfContainsCarton(o, queueId, isMachineGroupSpecific);
                    if (OrderContainsAnyNonProducibleCarton(o))
                    {
                        SetOrderAndProducibleStatusesToNotProducible(o);
                        mgQ.Remove(o);
                    }
                    orders.Update(o);
                });
        }
        private void CreateNext(IMessage<List<Guid>> msg)
        {
            IEnumerable<Guid> ordersToCreateNext = msg.Data;
            var orderCtr = ordersToCreateNext.Count() + 1;

            //Reverse because the UI is sending multiple orders. Start with the last and move to front of queue
            ordersToCreateNext.Reverse().ForEach(orderId =>
            {
                var queueGuid = FindMachineGroupOrProductionGroupForOrder(orderId);
                if (queueGuid == Guid.Empty)
                {
                    return;
                }

                var orderQueueOrders = orderQueues[queueGuid];
                var order = orderQueueOrders.FirstOrDefault(o => o.Id == orderId);
                if (order == null)
                {
                    return;
                }
                orderQueueOrders.Remove(order);
                orderQueueOrders.Insert(0, order);
                logger.Log(LogLevel.Info, "User '{0}' updated priority for {1} - Order moved to the top of the queue", msg.UserName, order);

                orderQueueOrders.ForEach(o =>
                {
                    if (o.OrderId == order.OrderId)
                    {
                        o.ProductionSequence = orderCtr * -1;
                        orderCtr = orderCtr - 1;
                        orders.Update(o);
                    }
                    //orders.UpdateWithRetries(o); //partial fix for TFS 10256
                });
            });

            SendOrders(msg.ReplyTo);
        }

        private void HandleReproduceOrderRequest(Order order)
        {
            var queueGuid = FindMachineGroupOrProductionGroupForOrder(order.Id);
            if (queueGuid != Guid.Empty)
            {
                var orderQueueOrders = orderQueues[queueGuid];
                var orderFromQueue = orderQueueOrders.Single(o => o.Id == order.Id);
                orderFromQueue.ReproducedQuantity += order.ReproducedQuantity;
                orders.Update(orderFromQueue);
            }
            else
            {
                var orderFromDb = orders.Find(order.Id);
                orderFromDb.ReproducedQuantity += order.ReproducedQuantity;
                var restriction = orderFromDb.Restrictions.FirstOrDefault(
                    r => r is MachineGroupSpecificRestriction || r is ProductionGroupSpecificRestriction);
                if (restriction == null)
                {
                    // TODO: this is way edge case... how do we handle it if it doesn't exist in the db
                }
                var machineGroupSpecificRestriction = restriction as MachineGroupSpecificRestriction;
                if (machineGroupSpecificRestriction != null)
                {
                    AddOrderToMachineGroupQueue(machineGroupSpecificRestriction.Value, orderFromDb);
                    //Trigger selection for machine group.
                    publisher.Publish(new Message<Guid>
                    {
                        MessageType = MachineGroupMessages.GetWorkForMachineGroup,
                        MachineGroupId = machineGroupSpecificRestriction.Value
                    });
                }
                var productionGroupRestriction = restriction as ProductionGroupSpecificRestriction;
                if (productionGroupRestriction != null)
                {
                    AddOrderToProductionGroupQueue(productionGroupRestriction.Value, orderFromDb);
                    //Trigger production group to select work
                    publisher.Publish(new Message<Guid>
                    {
                        MessageType = ProductionGroupMessages.GetWorkForProductionGroup,
                        Data = productionGroupRestriction.Value
                    });
                }
            }
        }

        private void UpdateUI(string replyTo = null)
        {
            //Don't pound the UI with messages ever
            //limit it to once every half second in the subscription
            uiUpdateSubject.OnNext(replyTo);
        }

        private void SendOrdersHistory(IMessage obj)
        {
            if (SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(obj.MachineGroupId, productionGroupService, machineGroupService, SelectionAlgorithmTypes.Order))
                SendOrdersHistory(obj.ReplyTo, obj.MachineGroupId);
        }

        private void SendOrdersHistory(string replyTo = null, Guid? machineGroupId = null)
        {
            if (machineGroupId.HasValue)
            {
                var mg = machineGroupService.FindByMachineGroupId(machineGroupId.Value);
                var pg = productionGroupService.GetProductionGroupForMachineGroup(machineGroupId.Value);
                uiCommunicationService.SendMessageToUI(new Message<List<Order>>
                {
                    MessageType = OrderMessages.OrdersHistory,
                    ReplyTo = replyTo,
                    Data = orders.GetCompletedOrdersFor(pg, mg).ToList()
                }, false, true);
            }
            else
            {
                uiCommunicationService.SendMessageToUI(new Message<List<Order>>
                {
                    MessageType = OrderMessages.OrdersHistory,
                    ReplyTo = replyTo,
                    Data = orders.GetCompletedOrders().ToList()
                }, false, true);
            }
        }

        private void SendOrders(IMessage msg)
        {
            if (SelectionAlgorithmHelper.MachineGroupUsesSelectionAlgorithm(msg.MachineGroupId, productionGroupService, machineGroupService, SelectionAlgorithmTypes.Order))
                SendOrders(msg.ReplyTo);
        }

        private void SendOrders(string replyTo = null, Guid? machineGroupId = null)
        {
            if (machineGroupId.HasValue)
            {
                var mg = machineGroupService.FindByMachineGroupId(machineGroupId.Value);
                var pg = productionGroupService.GetProductionGroupForMachineGroup(machineGroupId.Value);

                uiCommunicationService.SendMessageToUI(new Message<List<Order>>
                {
                    MessageType = OrderMessages.OrdersHistory,
                    ReplyTo = replyTo,
                    Data = orders.GetOpenOrdersFor(pg, mg).ToList()
                }, false, true);
            }
            else
            {
                uiCommunicationService.SendMessageToUI(new Message<List<Order>>
                {
                    MessageType = OrderMessages.Orders,
                    ReplyTo = replyTo,
                    Data = orders.GetOpenOrders().ToList()
                }, false, true);
            }
        }

        public void Dispose()
        {
            observersToDispose.ForEach(o => o.Dispose());
        }

        /// <summary>
        /// This is a staging call for manual cartons
        /// </summary>
        /// <param name="machineGroupId"></param>
        /// <param name="order"></param>
        public void AddOrderToMachineGroupQueue(Guid machineGroupId, Order order)
        {  //This method is used in import workflow

            AddOptimalCorrugateIfContainsCarton(order, machineGroupId, true);

            if (!order.Restrictions.Any(r => r is MachineGroupSpecificRestriction && ((MachineGroupSpecificRestriction)r).Value == machineGroupId))
                order.Restrictions.Add(new MachineGroupSpecificRestriction(machineGroupId));

            var flatOrder = FlattenOrder(order);

            AddOrderToQueue(machineGroupId, flatOrder);
        }

        /// <summary>
        /// This is a staging call for auto cartons
        /// </summary>
        /// <param name="productionGroupId"></param>
        /// <param name="order"></param>
        public void AddOrderToProductionGroupQueue(Guid productionGroupId, Order order)
        {
            AddOptimalCorrugateIfContainsCarton(order, productionGroupId);
            var productionGroup = productionGroupService.Find(productionGroupId);
            if (productionGroup != null)
            {
                order.IsDistributable = productionGroup.OrdersConfiguration().DefaultDistributeValue;
            }

            var flatOrder = FlattenOrder(order);

            AddOrderToQueue(productionGroupId, flatOrder);
        }

        private Order FlattenOrder(Order order)
        {
            if (order.Producible as Kit != null && (order.Producible as Kit).ItemsToProduce.All(p => p.ProducibleType == ProducibleTypes.Order))
            {
                var kit = MongoDbHelpers.DeepCopy(order.Producible as Kit);

                (order.Producible as Kit).ItemsToProduce.Clear();

                var ordersInKit = kit.ItemsToProduce.OfType<Order>();

                ordersInKit.ForEach(o =>
                {
                    var remaining = o.RemainingQuantity;
                    var carton = o.Producible as ICarton;
                    if (carton != null)
                    {
                        var copies = Enumerable.Range(0, remaining).Select(c => MongoDbHelpers.DeepCopy(carton)).ToList();

                        foreach (var copy in copies)
                        {
                            copy.Id = Guid.NewGuid();
                            (order.Producible as Kit).AddProducible(copy);
                        }
                    }
                    else
                    {
                        var label = o.Producible as IPrintable;
                        if (label != null)
                        {
                            for (int i = 0; i < remaining; i++)
                            {
                                (order.Producible as Kit).AddProducible(label);
                            }
                        }
                    }
                });

                return order;
            }

            return order;
        }

        private void WaitForMachineGroupToPauseToRemoveLastOrder(Guid machineGroupId)
        {
            if (!completingQueueMachineGroups.ContainsKey(machineGroupId))
            {
                IDisposable subscription = null;
                subscription =
                    machineGroupService.FindByMachineGroupId(machineGroupId)
                        .CurrentStatusChangedObservable.DurableSubscribe(status =>
                        {
                            //only clear out machines last job when the machine has completed the queue and been set to paused.
                            if (status != MachineGroupAvailableStatuses.ProduceQueueAndPause)
                            {
                                //clear out last job
                                lastOrderSentToMachine.AddOrUpdate(machineGroupId, (Order)null,
                                    (guid, o) => null);
                                subscription.Dispose();
                                completingQueueMachineGroups.TryRemove(machineGroupId, out subscription);
                            }
                        });
                //try to add it to our list if it fails then someone already beat us to adding it.
                if (!completingQueueMachineGroups.TryAdd(machineGroupId, subscription))
                    subscription.Dispose();
            }
        }

        public IProducible GetAutoJobForMachineGroup(Guid machineGroupId)
        {
            var machineGroup = machineGroupService.FindByMachineGroupId(machineGroupId);
            if (machineGroup.CurrentStatus == MachineGroupAvailableStatuses.ProduceCurrentAndPause)
            {
                return null;
            }

            var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(machineGroupId);

            if (productionGroup == null)
            {
                logger.Log(LogLevel.Info, "Production Group for Machine Group '{0}' can't be found in ProductionGroupService... No job will be sent to machine group");
                return null;
            }

            var orderAndProducible = GetNextOrderAndProducibleInQueue(productionGroup.Id, machineGroupId);

            if (orderAndProducible == null)
            {
                return null;
            }

            //Handle pause between orders
            Order lastOrder;
            lastOrderSentToMachine.TryGetValue(machineGroupId, out lastOrder);
            if (lastOrder != null && lastOrder.OrderId != orderAndProducible.Item1.OrderId)
            {
                //Switching orders... Should we go into pause?
                if (productionGroup.OrdersConfiguration().PauseMachineBetweenOrders)
                {
                    orderAndProducible.Item2.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                    machineGroupService.CompleteQueueAndPause(machineGroupId);
                    WaitForMachineGroupToPauseToRemoveLastOrder(machineGroupId);
                    return null; //Starve the queue
                }
            }

            lastOrderSentToMachine.AddOrUpdate(machineGroupId, orderAndProducible.Item1, (guid, order) => orderAndProducible.Item1);
            logger.Log(LogLevel.Info, "Orders selecting producible with Id '{0}' for order '{1}'", orderAndProducible.Item2.Id, orderAndProducible.Item1.OrderId);
            return orderAndProducible.Item2;
        }


        public IProducible GetManualJobForMachineGroup(Guid machineGroupId)
        {
            var machineGroup = machineGroupService.FindByMachineGroupId(machineGroupId);
            if (machineGroup.CurrentStatus == MachineGroupAvailableStatuses.ProduceQueueAndPause
                || machineGroup.CurrentStatus == MachineGroupAvailableStatuses.ProduceCurrentAndPause)
            {
                return null;
            }

            var orderAndProducible = GetNextOrderAndProducibleInQueue(machineGroupId, machineGroupId);

            if (orderAndProducible == null)
            {
                //if we don't have a next job then doen't make the machine go into pause the next time a job is added.
                lastOrderSentToMachine.AddOrUpdate(machineGroupId, (Order)null, (guid, o) => null);
                return null;
            }

            Order lastOrder;
            lastOrderSentToMachine.TryGetValue(machineGroupId, out lastOrder);
            if (lastOrder != null && lastOrder.Restrictions.Any(r => r is MachineGroupSpecificRestriction) && lastOrder.OrderId != orderAndProducible.Item1.OrderId)
            {
                //Switching orders... Pause
                logger.Log(LogLevel.Debug, "Switching orders... Not dispatching {0} to starve queue", orderAndProducible.Item2);
                orderAndProducible.Item2.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                machineGroupService.CompleteQueueAndPause(machineGroupId);
                WaitForMachineGroupToPauseToRemoveLastOrder(machineGroupId);
                return null; //Starve the queue
            }
            lastOrderSentToMachine.AddOrUpdate(machineGroupId, orderAndProducible.Item1, (guid, order) => orderAndProducible.Item1);

            logger.Log(LogLevel.Info, "Orders selecting producible {0} for {1}", orderAndProducible.Item2, orderAndProducible.Item1);
            return orderAndProducible.Item2;
        }

        private Tuple<Order, IProducible> GetNextOrderAndProducibleInQueue(Guid queueId, Guid machineGroupId)
        {
            var queue = GetQueue(queueId);
            foreach (var mainOrder in queue)
            {
                if (mainOrder == null)
                {
                    break;
                }

                if (mainOrder.RemainingQuantity == 0 || mainOrder.ProducibleStatus == ErrorProducibleStatuses.NotProducible)
                {
                    continue;
                }

                var mg = machineGroupService.FindByMachineGroupId(machineGroupId);
                var failedRestrictions = mainOrder.Producible.Restrictions.Where(r => !restrictionResolverService.Resolve(r, mg.AllMachineCapabilities)).ToList();

                if (failedRestrictions.Any())
                {
                    var failedCorrugates = failedRestrictions.Where(
                                        r =>
                    r is MustProduceWithOptimalCorrugateRestriction
                    || (r is BasicRestriction<string> && ((BasicRestriction<string>)r).Value == "CartonOnCorrugate")).ToList();
  
                    if (!failedCorrugates.Any())
                    {
                        MarkOrderAsFailedForMachineGroupAndNotify(machineGroupId, mainOrder, failedRestrictions, mg);
                    }
                    continue;
                }

                var machineGroupsAssignedToOrder = machineGroupsWorkingOnOrder.GetOrAdd(mainOrder.Id, new HashSet<Guid>());
                lock (machineGroupsAssignedToOrder) //Only one machine can add itself to the machines working orders hashset at a time
                {
                    var allowTilingBetweenKits = false;
                    //make sure the order is still valid

                    if (mainOrder.RemainingQuantity > 0 &&
                        (mainOrder.IsDistributable
                        || machineGroupsAssignedToOrder.Any(mgId => mgId == machineGroupId) //Is my machine group already working on this order?
                        || !machineGroupsAssignedToOrder.Any() //No machine is working on this order
                        || (machineGroupsAssignedToOrder.All(
                            mgId =>
                                this.machineGroupService.FindByMachineGroupId(mgId) != null
                                && this.machineGroupService.FindByMachineGroupId(mgId).CurrentStatus
                                == MachineGroupUnavailableStatuses.MachineGroupOffline))))
                    {
                        Kit kitToProduce = null;
                        IProducible producible = null;
                        if (mainOrder.Producible is Kit)
                        {
                            kitToProduce = mainOrder.Producible as Kit;
                            allowTilingBetweenKits = kitToProduce.ItemsToProduce.OfType<ICarton>().Count() == 1;
                            producible = this.PrepareKitFromProduciblesInOrder(
                                kitToProduce.ItemsToProduce,
                                mainOrder,
                                allowTilingBetweenKits);
                            if (producible == null)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            producible = this.CreateProducibleAsSingleOrTiledCarton(mainOrder);
                        }

                        producible.ProducedOnMachineGroupId = machineGroupId;
                        mainOrder.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;

                        this.AddProducibleToOrderAndMachineGroup(machineGroupId, mainOrder, producible, allowTilingBetweenKits);

                        this.orders.Update(mainOrder);

                        this.SetupProducibleStatusObservable(machineGroupId, producible, mainOrder, queue);

                        this.logger.Log(LogLevel.Debug, "Orders: Created subscription for producible {0}", producible);

                        machineGroupsAssignedToOrder.Add(machineGroupId);

                        //TODO This should be a derived member since it's basically a reflection of the occured time of the last history item
                        mainOrder.StatusChangedDateTime = DateTime.UtcNow;
                        return new Tuple<Order, IProducible>(mainOrder, producible);
                    }
                }
            }
            return null;
        }

        private void MarkOrderAsFailedForMachineGroupAndNotify(
            Guid machineGroupId,
            Order mOrder,
            IEnumerable<IRestriction> failedRestrictions,
            MachineGroup mg)
        {
            var askedForJobAndFailed = this.mgAskedForJobAndFailed.GetOrAdd(machineGroupId, new HashSet<Guid>());

            lock (askedForJobAndFailed)
            {
                var notify = askedForJobAndFailed.All(g => g != mOrder.Id);
                if (notify)
                {
                    askedForJobAndFailed.Add(mOrder.Id);

                    var failureMessage =
                        failedRestrictions.Select(
                            failure =>
                                failure.ToString().Contains("Zebra")
                                    ? "No suitable printer found for MachineGroup"
                                    : failure.ToString().TrimEnd()).ToList();

                    var message = string.Format(
                        "Order Selection skipped {0} because Restrictions ({1}) were not satisfied by MachineGroup",
                        mOrder.OrderId,
                        string.Join(", ", failureMessage));

                    this.userNotificationService.SendNotificationToMachineGroup(
                        NotificationSeverity.Warning,
                        message,
                        string.Empty,
                        machineGroupId);
                    this.logger.Log(LogLevel.Info, message + mg);
                }
            }
        }

        private Kit PrepareKitFromProduciblesInOrder(ConcurrentList<IProducible> producibles, Order mainOrder, bool allowTilingBetweenKits)
        {
            if (!producibles.Any())
                return null;

            var returnKit = new Kit();

            var cartonsInKit = producibles.OfType<ICarton>();

            var labelsInKit = producibles.OfType<IPrintable>();
            var groupedCartonsInKit = GroupByTilableCartons(cartonsInKit);

            var cartons = PrepareAndGetCartonsFromKit(mainOrder, groupedCartonsInKit, allowTilingBetweenKits);
            cartons.ForEach(returnKit.AddProducible);

            if (allowTilingBetweenKits)
                AddLabelsForTiles(mainOrder, labelsInKit, returnKit);
            else
            {
                var labels = PrepareAndGetLabelsFromKit(mainOrder, labelsInKit.ToList());
                labels.ForEach(returnKit.AddProducible);
            }

            returnKit.CustomerUniqueId = mainOrder.OrderId;
            returnKit.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            return returnKit;
        }

        private void AddLabelsForTiles(Order mainOrder, IEnumerable<IPrintable> labelsInKit, Kit returnKit)
        {
            var kitInMain = mainOrder.Producible as Kit;
            if (kitInMain != null)
            {
                var cartonsInMain = kitInMain.ItemsToProduce.OfType<ICarton>();
                var cartonsToReturn = returnKit.ItemsToProduce.OfType<ICarton>();
                if (cartonsInMain.Count() == 1)
                {
                    var tiled = cartonsToReturn.OfType<TiledCarton>().FirstOrDefault();
                    var labelsList = new List<IPrintable>();
                    if (tiled != null)
                    {
                        for (int i = 0; i < tiled.Tiles.Count(); i++)
                        {
                            labelsList.AddRange(labelsInKit.Select(MongoDbHelpers.DeepCopy<IPrintable>).ToList());
                        }
                    }
                    else
                    {
                        labelsList.AddRange(labelsInKit.ToList());
                    }

                    var labels = PrepareAndGetLabelsFromKit(mainOrder, labelsList);
                    labels.ForEach(returnKit.AddProducible);
                }
            }
        }
        private IEnumerable<IProducible> PrepareAndGetLabelsFromKit(Order mainOrder, List<IPrintable> labels)
        {
            var producibles = new List<IProducible>();
            for (int i = 0; i < labels.Count; i++)
            {
                var prod = PrepareForProduction(labels[i]);
                prod.CustomerUniqueId = string.Format("{0}:{1}:{2}:{3}", mainOrder.OrderId, prod.CustomerUniqueId, mainOrder.RemainingQuantity, i + 1);
                producibles.Add(prod);

            }

            return producibles;
        }

        private IEnumerable<IProducible> PrepareAndGetCartonsFromKit(Order mainOrder, IEnumerable<IEnumerable<ICarton>> groupedCartons, bool allowTilingBetweenKits)
        {
            var producibles = new List<IProducible>();
            foreach (var group in groupedCartons)
            {
                var cartons = group.ToList();
                var carton = group.First();

                var remaining = allowTilingBetweenKits ? mainOrder.RemainingQuantity : cartons.Count;
                var tileCountToUse = Math.Min(remaining, carton.CartonOnCorrugate.TileCount);

                if (allowTilingBetweenKits)
                {
                    for (int i = 1; i < tileCountToUse; i++)
                    {
                        cartons.Add(MongoDbHelpers.DeepCopy(carton));
                    }
                }

                while (tileCountToUse > 1)
                {
                    var numberOfTilesInTheGroup = allowTilingBetweenKits ? 1 : remaining / tileCountToUse;

                    for (var i = 0; i < numberOfTilesInTheGroup; i++)
                    {
                        var tiledCarton = GetTiledCarton(mainOrder, tileCountToUse, cartons, remaining);

                        remaining -= tiledCarton.Tiles.Count();

                        tileCountToUse = Math.Min(remaining, carton.CartonOnCorrugate.TileCount);
                        tiledCarton.Id = Guid.NewGuid();
                        tiledCarton.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
                        producibles.Add(tiledCarton);

                        if (allowTilingBetweenKits)
                            return producibles;

                    }
                }

                if (tileCountToUse <= 0 || remaining <= 0)
                {
                    continue;
                }

                // untiled cartons
                var count = 1;
                cartons.Skip(cartons.Count() - remaining).ForEach(c =>
                {
                    var prod = PrepareForProduction(c);
                    prod.CustomerUniqueId = string.Format("{0}:{1}:{2}:{3}", mainOrder.OrderId, prod.CustomerUniqueId, mainOrder.RemainingQuantity, count);
                    producibles.Add(prod);
                    count++;
                });
            }
            return producibles;
        }

        private static TiledCarton GetTiledCarton(Order mainOrder, int tileCountToUse, IEnumerable<ICarton> cartons, int remaining)
        {
            var tiledCarton = new TiledCarton();

            var tiles = cartons.Skip(cartons.Count() - remaining).Take(tileCountToUse).Select(t => PrepareForProduction(t) as ICarton);

            tiledCarton.AddTiles(tiles);

            tiledCarton.CustomerUniqueId = mainOrder.OrderId;

            tiledCarton.CustomerUniqueId += ":" + tiles.First().CustomerUniqueId + ":" + remaining;

            for (var j = remaining - 1; j > remaining - tileCountToUse; j--)
            {
                tiledCarton.CustomerUniqueId += "+" + j;
            }

            return tiledCarton;
        }

        private static IProducible PrepareForProduction(IProducible producible)
        {
            var p = MongoDbHelpers.DeepCopy(producible);

            p.Id = Guid.NewGuid();
            p.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;

            IDisposable disposable = null;

            disposable = p.ProducibleStatusObservable.DurableSubscribe(ps =>
            {
                // these statuses are set from the kit
                if (ps == ProducibleStatuses.ProducibleCompleted || ps == ProducibleStatuses.ProducibleRemoved
                    || ps == ErrorProducibleStatuses.ProducibleFailed || ps == ErrorProducibleStatuses.NotProducible)
                {
                    disposable.Dispose();
                }
                else
                {
                    producible.ProducibleStatus = ps;
                }
            });

            return p;
        }

        private void SetupProducibleStatusObservable(
            Guid machineGroupId,
            IProducible producible,
            Order order,
            ConcurrentList<Order> queue)
        {
            this.producibleStatusesDisposables.GetOrAdd(
                producible.Id,
                producible.ProducibleStatusObservable.DurableSubscribe(
                    ps =>
                    {
                        var tileCount = GetTileCountFromProducible(producible, order);

                        HandleProducibleCompletedOrRemoved(machineGroupId, producible, order, ps, tileCount);
                        HandleProducibleFailedOrNotProducible(machineGroupId, producible, order, tileCount, ps);
                        HandleRemainingQuantityForNonKitOrder(order, queue);

                        order.StatusChangedDateTime = DateTime.UtcNow;
                        this.orders.Update(order);

                        //orders.UpdateWithRetries(order); //partial fix for TFS 10256

                        if (ps == ProducibleStatuses.ProducibleCompleted || ps == ProducibleStatuses.ProducibleRemoved
                            || ps == ErrorProducibleStatuses.ProducibleFailed || ps == ErrorProducibleStatuses.NotProducible)
                        {
                            DisposeProducibleSubscription(producible, order);
                        }

                        if (order.ProducibleStatus == ProducibleStatuses.ProducibleCompleted || order.ProducibleStatus == ErrorProducibleStatuses.NotProducible)
                        {
                            // TODO: this is a quite expensive call -- we dont want to perform it too often
                            this.SendOrders();
                        }

                        this.UpdateUIWithOrderChange(order);

                    },
                    this.logger));
        }

        private int GetTileCountFromProducible(IProducible producible, Order order)
        {
            var kit = producible as Kit;
            if (kit != null)
            {
                var kitInOrder = order.Producible as Kit;
                if (kitInOrder != null && kitInOrder.ItemsToProduce.OfType<ICarton>().Count() == 1)
                {
                    var tiledCarton = kit.ItemsToProduce.OfType<TiledCarton>().FirstOrDefault();
                    if (tiledCarton != null)
                        return tiledCarton.Tiles.Count();
                }
                return 1;
            }

            var tiled = producible as TiledCarton;

            return tiled != null ? tiled.Tiles.Count() : 1;
        }
        private void UpdateUIWithOrderChange(Order order, string replyTo = null)
        {
            uiCommunicationService.SendMessageToUI(new Message<Order>
            {
                MessageType = OrderMessages.OrderChanged,
                ReplyTo = replyTo,
                Data = order
            });
        }

        private void DisposeProducibleSubscription(IProducible producible, Order order)
        {
            IDisposable subscription;
            this.producibleStatusesDisposables.TryRemove(producible.Id, out subscription);
            if (subscription != null)
            {
                this.logger.Log(LogLevel.Debug, "Orders {1} Disposed subscription for producible {0}", producible, order);
                subscription.Dispose();
            }
        }

        private IProducible CreateProducibleAsSingleOrTiledCarton(Order order)
        {
            var producible = MongoDbHelpers.DeepCopy(order.Producible);
            producible.CustomerUniqueId = order.OrderId + ":" + order.RemainingQuantity;
            var carton = producible.FindFirstICarton();
            if (carton != null)
            {
                var tileCountToUse = Math.Min(order.RemainingQuantity, carton.CartonOnCorrugate.TileCount);
                if (tileCountToUse > 1)
                {
                    var tiledCarton = new TiledCarton();
                    tiledCarton.AddTiles(Enumerable.Range(0, tileCountToUse).Select(i => carton).ToList());
                    tiledCarton.CustomerUniqueId = producible.CustomerUniqueId;
                    for (var i = order.RemainingQuantity - 1; i > order.RemainingQuantity - tileCountToUse; i--)
                        tiledCarton.CustomerUniqueId += "+" + i;

                    //if (producible is ICarton || producible is Kit)
                    if (producible is ICarton)
                        producible = tiledCarton;
                    else
                        carton = tiledCarton;
                }
            }
            producible.Id = Guid.NewGuid();
            producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            return producible;
        }


        private void RemoveProducibleFromMachineGroup(Guid machineGroupId, IProducible producible)
        {
            var list = ordersSentToMachineGroup[machineGroupId];
            list.Remove(producible);
        }

        private void AddProducibleToOrderAndMachineGroup(Guid machineGroupId, Order order, IProducible producible, bool allowTilingBetweenKits)
        {
            var tileCount = 1;
            var producibleAsCarton = producible as ICarton;
            var producibleAsTiled = producible as TiledCarton;
            var producibleAsKit = producible as Kit;

            if (producibleAsTiled != null)
            {
                tileCount = producibleAsTiled.Tiles.Count();
            }
            if (producibleAsCarton != null)
                producibleAsCarton.CartonOnCorrugate.TileCount = tileCount;

            if (producibleAsKit != null && producibleAsKit.ItemsToProduce.OfType<ICarton>().Count() == 1
                && producibleAsKit.ItemsToProduce.OfType<TiledCarton>().Count() == 1)
                tileCount = GetTileCountFromProducible(producible, order);
            var list = ordersSentToMachineGroup.GetOrAdd(machineGroupId, new ConcurrentList<IProducible>());

            for (var i = 0; i < tileCount; i++)
            {
                order.ProducibleInProgress.Add(producible);
                list.Add(producible);
            }
        }

        /// <summary>
        /// Gets the machine from producible and add it to the list of machines that the order have been worked on.
        /// </summary>
        /// <param name="producible">The producible.</param>
        /// <param name="order">The order.</param>
        private void GetMachineFromProducibleAndAddItToTheListOfMachinesThatTheOrderHaveBeenWorkedOn(IProducible producible, Order order)
        {
            if (producible.ProduceOnMachineId == Guid.Empty)
                return;

            if (order.MachinesThatHaveWorkedOnOrder.SingleOrDefault(m => m.Id == producible.ProduceOnMachineId) == null)
            {
                var machine = machineGroupService.GetMachinesInGroup(producible.ProducedOnMachineGroupId).SingleOrDefault(m => m.Id == producible.ProduceOnMachineId);
                if (machine != null)
                {
                    order.MachinesThatHaveWorkedOnOrder.Add(machine);
                }
            }
        }

        protected ConcurrentList<Order> GetQueue(Guid id)
        {
            return orderQueues.GetOrAdd(id, new ConcurrentList<Order>());
        }

        private void AddOrderToQueue(Guid id, Order order)
        {
            if (OrderContainsAnyNonProducibleCarton(order))
            {
                SetOrderAndProducibleStatusesToNotProducible(order);
            }

            var queue = GetQueue(id);

            if (order.ProducibleStatus != ErrorProducibleStatuses.NotProducible)
            {
                order.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
                order.ProductionSequence = queue.Count > 0 ? queue.Last().ProductionSequence + 1 : 0;
                queue.Add(order);
                logger.Log(LogLevel.Info, "Order '{0}({1})' added to queue '{2}'", order.OrderId, order.Id, id);
            }
            else if (order.ProducibleStatus == ErrorProducibleStatuses.NotProducible)
            {
                if (queue.Contains(order))
                {
                    queue.Remove(order);
                }
            }

            CreatOrUpdateOrderInRepository(order);

            UpdateUI();
        }

        private static void SetOrderAndProducibleStatusesToNotProducible(Order order)
        {
            if (order.Producible is Kit)
            {
                (order.Producible as Kit).ItemsToProduce.OfType<ICarton>().ForEach(p =>
                {
                    if (p.CartonOnCorrugate == null)
                    {
                        p.DoStatusTransition(p.ProducibleStatus, ErrorProducibleStatuses.NotProducible);
                        FailProducibleForOrder(p, order);
                    }
                });

                order.DoStatusTransition(order.ProducibleStatus, ErrorProducibleStatuses.NotProducible);
            }
            else if (order.Producible is ICarton)
            {
                order.Producible.DoStatusTransition(order.Producible.ProducibleStatus, ErrorProducibleStatuses.NotProducible);
                order.DoStatusTransition(order.ProducibleStatus, ErrorProducibleStatuses.NotProducible);
                FailProducibleForOrder(order.Producible, order);
            }
        }

        private bool OrderContainsAnyNonProducibleCarton(Order order)
        {
            if (order.Producible is Kit)
            {
                if ((order.Producible as Kit).ItemsToProduce.OfType<ICarton>().Any(pr => pr.CartonOnCorrugate == null))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (order.Producible is ICarton && (order.Producible as ICarton).CartonOnCorrugate == null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private void CreatOrUpdateOrderInRepository(Order order)
        {
            var orderOnDb = orders.Find(order.CustomerUniqueId);
            if (orderOnDb == null)
            {
                orders.Create(order);
            }
            else
            {
                if (orderOnDb.Id == order.Id)
                {
                    orders.Update(order);
                    //orders.UpdateWithRetries(order); //partial fix for TFS 10256
                }
                else if (orderOnDb.Id != order.Id)
                {
                    orders.Create(order);
                }
                else
                {
                    orderOnDb.ReproducedQuantity += order.OriginalQuantity;
                    orders.Update(orderOnDb);
                    //orders.UpdateWithRetries(orderOnDb); //partial fix for TFS 10256
                }
            }
        }

        //todo: refactor this into staging workflow or somewhere else.
        private void AddOptimalCorrugateIfContainsCarton(Order order, Guid id, bool machineGroupSpecificCarton = false)
        {
            if (string.IsNullOrWhiteSpace(order.Producible.CustomerUniqueId))
            {
                order.Producible.CustomerUniqueId = order.CustomerUniqueId;
            }

            var carton = order.Producible as ICarton;

            if (carton == null)
            {
                var kit = order.Producible as Kit;

                if (kit == null)
                    return;

                var assignedCartonOnCorrugate = false;

                //TODO: order should be flattened before, to handle tiling the same way as when it is reproduced.
                kit.ItemsToProduce.OfType<Order>().ForEach(o =>
                {
                    if (o.Producible is ICarton)
                    {
                        // Take into account kit quantities for tile count
                        var tileCount = order.RemainingQuantity * o.RemainingQuantity;
                        assignedCartonOnCorrugate = true;
                        AssignCartonOnCorrugateToCarton(o, id, machineGroupSpecificCarton, o.Producible as Carton, tileCount);
                    }
                });

                if (!assignedCartonOnCorrugate)
                {
                    foreach (var group in GroupByTilableCartons(kit.ItemsToProduce.OfType<Carton>()))
                    {
                        AssignCartonOnCorrugateToCartonsInGroup(order, id, machineGroupSpecificCarton, group);
                    }
                }

            }
            else
                AssignCartonOnCorrugateToCarton(order, id, machineGroupSpecificCarton, carton, order.RemainingQuantity);
        }

        private IEnumerable<List<ICarton>> GroupByTilableCartons(IEnumerable<ICarton> cartonsToEvaluate)
        {
            var evaluatedCartonList = new List<List<ICarton>>();
            var firstCarton = cartonsToEvaluate.FirstOrDefault();
            if (firstCarton == null)
                return evaluatedCartonList;
            evaluatedCartonList.Add(new List<ICarton>() { firstCarton });
            return GroupByTilableCartons(cartonsToEvaluate.Skip(1), evaluatedCartonList, firstCarton);
        }

        private IEnumerable<List<ICarton>> GroupByTilableCartons(IEnumerable<ICarton> cartonsToEvaluate, List<List<ICarton>> evaluatedCartons, ICarton currentCarton)
        {
            var comparer = new BoxFirstCartonTilingEqualityComparer();
            var nextCarton = cartonsToEvaluate.FirstOrDefault();
            if (nextCarton == null)
                return evaluatedCartons;

            if (comparer.Equals(currentCarton, nextCarton))
            {
                var lastList = evaluatedCartons.LastOrDefault();
                if (lastList != null)
                    lastList.Add(nextCarton);
            }
            else
            {
                evaluatedCartons.Add(new List<ICarton>() { nextCarton });
            }

            return GroupByTilableCartons(cartonsToEvaluate.Skip(1), evaluatedCartons, nextCarton);
        }

        private void AssignCartonOnCorrugateToCartonsInGroup(Order order, Guid id, bool machineGroupSpecificCarton, IEnumerable<ICarton> cartons)
        {
            var tileCount = cartons.Count() * order.RemainingQuantity;
            foreach (var carton in cartons)
            {
                AssignCartonOnCorrugateToCarton(order, id, machineGroupSpecificCarton, carton, tileCount);
            }
        }

        private void AssignCartonOnCorrugateToCarton(Order order, Guid id, bool machineGroupSpecificCarton, ICarton carton, int tileCount)
        {
            if (machineGroupSpecificCarton)
            {
                var machineGroup = this.machineGroupService.FindByMachineGroupId(id);
                carton.CartonOnCorrugate = this.corrugateService.GetOptimalCorrugateForMachineGroup(
                    carton,
                    machineGroup,
                    tileCount);
            }
            else
            {
                var productionGroup = this.productionGroupService.ProductionGroups.Single(p => p.Id == id);
                carton.CartonOnCorrugate = this.corrugateService.GetOptimalCorrugateForProductionGroup(
                    carton,
                    productionGroup,
                    tileCount);
            }

            //remove existing optimal corrugate restrictions
            order.Producible.Restrictions.RemoveAll(
                r =>
                    r is MustProduceWithOptimalCorrugateRestriction
                    || (r is BasicRestriction<string> && ((BasicRestriction<string>)r).Value == "OptimalCorrugateNotFound"));

            if (carton.CartonOnCorrugate != null)
            {
                order.Producible.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(carton.CartonOnCorrugate));
            }
            else
            {
                //Adding this restriction so that when the select process runs we will not have an MG that can fulfill this restriction.
                order.Producible.Restrictions.Add(new BasicRestriction<string>("OptimalCorrugateNotFound"));
            }
        }

        private static void HandleRemainingQuantityForNonKitOrder(Order order, ConcurrentList<Order> queue)
        {
            //Remove order from queue if all items have completed
            if (order.RemainingQuantity == 0 && order.ProducibleInProgress.Count == 0)
            {
                order.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                queue.Remove(order);
            }
        }

        private void HandleProducibleFailedOrNotProducible(
            Guid machineGroupId,
            IProducible producible,
            Order order,
            int tileCount,
            ProducibleStatuses ps)
        {
            if (ps != ErrorProducibleStatuses.ProducibleFailed && ps != ErrorProducibleStatuses.NotProducible)
            {
                return;
            }

            for (var i = 0; i < tileCount; i++)
            {
                RemoveProducibleFromOrderInProgress(producible, order);

                if (ps == ErrorProducibleStatuses.NotProducible)
                {
                    OnProducibleNotProducible(producible, order);
                }

                this.RemoveProducibleFromMachineGroup(machineGroupId, producible);
            }
        }

        private static void OnProducibleNotProducible(IProducible producible, Order order)
        {
            FailProducibleForOrder(producible, order);
        }

        private void HandleProducibleCompletedOrRemoved(
            Guid machineGroupId,
            IProducible producible,
            Order order,
            ProducibleStatuses ps,
            int tileCount)
        {
            if (ps != ProducibleStatuses.ProducibleCompleted && ps != ProducibleStatuses.ProducibleRemoved) return;

            var workItems = new ConcurrentList<IProducible>();
            if (producible.ProducibleType == ProducibleTypes.Kit)
            {
                var kit = producible as Kit;
                if (kit != null)
                    workItems = kit.ItemsToProduce;
            }
            else
            {
                workItems.Add(producible);
            }

            foreach (var workItem in workItems)
            {
                GetMachineFromProducibleAndAddItToTheListOfMachinesThatTheOrderHaveBeenWorkedOn(workItem, order);
            }

            if (ps == ProducibleStatuses.ProducibleCompleted)
            {
                AddProducedQuantityToOrderProduced(producible, order, tileCount);
            }

            if (order.Producible is Kit)
            {
                order.Producible.ProducibleStatus = ps;
            }

            this.logger.Log(LogLevel.Debug, "{0} removed item {1} from InProgress because producible moved to state {2}. TileCount: {3}", order, producible, ps.DisplayName, tileCount);
            for (var i = 0; i < tileCount; i++)
            {
                RemoveProducibleFromOrderInProgress(producible, order);
                RemoveProducibleFromMachineGroup(machineGroupId, producible);
            }
        }

        private static void AddProducedQuantityToOrderProduced(IProducible producible, Order order, int tileCount)
        {
            for (var i = 0; i < tileCount; i++)
            {
                order.Produced.Add(producible.Id);
            }
        }

        private static void RemoveProducibleFromOrderInProgress(
            IProducible producible,
            Order order)
        {
            order.ProducibleInProgress.Remove(producible);
        }

        private static void FailProducibleForOrder(
            IProducible producible,
            Order order)
        {
            order.Failed.Add(producible.Id);
        }


        private Guid FindMachineGroupOrProductionGroupForOrder(Order order)
        {
            return FindMachineGroupOrProductionGroupForOrder(order.Id);
        }

        private Guid FindMachineGroupOrProductionGroupForOrder(Guid orderId)
        {
            foreach (var orderQueue in orderQueues)
            {
                if (orderQueue.Value.Any(o => o.Id == orderId))
                    return orderQueue.Key;
            }

            return Guid.Empty;
        }

        public IEnumerable<IProducible> GetProduciblesSentToMachineGroup(Guid machineGroupId)
        {
            if (ordersSentToMachineGroup.ContainsKey(machineGroupId))
                return ordersSentToMachineGroup[machineGroupId].ToList();

            return new List<IProducible>();
        }

        public IReadOnlyCollection<IProducible> GetProducibles(ProducibleStatuses status = null)
        {
            throw new NotImplementedException();
        }
    }
}