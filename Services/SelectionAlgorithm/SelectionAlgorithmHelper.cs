﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services.SelectionAlgorithm
{
    public static class SelectionAlgorithmHelper
    {
        public static void CheckForLabelSentToPrinterAndCompleteIfSo(IProducibleWrapper producibleWrapper, ProducibleStatuses notProducibleStatuses)
        {
            if (producibleWrapper.ProducibleStatus == CartonPropertyGroupMessages.ProducibleCompleted)
                return;// we don't need to update the producible again to completed

            var labels = GetAllProducibleItems(producibleWrapper).OfType<Label>().ToList();
            var labelPrinted = false;
            if (labels.Any())
            {
                labelPrinted = labels.All(
                    l =>
                        l.ProducibleStatus == ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff ||
                        l.ProducibleStatus == ZebraInProductionProducibleStatuses.ProducibleCompleted ||
                        (l.ProducibleStatus == notProducibleStatuses &&
                         RequireManualReproduction(l.History.Last().PreviousStatus)));
            }

            UpdateStatus(producibleWrapper, labelPrinted ? ProducibleStatuses.ProducibleCompleted : notProducibleStatuses);
            if (!labelPrinted)
            {
                producibleWrapper.ProducedOnMachineGroupId = Guid.Empty;

                UpdateStatus(producibleWrapper, NotInProductionProducibleStatuses.ProducibleStaged);
            }
        }

        public static IEnumerable<IProducible> GetAllProducibleItems(IProducible producible)
        {
            return GetAllProducibleItems(new List<IProducible>() { producible });
        }

        public static bool MachineGroupUsesSelectionAlgorithm(Guid? machineGroupId, IProductionGroupService productionGroupService, IMachineGroupService machineGroupService, SelectionAlgorithmTypes selectionAlgorithmType)
        {
            if (machineGroupId.HasValue)
            {
                var pg = productionGroupService.GetProductionGroupForMachineGroup(machineGroupId.Value);
                var mg = machineGroupService.FindByMachineGroupId(machineGroupId.Value);

                if(mg.ProductionMode == MachineGroupProductionModes.ManualProductionMode)
                    return SelectionAlgorithmTypes.Order == selectionAlgorithmType;

                return pg.SelectionAlgorithm == selectionAlgorithmType;
            }
            return SelectionAlgorithmTypes.Order == selectionAlgorithmType;
        }

        private static IEnumerable<IProducible> GetAllProducibleItems(IEnumerable<IProducible> selectedProducibles)
        {
            var producibleItems = new List<IProducible>();
            foreach (var producible in selectedProducibles)
            {
                var prod = producible;

                var wrapper = producible as IProducibleWrapper;
                if (wrapper != null)
                    prod = wrapper.Producible;
                
                var kit = prod as Kit;
                if (kit != null)
                    producibleItems.AddRange(GetAllProducibleItems(kit.ItemsToProduce));
                else
                    producibleItems.Add(prod);
                
            }

            return producibleItems;
        }

        private static bool RequireManualReproduction(ProducibleStatuses previousStatus)
        {
            return previousStatus == ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff ||
                   previousStatus == ZebraErrorProducibleStatus.FailedToStart ||
                   previousStatus == InProductionProducibleStatuses.ProducibleProductionStarted;
        }

        private static void UpdateStatus(IProducibleWrapper wrapper, ProducibleStatuses status)
        {
            wrapper.ProducibleStatus = status;
            wrapper.Producible.ProducibleStatus = status;
        }
    }
}
