﻿using System.Diagnostics;

using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Scanning;
using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.Producibles;


using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

using PackNet.Services.Properties;

namespace PackNet.Services.SelectionAlgorithm.BoxLast
{
    /// <summary>
    /// Manages state for the Box Last selection algorithm
    /// </summary>
    public class BoxLastSelectionAlgorithmService : IBoxLastSelectionAlgorithmService
    {
        private readonly IRestrictionResolverService restrictionResolverService;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IProductionGroupService productionGroupService;
        private readonly IBoxLastRepository boxLastRepository;
        private readonly IOptimalCorrugateCalculator optimalCorrugateCalculator;
        private readonly IMachineGroupService machineGroupService;
        private readonly IAggregateMachineService machineService;
        private readonly IServiceLocator serviceLocator;
        private readonly ILogger logger;

        public IReadOnlyCollection<BoxLastProducible> Producibles { get { return CurrentCartonRequests.Values.ToList().AsReadOnly(); } }
        public IReadOnlyCollection<BoxLastProducible> TriggeredRequests { get { return ProductionGroupQueues.SelectMany(p => p.Value).ToList().AsReadOnly(); } }
        public string Name { get { return "BoxLastSelectionAlgorithmService"; } }
        protected ConcurrentDictionary<string /*CustomerUniqueId*/, BoxLastProducible> CurrentCartonRequests = new ConcurrentDictionary<string, BoxLastProducible>();
        protected ConcurrentDictionary<string /*ProductionGroupAlias*/, ConcurrentList<BoxLastProducible>> ProductionGroupQueues = new ConcurrentDictionary<string, ConcurrentList<BoxLastProducible>>();

        private ConcurrentDictionary<Guid /*ProducibleId*/, IDisposable> producibleStatusesDisposables = new ConcurrentDictionary<Guid, IDisposable>();

        private IDisposable getProduciblesDisposable;
        private IDisposable uiGetProduciblesDisposable;
        private IDisposable reproduceDisposable;
        private IDisposable productionGroupChangedDisposible;
        private IDisposable searchDisposable;
        private IDisposable reproduceCartonsDisposable;

        private IUICommunicationService uiCommunicationService;
        private bool disposed;
        //todo: we should not be using type ICartonRequestOrder
        private readonly ConcurrentDictionary<Guid/*machine group id*/, ConcurrentList<IProducible>> jobsSentToMachineGroup;

        public BoxLastSelectionAlgorithmService(
            IRestrictionResolverService restrictionResolverService,
            IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher,
            IProductionGroupService productionGroupService,
            IBoxLastRepository boxLastRepository,
            IOptimalCorrugateCalculator optimalCorrugateCalculator,
            IMachineGroupService machineGroupService,
            IAggregateMachineService machineService,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            ILogger logger)
        {
            jobsSentToMachineGroup = new ConcurrentDictionary<Guid, ConcurrentList<IProducible>>();
            this.restrictionResolverService = restrictionResolverService;
            this.subscriber = subscriber;
            this.publisher = publisher;
            this.productionGroupService = productionGroupService;
            this.boxLastRepository = boxLastRepository;
            this.optimalCorrugateCalculator = optimalCorrugateCalculator;
            this.machineGroupService = machineGroupService;
            this.uiCommunicationService = uiCommunicationService;
            this.machineService = machineService;
            this.logger = logger;
            this.serviceLocator = serviceLocator;
            Wireups();

            var sw = Stopwatch.StartNew();
            LoadCartonRequests();
            sw.Stop();
            Console.WriteLine("BoxLastSelectionAlgorithmService loaded and ready with all existing producibles... took " + sw.ElapsedMilliseconds + "(ms)");
            serviceLocator.RegisterAsService<IBoxLastSelectionAlgorithmService>(this);
        }

        private void Wireups()
        {
            // Setup production group queues
            productionGroupService.ProductionGroups
                .Where(pg => pg.SelectionAlgorithm == SelectionAlgorithmTypes.BoxLast)
                .ForEach(pg => ProductionGroupQueues.TryAdd(pg.Alias, new ConcurrentList<BoxLastProducible>()));


            productionGroupChangedDisposible =
            ProductionGroupMessages.ProductionGroupChanged
                .OnMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>(subscriber, logger,
                    msg =>
                    {
                        if (!ProductionGroupQueues.ContainsKey(msg.Data.Alias))
                            return;
                        ReStageAllProduciblesInPg(msg.Data);
                    }
                );


            subscriber.GetEvent<IMessage<Common.Interfaces.DTO.ProductionGroups.ProductionGroup>>()
                .Where(msg => msg.MessageType == ProductionGroupMessages.ProductionGroupCreated)
                .DurableSubscribe(newPgMsg =>
                {
                    var alias = newPgMsg.Data.Alias;

                    if (!ProductionGroupQueues.ContainsKey(alias))
                    {
                        ProductionGroupQueues.TryAdd(alias, new ConcurrentList<BoxLastProducible>());
                    }

                }, logger);

            subscriber.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == MachineMessages.AssignCorrugatesResponse)
                .DurableSubscribe(RetriggerStagingForMachineGroupsProdcutionGroup);

            //todo: what is the difference between this wireup and the next one? can they be combined in some fashion?
            getProduciblesDisposable =
                subscriber.GetEvent<IMessage<DataRequest>>()
                    .ObserveOn(System.Reactive.Concurrency.Scheduler.Default)
                    .Where(s => s.MessageType == MessageTypes.GetProducibles && s.Data.DataSource.Equals("BoxLast"))
                    .DurableSubscribe(GetProducibles, logger);

            uiGetProduciblesDisposable =
                uiCommunicationService.UIEventObservable
                    .Where(
                        m =>
                        {
                            if (m.Item1 != MessageTypes.GetProducibles)
                                return false;
                            //TODO: stop using json directly... Thanks Ryan M ಠ_ಠ
                            var dataRequest = JsonToDataRequestConverter.ConvertJsonToDataRequest(m.Item2);
                            return dataRequest != null && dataRequest.DataSource == "BoxLast";
                        })
                    .DurableSubscribe(
                        m =>
                        {
                            //TODO: stop using json directly... Thanks Ryan M ಠ_ಠ
                            var dataRequest = JsonToDataRequestConverter.ConvertJsonToDataRequest(m.Item2);
                            var replyTo = JObject.Parse(m.Item2)["ReplyTo"].ToString();
                            GetProducibles(dataRequest, replyTo);
                        }, logger);


            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineMessages.GetProductionHistory);
            MachineMessages.GetProductionHistory
                .OnMessage(subscriber, logger,
                    msg =>
                    {
                        if (!msg.MachineGroupId.HasValue)
                        {
                            logger.Log(LogLevel.Warning,
                                "request for getproductionhistory does not have a machinegroupid set");
                            return;
                        }

                        var machineGroup = machineGroupService.FindByMachineGroupId(msg.MachineGroupId.Value);
                        if (machineGroup == null)
                            return;

                        var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(msg.MachineGroupId.Value);
                        if (productionGroup == null)
                            return;

                        if (productionGroup.SelectionAlgorithm != SelectionAlgorithmTypes.BoxLast)
                            return;

                        var history = boxLastRepository.GetProductionHistoryForMachineGroup(machineGroup.Id);

                        uiCommunicationService.SendMessageToUI(new Message<List<BoxLastProducible>>
                        {
                            MessageType = MachineMessages.MachineProductionHistory,
                            ReplyTo = msg.ReplyTo,
                            Data = history.ToList()
                        });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Tuple<DateTime, DateTime>>(CartonMessages.CartonRemovalBetweenDate);
            subscriber.GetEvent<IMessage<Tuple<DateTime, DateTime>>>()
                .Where(t => t.MessageType == CartonMessages.CartonRemovalBetweenDate)
                .DurableSubscribe(m =>
                {
                    var dates = m.Data;
                    logger.Log(LogLevel.Info, "User {2} Deleting Box Last Producibles from {0} to {1}", dates.Item1, dates.Item2, m.UserName);
                    var cartonsAffectedCount = boxLastRepository.DeleteCartonsBetweenDates(dates.Item1, dates.Item2);
                    logger.Log(LogLevel.Info, "User {1} Deleted {0} Box Last Producibles", cartonsAffectedCount, m.UserName);

                    var cartonsToRemove = CurrentCartonRequests.Where(cr =>
                    {
                        var created = cr.Value.History.First().OccuredOn;
                        return created > dates.Item1 && created < dates.Item2;
                    }).Select(cr => cr.Key).ToList();

                    BoxLastProducible bp;
                    cartonsToRemove.ForEach(ctr => CurrentCartonRequests.TryRemove(ctr, out bp));

                    uiCommunicationService.SendMessageToUI(new Message<long>
                    {
                        MessageType = CartonMessages.CartonRemovedCount,
                        ReplyTo = m.ReplyTo,
                        Data = cartonsAffectedCount
                    });

                }, logger);


            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Producible>(CartonMessages.ReproduceCarton);
            reproduceDisposable = subscriber.GetEvent<IMessage<Producible>>()
                .Where(msg => msg.MessageType == CartonMessages.ReproduceCarton)
                .DurableSubscribe(m =>
                {
                    var producible = boxLastRepository.FindByCustomerUniqueId(m.Data.CustomerUniqueId);
                    if (producible == null)
                        return;

                    Reproduce(producible);
                }, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<IEnumerable<Producible>>(CartonMessages.ReproduceCartons);
            reproduceCartonsDisposable = subscriber.GetEvent<IMessage<IEnumerable<Producible>>>()
                .Where(msg => msg.MessageType == CartonMessages.ReproduceCartons)
                .DurableSubscribe(m => m.Data.ForEach(p =>
                {
                    var producible = boxLastRepository.FindByCustomerUniqueId(p.CustomerUniqueId);
                    if (producible == null)
                        return;

                    Reproduce(producible);
                }), logger);


            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SearchProducibles>(SelectionAlgorithmMessages.SearchForProducibles);
            searchDisposable = subscriber.GetEvent<IMessage<SearchProducibles>>()
                .Where(m => m.Data.SelectionAlgorithmTypes == SelectionAlgorithmTypes.BoxLast)
                .DurableSubscribe(m =>
                {
                    var searchResult = boxLastRepository.Search(m.Data.Criteria, Settings.Default.MaxBoxLastSearchResults);

                    uiCommunicationService.SendMessageToUI(new Message<IEnumerable<BoxLastProducible>>
                    {
                        MessageType = CartonMessages.SearchBoxLastResponse,
                        ReplyTo = m.ReplyTo,
                        Data = searchResult
                    });
                }, logger);
        }

        private void RetriggerStagingForMachineGroupsProdcutionGroup(IMessage msg)
        {
            var affectedPg = productionGroupService.ProductionGroups.First(pg => pg.ConfiguredMachineGroups.Contains(msg.MachineGroupId));
            ReStageAllProduciblesInPg(affectedPg);
        }

        /// <summary>
        /// Returns the producibles requested in the message.
        /// </summary>
        /// <param name="message"><see cref="DataRequest"/> with filters for data request</param>
        private void GetProducibles(IMessage<DataRequest> message)
        {
            var requestList = GetRequestList(
                message.Data.Filters["RequestSet"],
                message.Data.Filters["ProductionGroup"]).ToList();

            var filteredRequests = FilterRequests(requestList,
                message.Data.Filters["SearchType"],
                message.Data.Filters["Value"]);

            var chunkedRequests = filteredRequests.Skip(message.Data.Chunk * message.Data.ChunkSize).Take(message.Data.ChunkSize);

            publisher.Publish(
                new SearchResponse<IEnumerable<BoxLastProducible>>(
                    message.Data.DataSource,
                    requestList.Count(),
                    ResponseMessages.SearchedProducibles,
                    ResultTypes.Success,
                    chunkedRequests,
                    message.ReplyTo));
        }

        /// <summary>
        /// Returns the producibles requested in the message.
        /// </summary>
        /// <param name="data"><see cref="DataRequest"/> with filters for data request</param>
        /// <param name="replyTo">String representation of whom to reply to on the message queue</param>
        private void GetProducibles(DataRequest data, string replyTo)
        {
            var requestList = GetRequestList(
                data.Filters["RequestSet"],
                data.Filters["ProductionGroup"]).ToList();

            var filteredRequests = FilterRequests(requestList,
                data.Filters["SearchType"],
                data.Filters["Value"]);

            var chunkedRequests = filteredRequests.Skip(data.Chunk * data.ChunkSize).Take(data.ChunkSize);

            uiCommunicationService.SendMessageToUI(new SearchResponse<IEnumerable<BoxLastProducible>>(
                data.DataSource,
                requestList.Count(),
                ResponseMessages.SearchedProducibles,
                ResultTypes.Success,
                chunkedRequests,
                replyTo));
        }

        /// <summary>
        /// Filters the requestList using the filterType and filterValue
        /// </summary>
        /// <param name="requestList">List of requests to filter</param>
        /// <param name="filterType">Type of filter</param>
        /// <param name="filterValue">Value of filter</param>
        /// <returns></returns>
        private static IEnumerable<BoxLastProducible> FilterRequests(IEnumerable<BoxLastProducible> requestList, string filterType, string filterValue)
        {
            var requests = requestList as BoxLastProducible[] ?? requestList.ToArray();
            if (requestList == null || !requests.Any())
            {
                return new List<BoxLastProducible>();
            }

            return filterType != string.Empty
                ? requests.Where(
                    r => r.GetType().GetProperty(filterType).GetValue(r, null).ToString().Contains(filterValue))
                : requests;
        }

        /// <summary>
        /// Returns a list of cartons for the given production group and either triggered or all.
        /// </summary>
        /// <param name="requestSetFilter">Filter by either all or triggered</param>
        /// <param name="productionGroup">Production group to filter by</param>
        /// <returns></returns>
        private IEnumerable<BoxLastProducible> GetRequestList(string requestSetFilter, string productionGroup)
        {
            if (!string.IsNullOrWhiteSpace(productionGroup) && ProductionGroupQueues.ContainsKey(productionGroup))
            {

                return requestSetFilter.Equals("triggered")
                    ? TriggeredRequests.Where(t => t.ProductionGroupAlias == productionGroup).OrderBy(t => t.TimeTriggered)
                    : Producibles.Where(p => p.ProductionGroupAlias == productionGroup)
                        .Concat(TriggeredRequests.Where(t => t.ProductionGroupAlias == productionGroup))
                        .OrderBy(p => p.CustomerUniqueId);
            }
            if (requestSetFilter.Equals("all"))
            {
                return requestSetFilter.Equals("triggered")
                    ? TriggeredRequests.OrderBy(t => t.TimeTriggered)
                    : Producibles.Concat(TriggeredRequests).OrderBy(p => p.CustomerUniqueId);
            }

            return new List<BoxLastProducible>();
        }

        /// <summary>
        /// Triggers a carton request by setting it in the FIFO queue for the production group.
        /// </summary>
        /// <param name="createCartonTriggerEventData">Barcode data  and production group to trigger</param>
        /// <param name="triggeredProducible">Returns carton that was triggered</param>
        /// <returns>True if successfully trigger request, false otherwise</returns>
        private bool TriggerCartonRequest(IScanTrigger createCartonTriggerEventData, out BoxLastProducible triggeredProducible)
        {
            //todo: refactor all usages of the word Carton out to producible unless they are really using a carton then question why we car if it's a carton.
            bool retval = false;
            //Producibles that have not yet been triggered
            if (CurrentCartonRequests.TryRemove(createCartonTriggerEventData.BarcodeData, out triggeredProducible))
            {

                if (string.IsNullOrEmpty(triggeredProducible.ProductionGroupAlias))
                {
                    logger.Log(LogLevel.Debug,
                        "Producible CustomerUniqueId:{0} triggered to ProductionGroup:{1}",
                        createCartonTriggerEventData.BarcodeData, createCartonTriggerEventData.ProductionGroupAlias);
                    triggeredProducible.ProductionGroupAlias = createCartonTriggerEventData.ProductionGroupAlias;
                }

                if (triggeredProducible.ProductionGroupAlias != createCartonTriggerEventData.ProductionGroupAlias)
                {
                    logger.Log(LogLevel.Debug,
                        "Producible CustomerUniqueId:{0} retriggered from ProductionGroup:{1} to ProductionGroup:{2}",
                        createCartonTriggerEventData.BarcodeData, triggeredProducible.ProductionGroupAlias,
                        createCartonTriggerEventData.ProductionGroupAlias);

                    triggeredProducible.ProductionGroupAlias = createCartonTriggerEventData.ProductionGroupAlias;
                }

                triggeredProducible.TimeTriggered = createCartonTriggerEventData.TriggeredOn;

                retval = true;
                PublishSuccess(
                    string.Format("Successfully triggered producible with CustomerUniqueId: {0}",
                        createCartonTriggerEventData.BarcodeData), createCartonTriggerEventData.ProductionGroupAlias);
            }
            //Retriggering a producible.
            else if (RemoveRequestFromProductionQueue(createCartonTriggerEventData.BarcodeData, out triggeredProducible))
            {
                if (triggeredProducible.ProductionGroupAlias != createCartonTriggerEventData.ProductionGroupAlias)
                {
                    triggeredProducible.ProductionGroupAlias = createCartonTriggerEventData.ProductionGroupAlias;
                    logger.Log(LogLevel.Debug, "Producible CustomerUniqueId:{0} triggered to PG: {1}",
                        createCartonTriggerEventData.BarcodeData, createCartonTriggerEventData.ProductionGroupAlias);
                }
                logger.Log(LogLevel.Debug, "Producible CustomerUniqueId:{0} retriggered", createCartonTriggerEventData.BarcodeData);

                PublishSuccess(
                    string.Format("Successfully retriggered producible with CustomerUniqueId: {0}",
                        createCartonTriggerEventData.BarcodeData), createCartonTriggerEventData.ProductionGroupAlias);
                retval = true;
            }
            else
            {
                var errorMessage =
                string.Format(
                    "Could not find producible with CustomerUniqueId: {0} for production group {1}",
                    createCartonTriggerEventData.BarcodeData, createCartonTriggerEventData.ProductionGroupAlias);

                logger.Log(
                    LogLevel.Error,
                    errorMessage);

                PublishError(errorMessage, createCartonTriggerEventData.ProductionGroupAlias);
            }

            // We always reimport in order to recalculate optimal corrugate
            if (triggeredProducible != null)
            {
                ReImportProducible(triggeredProducible);
            }

            return retval;
        }

        private void ReStageAllProduciblesInPg(Common.Interfaces.DTO.ProductionGroups.ProductionGroup pg)
        {
            ConcurrentList<BoxLastProducible> boxLastProduciblesToReload;
            ProductionGroupQueues.TryGetValue(pg.Alias, out boxLastProduciblesToReload);
            if (boxLastProduciblesToReload == null || boxLastProduciblesToReload.Count == 0)
                return;

            boxLastProduciblesToReload.ForEach(RemoveOptimalCorrugateRestrictions);

            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                MessageType = MessageTypes.Restaged,
                Data = boxLastProduciblesToReload.ToList(),
                SelectionAlgorithmType = SelectionAlgorithmTypes.BoxLast
            };

            // Carton will need to be restaged so that its optimal corrugate may be recalculatecd
            publisher.Publish(message);
        }

        /// <summary>
        /// Raises an import message for this carton so that it will go through the staging workflow
        /// again to have its restrictions set
        /// </summary>
        /// <param name="producible">Carton to publish an import message</param>
        /// <remarks>
        /// Use this when we know something has changed with the request, such as a carton's production group
        /// has changed through triggering the request. The staging workflow will re-add the producible to
        /// the service through AddProducibles
        /// </remarks>
        private void Reproduce(BoxLastProducible producible)
        {
            RemoveOptimalCorrugateRestrictions(producible);
            UpdateProducibleStatus(producible, NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction);
            Persist(producible);

            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                Data = new List<IProducible> { producible },
                SelectionAlgorithmType = SelectionAlgorithmTypes.BoxLast,
                MessageType = MessageTypes.Restaged
            };

            // Carton will need to be restaged so that its optimal corrugate may be recalculatecd
            publisher.Publish(message);
        }

        private void ReImportProducible(BoxLastProducible producible)
        {
            // Remove corrugate restrictions as it will be set again when carton returns to staging
            RemoveOptimalCorrugateRestrictions(producible);

            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                Data = new List<IProducible> { producible },
                SelectionAlgorithmType = SelectionAlgorithmTypes.BoxLast,
                MessageType = MessageTypes.Restaged
            };

            // Carton will need to be restaged so that its optimal corrugate may be recalculatecd
            publisher.Publish(message);
        }

        private void RemoveShouldWaitForBoxReleaseRestrictions(IProducible producible)
        {
            if (producible is Kit)
            {
                (producible as Kit).ItemsToProduce.OfType<ICarton>()
                    .ForEach(RemoveShouldWaitForBoxReleaseRestrictions);
                producible.Restrictions.Remove(new BasicRestriction<string>(CartonRestrictions.ShouldWaitForBoxRelease));
            }
            if (producible is BoxLastProducible)
            {
                RemoveShouldWaitForBoxReleaseRestrictions(((BoxLastProducible)producible).Producible);
            }
            else if (producible is ICarton)
            {
                producible.Restrictions.Remove(new BasicRestriction<string>(CartonRestrictions.ShouldWaitForBoxRelease));
            }
        }

        private void RemoveOptimalCorrugateRestrictions(IProducible producible)
        {
            if (producible is Kit)
            {
                (producible as Kit).ItemsToProduce.OfType<ICarton>()
                    .ForEach(RemoveOptimalCorrugateRestrictions);
            }
            if (producible is BoxLastProducible)
            {
                RemoveOptimalCorrugateRestrictions(((BoxLastProducible)producible).Producible);
            }
            else if (producible is ICarton)
            {
                producible.Restrictions.RemoveAll(r => r is MustProduceWithOptimalCorrugateRestriction);
            }
        }

        /// <summary>
        /// Publishes a Message&lt;ScanMessage&gt; with a MessageType as <see cref="MessageTypes.ScanTriggerError"/> 
        /// </summary>
        /// <param name="message">Error message</param>
        /// <param name="productionGroupAlias">Alias of production group</param>
        private void PublishError(string message, string productionGroupAlias)
        {
            uiCommunicationService.SendMessageToUI(new Message<ScanMessage>
            {
                MessageType = MessageTypes.ScanTriggerError,
                Data = new ScanMessage(message, productionGroupAlias)
            });
        }

        /// <summary>
        /// Publishes a Message&lt;ScanMessage&gt; with a MessageType as <see cref="MessageTypes.ScanTriggerSuccess"/>
        /// </summary>
        /// <param name="message">Success message</param>
        /// <param name="productionGroupAlias">Alias of production group</param>
        private void PublishSuccess(string message, string productionGroupAlias)
        {
            uiCommunicationService.SendMessageToUI(new Message<ScanMessage>
            {
                MessageType = MessageTypes.ScanTriggerSuccess,
                Data = new ScanMessage(message, productionGroupAlias)
            });
        }

        /// <summary>
        /// Initial load of carton requests from the repository used when instantiating the class.
        /// </summary>
        private void LoadCartonRequests()
        {
            var boxLastProduciblesToReload = boxLastRepository.FindWhereStatusIsNot(ProducibleStatuses.ProducibleCompleted);

            // Remove time triggered as functionality expected is to have the carton be retriggered on system restart
            boxLastProduciblesToReload.ForEach(c =>
            {
                c.TimeTriggered = null;
                RemoveOptimalCorrugateRestrictions(c);
                //Due to the way that kits gets their restrictions should wait for box release could propgate 
                //from the carton to the kit if the system is restarted when a carton is waiting for box release
                RemoveShouldWaitForBoxReleaseRestrictions(c);
                c.ProduceOnMachineId = Guid.Empty;
                c.ProducedOnMachineGroupId = Guid.Empty;
                UpdateProducibleStatus(c, NotInProductionProducibleStatuses.ProducibleStaged);
                Persist(c);
                CurrentCartonRequests.AddOrUpdate(c.CustomerUniqueId, c, (s, carton) => c);
            });
        }

        /// <summary>
        /// Handler for when cartons are added to the system.
        /// </summary>
        /// <param name="producibles">Cartons added to the system</param>
        public void AddProducibles(IEnumerable<IProducible> producibles)
        {
            var produciblesList = producibles as IList<IProducible> ?? producibles.ToList();
            if (producibles == null || !produciblesList.Any())
            {
                return;
            }

            var overwriteCount = 0;
            var skipCount = 0;
            var produciblesOverwrittenOrSkipped = new List<IProducible>();

            //todo: possible bug if a producible is imported with a production group value we will assume is should be auto triggered.
            produciblesList
                .OfType<BoxLastProducible>()
                .Where(p => !string.IsNullOrWhiteSpace(p.CustomerUniqueId))
                .ForEach(p =>
                {
                    // Check for uniqueness
                    var addOrUpdate = false;

                    var existingProducible = boxLastRepository.FindByCustomerUniqueId(p.CustomerUniqueId);

                    if (existingProducible != null && p.Id != existingProducible.Id)
                    {
                        produciblesOverwrittenOrSkipped.Add(p);

                        if(existingProducible.TimeTriggered.HasValue == false)
                        {
                            //The existing producible is in a state that can be replaced. Removing old values.
                            RemoveRequestFromCurrentRequests(existingProducible.CustomerUniqueId);
                            boxLastRepository.Delete(existingProducible.Id);
                            addOrUpdate = true;

                            logger.Log(LogLevel.Info, String.Format("Box Last Carton Selector Overwritten carton with CustomerUniqueId {0}", p.CustomerUniqueId));
                            overwriteCount++;
                        }
                        else
                        {
                            logger.Log(LogLevel.Info, String.Format("Box Last Carton Selector Skipped duplicate carton with CustomerUniqueId {0}", p.CustomerUniqueId));
                            skipCount++;
                        }
                    }
                    else
                    {
                        addOrUpdate = true;
                    }

                    if (addOrUpdate)
                    {
                        // See if this request needs to be sent to production
                        if (!AddTriggeredProducibleToProducitonGroupQueue(p))
                        {
                            CurrentCartonRequests.AddOrUpdate(p.CustomerUniqueId, p, (s, carton) => p);
                            p.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
                        }
                        Persist(p);
                    }
                });

            // Adjust the Producible Items Imported metric
            if (produciblesOverwrittenOrSkipped.Count > 0)
            {
                var message = new ImportMessage<IEnumerable<IProducible>>
                {
                    MessageType = MessageTypes.DataOverwrittenOrSkipped,
                    Data = produciblesOverwrittenOrSkipped,
                    SelectionAlgorithmType = SelectionAlgorithmTypes.BoxLast
                };

                publisher.Publish(message);
            }

            logger.Log(LogLevel.Info, String.Format("Box Last Carton Selector Added {0} cartons, Overwritten {1} cartons and Skipped {2} cartons", produciblesList.Count() - (overwriteCount + skipCount), overwriteCount, skipCount));
        }
        
        public bool Trigger(IScanTrigger triggerDetails)
        {
            BoxLastProducible producible;
            if (TriggerCartonRequest(triggerDetails, out producible))
            {
                producible.Producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
                Persist(producible);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Removes request from current requests.
        /// </summary>
        /// <param name="customerUniqueId">CustomerUniqueId of carton to remove</param>
        /// <returns>True if successfully removed, false otherwise</returns>
        private bool RemoveRequestFromCurrentRequests(string customerUniqueId)
        {
            BoxLastProducible removedRequest;
            CurrentCartonRequests.TryRemove(customerUniqueId, out removedRequest);
            if (removedRequest != null) logger.Log(LogLevel.Info, "Carton CustomerUniqueId:{0} removed from current requests", customerUniqueId);

            return removedRequest != null;
        }

        /// <summary>
        /// Removes request from production group queue. Use this method when you need to reassign
        /// the request to the current queue or reassign it to another production queue. Use the 
        /// out parameter to reassign the request.
        /// </summary>
        /// <param name="customerUniqueId">CustomerUniqueId of carton to remove</param>
        /// <param name="carton">Carton that was removed</param>
        /// <returns>True if successfully removed, false otherwise</returns>
        private bool RemoveRequestFromProductionQueue(string customerUniqueId, out BoxLastProducible carton)
        {
            carton = null;
            foreach (var productionGroupAlias in ProductionGroupQueues.Keys)
            {
                ConcurrentList<BoxLastProducible> pgQueue;
                ProductionGroupQueues.TryGetValue(productionGroupAlias, out pgQueue);
                if (pgQueue == null || !pgQueue.Any()) continue; // Do nothing if no requests in queue

                lock (pgQueue)
                {
                    var requestToRemove = pgQueue.FirstOrDefault(r => r.CustomerUniqueId == customerUniqueId);
                    if (requestToRemove == null) continue; // Do nothing if request is not in this production queue

                    carton = requestToRemove;
                    pgQueue.Remove(requestToRemove);
                    logger.Log(LogLevel.Info, "Carton CustomerUniqueId:{0} removed from production queue:{1}", customerUniqueId, productionGroupAlias);
                }
                return true;
            }

            return false;
        }

        /// <summary>
        /// Re-add a carton to its associated production group queue. Used for when a carton has been 
        /// broken or retracted and needs to be re-added for production.
        /// </summary>
        /// <param name="producible">Producible</param>
        /// <returns>True if successfully added into a production queue, false otherwise</returns>
        private bool AddTriggeredProducibleToProducitonGroupQueue(BoxLastProducible producible)
        {
            if (!producible.TimeTriggered.HasValue || String.IsNullOrEmpty(producible.ProductionGroupAlias))
            {
                return false;
            }

            //Trigger the carton
            ConcurrentList<BoxLastProducible> pgRequests;
            if (!ProductionGroupQueues.TryGetValue(producible.ProductionGroupAlias, out pgRequests))
                return false; // pgQueue does not exist so return

            if(producible.ProducibleStatus == ErrorProducibleStatuses.NotProducible)
                UpdateProducibleStatus(producible, ErrorProducibleStatuses.NotProducible);
            else
                UpdateProducibleStatus(producible, NotInProductionProducibleStatuses.ProducibleStaged);

            lock (pgRequests)
            {
                logger.Log(LogLevel.Trace, "Producible CustomerUniqueId:{0} has been added to Production Group queue{1}", producible.CustomerUniqueId, producible.ProductionGroupAlias);

                // Find the request's position in queue
                var index = 0;
                foreach (var request in pgRequests)
                {
                    // Insert the request based on time triggered. We assume time triggered correlates to its former position in queue, fix for bug 3235
                    if (producible.TimeTriggered <= request.TimeTriggered)
                    {
                        pgRequests.Insert(index, producible);
                        return true; // carton added to pgQueue so return
                    }
                    index++;
                }

                pgRequests.Insert(index, producible);
                return true;
            }
        }

        //TODO: how do cartons get back into queue?
        //On .Server Start up call OnCartonRequestChanged
        /// <summary>
        /// Returns next job for the requesting machine.
        /// </summary>
        /// <param name="machineGroup">MachineGroup to return a job for.</param>
        /// <returns>Next job for machine.</returns>
        public IProducible GetNextJobForMachineGroup(MachineGroup machineGroup)
        {
            //Get PG for machine
            var machinePg = productionGroupService.GetProductionGroupForMachineGroup(machineGroup.Id);
            var userNotification = serviceLocator.Locate<IUserNotificationService>();
            //Get next carton from queue
            var currentLocationInList = 0;
            IProducible producible = null;
            CartonOnCorrugate corrugateToUse = null;
            ConcurrentList<BoxLastProducible> pgRequests;
            BoxLastProducible currentBoxLastProducible = null;
            ProductionGroupQueues.TryGetValue(machinePg.Alias, out pgRequests);
            if (pgRequests == null)
            {
                userNotification.SendNotificationToMachineGroup(NotificationSeverity.Info, string.Format("The queue is empty."), string.Empty, machineGroup.Id);
                logger.Log(LogLevel.Debug, "failed to lookup production group specific queue for machine group " + machineGroup.Id);
                return null;
            }

            IPacksizeCutCreaseMachine packsizeCutCreaseMachine;
            //todo:kits refactor out.
            ICarton carton;
            /*Given our list let's find the next carton to send out.  
             * Loop through the list until we have one that:
             * *Is Valid
             * *Can be produced on corrugate loaded on the machine
             * *Can be sent to the machine
             * 
            */

            var failedRestrictionCount = 0;
            var missingCorrugateCount = 0;
            var failedRestr = new List<IRestriction>();

            lock (pgRequests)
            {
                //resume our search since last
                while (currentLocationInList < pgRequests.Count)
                {
                    logger.Log(LogLevel.Debug, "Iteration {0} over {1} triggered requests in ProductionGroupAlias:{2}", currentLocationInList, pgRequests.Count, machinePg.Alias);
                    currentBoxLastProducible = pgRequests[currentLocationInList];
                    producible = currentBoxLastProducible.Producible;

                    var failedRestrictions =
                        producible.Restrictions.Where(r => !restrictionResolverService.Resolve(r, machineGroup.AllMachineCapabilities)).ToList();

                    if (failedRestrictions.Any())
                    {
                        failedRestrictionCount++;
                        logger.Log(LogLevel.Info,
                            "BoxLast Selection skipped Producible:{0}({1}) because Restrictions ({2}) were not satisfied by MachineGroup",
                            producible.CustomerUniqueId, producible.Id, String.Join(", ", failedRestrictions));

                        failedRestr = failedRestr.Concat(failedRestrictions).DistinctBy(r => r.GetType()).ToList();

                        producible = null;
                        currentBoxLastProducible = null;
                        currentLocationInList++;
                        continue;
                    }

                    if (currentBoxLastProducible.ProducibleStatus != NotInProductionProducibleStatuses.ProducibleStaged)
                    {
                        logger.Log(LogLevel.Debug, "BoxLast Selection skipped Producible:{0}({1}) with status {2} because it is not in status ProducibleStaged",
                            currentBoxLastProducible.CustomerUniqueId, currentBoxLastProducible.Id, currentBoxLastProducible.ProducibleStatus);
                        currentLocationInList++;
                        producible = null;
                        continue;
                    }

                    //todo:kits refactor here can we do this with restrictions?
                    carton = producible as ICarton;
                    if (carton == null && producible is Kit)
                    {
                        carton = ((Kit)producible).ItemsToProduce.OfType<ICarton>().FirstOrDefault();
                    }
                    if (carton == null)
                    {
                        logger.Log(LogLevel.Info, "Producible {0} ({1}) was not a carton or kit with carton", producible.CustomerUniqueId, producible.Id);
                        pgRequests.RemoveAt(currentLocationInList);
                        break;
                    }


                    var corrugateRestriction = producible.Restrictions.OfType<MustProduceWithOptimalCorrugateRestriction>().FirstOrDefault();
                    var machinesInMachineGroup = machineService.Machines
                       .Where(m => m is IPacksizeCutCreaseMachine && machineGroup.ConfiguredMachines.Contains(m.Id))
                       .Cast<IPacksizeCutCreaseMachine>().ToList();

                    if (corrugateRestriction != null)
                    {
                        corrugateToUse = optimalCorrugateCalculator.GetOptimalCorrugate(
                            new List<Corrugate> { corrugateRestriction.Value.Corrugate },
                            machinesInMachineGroup,
                            carton,
                            1);
                    }

                    //if the corrugate was not found try to find the next job for this machine. Do not remove carton so some other machine in PG may select it
                    if (corrugateToUse == null)
                    {
                        missingCorrugateCount++;
                        uiCommunicationService.SendMessageToUI(new Message<IProducible>
                        {
                            MessageType = CartonMessages.NoCorrugateAvailable,
                            Data = producible,
                            MachineGroupName = machineGroup.Alias
                        });

                        currentLocationInList++;
                        logger.Log(LogLevel.Info, "Optimal Corrugate not found for machine group {0} on producible {1} ({2})", machineGroup.Id, producible.CustomerUniqueId, producible.Id);
                        producible = null;
                        continue;
                    }

                    // todo: set machine id to the actual machine that produced this box
                    carton.ProduceOnMachineId = machineGroup.Id;
                    carton.ProducedOnMachineGroupId = machineGroup.Id;

                    //todo: this should be added as a restriction to the carton not a property.
                    carton.CartonOnCorrugate = corrugateToUse;

                    packsizeCutCreaseMachine = machinesInMachineGroup.FirstOrDefault(m => m.Tracks.Any(t => t.LoadedCorrugate.Id == corrugateToUse.Corrugate.Id));
                    //todo: this should be moved into the MG workflow
                    carton.TrackNumber = packsizeCutCreaseMachine.Tracks.First(track => corrugateToUse.Corrugate.Id == track.LoadedCorrugate.Id).TrackNumber;

                    logger.Log(LogLevel.Debug, "Found job after {0} iterations with a list of {1}", currentLocationInList,
                        pgRequests.Count);

                    // Valid request found, remove it from the pg queue
                    pgRequests.RemoveAt(currentLocationInList);
                    break;

                }
            } // end our lock

            if (producible == null)
            {
                if (failedRestrictionCount > 0)
                {
                    IEnumerable<IRestriction> labelRestrictions = failedRestr.OfType<BasicRestriction<string>>()
                            .Where(fr => fr.Value == "StandardLabel" || fr.Value == "PriorityLabel");

                    IEnumerable<IRestriction> printerRestrictions = failedRestr.OfType<BasicRestriction<Template>>().Union(labelRestrictions).ToList();

                    if (printerRestrictions.Any())
                    {
                        var rest = failedRestr.Except(printerRestrictions);

                        userNotification.SendNotificationToMachineGroup(NotificationSeverity.Warning, string.Format("Failed to produce {0} producibles because : {1} {2}.", failedRestrictionCount, String.Join(", ", rest), Environment.NewLine + ". Either there is no printer in the Machine Group or the printer cannot produce the label(s) because : " + String.Join(", ", printerRestrictions)), string.Empty, machineGroup.Id);
                    }
                    else
                    {
                        userNotification.SendNotificationToMachineGroup(NotificationSeverity.Warning, string.Format("Failed to produce {0} producibles because : {1}.", failedRestrictionCount, String.Join(", ", failedRestr)), string.Empty, machineGroup.Id);
                    }
                }

                if (missingCorrugateCount > 0)
                {
                    userNotification.SendNotificationToMachineGroup(NotificationSeverity.Warning, string.Format("Failed to produce {0} producibles because no optimal corrugate could be found. It may not exist in the system/machine group or there are no machines that can produce {1}", missingCorrugateCount, (missingCorrugateCount > 1 ? "them" : "it")), string.Empty, machineGroup.Id);
                }

                logger.Log(LogLevel.Info, "Job not found for MachineGroupId:{0} out of {1} triggered jobs in ProductionGroup:{2}",
                    machineGroup.Id, pgRequests.Count, machinePg.Alias);
                return null;
            }


            // todo: create history for ProducedOnMachineGroupId property
            currentBoxLastProducible.ProducedOnMachineGroupId = machineGroup.Id;
            producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;

            producibleStatusesDisposables.GetOrAdd(producible.Id, producible.ProducibleStatusObservable.DurableSubscribe(p =>
            {

                logger.Log(LogLevel.Debug, "BL ProducibleStatusChanged {0}, Status:{1}", currentBoxLastProducible, p);
                currentBoxLastProducible.ProducibleStatus = p;
                if (p == ProducibleStatuses.ProducibleCompleted)
                {
                    RemoveItemFromMachineGroup(machineGroup.Id, currentBoxLastProducible);
                    producibleStatusesDisposables[producible.Id].Dispose();

                    IDisposable subscription;
                    producibleStatusesDisposables.TryRemove(producible.Id, out subscription);
                    if (subscription != null)
                    {
                        logger.Log(LogLevel.Debug, "BoxLast: Disposed subscription for {0} last status {1}", producible, p);
                        subscription.Dispose();
                    }
                }
                else if (p == ProducibleStatuses.ProducibleRemoved || p == ErrorProducibleStatuses.ProducibleFailed || p == NotInProductionProducibleStatuses.ProducibleStaged)
                {
                    DisposeSubscriptionForProducible(producible, p);

                    RemoveItemFromMachineGroup(machineGroup.Id, currentBoxLastProducible);

                    if(p == ProducibleStatuses.ProducibleRemoved)
                        SelectionAlgorithmHelper.CheckForLabelSentToPrinterAndCompleteIfSo(currentBoxLastProducible, ErrorProducibleStatuses.ProducibleRemoved);
                    if(p == ErrorProducibleStatuses.ProducibleFailed)
                        SelectionAlgorithmHelper.CheckForLabelSentToPrinterAndCompleteIfSo(currentBoxLastProducible, ErrorProducibleStatuses.ProducibleFailed);

                    if (currentBoxLastProducible.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged)
                        AddTriggeredProducibleToProducitonGroupQueue(currentBoxLastProducible);
                }
                //TODO: Do we want to handle failed producibles here?
                Persist(currentBoxLastProducible);

            }, logger));
            if (!jobsSentToMachineGroup.ContainsKey(machineGroup.Id))
                jobsSentToMachineGroup.TryAdd(machineGroup.Id, new ConcurrentList<IProducible> { currentBoxLastProducible });
            else
                jobsSentToMachineGroup[machineGroup.Id].Add(currentBoxLastProducible);

            return currentBoxLastProducible.Producible;

        }

        private void DisposeSubscriptionForProducible(IProducible producible, ProducibleStatuses p)
        {
            IDisposable subscription;
            producibleStatusesDisposables.TryRemove(producible.Id, out subscription);
            if (subscription != null)
            {
                logger.Log(LogLevel.Debug, "BoxLast: Disposed subscription for {0} last status {1}", producible, p);
                subscription.Dispose();
            }
        }

        private void UpdateProducibleStatus(BoxLastProducible producible, params ProducibleStatuses[] newStatus)
        {
            var p = producible.Producible;
            newStatus.ForEach(s => p.ProducibleStatus = s);
            producible.ProducibleStatus = p.ProducibleStatus;
        }

        private void RemoveItemFromMachineGroup(Guid id, IProducible carton)
        {
            jobsSentToMachineGroup[id].Remove(carton);
        }

        ~BoxLastSelectionAlgorithmService()
        {
            Dispose(false);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            CurrentCartonRequests.Clear();
            ProductionGroupQueues.Clear();

            if (disposing)
            {
                if (getProduciblesDisposable != null)
                    getProduciblesDisposable.Dispose();

                if (productionGroupChangedDisposible != null)
                    productionGroupChangedDisposible.Dispose();

                if (uiGetProduciblesDisposable != null)
                    uiGetProduciblesDisposable.Dispose();

                if (reproduceDisposable != null)
                    reproduceDisposable.Dispose();

                if (searchDisposable != null)
                {
                    searchDisposable.Dispose();
                }

                if (reproduceCartonsDisposable != null)
                {
                    reproduceCartonsDisposable.Dispose();
                }
            }

            disposed = true;
        }

        private void Persist(BoxLastProducible producible)
        {
            try
            {
                boxLastRepository.Create(producible);
            }
            catch (Exception)
            {
                boxLastRepository.Update(producible);
            }
        }

        public IReadOnlyCollection<IProducible> GetProducibles(ProducibleStatuses status = null)
        {
            throw new NotImplementedException();
        }
    }
}