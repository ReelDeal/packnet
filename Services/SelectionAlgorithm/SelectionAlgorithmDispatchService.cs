﻿using System.Reactive.Linq;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PackNet.Services.SelectionAlgorithm
{
    public class SelectionAlgorithmDispatchService : ISelectionAlgorithmDispatchService
    {
        private readonly SelectionAlgorithmWorkflowDispatcher dispatcher;
        private readonly ILogger logger;
        private readonly IDisposable importDisposable;
        private bool disposed;

        public string Name { get { return "SelectionAlgorithmService"; } }

        public SelectionAlgorithmDispatchService(IEventAggregatorSubscriber subscriber, 
            SelectionAlgorithmWorkflowDispatcher dispatcher, 
            ILogger logger)
        {
            this.dispatcher = dispatcher;
            this.logger = logger;
            importDisposable = subscriber.GetEvent<ImportMessage<IEnumerable<IProducible>>>()
                .Where(msg => msg.MessageType == MessageTypes.DataImported || msg.MessageType == MessageTypes.Restaged)
                .DurableSubscribeOnTaskPool(message => DispatchStagingWorkflow(message.Data, message.SelectionAlgorithmType), logger);

           
        }

        /// <summary>
        /// Dispatches the Importable to the corresponding workflow that will publish the output on the event aggregator.
        /// </summary>
        /// <param name="producibles"></param>
        /// <param name="selectionAlgorithmTypes"></param>
        /// <param name="messageType"></param>
        /// <exception cref="Exception">Throws an Exception if the corresponding workflow for this Importable.ImportType cannot be found</exception>
        /// <remarks>
        /// The corresponding workflow is found by a case-insensitive match of the IWorkflowImport.Name to the importable.ImportType
        /// </remarks>
        public void DispatchStagingWorkflow(IEnumerable<IProducible> producibles, SelectionAlgorithmTypes selectionAlgorithmTypes)
        {
            dispatcher.DispatchStagingWorkflow(producibles, selectionAlgorithmTypes);
        }

        /// <summary>
        /// Dispatches the "Select Producible" workflow for a given machine group and producible type
        /// </summary>
        /// <param name="machineGroupId"></param>
        /// <returns>The Producible that the machine group should Produce</returns>
        public IProducible DispatchSelectProducibleWorkflow(MachineGroup machineGroupId)
        {
            return dispatcher.DispatchSelectProducibleWorkflow(machineGroupId);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="SelectionAlgorithmDispatchService"/> class.
        /// </summary>
        ~SelectionAlgorithmDispatchService()
        {
            Dispose(false);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        private void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                importDisposable.Dispose();
            }

            disposed = true;
        }
    }
}
