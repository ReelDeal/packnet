﻿using System;
using System.Reactive.Linq;
using System.Reflection;

using PackNet.Business.Settings;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Services.Properties;

using PackNetServerSettings = PackNet.Common.Interfaces.DTO.Settings.PackNetServerSettings;

namespace PackNet.Services
{
	public class PackNetServerSettingsService : IPackNetServerSettingsService
	{
		private readonly IPackNetServerSettings packNetServerSettings;

		public string Name { get { return "PackNet Server Settings Service"; } }

		public PackNetServerSettingsService(IPackNetServerSettings packNetServerSettings, 
			IEventAggregatorSubscriber subscriber,
			IUICommunicationService uiCommunicationService,
			ILogger logger)
		{
			this.packNetServerSettings = packNetServerSettings;

			uiCommunicationService.RegisterUIEventWithInternalEventAggregator(PackNetServerSettingsMessages.GetSettings);

			subscriber.GetEvent<IMessage>()
				.Where(m => m.MessageType == PackNetServerSettingsMessages.GetSettings)
				.DurableSubscribe(
					m =>
					{
						PackNetServerSettings data = GetSettings();

						var response = new Message<PackNetServerSettings>
						{
							MessageType = PackNetServerSettingsMessages.ServerSettings,
							Data = data,
							ReplyTo = m.ReplyTo
						};

						uiCommunicationService.SendMessageToUI(response);
					}, logger);

			uiCommunicationService.RegisterUIEventWithInternalEventAggregator<PackNetServerSettings>(PackNetServerSettingsMessages.UpdateSettings);

			subscriber.GetEvent<IMessage<PackNetServerSettings>>()
				.Where(m => m.MessageType == PackNetServerSettingsMessages.UpdateSettings)
				.DurableSubscribe(
					m =>
					{
						var result = ResultTypes.Success;
						PackNetServerSettings data = null;
						try
						{
							data = UpdateSettings(m.Data);
						}
						catch
						{
							result = ResultTypes.Fail;
						}

						var response = new ResponseMessage<PackNetServerSettings>
						{
							MessageType = PackNetServerSettingsMessages.ServerSettingsUpdated,
							Result = result,
							Data = data,
							ReplyTo = m.ReplyTo
						};

						uiCommunicationService.SendMessageToUI(response, true);
					}, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SearchConfiguration>(PackNetServerSettingsMessages.GetSearchConfiguration);

            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == PackNetServerSettingsMessages.GetSearchConfiguration)
                .DurableSubscribe(
                    m =>
                    {
                        var response = new Message<SearchConfiguration>
                        {
                            MessageType = PackNetServerSettingsMessages.SearchConfiguration,
                            Data = GetSearchConfiguration(),
                            ReplyTo = m.ReplyTo
                        };

                        uiCommunicationService.SendMessageToUI(response, true);
                    }, logger);
            
			uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MessageTypes.GetVersionInfo);

			MessageTypes.GetVersionInfo
				.OnMessage(subscriber, logger,
					msg =>
					{
						var version = Assembly.GetEntryAssembly().GetName().Version;
						uiCommunicationService.SendMessageToUI(new Message<Version>
						{
							MessageType = MessageTypes.VersionInfo,
							ReplyTo = msg.ReplyTo,
							Data = version
						}, true);
					});
		}

		public PackNetServerSettings UpdateSettings(PackNetServerSettings settings)
		{
			return packNetServerSettings.Update(settings);
		}

		public PackNetServerSettings GetSettings()
		{
			return packNetServerSettings.GetSettings();
		}

	    public SearchConfiguration GetSearchConfiguration()
	    {
	        return new SearchConfiguration
	        {
                MaxBoxFirstSearchResults = Settings.Default.MaxBoxFirstSearchResults,

                MaxBoxLastSearchResults = Settings.Default.MaxBoxFirstSearchResults,
                MaxOrdersSearchResults = Settings.Default.MaxBoxFirstSearchResults,
                MaxScanToCreateSearchResults = Settings.Default.MaxBoxFirstSearchResults,
	        };
	    }

	    public void Dispose()
		{
		}
	}
}
