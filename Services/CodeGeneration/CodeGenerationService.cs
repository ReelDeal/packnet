﻿using System.Reactive.Linq;
using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Data.ComponentsConfigurations;

namespace PackNet.Services.CodeGeneration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using System.Collections.Concurrent;

    using Business.CodeGenerator.LongHeadPositioner;
    using Business.EmJobCreator;
    using Business.IqFusionJobCreator;
    using Common.Interfaces.DTO.Carton;
    using Common.Interfaces.DTO.Machines;
    using Common.Interfaces.Enums;
    using Common.Interfaces.Eventing;
    using Common.Interfaces.Logging;
    using Common.Interfaces.Machines;
    using Common.Interfaces.Producible;
    using Common.Interfaces.Services;
    using Common.Interfaces.Services.Machines;

    using Business.Orders;
    using Common.Interfaces.DTO.Orders;
    using Common.Interfaces.RestrictionsAndCapabilities.CanProduce;

    public class CodeGenerationService : ICodeGenerationService
    {
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ILogger logger;
        private readonly IAggregateMachineService machineService;
	    private readonly IUserNotificationService userNotificationService;
        private readonly IPackagingDesignManager designManager;

        private readonly IDisposable startupCompletedDisposable;

        private readonly ConcurrentDictionary<Guid, IMachineInstructionCreator<FusionPhysicalMachineSettings>> fusionCodegeneratorDictionary;
        private readonly ConcurrentDictionary<Guid, IMachineInstructionCreator<EmPhysicalMachineSettings>> emCodegeneratorDictionary;

        public CodeGenerationService(IServiceLocator serviceLocator,
                                IPackagingDesignManager designManager)
        {
            this.designManager = designManager;

            serviceLocator.Locate<IEventAggregatorPublisher>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();

            logger = serviceLocator.Locate<ILogger>();
            machineService = serviceLocator.Locate<IAggregateMachineService>();

			userNotificationService = serviceLocator.Locate<IUserNotificationService>();

            fusionCodegeneratorDictionary = new ConcurrentDictionary<Guid, IMachineInstructionCreator<FusionPhysicalMachineSettings>>();
            emCodegeneratorDictionary = new ConcurrentDictionary<Guid, IMachineInstructionCreator<EmPhysicalMachineSettings>>();

            startupCompletedDisposable = subscriber.GetEvent<Message<IMachine>>()
                .Where(m => m.MessageType == MachineMessages.StartupProcedureCompleted)
                .DurableSubscribe(m => ResetCodegeneratorForMachine(m.Data), logger);

            serviceLocator.RegisterAsService(this);
        }

        private void ResetCodegeneratorForMachine(IMachine machine)
        {            
            if (machine is EmMachine)
            {
                emCodegeneratorDictionary.GetOrAdd(machine.Id,
                    new EmMachineInstructionCreator(logger)).ResetCodeGenerator();
            }
            else if (machine is FusionMachine)
            {
                fusionCodegeneratorDictionary.GetOrAdd(
                    machine.Id,
                    new FusionMachineInstructionCreator(logger)).ResetCodeGenerator();
            }
        }
        
        public bool CanProduce(IProducible producible, IPacksizeCutCreaseMachine machine)
        {
            var carton = producible as ICarton;
            
            if (carton == null)
                return false;

            var corrugateRestriction =  GetRestrictionsFromCartonByType<CanProduceWithCorrugateRestriction>(carton);
            var rotationRestriction = GetRestrictionsFromCartonByType<CanProduceWithRotationRestriction>(carton);
            var tileCountRestriction = GetRestrictionsFromCartonByType<CanProduceWithTileCountRestriction>(carton);
            var requests = Enumerable.Range(0, tileCountRestriction.Value).Select(i => carton).ToList();
            var packaging = OrderHelpers.MergeDesignsSideBySide(requests, corrugateRestriction.Value, true, designManager, rotationRestriction.Value);

            if (machine is FusionMachine)
            {
                return CanProducePackagingForFusionMachine(machine as FusionMachine, corrugateRestriction, carton, packaging);
            }

            if (machine is EmMachine)
            {
                return CanProducePackagingForEmMachine(machine as EmMachine, corrugateRestriction, carton, packaging);
            }

            return false;
        }

        private static T GetRestrictionsFromCartonByType<T>(ICarton carton) where T : class
        {
            return carton.Restrictions.Single(r => r is T) as T;
        }

        private bool CanProducePackagingForFusionMachine(FusionMachine machine,
            CanProduceWithCorrugateRestriction corrugateRestriction, ICarton carton, PhysicalDesign packaging)
        {
            var creator = fusionCodegeneratorDictionary.GetOrAdd(
                machine.Id,
                new FusionMachineInstructionCreator(logger));

            var maxLhPos = (machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters.MaximumPosition;           
            return creator.CanProducePackagingFor(new CanProducePackagingForParameters
            {
                Corrugate = corrugateRestriction.Value,
                PackagingDesign = packaging
            }, machine.PhysicalMachineSettings as FusionPhysicalMachineSettings, machine.NumTracks, maxLhPos);
        }

        private bool CanProducePackagingForEmMachine(EmMachine machine,
            CanProduceWithCorrugateRestriction corrugateRestriction, ICarton carton, PhysicalDesign packaging)
        {
            var creator = emCodegeneratorDictionary.GetOrAdd(
                machine.Id,
                new EmMachineInstructionCreator(logger));
            //EM machine can create same carton on any track therefore we only need to evaluate 1 track
            var maxLhPos = (machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters.MaximumPosition;
            return creator.CanProducePackagingFor(new CanProducePackagingForParameters
            {
                Corrugate = corrugateRestriction.Value,
                PackagingDesign = packaging
            }, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, 1, maxLhPos);
        }

        public IEnumerable<IInstructionItem> GenerateCode(Guid machineId, IProducible producible)
        {
            var carton = producible as ICarton;
            if (carton == null)
                return null;

            IEnumerable<ICarton> requests;
            if (producible is TiledCarton)
                requests = (producible as TiledCarton).Tiles;
            else
                requests = new [] { carton };

            PhysicalDesign design = OrderHelpers.MergeDesignsSideBySide(requests, carton.CartonOnCorrugate.Corrugate,
                true, designManager, carton.CartonOnCorrugate.Rotated);

            IEnumerable<IInstructionItem> instructions = null;
            if (machineService.Machines.All(m => m.Id != machineId))
            {
                logger.Log(LogLevel.Info, string.Format("Got a code generation request for a machine that doesn't exist (ID: {0})", machineId));
                return instructions;
            }

            var machine = machineService.Machines.First(m => m.Id == machineId);

            if (machine != null)
            {
                if (machine.MachineType == MachineTypes.Em)
                    instructions = GetEmCodeForProducible(carton, design, machine as EmMachine);
                else if (machine.MachineType == MachineTypes.Fusion)
                    instructions = GetFusionCodeForProducible(carton, design, machine as FusionMachine);
                else
                    logger.Log(LogLevel.Info, string.Format("Got a code generation request for an unsupported machine type (ID: {0})", machineId));
            }
            else
                logger.Log(LogLevel.Info, string.Format("Got a code generation request for a machine that doesn't exist (ID: {0})", machineId));
            
            return instructions;
        }

        private FusionMachineInstructionCreator GetDefaultFusionMachineInstructionCreator()
        {
            return new FusionMachineInstructionCreator(logger);
        }

        private IEnumerable<IInstructionItem> GetEmCodeForProducible(IProducible producible, PhysicalDesign design, EmMachine machine)
        {
            IEnumerable<IInstructionItem> instructions = null;
            if (producible as ICarton != null)
            {
                var carton = (producible as ICarton);
                try
                {
                    instructions = GetGeneratedEmCodeForProducible(carton, design, machine);
                }
                catch (LongHeadDistributionException)
                {
                    LogFailedCodeGeneration(machine.Alias, carton);
                }
            }
            else
                logger.Log(LogLevel.Info, string.Format("Got a code generation request for a non IPacksizeCarton: {0} for EM machine {1}", producible.Id, machine.Id));
            return instructions;
        }

        private IEnumerable<IInstructionItem> GetFusionCodeForProducible(IProducible producible, PhysicalDesign design, FusionMachine machine)
        {
            IEnumerable<IInstructionItem> instructions = null;
            if (producible as ICarton != null)
            {
                var carton = (producible as ICarton);

                try
                {
                    instructions = GetGeneratedFusionCodeForProducible(carton, design, machine);
                }
                catch (LongHeadDistributionException)
                {
                    LogFailedCodeGeneration(machine.Alias, carton);
                }
            }
            else
                logger.Log(LogLevel.Info, string.Format("Got a code generation request for a non IPacksizeCarton: {0} for fusion machine {1}", producible.Id, machine.Id));
            return instructions;
        }

        private IEnumerable<IInstructionItem> GetGeneratedFusionCodeForProducible(ICarton carton, PhysicalDesign design, FusionMachine machine)
        {
            return
                fusionCodegeneratorDictionary.GetOrAdd(machine.Id, GetDefaultFusionMachineInstructionCreator())
                    .GetInstructionListForJob(carton, design, machine.Tracks.Single(t => t.TrackNumber == carton.TrackNumber), machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);
        }
        
        private IEnumerable<IInstructionItem> GetGeneratedEmCodeForProducible(ICarton carton, PhysicalDesign design, EmMachine machine)
        {
            return
                emCodegeneratorDictionary.GetOrAdd(machine.Id, GetDefaultEmMachineInstructionCreator())
                    .GetInstructionListForJob(carton, design, machine.Tracks.Single(t => t.TrackNumber == carton.TrackNumber), machine.PhysicalMachineSettings as EmPhysicalMachineSettings);
        }

        private EmMachineInstructionCreator GetDefaultEmMachineInstructionCreator()
        {
            return new EmMachineInstructionCreator(logger);
        }

        private void LogFailedCodeGeneration(string machineName, ICarton carton)
        {
            var message = string.Format(
                "Could not place longheads for producible {0} ({8}) on machine {1}\r\nTrack number: {2}, Corrugate: {3}, DesignId: {4}, Length: {5}, Width: {6}, Height: {7}",
                carton.CustomerUniqueId,
                machineName,
                carton.TrackNumber,
                carton.CartonOnCorrugate.Corrugate,
                carton.DesignId,
                carton.Length,
                carton.Width,
                carton.Height,
                carton.Id);
            
			logger.Log(LogLevel.Warning, message);
            
			userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, message, string.Empty, carton.ProducedOnMachineGroupId);
        }

        public void Dispose()
        {
            if (startupCompletedDisposable != null)
                startupCompletedDisposable.Dispose();
        }
        
        public string Name { get { return "PackNet.Server Code generation service."; } }
    }
}
