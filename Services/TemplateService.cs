﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using MongoDB.Driver;

using PackNet.Business.Templates;
using PackNet.Business.UserNotifications;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services
{
    public class TemplateService : ITemplateService
    {
        private readonly ITemplates templates;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly ILogger logger;

        public string Name { get { return "Template Service"; } }

        public TemplateService(
            ITemplates templates,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            IUICommunicationService uiCommunicationService,
            ILogger logger,
            IServiceLocator serviceLocator)
        {
            this.templates = templates;
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.uiCommunicationService = uiCommunicationService;
            this.logger = logger;

            // Get Templates
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(TemplateMessages.GetTemplates);

            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == TemplateMessages.GetTemplates)
                .DurableSubscribe(
                    m =>
                    {
                        IEnumerable<Template> data = this.templates.GetAll();

                        var response = new Message<IEnumerable<Template>>
                        {
                            MessageType = TemplateMessages.Templates,
                            Data = data,
                            ReplyTo = m.ReplyTo
                        };

                        uiCommunicationService.SendMessageToUI(response);
                    }, logger);

            serviceLocator.RegisterAsService(this);
        }

        public void Dispose()
        {
        }

        public Template FindByName(string name)
        {
            return templates.Get(name);
        }

        public Template FindById(Guid id)
        {
            return templates.GetAll().SingleOrDefault(t => t.Id == id);
        }
    }
}