﻿using System;
using System.Collections.Generic;

using PackNet.Business.Machines;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Printing;

namespace PackNet.Services.PrinterServices
{
    public class ZebraPrinterMachineService : IZebraPrinterMachineService
    {
        private readonly IZebraPrintMachines zebraPrintMachines;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IServiceLocator serviceLocator;
        private readonly ILogger logger;
        private IAggregateMachineService machineService;
        private IMachineGroupService machineGroupService;

        public string Name { get { return "ZebraPrintMachineService"; } }

        private IMachineGroupService MachineGroupService
        {
            get { return machineGroupService ?? (machineGroupService = serviceLocator.Locate<IMachineGroupService>()); }
        }

        public ZebraPrinterMachineService(IZebraPrintMachines zebraPrintMachines,
            IUICommunicationService uiCommunicationService,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            IServiceLocator serviceLocator,
            ILogger logger)
        {
            this.zebraPrintMachines = zebraPrintMachines;
            this.uiCommunicationService = uiCommunicationService;
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.serviceLocator = serviceLocator;
            this.logger = logger;

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ZebraPrinter>(ZebraPrinterMessages.CreateZebraPrinter);
            ZebraPrinterMessages.CreateZebraPrinter
                .OnMessage<ZebraPrinter>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        ZebraPrinter createdZebraPrinter = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachineConfiguration(msg.Data);
                            
                            if (result == ResultTypes.Success)
                                createdZebraPrinter = Create(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<ZebraPrinter>
                        {
                            MessageType = MachineMessages.MachineCreated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = createdZebraPrinter
                        });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ZebraPrinter>(ZebraPrinterMessages.DeleteZebraPrinter);
            ZebraPrinterMessages.DeleteZebraPrinter
                .OnMessage<ZebraPrinter>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        try
                        {
                            Delete(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is RelationshipExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<ZebraPrinter>
                        {
                            MessageType = MachineMessages.MachineDeleted,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = msg.Data
                        });
                        publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<ZebraPrinter>(ZebraPrinterMessages.UpdateZebraPrinter);
            ZebraPrinterMessages.UpdateZebraPrinter
                .OnMessage<ZebraPrinter>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        ZebraPrinter updatedZebraPrinter = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachineConfiguration(msg.Data);
                            
                            if (result== ResultTypes.Success)
                                updatedZebraPrinter = Update(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<ZebraPrinter>
                        {
                            MessageType = MachineMessages.MachineUpdated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = updatedZebraPrinter
                        });
                    }
                );

            machineService = serviceLocator.Locate<IAggregateMachineService>();
            machineService.RegisterMachineService(this);
        }

        public void Dispose()
        {
            zebraPrintMachines.Dispose();
        }

        public IEnumerable<ZebraPrinter> Machines { get { return zebraPrintMachines.GetMachines(); } }

        public void Produce(Guid machineId, IProducible producible)
        {
            zebraPrintMachines.Produce(machineId, producible);
        }

        public IObservable<ZebraPrinter> MachineErrorOccuredObservable { get; private set; }

        public bool CanProduce(Guid machineId, IProducible producible)
        {
            throw new NotImplementedException();
        }

        public ZebraPrinter Create(ZebraPrinter machine)
        {
            machine = zebraPrintMachines.Create(machine);
            return machine;
        }

        public void Delete(ZebraPrinter machine)
        {
            zebraPrintMachines.Delete(machine);
        }

        public ZebraPrinter Update(ZebraPrinter machine)
        {
            return zebraPrintMachines.Update(machine);
        }

        public IEnumerable<ZebraPrinter> GetMachines()
        {
            return zebraPrintMachines.GetMachines();
        }

        public ZebraPrinter Find(Guid machineId)
        {
            return zebraPrintMachines.Find(machineId);
        }
    }
}
