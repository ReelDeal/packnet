﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using PackNet.Business.Corrugates;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services
{
    public class CorrugateService : ICorrugateService
    {
        private readonly IOptimalCorrugateCalculator optimalCorrugateCalculator;

        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;

        private readonly ILogger logger;

        private readonly ICorrugates corrugatesManager;
        private readonly IServiceLocator serviceLocator;
        private IProductionGroupService productionGroupService;
        private readonly IAggregateMachineService machineService;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IUserNotificationService userNotificationService;
        private IDisposable createObserver;
        private IDisposable editObserver;
        private IDisposable deleteObserver;
        private IDisposable getCorrugatesObserver;
        private ConcurrentDictionary<string /*corrugate alias*/, int /*current Count*/> currentCountOfProduciblesForCorrugates = new ConcurrentDictionary<string, int>(); 

        private IProductionGroupService ProductionGroupService { get { return productionGroupService ?? (productionGroupService = serviceLocator.Locate<IProductionGroupService>()); } }
        public IEnumerable<Corrugate> Corrugates
        {
            get
            {
                return corrugatesManager.GetCorrugates().ToList();
            }
        }

        public CorrugateService(IOptimalCorrugateCalculator optimalCorrugateCalculator,
            ICorrugates corrugatesManager,
            IServiceLocator serviceLocator,
            IAggregateMachineService machineService,
            IUICommunicationService uiCommunicationService,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            ILogger logger)
        {
            this.corrugatesManager = corrugatesManager;
            this.serviceLocator = serviceLocator;
            this.machineService = machineService;
            this.uiCommunicationService = uiCommunicationService;
            this.subscriber = subscriber;
            this.publisher = publisher;
            this.optimalCorrugateCalculator = optimalCorrugateCalculator;
            this.logger = logger;
            this.userNotificationService = serviceLocator.Locate<IUserNotificationService>();
           
            serviceLocator.RegisterAsService(this);
            SetupSubscribers();
        }

        private void SetupSubscribers()
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Corrugate>(CorrugateMessages.CreateCorrugate);
            createObserver = subscriber.GetEvent<IMessage<Corrugate>>()
                .Where(m => m.MessageType == CorrugateMessages.CreateCorrugate)
                .DurableSubscribe(CreateCorrugate, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Corrugate>(CorrugateMessages.UpdateCorrugate);
            editObserver = CorrugateMessages.UpdateCorrugate
                .OnMessage<Corrugate>(subscriber, logger, UpdateCorrugate);

            //TODO: get rid of the duplication with this message
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(CorrugateMessages.GetCorrugates);
            CorrugateMessages.GetCorrugates
                .OnMessage(subscriber, logger,
                    msg => uiCommunicationService.SendMessageToUI(new Message<IEnumerable<Corrugate>>
                    {
                        MessageType = CorrugateMessages.Corrugates,
                        Data = corrugatesManager.GetCorrugates().ToList(),
                        ReplyTo = msg.ReplyTo
                    }));
            getCorrugatesObserver = subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == CorrugateMessages.GetCorrugates)
                .DurableSubscribe(GetCorrugates, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Corrugate>(CorrugateMessages.DeleteCorrugates);
            deleteObserver = subscriber.GetEvent<IMessage<Corrugate>>()
                .Where(m => m.MessageType == CorrugateMessages.DeleteCorrugates)
                .DurableSubscribe(DeleteCorrugate, logger);

            subscriber.GetEvent<Message<CartonOnCorrugate>>()
                .Where(m => m.MessageType == CorrugateMessages.CartonCanBeProducedOnCorrugate)
                .DurableSubscribe(m =>
                {
                    //Update the dictionary count with a producible item
                    currentCountOfProduciblesForCorrugates.AddOrUpdate(m.Data.Corrugate.Alias, s => 1, (s, i) => i + 1);
                    uiCommunicationService.SendMessageToUI(new Message<List<KeyValuePair<string, int>>>()
                    {
                        MessageType = CorrugateMessages.CartonsThatCanBeProducedOnCorrugates,
                        Data = currentCountOfProduciblesForCorrugates.ToList()
                    } );
                });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(CorrugateMessages.GetCartonsThatCanBeProducedOnCorrugates);
            CorrugateMessages.GetCartonsThatCanBeProducedOnCorrugates.OnMessage(subscriber, logger, m =>
            {
                uiCommunicationService.SendMessageToUI(new Message<List<KeyValuePair<string, int>>>()
                {
                    MessageType = CorrugateMessages.CartonsThatCanBeProducedOnCorrugates,
                    Data = currentCountOfProduciblesForCorrugates.ToList()
                } );
            });
        }

        private void DeleteCorrugate(IMessage<Corrugate> message)
        {
            var result = ResultTypes.Success;
            Exception exception = null;
            try
            {
                if (machineService.IsCorrugateLoadedOnAnyMachine(message.Data))
                {
                    result = CorrugateResultTypes.CorrugateCurrentlyLoadedOnMachine;
                }
                else
                {
                    ProductionGroupService.RemoveCorrugateFromProductionGroups(message.Data);
                    corrugatesManager.Delete(message.Data);
                }
            }
            catch (Exception e)
            {
                exception = e;

                result = ResultTypes.Fail;
            }

            LogResult(result, "delete", message, exception);

            var response = new ResponseMessage<Corrugate>()
            {
                MessageType = CorrugateMessages.CorrugateDeleted,
                Data = message.Data,
                Result = result,
                ReplyTo = message.ReplyTo
            };

            publisher.Publish(response);
            uiCommunicationService.SendMessageToUI(response);
            publisher.Publish(new Message() { MessageType = CorrugateMessages.GetCorrugates, UserName = message.UserName });
        }

        private void GetCorrugates(IMessage getCorrugatesMessage)
        {
            var corrugates = new List<Corrugate>();

            try
            {
                corrugates = corrugatesManager.GetCorrugates().ToList();
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to get corrugates", e);
            }

            var response = new Message<IEnumerable<Corrugate>>
            {
                MessageType = CorrugateMessages.Corrugates,
                Data = corrugates,
                ReplyTo = getCorrugatesMessage.ReplyTo,
            };

            publisher.Publish(response);
            uiCommunicationService.SendMessageToUI(response);
        }

        private void UpdateCorrugate(IMessage<Corrugate> msg)
        {
            var result = ResultTypes.Success;
            Exception exception = null;
            Corrugate updatedCorrugate = null;
            try
            {

                updatedCorrugate = corrugatesManager.Update(msg.Data, GetLoadedCorrugates());
            }
            catch (AlreadyPersistedException e)
            {
                exception = e;
                result = ResultTypes.Exists;
            }
            catch (Exception e)
            {
                exception = e;
                result = ResultTypes.Fail;
            }

            LogResult(result, "update", msg, exception);

            publisher.Publish(new Message { MessageType = MachineMessages.GetCorrugatesLoadedOnMachines });

            var response = new ResponseMessage<Corrugate>
            {
                MessageType = CorrugateMessages.CorrugateUpdated,
                Result = result,
                Data = updatedCorrugate,
                ReplyTo = msg.ReplyTo,
                Message = exception == null ? string.Empty : exception.Message
            };

            publisher.Publish(response);
            uiCommunicationService.SendMessageToUI(response);
            publisher.Publish(new Message { MessageType = CorrugateMessages.GetCorrugates, UserName = msg.UserName });
        }

        private void CreateCorrugate(IMessage<Corrugate> createCorrugateMessage)
        {
            var result = ResultTypes.Success;
            Exception exception = null;
            Corrugate created = null;
            try
            {
                created = corrugatesManager.Create(createCorrugateMessage.Data);
            }
            catch (AlreadyPersistedException e)
            {
                exception = e;
                result = ResultTypes.Exists;
            }
            catch (Exception e)
            {
                exception = e;
                result = ResultTypes.Fail;
            }

            LogResult(result, "create", createCorrugateMessage, exception);

            var response = new ResponseMessage<Corrugate>
            {
                MessageType = CorrugateMessages.CorrugateCreated,
                Result = result,
                Data = created,
                Message = result == ResultTypes.Success ? "Successfully created corrugate" : exception.Message,
                ReplyTo = createCorrugateMessage.ReplyTo
            };

            publisher.Publish(response);
            uiCommunicationService.SendMessageToUI(response);
            publisher.Publish(new Message { MessageType = CorrugateMessages.GetCorrugates, UserName = createCorrugateMessage.UserName });
        }

        public CartonOnCorrugate GetOptimalCorrugate(IEnumerable<Corrugate> corrugatesToCompare, IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate, ICarton carton,
            int allowedTileCount, bool recalculateCachedItem = false)
        {
            return optimalCorrugateCalculator.GetOptimalCorrugate(corrugatesToCompare, machinesToEvaluate, carton,
                allowedTileCount, recalculateCachedItem);
        }

        public IEnumerable<CartonOnCorrugate> GetOptimalCorrugates(IEnumerable<Corrugate> corrugatesToCompare,
            IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate,
            ICarton carton, bool recalculateCachedItem = false, int allowedTileCount = Int32.MaxValue)
        {
            return optimalCorrugateCalculator.GetOptimalCorrugates(corrugatesToCompare, machinesToEvaluate, carton,
                recalculateCachedItem);
        }

        public CartonOnCorrugate GetOptimalCorrugateForProductionGroup(IProducible producible, Common.Interfaces.DTO.ProductionGroups.ProductionGroup machinePg, int tileCount)
        {
            try
            {
                var machineGroupsInProductionGroup = serviceLocator.Locate<IMachineGroupService>().MachineGroups
                    .Where(mg => machinePg.ConfiguredMachineGroups.Contains(mg.Id)).ToList();

                var machinesInProductionGroup = machineGroupsInProductionGroup
                    .SelectMany(mg => machineService.Machines
                        .Where(m => m is IPacksizeCutCreaseMachine && mg.ConfiguredMachines.Contains(m.Id))
                        .Cast<IPacksizeCutCreaseMachine>()).ToList();

                var corrugatesInProductionGroup =
                    Corrugates.Where(c => machinePg.ConfiguredCorrugates.Contains(c.Id)).ToList();

                //Best Corrugate to use looking at all corrugates in my PG
                var optimalCorrugateToUse = GetOptimalCorrugate(
                    corrugatesInProductionGroup,
                    machinesInProductionGroup,
                    producible as ICarton,
                    tileCount);

                return optimalCorrugateToUse;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Warning, e.Message);
                userNotificationService.SendNotificationToSystem(NotificationSeverity.Warning, e.Message, "");
            }
            return null;
        }

        public CartonOnCorrugate GetOptimalCorrugateForMachineGroup(IProducible producible, MachineGroup machineGroup, int tileCount)
        {
            var machinesInMachineGroup = machineService.Machines
                .Where(m => m is IPacksizeCutCreaseMachine && machineGroup.ConfiguredMachines.Contains(m.Id))
                .Cast<IPacksizeCutCreaseMachine>().ToList();

            var corrugatesInMachineGroup = machinesInMachineGroup
                .SelectMany(machine => machine.Tracks)
                .Select(track => track.LoadedCorrugate)
                .Where(corrugate => corrugate != null)
                .DistinctBy(c => c.Id);

            //Best Corrugate to use looking at all corrugates in my Machine
            var corrugateToUse = GetOptimalCorrugate(
                corrugatesInMachineGroup,
                machinesInMachineGroup,
                producible as ICarton,
                tileCount);
            return corrugateToUse;
        }

        public void Dispose()
        {
            if (createObserver != null)
                createObserver.Dispose();
            if (editObserver != null)
                editObserver.Dispose();
            if (deleteObserver != null)
                deleteObserver.Dispose();
            if (getCorrugatesObserver != null)
                getCorrugatesObserver.Dispose();
        }

        public string Name
        {
            get { return "Corrugate Calculation Service"; }
        }

        private IEnumerable<Corrugate> GetLoadedCorrugates()
        {
            return machineService.Machines.Where(m => m is ITrackBasedCutCreaseMachine)
                .Cast<ITrackBasedCutCreaseMachine>()
                .SelectMany(m => m.Tracks)
                .Where(t => t.LoadedCorrugate != null)
                .Select(t => t.LoadedCorrugate)
                .Distinct();
        }

        private void LogResult(Enumeration result, string action, IMessage<Corrugate> msg, Exception exception = null)
        {
            LogResult(result,
                "Corrugate with alias: {0}, width: {1}, thickness: {2}, quality: {3} was " + action + "d by user: {4}",
                "Unable to " + action + " Corrugate with alias: {0}, width: {1}, thickness: {2}, quality: {3} requested by user: {4}. Exception detail: {5}",
                string.Empty,
                "Corrugate with alias: {0}, width: {1}, thickness: {2}, quality: {3} requested by user: {4} already exists",
                msg,
                exception
                );
        }

        private void LogResult(Enumeration result, string successMessage, string failMessage, string partialSuccessMessage, string existsMessage, IMessage<Corrugate> msg, Exception exception = null, Corrugate oldCorrugate = null)
        {
            switch (result.DisplayName)
            {
                case "Success":
                    if (successMessage == string.Empty)
                        return;
                    if (oldCorrugate == null)
                    {
                        logger.Log(LogLevel.Info,
                            string.Format(successMessage, msg.Data.Alias, msg.Data.Width, msg.Data.Thickness, msg.Data.Quality,
                                msg.UserName));
                    }
                    else
                    {
                        logger.Log(LogLevel.Info,
                            string.Format(successMessage, msg.Data.Alias, msg.Data.Width, msg.Data.Thickness, msg.Data.Quality,
                                msg.UserName, oldCorrugate.Alias, oldCorrugate.Width, oldCorrugate.Thickness, oldCorrugate.Quality));
                    }
                    break;
                case "Fail":
                    if (failMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Error,
                        string.Format(failMessage, msg.Data.Alias, msg.Data.Width, msg.Data.Thickness, msg.Data.Quality, msg.UserName, exception));
                    break;
                case "PartialSuccess":
                    if (partialSuccessMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Info,
                        string.Format(partialSuccessMessage, msg.Data.Alias, msg.Data.Width, msg.Data.Thickness, msg.Data.Quality, msg.UserName, exception));
                    break;
                case "Exists":
                    if (existsMessage == string.Empty)
                        return;

                    logger.Log(LogLevel.Error,
                        string.Format(existsMessage, msg.Data.Alias, msg.Data.Width, msg.Data.Thickness, msg.Data.Quality, msg.UserName));
                    break;
                default:
                    break;
            }
        }
    }
}