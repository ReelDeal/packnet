﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using PackNet.Business.ArticleManagement;

using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Articles;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services.Articles
{
    using System.Activities;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;

    using Common;
    using Common.Utils;

    public class ArticleService : IArticleService
    {
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;

        private readonly Subject<IResponseMessage<Article>> createdSubject = new Subject<IResponseMessage<Article>>();
        private readonly Subject<IResponseMessage<List<string>>> deletedSubject = new Subject<IResponseMessage<List<string>>>();
        private readonly Subject<IResponseMessage<Article>> modifiedSubject = new Subject<IResponseMessage<Article>>();
        private readonly Subject<IEnumerable<Article>> searchResultSubject = new Subject<IEnumerable<Article>>();
        private readonly Subject<IResponseMessage<Article>> failedSubject = new Subject<IResponseMessage<Article>>();
        private readonly Subject<IResponseMessage<string>> importedSubject = new Subject<IResponseMessage<string>>();
        private readonly Subject<IResponseMessage<string>> exportedSubject = new Subject<IResponseMessage<string>>(); 
        private readonly IArticles articles;
        private readonly IProductionGroupService productionGroupService;

        private readonly ILogger logger;

        private readonly IPackagingDesignManager packagingDesignManager;
        private readonly IUICommunicationService uiCommunicationService;
        private IDisposable articleListDisposable;
        private IDisposable articleDisposable;
        private IDisposable searchArticleDisposable;
        private IDisposable produceArticlesDisposable;
        private IDisposable uiGetArticleDisposable;
        private IDisposable getArticleDisposable;
        private IDisposable uiCreateArticleDisposable;
        private IDisposable createArticleDisposable;
        private IDisposable uiUpdateArticleDisposable;
        private IDisposable updateArticleDisposable;
        private IDisposable uiDeleteArticleDisposable;
        private IDisposable deleteArticleDisposable;
        private IDisposable articleScanTriggerRequestDisposable;
        private IDisposable uiSearchArticlesDisposable;
        private IDisposable uiImportArticlesDisposable;
        private IDisposable importArticlesDisposable;

        private IDisposable uiExportArticlesDisposable;
        private IDisposable exportArticlesDisposable;
        
        private bool disposed;

        private readonly IServiceLocator serviceLocator;

        public IObservable<IResponseMessage<Article>> CreatedObservable { get { return createdSubject.AsObservable(); } }
        public IObservable<IResponseMessage<List<string>>> DeletedObservable { get { return deletedSubject.AsObservable(); } }
        public IObservable<IResponseMessage<Article>> ModifiedObservable { get { return modifiedSubject.AsObservable(); } }
        public IObservable<IEnumerable<Article>> SearchResultObservable { get { return searchResultSubject.AsObservable(); } }
        public IObservable<IResponseMessage<Article>> FailedObservable { get { return failedSubject.AsObservable(); } }
        public IObservable<IResponseMessage<string>> ImportedObservable { get { return importedSubject.AsObservable(); } }
        public IObservable<IResponseMessage<string>> ExportedObservable { get { return exportedSubject.AsObservable(); } }

        public ArticleService(
            IArticles articles,
            IServiceLocator serviceLocator,
            IProductionGroupService productionGroupService,
            IPackagingDesignManager packagingDesignManager,
            IUICommunicationService uiCommunicationService,
            IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher,
            ILogger logger)
        {
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.articles = articles;
            this.productionGroupService = productionGroupService;
            this.packagingDesignManager = packagingDesignManager;
            this.uiCommunicationService = uiCommunicationService;
            this.logger = logger;
            this.serviceLocator = serviceLocator;

            SetupSubscriber();
            serviceLocator.RegisterAsService(this);
        }

        private void SetupSubscriber()
        {
            //TODO: this is probably not needed, whomever publishes should talk to the ui instead
            articleListDisposable =
                subscriber.GetEvent<IResponseMessage<List<Article>>>()
                    .ObserveOn(System.Reactive.Concurrency.Scheduler.Default)
                    .DurableSubscribe(m => uiCommunicationService.SendMessageToUI(m), logger);

            //TODO: this is probably not needed, whomever publishes should talk to the ui instead
            articleDisposable =
                subscriber.GetEvent<IResponseMessage<Article>>()
                    .ObserveOn(System.Reactive.Concurrency.Scheduler.Default)
                    .DurableSubscribe(m => uiCommunicationService.SendMessageToUI(m), logger);

            //TODO: this is probably not needed, whomever publishes should talk to the ui instead
            searchArticleDisposable =
                subscriber.GetEvent<IResponseMessage<SearchArticle>>()
                    .ObserveOn(System.Reactive.Concurrency.Scheduler.Default)
                    .DurableSubscribe(m => uiCommunicationService.SendMessageToUI(m), logger);

            produceArticlesDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<Carton>>(ArticleMessages.ProduceArticles);
            uiGetArticleDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Article>(ArticleMessages.GetArticle);
            getArticleDisposable = subscriber.GetEvent<IMessage<Article>>()
                .Where(x => x.MessageType == ArticleMessages.GetArticle)
                .DurableSubscribe(x => Find(x.Data.ArticleId), logger);

            uiCreateArticleDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Article>(ArticleMessages.CreateArticle);
            createArticleDisposable =
                subscriber.GetEvent<IMessage<Article>>()
                    .Where(x => x.MessageType == ArticleMessages.CreateArticle)
                    .DurableSubscribe(x => Create(x.Data), logger);

            uiUpdateArticleDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Article>(ArticleMessages.UpdateArticle);
            updateArticleDisposable =
                subscriber.GetEvent<IMessage<Article>>()
                    .Where(x => x.MessageType == ArticleMessages.UpdateArticle)
                    .DurableSubscribe(x => Update(x.Data.ArticleId, x.Data), logger);

            uiDeleteArticleDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<string>>(ArticleMessages.DeleteArticles);
            deleteArticleDisposable =
                subscriber.GetEvent<IMessage<List<string>>>()
                    .Where(x => x.MessageType == ArticleMessages.DeleteArticles)
                    .DurableSubscribe(x => Delete(x.Data), logger);

            uiImportArticlesDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<Dictionary<string, string>>>(ArticleMessages.ImportArticles);
            importArticlesDisposable = subscriber.GetEvent<IMessage<List<Dictionary<string, string>>>>()
                .Where(m => m.MessageType == ArticleMessages.ImportArticles)
                .DurableSubscribe(
                    m =>
                    {
                        DispatchImportArticleWorkflow(m.Data);
                        var successResponse = new ResponseMessage<string>
                                              {
                                                  MessageType = ArticleMessages.ArticleImportStarted,
                                                  Result = ResultTypes.Success,
                                                  Data = "Articles.ImportExport.ImportingArticles",
                                                  ReplyTo = m.ReplyTo
                                              };
                        uiCommunicationService.SendMessageToUI(successResponse);
                        importedSubject.OnNext(successResponse);
                    }, logger);

            uiExportArticlesDisposable = uiCommunicationService.RegisterUIEventWithInternalEventAggregator<string>(ArticleMessages.ExportArticles);
            exportArticlesDisposable = subscriber.GetEvent<IMessage<string>>()
                .Where(m => m.MessageType == ArticleMessages.ExportArticles)
                .DurableSubscribe(
                    m => DispatchExportArticleWorkflow(m.ReplyTo), logger);

            articleScanTriggerRequestDisposable = subscriber.GetEvent<ArticleScanTriggerRequest>().Subscribe(ArticleScanRequestsArticle);

            uiSearchArticlesDisposable =
                uiCommunicationService.UIEventObservable.Where(e => e.Item1 == ArticleMessages.SearchArticles)
                    .DurableSubscribe(e => Search(""), logger);

            produceArticlesDisposable =
                subscriber.GetEvent<Message<List<Carton>>>()
                    .Where(m => m.MessageType == ArticleMessages.ProduceArticles)
                    .ObserveOn(System.Reactive.Concurrency.Scheduler.Default)
                    .DurableSubscribe(
                        m =>
                        {
                            Guid machineGroupId = Guid.Empty;
                            if (m.MachineGroupId.HasValue)
                                machineGroupId = m.MachineGroupId.Value;

                            var orders = new List<IProducible>();

                            foreach (var articleProducible in m.Data)
                            {
                                var order = new Order(articleProducible, articleProducible.Quantity)
                                {
                                    OrderId = Guid.NewGuid().ToString(),
                                };

                                if (!String.IsNullOrEmpty(articleProducible.CustomerUniqueId))
                                {
                                    order.OrderId = articleProducible.CustomerUniqueId;
                                }

                                Common.Interfaces.DTO.ProductionGroups.ProductionGroup pg = null;

                                if (!String.IsNullOrEmpty(articleProducible.ProductionGroupAlias))
                                {
                                    pg = productionGroupService.FindByAlias(articleProducible.ProductionGroupAlias);
                                }
                                if (pg != null)
                                {
                                    order.Restrictions.Add(new ProductionGroupSpecificRestriction(pg.Id));
                                }
                                else if (!order.Restrictions.Any(r =>r is MachineGroupSpecificRestriction && ((MachineGroupSpecificRestriction)r).Value == machineGroupId))
                                {
                                    order.Restrictions.Add(new MachineGroupSpecificRestriction(machineGroupId));
                                }

                                orders.Add(order);
                            }

                            //This will send the order through the staging workflow.
                            publisher.Publish(new ImportMessage<IEnumerable<IProducible>>
                            {
                                ImportType = ImportTypes.Order,
                                SelectionAlgorithmType = SelectionAlgorithmTypes.Order,
                                MachineGroupId = machineGroupId,
                                Data = orders,
                                MessageType = MessageTypes.DataImported
                            });

                            //todo: not quite done
                            uiCommunicationService.SendMessageToUI(new ResponseMessage
                            {
                                MessageType = CartonMessages.CustomCartonCreated,
                                Result = ResultTypes.Success,
                                ReplyTo = m.ReplyTo,
                            });

                        }, logger);
        }

        public void ArticleScanRequestsArticle(ArticleScanTriggerRequest request)
        {
            publisher.Publish(new ArticleScanTriggerResponse(request) { Article = Find(request.ArticleId) });
        }

        public Article Create(Article article)
        {
            var responseType = ArticleMessages.ArticleCreated;
            try
            {
                var created = articles.Create(article);
                var successResponseMessage = new ResponseMessage<Article> { MessageType = responseType, Result = ResultTypes.Success, Data = created };
                createdSubject.OnNext(successResponseMessage);
                publisher.Publish(successResponseMessage);

                return created;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to create article", e);
                var resultType = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                var failedResponseMessage = new ResponseMessage<Article> { MessageType = responseType, Result = resultType, Data = article };
                failedSubject.OnNext(failedResponseMessage);
                publisher.Publish(failedResponseMessage);
                return null;
            }
        }

        public Article Update(string articleId, Article article, bool doSearch = true)
        {
            var responseType = ArticleMessages.ArticleUpdated;
            try
            {
                var updated = articles.Update(article);
                var successResponseMessage = new ResponseMessage<Article> { MessageType = responseType, Result = ResultTypes.Success, Data = updated };
                modifiedSubject.OnNext(successResponseMessage);
                publisher.Publish(successResponseMessage);

                //Trigger the search event to send the updated articles 
                if (doSearch)
                    Search("");

                return updated;
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to update article", e);
                var resultType = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                var failedResponseMessage = new ResponseMessage<Article> { MessageType = responseType, Result = resultType, Data = article };
                failedSubject.OnNext(failedResponseMessage);
                publisher.Publish(failedResponseMessage);
                return null;
            }
        }

        /// <summary>
        /// Delete the articles by id
        /// </summary>
        /// <param name="articleIdsToDelete"></param>
        public void Delete(List<string> articleIdsToDelete)
        {
            var deleted = new List<string>();
            foreach (var articleId in articleIdsToDelete)
            {
                var guid = Guid.Empty;
                if (!Guid.TryParse(articleId, out guid))
                {
                    logger.Log(LogLevel.Error, "Invalid article ID (not a valid guid): " + articleId);
                    continue;
                }
                try
                {
                    articles.Delete(guid);
                    deleted.Add(articleId);
                }
                catch (Exception e)
                {
                    logger.LogException(LogLevel.Error, "Failed to delete article: " + articleId, e);
                }

            }

            var result = ResultTypes.Success;
            if (deleted.Count == 0)
            {
                result = ResultTypes.Fail;
                failedSubject.OnNext(new ResponseMessage<Article> { Result = result });
            }
            else if (deleted.Count != articleIdsToDelete.Count)
                result = ResultTypes.PartialSuccess;

            var responseMessage = new ResponseMessage<Article> { MessageType = ArticleMessages.ArticleDeleted, Result = result };
            publisher.Publish(responseMessage);

            deletedSubject.OnNext(new ResponseMessage<List<string>> { MessageType = ArticleMessages.ArticleDeleted, Result = result, Data = deleted });

            //Trigger the search event to send the updated articles 
            Search("");
        }

        public Article Find(string articleId)
        {
            Contract.Requires(!String.IsNullOrEmpty(articleId), "articleId cannot be null or empty");
            try
            {
                var article = articles.Find(articleId);
                var designName = packagingDesignManager.GetDesignName(article.DesignId);
                article.MissingDesignFile = String.IsNullOrEmpty(designName);
                article.DesignName = article.MissingDesignFile ? String.Format("Missing file (design id: {0})", article.DesignId) : designName;

                return article;
            }
            catch (Exception ex)
            {
                logger.LogException(LogLevel.Error, string.Format("Failed to get article id:{0}", articleId), ex);
                return null;
            }
        }

     public IEnumerable<Article> Search(string articleSearchPattern)
        {
            //TODO; this is really gonna suck with lots of articles
            try
            {
                var allArticles = articles.All();
                var filteredArticles = String.IsNullOrWhiteSpace(articleSearchPattern) ? allArticles : FilterArticles(allArticles, articleSearchPattern);

                foreach (var article in filteredArticles)
                {
                    var designName = packagingDesignManager.GetDesignName(article.DesignId);
                    article.MissingDesignFile = String.IsNullOrEmpty(designName);
                    article.DesignName = article.MissingDesignFile ? String.Format("Missing file (design id: {0})", article.DesignId) : designName;
                }

                searchResultSubject.OnNext(filteredArticles);
                publisher.Publish(new ResponseMessage<SearchArticle>
                                  {
                                      MessageType = ArticleMessages.Articles,
                                      Result = ResultTypes.Success,
                                      Data = new SearchArticle { Articles = filteredArticles }
                                  });
                return filteredArticles;
            }
            catch (Exception ex)
            {
                var responseMessage = new ResponseMessage<Article> { MessageType = ArticleMessages.SearchArticle, Result = ResultTypes.Fail };
                publisher.Publish(responseMessage);
                logger.LogException(LogLevel.Error, "Failed to search articles", ex);
                failedSubject.OnNext(responseMessage);
                return null;
            }
        }

        private static List<Article> FilterArticles(IEnumerable<Article> articles, string searchPattern)
        {
            var searchPatternLowerCased = searchPattern.ToLower();
            return articles.Where(
                a => (!string.IsNullOrWhiteSpace(a.ArticleId) && a.ArticleId.ToLower().Contains(searchPatternLowerCased)) ||
                     (a.CorrugateQuality.HasValue && a.CorrugateQuality.Value.ToString(CultureInfo.InvariantCulture).Contains(searchPatternLowerCased)) ||
                     (!string.IsNullOrWhiteSpace(a.Description) && a.Description.ToLower().Contains(searchPatternLowerCased)) ||
                     a.DesignId.ToString(CultureInfo.InvariantCulture).Contains(searchPatternLowerCased) ||
                     (!string.IsNullOrWhiteSpace(a.DesignName) && a.DesignName.ToLower().Contains(searchPatternLowerCased)) ||
                     a.Height.ToString(CultureInfo.InvariantCulture).Contains(searchPatternLowerCased) ||
                     a.Length.ToString(CultureInfo.InvariantCulture).Contains(searchPatternLowerCased) ||
                     a.Width.ToString(CultureInfo.InvariantCulture).Contains(searchPatternLowerCased) ||
                     (!string.IsNullOrWhiteSpace(a.UserName) && a.UserName.ToLower().Contains(searchPatternLowerCased))).ToList();
        }

        ~ArticleService()
        {
            Dispose(false);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                if (articleListDisposable != null)
                    articleListDisposable.Dispose();
                if (articleDisposable != null)
                    articleDisposable.Dispose();
                if (searchArticleDisposable != null)
                    searchArticleDisposable.Dispose();
                if (produceArticlesDisposable != null)
                    produceArticlesDisposable.Dispose();
                if (uiGetArticleDisposable != null)
                    uiGetArticleDisposable.Dispose();
                if (getArticleDisposable != null)
                    getArticleDisposable.Dispose();
                if (uiCreateArticleDisposable != null)
                    uiCreateArticleDisposable.Dispose();
                if (createArticleDisposable != null)
                    createArticleDisposable.Dispose();
                if (uiUpdateArticleDisposable != null)
                    uiUpdateArticleDisposable.Dispose();
                if (updateArticleDisposable != null)
                    updateArticleDisposable.Dispose();
                if (uiDeleteArticleDisposable != null)
                    uiDeleteArticleDisposable.Dispose();
                if (deleteArticleDisposable != null)
                    deleteArticleDisposable.Dispose();
                if (articleScanTriggerRequestDisposable != null)
                    articleScanTriggerRequestDisposable.Dispose();
                if (uiSearchArticlesDisposable != null)
                    uiSearchArticlesDisposable.Dispose();
                if (uiImportArticlesDisposable != null)
                    uiImportArticlesDisposable.Dispose();
                if (uiExportArticlesDisposable != null)
                    uiExportArticlesDisposable.Dispose();
                if (exportArticlesDisposable != null)
                    exportArticlesDisposable.Dispose();
                if(importArticlesDisposable != null)
                    importArticlesDisposable.Dispose();
            }

            disposed = true;
        }

        private void DispatchImportArticleWorkflow(List<Dictionary<string, string>> articlesToImport)
        {
            var inArgs = new Dictionary<string, object>()
                         {
                             { "ArticlesToImport", articlesToImport },
                             { "ServiceLocator", serviceLocator}
                         };
            this.InvokeWorkflow("%PackNetWorkflows%\\Articles\\ImportArticles.xaml", inArgs, this.GetType().Assembly);
        }

        private void DispatchExportArticleWorkflow(string replyTo)
        {
            var msg = new ResponseMessage<string>() { MessageType = MessageTypes.AdminNotification, Data = "Export started" };
            exportedSubject.OnNext(msg);

            var inArgs = new Dictionary<string, object>()
                         {
                             { "ArticlesToExport", articles.All() },
                             { "ReplyTo", replyTo},
                             { "ServiceLocator", serviceLocator}
                         };
            this.InvokeWorkflow("%PackNetWorkflows%\\Articles\\ExportArticles.xaml", inArgs, this.GetType().Assembly);
        }

        private void InvokeWorkflow(string workflowPath, IDictionary<string, object> inArgs, Assembly importAssembly)
        {
            var stopWatch = Stopwatch.StartNew();

            Activity activity = null;

            try
            {
                activity = WorkflowHelper.GetActivity(DirectoryHelpers.ReplaceEnvironmentVariables(workflowPath), importAssembly);
            }
            catch (Exception exception)
            {
                var userNotificationService = serviceLocator.Locate<IUserNotificationService>();

                logger.LogException(LogLevel.Error, "Error running workflow '" + workflowPath + "' ", exception);

                userNotificationService.SendNotificationToSystem(
                    NotificationSeverity.Error,
                    String.Format("Failed to load XAML File '{0}'. Working from: {1}", workflowPath, Directory.GetCurrentDirectory()),
                    exception.ToString());

                return;
            }
            // If workflow is Aborted, Canceled or Faulted then OnError is published, which halts the sequence,
            // so it should be treated as a fatal error by handlers
            var message = string.Empty;
            //todo:workflows - can we move this execution into the workflowHelper?
            var wfApp = new WorkflowApplication(activity, inArgs)
            {
                Aborted = e =>
                {
                    stopWatch.Stop();
                    message = string.Format("Workflow:{0} took {3}(ms) unexpectedly aborted:{2}",
                        ((DynamicActivity)activity).Name, e.Reason, stopWatch.ElapsedMilliseconds);
                    LogAndPublishError(message);
                },
                Completed = e =>
                {
                    stopWatch.Stop();
                    if (e.CompletionState == ActivityInstanceState.Canceled)
                    {
                        message = string.Format("Workflow:{0}  took {2}(ms) unexpectedly canceled:{1}",
                            ((DynamicActivity)activity).Name, e.TerminationException, stopWatch.ElapsedMilliseconds);
                        LogAndPublishError(message);
                    }
                    if (e.CompletionState == ActivityInstanceState.Faulted)
                    {
                        message = string.Format("Workflow:{0} took {2}(ms) unexpectedly faulted:{1}",
                            ((DynamicActivity)activity).Name, e.TerminationException, stopWatch.ElapsedMilliseconds);
                        LogAndPublishError(message);
                    }
                    if (e.CompletionState == ActivityInstanceState.Closed)
                    {
                        message = string.Format("Completed workflow:{0} took {1}(ms)", ((DynamicActivity)activity).Name, stopWatch.ElapsedMilliseconds);
                        LogAndPublish(message);
                    }
                },
                OnUnhandledException = (WorkflowApplicationUnhandledExceptionEventArgs e) =>
                {
                    stopWatch.Stop();
                    message = string.Format("Workflow:{0} took {2}(ms) OnUnhandledException:{1} ExceptionSource:{3} - {4}",
                        ((DynamicActivity)activity).Name, e.UnhandledException.Message, stopWatch.ElapsedMilliseconds, e.ExceptionSource.DisplayName, e.ExceptionSourceInstanceId);
                    LogAndPublishError(message);

                    return UnhandledExceptionAction.Cancel;
                }
            };

            try
            {
                wfApp.Run();
            }
            catch (InvalidWorkflowException e)
            {
                WorkflowHelper.PublishAndLogWorkflowError(workflowPath, serviceLocator.Locate<IUserNotificationService>(), logger, e);
                throw;
            }

            message = string.Format("Started workflow:{0} with InstanceId:{1}", ((DynamicActivity)activity).Name, wfApp.Id);
            LogAndPublish(message);
        }

        private void LogAndPublish(string message)
        {
            logger.Log(LogLevel.Trace, message);
        }

        private void LogAndPublishError(string message)
        {
            logger.Log(LogLevel.Error, message);
        }

        public string Name
        {
            get { return "ArticleService"; }
        }
    }
}
