﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reactive.Linq;
using PackNet.Business.Support;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Services
{
    public class SupportIncidentService
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber eventSubscriber;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly ISupportIncidents supportIncidents;

        public SupportIncidentService(
            ISupportIncidents supportIncidents,
            IEventAggregatorPublisher publisher, 
            IEventAggregatorSubscriber eventSubscriber, 
            IUICommunicationService uiCommunicationService,
            ILogger logger)
        {
            this.publisher = publisher;
            this.eventSubscriber = eventSubscriber;
            this.uiCommunicationService = uiCommunicationService;
            this.supportIncidents = supportIncidents;

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SupportIncident>(SupportMessages.CreateSupportIncident);
            this.eventSubscriber.GetEvent<IMessage<SupportIncident>>()
                .Where(m => m.MessageType == SupportMessages.CreateSupportIncident)
                .DurableSubscribe(GenerateSupportTicket, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SupportIncidentUpdate>(SupportMessages.SupportIncidentUpdateUserMessage);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<SupportIncidentUpdate>(SupportMessages.SupportIncidentUpdateLogEntry);
            this.eventSubscriber.GetEvent<IMessage<SupportIncidentUpdate>>()
                .Where(msg=>msg.MessageType  == SupportMessages.SupportIncidentUpdateLogEntry || msg.MessageType == SupportMessages.SupportIncidentUpdateUserMessage)
                .DurableSubscribe(UpdateSupportTicket, logger);

            this.eventSubscriber.GetEvent<IMessage<SupportIncidentUpdate>>()
                .Where(msg => msg.MessageType == SupportMessages.SupportIncidentFinished)
                .DurableSubscribe(FinalizeSupportTicket, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(SupportMessages.GetAllUnsentSupportIncidents);
            this.eventSubscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == SupportMessages.GetAllUnsentSupportIncidents)
                .DurableSubscribe(GetUnsent, logger);
        }

        private void FinalizeSupportTicket(IMessage<SupportIncidentUpdate> obj)
        {
            //Unused for now
            //var wr = (HttpWebRequest)WebRequest.Create("https://hooks.slack.com/services/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");

            //wr.ContentType = "text/json";
            //wr.Method = "POST";

            //using (var sw = new StreamWriter(wr.GetRequestStream()))
            //{
            //    string json = "{\"text\": \"Username: admin, Vendor: Cabelas has reported an error! <https://alert-system.com/alerts/1234|Click here to view it!>\"}";

            //    sw.Write(json);
            //    sw.Flush();
            //    sw.Close();

            //    var httpResponse = (HttpWebResponse)wr.GetResponse();
            //    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            //    {
            //        var result = streamReader.ReadToEnd();
            //    }
            //}
        }

        private void UpdateSupportTicket(IMessage<SupportIncidentUpdate> obj)
        {
            SupportIncidentUpdate update = obj.Data;
            var incident = supportIncidents.Find(obj.Data.IncidentToUpdate);

            if (incident != null && update != null)
            {
                if (obj.MessageType == SupportMessages.SupportIncidentUpdateLogEntry)
                {
                    incident.UiLogMessages.AddRange(update.UiLogMessages);
                }
                else if (obj.MessageType == SupportMessages.SupportIncidentUpdateUserMessage)
                {
                    incident.UserMessage += update.UserMessage;
                }

                supportIncidents.Update(incident);
            }
        }

        private void GetUnsent(IMessage obj)
        {
            var result = supportIncidents.FindUnresolvedIncidents().ToList();

            var message = new ResponseMessage<IEnumerable<SupportIncident>>
            {
                ReplyTo = obj.ReplyTo,
                Data = result,
                MessageType = SupportMessages.AllUnsentSupportIncidents
            };

            uiCommunicationService.SendMessageToUI(message);
        }

        private void GenerateSupportTicket(IMessage<SupportIncident> obj)
        {
            Contract.Requires(obj != null, "Message cannot be null!");

            supportIncidents.Create(obj.Data);

            var message = new ResponseMessage<Guid>()
            {
                ReplyTo = obj.ReplyTo,
                Data = obj.Data.Id,
                MessageType = SupportMessages.SupportIncidentCreated
            };

            uiCommunicationService.SendMessageToUI(message);
        }
    }
}