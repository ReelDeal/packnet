﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;

using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Services
{
    using System.ComponentModel.Composition;

    using Common.Interfaces.DTO;
    using Common.Plugins;

    public class PrinterFactoryComposer : IPrinterFactory
    {
        [ImportMany]
        public List<IPrinterFactory> PrinterFactories { get; private set; }

        public string PluginDirectory { get { return "PrinterFactoryPlugins"; } }

        public IEnumerable<string> ProvidedPrinterTypes
        {
            get { return PrinterFactories.SelectMany(x => x.ProvidedPrinterTypes); }
        }

        public PrinterFactoryComposer(ILogger logger)
        {
            var batch = new CompositionBatch();
            batch.AddExportedValue(logger);
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            //Load Common
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(MefComposer).Assembly));
            batch.AddPart(this);
            MefComposer.Compose(PluginDirectory, batch, catalog);
        }

        public IPrinter GetPrinter(string printerTypeIdentifier, IPrinterSettings printerSettings)
        {
            var factory = PrinterFactories.FirstOrDefault(x => x.ProvidedPrinterTypes.Contains(printerTypeIdentifier.ToLower()));

            if (factory != null)
            {
                return factory.GetPrinter(printerTypeIdentifier, printerSettings);
            }

            throw new ArgumentException(String.Format("Printer type {0} has no factory present", printerTypeIdentifier));
        }

        public void Dispose()
        {
            PrinterFactories.ForEach(pf => pf.Dispose());
        }
    }
}
