﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using MongoDB.Driver;

using Newtonsoft.Json;

using PackNet.Business.UserNotifications;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services
{
	public class UserNotificationService : IUserNotificationService
	{
		private readonly IUserNotifications userNotifications;
		private readonly IEventAggregatorPublisher publisher;
		private readonly IEventAggregatorSubscriber subscriber;
		private readonly IUICommunicationService uiCommunicationService;
		private readonly IProductionGroupService productionGroupService;
		private readonly IUserService userService;
		private readonly ILogger logger;

		public string Name { get { return "User Notification Service"; } }

		public UserNotificationService(
			IUserNotifications userNotifications,
			IEventAggregatorPublisher publisher,
			IEventAggregatorSubscriber subscriber,
			IUICommunicationService uiCommunicationService,
			IProductionGroupService productionGroupService,
			IUserService userService,
			ILogger logger,
			IServiceLocator serviceLocator)
		{
			this.userNotifications = userNotifications;
			this.publisher = publisher;
			this.subscriber = subscriber;
			this.uiCommunicationService = uiCommunicationService;
			this.productionGroupService = productionGroupService;
			this.userService = userService;
			this.logger = logger;

			// Dismiss All Notifications
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(UserNotificationMessages.DismissAllUserNotificationsForMg);

			subscriber.GetEvent<IMessage<Guid>>()
                .Where(m => m.MessageType == UserNotificationMessages.DismissAllUserNotificationsForMg)
				.DurableSubscribe(
					m =>
					{
						var result = ResultTypes.Success;

						try
						{
							userNotifications.DismissAllByUser(m.Data);
						}
						catch
						{
							result = ResultTypes.Fail;
						}

						var response = new ResponseMessage
						{
							MessageType = UserNotificationMessages.DismissedAllUserNotifications,
							Result = result,
							UserName = userService.Find(m.Data).UserName
						};

						uiCommunicationService.SendMessageToUI(response, true);
					}, logger);

			uiCommunicationService.RegisterUIEventWithInternalEventAggregator<List<Guid>>(UserNotificationMessages.DismissAllUserNotifications);

			subscriber.GetEvent<IMessage<List<Guid>>>()
				.Where(m => m.MessageType == UserNotificationMessages.DismissAllUserNotifications)
				.DurableSubscribe(
					m =>
					{
						var result = ResultTypes.Success;
					    if (m.Data.Count < 2)
					    {
                            logger.Log(LogLevel.Error, "Invalid call to DismissAllUserNotifications, expected two values in the list: MachineGroup.Id and User.Id.");
					        return;
                        }
						try
						{
							userNotifications.DismissAllByUser(m.Data[0], m.Data[1]);
						}
						catch
						{
							result = ResultTypes.Fail;
						}

						var response = new ResponseMessage
						{
							MessageType = UserNotificationMessages.DismissedAllUserNotifications,
							Result = result,
							UserName = userService.Find(m.Data[1]).UserName
						};

						uiCommunicationService.SendMessageToUI(response, true);
					}, logger);

			// Dismiss Notification
			uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Tuple<Guid, Guid>>(UserNotificationMessages.DismissUserNotification);

			subscriber.GetEvent<IMessage<Tuple<Guid, Guid>>>()
				.Where(m => m.MessageType == UserNotificationMessages.DismissUserNotification)
				.DurableSubscribe(
					m =>
					{
						var result = ResultTypes.Success;

						try
						{
							userNotifications.DismissByUser(m.Data.Item1, m.Data.Item2);
						}
						catch
						{
							result = ResultTypes.Fail;
						}

						var response = new ResponseMessage<Guid>
						{
							MessageType = UserNotificationMessages.DismissedUserNotification,
							Data = m.Data.Item1,
							Result = result,
							UserName = userService.Find(m.Data.Item2).UserName
						};

						uiCommunicationService.SendMessageToUI(response, true);
					}, logger);

			// Get Active Notifications
			uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(UserNotificationMessages.GetActiveUserNotificationsByUserId);

			subscriber.GetEvent<IMessage<Guid>>()
				.Where(m => m.MessageType == UserNotificationMessages.GetActiveUserNotificationsByUserId)
				.DurableSubscribe(
					m =>
					{
						IEnumerable<UserNotification> data = this.userNotifications.FindActiveByUserId(m.Data);

						var response = new Message<IEnumerable<UserNotification>>
						{
							MessageType = UserNotificationMessages.ActiveUserNotificationsByUserId,
							Data = data,
							ReplyTo = m.ReplyTo
						};

						uiCommunicationService.SendMessageToUI(response, false, true);
					}, logger);

			// Search Notifications
			uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Tuple<DateTime, DateTime, String[], String[]>>(UserNotificationMessages.SearchUserNotifications);

			subscriber.GetEvent<IMessage<Tuple<DateTime, DateTime, String[], String[]>>>()
				.Where(m => m.MessageType == UserNotificationMessages.SearchUserNotifications)
				.DurableSubscribe(
					m =>
					{
						var notificationTypes = m.Data.Item3.Select(x => (NotificationType)x).ToList();
						var notificationSeverities = m.Data.Item4.Select(x => (NotificationSeverity)x).ToList();

						IEnumerable<UserNotification> data = this.userNotifications.SearchUserNotifications(m.Data.Item1, m.Data.Item2, notificationTypes, notificationSeverities);

						var response = new Message<IEnumerable<UserNotification>>
						{
							MessageType = UserNotificationMessages.SearchedUserNotifications,
							Data = data,
							ReplyTo = m.ReplyTo
						};

						uiCommunicationService.SendMessageToUI(response, false, true);
					}, logger);

			subscriber.GetEvent<IMessage<UserNotification>>()
			   .DurableSubscribe(m => SendNotificationToSystem(m.Data.Severity, m.Data.Message, m.Data.AdditionalInfo), logger);
			
			serviceLocator.RegisterAsService(this);
		}

		public void Dispose()
		{
		}

		/// <summary>
		/// Send a user notification that is only visible to users operating the specified machine group.
		/// </summary>
		/// <param name="severity">Notification severity.</param>
		/// <param name="message">Notification message.</param>
		/// <param name="additionalInfo">Additional information.</param>
		/// <param name="machineGroupId">Machine Group identification.</param>
		public void SendNotificationToMachineGroup(NotificationSeverity severity, string message, string additionalInfo, Guid machineGroupId)
		{
            var machineGroupIds = new ConcurrentList<Guid> { machineGroupId };

			SendNotification(NotificationType.MachineGroup, severity, message, additionalInfo, machineGroupIds);
		}

		/// <summary>
		/// Send a user notification that is only visible to users operating in machine groups that belong to the production group of the specified machine group.
		/// </summary>
		/// <param name="severity">Notification severity.</param>
		/// <param name="message">Notification message.</param>
		/// <param name="additionalInfo">Additional information.</param>
		/// <param name="machineGroupId">Machine Group identification.</param>
		public void SendNotificationToAllMachineGroupsInMySameProductionGroup(NotificationSeverity severity, string message, string additionalInfo, Guid machineGroupId)
		{
			var machineGroupIds = productionGroupService.GetProductionGroupForMachineGroup(machineGroupId).ConfiguredMachineGroups;

			SendNotification(NotificationType.ProductionGroup, severity, message, additionalInfo, machineGroupIds);
		}

		/// <summary>
		/// Send a user notification that is only visible to users operating in machine groups that belong to the production group.
		/// </summary>
		/// <param name="severity">Notification severity.</param>
		/// <param name="message">Notification message.</param>
		/// <param name="additionalInfo">Additional information.</param>
		/// <param name="productionGroupId">Production Group identification.</param>
		public void SendNotificationToProductionGroup(NotificationSeverity severity, string message, string additionalInfo, Guid productionGroupId)
		{
			var pg = productionGroupService.ProductionGroups.FirstOrDefault(p => p.Id == productionGroupId);
			
			if (pg == null)
			{
				logger.Log(LogLevel.Error, "Send notification to PG failed. ProductionGroupId:{0} not found.", productionGroupId);
				return;
			}
			
			var machineGroupIds = pg.ConfiguredMachineGroups;

			SendNotification(NotificationType.ProductionGroup, severity, message, additionalInfo, machineGroupIds);
		}

		/// <summary>
		/// Send a user notification for all users.
		/// </summary>
		/// <param name="severity">Notification severity.</param>
		/// <param name="message">Notification message.</param>
		/// <param name="additionalInfo">Additional information.</param>
		public void SendNotificationToSystem(NotificationSeverity severity, string message, string additionalInfo)
		{
            var machineGroupIds = new ConcurrentList<Guid>();

			SendNotification(NotificationType.System, severity, message, additionalInfo, machineGroupIds);
		}

		/// <summary>
		/// Send user notification.
		/// </summary>
		/// <param name="type">Notification type.</param>
		/// <param name="severity">Notification severity.</param>
		/// <param name="message">Notification message.</param>
		/// <param name="additionalInfo">Additional information.</param>
		/// <param name="machineGroupIds">Machine group identifications that will have visibility of the notification. If empty, it is visible to everybody.</param>
        private void SendNotification(NotificationType type, NotificationSeverity severity, string message, string additionalInfo, ConcurrentList<Guid> machineGroupIds)
		{
			var notifiedUserIds = new ConcurrentList<Guid>(userService.Users.Select(u => u.Id).ToList());

			var userNotification = new UserNotification
			{
				NotificationType = type,
				Severity = severity,
				Message =  message,
				AdditionalInfo =  additionalInfo,
				NotifiedMachineGroups = machineGroupIds,
				NotifiedUsers = notifiedUserIds
			};

			try
			{
				userNotification = userNotifications.Create(userNotification);

				var response = new Message<UserNotification>
				{
					MessageType = UserNotificationMessages.PublishedUserNotification,
					Data = userNotification
				};

				uiCommunicationService.SendMessageToUI(response);
			}
			catch (Exception e)
			{
				logger.LogException(LogLevel.Error, "Failed to create user notification", e);	
			}
		}
	}
}