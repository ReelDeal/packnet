﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Reactive.Linq;

using PackNet.Business.DatabaseTask;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services
{
    public class DatabaseTasksService : IDatabaseTasksService
    {
        private readonly IServiceLocator serviceLocator;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IDatabaseTasks databaseTasks;
        private readonly ILogger logger;

        public DatabaseTasksService(IServiceLocator serviceLocator, IEventAggregatorSubscriber subscriber, IUICommunicationService uiCommunicationService, IDatabaseTasks databaseTasks, ILogger logger)
        {
            this.serviceLocator = serviceLocator;
            this.subscriber = subscriber;
            this.uiCommunicationService = uiCommunicationService;
            this.databaseTasks = databaseTasks;

            this.logger = logger;

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(DatabaseTaskMessages.DatabaseBackupRequested);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(DatabaseTaskMessages.DatabaseRestoreFilesRequested);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Tuple<string, bool>>(DatabaseTaskMessages.RestoreDatabase);

            databaseTasks.Messaging.Subscribe(SendMessageToUi);

            WireEvents();

            serviceLocator.RegisterAsService(this);
        }

        private void WireEvents()
        {
            subscriber.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == DatabaseTaskMessages.DatabaseBackupRequested)
                .DurableSubscribe(BackupDatabase, logger);

            subscriber.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == DatabaseTaskMessages.DatabaseRestoreFilesRequested)
                .DurableSubscribe(GetDatabaseRestoreFiles, logger);

            subscriber.GetEvent<IMessage<Tuple<string, bool>>>()
                .Where(msg => msg.MessageType == DatabaseTaskMessages.RestoreDatabase)
                .DurableSubscribe(RestoreDatabase, logger);
        }

        private void RestoreDatabase(IMessage<Tuple<string, bool>> obj)
        {
            databaseTasks.RestoreDatabase(obj.Data.Item1, obj.ReplyTo, obj.Data.Item2);        
        }

        public void Dispose()
        {

        }

        public string Name
        {
            get { return "Database Tasks Service"; }
        }

        private void GetDatabaseRestoreFiles(IMessage msg)
        {
            var restoreFiles = databaseTasks.GetDatabaseRestoreFiles();

            uiCommunicationService.SendMessageToUI(new Message<List<Tuple<String, String>>>()
            {
                MessageType = DatabaseTaskMessages.DatabaseRestoreFilesResponse,
                ReplyTo = msg.ReplyTo,
                Data = restoreFiles
            });
        }

        public void BackupDatabase(IMessage message)
        {
            databaseTasks.BackupDatabase(message.ReplyTo);
        }

        private void SendMessageToUi(IMessage<DatabaseTask> msg)
        {
            uiCommunicationService.SendMessageToUI(msg);
        }


    }
}