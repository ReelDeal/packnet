//using PackNet.Common.Interfaces.DTO.PrintingMachines;
//using PackNet.Common.Interfaces.Logging;

//namespace PackNet.Services
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Diagnostics;
//    using System.Linq;
//    using System.Reactive.Linq;
//    using System.Reactive.Subjects;

//    using Common.Interfaces.Enums;
//    using Common.Interfaces.Eventing;
//    using Common.Interfaces.Exceptions;
//    using Common.Interfaces.Services;

//    public class PrintService : IPrintService
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        private readonly Dictionary<IPrinter /*Printer*/, bool/* have we wired up?*/> printers;

//        private readonly IEventAggregatorPublisher publisher;

//        protected readonly ILogger logger;

//        /// <summary>
//        /// Tracks the unique printer requests within a batch
//        /// </summary>
//        private Dictionary<Guid /*printer request guid*/ , Guid /*request group guid*/> jobsSentToPrinter = new Dictionary<Guid, Guid>();


//        private Dictionary<Guid /*printer printerid */ , Guid /*request group guid*/> activePrintJobs = new Dictionary<Guid, Guid>();


//        /// <summary>
//        /// Keeps track of the original batch requests as <see cref="PrintJobStatus"/>
//        /// </summary>
//        private Dictionary<Guid /*request group guid*/ , Dictionary<Guid /*printer key*/, List<PrintJobStatus>> /*request printables*/> batchJobsInProgress = new Dictionary<Guid, Dictionary<Guid /*printer key*/, List<PrintJobStatus>>>();

//        private readonly Subject<PrintRequestStatus> printRequestStatusChangedSubject = new Subject<PrintRequestStatus>();

//        public PrintService(List<IPrinter> printers, IEventAggregatorPublisher publisher, ILogger logger)
//        {
//            this.printers = printers.ToDictionary(p => p, p => false);
//            this.publisher = publisher;
//            this.logger = logger;
//            logger.Log(LogLevel.Info, "PrintService created with Printers: " + printers.Aggregate("", (s, printer) => s + ", " + printer.Alias));
//        }

//        #region IPrintService Interface

//        /// <summary>
//        /// Status of a given Print Request.
//        /// </summary>
//        public IObservable<PrintRequestStatus> PrintRequestStatusChangedObservable { get { return printRequestStatusChangedSubject.AsObservable(); } }

//        /// <summary>
//        /// Request to Print multiple Printables. 
//        /// </summary>
//        /// <param name="itemsToPrint">A dictionary of the key of string which represents a unique printer name and a list of <see cref="Printable"/> to be printed on that particular printer.</param>
//        /// <returns>unique identifier for the group of Printables</returns>
//        public Guid Print(Dictionary<Guid /*printer key*/, List<Printable>> itemsToPrint)
//        {
//            ValidateInputs(itemsToPrint);

//            var batchId = Guid.NewGuid();

//            //transform the input data to a dictionary of <string, List<PrintJobStatus>>
//            var batchItemsAsPrintJobStatus = from item in itemsToPrint
//                                             let li = (from ps in item.Value
//                                                       select new PrintJobStatus { JobStatus = PrintJobStatuses.Unknown, ItemToPrint = ps })
//                                             select new KeyValuePair<Guid, List<PrintJobStatus>>(item.Key, li.ToList());

//            batchJobsInProgress.Add(batchId, batchItemsAsPrintJobStatus.ToDictionary(p => p.Key, p => p.Value));

//            foreach (var itemToPrint in itemsToPrint)
//            {
//                LookupPrinterAndPrint(itemToPrint.Key, itemsToPrint[itemToPrint.Key].First(), batchId);
//            }

//            return batchId;
//        }

//        private void ValidateInputs(Dictionary<Guid, List<Printable>> itemsToPrint)
//        {
//            if (itemsToPrint.Any(i => i.Key == Guid.Empty))
//            {
//                throw new ArgumentNullException("itemsToPrint", String.Format("Referenced printer id has invalid data (null or empty)"));
//            }

//            var nullDataEntry = itemsToPrint.Where(i => i.Value == null || i.Value.Count == 0).Select(i => i.Key).FirstOrDefault();
//            if (nullDataEntry != default(Guid))
//            {
//                throw new ArgumentNullException("itemsToPrint", String.Format("Data for printer '{0}' has invalid data (null or empty)", nullDataEntry));
//            }

//            if (activePrintJobs.Keys.Any(itemsToPrint.ContainsKey))
//            {
//                throw new Exception("Service has already sent a batch to one of the printers");
//            }
//        }

//        #endregion

//        /// <summary>
//        /// Callback method when the printer reports a job status event
//        /// </summary>
//        /// <param name="pjs"></param>
//        private void HandlePrinterJobStatusChanged(PrintJobStatus pjs)
//        {
//            if (pjs.JobStatus != PrintJobStatuses.Error && pjs.JobStatus != PrintJobStatuses.Started && pjs.JobStatus != PrintJobStatuses.Success)
//            {
//                logger.Log(LogLevel.Debug, "Unhandled PrintJobStatus event " + pjs.JobStatus);
//                return;
//            }

//            Debug.WriteLine("Pjs event- Status: {0} ErrorMessage: {1}", pjs.JobStatus, pjs.ErrorMessage);
//            // Get the batch that this job belongs to
//            var batchId = jobsSentToPrinter[pjs.UniqueId];

//            //Get that batch from the cached data
//            var originalData = batchJobsInProgress[batchId]; // data for specific printers

//            //Get the jobs that were meant for that printer
//            var printerData = originalData[pjs.PrinterId];

//            //Set the status of the current job
//            var currentJob = printerData.FirstOrDefault(p => p.ItemToPrint.Id == pjs.ItemToPrint.Id);
//            currentJob.JobStatus = pjs.JobStatus;

//            // Notify if this is the first print job started in the batch
//            var firstPrintJobInBatch = printerData.FirstOrDefault();
//            if (firstPrintJobInBatch != null && firstPrintJobInBatch.JobStatus == PrintJobStatuses.Started)
//            {
//                SendStatusChangedEvent(pjs, batchId, PrintRequestStatuses.Started);
//                return; // When job status is Started we do not need to take further action so return
//            }

//            //When job status is Error or Success then get the next item for that printer
//            var nextJobToProduce = printerData
//                .Where(p => p.ItemToPrint.Id != pjs.ItemToPrint.Id)
//                .FirstOrDefault(p => p.JobStatus == PrintJobStatuses.Unknown);

//            activePrintJobs.Remove(pjs.PrinterId);
//            jobsSentToPrinter.Remove(pjs.UniqueId);

//            //If the printer is not busy and there is another job send it the next job from the batch
//            if (nextJobToProduce != null)
//            {
//                LookupPrinterAndPrint(pjs.PrinterId, nextJobToProduce.ItemToPrint, batchId);

//                SendStatusChangedEvent(pjs, batchId, PrintRequestStatuses.InProgress);
//            }
//            else
//            {
//                if (originalData.Where(x => x.Key != pjs.PrinterId).SelectMany(p => p.Value).All(p => p.JobStatus != PrintJobStatuses.Unknown))
//                {
//                    SendStatusChangedEvent(pjs, batchId, PrintRequestStatuses.Complete);
//                }
//                else
//                {
//                    SendStatusChangedEvent(pjs, batchId, PrintRequestStatuses.InProgress);
//                }

//            }

//        }

//        /// <summary>
//        /// Fires the Status changed event
//        /// </summary>
//        /// <param name="pjs"></param>
//        /// <param name="batchId"></param>
//        /// <param name="currentStatus"></param>
//        private void SendStatusChangedEvent(PrintJobStatus pjs, Guid batchId, PrintRequestStatuses currentStatus)
//        {
//            // TODO: When we change this to lists, we will need to re-factor.
//            if (pjs.JobStatus == PrintJobStatuses.Error)
//            {
//                currentStatus = PrintRequestStatuses.Error;
//            }

//            Debug.WriteLine("PrintService job {0} changed status to {1}, batch status is {2}", pjs.UniqueId, pjs.JobStatus, currentStatus);
//            logger.Log(LogLevel.Info, String.Format("PrintService job {0} changed status to {1}, batch status is {2}", pjs.UniqueId, pjs.JobStatus, currentStatus));

//            var printRequestStatus = new PrintRequestStatus
//            {
//                PrintRequests = new List<PrintJobStatus> { pjs }, //????
//                UniqueID = batchId,
//                RequestStatus = currentStatus
//            };

//            publisher.Publish(printRequestStatus);
//            printRequestStatusChangedSubject.OnNext(printRequestStatus);
//        }

//        /// <summary>
//        /// Looks up the current printer and dispatches a job
//        /// </summary>
//        /// <param name="printerId"></param>
//        /// <param name="itemToPrint"></param>
//        /// <param name="batchId"></param>
//        private void LookupPrinterAndPrint(Guid printerId, Printable itemToPrint, Guid batchId)
//        {
//            var printer = printers.Keys.SingleOrDefault(p => p.Id == printerId);
//            if (printer == null)
//                throw new PrinterNotFoundException(String.Format("Printer '{0}' has not been configured", printerId));

//            logger.Log(LogLevel.Info, String.Format("PrintService dispatching printable {0} to printer {1} from batch {2}", itemToPrint.Id, printerId, batchId));
//            try
//            {
//                jobsSentToPrinter.Add(itemToPrint.Id, batchId);// order matters here make sure set before calling to print.
//                printer.Print(itemToPrint);
//                logger.Log(LogLevel.Debug, String.Format("PrintService dispatched printable {0} as ID {1}", itemToPrint.Id, itemToPrint.Id));
//                activePrintJobs.Add(printerId, itemToPrint.Id);
//            }
//            catch (PrinterBusyException pbe)
//            {
//                activePrintJobs.Add(printerId, itemToPrint.Id);

//                HandlePrinterJobStatusChanged(new PrintJobStatus
//                                              {
//                                                  ItemToPrint = itemToPrint,
//                                                  JobStatus = PrintJobStatuses.Error,
//                                                  UniqueId = itemToPrint.Id,
//                                                  PrinterId = printerId,
//                                                  ErrorMessage = pbe.Message,
//                                              });
//            }

//            // wire up events for capturing job status from the printer for each Printable
//            if (!printers[printer])
//            {
//                printer.PrinterStatusChangedObservable.Subscribe((p) => publisher.Publish(p));
//                printer.PrintJobStatusChangedObservable.Subscribe(HandlePrinterJobStatusChanged);
//                //TODO: We should dispose of these events after the batch is complete
//                printers[printer] = true;
//            }
//        }
//    }
//}