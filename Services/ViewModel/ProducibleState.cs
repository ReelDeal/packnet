﻿using PackNet.Common.Interfaces.Enums.ProducibleStates;
using System;

namespace PackNet.Services.ViewModel
{
    public class ProducibleState
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the producible status.
        /// </summary>
        /// <value>
        /// The producible status.
        /// </value>
        public ProducibleStatuses ProducibleStatus { get; set; }

        /// <summary>
        /// Gets or sets the produced on machine group identifier.
        /// </summary>
        /// <value>
        /// The produced on machine group identifier.
        /// </value>
        public Guid ProducedOnMachineGroupId { get; set; }

    }
}
