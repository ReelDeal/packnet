﻿using PackNet.Business.CloudReporting;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Services
{
    public class CloudReportingService : ICloudReportingService
    {
        public CloudReportingService(IServiceLocator serviceLocator)
        {
            var logger = serviceLocator.Locate<ILogger>();
            var subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            var recorder = new ProducibleCompleteRecorder(subscriber, logger);
        }

        public void Dispose()
        {
        }

        public string Name { get { return "Cloud Reporting Service"; } }
    }
}
