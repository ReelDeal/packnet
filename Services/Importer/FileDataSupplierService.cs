﻿using PackNet.Common.FileHandling.DynamicImporter;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

using PackNet.Common.Interfaces.DTO.Settings;

namespace PackNet.Services.Importer
{
    public class FileDataSupplierService : IDataSupplierService
    {
        private readonly IEventAggregatorPublisher eventPublisher;
        private readonly IEventAggregatorSubscriber eventSubscriber;
        private readonly IPackNetServerSettingsService packNetServerSettingsService;
        private readonly IUserNotificationService userNotificationService;
        private readonly ILogger logger;

        private bool isDisposed;
        private FileImportSettings settings;
        private FileSystemWatcher fileWatcher;

        private IObservable<FileSystemEventArgs> createdObservable;
        private IObservable<FileSystemEventArgs> renamedObservable;
        private IObservable<FileSystemEventArgs> changedObservable;
        private IObservable<ErrorEventArgs> errorObservable;
        private IDisposable createdDisposable;
        private IDisposable renamedDisposable;
        private IDisposable changedDisposable;
        private IDisposable errorDisposable;

        private readonly ConcurrentDictionary<string, WatcherChangeTypes> filesCurrentlyBeingProcessed = new ConcurrentDictionary<string, WatcherChangeTypes>();
        private IDisposable serverSettingsDisposable;

        /// <summary>
        /// File watcher that monitors the folder given in the FileImporterSettings. Files with the monitored extension are read
        /// then published in an IMessage of MessageType.RawDataImported with data type &lt;IEnumerable&lt;dynamic&gt;&gt;
        /// If the monitored path folder is deleted, removed or rendered inaccessible then the DataObservable publishes
        /// an OnError and the eventPublisher publishes an IMessage of MessageType.ImportFailed with data type Exception.
        /// </summary>
        /// <param name="eventPublisher">Publisher used to return results of the read file</param>
        /// <param name="eventSubscriber">Subscriber</param>
        /// <param name="packNetServerSettingsService">Pack Net Server Settings Service <see cref="FileImportSettings"/></param>
        /// <param name="userNotificationService">User Notification Service</param>
        /// <param name="logger">Logger to record actions taken by this class</param>
        public FileDataSupplierService(IEventAggregatorPublisher eventPublisher, IEventAggregatorSubscriber eventSubscriber, IPackNetServerSettingsService packNetServerSettingsService, IUserNotificationService userNotificationService, ILogger logger)
        {
            this.eventPublisher = eventPublisher;
            this.eventSubscriber = eventSubscriber;
            this.packNetServerSettingsService = packNetServerSettingsService;
            this.userNotificationService = userNotificationService;
            settings = packNetServerSettingsService.GetSettings().FileImportSettings;
            this.logger = logger;

            serverSettingsDisposable = eventSubscriber.GetEvent<ResponseMessage<PackNetServerSettings>>()
                .Where(m => m.MessageType == PackNetServerSettingsMessages.ServerSettingsUpdated)
                .DurableSubscribe(m =>
                {
                    logger.Log(LogLevel.Info, "File System Watcher restarted due to changes in the file import settings");

                    settings = packNetServerSettingsService.GetSettings().FileImportSettings;
                    SetupFileWatcher();
                }, logger);

            SetupFileWatcher();
        }

        /// <summary>
        /// Sets up an underlying FileSystemWatcher <see cref="FileSystemWatcher(string, string)"/>. Watching files from the path 
        /// and extension to watch in the FileImportSettings <see cref="FileImportSettings"/> injected into the constructor.
        /// Any existing files in the monitored path are processed immediately, then the FileSystemWatcher is started.
        /// </summary>
        private void SetupFileWatcher()
        {
            DisposeObservables();

            if (settings.FileExtension == "*")
            {
                logger.Log(LogLevel.Error, "Cannot use an asterisk(*) as the file extension to watch. Change file extension in file import settings.");
                userNotificationService.SendNotificationToSystem(NotificationSeverity.Error, "Cannot use an asterisk(*) as the file extension to watch. Change file extension in file import settings.", string.Empty);
                return;
            }

            try
            {
                fileWatcher = new FileSystemWatcher(settings.MonitoredFolderPath, string.Format("*.{0}", settings.FileExtension))
                {
                    NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName
                };
            }
            catch (Exception)
            {
                var logMessage = string.Format("Data import file path:{0} is unavailable. No further data will be imported. Change monitor folder in file import settings.",
                    settings.MonitoredFolderPath);
                logger.Log(LogLevel.Error, logMessage);
                userNotificationService.SendNotificationToSystem(NotificationSeverity.Error, logMessage, string.Empty);
                return;
            }
            SetupObservables();

            // Process any existing files
            foreach (var file in Directory.GetFiles(settings.MonitoredFolderPath, string.Format("*.{0}", settings.FileExtension)))
            {
                logger.Log(LogLevel.Trace, "Begin processing existing files");
                ProcessFile(file, WatcherChangeTypes.Created);
            }
            fileWatcher.EnableRaisingEvents = true;
            logger.Log(LogLevel.Info, "Monitoring directory {0} for files with extension {1}", settings.MonitoredFolderPath, settings.FileExtension);
        }

        /// <summary>
        /// Instantiates the created, renamed, changed and error observables by wiring up to the FileWatcher events using Observable.FromEvent
        /// </summary>
        private void SetupObservables()
        {
            createdObservable =
                Observable.FromEvent<FileSystemEventHandler, FileSystemEventArgs>(
                    handler => (FileSystemEventHandler)((sender, e) => handler(e)),
                    handler => fileWatcher.Created += handler,
                    handler => fileWatcher.Created -= handler);
            renamedObservable =
                Observable.FromEvent<RenamedEventHandler, FileSystemEventArgs>(
                    handler => (RenamedEventHandler)((sender, e) => handler(e)),
                    handler => fileWatcher.Renamed += handler,
                    handler => fileWatcher.Renamed -= handler);
            changedObservable =
                Observable.FromEvent<FileSystemEventHandler, FileSystemEventArgs>(
                    handler => (FileSystemEventHandler)((sender, e) => handler(e)),
                    handler => fileWatcher.Changed += handler,
                    handler => fileWatcher.Changed -= handler);
            errorObservable =
                Observable.FromEvent<ErrorEventHandler, ErrorEventArgs>(
                    handler => (ErrorEventHandler)((sender, e) => handler(e)),
                    handler => fileWatcher.Error += handler,
                    handler => fileWatcher.Error -= handler);

            createdDisposable = createdObservable.DurableSubscribe(args => ProcessFile(args.FullPath, args.ChangeType), logger);
            renamedDisposable = renamedObservable.DurableSubscribe(args => ProcessFile(args.FullPath, args.ChangeType), logger);
            changedDisposable = changedObservable.DurableSubscribe(args => ProcessFile(args.FullPath, args.ChangeType), logger);
            errorDisposable = errorObservable.DurableSubscribe(
                a =>
                {
                    logger.Log(LogLevel.Fatal, "Data import file path:{0} is no longer available. Exception:{1}", settings.MonitoredFolderPath, a.GetException());
                    eventPublisher.Publish(new Message<string>
                    {
                        MessageType = MessageTypes.AdminNotification,
                        Data =
                            string.Format(
                                "Data import file path:{0} is unavailable. No further data will be imported until it becomes available. Will retry reconnection",
                                settings.MonitoredFolderPath)
                    });
                    ReconnectToMonitoredFilePath();
                }, logger);
        }

        private void ReconnectToMonitoredFilePath()
        {
            for (var i = 0; i < settings.TimesToRetryToAccessMonitoredFilePath; i++)
            {
                Thread.Sleep(TimeSpan.FromSeconds(settings.SecondsToRetryToAccessMonitoredFilePath));

                if (DirectoryHelpers.CanRead(settings.MonitoredFolderPath))
                {
                    SetupFileWatcher();
                    return;
                }

                logger.Log(LogLevel.Error, "Failed attempt:{0} to connect to data import file path:{1}", i, settings.MonitoredFolderPath);
            }

            eventPublisher.Publish(new Message<string>
            {
                MessageType = MessageTypes.AdminNotification,
                Data =
                    string.Format(
                        "All attempts failed to reconnect to data import file path:{0}. No further data will be imported. Change monitor folder in file import settings.",
                        settings.MonitoredFolderPath)
            });
            var logMessage = string.Format("Data import file path:{0} is unavailable. No further data will be imported. Change monitor folder in file import settings.",
                    settings.MonitoredFolderPath);
            userNotificationService.SendNotificationToSystem(NotificationSeverity.Error, logMessage, string.Empty);
        }

        /// <summary>
        /// Parses data from the file in the given path. Data read from the file is returned in an IMessage&lt;IEnumerable&lt;dynamic&gt;&gt;
        /// </summary>
        /// <param name="fullFilePath">Full path to the file to read data from</param>
        /// <param name="changeType">WatcherChangeType, usually from the FileSystemWatcher event args</param>
        private void ProcessFile(string fullFilePath, WatcherChangeTypes changeType)
        {
            if (!fullFilePath.ToLower().EndsWith(settings.FileExtension))
            {
                logger.Log(LogLevel.Trace, "File:{0} is being ignored because it does not end in extension:{1}", fullFilePath, settings.FileExtension);
                return;
            }

            if (!filesCurrentlyBeingProcessed.TryAdd(fullFilePath, changeType))
            {
                logger.Log(LogLevel.Trace, "File:{0} is already being processed, ignoring change:{1}", fullFilePath, changeType);
                return;
            }

            logger.Log(LogLevel.Debug, "Started processing file:{0}, change:{1}, thread:{2}", fullFilePath, changeType, Thread.CurrentThread.ManagedThreadId);
            WatcherChangeTypes fileBeingProcessChangeType;
            Observable
                .Start( // Processes this asynchronously
                    () =>
                    {
                        var retryCount = settings.TimesToRetryLockedFile;
                        while (retryCount > 0)
                        {
                            try
                            {
                                var dynamicData = new FileEnumerator(fullFilePath, userNotificationService, logger, settings.HeaderFields,
                                    settings.FieldDelimiter, settings.CommentIndicator, settings.SecondsToRetryLockedFile);

                                var data = dynamicData.ToList<dynamic>();
                                dynamicData.Dispose();
                                return data;
                            }
                            catch (Exception exception)
                            {
                                const string message = "Failed to process import file '{0}'";
                                string formattedMessage = String.Format(message, fullFilePath);

                                logger.LogException(LogLevel.Error, message, exception, fullFilePath);

                                eventPublisher.Publish<IMessage<Tuple<string, Exception>>>(
                                    new Message<Tuple<string, Exception>>
                                    {
                                        MessageType = MessageTypes.ImportFailed,
                                        Data = new Tuple<string, Exception>(fullFilePath, exception)
                                    });

                                var unauthorizedAccessException = exception as UnauthorizedAccessException;

                                if (unauthorizedAccessException == null)
                                {
                                    filesCurrentlyBeingProcessed.TryRemove(fullFilePath, out fileBeingProcessChangeType);

                                    FileRename(fullFilePath, ".failed");
                                    userNotificationService.SendNotificationToSystem(
                                        NotificationSeverity.Error,
                                        formattedMessage,
                                        string.Empty);
                                    return null;
                                }

                                formattedMessage += String.Format(". File was locked., sleeping {0} seconds until next retry {1}/{2}", settings.SecondsToRetryLockedFile, retryCount, settings.TimesToRetryLockedFile);
                                userNotificationService.SendNotificationToSystem(
                                    NotificationSeverity.Error,
                                    formattedMessage,
                                    string.Empty);
                                retryCount--;
                                logger.Log(LogLevel.Warning, "File {0} was locked, sleeping {1} seconds until next retry {2}/{3}", fullFilePath, settings.SecondsToRetryLockedFile, retryCount, settings.TimesToRetryLockedFile);
                                Thread.Sleep(TimeSpan.FromSeconds(settings.SecondsToRetryLockedFile));
                            }
                        }
                        filesCurrentlyBeingProcessed.TryRemove(fullFilePath, out fileBeingProcessChangeType);
                        return null;
                    })
                .Subscribe( // Continuation after all Start is completed
                    data =>
                    {
                        if (data == null || !data.Any()) return;

                        var importMessage = new Message<IEnumerable<dynamic>>
                            {
                                MessageType = MessageTypes.RawDataImported,
                                Data = data,
                                ReplyTo = fullFilePath
                            };
                        eventPublisher.Publish<IMessage<IEnumerable<dynamic>>>(importMessage);
                    },
                    () =>
                    {
                        //Addresses TFS bug 9277 - do not remove this
                        try
                        {
                            if (settings.DeleteImportedFiles)
                                File.Delete(fullFilePath);
                            else
                                FileRename(fullFilePath, ".processed");
                        }
                        catch //WTF TOM? Handle the exception correctly
                        {
                            return;
                        }

                        userNotificationService.SendNotificationToSystem(
                            NotificationSeverity.Success,
                            String.Format("Processed import file '{0}'", fullFilePath),
                            String.Empty);

                        logger.Log(LogLevel.Debug, "Done processing file {0}. Thread {1}", fullFilePath,
                            Thread.CurrentThread.ManagedThreadId);

                        filesCurrentlyBeingProcessed.TryRemove(fullFilePath, out fileBeingProcessChangeType);
                    });
        }

        /// <summary>
        /// Rename a file in its current location with an additional extension and a timestamp DateTime.Now.ToString("yyyyMMddHHmmssffff")
        /// </summary>
        /// <param name="fullPath">Full path of file to rename</param>
        /// <param name="addExtension">Additonal extension to append to the file name (e.g., .failed or .success)</param>
        /// <param name="retries">Number of times to retry, default is five</param>
        private void FileRename(string fullPath, string addExtension, int retries = 5)
        {
            try
            {
                //DONT use UTCNow here
                File.Move(fullPath, string.Format("{0}{1}{2}", fullPath, addExtension, DateTime.Now.ToString("yyyyMMddHHmmssffff")));
            }
            catch (IOException e)
            {
                --retries;

                if (retries == 0)
                {
                    logger.Log(LogLevel.Error, "Failed to rename file:{0} due to exception:{1}", fullPath, e);
                    return;
                }

                Thread.Sleep(500);
                FileRename(fullPath, addExtension, retries);
            }
        }

        ~FileDataSupplierService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                if (disposing)
                {
                    fileWatcher.EnableRaisingEvents = false;
                    DisposeObservables();
                    fileWatcher.Dispose();
                }
                fileWatcher = null;
            }
            isDisposed = true;
        }

        /// <summary>
        /// Disposes the created, renamed,changed and error observables.
        /// </summary>
        private void DisposeObservables()
        {
            if (createdDisposable != null) createdDisposable.Dispose();
            if (renamedDisposable != null) renamedDisposable.Dispose();
            if (changedDisposable != null) changedDisposable.Dispose();
            if (errorDisposable != null) errorDisposable.Dispose();
        }

        public string Name { get { return "FileDataSupplierService"; } }
    }
}