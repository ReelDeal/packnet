﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services.Importer
{
    public class ImporterService : IImporterService
    {
        private readonly IImporterWorkflowDispatcher importerWorkflowDispatcher;
        private readonly ILogger logger;
        private readonly IDisposable importDisposable;
        private bool disposed;

        /// <summary>
        /// The ImporterService subscribes to import events on the aggregator then dispatches imported data to the importerWorkflowDispatcher.
        /// </summary>
        /// <param name="subscriber">Subscribe to events from the data supplier service</param>
        /// <param name="importerWorkflowDispatcher">Dispatches importables to a process that converts them to concrete objects usable in the system</param>
        /// <param name="packNetServerSettingsService">PackNet Server Settings Service</param>
        /// <param name="logger">Logs actions, errors, etc.</param>
        public ImporterService(IEventAggregatorSubscriber subscriber, IImporterWorkflowDispatcher importerWorkflowDispatcher, IPackNetServerSettingsService packNetServerSettingsService, ILogger logger)
		{
			this.importerWorkflowDispatcher = importerWorkflowDispatcher;
			this.importerWorkflowDispatcher
				.StatusObservable
				.Subscribe(
					_ => { }, // currently ignoring status updates
					exception => // an exception means a fatal error so rethrow
					{
						throw exception;
					});

			this.logger = logger;

			importDisposable =
				subscriber
					.GetEvent<IMessage<IEnumerable<dynamic>>>()
					.Where(m => m.MessageType == MessageTypes.RawDataImported)
					.DurableSubscribe(
						message =>
						{
							this.logger.Log(LogLevel.Trace,
								"Received IMessage<IEnumerable<dynamic>> RawDataImported event from aggregator");
							
                            var defaultImportType = packNetServerSettingsService.GetSettings().FileImportSettings.DefaultImportType;

							// Group on Type from import data
							var grouped = message.Data.GroupBy(
								d =>
								{

									try
									{
										if (((IDictionary<String, object>)d).ContainsKey("Type"))
										{
											return d.Type as string;
										}

										this.logger.Log(LogLevel.Debug,
											"Type was not included in imported data, grouping data by default type:{0} from settings", defaultImportType);
										return defaultImportType;
									}
									catch (Exception e)
									{
										this.logger.Log(LogLevel.Error, "Exception thrown while grouping imported data. {0}", e);
										return defaultImportType;
									}
								});

							foreach (var group in grouped)
							{
								this.logger.Log(LogLevel.Debug, "Importing group:{0} count:{1}", group.Key, group.Count());
								var importType = Enumeration.GetAllByType<ImportTypes>().FirstOrDefault(e => e.DisplayName == group.Key) ??
								                 Enumeration.GetAllByType<ImportTypes>().FirstOrDefault(e => e.DisplayName == defaultImportType);
							    this.importerWorkflowDispatcher.Dispatch(new Importable
								{
									ImportType = importType, 
									ImportedItems = new ConcurrentList<dynamic>(group),
                                    Source = message.ReplyTo
								});
							}
						}, logger);
		}

        public string Name { get { return "ImporterService"; } }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ImporterService()
        {
            Dispose(false);
        }

        private void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                importDisposable.Dispose();
            }

            disposed = true;
        }
    }
}