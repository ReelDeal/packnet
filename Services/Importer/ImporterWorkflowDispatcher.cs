﻿using System.Diagnostics;
using System.Text;

using PackNet.Common;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;

using System;
using System.Activities;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Reflection;

using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;

namespace PackNet.Services.Importer
{
    public class ImporterWorkflowDispatcher : IImporterWorkflowDispatcher
    {
        /// <summary>
        /// OnError should be handled as an application-level fatal exception.
        /// This means a workflow has thrown an unhandled exception.
        /// </summary>
        public IObservable<string> StatusObservable { get { return statusSubject.AsObservable(); } }
        private readonly Subject<string> statusSubject = new Subject<string>();

        private readonly IEnumerable<IWorkflowImporter> importers;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private readonly string pluginPath;
        private IServiceLocator serviceLocator;
        private IWorkflowTrackingService workflowTrackingService;

        /// <summary>
        /// Dispatches an importable to the corresponding workflow. Passes the publisher so that the workflow result may be published.
        /// </summary>
        /// <param name="pluginPath">The path to the importer plugins directory</param>
        /// <param name="importers">List of workflow importers implementing the IWorkflowImporter interface</param>
        /// <param name="publisher">IEventAggregatorPublisher that is passed through to workflows</param>
        /// <param name="serviceLocator">Used to locate services during import</param>
        /// <param name="logger">ILogger that is passed through to the workflows</param>
        public ImporterWorkflowDispatcher(string pluginPath, IEnumerable<IWorkflowImporter> importers, IEventAggregatorPublisher publisher, IServiceLocator serviceLocator, ILogger logger)
        {
            this.pluginPath = pluginPath;
            this.importers = importers;
            this.publisher = publisher;
            this.serviceLocator = serviceLocator;
            this.logger = logger;
            try
            {
                this.workflowTrackingService = serviceLocator.Locate<IWorkflowTrackingService>();
            }
            catch (Exception)
            {
                IDisposable subscription = null;
                subscription = serviceLocator.ServiceAddedObservable.Subscribe(s =>
                {
                    if (s is IWorkflowTrackingService)
                    {
                        this.workflowTrackingService = s as IWorkflowTrackingService;
                        subscription.Dispose();
                    }
                });
            }
        }

        /// <summary>
        /// Dispatches the Importable to the corresponding workflow that will publish the output on the event aggregator.
        /// </summary>
        /// <param name="importable">Importable to import using a workflow</param>
        /// <exception cref="Exception">Throws an Exception if the corresponding workflow for this Importable.ImportType cannot be found</exception>
        /// <remarks>
        /// The corresponding workflow is found by a case-insensitive match of the IWorkflowImport.Name to the importable.ImportType 
        /// </remarks>
        public void Dispatch(Importable importable)
        {
            var importer = importers.FirstOrDefault(i => i.ImportType == importable.ImportType);
            if (importer == null)
            {
                logger.Log(LogLevel.Error,
                    "ImportType:{0} not found in WorkflowDispatcher, cannot dispatch importables for processing into the system",
                    importable.ImportType);
                statusSubject.OnError( //TODO: this unwires the subscription... Is this what we want???
                    new Exception(String.Format("Failed to locate importer for importable '{0}'", importable.ImportType)));
                return;
            }

            InvokeWorkflow(Path.Combine(pluginPath, importer.Workflow), importable, importer.GetType().Assembly);
        }

        /// <summary>
        /// Invokes the workflow on the given path with the given Importable using the import assembly.
        /// </summary>
        /// <param name="workflowPath">Path to the workflow</param>
        /// <param name="importable">Importable to pass to the workflow</param>
        /// <param name="importAssembly">Assembly to </param>
        private void InvokeWorkflow(string workflowPath, Importable importable, Assembly importAssembly)
        {
            var stopWatch = Stopwatch.StartNew();
            var inArgs = new Dictionary<string, object> 
            { 
                { "Importable", importable},
                { "Publisher", publisher },
                { "ServiceLocator", serviceLocator }
            };
            Activity activity = null;

            try
            {
                activity = WorkflowHelper.GetActivity(DirectoryHelpers.ReplaceEnvironmentVariables(workflowPath), importAssembly);
            }
            catch (FileNotFoundException e)
            {
                WorkflowHelper.PublishAndLogWorkflowError(workflowPath, serviceLocator.Locate<IUserNotificationService>(), logger, e);
                return;
            }
            catch (InvalidWorkflowException e)
            {
                WorkflowHelper.PublishAndLogWorkflowError(workflowPath, serviceLocator.Locate<IUserNotificationService>(), logger, e);
                return;
            }

            WorkflowLifetime lt = null;
            if (this.workflowTrackingService != null)
            {
                var additionalInfo = new Dictionary<string, object>
                {
                    {"Source",importable.Source},
                    {"ImportType",importable.ImportType},
                    {"NumberOfItems",importable.ImportedItems.Count()}
                };
                lt = this.workflowTrackingService.Factory(WorkflowTypes.ImportWorkflow, workflowPath, additionalInfo);
            }

            // If workflow is Aborted, Canceled or Faulted then OnError is published, which halts the sequence,
            // so it should be treated as a fatal error by handlers
            var message = string.Empty;
            //todo:workflows - can we move this execution into the workflowHelper?
            var wfApp = new WorkflowApplication(activity, inArgs)
            {
                Aborted = e =>
                {
                    stopWatch.Stop();
                    message = string.Format("Workflow:{0} with importables:{1} took {3}(ms) unexpectedly aborted:{2}",
                        ((DynamicActivity)activity).Name, importable, e.Reason, stopWatch.ElapsedMilliseconds);
                    LogAndPublishError(message);
                    if (lt != null)
                        lt.StopTracking();
                },
                Completed = e =>
                {
                    stopWatch.Stop();
                    if (e.CompletionState == ActivityInstanceState.Canceled)
                    {
                        message = string.Format("Workflow:{0} with importables:{1} took {3}(ms) unexpectedly canceled:{2}",
                            ((DynamicActivity)activity).Name, importable, e.TerminationException, stopWatch.ElapsedMilliseconds);
                        LogAndPublishError(message);
                    }
                    if (e.CompletionState == ActivityInstanceState.Faulted)
                    {
                        message = string.Format("Workflow:{0} with importables:{1} took {3}(ms) unexpectedly faulted:{2}",
                            ((DynamicActivity)activity).Name, importable, e.TerminationException, stopWatch.ElapsedMilliseconds);
                        LogAndPublishError(message);
                    }
                    if (e.CompletionState == ActivityInstanceState.Closed)
                    {
                        message = string.Format("Completed workflow:{0} with importables:{1} took {2}(ms)", ((DynamicActivity)activity).Name, importable, stopWatch.ElapsedMilliseconds);
                        LogAndPublish(message);
                    }

                    if (lt != null)
                        lt.StopTracking();
                },
                OnUnhandledException = (WorkflowApplicationUnhandledExceptionEventArgs e) =>
                    {
                        stopWatch.Stop();
                        message = string.Format("Workflow:{0} for importables:{1} took {3}(ms) OnUnhandledException:{2} ExceptionSource:{4} - {5}",
                            ((DynamicActivity)activity).Name, importable, e.UnhandledException.Message, stopWatch.ElapsedMilliseconds, e.ExceptionSource.DisplayName, e.ExceptionSourceInstanceId);
                        LogAndPublishError(message);

                        if (lt != null)
                            lt.StopTracking();

                        // Instruct the runtime to terminate the workflow.
                        return UnhandledExceptionAction.Cancel;
                    }
            };

            try
            {
                if (lt != null)
                    lt.StartTracking();
                wfApp.Run();
            }
            catch (InvalidWorkflowException e)
            {
                WorkflowHelper.PublishAndLogWorkflowError(workflowPath, serviceLocator.Locate<IUserNotificationService>(), logger,
                    e);
                throw;
            }

            message = string.Format("Started workflow:{0} with InstanceId:{2} to process importables:{1}", ((DynamicActivity)activity).Name, importable, wfApp.Id);
            LogAndPublish(message);
        }

        private void LogAndPublish(string message)
        {
            logger.Log(LogLevel.Trace, message);
            statusSubject.OnNext(message);
        }

        private void LogAndPublishError(string message)
        {
            logger.Log(LogLevel.Error, message);
            statusSubject.OnError(new ImportFailedException(message));
        }
    }

}
