﻿using System;
using System.Collections.Generic;
using PackNet.Business.CartonPropertyGroup;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Data.CartonPropertyGroups;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Business.ProductionGroups;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Data.ProductionGroups;

using Testing.Specificity;

namespace BusinessTests
{
    [TestClass]
    public class CartonPropertyGroupsManagerTests
    {
        private Mock<ICartonPropertyGroupRepository> provider;
        private CartonPropertyGroups manager;
        private List<CartonPropertyGroup> cartonPropertyGroups;
        private Guid cpg1Guid;
        private Guid cpg2Guid;

        [TestInitialize]
        public void TestInitialize()
        {
            provider = SetUpProviderMock();
            var loggerMock = new Mock<ILogger>();
            var serviceLocator = new PackNet.Services.ServiceLocator("", new List<IService>(), loggerMock.Object);
            var eventAggregator = new EventAggregator();
            var productionGroupRepository = new Mock<IProductionGroupRepository>();
            var productionGroups = new PackNet.Business.ProductionGroups.ProductionGroups(eventAggregator, eventAggregator,
                productionGroupRepository.Object, serviceLocator, loggerMock.Object);
            manager = new CartonPropertyGroups(provider.Object, eventAggregator, productionGroups);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionWhenTryingToDeleteCartonPropertyGroupWithAssignedWork()
        {
            var cpgToDelete = new CartonPropertyGroup { Alias = "CPG1", NumberOfRequests = 2 };
            try
            {
                manager.Delete(cpgToDelete);
            }
            catch (Exception e)
            {
                Specify.That(e is InUseException).Should.BeTrue();
            }

            provider.Verify(m => m.Delete(cpgToDelete), Times.Never);
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionWhenTryingToEditCartonPropertyGroupWithAssignedWork()
        {
            var model = cartonPropertyGroups[0];
            var cpgToUpdate = new CartonPropertyGroup { Id = model.Id, Alias = model.Alias + "_renamed" };
            try
            {
                var oldValue = model.NumberOfRequests;
                model.NumberOfRequests = 1;
                manager.Update(cpgToUpdate);
                model.NumberOfRequests = oldValue;
            }
            catch (Exception e)
            {
                Specify.That(e is InUseException).Should.BeTrue();
            }

            provider.Verify(m => m.Update(cpgToUpdate), Times.Never);
        }

        private Mock<ICartonPropertyGroupRepository> SetUpProviderMock()
        {
            var providerMock = new Mock<ICartonPropertyGroupRepository>();

            cpg1Guid = Guid.NewGuid();
            cpg2Guid = Guid.NewGuid();

            cartonPropertyGroups = new List<CartonPropertyGroup>()
            {
                new CartonPropertyGroup { Alias = "CPG1", Id = cpg1Guid }, 
                new CartonPropertyGroup { Alias = "CPG2", Id = cpg2Guid }
            };

            providerMock.Setup(provider => provider.All()).Returns(cartonPropertyGroups);
            providerMock.Setup(provider => provider.Create(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>((a) => a.Id = Guid.NewGuid());
            providerMock.Setup(provider => provider.Update(It.IsIn<CartonPropertyGroup>(cartonPropertyGroups))).Returns<CartonPropertyGroup>((a) => a);

            return providerMock;
        }
    }
}
