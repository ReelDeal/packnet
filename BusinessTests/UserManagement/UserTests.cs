﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Logging;
using PackNet.Data.UserManagement;
using Testing.Specificity;
using Users = PackNet.Business.UserManagement.Users;

namespace BusinessTests.UserManagement
{
    [TestClass]
    public class UserTests
    {
        private User user;
        private Mock<IUserRepository> usersDataMock;
        private Mock<ILogger> mockLogger;
        private Users businessUsers;

        [TestInitialize]
        public void Setup()
        {
            user = new User
            {
                UserName = "test",
                Password = "testPass",
                IsAutoLogin = true,
                IsAdmin = false
            };

            usersDataMock = new Mock<IUserRepository>();
            mockLogger = new Mock<ILogger>();

            businessUsers = new Users(usersDataMock.Object);
        }

        [TestMethod]
        public void UpdateUser()
        {
            usersDataMock.Setup(repo => repo.Find(It.IsAny<string>())).Returns(user);
            businessUsers.Update(user);

            usersDataMock.Verify(x => x.Update(user), Times.Once);
        }

        [TestMethod]
        public void RemoveUser()
        {
            businessUsers.Delete(user);

            usersDataMock.Verify(x => x.Delete(user), Times.Once);
        }

        [TestMethod]
        public void FindByGuid()
        {
            businessUsers.Find(user.Id);

            usersDataMock.Verify(x => x.Find(user.Id), Times.Once);
        }

        [TestMethod]
        public void FindByUserName()
        {
            businessUsers.Find(user.UserName);

            usersDataMock.Verify(x => x.Find(user.UserName), Times.Once);
        }

        [TestMethod]
        public void FindAll()
        {
            var allUsers = new List<User> { user };
            usersDataMock.Setup(x => x.All()).Returns(allUsers);
            var allUsersReturn = businessUsers.All();

            usersDataMock.Verify(x => x.All());
            Specify.That(allUsersReturn).Should.BeLogicallyEqualTo(allUsers);
        }
#if DEBUG
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnUserButNotAssignUserToMachineWhenLoginAsManager()
        {
            //var userToReturn = new User { UserName = "u", Password = "p" };
            //userManagerMock.Setup(
            //    m => m.GetUserWithLoginAndPermission(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<UserPermission>()))
            //    .Returns(userToReturn);

            //var user = logicManager.LoginUser("u", "p", 1, UserPermission.Manager);

            //Specify.That(user).Should.BeEqualTo(userToReturn);
            //userManagerMock.Verify(m => m.GetUserWithLoginAndPermission("u", "p", UserPermission.Manager), Times.Once());
            //machinesManager.Verify(m => m.AssignUserAsOperator("u", 1), Times.Never());
            Assert.Fail("Move functionality");
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnUserAndAssingUserToMachineWhenLoginAsOperator()
        {
            //var userToReturn = new User { UserName = "u", Password = "p" };
            //userManagerMock.Setup(
            //    m => m.GetUserWithLoginAndPermission(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<UserPermission>()))
            //    .Returns(userToReturn);

            //var user = logicManager.LoginUser("u", "p", 1, UserPermission.Operator);

            //Specify.That(user).Should.BeEqualTo(userToReturn);
            //userManagerMock.Verify(m => m.GetUserWithLoginAndPermission("u", "p", UserPermission.Operator), Times.Once());
            //machinesManager.Verify(m => m.AssignUserAsOperator("u", 1), Times.Once());
            Assert.Fail("Move functionality");

        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnNullAndNotAssingUserToMachineWhenLoginWithWrongCredentials()
        {
            //var userToReturn = new User { UserName = "u", Password = "p" };
            //userManagerMock.Setup(
            //    m => m.GetUserWithLoginAndPermission("u", "p", It.IsAny<UserPermission>()))
            //    .Returns(userToReturn);

            //var user = logicManager.LoginUser("u", "wrong", 1, UserPermission.Operator);

            //Specify.That(user).Should.BeNull();
            //userManagerMock.Verify(m => m.GetUserWithLoginAndPermission("u", "wrong", UserPermission.Operator), Times.Once());
            //machinesManager.Verify(m => m.AssignUserAsOperator("u", 1), Times.Never());
            Assert.Fail("Move functionality");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnUserAndAssingUserToMachineWhenAutoLoginingAsOperatorWhenAutoLoginUserHasOperatorPermissions()
        {
            //var autoLoginUser = new User { UserName = "u", Password = "p" };
            //userManagerMock.Setup(m => m.AutoLoginUser).Returns(autoLoginUser);
            //userManagerMock.Setup(
            //    m => m.GetUserWithLoginAndPermission(autoLoginUser.UserName, autoLoginUser.Password, UserPermission.Operator))
            //    .Returns(autoLoginUser);

            //var user = logicManager.AutoLoginUser(1, UserPermission.Operator);

            //Specify.That(user).Should.BeEqualTo(autoLoginUser);
            //userManagerMock.Verify(m => m.GetUserWithLoginAndPermission(user.UserName, autoLoginUser.Password, UserPermission.Operator), Times.Once());
            //machinesManager.Verify(m => m.AssignUserAsOperator(user.UserName, 1), Times.Once());
            Assert.Fail("Move functionality");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnUserButNotAssingUserToMachineWhenAutoLoginAsManagerWhenAutoLoginUserHasManagerPermissions()
        {
            //var autoLoginUser = new User { UserName = "u", Password = "p" };
            //userManagerMock.Setup(m => m.AutoLoginUser).Returns(autoLoginUser);
            //userManagerMock.Setup(
            //    m => m.GetUserWithLoginAndPermission(autoLoginUser.UserName, autoLoginUser.Password, UserPermission.Manager))
            //    .Returns(autoLoginUser);

            //var user = logicManager.AutoLoginUser(1, UserPermission.Manager);

            //Specify.That(user).Should.BeEqualTo(autoLoginUser);
            //userManagerMock.Verify(m => m.GetUserWithLoginAndPermission(user.UserName, autoLoginUser.Password, UserPermission.Manager), Times.Once());
            //machinesManager.Verify(m => m.AssignUserAsOperator(user.UserName, 1), Times.Never());

            Assert.Fail("Move functionality");

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnNullAndNotAssingUserToMachineWhenAutoLoginUserHasWrongPermissions()
        {
            //var autoLoginUser = new User { UserName = "u", Password = "p" };
            //userManagerMock.Setup(m => m.AutoLoginUser).Returns(autoLoginUser);
            //userManagerMock.Setup(
            //    m => m.GetUserWithLoginAndPermission(autoLoginUser.UserName, autoLoginUser.Password, UserPermission.Operator))
            //    .Returns((User)null);

            //var user = logicManager.AutoLoginUser(1, UserPermission.Operator);

            //Specify.That(user).Should.BeNull();
            //userManagerMock.Verify(m => m.GetUserWithLoginAndPermission(autoLoginUser.UserName, autoLoginUser.Password, UserPermission.Operator), Times.Once());
            //machinesManager.Verify(m => m.AssignUserAsOperator(autoLoginUser.UserName, 1), Times.Never());

            Assert.Fail("Move functionality");
        }
#endif
    }
}
