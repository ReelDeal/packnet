﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Business.UserManagement;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Data.UserManagement;
using Testing.Specificity;

namespace BusinessTests.UserManagement
{
    [TestClass]
    public class PreferenceTests
    {
        private Mock<IPreferenceRepository> mockPreferencesRepository;
        private Preferences preferences;
        private Preference preference;

        [TestInitialize]
        public void Setup()
        {
            preference = new Preference { Id = Guid.NewGuid(), UserId = Guid.NewGuid() };

            mockPreferencesRepository = new Mock<IPreferenceRepository>();

            preferences = new Preferences(mockPreferencesRepository.Object);
        }

        [TestMethod]
        public void FindById()
        {
            Guid id = Guid.NewGuid();
            preferences.Find(id);

            mockPreferencesRepository.Verify(x => x.Find(id), Times.Once);
        }

        [TestMethod]
        public void FindByUser()
        {
            User u = new User() { Id = Guid.NewGuid() };
            preferences.Find(u);

            mockPreferencesRepository.Verify(x => x.Find(u), Times.Once);
        }

        [TestMethod]
        public void Update()
        {
            preferences.Update(preference);
            mockPreferencesRepository.Verify(x => x.Update(preference), Times.Once);
        }

        [TestMethod]
        public void FindByIdReturnsDefaultWhenReturnDefaultIsTrueAndCannotBeFound()
        {
            var prefId = Guid.NewGuid();
            mockPreferencesRepository.Setup(x => x.Find(It.IsAny<Guid>())).Returns((Preference)null);

            var result = preferences.Find(prefId, true);

            Specify.That(result).Should.Not.BeNull();
            Specify.That(result.Id).Should.BeEqualTo(prefId);
        }

        [TestMethod]
        public void FindByUserReturnsDefaultWhenReturnDefaultIsTrueAndCannotBeFound()
        {
            var user = new User() { Id = Guid.NewGuid() };
            mockPreferencesRepository.Setup(x => x.Find(It.IsAny<User>())).Returns((Preference)null);

            var result = preferences.Find(user, true);

            Specify.That(result).Should.Not.BeNull();
            Specify.That(result.Id).Should.Not.BeEqualTo(Guid.Empty);
            Specify.That(result.UserId).Should.BeEqualTo(user.Id);
        }

        [TestMethod]
        public void FindByIdReturnsNullWhenReturnDefaultIsFalseAndCannotBeFound()
        {
            var prefId = Guid.NewGuid();
            mockPreferencesRepository.Setup(x => x.Find(It.IsAny<Guid>())).Returns((Preference)null);

            var result = preferences.Find(prefId, false);

            Specify.That(result).Should.BeNull();
        }

        [TestMethod]
        public void FindByUserReturnsNullWhenReturnDefaultIsFalseAndCannotBeFound()
        {
            var user = new User() { Id = Guid.NewGuid() };
            mockPreferencesRepository.Setup(x => x.Find(It.IsAny<User>())).Returns((Preference)null);

            var result = preferences.Find(user, false);

            Specify.That(result).Should.BeNull();
        }

    }
}