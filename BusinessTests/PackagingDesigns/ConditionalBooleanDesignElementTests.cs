﻿namespace BusinessTests.PackagingDesigns
{
    using System.IO;
    using System.Xml.Serialization;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO.PackagingDesigns;

    [TestClass]
    public class ConditionalBooleanDesignElementTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void SerializesAllowedAttributeToTrue()
        {
            var serialized = "<Degree0 Allowed=\"true\"></Degree0>";
            var serializer = "Degree0".GetSerializer<ConditionalBooleanDesignElement>();
            var result = serializer.Deserialize(new StringReader(serialized)) as ConditionalBooleanDesignElement;
            Assert.IsTrue(result.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializesAllowedAttributeToFalse()
        {
            var serialized = "<Degree0 Allowed=\"false\"></Degree0>";
            var serializer = "Degree0".GetSerializer<ConditionalBooleanDesignElement>();
            var result = serializer.Deserialize(new StringReader(serialized)) as ConditionalBooleanDesignElement;
            Assert.IsFalse(result.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializesAllowedAttributeWhenMissing()
        {
            var serialized = "<Degree0></Degree0>";
            var serializer = "Degree0".GetSerializer<ConditionalBooleanDesignElement>();
            var result = serializer.Deserialize(new StringReader(serialized)) as ConditionalBooleanDesignElement;
            Assert.IsFalse(result.Allowed);
        }
    }

    public static class Extensions
    {
        public static XmlRootAttribute GetRoot(this string elementName)
        {
            return new XmlRootAttribute { Namespace = "", IsNullable = true, ElementName = elementName };
        }

        public static XmlSerializer GetSerializer<T>(this string rootElement)
        {
            return new XmlSerializer(typeof(T), GetRoot(rootElement));
        }
    }
}
