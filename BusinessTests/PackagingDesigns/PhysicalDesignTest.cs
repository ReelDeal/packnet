﻿using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO;

using Testing.Specificity;

namespace BusinessTests.PackagingDesigns
{
    [TestClass]
    public class PhysicalDesignTest
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_GetVerticalLines()
        {
            var design = new PhysicalDesign();
            var line = new PhysicalLine(new PhysicalCoordinate(0.0, 0.0), new PhysicalCoordinate(0.0, 6.0), LineType.cut);
            design.Add(line);

            var lines = design.GetVerticalPhysicalLines();

            Specify.That(lines.Count()).Should.BeEqualTo(1);
            Specify.That(lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(0.0);
            Specify.That(lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(0.0);
            Specify.That(lines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(0.0);
            Specify.That(lines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(6.0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_GetHorizontalLines()
        {
            var design = new PhysicalDesign();
            var line = new PhysicalLine(new PhysicalCoordinate(0.0, 0.0), new PhysicalCoordinate(6.0, 0.0), LineType.cut);
            design.Add(line);

            var lines = design.GetHorizontalPhysicalLines();

            Specify.That(lines.Count()).Should.BeEqualTo(1);
            Specify.That(lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(0.0);
            Specify.That(lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(0.0);
            Specify.That(lines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(6.0);
            Specify.That(lines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(0.0);
        }
        
        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_GetHorizontalLines_Coordinates()
        {
            var design = new PhysicalDesign();
            var line = new PhysicalLine(new PhysicalCoordinate(4.0, 2.0), new PhysicalCoordinate(6.0, 2.0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(7.0, 0.0), new PhysicalCoordinate(10.0, 0.0), LineType.cut);
            design.Add(line);

            var horizontalCoordinates = design.GetHorizontalLineCoordinates();

            Specify.That(horizontalCoordinates.Count()).Should.BeEqualTo(2);
            Specify.That(horizontalCoordinates.First()).Should.BeLogicallyEqualTo(2.0);
            Specify.That(horizontalCoordinates.Last()).Should.BeLogicallyEqualTo(0.0);
        }
  
        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_GetHorizontalLinesAtCoordinate()
        {
            var design = new PhysicalDesign();
            var line = new PhysicalLine(new PhysicalCoordinate(4.0, 2.0), new PhysicalCoordinate(6.0, 2.0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(7.0, 2.0), new PhysicalCoordinate(10.0, 2.0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(7.0, 3.0), new PhysicalCoordinate(10.0, 3.0), LineType.cut);
            design.Add(line);

            var lines = design.GetHorizontalLinesAtCoordinate(2.0);

            Specify.That(lines.Count()).Should.BeEqualTo(2);

            Specify.That(lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(4.0);
            Specify.That(lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(2.0);

            Specify.That(lines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(6.0);
            Specify.That(lines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(2.0);

            Specify.That(lines.Last().StartCoordinate.X).Should.BeLogicallyEqualTo(7.0);
            Specify.That(lines.Last().StartCoordinate.Y).Should.BeLogicallyEqualTo(2.0);

            Specify.That(lines.Last().EndCoordinate.X).Should.BeLogicallyEqualTo(10.0);
            Specify.That(lines.Last().EndCoordinate.Y).Should.BeLogicallyEqualTo(2.0);

            lines = design.GetHorizontalLinesAtCoordinate(3.0);

            Specify.That(lines.Count()).Should.BeEqualTo(1);

            Specify.That(lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(7.0);
            Specify.That(lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(3.0);

            Specify.That(lines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(10.0);
            Specify.That(lines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(3.0);
        }
    }
}
