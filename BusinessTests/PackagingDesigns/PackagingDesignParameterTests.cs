﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using Testing.Specificity;
using PackNet.Common.Interfaces.DTO;

namespace BusinessTests.PackagingDesigns
{
    [TestClass]
    public class PackagingDesignParameterTests
    {
        public const string TestDataPath = ".\\TestData\\";

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldApplyAdditionalParametersToPhysicalDesign()
        {            
            var design = GivenADesignWithVerticalLinesStartingAtXValues(new List<string> { "X1", "X2" });

            var xvalues = new Dictionary<string, double> { { "X1", 6.0 }, { "X2", 8.0 } };

            var parameters = new DesignFormulaParameters
            {
                CartonHeight = 10.1,
                CartonLength = 11.2,
                CartonWidth = 12.3,
                ZfoldThickness = 0.3,
                ZfoldWidth = 27.55,              
            };

            xvalues.ForEach(kv => parameters.XValues[kv.Key] = kv.Value);
            
            var appliedDesign = design.AsPhysicalDesign(parameters);

            Specify.That(appliedDesign.Lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(6.0);
            Specify.That(appliedDesign.Lines.Last().StartCoordinate.X).Should.BeLogicallyEqualTo(8.0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldApplyAdditionalParametersToPhysicalDesignInYCoordinateAndLength()
        {
            var design = GivenADesignWithLinesAllOverThePlace();

            var xvalues = new Dictionary<string, double> { { "X1", 2.0 }, { "X2", 4.0 }, { "X3", 6.0 }, { "X4", 8.0 }, { "X5", 10.0 }, { "X6", 12.0 }, { "X7", 14.0 } };

            var parameters = new DesignFormulaParameters
            {
                CartonHeight = 10.1,
                CartonLength = 11.2,
                CartonWidth = 12.3,
                ZfoldThickness = 0.3,
                ZfoldWidth = 40.55,                
            };
                        
            xvalues.ForEach(kv => parameters.XValues[kv.Key] = kv.Value);

            var appliedDesign = design.AsPhysicalDesign(parameters);

            Specify.That(appliedDesign.Lines.Count()).Should.BeEqualTo(2);

            Specify.That(appliedDesign.Lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(xvalues["X1"] + xvalues["X2"]);
            Specify.That(appliedDesign.Lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(xvalues["X3"] + parameters.CartonLength);
            Specify.That(appliedDesign.Length).Should.BeLogicallyEqualTo(parameters.CartonLength + xvalues["X1"] + xvalues["X2"] + xvalues["X3"] + xvalues["X4"]);
            Specify.That(appliedDesign.Width ).Should.BeLogicallyEqualTo(parameters.ZfoldWidth  + xvalues["X5"] + xvalues["X6"] + xvalues["X7"]);

            Specify.That(appliedDesign.Lines.Last().StartCoordinate.X).Should.BeLogicallyEqualTo(xvalues["X5"] - xvalues["X6"]);
            Specify.That(appliedDesign.Lines.Last().StartCoordinate.Y).Should.BeLogicallyEqualTo(xvalues["X7"] * parameters.CartonLength);            
        }

        private PackagingDesign GivenADesignWithVerticalLinesStartingAtXValues(List<string> xLines)
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW",
                LengthOnZFold = "L"
            };

            xLines.ForEach(val => packagingDesign.Lines.Add(new Line
            {
                StartX = val,
                StartY = "0",
                Length = "L",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            }));

            return packagingDesign;
        }

        private PackagingDesign GivenADesignWithLinesAllOverThePlace()
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW+X5+X6+X7",
                LengthOnZFold = "L+X1+X2+X3+X4"
            };

            var line = new Line
            {
                StartX = "X1+X2",
                StartY = "X3+L",
                Length = "L+X4",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            };

            packagingDesign.Lines.Add(line);

            line = new Line
            {
                StartX = "X5-X6",
                StartY = "X7*L",
                Length = "FW+X7",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };

            packagingDesign.Lines.Add(line);

            return packagingDesign;
        }
    }
}
