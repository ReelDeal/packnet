﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO;

using Testing.Specificity;

namespace BusinessTests.PackagingDesigns
{
    [TestClass]
    public class PhysicalLineTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReverse_LinedirectionIfInNegativeDirection_Horizontal()
        {
            var startCoordinate = new PhysicalCoordinate(5.0, 5.0);
            var endCoordinate = new PhysicalCoordinate(0.0, 5.0);

            var line = new PhysicalLine(startCoordinate, endCoordinate, LineType.cut);

            Specify.That(line.StartCoordinate.X).Should.BeLogicallyEqualTo(0.0);
            Specify.That(line.StartCoordinate.Y).Should.BeLogicallyEqualTo(5.0);

            Specify.That(line.EndCoordinate.X).Should.BeLogicallyEqualTo(5.0);
            Specify.That(line.EndCoordinate.Y).Should.BeLogicallyEqualTo(5.0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReverse_LinedirectionIfInNegativeDirection_Vertical()
        {
            var startCoordinate = new PhysicalCoordinate(5.0, 5.0);
            var endCoordinate = new PhysicalCoordinate(5.0, 0.0);

            var line = new PhysicalLine(startCoordinate, endCoordinate, LineType.cut);

            Specify.That(line.StartCoordinate.X).Should.BeLogicallyEqualTo(5.0);
            Specify.That(line.StartCoordinate.Y).Should.BeLogicallyEqualTo(0.0);

            Specify.That(line.EndCoordinate.X).Should.BeLogicallyEqualTo(5.0);
            Specify.That(line.EndCoordinate.Y).Should.BeLogicallyEqualTo(5.0);
        }
    }
}
