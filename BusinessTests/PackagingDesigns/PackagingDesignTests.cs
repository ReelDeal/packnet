﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces;

namespace BusinessTests.PackagingDesigns
{
    using System.Reflection;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;

    using StoryQ;

    using Testing.Specificity;

    [TestClass]
    public class PackagingDesignTests
    {
        private PackagingDesign packagingDesign;
        private PhysicalDesign physicalDesign;
        private DesignFormulaParameters dimensions;

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnAPhysicalDesignWithCorrectParameters()
        {
            new Story("PhysicalDesign is derived from PackagingDesign")
                .InOrderTo("To simplify code generation")
                .AsA("A Developer")
                .IWant("The designs to have lines in physical dimensions instead of parameters")
                .WithScenario("Apply measurments to design")

                .Given(AFormulaBasedFefco201)
                .And(DesignParameters)
                .When(DimensionsAreAppliedToTheDesign)
                .Then(APhysicalDesignIsReturned)
                .And(LengthAndWidthIsSet)
                .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_FlipLines_That_ArePointingInNegativeVerticalDirection()
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW",
                LengthOnZFold = "L"
            };

            var xvalues = new Dictionary<string, double> { { "X1", 5.0 }, { "X2", -5.0 } };

            packagingDesign.Lines.Add(new Line
            {
                StartX = "X1",
                StartY = "X1",
                Length = "X2",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            });

            var parameters = new DesignFormulaParameters
            {
                CartonHeight = 10.1,
                CartonLength = 10,
                CartonWidth = 12.3,                
                ZfoldThickness = 0.3,
                ZfoldWidth = 40.55,
            };

            xvalues.ForEach(kv => parameters.XValues[kv.Key] = kv.Value);

            var appliedDesign = packagingDesign.AsPhysicalDesign(parameters);

            Specify.That(appliedDesign.Lines.Count()).Should.BeEqualTo(1);

            Specify.That(appliedDesign.Lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(5.0);
            Specify.That(appliedDesign.Lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(0.0);

            Specify.That(appliedDesign.Lines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(5.0);
            Specify.That(appliedDesign.Lines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(5.0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_FlipLines_That_ArePointingInNegativeHorizontalDirection()
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW",
                LengthOnZFold = "L"
            };

            var xvalues = new Dictionary<string, double> { { "X1", 5.0 }, { "X2", -5.0 } };

            packagingDesign.Lines.Add(new Line
            {
                StartX = "X1",
                StartY = "X1",
                Length = "X2",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            });

            var parameters = new DesignFormulaParameters
            {
                CartonHeight = 10.1,
                CartonLength = 10,
                CartonWidth = 12.3,
                ZfoldThickness = 0.3,
                ZfoldWidth = 40.55,
            };

            xvalues.ForEach(kv => parameters.XValues[kv.Key] = kv.Value);

            var appliedDesign = packagingDesign.AsPhysicalDesign(parameters);

            Specify.That(appliedDesign.Lines.Count()).Should.BeEqualTo(1);

            Specify.That(appliedDesign.Lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(0.0);
            Specify.That(appliedDesign.Lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(5.0);

            Specify.That(appliedDesign.Lines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(5.0);
            Specify.That(appliedDesign.Lines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(5.0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_FlipLines_That_ArePointingInNegativeHorizontalDirection_OutSideZFold()
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW",
                LengthOnZFold = "L"
            };

            var xvalues = new Dictionary<string, double> { { "X1", 5.0 }, { "X2", -20.0 } };

            packagingDesign.Lines.Add(new Line
            {
                StartX = "X1",
                StartY = "X1",
                Length = "X2",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            });

            var parameters = new DesignFormulaParameters
            {
                CartonHeight = 10.1,
                CartonLength = 10,
                CartonWidth = 12.3,
                ZfoldThickness = 0.3,
                ZfoldWidth = 40.55,
            };

            xvalues.ForEach(kv => parameters.XValues[kv.Key] = kv.Value);

            var appliedDesign = packagingDesign.AsPhysicalDesign(parameters);

            Specify.That(appliedDesign.Lines.Count()).Should.BeEqualTo(1);

            Specify.That(appliedDesign.Lines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(-15.0);
            Specify.That(appliedDesign.Lines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(5.0);

            Specify.That(appliedDesign.Lines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(5.0);
            Specify.That(appliedDesign.Lines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(5.0);
        }        

        private void LengthAndWidthIsSet()
        {
            Specify.That(physicalDesign.Width).Should.BeLogicallyEqualTo(100);
            Specify.That(physicalDesign.Length).Should.BeLogicallyEqualTo(0);
        }

        private void APhysicalDesignIsReturned()
        {
            Specify.That(physicalDesign).Should.Not.BeNull();
        }

        private void DesignParameters()
        {
            dimensions = new DesignFormulaParameters() {
                CartonLength = 100,
                CartonWidth = 100,
                CartonHeight = 100,
                ZfoldThickness = 3,
                ZfoldWidth = 800
            };
        }

        private void DimensionsAreAppliedToTheDesign()
        {
            physicalDesign = packagingDesign.AsPhysicalDesign(dimensions);
        }

        private void AFormulaBasedFefco201()
        {
            packagingDesign = new PackagingDesign {
                WidthOnZFold = "L"
            };

            var line = new Line {
                StartX = "0",
                StartY = "0",
                Length = "2L+2W+3.5FT+25",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "0.5W+FT",
                StartY = "0",
                Length = "2L+2W+3.5FT+25",
                Direction = LineDirection.vertical,
                Type = LineType.crease
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "H+0.5W+3FT",
                StartY = "0",
                Length = "2L+2W+3.5FT+25",
                Direction = LineDirection.vertical,
                Type = LineType.crease
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "H+W+4FT",
                StartY = "0",
                Length = "2L+2W+3.5FT+25",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);

            line = new Line {
                StartX = "0",
                StartY = "0",
                Length = "W+H+4FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);

            line = new Line {
                StartX = "0",
                StartY = "25",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "0.5W+FT",
                StartY = "25",
                Length = "H+2FT",
                Direction = LineDirection.horizontal,
                Type = LineType.crease
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "H+0.5W+3FT",
                StartY = "25",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);

            line = new Line {
                StartX = "0",
                StartY = "25+L+FT",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "0.5W+FT",
                StartY = "25+L+FT",
                Length = "H+2FT",
                Direction = LineDirection.horizontal,
                Type = LineType.crease
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "H+0.5W+3FT",
                StartY = "25+L+FT",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);

            line = new Line {
                StartX = "0",
                StartY = "25+L+2FT+W",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "0.5W+FT",
                StartY = "25+L+2FT+W",
                Length = "H+2FT",
                Direction = LineDirection.horizontal,
                Type = LineType.crease
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "H+0.5W+3FT",
                StartY = "25+L+2FT+W",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);

            line = new Line {
                StartX = "0",
                StartY = "25+2L+3FT+W",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "0.5W+FT",
                StartY = "25+2L+3FT+W",
                Length = "H+2FT",
                Direction = LineDirection.horizontal,
                Type = LineType.crease
            };
            packagingDesign.Lines.Add(line);
            line = new Line {
                StartX = "H+0.5W+3FT",
                StartY = "25+2L+3FT+W",
                Length = "0.5W+FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);

            line = new Line {
                StartX = "H+0.5W+3FT",
                StartY = "25+2L+3.5FT+2W",
                Length = "H+W+4FT",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };
            packagingDesign.Lines.Add(line);
        }
    }
}
