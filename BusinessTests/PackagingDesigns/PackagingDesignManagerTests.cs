﻿using System.Collections.Generic;

using Moq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace BusinessTests.PackagingDesigns
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.PackagingDesigns;
    using PackNet.Common.Caching;
    using PackNet.Common.ExtensionMethods;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Utils;

    using Testing.Specificity;

    [TestClass]
    [DeploymentItem("TestData", "TestData")]
    public class PackagingDesignManagerTests
    {
        private const string TestDataPath = ".\\TestData\\";
        private const int FileAccessAttempts = 20;
        private PackagingDesignManager designManager;
        private readonly Mock<ILogger> mockLogger = new Mock<ILogger>(MockBehavior.Loose);
        private readonly Mock<ICachingService> cachingMock = new Mock<ICachingService>();

        [TestInitialize]
        public void TestInitialize()
        {
            var combinedPath = Path.Combine(Environment.CurrentDirectory, TestDataPath);
            designManager = new PackagingDesignManager(combinedPath, new MemoryCache(mockLogger.Object, "PackNetDesignManagerTestCache"), mockLogger.Object);

            if (File.Exists(Path.Combine(combinedPath, "IWasAdded.ezd")))
            {
                File.Delete(Path.Combine(combinedPath, "IWasAdded.ezd"));
            }
        }

        [TestCleanup]
        public void TestCleanup()
        {
            if (designManager != null)
            {
                designManager.Dispose();
            }
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldLoadAllFiles()
        {
            designManager.LoadDesigns();
            Specify.That(designManager.Designs.Count).Should.Not.BeEqualTo(0);
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldReturnDesignNameOnExistingDesignId()
        {
            designManager.LoadDesigns();
            Specify.That(designManager.GetDesignName(201)).Should.BeEqualTo("Two-0-one");
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldReturnEmptyStringOnNonExistentDesignId()
        {
            designManager.LoadDesigns();
            Specify.That(designManager.GetDesignName(000000)).Should.BeEqualTo(String.Empty);
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldNotRaiseErrorEventWhenAllDesignsAreValid()
        {
            var eventCalled = false;

            designManager.DesignLoadError += a =>
            {
                eventCalled = true;
            };

            designManager.LoadDesigns();

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));

            Specify.That(eventCalled).Should.BeFalse();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldRaiseErrorEventWhenGetDesignHasSpecialXMLCharacters()
        {
            var eventCalled = false;
            File.Copy(".\\TestData\\211InvalidSpecialChar.ezdx", ".\\TestData\\211InvalidSpecialChar.ezd", true);
            designManager.DesignLoadError += a =>
            {
                eventCalled = true;
                Specify.That(a.Contains("special xml characters")).Should.BeTrue();
            };
            try
            {

                designManager.LoadDesigns();

                Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));

                Specify.That(eventCalled).Should.BeTrue();

                designManager.Dispose();
            }
            catch (Exception e)
            {
                designManager.Dispose();
                Assert.Fail("Unexpected exception occured. {0}", e);
            }
            finally
            {
                DeleteFile("211InvalidSpecialChar.ezd", FileAccessAttempts);
            }

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldLoadRubberLineDesigns()
        {
            designManager.LoadDesigns();
            var design = designManager.GetDesigns().FirstOrDefault(d => d.Id == 1114);
            Specify.That(design).Should.Not.BeNull();
            Specify.That(design.IsElasticLineDesign).Should.BeTrue();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldNotUseRubberLines_WhenThereIsNoWaste()
        {
            designManager.LoadDesigns();

            var corrugate = new Corrugate() { Alias = "NoRubberLineNeeded", Thickness = 3, Width = 200, Quality = 1 };
            var carton = new Carton() { DesignId = 1114, Length = 400, Width = 100, Height = 100 };

            var physicalDesign = designManager.GetPhyscialDesignForCarton(carton, corrugate);


            /*0   50  150 200
             *      ___ 
             *  ___|...|___
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |___¦...¦___|
             *     |___|   
             * */

            #region lines

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 0 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 50 && 
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 150 &&
                l.EndCoordinate.Y == 0 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 150 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 150 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 150 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 200 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 500 &&
                l.EndCoordinate.X == 150 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 150 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 150 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 150 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 150 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 150 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 150 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 150 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            #endregion
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldUseNonFullLengthRubberLines_WhenThereIsNotEnoughWaste_ForFullLengthRubberLines()
        {
            designManager.LoadDesigns();

            var corrugate = new Corrugate() { Alias = "UseNonFullLengthRubberLines", Thickness = 3, Width = 250, Quality = 1 };
            var carton = new Carton() { DesignId = 1114, Length = 400, Width = 100, Height = 100 };

            var physicalDesign = designManager.GetPhyscialDesignForCarton(carton, corrugate);

            /*0 25  75  175 225 250
             *        ___ 
             *  _____|...|_____
             * |᷈ ᷈    ¦   ¦   ᷈ ᷈ |
             * |     ¦   ¦     |
             * |     ¦   ¦     |
             * |     ¦   ¦     |
             * |     ¦   ¦     |
             * |_____¦...¦_____|
             *  ᷈ ᷈    |___|   ᷈ ᷈ 
             * */



            #region lines

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 0 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 25 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 25 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 75 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 75 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 75 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 75 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 175 &&
                l.EndCoordinate.Y == 0 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 175 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 175 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 175 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 225 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 225 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 250 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 250 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 250 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 25 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 25 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 75 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 75 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 75 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 75 &&
                l.StartCoordinate.Y == 500 &&
                l.EndCoordinate.X == 175 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 175 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 175 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 175 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 225 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 225 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 250 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 75 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 75 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 75 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 175 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 75 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 175 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 175 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 175 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            #endregion
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldUseFullLengthRubberLines_WhenThereIsTooMuchWaste()
        {
            designManager.LoadDesigns();

            var corrugate = new Corrugate() { Alias = "UseFullLengthRubberLines", Thickness = 3, Width = 500, Quality = 1 };
            var carton = new Carton() { DesignId = 1114, Length = 400, Width = 100, Height = 100 };

            var physicalDesign = designManager.GetPhyscialDesignForCarton(carton, corrugate);


            /*0   50 100 200 250 300
             *         ___ 
             *  ______|...|______
             * |᷈ ᷈ ᷈    ¦   ¦   ᷈ ᷈ ᷈ |
             * |      ¦   ¦      |
             * |      ¦   ¦      |
             * |      ¦   ¦      |
             * |      ¦   ¦      |
             * |______¦...¦______|
             *  ᷈ ᷈ ᷈    |___|   ᷈ ᷈ ᷈ 
             * */

            #region lines

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 0 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);
            
            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 100 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 100 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 100 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 100 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 0 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 200 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 200 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 250 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 250 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 300 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 300 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 300 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 100 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 100 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 100 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 100 &&
                l.StartCoordinate.Y == 500 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 200 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 500 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 200 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 250 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 250 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 300 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 100 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 100 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 100 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 50 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 100 &&
                l.StartCoordinate.Y == 450 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 200 &&
                l.StartCoordinate.Y == 50 &&
                l.EndCoordinate.X == 200 &&
                l.EndCoordinate.Y == 450 &&
                l.Type == LineType.crease)).Should.BeEqualTo(1);

            #endregion
        }

        [TestMethod]
        [Description("Integration with tiled rubberlines")]
        [TestCategory("Integration")]
        public void ShouldAdjustSwellPecentageWhenTiling()
        {
            designManager.LoadDesigns();

            var corrugate = new Corrugate() { Alias = "UseRubberLines", Thickness = 3, Width = 500, Quality = 1 };
            var tiledCarton = new TiledCarton();
            tiledCarton.AddTiles(new List<Carton> {
                new Carton() { DesignId = 1114, Length = 400, Width = 100, Height = 100 },
                new Carton() { DesignId = 1114, Length = 400, Width = 100, Height = 100 }
            });

            var physicalDesign = designManager.GetPhyscialDesignForCarton(tiledCarton, corrugate, OrientationEnum.Degree0, tiledCarton.Tiles.Count());

            /*0  25  75 175 225 250
             *         ___               ___ 
             *  ______|...|______ ______|...|______
             * |᷈ ᷈ ᷈    ¦   ¦   ᷈ ᷈ ᷈ |᷈ ᷈ ᷈    ¦   ¦   ᷈ ᷈ ᷈ |
             * |      ¦   ¦      |      ¦   ¦      |
             * |      ¦   ¦      |      ¦   ¦      |
             * |      ¦   ¦      |      ¦   ¦      |
             * |      ¦   ¦      |      ¦   ¦      |
             * |______¦...¦______|______¦...¦______|
             *  ᷈ ᷈ ᷈    |___|   ᷈ ᷈ ᷈  ᷈ ᷈ ᷈    |___|   ᷈ ᷈ ᷈  
             */

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 0 &&
                l.EndCoordinate.X == 25 &&
                l.Type == LineType.cut)).Should.BeEqualTo(2);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 225 &&
                l.EndCoordinate.X == 250 &&
                l.Type == LineType.cut)).Should.BeEqualTo(2);
        }

        [TestMethod]
        [Description("Integration with tiled rubberlines")]
        [TestCategory("Integration")]
        public void ShouldUseZeroLengthOnRubberLines_IfRotated()
        {
            designManager.LoadDesigns();

            var corrugate = new Corrugate { Alias = "UseZeroLengthRubberLines", Thickness = 3, Width = 500, Quality = 1 };
            var carton = new Carton { DesignId = 1114, Length = 400, Width = 100, Height = 100 };
            var physicalDesign = designManager.GetPhyscialDesignForCarton(carton, corrugate, OrientationEnum.Degree90);

            /* Rotated 90°
             * 0   50  150 200
             *      ___ 
             *  ___|...|___
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |   ¦   ¦   |
             * |___¦...¦___|
             *     |___|   
             * */

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 0 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 50 &&
                l.StartCoordinate.Y == 200 &&
                l.EndCoordinate.X == 50 &&
                l.EndCoordinate.Y == 200 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 450 &&
                l.StartCoordinate.Y == 0 &&
                l.EndCoordinate.X == 450 &&
                l.EndCoordinate.Y == 0 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);

            Specify.That(physicalDesign.Lines.Count(l =>
                l.StartCoordinate.X == 450 &&
                l.StartCoordinate.Y == 200 &&
                l.EndCoordinate.X == 450 &&
                l.EndCoordinate.Y == 200 &&
                l.Type == LineType.cut)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldRaiseErrorEventWhenGetDesignIsInvalidXML()
        {
            var eventCalled = false;
            File.Copy(".\\TestData\\210InvalidXml.ezdx", ".\\TestData\\210InvalidXml.ezd", true);
            designManager.DesignLoadError += a =>
            {
                eventCalled = true;
                Specify.That(a.Contains("valid packaging file")).Should.BeTrue();
            };
            try
            {

                designManager.LoadDesigns();

                Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));

                Specify.That(eventCalled).Should.BeTrue();

                designManager.Dispose();
            }
            catch (Exception e)
            {
                designManager.Dispose();
                Assert.Fail("Unexpected exception occured. {0}", e);
            }
            finally
            {
                DeleteFile("210InvalidXml.ezd", FileAccessAttempts);
            }

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldReturnPackagingDesigns()
        {
            designManager.LoadDesigns();
            var designs = designManager.GetDesigns();
            Specify.That(designs.Any()).Should.BeTrue();
            Specify.That(designs.First().Id).Should.BeEqualTo(2010000);
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldRaiseDesignChangedEventWhenDesignDeleted()
        {
            var eventCalled = false;

            const string FileName = "204.ezd";
            CreateFileBackup(FileName, FileAccessAttempts);

            try
            {
                designManager.DesignsChanged += (a, d) => { eventCalled = true; };
                designManager.LoadDesigns();
                var cnt = designManager.Designs.Count;
                DeleteFile(FileName, FileAccessAttempts);

                Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));

                Specify.That(eventCalled).Should.BeTrue();
                Specify.That(designManager.Designs.Count).Should.BeEqualTo(--cnt);
                designManager.Dispose();
            }
            catch (Exception e)
            {
                Assert.Fail("Unexpected exception occured. {0}", e);
            }
            finally
            {
                RestoreFile(FileName, FileAccessAttempts);
                DeleteFile(string.Format("{0}bckup", FileName), FileAccessAttempts);
            }

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldRaiseDesignLoadErrorEventWhenDesignHasNotRotationPermissions()
        {
            File.Copy(".\\TestData\\DesignWithoutRotation.ezdx", ".\\TestData\\DesignWithoutRotation.ezd", true);
            var eventCalled = false;

            designManager.DesignLoadError += a =>
            {
                eventCalled = true;

                Specify.That(a.Contains("is missing rotation permission")).Should.BeTrue();
            };

            try
            {
                designManager.LoadDesigns();
                Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));
            }
            catch (Exception e)
            {
                designManager.Dispose();
                Assert.Fail("Unexpected exception occured. {0}", e);
            }
            finally
            {
                DeleteFile("DesignWithoutRotation.ezd", FileAccessAttempts);
            }

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldValidateUniqueDesignIds()
        {
            File.Copy(".\\TestData\\duplicateId.ezdx", ".\\TestData\\duplicateId.ezd", true);

            var eventCalled = false;

            designManager.DesignLoadError += a =>
            {
                eventCalled = true;

                Specify.That(a.Contains("201")).Should.BeTrue();
                Specify.That(a.Contains("duplicate")).Should.BeTrue();
            };
            try
            {
                designManager.LoadDesigns();

                Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));

            }
            catch (Exception e)
            {
                designManager.Dispose();
                Assert.Fail("Unexpected exception occured. {0}", e);
            }
            finally
            {
                DeleteFile("duplicateId.ezd", FileAccessAttempts);
            }

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetDesignRotated0DegreesAndFlip()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree0Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270Flip = new ConditionalBooleanDesignElement { Allowed = true }
            };

            pd.Lines.Add(new Line { StartX = "W-10", StartY = "0", Length = "H-15", Direction = LineDirection.vertical });
            pd.Lines.Add(new Line { StartX = "10", StartY = "L-5", Length = "10", Direction = LineDirection.horizontal });

            pd.WidthOnZFold = "W";
            pd.LengthOnZFold = "L";

            /* 
             * +------------+               +-----------+
             * |         |  |               | |         |
             * |         |  | L  0'flip =>  | |         | L
             * | --         |               |        -- |
             * +------------+               +-----------+
             *      W                           W
             */


            designManager.AddDesign(pd);
            var rotatedDesign =
                designManager.GetPhyscialDesignForCarton(new Carton() { DesignId = 1321123, Length = 100, Width = 200, Height = 90 }, new Corrugate(), OrientationEnum.Degree0_flip);

            Specify.That(rotatedDesign.Lines.Count()).Should.BeEqualTo(2);
            Specify.That(rotatedDesign.Width).Should.BeLogicallyEqualTo(200);
            Specify.That(rotatedDesign.Length).Should.BeLogicallyEqualTo(100d);

            var rotatedLines = rotatedDesign.Lines.OrderBy(l => l.StartCoordinate.Y).ThenBy(l => l.StartCoordinate.X);

            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(10d); //W-10
            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(0d); // 0
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(10d); //W-10
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(75d); // 0+(H-15)

            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(180d); // 10
            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(95d); // L-5
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(190d); // 10+10
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(95d); // L-5
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetDesignRotated90Degrees()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree0Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270Flip = new ConditionalBooleanDesignElement { Allowed = true }
            };

            pd.Lines.Add(new Line { StartX = "W-10", StartY = "0", Length = "H-15", Direction = LineDirection.vertical });
            pd.Lines.Add(new Line { StartX = "10", StartY = "L-5", Length = "10", Direction = LineDirection.horizontal });

            pd.WidthOnZFold = "W";
            pd.LengthOnZFold = "L";

            /* 
             * +------------+               +--------+
             * |         |  |               | |      |
             * |         |  | L     90' =>  |        | W
             * | --         |               |   -----|
             * +------------+               +--------+
             *      W                           L
             */


            designManager.AddDesign(pd);
            var rotatedDesign =
                designManager.GetPhyscialDesignForCarton(new Carton() { DesignId = 1321123, Length = 100, Width = 200, Height = 90 }, new Corrugate(), OrientationEnum.Degree90);

            Specify.That(rotatedDesign.Lines.Count()).Should.BeEqualTo(2);
            Specify.That(rotatedDesign.Width).Should.BeLogicallyEqualTo(100);
            Specify.That(rotatedDesign.Length).Should.BeLogicallyEqualTo(200d);

            var rotatedLines = rotatedDesign.Lines.OrderBy(l => l.StartCoordinate.Y).ThenBy(l => l.StartCoordinate.X);

            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(5d); //L-5
            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10d); // 10
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(5d); //L-5
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(20d); // 10+10

            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(25d); //L-(H-15)
            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(190d); // W-10
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(100d); // L-0
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(190d); // W-10
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetDesignRotated90DegreesAndFlip()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree0Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270Flip = new ConditionalBooleanDesignElement { Allowed = true }
            };

            pd.Lines.Add(new Line { StartX = "W-10", StartY = "0", Length = "H-15", Direction = LineDirection.vertical });
            pd.Lines.Add(new Line { StartX = "10", StartY = "L-5", Length = "10", Direction = LineDirection.horizontal });

            pd.WidthOnZFold = "W";
            pd.LengthOnZFold = "L";

            /* 
             * +------------+               +--------+
             * |         |  |               |      | |
             * |         |  | L 90'flip =>  |        | W
             * | --         |               |-----   |
             * +------------+               +--------+
             *      W                           L
             */


            designManager.AddDesign(pd);
            var rotatedDesign =
                designManager.GetPhyscialDesignForCarton(new Carton() { DesignId = 1321123, Length = 100, Width = 200, Height = 90 }, new Corrugate(), OrientationEnum.Degree90_flip);

            Specify.That(rotatedDesign.Lines.Count()).Should.BeEqualTo(2);
            Specify.That(rotatedDesign.Width).Should.BeLogicallyEqualTo(100);
            Specify.That(rotatedDesign.Length).Should.BeLogicallyEqualTo(200d);

            Specify.That(rotatedDesign.Lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0d);
            Specify.That(rotatedDesign.Lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(190d); // L-10
            Specify.That(rotatedDesign.Lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(75d); // H-15
            Specify.That(rotatedDesign.Lines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(190d); // L-10

            Specify.That(rotatedDesign.Lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(95d); //L-5
            Specify.That(rotatedDesign.Lines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(10d);
            Specify.That(rotatedDesign.Lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(95d); // L-5+10
            Specify.That(rotatedDesign.Lines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(20d); // 10+10
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetDesignRotated180Degrees()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree0Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270Flip = new ConditionalBooleanDesignElement { Allowed = true }
            };

            pd.Lines.Add(new Line { StartX = "W-10", StartY = "0", Length = "H-15", Direction = LineDirection.vertical });
            pd.Lines.Add(new Line { StartX = "10", StartY = "L-5", Length = "10", Direction = LineDirection.horizontal });

            pd.WidthOnZFold = "W";
            pd.LengthOnZFold = "L";

            /* 
             * +------------+               +------------+
             * |         |  |               |         -- |
             * |         |  | L    180' =>  |  |         | L
             * | --         |               |  |         |
             * +------------+               +------------+
             *      W                           W
             */


            designManager.AddDesign(pd);
            var rotatedDesign =
                designManager.GetPhyscialDesignForCarton(new Carton() { DesignId = 1321123, Length = 100, Width = 200, Height = 90 }, new Corrugate(), OrientationEnum.Degree180);

            Specify.That(rotatedDesign.Lines.Count()).Should.BeEqualTo(2);
            Specify.That(rotatedDesign.Width).Should.BeLogicallyEqualTo(200);
            Specify.That(rotatedDesign.Length).Should.BeLogicallyEqualTo(100d);
            
            var rotatedLines = rotatedDesign.Lines.OrderBy(l => l.StartCoordinate.Y).ThenBy(l => l.StartCoordinate.X);


            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(180d); // W-20
            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(5d); // L-5
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(190d); // W-20+10
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(5d); // L-5

            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(10d); //W-10
            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(25d); //H-15
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(10d); // W-10
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(100d); // 0
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetDesignRotated180DegreesAndFlip()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree0Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270Flip = new ConditionalBooleanDesignElement { Allowed = true }
            };

            pd.Lines.Add(new Line { StartX = "W-10", StartY = "0", Length = "H-15", Direction = LineDirection.vertical });
            pd.Lines.Add(new Line { StartX = "10", StartY = "L-5", Length = "10", Direction = LineDirection.horizontal });

            pd.WidthOnZFold = "W";
            pd.LengthOnZFold = "L";

            /* 
             * +------------+               +------------+
             * |         |  |               | --         |
             * |         |  | L 180'flip => |         |  | L
             * | --         |               |         |  |
             * +------------+               +------------+
             *      W                           W
             */


            designManager.AddDesign(pd);
            var rotatedDesign =
                designManager.GetPhyscialDesignForCarton(new Carton() { DesignId = 1321123, Length = 100, Width = 200, Height = 90 }, new Corrugate(), OrientationEnum.Degree180_flip);

            Specify.That(rotatedDesign.Lines.Count()).Should.BeEqualTo(2);
            Specify.That(rotatedDesign.Width).Should.BeLogicallyEqualTo(200);
            Specify.That(rotatedDesign.Length).Should.BeLogicallyEqualTo(100d);

            var rotatedLines = rotatedDesign.Lines.OrderBy(l => l.StartCoordinate.Y).ThenBy(l => l.StartCoordinate.X);


            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(10d); // W-20
            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(5d); // L-5
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(20d); // W-20+10
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(5d); // L-5

            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(190d); //W-10
            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(25d); //H-15
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(190d); // W-10
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(100d); // 0
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetDesignRotated270Degrees()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree0Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270Flip = new ConditionalBooleanDesignElement { Allowed = true }
            };

            pd.Lines.Add(new Line { StartX = "W-10", StartY = "0", Length = "H-15", Direction = LineDirection.vertical });
            pd.Lines.Add(new Line { StartX = "10", StartY = "L-5", Length = "10", Direction = LineDirection.horizontal });

            pd.WidthOnZFold = "W";
            pd.LengthOnZFold = "L";

            /* 
             * +------------+               +------------+
             * |         |  |               |---------   |
             * |         |  | L    270' =>  |            | W
             * | --         |               |          | |
             * +------------+               +------------+
             *      W                           L
             */


            designManager.AddDesign(pd);
            var rotatedDesign =
                designManager.GetPhyscialDesignForCarton(new Carton() { DesignId = 1321123, Length = 100, Width = 200, Height = 90 }, new Corrugate(), OrientationEnum.Degree270);

            Specify.That(rotatedDesign.Lines.Count()).Should.BeEqualTo(2);
            Specify.That(rotatedDesign.Width).Should.BeLogicallyEqualTo(100);
            Specify.That(rotatedDesign.Length).Should.BeLogicallyEqualTo(200d);
            
            var rotatedLines = rotatedDesign.Lines.OrderBy(l => l.StartCoordinate.Y).ThenBy(l => l.StartCoordinate.X);


            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0d); // 0
            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10d); // W-10
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(75d); // H-15
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(10d); // W-10

            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(95d); //L-5
            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(180d); //10
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(95d); //L-5
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(190d); //10+10
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetDesignRotated270DegreesAndFlip()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree0Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree90Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree180Flip = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270 = new ConditionalBooleanDesignElement { Allowed = true },
                Degree270Flip = new ConditionalBooleanDesignElement { Allowed = true }
            };

            pd.Lines.Add(new Line { StartX = "W-10", StartY = "0", Length = "H-15", Direction = LineDirection.vertical });
            pd.Lines.Add(new Line { StartX = "10", StartY = "L-5", Length = "10", Direction = LineDirection.horizontal });

            pd.WidthOnZFold = "W";
            pd.LengthOnZFold = "L";

            /* 
             * +------------+               +------------+
             * |         |  |               |   ---------|
             * |         |  | L 270'flip => |            | W
             * | --         |               | |          |
             * +------------+               +------------+
             *      W                           L
             */


            designManager.AddDesign(pd);
            var rotatedDesign =
                designManager.GetPhyscialDesignForCarton(new Carton() { DesignId = 1321123, Length = 100, Width = 200, Height = 90 }, new Corrugate(), OrientationEnum.Degree270_flip);

            Specify.That(rotatedDesign.Lines.Count()).Should.BeEqualTo(2);
            Specify.That(rotatedDesign.Width).Should.BeLogicallyEqualTo(100);
            Specify.That(rotatedDesign.Length).Should.BeLogicallyEqualTo(200d);

            var rotatedLines = rotatedDesign.Lines.OrderBy(l => l.StartCoordinate.Y).ThenBy(l => l.StartCoordinate.X);


            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(25d); // 0
            Specify.That(rotatedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10d); // W-10
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(100d); // H-15
            Specify.That(rotatedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(10d); // W-10

            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(5d); //L-5
            Specify.That(rotatedLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(180d); //10
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(5d); //L-5
            Specify.That(rotatedLines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(190d); //10+10
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldUseCacheKey_GeneratedByDesignId_Rotation_CorrugateWidthAndThickness_Dimensions()
        {
            var pd = new PackagingDesign { Id = 1321123 };
            var pd2 = new PackagingDesign { Id = 1, AllowedRotations = new RotationPermissions{Degree90 = new ConditionalBooleanDesignElement{ Allowed = true }}};
            var designManagerWithMockedCache = new PackagingDesignManager(Path.Combine(Environment.CurrentDirectory, TestDataPath), cachingMock.Object, mockLogger.Object);
            designManagerWithMockedCache.DesignLoadError += Console.Write;
            designManagerWithMockedCache.AddDesign(pd);
            designManagerWithMockedCache.AddDesign(pd2);

            var carton = new Carton { DesignId = 1321123, Length = 100, Width = 200, Height = 90 };
            carton.XValues.Add("X1", 5);
            var corrugate = new Corrugate { Width = 1000, Thickness = 3 };
            PhysicalDesign design;

            designManagerWithMockedCache.GetPhyscialDesignForCarton(carton, corrugate);
            cachingMock.Verify(cm => cm.TryGet("1321123:Degree0:1000:3:200:100:90:1:5", out design), Times.Once());

            corrugate.Width = 1234;
            corrugate.Thickness = 5;
            carton.DesignId = 1;
            carton.Length = 2;
            carton.Width = 3;
            carton.Height = 4;
            carton.XValues["X1"] = 6;
            carton.XValues.Add("X", 7);
            designManagerWithMockedCache.GetPhyscialDesignForCarton(carton, corrugate, OrientationEnum.Degree90);
            cachingMock.Verify(cm => cm.TryGet("1:Degree90:1234:5:3:2:4:1:6:7", out design), Times.Once());
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void GetPhyiscalDesign_ShouldReturnNull_WhenSpecifiedRotationIsNotAllowed()
        {
            designManager.LoadDesigns();
            var design =
                designManager.GetPhyscialDesignForCarton(new Carton { DesignId = 201, Length = 100, Width = 100, Height = 100 },
                    new Corrugate { Width = 700, Thickness = 3 }, OrientationEnum.Degree90);
            Specify.That(design).Should.BeNull("Design should not be allowed to produce rotated");

            design =
                designManager.GetPhyscialDesignForCarton(new Carton { DesignId = 2010101, Length = 100, Width = 100, Height = 100 },
                    new Corrugate { Width = 700, Thickness = 3 });
            Specify.That(design).Should.BeNull("Design should only be allowed to produce rotated");
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldNotCrashWhenDesignIsNotFound()
        {
            var error = "";
            try
            {
                designManager.DesignLoadError += s => error = s;
                designManager.LoadDesigns();
                var result = designManager.GetPhyscialDesignForCarton(new Carton { DesignId = 2001, Length = 100, Width = 100, Height = 100 },
                    new Corrugate { Width = 700, Thickness = 3 });
                Specify.That(error).Should.BeEqualTo("Design with id 2001 was not found");
                Specify.That(result).Should.BeNull();
            }
            catch
            {
                Assert.Fail("Should not throw an exception here");
            }
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldPopulateParameterListWithParametersUsedInFormulasOfLegacyDesign()
        {
            var testDesignManager = new PackagingDesignManager(TestDataPath, new Mock<ICachingService>().Object, mockLogger.Object);

            testDesignManager.LoadDesigns();
            var design = testDesignManager.GetDesigns().FirstOrDefault(d => d.Id.Equals(4321));

            var parameters = design.DesignParameters;

            Specify.That(parameters.Count()).Should.BeEqualTo(1);
            Specify.That(parameters.First().Name).Should.BeEqualTo("X1");
            Specify.That(parameters.First().Alias).Should.BeEqualTo("X1");
            Specify.That(parameters).Should.Not.Contain("FW");
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldPopulateNameAndAlias_WithParameterDefinitionInFile_WhenDeserializingFileInNewFormat()
        {
            var testDesignManager = new PackagingDesignManager(TestDataPath, new Mock<ICachingService>().Object, mockLogger.Object);

            testDesignManager.LoadDesigns();
            var design = testDesignManager.GetDesigns().FirstOrDefault(d => d.Id.Equals(123456));

            var parameters = design.DesignParameters;

            Specify.That(parameters.Count()).Should.BeEqualTo(1);
            Specify.That(parameters.First().Name).Should.BeEqualTo("X1");
            Specify.That(parameters.First().Alias).Should.BeEqualTo("test");
            Specify.That(parameters).Should.Not.Contain("FW");
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldHandleNewDesignsAddedDuringRuntime()
        {
            var testDesignManager = new PackagingDesignManager(TestDataPath, new Mock<ICachingService>().Object, mockLogger.Object);

            testDesignManager.LoadDesigns();

            var designChanged = false;
            testDesignManager.DesignsChanged += (s, e) =>
            {
                designChanged = true;
            };

            File.Copy(Path.Combine(TestDataPath, "NewDesign", "IWasAdded.ezd"), Path.Combine(TestDataPath, "IWasAdded.ezd"));
            Retry.For(() => designChanged, TimeSpan.FromSeconds(15));
            Specify.That(designChanged).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldLogWhenLegacyDesignIsDetected()
        {
            designManager.LoadDesigns();

            mockLogger.Verify(l => l.Log(LogLevel.Warning, It.Is<string>(s =>
                s.Contains("4321") &&
                s.Contains("XVal.ezd")
            )), Times.Exactly(1));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void HasDesignWithIdShouldReturnTrueWhenDesignExists()
        {
            designManager.AddDesign(new PackagingDesign { Id = 5555 });

            Specify.That(designManager.HasDesignWithId(5555)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void HasDesignWithIdShouldReturnFalseWhenDesignDoesNotExists()
        {
            Specify.That(designManager.HasDesignWithId(7777)).Should.BeFalse();
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ShouldReturnWidthAndLengthOnCorrugate_BasedOnCartonDimensions()
        {
            var allowRotation = new ConditionalBooleanDesignElement { Allowed = true };
            var pd = new PackagingDesign { Id = 1321123, LengthOnZFold = "L+H+4FT", WidthOnZFold = "W+2.5FT"};
            pd.AllowedRotations = new RotationPermissions
            {
                Degree0 = allowRotation,
                Degree0Flip = allowRotation,
                Degree90 = allowRotation,
                Degree90Flip = allowRotation
            };

            designManager.AddDesign(pd);

            var carton = new Carton { DesignId = 1321123, Length = 100, Width = 200, Height = 300 };
            var corrugate = new Corrugate { Thickness = 3 };

            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, false).Width).Should.BeLogicallyEqualTo(200 + 2.5 * 3); // W+2.5FT
            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, true).Width).Should.BeLogicallyEqualTo(100 + 300 + 4 * 3); // L+H+4FT
            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, false).Length)
                .Should.BeLogicallyEqualTo(designManager.CalculateUsage(carton, corrugate, 1, true).Width,
                    "Length on corrugate should be same as width on corrugate for a rotated design");
            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, true).Length)
                .Should.BeLogicallyEqualTo(designManager.CalculateUsage(carton, corrugate, 1, false).Width,
                    "Length on corrugate for a rotated design should be same as width on corrugate for a non rotated design");

            carton.Length = 150;
            carton.Width = 400;
            carton.Height = 350;
            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, false).Width).Should.BeLogicallyEqualTo(400 + 2.5 * 3); // W+2.5FT
            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, true).Width).Should.BeLogicallyEqualTo(150 + 350 + 4 * 3); // L+H+4FT
            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, false).Length)
                .Should.BeLogicallyEqualTo(designManager.CalculateUsage(carton, corrugate, 1, true).Width,
                    "Length on corrugate should be same as width on corrugate for a rotated design");
            Specify.That(designManager.CalculateUsage(carton, corrugate, 1, true).Length)
                .Should.BeLogicallyEqualTo(designManager.CalculateUsage(carton, corrugate, 1, false).Width,
                    "Length on corrugate for a rotated design should be same as width on corrugate for a non rotated design");
        }

        [TestMethod]
        [Description("Integration with packaging design files")]
        [TestCategory("Integration")]
        public void ModifyingReturnValueFromCache_ShouldNotAffectItemInCache()
        {
            designManager.LoadDesigns();
            var carton = new Carton { DesignId = 201, Length = 100, Width = 100, Height = 100 };
            var corrugate = new Corrugate { Width = 700, Thickness = 3 };
            var design = designManager.GetPhyscialDesignForCarton(carton, corrugate);

            var designWidth = design.Width;

            design.Width += design.Width;
            var design2 = designManager.GetPhyscialDesignForCarton(carton, corrugate);

            Specify.That(design2.Width).Should.BeLogicallyEqualTo(designWidth, "Modifying the value from 'GetPhyscialDesignForCarton' should not modify the cached object");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_ChangeWidth_OfDesign_IfRubberLines_AreExtended()
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "W",
                LengthOnZFold = "L"
            };

            packagingDesign.Lines.Add(new Line
            {
                StartX = "0",
                StartY = "0",
                Length = "W",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            });

            packagingDesign.Lines.Add(new ElasticLine()
            {
                StartX = "W",
                StartY = "0",
                Length = "200",
                Direction = LineDirection.horizontal,
                Type = LineType.cut,
                SwellPercent = 100,
            });

            packagingDesign.Lines.Add(new Line
            {
                StartX = "0",
                StartY = "H",
                Length = "W",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            });

            packagingDesign.Lines.Add(new ElasticLine()
            {
                StartX = "W",
                StartY = "H",
                Length = "200",
                Direction = LineDirection.horizontal,
                Type = LineType.cut,
                SwellPercent = 100,
            });

            packagingDesign.Lines.Add(new Line
            {
                StartX = "0",
                StartY = "H",
                Length = "W",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            });

            packagingDesign.Lines.Add(new Line
            {
                StartX = "0",
                StartY = "0",
                Length = "L",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            });

            packagingDesign.Lines.Add(new Line
            {
                StartX = "W+200",
                StartY = "0",
                Length = "L",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            });

            var parameters = new DesignFormulaParameters
            {
                CartonHeight = 200,
                CartonLength = 200,
                CartonWidth = 600,
                ZfoldThickness = 3,
                ZfoldWidth = 800,
            };

            var appliedDesign = packagingDesign.AsPhysicalDesign(parameters);
            Specify.That(appliedDesign.Width).Should.BeLogicallyEqualTo(800);
        }

        private static void CreateFileBackup(string fileName, int fileAccessAttempts)
        {
            var originalFilePath = string.Format(".\\TestData\\{0}", fileName);
            Specify.That(File.Exists(originalFilePath)).Should.BeTrue();

            var backupFilePath = string.Format(".\\TestData\\{0}bckup", fileName);

            try
            {
                File.Copy(originalFilePath, backupFilePath, true);
            }
            catch
            {
                --fileAccessAttempts;
                Specify.That(fileAccessAttempts).Should.Not.BeEqualTo(0);
                CreateFileBackup(fileName, fileAccessAttempts);
            }

            Specify.That(File.Exists(backupFilePath)).Should.BeTrue();
        }

        private static void RestoreFile(string fileName, int fileAccessAttempts)
        {
            var backupFilePath = string.Format(".\\TestData\\{0}bckup", fileName);
            Specify.That(File.Exists(backupFilePath)).Should.BeTrue();

            var originalFilePath = string.Format(".\\TestData\\{0}", fileName);

            try
            {
                File.Copy(backupFilePath, originalFilePath, true);
            }
            catch
            {
                --fileAccessAttempts;
                Specify.That(fileAccessAttempts).Should.Not.BeEqualTo(0);
                RestoreFile(fileName, fileAccessAttempts);
            }

            Specify.That(File.Exists(originalFilePath)).Should.BeTrue();
        }

        private static void DeleteFile(string fileName, int fileAccessAttempts)
        {
            var filePath = string.Format(".\\TestData\\{0}", fileName);
            Specify.That(File.Exists(filePath)).Should.BeTrue();

            try
            {
                File.Delete(filePath);
            }
            catch (Exception e)
            {
                Thread.Sleep(1000);
                Debug.WriteLine(e.ToString());
                --fileAccessAttempts;
                Specify.That(fileAccessAttempts).Should.Not.BeEqualTo(0);
                DeleteFile(fileName, fileAccessAttempts);
            }

            Specify.That(File.Exists(filePath)).Should.BeFalse();
        }
    }
}
