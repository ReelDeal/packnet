﻿namespace BusinessTests.PackagingDesigns
{
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO.PackagingDesigns;

    [TestClass]
    public class RotationPermissionsTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree0NoneSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree0>none</degree0></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree0.Allowed);
            Assert.IsFalse(result.Degree0Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree0BothSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree0>both</degree0></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree0.Allowed);
            Assert.IsTrue(result.Degree0Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree0NormalSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree0>normal</degree0></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree0.Allowed);
            Assert.IsFalse(result.Degree0Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree0FlipSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree0>flip</degree0></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree0.Allowed);
            Assert.IsTrue(result.Degree0Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree270NoneSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree270>none</degree270></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree270.Allowed);
            Assert.IsFalse(result.Degree270Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree270BothSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree270>both</degree270></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree270.Allowed);
            Assert.IsTrue(result.Degree270Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree270NormalSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree270>normal</degree270></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree270.Allowed);
            Assert.IsFalse(result.Degree270Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree270FlipSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree270>flip</degree270></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree270.Allowed);
            Assert.IsTrue(result.Degree270Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree90NoneSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree90>none</degree90></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree90.Allowed);
            Assert.IsFalse(result.Degree90Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree90BothSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree90>both</degree90></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree90.Allowed);
            Assert.IsTrue(result.Degree90Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree90NormalSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree90>normal</degree90></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree90.Allowed);
            Assert.IsFalse(result.Degree90Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree90FlipSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree90>flip</degree90></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree90.Allowed);
            Assert.IsTrue(result.Degree90Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree180NoneSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree180>none</degree180></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree180.Allowed);
            Assert.IsFalse(result.Degree180Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree180BothSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree180>both</degree180></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree180.Allowed);
            Assert.IsTrue(result.Degree180Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree180NormalSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree180>normal</degree180></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsTrue(result.Degree180.Allowed);
            Assert.IsFalse(result.Degree180Flip.Allowed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SerializedDegree180FlipSetsCorrectDegrees()
        {
            var serialized = "<rotate><degree180>flip</degree180></rotate>";
            var serializer = "rotate".GetSerializer<RotationPermissions>();
            var result = serializer.Deserialize(new StringReader(serialized)) as RotationPermissions;
            Assert.IsFalse(result.Degree180.Allowed);
            Assert.IsTrue(result.Degree180Flip.Allowed);
        }
    }
}
