﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Business.Orders;
using PackNet.Common;
using PackNet.Business.ProductionGroups;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Services.SelectionAlgorithm.Orders;

using Testing.Specificity;
using TestUtils;
using PackNet.Data.Orders;
using PackNet.Services.ProductionGroup;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Data.ProductionGroups;
using PackNet.Services;
using PackNet.Business.CartonPropertyGroup;
using PackNet.Business.Machines;
using PackNet.Data.CartonPropertyGroups;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Data.Machines;
using PackNet.Services.MachineServices;
using PackNet.Services.SelectionAlgorithm;

namespace BusinessTests.WorkFlow
{
    [TestClass]
    [DeploymentItem("Workflow/Workflows", "TestingWorkflows/Workflows")]
    public class OrderSelectProducibleWorkFlowTests
    {
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IUserNotificationService> userNotificationServiceMock;

        private Mock<IMachineGroupService> machineGroupServiceMock;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IAggregateMachineService> aggregateMachineServiceMock;
        private Mock<ISelectionAlgorithmDispatchService> selectionDispatchServiceMock;
        private Mock<IOrdersSelectionAlgorithmService> selectionAlgorithmServiceMock; // should be real implementation man

        //private Mock<IMachineGroups> machineGroupsMock;
        //private Mock<IAggregateMachineService> aggregateMachineServiceMock;
        //private Mock<IProductionGroupService> productionGroupServiceMock;

        //private Mock<ICartonPropertyGroupService> cpgServiceMock;
        //private Mock<IClassificationService> classificationServiceMock;

        //private Mock<IPickZoneService> pickZoneServiceMock;

        private EventAggregator aggregator;
        //private IEnumerable<BoxFirstProducible> producibles;

        private ILogger logger = new ConsoleLogger();

        private const string workFlowPath = @"TestingWorkflows\Workflows\OrderSelectProducibleWorkFlow.xaml";

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();

            userNotificationServiceMock = new Mock<IUserNotificationService>();
            machineGroupServiceMock = new Mock<IMachineGroupService>();
            selectionAlgorithmServiceMock = new Mock<IOrdersSelectionAlgorithmService>();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
            aggregateMachineServiceMock = new Mock<IAggregateMachineService>();
            selectionDispatchServiceMock = new Mock<ISelectionAlgorithmDispatchService>();

            //var orderSelectionAlgorithm = SetupOrderSelectionAlgorithmService();

            //IMachineGroupService = new MachineGroupService(machineGroupsMock.Object, aggregateMachineServiceMock.Object, productionGroupServiceMock.Object, )

            /*mg = new MachineGroup()
            {
                Id = Guid.NewGuid(),
            };*/

        }
        
        [TestMethod]
        [TestCategory("Integration")]
        public void Should_Select_Producible_ThatIsAKitContaining_ASingle_Carton()
        {
            Assert.Inconclusive("Not implemented");
            var mgId = Guid.NewGuid();

            var machineGroup = new MachineGroup
            {
                Alias = "MG1",
                Id = mgId,
                ProductionMode = MachineGroupProductionModes.ManualProductionMode
            };

            machineGroupServiceMock.Setup(mgs => mgs.FindByMachineGroupId(It.Is<Guid>(g => g == mgId))).Returns(machineGroup);

            serviceLocatorMock = SetupServiceLocatorMock();

            var inArgs = new Dictionary<string, object>
                             {                                 
                                 { "ServiceLocator", serviceLocatorMock.Object},                                 
                                 { "Publisher", aggregator},
                                 { "MachineGroupId", mgId}
                             };

            var assembly = GetType().Assembly;
                        
            var testingWorkflowsPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), workFlowPath);

            var outPut = WorkflowHelper.InvokeWorkFlowAndWaitForResult(testingWorkflowsPath, assembly, inArgs, userNotificationServiceMock.Object, logger, new Mock<WorkflowLifetime>().Object);

            machineGroupServiceMock.Verify(mgs => mgs.FindByMachineGroupId(It.Is<Guid>(g => g == mgId)), Times.Once());

            selectionAlgorithmServiceMock.Verify(sel => sel.GetManualJobForMachineGroup(It.Is<Guid>(g => g == mgId)), Times.Once());

            selectionAlgorithmServiceMock.Verify(sel => sel.GetAutoJobForMachineGroup(It.Is<Guid>(g => g == mgId)), Times.Never());
        }

        private Mock<IServiceLocator> SetupServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<IOrdersSelectionAlgorithmService>()).Returns(selectionAlgorithmServiceMock.Object);
            mock.Setup(m => m.Locate<IMachineGroupService>()).Returns(machineGroupServiceMock.Object);
            //mock.Setup(m => m.Locate<ICartonPropertyGroupService>()).Returns(cpgServiceMock.Object);
            //mock.Setup(m => m.Locate<IClassificationService>()).Returns(classificationServiceMock.Object);
            //mock.Setup(m => m.Locate<IProductionGroupService>()).Returns(productionGroupServiceMock.Object);
            //mock.Setup(m => m.Locate<IPickZoneService>()).Returns(pickZoneServiceMock.Object);

            return mock;
        }

        private IOrdersSelectionAlgorithmService SetupOrderSelectionAlgorithmService()
        {
            var productionGroups = new PackNet.Business.ProductionGroups.ProductionGroups(aggregator, aggregator, new Mock<IProductionGroupRepository>().Object, serviceLocatorMock.Object, new Mock<ILogger>().Object);

            var cpgs = new CartonPropertyGroups(new Mock<ICartonPropertyGroupRepository>().Object, aggregator, productionGroups);

            var cpgService = new CartonPropertyGroupService(aggregator, aggregator, cpgs, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger);

            var productionGroupService = new ProductionGroupService(productionGroups, serviceLocatorMock.Object, uiCommunicationServiceMock.Object, aggregator, aggregator, cpgService, logger);

            var ordersRepoMock = new Mock<IOrdersRepository>();

            var orders = new Orders(ordersRepoMock.Object);

            //var machineGroupse = new MachineGroups(;)

            var machineGroupsBl = new MachineGroupBl(new Mock<MachineGroupRepository>().Object,
                selectionDispatchServiceMock.Object,
                aggregateMachineServiceMock.Object,
                aggregator,
                new Mock<IMachineGroupWorkflowDispatcher>().Object,
                logger,
                aggregator,
                userNotificationServiceMock.Object
                );

            var machineGroupService = new MachineGroupService(machineGroupsBl,
                aggregateMachineServiceMock.Object,
                productionGroupService,
                selectionDispatchServiceMock.Object,
                serviceLocatorMock.Object,
                uiCommunicationServiceMock.Object,
                aggregator,
                aggregator, logger);

            //var orderSelectionAlgorithm = new OrdersSelectionAlgorithmService(orders, productionGroupService, machineGroupService, )

            return null;   
        }
    }
}
