﻿using System;
using System.Activities;
using System.Collections.Generic;

using PackNet.Business.SelectionAlgorithms.Plugins.BoxFirst;
using PackNet.Common;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace BusinessTests.WorkFlow
{
    public class WorkflowHelpers
    {
        public static IProducible DispatchBoxFirstSelectProducibleWorkFlow(string wokflowPath, Guid MachineGroupId, IServiceLocator locator, IEventAggregatorPublisher publisher)
        {
            var assembly = typeof(BoxFirstSelectionAlgorithmPlugin).Assembly;
            var activity = WorkflowHelper.GetActivity(wokflowPath, assembly);

            var inArgs = new Dictionary<string, object>()
            {
                {"MachineGroupId", MachineGroupId},
                {"ServiceLocator", locator},
                {"Publisher", publisher},
            };

            var outputs = WorkflowInvoker.Invoke(activity, inArgs);

            object selectedProducible = null;
            if (!outputs.TryGetValue("selectedProducible", out selectedProducible))
            {
                throw new Exception("Couldnt find selectedProducible");
            }

            return selectedProducible as IProducible;
        }

        public static void DispatchBoxFirstStageProducibleWorkFlow(string wokflowPath, IEnumerable<IProducible> producibles, IServiceLocator locator, IEventAggregatorPublisher publisher)
        {
            var assembly = typeof(BoxFirstSelectionAlgorithmPlugin).Assembly;
            var activity = WorkflowHelper.GetActivity(wokflowPath, assembly);

            var inArgs = new Dictionary<string, object>()
            {
                {"Producibles", producibles},
                {"ServiceLocator", locator},
                {"Publisher", publisher},
            };

            WorkflowInvoker.Invoke(activity, inArgs);
        }
    }
}
