﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using System;
using System.Collections.Generic;
using System.Linq;
using Testing.Specificity;

namespace BusinessTests.WorkFlow
{
    [TestClass]
    [DeploymentItem("Workflow/Workflows", "TestingWorkflows/Workflows")]
    public class BoxFirstStagingTests
    {
        private Mock<IServiceLocator> locatorMock;
        private Mock<IBoxFirstSelectionAlgorithmService> selectionAlgorithmServiceMock;

        //private MachineGroup mg;

        private EventAggregator aggregator;

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();
        }

        [TestMethod]
        public void ProduciblesWithoutCartonsOnCorrugate_ShouldGetStatus_NotProducible()
        {
            var producibles = GetSomeProducibles();
            SetupSelectionAlgorithmService(producibles);
            SetupServiceLocatorMock();

            WorkflowHelpers.DispatchBoxFirstStageProducibleWorkFlow("./TestingWorkflows/Workflows/BoxFirstStagingWorkFlow.xaml", producibles, locatorMock.Object, aggregator);
            Specify.That(producibles.All(p => p.ProducibleStatus == ErrorProducibleStatuses.NotProducible))
                .Should.BeTrue("None of the producibles have a carton on corrugate but was marked as ProducibleStaged");
        }

        [TestMethod]
        public void ProduciblesWithCartonsOnCorrugate_ShouldNotGetStatus_NotProducible()
        {
            var producibles = GetSomeProducibles();

            producibles.First().CartonOnCorrugates = new List<CartonOnCorrugate>{ new CartonOnCorrugate() };

            SetupSelectionAlgorithmService(producibles);
            SetupServiceLocatorMock();

            WorkflowHelpers.DispatchBoxFirstStageProducibleWorkFlow("./TestingWorkflows/Workflows/BoxFirstStagingWorkFlow.xaml", producibles, locatorMock.Object, aggregator);
            Specify.That(producibles.First().ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged)
                .Should.BeTrue("First producible has a carton on corrugate but was not marked as staged");
        }

        [TestMethod]
        [TestCategory("Bug 10424")]
        [Description("When a production group has changed we must restage producible in not producible status since assigned machine groups and/or corrugates could have been changed")]
        public void ProducibleWithPreviousStatusNotProducible_ShouldGetStatusStagedAfterStaging()
        {
            var producibles = GetSomeProducibles();
            SetupSelectionAlgorithmService(producibles);
            SetupServiceLocatorMock();

            WorkflowHelpers.DispatchBoxFirstStageProducibleWorkFlow("./TestingWorkflows/Workflows/BoxFirstStagingWorkFlow.xaml", producibles, locatorMock.Object, aggregator);
            Specify.That(producibles.All(p => p.ProducibleStatus == ErrorProducibleStatuses.NotProducible))
                .Should.BeTrue("None of the producibles have a carton on corrugate but was marked as ProducibleStaged");

            producibles.First().CartonOnCorrugates = new List<CartonOnCorrugate> { new CartonOnCorrugate() };
            WorkflowHelpers.DispatchBoxFirstStageProducibleWorkFlow("./TestingWorkflows/Workflows/BoxFirstStagingWorkFlow.xaml", producibles, locatorMock.Object, aggregator);
            Specify.That(producibles.First().ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged)
                .Should.BeTrue("First producible has a carton on corrugate but was not marked as staged");
        }

        private IEnumerable<BoxFirstProducible> GetSomeProducibles()
        {
            var producibles = new List<BoxFirstProducible>()
            {
                this.GetProducible("1"),
                this.GetProducible("2"),
                this.GetProducible("3"),
                this.GetProducible("4"),
                this.GetProducible("5"),
                this.GetProducible("6"),
                this.GetProducible("7"),
                this.GetProducible("8"),
                this.GetProducible("9"),
                this.GetProducible("10"),
                
                this.GetProducible("11"),
                this.GetProducible("12"),
                this.GetProducible("13"),
                this.GetProducible("14"),
                this.GetProducible("15"),
                this.GetProducible("16"),
                this.GetProducible("17"),
                this.GetProducible("18"),
                this.GetProducible("19"),
                this.GetProducible("20"),
                
                this.GetProducible("21"),
                this.GetProducible("22"),
                this.GetProducible("23"),
                this.GetProducible("24"),
                this.GetProducible("25"),
                this.GetProducible("26"),
                this.GetProducible("27"),
                this.GetProducible("28"),
                this.GetProducible("29"),
                this.GetProducible("30"),
            };

            return producibles;
        }

        private BoxFirstProducible GetProducible(string customerId)
        {
            var p1 = new BoxFirstProducible
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = customerId,
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported
            };
            return p1;
        }

        private void SetupServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<IBoxFirstSelectionAlgorithmService>()).Returns(selectionAlgorithmServiceMock.Object);

            locatorMock = mock;
        }

        private void SetupSelectionAlgorithmService(IEnumerable<IProducible> producibles)
        {
            var mock = new Mock<IBoxFirstSelectionAlgorithmService>();

            mock.Setup(m => m.GetAllProduciblesWithStatuses(It.IsAny<IEnumerable<ProducibleStatuses>>())).Returns(producibles);
            mock.Setup(m => m.AssignOptimalCorrugateAndTilingPartnersForProducibles(It.IsAny<IEnumerable<BoxFirstProducible>>())).Returns((IEnumerable<BoxFirstProducible> bfs) => bfs);
            mock.Setup(m => m.UpdateProducibleStatuses(It.IsAny<IEnumerable<BoxFirstProducible>>(), It.IsAny<ProducibleStatuses>()))
                .Callback<IEnumerable<BoxFirstProducible>, ProducibleStatuses>(
                    (boxFirstProducibles, status) => boxFirstProducibles.ForEach(bf => bf.ProducibleStatus = status));

            selectionAlgorithmServiceMock = mock;
        }
    }
}
