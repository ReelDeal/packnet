﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

using Testing.Specificity;

namespace BusinessTests.WorkFlow
{
    [TestClass]
    [DeploymentItem("Workflow/Workflows", "TestingWorkflows/Workflows")]
    public class BoxFirstSelectProducibleWorkflowTests
    {
        private Mock<IServiceLocator> locatorMock;
        private Mock<IMachineGroupService> machineGroupServiceMock;
        private Mock<IBoxFirstSelectionAlgorithmService> selectionAlgorithmServiceMock;
        private Mock<ICartonPropertyGroupService> cpgServiceMock;
        private Mock<IClassificationService> classificationServiceMock;
        private Mock<IProductionGroupService> productionGroupServiceMock;
        private Mock<IPickZoneService> pickZoneServiceMock;

        //private MachineGroup mg;

        private EventAggregator aggregator;
        private IEnumerable<BoxFirstProducible> producibles;

        private Classification c1;
        private Classification c2;
        private Classification c3;
        private Classification c4;

        private CartonPropertyGroup g1;
        private CartonPropertyGroup g2;
        private CartonPropertyGroup g3;

        private int callCount;
        
        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();

            /*mg = new MachineGroup()
            {
                Id = Guid.NewGuid(),
            };*/
            
        }

        [TestMethod]
        [TestCategory("Integration")]
        [Ignore]
        public void ShouldSelectProducible_WithTilingPartners_WhenRunningSelectionWorkflow()
        {
            var mg = new MachineGroup() { Id = Guid.NewGuid() };


            SetupRestrictions();
            producibles = GetSomeProducibles();
            UpdateStatusForProducibles(producibles, new List<int>() { 2, 11 }, InProductionProducibleStatuses.ProducibleSentToMachine);

            SetupCartonPropertyGroupServiceMock(new List<CartonPropertyGroup>() { g1, g2, g3 });
            SetClassificationServiceMock(new List<Classification>() { c1, c2, c3, c4 });
            SetupProductionGroupService();
            SetupPickZoneService();
            SetupMachineGroupServiceMock(new List<MachineGroup>() { mg });
            SetupSelectionAlgorithmService(Verify);
            SetupServiceLocatorMock();

            var output =
                WorkflowHelpers.DispatchBoxFirstSelectProducibleWorkFlow(@"./TestingWorkflows/Workflows/BoxFirstSelectProducibleWorkFlow.xaml", mg.Id,
                    locatorMock.Object, aggregator);

            Specify.That(output.Id).Should.BeEqualTo(producibles.First().Id);
        }



        [TestMethod]
        [TestCategory("Integration")]
        [Ignore]
        public void ShouldProduceBoxesAsExpectedWhenPrioritizingByCpg()
        {
            var mg = new MachineGroup() { Id = Guid.NewGuid() };

            SetupOrderingTest(VerifyCpgOrdering);

            var output =
                WorkflowHelpers.DispatchBoxFirstSelectProducibleWorkFlow(
                    "./TestingWorkflows/Workflows/BoxFirstSelectProducibleWithPrioritizedCpgWorkFlow.xaml", mg.Id, locatorMock.Object, aggregator);

            Specify.That(output).Should.Not.BeNull();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [Ignore]
        public void ShouldProduceBoxesAsExpectedWhenPrioritizingByClassification()
        {
            var mg = new MachineGroup() { Id = Guid.NewGuid() };

            SetupOrderingTest(VerifyClassificationOrdering);

            var output =
                WorkflowHelpers.DispatchBoxFirstSelectProducibleWorkFlow(
                    "./TestingWorkflows/Workflows/BoxFirstSelectProducibleWorkFlow.xaml", mg.Id, locatorMock.Object, aggregator);

            Specify.That(output).Should.Not.BeNull();
        }

        private void SetupOrderingTest(Action<IEnumerable<IProducible>> veriyFunction)
        {
            var mg = new MachineGroup() { Id = Guid.NewGuid() };

            var cls1 = new Classification() { Id = Guid.NewGuid(), Alias = "CLS1", Status = ClassificationStatuses.Expedite };
            var cls2 = new Classification() { Id = Guid.NewGuid(), Alias = "CLS2", Status = ClassificationStatuses.Normal };
            var cls3 = new Classification() { Id = Guid.NewGuid(), Alias = "CLS3", Status = ClassificationStatuses.Last };

            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ1", Status = CartonPropertyGroupStatuses.Surge };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "AZ2", Status = CartonPropertyGroupStatuses.Normal };

            var c1 = GetBoxFirstProducibleCarton("1", cls1, cpg1);
            var c2 = GetBoxFirstProducibleCarton("2", cls2, cpg1);
            var c3 = GetBoxFirstProducibleCarton("3", cls3, cpg1);
            var c4 = GetBoxFirstProducibleCarton("4", cls1, cpg2);
            var c5 = GetBoxFirstProducibleCarton("5", cls2, cpg2);
            var c6 = GetBoxFirstProducibleCarton("6", cls3, cpg2);

            producibles = new List<BoxFirstProducible>() { c6, c5, c4, c3, c2, c1 };
            foreach (var boxFirstProducible in producibles)
            {
                boxFirstProducible.PossibleTilingPartners = producibles.Where(p => p.Id.Equals(boxFirstProducible.Id) == false).ToList();
            }

            SetupCartonPropertyGroupServiceMock(new List<CartonPropertyGroup>() { cpg1, cpg2 });
            SetClassificationServiceMock(new List<Classification>() { cls1, cls2, cls3 });
            SetupProductionGroupService();
            SetupPickZoneService();
            SetupMachineGroupServiceMock(new List<MachineGroup>() { mg });
            SetupSelectionAlgorithmService(veriyFunction);
            SetupServiceLocatorMock();
        }

        private void SetupCartonPropertyGroupServiceMock(IEnumerable<CartonPropertyGroup> cpgs)
        {
            cpgServiceMock = new Mock<ICartonPropertyGroupService>();
            cpgServiceMock.Setup(m => m.GetCartonPropertyGroupsByStatus(It.IsAny<CartonPropertyGroupStatuses>()))
                                    .Returns<CartonPropertyGroupStatuses>((status) => cpgs.Where(g => g.Status == status));

            cpgServiceMock.Setup(
                s =>
                    s.GetNextCPGToDispatchWorkTo(It.IsAny<ProductionGroup>(), It.IsAny<IEnumerable<IProducible>>(),
                        It.IsAny<IEnumerable<IProducible>>()))
                .Returns<ProductionGroup, IEnumerable<IProducible>, IEnumerable<IProducible>>(
                    (pg, b, c) => GetNextCartonPropertyGroupForMix(cpgs));
        }

        private CartonPropertyGroup GetNextCartonPropertyGroupForMix(IEnumerable<CartonPropertyGroup> cpgs)
        {
            if (callCount >= cpgs.Count())
                callCount = 0;
            var cpg = cpgs.ElementAt(callCount);
            callCount++;
            return cpg;
        }

        private void SetClassificationServiceMock(IEnumerable<Classification> classifications)
        {
            classificationServiceMock = new Mock<IClassificationService>();
            classificationServiceMock.Setup(m => m.GetClassificationsByStatus(It.IsAny<ClassificationStatuses>()))
                                        .Returns<ClassificationStatuses>(status => classifications.Where(c => c.Status == status));
        }

        private void UpdateStatusForProducibles(IEnumerable<BoxFirstProducible> boxFirstProducibles, List<int> elementsToUpdate, ProducibleStatuses newStatus)
        {
            foreach (var elementId in elementsToUpdate)
            {
                boxFirstProducibles.ElementAt(elementId).ProducibleStatus = newStatus;
                boxFirstProducibles.ElementAt(elementId).Producible.ProducibleStatus = newStatus;
            }
        }

        private void SetupRestrictions()
        {
            c1 = new Classification()
            {
                Id = Guid.NewGuid(),
                Status = ClassificationStatuses.Expedite,
                Number = "1"
            };
            c2 = new Classification()
            {
                Id = Guid.NewGuid(),
                Status = ClassificationStatuses.Normal,
                Number = "2"
            };
            c3 = new Classification()
            {
                Id = Guid.NewGuid(),
                Status = ClassificationStatuses.Last,
                Number = "3"
            };
            c4 = new Classification()
            {
                Id = Guid.NewGuid(),
                Status = ClassificationStatuses.Stop,
                Number = "4"
            };

            g1 = new CartonPropertyGroup()
            {
                Id = Guid.NewGuid(),
                Status = CartonPropertyGroupStatuses.Surge
            };
            g2 = new CartonPropertyGroup()
            {
                Id = Guid.NewGuid(),
                Status = CartonPropertyGroupStatuses.Normal
            };
            g3 = new CartonPropertyGroup()
            {
                Id = Guid.NewGuid(),
                Status = CartonPropertyGroupStatuses.Stop
            };
        }

        private IEnumerable<BoxFirstProducible> GetSomeProducibles()
        {
            var producibles = new List<BoxFirstProducible>()
            {
                GetBoxFirstProducibleCarton("1", c1, g1),
                GetBoxFirstProducibleCarton("2", c2, g1),
                GetBoxFirstProducibleCarton("3", c2, g1),
                GetBoxFirstProducibleCarton("4", c3, g1),
                GetBoxFirstProducibleCarton("5", c3, g1),
                GetBoxFirstProducibleCarton("6", c3, g1),
                GetBoxFirstProducibleCarton("7", c4, g1),
                GetBoxFirstProducibleCarton("8", c4, g1),
                GetBoxFirstProducibleCarton("9", c4, g1),
                GetBoxFirstProducibleCarton("10", c4, g1),
                
                GetBoxFirstProducibleCarton("11", c1, g2),
                GetBoxFirstProducibleCarton("12", c2, g2),
                GetBoxFirstProducibleCarton("13", c2, g2),
                GetBoxFirstProducibleCarton("14", c3, g2),
                GetBoxFirstProducibleCarton("15", c3, g2),
                GetBoxFirstProducibleCarton("16", c3, g2),
                GetBoxFirstProducibleCarton("17", c4, g2),
                GetBoxFirstProducibleCarton("18", c4, g2),
                GetBoxFirstProducibleCarton("19", c4, g2),
                GetBoxFirstProducibleCarton("20", c4, g2),
                
                GetBoxFirstProducibleCarton("21", c1, g3),
                GetBoxFirstProducibleCarton("22", c2, g3),
                GetBoxFirstProducibleCarton("23", c2, g3),
                GetBoxFirstProducibleCarton("24", c3, g3),
                GetBoxFirstProducibleCarton("25", c3, g3),
                GetBoxFirstProducibleCarton("26", c3, g3),
                GetBoxFirstProducibleCarton("27", c4, g3),
                GetBoxFirstProducibleCarton("28", c4, g3),
                GetBoxFirstProducibleCarton("29", c4, g3),
                GetBoxFirstProducibleCarton("30", c4, g3),
            };
            foreach (var boxFirstProducible in producibles)
            {
                boxFirstProducible.PossibleTilingPartners = producibles.Where(p => p.Id.Equals(boxFirstProducible.Id) == false).ToList();
            }

            return producibles;
        }

        private BoxFirstProducible GetBoxFirstProducibleCarton(string customerId, Classification classification, CartonPropertyGroup cpg)
        {
            var p1 = new BoxFirstProducible
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = customerId,
                Producible = new Carton { CustomerUniqueId = customerId, ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged},
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged
            };
            p1.Restrictions.Add(new BasicRestriction<Classification>(classification));
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg));

            p1.CartonOnCorrugates = new [] { new CartonOnCorrugate() };

            return p1;
        }

        private void SetupServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<IBoxFirstSelectionAlgorithmService>()).Returns(selectionAlgorithmServiceMock.Object);
            mock.Setup(m => m.Locate<IMachineGroupService>()).Returns(machineGroupServiceMock.Object);
            mock.Setup(m => m.Locate<ICartonPropertyGroupService>()).Returns(cpgServiceMock.Object);
            mock.Setup(m => m.Locate<IClassificationService>()).Returns(classificationServiceMock.Object);
            mock.Setup(m => m.Locate<IProductionGroupService>()).Returns(productionGroupServiceMock.Object);
            mock.Setup(m => m.Locate<IPickZoneService>()).Returns(pickZoneServiceMock.Object);

            locatorMock = mock;
        }

        private void SetupSelectionAlgorithmService(Action<IEnumerable<IProducible>> orderVerification)
        {
            var mock = new Mock<IBoxFirstSelectionAlgorithmService>();

            //mock.Setup(m => m.GetAllJobsOfAnyStatusForMachineGroup(It.IsAny<Guid>())).Callback<Guid>(g =>
            //    Specify.That(g).Should.BeEqualTo(mg.Id)).Returns(producibles);
            mock.Setup(m => m.GetAllStagedJobsForMachineGroup(It.IsAny<Guid>())).Returns(producibles);

            mock.Setup(m => m.GetProduciblesWhereMachineGroupContainsMostOptimalCorrugate(It.IsAny<MachineGroup>(), It.IsAny<IEnumerable<BoxFirstProducible>>()))
                .Returns(producibles);

            mock.Setup(m => m.GetTileCountToUse(It.IsAny<Guid>(), It.IsAny<BoxFirstProducible>(), It.IsAny<IEnumerable<IProducible>>()))
                .Returns<Guid, BoxFirstProducible, IEnumerable<IProducible>>((mg, p, c) => p.CartonOnCorrugates.First().TileCount);

            mock.Setup(m => m.PrepareNextJobForMachineGroup(It.IsAny<MachineGroup>(), It.IsAny<IEnumerable<IProducible>>()))
                .Callback<MachineGroup, IEnumerable<IProducible>>((group, prods) =>
                {
                    //Specify.That(group).Should.BeEqualTo(mg);
                    //orderVerification(prods);
                })
                .Returns(producibles.First());

            selectionAlgorithmServiceMock = mock;
        }

        private void SetupMachineGroupServiceMock(IEnumerable<MachineGroup> mgs)
        {
            var mock = new Mock<IMachineGroupService>();

            mock.Setup(m => m.FindByMachineGroupId(It.IsAny<Guid>())).Returns<Guid>(mgId => mgs.First(mg => mg.Id == mgId));

            machineGroupServiceMock = mock;
        }

        private void SetupProductionGroupService()
        {
            productionGroupServiceMock = new Mock<IProductionGroupService>();
            productionGroupServiceMock.Setup(m => m.GetProductionGroupForMachineGroup(It.IsAny<Guid>()))
                .Returns(new ProductionGroup());
        }

        private void SetupPickZoneService()
        {
            pickZoneServiceMock = new Mock<IPickZoneService>();
            pickZoneServiceMock.Setup(m => m.GetPickZonesByStatus(It.IsAny<PickZoneStatuses>())).Returns(new List<PickZone>());
        }

        private void Verify(IEnumerable<IProducible> prods)
        {
            Specify.That(prods.Count()).Should.BeEqualTo(10);
            Specify.That(prods.ElementAt(0).Id).Should.BeEqualTo(producibles.ElementAt(0).Id);
            Specify.That(prods.ElementAt(1).Id).Should.BeEqualTo(producibles.ElementAt(10).Id);
            Specify.That(prods.ElementAt(2).Id).Should.BeEqualTo(producibles.ElementAt(1).Id);
            Specify.That(prods.ElementAt(3).Id).Should.BeEqualTo(producibles.ElementAt(12).Id);
            Specify.That(prods.ElementAt(4).Id).Should.BeEqualTo(producibles.ElementAt(3).Id);
            Specify.That(prods.ElementAt(5).Id).Should.BeEqualTo(producibles.ElementAt(4).Id);
            Specify.That(prods.ElementAt(6).Id).Should.BeEqualTo(producibles.ElementAt(5).Id);
            Specify.That(prods.ElementAt(7).Id).Should.BeEqualTo(producibles.ElementAt(13).Id);
            Specify.That(prods.ElementAt(8).Id).Should.BeEqualTo(producibles.ElementAt(14).Id);
            Specify.That(prods.ElementAt(9).Id).Should.BeEqualTo(producibles.ElementAt(15).Id);
        }

        private void VerifyCpgOrdering(IEnumerable<IProducible> prods)
        {
            Specify.That(prods.Count()).Should.BeEqualTo(6);
            Specify.That(prods.ElementAt(0).Id).Should.BeEqualTo(producibles.ElementAt(5).Id);
            Specify.That(prods.ElementAt(1).Id).Should.BeEqualTo(producibles.ElementAt(4).Id);
            Specify.That(prods.ElementAt(2).Id).Should.BeEqualTo(producibles.ElementAt(3).Id);
            Specify.That(prods.ElementAt(3).Id).Should.BeEqualTo(producibles.ElementAt(2).Id);
            Specify.That(prods.ElementAt(4).Id).Should.BeEqualTo(producibles.ElementAt(1).Id);
            Specify.That(prods.ElementAt(5).Id).Should.BeEqualTo(producibles.ElementAt(0).Id);
        }

        private void VerifyClassificationOrdering(IEnumerable<IProducible> prods)
        {
            Specify.That(prods.Count()).Should.BeEqualTo(6);
            Specify.That(prods.ElementAt(0).Id).Should.BeEqualTo(producibles.ElementAt(5).Id);
            Specify.That(prods.ElementAt(1).Id).Should.BeEqualTo(producibles.ElementAt(2).Id);
            Specify.That(prods.ElementAt(2).Id).Should.BeEqualTo(producibles.ElementAt(4).Id);
            Specify.That(prods.ElementAt(3).Id).Should.BeEqualTo(producibles.ElementAt(1).Id);
            Specify.That(prods.ElementAt(4).Id).Should.BeEqualTo(producibles.ElementAt(3).Id);
            Specify.That(prods.ElementAt(5).Id).Should.BeEqualTo(producibles.ElementAt(0).Id);
        }
    }
}
