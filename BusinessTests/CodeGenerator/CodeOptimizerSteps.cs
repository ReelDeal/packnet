﻿namespace BusinessTests.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    [Scope(Tag = "CodeOptimizer")]
    public class CodeOptimizerSteps
    {
        private List<InstructionItem> instructionList;
        private IEnumerable<InstructionItem> optimizedInstructionList;
        private CodeOptimizer optimizer;

        [Given(@"instruction list with crosshead positioning to the same position without feed in between")]
        public void GivenInstructionListWithCrossheadPositioningToTheSamePositionWithoutFeedInBetween()
        {
            instructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            instructionList.Add(new InstructionCrossHeadMovementItem(0));
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(100));
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.None });
            instructionList.Add(new InstructionCrossHeadMovementItem(100));
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            instructionList.Add(new InstructionCrossHeadMovementItem(200));
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.None });
        }

        [Given(@"a code generator optimizer")]
        public void GivenACodeGeneratorOptimizer()
        {
            optimizer = new CodeOptimizer();
        }

        [When(@"I optimize the instruction list")]
        public void WhenIOptimizeTheInstructionList()
        {
            optimizedInstructionList = optimizer.Optimize(instructionList);
        }

        [Then(@"unnecessary crosshead move and tool activation instructions are removed")]
        public void ThenUnnecessaryCrossheadMoveAndToolActivationInstructionsAreRemoved()
        {
            Specify.That(optimizedInstructionList.Count()).Should.BeEqualTo(7);
            Specify.That(optimizedInstructionList.ElementAt(0) is InstructionMetaDataItem).Should.BeTrue();
            Specify.That(optimizedInstructionList.ElementAt(1) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(1) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(0);
            Specify.That(optimizedInstructionList.ElementAt(2) is InstructionCrossHeadToolActivation).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(2) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(optimizedInstructionList.ElementAt(3) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(optimizedInstructionList.ElementAt(4) is InstructionCrossHeadToolActivation).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(optimizedInstructionList.ElementAt(5) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(optimizedInstructionList.ElementAt(6) is InstructionCrossHeadToolActivation).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.None);
        }

        [Given(@"instruction list with crosshead positioning to the same position with feed in between")]
        public void GivenInstructionListWithCrossheadPositioningToTheSamePositionWithFeedInBetween()
        {
            instructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            instructionList.Add(new InstructionCrossHeadMovementItem(200));
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.None });
            instructionList.Add(new InstructionFeedRollerItem(50));
            instructionList.Add(new InstructionCrossHeadMovementItem(200));
        }

        [Then(@"unnecessary crosshead move instructions are removed")]
        public void ThenUnnecessaryCrossheadMoveInstructionsAreRemoved()
        {
            Specify.That(optimizedInstructionList.Count()).Should.BeEqualTo(4);
            Specify.That(optimizedInstructionList.ElementAt(0) is InstructionMetaDataItem).Should.BeTrue();
            Specify.That(optimizedInstructionList.ElementAt(1) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(1) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(optimizedInstructionList.ElementAt(2) is InstructionCrossHeadToolActivation).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(2) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.None);
            Specify.That(optimizedInstructionList.ElementAt(3) is InstructionFeedRollerItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(3) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);
        }

        [Given(@"an instruction list with sequential crosshead positioning that have same tool state but disconnect track is set on the first movement")]
        public void GivenAnInstructionListWithSequentialCrossheadPositioningThatHaveSameToolStateButDisconnectIsSetOnTheFirstMovement()
        {
            instructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(200) { DisconnectTrack = true });
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(300) { DisconnectTrack = false });
        }

        [Given(@"an instruction list with sequential crosshead positioning that have same tool state but disconnect track is set on the second movement")]
        public void GivenAnInstructionListWithSequentialCrossheadPositioningThatHaveSameToolStateButDisconnectIsSetOnTheSecondMovement()
        {
            instructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(200) { DisconnectTrack = false });
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(300) { DisconnectTrack = true });
        }

        [Then(@"crosshead positionings are merged to one")]
        public void ThenCrossheadPositioningsAreMergedToOne()
        {
            Specify.That(optimizedInstructionList.Count(x => x is InstructionCrossHeadMovementItem)).Should.BeEqualTo(1);
            Specify.That((optimizedInstructionList.First(x => x is InstructionCrossHeadMovementItem) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(300);
        }

        [Then(@"disconnect track is set")]
        public void ThenDisconnectTrackIsSet()
        {
            Specify.That((optimizedInstructionList.First(x => x is InstructionCrossHeadMovementItem) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeLogicallyEqualTo(true);
        }

        [Then(@"tool activations are merged to one")]
        public void ThenToolActivationsAreMergedToOne()
        {
            Specify.That(optimizedInstructionList.Count(x => x is InstructionCrossHeadToolActivation)).Should.BeLogicallyEqualTo(1);
        }

        [Given(@"an instruction list with sequential crosshead positioning that have same tool state but disconnect track differs and are separated by feed")]
        public void GivenAnInstructionListWithSequentialCrossheadPositioningThatHaveSameToolStateButDisconnectTrackDiffersAndAreSeparatedByFeed()
        {
            instructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(200) { DisconnectTrack = false });
            instructionList.Add(new InstructionFeedRollerItem(50));
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(300) { DisconnectTrack = true });
        }

        [Then(@"crosshead positionings are not merged to one")]
        public void ThenCrossheadPositioningsAreNotMergedToOne()
        {
            Specify.That(optimizedInstructionList.Count()).Should.BeEqualTo(5);
            Specify.That(optimizedInstructionList.ElementAt(0) is InstructionMetaDataItem).Should.BeTrue();
            Specify.That(optimizedInstructionList.ElementAt(1) is InstructionCrossHeadToolActivation).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(1) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(optimizedInstructionList.ElementAt(2) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(2) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(200);
            Specify.That((optimizedInstructionList.ElementAt(2) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeLogicallyEqualTo(false);
            Specify.That(optimizedInstructionList.ElementAt(3) is InstructionFeedRollerItem).Should.BeTrue();
            Specify.That(optimizedInstructionList.ElementAt(4) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(4) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(300);
            Specify.That((optimizedInstructionList.ElementAt(4) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeLogicallyEqualTo(true);
        }

        [Given(@"an instruction list with sequential crosshead positioning that have same tool state but simultaneous with feed differs")]
        public void GivenAnInstructionListWithSequentialCrossHeadPositioningThatHaveSameToolStateButSimultaneousWithFeedDiffers()
        {
            instructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(200) { SimultaneousWithFeed = false });
            instructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            instructionList.Add(new InstructionCrossHeadMovementItem(300) { SimultaneousWithFeed = true });
        }

        [Then(@"crosshead positionings are not merged")]
        public void ThenCrossheadPositioningsAreNotMerged()
        {
            Specify.That(optimizedInstructionList.Count()).Should.BeEqualTo(4);
            Specify.That(optimizedInstructionList.ElementAt(0) is InstructionMetaDataItem).Should.BeTrue();
            Specify.That(optimizedInstructionList.ElementAt(1) is InstructionCrossHeadToolActivation).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(1) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(optimizedInstructionList.ElementAt(2) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(2) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(200);
            Specify.That((optimizedInstructionList.ElementAt(2) as InstructionCrossHeadMovementItem).SimultaneousWithFeed).Should.BeLogicallyEqualTo(false);
            Specify.That(optimizedInstructionList.ElementAt(3) is InstructionCrossHeadMovementItem).Should.BeTrue();
            Specify.That((optimizedInstructionList.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(300);
            Specify.That((optimizedInstructionList.ElementAt(3) as InstructionCrossHeadMovementItem).SimultaneousWithFeed).Should.BeLogicallyEqualTo(true);
        }
    }
}
