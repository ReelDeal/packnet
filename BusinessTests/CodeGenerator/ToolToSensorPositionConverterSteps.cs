﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace BusinessTests.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;

    using TechTalk.SpecFlow;
    using TechTalk.SpecFlow.Assist;

    [Binding]
    public class ToolToSensorPositionConverterSteps
    {
        [Given(@"longhead parameters with left side to sensor pin length (.*)")]
        public void GivenLongHeadParameters(double leftSideToSensorPinLength)
        {
            var longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = 1300,
                LeftSideToSensorPin = leftSideToSensorPinLength,
                LongheadWidth = 64
            };
            
            ScenarioContext.Current.Add("LongHeadParameters", longHeadParameters);
        }

        [Given(@"a number of longheads with left side to tool offset")]
        public void GivenANumberOfLongheadsWithLeftSideToToolOffset(Table table)
        {
            var longHeadParameters = ScenarioContext.Current["LongHeadParameters"] as FusionLongHeadParameters;
            var longHeadList = table.CreateSet<LongHeadTestContainer>();
            foreach (var testContainer in longHeadList)
            {
                var longHead = new FusionLongHead {
                    Number = testContainer.Lh,
                    LeftSideToTool = testContainer.LeftSideToToolOffset,
                    Type = ToolType.Cut
                };
                longHeadParameters.AddLongHead(longHead);
            }
        }

        [When(@"I convert the list of positions")]
        public void WhenIConvertTheListOfPositions(Table table)
        {
            var positions = table.CreateSet<PositionTestDataContainer>();
            var positionsAsInProductionCode = positions.Select(x => new LongHeadPositioningInformation(x.Lh, x.Pos, 0));
            var longHeadParameters = ScenarioContext.Current["LongHeadParameters"] as FusionLongHeadParameters;
            var longHeads = longHeadParameters.LongHeads;
            var longHeadInformations = new List<LongHeadInformation>();
            foreach (var longHead in longHeads)
            {
                var offset = longHeadParameters.LeftSideToSensorPin;
                var leftSideToTool = longHead.LeftSideToTool;
                var longHeadNumber = longHead.Number;
                longHeadInformations.Add(new LongHeadInformation(longHeadNumber, leftSideToTool, offset));
            }
            
            var result = new ToolToSensorPositionConverter(longHeadInformations).ConvertPosition(positionsAsInProductionCode);
            ScenarioContext.Current.Set(result);
        }

        [Then(@"the converted list of positions should be")]
        public void ThenTheConvertedListOfPositionsShouldBe(Table table)
        {
            var convertedPositions = ScenarioContext.Current.Get<IEnumerable<LongHeadPositioningInformation>>();
            var convertedPositionsForSpecFlow =
                convertedPositions.Select(
                    x => new PositionTestDataContainer { Lh = x.LongHeadNumber, Pos = x.Position });

            // Does not match order, but all values are unique.
            table.CompareToSet(convertedPositionsForSpecFlow);
        }
    }
}
