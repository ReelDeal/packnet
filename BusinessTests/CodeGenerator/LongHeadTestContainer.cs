namespace BusinessTests.CodeGenerator
{
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;

    public class LongHeadTestContainer
    {        
        public int Lh { get; set; }

        public double LeftSideToToolOffset { get; set; }

        public ToolType Type { get; set; }

        public double Position { get; set; }
    }
}