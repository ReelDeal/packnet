﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Moq;
using PackNet.Business.EmJobCreator;
using PackNet.Business.Orders;
using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace BusinessTests.CodeGenerator
{
    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.AStarStuff;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;

    internal static class EmCodeGeneratorTestHelper
    {
        internal class EmCodeGeneratorTester : EmCodeGenerator
        {
            public EmCodeGeneratorTester(MicroMeter crossHeadPosition, EmLongHeadParameters longHeadParameters, ICodeOptimizer optimizer)
                : base(crossHeadPosition, longHeadParameters, optimizer, new EmLongHeadDistributor())
            {

            }

            public new IEnumerable<InstructionItem> Generate(
                PhysicalDesign design,
                EmPhysicalMachineSettings machineSettings,
                LongHeadDistributionSolution longHeadDistribution,
                Track track,
                bool onTheFly)
            {
                return base.Generate(design, machineSettings, longHeadDistribution, track, onTheFly);
        }
        }

        internal class NonOptimizingOptimizer : ICodeOptimizer
        {
            public IEnumerable<InstructionItem> Optimize(IEnumerable<InstructionItem> instructionList)
            {
                return instructionList;
            }
        }

        internal static RuleAppliedPhysicalDesign GetDesignWithReverses()
        {
            var design = new RuleAppliedPhysicalDesign();

            var l1 = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.crease);
            var l2 = new PhysicalLine(new PhysicalCoordinate(160, 0), new PhysicalCoordinate(160, 500), LineType.crease);
            var l3 = new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(500, 500), LineType.cut);

            design.Add(l1);
            design.Add(l2);
            design.Add(l3);

            return design;
        }

        internal static RuleAppliedPhysicalDesign GetDesignForCrossheadOnTheFlyTest()
        {
            /*
             *  ________________________
             * |       ¦........¦       |
             * |____...¦________¦...____|
             * |       ¦        ¦       |
             * |_______¦________¦_______|
             * 
             * */

            var design = new RuleAppliedPhysicalDesign();
            design.Width = 600;
            design.Length = 200;
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(600, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(600, 200), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(600, 0), new PhysicalCoordinate(600, 200), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 25), new PhysicalCoordinate(400, 25), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 50), new PhysicalCoordinate(400, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(500, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(500, 50), new PhysicalCoordinate(600, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(100, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 50), new PhysicalCoordinate(200, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 50), new PhysicalCoordinate(200, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(400, 200), LineType.crease));



            return design;
        }

        internal static EmPhysicalMachineSettings GetInchEmMachineParameters(int nrLongheads = 4, double crossHeadPosition = 10)
        {
            var longHeads = new List<EmLongHead>();

            for (int i = 0; i < nrLongheads; i++)
            {
                longHeads.Add(new EmLongHead()
                {
                    ToolToSensorOffset = (i == 0 || i == 1 || i == 3) ? -0.236 : 0.236,
                    LeftSideToTool = (i == 0 || i == 1 || i == 3) ? 1.85 : 0.236,
                    MaximumPosition = 98.189,
                    MinimumPosition = 1.969,
                    Number = (i + 1),
                    Position = (i + 1) * 10,
                    Width = 2.126
                });
            }

            var longHeadParameters = new EmLongHeadParameters(
                longHeads: longHeads, 
                maximumMovementSpeed: 14.1732, 
                percentMovementSpeed: 55,
                maximumAcceleration: 59.5514, 
                percentAcceleration: 55, 
                longheadConnectionDelay: 80, 
                maximumPosition: 98.189, 
                minimumPosition: 1.959, 
                longheadWidth: 2.126, 
                longheadYOffset: 5.591, 
                leftSideToWasteSeparator: 0.59, 
                wasteSeparatorWidth: 0.945); 

            var rollerParameters = new EmFeedRollerParameters(
                maximumMovementSpeed:  5.9055, 
                percentMovementSpeed: 100,
                maximumAcceleration: 16.777, 
                percentageAcceleration: 100, 
                maximumReverseDistance: 23.6, 
                outFeedLength: 15.7); 

            var crossHeadParameters = new EmCrossHeadParameters()
            {
                Position = crossHeadPosition,
                CutCompensation = 0.276,
                CreaseCompensation = 0.276,
                MovementData = new EmNormalMovementData()
                {
                    Acceleration = 59.5514,
                    Speed = 14.173,
                    NormalMovement = new EmMovementData()
                    {
                        AccelerationInPercent = 100,
                        SpeedInPercent = 100,
                        Torque = 100
                    }
                }
            };

            var emMachineParameters = new EmPhysicalMachineSettings();
            emMachineParameters.LongHeadParameters = longHeadParameters;
            emMachineParameters.FeedRollerParameters = rollerParameters;
            emMachineParameters.CrossHeadParameters = crossHeadParameters;
            emMachineParameters.CodeGeneratorSettings = new CodeGeneratorSettings { MinimumLineDistanceToCorrugateEdge = 0.196, MinimumLineLength = 0.078, PerforationSafetyDistance = 1.6 };
            emMachineParameters.WasteAreaParameters = new WasteAreaParameters { Enable = false };
            return emMachineParameters;
        }

        internal static EmPhysicalMachineSettings GetEmMachineParameters(int nrLongheads = 4, double crossHeadPosition = 100)
        {
            var longHeads = new List<EmLongHead>();

            for (int i = 0; i < nrLongheads; i++)
            {
                longHeads.Add(new EmLongHead()
                {
                    ToolToSensorOffset = (i == 0 || i == 1 || i == 3) ? -5 : -17,
                    LeftSideToTool = (i == 0 || i == 1 || i == 3) ? 47 : 7,
                    MaximumPosition = 5080,
                    MinimumPosition = 0,
                    Number = (i + 1),
                    Position = (i + 1) * 100,
                    Width = 54
                });
            }

            var longHeadParameters = new EmLongHeadParameters(longHeads, 3600, 55, 15126.05, 55, 80, 2464, -50, 54, 143, 15d, 24d);
            var rollerParameters = new EmFeedRollerParameters(1500, 100, 4261.36, 100, 600, 400);
            var crossHeadParameters = new EmCrossHeadParameters()
            {
                Position = crossHeadPosition,
                CutCompensation = 7,
                CreaseCompensation = 7,
                MovementData = new EmNormalMovementData()
                {
                    Acceleration = 15126,
                    Speed = 3600,
                    NormalMovement = new EmMovementData()
                    {
                        AccelerationInPercent = 100,
                        SpeedInPercent = 100,
                        Torque = 100
                    }
                }
            };

            var emMachineParameters = new EmPhysicalMachineSettings();
            emMachineParameters.LongHeadParameters = longHeadParameters;
            emMachineParameters.FeedRollerParameters = rollerParameters;
            emMachineParameters.CrossHeadParameters = crossHeadParameters;
            emMachineParameters.CodeGeneratorSettings = new CodeGeneratorSettings() { MinimumLineDistanceToCorrugateEdge = 5d,MinimumLineLength = 2d, PerforationSafetyDistance = 40d };
            emMachineParameters.WasteAreaParameters = new WasteAreaParameters() { Enable = false };
            return emMachineParameters;
        }

        internal static Track GetTrack(int trackNumber)
        {
            var track = new Track { Number = trackNumber};
            return track;
        }

        internal static IPackagingDesign LoadPackagingDesignFromDisk(int designId)
        {
            var testDataPath = Path.Combine(Environment.CurrentDirectory, "EmCodeGenTestData");

            var designManager =
                new PackagingDesignManager(testDataPath, new Mock<ICachingService>().Object, 
                    new Mock<ILogger>().Object);

            designManager.LoadDesigns();
            return designManager.GetDesigns().FirstOrDefault(d => d.Id == designId);
        }

        internal static PhysicalDesign LoadAndTileDesign(int designId, double length, double width, double height, Corrugate corrugate, int numberOfTiles = 1)
        {
            var cartons = new List<ICarton>();

            for (int i = 1; i <= numberOfTiles; i++)
            {
                cartons.Add(new Carton { DesignId = designId, Length = length, Width = width, Height = height });
            }

            var testDataPath = Path.Combine(Environment.CurrentDirectory, "EmCodeGenTestData");

            var designManager =
                new PackagingDesignManager(testDataPath, new Mock<ICachingService>().Object,
                    new Mock<ILogger>().Object);

            designManager.LoadDesigns();

            return OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, false, designManager);
        }

        internal static RuleAppliedPhysicalDesign GetCompensatedDesign(EmPhysicalMachineSettings machineSettings, double minimumLineDistanceToCorrugateEdge, PhysicalDesign design, double length, double width, double height, double zFoldWidth, double zFoldThickness, int trackNumber, bool isTrackRightSideFixed, MicroMeter trackOffset, double perforationSafetyDistance = 40)
        {
            var c = new EmMachineInstructionCreatorTestGuy(new CodeGeneratorSettings()
            {
                MinimumLineDistanceToCorrugateEdge = minimumLineDistanceToCorrugateEdge,
                PerforationSafetyDistance = perforationSafetyDistance,
            });
            return c.GetCompensatedDesign(design, zFoldWidth, zFoldThickness, trackOffset, machineSettings);
        }

        private class EmMachineInstructionCreatorTestGuy : EmMachineInstructionCreator
        {
            public EmMachineInstructionCreatorTestGuy(CodeGeneratorSettings generatorSettings)
                : base(null)
            {
            }

            public RuleAppliedPhysicalDesign GetCompensatedDesign(PhysicalDesign design, double zFoldWidth, double zFoldThickness, MicroMeter trackOffset, EmPhysicalMachineSettings machineSettings)
            {
                var order = new Carton()
                {
                    CartonOnCorrugate = new CartonOnCorrugate()
                    {
                        Corrugate = new Corrugate() { Width = zFoldWidth, Thickness = zFoldThickness }
                    }
                };

                var compensatedDesign = base.CreateCompensatedDesign(design, order,  trackOffset, machineSettings);

                return compensatedDesign;
            }
        }
    }
}
