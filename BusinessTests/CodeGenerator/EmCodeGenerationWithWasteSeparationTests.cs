﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator;
using PackNet.Business.CodeGenerator.AStarStuff;
using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

using Testing.Specificity;

using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace BusinessTests.CodeGenerator
{
    [TestClass]
    public class EmCodeGenerationWithWasteSeparationTests
    {
        //TODO: add when fixing CodeGenWithWasteSeparation
        //[TestMethod]
        //public void ShouldGenerateCodeForDesignWithWasteAreas()
        //{
        //    /*
        //        0, 50   950 1000   1200
        //      0     _____
        //     50  __|     |__          |
        //        |           |         |
        //        |           |         |
        //    550 |__       __|         |
        //    600    |_____|            |*/

        //    var corrugateWidth = new MicroMeter(1200d);
        //    var lines = new List<PhysicalLine>()
        //    {
        //        new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(0, 550), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(950, 0), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(50, 50), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 50), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(950, 50), new PhysicalCoordinate(1000, 50), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(950, 0), new PhysicalCoordinate(950, 50), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(1000, 50), new PhysicalCoordinate(1000, 550), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(0, 550), new PhysicalCoordinate(50, 550), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(50, 550), new PhysicalCoordinate(50, 600), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(950, 550), new PhysicalCoordinate(1000, 550), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(950, 550), new PhysicalCoordinate(950, 600), LineType.cut),
        //        new PhysicalLine(new PhysicalCoordinate(50, 600), new PhysicalCoordinate(950, 600), LineType.cut),
        //    };
        //    var design = new PhysicalDesign(lines);
        //    design.Length = 600;
        //    design.Width = 1000;

        //    var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 1000, MinimumWasteWidth = 0, MinimumWasteLength = 25 };
        //    var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams).ToList();

        //    var ruleAppliedDesign = new RuleAppliedPhysicalDesign(design, wasteAreas);

        //    Specify.That(wasteAreas.Count()).Should.BeEqualTo(5);

        //    var machineSettings = GetEmPhysicalMachineSettings(wasteParams);

        //    var codeGen = new EmCodeGeneratorWithWasteSeparation(100d, machineSettings.LongHeadParameters);

        //    var instructionList = codeGen.Generate(ruleAppliedDesign, machineSettings, corrugateWidth, new Track(), false);

        //    instructionList.ForEach(Console.WriteLine);
            
        //    Specify.That(instructionList).Should.Not.BeNull();
        //    var longHeadActivations = instructionList.OfType<InstructionLongHeadToolActivations>();
        //    Specify.That(longHeadActivations.Count(info => info.ToolActivations.Any(a => a.ToolState == ToolStates.CutWithWasteSeparation))).Should.BeEqualTo(5);
        //}
        
        private EmPhysicalMachineSettings GetEmPhysicalMachineSettings(WasteAreaParameters wasteAreaParameters, int nrLongheads = 6, double crossHeadPosition = 100d)
        {
            var longHeads = new List<EmLongHead>();

            for (int i = 0; i < nrLongheads; i++)
            {
                longHeads.Add(new EmLongHead()
                {
                    ToolToSensorOffset = (i == 0 || i == 1 || i == 3) ? -5 : -17,
                    LeftSideToTool = (i == 0 || i == 1 || i == 3) ? 47 : 7,
                    MaximumPosition = 5080,
                    MinimumPosition = 0,
                    Number = (i + 1),
                    Position = (i + 1) * 100,
                    Width = 54,
                    WasteSeparatorWidth = 24,
                    LeftSideToWasteSeparator = 15
                });
            }

            var longHeadParameters = new EmLongHeadParameters(longHeads, 3600, 55, 15126.05, 55, 80, 2464, -50, 54, 143, 15d, 24d);
            longHeadParameters.WasteSeparatorActivationOffset = 243d;
            longHeadParameters.WasteSeparatorDeactivationOffset = 203d;
            var rollerParameters = new EmFeedRollerParameters(1500, 100, 4261.36, 100, 600, 400);
            var crossHeadParameters = new EmCrossHeadParameters()
            {
                Position = crossHeadPosition,
                CutCompensation = 7,
                CreaseCompensation = 7,
                MovementData = new EmNormalMovementData()
                {
                    Acceleration = 15126,
                    Speed = 3600,
                    NormalMovement = new EmMovementData()
                    {
                        AccelerationInPercent = 100,
                        SpeedInPercent = 100,
                        Torque = 100
                    }
                }
            };

            var emMachineParameters = new EmPhysicalMachineSettings();
            emMachineParameters.LongHeadParameters = longHeadParameters;
            emMachineParameters.FeedRollerParameters = rollerParameters;
            emMachineParameters.CrossHeadParameters = crossHeadParameters;
            emMachineParameters.CodeGeneratorSettings = new CodeGeneratorSettings() { MinimumLineDistanceToCorrugateEdge = 5d,MinimumLineLength = 2d, PerforationSafetyDistance = 40d };
            emMachineParameters.WasteAreaParameters = wasteAreaParameters;
            return emMachineParameters;
        
        }
    }
}
