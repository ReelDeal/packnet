﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CodeGenerator
{
    using System.Linq;

    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.IqFusionJobCreator;
    using PackNet.Common.Interfaces;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.Orders;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;

    using Testing.Specificity;

    using TestUtils;

    [TestClass]
    public class FusionCodeGeneratorTests
    {
        [TestMethod]
        public void ShouldGenerateCode_ForASheetWithAHorizontalCreaseLine_WithTheSameWidth_AsTheCorrugate()
        {
            /* 
             *  0   100   200   300   400
             * 0      __________________
             *       |                  |
             *       |                  |
             * 400   |------------------|                  
             *       |                  |
             * 800   |__________________|
             */

            var design = new PhysicalDesign { Length = 800, Width = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 800), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(400, 0), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 800), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(400, 800), LineType.separate));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 400), new PhysicalCoordinate(400, 400), LineType.crease));

            var mic = new FusionMachineInstructionCreator(new ConsoleLogger());


            var machineSettings = FusionCodeGeneratorTestHelper.GetFusionMachineParameters();
            var track = new Track { TrackNumber = machineSettings.TrackParameters.Tracks.First().Number, TrackOffset = machineSettings.LongHeadParameters.MaximumPosition - design.Width };

            var carton = new Carton();
            var corrugate = new Corrugate { Alias = "MyCorrugate", Thickness = 3d, Width = 400d };
            carton.CartonOnCorrugate = new CartonOnCorrugate(
                carton,
                corrugate,
                "800:400:0",
                new PackagingDesignCalculation { Width = 400, Length = 800 },
                OrientationEnum.Degree0);
            carton.TrackNumber = track.TrackNumber;
            var instructionList = mic.GetInstructionListForJob(carton, design, track, machineSettings);

            Specify.That(instructionList).Should.Not.BeNull("Should not get an exception");
            Specify.That(instructionList.Count(i => i is InstructionCrossHeadMovementItem))
                .Should.BeLogicallyEqualTo(
                    3,
                    "Should get 3 Crosshead movement instructions. One to place the longhead at the start of the crease, one to make the crease, and then one to make the final cut.");
            Specify.That(instructionList.Count(i => i is InstructionCrossHeadToolActivation))
                .Should.BeLogicallyEqualTo(2, "Should get 2 Crosshead tool activations: Cut -> Crease. Since the crease is always active it's not set for the first crease line");
        }

        [TestMethod]
        public void ShouldGenerateCode_ForASheet_WithTheSameWidth_AsTheCorrugate()
        {
            /* 
             *  0   100   200   300   762
             * 0      __________________
             *       |                  |
             *       |                  |
             * 400   |                  |                  
             *       |                  |
             * 800   |__________________|
             */

            var design = new PhysicalDesign { Length = 800, Width = 762 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 800), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(762, 0), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(762, 0), new PhysicalCoordinate(762, 800), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(762, 800), LineType.separate));

            var mic = new FusionMachineInstructionCreator(new ConsoleLogger());

            var corrugate = new Corrugate { Alias = "MyCorrugate", Thickness = 3d, Width = 762d };
            var machineSettings = FusionCodeGeneratorTestHelper.GetFusionMachineParameters();
            var track = new Track { TrackNumber = 2, TrackOffset = 783d };

            var carton = new Carton();
            
            carton.CartonOnCorrugate = new CartonOnCorrugate(
                carton,
                corrugate,
                "800:400:0",
                new PackagingDesignCalculation { Width = 762d, Length = 800 },
                OrientationEnum.Degree0);
            carton.TrackNumber = track.TrackNumber;
            var canProduceParameters = new CanProducePackagingForParameters();
            canProduceParameters.Corrugate = corrugate;
            canProduceParameters.PackagingDesign = design;
            var canProduce = mic.CanProducePackagingFor(canProduceParameters, machineSettings, 2, machineSettings.LongHeadParameters.MaximumPosition);

            Specify.That(canProduce).Should.BeTrue("This is a valid design and should return an instruction list.");
        }

        [TestMethod]
        public void ShouldGenerateCode_ForATiled201_WithTHeSameWidth_AsTheCorrugate()
        {
            /* 
             *       0 100   300   500   600 800
             * 0      _______________________
             * 50    |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |                  
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 200   |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 400   |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 600   |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 800   |__⁞_____⁞__|__⁞_____⁞__|
             */

            var design = GetTiled201(800);

            var mic = new FusionMachineInstructionCreator(new ConsoleLogger());


            var machineSettings = FusionCodeGeneratorTestHelper.GetFusionMachineParameters();
            var corrugate = new Corrugate { Alias = "MyCorrugate", Thickness = 3d, Width = design.Width };

            var canProduceParameters = new CanProducePackagingForParameters { Corrugate = corrugate, PackagingDesign = design };
            var canProduceT1 = mic.CanProducePackagingFor(canProduceParameters, machineSettings, 1, machineSettings.LongHeadParameters.MaximumPosition);
            var canProduceT2 = mic.CanProducePackagingFor(canProduceParameters, machineSettings, 2, machineSettings.LongHeadParameters.MaximumPosition);

            Specify.That(canProduceT1).Should.BeTrue("Should be able to produce on track 1");
            Specify.That(canProduceT2).Should.BeTrue("Should be able to produce on track 2");
        }

        private PhysicalDesign GetTiled201(double totalWidth = 800d)
        {
            /* 
             *       0 100   300   500   600 800
             * 0      _______________________
             * 50    |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |                  
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 200   |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 400   |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 600   |__⁞……………⁞__|__⁞……………⁞__|
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             *       |  ⁞     ⁞  |  ⁞     ⁞  |
             * 800   |__⁞_____⁞__|__⁞_____⁞__|
             */

            var design = new PhysicalDesign { Length = 800, Width = 800 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0),                    new PhysicalCoordinate(0, 800), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0),                    new PhysicalCoordinate(totalWidth, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(totalWidth, 0),           new PhysicalCoordinate(totalWidth, 800), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.5 * totalWidth, 0),     new PhysicalCoordinate(0.5 * totalWidth, 800), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.125 * totalWidth, 0),   new PhysicalCoordinate(0.125 * totalWidth, 800), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.375 * totalWidth, 0),   new PhysicalCoordinate(0.375 * totalWidth, 800), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.625 * totalWidth, 0),   new PhysicalCoordinate(0.625 * totalWidth, 800), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.875 * totalWidth, 0),   new PhysicalCoordinate(0.875 * totalWidth, 800), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 800),                  new PhysicalCoordinate(totalWidth, 800), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50),                   new PhysicalCoordinate(0.125 * totalWidth, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.125 * totalWidth, 50),  new PhysicalCoordinate(0.375 * totalWidth, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.375 * totalWidth, 50),  new PhysicalCoordinate(0.5 * totalWidth, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.5 * totalWidth, 50),    new PhysicalCoordinate(0.625 * totalWidth, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.625 * totalWidth, 50),  new PhysicalCoordinate(0.875 * totalWidth, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(700, 50),                 new PhysicalCoordinate(totalWidth, 50), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 200),                  new PhysicalCoordinate(0.125 * totalWidth, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.125 * totalWidth, 200), new PhysicalCoordinate(0.375 * totalWidth, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.375 * totalWidth, 200), new PhysicalCoordinate(0.5 * totalWidth, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.5 * totalWidth, 200),   new PhysicalCoordinate(0.625 * totalWidth, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.625 * totalWidth, 200), new PhysicalCoordinate(0.875 * totalWidth, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.875 * totalWidth, 200), new PhysicalCoordinate(totalWidth, 200), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 400),                  new PhysicalCoordinate(0.125 * totalWidth, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.125 * totalWidth, 400), new PhysicalCoordinate(0.375 * totalWidth, 400), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.375 * totalWidth, 400), new PhysicalCoordinate(0.5 * totalWidth, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.5 * totalWidth, 400),   new PhysicalCoordinate(0.625 * totalWidth, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.625 * totalWidth, 400), new PhysicalCoordinate(0.875 * totalWidth, 400), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.875 * totalWidth, 400), new PhysicalCoordinate(totalWidth, 400), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 600),                  new PhysicalCoordinate(0.125 * totalWidth, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.125 * totalWidth, 600), new PhysicalCoordinate(0.375 * totalWidth, 600), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.375 * totalWidth, 600), new PhysicalCoordinate(0.5 * totalWidth, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.5 * totalWidth, 600),   new PhysicalCoordinate(0.625 * totalWidth, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.625 * totalWidth, 600), new PhysicalCoordinate(0.875 * totalWidth, 600), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0.875 * totalWidth, 600), new PhysicalCoordinate(totalWidth, 600), LineType.cut));
            return design;
        }
    }
}
