﻿Feature: On The Fly Compensation
	In order to get a more accurate box
	As a user
	I want the cuts and creases to be compensated for velocity

Scenario: In acceleration between tool change
	Given an instruction list with cross head movements and tool activations 
	And velocity parameters
	And length of movment to tool change for first otf instruction is shorter than the time to accelerate to full speed
	And time to second tool change for first otf instruction is less than delay time units into full speed movement
	When I compensate for velocity
	Then the instruction list should be compensated for full acceleration
