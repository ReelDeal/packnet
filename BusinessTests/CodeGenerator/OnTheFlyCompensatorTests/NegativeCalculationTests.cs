namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

    [TestClass]
    public class NegativeCalculationTests
    {
        [TestMethod]
        public void DistanceTravelledDuringAccelerationIsFiveWhenAcceleratingFromZeroToTenWithTenAsAcceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0);
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(0, 10, 10);
            Assert.AreEqual(5, res);
        }

        [TestMethod]
        public void DistanceTravelledDuringAccelerationIsFifteenWhenAcceleratingFromTenToTwentyWithTenAsAcceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0);
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(10, 20, 10);
            Assert.AreEqual(15, res);
        }

        [TestMethod]
        public void DistanceTravelledDuringRetardationIsFiveWhenBrakingFromTenToZeroWithTenAsDeceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0); 
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(10, 0, 10);
            Assert.AreEqual(5, res);
        }

        [TestMethod]
        public void DistanceTravelledDuringRetardationIsFifteenWhenBrakingFromTwentyToTenWithTenAsDeceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0);
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(20, 10, 10);
            Assert.AreEqual(15, res);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnEndOfAccelerationAtPosition105WhenStartingAt120AndStoppingAt100()
        {
            var sut = new PositionCompensator(10, 10, 10, 120, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(115, res.AccelerationStopPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnStartOfDecelerationAtPosition105WhenStartingAt120AndStoppingAt100()
        {
            var sut = new PositionCompensator(10, 10, 10, 120, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(105, res.DecelerationStartPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnTopSpeedReachedAs10WhenStartingAt120AndStoppingAt100()
        {
            var sut = new PositionCompensator(10, 10, 10, 120, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(10, res.TopSpeedReached);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnEndOfAccelerationAtPosition102Point5WhenStartingAt105AndStoppingAt100()
        {
            var sut = new PositionCompensator(10, 10, 10, 105, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(102.5, res.AccelerationStopPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnStartOfDecelerationAtPosition102Point5WhenStartingAt105AndStoppingAt100()
        {
            var sut = new PositionCompensator(10, 10, 10, 105, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(102.5, res.DecelerationStartPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnTopSpeedReachedAs7Point07SomethingWhenStartingAt105AndStoppingAt100()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 105, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(7.0710678118654752440084436210485, res.TopSpeedReached);
        }

        [TestMethod]
        public void TenAsMaxSpeed100AsMaxAccTenAsMaxDecShouldReturnEndOfAccelerationAtPosition100Point454545454545454WhenStartingAt105AndStoppingAt100WhenDecelerationIsFourTimesOfAcceleration()
        {
            var sut = new PositionCompensator(10, 10, 100, 105, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(100.454545454545454, res.AccelerationStopPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeed100AsMaxAccTenAsMaxDecShouldReturnStartOfDecelerationAtPosition100Point454545454545454WhenStartingAt105AndStoppingAt100IsFourTimesOfAcceleration()
        {
            var sut = new PositionCompensator(10, 10, 100, 105, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(100.454545454545454, res.DecelerationStartPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeed100AsMaxAccTenAsMaxDecShouldReturnTopSpeedReachedAsTopSpeedReachedAs7Point07SomethingWhenStartingAt105AndStoppingAt100()
        {
            var sut = new PositionCompensator(10, 10, 10, 105, 100, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(7.0710678118654752440084436210485, res.TopSpeedReached);
        }
    }
}