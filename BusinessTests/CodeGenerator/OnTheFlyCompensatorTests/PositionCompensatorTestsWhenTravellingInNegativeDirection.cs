namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
    using TestUtils;

    [TestClass]
    public class PositionCompensatorTestsWhenTravellingInNegativeDirection
    {
        private PositionCompensator sut;

        public PositionCompensator GetCompensatorForDelays(int activationDelayInMilliseconds, int deactivationDelayInMilliseconds, int creaseActivationDelayInMs, int creaseDeactivationDelayInMs)
        {
            var compensator = new PositionCompensator(10, 10, 10, 120, 100, new OtfDelayHandler(activationDelayInMilliseconds, deactivationDelayInMilliseconds, creaseActivationDelayInMs, creaseDeactivationDelayInMs));
            var res = compensator.GetMovementCompensationInformation();
            Assert.AreEqual(115, res.AccelerationStopPosition);
            Assert.AreEqual(105, res.DecelerationStartPosition);
            return compensator;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayWhenCutShouldBePerformedWhenRunningAtFullSpeed()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(112, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithDeactivationDelayWhenCreaseShouldBePerformedAfterCutWhenRunningAtFullSpeed()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Crease);
            var res = sut.CompensatePosition(position, ToolStates.Cut);
            Assert.AreEqual(111, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotCompensateWhenRaiseShouldBePerformedWhenRunningAtFullSpeed()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.None);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(110, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationWhenCutShouldBePerformedWhenInAccelerationAndPositionIsAccelerationEndPosition()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(115, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(116.8, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationWhenCutShouldBePerformedWhenInAccelerationForEntireDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(117, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(118.349, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationWhenCutShouldBePerformedWhenInAccelerationPartOfDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(114, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(115.95, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayButNotAdjustForDecelerationWhenCutShouldBePerformedWhenInDecelerationAndPositionIsDecelerationStartPosition()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(105, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(107, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForDecelerationWhenCutShouldBePerformedWhenInDecelerationForEntireDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(103, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(104.749, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForDecelerationWhenCutShouldBePerformedWhenInDecelerationPartOfDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(104, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(105.944, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCreaseActivationDelayAndAdjustForAccelerationAndDecelerationWhenCutShouldBePerformedWhenInAccelerationFullSpeedAndDecelerationDuringDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 2460, 100);
            var position = new CrossHeadOnTheFlyPosition(100, ToolStates.Cut);

            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(118.542, (double)res.GetCompensatedPosition());
        }
    }
}