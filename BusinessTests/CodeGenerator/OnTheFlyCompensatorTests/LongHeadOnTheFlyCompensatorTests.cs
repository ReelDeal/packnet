﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{
    [TestClass]
    public class LongHeadOnTheFlyCompensatorTests
    {
        LongheadOnTheFlyCompensator compensator;

        [TestInitialize]
        public void Setup()
        {
            compensator = new LongheadOnTheFlyCompensator();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateLongHeadInstruction()
        {
            //Look at positions to understand order of instructions, this is intended to verify insertion sort when adding positions to an otf longhead item
            var otfInstruction = new InstructionLongHeadOnTheFlyItem() { FinalPosition = 1000 };
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(200, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None),
                new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
            }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(600, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Perforation),
                new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Cut),
            }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(400, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(1, ToolStates.Perforation, ToolStates.Crease),
                new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.Crease),
            }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(800, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease),
                new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.None),
            }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(1000, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut),
                new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Cut),
            }));


            var compensatedInstructionList = compensator.CompensateForVelocity(new List<InstructionItem> { otfInstruction }, 0,
                1000, 1000, 1000, new OtfDelayHandler(50, 51, 52, 53));

            Specify.That(compensatedInstructionList.Count()).Should.BeEqualTo(1);
            var compensatedOtfItem = compensatedInstructionList.First() as InstructionLongHeadOnTheFlyItem;

            Specify.That(compensatedOtfItem).Should.Not.BeNull();
            Specify.That(compensatedOtfItem.Positions.Count()).Should.BeEqualTo(6, "The number of activations was not correct in regards to how they are split when it comes to the delays of different tools.");

            for (int i = 1; i < compensatedOtfItem.Positions.Count(); i++)
            {
                var prev = compensatedOtfItem.Positions.ElementAt(i - 1);
                var curr = compensatedOtfItem.Positions.ElementAt(i);

                Specify.That(prev.Position < curr.Position).Should.BeTrue("Activation positions are not sorted in ascending order.");
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSplitOtfPositionIntoTwoParts_WhenGoingFromRaisedToCutAndCutToRaised()
        {
            var otfInstruction = new InstructionLongHeadOnTheFlyItem { FinalPosition = 2000 };
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(600, 100,
                new[]
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.None)
                }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(1000, 100,
                new[]
                {
                    new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Cut),
                    new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Cut)
                }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(1200, 100,
                new[]
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None)
                }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(1400, 100,
                new[]
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.None)
                }));
            

            var compensated = compensator.CompensateForVelocity(new List<InstructionItem>() { otfInstruction }, 0, 1000, 1000, 1000,
                new OtfDelayHandler(5 /*Cut on*/, 10/*Cut off*/, 50/*Crease on*/, 100/*Crease off*/));

            Specify.That(compensated.Count()).Should.BeEqualTo(1);
            var compensatedOtfInstruction = compensated.First() as InstructionLongHeadOnTheFlyItem;

            Specify.That(compensatedOtfInstruction).Should.Not.BeNull();
            Specify.That(compensatedOtfInstruction.FinalPosition).Should.BeLogicallyEqualTo(2000);
            Specify.That(compensatedOtfInstruction.Positions.Count())
                .Should.BeEqualTo(7,
                    "Expected 7 positions since we have two none->cut that should have been converted to none->crease->cut and we have one cut->none that should have been converted to cut->crease->none");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).Position)
                .Should.BeLogicallyEqualTo(550, "First part of the split none->cut should have been compensated with crease activation delay");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).ToolStates.Count()).Should.BeEqualTo(2, "Both LH 1 and 2 should be included");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).ToolStates.All(t => t.ToolState == ToolStates.Crease)).Should.BeTrue();

            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).Position)
                .Should.BeLogicallyEqualTo(595,
                    "Second part of the split none->cut should have been compensated with cut activation delay");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).ToolStates.Count()).Should.BeEqualTo(2, "Both LH 1 and 2 should be included");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).ToolStates.All(t => t.ToolState == ToolStates.Cut)).Should.BeTrue();

            Specify.That(compensatedOtfInstruction.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(990, "First part of cut->none should have been compensated with cut deactivation delay");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(2).ToolStates.Count()).Should.BeEqualTo(2, "Both LH 1 and 2 should be included");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(2).ToolStates.All(t => t.ToolState == ToolStates.Crease)).Should.BeTrue();

            Specify.That(compensatedOtfInstruction.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(1000, "Second part of cut->none should not be compensated");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(3).ToolStates.Count()).Should.BeEqualTo(2, "Both LH 1 and 2 should be included");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(3).ToolStates.All(t => t.ToolState == ToolStates.None)).Should.BeTrue();

            Specify.That(compensatedOtfInstruction.Positions.ElementAt(4).Position).Should.BeLogicallyEqualTo(1150, "None->crease should have been compensated with crease activation delay");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(4).ToolStates.Count()).Should.BeEqualTo(1);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(4).ToolStates.ElementAt(0).LongHeadNumber).Should.BeEqualTo(1);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(4).ToolStates.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);

            Specify.That(compensatedOtfInstruction.Positions.ElementAt(5).Position)
                .Should.BeLogicallyEqualTo(1350,
                    "First part of none->cut for LH2 should have been compensated with crease activation delay");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(5).ToolStates.Count()).Should.BeEqualTo(1);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(5).ToolStates.ElementAt(0).LongHeadNumber).Should.BeEqualTo(2);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(5).ToolStates.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);

            Specify.That(compensatedOtfInstruction.Positions.ElementAt(6).Position)
                .Should.BeLogicallyEqualTo(1395,
                    "LH1 cut->crease and second part of none->cut for LH2 should be compensated with cut activation delay");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(6).ToolStates.Count()).Should.BeEqualTo(2, "Both LH 1 and 2 should be included");
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(6).ToolStates.All(t => t.ToolState == ToolStates.Cut)).Should.BeTrue();
        }
    }
}
