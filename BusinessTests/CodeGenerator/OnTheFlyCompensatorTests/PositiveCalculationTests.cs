﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{

    using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

    [TestClass]
    public class PositiveCalculationTests
    {
        [TestMethod]
        public void DistanceTravelledDuringAccelerationIsFiveWhenAcceleratingFromZeroToTenWithTenAsAcceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0);
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(0, 10, 10);
            Assert.AreEqual(5, res);
        }

        [TestMethod]
        public void DistanceTravelledDuringAccelerationIsFifteenWhenAcceleratingFromTenToTwentyWithTenAsAcceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0);
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(10, 20, 10);
            Assert.AreEqual(15, res);
        }

        [TestMethod]
        public void DistanceTravelledDuringRetardationIsFiveWhenBrakingFromTenToZeroWithTenAsDeceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0);
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(10, 0, 10);
            Assert.AreEqual(5, res);
        }

        [TestMethod]
        public void DistanceTravelledDuringRetardationIsFifteenWhenBrakingFromTwentyToTenWithTenAsDeceleration()
        {
            var sut = new MovementCompensationInformation(0, 0, 0, 0, 0);
            var res = sut.CalculateDistanceTravelledWhileChangingSpeed(20, 10, 10);
            Assert.AreEqual(15, res);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnEndOfAccelerationAtPosition105WhenStartingAt100AndStoppingAt120()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 120, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(105, res.AccelerationStopPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnStartOfDecelerationAtPosition115WhenStartingAt100AndStoppingAt120()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 120, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(115, res.DecelerationStartPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnTopSpeedReachedAs10WhenStartingAt100AndStoppingAt120()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 120, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(10, res.TopSpeedReached);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnEndOfAccelerationAtPosition102Point5WhenStartingAt100AndStoppingAt105()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 105, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(102.5, res.AccelerationStopPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnStartOfDecelerationAtPosition102Point5WhenStartingAt100AndStoppingAt105()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 105, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(102.5, res.DecelerationStartPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeedTenAsMaxAccTenAsMaxDecShouldReturnTopSpeedReachedAs7Point07SomethingWhenStartingAt100AndStoppingAt105()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 105, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(7.0710678118654752440084436210485, res.TopSpeedReached);
        }

        [TestMethod]
        public void TenAsMaxSpeed100AsMaxAccTenAsMaxDecShouldReturnEndOfAccelerationAtPosition104Point545454545454545WhenStartingAt100AndStoppingAt105WhenDecelerationIsFourTimesOfAcceleration()
        {
            var sut = new PositionCompensator(10, 10, 100, 100, 105, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(104.545454545454545, res.AccelerationStopPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeed100AsMaxAccTenAsMaxDecShouldReturnStartOfDecelerationAtPosition104Point545454545454545WhenStartingAt100AndStoppingAt105IsFourTimesOfAcceleration()
        {
            var sut = new PositionCompensator(10, 10, 100, 100, 105, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(104.545454545454545, res.DecelerationStartPosition);
        }

        [TestMethod]
        public void TenAsMaxSpeed100AsMaxAccTenAsMaxDecShouldReturnTopSpeedReachedAsTopSpeedReachedAs7Point07SomethingWhenStartingAt100AndStoppingAt105()
        {
            var sut = new PositionCompensator(10, 10, 10, 100, 105, new OtfDelayHandler(0, 0, 0, 0));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(7.0710678118654752440084436210485, res.TopSpeedReached);
        }
    }
}