﻿namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

    [TestClass]
    public class PositionCompensatorTestsWhenMaxSpeedIsNeverReached
    {
        private PositionCompensator sut;

        [TestInitialize]
        public void SetUp()
        {
            sut = new PositionCompensator(10, 10, 10, 100, 108, new OtfDelayHandler(200, 100, 200, 100));
            var res = sut.GetMovementCompensationInformation();
            Assert.AreEqual(104, res.AccelerationStopPosition);
            Assert.AreEqual(104, res.DecelerationStartPosition);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationWhenCutShouldBePerformedWhenInAccelerationForEntireDelay()
        {
            var position = new CrossHeadOnTheFlyPosition(100.8, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(100.2, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForDecelerationWhenCutShouldBePerformedWhenInDecelerationForEntireDelay()
        {
            var position = new CrossHeadOnTheFlyPosition(107, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(105.906, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationAndDecelerationWhenCutShouldBePerformedWhenInBothAccelerationAndDecelerationDuringDelay()
        {
            var position = new CrossHeadOnTheFlyPosition(105.57, ToolStates.Cut);

            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(103.976, (double)res.GetCompensatedPosition());

            position = new CrossHeadOnTheFlyPosition(104.5, ToolStates.Cut);

            res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(102.829, (double)res.GetCompensatedPosition());
        }
    }
}