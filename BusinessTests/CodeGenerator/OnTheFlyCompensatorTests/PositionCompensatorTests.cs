﻿using System.Security.Policy;

using TestUtils;

namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

    [TestClass]
    public class PositionCompensatorTests
    {
        private PositionCompensator sut;

        public PositionCompensator GetCompensatorForDelays(int activationDelayInMilliseconds, int deactivationDelayInMilliseconds, int creaseActivationDelayInMilliseconds, int creaseDeactivationDelayInMilliseconds)
        {
            var compensator = new PositionCompensator(10, 10, 10, 100, 120, new OtfDelayHandler(activationDelayInMilliseconds, deactivationDelayInMilliseconds, creaseActivationDelayInMilliseconds, creaseDeactivationDelayInMilliseconds));
            var res = compensator.GetMovementCompensationInformation();
            Assert.AreEqual(105, res.AccelerationStopPosition);
            Assert.AreEqual(115, res.DecelerationStartPosition);
            return compensator;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayWhenCutShouldBePerformedWhenRunningAtFullSpeed()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(108, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCreaseActivationDelayWhenCreaseShouldBePerformedWhenRunningAtFullSpeed()
        {
            sut = GetCompensatorForDelays(200, 100, 300, 100);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Crease);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(107, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotCompensateWhenTheSameToolActuatesTwiceInARow()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.None);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(110, (int)res.GetCompensatedPosition());

            sut = GetCompensatorForDelays(200, 100, 200, 100);
            position = new CrossHeadOnTheFlyPosition(110, ToolStates.Crease);
            res = sut.CompensatePosition(position, ToolStates.Crease);
            Assert.AreEqual(110, (int)res.GetCompensatedPosition());

            sut = GetCompensatorForDelays(200, 100, 200, 100);
            position = new CrossHeadOnTheFlyPosition(110, ToolStates.Cut);
            res = sut.CompensatePosition(position, ToolStates.Cut);
            Assert.AreEqual(110, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationWhenCutShouldBePerformedWhenInAccelerationAndPositionIsAccelerationEndPosition()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(105, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(103.2, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationWhenCutShouldBePerformedWhenInAccelerationForEntireDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(103, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(101.651, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationWhenCutShouldBePerformedWhenInAccelerationPartOfDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(106, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(104.05, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayButNotAdjustForDecelerationWhenCutShouldBePerformedWhenInDecelerationAndPositionIsDecelerationStartPosition()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(115, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(113, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForDecelerationWhenCutShouldBePerformedWhenInDecelerationForEntireDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(117, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(115.251, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForDecelerationWhenCutShouldBePerformedWhenInDecelerationPartOfDelay()
        {
            sut = GetCompensatorForDelays(200, 100, 200, 100);
            var position = new CrossHeadOnTheFlyPosition(116, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(114.056, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithActivationDelayAndAdjustForAccelerationAndDecelerationWhenCutShouldBePerformedWhenInAccelerationFullSpeedAndDecelerationDuringDelay()
        {
            sut = GetCompensatorForDelays(2460, 100, 2460, 100);
            var position = new CrossHeadOnTheFlyPosition(120, ToolStates.Cut);

            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(101.458, (double)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCutDeactivationDelayWhenGoingFromCutToCrease()
        {
            sut = GetCompensatorForDelays(0, 100, 0, 0);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Crease);
            var res = sut.CompensatePosition(position, ToolStates.Cut);
            Assert.AreEqual(109, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCutDeactivationDelayWhenGoingFromCutToRaise()
        {
            //Using cut deactivation delay even if the crease is also lifted. This is because the crease 
            //deactivation delay i much longer which gives the result in knife being lifted with crease wheel. 
            //In other words cut will stop before it should. This means that we will over crease in this scenario
            //but over-creasing is acceptable compared to under-cutting.
            sut = GetCompensatorForDelays(0, 100, 0, 1000);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.None);
            var res = sut.CompensatePosition(position, ToolStates.Cut);
            Assert.AreEqual(109, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCreaseDeactivationDelayWhenGoingFromCreaseToRaise()
        {
            sut = GetCompensatorForDelays(0, 0, 0, 100);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.None);
            var res = sut.CompensatePosition(position, ToolStates.Crease);
            Assert.AreEqual(109, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCutActivationDelayWhenGoingFromCreaseToCut()
        {
            sut = GetCompensatorForDelays(100, 0, 0, 0);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.Crease);
            Assert.AreEqual(109, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCreaseActivationDelayWhenGoingFromRaiseToCrease()
        {
            sut = GetCompensatorForDelays(0, 0, 100, 0);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Crease);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(109, (int)res.GetCompensatedPosition());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateWithCreaseActivationDelayWhenGoingFromRaiseToCut()
        {
            //Using crease delay since the cut delay is only the time needed to push the knife through the 
            //fanfold when the crease wheel is down, when tools are raised, the knife will be down way before 
            //the crease wheel. Thats why the crease activation delay is used

            sut = GetCompensatorForDelays(0, 0, 100, 0);
            var position = new CrossHeadOnTheFlyPosition(110, ToolStates.Cut);
            var res = sut.CompensatePosition(position, ToolStates.None);
            Assert.AreEqual(109, (int)res.GetCompensatedPosition());
        }
    }
}