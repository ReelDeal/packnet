﻿

namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.OnTheFlyCompensator;

    [TestClass]
    public class CompensationForPositionsDuringEntireMovement
    {
        private static PositionCompensator sut;

        [TestMethod]
        public void ShouldPrintValuesWhenMaxSpeedIsReached()
        {
            const int StartPosition = 100;
            const int EndPosition = 120;
            const int ActivationDelayInMilliseconds = 200;
            var res = CreateCompensator(10, 10, 10, StartPosition, EndPosition, ActivationDelayInMilliseconds, 100, ActivationDelayInMilliseconds, 100);
            Assert.AreEqual(105, res.AccelerationStopPosition);
            Assert.AreEqual(115, res.DecelerationStartPosition);
            const ToolStates ToolState = ToolStates.Cut;
            const double Increment = 0.01;
            CheckValues(ToolState, StartPosition, EndPosition, Increment, ActivationDelayInMilliseconds);
        }

        [TestMethod]
        public void ShouldPrintValuesWhenMaxSpeedIsNotReached()
        {
            const int StartPosition = 100;
            const int EndPosition = 108;
            const int ActivationDelayInMilliseconds = 200;
            var res = CreateCompensator(10, 10, 10, StartPosition, EndPosition, ActivationDelayInMilliseconds, 100, 0, 0);
            Assert.AreEqual(104, res.AccelerationStopPosition);
            Assert.AreEqual(104, res.DecelerationStartPosition);
            const ToolStates ToolStates = ToolStates.Cut;
            const double Increment = 0.01;
            CheckValues(ToolStates, StartPosition, EndPosition, Increment, ActivationDelayInMilliseconds);
        }

        [TestMethod]
        public void ShouldPrintValuesWhenMaxSpeedIsNotReachedAndDecelerationDiffersFromAcceleration()
        {
            const int StartPosition = 100;
            const double EndPosition = 104.4;
            const int ActivationDelayInMilliseconds = 200;
            var res = CreateCompensator(10, 10, 100, StartPosition, EndPosition, ActivationDelayInMilliseconds, 100, 0, 0);
            Assert.AreEqual(104, res.AccelerationStopPosition);
            Assert.AreEqual(104, res.DecelerationStartPosition);
            const ToolStates ToolStates = ToolStates.Cut;
            const double Increment = 0.01;
            CheckValues(ToolStates, StartPosition, EndPosition, Increment, ActivationDelayInMilliseconds);
        }

        [TestMethod]
        public void ShouldPrintValuesWhenMaxSpeedIsReachedMachineLikeValues()
        {
            const int StartPosition = 100;
            const int EndPosition = 1324;
            const int ActivationDelayInMilliseconds = 200;
            var res = CreateCompensator(1800, 10000, 10000, StartPosition, EndPosition, ActivationDelayInMilliseconds, 100, 0, 0);
            Assert.AreEqual(262, res.AccelerationStopPosition);
            Assert.AreEqual(1162, res.DecelerationStartPosition);
            const ToolStates ToolStates = ToolStates.Cut;
            const int Increment = 1;
            CheckValues(ToolStates, StartPosition, EndPosition, Increment, ActivationDelayInMilliseconds);
        }

        [TestMethod]
        public void ShouldPrintValuesWhenMaxSpeedIsNotReachedMachineLikeValues()
        {
            const int StartPosition = 100;
            const int EndPosition = 420;
            const int ActivationDelayInMilliseconds = 200;
            var res = CreateCompensator(1800, 10000, 10000, StartPosition, EndPosition, ActivationDelayInMilliseconds, 100, 0, 0);
            Assert.AreEqual(260, res.AccelerationStopPosition);
            Assert.AreEqual(260, res.DecelerationStartPosition);
            const ToolStates ToolStates = ToolStates.Cut;
            const int Increment = 1;
            CheckValues(ToolStates, StartPosition, EndPosition, Increment, ActivationDelayInMilliseconds);
        }

        [TestMethod]
        public void ShouldPrintValuesWhenMaxSpeedIsNotReachedAndDecelerationDiffersFromAccelerationMachineLikeValues()
        {
            const double StartPosition = 100;
            const double EndPosition = 430;
            const int ActivationDelayInMilliseconds = 200;
            var res = CreateCompensator(1800, 1000, 10000, StartPosition, EndPosition, ActivationDelayInMilliseconds, 100,0,0);
            Assert.AreEqual(400, res.AccelerationStopPosition);
            Assert.AreEqual(400, res.DecelerationStartPosition);
            const ToolStates ToolStates = ToolStates.Cut;
            const double Increment = 0.1;
            CheckValues(ToolStates, StartPosition, EndPosition, Increment, ActivationDelayInMilliseconds);
        }

        private static MovementCompensationInformation CreateCompensator(int maxSpeed, int acceleration, int unsignedDeceleration, double startPosition, double endPosition, int activationDelayInMilliseconds, int deactivationDelayInMilliseconds, int creaseActivationDelayMs, int creaseDeactivationDelayMs)
        {
            sut = new PositionCompensator(maxSpeed, acceleration, unsignedDeceleration, startPosition, endPosition, new OtfDelayHandler(activationDelayInMilliseconds, deactivationDelayInMilliseconds, creaseActivationDelayMs, creaseDeactivationDelayMs));
            return sut.GetMovementCompensationInformation();
        }

        private static void CheckValues(ToolStates toolStates, double startPosition, double endPosition, double increment, double delayInMilliseconds)
        {
            double numberOfSamples = (endPosition  - startPosition) / increment;
            for (var i = 0; i <= numberOfSamples; i++)
            {
                var distanceIn = startPosition + (i * increment);
                var pos = new CrossHeadOnTheFlyPosition(distanceIn, toolStates);
                var compensatedPosition = sut.CompensatePosition(pos, ToolStates.None);
                var calculatedDataForCompensatedPosition =
                    sut.CompensatePosition(
                        new CrossHeadOnTheFlyPosition(compensatedPosition.GetCompensatedPosition(), pos.ToolState), ToolStates.None);
                if (compensatedPosition.GetTimeToPosition() > delayInMilliseconds / 1000
                    && ((double)compensatedPosition.GetCompensatedPosition() > distanceIn))
                {
                    Assert.AreEqual(
                        compensatedPosition.GetTimeToPosition() - (delayInMilliseconds / 1000),
                        calculatedDataForCompensatedPosition.GetTimeToPosition(),
                        0.001);
                }

                // Comment back for output of values to excel friendly format
                // PrintValues(compensatedPositionData, pos, compensatedPosition);
            }
        }

        private static void PrintValues(
            DistanceCompensation compensatedPositionData,
            CrossHeadOnTheFlyPosition pos,
            DistanceCompensation compensatedPosition)
        {
            Console.WriteLine(compensatedPosition.GetTimeToPosition() + ";" + pos.Position + ";" +
                                     compensatedPosition.GetTimeToCompensatedPosition() + ";" +
                                     compensatedPositionData.GetTimeToPosition() + ";" +
                                     compensatedPosition.GetCompensatedPosition() + ";" +
                                     PrintCompensationInPhase(compensatedPosition.AccelerationCompensation) + ";" +
                                     PrintCompensationInPhase(compensatedPosition.FullSpeedCompensation) + ";" +
                                     PrintCompensationInPhase(compensatedPosition.DecelerationCompensation));
        }

        private static string PrintCompensationInPhase(DistanceCompensationInPhase compensationInPhase)
        {
            return compensationInPhase.TimeInPhase + ";" + compensationInPhase.TimeToCompensatedPositionInPhase + ";" + compensationInPhase.CompensationInPhase;
        }
    }
}