﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
using PackNet.Common.Interfaces.Machines;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator.OnTheFlyCompensatorTests
{
    [TestClass]
    public class CrossheadOnTheFlyCompensatorTests
    {
        CrossHeadOnTheFlyCompensator otfCompensator;

        [TestInitialize]
        public void Setup()
        {
            otfCompensator = new CrossHeadOnTheFlyCompensator();
        }

        [TestMethod]
        public void ShouldCompensateForVelocityWhenPerforationsAreWithinTheOtfInstructions()
        {
            var otfInstruction = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 1000 };
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(200, ToolStates.Crease));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(300, ToolStates.Perforation));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(600, ToolStates.Crease));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(800, ToolStates.Cut));

            var compensated = otfCompensator.CompensateForVelocity(new List<InstructionItem>(){otfInstruction}, 0, 1000, 1000, 1000,
                new OtfDelayHandler(50, 50, 50, 50));

            Specify.That(compensated.Count()).Should.BeEqualTo(1);
            var compensatedOtfInstruction = compensated.First() as InstructionCrossHeadOnTheFlyItem;

            Specify.That(compensatedOtfInstruction).Should.Not.BeNull();
            Specify.That(compensatedOtfInstruction.FinalPosition).Should.BeLogicallyEqualTo(1000);
            Specify.That(compensatedOtfInstruction.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).ToolState).Should.BeEqualTo(ToolStates.Perforation);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).Position < 300).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldUseLastToolStateWhenCompensatingFirstOtfPosition()
        {
            var chActivation = new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.Cut };
            var otfInstruction = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 1000 };
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(200, ToolStates.Crease));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(800, ToolStates.Cut));

            var compensated = otfCompensator.CompensateForVelocity(new List<InstructionItem>() { chActivation, otfInstruction }, 0, 1000, 1000, 1000,
                new OtfDelayHandler(5, 5, 50, 50));

            Specify.That(compensated.Count()).Should.BeEqualTo(2);
            var compensatedOtfInstruction = compensated.Last() as InstructionCrossHeadOnTheFlyItem;

            Specify.That(compensatedOtfInstruction).Should.Not.BeNull();
            Specify.That(compensatedOtfInstruction.FinalPosition).Should.BeLogicallyEqualTo(1000);
            Specify.That(compensatedOtfInstruction.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(196.85);

            //Run same test but with crease activated when starting OTF
            chActivation.Toolstate = ToolStates.Crease;
            compensated = otfCompensator.CompensateForVelocity(new List<InstructionItem>() { chActivation, otfInstruction }, 0, 1000, 1000, 1000,
                new OtfDelayHandler(5, 5, 50, 50));

            Specify.That(compensated.Count()).Should.BeEqualTo(2);
            compensatedOtfInstruction = compensated.Last() as InstructionCrossHeadOnTheFlyItem;

            Specify.That(compensatedOtfInstruction).Should.Not.BeNull();
            Specify.That(compensatedOtfInstruction.FinalPosition).Should.BeLogicallyEqualTo(1000);
            Specify.That(compensatedOtfInstruction.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(200);
        }

        [TestMethod]
        public void ShouldSplitOtfPositionIntoTwoParts_WhenGoingFromRaisedToCut()
        {
            var otfInstruction = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 2000 };
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(600, ToolStates.Cut));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(1000, ToolStates.Crease));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(1400, ToolStates.Cut));

            var compensated = otfCompensator.CompensateForVelocity(new List<InstructionItem>() { otfInstruction }, 0, 1000, 1000, 1000,
                new OtfDelayHandler(5, 10, 50, 100));

            Specify.That(compensated.Count()).Should.BeEqualTo(1);
            var compensatedOtfInstruction = compensated.First() as InstructionCrossHeadOnTheFlyItem;

            Specify.That(compensatedOtfInstruction).Should.Not.BeNull();
            Specify.That(compensatedOtfInstruction.FinalPosition).Should.BeLogicallyEqualTo(2000);
            Specify.That(compensatedOtfInstruction.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(550);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).ToolState).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(595);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(2).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(990);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(3).ToolState).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(1395);
        }

        [TestMethod]
        public void ShouldSplitOtfPositionIntoTwoParts_WhenGoingFromCutToRaised()
        {
            var otfInstruction = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 2000 };
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(600, ToolStates.Crease));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(1000, ToolStates.Cut));
            otfInstruction.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(1400, ToolStates.None));

            var compensated = otfCompensator.CompensateForVelocity(new List<InstructionItem>() { otfInstruction }, 0, 1000, 1000, 1000,
                new OtfDelayHandler(5, 10, 50, 100));

            Specify.That(compensated.Count()).Should.BeEqualTo(1);
            var compensatedOtfInstruction = compensated.First() as InstructionCrossHeadOnTheFlyItem;

            Specify.That(compensatedOtfInstruction).Should.Not.BeNull();
            Specify.That(compensatedOtfInstruction.FinalPosition).Should.BeLogicallyEqualTo(2000);
            Specify.That(compensatedOtfInstruction.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(550);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(995);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(1).ToolState).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(1390);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(2).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(1400);
            Specify.That(compensatedOtfInstruction.Positions.ElementAt(3).ToolState).Should.BeEqualTo(ToolStates.None);
        }
    }
}
