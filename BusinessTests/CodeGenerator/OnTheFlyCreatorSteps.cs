﻿using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace BusinessTests.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    [Scope(Tag = "OnTheFlyCreator")]
    public class OnTheFlyCreatorSteps
    {
        private List<InstructionItem> originalInstructionList;
        private IEnumerable<InstructionItem> instructionList;
        private IOnTheFlyCreator creator;
        private const double accelerationDistance = 12.5;
        private const double decelerationDistance = 12.5;
        private const double maxSpeedDistance = 90;
        private double firstPosition;
        private double lastPosition;

        [Given(@"an instruction list with cross head movements and tool activations")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivations()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
        }

        [Given(@"an instruction list with cross head movements and tool activations where two activation is within a short distance in the middle and next position is longer than activationDistance")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereTwoActivationIsWithinAShortDistanceInTheMiddleAndNextPositionIsLongerThanActivationDistance()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(100));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200 + maxSpeedDistance - 1));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200 + maxSpeedDistance + decelerationDistance));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(420));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(620));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
        }

        [Given(@"an instruction list with cross head movements and tool activations where two activation is within a short distance in the middle and next position is shorter than activationDistance")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereTwoActivationIsWithinAShortDistanceInTheMiddleAndNextPositionIsShorterThanActivationDistance()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(100));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200 + maxSpeedDistance - 1));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200 + maxSpeedDistance - 1 + decelerationDistance - 1));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(420));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(620));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
        }

        [Given(@"an instruction list with cross head movements and tool activations where first activation is greater than deactivationMinimumDistance and toolstate is (.*) in the beginning")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereFirstActivationIsGreaterThanDeactivationMinimumDistanceAndToolstateIsStateInTheBeginning(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndFirstToolActivationIsWithinADistance(accelerationDistance + 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where first activation is within deactivationMinimumDistance and toolstate is (.*) in the beginning")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereFirstActivationIsWithinDeactivationMinimumDistanceAndToolstateIsStateInTheBeginning(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndFirstToolActivationIsWithinADistance(accelerationDistance - 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where first activation is greater than activationMinimumDistance and toolstate is (.*) in the beginning")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereFirstActivationIsGreaterThanActivationMinimumDistanceAndToolstateInTheBeginning(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndFirstToolActivationIsWithinADistance(decelerationDistance + 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where first activation is within activationMinimumDistance and toolstate is (.*) in the beginning")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereFirstActivationIsWithinActivationMinimumDistanceAndToolstateInTheBeginning(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndFirstToolActivationIsWithinADistance(decelerationDistance - 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where last activation is within deactivationMinimumDistanceInEnd and toolstate is (.*)")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereLastActivationIsWithinDeactivationMinimumDistanceInEndAndToolstate(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndLastToolActivationIsWithinADistance(accelerationDistance - 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where last activation is greater than deactivationMinimumDistanceInEnd and toolstate is (.*)")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereLastActivationIsGreaterThanDeactivationMinimumDistanceInEndAndToolstate(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndLastToolActivationIsWithinADistance(accelerationDistance + 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where last activation is within activationMinimumDistanceInEnd and toolstate is (.*)")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereLastActivationIsWithinActivationMinimumDistanceInEndAndToolstate(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndLastToolActivationIsWithinADistance(decelerationDistance - 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where last activation is greater than activationMinimumDistanceInEnd and toolstate is (.*)")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereLastActivationIsGreaterThanActivationMinimumDistanceInEndAndToolstate(ToolStates state)
        {
            CreateInstructionListWithCrossHeadMovmentsAndLastToolActivationIsWithinADistance(decelerationDistance + 1, state);
        }

        [Given(@"an instruction list with cross head movements and tool activations where last activation is greater than deactivationMinimumDistanceInEnd and cross head movments exists after roller feed and toolstate is CREASE")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereLastActivationIsGreaterThanDeactivationMinimumDistanceInEndAndCrossHeadMovmentsExistsAfterRollerFeedAndToolstateIsCREASE()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(100));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(300));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(400));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(lastPosition));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(600));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(580));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(500));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });

        }

        private void CreateInstructionListWithCrossHeadMovmentsAndLastToolActivationIsWithinADistance(double distance, ToolStates state)
        {
            lastPosition = 600 - distance;
            var everySecondState = ToolStates.Cut;
            if (state == ToolStates.Cut)
            {
                everySecondState = ToolStates.Crease;
            }

            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(100));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = state });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = everySecondState });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(300));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = state });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(400));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = everySecondState });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(lastPosition));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = state });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(600));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = everySecondState });
        }

        private void CreateInstructionListWithCrossHeadMovmentsAndFirstToolActivationIsWithinADistance(double distance, ToolStates state)
        {
            firstPosition = distance;
            var everySecondState = ToolStates.Cut;
            if (state == ToolStates.Cut)
            {
                everySecondState = ToolStates.Crease;
            }

            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(distance));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = state });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(120));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = everySecondState });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(220));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = state });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(320));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = everySecondState });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(420));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = state });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(620));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = everySecondState });
        }

        [Given(@"an instruction list with cross head movements and tool activations where first activation is within a really short distance in the beginning")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivationsWhereFirstActivationIsWithinAReallyShortDistanceInTheBeginning()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(20));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(120));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(220));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(320));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(420));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(620));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
        }

        [Given(@"an instruction list with tool activations and a movement in between")]
        public void GivenAnInstructionListWithToolActivationsAndAMovementInBetween()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.None });
        }

        [Given(@"an instruction list with movement with and without disconnect")]
        public void GivenAnInstructionListWithMovementWithAndWithoutDisconnect()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2) };
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0) { DisconnectTrack = false });
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200) { DisconnectTrack = true });
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
        }

        [Given(@"an instruction list with a series of movements and activations interrupted by a different type")]
        public void GivenAnInstructionListWithASeriesOfMovementsAndActivationsInterruptedByADifferentType()
        {
            originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2)};
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionFeedRollerItem(100));
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(300));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(200));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Crease });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(100));
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.Cut });
            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0) { DisconnectTrack = true });
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.None });
        }

        [Given(@"a OnTheFly creator")]
        public void GivenAOnTheFlyCreator()
        {
            creator = new OnTheFlyCrossHeadCreator();
        }

        [When(@"I create OTF instructions")]
        public void WhenICreateOTFInstructions()
        {
            var physicalMachineSettings = new FusionPhysicalMachineSettings()
            {
                CrossHeadParameters = new FusionCrossHeadParameters()
                {
                    MaximumAcceleration= 10000,
                    MaximumDeceleration = 10000,
                    Acceleration = 100,
                    Speed = 100,
                    MaximumSpeed = 1800
                }
            };

            var delayHandler = new OtfDelayHandler(50,50,50,50);
            var distanceHandler = new OtfActuationDistanceCalculation(new OtfKinematicCalculation(physicalMachineSettings), delayHandler);

            instructionList = creator.Create(originalInstructionList, distanceHandler);
        }

        [Then(@"an instructions list with one OTF instruction is returned")]
        public void ThenAnInstructionsListWithOneOTFInstructionIsReturned()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(3);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(1) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(0);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Cut);
        }

        [Then(@"an instructions list with two OTFs")]
        public void ThenAnInstructionsListWithTwoOTFs()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(9);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(3) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(200);

            Specify.That(instructionList.ElementAt(7)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            otfItem = instructionList.ElementAt(7) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(320);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(420);
            Specify.That(otfItem.Positions.ElementAt(1).ToolState).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(620);
        }

        [Then(@"a regular movement and tool activation in between is returned")]
        public void ThenARegularMovementInBetweenIsReturned()
        {
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That((instructionList.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(220);
            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
        }

        [Then(@"an instructions list with one OTF in the end")]
        public void ThenAnInstructionsListWithOneOTFInTheEnd()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(7);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(5) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(120);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(220);
            Specify.That(otfItem.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(320);
            Specify.That(otfItem.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(420);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(620);
        }

        [Then(@"an instructions list with one OTF including last tool change")]
        public void ThenAnInstructionsListWithOneOTFIncludingLastToolChange()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(5);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(3) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(5);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(otfItem.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(300);
            Specify.That(otfItem.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(400);
            Specify.That(otfItem.Positions.ElementAt(4).Position).Should.BeLogicallyEqualTo(lastPosition);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(600);
        }

        [Then(@"an instructions list with two OTFs is returned")]
        public void ThenAnInstructionsListWithTwoOTFsIsReturned()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(12);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));

            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(3) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(3);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(otfItem.Positions.ElementAt(1).ToolState).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That(otfItem.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(300);
            Specify.That(otfItem.Positions.ElementAt(2).ToolState).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(400);

            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That((instructionList.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(lastPosition); // 0

            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(instructionList.ElementAt(7)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That((instructionList.ElementAt(7) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(600);

            Specify.That(instructionList.ElementAt(8)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(8) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(instructionList.ElementAt(9)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(10)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            otfItem = instructionList.ElementAt(10) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(580);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(500);
            Specify.That(instructionList.ElementAt(11)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(11) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
        }

        [Then(@"an instructions list with two OTFs separated by a tool activation is returned")]
        public void ThenAnInstructionsListWithTwoOTFsSeparatedByAToolActivationIsReturned()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(7);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(3) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(otfItem.Positions.ElementAt(1).ToolState).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(289);
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            otfItem = instructionList.ElementAt(5) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That(otfItem.Positions.ElementAt(1).ToolState).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(620);
            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Crease);
        }

        [Then(@"an instructions list with two OTFs separated by tool activation and cross head movement is returned")]
        public void ThenAnInstructionsListWithTwoOTFsSeparatedByToolActivationAndCrossHeadMovementIsReturned()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(9);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(3) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(otfItem.Positions.ElementAt(1).ToolState).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(289);
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That((instructionList.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(300.5);
            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(instructionList.ElementAt(7)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            otfItem = instructionList.ElementAt(7) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(420);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(620);
            Specify.That(instructionList.ElementAt(8)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(8) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Crease);
        }

        [Then(@"an instructions list with one OTF")]
        public void ThenAnInstructionsListWithOneOTF()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(5);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(3) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(5);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(firstPosition);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(120);
            Specify.That(otfItem.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(220);
            Specify.That(otfItem.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(320);
            Specify.That(otfItem.Positions.ElementAt(4).Position).Should.BeLogicallyEqualTo(420);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(620);
        }

        [Then(@"no regular movement after OTF is returned")]
        public void ThenNoRegularMovementAfterOTFIsReturned()
        {
            Specify.That(instructionList.ElementAt(4)).Should.Not.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
        }

        [Then(@"a regular movement and tool activation of state (.*) in the beginning is returned")]
        public void ThenARegularMovementAndToolActivationOfStateCreaseInTheBeginningIsReturned(ToolStates state)
        {
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(state);
        }

        [Then(@"an instructions list with one OTF in the beginning")]
        public void ThenAnInstructionsListWithOneOTFInTheBeginning()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(7);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(3) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(otfItem.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(300);
            Specify.That(otfItem.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(400);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(lastPosition);
        }

        [Then(@"a regular movement and tool activation of state (.*) in the end is returned")]
        public void ThenARegularMovementAndToolActivationOfToolStateInTheEndIsReturned(ToolStates state)
        {
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(state);
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That((instructionList.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(600);
            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
        }

        [Then(@"the end position for the OTF instruction is populated")]
        public void TheEndPositionForTheOTFInstructionIsPopulated()
        {
            var otfItem = instructionList.ElementAt(1) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(200);
        }

        [Then(@"the OTF instruction is followed by a tool activation")]
        public void TheOTFInstructionIsFollowedByAToolActivation()
        {
            var crossHeadToolActivation = instructionList.ElementAt(2) as InstructionCrossHeadToolActivation;
            Specify.That(crossHeadToolActivation.Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
        }

        [Then(@"the instruction list is unchanged")]
        public void ThenTheInstructionListIsUnchanged()
        {
            Specify.That(originalInstructionList.Count()).Should.BeEqualTo(instructionList.Count());
        }

        [Then(@"an instructions list with OTF is returned")]
        public void ThenAnInstructionsListWithOTFIsReturned()
        {
            Specify.That(instructionList.Count()).Should.BeEqualTo(10);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));

            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            var otfItem = instructionList.ElementAt(1) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(0);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(200);

            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            var activationItem = instructionList.ElementAt(2) as InstructionCrossHeadToolActivation;
            Specify.That(activationItem.Toolstate).Should.BeEqualTo(ToolStates.Crease);

            originalInstructionList.Add(new InstructionCrossHeadMovementItem(0) { DisconnectTrack = true });
            originalInstructionList.Add(new InstructionCrossHeadToolActivation { Toolstate = ToolStates.None });

            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(5) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.Cut);

            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            otfItem = instructionList.ElementAt(6) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(200);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(100);

            Specify.That(instructionList.ElementAt(7)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            activationItem = instructionList.ElementAt(7) as InstructionCrossHeadToolActivation;
            Specify.That(activationItem.Toolstate).Should.BeEqualTo(ToolStates.Cut);

            Specify.That(instructionList.ElementAt(8)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That((instructionList.ElementAt(8) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(0);
            Specify.That((instructionList.ElementAt(8) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeLogicallyEqualTo(true);

            Specify.That(instructionList.ElementAt(9)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(9) as InstructionCrossHeadToolActivation).Toolstate).Should.BeEqualTo(ToolStates.None);
        }

        [Then(@"the end position for the OTF instructions are populated")]
        public void TheEndPositionForTheOTFInstructionsArePopulated()
        {
            var otfItem = instructionList.ElementAt(1) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(200);
            var otfItem2 = instructionList.ElementAt(6) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem2.FinalPosition).Should.BeLogicallyEqualTo(100);
        }

        [Then(@"each OTF instruction is followed by a tool activation")]
        public void EachOTFInstructionIsFollowedByAToolActivation()
        {
            var otfItem = instructionList.ElementAt(2) as InstructionCrossHeadToolActivation;
            Specify.That(otfItem.Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
        }
    }
}
