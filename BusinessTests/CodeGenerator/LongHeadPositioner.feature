﻿@LongHeadPositioner
Feature: Long Head Positioner
	In order to simplify code generation
	As a developer
	I want the long head positioning to be performed in a step of its own

Scenario: Long head positioner should validate if long heads can be distributed
	Given a collection of vertical lines
	And a line of type cut at 150
	And a line of type crease at 550
	And a line of type crease at 700
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I check if long heads can be distributed
	Then positioning check returns true
	And long head positions should be
    | Lh | Type   | Position	| LeftSideToToolOffset	|
	| 1  | cut    | 100			| 57.5					|
	| 2  | crease | 200			| 40.5					|
	| 3  | crease | 300			| 7						|
	| 4  | cut	  | 400			| 7						|

Scenario: Long head positioner should position the first unused long head one corrugate width tolerance from the corrugate edge
	Given a collection of vertical lines
	And a line of type crease at 550
	And a line of type crease at 700
	And long head parameters
	And a number of long heads
	| Lh | Type		| Position	| LeftSideToToolOffset	|
	| 1  | cut		| 100		| 7						|
	| 2  | cut		| 399		| 7						|
	| 3  | cut		| 463		| 7						|
	| 4  | cut		| 550		| 7						|
	| 5  | crease	| 650		| 7						|
	| 6  | crease	| 720		| 7						|
	| 7  | cut		| 790		| 7						|
	| 8  | cut		| 854		| 7						|
	And track parameters with a offset of 800
	And the corrugate width tolerance is 5
	And a corrugate with a width of 400
	And a long head positioner
	When I position the long heads
	Then long head positions should be
	| Lh | Type		| Position	| LeftSideToToolOffset	|
	| 1  | cut		| 100		| 7						|
	| 2  | cut		| 267		| 7						|
	| 3  | cut		| 331		| 7						|
	| 4  | cut		| 395		| 7						|
	| 5  | crease	| 550 		| 7						|
	| 6  | crease	| 700	  	| 7						|
	| 7  | cut		| 805		| 7						|
	| 8  | cut		| 869		| 7						|

Scenario: Long head positioner should skip first long head when required by line type
	Given a collection of vertical lines
	And a line of type crease at 150
	And a line of type crease at 550
	And a line of type cut at 700
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then long head 1 is positioned outside the corrugate on left side as move 1
	Then long head 2 should be positioned to line 1 as move 2
	And long head 4 should be positioned to line 3 as move 3
	And long head 3 should be positioned to line 2 as move 4
	
Scenario: Long head positioner should use first three long head when required by line type
	Given a collection of vertical lines
	And a line of type cut at 150
	And a line of type crease at 550
	And a line of type crease at 700
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then long head 1 should be positioned to line 1 as move 1
	And long head 4 is positioned outside the corrugate on right side as move 2
	And long head 3 should be positioned to line 3 as move 3
	And long head 2 should be positioned to line 2 as move 4


Scenario: Long head positioner should skip first long head when required by tool offset
	Given a collection of vertical lines
	And a line of type crease at 150
	And a line of type crease at 165
	And a line of type crease at 700
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then long head 1 is positioned outside the corrugate on left side as move 1
	And long head 2 should be positioned to line 1 as move 2
	And long head 3 should be positioned to line 2 as move 3
	And long head 4 should be positioned to line 3 as move 4

Scenario: Long head positioner should use first three long head when required by tool offset
	Given a collection of vertical lines
	And a line of type crease at 150
	And a line of type crease at 685
	And a line of type crease at 700
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then long head 4 is positioned outside the corrugate on right side as move 1
	And long head 3 should be positioned to line 3 as move 2
	And long head 2 should be positioned to line 2 as move 3
	And long head 1 should be positioned to line 1 as move 4

Scenario: Long head positioner should move unused long heads outside corrugate on both sides
	Given a collection of vertical lines
	And a line of type crease at 150
	And a line of type crease at 550
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then long head 1 is positioned outside the corrugate on left side as move 1
	And long head 2 should be positioned to line 1 as move 2
	And long head 4 is positioned outside the corrugate on right side as move 3
	And long head 3 should be positioned to line 2 as move 4

Scenario: Long head positioner should skip first two long heads when required to process the design
	Given a collection of vertical lines
	And a line of type crease at 550
	And a line of type cut at 700
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 810
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then long head 1 is positioned outside the corrugate on left side as move 1
	And long head 2 is positioned outside the corrugate on left side as move 2
	And long head 4 should be positioned to line 2 as move 3
	And long head 3 should be positioned to line 1 as move 4

Scenario: Long head positioner should use first two long heads when required to process the design
	Given a collection of vertical lines
	And a line of type cut at 150
	And a line of type crease at 450
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then long head 1 should be positioned to line 1 as move 1
	And long head 4 is positioned outside the corrugate on right side as move 2
	And long head 3 is positioned outside the corrugate on right side as move 3
	And long head 2 should be positioned to line 2 as move 4

Scenario: Long head positioner should throw exception if unable to distribute long head because of line type and tool configuration
	Given a collection of vertical lines
	And a line of type cut at 150
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And track parameters with a offset of 800
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then an exception was thrown

Scenario: Long head positioner should throw exception if unable to distribute long head because of minimum long head position
	Given a collection of vertical lines
	And a line of type crease at 5
	And a line of type cut at 400
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 750
	And a corrugate with a width of 750
	And a long head positioner
	When I position the long heads
	Then an exception was thrown
	
Scenario: Long head positioner should throw exception if unable to distribute long head because of maximum long head position
	Given a collection of vertical lines
	And a line of type cut at 1100
	And a line of type crease at 1265
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And track parameters with a offset of 1270
	And a corrugate with a width of 720
	And a long head positioner
	When I position the long heads
	Then an exception was thrown

Scenario: Should not position first two long heads when unused and outside the corrugate
	Given a collection of vertical lines
	And a line of type crease at 290
	And a line of type cut at 490
	And long head parameters with four long heads with type; crease, crease, crease, cut
	And track parameters with a offset of 1000
	And a corrugate with a width of 720
	And long head 1 is positioned at 50
	And long head 2 is positioned at 150
	And a long head positioner
	When I position the long heads
	Then long head 3 should be positioned to line 1 as move 1
	And long head 4 should be positioned to line 2 as move 2
	And 2 long heads are positioned

Scenario: Should reposition first long head when unused and outside the corrugate but required to move to enable the second long head to be position outside the corrugate
	Given a collection of vertical lines
	And a line of type crease at 310
	And a line of type cut at 510
	And long head parameters with four long heads with type; crease, crease, crease, cut
	And track parameters with a offset of 1020
	And a corrugate with a width of 720
	And long head 1 is positioned at 295
	And long head 2 is positioned at 370
	And a long head positioner
	When I position the long heads
	Then long head 1 is positioned outside the corrugate on left side as move 1
	And long head 2 is positioned outside the corrugate on left side as move 2
	And long head 3 should be positioned to line 1 as move 3
	And long head 4 should be positioned to line 2 as move 4
	And 4 long heads are positioned

Scenario: Should reposition skip first and reposition second outside corrugate when required to process packaging
	Given a collection of vertical lines
	And a line of type crease at 400
	And a line of type cut at 600
	And long head parameters with four long heads with type; crease, crease, crease, cut
	And track parameters with a offset of 920
	And a corrugate with a width of 720
	And long head 1 is positioned at 50
	And long head 2 is positioned at 250
	And a long head positioner
	When I position the long heads
	Then long head 2 is positioned outside the corrugate on left side as move 1
	And long head 3 should be positioned to line 1 as move 2
	And long head 4 should be positioned to line 2 as move 3
	And 3 long heads are positioned

Scenario: Should skip positioning long head already in the correct position and position long head first outside due to minimal number of repositions
	Given a collection of vertical lines
	And a line of type crease at 500
	And a line of type crease at 600
	And a line of type crease at 700
	And track parameters with a offset of 800
	And a corrugate with a width of 700
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And long head 1 is positioned at 450
	And long head 2 is positioned at 550
	And long head 3 is positioned at 600
	And long head 4 is positioned at 650
	And a long head positioner
	When I position the long heads
	Then long head 1 is positioned outside the corrugate on left side as move 1
	And long head 2 should be positioned to line 1 as move 2
	And long head 4 should be positioned to line 3 as move 3
	And 3 long heads are positioned

Scenario: Should skip positioning long head already in the correct position and position long head four outside due to minimal number of repositions
	Given a collection of vertical lines
	And a line of type crease at 200
	And a line of type crease at 300
	And a line of type crease at 400
	And track parameters with a offset of 800
	And a corrugate with a width of 700
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And long head 1 is positioned at 150
	And long head 2 is positioned at 250
	And long head 3 is positioned at 400
	And long head 4 is positioned at 450
	And a long head positioner
	When I position the long heads
	Then long head 2 should be positioned to line 2 as move 1
	And long head 1 should be positioned to line 1 as move 2
	And long head 4 is positioned outside the corrugate on right side as move 3
	And 3 long heads are positioned

Scenario: Should use distribution with minimal position distance when multiple distributions are possible where first is closest to edge
	Given a collection of vertical lines
	And a line of type crease at 200
	And a line of type crease at 300
	And a line of type crease at 400
	And track parameters with a offset of 500
	And a corrugate with a width of 400
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And long head 1 is positioned at 149
	And long head 2 is positioned at 250
	And long head 3 is positioned at 350
	And long head 4 is positioned at 450
	And a long head positioner
	When I position the long heads
	Then long head 1 is positioned outside the corrugate on left side as move 1
	And long head 2 should be positioned to line 1 as move 2
	And long head 3 should be positioned to line 2 as move 3
	And long head 4 should be positioned to line 3 as move 4
	And 4 long heads are positioned

Scenario: Should use distribution with minimal position distance when multiple distributions are possible where fourth is closest to edge
	Given a collection of vertical lines
	And a line of type crease at 200
	And a line of type crease at 300
	And a line of type crease at 400
	And track parameters with a offset of 500
	And a corrugate with a width of 400
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And long head 1 is positioned at 150
	And long head 2 is positioned at 250
	And long head 3 is positioned at 350
	And long head 4 is positioned at 451
	And a long head positioner
	When I position the long heads
	Then long head 2 should be positioned to line 2 as move 1
	And long head 1 should be positioned to line 1 as move 2
	And long head 4 is positioned outside the corrugate on right side as move 3
	And long head 3 should be positioned to line 3 as move 4
	And 4 long heads are positioned

Scenario: Should skip positioning long head already in the correct position by previous positioning
	Given a collection of vertical lines
	And a line of type crease at 500
	And a line of type crease at 600
	And a line of type crease at 700
	And track parameters with a offset of 800
	And a corrugate with a width of 700
	And long head parameters with four long heads with type; crease, crease, crease, crease
	And long head 1 is positioned at 450
	And long head 2 is positioned at 550
	And long head 3 is positioned at 600
	And long head 4 is positioned at 650
	And a long head positioner
	When I position the long heads
	And I position the long heads again
	Then 0 long heads are positioned

Scenario: Should not generate long head distribution where long heads in the middle are skipped
	Given a collection of vertical lines
	And a line of type cut at 500
	And a line of type crease at 600
	And track parameters with a offset of 800
	And a corrugate with a width of 400
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And long head 1 is positioned at 0
	And long head 2 is positioned at 64
	And long head 3 is positioned at 128
	And long head 4 is positioned at 192
	And a long head positioner
	When I position the long heads
	Then 4 long heads are positioned

	@ignore
Scenario: Should distribute long head when design has no vertical lines
	Given a collection of vertical lines
	And track parameters with a offset of 530 for the left track and 851.8 for the right track
	And a corrugate with a width of 480
	And long head parameters
	And a number of long heads
	| Lh | Type		| Position	| LeftSideToToolOffset	|
	| 1  | cut		| 85.54		| 56.8					|
	| 2  | crease	| 581.13	| 41.2					|
	| 3  | crease	| 664.985	| 22.8					|
	| 4  | crease	| 795.62	| 41.2					|
	| 5  | crease	| 841.235	| 22.8					|
	| 6  | cut		| 1305.29	| 7.2					|
	And a long head positioner
	When I position the long heads on track 2
	Then positioning check returns true

	Scenario: Should throw exception when vertical line is outside corrugate
	Given a collection of vertical lines
	And a line of type cut at 10
	And a line of type crease at 150
	And a line of type crease at 400
	And track parameters with a offset of 450
	And a corrugate with a width of 400
	And long head parameters with four long heads with type; cut, crease, crease, cut
	And long head 1 is positioned at 0
	And long head 2 is positioned at 64
	And long head 3 is positioned at 128
	And long head 4 is positioned at 192
	And a long head positioner
	When I position the long heads
	Then an exception was thrown

	Scenario: Should reposition all long heads outside corrugate with tool width as distance between their positions
	Given a collection of vertical lines
	And long head parameters with four long heads with type; crease, cut, crease, cut
	And track parameters with a offset of 720
	And a corrugate with a width of 720
	And long head 1 is positioned at 100
	And long head 2 is positioned at 200
	And long head 3 is positioned at 300
	And long head 4 is positioned at 400
	And a long head positioner
	When I position the long heads
	Then long head 4 is positioned outside the corrugate at position 866.5 on right side as move 1
	And long head 3 is positioned outside the corrugate at position 802.5 on right side as move 2
	And long head 2 is positioned outside the corrugate at position 789.0 on right side as move 3
	And long head 1 is positioned outside the corrugate at position 725 on right side as move 4
	And 4 long heads are positioned

	Scenario: Should handle storing of all positions as expected when generating code twice
	Given a collection of vertical lines
	And a line of type crease at 525.18
	And a line of type crease at 731.38
	And a line of type cut at 834.48
	And a second collection of vertical lines
	And in second collection a line of type crease at 551.35
	And in second collection a line of type crease at 808.35
	And in second collection a line of type cut at 936.85
	And long head parameters
	And a number of long heads
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 66.125   | 59.5                 |
	| 2  | crease | 144.915  | 44.5                 |
	| 3  | crease | 289.35   | 20                   |
	| 4  | crease | 552.065  | 38.5                 |
	| 5  | crease | 808.975  | 20                   |
	| 6  | cut    | 937.21   | 6                    |
	And track parameters with a offset of 422.21 with right side fixed false
	And a corrugate with a width of 902.0
	And a long head positioner
	When I position the long heads
	And I position the long heads for second collection
	Then long head 4 should be positioned to line 1 as move 1
	And long head 5 should be positioned to line 2 as move 2
	And long head 6 should be positioned to line 3 as move 3
	And 3 long heads are positioned
	And long head positions should be 
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 66.125   | 59.5                 |
	| 2  | crease | 144.915  | 44.5                 |
	| 3  | crease | 289.35   | 20                   |
	| 4  | crease | 525.18   | 38.5                 |
	| 5  | crease | 731.38   | 20                   |
	| 6  | cut    | 834.48   | 6                    |
	And for second collection long head 4 should be positioned to line 1 as move 1
	And for second collection long head 6 should be positioned to line 3 as move 2
	And for second collection long head 5 should be positioned to line 2 as move 3
	And for second collection 3 long heads are positioned
	And for second collection long head positions should be
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 66.125   | 59.5                 |
	| 2  | crease | 144.915  | 44.5                 |
	| 3  | crease | 289.35   | 20                   |
	| 4  | crease | 551.35   | 38.5                 |
	| 5  | crease | 808.35   | 20                   |
	| 6  | cut    | 936.85   | 6                    |

	Scenario: Should not put longheads outside the maximum LH-position
	Given a collection of vertical lines
	And a line of type crease at 44.0
	And track parameters with a offset of 25.0 for the left track and 35.0 for the right track
	And a corrugate with a width of 15.5
	And the corrugate width tolerance is 0.276
	And long head inch parameters with a maximumToolPosition of 54.10
	And a number of long heads
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 10.0	 | 2.205                |
	| 2  | crease | 18.0	 | 1.575                |
	| 3  | crease | 25.0	 | 0.945                |
	| 4  | cut	  | 35.0     | 0.945                |
	| 5  | crease |	40.0	 | 2.205                |
	| 6  | crease | 45.0     | 0.945                |
	| 7  | cut    | 48.0	 | 0.275                |
	And a long head positioner
	When I position the long heads on track 2
	Then long head positions should be
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 10.0	 | 2.205                |
	| 2  | crease | 18.0	 | 1.575                |
	| 3  | crease | 25.0	 | 0.945                |
	| 4  | cut	  | 30.944   | 0.945                |
	| 5  | crease |	34.724	 | 2.205                |
	| 6  | crease | 44.0     | 0.945                |
	| 7  | cut    | 50.776	 | 0.275                |

	Scenario: Should not put longheads outside the minimum LH-position
	Given a collection of vertical lines
	And a line of type crease at 10.0
	And track parameters with a offset of 25.0 for the left track and 35.0 for the right track
	And a corrugate with a width of 22.0
	And the corrugate width tolerance is 0.276
	And long head inch parameters with a minimumToolPosition of 2.0
	And a number of long heads
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 10.0	 | 2.205                |
	| 2  | crease | 18.0	 | 1.575                |
	| 3  | crease | 25.0	 | 0.945                |
	| 4  | cut	  | 35.0     | 0.945                |
	| 5  | crease |	40.0	 | 2.205                |
	| 6  | crease | 45.0     | 0.945                |
	| 7  | cut    | 48.0	 | 0.275                |
	And a long head positioner
	When I check if long heads can be distributed on track 1
	Then positioning check returns true

	Scenario: Should be able to check distribution with 7 longheads
	Given a collection of vertical lines
	And a line of type cut at 45.0
	And track parameters with a offset of 25.0 for the left track and 35.0 for the right track
	And a corrugate with a width of 15.5
	And the corrugate width tolerance is 0.276
	And long head inch parameters with a maximumToolPosition of 54.10
	And a number of long heads
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 10.0	 | 2.205                |
	| 2  | crease | 18.0	 | 1.575                |
	| 3  | crease | 25.0	 | 0.945                |
	| 4  | cut	  | 35.0     | 0.945                |
	| 5  | crease |	40.0	 | 2.205                |
	| 6  | crease | 45.0     | 0.945                |
	| 7  | cut    | 48.0	 | 0.275                |
	And a long head positioner
	When I check if long heads can be distributed on track 2
	Then positioning check returns true

	Scenario: CAR 719
	Given a collection of vertical lines
	And lines at
	| Type	| Position	|
	| cut	| 1280		|
	And track parameters
	And track parameters with a offset of 880 for the left track and 900 for the right track
	And a corrugate with a width of 402
	And the corrugate width tolerance is 7.0
	And long head millimeter parameters with a maximumToolPosition of 1384
	And a number of long heads
	| Lh | Type   | Position | LeftSideToToolOffset |
	| 1  | cut    | 40.0	 | 56.0	                |
	| 2  | crease | 894.0	 | 38.0	                |
	| 3  | crease | 942.5	 | 22.5	                |
	| 4  | crease |	1022.7	 | 38.7	                |
	| 5  | crease | 1070.5   | 22.5	                |
	| 6  | cut    | 1280	 | 5.0	                |
	And a long head positioner
	When I check if long heads can be distributed on track 2
	Then positioning check returns true
