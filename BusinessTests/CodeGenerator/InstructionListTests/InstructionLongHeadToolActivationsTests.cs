﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.LongHeadPositioner;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator.InstructionListTests
{
    [TestClass]
    public class InstructionLongHeadToolActivationsTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPrintPlcArray()
        {
            var lhs = new List<LongHeadToolActivationInformation>();
            lhs.Add(new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None));
            lhs.Add(new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None));
            lhs.Add(new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.None));
            lhs.Add(new LongHeadToolActivationInformation(4, ToolStates.None, ToolStates.None));

            var array = new InstructionLongHeadToolActivations(lhs).ToPlcArray();

            Specify.That(array).Should.Not.BeNull();
            Specify.That(array.Count()).Should.BeEqualTo(10);
            Specify.That(array.ElementAt(0)).Should.BeEqualTo((short)InstructionTypes.NewInstruction);
            Specify.That(array.ElementAt(1)).Should.BeEqualTo((short)InstructionTypes.LongHeadToolActivation);
            Specify.That(array.ElementAt(7)).Should.BeEqualTo((short)ToolStates.Cut);
        }
    }
}
