﻿

namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;

    using Testing.Specificity;

    [TestClass]
    public class InstructionCrossHeadToolActivationItemTests
    {
        [TestMethod]
        public void ShouldContainAToolState()
        {
            var instructionCrossHeadToolStateActivationItem = new InstructionCrossHeadToolActivation
                { Toolstate = ToolStates.Crease };

            Specify.That(instructionCrossHeadToolStateActivationItem.Toolstate).Should.BeEqualTo(ToolStates.Crease);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntries()
        {
            var instructionCrossHeadToolStateActivationItem = new InstructionCrossHeadToolActivation
                { Toolstate = ToolStates.Cut };

            var plcInstructionArray = instructionCrossHeadToolStateActivationItem.ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)3);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)300);

            instructionCrossHeadToolStateActivationItem.Toolstate = ToolStates.Crease;

            plcInstructionArray = instructionCrossHeadToolStateActivationItem.ToPlcArray();
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)100);
        }

        [TestMethod]
        public void ToStringShouldReturnHumanReadableFormat()
        {
            var instructionCrossHeadToolStateActivationItem = new InstructionCrossHeadToolActivation
                { Toolstate = ToolStates.Crease };

            Specify.That(instructionCrossHeadToolStateActivationItem.ToString()).Should.BeEqualTo("Set CrossHead toolstate to 'Crease'");
        }
    }
}
