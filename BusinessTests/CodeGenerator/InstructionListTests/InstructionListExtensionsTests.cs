﻿using PackNet.Communication.Communicators.Fusion;

namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;

    using Testing.Specificity;

    [TestClass]
    public class InstructionListExtensionsTests
    {
        [TestMethod]
        public void GetPlcInstructionArrayShouldReturnArrayWithPlcInstructions()
        {
            var instructionList = new List<InstructionItem> {
                new InstructionMetaDataItem(2550, 1),
                new InstructionCrossHeadMovementItem
                { DisconnectTrack = false, SimultaneousWithFeed = false, Position = 1020 },
                new InstructionCrossHeadToolActivation
                { Toolstate = ToolStates.Cut },
            };

            var plcInstructionArray = instructionList.GetPlcInstructionArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)5);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)25);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)5000);
            Specify.That(plcInstructionArray[4]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[5]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[6]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[7]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[8]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[9]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[10]).Should.BeEqualTo((short)10);
            Specify.That(plcInstructionArray[11]).Should.BeEqualTo((short)2000);
            Specify.That(plcInstructionArray[12]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[13]).Should.BeEqualTo((short)3);
            Specify.That(plcInstructionArray[14]).Should.BeEqualTo((short)300);
        }
    }
}
