﻿namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.InstructionList;

    using Testing.Specificity;

    [TestClass]
    public class InstructionListEndMarkerItemTests
    {
        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntries()
        {
            var endMarker = new InstructionEndMarkerItem();

            var plcInstruction = endMarker.ToPlcArray();

            Specify.That(plcInstruction[0]).Should.BeEqualTo((short)32000);
        }

        [TestMethod]
        public void ToStringShouldWriteDataInAHumanFriendlyForm()
        {
            var endMarker = new InstructionEndMarkerItem();

            Specify.That(endMarker.ToString()).Should.BeEqualTo("End of packaging");
        }
    }
}
