﻿namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.InstructionList;

    using Testing.Specificity;

    [TestClass]
    public class InstructionListMetaDataItemTests
    {
        [TestMethod]
        public void ShouldContainTrackNumberAndBoxLengthAndSynchronizeLabelIndicator()
        {
            var metaContainer = new InstructionMetaDataItem(10, 1);

            Specify.That(metaContainer.TrackNumber).Should.BeEqualTo(1);
            Specify.That(metaContainer.PackagingLength).Should.BeLogicallyEqualTo(10);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntries()
        {
            var metaData = new InstructionMetaDataItem(5165, 1);

            var plcInstruction = metaData.ToPlcArray();

            Specify.That(plcInstruction[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstruction[1]).Should.BeEqualTo((short)5);
            Specify.That(plcInstruction[2]).Should.BeEqualTo((short)51);
            Specify.That(plcInstruction[3]).Should.BeEqualTo((short)6500);
            Specify.That(plcInstruction[4]).Should.BeEqualTo((short)1);
            Specify.That(plcInstruction[5]).Should.BeEqualTo((short)0);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntriesWhenDecimalsAreUsed()
        {
            var metaData = new InstructionMetaDataItem(2251.65, 2);

            var plcInstruction = metaData.ToPlcArray();

            Specify.That(plcInstruction[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstruction[1]).Should.BeEqualTo((short)5);
            Specify.That(plcInstruction[2]).Should.BeEqualTo((short)22);
            Specify.That(plcInstruction[3]).Should.BeEqualTo((short)5165);
            Specify.That(plcInstruction[4]).Should.BeEqualTo((short)2);
            Specify.That(plcInstruction[5]).Should.BeEqualTo((short)0);
        }

        [TestMethod]
        public void ToStringShouldWriteDataInAHumanFriendlyForm()
        {
            var metaData = new InstructionMetaDataItem(10, 1);

            Specify.That(metaData.ToString()).Should.BeEqualTo("Active Track: 1 | Packaging Length: 10");
        }
    }
}
