﻿
namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;

    using Testing.Specificity;

    [TestClass]
    public class CrossHeadOnTheFlyPositionTests
    {
        [TestMethod]
        public void ConstructorShouldSetProperties()
        {
            var otfPosition = new CrossHeadOnTheFlyPosition(100, ToolStates.Crease);
            Specify.That(otfPosition.Position).Should.BeLogicallyEqualTo(100);
            Specify.That(otfPosition.ToolState).Should.BeEqualTo(ToolStates.Crease);
            otfPosition = new CrossHeadOnTheFlyPosition(200, ToolStates.Cut);
            Specify.That(otfPosition.Position).Should.BeLogicallyEqualTo(200);
            Specify.That(otfPosition.ToolState).Should.BeEqualTo(ToolStates.Cut);
        }

        [TestMethod]
        public void CopyShouldHaveSameValuesButShouldNotBeSameObject()
        {
            var otfPosition = new CrossHeadOnTheFlyPosition(100, ToolStates.Crease);
            var copy = otfPosition.Copy();
            Specify.That(otfPosition.Position).Should.BeLogicallyEqualTo(copy.Position);
            Specify.That(otfPosition.ToolState).Should.BeLogicallyEqualTo(copy.ToolState);
        }

        [TestMethod]
        public void ToStringReturnsHumanReadableFormat()
        {
            var pos = new CrossHeadOnTheFlyPosition(100, ToolStates.None);

            Specify.That(pos.ToString()).Should.BeEqualTo("100,None");
        }
    }
}
