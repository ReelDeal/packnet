﻿namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class InstructionCrossHeadMovementItemTests
    {
        [TestMethod]
        public void ShouldContainPosition()
        {
            var crossHeadMovementItem = new InstructionCrossHeadMovementItem(new MicroMeter(100));

            Specify.That(crossHeadMovementItem.Position).Should.BeLogicallyEqualTo(new MicroMeter(100));
        }

        [TestMethod]
        public void ShouldContainDisconnectTrack()
        {
            var crossHeadMovementItem = new InstructionCrossHeadMovementItem(new MicroMeter(100))
                { DisconnectTrack = true };

            Specify.That(crossHeadMovementItem.DisconnectTrack).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldContainSimultaneousWithFeed()
        {
            var crossHeadMovementItem = new InstructionCrossHeadMovementItem(new MicroMeter(100))
                { SimultaneousWithFeed = true };

            Specify.That(crossHeadMovementItem.SimultaneousWithFeed).Should.BeTrue();
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntries()
        {
            var crossHeadMovementItem = new InstructionCrossHeadMovementItem
                { Position = 1528, SimultaneousWithFeed = false, DisconnectTrack = false };

            var plcInstructionArray = crossHeadMovementItem.ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[4]).Should.BeEqualTo((short)15);
            Specify.That(plcInstructionArray[5]).Should.BeEqualTo((short)2800);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntriesWhenDecimalsAreUsed()
        {
            var crossHeadMovementItem = new InstructionCrossHeadMovementItem
                { Position = 115.28, SimultaneousWithFeed = false, DisconnectTrack = false };

            var plcInstructionArray = crossHeadMovementItem.ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[4]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[5]).Should.BeEqualTo((short)1528);
        }

        [TestMethod]
        public void ToStringReturnsHumanReadableFormat()
        {
            var crossHeadMovementItem = new InstructionCrossHeadMovementItem
                { SimultaneousWithFeed = true, DisconnectTrack = false, Position = 456 };

            Specify.That(crossHeadMovementItem.ToString()).Should.BeEqualTo("Move cross head to position 456. Disconnect: False, SimultaneousWithFeed: True");
        }
    }
}
