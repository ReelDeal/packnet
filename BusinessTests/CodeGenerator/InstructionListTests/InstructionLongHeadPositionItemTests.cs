﻿namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    using Testing.Specificity;

    [TestClass]
    public class InstructionLongHeadPositionItemTests
    {
        [TestMethod]
        public void ShouldHaveNoLongHeadsToPositionIfEmptyListIsGiven()
        {
            var instruction = new InstructionLongHeadPositionItem(new List<LongHeadPositioningInformation>());

            Specify.That(instruction.LongHeadsToPosition.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        public void ShouldHaveExpectedLongHeadsToPositionIfListIsGiven()
        {
            var positions = new List<LongHeadPositioningInformation>
                {
                    new LongHeadPositioningInformation(2, 201, 0),
                    new LongHeadPositioningInformation(4, 404, 0),
                    new LongHeadPositioningInformation(5, 506, 0)
                };
            var instruction = new InstructionLongHeadPositionItem(positions);

            Specify.That(instruction.LongHeadsToPosition.ToList()).Should.BeEquivalentTo(positions);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntries()
        {
            var positions = new List<LongHeadPositioningInformation>
                {
                    new LongHeadPositioningInformation(4, 121, 0),
                    new LongHeadPositioningInformation(5, 205, 0),
                    new LongHeadPositioningInformation(6, 308, 0)
                };
            var plcInstructionArray = new InstructionLongHeadPositionItem(positions).ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)2);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)4);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[4]).Should.BeEqualTo((short)2100);
            Specify.That(plcInstructionArray[5]).Should.BeEqualTo((short)5);
            Specify.That(plcInstructionArray[6]).Should.BeEqualTo((short)2);
            Specify.That(plcInstructionArray[7]).Should.BeEqualTo((short)500);
            Specify.That(plcInstructionArray[8]).Should.BeEqualTo((short)6);
            Specify.That(plcInstructionArray[9]).Should.BeEqualTo((short)3);
            Specify.That(plcInstructionArray[10]).Should.BeEqualTo((short)800);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntriesWhenDecimalsAreUsed()
        {
            var positions = new List<LongHeadPositioningInformation>
                {
                    new LongHeadPositioningInformation(4, 1568.12, 0),
                    new LongHeadPositioningInformation(5, 2401.68, 0),
                    new LongHeadPositioningInformation(6, 3125.2, 0)
                };
            var plcInstructionArray = new InstructionLongHeadPositionItem(positions).ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)2);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)4);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)15);
            Specify.That(plcInstructionArray[4]).Should.BeEqualTo((short)6812);
            Specify.That(plcInstructionArray[5]).Should.BeEqualTo((short)5);
            Specify.That(plcInstructionArray[6]).Should.BeEqualTo((short)24);
            Specify.That(plcInstructionArray[7]).Should.BeEqualTo((short)168);
            Specify.That(plcInstructionArray[8]).Should.BeEqualTo((short)6);
            Specify.That(plcInstructionArray[9]).Should.BeEqualTo((short)31);
            Specify.That(plcInstructionArray[10]).Should.BeEqualTo((short)2520);
        }

        [TestMethod]
        public void ToStringShouldWriteDataInAHumanFriendlyForm()
        {
            var positions = new List<LongHeadPositioningInformation>
                {
                    new LongHeadPositioningInformation(4, 121, 0),
                    new LongHeadPositioningInformation(5, 205, 0),
                    new LongHeadPositioningInformation(6, 307, 0)
                };
            var text = new InstructionLongHeadPositionItem(positions).ToString();

            Specify.That(text).Should.BeEqualTo("Move long heads - 4:121|5:205|6:307");
        }
    }
}
