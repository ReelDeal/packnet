﻿

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using System.Linq;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class InstructionCrossHeadOnTheFlyItemTests
    {
        [TestMethod]
        public void ConstructorTest()
        {
            var otfItem = new InstructionCrossHeadOnTheFlyItem();
            Specify.That(otfItem.HighSpeed).Should.BeTrue();
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntries()
        {
            var otfItem = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 123 };
            otfItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(1050, ToolStates.Cut));
            otfItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(201, ToolStates.Crease));

            var plcInstructionArray = otfItem.ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)6);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[4]).Should.BeEqualTo((short)2300);
            Specify.That(plcInstructionArray[5]).Should.BeEqualTo((short)10);
            Specify.That(plcInstructionArray[6]).Should.BeEqualTo((short)5000);
            Specify.That(plcInstructionArray[7]).Should.BeEqualTo((short)300);
            Specify.That(plcInstructionArray[8]).Should.BeEqualTo((short)2);
            Specify.That(plcInstructionArray[9]).Should.BeEqualTo((short)100);
            Specify.That(plcInstructionArray[10]).Should.BeEqualTo((short)100);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntriesWhenDecimalsAreUsed()
        {
            var otfItem = new InstructionCrossHeadOnTheFlyItem { FinalPosition = new MicroMeter(123.456) };
            otfItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(2121.245, ToolStates.Cut));
            otfItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(201.202, ToolStates.Crease));

            var plcInstructionArray = otfItem.ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)6);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)0);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)1);
            Specify.That(plcInstructionArray[4]).Should.BeEqualTo((short)2346);
            Specify.That(plcInstructionArray[5]).Should.BeEqualTo((short)21);
            Specify.That(plcInstructionArray[6]).Should.BeEqualTo((short)2124);
            Specify.That(plcInstructionArray[7]).Should.BeEqualTo((short)300);
            Specify.That(plcInstructionArray[8]).Should.BeEqualTo((short)2);
            Specify.That(plcInstructionArray[9]).Should.BeEqualTo((short)120);
            Specify.That(plcInstructionArray[10]).Should.BeEqualTo((short)100);
        }

        [TestMethod]
        public void ToStringReturnsHumanReadableFormat()
        {
            var otfItem = new InstructionCrossHeadOnTheFlyItem { HighSpeed = false };

            otfItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(100, ToolStates.Cut));
            otfItem.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(200, ToolStates.Crease));
            otfItem.FinalPosition = 300;

            Specify.That(otfItem.ToString()).Should.BeEqualTo("OnTheFly cross head @ low speed: 100,Cut|200,Crease|FINAL POSITION: 300");
        }
    }
}
