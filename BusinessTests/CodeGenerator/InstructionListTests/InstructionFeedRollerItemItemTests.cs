﻿namespace BusinessTests.CodeGenerator.InstructionListTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class InstructionFeedRollerItemTests
    {
        [TestMethod]
        public void ShouldContainPosition()
        {
            var instructionFeedRollerItem = new InstructionFeedRollerItem(new MicroMeter(123));

            Specify.That(instructionFeedRollerItem.Position).Should.BeLogicallyEqualTo(new MicroMeter(123));
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntries()
        {
            var instructionFeedRollerItem = new InstructionFeedRollerItem { Position = 1564 };

            var plcInstructionArray = instructionFeedRollerItem.ToPlcArray();

            Specify.That(plcInstructionArray[0]).Should.BeEqualTo((short)31000);
            Specify.That(plcInstructionArray[1]).Should.BeEqualTo((short)4);
            Specify.That(plcInstructionArray[2]).Should.BeEqualTo((short)15);
            Specify.That(plcInstructionArray[3]).Should.BeEqualTo((short)6400);
        }

        [TestMethod]
        public void ToPlcArrayReturnsCorrectInstructionEntriesWhenDecimalsAreUsed()
        {
            var instructionFeedRollerItem = new InstructionFeedRollerItem { Position = 100.01 };

            Specify.That(instructionFeedRollerItem.ToPlcArray()[0]).Should.BeEqualTo((short)31000);
            Specify.That(instructionFeedRollerItem.ToPlcArray()[1]).Should.BeEqualTo((short)4);
            Specify.That(instructionFeedRollerItem.ToPlcArray()[2]).Should.BeEqualTo((short)1);
            Specify.That(instructionFeedRollerItem.ToPlcArray()[3]).Should.BeEqualTo((short)1);
        }

        [TestMethod]
        public void ToStringReturnsHumanReadableFormat()
        {
            var instructionFeedRollerItem = new InstructionFeedRollerItem(100);

            Specify.That(instructionFeedRollerItem.ToString()).Should.BeEqualTo("Feed roller 100 mm");
        }
    }
}
