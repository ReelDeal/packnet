﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BusinessTests.Machines;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.LongHeadPositioner;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator.InstructionListTests
{
    [TestClass]
    public class InstructionLongHeadOnTheFlyItemTests
    {
        [TestMethod]
        public void ShouldReturnPlcArray()
        {
            var otfInstruction = new InstructionLongHeadOnTheFlyItem();
            otfInstruction.FinalPosition = 1000;
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(150, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None)
            }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(350, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.None)
            }));
            otfInstruction.AddOnTheFlyPosition(new LongHeadOnTheFlyPosition(800, 66, new List<LongHeadToolActivationInformation>()
            {
                new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Cut),
                new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Crease),
                new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.Crease)
            }));

            var instruction = otfInstruction.ToPlcArray();

            foreach(var i in instruction)
                Console.WriteLine(i);

            Specify.That(instruction).Should.Not.BeNull();
            Specify.That(instruction.Count()).Should.BeEqualTo(29);
            Specify.That(instruction.Count(i => i == (short)InstructionTypes.NewInstruction)).Should.BeEqualTo(1);
            Specify.That(instruction.Count(i => i == (short)InstructionTypes.OnTheFlyNewPositionMarker)).Should.BeEqualTo(3);
            Specify.That(instruction.ElementAt(1)).Should.BeEqualTo((short)InstructionTypes.OnTheFlyLongHeadInstruction);
        }
    }
}
