﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator.InstructionListTests
{
    [TestClass]
    public class InstructionTrackActivationItemTests
    {
        [TestMethod]
        public void ShouldSetTrackNumberAndActivationProperties()
        {
            var instruction = new InstructionTrackActivationItem(2, true);
            Specify.That(instruction.TrackNumber).Should.BeEqualTo((short)2);
            Specify.That(instruction.Activate).Should.BeTrue();
            Specify.That(instruction.ToString()).Should.BeEqualTo("Activate track 2");
        }

        [TestMethod]
        public void ShouldReturnCorrectPlcInstructionList()
        {
            short tracknumber = 2;
            var instruction = new InstructionTrackActivationItem(tracknumber, true);
            var array = instruction.ToPlcArray();
            
            Specify.That(array.Length).Should.BeEqualTo(4);
            Specify.That(array.ElementAt(0)).Should.BeEqualTo((short)InstructionTypes.NewInstruction);
            Specify.That(array.ElementAt(1)).Should.BeEqualTo((short)InstructionTypes.TrackActivation);
            Specify.That(array.ElementAt(2)).Should.BeEqualTo(tracknumber);
            Specify.That(array.ElementAt(3)).Should.BeEqualTo((short)ActivationStates.Active);
        }
    }
}
