﻿//using System.Diagnostics;
//using System.IO;

//using Moq;

//using PackNet.Business.CodeGenerator;
//using PackNet.Common.ExtensionMethods;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Services;
//using PackNet.Business.DesignRulesApplicator;
//using DesignFormula = PackNet.Common.Interfaces.DTO.PackagingDesigns.DesignFormulaParameters;
//using PackagingDesignManager = PackNet.Business.PackagingDesigns.PackagingDesignManager;

//namespace BusinessTests.CodeGenerator.AStarEmTests
//{
//    using PackNet.Business.EmJobCreator;
//    using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
//    using PackNet.Common.Interfaces.DTO.Corrugates;

//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using PackNet.Business.CodeGenerator.AStarStuff;
//    using PackNet.Common.Interfaces.DTO;

//    using Testing.Specificity;
//    #if DEBUG
//    [TestClass]
//    [DeploymentItem("..\\..\\CodeGenerator\\AStarEmTests\\TestData", "CodeGenTestData")]
//    public class AStarAlgorithmEMOptimizedPhysicalDesignTests
//    {
//        private EmPhysicalMachineSettings machineSettings;

//        private MicroMeter[] currentPositions;
//        [TestInitialize]
//        public void Setup()
//        {
//            this.currentPositions = new MicroMeter[4] { 0, 0, 0, 0 };
//            this.machineSettings = new EmPhysicalMachineSettings();
//            machineSettings.LongHeadParameters = new EmLongHeadParameters(new List<EmLongHead>(), 3600, 55, 15126.05, 55, 80, 2464, -50, 54, 143);
//            machineSettings.FeedRollerParameters = new EmFeedRollerParameters(1500, 100, 4261.36, 100, 600, 400);
//            machineSettings.CrossHeadParameters = new EmCrossHeadParameters{CutCompensation = 7d, CreaseCompensation = 7d, MinimumLineLength = 5d};
//        }

//        [TestMethod]
//        [TestCategory("Bug")]
//        [TestCategory("Bug 10172")]
//        [TestCategory("Integration")]
//        [Description("Test for bug 10172 where a line at the end of the design would be processed twice "
//                   + "and another one not processed at all because the iterations had the same NewLongheadPositions")]
//        public void DifferentIterationsInSameNode_ShouldhaveDifferentNewLongheadPositions()
//        {
//            var offSet = new MicroMeter(550d);
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();
//            var xValues = new Dictionary<string, double>() { { "X1", 0d }, { "X2", 50d } };

//            var design = GetPhysicalDesign(10, 200d, 200d, 200d, 1000d, xValues);
//            var compensatedDesign = new DesignCreator(10d, 10d).CreateCompensatedDesign(
//                design,
//                new Corrugate() { Thickness = 3, Width = 1000d },
//                false,
//                offSet,
//                machineSettings);

//            var sectionCreator = new SectionCreator();

//            var timer = new Stopwatch();
//            timer.Start();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, design.GetVerticalPhysicalLines(), sectionCreator, new SectionEvaluator(), compensatedDesign);
//            timer.Stop();
//            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds);
//            Console.WriteLine("----------------------");
//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();
//            var solutionNodes = solution.GetSolutionNodes().ToList();
//            Specify.That(solutionNodes.Count()).Should.BeEqualTo(4);

//            Specify.That(solutionNodes.ElementAt(0).Iterations.Count()).Should.BeEqualTo(1);
//            Specify.That(solutionNodes.ElementAt(1).Iterations.Count()).Should.BeEqualTo(2);
//            Specify.That(solutionNodes.ElementAt(2).Iterations.Count()).Should.BeEqualTo(1);
//            Specify.That(solutionNodes.ElementAt(3).Iterations.Count()).Should.BeEqualTo(1);

//            var newLHPositionsSecondNodeIteration1 = solutionNodes.ElementAt(1).Iterations.ElementAt(0).First().NewLongHeadPositions;
//            var newLHPositionsSecondNodeIteration2 = solutionNodes.ElementAt(1).Iterations.ElementAt(1).First().NewLongHeadPositions;

//            Specify.That(newLHPositionsSecondNodeIteration1.SequenceEqual(newLHPositionsSecondNodeIteration2)).Should.BeFalse("The longhead positions should be different for the two iterations in this node");
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForAccuracyTestDesig()
//        {
//            this.SetupLongHeads(10);

//            var algorithmEmOptimized = new EmLongHeadDistributor();

//            var design = GetPhysicalDesign(1111208, 0, 0, 0, 1000);
//            var lines = LoadPhysicalDesignWithDimentions_AndApplyRulesOnIt(1111208, 0, 0, 0, 1000);

//            var sectionCreator = new SectionCreator();
//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);
//            // TODO: Should uncommented when implementation is done to make the time measured more correct.

//            var timer = new Stopwatch();
//            timer.Start();
//            for(var i = 0; i < 10; i++)
//                solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);
//            timer.Stop();
//            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds/10);
//            Console.WriteLine("Score for solution: " + solution.Score);
//            Console.WriteLine("Expanded nodes: " + AStarNodeEM.NumberOfExpandedNodes/11);
//            Console.WriteLine("----------------------");
//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();
//            //Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(1);
//            //Specify.That(solution.GetSolutionNodes().ElementAt(0).Iterations.Count()).Should.BeLogicallyEqualTo(4);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForTestEmb_0402_0001Design()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();

//            var design = GetPhysicalDesign(1003, 200, 200, 200, 1000);
//            var lines = LoadPhysicalDesignWithDimentions_AndApplyRulesOnIt(1003, 200, 200, 200, 1000);

//            var sectionCreator = new SectionCreator();
//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);
//            var timer = new Stopwatch();

//            timer.Start();

//            solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);

//            timer.Stop();
//            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds);
//            Console.WriteLine("----------------------");

//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(1);

//            Specify.That(solution.Score)
//                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)100d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)300d);
//            //TODO: Longhead 2 default position is 200, and longhead 3 is 300, thus longhead 3 should be assigned to The second vertical line.

//            //TODO: The position of longhead that will not be used in the design is not updated.
//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)314d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[3]).Should.BeEqualTo((MicroMeter)500d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)700d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[5]).Should.BeEqualTo((MicroMeter)800d);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForDiffDistByOffSet_04160200_Design_CloseToLeftBoundry()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();

//            var design = GetPhysicalDesign(4160200, 270, 200, 20, 330);
//            var lines = LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(4160200, 270, 200, 20, 330);

//            var sectionCreator = new SectionCreator();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);

//            Console.WriteLine("The score of using LH 1, 2 and 3 : " +
//                              (EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay)
//                );
//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(1);

//            Specify.That(solution.Score)
//                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(110, machineSettings.LongHeadParameters) +
//                               EmScoreCalculator.CalculateMovementScoreForDistance(70, machineSettings.LongHeadParameters) +
//                               EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
//                               machineSettings.LongHeadParameters.LongheadConnectionDelay);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)(-34d));

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)20d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)196d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[3]).Should.BeEqualTo((MicroMeter)290d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)310d);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForDiffDistByOffSet_04160200_Design_CloseToRightBoundryButNoReverse()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();

//            var design = GetPhysicalDesign(4160200, 270, 200, 20, 330);
//            var lines = LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(4160200, 270, 200, 20, 330);

//            //Max position is 2464
//            var offSet = new MicroMeter(1900d);
//            lines.ForEach(ln =>
//            {
//                ln.StartCoordinate.X += offSet;
//                ln.EndCoordinate.X += offSet;
//            });

//            var sectionCreator = new SectionCreator();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);

//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(1);

//            Specify.That(solution.Score)
//                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(1620, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(90, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)1920d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[3]).Should.BeEqualTo((MicroMeter)2190d);

//            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)2210d);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForDiffDistByOffSet_04160200_Design_CloseToRightBoundryAndNeedReverse()
//        {
//            this.SetupLongHeads(6);
//            var algorithmEmOptimized = new EmLongHeadDistributor();

//            var design = GetPhysicalDesign(4160200, 270, 200, 20, 330);
//            var lines = LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(4160200, 270, 200, 20, 330);

//            //Max position is 2464, if longhead 5 was in 2420, longhead 6 would at least be 2420 + 54 = 2474 > 2464(max position)
//            var offSet = new MicroMeter(2110d);
//            lines.ForEach(ln =>
//            {
//                ln.StartCoordinate.X += offSet;
//                ln.EndCoordinate.X += offSet;
//            });

//            var sectionCreator = new SectionCreator();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);

//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Assert.Inconclusive("The long head distribution is not correct yet!");

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(1);

//            var firstIteration = solution.GetSolutionNodes().First().Iterations.First();
//            Specify.That(firstIteration.Count()).Should.BeEqualTo(2); //Two longhead need to move
//            Specify.That(firstIteration.First().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)2120d);
//            Specify.That(firstIteration.First().NewLongHeadPositions[5]).Should.BeEqualTo((MicroMeter)2410d);

//            var secondIteration = solution.GetSolutionNodes().First().Iterations.Last();
//            Specify.That(secondIteration.Count()).Should.BeEqualTo(2); //Two longhead need to move
//            Specify.That(secondIteration.First().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)2120d);
//            Specify.That(secondIteration.First().NewLongHeadPositions[5]).Should.BeEqualTo((MicroMeter)2390d);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        [Ignore]
//        //Result doesnt contain 2 sections as it should. 
//        public void ShouldReturnSolution_ForPaperJamDesign()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();
//            var design = GetPhysicalDesign(10221, 200, 200, 200, 1000);
//            var lines = LoadPhysicalDesignWithDimentions_AndApplyRulesOnIt(10221, 200, 200, 200, 1000);
//            //Design 10221: length is 1500

//            var sectionCreator = new SectionCreator();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);

//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(3);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        //[Ignore]
//        //Result doesnt contain 2 sections as it should. 
//        public void ShouldReturnSolution_ForReposAtEnd1Design_ShouldOptimaizeTheReverseLikePSAtTheEnd()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();
//            var design = GetPhysicalDesign(1515155, 200, 200, 100, 1000);
//            var lines = LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(1515155, 200, 200, 100, 1000);

//            var sectionCreator = new SectionCreator();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);

//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Assert.Inconclusive("The long head distribution is not correct yet!");

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);

//            Specify.That(solution.GetSolutionNodes().First().Iterations.Count()).Should.BeEqualTo(1);

//            var newLHPositions = solution.GetSolutionNodes().First().NewLongHeadPositions;

//            Specify.That(newLHPositions[1]).Should.BeEqualTo((MicroMeter)100);
//            Specify.That(newLHPositions[2]).Should.BeEqualTo((MicroMeter)120);
//            Specify.That(newLHPositions[3]).Should.BeEqualTo((MicroMeter)360);
//            Specify.That(newLHPositions[4]).Should.BeEqualTo((MicroMeter)380);
//            Specify.That(newLHPositions[5]).Should.BeEqualTo((MicroMeter)480);

//            Specify.That(solution.GetSolutionNodes().Last().Iterations.Count()).Should.BeEqualTo(2);

//            var newLHPositionsNode2 = solution.GetSolutionNodes().Last().NewLongHeadPositions;

//            Specify.That(newLHPositionsNode2[1]).Should.BeEqualTo((MicroMeter)100);
//            Specify.That(newLHPositionsNode2[2]).Should.BeEqualTo((MicroMeter)140);
//            Specify.That(newLHPositionsNode2[3]).Should.BeEqualTo((MicroMeter)340);
//            Specify.That(newLHPositionsNode2[4]).Should.BeEqualTo((MicroMeter)380);
//            Specify.That(newLHPositionsNode2[5]).Should.BeEqualTo((MicroMeter)480);

//            //TODO: Specification of the reverse, should reverse from the start coordinate of section 2.
//            Specify.That(solution.GetSolutionNodes().First().Iterations.Count()).Should.BeEqualTo(2);

//            var firstIteration = solution.GetSolutionNodes().Last().Iterations.First();
//            Specify.That(firstIteration.Count()).Should.BeEqualTo(2); // Move long head 2 and 4 to cut two lines first

//            var secondIteration = solution.GetSolutionNodes().Last().Iterations.Last();
//            // Reposition long head 2, 3, 4 and 5 to cut four lines, and continue use longhead 6 to for the last line.
//            Specify.That(secondIteration.Count()).Should.BeEqualTo(5);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForSkipTest_3310102_Design_ShouldOptimaizeTheReverseLikePSAtTheEnd()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();

//            var design = GetPhysicalDesign(3310102, 200, 200, 70, 400);
//            var lines = LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(3310102, 200, 200, 70, 400);

//            var offSet = new MicroMeter(2000d);
//            lines.ForEach(ln =>
//            {
//                ln.StartCoordinate.X += offSet;
//                ln.EndCoordinate.X += offSet;
//            });

//            var sectionCreator = new SectionCreator();

//            var timer = new Stopwatch();
//            timer.Start();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);
//            timer.Stop();
//            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds);
//            Console.WriteLine("----------------------");
//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(1);

//            Specify.That(solution.GetSolutionNodes().First().Iterations.Count()).Should.BeEqualTo(1);

//            var newLHPositions = solution.GetSolutionNodes().First().NewLongHeadPositions;

//            Specify.That(newLHPositions[1]).Should.BeEqualTo((MicroMeter)2050);
//            Specify.That(newLHPositions[2]).Should.BeEqualTo((MicroMeter)2070);
//            Specify.That(newLHPositions[3]).Should.BeEqualTo((MicroMeter)(2070 + 94));
//            Specify.That(newLHPositions[4]).Should.BeEqualTo((MicroMeter)2270);
//            Specify.That(newLHPositions[5]).Should.BeEqualTo((MicroMeter)2340);

//            Specify.That(solution.Score).Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(1740, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForReposAtEnd2Design_ShouldOptimaizeTheReverseLikePSAtTheEnd()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();
//            var design = GetPhysicalDesign(4730100, 200, 200, 100, 1000);
//            var lines = LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(4730100, 200, 200, 100, 1000);

//            var sectionCreator = new SectionCreator();

//            var timer = new Stopwatch();
//            timer.Start();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);
//            timer.Stop();
//            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds);
//            Console.WriteLine("----------------------");
//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Assert.Inconclusive("The long head distribution is not correct yet!");

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);

//            Specify.That(solution.GetSolutionNodes().First().Iterations.Count()).Should.BeEqualTo(1);

//            var newLHPositions = solution.GetSolutionNodes().First().NewLongHeadPositions;

//            Specify.That(newLHPositions[1]).Should.BeEqualTo((MicroMeter)100);
//            Specify.That(newLHPositions[2]).Should.BeEqualTo((MicroMeter)120);
//            Specify.That(newLHPositions[3]).Should.BeEqualTo((MicroMeter)360);
//            Specify.That(newLHPositions[4]).Should.BeEqualTo((MicroMeter)380);
//            Specify.That(newLHPositions[5]).Should.BeEqualTo((MicroMeter)480);

//            Specify.That(solution.GetSolutionNodes().Last().Iterations.Count()).Should.BeEqualTo(2);

//            var newLHPositionsNode2 = solution.GetSolutionNodes().Last().NewLongHeadPositions;

//            Specify.That(newLHPositionsNode2[1]).Should.BeEqualTo((MicroMeter)100);
//            Specify.That(newLHPositionsNode2[2]).Should.BeEqualTo((MicroMeter)140);
//            Specify.That(newLHPositionsNode2[3]).Should.BeEqualTo((MicroMeter)340);
//            Specify.That(newLHPositionsNode2[4]).Should.BeEqualTo((MicroMeter)380);
//            Specify.That(newLHPositionsNode2[5]).Should.BeEqualTo((MicroMeter)480);

//            //TODO: Specification of the reverse, should reverse from the start coordinate of section 2.
//            Specify.That(solution.GetSolutionNodes().First().Iterations.Count()).Should.BeEqualTo(2);

//            var firstIteration = solution.GetSolutionNodes().Last().Iterations.First();
//            Specify.That(firstIteration.Count()).Should.BeEqualTo(2); // Move long head 2 and 4 to cut two lines first

//            var secondIteration = solution.GetSolutionNodes().Last().Iterations.Last();
//            // Reposition long head 2, 3, 4 and 5 to cut four lines, and continue use longhead 6 to for the last line.
//            Specify.That(secondIteration.Count()).Should.BeEqualTo(5);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForStrangeReverseAtEndDesign_ShouldNotHaveUneccessaryReverseAtTheEndLikePS()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();
//            var design = GetPhysicalDesign(4100300, 200, 200, 100, 1000);
//            var lines = LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(4100300, 200, 200, 100, 1000);

//            var sectionCreator = new SectionCreator();

//            var timer = new Stopwatch();
//            timer.Start();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);
//            timer.Stop();
//            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds);
//            Console.WriteLine("----------------------");
//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Assert.Inconclusive("The long head distribution is not correct yet!");

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);

//            Specify.That(solution.GetSolutionNodes().First().Iterations.Count()).Should.BeEqualTo(3);

//            var newLHPositions = solution.GetSolutionNodes().First().NewLongHeadPositions;

//            Specify.That(newLHPositions[1]).Should.BeEqualTo((MicroMeter)100);
//            Specify.That(newLHPositions[2]).Should.BeEqualTo((MicroMeter)300);
//            Specify.That(newLHPositions[3]).Should.BeEqualTo((MicroMeter)400);

//            //Specify.That(solution.GetSolutionNodes().Last().Iterations.Count()).Should.BeEqualTo();

//            //var newLHPositionsNode2 = solution.GetSolutionNodes().Last().NewLongHeadPositions;

//            //Specify.That(newLHPositionsNode2[1]).Should.BeEqualTo((MicroMeter)100);
//            //Specify.That(newLHPositionsNode2[2]).Should.BeEqualTo((MicroMeter)140);
//            //Specify.That(newLHPositionsNode2[3]).Should.BeEqualTo((MicroMeter)340);
//            //Specify.That(newLHPositionsNode2[4]).Should.BeEqualTo((MicroMeter)380);
//            //Specify.That(newLHPositionsNode2[5]).Should.BeEqualTo((MicroMeter)480);

//            ////TODO: Specification of the reverse, should reverse from the start coordinate of section 2.
//            //Specify.That(solution.GetSolutionNodes().First().Iterations.Count()).Should.BeEqualTo(2);

//            //var firstIteration = solution.GetSolutionNodes().Last().Iterations.First();
//            //Specify.That(firstIteration.Count()).Should.BeEqualTo(2); // Move long head 2 and 4 to cut two lines first

//            //var secondIteration = solution.GetSolutionNodes().Last().Iterations.Last();
//            //// Reposition long head 2, 3, 4 and 5 to cut four lines, and continue use longhead 6 to for the last line.
//            //Specify.That(secondIteration.Count()).Should.BeEqualTo(5);
//        }

//        [TestMethod]
//        [TestCategory("Integration")]
//        public void ShouldReturnSolution_ForDesign0415000()
//        {
//            this.SetupLongHeads(6);

//            var algorithmEmOptimized = new EmLongHeadDistributor();

//            var lines = NineVerticalLinesFromDesign0415_0900();

//            var design = new PhysicalDesign() { Length = 100 };
//            lines.ForEach(design.Add);

//            var sectionCreator = new SectionCreator();

//            var timer = new Stopwatch();
//            timer.Start();

//            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, currentPositions, lines, sectionCreator, new SectionEvaluator(), design);
//            timer.Stop();
//            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds);
//            Console.WriteLine("Number of reverses: " + solution.NumberOfReverses);
//            Console.WriteLine("----------------------");
//            foreach (var node in solution.GetSolutionNodes())
//            {
//                Console.WriteLine(node);
//                Console.WriteLine("----------------------");
//            }

//            Specify.That(solution).Should.Not.BeNull();

//            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(1);

//            var firstIteration = solution.GetSolutionNodes().First().Iterations.First();
//            Specify.That(firstIteration.Count()).Should.BeEqualTo(6);

//            Specify.That(firstIteration.First().NewLongHeadPositions[5]).Should.BeEqualTo((MicroMeter)775d);
//            Specify.That(firstIteration.Skip(1).First().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)600d);
//            Specify.That(firstIteration.Skip(2).First().NewLongHeadPositions[3]).Should.BeEqualTo((MicroMeter)500d);
//            Specify.That(firstIteration.Skip(3).First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)350d);
//            Specify.That(firstIteration.Skip(4).First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)175d);
//            Specify.That(firstIteration.Skip(5).First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)75d);

//            var secondIteration = solution.GetSolutionNodes().First().Iterations.Skip(1).First();
//            Specify.That(secondIteration.Count()).Should.BeEqualTo(3);

//            Specify.That(secondIteration.First().NewLongHeadPositions[5]).Should.BeEqualTo((MicroMeter)850d);
//            Specify.That(secondIteration.Skip(1).First().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)675d);
//            Specify.That(secondIteration.Skip(2).First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)250d);

//            Specify.That(solution.Score)
//                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(75, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(25, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay +
//                                  EmScoreCalculator.CalculateMovementScoreForDistance(75, machineSettings.LongHeadParameters) +
//                                  machineSettings.LongHeadParameters.LongheadConnectionDelay +
//                                  EmScoreCalculator.CalculateReverseScoreForDistance(100, 1, machineSettings.FeedRollerParameters));
//        }

        
//        private List<PhysicalLine> LoadPhysicalDesignWithDimentionsAndXValue_AndApplyRulesOnIt(int designId, double length, double width, double height, double zFoldWidth, IDictionary<string, double> xValues = null)
//        {
//            var testDataPath = Path.Combine(Environment.CurrentDirectory, "CodeGenTestData");

//            var designManager =
//                new PackagingDesignManager(testDataPath, new Mock<ICachingService>().Object,
//                    new Mock<ILogger>().Object);

//            designManager.LoadDesigns();

//            var designFormularPara = GetDesignFormularParameters(length, width, height, zFoldWidth);
//            if (xValues != null)
//            {
//                xValues.Where(xV => designFormularPara.XValues.Keys.Contains(xV.Key))
//                    .ForEach(xV => designFormularPara.XValues[xV.Key] = xV.Value);
//            }
//            var design = designManager.GetDesigns().FirstOrDefault(d => d.Id == designId).AsPhysicalDesign(designFormularPara);

//            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator();

//            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400)).Lines.GetVerticalLines().ToList();

//            linesAfterRuleIsApplied.RemoveAll(l => l.StartCoordinate.X == 0);

//            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();

//            return linesAfterRuleIsApplied;
//        }
//        private static IEnumerable<PhysicalLine> NineVerticalLinesFromDesign0415_0900()
//        {
//            return new List<PhysicalLine>
//                        {
//                            new PhysicalLine(
//                                new PhysicalCoordinate(75, 0),
//                                new PhysicalCoordinate(75, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(175, 0),
//                                new PhysicalCoordinate(175, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(250, 0),
//                                new PhysicalCoordinate(250, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(350, 0),
//                                new PhysicalCoordinate(350, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(500, 0),
//                                new PhysicalCoordinate(500, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(600, 0),
//                                new PhysicalCoordinate(600, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(675, 0),
//                                new PhysicalCoordinate(675, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(775, 0),
//                                new PhysicalCoordinate(775, 100),
//                                LineType.crease),
//                            new PhysicalLine(
//                                new PhysicalCoordinate(850, 0),
//                                new PhysicalCoordinate(850, 100),
//                                LineType.crease)
//                        };
//        }
//        private List<PhysicalLine> LoadPhysicalDesignWithDimentions_AndApplyRulesOnIt(int designId, double length, double width, double height, double zFoldWidth)
//        {

//            var design = GetPhysicalDesign(designId, length, width, height, zFoldWidth);

//            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator();

//            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400)).Lines.GetVerticalLines().ToList();

//            linesAfterRuleIsApplied.RemoveAll(l => l.StartCoordinate.X == 0);

//            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();

//            return linesAfterRuleIsApplied;
//        }



//        private void SetupLongHeads(int numberOfLongheads, double offset = 0)
//        {
//            currentPositions = new MicroMeter[numberOfLongheads];
//            for (var i = 0; i < numberOfLongheads; i++)
//            {
//                var lh = new EmLongHead() { Number = i + 1, Position = ((i + 1) * 100) + offset, LeftSideToTool = 47 };
//                if (lh.Number == 3 || lh.Number == 5 || lh.Number == 6 || lh.Number == 8 || lh.Number == 10)
//                {
//                    lh.LeftSideToTool = 7;
//                }
//                this.machineSettings.LongHeadParameters.AddLongHead(lh);
//                currentPositions[i] = lh.Position;
//            }

//        }

//        private class DesignCreator : EmMachineInstructionCreator
//        {
//            public DesignCreator(double minimumLineDistanceToCorrugateEdge, double perforationSafetyDistance)
//                : base(minimumLineDistanceToCorrugateEdge, perforationSafetyDistance)
//            {
//            }

//            public DesignCreator(CodeGeneratorSettings generatorSettings)
//                : base(generatorSettings)
//            {

//            }

//            public new PhysicalDesign CreateCompensatedDesign(PhysicalDesign design, Corrugate corrugate, bool isTrackRightSideFixed,
//            MicroMeter trackOffset, EmPhysicalMachineSettings machineSettings)
//            {
//                return base.CreateCompensatedDesign(design, corrugate, isTrackRightSideFixed, trackOffset, machineSettings);
//            }
//        }

//        private PhysicalDesign GetPhysicalDesign(int designId, double length, double width, double height, double zFoldWidth, IDictionary<string, double> xValues = null)
//        {
           

//            var testDataPath = Path.Combine(Environment.CurrentDirectory, "CodeGenTestData");

//            var designManager =
//                new PackagingDesignManager(testDataPath, new Mock<ICachingService>().Object,
//                    new Mock<ILogger>().Object);

//            designManager.LoadDesigns();

//            var designFormularPara = GetDesignFormularParameters(length, width, height, zFoldWidth);
//            if (xValues != null)
//            {
//                xValues.Where(xV => designFormularPara.XValues.Keys.Contains(xV.Key))
//                    .ForEach(xV => designFormularPara.XValues[xV.Key] = xV.Value);
//            }

//            return designManager.GetDesigns().FirstOrDefault(d => d.Id == designId).AsPhysicalDesign(designFormularPara);
//        }

//        private DesignFormulaParameters GetDesignFormularParameters(double length, double width, double height, double zFoldWidth)
//        {
//            var designFormularPara = new DesignFormulaParameters()
//            {
//                CartonLength = length,
//                CartonWidth = width,
//                CartonHeight = height,
//                ZfoldWidth = zFoldWidth,
//            };

//            designFormularPara.XValues.Add("X1", 20);
//            designFormularPara.XValues.Add("X2", 20);
//            designFormularPara.XValues.Add("X3", 20);
//            designFormularPara.XValues.Add("X4", 20);
//            designFormularPara.XValues.Add("X5", 20);
//            designFormularPara.XValues.Add("X6", 20);
//            designFormularPara.XValues.Add("X7", 20);
//            designFormularPara.XValues.Add("X8", 20);
//            designFormularPara.XValues.Add("X9", 20);
//            designFormularPara.XValues.Add("X10", 20);
//            designFormularPara.XValues.Add("X11", 20);
//            designFormularPara.XValues.Add("X12", 20);
//            designFormularPara.XValues.Add("X13", 20);
//            designFormularPara.XValues.Add("X14", 20);
//            designFormularPara.XValues.Add("X15", 20);
//            designFormularPara.XValues.Add("X16", 20);
//            designFormularPara.XValues.Add("X17", 20);
//            designFormularPara.XValues.Add("X18", 20);
//            designFormularPara.XValues.Add("X19", 20);
//            designFormularPara.XValues.Add("X20", 20);

//            return designFormularPara;
//        }

//    }
//    #endif
//}
