﻿using System;

using PackNet.Business.CodeGenerator;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace BusinessTests.CodeGenerator.AStarEmTests
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.AStarStuff;
    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;

    using Testing.Specificity;

    [TestClass]
    public class AStarNodeEmTests
    {
        private EmPhysicalMachineSettings machineSettings;

        private MicroMeter[] currentPositions;
        [TestInitialize]
        public void Setup()
        {
            this.currentPositions = new MicroMeter[4] { 0, 0, 0, 0 };
            this.machineSettings = new EmPhysicalMachineSettings();
            machineSettings.LongHeadParameters = new EmLongHeadParameters(new List<EmLongHead>(), 1.5, 90, 10, 55, 7, 2464, -50, 54, 143, 15d, 24d);
            machineSettings.FeedRollerParameters = new EmFeedRollerParameters(1.5, 30, 1, 55, 600, 45);       
        }

        private static IEnumerable<PhysicalLine> SetupLinesForSimpleDesign()
        {
            return new List<PhysicalLine>()
                        {
                            new PhysicalLine(
                                new PhysicalCoordinate(200, 0),
                                new PhysicalCoordinate(200, 600),
                                LineType.crease),
                            new PhysicalLine(
                                new PhysicalCoordinate(250, 0),
                                new PhysicalCoordinate(250, 600),
                                LineType.crease),
                            new PhysicalLine(
                                new PhysicalCoordinate(300, 0),
                                new PhysicalCoordinate(300, 600),
                                LineType.crease),
                            new PhysicalLine(
                                new PhysicalCoordinate(400, 0),
                                new PhysicalCoordinate(400, 600),
                                LineType.cut),
                        };
        }

        private IEnumerable<PhysicalLine> SetupLinesAt(IEnumerable<int> xStartValues)
        {
            var lines = new List<PhysicalLine>();

            xStartValues.ToList().ForEach(x => lines.Add(new PhysicalLine(new PhysicalCoordinate(x, 0), new PhysicalCoordinate(x, 600), LineType.crease)));

            return lines;
        }

        private void SetupLongheads(int numberOfLongheads)
        {
            currentPositions = new MicroMeter[numberOfLongheads];

            for (var i = 0; i < numberOfLongheads; i++)
            {

                var lh = new EmLongHead() { Number = i + 1, Position = (i + 1) * 100, LeftSideToTool = 47 };
                if (lh.Number == 3 || lh.Number == 5 || lh.Number == 6 || lh.Number == 8 || lh.Number == 10)
                {
                    lh.LeftSideToTool = 6;
                }
                this.currentPositions[i] = lh.Position;
                machineSettings.LongHeadParameters.AddLongHead(lh);
            }
        }
    
        [TestMethod]
        [TestCategory("Unit")]
        public void GetSuccessorsGeneratesFirstLayerCorrectly()
        {
            var lines = SetupLinesForSimpleDesign();
            SetupLongheads(4);

            var node = LongHeadDistributionEmTestHelper.GetStartNode(lines, currentPositions, machineSettings);
            var section = LongHeadDistributionEmTestHelper.CreateSectionFromLines(lines);
            var design = LongHeadDistributionEmTestHelper.CreateDesignFromLines(lines);
            var successorCreator = new EmSuccessorCreator();
            var successors = successorCreator.GetSuccessors(node, machineSettings, section, design);
            //var successors = node.GetSuccessors();

            Specify.That(successors.Count()).Should.BeEqualTo(4);

            Specify.That(successors.ElementAt(0).NewLongHeadPositions[0])
                .Should.BeLogicallyEqualTo(lines.ElementAt(0).StartCoordinate.X);

            Specify.That(successors.ElementAt(1).NewLongHeadPositions[1])
                .Should.BeLogicallyEqualTo(lines.ElementAt(0).StartCoordinate.X);

            Specify.That(successors.ElementAt(2).NewLongHeadPositions[2])
                .Should.BeLogicallyEqualTo(lines.ElementAt(0).StartCoordinate.X);

            Specify.That(successors.ElementAt(3).NewLongHeadPositions[3])
                .Should.BeLogicallyEqualTo(lines.ElementAt(0).StartCoordinate.X);
        }
        
        [TestMethod]
        [TestCategory("Unit")]
        public void GetSuccessorsGeneratesSecondLayerCorrectly()
        {
            var lines = SetupLinesForSimpleDesign();
            SetupLongheads(4);

            var node = LongHeadDistributionEmTestHelper.GetStartNode(lines, currentPositions, machineSettings);
            var section = LongHeadDistributionEmTestHelper.CreateSectionFromLines(lines);
            var design = LongHeadDistributionEmTestHelper.CreateDesignFromLines(lines);
            var successorCreator = new EmSuccessorCreator();

            var successorsFirstLayer = successorCreator.GetSuccessors(node, machineSettings, section, design);
            //var successorsFirstLayer = node.GetSuccessors();

            //var sucessorsSecondLayer = successorsFirstLayer.SelectMany(n => n.GetSuccessors());
            var sucessorsSecondLayer = successorsFirstLayer.SelectMany(n => successorCreator.GetSuccessors(n, machineSettings, section, design));

            Specify.That(sucessorsSecondLayer.Count()).Should.BeEqualTo(10);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void GetSuccessorsGeneratesThirdCorrectly()
        {
            var lines = SetupLinesForSimpleDesign();
            SetupLongheads(4);

            var node = LongHeadDistributionEmTestHelper.GetStartNode(lines, currentPositions, machineSettings);
            var section = LongHeadDistributionEmTestHelper.CreateSectionFromLines(lines);
            var design = LongHeadDistributionEmTestHelper.CreateDesignFromLines(lines);
            var successorCreator = new EmSuccessorCreator();
            var successorsFirstLayer = successorCreator.GetSuccessors(node, machineSettings, section, design);

            var sucessorsSecondLayer = successorsFirstLayer.SelectMany(n => successorCreator.GetSuccessors(n, machineSettings, section, design)).ToList();

            var sucessorsThirdLayer = sucessorsSecondLayer.SelectMany(n => successorCreator.GetSuccessors(n, machineSettings, section, design)).ToList();
            //var successorsFirstLayer = node.GetSuccessors();

            //var sucessorsSecondLayer = successorsFirstLayer.SelectMany(n => n.GetSuccessors()).ToList();

            //var sucessorsThirdLayer = sucessorsSecondLayer.SelectMany(n => n.GetSuccessors()).ToList();

            Specify.That(sucessorsThirdLayer.Count()).Should.BeEqualTo(20);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void GetSuccessorsDoesNotGenerateNewNodeWhenAllLinesAreAssignedWithLongHead()
        {
            var lines = SetupLinesForSimpleDesign();
            SetupLongheads(4);
            
            var startNode = LongHeadDistributionEmTestHelper.GetStartNode(lines, currentPositions, machineSettings);
            var section = LongHeadDistributionEmTestHelper.CreateSectionFromLines(lines);
            var design = LongHeadDistributionEmTestHelper.CreateDesignFromLines(lines);
            var successorCreator = new EmSuccessorCreator();

            var successorsFirstLayer = successorCreator.GetSuccessors(startNode, machineSettings, section, design);

            var sucessorsSecondLayer = successorsFirstLayer.SelectMany(firstLayerSuccessor => successorCreator.GetSuccessors(firstLayerSuccessor, machineSettings, section, design));

            var sucessorsFourthLayer = sucessorsSecondLayer.SelectMany(secondLayerSuccessor => successorCreator.GetSuccessors(secondLayerSuccessor, machineSettings, section, design))
                .SelectMany(thirdLayerSuccessor => successorCreator.GetSuccessors(thirdLayerSuccessor, machineSettings, section, design));

            var nonExistentLayerSuccessors = sucessorsFourthLayer.SelectMany(layer => successorCreator.GetSuccessors(layer, machineSettings, section, design));

            //var successorsFirstLayer = startNode.GetSuccessors();

            //var sucessorsSecondLayer = successorsFirstLayer.SelectMany(firstLayerSuccessor => firstLayerSuccessor.GetSuccessors());

            //var sucessorsFourthLayer = sucessorsSecondLayer.SelectMany(secondLayerSuccessor => secondLayerSuccessor.GetSuccessors()).SelectMany(thirdLayerSuccessor => thirdLayerSuccessor.GetSuccessors());

            //var nonExistentLayerSuccessors = sucessorsFourthLayer.SelectMany(layer => layer.GetSuccessors());

            Specify.That(nonExistentLayerSuccessors.Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnZeroScoreForStartingPosition()
        {
            var startNode = LongHeadDistributionEmTestHelper.GetStartNode(new List<PhysicalLine>() { new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 600), LineType.cut) }, 
                currentPositions, machineSettings);
            Specify.That(startNode.Score).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnZeroScore_WhenLongHeadIsAlreadyPositionedOnFirstCoordinate()
        {
            var coordinates = LongHeadDistributionEmTestHelper.SetupLinesAt(new[] { 700d });

            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(coordinates, new MicroMeter[] { 700, 1000, 1200 }, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + firstNode.Score);

            Specify.That(firstNode.Score).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnMovementScore_WhenLongHeadIsNotPositionedOnFirstCoordinate()
        {
            SetupLongheads(1);
            var coordinates = LongHeadDistributionEmTestHelper.SetupLinesAt(new[] { 50d });

            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(coordinates, new MicroMeter[] { 700, 1000, 1200 }, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + firstNode.Score);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(650, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPredictFourReverses_WhenUsingOneLonghead_OnFiveLines_AndLongHeadIsAlreadyPositionedOnFirstCoordinate()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new List<double> { 100, 105, 110, 115, 120 });
            var longHeadParams = new EmLongHeadParameters(new[] { new EmLongHead { LeftSideToTool = 47, Number = 1 } }, 1.5, 90, 10, 55, 7, 2464, 50, 54, 143, 15d, 24d);
            var feedRollerParameters = machineSettings.FeedRollerParameters;

            machineSettings = new EmPhysicalMachineSettings();
            machineSettings.LongHeadParameters = longHeadParams;
            machineSettings.FeedRollerParameters = feedRollerParameters;

            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, new MicroMeter[] { 100 }, longHeadParams, machineSettings);

            Console.WriteLine("Score: " + firstNode.Score);

            Specify.That(firstNode.Score).Should.BeEqualTo(
                EmScoreCalculator.CalculateReverseScoreForDistance(600, 4, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnMovementScoreAndPredictFourReverses_WhenUsingOneLonghead_OnFiveLines_AndLongHeadIsNotPositionedOnFirstCoordinate()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new List<double> { 100, 105, 110, 115, 120 });
            var longHeadParams = new EmLongHeadParameters(new[] { new EmLongHead() { LeftSideToTool = 47, Number = 1 } }, 1.5, 90, 10, 55, 7, 2464, 50, 54, 143, 15d, 24d);
            var feedRollerParameters = machineSettings.FeedRollerParameters;

            machineSettings = new EmPhysicalMachineSettings();
            machineSettings.LongHeadParameters = longHeadParams;
            machineSettings.FeedRollerParameters = feedRollerParameters;

            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, new MicroMeter[] { 50 }, longHeadParams, machineSettings);

            Console.WriteLine("Score: " + firstNode.Score);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, longHeadParams) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  (EmScoreCalculator.CalculateReverseScoreForDistance(600, 1,
                                      machineSettings.FeedRollerParameters)*4));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnThreeMovementScoreAndTwoReverseScore_AndPredictZeroReverses_WhenUsingOneLonghead_OnFiveLines_AndLineOneTwoThreeAreAlreadyProcessed()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new List<double> { 100, 200, 300, 400, 500 });
            var longHeadParams =
                new EmLongHeadParameters(
                    new[]
                    {
                        new EmLongHead() { LeftSideToTool = 47, Number = 1, Width = 54 },
                        new EmLongHead() { LeftSideToTool = 47, Number = 2, Width = 54 }
                    }, 1.5, 90, 10, 55, 7, 2464, 50, 54, 143, 15d, 24d);
            var feedRollerParameters = machineSettings.FeedRollerParameters;

            machineSettings = new EmPhysicalMachineSettings();
            machineSettings.LongHeadParameters = longHeadParams;
            machineSettings.FeedRollerParameters = feedRollerParameters;

            var lhPos = new MicroMeter[] { 50 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos));

            nodesInBranch.Add(LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, longHeadParams, machineSettings));
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, longHeadParams, machineSettings));
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 3, longHeadParams, machineSettings);

            Console.WriteLine("Score: " + thirdNode.Score);

            Specify.That(thirdNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, longHeadParams) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(100, longHeadParams) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(100, longHeadParams) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  (EmScoreCalculator.CalculateReverseScoreForDistance(600, 1,
                                      machineSettings.FeedRollerParameters)*2));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnMovementScoreAndPredictTwoReverses_WhenUsingTwoLonghead_OnFiveLines_AndLongHeadsAreNotPositionedOnCoordinate()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new List<double> { 100, 105, 110, 115, 120 });
            var longHeadParams = new EmLongHeadParameters(new[] { new EmLongHead() { LeftSideToTool = 47, Number = 1 }, new EmLongHead() { LeftSideToTool = 47, Number = 2 } }, 1.5, 90, 10, 55, 7, 2464, 50, 54, 143, 15d, 24d);
            var feedRollerParameters = machineSettings.FeedRollerParameters;

            machineSettings = new EmPhysicalMachineSettings();
            machineSettings.LongHeadParameters = longHeadParams;
            machineSettings.FeedRollerParameters = feedRollerParameters;

            var firstNode =LongHeadDistributionEmTestHelper.GetFirstNode(lines, new MicroMeter[] { 50 }, longHeadParams, machineSettings);

            Console.WriteLine("Score: " + firstNode.Score);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, longHeadParams) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  (EmScoreCalculator.CalculateReverseScoreForDistance(600, 1,
                                      machineSettings.FeedRollerParameters)*2));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnZeroMovementScore_WhenFirstIsPositionedOnFirstCoordinate_AndSecondLongHeadIsPositionedOnSecondCoordinate()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 700, 1000 });
            var lhPos = new MicroMeter[] { 700, 1000, 1200 };
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnMovementScore_AndReverseScoreBasedOnSectionLength()
        {
            SetupLongheads(2);

            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 700, 1000 });
            var lhPos = new MicroMeter[] { 700 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);

            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(300d, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                    EmScoreCalculator.CalculateReverseScoreForDistance(600d, 1, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnOneMovementScore_WhenFirstIsPositionedOnFirstCoordinate_AndSecondLongHeadIsNotPositionedOnSecondCoordinate()
        {
            SetupLongheads(2);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 700, 1100 });

            var lhPos = new MicroMeter[] { 700, 1000, 1200 };
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnOneMovementScore_WhenFirstIsNotPositionedOnFirstCoordinate_AndSecondLongHeadIsPositionedOnSecondCoordinate()
        {
            SetupLongheads(2);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 500, 1000 });

            var lhPos = new MicroMeter[] { 700, 1000, 1200 };
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(200, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnLongestMovementScore_WhenNoLongHeadIsInPostioned_AndBothArePositionedInTheSameDirection()
        {
            SetupLongheads(2);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 200 });
            var lhPos = new MicroMeter[] { 100, 1000, 1200 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(800, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementScores_WhenNoLongHeadIsInPostioned_AndEachLongHeadIsPositionedInTheDifferentDirection()
        {
            SetupLongheads(2);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 600, 1200 });
            var lhPos = new MicroMeter[] { 700, 1000, 1200 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateMovementScoreForDistance(200, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementScoresAndOneReverse_WhenSameLongHeadIsUsedForSecondCoordinate()
        {
            SetupLongheads(1);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 600, 650 });
            var lhPos = new MicroMeter[] { 700, 1000, 1200 };

            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                    EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters));

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementScoresAndOneReverse_WhenFirstLongHeadCannotBeUsedForFirstCoordinate_AtTheSameTimeAsSecondLongHeadIsUsedForSecondCoordinate()
        {
            SetupLongheads(2);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 600, 650 });
            var lhPos = new MicroMeter[] { 700, 1000, 1200 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateMovementScoreForDistance(350, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementScoresAndOneReverse_WhenFirstLongHeadCannotBeUsedForFirstCoordinate_AtTheSameTimeAsThirdLongHeadIsUsedForSecondCoordinate()
        {
            SetupLongheads(3);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 600, 660 });
            var lhPos = new MicroMeter[] { 700, 1000, 1200 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 2, machineSettings.LongHeadParameters, machineSettings);

            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateMovementScoreForDistance(540, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementScoresAndOneReverse_WhenFirstLongHeadCannotBeUsedForFirstCoordinate_AtTheSameTimeAsThirdLongHeadIsUsedForSecondCoordinate_DueToUnusedLongHead2Between()
        {
            SetupLongheads(3);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 600, 660 });
            var lhPos = new MicroMeter[] { 700, 800, 1000 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 2, machineSettings.LongHeadParameters, machineSettings);
            Console.WriteLine("Score: " + secondNode.Score);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateMovementScoreForDistance(340, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldProcessLinesInOrder_SoThatNumberOfIterationsIsMinimized()
        {
            SetupLongheads(4);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 110, 150, 160, 300, 310, 390, 400 });
            var lhPos = new MicroMeter[] { 0, 100, 200, 340 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings));
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings));
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 4, machineSettings.LongHeadParameters, machineSettings));
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 5, machineSettings.LongHeadParameters, machineSettings));
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 6, machineSettings.LongHeadParameters, machineSettings));
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 4, 7, machineSettings.LongHeadParameters, machineSettings));
            var eighthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 4, 8, machineSettings.LongHeadParameters, machineSettings);

            Console.WriteLine("Score: " + eighthNode.Score);

            Specify.That(eighthNode.NumberOfReverses).Should.BeEqualTo(2);
            Specify.That(eighthNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(90, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 2, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementScoresAndOneReverse_ForTwoLongHeadsAndFourLines_WhenDistanceBetweenLinesAreLessThanLongHeadDistance()
        {
            SetupLongheads(4);
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 130, 160, 190 });
            var lhPos = new MicroMeter[] { 110, 200 };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings));
            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings));
            var fourthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 4, machineSettings.LongHeadParameters, machineSettings);

            Console.WriteLine("Fourth Node Score: " + fourthNode.Score);

            Specify.That(fourthNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
                    EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds
                    + EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters));
        }
    }
}
