﻿using BusinessTests.PackagingDesigns;

using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace BusinessTests.CodeGenerator.AStarEmTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.AStarStuff;
    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;
    using System.Diagnostics;

    using Moq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Common.Interfaces;

    [TestClass]
    [DeploymentItem("..\\..\\CodeGenerator\\AStarEmTests\\TestData", "CodeGenTestData")]
    public class AStarAlgorithmEMOptimizedTests
    {
        private const double lhOffset = 147;
        private EmPhysicalMachineSettings machineSettings;
        private MicroMeter[] originalPositions;

        [TestInitialize]
        public void Setup()
        {
            machineSettings = new EmPhysicalMachineSettings();

            machineSettings.LongHeadParameters = new EmLongHeadParameters(new List<EmLongHead>(), 1500, 90, 10, 55, 7, 2464, -50, 54, 143, 15d, 24d);
            machineSettings.FeedRollerParameters = new EmFeedRollerParameters(1500, 30, 1, 55, 600, 45);           
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnTwoLongHeadDistributions_DesignContainsTwoLinesOnDifferent_XAndY_Coordinates()
        {
            SetupLongHeads(1);
            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 300), new PhysicalCoordinate(100, 600), LineType.crease));

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, new SectionCreator(lhOffset), new SectionEvaluator(), design);
            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(2);
            Specify.That(solution.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters)*2 +
                    2 * machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            var sn1 = solution.GetSolutionNodes().ElementAt(0);
            Specify.That(sn1).Should.Not.BeNull();
            Specify.That(sn1.Score)
                .Should.BeLogicallyEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(50d, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            var sn2 = solution.GetSolutionNodes().ElementAt(0);
            Specify.That(sn2).Should.Not.BeNull();
            Specify.That(sn2.Score)
                .Should.BeLogicallyEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(50d, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            Specify.That(solution.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters)*2 +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds * 2);

        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnOneLongHeadDistributions_DesignContainsTwoLinesOnDifferent_XAndY_Coordinates_AndLinesCanBeProcessedAtTheSameTime()
        {
            this.SetupLongHeads(2);
            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(500, 300), new PhysicalCoordinate(500, 600), LineType.crease));

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, new SectionCreator(lhOffset), new SectionEvaluator(), design);
            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(solution.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(200, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }
        
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotUseLongheadOnLineItsPositionedOn_IfSolutionWouldBeLessOptimal()
        {
            /*
             *      [70]       [200]       [300] 
             *      LH1         LH2         LH3        
             *       .           .           .
             *     |    |        |
             *     |    |        |
             *     |    |        |
             *   [50] [110]    [200]
             *   
             *      These three lines can be produced in one pass without reverses so LH2 should
             *      not be used on line 3 even though it's already placed on it.
             */

            SetupLongHeads(3);

            var design = this.SetupDesignWithLinesAtCoordinates(new List<double> { 50, 110, 200 });

            var originalPositions = new MicroMeter[] { 70, 200, 300 };
            var algorithmEmOptimized = new EmLongHeadDistributor();
            var sectionCreator = new SectionCreator(lhOffset);
            var sectionEvaluator = new SectionEvaluator();
            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, sectionCreator, sectionEvaluator, design);
            var timer = new Stopwatch();
            timer.Start();
            solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, sectionCreator, sectionEvaluator, design);
            timer.Stop();
            Console.WriteLine("Runtime for solution: " + timer.ElapsedMilliseconds);
            Console.WriteLine("----------------------");
            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            Specify.That(solution).Should.Not.BeNull();
            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(solution.GetSolutionNodes().ElementAt(0).NumberOfReverses).Should.BeLogicallyEqualTo(0);
            Specify.That(solution.GetSolutionNodes().ElementAt(0).Score).
                Should.BeLogicallyEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(20d, machineSettings.LongHeadParameters)
                    + EmScoreCalculator.CalculateMovementScoreForDistance(70d, machineSettings.LongHeadParameters)
                    + EmScoreCalculator.CalculateMovementScoreForDistance(10d, machineSettings.LongHeadParameters)
                    + 1 * machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldMoveLongheadThreeTimes_WithTenMmDistanceAndReverseTwice_WhenUsingOneLongHeadForThreeLines()
        {
            SetupLongHeads(1);

            var algorithmEmOptimized = new EmLongHeadDistributor();
            var sectionCreator = new SectionCreator(lhOffset);
            var sectionEvaluator = new SectionEvaluator();

            var leftSideDesign = SetupDesignWithLinesAtCoordinates(new[] { 110d, 120d, 130d });

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, sectionCreator,
                sectionEvaluator, leftSideDesign);


            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(1);
            Specify.That(solution.GetSolutionNodes().ElementAt(0).NumberOfReverses).Should.BeEqualTo(2);

            Specify.That(solution.GetSolutionNodes().ElementAt(0).Iterations.ElementAt(0).Last().NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(110d);
            Specify.That(solution.GetSolutionNodes().ElementAt(0).Iterations.ElementAt(1).Last().NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(120d);
            Specify.That(solution.GetSolutionNodes().ElementAt(0).Iterations.ElementAt(2).Last().NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(130d);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldBaseScoringForReversesOnSectionLength()
        {
            this.SetupLongHeads(1);

            var algorithmEmOptimized = new EmLongHeadDistributor();
            var sectionCreator = new SectionCreator(lhOffset);
            var sectionEvaluator = new SectionEvaluator();

            var leftSideDesign = SetupDesignWithLinesAtCoordinates(new[] { 110d, 120d, 130d }, 300);

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                sectionCreator, sectionEvaluator, leftSideDesign);

            var solutionNodes = solution.GetSolutionNodes();
            Specify.That(solutionNodes.Count()).Should.BeEqualTo(1);
            var solutionNode = solutionNodes.ElementAt(0);

            Console.WriteLine(solutionNode);
            Console.WriteLine("----------------------");

            Specify.That(solutionNode.NumberOfReverses).Should.BeEqualTo(2);
            Specify.That(solutionNode.Score)
                .Should.BeEqualTo((EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) * 3) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(300, 2, machineSettings.FeedRollerParameters));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void
            ShouldUseFirstThreeLongHeadsOnLeftSideOfMachine_AndLastThreeOnRightSideOfMachine_WhenProducingRscOnAlternatingTracks()
        {
            SetupLongHeads(new List<double>() { 150, 250, 350, 900, 1100, 1200 });

            var algorithmEmOptimized = new EmLongHeadDistributor();
       
            var leftSideRsc = SetupRscWithOffset(0);
            var rightSideRsc = SetupRscWithOffset(1500);

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), leftSideRsc);
            var solutionNodes = solution.GetSolutionNodes();
            Specify.That(solutionNodes.Count()).Should.BeEqualTo(1);

            var node = solutionNodes.ElementAt(0);
            Console.WriteLine(node);
            Console.WriteLine("----------------------");
            Specify.That(node.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(100);
            Specify.That(node.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(300);
            Specify.That(node.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(400);
            //Remaining should have the original position
            Specify.That(node.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(900);
            Specify.That(node.NewLongHeadPositions[4]).Should.BeLogicallyEqualTo(1100);
            Specify.That(node.NewLongHeadPositions[5]).Should.BeLogicallyEqualTo(1200);

            SetLongHeadPositions(node.NewLongHeadPositions);

            solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, node.NewLongHeadPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), rightSideRsc);
            solutionNodes = solution.GetSolutionNodes();
            Specify.That(solutionNodes.Count()).Should.BeEqualTo(1);
            node = solutionNodes.ElementAt(0);

            Console.WriteLine(node);
            Console.WriteLine("----------------------");
            //First 3 should have original position
            Specify.That(node.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(100);
            Specify.That(node.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(300);
            Specify.That(node.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(400);
            //Last 3 should be used to process the lines
            Specify.That(node.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(1600);
            Specify.That(node.NewLongHeadPositions[4]).Should.BeLogicallyEqualTo(1800);
            Specify.That(node.NewLongHeadPositions[5]).Should.BeLogicallyEqualTo(1900);
        }

        private void SetLongHeadPositions(MicroMeter[] newLongHeadPositions)
        {
            for (int i = 0; i < Math.Min(newLongHeadPositions.Count(), machineSettings.LongHeadParameters.LongHeads.Count()); i++)
            {
                machineSettings.LongHeadParameters.LongHeads.ElementAt(i).Position = newLongHeadPositions.ElementAt(i);
            }
        }

        private RuleAppliedPhysicalDesign SetupDesignWithLinesAtCoordinates(IEnumerable<double> xStartValues, int length = 600)
        {
            var design = new RuleAppliedPhysicalDesign() { Length = length };
            xStartValues.ToList().ForEach(x => design.Add(new PhysicalLine(new PhysicalCoordinate(x, 0), new PhysicalCoordinate(x, length), LineType.cut)));
            return design;
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnSolutionBasedOnIndividualSections_IfCombinedCostIsLowerThanProducingAllLinesAsOneSection()
        {
            SetupLongHeads(3);
            var algorithmEmOptimized = new EmLongHeadDistributor();
            var design = this.SetupDesignForOffsetCreases();

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, new SectionCreator(lhOffset),
                new SectionEvaluator(), design);
            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            
            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);

            Specify.That(solution.Score).Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(1, machineSettings.LongHeadParameters) // Movement LH2 + LH3 -->
                                                        + EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) // Movement LH2 2 -->
                                                        + machineSettings.LongHeadParameters.ConnectDelayOffInSeconds * 3
                                                        + EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters)); // Movement LH3  <--
        }

        [TestMethod]
        [TestCategory("Integration")]
        [Ignore] //The algorithm to process line 2 first in the second section is not implemented yet.
        public void Should_OnlySplit_Sections_ThatContain_ReverseMovements()
        {
            SetupLongHeads(1);

            var sectionCreator = new SectionCreator(lhOffset);

            var design = SetupDesignWithLinesAtCoordinates(new double[] { 50, 100 }, 1000);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, sectionCreator, new SectionEvaluator(), design);

            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);
            Specify.That(solution.NumberOfReverses).Should.BeEqualTo(2);

            Specify.That(solution.GetSolutionNodes().First().Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)100);

            Specify.That(solution.GetSolutionNodes().First().NumberOfReverses).Should.BeEqualTo(1);

            Specify.That(solution.GetSolutionNodes().Last().Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateReverseScoreForDistance(400, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)50);

            Specify.That(solution.GetSolutionNodes().First().NumberOfReverses).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnReverse_IfOneLongHeadNeedToCutTwoVerticalLine()
        {
            SetupLongHeads(1);
            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = SetupDesignWithLinesAtCoordinates(new double[] { 200, 300 }, 600);

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            
            Specify.That(solution.NumberOfReverses).Should.BeEqualTo(1);
        }


        [TestMethod]
        [TestCategory("Integration")]
        public void Should_ReturnTheFirst_SolutionWithNoReverses_InsteadOf_Evaluating_All_Solutions()
        {
            SetupLongHeads(3);

            var design = new RuleAppliedPhysicalDesign { Length = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 0), new PhysicalCoordinate(250, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 200), new PhysicalCoordinate(50, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 200), new PhysicalCoordinate(150, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 200), new PhysicalCoordinate(250, 400), LineType.cut));

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            Specify.That(solution).Should.Not.BeNull();

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(1);

            Specify.That(solution.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnOptimalSolution_IfTwoLinesWith14mmHorizontalGapCanBeCreasedAtTheSameTime()
        {
            // Given 3 longheads and 8 sections like this
            // should be the best solution
            /*  |-------------------------| <- initial positioning of LH1  - LH3
             *  |   |     |               |    Track offset: 100                      
             *  |   |         |           |    Line Position: 100, 200, 215                      
             *  |   |     |               |                          
             *  |   |         |           |                          
             *  |   |     |               |                          
             *  |   |         |           |
             *  |   |     |               |
             *  |   |         |           |
             */

            SetupLongHeads(3);
            var algorithmEmOptimized = new EmLongHeadDistributor();
            var design = this.SetupLinesWith14mmGapInBetween();

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions, new SectionCreator(lhOffset),
                new SectionEvaluator(), design);

            Console.WriteLine("---------Track Offset: 100mm-----------");
            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(2);

            //Minimum score = score of longhead 3 from 300 move to 215.

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)100d);
            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)200d);
            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)215d);

            Specify.That(solution.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(85, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnOptimalSolution_IfAllSectionsCanCombinedAsOneWithOneReverse()
        {
            this.SetupLongHeads(5);
            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = GetFourSectionsWithFiveVerticalLines();

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);
            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Console.WriteLine(solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(2);
            Specify.That(solution.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [Ignore]
        //Test is randomly failing, we need to modify it, probably shares states with other test
        public void ShouldReturnOptimalSolution_IfFourSectionsAreGeneratedWithoutRevers()
        {
            //todo:non deterministic test on my machine
            SetupLongHeads(3);
            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = GetFourSectionsWithFiveVerticalLines();

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            originalPositions.ForEach(p => Console.WriteLine(p));

            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Console.WriteLine(solution.Score);
            Console.WriteLine("----------------------");

            Console.WriteLine("----------Score for 200 movement is : {0}", EmScoreCalculator.CalculateMovementScoreForDistance(200, machineSettings.LongHeadParameters));

            Console.WriteLine("----------Score for 50 reverse is : {0}", EmScoreCalculator.CalculateReverseScoreForDistance(50, 1, machineSettings.FeedRollerParameters));

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(4);

            Specify.That(solution.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(150, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(200, machineSettings.LongHeadParameters) * 3 +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds * 3);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldUpdateLeftLH_WhenItBlocksTheMovementOfOtherLH()
        {
            this.SetupLongHeads(2);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = new RuleAppliedPhysicalDesign { Length = 200 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(151, 0), new PhysicalCoordinate(151, 200), LineType.cut));

            var solution = algorithmEmOptimized.DistributeLongHeads(this.machineSettings, this.originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Console.WriteLine(solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(1);

            Specify.That(solution.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(49, machineSettings.LongHeadParameters) +
                machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)97d);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)151d);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldUpdateRightLH_WhenItBlocksTheMovementOfOtherLH()
        {
            SetupLongHeads(2);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = new RuleAppliedPhysicalDesign { Length = 200 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(149, 0), new PhysicalCoordinate(149, 200), LineType.cut));

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Console.WriteLine(solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(1);

            Specify.That(solution.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(49, machineSettings.LongHeadParameters) +
                machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)149d);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)203d);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldUpdateRightTwoLHs_WhenItBlocksTheMovementOfOtherLH()
        {
            SetupLongHeads(3);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = new RuleAppliedPhysicalDesign { Length = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(215, 0), new PhysicalCoordinate(215, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(149, 200), new PhysicalCoordinate(149, 400), LineType.cut));

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Console.WriteLine(solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(solution.GetSolutionNodes().First().Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(85, machineSettings.LongHeadParameters) +
                machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)100d);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)200d);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)215d);

            //TODO: If move LH2 and LH3 the same distance as LH1, that means no extra delay will be added, and in this scenario is the optimal.
                  
            Specify.That(solution.GetSolutionNodes().ElementAt(1).Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(49, machineSettings.LongHeadParameters) +
                machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            Specify.That(solution.GetSolutionNodes().ElementAt(1).NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)149d);

            Specify.That(solution.GetSolutionNodes().ElementAt(1).NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)203d);
            //TODO: The position of longhead 3 can be optimized
            Specify.That(solution.GetSolutionNodes().ElementAt(1).NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)217d);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldUpdateLeftTwoLHs_WhenItBlocksTheMovementOfOtherLH()
        {
            this.SetupLongHeads(4);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = new RuleAppliedPhysicalDesign { Length = 400 };
            design.Add(new PhysicalLine( new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(215, 0), new PhysicalCoordinate(215, 200), LineType.cut));
            design.Add(new PhysicalLine( new PhysicalCoordinate(315, 0), new PhysicalCoordinate(315, 200), LineType.cut));
            design.Add(new PhysicalLine( new PhysicalCoordinate(300, 200), new PhysicalCoordinate(300, 400), LineType.cut));

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            foreach (var node in solution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }
            Console.WriteLine(solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(solution.GetSolutionNodes().First().Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(85, machineSettings.LongHeadParameters) +
                machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)100d);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)200d);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)215d);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[3]).Should.BeEqualTo((MicroMeter)315d);
            //TODO: If move LH2 and LH3 the same distance as LH4, that means no extra delay will be added, and in this scenario is the optimal.

            Specify.That(solution.GetSolutionNodes().ElementAt(1).Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(15, machineSettings.LongHeadParameters) +
                machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);

            Specify.That(solution.GetSolutionNodes().ElementAt(1).NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)100d);

            Specify.That(solution.GetSolutionNodes().ElementAt(1).NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)192d);

            Specify.That(solution.GetSolutionNodes().ElementAt(1).NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)206d);

            Specify.That(solution.GetSolutionNodes().ElementAt(1).NewLongHeadPositions[3]).Should.BeEqualTo((MicroMeter)300d);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnOptimalSolution_FirstSectionShouldMergedWithSecondLastSection()
        {
            SetupLongHeads(3);
            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = new RuleAppliedPhysicalDesign { Length = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(150, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 50), new PhysicalCoordinate(250, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(350, 150), new PhysicalCoordinate(350, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(450, 200), new PhysicalCoordinate(450, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 250), new PhysicalCoordinate(50, 300), LineType.crease));

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                 new SectionCreator(14d), new SectionEvaluator(), design);

            var nodes = solution.GetSolutionNodes();
            Console.WriteLine("Number of nodes " + nodes.Count());
            foreach (var node in nodes)
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(solution.Score).Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                                          machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                                          EmScoreCalculator.CalculateMovementScoreForDistance(200, machineSettings.LongHeadParameters) +
                                                          machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(solution.GetSolutionNodes().First().Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)50);
            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)150);
            Specify.That(solution.GetSolutionNodes().First().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)250);

            Specify.That(solution.GetSolutionNodes().ElementAt(1).Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(200, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)50);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)350);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)450);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_PickTheOptimalCombinationOfSections()
        {
            // Given 4 longheads and 5 sections this
            // should be the best solution
            /*  |------------------------| <- initial positioning of LH1 - LH4
             *  |   |                    |                          
             *  |   |                    |                          
             *  |        |               |                          
             *  |        |               |                          
             *  |            |           |                          
             *  |            |           |
             *  |                |       |
             *  |                |       |
             *  |------------------------| <- reposition goes here  
             *  |                    |   | 
             *  |                    |   |                   
             *  |------------------------|
             */
            SetupLongHeads(4);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = SetupDesignWithXSections(5);

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            var nodes = solution.GetSolutionNodes();

            Console.WriteLine("Number of nodes " + nodes.Count());
            foreach (var node in nodes)
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            Console.WriteLine("Solution score: " + solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);

            Specify.That(solution.Score).Should.BeLogicallyEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters)
                                                                   + machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_PickTheOptimalCombinationOfSections_TwoMergedSectionsWillBeGenerated()
        {
            // Given 4 longheads and random sections from 4 to 12
            // should return the best solution
            /*  |-----------------------------------| <- initial positioning of LH1 - LH4
             *  |   |                               |                          
             *  |   |                               |                          
             *  |        |                          |                          
             *  |        |                          |                          
             *  |            |                      |                          
             *  |            |                      |
             *  |                |                  |
             *  |                |                  |
             *  |-----------------------------------| <- reposition goes here  
             *  |                    |              | 
             *  |                    |              |                   
             *  |                       |           | 
             *  |                       |           | 
             *  |                           |       | 
             *  |                           |       |         
             *  |                               |   | 
             *  |                               |   |                                
             *  |-----------------------------------|
             */

            SetupLongHeads(4);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var random = new Random();
            var linesCount = random.Next(5, 8);
            var design = SetupDesignWithXSections(linesCount);

            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            var nodes = solution.GetSolutionNodes();

            foreach (var node in nodes)
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            Console.WriteLine("Solution score: " + solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);

            Specify.That(solution.GetSolutionNodes().First().Score).Should.BeLogicallyEqualTo(0);

            Specify.That(solution.Score)
                .Should.BeLogicallyEqualTo(machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                           EmScoreCalculator.CalculateMovementScoreForDistance((linesCount - 4) * 100,
                                               machineSettings.LongHeadParameters));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldFixAShitloadOfSectionsUnder1S_WhenNumberOfLinesEqualToNumberOfLHs()
        {
            SetupLongHeads(10);

            var algorithmEmOptimized = new EmLongHeadDistributor();

            var design = SetupDesignWithXSections(10);

            var timer = new Stopwatch();

            LongHeadDistributionSolution solution = null;
            IEnumerable<IAStarNodeEM> nodes = null;

            timer.Start();

            for (int i = 0; i < 10; i++)
            {
                solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                    new SectionCreator(lhOffset), new SectionEvaluator(), design);

                nodes = solution.GetSolutionNodes();
            }

            timer.Stop();

            Console.WriteLine("\n\n\nRunTime in ms: {0}\n\n\n", timer.ElapsedMilliseconds / 10);

            Console.WriteLine("Number of nodes " + nodes.Count());
            foreach (var node in nodes)
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            Console.WriteLine("Solution score: " + solution.Score);
            Console.WriteLine("----------------------");

            Specify.That(nodes).Should.Not.BeNull();

            Specify.That(solution.Score).Should.BeEqualTo(0d);

            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[0]).Should.BeEqualTo((MicroMeter)100d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[1]).Should.BeEqualTo((MicroMeter)200d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[2]).Should.BeEqualTo((MicroMeter)300d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[3]).Should.BeEqualTo((MicroMeter)400d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[4]).Should.BeEqualTo((MicroMeter)500d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[5]).Should.BeEqualTo((MicroMeter)600d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[6]).Should.BeEqualTo((MicroMeter)700d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[7]).Should.BeEqualTo((MicroMeter)800d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[8]).Should.BeEqualTo((MicroMeter)900d);
            Specify.That(solution.GetSolutionNodes().Last().NewLongHeadPositions[9]).Should.BeEqualTo((MicroMeter)1000d);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnSolutionBasedOnIndividualSection_ForDesignWithManySectionsAsWell()
        {
            //TODO: Make sure current position is updated AH
            SetupLongHeads(3);
            var algorithmEmOptimized = new EmLongHeadDistributor();
            var design = this.SetupLinesWithFourSections();
            var solution = algorithmEmOptimized.DistributeLongHeads(machineSettings, originalPositions,
                new SectionCreator(lhOffset), new SectionEvaluator(), design);

            var nodes = solution.GetSolutionNodes();
            Console.WriteLine("Number of nodes " + nodes.Count());
            foreach (var node in nodes)
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            Specify.That(solution.GetSolutionNodes().Count()).Should.BeEqualTo(2);

            Specify.That(solution.GetSolutionNodes().First().Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);  // Movement LH1+LH2 -->

            Specify.That(solution.GetSolutionNodes().Skip(1).First().Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(25, this.machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAdjustLongheadPositionIfNeeded_WhenOneLonghead_IsUnplacedBetweenMinPosAndFirstPlaced()
        {
            var origPos = new MicroMeter[] { 200, 150 };
            var lhParams = GetLongheadParamsWithLongheadsAtPositions(origPos);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMock = new Mock<IAStarNodeEM>();
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(2));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[1]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(
                solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(96d, "Longhead 1 was not moved to the correct position");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(150d, "Longhead 2 was on a valid position but was moved anyway");

            origPos = new MicroMeter[] { 150, 250 };
            lhParams = GetLongheadParamsWithLongheadsAtPositions(origPos);
            emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);

            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(2));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[1]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(
                solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(150d, "Longhead 1 was on a valid position but was moved anyway");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(250d, "Longhead 2 was on a valid position but was moved anyway");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAdjustFirstLongheadPositionAsLittleAsPossible_WhenTwoLongheads_AreUnplacedBetweenMinPosAndFirstPlaced_AndFirstLongheadFitsInFrontOfSecond()
        {    
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMock = new Mock<IAStarNodeEM>();
            var origPos = new MicroMeter[] { 100, 90, 300 };
            var lhParams = GetLongheadParamsWithLongheadsAtPositions(origPos);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[2]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(
                solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(36d, "Longhead 1 was on an invalid position and had to be moved but was not moved to the correct position");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(90d, "Longhead 2 was on a valid position but was moved anyway");
            Specify.That(origPos[2]).Should.BeLogicallyEqualTo(300d, "Longhead 3 was on a valid position but was moved anyway");

            origPos = new MicroMeter[] { 100, 310, 300 };
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[2]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(
                solutionNodeMock.Object);
            
            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(100d, "Longhead 1 was on a valid position but was moved anyway");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(286d, "Longhead 2 was on an invalid position and had to be moved but was not moved to the correct position");
            Specify.That(origPos[2]).Should.BeLogicallyEqualTo(300d, "Longhead 3 was on a valid position but was moved anyway");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAdjustSecondAndFirstLongheadPositionsAsLittleAsPossible_WhenTwoLongheadsAreUnplacedBetweenMinPosAndFirstPlaced_AndFirstCantBePlacedInFrontOfSecond()
        {
            var lhParams = GetLongheadParamsWithLongheads(3);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMock = new Mock<IAStarNodeEM>();

            var origPos = new MicroMeter[] { 100, 0, 300 };
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[2]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(100d, "Longhead 1 was on a valid position but was moved anyway");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(154d, "Longhead 2 was not moved the minimum distance needed");
            Specify.That(origPos[2]).Should.BeLogicallyEqualTo(300d, "Longhead 3 was on a valid position but was moved anyway");


            origPos = new MicroMeter[] { 290, 0, 300 };
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[2]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(232d);
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(286d);
            Specify.That(origPos[2]).Should.BeLogicallyEqualTo(300d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAdjustLongheadsCorrectlyWhenUnplacedBetweenLastPlacedAndMaxPos()
        {
            var origPos = new MicroMeter[] { 2000, 1700 };
            var lhParams = GetLongheadParamsWithLongheadsAtPositions(origPos);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMock = new Mock<IAStarNodeEM>();

            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(1));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[0]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(2000d, "Longhead 1 was on a valid position but was moved anyway");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(2054d, "Longhead 2 was not moved the minimum distance it had to be moved.");

            origPos = new MicroMeter[] { 2000, 2100 };
            lhParams = GetLongheadParamsWithLongheadsAtPositions(origPos);
            emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(1));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[0]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(2000d, "Longhead 1 was on a valid position but was moved anyway");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(2100d, "Longhead 2 was on a valid position but was moved anyway");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAdjustLongheadsCorrectly_WhenUnplacedHaveToBePlaced_BetweenTwoPlacedLongheads()
        {
            var origPos = new MicroMeter[] { 200, 0, 268 };
            var lhParams = GetLongheadParamsWithLongheadsAtPositions(origPos);

            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMockLh3 = new Mock<IAStarNodeEM>();
            var iterationMockLh1 = new Mock<IAStarNodeEM>();

            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMockLh1.Object, iterationMockLh3.Object } });

            iterationMockLh1.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMockLh1.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(1));
            iterationMockLh1.Setup(m => m.TargetCoordinate).Returns(origPos[0]);

            iterationMockLh3.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMockLh3.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));
            iterationMockLh3.Setup(m => m.TargetCoordinate).Returns(origPos[2]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(200d, "Longhead 1 was on a valid position but was moved anyway");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(254d, "Longhead 2 was not moved to the correct position");
            Specify.That(origPos[2]).Should.BeLogicallyEqualTo(268d, "Longhead 3 was on a valid position but was moved anyway");

            iterationMockLh3 = new Mock<IAStarNodeEM>();
            iterationMockLh1 = new Mock<IAStarNodeEM>();
            lhParams = GetLongheadParamsWithLongheads(4);
            origPos = new MicroMeter[] { 200, 0, 100, 362 };

            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMockLh1.Object, iterationMockLh3.Object } });

            iterationMockLh1.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMockLh1.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(1));
            iterationMockLh1.Setup(m => m.TargetCoordinate).Returns(origPos[0]);

            iterationMockLh3.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMockLh3.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(4));
            iterationMockLh3.Setup(m => m.TargetCoordinate).Returns(origPos[3]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(200d, "Longhead 1 was on a valid position but was moved anyway");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(254d, "Longhead 2 was not moved to the correct position");
            Specify.That(origPos[2]).Should.BeLogicallyEqualTo(268d, "Longhead 3 was not moved to the correct position");
            Specify.That(origPos[3]).Should.BeLogicallyEqualTo(362d, "Longhead 4 was on a valid position but was moved anyway");
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(LongHeadDistributionException))]
        public void ShouldThrowWhenUnableToAdjustUnplacedLongheads_WhenCloseToMinPos()
        {
            var lhParams = GetLongheadParamsWithLongheads(3);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMock = new Mock<IAStarNodeEM>();

            var origPos = new MicroMeter[] { 100, 0, 50 };
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));


            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(LongHeadDistributionException))]
        public void ShouldThrowWhenUnableToAdjustUnplacedLongheads_WhenCloseToMaxPos()
        {
            var lhParams = GetLongheadParamsWithLongheads(3);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMock = new Mock<IAStarNodeEM>();

            var origPos = new MicroMeter[] { 2450, 0, 2500 };
            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock.Object } });

            iterationMock.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(1));
            iterationMock.Setup(m => m.TargetCoordinate).Returns(origPos[0]);

            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(LongHeadDistributionException))]
        public void ShouldThrowWhenUnableToAdjustUnplacedLonghead_ToBePlacedBetweenTwoPlacedLongheads()
        {
            var lhParams = GetLongheadParamsWithLongheads(3);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMockLh3 = new Mock<IAStarNodeEM>();
            var iterationMockLh1 = new Mock<IAStarNodeEM>();

            var origPos = new MicroMeter[] { 200, 0, 210 };


            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMockLh1.Object, iterationMockLh3.Object } });

            iterationMockLh1.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMockLh1.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(1));

            iterationMockLh3.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMockLh3.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));


            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNewLongheadPositions_WhenRepositioningLongheadsInAnIteration()
        {
            var origPos = new MicroMeter[] { 200, 410, 600 };
            var lhParams = GetLongheadParamsWithLongheadsAtPositions(origPos);
            var emAstarTestGuy = new AstarEmOptimizedTestGuy(lhParams);
            var solutionNodeMock = new Mock<IAStarNodeEM>();
            var iterationMock1Lh3 = new Mock<IAStarNodeEM>();
            var iterationMock1Lh2 = new Mock<IAStarNodeEM>();
            var iterationMock1Lh1 = new Mock<IAStarNodeEM>();

            var iterationMock2Lh2 = new Mock<IAStarNodeEM>();
            
            iterationMock1Lh1.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock1Lh1.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(1));
            iterationMock1Lh1.Setup(m => m.TargetCoordinate).Returns(200d);

            iterationMock1Lh2.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock1Lh2.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(2));
            iterationMock1Lh2.Setup(m => m.TargetCoordinate).Returns(410d);

            iterationMock2Lh2.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock2Lh2.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(2));
            iterationMock2Lh2.Setup(m => m.TargetCoordinate).Returns(415d);

            iterationMock1Lh3.Setup(m => m.NewLongHeadPositions).Returns(origPos);
            iterationMock1Lh3.Setup(m => m.LongHead).Returns(lhParams.GetLongHeadByNumber(3));
            iterationMock1Lh3.Setup(m => m.TargetCoordinate).Returns(600d);

            solutionNodeMock.Setup(m => m.Iterations)
                .Returns(new List<List<IAStarNodeEM>>() { new List<IAStarNodeEM>() { iterationMock1Lh1.Object, iterationMock1Lh2.Object, iterationMock1Lh3.Object }, new List<IAStarNodeEM>() { iterationMock2Lh2.Object } });


            emAstarTestGuy.AdjustPositionOfLongheadsInSolutionNode(solutionNodeMock.Object);

            Specify.That(origPos[0]).Should.BeLogicallyEqualTo(200d, "NewLongheadPosition for Longhead 1 doens't reflect where it is");
            Specify.That(origPos[1]).Should.BeLogicallyEqualTo(415d, "NewLongheadPosition for Longhead 2 doens't reflect where it is");
            Specify.That(origPos[2]).Should.BeLogicallyEqualTo(600d, "NewLongheadPosition for Longhead 3 doens't reflect where it is");
        }

        private void SetupLongHeads(int numberOfLongheads, double offset = 0)
        {
            originalPositions = new MicroMeter[numberOfLongheads];
            for (var i = 0; i < numberOfLongheads; i++)
            {
                var lh = new EmLongHead() { Number = i + 1, Position = ((i + 1) * 100) + offset, LeftSideToTool = 47 };
                if (lh.Number == 3 || lh.Number == 5 || lh.Number == 6 || lh.Number == 8 || lh.Number == 10)
                {
                    lh.LeftSideToTool = 7;
                }
                this.machineSettings.LongHeadParameters.AddLongHead(lh);
                originalPositions[i] = lh.Position;
            }
        }

        private void SetupLongHeads(IReadOnlyList<double> positions)
        {
            originalPositions = new MicroMeter[positions.Count];
            for (var i = 0; i < positions.Count; i++)
            {
                var lh = new EmLongHead() { Number = i + 1, Position = positions[i], LeftSideToTool = 47 };
                if (lh.Number == 3 || lh.Number == 5 || lh.Number == 6 || lh.Number == 8 || lh.Number == 10)
                {
                    lh.LeftSideToTool = 7;
                }
                this.machineSettings.LongHeadParameters.AddLongHead(lh);
                originalPositions[i] = lh.Position;
            }
        }

        private EmLongHeadParameters GetLongheadParamsWithLongheads(int numberofLongheads)
        {
            var longheadParams = new EmLongHeadParameters(new List<EmLongHead>(), 1, 1, 1, 1, 7, 2500, 0, 54, 143, 15d, 24d);

            for (var i = 0; i < numberofLongheads; i++)
            {
                var lh = new EmLongHead() { Number = i + 1, Position = (i + 1) * 100, LeftSideToTool = 47 };
                if (lh.Number == 3 || lh.Number == 5 || lh.Number == 6 || lh.Number == 8 || lh.Number == 10)
                {
                    lh.LeftSideToTool = 7;
                }
                longheadParams.AddLongHead(lh);
            }

            return longheadParams;
        }

        private EmLongHeadParameters GetLongheadParamsWithLongheadsAtPositions(IEnumerable<MicroMeter> positions)
        {
            var longheadParams = new EmLongHeadParameters(new List<EmLongHead>(), 1, 1, 1, 1, 7, 2500, 0, 54, 143, 15d, 24d);

            for (var i = 0; i < positions.Count(); i++)
            {
                var lh = new EmLongHead() { Number = i + 1, Position = positions.ElementAt(i), LeftSideToTool = 47 };
                if (lh.Number == 3 || lh.Number == 5 || lh.Number == 6 || lh.Number == 8 || lh.Number == 10)
                {
                    lh.LeftSideToTool = 7;
                }
                longheadParams.AddLongHead(lh);
            }

            return longheadParams;
        }

        private RuleAppliedPhysicalDesign SetupRscWithOffset(MicroMeter trackOffset)
        {
            var design = new RuleAppliedPhysicalDesign { Length = 600 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(100 + trackOffset, 0),
                new PhysicalCoordinate(100 + trackOffset, 600), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(300 + trackOffset, 0),
                new PhysicalCoordinate(300 + trackOffset, 600), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400 + trackOffset, 0),
                new PhysicalCoordinate(400 + trackOffset, 600), LineType.cut));

            return design;
        }

        private RuleAppliedPhysicalDesign SetupDesignForOffsetCreases()
        {
            var design = new RuleAppliedPhysicalDesign { Length = 300 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(201, 0), new PhysicalCoordinate(201, 150), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(206, 150), new PhysicalCoordinate(206, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(296, 150), new PhysicalCoordinate(296, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(301, 0), new PhysicalCoordinate(301, 150), LineType.crease));
            return design;
        }

        private RuleAppliedPhysicalDesign SetupLinesWith14mmGapInBetween()
        {
            var design = new RuleAppliedPhysicalDesign { Length = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 400), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(215, 50), new PhysicalCoordinate(215, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(200, 150), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(215, 150), new PhysicalCoordinate(215, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 200), new PhysicalCoordinate(200, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(215, 250), new PhysicalCoordinate(215, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 300), new PhysicalCoordinate(200, 350), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(215, 350), new PhysicalCoordinate(215, 400), LineType.crease));

            return design;
        }

        private RuleAppliedPhysicalDesign SetupLinesWithFourSections()
        {
            var design = new RuleAppliedPhysicalDesign { Length = 350 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 150), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(75, 150), new PhysicalCoordinate(75, 500), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 350), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 150), new PhysicalCoordinate(200, 350), LineType.crease));

            return design;
        }

        private RuleAppliedPhysicalDesign SetupDesignWithXSections(int numberOfSections, int xDistanceBetweenLines = 100)
        {
            var design = new RuleAppliedPhysicalDesign { Length = (numberOfSections - 1) * 150 };
            for (int i = 1; i <= numberOfSections; i++)
            {
                design.Add(new PhysicalLine(
                    new PhysicalCoordinate(i * xDistanceBetweenLines, (i - 1) * 150),
                    new PhysicalCoordinate(i * xDistanceBetweenLines, i * 150), LineType.crease));
            }

            return design;
        }

        private static RuleAppliedPhysicalDesign GetFourSectionsWithFiveVerticalLines()
        {
            var design = new RuleAppliedPhysicalDesign { Length = 500 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 100), new PhysicalCoordinate(250, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(350, 100), new PhysicalCoordinate(350, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 200), new PhysicalCoordinate(50, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 200), new PhysicalCoordinate(150, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 300), new PhysicalCoordinate(250, 400), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(350, 300), new PhysicalCoordinate(350, 400), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(450, 0), new PhysicalCoordinate(450, 400), LineType.crease));

            return design;
        }

        private class AstarEmOptimizedTestGuy : LongheadPositionAdjuster
        {

            public AstarEmOptimizedTestGuy(EmLongHeadParameters lhParams)
                : base(lhParams)
            {

            }

            public new void AdjustPositionOfLongheadsInSolutionNode(IAStarNodeEM node)
            {
                base.AdjustPositionOfLongheadsInSolutionNode(node);
            }
        }
    }

    

}
