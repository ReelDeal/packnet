﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Business.CodeGenerator;
using PackNet.Business.CodeGenerator.AStarStuff;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace BusinessTests.CodeGenerator.AStarEmTests
{
    public static class LongHeadDistributionEmTestHelper
    {
        public static IEnumerable<PhysicalLine> SetupLinesAt(IEnumerable<double> xStartValues, double lineLength = 600)
        {
            var lines = new List<PhysicalLine>();

            xStartValues.ToList().ForEach(x => lines.Add(new PhysicalLine(new PhysicalCoordinate(x, 0), new PhysicalCoordinate(x, lineLength), LineType.crease)));

            return lines;
        }

        public static IAStarNodeEM GetStartNode(IEnumerable<PhysicalLine> lines, MicroMeter[] currentPositions, EmPhysicalMachineSettings machineParameters)
        {
            var section = CreateSectionFromLines(lines);
            var design = CreateDesignFromLines(lines);
            return AStarNodeEM.CreateStartNode(machineParameters, currentPositions, section, design);
        }

        public static AStarNodeEM GetFirstNode(IEnumerable<PhysicalLine> lines, MicroMeter[] currentPosition, EmLongHeadParameters longHeadParams, EmPhysicalMachineSettings machineParameters)
        {
            var target = lines.First();
            var section = CreateSectionFromLines(lines);
            var design = CreateDesignFromLines(lines);
            return new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(currentPosition)), machineParameters,
                currentPosition, lines.Skip(1).Select(l => l.StartCoordinate.X), target.StartCoordinate.X, section,
                longHeadParams.GetLongHeadByNumber(1), design);
        }

        public static IAStarNodeEM CreateNodeForLineWithLongHead(SortedSet<IAStarNodeEM> nodesInBranch, IEnumerable<PhysicalLine> lines, MicroMeter[] lhPos,
            int lhToUse, int lineToProcess, EmLongHeadParameters longHeadParams, EmPhysicalMachineSettings machineParameters)
        {
            var section = CreateSectionFromLines(lines);
            var design = CreateDesignFromLines(lines);

            return new AStarNodeEmTestGuy(nodesInBranch, machineParameters, lhPos,
                lines.Skip(lineToProcess).Select(l => l.StartCoordinate.X), lines.ElementAt(lineToProcess - 1).StartCoordinate.X, section, longHeadParams.GetLongHeadByNumber(lhToUse), design);
        }

        public static Section CreateSectionFromLines(IEnumerable<PhysicalLine> lines)
        {
            return new Section(lines, new WasteArea[0], lines.Min(l => l.StartCoordinate.Y), lines.Max(l => l.EndCoordinate.Y));
        }

        public static RuleAppliedPhysicalDesign CreateDesignFromLines(IEnumerable<PhysicalLine> lines)
        {
            var design = new RuleAppliedPhysicalDesign();
            lines.ForEach(design.Add);
            design.Length = lines.Max(l => l.StartCoordinate.Y);
            return design;
        }
    }

    public class AStarNodeEmTestGuy : AStarNodeEM
    {
        public AStarNodeEmTestGuy(SortedSet<IAStarNodeEM> visitedNodesInBranch, EmPhysicalMachineSettings machineParameters,
            MicroMeter[] currentLongHeadPositions, IEnumerable<MicroMeter> remainingCoordinates, MicroMeter targetCoordinate,
            Section section, EmLongHead longHead, RuleAppliedPhysicalDesign design, IEnumerable<WasteArea> remainingWasteAreas = null)
            : base(
                visitedNodesInBranch, machineParameters, currentLongHeadPositions, remainingCoordinates, remainingWasteAreas ?? new List<WasteArea>(), targetCoordinate, section, longHead, design)
        {
        }

        public static double CalculateMovementScoreForIteration(IAStarNodeEM node, List<IAStarNodeEM> distinctLongheads)
        {
            return EmScoreCalculator.CalculateMovementScoreForIteration(node, distinctLongheads);
        }
    }
}
