﻿using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;

using PackNet.Business.CodeGenerator.AStarStuff;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace BusinessTests.CodeGenerator.AStarEmTests
{
    using System.Linq;
    using PackNet.Business.CodeGenerator;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class CoordinateOrderProcessor
    {
        private EmPhysicalMachineSettings machineSettings;

        [TestInitialize]

        public void Setup()
        {
            this.machineSettings = new EmPhysicalMachineSettings();
            machineSettings.LongHeadParameters = new EmLongHeadParameters(new List<EmLongHead>(), 1.5, 90, 10, 55, 7, 2464, -50, 54, 143, 15d, 24d);
            machineSettings.FeedRollerParameters = new EmFeedRollerParameters(1.5, 30, 1, 55, 600, 45);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsOutFeedCoordinateBasedOnLinesInDesignAndSection()
        {
            var outFeedLine = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.crease);
            var nonOutFeedLine = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 500), LineType.crease);
            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(outFeedLine);
            design.Add(nonOutFeedLine);
            var section = new Section(new[] { outFeedLine, nonOutFeedLine }, new WasteArea[0], 0, 600);

            var outFeedNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], outFeedLine.StartCoordinate.X, section, null, design);
            var nonOutFeedNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonOutFeedLine.StartCoordinate.X, section, null, design);

            Specify.That(outFeedNode.IsOutFeedCoordinate).Should.BeTrue();
            Specify.That(nonOutFeedNode.IsOutFeedCoordinate).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldOnlySetIsOutFeedCoordinate_ForLastSectionInDesign()
        {
            var outFeedLine = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.crease);
            var nonOutFeedLine = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 500), LineType.crease);
            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(outFeedLine);
            design.Add(nonOutFeedLine);
            var section = new Section(new[] { outFeedLine, nonOutFeedLine }, new WasteArea[0], 0, 500);

            var nodeWithOutFeedLineButSectionIsNotLast = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], outFeedLine.StartCoordinate.X, section, null, design);
            var nonOutFeedNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonOutFeedLine.StartCoordinate.X, section, null, design);

            Specify.That(nodeWithOutFeedLineButSectionIsNotLast.IsOutFeedCoordinate).Should.BeFalse();
            Specify.That(nonOutFeedNode.IsOutFeedCoordinate).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsSplitCoordinateBasedOnLinesInSection()
        {
            var splitLine = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.cut);
            var nonSplitLineDueToType = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 600), LineType.crease);
            var nonSplitLineDueToStartPosition = new PhysicalLine(new PhysicalCoordinate(100, 50), new PhysicalCoordinate(100, 600), LineType.cut);
            var nonSplitLineDueToEndPosition = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 550), LineType.cut);

            var section = new Section(new[] { splitLine, nonSplitLineDueToType, nonSplitLineDueToStartPosition, nonSplitLineDueToEndPosition }, new WasteArea[0], 0, 600);
            var splitNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], splitLine.StartCoordinate.X, section, null, null);
            var nonSplitNodeDueToType = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonSplitLineDueToType.StartCoordinate.X, section, null, null);
            var nonSplitNodeDueToStartPosition = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonSplitLineDueToStartPosition.StartCoordinate.X, section, null, null);
            var nonSplitNodeDueToEndPosition = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonSplitLineDueToEndPosition.StartCoordinate.X, section, null, null);

            Specify.That(splitNode.IsSplitCoordinate).Should.BeTrue();
            Specify.That(nonSplitNodeDueToType.IsSplitCoordinate).Should.BeFalse();
            Specify.That(nonSplitNodeDueToStartPosition.IsSplitCoordinate).Should.BeFalse();
            Specify.That(nonSplitNodeDueToEndPosition.IsSplitCoordinate).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsDirectlyContinuedBasedOnLinesInSectionAndUpcomingSection()
        {
            var directlyContinued = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);
            var nonDirectlyContinuedDueToEndingAtSectionEnd = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 600), LineType.crease);
            var directlyContinuedNoGapBetweenThisAndSecondLine1 = new PhysicalLine(new PhysicalCoordinate(100, 50), new PhysicalCoordinate(100, 600), LineType.cut);
            var directlyContinuedNoGapBetweenThisAndSecondLine2 = new PhysicalLine(new PhysicalCoordinate(100, 600), new PhysicalCoordinate(100, 1000), LineType.cut);
            var nonDirectlyContinuedDueGapBetweenThisAndSecondLine1 = new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(150, 600), LineType.cut);
            var nonDirectlyContinuedDueGapBetweenThisAndSecondLine2 = new PhysicalLine(new PhysicalCoordinate(150, 601), new PhysicalCoordinate(150, 1000), LineType.cut);
            var design = new RuleAppliedPhysicalDesign();
            design.Add(directlyContinued);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd);
            design.Add(directlyContinuedNoGapBetweenThisAndSecondLine1);
            design.Add(directlyContinuedNoGapBetweenThisAndSecondLine2);
            design.Add(nonDirectlyContinuedDueGapBetweenThisAndSecondLine1);
            design.Add(nonDirectlyContinuedDueGapBetweenThisAndSecondLine2);

            var section =
                new Section(
                    new[]
                    {
                        directlyContinued, directlyContinuedNoGapBetweenThisAndSecondLine1,
                        nonDirectlyContinuedDueToEndingAtSectionEnd, nonDirectlyContinuedDueGapBetweenThisAndSecondLine1
                    }, new WasteArea[0], 0, 600);

            var directlyContinuedNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], directlyContinued.StartCoordinate.X, section, null, design);
            var nonDirectlyContinuedDueToEndingAtSectionEndNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonDirectlyContinuedDueToEndingAtSectionEnd.StartCoordinate.X, section, null, design);
            var directlyContinuedNoGapBetweenThisAndSecondLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], directlyContinuedNoGapBetweenThisAndSecondLine1.StartCoordinate.X, section, null, design);
            var nonDirectlyContinuedDueGapBetweenThisAndSecondLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonDirectlyContinuedDueGapBetweenThisAndSecondLine1.StartCoordinate.X, section, null, design);

            Specify.That(directlyContinuedNode.IsDirectlyContinued).Should.BeTrue();
            Specify.That(nonDirectlyContinuedDueToEndingAtSectionEndNode.IsDirectlyContinued).Should.BeFalse();
            Specify.That(directlyContinuedNoGapBetweenThisAndSecondLineNode.IsDirectlyContinued).Should.BeTrue();
            Specify.That(nonDirectlyContinuedDueGapBetweenThisAndSecondLineNode.IsDirectlyContinued).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsContinuedBasedOnLinesInSectionAndUpcomingSections()
        {
            var continued1 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 500), LineType.cut);
            var continued2 = new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(0, 800), LineType.cut);
            var nonContinued = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 500), LineType.crease);
            var design = new RuleAppliedPhysicalDesign();
            design.Add(continued1);
            design.Add(continued2);
            design.Add(nonContinued);

            var section = new Section(new[] { continued1, continued2, nonContinued }, new WasteArea[0], 0, 500);

            var continuedNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], continued1.StartCoordinate.X, section, null, design);
            var nonContinuedNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], nonContinued.StartCoordinate.X, section, null, design);

            Specify.That(continuedNode.IsContinued).Should.BeTrue();
            Specify.That(nonContinuedNode.IsContinued).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsMinimumYBasedOnLinesInSection()
        {
            var startsBeforeSection = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 500), LineType.cut);
            var startsAtSection = new PhysicalLine(new PhysicalCoordinate(50, 50), new PhysicalCoordinate(50, 500), LineType.cut);
            var startsAfterSectionStart = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 500), LineType.cut);

            var section = new Section(new[] { startsBeforeSection, startsAtSection, startsAfterSectionStart }, new WasteArea[0], 50, 500);

            var startsBeforeSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], startsBeforeSection.StartCoordinate.X, section, null, null);
            var startsAtSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], startsAtSection.StartCoordinate.X, section, null, null);
            var startsAfterSectionStartNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], startsAfterSectionStart.StartCoordinate.X, section, null, null);

            Specify.That(startsBeforeSectionNode.MinYCoordinate)
                .Should.BeEqualTo(section.StartCoordinateY,
                    "Lines that begins before section should get MinYCoordinate of section start.");
            Specify.That(startsAtSectionNode.MinYCoordinate)
                .Should.BeEqualTo(startsAtSection.StartCoordinate.Y,
                    "Lines starting within section should get line start coordinate as MinYCoordinate.");
            Specify.That(startsAfterSectionStartNode.MinYCoordinate)
                .Should.BeEqualTo(startsAfterSectionStart.StartCoordinate.Y,
                    "Lines starting within section should get line start coordinate as MinYCoordinate.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsMaximumYBasedOnLinesInSection()
        {
            var endsAfterSection = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.cut);
            var endsAtSection = new PhysicalLine(new PhysicalCoordinate(50, 300), new PhysicalCoordinate(50, 500), LineType.cut);
            var endsBeforeSection = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 400), LineType.cut);

            var section = new Section(new[] { endsAfterSection, endsAtSection, endsBeforeSection }, new WasteArea[0], 0, 500);

            var endsAfterSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsAfterSection.StartCoordinate.X, section, null, null);
            var endsAtSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsAtSection.StartCoordinate.X, section, null, null);
            var endsBeforeSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsBeforeSection.StartCoordinate.X, section, null, null);

            Specify.That(endsAfterSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(section.EndCoordinateY,
                    "Lines that ends after section should get MaxYCoordinate of section end.");
            Specify.That(endsAtSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(endsAtSection.EndCoordinate.Y,
                    "Lines ending within section should get line end coordinate as MaxYCoordinate.");
            Specify.That(endsBeforeSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(endsBeforeSection.EndCoordinate.Y,
                    "Lines ending within section should get line end coordinate as MaxYCoordinate.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsMaximumYBasedOnLastLinesInSection()
        {
            var endsAfterSection = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.cut);
            var endsAtSection1 = new PhysicalLine(new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 200), LineType.cut);
            var endsAtSection2 = new PhysicalLine(new PhysicalCoordinate(50, 200), new PhysicalCoordinate(50, 500), LineType.crease);
            var endsBeforeSection = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 400), LineType.cut);

            var section = new Section(new[] { endsAfterSection, endsAtSection1, endsAtSection2, endsBeforeSection }, new WasteArea[0], 0, 500);

            var endsAfterSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsAfterSection.StartCoordinate.X, section, null, null);
            var endsAtSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsAtSection1.StartCoordinate.X, section, null, null);
            var endsBeforeSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsBeforeSection.StartCoordinate.X, section, null, null);

            Specify.That(endsAfterSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(section.EndCoordinateY,
                    "Lines that ends after section should get MaxYCoordinate of section end.");
            Specify.That(endsAtSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(endsAtSection2.EndCoordinate.Y,
                    "Lines ending within section should get line end coordinate as MaxYCoordinate.");
            Specify.That(endsBeforeSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(endsBeforeSection.EndCoordinate.Y,
                    "Lines ending within section should get line end coordinate as MaxYCoordinate.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetIsMaximumYBasedOnLastUncontinuedLinesInSection()
        {
            var endsAfterSection = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.cut);
            var endsAtSection1 = new PhysicalLine(new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 200), LineType.cut);
            var endsAtSection2 = new PhysicalLine(new PhysicalCoordinate(50, 300), new PhysicalCoordinate(50, 500), LineType.crease);
            var endsBeforeSection = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 400), LineType.cut);

            var section = new Section(new[] { endsAfterSection, endsAtSection1, endsAtSection2, endsBeforeSection }, new WasteArea[0], 0, 500);

            var endsAfterSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsAfterSection.StartCoordinate.X, section, null, null);
            var endsAtSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsAtSection1.StartCoordinate.X, section, null, null);
            var endsBeforeSectionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), null, new MicroMeter[0], new MicroMeter[0], endsBeforeSection.StartCoordinate.X, section, null, null);

            Specify.That(endsAfterSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(section.EndCoordinateY,
                    "Lines that ends after section should get MaxYCoordinate of section end.");
            Specify.That(endsAtSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(endsAtSection2.EndCoordinate.Y,
                    "Lines ending within section should get line end coordinate as MaxYCoordinate.");
            Specify.That(endsBeforeSectionNode.MaxYCoordinate)
                .Should.BeEqualTo(endsBeforeSection.EndCoordinate.Y,
                    "Lines ending within section should get line end coordinate as MaxYCoordinate.");
        }

        #region Comparison of a Outfeed line to other type of lines.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_OutFeedLineAfter_CompareWithLineThatLHAlreadyInPosition()
        {
            var OutfeedLine = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 500), LineType.cut);
            var LineWithLHInPosition = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 400), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign() { Length = 500 };
            design.Add(OutfeedLine);
            design.Add(LineWithLHInPosition);

            var section = new Section(new[] { OutfeedLine, LineWithLHInPosition }, new WasteArea[0], 0, 500);

            var OutfeedLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0], OutfeedLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var LineWithLHInPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0], LineWithLHInPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(OutfeedLineNode, LineWithLHInPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineWithLHInPositionNode, OutfeedLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineWithLHInPositionNode, LineWithLHInPositionNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(OutfeedLineNode, OutfeedLineNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_OutFeedLineAfter_CompareWithLineCanGenerateMinimizeReverse()
        {
            var OutfeedLine = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.cut);
            var LineProcessFirstCanGenerateShorterReverse = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 100), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign() { Length = 500 };
            design.Add(OutfeedLine);
            design.Add(LineProcessFirstCanGenerateShorterReverse);

            var section = new Section(new[] { OutfeedLine, LineProcessFirstCanGenerateShorterReverse }, new WasteArea[0], 0, 500);

            var OutfeedLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                OutfeedLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineProcessFirstCanGenerateShorterReverseNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineProcessFirstCanGenerateShorterReverse.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(OutfeedLineNode, LineProcessFirstCanGenerateShorterReverseNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineProcessFirstCanGenerateShorterReverseNode, OutfeedLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineProcessFirstCanGenerateShorterReverseNode, LineProcessFirstCanGenerateShorterReverseNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(OutfeedLineNode, OutfeedLineNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_OutFeedLineAfter_CompareWithLineThatTargetCoordinateIsSmaller()
        {

            var OutfeedLine = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.cut);
            var LineInSmallerPosition = new PhysicalLine(new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 400), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign() { Length = 500 };
            design.Add(OutfeedLine);
            design.Add(LineInSmallerPosition);

            var section = new Section(new[] { OutfeedLine, LineInSmallerPosition }, new WasteArea[0], 0, 500);

            var OutfeedLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                OutfeedLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineInSmallerPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineInSmallerPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(OutfeedLineNode, LineInSmallerPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineInSmallerPositionNode, OutfeedLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineInSmallerPositionNode, LineInSmallerPositionNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(OutfeedLineNode, OutfeedLineNode)).Should.BeEqualTo(0);
        }

        #endregion

        #region Comparison of a Split line to other type of lines.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_SplitLinesAfter_CompareWithLineThatLHAlreadyInPosition()
        {
            var SplitLine = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 500), LineType.cut);
            var LineWithLHInPosition = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 400), LineType.cut);
            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(SplitLine);
            design.Add(LineWithLHInPosition);

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var section = new Section(new[] { SplitLine, LineWithLHInPosition }, new WasteArea[0], 0, 500);

            var SplitLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                SplitLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineWithLHInPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineWithLHInPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(SplitLineNode, LineWithLHInPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineWithLHInPositionNode, SplitLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineWithLHInPositionNode, LineWithLHInPositionNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(SplitLineNode, SplitLineNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_SplitLineAfter_CompareWithLineThatStartPositionIsSmaller()
        {
            var SplitLine = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.cut);
            var LineInSmallerPosition = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 400), LineType.cut);
            
            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(SplitLine);
            design.Add(LineInSmallerPosition);

            var section = new Section(new[] { SplitLine, LineInSmallerPosition }, new WasteArea[0], 0, 500);

            var SplitLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                SplitLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineInSmallerPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineInSmallerPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(SplitLineNode, LineInSmallerPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineInSmallerPositionNode, SplitLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineInSmallerPositionNode, LineInSmallerPositionNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(SplitLineNode, SplitLineNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_SplitLineAfter_CompareWithDirectlyContinuedLine()
        {
            var SplitLine = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.cut);
            var directlyContinuedLine = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 600), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign() { Length = 700 };
            design.Add(SplitLine);
            design.Add(directlyContinuedLine);

            var section = new Section(new[] { SplitLine, directlyContinuedLine }, new WasteArea[0], 0, 500);

            var SplitLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                SplitLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var directlyContinuedLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(SplitLineNode, directlyContinuedLineNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(directlyContinuedLineNode, SplitLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(directlyContinuedLineNode, directlyContinuedLineNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(SplitLineNode, SplitLineNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_SplitLineAfter_CompareWithIndirectlyContinuedLine()
        {
            var SplitLine = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.cut);
            var indirectlyContinuedLine = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 500), LineType.crease);
            var indirectlyContinuedLine2 = new PhysicalLine(new PhysicalCoordinate(50, 510), new PhysicalCoordinate(50, 600), LineType.crease);
            
            var currentLongHeadPositions = new MicroMeter[] { 100 };
            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(SplitLine);
            design.Add(indirectlyContinuedLine);
            design.Add(indirectlyContinuedLine2);

            var section = new Section(new[] { SplitLine, indirectlyContinuedLine }, new WasteArea[0], 0, 500);

            var SplitLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                SplitLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var indirectlyContinuedLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                indirectlyContinuedLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(SplitLineNode, indirectlyContinuedLineNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(indirectlyContinuedLineNode, SplitLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(indirectlyContinuedLineNode, indirectlyContinuedLineNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(SplitLineNode, SplitLineNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_SplitLineAfter_CompareWithLineCanGenerateMinimizeReverse()
        {
            var SplitLine = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.cut);
            var LineProcessFirstCanGenerateShorterReverse = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 100), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(SplitLine);
            design.Add(LineProcessFirstCanGenerateShorterReverse);

            var section = new Section(new[] { SplitLine, LineProcessFirstCanGenerateShorterReverse }, new WasteArea[0], 0, 500);

            var SplitLineNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                SplitLine.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineProcessFirstCanGenerateShorterReverseNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineProcessFirstCanGenerateShorterReverse.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(SplitLineNode, LineProcessFirstCanGenerateShorterReverseNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineProcessFirstCanGenerateShorterReverseNode, SplitLineNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineProcessFirstCanGenerateShorterReverseNode, LineProcessFirstCanGenerateShorterReverseNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(SplitLineNode, SplitLineNode)).Should.BeEqualTo(0);
        }

        #endregion

        #region Comparison of a direactly continued line to other type of lines.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_DirectlyContinuedAfter_CompareWithNonDirectlyContinuedDueToEndingAtSectionEnd()
        {
            var directlyContinuedButNotSplit = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);
            var nonDirectlyContinuedDueToEndingAtSectionEnd = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 600), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var design = new RuleAppliedPhysicalDesign();
            design.Add(directlyContinuedButNotSplit);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd);

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var section =
                new Section(
                    new[]
                    {
                        directlyContinuedButNotSplit, nonDirectlyContinuedDueToEndingAtSectionEnd
                    }, new WasteArea[0], 0, 600);

            var directlyContinuedButNotSplitNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButNotSplit.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var nonDirectlyContinuedDueToEndingAtSectionEndNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                nonDirectlyContinuedDueToEndingAtSectionEnd.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_DirectlyContinuedAfter_CompareWithNonDirectlyContinuedDueToGapInTheMiddle()
        {
            var directlyContinuedButNotSplit = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);
            var nonDirectlyContinuedDueToEndingAtSectionEnd1 = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 600), LineType.crease);
            var nonDirectlyContinuedDueToEndingAtSectionEnd2 = new PhysicalLine(new PhysicalCoordinate(50, 700), new PhysicalCoordinate(50, 800), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(directlyContinuedButNotSplit);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd1);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd2);

            var section =
                new Section(
                    new[]
                    {
                        directlyContinuedButNotSplit, nonDirectlyContinuedDueToEndingAtSectionEnd1
                    }, new WasteArea[0], 0, 600);


            var directlyContinuedButNotSplitNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButNotSplit.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var nonDirectlyContinuedDueToEndingAtSectionEndNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                nonDirectlyContinuedDueToEndingAtSectionEnd1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_DirectlyContinuedAccordingToTargetPosition_CompareWithDirectlyContinuedButDifferentLineTypeAndDifferentBreakPosition()
        {
            var directlyContinuedButNotSplit = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);

            var directlyContinuedButTypeChangedAtSectionEnd1 = new PhysicalLine(new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 600), LineType.cut);
            var directlyContinuedButTypeChangedAtSectionEnd2 = new PhysicalLine(new PhysicalCoordinate(50, 600), new PhysicalCoordinate(50, 800), LineType.crease);

            var directlyContinuedButTypeChangedInTheSectionMiddle1 = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 300), LineType.cut);
            var directlyContinuedButTypeChangedInTheSectionMiddle2 = new PhysicalLine(new PhysicalCoordinate(100, 300), new PhysicalCoordinate(100, 800), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 200 };
            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(directlyContinuedButNotSplit);
            design.Add(directlyContinuedButTypeChangedAtSectionEnd1);
            design.Add(directlyContinuedButTypeChangedAtSectionEnd2);
            design.Add(directlyContinuedButTypeChangedInTheSectionMiddle1);
            design.Add(directlyContinuedButTypeChangedInTheSectionMiddle2);

            var section =
                new Section(
                    new[]
                    {
                        directlyContinuedButNotSplit, directlyContinuedButTypeChangedAtSectionEnd1, directlyContinuedButTypeChangedInTheSectionMiddle1, directlyContinuedButTypeChangedInTheSectionMiddle2
                    }, new WasteArea[0], 0, 600);

            var directlyContinuedButNotSplitNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButNotSplit.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var directlyContinuedButTypeChangedAtSectionEndNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButTypeChangedAtSectionEnd1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var directlyContinuedButTypeChangedInTheSectionMiddleNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButTypeChangedInTheSectionMiddle1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, directlyContinuedButTypeChangedAtSectionEndNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(directlyContinuedButTypeChangedAtSectionEndNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, directlyContinuedButTypeChangedInTheSectionMiddleNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(directlyContinuedButTypeChangedInTheSectionMiddleNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(directlyContinuedButTypeChangedAtSectionEndNode, directlyContinuedButTypeChangedInTheSectionMiddleNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(directlyContinuedButTypeChangedInTheSectionMiddleNode, directlyContinuedButTypeChangedAtSectionEndNode)).Should.BeEqualTo(1);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(directlyContinuedButTypeChangedAtSectionEndNode, directlyContinuedButTypeChangedAtSectionEndNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(directlyContinuedButTypeChangedInTheSectionMiddleNode, directlyContinuedButTypeChangedInTheSectionMiddleNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_DirectlyContinuedAfter_CompareWithLineThatCanGenerateMinimumReverse()
        {
            var directlyContinuedButNotSplit = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);

            var LineProcessedFirstCanGenerateMinimumReverse = new PhysicalLine(new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 300), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(directlyContinuedButNotSplit);
            design.Add(LineProcessedFirstCanGenerateMinimumReverse);

            var section =
                new Section(
                    new[]
                    {
                        directlyContinuedButNotSplit, LineProcessedFirstCanGenerateMinimumReverse
                    }, new WasteArea[0], 0, 600);

            var directlyContinuedButNotSplitNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButNotSplit.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineProcessedFirstCanGenerateMinimumReverseNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineProcessedFirstCanGenerateMinimumReverse.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, LineProcessedFirstCanGenerateMinimumReverseNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineProcessedFirstCanGenerateMinimumReverseNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(-1);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(LineProcessedFirstCanGenerateMinimumReverseNode, LineProcessedFirstCanGenerateMinimumReverseNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_DirectlyContinuedAfter_CompareWithLineThatLHAlreadyInPosition()
        {
            var directlyContinuedButNotSplit = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);

            var LineWithLongHeadOnPosition = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 600), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(directlyContinuedButNotSplit);
            design.Add(LineWithLongHeadOnPosition);

            var section =
                new Section(
                    new[]
                    {
                        directlyContinuedButNotSplit, LineWithLongHeadOnPosition
                    }, new WasteArea[0], 0, 600);

            var directlyContinuedButNotSplitNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButNotSplit.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineWithLongHeadOnPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineWithLongHeadOnPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, LineWithLongHeadOnPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineWithLongHeadOnPositionNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(-1);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitNode, directlyContinuedButNotSplitNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(LineWithLongHeadOnPositionNode, LineWithLongHeadOnPositionNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_DirectlyContinuedAfter_CompareWithLineThatTargetCoordinateIsSmaller()
        {
            var directlyContinuedButNotSplitAt100 = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 1000), LineType.cut);

            var directlyContinuedButNotSplitAt0 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 200 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(directlyContinuedButNotSplitAt100);
            design.Add(directlyContinuedButNotSplitAt0);

            var section =
                new Section(
                    new[]
                    {
                        directlyContinuedButNotSplitAt100, directlyContinuedButNotSplitAt0
                    }, new WasteArea[0], 0, 600);

            var directlyContinuedButNotSplitAt100Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButNotSplitAt100.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var directlyContinuedButNotSplitAt0Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                directlyContinuedButNotSplitAt0.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitAt100Node, directlyContinuedButNotSplitAt0Node)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(directlyContinuedButNotSplitAt0Node, directlyContinuedButNotSplitAt100Node)).Should.BeEqualTo(-1);

            Specify.That(comparer.Compare(directlyContinuedButNotSplitAt100Node, directlyContinuedButNotSplitAt100Node)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(directlyContinuedButNotSplitAt0Node, directlyContinuedButNotSplitAt0Node)).Should.BeEqualTo(0);
        }
        #endregion

        #region Comparison of a indireactly continued line to other type of lines.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_IndirectlyContinuedWithGapOutsideOfSectionAfter_CompareWithLineToMinimizeReverseDistance()
        {
            var LineToMinimizeReverseDistance = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 500), LineType.cut);
            var nonDirectlyContinuedDueToEndingAtSectionEnd1 = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 600), LineType.crease);
            var nonDirectlyContinuedDueToEndingAtSectionEnd2 = new PhysicalLine(new PhysicalCoordinate(50, 700), new PhysicalCoordinate(50, 800), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(LineToMinimizeReverseDistance);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd1);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd2);

            var section =
                new Section(
                    new[]
                    {
                        LineToMinimizeReverseDistance, nonDirectlyContinuedDueToEndingAtSectionEnd1
                    }, new WasteArea[0], 0, 600);

            var LineToMinimizeReverseDistanceNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineToMinimizeReverseDistance.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var nonDirectlyContinuedDueToEndingAtSectionEndNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                nonDirectlyContinuedDueToEndingAtSectionEnd1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_IndirectlyContinuedWithGapInsideOfSectionAfter_CompareWithLineToMinimizeReverseDistance()
        {
            var LineToMinimizeReverseDistance = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 400), LineType.cut);
            var nonDirectlyContinuedDueToEndingAtSectionEnd1 = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 300), LineType.crease);
            var nonDirectlyContinuedDueToEndingAtSectionEnd2 = new PhysicalLine(new PhysicalCoordinate(50, 700), new PhysicalCoordinate(50, 800), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(LineToMinimizeReverseDistance);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd1);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd2);

            var section =
                new Section(
                    new[]
                    {
                        LineToMinimizeReverseDistance, nonDirectlyContinuedDueToEndingAtSectionEnd1
                    }, new WasteArea[0], 0, 600);

            var LineToMinimizeReverseDistanceNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineToMinimizeReverseDistance.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var nonDirectlyContinuedDueToEndingAtSectionEndNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                nonDirectlyContinuedDueToEndingAtSectionEnd1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_IndirectlyContinuedAfter_CompareWithLineThatLHAlreadyInPosition()
        {
            var LineWithLongHeadOnPosition = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 500), LineType.cut);
            var nonDirectlyContinuedDueToEndingAtSectionEnd1 = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 300), LineType.crease);
            var nonDirectlyContinuedDueToEndingAtSectionEnd2 = new PhysicalLine(new PhysicalCoordinate(50, 700), new PhysicalCoordinate(50, 800), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(LineWithLongHeadOnPosition);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd1);
            design.Add(nonDirectlyContinuedDueToEndingAtSectionEnd2);

            var section =
                new Section(
                    new[]
                    {
                        LineWithLongHeadOnPosition, nonDirectlyContinuedDueToEndingAtSectionEnd1
                    }, new WasteArea[0], 0, 600);

            var LineWithLongHeadOnPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineWithLongHeadOnPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var nonDirectlyContinuedDueToEndingAtSectionEndNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                nonDirectlyContinuedDueToEndingAtSectionEnd1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(LineWithLongHeadOnPositionNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, LineWithLongHeadOnPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineWithLongHeadOnPositionNode, LineWithLongHeadOnPositionNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(nonDirectlyContinuedDueToEndingAtSectionEndNode, nonDirectlyContinuedDueToEndingAtSectionEndNode)).Should.BeEqualTo(0);
        }
        #endregion

        #region Comparison of a line that processing first can generate minimized reverse to other type of lines.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_LineToMinimizeReverseDistanceFirst_CompareWithLineThatLHAlreadyInPosition()
        {
            var LineToMinimizeReverseDistance = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 400), LineType.cut);
            var LineThatLHAlreadyInPosition = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 600), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(LineToMinimizeReverseDistance);
            design.Add(LineThatLHAlreadyInPosition);

            var section =
                new Section(
                    new[]
                    {
                        LineToMinimizeReverseDistance, LineThatLHAlreadyInPosition
                    }, new WasteArea[0], 0, 700);

            var LineToMinimizeReverseDistanceNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineToMinimizeReverseDistance.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var LineThatLHAlreadyInPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineThatLHAlreadyInPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, LineThatLHAlreadyInPositionNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineThatLHAlreadyInPositionNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(LineThatLHAlreadyInPositionNode, LineThatLHAlreadyInPositionNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_LineToMinimizeReverseDistanceFirst_CompareWithLineThatStartYPositionIsSmaller()
        {
            var LineToMinimizeReverseDistance = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 400), LineType.cut);
            var LineThatStartYPositionIsSmaller = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 600), LineType.crease);

            var currentLongHeadPositions = new MicroMeter[] { 200 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(LineToMinimizeReverseDistance);
            design.Add(LineThatStartYPositionIsSmaller);

            var section =
                new Section(
                    new[]
                    {
                        LineToMinimizeReverseDistance, LineThatStartYPositionIsSmaller
                    }, new WasteArea[0], 0, 700);

            var LineToMinimizeReverseDistanceNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineToMinimizeReverseDistance.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var LineThatStartYPositionIsSmallerNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineThatStartYPositionIsSmaller.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, LineThatStartYPositionIsSmallerNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineThatStartYPositionIsSmallerNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineToMinimizeReverseDistanceNode, LineToMinimizeReverseDistanceNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(LineThatStartYPositionIsSmallerNode, LineThatStartYPositionIsSmallerNode)).Should.BeEqualTo(0);
        }

        #endregion

        #region Comparison of a line that LH is already at the position to other type of lines.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_LineThatLHAlreadyInPosition_CompareWithLineWithoutLHInPosition()
        {
            var LineWithoutLHInPosition = new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 400), LineType.cut);
            var LineThatLHAlreadyInPosition = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 400), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(LineWithoutLHInPosition);
            design.Add(LineThatLHAlreadyInPosition);

            var section =
                new Section(
                    new[]
                    {
                        LineWithoutLHInPosition, LineThatLHAlreadyInPosition
                    }, new WasteArea[0], 0, 600);

            var LineWithoutLHInPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineWithoutLHInPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var LineThatLHAlreadyInPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineThatLHAlreadyInPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(LineWithoutLHInPositionNode, LineThatLHAlreadyInPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineThatLHAlreadyInPositionNode, LineWithoutLHInPositionNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineWithoutLHInPositionNode, LineWithoutLHInPositionNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(LineThatLHAlreadyInPositionNode, LineThatLHAlreadyInPositionNode)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_LineThatLHAlreadyInPosition_CompareWithLineWithoutLHInPositionAndCanGenerateEquallyReverse()
        {
            var LineWithoutLHInPositionAndCanGenerateEquallyReverse = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 400), LineType.cut);
            var LineThatLHAlreadyInPosition = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 500), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(LineWithoutLHInPositionAndCanGenerateEquallyReverse);
            design.Add(LineThatLHAlreadyInPosition);

            var section =
                new Section(
                    new[]
                    {
                        LineWithoutLHInPositionAndCanGenerateEquallyReverse, LineThatLHAlreadyInPosition
                    }, new WasteArea[0], 0, 600);

            var LineWithoutLHInPositionAndCanGenerateEquallyReverseNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineWithoutLHInPositionAndCanGenerateEquallyReverse.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var LineThatLHAlreadyInPositionNode = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                LineThatLHAlreadyInPosition.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(LineWithoutLHInPositionAndCanGenerateEquallyReverseNode, LineThatLHAlreadyInPositionNode)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(LineThatLHAlreadyInPositionNode, LineWithoutLHInPositionAndCanGenerateEquallyReverseNode)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(LineWithoutLHInPositionAndCanGenerateEquallyReverseNode, LineWithoutLHInPositionAndCanGenerateEquallyReverseNode)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(LineThatLHAlreadyInPositionNode, LineThatLHAlreadyInPositionNode)).Should.BeEqualTo(0);
        }

        #endregion

        #region Comparison of a lines using the start position of lines rule.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_LineThatStartYPositionOfLineIsSmallerFirst()
        {
            var Line1 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 400), LineType.cut);
            var Line2 = new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 500), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(Line1);
            design.Add(Line2);

            var section =
                new Section(
                    new[]
                    {
                        Line1, Line2
                    }, new WasteArea[0], 0, 600);

            var Line1Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var Line2Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line2.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(Line1Node, Line2Node)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(Line2Node, Line1Node)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(Line1Node, Line1Node)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(Line2Node, Line2Node)).Should.BeEqualTo(0);
        }

        #endregion

        #region Comparison of a lines using the target coordinate of lines rule.

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_TwoOutFeedLines_SmallerTargetCoordinateShouldBeFirst()
        {
            var OutfeedLine1 = new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 500), LineType.cut);
            var OutfeedLine2 = new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 500), LineType.cut);
            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var design = new RuleAppliedPhysicalDesign() { Length = 500 };
            design.Add(OutfeedLine1);
            design.Add(OutfeedLine2);

            var section = new Section(new[] { OutfeedLine1, OutfeedLine2 }, new WasteArea[0], 0, 500);
            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var OutfeedLine1Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                OutfeedLine1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var OutfeedLine2Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                OutfeedLine2.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(OutfeedLine1Node, OutfeedLine2Node)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(OutfeedLine2Node, OutfeedLine1Node)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(OutfeedLine2Node, OutfeedLine2Node)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(OutfeedLine1Node, OutfeedLine1Node)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_TwoSplitLines_SmallerTargetCoordinateShouldBeFirst()
        {
            var SplitLine1 = new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 500), LineType.cut);
            var SplitLine2 = new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 500), LineType.cut);
            var currentLongHeadPositions = new MicroMeter[] { 100 };

            var design = new RuleAppliedPhysicalDesign() { Length = 600 };
            design.Add(SplitLine1);
            design.Add(SplitLine2);

            var section = new Section(new[] { SplitLine1, SplitLine2 }, new WasteArea[0], 0, 500);

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var SplitLine1Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                SplitLine1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);
            var SplitLine2Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                SplitLine2.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(SplitLine1Node, SplitLine2Node)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(SplitLine2Node, SplitLine1Node)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(SplitLine2Node, SplitLine2Node)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(SplitLine1Node, SplitLine1Node)).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_TwoDirectlyContinuedLines_AccordingToTargetCoordinate()
        {
            var Line1 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1000), LineType.cut);
            var Line2 = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 1000), LineType.cut);

            var currentLongHeadPositions = new MicroMeter[] { 200 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(Line1);
            design.Add(Line2);

            var section =
                new Section(new[] { Line1, Line2 }, new WasteArea[0], 0, 600);

            var Line1Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var Line2Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line2.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(Line1Node, Line2Node)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(Line2Node, Line1Node)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(Line1Node, Line1Node)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(Line2Node, Line2Node)).Should.BeEqualTo(0);
        }
   
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_TwoIndirectlyContinuedLines_AccordingToTargetCoordinate()
        {
            var Line1 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 300), LineType.cut);
            var Line1_Part2 = new PhysicalLine(new PhysicalCoordinate(0, 400), new PhysicalCoordinate(0, 1000), LineType.cut);
            var Line2 = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 200), LineType.cut);
            var Line2_Part2 = new PhysicalLine(new PhysicalCoordinate(100, 400), new PhysicalCoordinate(100, 1000), LineType.cut);
            var currentLongHeadPositions = new MicroMeter[] { 200 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(Line1);
            design.Add(Line2);
            design.Add(Line1_Part2);
            design.Add(Line2_Part2);

            var section =
                new Section(new[] { Line1, Line2, Line1_Part2, Line2_Part2 }, new WasteArea[0], 0, 600);

            var Line1Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var Line2Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line2.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(Line1Node, Line2Node)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(Line2Node, Line1Node)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(Line1Node, Line1Node)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(Line2Node, Line2Node)).Should.BeEqualTo(0);

            Specify.That(Line1Node.MaxYCoordinate)
                .Should.BeEqualTo(section.EndCoordinateY,
                    "Lines that ends after section should get MaxYCoordinate of section end.");
            Specify.That(Line2Node.MaxYCoordinate)
                .Should.BeEqualTo(section.EndCoordinateY,
                    "Lines that ends after section should get MaxYCoordinate of section end.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSort_TwoLinesThatStartAtSameYCoordinate_AccordingToTargetCoordinate()
        {
            var Line1 = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 300), LineType.cut);
            var Line2 = new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 300), LineType.cut);
            var currentLongHeadPositions = new MicroMeter[] { 200 };

            var comparer = new CoordinateProcessOrderComparer(currentLongHeadPositions);

            var design = new RuleAppliedPhysicalDesign();
            design.Add(Line1);
            design.Add(Line2);

            var section =
                new Section(new[] { Line1, Line2 }, new WasteArea[0], 0, 600);

            var Line1Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line1.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            var Line2Node = new AStarNodeEmTestGuy(new SortedSet<IAStarNodeEM>(), machineSettings, currentLongHeadPositions, new MicroMeter[0],
                Line2.StartCoordinate.X, section, new EmLongHead() { Number = 1 }, design);

            Specify.That(comparer.Compare(Line1Node, Line2Node)).Should.BeEqualTo(-1);
            Specify.That(comparer.Compare(Line2Node, Line1Node)).Should.BeEqualTo(1);
            Specify.That(comparer.Compare(Line1Node, Line1Node)).Should.BeEqualTo(0);
            Specify.That(comparer.Compare(Line2Node, Line2Node)).Should.BeEqualTo(0);

            Specify.That(Line1Node.MaxYCoordinate)
                .Should.BeEqualTo(Line1.EndCoordinate.Y,
                    "Lines that ends within section should get MaxYCoordinate of line end.");
            Specify.That(Line2Node.MaxYCoordinate)
                .Should.BeEqualTo(Line2.EndCoordinate.Y,
                    "Lines that ends after section should get MaxYCoordinate of line end.");
        }

        #endregion
    }
}
