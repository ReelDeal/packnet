﻿using Moq;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace BusinessTests.CodeGenerator.AStarEmTests
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Business.CodeGenerator.AStarStuff;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class EmLongHeadPositionerScoringTests
    {
        private EmPhysicalMachineSettings machineSettings;

        [TestInitialize]
        public void Setup()
        {
            var lh1 = new EmLongHead() { LeftSideToTool = 47, Number = 1 };
            var lh2 = new EmLongHead() { LeftSideToTool = 47, Number = 2 };
            var lh3 = new EmLongHead() { LeftSideToTool = 6, Number = 3 };
            machineSettings = new EmPhysicalMachineSettings();

            machineSettings.LongHeadParameters = new EmLongHeadParameters(new[] { lh1, lh2, lh3 }, 1.5, 90, 10, 55, 7, 2464, 50, 54, 143, 15d, 24d);
            machineSettings.FeedRollerParameters = new EmFeedRollerParameters(1.5, 30, 1, 55, 600, 45);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldTakeAccelerationAndDecelerationIntoAccountWhenScoringReverses()
        {
            var score = EmScoreCalculator.CalculateReverseScoreForDistance(1, 1, new EmFeedRollerParameters(1, 100, 1, 100, 1, 45));

            Specify.That(score).Should.BeEqualTo(10.0, 0.01);

            score = EmScoreCalculator.CalculateReverseScoreForDistance(100, 1, new EmFeedRollerParameters(1, 100, 1, 100, 1, 45));

            Specify.That(score).Should.BeEqualTo(505.0);

            score = EmScoreCalculator.CalculateReverseScoreForDistance(1000, 1, new EmFeedRollerParameters(1, 100, 1, 100, 1, 45));

            Specify.That(score).Should.BeEqualTo(5005.0);

            score = EmScoreCalculator.CalculateReverseScoreForDistance(1500, 1, new EmFeedRollerParameters(1, 100, 1, 100, 1, 45));
            
            Specify.That(score).Should.BeEqualTo(5 * (1499.0 + 2));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldTakeAccelerationAndDecelerationIntoAccountWhenScoringMovements()
        {
            var lh1 = new EmLongHead() { LeftSideToTool = 47, Number = 1 };
            var lh2 = new EmLongHead() { LeftSideToTool = 47, Number = 2 };
            var lh3 = new EmLongHead() { LeftSideToTool = 6, Number = 3 };
            var lhParams = new EmLongHeadParameters(new[] { lh1, lh2, lh3 }, 1, 100, 1, 100, 10, 2464, 50, 54, 143, 15d, 24d);

            var score = EmScoreCalculator.CalculateMovementScoreForDistance(1, lhParams);

            Specify.That(Math.Round(score, 3)).Should.BeEqualTo(2 + 1 * lhParams.ConnectDelayOffInSeconds, 0.01);

            score = EmScoreCalculator.CalculateMovementScoreForDistance(100, lhParams);

            Specify.That(Math.Round(score, 3)).Should.BeEqualTo(101 + 1 * lhParams.ConnectDelayOffInSeconds, 0.01);

            score = EmScoreCalculator.CalculateMovementScoreForDistance(1000, lhParams);

            Specify.That(Math.Round(score, 3)).Should.BeEqualTo(1001 + 1 * lhParams.ConnectDelayOffInSeconds, 0.01);

            score = EmScoreCalculator.CalculateMovementScoreForDistance(1500, lhParams);

            Specify.That(Math.Round(score, 3)).Should.BeEqualTo(1501 + 1 * lhParams.ConnectDelayOffInSeconds, 0.01);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldTakeIntermittentLhPositionsIntoAccountWhenScoringMovementsToTheRight()
        {
            var lh1 = new EmLongHead() { LeftSideToTool = 0, Number = 1 };
            var lh2 = new EmLongHead() { LeftSideToTool = 0, Number = 2 };
            var lh3 = new EmLongHead() { LeftSideToTool = 0, Number = 3 };
            var lh4 = new EmLongHead() { LeftSideToTool = 0, Number = 4 };
            var lh5 = new EmLongHead() { LeftSideToTool = 0, Number = 5 };
            var lhParams = new EmLongHeadParameters(new[] { lh1, lh2, lh3, lh4, lh5 }, 1, 100, 1, 100, 10, 2464, 50, 54, 143, 15d, 24d);

            var n1 = new Mock<IAStarNodeEM>();
            n1.Setup(m => m.TargetCoordinate).Returns(200);
            n1.Setup(m => m.LongHead).Returns(lh1);

            var n2 = new Mock<IAStarNodeEM>();
            n2.Setup(m => m.TargetCoordinate).Returns(300);
            n2.Setup(m => m.LongHead).Returns(lh2);

            var n3 = new Mock<IAStarNodeEM>();
            n3.Setup(m => m.TargetCoordinate).Returns(500);
            n3.Setup(m => m.LongHead).Returns(lh3);

            var n4 = new Mock<IAStarNodeEM>();
            n4.Setup(m => m.TargetCoordinate).Returns(700);
            n4.Setup(m => m.LongHead).Returns(lh4);

            var n5 = new Mock<IAStarNodeEM>();
            n5.Setup(m => m.TargetCoordinate).Returns(900);
            n5.Setup(m => m.LongHead).Returns(lh5);

            var nodes = new List<IAStarNodeEM>()
            {
                n1.Object,
                n2.Object,
                n3.Object,
                n4.Object,
                n5.Object
            };

            var mock = new Mock<IAStarNodeEM>();
            mock.Setup(m => m.NewLongHeadPositions).Returns(new MicroMeter[] { 100, 200, 300, 400, 500 });
            mock.Setup(m => m.MachineParameters).Returns(new EmPhysicalMachineSettings(){FeedRollerParameters = new EmFeedRollerParameters(1, 1, 1, 1, 1, 45), LongHeadParameters = lhParams});

            var score = AStarNodeEmTestGuy.CalculateMovementScoreForIteration(mock.Object, nodes);

            Specify.That(score).Should.BeEqualTo(4 * (101 + lhParams.ConnectDelayOffInSeconds) + lhParams.ConnectDelayOnInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldTakeIntermittentLhPositionsIntoAccountWhenScoringMovementsToTheLeft()
        {
            var lh1 = new EmLongHead() { LeftSideToTool = 0, Number = 1 };
            var lh2 = new EmLongHead() { LeftSideToTool = 0, Number = 2 };
            var lh3 = new EmLongHead() { LeftSideToTool = 0, Number = 3 };
            var lh4 = new EmLongHead() { LeftSideToTool = 0, Number = 4 };
            var lh5 = new EmLongHead() { LeftSideToTool = 0, Number = 5 };
            var lhParams = new EmLongHeadParameters(new[] { lh1, lh2, lh3, lh4, lh5 }, 1, 100, 1, 100, 10, 2464, 50, 54, 143, 15d, 24d);

            var n1 = new Mock<IAStarNodeEM>();
            n1.Setup(m => m.TargetCoordinate).Returns(600);
            n1.Setup(m => m.LongHead).Returns(lh1);

            var n2 = new Mock<IAStarNodeEM>();
            n2.Setup(m => m.TargetCoordinate).Returns(900);
            n2.Setup(m => m.LongHead).Returns(lh2);

            var n3 = new Mock<IAStarNodeEM>();
            n3.Setup(m => m.TargetCoordinate).Returns(1000);
            n3.Setup(m => m.LongHead).Returns(lh3);

            var n4 = new Mock<IAStarNodeEM>();
            n4.Setup(m => m.TargetCoordinate).Returns(1300);
            n4.Setup(m => m.LongHead).Returns(lh4);

            var n5 = new Mock<IAStarNodeEM>();
            n5.Setup(m => m.TargetCoordinate).Returns(1700);
            n5.Setup(m => m.LongHead).Returns(lh5);

            var nodes = new List<IAStarNodeEM>()
            {
                n1.Object,
                n2.Object,
                n3.Object,
                n4.Object,
                n5.Object
            };

            var mock = new Mock<IAStarNodeEM>();
            mock.Setup(m => m.NewLongHeadPositions).Returns(new MicroMeter[] { 1200, 1400, 1600, 1800, 2000 });
            mock.Setup(m => m.MachineParameters).Returns(new EmPhysicalMachineSettings(){FeedRollerParameters = new EmFeedRollerParameters(1, 1, 1, 1, 1, 45), LongHeadParameters = lhParams});

            var score = AStarNodeEmTestGuy.CalculateMovementScoreForIteration(mock.Object, nodes);

            Specify.That(score)
                .Should.BeEqualTo(lhParams.ConnectDelayOnInSeconds + 101 + lhParams.ConnectDelayOffInSeconds + 201 +
                                  lhParams.ConnectDelayOffInSeconds + 301 + lhParams.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnZeroScoreForStartingPosition()
        {
            var startNode =
                LongHeadDistributionEmTestHelper.GetStartNode(
                    new[]
                    { new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(100, 200), LineType.crease), },
                    new MicroMeter[] { 100, 200, 300 }, machineSettings);
            Specify.That(startNode.Score).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPredictFiveReverses_WhenUsingOneLonghead_OnSixLines()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 105, 110, 115, 120, 125 });
            var longHeadParams = new EmLongHeadParameters(new[] { new EmLongHead() { LeftSideToTool = 47, Number = 1 } }, 1.5, 90, 10, 55, 7, 2464, 50, 54, 143, 15d, 24d);
            this.machineSettings = new EmPhysicalMachineSettings(){FeedRollerParameters = machineSettings.FeedRollerParameters, LongHeadParameters = longHeadParams};

            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, new MicroMeter[] { 50 }, longHeadParams,
                machineSettings);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, longHeadParams) +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) * 5 +
                                  longHeadParams.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnMovementScore_WhenLongHeadIsPositionedOnFirstCoordinate()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 50 });
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, new MicroMeter[] { 100, 200, 300 },
                machineSettings.LongHeadParameters, machineSettings);
            Specify.That(firstNode.Score).Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnZeroScore_WhenLongHeadIsAlreadyPositionedOnFirstCoordinate()
        {
            var lhPos = new MicroMeter[] { 90, 200, 300 };
            var lines = new[]
            {
                new PhysicalLine(new PhysicalCoordinate(90, 0), new PhysicalCoordinate(90, 600), LineType.crease)
            };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            Specify.That(firstNode.Score).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementAndOneReverseScore_WhenSameLongHeadIsPositionedOnSecondCoordinate()
        {
            var lhPos = new MicroMeter[] { 100, 200, 300 };

            var lines = new[]
            {
                new PhysicalLine(new PhysicalCoordinate(90, 0), new PhysicalCoordinate(90, 600), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 600), LineType.crease)
            };

            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos));
            var node = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(node);

            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(node.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
            Specify.That(secondNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) + machineSettings.LongHeadParameters.ConnectDelayOffInSeconds
                    + EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters)
                    + EmScoreCalculator.CalculateMovementScoreForDistance(60, machineSettings.LongHeadParameters) + machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnOneMovementScore_WhenSecondLongHeadIsPositionedOnSecondCoordinate()
        {
            var lhPos = new MicroMeter[] { 100, 150, 300 };
            var lines = new[]
            {
                new PhysicalLine(new PhysicalCoordinate(90, 0), new PhysicalCoordinate(90, 600), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 600), LineType.crease)
            };
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };

            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Specify.That(secondNode.Score).Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnOneMovementScore_WhenFirstLongHeadIsInPosition_AndSecondIsNot()
        {
            var lhPos = new MicroMeter[] { 90, 200, 300 };
            var lines = new[]
            {
                new PhysicalLine(new PhysicalCoordinate(90, 0), new PhysicalCoordinate(90, 600), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 600), LineType.crease)
            };
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };

            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Specify.That(secondNode.Score).Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(50, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnZeroScore_WhenFirstAndSecondLongHeadIsInPosition()
        {
            var lhPos = new MicroMeter[] { 100, 200, 300 };
            var lines = new[]
            {
                new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 600), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 600), LineType.crease)
            };
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };

            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 2, machineSettings.LongHeadParameters, machineSettings);
            Specify.That(secondNode.Score).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoMovementAndOneReverseScore_WhenLongHeadIsPositionedOnSecondCoordinate_AndLongHeadOnFirstCoordinateMustBeMoved()
        {
            var lhPos = new MicroMeter[] { 110, 200, 300 };
            var lines = new[]
            {
                new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 600), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 600), LineType.crease)
            };
            var node = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                node
            };

            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 2, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(node.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
            Specify.That(secondNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(10, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(150, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnOneRevereseAndTwoMovementScore_WhenLongHeadIsPositionedOnThirdCoordinate_AndLongHeadOnFirstAndSecondCoordinateMustBeMoved()
        {
            /* 90__150_200
             * |---|---|
             * |---|---|
             * |---|---|
             * LH1_LH3_LH2
             * 
             * */
            var lhPos = new MicroMeter[] { 700, 760, 1100 };
            var lines = new[]
            {
                new PhysicalLine(new PhysicalCoordinate(90, 0), new PhysicalCoordinate(90, 600), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 600), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 600), LineType.crease),
            };
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings)
            };

            nodesInBranch.Add(LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings));
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 2,
                machineSettings.LongHeadParameters, machineSettings);

            Specify.That(thirdNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(560, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(950, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) * 1, 0.01);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnScoreOfOneReverseAndTwoMovements_WhenLongheadsNeedToBeRepositionedOnce()
        {
            /* 50__100_____250____350
             * |---|--------|------|
             * |---|--------|------|
             * |---|--------|------|
             * LH1_LH1_____LH2____LH2
             * */

            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 50, 100, 250, 350 });
            var lhPos = new MicroMeter[] { 900, 1000 };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(secondNode);
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(thirdNode);
            var fourthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 4, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(850, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters));

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(850, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 0.01);

            Specify.That(thirdNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(100, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(750, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 0.01);

            Specify.That(fourthNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(100, this.machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(750, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, this.machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 0.01);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnScoreOfOneReverseAndTwoMovements_WhenUsingTwoLongHeadsOnThreeLinesAndTwoLinesAreTooClose()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 150, 163 });
            var lhPos = new MicroMeter[] { 500, 700 };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(secondNode);
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(400, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(secondNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(400, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(Math.Round(thirdNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(400, machineSettings.LongHeadParameters) +
                                             machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(137, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                             machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 3));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnScoreOfOneReverseAndTwoMovements_WhenUsingThreeLongHeadsOnSixLines()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 50, 105, 120, 250, 305, 320 });
            var lhPos = new MicroMeter[] { 500, 700, 1000 };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(secondNode);
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(thirdNode);
            var fourthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 4, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(fourthNode);
            var fifthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 5, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(fifthNode);
            var sixthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 6, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(Math.Round(firstNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(450, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters), 3), 0.01);

            Specify.That(Math.Round(secondNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(450, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(55, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 3), 0.01);

            Specify.That(Math.Round(thirdNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(450, this.machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(130, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(55, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 3), 0.01);

            Specify.That(Math.Round(fourthNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(450, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(130, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(55, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(75, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 3), 0.01);

            Specify.That(Math.Round(fifthNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(450, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(130, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(115, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(55, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(75, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 3), 0.01);

            Specify.That(Math.Round(sixthNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(450, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(130, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(115, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(40, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(75, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(15, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 3), 0.01);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnScoreOfTwoReversesAndThreeMovementsOnFourthNodeAndAbove_WhenUsingThreeLongHeadsOnSixLines_AndTwoLinesAreTooClose()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 50, 105, 120, 150, 205, 220 });
            var lhPos = new MicroMeter[] { 100, 200, 300 };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(secondNode);
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(thirdNode);
            var fourthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 4, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(fourthNode);
            var fifthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 5, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(fifthNode);
            var sixthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 6, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(Math.Round(firstNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters), 3));

            Specify.That(Math.Round(secondNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(55, machineSettings.LongHeadParameters), 3) +
                    machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(Math.Round(thirdNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(55, machineSettings.LongHeadParameters), 3) +
                    machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(Math.Round(fourthNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(55, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters), 3) +
                    machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(Math.Round(fifthNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(15, machineSettings.LongHeadParameters) +
                                             machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                             EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                                             machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                             EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(55, machineSettings.LongHeadParameters), 3) +
                                  machineSettings.LongHeadParameters.ConnectDelayOffInSeconds);

            Specify.That(Math.Round(sixthNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(30, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(15, machineSettings.LongHeadParameters) +

                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                             EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(15, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(15, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds +
                                             EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(55, machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOffInSeconds, 3));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBaseScoringForReversesOnSectionLength()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 150, 163 });
            var lhPos = new MicroMeter[] { 500, 700 };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(secondNode);
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(400, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
            Specify.That(secondNode.Score)
                .Should.BeEqualTo(EmScoreCalculator.CalculateMovementScoreForDistance(400, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(50, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
            Specify.That(Math.Round(thirdNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(400, this.machineSettings.LongHeadParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(137, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateMovementScoreForDistance(50, this.machineSettings.LongHeadParameters), 3) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
      
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBaseUpcomingReverseScoringOnRemainningLinesAndLongHeads()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 150, 163 });
            var lhPos = new MicroMeter[] { 100 };
            var lh1 = new EmLongHead() { LeftSideToTool = 47, Number = 1 };

            var longHeadParameters = new EmLongHeadParameters(new[] { lh1 }, 1.5, 90, 10, 55, 7, 2464, 50, 54, 143, 15d, 24d);
            var feedRollerParameters = new EmFeedRollerParameters(1.5, 30, 10, 55, 600, 45);

            var machineSettings = new EmPhysicalMachineSettings(){FeedRollerParameters = feedRollerParameters, LongHeadParameters = longHeadParameters};

            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(secondNode);
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 3, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(Math.Round(firstNode.Score, 3)).Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateReverseScoreForDistance(600, 2, machineSettings.FeedRollerParameters), 3));
            Specify.That(Math.Round(secondNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(50, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters), 3));
            Specify.That(Math.Round(thirdNode.Score, 3))
                .Should.BeEqualTo(
                    Math.Round(
                        EmScoreCalculator.CalculateMovementScoreForDistance(13, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                        EmScoreCalculator.CalculateMovementScoreForDistance(50, this.machineSettings.LongHeadParameters) +
                        machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                        EmScoreCalculator.CalculateReverseScoreForDistance(600, 2, machineSettings.FeedRollerParameters), 3));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotAddReverses_WhenLongheadsMoveInOppositeDirections()
        {
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 15, 20, 120, 170, 500, 700 });
            var lhPos = new MicroMeter[] { 100, 200, 900 };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(secondNode);
            var thirdNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 3, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(thirdNode);
            var fourthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 2, 4, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(fourthNode);
            var fifthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 5, machineSettings.LongHeadParameters, machineSettings);
            nodesInBranch.Add(fifthNode);
            var sixthNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 3, 6, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(Math.Round(firstNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(85, machineSettings.LongHeadParameters) +
                                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters), 3));

            Specify.That(Math.Round(secondNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(85, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds, 3));

            Specify.That(Math.Round(thirdNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds), 3);

            Specify.That(Math.Round(fourthNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(45, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds, 3));

            Specify.That(Math.Round(fifthNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(315, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                  EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(45, machineSettings.LongHeadParameters) +
                                  EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                  machineSettings.LongHeadParameters.ConnectDelayOnInSeconds, 3));

            Specify.That(Math.Round(sixthNode.Score, 3))
                .Should.BeEqualTo(Math.Round(EmScoreCalculator.CalculateMovementScoreForDistance(80, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(315, machineSettings.LongHeadParameters) +
                                             machineSettings.LongHeadParameters.ConnectDelayOnInSeconds +
                                             EmScoreCalculator.CalculateReverseScoreForDistance(600, 1, machineSettings.FeedRollerParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(45, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(5, machineSettings.LongHeadParameters) +
                                             EmScoreCalculator.CalculateMovementScoreForDistance(150, machineSettings.LongHeadParameters) +
                                             machineSettings.LongHeadParameters.ConnectDelayOnInSeconds, 3));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGiveNodesWithASectionLongerThanMaxReverseDistanceWithReversesAHugeScore()
        {
            var maximumReverseScore = 1000000d;
            var lines = LongHeadDistributionEmTestHelper.SetupLinesAt(new double[] { 100, 150, 163 }, 800);
            var lhPos = new MicroMeter[] { 500, 700 };
            var firstNode = LongHeadDistributionEmTestHelper.GetFirstNode(lines, lhPos, machineSettings.LongHeadParameters, machineSettings);
            var nodesInBranch = new SortedSet<IAStarNodeEM>(new CoordinateProcessOrderComparer(lhPos))
            {
                firstNode
            };
            var secondNode = LongHeadDistributionEmTestHelper.CreateNodeForLineWithLongHead(nodesInBranch, lines, lhPos, 1, 2, machineSettings.LongHeadParameters, machineSettings);

            Specify.That(firstNode.Score)
                .Should.BeEqualTo(
                    EmScoreCalculator.CalculateMovementScoreForDistance(400, machineSettings.LongHeadParameters) +
                    machineSettings.LongHeadParameters.ConnectDelayOnInSeconds);
            Specify.That(secondNode.Score > maximumReverseScore).Should.BeTrue();
        
        }
    }
}
