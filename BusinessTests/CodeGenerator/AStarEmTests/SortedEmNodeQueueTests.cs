﻿namespace BusinessTests.CodeGenerator.AStarEmTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using PackNet.Business.CodeGenerator.AStarStuff;

    using Testing.Specificity;

    [TestClass]
    public class SortedEmNodeQueueTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSortLowestScoreFirst()
        {
            var n1 = new Mock<IAStarNodeEM>();
            n1.Setup(em => em.Score).Returns(3000);
            var n2 = new Mock<IAStarNodeEM>();
            n2.Setup(em => em.Score).Returns(2000);
            var queue = new SortedEmNodeQueue();
            queue.Enqueue(new[] {n1.Object, n2.Object});
            
            Specify.That(queue.Dequeue().Score).Should.BeEqualTo(n2.Object.Score);
        }
    }
}
