﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Utils;

namespace BusinessTests.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.AStarStuff;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Common.Interfaces;
    using PackNet.Common.Interfaces.DTO;
    using Testing.Specificity;

    [TestClass]
    public class CodeOptimizerTests
    {
        private const double lhOffset = 147;
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldMergeFeedInstructionsIntoOne_WhenCodeForEmIsOptimized()
        {
            var compensatedDesign = EmCodeGeneratorTestHelper.GetDesignWithReverses();
            var longHeadParams = new EmLongHeadParameters() { LongHeadYOffset = 14d };
            longHeadParams.AddLongHead(new EmLongHead() { Number = 1 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 2 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 3 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 4 });
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100d, longHeadParams, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var codeGenOptimized = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100d, longHeadParams, new CodeOptimizer());
            var machineSettings = GetEmMachineParameters();
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, new MicroMeter[] { 100, 200, 300, 400 }, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number
                    }
                }
            };

            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            var feedLength = 0d;
            instructionList.OfType<InstructionFeedRollerItem>().ForEach(i => feedLength+= i.Position);
            Specify.That(feedLength).Should.BeLogicallyEqualTo(500+longHeadParams.LongHeadYOffset+machineSettings.FeedRollerParameters.OutFeedLength);

            var nrOfFeedInstructionsInARow = 0;
            for (int i = 0; i < instructionList.Count()-1; i++)
            {
                if(instructionList.ElementAt(i) is InstructionFeedRollerItem && instructionList.ElementAt(i+1) is InstructionFeedRollerItem)
                    nrOfFeedInstructionsInARow++;
            }
            Specify.That(nrOfFeedInstructionsInARow).Should.Not.BeLogicallyEqualTo(0, "If this is 0 we can't verify that the optimizer merges feed instructions that follows another feed instruction");
            var optimizedInstr = codeGenOptimized.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            var nrOfFeedInstructionsInARowAfterOptimization = 0;
            for (int i = 0; i < optimizedInstr.Count() - 1; i++)
            {
                if (optimizedInstr.ElementAt(i) is InstructionFeedRollerItem && optimizedInstr.ElementAt(i + 1) is InstructionFeedRollerItem)
                    nrOfFeedInstructionsInARowAfterOptimization++;
            }
            Specify.That(nrOfFeedInstructionsInARowAfterOptimization).Should.BeLogicallyEqualTo(0, "We expect all the feed instructions that follows another feed instruction to be merged");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldRemoveUnnecessaryCrossHeadActivations_WhenCodeForEmIsOptimized()
        {
            var instructionList = new List<InstructionItem>()
            {
                new InstructionCrossHeadMovementItem(100),
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.None},
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.Crease},
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.None},
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.Cut},
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.None},
                new InstructionCrossHeadMovementItem(300),
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.Crease},
                new InstructionCrossHeadMovementItem(400),
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.None},
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.None},
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.None},
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.Cut}
            };

            var optimizedList = new CodeOptimizer().Optimize(instructionList);

            Specify.That(optimizedList.Count()).Should.BeEqualTo(8);
            Specify.That(optimizedList.ElementAt(0)).Should.BeLogicallyEqualTo(instructionList.ElementAt(0));
            Specify.That(optimizedList.ElementAt(1)).Should.BeLogicallyEqualTo(instructionList.ElementAt(2));
            Specify.That(optimizedList.ElementAt(2)).Should.BeLogicallyEqualTo(instructionList.ElementAt(3));
            Specify.That(optimizedList.ElementAt(3)).Should.BeLogicallyEqualTo(instructionList.ElementAt(6));
            Specify.That(optimizedList.ElementAt(4)).Should.BeLogicallyEqualTo(instructionList.ElementAt(7));
            Specify.That(optimizedList.ElementAt(5)).Should.BeLogicallyEqualTo(instructionList.ElementAt(8));
            Specify.That(optimizedList.ElementAt(6)).Should.BeLogicallyEqualTo(instructionList.ElementAt(9));
            Specify.That(optimizedList.ElementAt(7)).Should.BeLogicallyEqualTo(instructionList.ElementAt(13));

        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldRemoveUnnecessaryLongHeadActivations_WhenCodeForEmIsOptimized()
        {
            var instructionList = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(new MicroMeter(200d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None)
                    }),
                new InstructionFeedRollerItem(new MicroMeter(150d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Crease),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.None, ToolStates.Cut)
                    }),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None)
                    }),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Crease),
                        new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.Cut)
                    })
            };

            var optimizedList = new CodeOptimizer().Optimize(instructionList);

            Specify.That(optimizedList.Count()).Should.BeEqualTo(4);
            Specify.That(optimizedList.ElementAt(0)).Should.BeLogicallyEqualTo(instructionList.ElementAt(0));
            Specify.That(optimizedList.ElementAt(1)).Should.BeLogicallyEqualTo(instructionList.ElementAt(1));
            Specify.That(optimizedList.ElementAt(2)).Should.BeLogicallyEqualTo(instructionList.ElementAt(2));
            Specify.That((optimizedList.ElementAt(3) as InstructionLongHeadToolActivations).ToolActivations.Count()).Should.BeLogicallyEqualTo(4);
            Specify.That((optimizedList.ElementAt(3) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(0).ToolState).Should.BeLogicallyEqualTo((instructionList.ElementAt(5) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(0).ToolState);
            Specify.That((optimizedList.ElementAt(3) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(1).ToolState).Should.BeLogicallyEqualTo((instructionList.ElementAt(5) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(1).ToolState);
            Specify.That((optimizedList.ElementAt(3) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(2).ToolState).Should.BeLogicallyEqualTo((instructionList.ElementAt(5) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(2).ToolState);
            Specify.That((optimizedList.ElementAt(3) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(3).ToolState).Should.BeLogicallyEqualTo((instructionList.ElementAt(5) as InstructionLongHeadToolActivations).ToolActivations.ElementAt(3).ToolState);
            
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldRemoveUnnecessaryLongHeadDeactivations_WhenCodeForEmIsOptimized()
        {
            var instructionList = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(new MicroMeter(200d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None)
                    }),
                new InstructionFeedRollerItem(new MicroMeter(150d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Crease),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.None, ToolStates.Cut)
                    }),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None)
                    }),
                    new InstructionFeedRollerItem(new MicroMeter(150d)),
                                    new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Crease),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.None, ToolStates.Cut)
                    }),
            };

            var optimizedList = new CodeOptimizer().Optimize(instructionList);

            Specify.That(optimizedList.Count()).Should.BeEqualTo(4);
            Specify.That(optimizedList.ElementAt(0)).Should.BeLogicallyEqualTo(instructionList.ElementAt(0));
            Specify.That((optimizedList.Last() as InstructionLongHeadToolActivations).ToolActivations.All(a => a.ToolState == ToolStates.None)).Should.BeTrue("There should only be one instruction where all longheads are deactivated and that's the one at the end");
            Specify.That(optimizedList.Count(i => i is InstructionLongHeadToolActivations && (i as InstructionLongHeadToolActivations).ToolActivations.All(a => a.ToolState == ToolStates.None))).Should.BeLogicallyEqualTo(1, "There should only be one instruction where all longheads are deactivated and that's the one at the end");
            

        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("Bug 10171")]
        public void ShouldNotRemoveNecessaryLongHeadActivations_WhenCodeForEmIsOptimized()
        {
            var instructionList = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(new MicroMeter(200d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.None)
                    }),
                new InstructionFeedRollerItem(new MicroMeter(150d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease),
                    }),
                new InstructionFeedRollerItem(new MicroMeter(150d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.Crease),
                    }),
                new InstructionFeedRollerItem(new MicroMeter(150d)),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Cut),
                        new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Cut),
                        new LongHeadToolActivationInformation(3, ToolStates.None, ToolStates.Crease),
                        new LongHeadToolActivationInformation(4, ToolStates.None, ToolStates.Crease)
                    })
            };

            var optimizedList = new CodeOptimizer().Optimize(instructionList);

            Specify.That(optimizedList.Count()).Should.BeEqualTo(8);
            Specify.That(optimizedList.ElementAt(0)).Should.BeLogicallyEqualTo(instructionList.ElementAt(0));
            Specify.That(optimizedList.ElementAt(1)).Should.BeLogicallyEqualTo(instructionList.ElementAt(1));
            Specify.That(optimizedList.ElementAt(2)).Should.BeLogicallyEqualTo(instructionList.ElementAt(2));
            Specify.That(optimizedList.ElementAt(3)).Should.BeLogicallyEqualTo(instructionList.ElementAt(3));
            Specify.That(optimizedList.ElementAt(4)).Should.BeLogicallyEqualTo(instructionList.ElementAt(4));
            Specify.That(optimizedList.ElementAt(5)).Should.BeLogicallyEqualTo(instructionList.ElementAt(5));
            Specify.That(optimizedList.ElementAt(6)).Should.BeLogicallyEqualTo(instructionList.ElementAt(6));
            Specify.That(optimizedList.ElementAt(7)).Should.BeLogicallyEqualTo(instructionList.ElementAt(7));
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug 11118")]
        public void ShouldMergeConsecutiveLongHeadToolActivation_IntoOnLongHeadToolActivationWithLastLongHeadStateForEachTool()
        {
            var instructionList = new List<InstructionItem>()
            {
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.None),
                        new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.None),
                    }),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease),
                        new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.Crease),
                        new LongHeadToolActivationInformation(5, ToolStates.Cut, ToolStates.Crease),
                        new LongHeadToolActivationInformation(6, ToolStates.Crease, ToolStates.None),
                    }),
                new InstructionLongHeadToolActivations(
                    new List<LongHeadToolActivationInformation>()
                    {
                        new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.Crease),
                        new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.None),
                    })
            };

            var optimizedList = new CodeOptimizer().Optimize(instructionList);

            Specify.That(optimizedList.Count()).Should.BeEqualTo(1);
            var lhActivation = optimizedList.First() as InstructionLongHeadToolActivations;
            Specify.That(lhActivation).Should.Not.BeNull();
            Specify.That(lhActivation.ToolActivations.Count()).Should.BeEqualTo(6);
            Specify.That(lhActivation.ToolActivations.Count(t => t.LongHeadNumber == 1 && t.ToolState == ToolStates.Cut && t.PreviousState == ToolStates.None)).Should.BeEqualTo(1);
            Specify.That(lhActivation.ToolActivations.Count(t => t.LongHeadNumber == 2 && t.ToolState == ToolStates.Cut && t.PreviousState == ToolStates.None)).Should.BeEqualTo(1);
            Specify.That(lhActivation.ToolActivations.Count(t => t.LongHeadNumber == 3 && t.ToolState == ToolStates.Cut && t.PreviousState == ToolStates.None)).Should.BeEqualTo(1);
            Specify.That(lhActivation.ToolActivations.Count(t => t.LongHeadNumber == 4 && t.ToolState == ToolStates.Crease && t.PreviousState == ToolStates.None)).Should.BeEqualTo(1);
            Specify.That(lhActivation.ToolActivations.Count(t => t.LongHeadNumber == 5 && t.ToolState == ToolStates.Cut && t.PreviousState == ToolStates.None)).Should.BeEqualTo(1);
            Specify.That(lhActivation.ToolActivations.Count(t => t.LongHeadNumber == 6 && t.ToolState == ToolStates.Crease && t.PreviousState == ToolStates.None)).Should.BeEqualTo(1);
        }

        private EmPhysicalMachineSettings GetEmMachineParameters()
        {
            var longHeads = new List<EmLongHead>();
            longHeads.Add(new EmLongHead() { LeftSideToTool = 47, MaximumPosition = 1200, MinimumPosition = 0, Number = 1, Position = 100, Width = 54 });
            longHeads.Add(new EmLongHead() { LeftSideToTool = 47, MaximumPosition = 1200, MinimumPosition = 0, Number = 2, Position = 100, Width = 54 });
            longHeads.Add(new EmLongHead() { LeftSideToTool = 7, MaximumPosition = 1200, MinimumPosition = 0, Number = 3, Position = 100, Width = 54 });
            longHeads.Add(new EmLongHead() { LeftSideToTool = 47, MaximumPosition = 1200, MinimumPosition = 0, Number = 4, Position = 100, Width = 54 });
            var longHeadParameters = new EmLongHeadParameters(longHeads, 3.6, 1, 0.55, 10, 7, 1200, 0, 54, 143, 15d, 24d);
            var rollerParameters = new EmFeedRollerParameters(1.5, 30, 1, 55, 600, 45);
            var crossHeadParameters = new EmCrossHeadParameters()
            {
                MovementData = new EmNormalMovementData()
                {
                    Acceleration = 15126,
                    Speed = 3600,
                    NormalMovement =
                        new EmMovementData()
                        {
                            AccelerationInPercent = 100,
                            SpeedInPercent = 100,
                            Torque = 100
                        }
                }
            };
            var emMachineParameters = new EmPhysicalMachineSettings() { CrossHeadParameters = crossHeadParameters, FeedRollerParameters = rollerParameters, LongHeadParameters = longHeadParameters};
            
            return emMachineParameters;
        }


    }
}
