﻿@CodeOptimizer
Feature: Code Generator Optimizer
	In order to simplify code generation
	As a developer
	I want a optimizer to remove unnecessary instruction from the instruction list

Scenario: Should remove unnecessary crosshead move and tool activation instructions
	Given instruction list with crosshead positioning to the same position without feed in between
	And a code generator optimizer
	When I optimize the instruction list
	Then unnecessary crosshead move and tool activation instructions are removed

Scenario: Should remove unnecessary crosshead move
	Given instruction list with crosshead positioning to the same position with feed in between
	And a code generator optimizer
	When I optimize the instruction list
	Then unnecessary crosshead move instructions are removed

	Scenario: Should merge crosshead moves with same tool state when disconnect track is set on the first but not on the second movement
	Given an instruction list with sequential crosshead positioning that have same tool state but disconnect track is set on the first movement
	And a code generator optimizer
	When I optimize the instruction list
	Then crosshead positionings are merged to one
	And disconnect track is set
	And tool activations are merged to one

	Scenario: Should merge crosshead moves with same tool state when disconnect track is the second but not on first the movement
	Given an instruction list with sequential crosshead positioning that have same tool state but disconnect track is set on the second movement
	And a code generator optimizer
	When I optimize the instruction list
	Then crosshead positionings are merged to one
	And disconnect track is set
	And tool activations are merged to one

	Scenario: Should not merge crosshead moves with same tool state when disconnect track differs and moves are separated by feed
	Given an instruction list with sequential crosshead positioning that have same tool state but disconnect track differs and are separated by feed
	And a code generator optimizer
	When I optimize the instruction list
	Then crosshead positionings are not merged to one
	And tool activations are merged to one

	Scenario: Should not merge crosshead moves with same tool state when simultaneous with feed differs
	Given an instruction list with sequential crosshead positioning that have same tool state but simultaneous with feed differs
	And a code generator optimizer
	When I optimize the instruction list
	Then crosshead positionings are not merged
	And tool activations are merged to one