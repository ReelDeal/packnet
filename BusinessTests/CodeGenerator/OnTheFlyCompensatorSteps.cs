﻿namespace BusinessTests.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class OnTheFlyCompensatorSteps
    {
        [Given(@"an instruction list with cross head movements and tool activations")]
        public void GivenAnInstructionListWithCrossHeadMovementsAndToolActivations()
        {
            var otfItemOne = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 350 };
            otfItemOne.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(100.0, ToolStates.Cut));
            otfItemOne.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(170.0, ToolStates.Crease));
            
            var crossHeadMovementPosition = new InstructionCrossHeadMovementItem(1400);

            var otfItemTwo = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 0 };
            otfItemTwo.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(250.0, ToolStates.Cut));

            var otfItemThree = new InstructionCrossHeadOnTheFlyItem { FinalPosition = 1400 };
            otfItemThree.AddOnTheFlyPosition(new CrossHeadOnTheFlyPosition(350, ToolStates.Cut));

            var originalInstructionList = new List<InstructionItem> { new InstructionMetaDataItem(1, 2)};
            originalInstructionList.Add(otfItemOne);
            originalInstructionList.Add(new InstructionFeedRollerItem());
            originalInstructionList.Add(crossHeadMovementPosition);
            originalInstructionList.Add(otfItemTwo);
            originalInstructionList.Add(new InstructionFeedRollerItem());
            originalInstructionList.Add(otfItemThree);

            var endItem = new InstructionCrossHeadToolActivation();
            originalInstructionList.Add(endItem);

            ScenarioContext.Current.Add("OriginalInstructionList", originalInstructionList);
            ScenarioContext.Current.Add("FirstPosition", otfItemOne.Positions.ElementAt(0).Position);
            ScenarioContext.Current.Add("SecondPosition", otfItemOne.Positions.ElementAt(1).Position);
            ScenarioContext.Current.Add("FinalPositionFirstOtf", otfItemOne.FinalPosition);
        }

        [Given(@"velocity parameters")]
        public void GivenVelocityParameters()
        {
            ScenarioContext.Current.Add("MaxAcceleration", 10000.0);
            ScenarioContext.Current.Add("MaxDeceleration", 10000.0);
            ScenarioContext.Current.Add("MaxSpeed", 1800.0);
            ScenarioContext.Current.Add("ToolActivationDelayInMilliseconds", 10.0);
            ScenarioContext.Current.Add("ToolDectivationDelayInMilliseconds", 10.0);
        }

        [Given(@"first otf instruction reaches max speed")]
        public void GivenFirstOtfInstructionReachesMaxSpeed()
        {
            var acceleration = (double)ScenarioContext.Current["MaxAcceleration"];
            var timeInAcceleration = (double)ScenarioContext.Current["MaxSpeed"] / acceleration;

            var distanceInAcceleration = (acceleration * timeInAcceleration * timeInAcceleration) / 2;

            var finalPosition = (MicroMeter)ScenarioContext.Current["FinalPositionFirstOtf"];
            var deceleration = (double)ScenarioContext.Current["MaxDeceleration"];
            var timeInDeceleration = (double)ScenarioContext.Current["MaxSpeed"] / deceleration;
            var distanceInDeceleration = (deceleration * timeInDeceleration * timeInDeceleration) / 2;

            Specify.That(finalPosition - 0 > distanceInAcceleration + distanceInDeceleration).Should.BeTrue();
        }

        [Given(@"length of movment to tool change for first otf instruction is shorter than the time to accelerate to full speed")]
        public void GivenLengthOfMovmentToToolChangeForFirstOtfInstructionIsShorterThanTheTimeToAccelerateToFullSpeed()
        {
            var acceleration = (double)ScenarioContext.Current["MaxAcceleration"];
            var timeInAcceleration = (double)ScenarioContext.Current["MaxSpeed"] / acceleration;

            var distance = (acceleration * timeInAcceleration * timeInAcceleration) / 2;

            var firstPosition = (MicroMeter)ScenarioContext.Current["FirstPosition"];
            Specify.That(firstPosition < distance).Should.BeTrue();
        }

        [Given(@"time to second tool change for first otf instruction is less than delay time units into full speed movement")]
        public void GivenTimeToSecondToolChangeForFirstOtfInstructionIsLessThanDelayTimeUnitsIntoFullSpeedMovement()
        {
            var acceleration = (double)ScenarioContext.Current["MaxAcceleration"];
            var speed = (double)ScenarioContext.Current["MaxSpeed"];
            var timeInAcceleration = speed / acceleration;

            var distanceInAcceleration = (acceleration * timeInAcceleration * timeInAcceleration) / 2;
            var secondToolChangePosition = (MicroMeter)ScenarioContext.Current["SecondPosition"];
            var distanceInFullSpeed = secondToolChangePosition - distanceInAcceleration;
            var timeInFullSpeed = distanceInFullSpeed / speed;
            var delay = (double)ScenarioContext.Current["ToolDectivationDelayInMilliseconds"];
            Specify.That(timeInFullSpeed < delay).Should.BeTrue();
        }

        [When(@"I compensate for velocity")]
        public void WhenICompensateForVelocity()
        {
            var compensator = new CrossHeadOnTheFlyCompensator();

            var compensatedInstructionList = compensator.CompensateForVelocity(
                ScenarioContext.Current["OriginalInstructionList"] as IEnumerable<InstructionItem>,
                0,
                (double)ScenarioContext.Current["MaxAcceleration"],
                (double)ScenarioContext.Current["MaxDeceleration"],
                (double)ScenarioContext.Current["MaxSpeed"], new OtfDelayHandler(
                (double)ScenarioContext.Current["ToolActivationDelayInMilliseconds"],
                (double)ScenarioContext.Current["ToolDectivationDelayInMilliseconds"],
                (double)ScenarioContext.Current["ToolActivationDelayInMilliseconds"],
                (double)ScenarioContext.Current["ToolDectivationDelayInMilliseconds"]));
            
            ScenarioContext.Current.Add("CompensatedInstructionList", compensatedInstructionList);
        }

        [Then(@"the instruction order should remain the same")]
        public void ThenTheInstructionOrderShouldRemainTheSame()
        {
            var instructions = (ScenarioContext.Current["CompensatedInstructionList"] as IEnumerable<InstructionItem>);

            Specify.That(instructions.Count()).Should.BeEqualTo(8);

            Specify.That(instructions.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructions.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));

            var otfItem = instructions.ElementAt(1) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(350);
            
            Specify.That(instructions.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructions.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));

            var otfItem2 = instructions.ElementAt(4) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem2.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem2.FinalPosition).Should.BeLogicallyEqualTo(0);

            Specify.That(instructions.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            
            var otfItem3 = instructions.ElementAt(6) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem3.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfItem3.FinalPosition).Should.BeLogicallyEqualTo(1400);

            Specify.That(instructions.ElementAt(7)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
        }

        [Then(@"the first instruction of the first otf movement is compensated for full acceleration")]
        public void ThenTheFirstInstructionOfTheFirstOtfMovementIsCompensatedForFullAcceleration()
        {
            var instructions = (ScenarioContext.Current["CompensatedInstructionList"] as IEnumerable<InstructionItem>);

            var otfItem = instructions.ElementAt(1) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(86.358);
            Specify.That(otfItem.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Cut);
        }

        [Then(@"the second instruction of the first otf movement is compensated for full acceleration and full speed")]
        public void TheSecondInstructionOfTheFirstOtfMovementIsCompensatedForFullAccelerationAndFullSpeed()
        {
            var instructions = (ScenarioContext.Current["CompensatedInstructionList"] as IEnumerable<InstructionItem>);

            var otfItem = instructions.ElementAt(1) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(152.154);
            Specify.That(otfItem.Positions.ElementAt(1).ToolState).Should.BeEqualTo(ToolStates.Crease);
        }

        [Then(@"the second otf instruction is compensated for full speed in negative direction")]
        public void ThenTheSecondOtfInstructionIsCompensatedForFullSpeedInNegativeDirection()
        {
            var instructions = (ScenarioContext.Current["CompensatedInstructionList"] as IEnumerable<InstructionItem>);
            
            var otfItem2 = instructions.ElementAt(4) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem2.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(268);
            Specify.That(otfItem2.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Cut);
        }

        [Then(@"the third otf instruction is compensated for full speed")]
        public void ThenTheThirdOtfInstructionIsCompensatedForFullSpeed()
        {
            var instructions = (ScenarioContext.Current["CompensatedInstructionList"] as IEnumerable<InstructionItem>);
            var otfItem3 = instructions.ElementAt(6) as InstructionCrossHeadOnTheFlyItem;
            Specify.That(otfItem3.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(332);
            Specify.That(otfItem3.Positions.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Cut);
            Specify.That(otfItem3.FinalPosition).Should.BeLogicallyEqualTo(1400);
        }
    }
}