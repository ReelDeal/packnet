﻿Feature: Conversion from tool position to sensor position
	In order to not have to consider sensor to tool in later steps
	As a proogrammer
	I want to be able to conver tool positions to sensor positions

@ToolToSensorPositionConversion
	Scenario: Convert a list of tool position for a longhead to sensor position
	Given longhead parameters with left side to sensor pin length 5.2
	And a number of longheads with left side to tool offset
	| Lh | LeftSideToToolOffset |
	| 1  | 57.5           |
	| 3  | 0.0            |
	| 6  | 64.0           |
	When I convert the list of positions
	| Lh | Pos    |
	| 1  | -100.1 |
	| 3  | -100.1 |
	| 6  | -100.1 |
	| 1  | 0.0    |
	| 3  | 0.0    |
	| 6  | 0.0    |
	| 1  | 115.2  |
	| 3  | 115.2  |
	| 6  | 115.2  |
	Then the converted list of positions should be
	| Lh | Pos    |
	| 1  | -152.4 |
	| 3  | -94.9  |
	| 6  | -158.9 |
	| 1  | -52.3  |
	| 3  | 5.2    |
	| 6  | -58.8  |
	| 1  | 62.9   |
	| 3  | 120.4  |
	| 6  | 56.4   |