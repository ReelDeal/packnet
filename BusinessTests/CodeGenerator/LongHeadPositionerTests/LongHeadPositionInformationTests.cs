﻿namespace BusinessTests.CodeGenerator.LongHeadPositionerTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    using Testing.Specificity;

    [TestClass]
    public class LongHeadPositionInformationTests
    {
        [TestMethod]
        public void ShouldReturnFalseForEqualsWithObjectThatIsNull()
        {
            object nullObject = null;
            var sut = new LongHeadPositioningInformation(1, 0, 0);
            Specify.That(sut.Equals(nullObject)).Should.BeEqualTo(false);
        }

        [TestMethod]
        public void ShouldReturnTrueForEqualsWithItselfAsObject()
        {
            var sut = new LongHeadPositioningInformation(1, 0, 0);
            var sutAsObject = sut as object;
            Specify.That(sut.Equals(sutAsObject)).Should.BeEqualTo(true);
        }

        [TestMethod]
        public void ShouldReturnFalseForEqualsWithObjectThatHasOtherTypeThanLongheadMovement()
        {
            var sut = new LongHeadPositioningInformation(1, 0, 0);
            object i = 5;
            Specify.That(sut.Equals(i)).Should.BeEqualTo(false);
        }

        [TestMethod]
        public void ShouldReturnTrueForEqualsWithOtherLongHeadMovementThatHasSameNumberAndPositionAsObject()
        {
            var sut = new LongHeadPositioningInformation(1, 0, 0);
            object itemWithSameValuesAsSut = new LongHeadPositioningInformation(1, 0, 0);
            Specify.That(itemWithSameValuesAsSut.Equals(sut)).Should.BeEqualTo(true);
        }

        [TestMethod]
        public void ShouldReturnSameHashCodeForSameValues()
        {
            var sut = new LongHeadPositioningInformation(1, 0, 0);
            object itemWithSameValuesAsSut = new LongHeadPositioningInformation(1, 0, 0);
            Specify.That(sut.GetHashCode()).Should.BeEqualTo(itemWithSameValuesAsSut.GetHashCode());
        }

        [TestMethod]
        public void ShouldReturnDifferentHashCodeForDifferentLongHeadNumber()
        {
            var sut = new LongHeadPositioningInformation(1, 0, 0);
            object itemWithDifferentLongheadNumber = new LongHeadPositioningInformation(2, 0, 0);
            Specify.That(sut.GetHashCode()).Should.Not.BeEqualTo(itemWithDifferentLongheadNumber.GetHashCode());
        }

        [TestMethod]
        public void ShouldReturnDifferentHashCodeForDifferentPosition()
        {
            var sut = new LongHeadPositioningInformation(1, 0, 0);
            object itemWithDifferentPosition = new LongHeadPositioningInformation(1, 1, 0);
            Specify.That(sut.GetHashCode()).Should.Not.BeEqualTo(itemWithDifferentPosition.GetHashCode());
        }

        [TestMethod]
        public void ToStringShouldWriteDataInAHumanFriendlyForm()
        {
            var sut = new LongHeadPositioningInformation(1, 0.1, 0);
            Specify.That(sut.ToString()).Should.BeEqualTo("LongHeadNumber: 1, Position: 0.1");
        }
    }
}