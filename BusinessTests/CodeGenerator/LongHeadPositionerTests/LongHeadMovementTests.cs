﻿namespace BusinessTests.CodeGenerator.LongHeadPositionerTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    using Testing.Specificity;

    [TestClass]
    public class LongHeadMovementTests
    {
        private const int ThisLh = 1;
        private static int otherLh;

        public static LongHeadPositioningInformation OtherAt(int position)
        {
            otherLh = 2;
            return new LongHeadPositioningInformation(otherLh, position, 64);
        }

        public LongHeadMovement MovementInPositiveDirection(int start, int end)
        {
            return new LongHeadMovement(new LongHeadPositioningInformation(ThisLh, start, 64), new LongHeadPositioningInformation(ThisLh, end, 64).WithToolWidth(64));
        }

        public LongHeadMovement MovementInNegativeDirection(int start, int end)
        {
            return new LongHeadMovement(new LongHeadPositioningInformation(ThisLh, start, 64), new LongHeadPositioningInformation(ThisLh, end, 64).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsFalseForSameLongHead()
        {
            var sut = MovementInPositiveDirection(0, 100);
            Specify.That(sut.IsBlockedBy(new LongHeadPositioningInformation(ThisLh, 50, 64))).Should.BeLogicallyEqualTo(false);
        }

        [TestMethod]
        public void IsBlockedByReportsFalseForOtherLongHeadOcurringBeforeStartInPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldNotBeBlockedBy(OtherAt(-100).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtToolWidthBeforeStartInPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldNotBeBlockedBy(OtherAt(-64).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringBeforeStartButWithinToolWidthInPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldNotBeBlockedBy(OtherAt(-63).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtStartPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldBeBlockedBy(OtherAt(0).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringBetweenStartAndEndInPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldBeBlockedBy(OtherAt(50).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtEndPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldBeBlockedBy(OtherAt(100).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAfterEndButWithinToolWidthInPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldBeBlockedBy(OtherAt(163).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtToolWidthAfterEndInPositiveDirection()
        {
            MovementInPositiveDirection(0, 100).ShouldNotBeBlockedBy(OtherAt(164).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsFalseForOtherLongHeadOcurringAfterEndInPositiveDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldNotBeBlockedBy(OtherAt(200).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsFalseForOtherLongHeadOcurringBeforeStartInNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldNotBeBlockedBy(OtherAt(200).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtToolWidthBeforeStartInNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldNotBeBlockedBy(OtherAt(164).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringBeforeStartButWithinToolWidthInNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldNotBeBlockedBy(OtherAt(163).WithToolWidth(64)); //changed
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtStartNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldBeBlockedBy(OtherAt(100).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringBetweenStartAndEndInNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldBeBlockedBy(OtherAt(50).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtEndNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldBeBlockedBy(OtherAt(0).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAfterEndButWithinToolWidthInNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldBeBlockedBy(OtherAt(-63).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsTrueForOtherLongHeadOcurringAtToolWidthAfterEndInNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldNotBeBlockedBy(OtherAt(-64).WithToolWidth(64));
        }

        [TestMethod]
        public void IsBlockedByReportsFalseForOtherLongHeadOcurringAfterEndInNegativeDirection()
        {
            MovementInNegativeDirection(100, 0).ShouldNotBeBlockedBy(OtherAt(-100).WithToolWidth(64));
        }
    }
}