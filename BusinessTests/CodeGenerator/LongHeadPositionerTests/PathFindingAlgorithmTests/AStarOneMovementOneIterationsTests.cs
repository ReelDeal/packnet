namespace BusinessTests.CodeGenerator.LongHeadPositionerTests.PathFindingAlgorithmTests
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    using Testing.Specificity;

    [TestClass]
    public class AStarOneMovementOneIterationsTests
    {
        private State start;
        private State end;
        private AStarPathFinder sut;
        private LongHeadPositioningInformation longheadBeforeMove;
        private LongHeadPositioningInformation longheadAfterMove;

        [TestInitialize]
        public virtual void SetUp()
        {
            // TODO: How to handle cross head targetState positioning in algorithm
            longheadBeforeMove = new LongHeadPositioningInformation(1, 102.33, 0);
            start = new State(102.33, new List<LongHeadPositioningInformation> { longheadBeforeMove }, null);
            longheadAfterMove = new LongHeadPositioningInformation(1, 104.33, 0);
            end = new State(104.33, new List<LongHeadPositioningInformation> { longheadAfterMove }, null);
            sut = new AStarPathFinder(start, end);
            sut.RunNumberOfIterations(1);
        }

        [TestMethod]
        public void ShouldReportThatPathIsFound()
        {
            Specify.That(sut.IsPathFound()).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldReportExpectedMovement()
        {
            var stateInformations = sut.BestChoice().ToList();
            Assert.AreEqual(2, stateInformations.Count);
            var expectedFirstItem = stateInformations.First();
            Specify.That(expectedFirstItem.Item2.CrossHeadPosition).Should.BeLogicallyEqualTo(start.CrossHeadPosition);
            Specify.That(expectedFirstItem.Item2.LongHeadPositions).Should.BeLogicallyEqualTo(start.LongHeadPositions);
            Specify.That(expectedFirstItem.Item2.MovedLongHead).Should.BeLogicallyEqualTo(null);
            var expectedLastItem = stateInformations.Last();
            Specify.That(expectedLastItem.Item2.CrossHeadPosition).Should.BeLogicallyEqualTo(end.CrossHeadPosition);
            Specify.That(expectedLastItem.Item2.LongHeadPositions).Should.BeLogicallyEqualTo(end.LongHeadPositions);
            Specify.That(expectedLastItem.Item2.MovedLongHead).Should.BeLogicallyEqualTo(longheadAfterMove);
        }
    }
}