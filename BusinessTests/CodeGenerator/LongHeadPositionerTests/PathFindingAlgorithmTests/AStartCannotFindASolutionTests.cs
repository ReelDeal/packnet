﻿namespace BusinessTests.CodeGenerator.LongHeadPositionerTests.PathFindingAlgorithmTests
{
    using System.Collections.Generic;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    using Testing.Specificity;

    [TestClass]
    public class AStartCannotFindASolutionTests
    {
        [TestMethod]
        public void ShouldThrowException()
        {
            var start = new State(
                0,
                new List<LongHeadPositioningInformation>
                    {
                        new LongHeadPositioningInformation(1, 100, 64),
                        new LongHeadPositioningInformation(2, 200, 64)
                    },
                null);
            var end = new State(
                0,
                new List<LongHeadPositioningInformation>
                    {
                    new LongHeadPositioningInformation(1, 800, 64),
                    new LongHeadPositioningInformation(2, 800, 64)
                    },
                null);
            var sut = new AStarPathFinder(start, end);
            sut.FindBestSolution();
            Specify.ThatAction(() => sut.GetBestPath()).Should.HaveThrown(typeof(NoMovementOrderCouldBeFoundException));
        }

        public void ShouldNotThrowExceptionWhenStartingPositionsAreOverlappingAndMovingInPositioveDirection()
        {
            var start = new State(
                0,
                new List<LongHeadPositioningInformation> {
                    new LongHeadPositioningInformation(1, 65.2, 64),
                    new LongHeadPositioningInformation(2, 290.8, 64),
                    new LongHeadPositioningInformation(3, 709.2, 64),
                    new LongHeadPositioningInformation(4, 754.8, 64),
                    new LongHeadPositioningInformation(5, 837.2, 64),
                    new LongHeadPositioningInformation(6, 924.8, 64)
                },
                null);

            var end = new State(
                0,
                new List<LongHeadPositioningInformation>
                    {
                    new LongHeadPositioningInformation(1, 65.2, 64),
                    new LongHeadPositioningInformation(2, 290.8, 64),
                    new LongHeadPositioningInformation(3, 709.2, 64),
                    new LongHeadPositioningInformation(4, 782.8, 64),
                    new LongHeadPositioningInformation(5, 846.8, 64),
                    new LongHeadPositioningInformation(6, 1024.8, 64)
                    },
                null);
            var sut = new AStarPathFinder(start, end);
            sut.FindBestSolution();

            Specify.ThatAction(() => sut.GetBestPath()).Should.Not.HaveThrown();
        }

        public void ShouldNotThrowExceptionWhenStartingPositionsAreOverlappingAndMovingInNegativeDirection()
        {
            var start = new State(
                0,
                new List<LongHeadPositioningInformation> {
                    new LongHeadPositioningInformation(1, 65.2, 64),
                    new LongHeadPositioningInformation(2, 290.8, 64),
                    new LongHeadPositioningInformation(3, 709.2, 64),
                    new LongHeadPositioningInformation(4, 754.8, 64),
                    new LongHeadPositioningInformation(5, 837.2, 64),
                    new LongHeadPositioningInformation(6, 924.8, 64)
                },
                null);

            var end = new State(
                0,
                new List<LongHeadPositioningInformation>
                    {
                    new LongHeadPositioningInformation(1, 65.2, 64),
                    new LongHeadPositioningInformation(2, 290.8, 64),
                    new LongHeadPositioningInformation(3, 500.2, 64),
                    new LongHeadPositioningInformation(4, 754.8, 64),
                    new LongHeadPositioningInformation(5, 837.2, 64),
                    new LongHeadPositioningInformation(6, 924.8, 64)
                    },
                null);
            var sut = new AStarPathFinder(start, end);
            sut.FindBestSolution();

            Specify.ThatAction(() => sut.GetBestPath()).Should.Not.HaveThrown();
        }
    }
}