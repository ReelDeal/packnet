﻿namespace BusinessTests.CodeGenerator.LongHeadPositionerTests.PathFindingAlgorithmTests
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    using Testing.Specificity;

    [TestClass]
    public class AStarOneMovementZeroIterationsTests
    {
        private State start;
        private State end;
        private AStarPathFinder sut;

        [TestInitialize]
        public virtual void SetUp()
        {
            start = new State(102.33, new List<LongHeadPositioningInformation> { new LongHeadPositioningInformation(1, 102.33, 0) }, null);
            end = new State(104.33, new List<LongHeadPositioningInformation> { new LongHeadPositioningInformation(1, 104.33, 0) }, null);
            sut = new AStarPathFinder(start, end);
            sut.RunNumberOfIterations(0);
        }

        [TestMethod]
        public void ShouldReturnEmptyListAsClosedListAtIterationZero()
        {
            Specify.That(sut.ClosedList.Count).Should.BeLogicallyEqualTo(0);
        }

        [TestMethod]
        public void ShouldReturnListWithEndItemAsOpenListAtIterationZero()
        {
            Specify.That(sut.OpenList.Count).Should.BeLogicallyEqualTo(1);
            var firstOpen = sut.OpenList.First();
            Specify.That(firstOpen.CurrentState.LongHeadPositions).Should.BeLogicallyEqualTo(end.LongHeadPositions);
            Specify.That(firstOpen.CurrentState.CrossHeadPosition).Should.BeLogicallyEqualTo(end.CrossHeadPosition);
        }

        [TestMethod]
        public void ShouldSetParentNodeAsExpectedOnItemInOpenList()
        {
            var firstOpen = sut.OpenList.First();
            Specify.That<Node>(new StartNode(start)).Should.BeLogicallyEqualTo(firstOpen.ParentNode);
        }

        [TestMethod]
        public void ShouldCalculateCostFromStartTo2ForStartingPosition()
        {
            var firstOpen = sut.OpenList.First();
            Specify.That(firstOpen.ParentNode.EstimatedCost(end)).Should.BeLogicallyEqualTo(2);
        }

        [TestMethod]
        public void ShouldCalculateCostFromEndToTwoForEndPosition()
        {
            var firstOpen = sut.OpenList.First();
            Specify.That(firstOpen.EstimatedCost(end)).Should.BeLogicallyEqualTo(2);
        }
    }
}