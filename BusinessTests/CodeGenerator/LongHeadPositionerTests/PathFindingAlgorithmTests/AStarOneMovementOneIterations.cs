namespace BusinessTests.CodeGenerator.LongHeadPositionerTests.PathFindingAlgorithmTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class AStarTwoMovementOneIterations
    {
        private State start;

        private State end;

        private AStarPathFinder sut;

        [TestInitialize]
        public virtual void SetUp()
        {
            // TODO: How to handle cross head end positioning in algorithm
            start = new State(102.33, new List<LongHeadPositioningInformation> { new LongHeadPositioningInformation(1, 102.33, 0), new LongHeadPositioningInformation(2, 105.33, 0) }, null);
            end = new State(107.33, new List<LongHeadPositioningInformation> { new LongHeadPositioningInformation(1, 104.33, 0), new LongHeadPositioningInformation(2, 107.33, 0) }, null);
            sut = new AStarPathFinder(start, end);

            // TODO: Should not add movement more than once...
            sut.RunNumberOfIterations(4);
        }

        [TestMethod]
        public void ShouldReportThatPathIsFound()
        {
            Specify.That(sut.IsPathFound()).Should.BeTrue();
        }

        [TestMethod]
        public void SholdReportExpectedMovement()
        {
            var stateTwo = new Tuple<MicroMeter, List<LongHeadPositioningInformation>>(104.33, new List<LongHeadPositioningInformation> { new LongHeadPositioningInformation(1, 104.33, 0), new LongHeadPositioningInformation(2, 105.33, 0) });
            var bestPath = sut.BestChoice();

            Assert.AreEqual(3, bestPath.Count());
            Assert.AreEqual(start.CrossHeadPosition, bestPath.ElementAt(0).Item2.CrossHeadPosition);
            CollectionAssert.AreEqual(start.LongHeadPositions, bestPath.ElementAt(0).Item2.LongHeadPositions);
            Assert.AreEqual(stateTwo.Item1, bestPath.ElementAt(1).Item2.CrossHeadPosition);
            CollectionAssert.AreEqual(stateTwo.Item2, bestPath.ElementAt(1).Item2.LongHeadPositions);
            Assert.AreEqual(end.CrossHeadPosition, bestPath.ElementAt(2).Item2.CrossHeadPosition);
            CollectionAssert.AreEqual(end.LongHeadPositions, bestPath.ElementAt(2).Item2.LongHeadPositions);
        }
    }
}