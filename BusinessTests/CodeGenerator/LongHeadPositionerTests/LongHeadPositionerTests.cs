﻿namespace BusinessTests.CodeGenerator.LongHeadPositionerTests
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class LongHeadPositionerTests
    {
        [TestMethod]
        public void ShouldReturnEmptyListWhenOriginalPositionsIsEmpty()
        {
            var originalPositions = new MicroMeter[] { };
            var sut = new LongHeadPositioner(originalPositions);
            var res = sut.FilterPositionsThatShouldBeMoved(new MicroMeter[] { 123 });
            Specify.That(res.Count()).Should.BeLogicallyEqualTo(0);
        }

        [TestMethod]
        public void ShouldReturnExpectedResultWithFilteredMovements()
        {
            var originalPositions = new MicroMeter[] { 100, 200, 300, 400, 500, 600 };
            var sut = new LongHeadPositioner(originalPositions);
            var res = sut.FilterPositionsThatShouldBeMoved(new MicroMeter[] { 100, 201, 300, 456, 567, 600 });
            var expected = new List<LongHeadPositioningInformation>
                {
                    new LongHeadPositioningInformation(2, 201, 0),
                    new LongHeadPositioningInformation(4, 456, 0),
                    new LongHeadPositioningInformation(5, 567, 0)
                };
            Specify.That(res.ToList()).Should.BeLogicallyEqualTo(expected.ToList());
        }
    }
}