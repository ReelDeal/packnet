namespace BusinessTests.CodeGenerator.LongHeadPositionerTests
{
    using PackNet.Business.CodeGenerator.LongHeadPositioner;

    using Testing.Specificity;

    public static class TestExtensions
    {
        public static void ShouldBeBlockedBy(this LongHeadMovement movement, LongHeadPositioningInformation position)
        {
            var result = movement.IsBlockedBy(position);
            Specify.That(result).Should.BeLogicallyEqualTo(true);
        }

        public static void ShouldNotBeBlockedBy(this LongHeadMovement movement, LongHeadPositioningInformation position)
        {
            var result = movement.IsBlockedBy(position);
            Specify.That(result).Should.BeLogicallyEqualTo(false);
        }

        public static LongHeadPositioningInformation WithToolWidth(this LongHeadPositioningInformation position, int toolWidth)
        {
            return new LongHeadPositioningInformation(position.LongHeadNumber, position.Position, toolWidth);
        }
    }
}