﻿namespace BusinessTests.CodeGenerator.LongHeadPositionerTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class MovementOrderOptimizerSteps
    {
        [Given("default scenario setup")]
        public void GivenDefaultScenarioSetup()
        {
            ScenarioContext.Current["TargetPositionList"] = new List<LongHeadPositioningInformation>();
            ScenarioContext.Current["CurrentPositionList"] = new List<LongHeadPositioningInformation>();
            ScenarioContext.Current["CurrentCrossHeadPosition"] = 0.0;
            ScenarioContext.Current["ToolWidth"] = 0.0;
        }

        [Given(@"a tool width of (.*)")]
        public void GivenAToolWidthOf(double toolWidth)
        {
            ScenarioContext.Current["ToolWidth"] = toolWidth;
        }

        [Given("position (.*) is current position for cross head")]
        public void PositionIsCurrentPositionForCrossHead(double position)
        {
            ScenarioContext.Current["CurrentCrossHeadPosition"] = position;
        }

        [Given("longhead (.*) should be moved from (.*) to (.*)")]
        public void LongheadShouldBeMovedFromTo(int longHeadNumber, double currentPosition, double destinationPosition)
        {
            PositionIsCurrentPositionForLongHead(currentPosition, longHeadNumber);
            PositionIsTargetPositionForLongHead(destinationPosition, longHeadNumber);
        }

        [Given("position (.*) is target position for long head (.*)")]
        public void PositionIsTargetPositionForLongHead(double position, int longHead)
        {
            var toolWidth = (double)ScenarioContext.Current["ToolWidth"];
            var targetPositionList = ScenarioContext.Current["TargetPositionList"] as List<LongHeadPositioningInformation>;
            targetPositionList.Add(new LongHeadPositioningInformation(longHead, position, toolWidth));
        }

        [Given("position (.*) is current position for long head (.*)")]
        public void PositionIsCurrentPositionForLongHead(double position, int longHead)
        {
            var toolWidth = (double)ScenarioContext.Current["ToolWidth"];
            var currentPositionList = ScenarioContext.Current["CurrentPositionList"] as List<LongHeadPositioningInformation>;
            currentPositionList.Add(new LongHeadPositioningInformation(longHead, position, toolWidth));
        }

        [When("I optimize the movement order")]
        public void WhenIOptimizeTheMovementOrder()
        {
            var targetPositions = ScenarioContext.Current["TargetPositionList"] as IEnumerable<LongHeadPositioningInformation>;
            var currentPositions = ScenarioContext.Current["CurrentPositionList"] as IEnumerable<LongHeadPositioningInformation>;
            var currentCrossHeadPosition = (double)ScenarioContext.Current["CurrentCrossHeadPosition"];
            try
            {
                var movementOrderOptimizer = new MovementOrderOptimizer();
                ScenarioContext.Current["OptimizedMovementResult"] = movementOrderOptimizer.OptimizeMovement(targetPositions, currentPositions, currentCrossHeadPosition);
            }
            catch (CurrentLongHeadPositionUnknownException e)
            {
                ScenarioContext.Current["CurrentLongHeadPositionUnknownException"] = e;
            }
        }

        [Then("the result should be a list with (.*) items")]
        public void ThenTheResultShouldBeAnEmptyList(int number)
        {
            var optimizedMovement = ScenarioContext.Current["OptimizedMovementResult"] as IEnumerable<Tuple<LongHeadPositioningInformation, MicroMeter>>;
            Specify.That(optimizedMovement.Count()).Should.BeLogicallyEqualTo(number);
        }

        [Then("the distance crosshead should travel should be (.*)")]
        public void ThenTheDistanceCrossHeadShouldTravelShouldBe(double distance)
        {
            var optimizedMovement = ScenarioContext.Current["OptimizedMovementResult"] as IEnumerable<Tuple<LongHeadPositioningInformation, MicroMeter>>;
            var distanceTravelledForCrossHead = optimizedMovement.Last().Item2;
            Specify.That(distanceTravelledForCrossHead).Should.BeLogicallyEqualTo(distance);
        }

        [Then("the movement at index (.*) should be move long head (.*) to position (.*)")]
        public void ThenTheMovementAtIndexShouldBeMoveLongHeadToPosition(int index, int longHeadNumber, double position)
        {
            var optimizedMovement = ScenarioContext.Current["OptimizedMovementResult"] as IEnumerable<Tuple<LongHeadPositioningInformation, MicroMeter>>;
            var targetPosition = optimizedMovement.ElementAtOrDefault(index);
            Specify.That(targetPosition.Item1.LongHeadNumber).Should.BeLogicallyEqualTo(longHeadNumber);
            Specify.That(targetPosition.Item1.Position).Should.BeLogicallyEqualTo(position);
        }

        [Then("a current long head position unknown exception is thrown")]
        public void ThenACurrentLongHeadPositionUnknownExceptionIsThrown()
        {
            var expectedType = typeof(CurrentLongHeadPositionUnknownException);
            Specify.That(ScenarioContext.Current["CurrentLongHeadPositionUnknownException"]).Should.BeInstanceOfType(
                expectedType);
        }
    }
}
