﻿Feature: Movement order optimizer
	In order to be able to produce the design as fast as possible
	As a machine operator
	I want the positioning of long head to be carried out as fast as possible

@movmentOptimization
Scenario: Move zero long heads to position
	Given default scenario setup
	When I optimize the movement order
	Then the result should be a list with 0 items

Scenario: Move one long head to position
	Given default scenario setup
	And longhead 1 should be moved from 0.0 to 100.0
	When I optimize the movement order
	Then the result should be a list with 1 items
	And the movement at index 0 should be move long head 1 to position 100.0

	Scenario: Move one long head to position when long head is already at position
	Given default scenario setup
	And longhead 1 should be moved from 100.0 to 100.0
	When I optimize the movement order
	Then the result should be a list with 0 items

	Scenario: Move one long head to position when long head is at an unknown position
	Given default scenario setup
	And position 100.0 is target position for long head 1
	When I optimize the movement order
	Then a current long head position unknown exception is thrown

	Scenario: Move two long head to position should return 1 when 2 is moving within tool width from right of 1 and 2 is the normal best move
	Given default scenario setup
	And a tool width of 64.0
	And longhead 1 should be moved from 100.0 to 0.0
	And longhead 2 should be moved from 300.0 to 163.0
	And position 300.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the movement at index 0 should be move long head 1 to position 0.0
	And the movement at index 1 should be move long head 2 to position 163.0

	Scenario: Move two long head to position should return 2 when 2 is moving to extactly tool width from right of 1 and 1 is the normal best move
	Given default scenario setup
	And a tool width of 64.0
	And longhead 1 should be moved from 100.0 to 0.0
	And longhead 2 should be moved from 300.0 to 164.0
	And position 300.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the movement at index 0 should be move long head 2 to position 164.0
	And the movement at index 1 should be move long head 1 to position 0.0

	Scenario: Move two long head to position should return 2 when 1 is moving within tool width from left of 2 and 1 is the normal best move
	Given default scenario setup
	And a tool width of 64.0
	And longhead 1 should be moved from 100.0 to 137.0
	And longhead 2 should be moved from 200.0 to 300.0
	And position 0.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the movement at index 0 should be move long head 2 to position 300.0
	And the movement at index 1 should be move long head 1 to position 137.0

	Scenario: Move two long head to position should return 1 when 1 is moving to extactly tool width from left of 2
	Given default scenario setup
	And a tool width of 64.0
	And longhead 1 should be moved from 100.0 to 136.0
	And longhead 2 should be moved from 200.0 to 300.0
	And position 0.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the movement at index 0 should be move long head 1 to position 136.0
	And the movement at index 1 should be move long head 2 to position 300.0

	Scenario: Move two long head to position should return 1 when best order is 1 then 2
	Given default scenario setup
	And longhead 1 should be moved from 0.0 to 100.0
	And longhead 2 should be moved from 300.0 to 400.0
	And position 249.9 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the movement at index 0 should be move long head 1 to position 100.0
	And the movement at index 1 should be move long head 2 to position 400.0

	Scenario: Move two long head to position should return 2 when best order is 2 then 1
	Given default scenario setup
	And longhead 1 should be moved from 0.0 to 100.0
	And longhead 2 should be moved from 300.0 to 400.0
	And position 250.1 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the movement at index 0 should be move long head 2 to position 400.0
	And the movement at index 1 should be move long head 1 to position 100.0

	Scenario: Move two long head to position should return 1 when either order is best when 1 is first in target positions because we check movements from lower lh first in pathfinding
	Given default scenario setup
	And longhead 1 should be moved from 0.0 to 100.0
	And longhead 2 should be moved from 300.0 to 400.0
	And position 250.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the distance crosshead should travel should be 650.0
	And the movement at index 0 should be move long head 1 to position 100.0
	And the movement at index 1 should be move long head 2 to position 400.0

	Scenario: Move two long head to position should return 1 when either order is best when 2 is first in target positions because we check movements from lower lh first in pathfinding
	Given default scenario setup
	And longhead 1 should be moved from 0.0 to 100.0
	And longhead 2 should be moved from 300.0 to 400.0
	And position 250.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the distance crosshead should travel should be 650.0
	And the movement at index 0 should be move long head 1 to position 100.0
	And the movement at index 1 should be move long head 2 to position 400.0

	Scenario: Move two long head to position should return 2 when 2 is blocking 1
	Given default scenario setup
	And longhead 1 should be moved from 0.0 to 300.0
	And longhead 2 should be moved from 300.0 to 400.0
	And position 249.9 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 2 items
	And the movement at index 0 should be move long head 2 to position 400.0
	And the movement at index 1 should be move long head 1 to position 300.0

	Scenario: Move six long head to position should return 1 then 5 then 4 when 5 is blocking 4 and 2, 3 and 6 are in position
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 300.0
	And longhead 2 should be moved from 400.0 to 400.0
	And longhead 3 should be moved from 600.0 to 600.0
	And longhead 4 should be moved from 800.0 to 1000.0
	And longhead 5 should be moved from 1000.0 to 1200.0
	And longhead 6 should be moved from 1300.0 to 1300.0
	And position 500.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 3 items
	And the movement at index 0 should be move long head 1 to position 300.0
	And the movement at index 1 should be move long head 5 to position 1200.0
	And the movement at index 2 should be move long head 4 to position 1000.0

	Scenario: Move six long head to position should return 1 then 5 then 4 when 2, 3 and 6 are in position
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 300.0
	And longhead 2 should be moved from 400.0 to 400.0
	And longhead 3 should be moved from 600.0 to 600.0
	And longhead 4 should be moved from 800.0 to 1000.0
	And longhead 5 should be moved from 1100.0 to 1200.0
	And longhead 6 should be moved from 1300.0 to 1300.0
	And position 500.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 3 items
	And the movement at index 0 should be move long head 1 to position 300.0
	And the movement at index 1 should be move long head 4 to position 1000.0
	And the movement at index 2 should be move long head 5 to position 1200.0

	Scenario: Move six long head to position should return 4 then 5 then 1 when 5 is blocking 4 and 2, 3 and 6 are in position and cross head is closer to rightmost long head
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 300.0
	And longhead 2 should be moved from 400.0 to 400.0
	And longhead 3 should be moved from 600.0 to 600.0
	And longhead 4 should be moved from 800.0 to 1000.0
	And longhead 5 should be moved from 1000.0 to 1200.0
	And longhead 6 should be moved from 1300.0 to 1300.0
	And position 700.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 3 items
	And the movement at index 0 should be move long head 5 to position 1200.0
	And the movement at index 1 should be move long head 4 to position 1000.0
	And the movement at index 2 should be move long head 1 to position 300.0

	Scenario: Move six long head to position should return 1 then 5 then 4 when 2, 3 and 6 are in position and cross head is closer to rightmost long head
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 300.0
	And longhead 2 should be moved from 400.0 to 400.0
	And longhead 3 should be moved from 600.0 to 600.0
	And longhead 4 should be moved from 800.0 to 1000.0
	And longhead 5 should be moved from 1100.0 to 1200.0
	And longhead 6 should be moved from 1300.0 to 1300.0
	And position 700.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 3 items
	And the movement at index 0 should be move long head 1 to position 300.0
	And the movement at index 1 should be move long head 4 to position 1000.0
	And the movement at index 2 should be move long head 5 to position 1200.0

	Scenario: Move six long head to position should return 1 then 2 then 3 then 4 then 6 then 5 when 6 is blocking 5 and 2 is blocking 3 and crosshead starting position is closest to 1
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 100.0
	And longhead 2 should be moved from 400.0 to 300.0
	And longhead 3 should be moved from 600.0 to 400.0
	And longhead 4 should be moved from 800.0 to 900.0
	And longhead 5 should be moved from 1000.0 to 1200.0
	And longhead 6 should be moved from 1200.0 to 1400.0
	And position 0.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 6 items
	And the movement at index 0 should be move long head 1 to position 100.0
	And the movement at index 1 should be move long head 2 to position 300.0
	And the movement at index 2 should be move long head 3 to position 400.0
	And the movement at index 3 should be move long head 4 to position 900.0
	And the movement at index 4 should be move long head 6 to position 1400.0
	And the movement at index 5 should be move long head 5 to position 1200.0

	Scenario: Move six long head to position should return 1 then 2 then 3 then 4 then 6 then 5 when 6 is blocking 5 and 2 is blocking 3 and crosshead starting position is between 1 and 2 closer to 1
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 100.0
	And longhead 2 should be moved from 400.0 to 300.0
	And longhead 3 should be moved from 600.0 to 400.0
	And longhead 4 should be moved from 800.0 to 900.0
	And longhead 5 should be moved from 1000.0 to 1200.0
	And longhead 6 should be moved from 1200.0 to 1400.0
	And position 299.9 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 6 items
	And the movement at index 0 should be move long head 1 to position 100.0
	And the movement at index 1 should be move long head 2 to position 300.0
	And the movement at index 2 should be move long head 3 to position 400.0
	And the movement at index 3 should be move long head 4 to position 900.0
	And the movement at index 4 should be move long head 6 to position 1400.0
	And the movement at index 5 should be move long head 5 to position 1200.0

	Scenario: Move six long head to position should return 1 then 2 then 3 then 4 then 6 then 5 when 6 is blocking 5 and 2 is blocking 3 and crosshead starting position is at 766,7
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 100.0
	And longhead 2 should be moved from 400.0 to 300.0
	And longhead 3 should be moved from 600.0 to 400.0
	And longhead 4 should be moved from 800.0 to 900.0
	And longhead 5 should be moved from 1000.0 to 1200.0
	And longhead 6 should be moved from 1200.0 to 1400.0
	And position 766.67 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 6 items
	And the movement at index 0 should be move long head 4 to position 900.0
	And the movement at index 1 should be move long head 6 to position 1400.0
	And the movement at index 2 should be move long head 5 to position 1200.0
	And the movement at index 3 should be move long head 2 to position 300.0
	And the movement at index 4 should be move long head 3 to position 400.0
	And the movement at index 5 should be move long head 1 to position 100.0
	And the distance crosshead should travel should be 2933.33

	Scenario: Move six long head to position should return 1 then 2 then 3 then 4 then 6 then 5 when 6 is blocking 5 and 2 is blocking 3 and crosshead starting position is at 800
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 100.0
	And longhead 2 should be moved from 400.0 to 300.0
	And longhead 3 should be moved from 600.0 to 400.0
	And longhead 4 should be moved from 800.0 to 900.0
	And longhead 5 should be moved from 1000.0 to 1200.0
	And longhead 6 should be moved from 1200.0 to 1400.0
	And position 800.0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 6 items
	And the movement at index 0 should be move long head 4 to position 900.0
	And the movement at index 1 should be move long head 6 to position 1400.0
	And the movement at index 2 should be move long head 5 to position 1200.0
	And the movement at index 3 should be move long head 2 to position 300.0
	And the movement at index 4 should be move long head 3 to position 400.0
	And the movement at index 5 should be move long head 1 to position 100.0
	And the distance crosshead should travel should be 2900.0


Scenario: Move six long head to position should return 1 then 2 then 3 then 4 then 6 then 5 when 6 is blocking 5 and 2 is blocking 3 and crosshead starting position is at 800.1
	Given default scenario setup
	And longhead 1 should be moved from 200.0 to 100.0
	And longhead 2 should be moved from 400.0 to 300.0
	And longhead 3 should be moved from 600.0 to 400.0
	And longhead 4 should be moved from 800.0 to 900.0
	And longhead 5 should be moved from 1000.0 to 1200.0
	And longhead 6 should be moved from 1200.0 to 1400.0
	And position 800.1 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 6 items
	And the movement at index 0 should be move long head 4 to position 900.0
	And the movement at index 1 should be move long head 6 to position 1400.0
	And the movement at index 2 should be move long head 5 to position 1200.0
	And the movement at index 3 should be move long head 2 to position 300.0
	And the movement at index 4 should be move long head 3 to position 400.0
	And the movement at index 5 should be move long head 1 to position 100.0
	And the distance crosshead should travel should be 2900.1

	Scenario: Move six long head to position should return 6 then 5 then 4 then 3 then 2 then 1 when using a fefco design and solution is not found because doubles are used in the code
	Given default scenario setup
	And a tool width of 64
	And longhead 1 should be moved from 48.0 to 179.1
	And longhead 2 should be moved from 112.0 to 273.8
	And longhead 3 should be moved from 176.0 to 446.3
	And longhead 4 should be moved from 240.0 to 510.3
	And longhead 5 should be moved from 304.0 to 574.3
	And longhead 6 should be moved from 368.0 to 638.3
	And position 0 is current position for cross head
	When I optimize the movement order
	Then the result should be a list with 6 items
	And the movement at index 0 should be move long head 6 to position 638.3
	And the movement at index 1 should be move long head 5 to position 574.3
	And the movement at index 2 should be move long head 4 to position 510.3
	And the movement at index 3 should be move long head 3 to position 446.3
	And the movement at index 4 should be move long head 2 to position 273.8
	And the movement at index 5 should be move long head 1 to position 179.1
	And the distance crosshead should travel should be 3305.1