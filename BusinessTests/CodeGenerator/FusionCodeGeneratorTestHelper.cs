﻿namespace BusinessTests.CodeGenerator
{
    using System.Collections.Generic;
    using System.Linq;
    using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
    using PackNet.Business.CodeGenerator;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

    using TestUtils;

    internal static class FusionCodeGeneratorTestHelper
    {
        internal class FusionCodeGeneratorTester : FusionCodeGenerator
        {
            public FusionCodeGeneratorTester(MicroMeter crossHeadPosition, FusionLongHeadParameters longHeadParameters)
                : base(crossHeadPosition, longHeadParameters.LongHeads.Select(l => l.Position).ToArray(), new ConsoleLogger())
            {

            }
        }

        internal static FusionPhysicalMachineSettings GetFusionMachineParameters(int nrLongheads = 7, double crossHeadPosition = 100d)
        {
             var machineSettings = new FusionPhysicalMachineSettings
            {
                CrossHeadParameters = new FusionCrossHeadParameters() { Position = crossHeadPosition },
                CodeGeneratorSettings =
                    new CodeGeneratorSettings()
                    {
                        MinimumLineDistanceToCorrugateEdge = 5d,
                        MinimumLineLength = 2d,
                        PerforationSafetyDistance = 40d
                    }
            };
            
            var fusionLongHeadParameters = new FusionLongHeadParameters();

            foreach (var lh in GetFusionLongheads(nrLongheads))
            {
                fusionLongHeadParameters.AddLongHead(lh);
            }

            fusionLongHeadParameters.MinimumPosition = -40.6d;
            fusionLongHeadParameters.MaximumPosition = 1385;
            fusionLongHeadParameters.ConnectionDelay = 150;
            fusionLongHeadParameters.LongheadWidth = 42d;
            fusionLongHeadParameters.LeftSideToSensorPin = 37.5d;
            fusionLongHeadParameters.PositioningSpeed = 100;
            fusionLongHeadParameters.PositioningAcceleration = 100;

            machineSettings.LongHeadParameters = fusionLongHeadParameters;

            machineSettings.TrackParameters = new FusionTracksParameters(); { };
  

            machineSettings.TrackParameters.AddTrack(new Track()
                        {
                            Number = 1,
                            LeftSensorPlateToCorrugate = 3d,
                            OutOfCorrugatePosition = 38.1d,
                            RightSensorPlateToCorrugate = -3d,
                            RightSideFixed = true,
                        });
            machineSettings.TrackParameters.AddTrack(new Track()
            {
                Number = 2,
                LeftSensorPlateToCorrugate = 3d,
                OutOfCorrugatePosition = 1297d,
                RightSensorPlateToCorrugate = -3d,
                RightSideFixed = false,
            });

            return machineSettings;
        }

        private static IEnumerable<FusionLongHead> GetFusionLongheads(int nrOfLhs = 7)
        {
            return new List<FusionLongHead> {
                       new FusionLongHead {
                           LeftSideToTool = 14.9d,
                           Number = 1,
                           Position = 100,
                           Type = ToolType.Cut
                       },
                       new FusionLongHead {
                           LeftSideToTool = -2d,
                           Number = 2,
                           Position = 200,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = -2d,
                           Number = 3,
                           Position = 300,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = 14.9d,
                           Number = 4,
                           Position = 400,
                           Type = ToolType.Cut
                       },
                       new FusionLongHead {
                           LeftSideToTool = -2d,
                           Number = 5,
                           Position = 500,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = -2d,
                           Number = 6,
                           Position = 600,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = -0.9d,
                           Number = 7,
                           Position = 700,
                           Type = ToolType.Cut
                       },
                   }.Take(nrOfLhs);
        }
    }
}
