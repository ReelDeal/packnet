﻿namespace BusinessTests.CodeGenerator
{
    using PackNet.Common.Interfaces.DTO;

    public class PhysicalLineTestContainer
    {
        public LineType Type { get; set; }

        public double Position { get; set; }
    }
}