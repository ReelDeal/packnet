﻿Feature: Code generation
	In order to get a packaging out of the machine
	As an API user
	I want to get an instruction list for production of a design

Scenario: Should generate instruction list
	Given a compensated fefco 201
	And a corrugate
	And a machine
	When I generate code
	Then an instruction list is returned
	And the fist instruction in the list is meta data
	And the second instruction in the list is a long head position instruction
	And after long head positioning instructions comes crosshead and feedroller instructions
	And the last instruction is an end marker

	Scenario: Should generate instruction list for two tiled fefco 201
	Given a compensated tiled fefco 201
	And a corrugate
	And a machine with seven longheads
	When I generate code with seven longheads
	Then an instruction list is returned
	And the second instruction in the list is a long head position instruction where seven longheads are positioned
	And after the long head positioning instruction there are the expected crosshead and feedroller instructions for the tiled design
	And the last instruction is an end marker

	Scenario: Should return true when a design with producible vertical lines is validated
	Given a compensated tiled fefco 201
	And a corrugate
	And a machine with seven longheads
	When I validate longheadpositions with two tiles
	Then validation return true

	Scenario: Should return false when a design with not producible vertical lines is validated
	Given a compensated tiled fefco 201 with small values
	And a machine with seven longheads
	And a corrugate
	When I validate longheadpositions with two tiles
	Then validation return false
	
	Scenario: Should generate instruction list when only two crease lines are in the design
	Given a design with only two crease lines
	And a corrugate
	And a machine
	When I generate code
	Then an instruction list is returned
	And the fist instruction in the list is meta data
	And the second instruction in the list for the crease design is a long head position instruction
	And after long head positioning instructions there are expected crosshead and feedroller instruction for the crease design
	And the last instruction is an end marker

	Scenario: Should generate instruction list when only two cut lines are in the design
	Given a design with only two cut lines
	And a corrugate
	And a machine
	When I generate code
	Then an instruction list is returned
	And the fist instruction in the list is meta data
	And the second instruction in the list for the cut design is a long head position instruction
	And after long head positioning instructions there are expected crosshead and feedroller instruction for the cut design
	And the last instruction is an end marker

	Scenario: Should generate instruction list when only two cut lines over the whole corrugate are in the design
	Given a design with only two cut lines over the whole corrugate
	And a corrugate
	And a machine
	When I generate code
	Then an instruction list is returned
	And the fist instruction in the list is meta data
	And the second instruction in the list for the cut over whole corrugate design is a long head position instruction
	And after long head positioning instructions there are expected crosshead and feedroller instruction for the cut over whole corrugate design
	And the last instruction is an end marker
	
	Scenario: Should generate instruction list with OnTheFly
	Given a compensated fefco 201
	And a corrugate
	And a machine
	When I generate code with OnTheFly
	Then an instruction list with OnTheFly is returned
	
	Scenario: Should generate metadata instruction with synchronize label inactive
	Given a compensated fefco 201
	And a corrugate
	And a machine
	When I generate code with synchronize label false
	Then the first instruction in the list is meta data with synchronize label false
	
	Scenario: Should generate metadata instruction with synchronize label active
	Given a compensated fefco 201
	And a corrugate
	And a machine
	When I generate code with synchronize label true
	Then the first instruction in the list is meta data with synchronize label true

Scenario: Should not update LH positions if LH distribution fails
	Given a compensated fefco 201
	And a corrugate
	And a machine
	And code has been generated once
	And then a design that is not producable on the machine is selected
	When I generate code again
	Then a lhDistribution Exception Is Thrown
	And the LH positions are not changed
