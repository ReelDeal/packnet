﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator;
using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator
{
    [TestClass]
    public class OnTheFlyInstructionCreatorTests
    {
        private List<InstructionItem> originalInstructionList;
        private IEnumerable<InstructionItem> instructionList;
        private IOnTheFlyCreator creator;
        private OtfActuationDistanceCalculation distanceCalculation;

        [TestInitialize]
        public void Setup()
        {
            creator = new OnTheFlyCrossHeadCreator();

            var physicalMachineSettings = new EmPhysicalMachineSettings()
            {
                CrossHeadParameters = new EmCrossHeadParameters()
                {
                    Position = 0,
                    CutCompensation = 7,
                    CreaseCompensation = 7,
                    MovementData = new EmNormalMovementData()
                    {
                        Acceleration = 10000,
                        Speed = 1800,
                        NormalMovement = new EmMovementData()
                        {
                            AccelerationInPercent = 100,
                            SpeedInPercent = 100,
                            Torque = 100
                        }   
                    }
                }
            };

            var delayHandler = new OtfDelayHandler(50, 50, 50, 50);
            distanceCalculation = new OtfActuationDistanceCalculation(new OtfKinematicCalculation(physicalMachineSettings.CrossHeadParameters.MovementData), delayHandler);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateOtfIntsructionWithPerforationAsAToolState()
        {
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(400),
                new InstructionCrossHeadToolActivation(ToolStates.Perforation),
                new InstructionCrossHeadMovementItem(600),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(800),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(1000),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(1);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That((instructionList.ElementAt(0) as InstructionCrossHeadOnTheFlyItem).FinalPosition).Should.BeLogicallyEqualTo(1000);
            Specify.That((instructionList.ElementAt(0) as InstructionCrossHeadOnTheFlyItem).Positions.ElementAt(2).ToolState).Should.BeLogicallyEqualTo(ToolStates.Perforation);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateOnTheFlyInstruction()
        {
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(400),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(600),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(800),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(1000),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(1);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That((instructionList.ElementAt(0) as InstructionCrossHeadOnTheFlyItem).FinalPosition).Should.BeLogicallyEqualTo(1000);
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotCreateOtfInstruction_ForMovementsInOppositeDirections()
        {
            // From high pos to low pos
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(500),
                new InstructionCrossHeadToolActivation(ToolStates.None),
                new InstructionFeedRollerItem(100),
                new InstructionCrossHeadMovementItem(1000),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.None),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(7);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(0);

            // From low pos to high pos
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(500),
                new InstructionCrossHeadToolActivation(ToolStates.None),
                new InstructionFeedRollerItem(100),
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(1000),
                new InstructionCrossHeadToolActivation(ToolStates.None),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(7);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(1000);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateTwoOtfInstructionsWhenFeedInTheMiddle()
        {
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(400),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(600),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(800),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(1000),
                new InstructionFeedRollerItem(300),
                new InstructionCrossHeadMovementItem(800),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(600),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(400),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(0),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(3);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That((instructionList.ElementAt(0) as InstructionCrossHeadOnTheFlyItem).Positions.Count()).Should.BeLogicallyEqualTo(5);
            Specify.That((instructionList.ElementAt(0) as InstructionCrossHeadOnTheFlyItem).FinalPosition).Should.BeLogicallyEqualTo(1000);
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That((instructionList.ElementAt(2) as InstructionCrossHeadOnTheFlyItem).Positions.Count()).Should.BeLogicallyEqualTo(4);
            Specify.That((instructionList.ElementAt(2) as InstructionCrossHeadOnTheFlyItem).FinalPosition).Should.BeLogicallyEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoActuationsWithAMovementInBetweenShouldNotBeConverted()
        {
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(3);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotCreateOtfInstructionsWhenActivationsAreToClose()
        {
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(1),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(2),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(3),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(4),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(5),
                new InstructionFeedRollerItem(300),
                new InstructionCrossHeadMovementItem(4),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(3),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(2),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(1),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(0),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(originalInstructionList.Count);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotHaveOtfMovementsWhereActivationsAreToClose()
        {
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(100),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(208),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(300),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(400),
                new InstructionFeedRollerItem(300),
                new InstructionCrossHeadMovementItem(300),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(100),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(0),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(7);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That(instructionList.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToGenerate2OtfInstructionsAfterEachOther()
        {
            originalInstructionList = new List<InstructionItem>
            {
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(100),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(218),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(300),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(400),
                new InstructionFeedRollerItem(300),
                new InstructionCrossHeadMovementItem(300),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation(ToolStates.Cut),
                new InstructionCrossHeadMovementItem(100),
                new InstructionCrossHeadToolActivation(ToolStates.Crease),
                new InstructionCrossHeadMovementItem(0),
            };

            instructionList = creator.Create(originalInstructionList, distanceCalculation);

            Specify.That(instructionList.Count()).Should.BeEqualTo(5);
            Specify.That(instructionList.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That(instructionList.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionCrossHeadToolActivation));
            Specify.That(instructionList.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
            Specify.That(instructionList.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(instructionList.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionCrossHeadOnTheFlyItem));
        }
    }
}
