﻿@OnTheFlyCreator
Feature: OnTheFly Creator
	In order to simplify code generation
	As a developer
	I want to generate positions and toolchanges and in a second step merge them to an OTF movement

Scenario: A series of cross head movements and tool activations should be replaced by OTF instruction
	Given an instruction list with cross head movements and tool activations
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF instruction is returned
	And the end position for the OTF instruction is populated
	And the OTF instruction is followed by a tool activation

Scenario: Should create OTF movements if distance between two tool changes is shorter than middleDistance and next item is longer than activationDistance
	Given an instruction list with cross head movements and tool activations where two activation is within a short distance in the middle and next position is longer than activationDistance
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with two OTFs separated by a tool activation is returned
	
Scenario: Should create OTF movements if distance between two tool changes is shorter than middleDistance and next item is shorter than activationDistance
	Given an instruction list with cross head movements and tool activations where two activation is within a short distance in the middle and next position is shorter than activationDistance
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with two OTFs separated by tool activation and cross head movement is returned

Scenario: Should not create OTF movments when first tool changes is shorter than deactivationMinimumDistance and toolstate is crease in the beginning
	Given an instruction list with cross head movements and tool activations where first activation is within deactivationMinimumDistance and toolstate is CREASE in the beginning
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF in the end
	And a regular movement and tool activation of state CREASE in the beginning is returned

Scenario: Should create OTF movments when first tool changes is longer than deactivationMinimumDistance and toolstate is crease in the beginning
	Given an instruction list with cross head movements and tool activations where first activation is greater than deactivationMinimumDistance and toolstate is CREASE in the beginning
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF
	And no regular movement after OTF is returned

Scenario: Should not create OTF movments when first tool changes is shorter than activationMinimumDistance and toolstate is cut in the beginning
	Given an instruction list with cross head movements and tool activations where first activation is within activationMinimumDistance and toolstate is cut in the beginning
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF in the end
	And a regular movement and tool activation of state cut in the beginning is returned

Scenario: Should create OTF movments when first tool changes is longer than activationMinimumDistance and toolstate is cut in the beginning
	Given an instruction list with cross head movements and tool activations where first activation is greater than activationMinimumDistance and toolstate is cut in the beginning
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF
	And no regular movement after OTF is returned

Scenario: Should not create OTF movments when last tool changes is shorter than deactivationMinimumDistanceInEnd and toolstate is crease
	Given an instruction list with cross head movements and tool activations where last activation is within deactivationMinimumDistanceInEnd and toolstate is CREASE
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF in the beginning
	And a regular movement and tool activation of state CREASE in the end is returned

Scenario: Should create OTF movments when last tool changes is longer than deactivationMinimumDistanceInEnd and toolstate is crease
	Given an instruction list with cross head movements and tool activations where last activation is greater than deactivationMinimumDistanceInEnd and toolstate is CREASE
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF including last tool change
	And no regular movement after OTF is returned

Scenario: Should create OTF movments when last tool changes is longer than deactivationMinimumDistanceInEnd and toolstate is crease and more cross head movements exists after roller feed
	Given an instruction list with cross head movements and tool activations where last activation is greater than deactivationMinimumDistanceInEnd and cross head movments exists after roller feed and toolstate is CREASE
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with two OTFs is returned
	
Scenario: Should not create OTF movments when last tool changes is shorter than activationMinimumDistanceInEnd and toolstate is cut
	Given an instruction list with cross head movements and tool activations where last activation is within activationMinimumDistanceInEnd and toolstate is cut
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF in the beginning
	And a regular movement and tool activation of state cut in the end is returned

Scenario: Should create OTF movments when last tool changes is longer than activationMinimumDistanceInEnd and toolstate is cut
	Given an instruction list with cross head movements and tool activations where last activation is greater than activationMinimumDistanceInEnd and toolstate is cut
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with one OTF including last tool change
	And no regular movement after OTF is returned

Scenario: A instruction list with two activation and a movement in between should not be replaced by OTF
	Given an instruction list with tool activations and a movement in between
	And a OnTheFly creator
	When I create OTF instructions
	Then the instruction list is unchanged

Scenario: Should only look for merges until wrong instruction typ is found
	Given an instruction list with a series of movements and activations interrupted by a different type
	And a OnTheFly creator
	When I create OTF instructions
	Then an instructions list with OTF is returned
	And the end position for the OTF instructions are populated
	And each OTF instruction is followed by a tool activation

Scenario: Should not merge movements with different properties
	Given an instruction list with movement with and without disconnect
	And a OnTheFly creator
	When I create OTF instructions
	Then the instruction list is unchanged