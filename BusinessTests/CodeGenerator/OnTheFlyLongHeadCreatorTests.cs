﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator;
using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.CodeGenerator.LongHeadPositioner;
using PackNet.Business.CodeGenerator.OnTheFlyCompensator;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator
{
    [TestClass]
    public class OnTheFlyLongHeadCreatorTests
    {
        private IOnTheFlyCreator creator;
        private OtfActuationDistanceCalculation otfActuationDistances;

        [TestInitialize]
        public void Setup()
        {
            creator = new OnTheFlyLongHeadCreator();
            otfActuationDistances =
                new OtfActuationDistanceCalculation(
                    new OtfKinematicCalculation(new EmFeedRollerParameters(1500, 100, 4261, 100, 600, 200).MovementData),
                    new OtfDelayHandler(50, 50, 100, 50));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateLongHeadOtfInstruction()
        {
            var instructions = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(5, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(6, ToolStates.Cut, ToolStates.None),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(6, ToolStates.Crease, ToolStates.Cut),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(5, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(6, ToolStates.Cut, ToolStates.Crease),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(6, ToolStates.Crease, ToolStates.Cut),
                }),
                new InstructionFeedRollerItem(200),
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);

            Specify.That(otfInstructions.Count()).Should.BeEqualTo(1);
            Specify.That(otfInstructions.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
            var otfInstruction = otfInstructions.ElementAt(0) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfInstruction.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(otfInstruction.FinalPosition).Should.BeLogicallyEqualTo(1000);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateOtfInstructionWithPerforations()
        {
            var instructions = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(5, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(6, ToolStates.Cut, ToolStates.None),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(6, ToolStates.Crease, ToolStates.Cut),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Perforation,ToolStates.Crease),
                    new LongHeadToolActivationInformation(2, ToolStates.Perforation,ToolStates.Crease),
                    new LongHeadToolActivationInformation(3, ToolStates.Perforation,ToolStates.Crease),
                    new LongHeadToolActivationInformation(4, ToolStates.Perforation,ToolStates.Crease),
                    new LongHeadToolActivationInformation(5, ToolStates.Perforation,ToolStates.Crease),
                    new LongHeadToolActivationInformation(6, ToolStates.Perforation,ToolStates.Crease),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(6, ToolStates.Crease, ToolStates.Cut),
                }),
                new InstructionFeedRollerItem(200),
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);

            Specify.That(otfInstructions.Count()).Should.BeEqualTo(1);
            Specify.That(otfInstructions.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
            var otfInstruction = otfInstructions.ElementAt(0) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfInstruction.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(otfInstruction.Positions.ElementAt(2).ToolStates.All(t => t.ToolState.Equals(ToolStates.Perforation))).Should.BeTrue();
            Specify.That(otfInstruction.FinalPosition).Should.BeLogicallyEqualTo(1000);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotMergeInstructionsThatAreTooClose()
        {
            var instructions = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(5, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(6, ToolStates.Cut, ToolStates.None),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(6, ToolStates.Crease, ToolStates.Cut),
                }),
                new InstructionFeedRollerItem(20),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(5, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(6, ToolStates.Cut, ToolStates.Crease),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(4, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.Cut),
                    new LongHeadToolActivationInformation(6, ToolStates.Crease, ToolStates.Cut),
                }),
                new InstructionFeedRollerItem(200),
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);

            Specify.That(otfInstructions.Count()).Should.BeEqualTo(3);
            Specify.That(otfInstructions.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
            var otfInstruction = otfInstructions.ElementAt(0) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfInstruction.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfInstruction.FinalPosition).Should.BeLogicallyEqualTo(420);
            Specify.That(otfInstructions.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem)); 
            otfInstruction = otfInstructions.ElementAt(2) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfInstruction.Positions.Count()).Should.BeEqualTo(1);
            Specify.That(otfInstruction.FinalPosition).Should.BeLogicallyEqualTo(400);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoToolActivationsWithAFeedInTheMiddleShouldNotResultInAnOtfInstruction()
        {
            var instructions = new List<InstructionItem>()
            {
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(3, ToolStates.Crease, ToolStates.None),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(5, ToolStates.Crease, ToolStates.Crease),
                })
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);
            Specify.That(otfInstructions.Count()).Should.BeEqualTo(3);
            Specify.That(otfInstructions.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(otfInstructions.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void IfNoPreviousActivationForThatLongheadNoneShouldBeUsed()
        {
            //This instruction list will produce no otf entries except for the last instructions since it uses the same tool as earlier.
            //None -> cut distance is the largest, so if any other we would get otf entries

            var instructions = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(10),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.None),
                }),
                new InstructionFeedRollerItem(10),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.None),
                }),
                new InstructionFeedRollerItem(10),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(3, ToolStates.Cut, ToolStates.None),
                }),
                new InstructionFeedRollerItem(10),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(4, ToolStates.Cut, ToolStates.Cut),
                }),
                new InstructionFeedRollerItem(1),
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);
            Specify.That(otfInstructions.Count()).Should.BeEqualTo(7);
            Specify.That(otfInstructions.ElementAtOrDefault(6)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAlwaysBeAbleToMergeInstructionsWithTheSameToolActivationConfiguration()
        {
            var instructions = new List<InstructionItem>()
            {
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None),
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Crease),
                }),
                new InstructionFeedRollerItem(1),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Crease),
                }),
                new InstructionFeedRollerItem(1),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Crease),
                }),
                new InstructionFeedRollerItem(1),
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);
            Specify.That(otfInstructions.Count()).Should.BeEqualTo(1);
            Specify.That(otfInstructions.ElementAtOrDefault(0)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
            var otfInstruction = otfInstructions.ElementAtOrDefault(0) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfInstruction.Positions.Count()).Should.BeEqualTo(4);
            Specify.That(otfInstruction.FinalPosition).Should.BeLogicallyEqualTo(403);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldMergeFeedAndLongHeadActivationsToOnTheFlyInstructions()
        {
            var instructions = new List<InstructionItem>
            {
                new InstructionFeedRollerItem(50),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None)
                }),
                new InstructionFeedRollerItem(150),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Cut)
                }),
                new InstructionFeedRollerItem(150),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Crease)
                }),
                new InstructionEndMarkerItem()
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);

            foreach (var i in otfInstructions)
            {
                Console.WriteLine(i);
            }

            Specify.That(otfInstructions).Should.Not.BeNull();
            Specify.That(otfInstructions.Count()).Should.BeEqualTo(3);
            Specify.That(otfInstructions.ElementAt(0).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadOnTheFlyItem));
            Specify.That(otfInstructions.ElementAt(1).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(2).GetType()).Should.BeEqualTo(typeof(InstructionEndMarkerItem));

            var onTheFlyItem = otfInstructions.ElementAt(0) as InstructionLongHeadOnTheFlyItem;
            Specify.That(onTheFlyItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(onTheFlyItem.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(50d);
            Specify.That(onTheFlyItem.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(200d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateTwoLongHeadOtfInstructions_IfCrossHeadInstructionsArePresentInBetween()
        {
            var instructions = new List<InstructionItem>
            {
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None)
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Cut)
                }),
                new InstructionCrossHeadMovementItem(0),
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.Cut },
                new InstructionCrossHeadMovementItem(200),
                new InstructionCrossHeadToolActivation() { Toolstate = ToolStates.None },
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.Crease)
                }),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Crease)
                }),
                new InstructionEndMarkerItem()
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);

            foreach (var i in otfInstructions)
            {
                Console.WriteLine(i);
            }

            Specify.That(otfInstructions).Should.Not.BeNull();
            Specify.That(otfInstructions.Count()).Should.BeEqualTo(9);
            Specify.That(otfInstructions.ElementAt(0).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadOnTheFlyItem));
            Specify.That(otfInstructions.ElementAt(1).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(2).GetType()).Should.BeEqualTo(typeof(InstructionCrossHeadMovementItem));
            Specify.That(otfInstructions.ElementAt(3).GetType()).Should.BeEqualTo(typeof(InstructionCrossHeadToolActivation));
            Specify.That(otfInstructions.ElementAt(4).GetType()).Should.BeEqualTo(typeof(InstructionCrossHeadMovementItem));
            Specify.That(otfInstructions.ElementAt(5).GetType()).Should.BeEqualTo(typeof(InstructionCrossHeadToolActivation));
            Specify.That(otfInstructions.ElementAt(6).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadOnTheFlyItem));
            Specify.That(otfInstructions.ElementAt(7).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(8).GetType()).Should.BeEqualTo(typeof(InstructionEndMarkerItem));

            Specify.That((otfInstructions.ElementAt(0) as InstructionLongHeadOnTheFlyItem).FinalPosition).Should.BeLogicallyEqualTo(400);
            Specify.That((otfInstructions.ElementAt(6) as InstructionLongHeadOnTheFlyItem).FinalPosition).Should.BeLogicallyEqualTo(300);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateOneLhOtfInstruction_WhenLinesAreOffsetByASmallDistance()
        {
            var instructions = new List<InstructionItem>
            {
                new InstructionFeedRollerItem(150),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.None),
                }),
                new InstructionFeedRollerItem(3),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None)
                }),
                new InstructionFeedRollerItem(500),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease),
                    new LongHeadToolActivationInformation(2, ToolStates.Cut, ToolStates.Crease)
                }),
                new InstructionFeedRollerItem(200),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Cut),
                    
                }),
                new InstructionFeedRollerItem(3),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Cut), 
                }),
                new InstructionFeedRollerItem(200),
                new InstructionEndMarkerItem()
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);

            foreach (var i in otfInstructions)
            {
                Console.WriteLine(i);
            }

            Specify.That(otfInstructions.OfType<InstructionLongHeadOnTheFlyItem>().Count()).Should.BeEqualTo(1);
            var lhOthInstruction = otfInstructions.OfType<InstructionLongHeadOnTheFlyItem>().First();
            Specify.That(lhOthInstruction.Positions.Count()).Should.BeEqualTo(5);
            Specify.That(lhOthInstruction.Positions.ElementAt(0).Position).Should.BeLogicallyEqualTo(150);
            
            Specify.That(lhOthInstruction.Positions.ElementAt(1).Position).Should.BeLogicallyEqualTo(153);
            Specify.That(lhOthInstruction.Positions.ElementAt(2).Position).Should.BeLogicallyEqualTo(653);
            Specify.That(lhOthInstruction.Positions.ElementAt(3).Position).Should.BeLogicallyEqualTo(853);
            Specify.That(lhOthInstruction.Positions.ElementAt(4).Position).Should.BeLogicallyEqualTo(856);
            Specify.That(lhOthInstruction.FinalPosition).Should.BeLogicallyEqualTo(1056);
            Specify.That(otfInstructions.OfType<InstructionLongHeadToolActivations>().Count()).Should.BeEqualTo(0);
            Specify.That(otfInstructions.OfType<InstructionFeedRollerItem>().Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotMergeReversesToLongHeadOnTheFlyInstruction()
        {
            var instructions = new List<InstructionItem>
            {
                new InstructionFeedRollerItem(50),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None),
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None)
                }),
                new InstructionFeedRollerItem(150),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.Cut),
                    new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Crease)
                }),
                new InstructionFeedRollerItem(-100),
                new InstructionLongHeadPositionItem(new List<LongHeadPositioningInformation>()
                {
                    new LongHeadPositioningInformation(2, 500, 140),
                }),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.Crease, ToolStates.None),
                }),
                new InstructionFeedRollerItem(150),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(2, ToolStates.None, ToolStates.None),
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None)
                }),
                new InstructionFeedRollerItem(150),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>()
                {
                    new LongHeadToolActivationInformation(1, ToolStates.None, ToolStates.Cut)
                }),
                new InstructionEndMarkerItem()
            };

            var otfInstructions = creator.Create(instructions, otfActuationDistances);

            foreach (var i in otfInstructions)
            {
                Console.WriteLine(i);
            }

            Specify.That(otfInstructions).Should.Not.BeNull();
            Specify.That(otfInstructions.Count()).Should.BeEqualTo(8);
            Specify.That(otfInstructions.ElementAt(0).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadOnTheFlyItem));
            Specify.That(otfInstructions.ElementAt(1).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(2).GetType()).Should.BeEqualTo(typeof(InstructionFeedRollerItem));
            Specify.That(otfInstructions.ElementAt(3).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadPositionItem));
            Specify.That(otfInstructions.ElementAt(4).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(5).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadOnTheFlyItem));
            Specify.That(otfInstructions.ElementAt(6).GetType()).Should.BeEqualTo(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(7).GetType()).Should.BeEqualTo(typeof(InstructionEndMarkerItem));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDecelerateDuringLastUsableOtfPartAndNotStartNewOtfInstructionBeforeItsPossible()
        {
            var originalInstructionList = new List<InstructionItem>
            {
                new InstructionMetaDataItem(1, 2),
                new InstructionFeedRollerItem(0),
                new InstructionCrossHeadMovementItem(100),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None)
                }),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut)
                }),
                new InstructionFeedRollerItem(
                    otfActuationDistances.GetMaxSpeedActuationDistance(ToolStates.Crease, ToolStates.Cut) - 1), //decelerate
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease)
                }),
                new InstructionFeedRollerItem(
                    otfActuationDistances.GetAccelerationActuationDistance(ToolStates.Cut, ToolStates.Crease) - 1),
                //unable to accelerate
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut)
                }),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease)
                }),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut)
                }),
            };

            var otfInstructions = creator.Create(originalInstructionList, otfActuationDistances);
            Specify.That(otfInstructions.Count()).Should.BeEqualTo(9);
            Specify.That(otfInstructions.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(otfInstructions.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(otfInstructions.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(otfInstructions.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
            var otfItem = otfInstructions.ElementAt(3) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(200 + otfActuationDistances.GetMaxSpeedActuationDistance(ToolStates.Crease, ToolStates.Cut) - 1);
            Specify.That(otfInstructions.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(otfInstructions.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(7)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
            Specify.That(otfInstructions.ElementAt(8)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldTryToDecelerateDuringFinalUsablePartInOtf()
        {
            var originalInstructionList = new List<InstructionItem>
            {
                new InstructionMetaDataItem(1, 2),
                new InstructionFeedRollerItem(0),
                new InstructionCrossHeadMovementItem(100),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.None)
                }),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut)
                }),
                new InstructionFeedRollerItem(
                    otfActuationDistances.GetMaxSpeedActuationDistance(ToolStates.Crease, ToolStates.Cut) - 1), //decelerate
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease)
                }),
                new InstructionFeedRollerItem(
                    otfActuationDistances.GetAccelerationActuationDistance(ToolStates.Cut, ToolStates.Crease)),
                //able to accelerate
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut)
                }),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Cut, ToolStates.Crease)
                }),
                new InstructionFeedRollerItem(100),
                new InstructionLongHeadToolActivations(new List<LongHeadToolActivationInformation>
                {
                    new LongHeadToolActivationInformation(1, ToolStates.Crease, ToolStates.Cut)
                }),
            };


            var otfInstructions = creator.Create(originalInstructionList, otfActuationDistances);

            Specify.That(otfInstructions.Count()).Should.BeEqualTo(7); 
            Specify.That(otfInstructions.ElementAt(0)).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(otfInstructions.ElementAt(1)).Should.BeInstanceOfType(typeof(InstructionFeedRollerItem));
            Specify.That(otfInstructions.ElementAt(2)).Should.BeInstanceOfType(typeof(InstructionCrossHeadMovementItem));
            Specify.That(otfInstructions.ElementAt(3)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem));
            var otfItem = otfInstructions.ElementAt(3) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(200 + otfActuationDistances.GetMaxSpeedActuationDistance(ToolStates.Crease, ToolStates.Cut) - 1);
            Specify.That(otfInstructions.ElementAt(4)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
            Specify.That(otfInstructions.ElementAt(5)).Should.BeInstanceOfType(typeof(InstructionLongHeadOnTheFlyItem)); 
            otfItem = otfInstructions.ElementAt(5) as InstructionLongHeadOnTheFlyItem;
            Specify.That(otfItem.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(otfItem.FinalPosition).Should.BeLogicallyEqualTo(200 + otfActuationDistances.GetAccelerationActuationDistance(ToolStates.Cut, ToolStates.Crease));
            Specify.That(otfInstructions.ElementAt(6)).Should.BeInstanceOfType(typeof(InstructionLongHeadToolActivations));
        }
       
    }
}
