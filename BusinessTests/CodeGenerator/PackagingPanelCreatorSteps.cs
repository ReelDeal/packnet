﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace BusinessTests.CodeGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    [Scope(Tag = "PackagingPanelCreator")]
    public class PackagingPanelCreatorSteps
    {
        private PhysicalDesign designTrack1, designTrack12, designTrack2;
        private MicroMeter corrugateWidthTrack1, corrugateWidthTrack2;
        private MicroMeter corrugateOffsetTrack1, corrugateOffsetTrack2;
        private FusionPhysicalMachineSettings machineSettings;
        private List<InstructionItem> instructionListTrack1;
        private List<InstructionItem> instructionListTrack12;
        private List<InstructionItem> instructionListTrack2;
        private bool exceptionThrown;

        [Given(@"a design for track 1 with horizontal lines added in an unsorted way")]
        public void GivenADesignForTrack1WithHorizontalLinesAddedInAnUnsortedWay()
        {
            designTrack1 = new PhysicalDesign();

            var line = new PhysicalLine(
                new PhysicalCoordinate(150, 0),
                new PhysicalCoordinate(150, 500),
                LineType.crease);
            designTrack1.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(550, 0),
                new PhysicalCoordinate(550, 500),
                LineType.crease);
            designTrack1.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(700, 0),
                new PhysicalCoordinate(700, 500),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(0, 450),
                new PhysicalCoordinate(150, 450),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(0, 50),
                new PhysicalCoordinate(150, 50),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(0, 500),
                new PhysicalCoordinate(800, 500),
                LineType.cut);
            designTrack1.Add(line);

            designTrack1.Length = 100;
            designTrack1.Width = 700;
        }

        [Given(@"a design for track 1 with an uneven amount of horizontal lines")]
        public void GivenADesignForTrack1WithAnUnevenAmountOfHorizontalLines()
        {
            designTrack1 = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500),
                LineType.crease);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 500),
                LineType.crease);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(700, 0), new PhysicalCoordinate(700, 500),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(150, 50),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(550, 50),
                LineType.crease);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(550, 50), new PhysicalCoordinate(800, 50),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 450), new PhysicalCoordinate(150, 450),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(150, 450), new PhysicalCoordinate(550, 450),
                LineType.crease);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(550, 450), new PhysicalCoordinate(800, 450),
                LineType.cut);
            designTrack1.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(800, 500),
                LineType.cut);
            designTrack1.Add(line);

            designTrack1.Length = 100;
            designTrack1.Width = 700;
        }

        [Given(@"a design for track 1 with an even amount of horizontal lines")]
        public void GivenADesignForTrack1WithAnEvenAmountOfHorizontalLines()
        {
            designTrack12 = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(500, 50),
                LineType.crease);
            designTrack12.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(500, 100),
                LineType.crease);
            designTrack12.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(500, 150),
                LineType.crease);
            designTrack12.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(500, 200),
                LineType.cut);
            designTrack12.Add(line);

            designTrack12.Length = 100;
            designTrack12.Width = 500;
        }

        [Given(@"a design for track 2 with an uneven amount of horizontal lines")]
        public void GivenADesignForTrack2WithAnUnevenAmountOfHorizontalLines()
        {
            designTrack2 = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(500, 50), new PhysicalCoordinate(550, 50),
                LineType.cut);
            designTrack2.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 50), new PhysicalCoordinate(750, 50),
                LineType.crease);
            designTrack2.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(750, 50), new PhysicalCoordinate(800, 50),
                LineType.cut);
            designTrack2.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(500, 350), new PhysicalCoordinate(550, 350),
                LineType.cut);
            designTrack2.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 350), new PhysicalCoordinate(750, 350),
                LineType.crease);
            designTrack2.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(750, 350), new PhysicalCoordinate(800, 350),
                LineType.cut);
            designTrack2.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(500, 400), new PhysicalCoordinate(800, 400),
                LineType.cut);
            designTrack2.Add(line);

            designTrack2.Length = 100;
            designTrack2.Width = 300;
        }
        
        [Given(@"corrugate")]
        public void GivenCorrugate()
        {
            corrugateWidthTrack1 = 800;
            //this.corrugateWidthTrack1.SetCorrugateThickness(3);

            corrugateWidthTrack2 = 400;
            //this.corrugateWidthTrack2.SetCorrugateThickness(2);
        }

        [Given(@"a machine with crosshead and tracks")]
        public void GivenAMachineWithCrossheadAndTracks()
        {
            machineSettings = new FusionPhysicalMachineSettings();
            machineSettings.FeedRollerParameters.OutFeedLength = 35;

            var crossHeadParameters = machineSettings.CrossHeadParameters;
            crossHeadParameters.MinimumPosition = -30;
            crossHeadParameters.MaximumPosition = 1400;
            crossHeadParameters.Width = 40;
            crossHeadParameters.LeftSideToTool = 20;
            crossHeadParameters.CreaseWheelOffset = 10;

            var trackParameters = machineSettings.TrackParameters;
            var track = new Track {
                Number = 1,
                RightSideFixed = true
            };
            corrugateOffsetTrack1 = 800;
            trackParameters.AddTrack(track);

            track = new Track {
                Number = 2,
                RightSideFixed = false
            };
            corrugateOffsetTrack2 = 500;
            trackParameters.AddTrack(track);
        }

        [Given(@"the track offset is to large")]
        public void GivenTheTrackOffsetIsToLarge()
        {
            corrugateOffsetTrack2 = 1200;
        }

        [Given(@"the track offset is to small")]
        public void GivenTheTrackOffsetIsToSmall()
        {
            corrugateOffsetTrack1 = -100;
        }

        [When(@"I generate code with PackagePanelCreator for design on track 1")]
        public void WhenIGenerateCodeWithPackagePanelCreatorForDesignOnTrack1()
        {
            try
            {
                instructionListTrack1 = new List<InstructionItem>();
                instructionListTrack1.AddRange(new PackagingPanelCreator().Create(designTrack1, machineSettings, corrugateWidthTrack1, new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackNumber = 1,
                        TrackOffset = corrugateOffsetTrack1
                    }));
            }
            catch (ArgumentOutOfRangeException)
            {
                exceptionThrown = true;
            }
        }

        [When(@"I generate code with PackagePanelCreator for design on track 2")]
        public void WhenIGenerateCodeWithPackagePanelCreatorForDesignOnTrack2()
        {
            try
            {
                instructionListTrack2 = new List<InstructionItem>();
                instructionListTrack2.AddRange(new PackagingPanelCreator().Create(designTrack2, machineSettings, corrugateWidthTrack2, new PackNet.Common.Interfaces.DTO.Machines.Track()
                {
                    TrackNumber = 2,
                    TrackOffset = corrugateOffsetTrack2
                }));
            }
            catch (ArgumentOutOfRangeException)
            {
                exceptionThrown = true;
            }
        }

        [When(@"I generate code with PackagePanelCreator for all designs")]
        public void WhenIGenerateCodeWithPackagePanelCreatorForAllDesigns()
        {
            instructionListTrack1 = new List<InstructionItem>();
            instructionListTrack1.AddRange(new PackagingPanelCreator().Create(designTrack1, machineSettings, corrugateWidthTrack1,
                            new PackNet.Common.Interfaces.DTO.Machines.Track()
                            {
                                TrackNumber = 1,
                                TrackOffset = corrugateOffsetTrack1
                            }));

            instructionListTrack12 = new List<InstructionItem>();
            instructionListTrack12.AddRange(new PackagingPanelCreator().Create(designTrack12, machineSettings, corrugateWidthTrack1,
                new PackNet.Common.Interfaces.DTO.Machines.Track()
                {
                    TrackNumber = 1,
                    TrackOffset = corrugateOffsetTrack1
                }));

            instructionListTrack2 = new List<InstructionItem>();
            instructionListTrack2.AddRange(new PackagingPanelCreator().Create(designTrack2, machineSettings, corrugateWidthTrack2, new PackNet.Common.Interfaces.DTO.Machines.Track()
            {
                TrackNumber = 2,
                TrackOffset = corrugateOffsetTrack2
            }));
        }

        [Then(@"an instruction list without reverses is returned")]
        public void ThenAnInstructionListWithoutReversesIsReturned()
        {
            var feedInstructions = instructionListTrack1.Where(instruction => instruction is InstructionFeedRollerItem).Cast<InstructionFeedRollerItem>();
            Specify.That(feedInstructions.Where(instruction => instruction.Position < 0).Count()).Should.BeEqualTo(0);
        }

        [Then(@"an instruction list for the first design is returned")]
        public void ThenAnInstructionListForTheFirstDesignIsReturned()
        {
           /* foreach (InstructionItem item in this.instructionListTrack1)
            {
                System.Console.WriteLine(item.ToString());
            }

            System.Console.WriteLine("----------------");
            System.Console.WriteLine(this.instructionListTrack1.Count); */
            Specify.That(instructionListTrack1).Should.Not.BeEmpty();
            Specify.That((instructionListTrack1.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((instructionListTrack1.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);
            Specify.That((instructionListTrack1.ElementAt(2) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((instructionListTrack1.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(800);
            Specify.That((instructionListTrack1.ElementAt(4) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((instructionListTrack1.ElementAt(5) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((instructionListTrack1.ElementAt(6) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((instructionListTrack1.ElementAt(7) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((instructionListTrack1.ElementAt(8) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((instructionListTrack1.ElementAt(9) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(0);
            Specify.That((instructionListTrack1.ElementAt(10) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(-10);

            Specify.That((instructionListTrack1.ElementAt(11) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(400);
            Specify.That((instructionListTrack1.ElementAt(12) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(0);            
            Specify.That((instructionListTrack1.ElementAt(13) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((instructionListTrack1.ElementAt(14) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((instructionListTrack1.ElementAt(15) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((instructionListTrack1.ElementAt(16) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((instructionListTrack1.ElementAt(17) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((instructionListTrack1.ElementAt(18) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(800);
            Specify.That((instructionListTrack1.ElementAt(19) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);


            Specify.That((instructionListTrack1.ElementAt(20) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);
            Specify.That((instructionListTrack1.ElementAt(21) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(800);            
            Specify.That((instructionListTrack1.ElementAt(22) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(-10);
            Specify.That((instructionListTrack1.ElementAt(22) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeTrue();
            Specify.That((instructionListTrack1.ElementAt(23) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((instructionListTrack1.ElementAt(24) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
        }

        [Then(@"the final crosshead movement is outside of the corrugate")]
        public void ThenTheFinalCrossheadMovementIsOutsideOfTheCorrugate()
        {
            Specify.That((instructionListTrack1.FirstOrDefault(item => (item as InstructionCrossHeadMovementItem != null) && (item as InstructionCrossHeadMovementItem).DisconnectTrack) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(-10);
        }

        [Then(@"an instruction list for the second design is returned")]
        public void ThenAnInstructionListForTheSecondDesignIsReturned()
        {
            /*foreach (InstructionItem item in this.instructionListTrack1_2)
            {
                System.Console.WriteLine(item.ToString());
            }*/

            Specify.That(instructionListTrack12.Count()).Should.BeLogicallyEqualTo(16);
            Specify.That((instructionListTrack12.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(-10);
            Specify.That((instructionListTrack12.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);

            Specify.That((instructionListTrack12.ElementAt(2) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);
            Specify.That((instructionListTrack12.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((instructionListTrack12.ElementAt(4) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);

            Specify.That((instructionListTrack12.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(0);
            Specify.That((instructionListTrack12.ElementAt(6) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(-10);
            Specify.That((instructionListTrack12.ElementAt(7) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);

            Specify.That((instructionListTrack12.ElementAt(8) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);
            Specify.That((instructionListTrack12.ElementAt(9) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((instructionListTrack12.ElementAt(10) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);
            Specify.That((instructionListTrack12.ElementAt(11) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);
            Specify.That((instructionListTrack12.ElementAt(12) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);            
            Specify.That((instructionListTrack12.ElementAt(13) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(-10);
            Specify.That((instructionListTrack12.ElementAt(13) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeTrue();
            Specify.That((instructionListTrack12.ElementAt(14) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((instructionListTrack12.ElementAt(15) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
        }

        [Then(@"an instruction list for the third design is returned")]
        public void ThenAnInstructionListForTheThirdDesignIsReturned()
        {
        
            
            
        //    line.SetPhysicalLine(500, 50, 550, 50, LineType.cut);
        //    this.designTrack2.Add(line);
        //    line = lineFactory.Create();
        //    line.SetPhysicalLine(550, 50, 750, 50, LineType.crease);
        //    this.designTrack2.Add(line);
        //    line = lineFactory.Create();
        //    line.SetPhysicalLine(750, 50, 800, 50, LineType.cut);
        //    this.designTrack2.Add(line);

        //    line = lineFactory.Create();
        //    line.SetPhysicalLine(500, 350, 550, 350, LineType.cut);
        //    this.designTrack2.Add(line);
        //    line = lineFactory.Create();
        //    line.SetPhysicalLine(550, 350, 750, 350, LineType.crease);
        //    this.designTrack2.Add(line);
        //    line = lineFactory.Create();
        //    line.SetPhysicalLine(750, 350, 800, 350, LineType.cut);
        //    this.designTrack2.Add(line);

        //    line = lineFactory.Create();
        //    line.SetPhysicalLine(500, 400, 800, 400, LineType.cut);

            /*foreach (InstructionItem item in this.instructionListTrack2)
            {
                System.Console.WriteLine(item.ToString());
            }*/

            Specify.That((instructionListTrack2.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(490);
            Specify.That((instructionListTrack2.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);
            Specify.That((instructionListTrack2.ElementAt(2) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((instructionListTrack2.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);            
            Specify.That((instructionListTrack2.ElementAt(4) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((instructionListTrack2.ElementAt(5) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);

            Specify.That((instructionListTrack2.ElementAt(6) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(750);
            Specify.That((instructionListTrack2.ElementAt(7) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(750);
            Specify.That((instructionListTrack2.ElementAt(8) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((instructionListTrack2.ElementAt(9) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(800);
            Specify.That((instructionListTrack2.ElementAt(10) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((instructionListTrack2.ElementAt(11) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(910);
            Specify.That((instructionListTrack2.ElementAt(12) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(300);
            Specify.That((instructionListTrack2.ElementAt(13) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(800); // CUT
            Specify.That((instructionListTrack2.ElementAt(14) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);            
            Specify.That((instructionListTrack2.ElementAt(15) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(750);
            Specify.That((instructionListTrack2.ElementAt(16) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);

            Specify.That((instructionListTrack2.ElementAt(17) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((instructionListTrack2.ElementAt(18) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((instructionListTrack2.ElementAt(19) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((instructionListTrack2.ElementAt(20) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);            
            Specify.That((instructionListTrack2.ElementAt(21) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(490);
            Specify.That((instructionListTrack2.ElementAt(22) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);
            Specify.That((instructionListTrack2.ElementAt(23) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);            
            Specify.That((instructionListTrack2.ElementAt(24) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(910);
            Specify.That((instructionListTrack2.ElementAt(24) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeTrue();
            Specify.That((instructionListTrack2.ElementAt(25) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((instructionListTrack2.ElementAt(26) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
        }

        [Then(@"an exception is thrown")]
        public void ThenAnExceptionIsThrown()
        {
            // Remove this pending when PackagingPanelCreator throw exception on too big/small ch pos
            ScenarioContext.Current.Pending();

            Specify.That(exceptionThrown).Should.BeTrue();
            exceptionThrown = false;
        }
    }
}
