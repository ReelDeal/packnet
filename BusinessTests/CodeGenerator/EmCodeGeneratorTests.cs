﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Business.CodeGenerator;
using PackNet.Business.CodeGenerator.AStarStuff;
using PackNet.Business.CodeGenerator.Enums;
using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.DesignCompensator;
using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Utils;

using Testing.Specificity;

using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace BusinessTests.CodeGenerator
{
    using System.IO;
    using System.Net;

    using PackNet.Business.EmJobCreator;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;

    using TestUtils;

    [TestClass]
    [DeploymentItem("..\\..\\CodeGenerator\\AStarEmTests\\TestData", "EmCodeGenTestData")]
    public class EmCodeGeneratorTests
    {
        private const double lhOffset = 147;

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGenerateCodeForDesign()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetACompensatedFefco201(machineSettings);
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 400;
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);
            var track = EmCodeGeneratorTestHelper.GetTrack(2);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };

            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            Specify.That(instructionList.Count()).Should.Not.BeEqualTo(0);
            var feedLength = 0d;
            instructionList.OfType<InstructionFeedRollerItem>().ForEach(i => feedLength += i.Position);
            Specify.That(feedLength).Should.BeLogicallyEqualTo(500 + 400 + lhOffset, "To first get to the start of the design (for the lhs) we need to feed lhOffset length."
                                                                                + " Then the length of the design is 500 so that needs to be fed and finally we feed the extra length of 400 to get the box out of the machine.");
            Specify.That(instructionList.OfType<InstructionCrossHeadToolActivation>().Count()).Should.BeEqualTo(10, "To make all the panels in this design we need to do 10 crosshead tool activations.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldProduceCrossHeadLinesForSection_ThatAreWithinTheLongHeadOffsetDistanceFromThePreviousSection()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetDesignWithTwoSectionsAndACrossHeadLineFiveMillimetersFromTheStartOfSectionTwo(machineSettings);
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };

            machineSettings.LongHeadParameters.LongHeadYOffset = 14;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 400;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 500;
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);
            var track = EmCodeGeneratorTestHelper.GetTrack(2);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };
            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());

            var i = 0;
            var found = false;
            var pos = 0d;
            for (; i <= instructionList.Count(); i++)
            {
                if (instructionList.ElementAt(i).GetType() == typeof(InstructionCrossHeadMovementItem) &&
                    (instructionList.ElementAt(i) as InstructionCrossHeadMovementItem).Position == 200)
                {
                    found = true;
                    break;
                }
                if (instructionList.ElementAt(i).GetType() == typeof(InstructionFeedRollerItem))
                {
                    pos += (instructionList.ElementAt(i) as InstructionFeedRollerItem).Position;
                }
            }

            Specify.That(found).Should.BeTrue();

            Specify.That(pos).Should.BeLogicallyEqualTo(105d);
            Specify.That(instructionList.ElementAt(++i).GetType()).Should.BeLogicallyEqualTo(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(i) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);

            Specify.That(instructionList.ElementAt(++i).GetType()).Should.BeLogicallyEqualTo(typeof(InstructionCrossHeadMovementItem));
            Specify.That((instructionList.ElementAt(i) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(0);

            Specify.That(instructionList.ElementAt(++i).GetType()).Should.BeLogicallyEqualTo(typeof(InstructionCrossHeadToolActivation));
            Specify.That((instructionList.ElementAt(i) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.None);
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldProduceLhOtfWithCompensatedVelocity()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetDesignWithLhOTF(machineSettings);

            machineSettings.LongHeadParameters.LongHeadYOffset = 14;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 10;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 110;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 210;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 310;
            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = EmCodeGeneratorTestHelper.GetTrack(2);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };
            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 800, machine.Tracks.First(), true);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());

            var lhOtfInst = instructionList.ElementAt(2) as InstructionLongHeadOnTheFlyItem;
            Specify.That(lhOtfInst).Should.Not.BeNull();
            Specify.That(lhOtfInst.Positions.Count()).Should.BeEqualTo(6);
            Specify.That(lhOtfInst.Positions.ElementAt(0).ToolStates.Count()).Should.BeEqualTo(1);
            Specify.That(lhOtfInst.Positions.ElementAt(0).ToolStates.All(t => t.ToolState == ToolStates.Crease)).Should.BeTrue();
            Specify.That(lhOtfInst.Positions.ElementAt(1).ToolStates.Count()).Should.BeEqualTo(3);

            Specify.That(lhOtfInst.Positions.ElementAt(2).ToolStates.Count()).Should.BeEqualTo(1);
            Specify.That(lhOtfInst.Positions.ElementAt(2).ToolStates.All(t => t.ToolState == ToolStates.Crease)).Should.BeTrue();

            Specify.That(lhOtfInst.Positions.ElementAt(3).ToolStates.Count()).Should.BeEqualTo(2);
            Specify.That(lhOtfInst.Positions.ElementAt(3).ToolStates.All(t => t.ToolState == ToolStates.Cut)).Should.BeTrue();

            Specify.That(lhOtfInst.Positions.ElementAt(4).ToolStates.Count()).Should.BeEqualTo(2);
            Specify.That(lhOtfInst.Positions.ElementAt(4).ToolStates.All(t => t.ToolState == ToolStates.Crease)).Should.BeTrue();

            Specify.That(lhOtfInst.Positions.ElementAt(5).ToolStates.Count()).Should.BeEqualTo(1);
            Specify.That(lhOtfInst.Positions.ElementAt(5).ToolStates.All(t => t.ToolState == ToolStates.Cut)).Should.BeTrue();

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldProduceCrossHeadLinesCorrectlyWhenLinesAreCloseToSectionStart()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetDesignWithCrossHeadLinesCloseToSectionStartAndReverses(machineSettings);
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };

            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 400;
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);
            var track = EmCodeGeneratorTestHelper.GetTrack(2);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };

            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());

            var horizontalYLineCoordinates = compensatedDesign.GetHorizontalLineCoordinates();


            for (int i = 0; i < instructionList.Count(); i++)
            {
                var instruction = instructionList.ElementAt(i);
                if (instruction is InstructionCrossHeadToolActivation)
                {
                    MicroMeter fedLengthToThisToolActivation = instructionList.Take(i + 1)
                                                            .Where(instr => instr.GetType() == typeof(InstructionFeedRollerItem))
                                                            .Cast<InstructionFeedRollerItem>()
                                                            .Sum(instr => instr.Position);
                    Specify.That(horizontalYLineCoordinates.Contains(fedLengthToThisToolActivation)).Should.BeTrue("horizontalLineCoordinates does not contain " + fedLengthToThisToolActivation);
                }
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGenerateCodeForDesignWithTwoSections()
        {
            var compensatedDesign = GetDesignWithTwoSections();
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 400;

            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };
            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());

            var feedLength = 0d;
            instructionList.OfType<InstructionFeedRollerItem>().ForEach(i => feedLength += i.Position);
            Specify.That(feedLength).Should.BeLogicallyEqualTo(500 + 400 + lhOffset, "To first get to the start of the design (for the lhs) we need to feed lhOffset length."
                                                                                + " Then the length of the design is 500 so that needs to be fed and finally we feed the extra length of 400 to get the box out of the machine.");

            Specify.That(instructionList.Any(i => i is InstructionTrackActivationItem)).Should.BeTrue();
            Specify.That(instructionList.Any(i => i is InstructionLongHeadPositionItem)).Should.BeTrue();
            Specify.That(instructionList.Any(i => i is InstructionFeedRollerItem)).Should.BeTrue();
            Specify.That(instructionList.Any(i => i is InstructionLongHeadToolActivations)).Should.BeTrue();
            Specify.That(instructionList.Any(i => i is InstructionFeedRollerItem)).Should.BeTrue();
            Specify.That(instructionList.Any(i => i is InstructionCrossHeadToolActivation)).Should.BeTrue();
            Specify.That(instructionList.Any(i => i is InstructionCrossHeadMovementItem)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGenerateCodeForDesignsWithReverses()
        {
            var compensatedDesign = EmCodeGeneratorTestHelper.GetDesignWithReverses();
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 400;

            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new CodeOptimizer());
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);
            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };
            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            var feedLength = 0d;
            instructionList.OfType<InstructionFeedRollerItem>().ForEach(i => feedLength += i.Position);
            Specify.That(feedLength).Should.BeLogicallyEqualTo(500 + 400 + lhOffset, "To first get to the start of the design (for the lhs) we need to feed lhOffset length."
                                                                                + " Then the length of the design is 500 so that needs to be fed and finally we feed the extra length of 400 to get the box out of the machine.");
            Specify.That(instructionList.Count(i => i.GetType() == typeof(InstructionLongHeadPositionItem))).Should.BeEqualTo(2, "Initial positioning -> make a crease with lh2 to produce the first line -> reposition lh2 and reverse to produce the second line");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGenerateCodeForDesignWithMultipleSectionsWithLongheadRepositioning()
        {
            var compensatedDesign = GetDesignWithFourSections();
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 400;
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };
            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            Specify.That(instructionList.Count(i => i.GetType() == typeof(InstructionLongHeadPositionItem))).Should.BeEqualTo(4);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldFinalCutInTheEnd_IfThereAreReversesWithinTheYOffset_FromTheEndOfDesign()
        {
            /* 
             *  0   100   200   300   400
             * 50    |
             *103    |
             *       |                  
             *200    |     
             *250    |     |  
             */

            var design = new PhysicalDesign { Length = 250, Width = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 200), new PhysicalCoordinate(200, 250), LineType.crease));

            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 1);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;

            var ruleAppliedDesign = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            }).ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0);

            var compensatorParameters = new CompensationParameters(ruleAppliedDesign, false, 0d, 400d, 0d, 0d, 0d, 0d, 0d, 0d);

            var compensator = new EmDesignCompensator();

            var compensatedDesign = compensator.Compensate(compensatorParameters);

            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = new Track
            {
                TrackNumber = 1,
                TrackOffset = 50
            };

            var longheadActive = false;
            var trackDeactivated = false;
            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 800, track, false);
            instructionList.ForEach(
                i =>
                {
                    Console.WriteLine(i);

                    if (i.GetType() == typeof(InstructionTrackActivationItem) && (i as InstructionTrackActivationItem).Activate == false)
                    {
                        trackDeactivated = true;
                    }
                    else if (i.GetType() == typeof(InstructionLongHeadToolActivations))
                    {
                        longheadActive = (i as InstructionLongHeadToolActivations).ToolActivations.Any(t => t.ToolState == ToolStates.Crease);
                    }
                    else if (i.GetType() == typeof(InstructionFeedRollerItem))
                    {
                        if (trackDeactivated)
                        {
                            Specify.That(longheadActive).Should.BeTrue("We should always have a longhead active when feeding after the final cut is made");
                        }
                    }
                });
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldProduceHorizontalLines_InTheLatestPossibleSection()
        {
            /* 
             *  0   100   200   300   400
             * 50    |
             * 95    |
             *200         | 
             *242   ______|______________ <- This horizontal line should not be made in the first section even though the crosshead is on this coordinate when done with the first section
             *250   ______|______________  
             */

            var design = new PhysicalDesign { Length = 250, Width = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 95), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 95 + lhOffset), new PhysicalCoordinate(400, 95 + lhOffset), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 95), new PhysicalCoordinate(200, 250), LineType.crease));

            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 1);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;

            var ruleAppliedDesign = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            }).ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0);

            var compensatorParameters = new CompensationParameters(ruleAppliedDesign, false, 0d, 400d, 0d, 0d, 0d, 0d, 0d, 0d);

            var compensator = new EmDesignCompensator();

            var compensatedDesign = compensator.Compensate(compensatorParameters);

            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = new Track
            {
                TrackNumber = 1,
                TrackOffset = 50
            };


            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 800, track, false);
            var crossheadMoved = false;
            for (var i = 0; i < instructionList.Count(); i++)
            {
                if (instructionList.ElementAt(i) is InstructionLongHeadPositionItem)
                {
                    Specify.That(crossheadMoved).Should.BeFalse("The crosshead movements in this design should be done in the last section, after the longhead has been repositioned");
                }
                if (instructionList.ElementAt(i) is InstructionCrossHeadMovementItem)
                {
                    crossheadMoved = true;
                }
            }
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldFinalCutInTheEnd_IfThereAreReversesWithinTheYOffset_FromTheEndOfDesign_ButNoLineDirectlyFromStartOfYOffset()
        {
            Assert.Inconclusive("The test should work like this but no implementation is done for extending lines yet.");

            /* 
             *  0   100   200   300   400
             * 50    |
             *103    |
             *                         
             *200         
             *250    |     |  
             */

            var design = new PhysicalDesign { Length = 250, Width = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 103), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 200), new PhysicalCoordinate(100, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 200), new PhysicalCoordinate(200, 250), LineType.crease));

            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 1);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            var ruleAppliedDesign = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            }).ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0);

            var compensatorParameters = new CompensationParameters(ruleAppliedDesign, false, 0d, 400d, 0d, 0d, 0d, 0d, 0d, 0d);

            var compensator = new EmDesignCompensator();

            var compensatedDesign = compensator.Compensate(compensatorParameters);

            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = new Track
            {
                TrackNumber = 1,
                TrackOffset = 50
            };

            var longheadActive = false;
            var trackDeactivated = false;
            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 800, track, false);
            instructionList.ForEach(
                i =>
                {
                    if (i.GetType() == typeof(InstructionTrackActivationItem) && (i as InstructionTrackActivationItem).Activate == false)
                    {
                        trackDeactivated = true;
                    }
                    else if (i.GetType() == typeof(InstructionLongHeadToolActivations))
                    {
                        longheadActive = (i as InstructionLongHeadToolActivations).ToolActivations.Any(t => t.ToolState == ToolStates.Crease);
                    }
                    else if (i.GetType() == typeof(InstructionFeedRollerItem))
                    {
                        if (trackDeactivated)
                        {
                            Specify.That(longheadActive).Should.BeTrue("We should always have a longhead active when feeding after the final cut is made");
                        }
                    }
                });
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPerformCrossHeadInstructionsOnLastPass()
        {
            var compensatedDesign = GetDesignWithReverseAndCrossHeadInstructions();
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 400;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 500;
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());

            var creator = new SectionCreator(lhOffset);


            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, creator, new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };

            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);
            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());

            var feedLength = 0d;
            instructionList.OfType<InstructionFeedRollerItem>().ForEach(i => feedLength += i.Position);
            Specify.That(feedLength).Should.BeLogicallyEqualTo(800 + 400 + lhOffset, "To first get to the start of the design (for the lhs) we need to feed lhOffset length."
                                                                                + " Then the length of the design is 800 so that needs to be fed and finally we feed the extra length of 400 to get the box out of the machine.");
            var trackDeactivationIndex = -1;
            for (var i = 0; i < instructionList.Count(); i++)
            {
                if (instructionList.ElementAt(i).GetType() == typeof(InstructionTrackActivationItem)
                    && (instructionList.ElementAt(i) as InstructionTrackActivationItem).Activate == false)
                    trackDeactivationIndex = i;
            }
            Specify.That(trackDeactivationIndex).Should.Not.BeEqualTo(-1, "No track deactivation instruction was found.");

            Specify.That(instructionList.ElementAt(trackDeactivationIndex - 3).GetType()).Should.BeEqualTo(typeof(InstructionCrossHeadToolActivation), "This should be the final cut");
            Specify.That((instructionList.ElementAt(trackDeactivationIndex - 3) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut, "This should be the final cut");

            Specify.That(instructionList.ElementAt(trackDeactivationIndex - 2).GetType()).Should.BeEqualTo(typeof(InstructionCrossHeadMovementItem), "Expected the movement to other edge of design (0 or 500), not the same as the first movement");
            Specify.That((instructionList.ElementAt(trackDeactivationIndex - 2) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500, "Expected the movement to other edge of design (0 or 500), not the same as the first movement");

            Specify.That(instructionList.ElementAt(trackDeactivationIndex - 1).GetType()).Should.BeEqualTo(typeof(InstructionCrossHeadToolActivation), "Deactivation after final cut");
            Specify.That((instructionList.ElementAt(trackDeactivationIndex - 1) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.None, "Deactivation after final cut");

            feedLength = 0d;
            instructionList.Skip(trackDeactivationIndex).OfType<InstructionFeedRollerItem>().ForEach(i => feedLength += i.Position);
            Specify.That(feedLength).Should.BeLogicallyEqualTo(400 + lhOffset, "After the deactivation we should have no reverses");


        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSplitSections_IfSectionIsLongerThanLongheadOffset_AndContainsAReverse()
        {
            var longHeadPositions = new MicroMeter[] { 100 };
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(1);
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.FeedRollerParameters.MaximumReverseDistance = 600;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 200;

            var design = new PhysicalDesign() { Length = 1200, Width = 600 };

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, design.Length), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, design.Length), new PhysicalCoordinate(design.Width, design.Length), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(design.Width, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(design.Width, 0), new PhysicalCoordinate(design.Width, design.Length), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, design.Length), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, design.Length), LineType.crease));

            var compensatedDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 5, design, design.Length, design.Width, 0, 600, 3, 1, false, 50);

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            Specify.That(longHeadDistribution.GetSolutionNodes().Count()).Should.BeEqualTo(2, "The design can be produced as one section but should have been split into 4 smaller sections because of maximum reverse distance");
            Specify.That(longHeadDistribution.NumberOfReverses).Should.BeLogicallyEqualTo(2, "We need four reverses to produce both lines (that are split into 8) with one longhead");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void LastSectionShouldBeAtLeast_LongheadOffsetLong_WhenSplittingSections()
        {
            var longHeadPositions = new MicroMeter[] { 100 };
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(1);
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.FeedRollerParameters.MaximumReverseDistance = 3 * lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 200;

            /*   ____________
             *  |   |    |   |
             *  |   |    |   |   
             *  |   |    |   |   
             *  |   |    |   |                          
             *  |   |    |   |
             *  |------------| <- First split location
             *  |   |    |   |                          
             *  |   |    |   |                       
             *  |   |    |   |    
             *  |   |    |   |   
             *  |   |    |   |                      
             *  |------------| <- Second split location 
             *  |   |    |   |
             *  |   |    |   |   
             *  |   |    |   |   
             *  |   |    |   | 
             *  |------------| <- Y-offset. Third split location should be moved here
             *  |   |    |   | <- Actual third split location                                              
             *  |___|____|___|
             */
            var design = new PhysicalDesign() { Length = 3.2 * machineSettings.FeedRollerParameters.MaximumReverseDistance, Width = 600 };

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, design.Length), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, design.Length), new PhysicalCoordinate(design.Width, design.Length), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(design.Width, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(design.Width, 0), new PhysicalCoordinate(design.Width, design.Length), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, design.Length), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, design.Length), LineType.crease));

            var compensatedDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 5, design, design.Length, design.Width, 0, 600, 3, 1, false, 50);

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            Specify.That(longHeadDistribution.GetSolutionNodes().Count()).Should.BeEqualTo(4, "The design can be produced as one section but should have been split into 4 smaller sections because of maximum reverse distance");
            Specify.That(longHeadDistribution.Sections.Last().Length).Should.BeLogicallyEqualTo(lhOffset, "The last section should be lhOffset long");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSplitMoreThanOnce_IfNecessary()
        {
            var compensatedDesign = GetLongDesignWithReverse();
            var longHeadPositions = new MicroMeter[] { 50 };
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(1);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 50;

            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new CodeOptimizer());

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };

            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);
            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            //Specify.That(instructionList.Count()).Should.BeEqualTo(41);

            Specify.That(instructionList.OfType<InstructionLongHeadPositionItem>().Count()).Should.BeEqualTo(24, "Should place longhead 24 times");
            Specify.That(instructionList.OfType<InstructionFeedRollerItem>().Count(i => i.Position < 0)).Should.BeEqualTo(16, "Should have 16 reverses");

            var totalFeedLength = instructionList.OfType<InstructionFeedRollerItem>().Sum(i => i.Position);
            var feedLengthMinusOutFeed = totalFeedLength - machineSettings.FeedRollerParameters.OutFeedLength;
            var feedLengthMinusOutFeedAndYOffset = feedLengthMinusOutFeed - machineSettings.LongHeadParameters.LongHeadYOffset;

            Specify.That(feedLengthMinusOutFeedAndYOffset).Should.BeLogicallyEqualTo(3100d);
        }

        [TestMethod]
        [TestCategory("Bug 12616")]
        [TestCategory("Integration")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\Em7PhysicalMachineSettingsWaitingForTrigger_inches.cfg.xml", "EmCodeGenTestData")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\0201-0013TopFlapsMedium.ezd", "EmCodeGenTestData")]
        public void ShouldOnlyAddOneTrackDeactivationEntry()
        {
            var machineSettings = EmPhysicalMachineSettings.CreateFromFile(
                new IPEndPoint(1, 1),
                Path.Combine(
                    Environment.CurrentDirectory,
                    "EmCodeGenTestData",
                    "Em7PhysicalMachineSettingsWaitingForTrigger_inches.cfg.xml"));

            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 20;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 30;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 40;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 50;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 60;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 70;

            machineSettings.LongHeadParameters.MovementData.Speed = 14.1732;
            machineSettings.LongHeadParameters.MovementData.Acceleration = 59.5514;

            machineSettings.FeedRollerParameters.MovementData.Acceleration = 16.777;
            machineSettings.FeedRollerParameters.MovementData.Speed = 5.9055;

            machineSettings.CrossHeadParameters.MovementData.Acceleration = 59.5514;
            machineSettings.CrossHeadParameters.MovementData.Speed = 14.1732;

            var corrugate = new Corrugate { Width = 29.25d, Thickness = 0.16 };
            var order = new Carton() { CartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate } };
            var mic = new EmMachineInstructionCreator(null);
            var track = new Track { TrackNumber = 2, TrackOffset = 4 };

            var design = EmCodeGeneratorTestHelper.LoadAndTileDesign(2010013, 11.5, 4.3, 4.9, corrugate, 2);
            var instructionList = mic.GetInstructionListForJob(order, design, track, machineSettings);
            foreach (var instr in instructionList)
                Console.WriteLine(instr);

            Specify.That(instructionList.OfType<InstructionTrackActivationItem>().Count()).Should.BeEqualTo(2);
        }


        [Ignore]
        [TestMethod]
        [TestCategory("Bug 18584")]
        [TestCategory("Integration")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\Em7PhysicalMachineSettingsWaitingForTrigger_inches.cfg.xml", "EmCodeGenTestData")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\IQ 0201-0001 INCHES.ezd", "EmCodeGenTestData")]
        public void LongheadsShouldOnlyBeDeactivatedOnce_AtTheEndOfTheDesign()
        {
            var machineSettings = EmPhysicalMachineSettings.CreateFromFile(
                new IPEndPoint(1, 1),
                Path.Combine(
                    Environment.CurrentDirectory,
                    "EmCodeGenTestData",
                    "Em7PhysicalMachineSettingsWaitingForTrigger_inches.cfg.xml"));

            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 20;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 30;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 40;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 50;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 60;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 70;

            machineSettings.LongHeadParameters.MovementData.Speed = 14.1732;
            machineSettings.LongHeadParameters.MovementData.Acceleration = 59.5514;

            machineSettings.FeedRollerParameters.MovementData.Acceleration = 16.777;
            machineSettings.FeedRollerParameters.MovementData.Speed = 5.9055;

            machineSettings.CrossHeadParameters.MovementData.Acceleration = 59.5514;
            machineSettings.CrossHeadParameters.MovementData.Speed = 14.1732;

            var corrugate = new Corrugate { Width = 80.0d, Thickness = 0.16 };
            var order = new Carton() { CartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate } };
            var mic = new EmMachineInstructionCreator(null);
            var track = new Track { TrackNumber = 1, TrackOffset = 5 };

            var design = EmCodeGeneratorTestHelper.LoadAndTileDesign(2010001, 28, 28, 28, corrugate, 1);
            var instructionList = mic.GetInstructionListForJob(order, design, track, machineSettings);

            var instructionsWithDeactivateAllLongheads =
                instructionList.Count(
                    i =>
                        i is InstructionLongHeadToolActivations
                        && (i as InstructionLongHeadToolActivations).ToolActivations.All(a => a.ToolState == ToolStates.None));

            Specify.That(instructionsWithDeactivateAllLongheads).Should.BeLogicallyEqualTo(1, "We only expect one deactivate all longheads instruction at the end of the design");
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Bug 18635")]
        [TestCategory("Integration")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\Em7PhysicalMachineSettings_mm.cfg.xml", "EmCodeGenTestData")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\05_Crease_Delay_LH_mm.ezd", "EmCodeGenTestData")]
        public void ShouldNotDoCutOff_UntilAtEndOfDesign()
        {
            var machineSettings = EmPhysicalMachineSettings.CreateFromFile(
                new IPEndPoint(1, 1),
                Path.Combine(
                    Environment.CurrentDirectory,
                    "EmCodeGenTestData",
                    "Em7PhysicalMachineSettings_mm.cfg.xml"));

            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 1100;
            machineSettings.CrossHeadParameters.Position = 2000;
            machineSettings.LongHeadParameters.MovementData.Speed = 3600;
            machineSettings.LongHeadParameters.MovementData.Acceleration = 15126;

            machineSettings.FeedRollerParameters.MovementData.Acceleration = 4261.36;
            machineSettings.FeedRollerParameters.MovementData.Speed = 1500;

            machineSettings.CrossHeadParameters.MovementData.Acceleration = 15126;
            machineSettings.CrossHeadParameters.MovementData.Speed = 3600;

            var corrugate = new Corrugate { Width = 1200d, Thickness = 3d };
            var order = new Carton() { CartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate } };
            var mic = new EmMachineInstructionCreator(null);
            var track = new Track { TrackNumber = 1, TrackOffset = 100 };

            var design = EmCodeGeneratorTestHelper.LoadAndTileDesign(102051, 500, 500, 500, corrugate, 1);
            var instructionList = mic.GetInstructionListForJob(order, design, track, machineSettings);

            var feedLength = 0d;
            var foundCutOf = false;
            foreach (var instructionItem in instructionList)
            {
                if (instructionItem is InstructionFeedRollerItem)
                {
                    feedLength += (instructionItem as InstructionFeedRollerItem).Position;
                }
                else if (instructionItem is InstructionLongHeadOnTheFlyItem)
                {
                    feedLength += (instructionItem as InstructionLongHeadOnTheFlyItem).FinalPosition;
                }
                else if (instructionItem is InstructionCrossHeadOnTheFlyItem
                             && (instructionItem as InstructionCrossHeadOnTheFlyItem).Positions.Any(
                                 otf => otf.ToolState == ToolStates.Cut))
                {
                    foundCutOf = true;
                    Specify.That(feedLength)
                        .Should.BeLogicallyEqualTo(1200, "The cutoff should be after design length");
                }
            }
            Specify.That(foundCutOf).Should.BeTrue("The cutoff instruction was not found");
        }


        [TestMethod]
        [TestCategory("Bug 18635")]
        [TestCategory("Integration")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\Em7PhysicalMachineSettings_mm.cfg.xml", "EmCodeGenTestData")]
        [DeploymentItem("CodeGenerator\\AStarEmTests\\TestData\\05_Crease_Delay_LH_mm.ezd", "EmCodeGenTestData")]
        public void ShouldNotDoCutOff_UntilAtEndOfDesign_WhenStartingOnOtherSide_OfCorrugate()
        {
            var machineSettings = EmPhysicalMachineSettings.CreateFromFile(
                new IPEndPoint(1, 1),
                Path.Combine(
                    Environment.CurrentDirectory,
                    "EmCodeGenTestData",
                    "Em7PhysicalMachineSettings_mm.cfg.xml"));

            machineSettings.CrossHeadParameters.Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 1100;

            machineSettings.LongHeadParameters.MovementData.Speed = 3600;
            machineSettings.LongHeadParameters.MovementData.Acceleration = 15126;

            machineSettings.FeedRollerParameters.MovementData.Acceleration = 4261.36;
            machineSettings.FeedRollerParameters.MovementData.Speed = 1500;

            machineSettings.CrossHeadParameters.MovementData.Acceleration = 15126;
            machineSettings.CrossHeadParameters.MovementData.Speed = 3600;

            var corrugate = new Corrugate { Width = 1200d, Thickness = 3d };
            var order = new Carton() { CartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate } };
            var mic = new EmMachineInstructionCreator(null);
            var track = new Track { TrackNumber = 1, TrackOffset = 100 };

            var design = EmCodeGeneratorTestHelper.LoadAndTileDesign(102051, 500, 500, 500, corrugate, 1);
            var instructionList = mic.GetInstructionListForJob(order, design, track, machineSettings);

            design = EmCodeGeneratorTestHelper.LoadAndTileDesign(102051, 500, 500, 500, corrugate, 1);
            instructionList = mic.GetInstructionListForJob(order, design, track, machineSettings);
            var feedLength = 0d;
            var foundCutOf = false;
            foreach (var instructionItem in instructionList)
            {
                if (instructionItem is InstructionFeedRollerItem)
                {
                    feedLength += (instructionItem as InstructionFeedRollerItem).Position;
                }
                else if (instructionItem is InstructionLongHeadOnTheFlyItem)
                {
                    feedLength += (instructionItem as InstructionLongHeadOnTheFlyItem).FinalPosition;
                }
                else if ((instructionItem is InstructionCrossHeadToolActivation
                          && (instructionItem as InstructionCrossHeadToolActivation).Toolstate == ToolStates.Cut))
                {
                    foundCutOf = true;
                    Specify.That(feedLength)
                        .Should.BeLogicallyEqualTo(1200, "The cutoff should be after design length");
                }
            }
            Specify.That(foundCutOf).Should.BeTrue("The cutoff instruction was not found");
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGenerateLongHeadMovementInstruction_WithSensorPinOnLongHeadAsTargetPosition()
        {
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            machineSettings.LongHeadParameters.LongHeadYOffset = 14;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 400;
            var compensatedDesign = GetACompensatedFefco201(machineSettings);
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100, machineSettings.LongHeadParameters, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(2);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number,
                        TrackOffset = 50
                    }
                }
            };
            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, EmCodeGeneratorTestHelper.GetTrack(2), false);
            //var instructionList = codeGen.Generate(compensatedDesign, machine, longHeadDistribution, 800, track, false);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());

            var longHeadPosItem = instructionList.ElementAt(1) as InstructionLongHeadPositionItem;
            Specify.That(longHeadPosItem.LongHeadsToPosition.ElementAt(0).Position).Should.BeLogicallyEqualTo(150 - 5, "Expected to get the line position - long head tool to sensor offset.");
            Specify.That(longHeadPosItem.LongHeadsToPosition.ElementAt(1).Position).Should.BeLogicallyEqualTo(150 + 54 - 5, "Lh2 is not used and positon should be just next to lh1. Expected Line 1 pos + lh width - sensor to tool lh2.");
            Specify.That(longHeadPosItem.LongHeadsToPosition.ElementAt(2).Position).Should.BeLogicallyEqualTo(550 - 17, "Expected to get the line position - long head tool to sensor offset.");
            Specify.That(longHeadPosItem.LongHeadsToPosition.ElementAt(3).Position).Should.BeLogicallyEqualTo(700 - 5, "Expected to get the line position - long head tool to sensor offset.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateLongHeadPositions_WithPositioningResultFromLastGeneration()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 6);
            var compensatedDesign = GetACompensatedFefco201(machineSettings);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 1300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 1700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 2100;
            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);
            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 800, new Track()
            {
                TrackNumber = 1,
                TrackOffset = 0
            }, false);

            var longHeadPosItem = instructionList.ElementAt(1) as InstructionLongHeadPositionItem;
            Specify.That(longHeadPosItem.LongHeadsToPosition.ElementAt(0).Position).Should.BeLogicallyEqualTo(150 - 5, "Expected to get the line position - long head tool to sensor offset.");
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position).Should.BeLogicallyEqualTo(150);
            Specify.That(longHeadPosItem.LongHeadsToPosition.ElementAt(1).Position).Should.BeLogicallyEqualTo(550 - 5, "Expected to get the line position - long head tool to sensor offset.");
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position).Should.BeLogicallyEqualTo(550);
            Specify.That(longHeadPosItem.LongHeadsToPosition.ElementAt(2).Position).Should.BeLogicallyEqualTo(700 - 17, "Expected to get the line position - long head tool to sensor offset.");
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position).Should.BeLogicallyEqualTo(700);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position).Should.BeLogicallyEqualTo(1300);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position).Should.BeLogicallyEqualTo(1700);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position).Should.BeLogicallyEqualTo(2100);
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Unit")]
        public void OTFCHMovement_Should_Never_PassTheFinalPosition()
        {
            const int finalPosition = 50;
            const int chStartPosition = 2000;

            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 6, crossHeadPosition: chStartPosition);
            var compensatedDesign = GetACompensatedFefco201(machineSettings, finalPosition);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 1300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 1700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 2100;
            var codeGen = new EmCodeGenerator(2000, machineSettings.LongHeadParameters);

            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 700, new Track()
            {
                TrackNumber = 1,
                TrackOffset = finalPosition
            }, true);

            var firstCHOTFMovement = instructionList.OfType<InstructionCrossHeadOnTheFlyItem>().First();
            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            Specify.That(firstCHOTFMovement.Positions.ToList()[0].Position >= 50).Should.BeTrue(String.Format("First CH movement should never pass the final position - CH start position <{0}> final position <{1}> CHChangePosition <{2}>", chStartPosition, finalPosition, firstCHOTFMovement.Positions.ToList()[0].Position));
            Specify.That(firstCHOTFMovement.Positions.ToList()[1].Position >= 50).Should.BeTrue(String.Format("First CH movement should never pass the final position - CH start position <{0}> final position <{1}> CHChangePosition <{2}>", chStartPosition, finalPosition, firstCHOTFMovement.Positions.ToList()[1].Position));
            Specify.That(firstCHOTFMovement.Positions.ToList()[2].Position >= 50).Should.BeTrue(String.Format("First CH movement should never pass the final position - CH start position <{0}> final position <{1}> CHChangePosition <{2}>", chStartPosition, finalPosition, firstCHOTFMovement.Positions.ToList()[2].Position));
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateCrossHeadPosition_WithLastCrossheadMovementInstruction()
        {
            MicroMeter chStartPosition = 2000;
            MicroMeter chFinalPosition = 50;

            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 6, crossHeadPosition: chStartPosition);

            var compensatedDesign = GetACompensatedFefco201(machineSettings, chFinalPosition);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 1300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 1700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 2100;
            var codeGen = new EmCodeGenerator(2000, machineSettings.LongHeadParameters);

            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 800, new Track()
            {
                TrackNumber = 1,
                TrackOffset = chFinalPosition
            }, true);

            var firstCHOTFMovement = instructionList.OfType<InstructionCrossHeadOnTheFlyItem>().First();
            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            Specify.That(machineSettings.CrossHeadParameters.Position).Should.BeLogicallyEqualTo(chFinalPosition);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateCrossHeadPosition_WithToolToSensorOffset()
        {
            MicroMeter linePosition = 50;

            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 6, crossHeadPosition: 2000);
            machineSettings.CrossHeadParameters.ToolPositionToSensor = -38.0;

            var compensatedDesign = GetACompensatedFefco201(machineSettings, linePosition);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 1300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 1700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 2100;
            var codeGen = new EmCodeGenerator(2000, machineSettings.LongHeadParameters);

            var instructionList = codeGen.Generate(compensatedDesign, machineSettings, 800, new Track()
            {
                TrackNumber = 1,
                TrackOffset = 50,
            }, true);
            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            Specify.That(machineSettings.CrossHeadParameters.Position).Should.BeLogicallyEqualTo(linePosition - machineSettings.CrossHeadParameters.ToolPositionToSensor);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUseTargetLongHeadPositionsFromLastGeneration_WhenGeneratingForNextBox()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 6);
            var compensatedDesign = GetACompensatedFefco201(machineSettings, 1200);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 1300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 1700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 2100;
            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = new Track()
            {
                TrackNumber = 1,
                TrackOffset = 50
            };
            codeGen.Generate(compensatedDesign, machineSettings, 800, track, true);

            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position).Should.BeLogicallyEqualTo(100);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position).Should.BeLogicallyEqualTo(500);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position).Should.BeLogicallyEqualTo(900);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position).Should.BeLogicallyEqualTo(1350);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position).Should.BeLogicallyEqualTo(1750);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position).Should.BeLogicallyEqualTo(1900);

            codeGen.Generate(GetACompensatedFefco201(machineSettings, 0), machineSettings, 800, track, false);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position).Should.BeLogicallyEqualTo(150);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position).Should.BeLogicallyEqualTo(550);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position).Should.BeLogicallyEqualTo(700);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position).Should.BeLogicallyEqualTo(1350);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position).Should.BeLogicallyEqualTo(1750);
            Specify.That(machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position).Should.BeLogicallyEqualTo(1900);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGenerateCodeForDesignWithPerforations()
        {
            /*      50     150    250    
             *  0    ⁞       
             *       ⁞       
             *  100  ¦      ⁞           
             *       ¦      ⁞       
             *  200  ¦      ¦      ⁞
             *       ¦      ¦      ⁞
             *  300  ¦      ¦      ¦
             *       ¦      ¦      ¦
             *  ...  ¦      ¦      ¦
             *       ⁞      ¦      ¦
             *  800  ⁞      ¦      ¦
             *              ⁞      ¦ <--- 853
             *  900         ⁞      ¦            
             *                     ⁞
             *  1000_ _ _ _ _ _ _ _⁞ <--- 1000
             *               
             */

            var design = new PhysicalDesign { Length = 1300, Width = 500 };
            var l1 = new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 800), LineType.perforation);
            var l2 = new PhysicalLine(new PhysicalCoordinate(150, 100), new PhysicalCoordinate(150, 900), LineType.perforation);
            var l3 = new PhysicalLine(new PhysicalCoordinate(250, 200), new PhysicalCoordinate(250, 1000), LineType.perforation);
            var l4 = new PhysicalLine(new PhysicalCoordinate(0, 1000), new PhysicalCoordinate(500, 1000), LineType.perforation);

            design.Add(l1);
            design.Add(l2);
            design.Add(l3);
            design.Add(l4);

            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(nrLongheads: 6);
            machineSettings.LongHeadParameters.LongHeadYOffset = 147;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 900;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 1300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 1700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(6).Position = 2100;

            var ruleAppliedDesign = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            }).ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0);

            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = new Track()
            {
                TrackNumber = 1,
                TrackOffset = 50
            };

            var instructionList = codeGen.Generate(ruleAppliedDesign, machineSettings, 800, track, false);

            Specify.That(instructionList).Should.Not.BeNull();
            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            var lhActivations = instructionList.OfType<InstructionLongHeadToolActivations>();
            Specify.That(lhActivations.Count()).Should.BeEqualTo(13);
            Specify.That(lhActivations.Count(l => l.ToolActivations.Any(t => t.ToolState == ToolStates.Perforation))).Should.BeEqualTo(3);
            Specify.That(lhActivations.Count(l => l.ToolActivations.Any(t => t.ToolState == ToolStates.Crease))).Should.BeEqualTo(6);
            Specify.That(lhActivations.Count(l => l.ToolActivations.Any(t => t.ToolState == ToolStates.None))).Should.BeEqualTo(4);

            var chActivations = instructionList.OfType<InstructionCrossHeadToolActivation>();
            Specify.That(chActivations.Count()).Should.BeEqualTo(4);
            Specify.That(chActivations.Count(l => l.Toolstate == ToolStates.Perforation)).Should.BeEqualTo(1);
            Specify.That(chActivations.Count(l => l.Toolstate == ToolStates.Crease)).Should.BeEqualTo(2);
            Specify.That(chActivations.Count(l => l.Toolstate == ToolStates.None)).Should.BeEqualTo(1);
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotMissHorizontalLines_WithinLongheadOffset_FromStartOfDesign()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(1);
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;

            machineSettings.CrossHeadParameters.CutDelayOff = 20;
            machineSettings.CrossHeadParameters.CutDelayOn = 23;
            machineSettings.CrossHeadParameters.CreaseDelayOn = 20;
            machineSettings.CrossHeadParameters.CreaseDelayOff = 23;

            machineSettings.LongHeadParameters.CutDelayOff = 10;
            machineSettings.LongHeadParameters.CutDelayOn = 36;
            machineSettings.LongHeadParameters.CreaseDelayOff = 10;
            machineSettings.LongHeadParameters.CreaseDelayOn = 36;

            var design = new PhysicalDesign();

            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 800), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 20), new PhysicalCoordinate(800, 20), LineType.crease));


            design.Length = 800;
            design.Width = 800;


            var ruleAppliedDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 800, 800, 800, 800, 3, 1, false, 0);

            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = new Track()
            {
                TrackNumber = 1,
                TrackOffset = 50
            };

            var instructionList = codeGen.Generate(ruleAppliedDesign, machineSettings, 800, track, true);
            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            Specify.That(instructionList).Should.Not.BeNull();


            var movementInstructions = instructionList.OfType<InstructionCrossHeadMovementItem>();
            Specify.That(movementInstructions.Any(i => i.Position == 800d)).Should.BeTrue("Expecting one movement from 0 to 800");
            Specify.That(movementInstructions.Any(i => i.Position == 0d)).Should.BeTrue("Expecting one movement from 800 to 0");


            var activationInstructions = instructionList.OfType<InstructionCrossHeadToolActivation>();
            Specify.That(activationInstructions.Count(i => i.Toolstate == ToolStates.Crease)).Should.BeEqualTo(1, "Expecting one crease to be made");
            Specify.That(activationInstructions.Count(i => i.Toolstate == ToolStates.Cut)).Should.BeEqualTo(1, "Expecting one cut at the end of the desigm");
            Specify.That(activationInstructions.Count(i => i.Toolstate == ToolStates.None)).Should.BeEqualTo(2, "Expecting two deactivations, one after the first crease and one after the cut");
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateOtfInstructionsWithPerforations()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(1);
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;

            machineSettings.CrossHeadParameters.CutDelayOff = 20;
            machineSettings.CrossHeadParameters.CutDelayOn = 23;
            machineSettings.CrossHeadParameters.CreaseDelayOn = 20;
            machineSettings.CrossHeadParameters.CreaseDelayOff = 23;

            machineSettings.LongHeadParameters.CutDelayOff = 10;
            machineSettings.LongHeadParameters.CutDelayOn = 36;
            machineSettings.LongHeadParameters.CreaseDelayOff = 10;
            machineSettings.LongHeadParameters.CreaseDelayOn = 36;

            var design = new PhysicalDesign();

            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 800), LineType.perforation));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 20), new PhysicalCoordinate(500, 20), LineType.perforation));


            design.Length = 800;
            design.Width = 800;


            var ruleAppliedDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 800, 800, 800, 800, 3, 1, false, 0);

            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);

            var track = new Track()
            {
                TrackNumber = 1,
                TrackOffset = 50
            };

            var instructionList = codeGen.Generate(ruleAppliedDesign, machineSettings, 800, track, true);

            CheckInstructionListConstraints(instructionList, machineSettings.LongHeadParameters.LongHeads.Count());
            Specify.That(instructionList).Should.Not.BeNull();

            var feedLength = 0d;
            instructionList.OfType<InstructionFeedRollerItem>().ForEach(i => feedLength += i.Position);
            instructionList.OfType<InstructionLongHeadOnTheFlyItem>().ForEach(i => feedLength += i.FinalPosition);
            Specify.That(feedLength).Should.BeLogicallyEqualTo(lhOffset + 800 + 400);

            var lhOtf = instructionList.OfType<InstructionLongHeadOnTheFlyItem>();
            Specify.That(lhOtf.Count()).Should.BeEqualTo(2, "Should get one LH OTF instruction for each section");
            var lh = lhOtf.First();
            Specify.That(lh.Positions.Count()).Should.BeEqualTo(1, "The first LH OTF instruction only contains the first crease activation");
            Specify.That(lh.Positions.ElementAt(0).ToolStates.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease, "The first LH OTF instruction only contains the first crease activation");
            Specify.That(lh.FinalPosition).Should.BeLogicallyEqualTo(167, "This position is LH Offset + 20 (the horizontal line)");

            lh = lhOtf.Last();
            Specify.That(lh.Positions.Count()).Should.BeEqualTo(1, "This is the final outfeed and will only contain the last crease activation");
            Specify.That(lh.Positions.ElementAt(0).ToolStates.ElementAt(0).ToolState).Should.BeEqualTo(ToolStates.Crease, "last first LH OTF instruction only contains the last crease activation");
            Specify.That(lh.FinalPosition).Should.BeLogicallyEqualTo(547, "At this position the cut-off line is made and we feed 400+lhoffset");

            var chActivations = instructionList.OfType<InstructionCrossHeadOnTheFlyItem>();
            Specify.That(chActivations.Count()).Should.BeEqualTo(1);
            var ch = chActivations.First();
            Specify.That(ch.Positions.Count()).Should.BeEqualTo(2);
            Specify.That(ch.FinalPosition).Should.BeLogicallyEqualTo(493);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUseAlreadyPositionedLongheadIfWasteCanBeSeparated()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetWasteSeparatorDesign(machineSettings);
            var longHeadPositions = new MicroMeter[] { 100, 200, 300, 400 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 400;

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(new EmWasteSeparationSuccessorCreator()), compensatedDesign);

            var solutionNodes = longHeadDistribution.GetSolutionNodes();

            Specify.That(solutionNodes.Count()).Should.BeEqualTo(1, "We only expect one section here");

            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(100d, "LH1 should cut the first line");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(200d, "LH2 should have been moved the minimum distance to allow LH3 to cut the line and separate the waste");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(400d, "LH3 should cut the second line and separate the waste at the same time");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(494d, "This longhead is not used");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPlaceLongheadsToConvertLinesAndSeparateWaste()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetWasteSeparatorDesign(machineSettings);
            var longHeadPositions = new MicroMeter[] { 600, 700, 800, 900 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 600;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 800;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 900;

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(
                machineSettings,
                longHeadPositions,
                new SectionCreator(lhOffset),
                new SectionEvaluator(new EmWasteSeparationSuccessorCreator()),
                compensatedDesign);

            var cg = new EmCodeGeneratorWithWasteSeparation(0, machineSettings.LongHeadParameters);


            var track = new Track() { TrackNumber = 1, TrackOffset = 50 };
            var instr = cg.Generate(compensatedDesign, machineSettings, 600d, track, false);
            instr.ForEach(Console.WriteLine);
            var solutionNodes = longHeadDistribution.GetSolutionNodes();
            Specify.That(solutionNodes.Count()).Should.BeEqualTo(1, "We only expect one section");

            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(200d, "LH1 should cut the first line");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(400d - 14d, "LH2 should have been moved the minimum distance to allow LH3 to cut the line and separate the waste");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(400d, "LH3 should cut the second line and separate the waste at the same time");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(900d, "This longhead is not used");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUseAlreadyPositionedLongheads_WhenWasteAndLinesCanBeConvertedWithoutReposition()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetSheetWithSideWasteDesign(machineSettings);
            var longHeadPositions = new MicroMeter[] { 600, 700, 800, 900 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 600;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 700;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 800;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 900;

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(new EmWasteSeparationSuccessorCreator()), compensatedDesign);


            Specify.That(longHeadDistribution.Sections.Count()).Should.BeEqualTo(1);
            foreach (var node in longHeadDistribution.GetSolutionNodes())
            {
                Specify.That(node.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(600d);
                Specify.That(node.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(700d);
                Specify.That(node.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(800d);
                Specify.That(node.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(900d);
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldMoveLhInWasteToMinPositionInWaste_WithoutMovingPreviousLh()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetDesignWithWasteBetweenTwoLines(machineSettings);
            var longHeadPositions = new MicroMeter[] { 300, 347, 450, 600 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 300;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).LeftSideToTool = 7;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 347;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 410;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).LeftSideToTool = 47;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 600;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).LeftSideToTool = 7;


            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(new EmWasteSeparationSuccessorCreator()), compensatedDesign);


            Specify.That(longHeadDistribution.Sections.Count()).Should.BeEqualTo(2);
            foreach (var node in longHeadDistribution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
                Specify.That(node.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(300);
                Specify.That(node.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(314);
                Specify.That(node.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(409);
                Specify.That(node.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(600);
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUseUpcomingCoordinates_WhenSeparatingWaste()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var compensatedDesign = GetWasteSeparatorDesignWidthCreaseAfterWasteArea(machineSettings);
            var longHeadPositions = new MicroMeter[] { 100, 170, 500, 600 };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 170;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 500;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 600;

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(new EmWasteSeparationSuccessorCreator()), compensatedDesign);

            Specify.That(longHeadDistribution.Sections.Count()).Should.BeEqualTo(1);
            foreach (var node in longHeadDistribution.GetSolutionNodes())
            {
                Specify.That(node.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(100);
                Specify.That(node.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(160);
                Specify.That(node.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(500);
                Specify.That(node.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(600);
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAllowWasteSeparatorsOutsideWasteAreas_WhenAtCorrugateEdges()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var offset = 100d;
            var compensatedDesign = this.GetWasteSeparatorDesignWithNarrowWasteAreas(machineSettings, offset);

            Specify.That(compensatedDesign.WasteAreas.Count()).Should.BeEqualTo(2);
            Specify.That(compensatedDesign.WasteAreas.Count(wa => wa.TopLeft.X == 0 + offset && wa.TopLeft.Y == 0 && wa.BottomRight.X == 31 + offset && wa.BottomRight.Y == 100)).Should.BeEqualTo(1);
            Specify.That(compensatedDesign.WasteAreas.Count(wa => wa.TopLeft.X == 600 + offset && wa.TopLeft.Y == 0 && wa.BottomRight.X == 631 + offset && wa.BottomRight.Y == 100)).Should.BeEqualTo(1);

            var longHeadPositions = new MicroMeter[] { 100 + offset, 200 + offset, 300 + offset, 600 + offset };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 100 + offset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 200 + offset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 300 + offset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 600 + offset;

            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(new EmWasteSeparationSuccessorCreator()), compensatedDesign);

            Specify.That(longHeadDistribution.Sections.Count()).Should.BeEqualTo(1);

            var solutionNodes = longHeadDistribution.GetSolutionNodes();
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(31 + offset, "LH1 should cut the first line");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(200 + offset, "LH2 should have been moved the minimum distance to allow LH3 to cut the line and separate the waste");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(600 + offset, "LH3 should cut the second line and separate the waste at the same time");
            Specify.That(solutionNodes.ElementAt(0).NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(694 + offset, "This longhead is not used");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldGenerateCode_ForASheet_WithTHeSameWidth_AsTheCorrugate()
        {
            /* 
             *  0   100   200   300   400
             * 0      __________________
             *       |                  |
             *       |                  |
             * 400   |------------------|
             *       |                  |
             * 800   |__________________|
             */

            var design = new PhysicalDesign { Length = 800, Width = 400 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 800), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(400, 0), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 800), LineType.separate));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(400, 800), LineType.separate));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 400), new PhysicalCoordinate(400, 400), LineType.crease));

            var mic = new EmMachineInstructionCreator(new ConsoleLogger());


            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            var track = new Track { TrackNumber = 1, TrackOffset = 100d };

            var carton = new Carton();
            var corrugate = new Corrugate { Alias = "MyCorrugate", Thickness = 3d, Width = 400d };
            carton.CartonOnCorrugate = new CartonOnCorrugate(carton, corrugate, "800:400:0", new PackagingDesignCalculation { Width = 400, Length = 800 }, OrientationEnum.Degree0);

            var instructionList = mic.GetInstructionListForJob(carton, design, track, machineSettings);

            instructionList.ForEach(Console.WriteLine);
            Specify.That(instructionList).Should.Not.BeNull("Should not get an exception");
            Specify.That(instructionList.Count(i => i is InstructionCrossHeadMovementItem)).Should.BeLogicallyEqualTo(2, "Should get 2 Crosshead movement instructions. One to make the crease since the crosshead starts at the correct position, and then one to make the final cut.");
            Specify.That(instructionList.Count(i => i is InstructionCrossHeadToolActivation)).Should.BeLogicallyEqualTo(4, "Should get 4 Crosshead tool activations: Crease -> None -> Cut -> None");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToProduceBoxesShorterThanDistanceBetweenCrossHeadAndLongHeads()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);
            var design = new PhysicalDesign() {Length = machineSettings.LongHeadParameters.LongHeadYOffset - 1};
            Specify.That(codeGen.ValidateLongHeadPositions(design, machineSettings, 0, false, 0)).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToProduceBoxesLongerOrEqualToDistanceBetweenCrossHeadAndLongHeads()
        {
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var codeGen = new EmCodeGenerator(0, machineSettings.LongHeadParameters);
            var design = new PhysicalDesign() { Length = machineSettings.LongHeadParameters.LongHeadYOffset };
            Specify.That(codeGen.ValidateLongHeadPositions(design, machineSettings, 0, false, 0)).Should.BeTrue();

            design = new PhysicalDesign() { Length = machineSettings.LongHeadParameters.LongHeadYOffset+1 };
            Specify.That(codeGen.ValidateLongHeadPositions(design, machineSettings, 0, false, 0)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleWasteAreas_InDesignWithManySections()
        {
            Assert.Inconclusive("Waste separation feature not used. Test passes when waste areas and waste cutting is performed on the design");
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters(5);
            var offset = 0d;
            var compensatedDesign = GetDesignWithMoreThanOneSectionAndSomeWasteAreas(machineSettings, offset);

            Specify.That(compensatedDesign.WasteAreas.Count()).Should.BeEqualTo(14);


            var longHeadPositions = new MicroMeter[] { 200 + offset, 500 + offset, 800 + offset, 1100 + offset, 1200 + offset };
            machineSettings.LongHeadParameters.LongHeadYOffset = lhOffset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(1).Position = 200 + offset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(2).Position = 500 + offset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(3).Position = 800 + offset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).Position = 1100 + offset;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(4).LeftSideToTool = 7;
            machineSettings.LongHeadParameters.GetLongHeadByNumber(5).Position = 1200 + offset;


            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, longHeadPositions, new SectionCreator(lhOffset), new SectionEvaluator(new EmWasteSeparationSuccessorCreator()), compensatedDesign);

            Specify.That(longHeadDistribution.Sections.Count()).Should.BeEqualTo(3);
            foreach (var node in longHeadDistribution.GetSolutionNodes())
            {
                Console.WriteLine(node);
                Console.WriteLine("----------------------");
            }

            var firstNode = longHeadDistribution.GetSolutionNodes().ElementAt(0);
            Specify.That(firstNode.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(100d + offset);
            Specify.That(firstNode.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(200d + offset);
            Specify.That(firstNode.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(550d + offset);
            Specify.That(firstNode.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(650d + offset);
            Specify.That(firstNode.NewLongHeadPositions[4]).Should.BeLogicallyEqualTo(750d + offset);

            var secondNode = longHeadDistribution.GetSolutionNodes().ElementAt(1);
            Specify.That(secondNode.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(96d + offset);
            Specify.That(secondNode.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(150d + offset);
            Specify.That(secondNode.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(200d + offset);
            Specify.That(secondNode.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(550d + offset);
            Specify.That(secondNode.NewLongHeadPositions[4]).Should.BeLogicallyEqualTo(750d + offset);

            var thirdNode = longHeadDistribution.GetSolutionNodes().ElementAt(2);
            Specify.That(thirdNode.NewLongHeadPositions[0]).Should.BeLogicallyEqualTo(200d + offset);
            Specify.That(thirdNode.NewLongHeadPositions[1]).Should.BeLogicallyEqualTo(254d + offset);
            Specify.That(thirdNode.NewLongHeadPositions[2]).Should.BeLogicallyEqualTo(550d + offset);
            Specify.That(thirdNode.NewLongHeadPositions[3]).Should.BeLogicallyEqualTo(650d + offset);
            Specify.That(thirdNode.NewLongHeadPositions[4]).Should.BeLogicallyEqualTo(742d + offset); // 750?
        }



        private void CheckInstructionListConstraints(IEnumerable<InstructionItem> instructionList, int lhCount)
        {
            Specify.That(instructionList).Should.Not.BeNull("The instruction list is null");
            var lhState = new short[lhCount];
            foreach (var instr in instructionList)
            {
                if (instr is InstructionLongHeadToolActivations)
                {
                    var arr = instr.ToPlcArray();
                    for (int i = 2; i < arr.Count(); i += 2)
                    {
                        lhState[arr[i] - 1] = arr[i + 1];
                    }
                }
                else if (instr is InstructionLongHeadOnTheFlyItem)
                {
                    var otfItem = instr as InstructionLongHeadOnTheFlyItem;
                    otfItem.Positions.ForEach(p => p.ToolStates.ForEach(ts => lhState[ts.LongHeadNumber - 1] = (short)ts.ToolState));
                }
                else if (instr is InstructionLongHeadPositionItem)
                {
                    var arr = instr.ToPlcArray();
                    for (int i = 2; i < arr.Count(); i += 3)
                    {
                        Specify.That(lhState[arr[i] - 1])
                            .Should.BeEqualTo(
                                (short)0,
                                string.Format(
                                    "Moving an activated longhead. LH {0} is not deactivated (State: {1})",
                                    arr[i],
                                    lhState[arr[i] - 1] == 3 ? "Cut" : "Crease"));
                    }
                }
            }

            var trackActivationInstructions = instructionList.OfType<InstructionTrackActivationItem>();
            Specify.That(trackActivationInstructions.Count(ta => ta.Activate)).Should.BeEqualTo(1, "There should be exactly one track activation instruction.");
            Specify.That(trackActivationInstructions.Count(ta => ta.Activate == false)).Should.BeEqualTo(1, "There should be exactly one track deactivation instruction.");
        }

        private RuleAppliedPhysicalDesign GetDesignWithReverseAndCrossHeadInstructions()
        {
            var design = new RuleAppliedPhysicalDesign();

            var l1 = new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 800), LineType.crease);
            var l2 = new PhysicalLine(new PhysicalCoordinate(210, 500), new PhysicalCoordinate(210, 800), LineType.crease);

            var l3 = new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(200, 300), LineType.cut);
            var l4 = new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(200, 600), LineType.cut);
            var l5 = new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(500, 800), LineType.cut);

            design.Add(l1);
            design.Add(l2);
            design.Add(l3);
            design.Add(l4);
            design.Add(l5);

            return design;
        }

        private RuleAppliedPhysicalDesign GetLongDesignWithReverse()
        {
            var design = new RuleAppliedPhysicalDesign();

            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 3100d), LineType.crease));

            design.Add(new PhysicalLine(new PhysicalCoordinate(210, 1000), new PhysicalCoordinate(210, 1200), LineType.crease));

            design.Add(new PhysicalLine(new PhysicalCoordinate(220, 500), new PhysicalCoordinate(220, 2200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(230, 900), new PhysicalCoordinate(230, 1000), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(240, 1500), new PhysicalCoordinate(240, 3100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 1700), new PhysicalCoordinate(250, 3100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(260, 2000), new PhysicalCoordinate(260, 2700), LineType.crease));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 3100d), new PhysicalCoordinate(500, 3100d), LineType.cut));

            return design;
        }

        private RuleAppliedPhysicalDesign GetDesignWithFourSections()
        {
            var design = new RuleAppliedPhysicalDesign();

            var l1 = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 200), LineType.crease);

            var l2 = new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 200), LineType.crease);
            var l3 = new PhysicalLine(new PhysicalCoordinate(350, 0), new PhysicalCoordinate(350, 800), LineType.cut);

            var l4 = new PhysicalLine(new PhysicalCoordinate(50, 200), new PhysicalCoordinate(50, 400), LineType.crease);
            var l5 = new PhysicalLine(new PhysicalCoordinate(250, 200), new PhysicalCoordinate(250, 400), LineType.crease);

            var l6 = new PhysicalLine(new PhysicalCoordinate(100, 400), new PhysicalCoordinate(100, 600), LineType.crease);
            var l7 = new PhysicalLine(new PhysicalCoordinate(200, 400), new PhysicalCoordinate(200, 600), LineType.crease);

            var l8 = new PhysicalLine(new PhysicalCoordinate(50, 600), new PhysicalCoordinate(50, 800), LineType.crease);
            var l9 = new PhysicalLine(new PhysicalCoordinate(250, 600), new PhysicalCoordinate(250, 800), LineType.crease);

            var l10 = new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(600, 800), LineType.cut);

            design.Add(l1);
            design.Add(l2);
            design.Add(l3);
            design.Add(l4);
            design.Add(l5);
            design.Add(l6);
            design.Add(l7);
            design.Add(l8);
            design.Add(l9);
            design.Add(l10);

            return design;
        }

        private RuleAppliedPhysicalDesign GetDesignWithTwoSectionsAndACrossHeadLineFiveMillimetersFromTheStartOfSectionTwo(EmPhysicalMachineSettings machineSettings, double trackOffset = 0)
        {
            var design = new PhysicalDesign();

            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(103, 100), new PhysicalCoordinate(103, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 105), new PhysicalCoordinate(200, 105), LineType.crease));

            design.Length = 200;
            design.Width = 200;

            return EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 200, 200, 200, 800, 3, 1, false, trackOffset);
        }

        private RuleAppliedPhysicalDesign GetDesignWithLhOTF(EmPhysicalMachineSettings machineSettings, double trackOffset = 0)
        {
            var design = new PhysicalDesign();

            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(50, 200), new PhysicalCoordinate(50, 300), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 100), new PhysicalCoordinate(150, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 200), new PhysicalCoordinate(150, 300), LineType.crease));

            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 0), new PhysicalCoordinate(250, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 100), new PhysicalCoordinate(250, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(250, 200), new PhysicalCoordinate(250, 300), LineType.crease));

            design.Length = 300;
            design.Width = 250;

            return EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 300, 200, 200, 800, 3, 1, false, trackOffset);
        }

        private RuleAppliedPhysicalDesign GetDesignWithCrossHeadLinesCloseToSectionStartAndReverses(EmPhysicalMachineSettings machineSettings, double trackOffset = 0)
        {
            var design = new PhysicalDesign();

            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(110, 300), new PhysicalCoordinate(110, 600), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(120, 550), new PhysicalCoordinate(120, 900), LineType.crease));


            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 70), new PhysicalCoordinate(50, 70), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 575), new PhysicalCoordinate(50, 575), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 670), new PhysicalCoordinate(50, 670), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 740), new PhysicalCoordinate(50, 740), LineType.cut));

            design.Length = 900;
            design.Width = 200;

            return EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 900, 200, 200, 600, 3, 1, false, trackOffset);
        }

        private RuleAppliedPhysicalDesign GetACompensatedFefco201(EmPhysicalMachineSettings machineSettings, double trackOffset = 0)
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 500),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 0), new PhysicalCoordinate(700, 500),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(150, 50),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 50), new PhysicalCoordinate(700, 50),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 450), new PhysicalCoordinate(150, 450),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 450), new PhysicalCoordinate(700, 450),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(700, 500),
                LineType.cut);
            design.Add(line);

            design.Length = 500;
            design.Width = 700;

            return EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 200, 200, 200, 800, 3, 1, false, trackOffset);
        }

        private RuleAppliedPhysicalDesign GetDesignWithTwoVerticalLines(EmPhysicalMachineSettings machineSettings, double trackOffset = 0)
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 500),
                LineType.crease);
            design.Add(line);

            design.Length = 500;
            design.Width = 700;

            return EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 200, 200, 200, 800, 3, 1, false, trackOffset);
        }

        private RuleAppliedPhysicalDesign GetDesignWithFourVerticalLines(EmPhysicalMachineSettings machineSettings, double trackOffset = 0)
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 500),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(220, 0), new PhysicalCoordinate(220, 500),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(340, 0), new PhysicalCoordinate(340, 500),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(460, 0), new PhysicalCoordinate(460, 500),
                LineType.crease);
            design.Add(line);

            design.Length = 500;
            design.Width = 700;

            return EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 200, 200, 200, 800, 3, 1, false, trackOffset);
        }

        private RuleAppliedPhysicalDesign GetDesignWithTwoSections()
        {
            var weirdDesign = new RuleAppliedPhysicalDesign();

            var l1 = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 400), LineType.crease);
            var l2 = new PhysicalLine(new PhysicalCoordinate(250, 300), new PhysicalCoordinate(250, 500), LineType.cut);
            var l3 = new PhysicalLine(new PhysicalCoordinate(500, 0), new PhysicalCoordinate(500, 500), LineType.cut);

            var l4 = new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(150, 200), LineType.cut);
            var l5 = new PhysicalLine(new PhysicalCoordinate(250, 300), new PhysicalCoordinate(500, 300), LineType.cut);
            var l6 = new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(500, 500), LineType.cut);

            weirdDesign.Add(l1);
            weirdDesign.Add(l2);
            weirdDesign.Add(l3);
            weirdDesign.Add(l4);
            weirdDesign.Add(l5);
            weirdDesign.Add(l6);

            return weirdDesign;
        }

        private RuleAppliedPhysicalDesign GetDesignWithWasteBetweenTwoLines(EmPhysicalMachineSettings machineSettings)
        {
            /*  _______     _____
             * |       |___|     |
             * |_________________|
             * 
             */
            var design = new PhysicalDesign
            {
                Length = 200,
                Width = 600
            };

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(300, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(409, 0), new PhysicalCoordinate(600, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(300, 100), new PhysicalCoordinate(409, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(600, 200), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(409, 0), new PhysicalCoordinate(409, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(600, 0), new PhysicalCoordinate(600, 200), LineType.cut));

            var compensatedDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 600, 600, 600, 700, 3, 1, false, 0);

            compensatedDesign.AddWasteArea(new WasteArea(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(362, 100)));
            compensatedDesign.AddWasteArea(new WasteArea(new PhysicalCoordinate(362, 0), new PhysicalCoordinate(409, 100)));
            compensatedDesign.AddWasteArea(new WasteArea(new PhysicalCoordinate(600, 0), new PhysicalCoordinate(700, 200)));

            return compensatedDesign;
        }

        private RuleAppliedPhysicalDesign GetWasteSeparatorDesignWidthCreaseAfterWasteArea(EmPhysicalMachineSettings machineSettings)
        {
            /*
             *       100  160  220
             *  ______
             * |      |________
             * |          :    |
             * |__________:____|
             */
            var design = new PhysicalDesign();
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(100, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 100), new PhysicalCoordinate(220, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(220, 300), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 300), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(160, 100), new PhysicalCoordinate(160, 300), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(220, 100), new PhysicalCoordinate(220, 300), LineType.cut));

            design.Length = 300;
            design.Width = 220;

            var compensatedDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 600, 600, 600, 220, 3, 1, false, 0);

            compensatedDesign.AddWasteArea(new WasteArea(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(220, 100)));

            return compensatedDesign;
        }

        private RuleAppliedPhysicalDesign GetWasteSeparatorDesign(EmPhysicalMachineSettings machineSettings)
        {
            /*
             *        ____
             *   ____|    |____
             *  |              |
             *  |              |
             *  |              |
             *  |______________|
             */
            var weirdDesign = new PhysicalDesign();
            var lines = new List<PhysicalLine>()
                        {
                            new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 600), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(200, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(400, 0), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(400, 100), new PhysicalCoordinate(600, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(600, 100), new PhysicalCoordinate(600, 600), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(600, 600), LineType.cut)
                        };

            lines.ForEach(weirdDesign.Add);
            weirdDesign.Length = 600;
            weirdDesign.Width = 600;

            var design = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, weirdDesign, 600, 600, 600, 600, 3, 1, false, 0);

            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(200, 100)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(600, 100)));

            return design;
        }

        private RuleAppliedPhysicalDesign GetSheetWithSideWasteDesign(EmPhysicalMachineSettings machineSettings)
        {
            var design = new PhysicalDesign();

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(600, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(600, 0), new PhysicalCoordinate(600, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(600, 600), LineType.cut));
            design.Length = 600;
            design.Width = 600;
            var compensatedDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 600, 600, 600, 750, 3, 1, false, 0);
            //compensatedDesign.AddWasteArea(new WasteArea(new PhysicalCoordinate(600,0), new PhysicalCoordinate(750, 600)));

            return compensatedDesign;
        }

        private RuleAppliedPhysicalDesign GetWasteSeparatorDesignWithNarrowWasteAreas(EmPhysicalMachineSettings machineSettings, double offset = 0)
        {
            /*
             *      _______
             *  ___|       |___
             *  |______________|
             */
            var weirdDesign = new PhysicalDesign();
            var lines = new List<PhysicalLine>()
                        {
                            new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 600), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(31, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(31, 0), new PhysicalCoordinate(31, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(31, 0), new PhysicalCoordinate(600, 0), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(600, 0), new PhysicalCoordinate(600, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(600, 100), new PhysicalCoordinate(631, 100), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(631, 100), new PhysicalCoordinate(631, 600), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(631, 600), LineType.cut)
                        };

            lines.ForEach(weirdDesign.Add);
            weirdDesign.Length = 600;
            weirdDesign.Width = 631;

            var design = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, weirdDesign, 600, 631, 600, 631, 3, 1, false, offset);
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(0 + offset, 0), new PhysicalCoordinate(31 + offset, 100)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(600 + offset, 0), new PhysicalCoordinate(631 + offset, 100)));

            return design;
        }


        private RuleAppliedPhysicalDesign GetDesignWithMoreThanOneSectionAndSomeWasteAreas(EmPhysicalMachineSettings machineSettings, double offset = 0)
        {
            /*
             *         _______
             *   _____|…………………|_____
             *  |     ¦       ¦     |
             *  |     ¦       ¦     |
             *  |_____¦…………………¦_____|
             *  |     ¦       ¦     |
             *  |     ¦       ¦     |
             *  |_____¦…………………¦_____|
             *  |     ¦       ¦  |  
             *  |     ¦       ¦  |  
             *  |     ¦       ¦  |  
             *  |_____¦…………………¦__|__
             *  |_____¦       ¦     |
             *    |   ¦       ¦     |
             *    |_  ¦       ¦     |
             *   ___|_¦       ¦   __|
             *  |     ¦       ¦  |  
             *  |_____¦_______¦__|
             */
            var design = new PhysicalDesign();


            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 650), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(0, 900), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 650), new PhysicalCoordinate(100, 750), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 750), new PhysicalCoordinate(150, 800), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(200, 900), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 100), new PhysicalCoordinate(550, 900), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(750, 100), new PhysicalCoordinate(750, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(650, 400), new PhysicalCoordinate(650, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(750, 600), new PhysicalCoordinate(750, 800), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(650, 800), new PhysicalCoordinate(650, 900), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(550, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(200, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(550, 100), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 100), new PhysicalCoordinate(750, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(200, 250), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 250), new PhysicalCoordinate(550, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 250), new PhysicalCoordinate(750, 250), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 400), new PhysicalCoordinate(200, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 400), new PhysicalCoordinate(550, 400), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 400), new PhysicalCoordinate(750, 400), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(200, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 600), new PhysicalCoordinate(550, 600), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 600), new PhysicalCoordinate(750, 600), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 650), new PhysicalCoordinate(200, 650), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 750), new PhysicalCoordinate(150, 750), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(200, 800), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(650, 800), new PhysicalCoordinate(750, 800), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 900), new PhysicalCoordinate(650, 900), LineType.cut));

            design.Length = 900;
            design.Width = 750;

            var compDesign = EmCodeGeneratorTestHelper.GetCompensatedDesign(machineSettings, 10, design, 900, 750, 600, 900, 3, 1, false, offset);

            return compDesign;
        }



    }
}
