﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Utils;

namespace BusinessTests.CodeGenerator
{
    using System.Linq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.AStarStuff;
    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class OnTheFlyTests
    {
        private const double lhOffset = 147;

        [Ignore]
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldGenerateOtfInstructions()
        {
            var compensatedDesign = EmCodeGeneratorTestHelper.GetDesignWithReverses();
            var longHeadParams = new EmLongHeadParameters() { LongHeadYOffset = 14 };
            longHeadParams.AddLongHead(new EmLongHead() { Number = 1 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 2 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 3 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 4 });
            var codeGen = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100d, longHeadParams, new EmCodeGeneratorTestHelper.NonOptimizingOptimizer());
            var codeGenOptimized = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(100d, longHeadParams, new CodeOptimizer());
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, new MicroMeter[] { 100, 200, 300, 400 }, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number
                    }
                }
            };

            var instructionList = codeGen.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, false);
            var otfInstructions = instructionList.Where(i => i is OnTheFlyInstructionItem);
            Specify.That(otfInstructions.Count()).Should.BeLogicallyEqualTo(0, "We should not have any OTF instructions yet.");

            var optimizedInstr = codeGenOptimized.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, true);

            otfInstructions = optimizedInstr.Where(i => i is OnTheFlyInstructionItem);
            Specify.That(otfInstructions.Any()).Should.BeTrue("We expect some instructions to have been converted into OTF instructions");
            Specify.That(otfInstructions.Count(i => i is InstructionCrossHeadOnTheFlyItem)).Should.BeEqualTo(1, "There should be one instance where a series of crosshead movements/activations are converted into OTF instructions (cut off line)");
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldMoveCrossheadToEndOrStart_BeforeCreatingOnTheFlyInstruction_IfNeeded()
        {
            var compensatedDesign = EmCodeGeneratorTestHelper.GetDesignForCrossheadOnTheFlyTest();
            var longHeadParams = new EmLongHeadParameters() { LongHeadYOffset = 147 };
            longHeadParams.AddLongHead(new EmLongHead() { Number = 1 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 2 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 3 });
            longHeadParams.AddLongHead(new EmLongHead() { Number = 4 });

            var codeGenOptimized = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(0d, longHeadParams, new CodeOptimizer());
            var machineSettings = EmCodeGeneratorTestHelper.GetEmMachineParameters();
            var longHeadDistribution = new EmLongHeadDistributor().DistributeLongHeads(machineSettings, new MicroMeter[] { 100, 200, 300, 400 }, new SectionCreator(lhOffset), new SectionEvaluator(), compensatedDesign);

            var track = EmCodeGeneratorTestHelper.GetTrack(1);
            var machine = new EmMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = track.Number
                    }
                }
            };


            var optimizedInstr = codeGenOptimized.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, true);

            Specify.That(optimizedInstr.OfType<InstructionCrossHeadMovementItem>().Count(i => i.Position == 0)).Should.BeLogicallyEqualTo(2);
            Specify.That(optimizedInstr.OfType<InstructionCrossHeadMovementItem>().Count(i => i.Position == 600)).Should.BeLogicallyEqualTo(1);
            Specify.That(optimizedInstr.OfType<InstructionCrossHeadOnTheFlyItem>().Count()).Should.BeLogicallyEqualTo(2);

            var chPos = 0d;
            for (var i = 0; i < optimizedInstr.Count(); i++)
            {
                if (optimizedInstr.ElementAt(i) is InstructionCrossHeadMovementItem)
                {
                    chPos = (optimizedInstr.ElementAt(i) as InstructionCrossHeadMovementItem).Position;
                }
                else if(optimizedInstr.ElementAt(i) is InstructionCrossHeadOnTheFlyItem)
                {
                    var otfChInstruction = optimizedInstr.ElementAt(i) as InstructionCrossHeadOnTheFlyItem;
                    Specify.That(otfChInstruction.FinalPosition).Should.Not.BeLogicallyEqualTo(chPos, "We don't want the final position to be where we are");
                    Specify.That(chPos == 0d || chPos == 600d).Should.BeTrue("In this design we expect the ch otf instructions to start at the edges of the design.");
                    chPos = otfChInstruction.FinalPosition;
                }
            }

            codeGenOptimized = new EmCodeGeneratorTestHelper.EmCodeGeneratorTester(600d, longHeadParams, new CodeOptimizer()); // Starting with crosshead on another position
            optimizedInstr = codeGenOptimized.Generate(compensatedDesign, machine.PhysicalMachineSettings as EmPhysicalMachineSettings, longHeadDistribution, track, true);

            Specify.That(optimizedInstr.OfType<InstructionCrossHeadMovementItem>().Count(i => i.Position == 0)).Should.BeLogicallyEqualTo(1);
            Specify.That(optimizedInstr.OfType<InstructionCrossHeadMovementItem>().Count(i => i.Position == 600)).Should.BeLogicallyEqualTo(2);
            Specify.That(optimizedInstr.OfType<InstructionCrossHeadOnTheFlyItem>().Count()).Should.BeLogicallyEqualTo(2);


            chPos = 0d;
            for (var i = 0; i < optimizedInstr.Count(); i++)
            {
                if (optimizedInstr.ElementAt(i) is InstructionCrossHeadMovementItem)
                {
                    chPos = (optimizedInstr.ElementAt(i) as InstructionCrossHeadMovementItem).Position;
                }
                else if (optimizedInstr.ElementAt(i) is InstructionCrossHeadOnTheFlyItem)
                {
                    var otfChInstruction = optimizedInstr.ElementAt(i) as InstructionCrossHeadOnTheFlyItem;
                    Specify.That(otfChInstruction.FinalPosition).Should.Not.BeLogicallyEqualTo(chPos, "We don't want the final position to be where we are");
                    Specify.That(chPos == 0d || chPos == 600d).Should.BeTrue("In this design we expect the ch otf instructions to start at the edges of the design.");
                    chPos = otfChInstruction.FinalPosition;
                }
            }

        }


    }
}
