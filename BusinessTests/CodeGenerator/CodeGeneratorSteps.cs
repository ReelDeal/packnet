﻿using Moq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Utils;

namespace BusinessTests.CodeGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.Enums;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;
    using PackNet.Common.Utils;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class CodeGeneratorSteps
    {
        private RuleAppliedPhysicalDesign design;
        private MicroMeter corrugateWidth;
        private FusionPhysicalMachineSettings machineSettings;
        private FusionMachine machine;
        private IEnumerable<InstructionItem> instructionList;
        private FusionCodeGenerator generator;
        private bool longHeadValidationResult;
        private bool lhDistributionExceptionThrown;

        private FusionCodeGenerator airCodeGenerator;
        private FusionCodeGenerator electricCodeGenerator;
        private IEnumerable<InstructionItem> airInstructionList;
        private IEnumerable<InstructionItem> electricInstructionList;

        private Mock<ILogger> logger = new Mock<ILogger>();

        [Given(@"a compensated fefco 201")]
        public void GivenACompensatedFefco201()
        {
            design = new RuleAppliedPhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 500),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 0), new PhysicalCoordinate(700, 500),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(150, 50),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 50), new PhysicalCoordinate(800, 50),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 450), new PhysicalCoordinate(150, 450),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 450), new PhysicalCoordinate(800, 450),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(800, 500),
                LineType.cut);
            design.Add(line);

            design.Length = 100;
        }

        [Given(@"a compensated tiled fefco 201")]
        public void GivenACompensatedTiledFefco201()
        {
            design = new RuleAppliedPhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 800),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 800),
                LineType.crease);
            design.Add(line); 
            line = new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 800),
                 LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(500, 0), new PhysicalCoordinate(500, 800),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(600, 0), new PhysicalCoordinate(600, 800),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 0), new PhysicalCoordinate(700, 800),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(300, 100),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(400, 100), new PhysicalCoordinate(600, 100),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 100), new PhysicalCoordinate(800, 100),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(300, 300),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(400, 300), new PhysicalCoordinate(600, 300),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 300), new PhysicalCoordinate(800, 300),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(800, 800),
                LineType.cut);
            design.Add(line);

            design.Length = 800;
            design.Width = 600;
        }

        [Given(@"a compensated tiled fefco 201 with small values")]
        public void GivenACompensatedTiledFefco201WithSmallValues()
        {
            design = new RuleAppliedPhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(250, 0), new PhysicalCoordinate(250, 800),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 800),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 800),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(450, 0), new PhysicalCoordinate(450, 800),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 800),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(750, 0), new PhysicalCoordinate(750, 800),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(300, 100),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(400, 100), new PhysicalCoordinate(600, 100),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 100), new PhysicalCoordinate(800, 100),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(300, 300),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(400, 300), new PhysicalCoordinate(600, 300),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 300), new PhysicalCoordinate(800, 300),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(800, 800),
                LineType.cut);
            design.Add(line);

            design.Length = 800;
            design.Width = 600;
        }

        [Given(@"a compensated fefco 201 with inches")]
        public void GivenACompensatedFefco201WithInches()
        {
            design = new RuleAppliedPhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(5.91, 0), new PhysicalCoordinate(5.91, 19.67),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(21.65, 0), new PhysicalCoordinate(21.65, 19.67),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(27.56, 0), new PhysicalCoordinate(27.56, 19.67),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(5.91, 1.97), new PhysicalCoordinate(11.82, 1.97),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(27.56, 1.97), new PhysicalCoordinate(37.4, 1.97),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(5.91, 17.72), new PhysicalCoordinate(11.82, 17.72),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(27.56, 17.72), new PhysicalCoordinate(37.4, 17.72),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 19.67), new PhysicalCoordinate(31.49, 19.67),
                LineType.cut);
            design.Add(line);

            design.Length = 3.94;
        }

        [Given(@"a corrugate")]
        public void GivenACorrugate()
        {
            corrugateWidth = 770;
            //this.corrugateWidth.SetCorrugateThickness(3);
        }

        [Given(@"a corrugate with inches")]
        public void GivenACorrugateWithInches()
        {
            corrugateWidth = 30.31;
            //this.corrugateWidth.SetCorrugateThickness(0.12);
        }

        [Given(@"a machine")]
        public void GivenAMachine()
        {
            machineSettings = new FusionPhysicalMachineSettings { FeedRollerParameters = { OutFeedLength = 35 } };

            var longHeadParameters = machineSettings.LongHeadParameters;
            longHeadParameters.MinimumPosition = 0;
            longHeadParameters.MaximumPosition = 1300;
            longHeadParameters.LongheadWidth = 64;
            
            var longHead = new FusionLongHead {
                Number = 1,
                LeftSideToTool = 57.5,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 2,
                LeftSideToTool = 40.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 3,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 4,
                LeftSideToTool = 7,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            var crossHeadParameters = machineSettings.CrossHeadParameters;
            crossHeadParameters.Width = 40;
            crossHeadParameters.LeftSideToTool = 20;
            crossHeadParameters.CreaseWheelOffset = 10;
            crossHeadParameters.MaximumAcceleration = 10000;
            crossHeadParameters.MaximumDeceleration = 10000;
            crossHeadParameters.MaximumSpeed = 1800;
            crossHeadParameters.Speed = 100;
            crossHeadParameters.Acceleration = 100;
            crossHeadParameters.KnifeActivationDelay = 27;
            crossHeadParameters.KnifeDeactivationDelay = 23;

            var trackParameters = machineSettings.TrackParameters;
            var track = new Track {
                Number = 1,
                RightSideFixed = true
            };
            trackParameters.AddTrack(track);

            track = new Track {
                Number = 2,
                RightSideFixed = false
            };
            trackParameters.AddTrack(track);

            machine = new FusionMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
                {
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackOffset = 800,
                        TrackNumber = 1
                    },
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackOffset = 900,
                        TrackNumber = 2
                    }
                }
            };
        }

        [Given(@"a machine with seven longheads")]
        public void GivenAMachineWithSevenLongheads()
        {
            GivenAMachine();
            var longHeadParameters = machineSettings.LongHeadParameters;
            
            var longHead = new FusionLongHead {
                Number = 5,
                LeftSideToTool = 40.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 6,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 7,
                LeftSideToTool = 7,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);
        }

        [Given(@"a machine using inches")]
        public void GivenAMachineUsingInches()
        {
            machineSettings = FusionPhysicalMachineSettings.CreateFromFile(
                Networking.FindIpEndPoint("127.0.0.1", 999, true),
                "./machine_inches.xml");

            var trackParameters = machineSettings.TrackParameters;
            var track = trackParameters.Tracks.Single(t => t.Number == 1);
            track.RightSideFixed = true;

            track = trackParameters.Tracks.Single(t => t.Number == 2);
            track.RightSideFixed = false;

            machine = new FusionMachine()
            {
                PhysicalMachineSettings = machineSettings,
                Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
                {
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackOffset = 31.5,
                        TrackNumber = 1
                    },
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackOffset = 33.5,
                        TrackNumber = 2
                    }
                }
            };
        }

        [Given(@"then a design that is not producable on the machine is selected")]
        public void GivenThenADesignThatIsNotProducableOnTheMachineIsSelected()
        {
            var line = new PhysicalLine(new PhysicalCoordinate(702, 0), new PhysicalCoordinate(702, 800),
                LineType.cut);
            design.Add(line);
        }

        [Given(@"code has been generated once")]
        public void GivenCodeHasBeenGeneratedOnce()
        {
            WhenIGenerateCode();
        }

        [When(@"I generate code again")]
        public void WhenIGenerateCodeAgain()
        {
            try
            {
                instructionList = generator.Generate(design, machineSettings, corrugateWidth, machine.Tracks.Single(t => t.TrackNumber == 1), false);
            }
            catch (LongHeadDistributionException)
            {
                lhDistributionExceptionThrown = true;
            }
        }

        [When(@"I generate code")]
        public void WhenIGenerateCode()
        {
            generator = new FusionCodeGenerator(100, new MicroMeter[] { 100, 200, 300, 400 }, logger.Object);
            instructionList = generator.Generate(design, machineSettings, corrugateWidth, machine.Tracks.Single(t => t.TrackNumber == 1), false);
        }

        [When(@"I generate code with seven longheads")]
        public void WhenIGenerateCodeWithSevenLongHeads()
        {
            generator = new FusionCodeGenerator(100, new MicroMeter[] { 100, 200, 300, 400, 500, 600, 700 }, logger.Object);
            instructionList = generator.Generate(design, machineSettings, corrugateWidth, machine.Tracks.Single(t => t.TrackNumber == 1), false);
        }

        [When(@"I generate code with inches")]
        public void WhenIGenerateCodeWithInches()
        {
            generator = new FusionCodeGenerator(4.2, new MicroMeter[] { 3.93, 7.84, 15.74, 19.68, 23.62, 27.56 }, logger.Object);
            instructionList = generator.Generate(design, machineSettings, corrugateWidth,
                machine.Tracks.Single(t => t.TrackNumber == 1), false);
        }

        [When(@"I generate code with OnTheFly")]
        public void WhenIGenerateCodeWithOnTheFly()
        {
            generator = new FusionCodeGenerator((MicroMeter)100, new MicroMeter[] { 100, 200, 300, 400 }, logger.Object);
            instructionList = generator.Generate(design, machineSettings, corrugateWidth,
                machine.Tracks.Single(t => t.TrackNumber == 1), true);
        }

        [When(@"I generate code with synchronize label (.*)")]
        public void WhenIGenerateCodeWithSynchronizeLabelFalse(bool synchronizeLabel)
        {
            generator = new FusionCodeGenerator(100, new MicroMeter[] { 100, 200, 300, 400 }, logger.Object);
            instructionList = generator.Generate(design, machineSettings, corrugateWidth, machine.Tracks.Single(t => t.TrackNumber == 1), false);
        }

        [When(@"I validate longheadpositions with two tiles")]
        public void WhenIValidateLongheadpositionsWithTwoTiles()
        {
            generator = new FusionCodeGenerator(100, new MicroMeter[] { 100, 200, 300, 400, 500, 600, 700 }, logger.Object);
            longHeadValidationResult = generator.ValidateLongHeadPositions(design, machineSettings, corrugateWidth, machineSettings.TrackParameters.Tracks.Single(t => t.Number == 1).RightSideFixed, machine.Tracks.Single(t => t.TrackNumber == 1).TrackOffset);
        }

        [Then(@"validation return true")]
        public void ThenValidationReturnTrue()
        {            
            Specify.That(longHeadValidationResult).Should.BeTrue();            
        }

        [Then(@"validation return false")]
        public void ThenValidationReturnFalse()
        {
            Specify.That(longHeadValidationResult).Should.BeFalse();            
        }

        [Then(@"the first instruction in the list is meta data with synchronize label (.*)")]
        public void ThenTheFirstInstructionInTheListIsMetaDataWithSynchronizeLabelFalse(bool synchronizeLabel)
        {
            var metaData = instructionList.ElementAt(0) as InstructionMetaDataItem;

            Specify.That(metaData).Should.Not.BeNull();
            //Specify.That(metaData.SynchronizeWithLabel).Should.BeEqualTo(synchronizeLabel);
        }

        [Then(@"an instruction list is returned")]
        public void ThenAnInstructionListIsReturned()
        {
            Specify.That(instructionList).Should.Not.BeNull();
            Specify.That(instructionList.Count(i => i is InstructionCrossHeadOnTheFlyItem) == 0).Should.BeTrue();
        }

        [Then(@"an instruction list with inches is returned")]
        public void ThenAnInstructionListWithInchesIsReturned()
        {
            Specify.That(instructionList).Should.Not.BeNull();
            Specify.That(instructionList.Count(i => i is InstructionCrossHeadOnTheFlyItem) == 0).Should.BeTrue();
        }

        [Then(@"an instruction list with OnTheFly is returned")]
        public void ThenAnInstructionListWithOnTheFlyIsReturned()
        {
            Specify.That(instructionList).Should.Not.BeNull();
            Specify.That(instructionList.Count(i => i is InstructionCrossHeadOnTheFlyItem) > 0).Should.BeTrue();
        }

        [Then(@"the fist instruction in the list is meta data")]
        public void ThenTheFistInstructionInTheListIsMetaData()
        {
            var metaData = instructionList.ElementAt(0);

            Specify.That(metaData.GetType()).Should.BeEqualTo(typeof(InstructionMetaDataItem));

            var mm = 100;
            Specify.That((metaData as InstructionMetaDataItem).TrackNumber).Should.BeEqualTo(1);
            Specify.That((metaData as InstructionMetaDataItem).PackagingLength).Should.BeLogicallyEqualTo(mm);
        }

        [Then(@"the last instruction is an end marker")]
        public void ThenTheLastInstructionIsAnEndMarker()
        {
            var endMarker = instructionList.Last();

            Specify.That(endMarker.ToPlcArray()[0]).Should.BeEqualTo((short)32000);
        }

        [Then(@"the second instruction in the list is a long head position instruction")]
        public void ThenTheSecondInstructionInTheListIsALongHeadPositionInstruction()
        {
            var longHeadInstruction = instructionList.ElementAt(1) as InstructionLongHeadPositionItem;

            Specify.That(longHeadInstruction).Should.Not.BeNull();
            Specify.That(longHeadInstruction.LongHeadsToPosition.Count()).Should.BeEqualTo(4);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(0).LongHeadNumber).Should.BeEqualTo(1);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(0).Position).Should.BeLogicallyEqualTo(30 - 57.5);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(1).LongHeadNumber).Should.BeEqualTo(2);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(1).Position).Should.BeLogicallyEqualTo(150 - 40.5);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(2).LongHeadNumber).Should.BeEqualTo(4);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(2).Position).Should.BeLogicallyEqualTo(700 - 7);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(3).LongHeadNumber).Should.BeEqualTo(3);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(3).Position).Should.BeLogicallyEqualTo(550 - 7);
        }

        [Then(@"the second instruction in the list is a long head position instruction where seven longheads are positioned")]
        public void ThenTheSecondInstructionInTheListIsALongHeadPositionInstructionWhereSevenLongheadsArePositioned()
        {
            var longHeadInstruction = instructionList.ElementAt(1) as InstructionLongHeadPositionItem;

            Specify.That(longHeadInstruction).Should.Not.BeNull();
            Specify.That(longHeadInstruction.LongHeadsToPosition.Count()).Should.BeEqualTo(7);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(0).LongHeadNumber).Should.BeEqualTo(7);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(0).Position).Should.BeLogicallyEqualTo(800 - 7);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(1).LongHeadNumber).Should.BeEqualTo(6);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(1).Position).Should.BeLogicallyEqualTo(700 - 7);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(2).LongHeadNumber).Should.BeEqualTo(5);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(2).Position).Should.BeLogicallyEqualTo(600 - 40.5);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(3).LongHeadNumber).Should.BeEqualTo(4);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(3).Position).Should.BeLogicallyEqualTo(500 - 7);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(4).LongHeadNumber).Should.BeEqualTo(3);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(4).Position).Should.BeLogicallyEqualTo(400 - 7);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(5).LongHeadNumber).Should.BeEqualTo(2);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(5).Position).Should.BeLogicallyEqualTo(300 - 40.5);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(6).LongHeadNumber).Should.BeEqualTo(1);
            Specify.That(longHeadInstruction.LongHeadsToPosition.ElementAt(6).Position).Should.BeLogicallyEqualTo(200 - 57.5);
        }

        [Then(@"after long head positioning instructions comes crosshead and feedroller instructions")]
        public void ThenAfterLongHeadPositioningInstructionsComesCrossheadAndFeedrollerInstructions()
        {
            var crossheadInstructions = instructionList.Skip(2);
            foreach (var item in crossheadInstructions)
            {
                Console.WriteLine(item.ToString());
            }

            Specify.That(crossheadInstructions.Count()).Should.BeLogicallyEqualTo(19);
            Specify.That((crossheadInstructions.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((crossheadInstructions.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(50);
            Specify.That((crossheadInstructions.ElementAt(2) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);            
            Specify.That((crossheadInstructions.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((crossheadInstructions.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((crossheadInstructions.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(7) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            Specify.That((crossheadInstructions.ElementAt(8) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(400);
            Specify.That((crossheadInstructions.ElementAt(9) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((crossheadInstructions.ElementAt(10) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(11) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(550);
            Specify.That((crossheadInstructions.ElementAt(12) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(13) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);

            Specify.That((crossheadInstructions.ElementAt(15) as InstructionCrossHeadMovementItem).DisconnectTrack).Should.BeTrue();
            Specify.That((crossheadInstructions.ElementAt(16) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(17) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
        }

        [Then(@"after the long head positioning instruction there are the expected crosshead and feedroller instructions for the tiled design")]
        public void ThenAfterTheLongHeadPositioningInstructionThereAreTheExpectedCrossheadAndFeedrollerInstructionsForTheTiledDesign()
        {
            var crossheadInstructions = instructionList.Skip(2);

            Specify.That(crossheadInstructions.Count()).Should.BeLogicallyEqualTo(27);
            Specify.That((crossheadInstructions.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((crossheadInstructions.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(100);
            Specify.That((crossheadInstructions.ElementAt(2) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(700);
            Specify.That((crossheadInstructions.ElementAt(4) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(600);
            Specify.That((crossheadInstructions.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(7) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(400);
            Specify.That((crossheadInstructions.ElementAt(8) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(9) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(300);
            Specify.That((crossheadInstructions.ElementAt(10) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(11) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            Specify.That((crossheadInstructions.ElementAt(12) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(200);
            Specify.That((crossheadInstructions.ElementAt(13) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(300);
            Specify.That((crossheadInstructions.ElementAt(14) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(15) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(400);
            Specify.That((crossheadInstructions.ElementAt(16) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(17) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(600);
            Specify.That((crossheadInstructions.ElementAt(18) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(19) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(700);
            Specify.That((crossheadInstructions.ElementAt(20) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(21) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((crossheadInstructions.ElementAt(22) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(500);
            
            Specify.That((crossheadInstructions.ElementAt(23) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            
            Specify.That((crossheadInstructions.ElementAt(24) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(25) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
            Specify.That((crossheadInstructions.ElementAt(26) as InstructionEndMarkerItem)).Should.Not.BeNull();
        }

        
        [Then(@"a lhDistribution Exception Is Thrown")]
        public void ThenALhDistributionExceptionIsThrown()
        {
            Specify.That(lhDistributionExceptionThrown).Should.BeTrue();
            lhDistributionExceptionThrown = false;
        }


        [Then(@"the LH positions are not changed")]
        public void ThenTheLHPositionsAreNotChanged()
        {
            var lhPositions = generator.GetUpdatedLongHeadPositions();
            Specify.That(lhPositions).Should.BeLogicallyEqualTo(new MicroMeter[] { 30, 150, 550, 700 });
        }

        [Given(@"a design with only two crease lines")]
        public void GivenADesignWithOnlyTwoCreaseLines()
        {
            design = new RuleAppliedPhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(100, 150), new PhysicalCoordinate(500, 150),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(100, 550), new PhysicalCoordinate(500, 550),
                LineType.crease);
            design.Add(line);

            design.Length = 100;
        }

        [Then(@"the second instruction in the list for the crease design is a long head position instruction")]
        public void ThenTheSecondInstructionInTheListForTheCreaseDesignIsALongHeadPositionInstruction()
        {
            var longHeadInstruction = instructionList.ElementAt(1) as InstructionLongHeadPositionItem;

            Specify.That(longHeadInstruction).Should.Not.BeNull();
        }

        [Then(@"after long head positioning instructions there are expected crosshead and feedroller instruction for the crease design")]
        public void ThenAfterLongHeadPositioningInstructionsThereAreExpectedCrossHeadAndFeedrollerInstructionForTheCreaseDesign()
        {
            var crossheadInstructions = instructionList.Skip(2).ToList();
            Specify.That(crossheadInstructions.Count()).Should.BeLogicallyEqualTo(8);

            Specify.That((crossheadInstructions.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            Specify.That((crossheadInstructions.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((crossheadInstructions.ElementAt(2) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);
            Specify.That((crossheadInstructions.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((crossheadInstructions.ElementAt(4) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(400);
            Specify.That((crossheadInstructions.ElementAt(5) as InstructionCrossHeadMovementItem).Position)
                .Should.BeLogicallyEqualTo(20);

            Specify.That((crossheadInstructions.ElementAt(6) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
        }

        [Given(@"a design with only two cut lines")]
        public void GivenADesignWithOnlyTwoCutLines()
        {
            design = new RuleAppliedPhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(100, 150), new PhysicalCoordinate(500, 150),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(100, 550), new PhysicalCoordinate(500, 550),
                LineType.cut);
            design.Add(line);

            design.Length = 100;
        }

        [Then(@"the second instruction in the list for the cut design is a long head position instruction")]
        public void ThenTheSecondInstructionInTheListForTheCutDesignIsALongHeadPositionInstruction()
        {
            var longHeadInstruction = instructionList.ElementAt(1) as InstructionLongHeadPositionItem;

            Specify.That(longHeadInstruction).Should.Not.BeNull();
        }

        [Then(@"after long head positioning instructions there are expected crosshead and feedroller instruction for the cut design")]
        public void ThenAfterLongHeadPositioningInstructionsThereAreExpectedCrossHeadAndFeedrollerInstructionForTheCutDesign()
        {
            var crossheadInstructions = instructionList.Skip(2).ToList();
            Specify.That(crossheadInstructions.Count()).Should.BeLogicallyEqualTo(14);

            Specify.That((crossheadInstructions.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            Specify.That((crossheadInstructions.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((crossheadInstructions.ElementAt(2) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(100);
            Specify.That((crossheadInstructions.ElementAt(3) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(4) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);
            Specify.That((crossheadInstructions.ElementAt(5) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(6) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((crossheadInstructions.ElementAt(7) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(400);
            Specify.That((crossheadInstructions.ElementAt(8) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(500);
            Specify.That((crossheadInstructions.ElementAt(9) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(10) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            Specify.That((crossheadInstructions.ElementAt(11) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(12) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
        }

        [Given(@"a design with only two cut lines over the whole corrugate")]
        public void GivenADesignWithOnlyTwoCutLinesOverTheWholeCorrugate()
        {
            design = new RuleAppliedPhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(800, 150),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 550), new PhysicalCoordinate(800, 550),
                LineType.cut);
            design.Add(line);

            design.Length = 100;
        }

        [Then(@"the second instruction in the list for the cut over whole corrugate design is a long head position instruction")]
        public void ThenTheSecondInstructionInTheListForTheCutOverWholeCorrugateDesignIsALongHeadPositionInstruction()
        {
            var longHeadInstruction = instructionList.ElementAt(1) as InstructionLongHeadPositionItem;

            Specify.That(longHeadInstruction).Should.Not.BeNull();
        }

        [Then(@"after long head positioning instructions there are expected crosshead and feedroller instruction for the cut over whole corrugate design")]
        public void ThenAfterLongHeadPositioningInstructionsThereAreExpectedCrossHeadAndFeedrollerInstructionForTheCutOverWholeCorrugateDesign()
        {
            var crossheadInstructions = instructionList.Skip(2).ToList();
            Specify.That(crossheadInstructions.Count()).Should.BeLogicallyEqualTo(9);

            Specify.That((crossheadInstructions.ElementAt(0) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            Specify.That((crossheadInstructions.ElementAt(1) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(150);
            Specify.That((crossheadInstructions.ElementAt(2) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Cut);
            Specify.That((crossheadInstructions.ElementAt(3) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(810);
            Specify.That((crossheadInstructions.ElementAt(4) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(400);
            Specify.That((crossheadInstructions.ElementAt(5) as InstructionCrossHeadMovementItem).Position).Should.BeLogicallyEqualTo(20);
            Specify.That((crossheadInstructions.ElementAt(6) as InstructionCrossHeadToolActivation).Toolstate).Should.BeLogicallyEqualTo(ToolStates.Crease);
            Specify.That((crossheadInstructions.ElementAt(7) as InstructionFeedRollerItem).Position).Should.BeLogicallyEqualTo(35);
        }

        [Then(@"the updated long head positions are set as expected")]
        public void ThenTheUpdatedLongHeadPositionsAreSetAsExpected()
        {
            var updatedLongHeadPositions = generator.GetUpdatedLongHeadPositions();
            const int ExpectedPositionOfFirst = 800;
            Specify.That(updatedLongHeadPositions.ElementAt(0)).Should.BeLogicallyEqualTo(ExpectedPositionOfFirst);
            double expectedSecondPosition = ExpectedPositionOfFirst + GetOffsetFromPreviousLongHeadTool(2);
            Specify.That(updatedLongHeadPositions.ElementAt(1)).Should.BeLogicallyEqualTo(expectedSecondPosition);
            double expectedThirdPosition = expectedSecondPosition + GetOffsetFromPreviousLongHeadTool(3);
            Specify.That(updatedLongHeadPositions.ElementAt(2)).Should.BeLogicallyEqualTo(expectedThirdPosition);
            double distanceBetweenThirdAndFourthTool = GetOffsetFromPreviousLongHeadTool(4);
            double expectedFourthPosition = expectedThirdPosition + distanceBetweenThirdAndFourthTool;
            Specify.That(updatedLongHeadPositions.ElementAt(3)).Should.BeLogicallyEqualTo(expectedFourthPosition);
        }

        [Given(@"a codegenerator for machine using full on the fly (.*)")]
        public void GivenACodeGeneratorForMachineOfType(bool fullOtf)
        {
            if (fullOtf)
            {
                airCodeGenerator = new FusionCodeGenerator((MicroMeter)100, new MicroMeter[] { 100, 200, 300, 400 }, logger.Object);
            }
            else
            {
                electricCodeGenerator = new FusionCodeGenerator((MicroMeter)100, new MicroMeter[] { 100, 200, 300, 400 }, logger.Object);
            }
        }

        [When(@"I generate code on a machine using full on the fly (.*)")]
        public void WhenIGenerateCodeOnMachineOfTypeAir(bool fullOtf)
        {
            if (fullOtf)
            {
                airInstructionList = airCodeGenerator.Generate(
                    design, machineSettings, corrugateWidth, machine.Tracks.Single(t => t.TrackNumber == 1),
                    true);
            }
            else
            {
                electricInstructionList = electricCodeGenerator.Generate(
                    design, machineSettings, corrugateWidth, machine.Tracks.Single(t => t.TrackNumber == 1),
                    true);
            }
        }

        [Then(@"instruction list from a machine with full on the fly (.*) is shorter than instruction list for a machine with full on the fly (.*)")]
        public void ThenInstructionListFromMachineTypeAirIsShorterThanInstructionListForMachineOfTypeElectric(bool fotf1, bool fotf2)
        {
            Assert.IsTrue(airInstructionList.Count() < electricInstructionList.Count());
        }

        public MicroMeter GetOffsetFromPreviousLongHeadTool(int longHeadNumber)
        {
            var parameters = machineSettings.LongHeadParameters;
            var lh1 = parameters.LongHeads.Single(lh => lh.Number == longHeadNumber - 1);
            var lh2 = parameters.LongHeads.Single(lh => lh.Number == longHeadNumber);
            return parameters.LongheadWidth - lh1.LeftSideToTool + lh2.LeftSideToTool;
        }
    }
}
