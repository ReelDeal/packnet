﻿@PackagingPanelCreator
Feature: Packaging Panel Creator
	In order to test the crosshead and roller instructions
	As a developer
	I want the crosshead and roller instructions to be performed in a step of their own

Scenario: Packaging Panel Creator should generate instructions for crosshead and roller
	Given a design for track 1 with an uneven amount of horizontal lines
	And a design for track 1 with an even amount of horizontal lines
	And a design for track 2 with an uneven amount of horizontal lines
	And corrugate
	And a machine with crosshead and tracks
	When I generate code with PackagePanelCreator for all designs
	Then an instruction list for the first design is returned
	And an instruction list for the second design is returned
	And an instruction list for the third design is returned

Scenario: Packaging Panel Creator should not generate reverses when horizontal line cooridantes are unsorted
	Given a design for track 1 with horizontal lines added in an unsorted way
	And corrugate
	And a machine with crosshead and tracks
	When I generate code with PackagePanelCreator for design on track 1
	Then an instruction list without reverses is returned

Scenario: Packaging Panel Creator should fail if crosshead movement would put it outside minimum position
	Given a machine with crosshead and tracks
	And a design for track 1 with an uneven amount of horizontal lines
	And the track offset is to small 
	And corrugate
	When I generate code with PackagePanelCreator for design on track 1
	Then an exception is thrown

Scenario: Packaging Panel Creator should fail if crosshead movement would put it outside maximum position
	Given a machine with crosshead and tracks
	And the track offset is to large 
	And corrugate
	And a design for track 2 with an uneven amount of horizontal lines
	When I generate code with PackagePanelCreator for design on track 2
	Then an exception is thrown

Scenario: Packaging Panel Creator should always move crosshead outside the edge of the corrugate
	Given a machine with crosshead and tracks
	And a design for track 1 with an uneven amount of horizontal lines
	And corrugate
	When I generate code with PackagePanelCreator for design on track 1
	Then the final crosshead movement is outside of the corrugate

