﻿using System;
using System.Linq;

using Testing.Specificity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Business.CodeGenerator;
using PackNet.Common.Interfaces.DTO;

namespace BusinessTests.CodeGenerator
{
    [TestClass]
    [DeploymentItem("..\\..\\CodeGenerator\\AStarEmTests\\TestData", "CodeGenTestData")]
    public class SectionCreatorTests
    {
        private static double lhOffset = 147d;
        [TestMethod]
        public void ShouldCreateSectionsFromVerticalLines()
        {
            var design = GetDesignWithVerticalLines(5);

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);

            Specify.That(sections.Count()).Should.BeEqualTo(5);
        }

        [TestMethod]
        public void ShouldMergeThreeSections()
        {
            var design = GetDesignWithVerticalLines(3);

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections).Should.Not.BeNull();
            Specify.That(mergedSections.Count()).Should.BeLogicallyEqualTo(4);

        }

        [TestMethod]
        public void ShouldMergeFourSections()
        {
            var design = GetDesignWithVerticalLines(4);

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections).Should.Not.BeNull();

            Specify.That(mergedSections.Count()).Should.BeLogicallyEqualTo(8);

        }

        [TestMethod]
        public void ShouldMergeFourSections_LinesShorterThanYOffset()
        {
            var design = GetDesignWithVerticalLines(4, 50d);

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections).Should.Not.BeNull();

            Specify.That(mergedSections.Count()).Should.BeLogicallyEqualTo(2);

        }

        [TestMethod]
        public void ShouldMergeFiveSections()
        {
            var design = GetDesignWithVerticalLines(5);

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections).Should.Not.BeNull();

            Specify.That(mergedSections.Count()).Should.BeLogicallyEqualTo(16);
        }

        [TestMethod]
        public void ShouldNotMergeLastSection()
        {
            var design = GetDesignWithVerticalLines(5, 50d);

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections).Should.Not.BeNull("We should be able to get 4 merge-combinations from this design.");
            Specify.That(mergedSections.Count()).Should.BeLogicallyEqualTo(4, "We should be able to get 4 merge-combinations from this design. (1+2+3)+4, (1+2)+3+4, 1+(2+3)+4 and 1+2+3+4");
            Specify.That(mergedSections.All(s => s.Sections.Count() >= 2)).Should.BeTrue("We expect to have at least 2 sections for all designs. One for the Y-offset and then the rest.");
            Specify.That(mergedSections.All(s => s.Sections.Last().Length == lhOffset)).Should.BeTrue("The last section should be y-offset long in all merge-combinations");

        }

        [TestMethod]
        public void Should_Return_OrderedBy_NumberOfSections_Ascending()
        {
            var design = GetDesignWithVerticalLines(4);

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);


            Specify.That(mergedSections.Count()).Should.BeEqualTo(8);

            Specify.That(mergedSections.First().Sections.Count()).Should.BeEqualTo(1);
            Specify.That(mergedSections.ElementAt(1).Sections.Count()).Should.BeEqualTo(2);
            Specify.That(mergedSections.ElementAt(2).Sections.Count()).Should.BeEqualTo(2);
            Specify.That(mergedSections.ElementAt(3).Sections.Count()).Should.BeEqualTo(2);
            Specify.That(mergedSections.ElementAt(4).Sections.Count()).Should.BeEqualTo(3);
            Specify.That(mergedSections.ElementAt(5).Sections.Count()).Should.BeEqualTo(3);
            Specify.That(mergedSections.ElementAt(6).Sections.Count()).Should.BeEqualTo(3);
            Specify.That(mergedSections.ElementAt(7).Sections.Count()).Should.BeEqualTo(4);
        }

        [TestMethod]
        public void ShouldMergeSixSections()
        {
            const int lineNumber = 6;
            var design = GetDesignWithVerticalLines(lineNumber);
            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections).Should.Not.BeNull();

            Specify.That(mergedSections.Count()).Should.BeLogicallyEqualTo(32);
        }

        [TestMethod]
        public void ShouldMergeRandomSections()
        {
            var random = new Random();
            var lineNumber = random.Next(7, 15);
            var design = GetDesignWithVerticalLines(lineNumber, 50d);
            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections).Should.Not.BeNull();

            Specify.That(mergedSections.Count() <= 512).Should.BeTrue(); ; // The number of sections have been reduced to 10 so 512 is currently the max ammount of merged sections we can get.
        }

        [TestMethod]
        public void ShouldOnlyGetUpTo10Sections_FromDesign()
        {
            var creator = new SectionCreator(lhOffset);
            var design = GetDesignWithVerticalLines(20);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);
            Specify.That(mergedSections).Should.Not.BeNull();
            Specify.That(mergedSections.Count()).Should.BeEqualTo(512);
        }

        [TestMethod]
        public void ShouldAssignWasteAreasToCorrectSection()
        {
            var design = GetDesignWithLinesAndWasteAreas();

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);

            Specify.That(sections.Count()).Should.BeEqualTo(3);
            Specify.That(sections.ElementAt(0).WasteAreas.Count()).Should.BeEqualTo(2);
            Specify.That(sections.ElementAt(0).WasteAreas.Count(w => w.TopLeft.X == 0 && w.BottomRight.X == 100)).Should.BeEqualTo(1);
            Specify.That(sections.ElementAt(0).WasteAreas.Count(w => w.TopLeft.X == 200 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(sections.ElementAt(1).WasteAreas.Count()).Should.BeEqualTo(1);
            Specify.That(sections.ElementAt(1).WasteAreas.Count(w => w.TopLeft.X == 200 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(sections.ElementAt(2).WasteAreas.Count()).Should.BeEqualTo(2);
            Specify.That(sections.ElementAt(2).WasteAreas.Count(w => w.TopLeft.X == 200 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(sections.ElementAt(2).WasteAreas.Count(w => w.TopLeft.X == 300 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
        }

        [TestMethod]
        public void ShouldAssignWasteAreasToCorrectSection_WhenMerging()
        {
            var design = GetDesignWithLinesAndWasteAreas();

            var creator = new SectionCreator(lhOffset);
            var sections = creator.GetSectionsFromDesign(design);
            var mergedSections = creator.MergeSections(sections);

            Specify.That(mergedSections.Count()).Should.BeEqualTo(MergedSectionAmount(sections.Count()-1));
            var leastSections = mergedSections.Single(s => s.Sections.Count() == 2).Sections.ElementAt(0);
            Specify.That(leastSections.WasteAreas.Count()).Should.BeEqualTo(2);
            Specify.That(leastSections.WasteAreas.Count(w => w.TopLeft.X == 0 && w.BottomRight.X == 100)).Should.BeEqualTo(1);
            Specify.That(leastSections.WasteAreas.Count(w => w.TopLeft.Y == 0 && w.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(leastSections.WasteAreas.Count(w => w.TopLeft.X == 200 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(leastSections.WasteAreas.Count(w => w.TopLeft.Y == 0 && w.BottomRight.Y == 100)).Should.BeEqualTo(1);

            var noneMerged = mergedSections.Single(sp => sp.Sections.Count() == 3);
            Specify.That(noneMerged.Sections.ElementAt(0).WasteAreas.Count()).Should.BeEqualTo(2);
            Specify.That(noneMerged.Sections.ElementAt(0).WasteAreas.Count(w => w.TopLeft.X == 0 && w.BottomRight.X == 100)).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(0).WasteAreas.Count(w => w.TopLeft.Y == 0 && w.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(0).WasteAreas.Count(w => w.TopLeft.X == 200 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(0).WasteAreas.Count(w => w.TopLeft.Y == 0 && w.BottomRight.Y == 50)).Should.BeEqualTo(1);

            Specify.That(noneMerged.Sections.ElementAt(1).WasteAreas.Count()).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(1).WasteAreas.Count(w => w.TopLeft.X == 200 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(1).WasteAreas.Count(w => w.TopLeft.Y == 0 && w.BottomRight.Y == 100)).Should.BeEqualTo(1);

            Specify.That(noneMerged.Sections.ElementAt(2).WasteAreas.Count()).Should.BeEqualTo(2);
            Specify.That(noneMerged.Sections.ElementAt(2).WasteAreas.Count(w => w.TopLeft.X == 200 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(2).WasteAreas.Count(w => w.TopLeft.Y == 0 && w.BottomRight.Y == 100)).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(2).WasteAreas.Count(w => w.TopLeft.X == 300 && w.BottomRight.X == 400)).Should.BeEqualTo(1);
            Specify.That(noneMerged.Sections.ElementAt(2).WasteAreas.Count(w => w.TopLeft.Y == 100 && w.BottomRight.Y == 200)).Should.BeEqualTo(1);
        }

        private RuleAppliedPhysicalDesign GetDesignWithLinesAndWasteAreas()
        {
            /*          100      200    300    400
             *            ________              
             * 50   _____|        |             |
             * 100 |              |______       |
             *     |                     |      |
             * 200 |_____________________|      |
             */
            var design = new RuleAppliedPhysicalDesign { Length = 200, Width = 300 };
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(200, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(100, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(300, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(300, 200), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(0, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 100), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(300, 100), new PhysicalCoordinate(300, 200), LineType.cut));

            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(100, 50)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(400, 100)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(300, 100), new PhysicalCoordinate(400, 200)));

            return design;
        }

        private RuleAppliedPhysicalDesign GetDesignWithVerticalLines(int lineNumber, double lineLength = 150d)
        {
            var design = new RuleAppliedPhysicalDesign();

            for (int i = 0; i < lineNumber; i++)
            {
                MicroMeter xCoordinate = i % 2 == 0 ? 100d : 140d;
                MicroMeter yStartCoordinate = lineLength * i;
                design.Add(new PhysicalLine(new PhysicalCoordinate(xCoordinate, yStartCoordinate), new PhysicalCoordinate(xCoordinate, yStartCoordinate + lineLength), LineType.cut));
            }

            return design;
        }

        private int MergedSectionAmount(int sectionnumber)
        {
            return (int)Math.Pow(2, sectionnumber - 1);
        }
    }
}