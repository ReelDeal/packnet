namespace BusinessTests.CodeGenerator
{
    public class PositionTestDataContainer
    {
        public double Pos { get; set; }

        public int Lh { get; set; }
    }
}