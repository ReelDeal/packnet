﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace BusinessTests.CodeGenerator
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.CodeGenerator;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.CodeGenerator.LongHeadPositioner;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;

    using TechTalk.SpecFlow;
    using TechTalk.SpecFlow.Assist;

    using Testing.Specificity;

    [Binding]
    [Scope(Tag = "LongHeadPositioner")]
    public class LongHeadPositionerSteps
    {
        private const double CrossHeadPosition = 0.0;
        private List<PhysicalLine> verticalLines;
        private ILongHeadPositioner positioner;
        private FusionLongHeadParameters longHeadParameters;
        private InstructionLongHeadPositionItem pos;
        private FusionTracksParameters fusionTrackParameters;
        private MicroMeter corrugateOffsetTrack1;
        private MicroMeter corrugateOffsetTrack2;
        private MicroMeter corrugateWidth;
        private bool exceptionWasThrown;
        private MicroMeter[] positions;
        private List<PhysicalLine> secondVerticalLines;
        private InstructionLongHeadPositionItem posForSecond;
        private IEnumerable<MicroMeter> positionsAfterFirst;
        private IEnumerable<MicroMeter> positionsAfterSecond;
        private bool isPositioningValid;

        [Given(@"a collection of vertical lines")]
        public void GivenACollectionOfVerticalLines()
        {
            verticalLines = new List<PhysicalLine>();
        }

        [Given(@"a line of type (.*) at (.*)")]
        public void GivenALineIsOfTypeCreaseAt(LineType type, double coordinate)
        {
            var line = new PhysicalLine(new PhysicalCoordinate(coordinate, 0), new PhysicalCoordinate(coordinate, 500), type);
            verticalLines.Add(line);
        }

        [Given(@"lines at")]
        public void VerticalLinesAt(Table table)
        {
            var lineTestContainers = table.CreateSet<PhysicalLineTestContainer>().OrderBy(x => x.Position);
            foreach (var lineData in lineTestContainers)
            {
                var line = new PhysicalLine(new PhysicalCoordinate(lineData.Position, 0), new PhysicalCoordinate(lineData.Position, 500), lineData.Type);
                verticalLines.Add(line);
            }
        }

        //[Given(@"a number of long heads")]
        //public void GivenANumberOfLongHeads(Table table)
        //{
        //    var longHeadFactory = new LongHeadFactory();
        //    var longHeadTestContainers = table.CreateSet<LongHeadTestContainer>().OrderBy(x => x.Lh);
        //    foreach (var longHeadData in longHeadTestContainers)
        //    {
        //        var longHead = longHeadFactory.Create();
        //        longHead.SetLongHeadNumber(longHeadData.Lh);
        //        longHead.SetLeftSideToToolOffset(longHeadData.LeftSideToToolOffset);
        //        longHead.SetToolType(longHeadData.Type);
        //        this.longHeadParameters.AddLongHead(longHead);
        //    }

        //    this.positions = longHeadTestContainers.Select(x => (MicroMeter)x.Position).ToArray();
        //}


        [Given(@"a collection of vertical lines that starts with a cut line")]
        public void GivenACollectionOfVerticalLinesThatStartsWithACutLine()
        {
            verticalLines = new List<PhysicalLine>();
            var line = new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 500), LineType.cut);
            verticalLines.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 500), LineType.crease);
            verticalLines.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(700, 0), new PhysicalCoordinate(700, 500), LineType.crease);
            verticalLines.Add(line);
        }

        [Given(@"a collection of vertical lines that starts with a cut line on track 2")]
        public void GivenACollectionOfVerticalLinesThatStartsWithACutLineOnTrack2()
        {
            verticalLines = new List<PhysicalLine>();

            var line = new PhysicalLine(new PhysicalCoordinate(860, 0), new PhysicalCoordinate(860, 500), LineType.crease);
            verticalLines.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(1000, 0), new PhysicalCoordinate(1000, 500), LineType.crease);
            verticalLines.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(1200, 0), new PhysicalCoordinate(1200, 500), LineType.cut);
            verticalLines.Add(line);
        }

        [Given(@"long head (.*) is positioned at (.*)")]
        public void GivenLongHeadIsPositionedAt(int longHeadNumber, double coordinate)
        {
            positions[longHeadNumber - 1] = coordinate;
        }

        [Given(@"a long head positioner")]
        public void GivenALongHeadPositioner()
        {
            positioner = new LongHeadPositioner(positions);
        }

        [Given(@"track parameters with a offset of (.*)")]
        public void GivenTrackParametersWithAOffsetOf(double coordinate)
        {
            fusionTrackParameters = new FusionTracksParameters {
                CorrugateWidthTolerance = 5
            };

            var track = new Track {
                Number = 1,
                RightSideFixed = true,
            };
            corrugateOffsetTrack1 = coordinate;
            fusionTrackParameters.AddTrack(track);

            track = new Track {
                Number = 2,
                RightSideFixed = false,
            };
            corrugateOffsetTrack2 = 850;
            fusionTrackParameters.AddTrack(track);
        }

        [Given(@"track parameters with a offset of (.*) for the left track and (.*) for the right track")]
        public void GivenTrackParametersWithAOffsetOf(double leftTrackOffset, double rightTrackOffset)
        {
            fusionTrackParameters = new FusionTracksParameters {
                CorrugateWidthTolerance = 5
            };
            
            var track = new Track {
                Number = 1,
                RightSideFixed = true
            };
            corrugateOffsetTrack1 = leftTrackOffset;
            fusionTrackParameters.AddTrack(track);

            track = new Track {
                Number = 2,
                RightSideFixed = false
            };
            corrugateOffsetTrack2 = rightTrackOffset;
            fusionTrackParameters.AddTrack(track);
        }

        [Given(@"the corrugate width tolerance is (.*)")]
        public void GivenTheCorrugateWidthToleranceIs(double coordinate)
        {
            fusionTrackParameters.CorrugateWidthTolerance = coordinate;
        }

        [Given(@"track parameters with a offset of (.*) with right side fixed (.*)")]
        public void GivenTrackParametersWithAOffsetOf(double coordinate, bool isRightSideFixed)
        {
            fusionTrackParameters = new FusionTracksParameters();

            var track = new Track {
                Number = 1,
                RightSideFixed = isRightSideFixed
            };
            corrugateOffsetTrack1 = coordinate;
            fusionTrackParameters.AddTrack(track);
        }

        [Given(@"track parameters")]
        public void TrackParameters()
        {
            fusionTrackParameters = new FusionTracksParameters();
            

        }

        [Given(@"a track with a offset of (.*) with right side fixed (.*) and track id (.*) and corrugate width (.*)")]
        public void GivenTrackParametersWithAOffsetOf(double coordinate, bool isRightSideFixed, int trackId, double width)
        {
            var track = new Track { RightSideFixed = isRightSideFixed, Number = trackId };

            if (trackId == 1)
            {
                corrugateOffsetTrack1 = coordinate;
            }
            else
            {
                corrugateOffsetTrack2 = coordinate;
            }

            fusionTrackParameters.AddTrack(track);
        }

        [Given(@"a corrugate with a width of (.*)")]
        public void GivenACorrugateWithAWidthOf(double width)
        {
            corrugateWidth = width;
            //this.corrugateWidth.SetCorrugateThickness(3);
        }

        [Given(@"long head parameters with four long heads with type; cut, crease, crease, cut")]
        public void GivenLongHeadParametersWithFourLongHeadsWithTypeCutCreaseCreaseCut()
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = 1300,
                LongheadWidth = 64
            };

            var longHead = new FusionLongHead {
                Number = 1,
                LeftSideToTool = 57.5,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 2,
                LeftSideToTool = 40.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 3,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 4,
                LeftSideToTool = 7,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            positions = new MicroMeter[] { 100, 200, 300, 400 };
        }


        [Given(@"long head parameters with seven longheads")]
        public void GivenLongHeadParametersWithSevenLongheads()
        {
            GivenLongHeadParametersWithFourLongHeadsWithTypeCutCreaseCreaseCut();

            var longHead = new FusionLongHead {
                Number = 5,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 6,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 7,
                LeftSideToTool = 7,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            positions = new MicroMeter[] { 100, 200, 300, 400, 500, 600, 700 };
        }

        [Given(@"long head parameters with four long heads with type; crease, crease, crease, crease")]
        public void GivenLongHeadParametersWithFourLongHeadsWithTypeCreaseCreaseCreaseCrease()
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = 1300,
                LongheadWidth = 64
            };

            var longHead = new FusionLongHead {
                Number = 1,
                LeftSideToTool = 57.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 2,
                LeftSideToTool = 57.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 3,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 4,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            positions = new MicroMeter[] { 100, 200, 300, 400 };
        }

        [Given(@"long head parameters with four long heads with type; crease, crease, crease, cut")]
        public void GivenLongHeadParametersWithFourLongHeadsWithTypeCreaseCreaseCreaseCut()
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = 1300,
                LongheadWidth = 64
            };

            var longHead = new FusionLongHead {
                Number = 1,
                LeftSideToTool = 57.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 2,
                LeftSideToTool = 57.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 3,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 4,
                LeftSideToTool = 7,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            positions = new MicroMeter[] { 201, 401, 601, 801 };
        }

        [Given(@"long head parameters with six longheads")]
        public void GivenLongHeadParametersWithSixLongheads()
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = 1300,
                LongheadWidth = 64
            };

            var longHead = new FusionLongHead {
                Number = 1,
                LeftSideToTool = 57.5,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 2,
                LeftSideToTool = 40.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 3,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 4,
                LeftSideToTool = 40.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 5,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 6,
                LeftSideToTool = 7,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            positions = new MicroMeter[] { 100, 200, 300, 400, 500, 600 };
        }

        [Given(@"a number of long heads")]
        public void GivenANumberOfLongHeads(Table table)
        {
            var longHeadTestContainers = table.CreateSet<LongHeadTestContainer>().OrderBy(x => x.Lh);
            foreach (var longHeadData in longHeadTestContainers)
            {
                var longHead = new FusionLongHead();
                longHead.Number = longHeadData.Lh;
                longHead.LeftSideToTool = longHeadData.LeftSideToToolOffset;
                longHead.Type = longHeadData.Type;
                longHeadParameters.AddLongHead(longHead);
            }

            positions = longHeadTestContainers.Select(x => (MicroMeter)x.Position).ToArray();
        }

        [Given(@"long head parameters")]
        public void GivenLongHeadParameters()
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = 1300,
                LeftSideToSensorPin = 48,
                LongheadWidth = 64
            };
        }

        [Given(@"long head millimeter parameters with a maximumToolPosition of (.*)")]
        public void GivenLongHeadMillimeterParameters(double maximumToolPosition)
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = maximumToolPosition,
                LeftSideToSensorPin = 48,
                LongheadWidth = 64
            };
        }

        [Given(@"long head inch parameters with a maximumToolPosition of (.*)")]
        public void GivenLongHeadInchParameters(double maximumToolPosition)
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = maximumToolPosition,
                LeftSideToSensorPin = 1.890,
                LongheadWidth = 2.520
            };
        }

        [Given(@"long head inch parameters with a minimumToolPosition of (.*)")]
        public void GivenLongHeadInchParametersWithMinimumPosition(double minimumToolPosition)
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = minimumToolPosition,
                MaximumPosition = 51,
                LeftSideToSensorPin = 1.890,
                LongheadWidth = 2.520
            };
        }

        [Given(@"long head parameters with four long heads with type; crease, cut, crease, cut")]
        public void GivenLongHeadParametersWithFourLongHeadsWithTypeCreaseCutCreaseCut()
        {
            longHeadParameters = new FusionLongHeadParameters {
                MinimumPosition = 0,
                MaximumPosition = 1300,
                LongheadWidth = 64
            };

            var longHead = new FusionLongHead {
                Number = 1,
                LeftSideToTool = 57.5,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 2,
                LeftSideToTool = 57.5,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 3,
                LeftSideToTool = 7,
                Type = ToolType.Crease
            };
            longHeadParameters.AddLongHead(longHead);

            longHead = new FusionLongHead {
                Number = 4,
                LeftSideToTool = 7,
                Type = ToolType.Cut
            };
            longHeadParameters.AddLongHead(longHead);

            positions = new MicroMeter[] { 100, 200, 300, 400 };
        }

        [When(@"I position the long heads")]
        public void WhenIPositionTheLongHeads()
        {
            try
            {
                pos = positioner.DistributeLongHeads(verticalLines, longHeadParameters, fusionTrackParameters.Tracks.Single(t => t.Number == 1).RightSideFixed, corrugateOffsetTrack1, corrugateWidth, CrossHeadPosition, fusionTrackParameters.CorrugateWidthTolerance);
            }
            catch (LongHeadDistributionException)
            {
                exceptionWasThrown = true;
            }
            catch (LinesOutsideCorrugateException)
            {
                exceptionWasThrown = true;
            }

            positionsAfterFirst = positioner.GetLongheadPositions();
        }

        [When(@"I position the long heads on track (.*)")]
        public void WhenIPositionTheLongHeads(int trackid)
        {
            try
            {
                pos = positioner.DistributeLongHeads(verticalLines, longHeadParameters, fusionTrackParameters.Tracks.Single(t => t.Number == trackid).RightSideFixed, trackid == 1 ? corrugateOffsetTrack1 : corrugateOffsetTrack2, corrugateWidth, CrossHeadPosition, fusionTrackParameters.CorrugateWidthTolerance);
            }
            catch (LongHeadDistributionException)
            {
                exceptionWasThrown = true;
            }
            catch (LinesOutsideCorrugateException)
            {
                exceptionWasThrown = true;
            }

            positionsAfterFirst = positioner.GetLongheadPositions();
        }

        [When(@"I position the long heads again")]
        public void WhenIPositionTheLongHeadsAgain()
        {
            try
            {
                pos = positioner.DistributeLongHeads(verticalLines, longHeadParameters, fusionTrackParameters.Tracks.Single(t => t.Number == 1).RightSideFixed, corrugateOffsetTrack1, corrugateWidth, CrossHeadPosition, fusionTrackParameters.CorrugateWidthTolerance);
            }
            catch (LongHeadDistributionException)
            {
                exceptionWasThrown = true;
            }

            positionsAfterFirst = positioner.GetLongheadPositions();
        }

        [When(@"I check if long heads can be distributed")]
        public void WhenICheckIfLongHeadsCanBeDistributed()
        {
            isPositioningValid = positioner.CanDistributeLongHeadsFor(verticalLines, longHeadParameters, fusionTrackParameters.Tracks.Single(t => t.Number == 1).RightSideFixed, corrugateOffsetTrack1, corrugateWidth, fusionTrackParameters.CorrugateWidthTolerance);
            positionsAfterFirst = positioner.GetLongheadPositions();
        }

        [When(@"I check if long heads can be distributed on track (.*)")]
        public void WhenICheckIfLongHeadsCanBeDistributed(int trackId)
        {
            isPositioningValid = positioner.CanDistributeLongHeadsFor(verticalLines, longHeadParameters, fusionTrackParameters.Tracks.Single(t => t.Number == trackId).RightSideFixed, trackId == 1 ? corrugateOffsetTrack1 : corrugateOffsetTrack2, corrugateWidth, fusionTrackParameters.CorrugateWidthTolerance);
            positionsAfterFirst = positioner.GetLongheadPositions();
        }

        [Then(@"an exception was thrown")]
        public void ThenAnExceptionWasThrown()
        {
            Specify.That(exceptionWasThrown).Should.BeTrue();
        }

        [Then(@"positioning check returns (.*)")]
        public void ThenPositioningCheckReturnsTrue(bool returns)
        {
            if (returns)
            {
                Specify.That(isPositioningValid).Should.BeTrue();
            }
            else 
            {
                Specify.That(isPositioningValid).Should.BeFalse();
            }
        }

        [Then(@"long head (.*) is positioned outside the corrugate on left side as move(.*)")]
        public void ThenLongHeadIsPositionedOutsideTheCorrugateOnLeftSide(int longHeadNumber, int movementOrder)
        {
            MicroMeter corrugateLeftSide = corrugateOffsetTrack1 - corrugateWidth;
            var movement = pos.LongHeadsToPosition.ElementAt(movementOrder - 1);
            Specify.That(movement.LongHeadNumber).Should.BeLogicallyEqualTo(longHeadNumber);
            var toolPosition = ConvertSensorPositionToToolPosition(longHeadNumber, movement.Position);
            Specify.That(toolPosition <= corrugateLeftSide).Should.BeTrue();
        }

        [Then(@"long head (.*) is positioned outside the corrugate on right side as move(.*)")]
        public void ThenLongHeadIsPositionedOutsideTheCorrugateOnRightSide(int longHeadNumber, int movementOrder)
        {
            MicroMeter corrugateRightSide = corrugateOffsetTrack1;
            var movement = pos.LongHeadsToPosition.ElementAt(movementOrder - 1);
            Specify.That(movement.LongHeadNumber).Should.BeLogicallyEqualTo(longHeadNumber);
            var toolPosition = ConvertSensorPositionToToolPosition(longHeadNumber, movement.Position);
            Specify.That(toolPosition >= corrugateRightSide).Should.BeTrue();
        }

        [Then(@"long head (.*) is positioned outside the corrugate at position (.*) on right side as move(.*)")]
        public void ThenLongHeadIsPositionedOutsideTheCorrugateAtPositionOnRightSide(int longHeadNumber, double position, int movementOrder)
        {
            MicroMeter corrugateRightSide = corrugateOffsetTrack1;
            var movement = pos.LongHeadsToPosition.ElementAt(movementOrder - 1);
            Specify.That(movement.LongHeadNumber).Should.BeLogicallyEqualTo(longHeadNumber);
            var toolPosition = ConvertSensorPositionToToolPosition(longHeadNumber, movement.Position);
            Specify.That(toolPosition >= corrugateRightSide).Should.BeTrue();
            Specify.That(toolPosition).Should.BeLogicallyEqualTo(position);
        }

        [Then(@"long head (.*) should be positioned to line (.*) as move (.*)")]
        public void ThenLongHeadShouldBePositionedToLineAsMove(int longHeadNumber, int lineNumber, int movementOrder)
        {
            var movement = pos.LongHeadsToPosition.ElementAt(movementOrder - 1);
            Specify.That(movement.LongHeadNumber).Should.BeLogicallyEqualTo(longHeadNumber);

            var lineCoordinate = verticalLines.ElementAt(lineNumber - 1).EndCoordinate.X;
            var toolPosition = ConvertSensorPositionToToolPosition(longHeadNumber, movement.Position);
            Specify.That(toolPosition).Should.BeLogicallyEqualTo(lineCoordinate);
        }

        public MicroMeter ConvertSensorPositionToToolPosition(int longHeadNumber, MicroMeter sensorPosition)
        {
            var leftSideToTool = longHeadParameters.LongHeads.Single(lh => lh.Number == longHeadNumber).LeftSideToTool;
            var leftSideToSensor = longHeadParameters.LeftSideToSensorPin;
            var toolPosition = sensorPosition - leftSideToSensor + leftSideToTool;
            return toolPosition;
        }

        [Then(@"(.*) long heads are positioned")]
        public void ThenOnlyXLongHeadsArePositioned(int numberOfLongHeadsPositioned)
        {
            Specify.That(pos.LongHeadsToPosition.Count()).Should.BeEqualTo(numberOfLongHeadsPositioned);
        }

        [Then(@"long head positions should be")]
        public void ThenLongHeadPositionsShouldBe(Table table)
        {
            var expectedPositions = table.CreateSet<LongHeadTestContainer>().OrderBy(x => x.Lh);
            var positonsFromPositioner = positionsAfterFirst;            

            positonsFromPositioner.ToList().ForEach(pos => Console.WriteLine(pos.ToString()));

            for (var i = 0; i < expectedPositions.Count(); i++)
            {
                Specify.That(positonsFromPositioner.ElementAt(i)).Should.BeLogicallyEqualTo(expectedPositions.ElementAt(i).Position);
            }
        }

        [Given(@"a second collection of vertical lines")]
        public void GivenASecondCollectionOfVerticalLines()
        {
            secondVerticalLines = new List<PhysicalLine>();
        }

        [Given(@"in second collection a line of type (.*) at (.*)")]
        public void GivenInSecondCollectionALineIsOfTypeCreaseAt(LineType type, double coordinate)
        {
            var line = new PhysicalLine(new PhysicalCoordinate(coordinate, 0), new PhysicalCoordinate(coordinate, 500), type);
            secondVerticalLines.Add(line);
        }

        [When(@"I position the long heads for second collection")]
        public void WhenIPositionTheLongHeadsForSecondCollection()
        {
            try
            {
                posForSecond = positioner.DistributeLongHeads(secondVerticalLines, longHeadParameters, fusionTrackParameters.Tracks.Single(t => t.Number == 1).RightSideFixed, corrugateOffsetTrack1, corrugateWidth, CrossHeadPosition, 0);
            }
            catch (LongHeadDistributionException)
            {
                exceptionWasThrown = true;
            }

            positionsAfterSecond = positioner.GetLongheadPositions();
        }

        [Then(@"for second collection long head (.*) should be positioned to line (.*) as move (.*)")]
        public void ThenForSecondCollectionLongHeadShouldBePositionedToLineAsMove(int longHeadNumber, int lineNumber, int movementOrder)
        {
            var movement = posForSecond.LongHeadsToPosition.ElementAt(movementOrder - 1);
            Specify.That(movement.LongHeadNumber).Should.BeLogicallyEqualTo(longHeadNumber);

            var lineCoordinate = secondVerticalLines.ElementAt(lineNumber - 1).EndCoordinate.X;
            var toolPosition = ConvertSensorPositionToToolPosition(longHeadNumber, movement.Position);
            Specify.That(toolPosition).Should.BeLogicallyEqualTo(lineCoordinate);
        }
        
        [Then(@"for second collection(.*) long heads are positioned")]
        public void ThenForSecondCollectionOnlyXLongHeadsArePositioned(int numberOfLongHeadsPositioned)
        {
            Specify.That(posForSecond.LongHeadsToPosition.Count()).Should.BeEqualTo(numberOfLongHeadsPositioned);
        }

        [Then(@"for second collection long head positions should be")]
        public void ThenForSecondCollectionLongHeadPositionsShouldBe(Table table)
        {
            var expectedPositions = table.CreateSet<LongHeadTestContainer>().OrderBy(x => x.Lh);
            var positonsFromPositioner = positionsAfterSecond;
            for (var i = 0; i < expectedPositions.Count(); i++)
            {
                Specify.That(positonsFromPositioner.ElementAt(i)).Should.BeLogicallyEqualTo(expectedPositions.ElementAt(i).Position);
            }
        }
    }
}
