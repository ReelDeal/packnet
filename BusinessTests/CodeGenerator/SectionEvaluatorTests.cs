﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator;
using PackNet.Business.CodeGenerator.AStarStuff;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

using Testing.Specificity;

namespace BusinessTests.CodeGenerator
{
    [TestClass]
    public class SectionEvaluatorTests
    {
        private SectionEvaluatorTestGuy sectionEvaluator;
        private EmPhysicalMachineSettings machinesettings;

        [TestInitialize]
        public void Setup()
        {
            sectionEvaluator = new SectionEvaluatorTestGuy();
            machinesettings = new EmPhysicalMachineSettings();
            var lh1 = new EmLongHead() { LeftSideToTool = 47, Number = 1, Position = 100};
            var lh2 = new EmLongHead() { LeftSideToTool = 47, Number = 2, Position = 200};
            var lh3 = new EmLongHead() { LeftSideToTool = 6, Number = 3, Position = 300};
            machinesettings.LongHeadParameters = new EmLongHeadParameters(new EmLongHead[] { }, 1.5, 90, 10, 55, 7, 2464, 0, 54, 143, 15d, 24d);

            machinesettings.LongHeadParameters.AddLongHead(lh1);
            machinesettings.LongHeadParameters.AddLongHead(lh2);
            machinesettings.LongHeadParameters.AddLongHead(lh3);

            machinesettings.FeedRollerParameters = new EmFeedRollerParameters(1.5, 30, 1, 55, 600, 45);
        }

        [TestMethod]
        public void ShouldNot_EvaluateSameSectionPermutationSeveralTimes()
        {
            //todo: fix non-deterministic test
            var timer = new Stopwatch();
            var originalPos = new MicroMeter[] { 100, 200, 900 };

            var section1 = new Section(new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(60,0), new PhysicalCoordinate(60, 600), LineType.crease),     
                new PhysicalLine(new PhysicalCoordinate(120,0), new PhysicalCoordinate(120, 600), LineType.crease),   
            }, new WasteArea[0], 0, 600);

            var section2 = new Section(new List<PhysicalLine>()
            {    
                new PhysicalLine(new PhysicalCoordinate(180,600), new PhysicalCoordinate(180, 1200), LineType.crease),     
                new PhysicalLine(new PhysicalCoordinate(240,600), new PhysicalCoordinate(240, 1200), LineType.crease),     
                new PhysicalLine(new PhysicalCoordinate(300,600), new PhysicalCoordinate(300, 1200), LineType.crease),     
                new PhysicalLine(new PhysicalCoordinate(360,600), new PhysicalCoordinate(360, 1200), LineType.crease) 
            }, new WasteArea[0], 600, 1200);

            var design = new RuleAppliedPhysicalDesign() { Length = 1200 };
            section1.Lines.ForEach(design.Add);
            section2.Lines.ForEach(design.Add);
            var sectionPermutation = new SectionPermutation(new[] { section1, section2 });

            Specify.That(sectionEvaluator.CachedSections.Count).Should.BeEqualTo(0);

            timer.Start();
            var lhDistribution1 = sectionEvaluator.GetLongHeadDistribution(machinesettings, originalPos, sectionPermutation, 2, design, double.MaxValue);
            timer.Stop();

            Specify.That(sectionEvaluator.CachedSections.Count).Should.BeEqualTo(2);

            var t1 = timer.ElapsedTicks;

            timer.Reset();

            timer.Start();
            var lhDistribution2 = sectionEvaluator.GetLongHeadDistribution(machinesettings, originalPos, sectionPermutation, 2, design, double.MaxValue);
            timer.Stop();

            var t2 = timer.ElapsedTicks;

            Console.WriteLine("T1: {0}, T2: {1}", t1, t2);

            Specify.That(t1 > t2/10).Should.BeTrue();
            Specify.That(sectionEvaluator.CachedSections.Count).Should.BeEqualTo(2);
            Specify.That(sectionEvaluator.CachedSections.ContainsKey(new Tuple<MicroMeter, MicroMeter>(0, 600))).Should.BeTrue();
            Specify.That(sectionEvaluator.CachedSections.ContainsKey(new Tuple<MicroMeter, MicroMeter>(600, 1200))).Should.BeTrue();
            Specify.That(lhDistribution1).Should.BeLogicallyEqualTo(lhDistribution2);
        }
    }

    public class SectionEvaluatorTestGuy : SectionEvaluator
    {
        public SectionEvaluatorTestGuy()
        {
            CachingThreshold = 0;
            CachedSections.Clear();
        }

        public IDictionary<Tuple<MicroMeter, MicroMeter>, IAStarNodeEM> CachedSections
        {
            get { return CachedSolutions; }
        }
    }
}
