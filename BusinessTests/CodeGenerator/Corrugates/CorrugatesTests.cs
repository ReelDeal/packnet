﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Exceptions;

namespace BusinessTests.Corrugates
{
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using PackNet.Business.Corrugates;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Data.Corrugates;

    using Testing.Specificity;
    
    [TestClass]
    public class CorrugatesTests
    {
        public const string TestDataPath = @".\CodeGenerator\TestData\";
        private List<Corrugate> corrugatesInDB;

        [TestInitialize]
        public void Setup()
        {
            corrugatesInDB = new List<Corrugate>();            
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_CreateSingleCorrugate()
        {
            var mockRepository = SetupMockRepo();

            var corrugatesManager = new Corrugates(mockRepository.Object);

            Specify.That(corrugatesManager.GetCorrugates().Count()).Should.BeEqualTo(0);

            var newCorrugate = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = Guid.Empty,
                Quality = 1,
                Thickness = 2
            };

            var created = corrugatesManager.Create(newCorrugate);
            
            mockRepository.Verify(repo => repo.Create(newCorrugate), Times.Once);

            Specify.That(corrugatesInDB).Should.Contain(newCorrugate);
            Specify.That(newCorrugate.Alias).Should.BeEqualTo(created.Alias);
            Specify.That(newCorrugate.Quality).Should.BeEqualTo(created.Quality);
            Specify.That(newCorrugate.Id).Should.BeEqualTo(created.Id);
            Specify.That(newCorrugate.Width).Should.BeEqualTo(created.Width);
            Specify.That(newCorrugate.Thickness).Should.BeEqualTo(created.Thickness);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_FindSpecificCorrugate()
        {
            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Dont do the time if youre not going to do the crime",
                Width = 2,
                Id = new Guid("bb7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 3
            };
            
            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate1, newCorrugate2 }); 

            var corrugatesManager = new Corrugates(mockRepository.Object);

            var foundCorrugate = corrugatesManager.Find(newCorrugate2.Id);

            mockRepository.Verify(repo => repo.Find(newCorrugate2.Id), Times.Once);

            Specify.That(foundCorrugate.Alias).Should.BeEqualTo(newCorrugate2.Alias);
            Specify.That(foundCorrugate.Quality).Should.BeEqualTo(newCorrugate2.Quality);
            Specify.That(foundCorrugate.Id).Should.BeEqualTo(newCorrugate2.Id);
            Specify.That(foundCorrugate.Width).Should.BeEqualTo(newCorrugate2.Width);
            Specify.That(foundCorrugate.Thickness).Should.BeEqualTo(newCorrugate2.Thickness);
            Specify.That(foundCorrugate.Id).Should.BeEqualTo(newCorrugate2.Id);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotAllow_CorrugateWithExistingGuid_ToBeAdded()
        {
            var newCorrugate = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate });

            var corrugatesManager = new Corrugates(mockRepository.Object);

            Specify.ThatAction(() => corrugatesManager.Create(newCorrugate)).Should.HaveThrown(typeof(AlreadyPersistedException));
        }

        [TestMethod]
        [TestCategory("Unit")]        
        public void ShouldNotAllow_CorrugatesWithIdenticalProperties_ToBeAdded()
        {
            var newCorrugate = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = Guid.Empty,
                Quality = 1,
                Thickness = 2
            };
            
            var corrugatesManager = new Corrugates(SetupMockRepo().Object);
            
            Specify.ThatAction(() => corrugatesManager.Create(newCorrugate)).Should.Not.HaveThrown(typeof(AlreadyPersistedException));

            Specify.ThatAction(() => corrugatesManager.Create(newCorrugate)).Should.HaveThrown(typeof(AlreadyPersistedException));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_GetAllCorrugates()
        {
            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Dont do the time if youre not going to do the crime",
                Width = 2,
                Id = new Guid("bb7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 3
            };

            var corrugates = new List<Corrugate> {newCorrugate1, newCorrugate2};

            var mockRepository = SetupMockRepo(corrugates);

            var corrugatesManager = new Corrugates(mockRepository.Object);

            var allCorrugates = corrugatesManager.GetCorrugates();

            mockRepository.Verify(repo => repo.All(), Times.Once);
            Specify.That(allCorrugates.SequenceEqual(corrugates)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_DeleteSingleCorrugate()
        {
            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Dont do the time if youre not going to do the crime",
                Width = 2,
                Id = new Guid("bb7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 3
            };

            var corrugates = new List<Corrugate> { newCorrugate1, newCorrugate2 };
            
            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate1, newCorrugate2 });

            var corrugatesManager = new Corrugates(mockRepository.Object);

            corrugatesManager.Delete(corrugates.First());

            mockRepository.Verify(repo => repo.Delete(It.Is<Corrugate>(c => c == newCorrugate1)), Times.Once);
            Specify.That(corrugatesManager.GetCorrugates().Count()).Should.BeEqualTo(1);
            Specify.That(corrugatesManager.GetCorrugates().First().Id.Equals(newCorrugate1.Id)).Should.BeFalse();
            Specify.That(corrugatesManager.GetCorrugates().First().Id.Equals(newCorrugate2.Id)).Should.BeTrue();            
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_DeleteManyCorrugates()
        {
            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Dont do the time if youre not going to do the crime",
                Width = 2,
                Id = new Guid("bb7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 3
            };

            var corrugates = new List<Corrugate> { newCorrugate1, newCorrugate2 };

            var newCorrugate3 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var newCorrugate4 = new Corrugate
            {
                Alias = "Dont do the time if youre not going to do the crime",
                Width = 2,
                Id = new Guid("bb7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 3
            };

            var corrugates2 = new List<Corrugate> { newCorrugate3, newCorrugate4 };

            var mockRepository = SetupMockRepo(corrugates);
            
            var corrugatesManager = new Corrugates(mockRepository.Object);

            corrugatesManager.Delete(corrugates2);

            mockRepository.Verify(repo => repo.Delete(It.Is<IEnumerable<Corrugate>>(c => c.SequenceEqual(corrugates2))), Times.Once);
            Specify.That(corrugatesManager.GetCorrugates().Count()).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_BeAbleTo_UpdateCorrrugate()
        {
            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate1 });

            var corrugatesManager = new Corrugates(mockRepository.Object);

            var updatedCorrugate = new Corrugate
            {
                Alias = "Omg I was updated",
                Width = 2,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 3,
                Thickness = 5
            };

            corrugatesManager.Update(updatedCorrugate, new List<Corrugate>());

            mockRepository.Verify(repo => repo.Update(It.Is<Corrugate>(c => c == updatedCorrugate)), Times.Once);
            Specify.That(corrugatesManager.GetCorrugates().Count()).Should.BeEqualTo(1);
            Specify.That(corrugatesManager.GetCorrugates().First().Alias).Should.BeEqualTo(updatedCorrugate.Alias);
            Specify.That(corrugatesManager.GetCorrugates().First().Width).Should.BeEqualTo(updatedCorrugate.Width);
            Specify.That(corrugatesManager.GetCorrugates().First().Id).Should.BeEqualTo(updatedCorrugate.Id);
            Specify.That(corrugatesManager.GetCorrugates().First().Quality).Should.BeEqualTo(updatedCorrugate.Quality);
            Specify.That(corrugatesManager.GetCorrugates().First().Id).Should.BeEqualTo(updatedCorrugate.Id);
            Specify.That(corrugatesManager.GetCorrugates().First().Thickness).Should.BeEqualTo(updatedCorrugate.Thickness);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(AlreadyPersistedException))]
        public void Should_NotAllow_CorrugateUpdate_IfAliasIsPersisted()
        {
            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = 1
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 2,
                Id = Guid.NewGuid(),
                Quality = 2,
                Thickness = 2
            };

            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate1 });

            var corrugatesManager = new Corrugates(mockRepository.Object);


            try
            {
                corrugatesManager.Update(newCorrugate2, new List<Corrugate>());
            }
            catch (Exception e)
            {
                Specify.That(e.Message.Contains("Alias")).Should.BeTrue();
                throw;
            }

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_Allow_CorrugateUpdate_OfAllFieldsIncludingAlias()
        {
            var id = Guid.NewGuid();

            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = id,
                Quality = 1,
                Thickness = 1
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Im the newer kid",
                Width = 2,
                Id = id,
                Quality = 2,
                Thickness = 2
            };

            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate1 });

            var corrugatesManager = new Corrugates(mockRepository.Object);

            corrugatesManager.Update(newCorrugate2, new List<Corrugate>());

            mockRepository.Verify(m => m.Update(newCorrugate2), Times.Once());

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_Allow_CorrugateUpdate_OfOnlyTheAlias()
        {
            var id = Guid.NewGuid();

            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = id,
                Quality = 1,
                Thickness = 1
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Im the newer kid",
                Width = 1,
                Id = id,
                Quality = 1,
                Thickness = 1
            };

            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate1 });

            var corrugatesManager = new Corrugates(mockRepository.Object);

            corrugatesManager.Update(newCorrugate2, new List<Corrugate>());

            mockRepository.Verify(m => m.Update(newCorrugate2), Times.Once());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_NotAllow_CorrugateUpdate_IfThereIsAnIdenticalCorrugate()
        {
            var newCorrugate1 = new Corrugate
            {
                Alias = "Im the new kid",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var newCorrugate2 = new Corrugate
            {
                Alias = "Dont do the time if youre not going to do the crime",
                Width = 2,
                Id = new Guid("bb7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 3
            };

            var mockRepository = SetupMockRepo(new List<Corrugate> { newCorrugate1, newCorrugate2 });

            var corrugatesManager = new Corrugates(mockRepository.Object);

            var updatedCorrugate = new Corrugate
            {
                Alias = "Omg I was updated",
                Width = 2,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 3
            };

            Specify.ThatAction(() => corrugatesManager.Update(updatedCorrugate, It.IsAny<IEnumerable<Corrugate>>())).Should.HaveThrown(typeof(AlreadyPersistedException));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldNot_BeAble_ToEditWidthOnCorrugateLoadedOnAMachine()
        {
            var loadedOnMachine = new Corrugate
            {
                Alias = "I am loaded on a machine",
                Width = 1,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var loadedEdited = new Corrugate
            {
                Alias = "I am loaded on a machine and someone wants to edit my width",
                Width = 3,
                Id = new Guid("ac7cf0ca-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var notLoadedOnMachine = new Corrugate
            {
                Alias = "I am not loaded on a machine",
                Width = 10,
                Id = new Guid("aaaaaaaa-9759-439e-9337-ae240a22d19b"),
                Quality = 1,
                Thickness = 2
            };

            var editedUnloaded = new Corrugate()
            {
                Alias = "I am not loaded on a machine and somoene wants to edit me",
                Width = 20,
                Id = new Guid("aaaaaaaa-9759-439e-9337-ae240a22d19b"),
                Quality = 100,
                Thickness = 15
            };

            var loadedCorrugates = new List<Corrugate> { loadedOnMachine };

            var mockRepository = SetupMockRepo(new List<Corrugate> { loadedOnMachine, notLoadedOnMachine });

            var corrugatesManager = new Corrugates(mockRepository.Object);

            Specify.ThatAction(() => corrugatesManager.Update(loadedEdited, loadedCorrugates)).Should.HaveThrown(typeof(EditOnLoadedCorrugateException));

            Specify.ThatAction(() => corrugatesManager.Update(editedUnloaded, loadedCorrugates)).Should.Not.HaveThrown(typeof(EditOnLoadedCorrugateException));
        }

        private Mock<ICorrugateRepository> SetupMockRepo(IEnumerable<Corrugate> initCorrugates = null)
        {
            if (initCorrugates != null)
            {
                initCorrugates.ForEach(corrugatesInDB.Add);    
            }
            
            var mockRepository = new Mock<ICorrugateRepository>();

            mockRepository.Setup(repo => repo.All()).Returns(corrugatesInDB);

            mockRepository.Setup(repo => repo.Create(It.IsAny<Corrugate>())).Callback<Corrugate>(corrugatesInDB.Add);

            mockRepository.Setup(repo => repo.Update(It.Is<Corrugate>(c => corrugatesInDB.Any(corr => corr.Id == c.Id)))).Callback<Corrugate>(c => corrugatesInDB[corrugatesInDB.FindIndex(corr => c.Id == corr.Id)] = c);

            mockRepository.Setup(repo => repo.Find(It.IsAny<Guid>())).Returns<Guid>(c => corrugatesInDB.FirstOrDefault(corr => corr.Id == c));

            mockRepository.Setup(repo => repo.Delete(It.Is<Corrugate>(c => corrugatesInDB.Contains(c)))).Callback<Corrugate>(c => corrugatesInDB.Remove(c));
            mockRepository.Setup(repo => repo.Delete(It.IsAny<IEnumerable<Corrugate>>())).Callback<IEnumerable<Corrugate>>(c => c.ForEach(corr =>
            {
                if (corrugatesInDB.Contains(corr))
                {
                    corrugatesInDB.Remove(corr);
                }
            }));

            return mockRepository;
        }
    }
}
