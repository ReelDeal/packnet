﻿namespace BusinessTests.Corrugates
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO.Corrugates;

    [TestClass]
    public class CorrugateTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        [Description("Test the == overload")]
        public void EqualsEqualsOperatorComparesNullCorrectly()
        {
            Corrugate nullCorrugate = null;
            var corrugate1 = new Corrugate();
            var corrugate2 = new Corrugate();

            Assert.IsFalse(nullCorrugate == corrugate1);
            Assert.IsTrue(nullCorrugate != corrugate1);
            Assert.IsFalse(corrugate1 == nullCorrugate);
            Assert.IsFalse(corrugate1.Equals(nullCorrugate));
            Assert.IsTrue(corrugate1 != nullCorrugate);
            Assert.IsTrue(corrugate1 == corrugate2);
            Assert.IsFalse(corrugate1 != corrugate2);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [Description("Test the == overload")]
        public void EqualsEqualsOperatorComparesCorrugateCorrectly()
        {
            var corrugate1 = new Corrugate{Quality = 1};
            var corrugate2 = new Corrugate{Quality = 2};

            Assert.IsFalse(corrugate1 == corrugate2);
            Assert.IsFalse(corrugate1.Equals(corrugate2));
            Assert.IsTrue(corrugate1 != corrugate2);
            corrugate2.Quality = 1;
            Assert.IsTrue(corrugate1 == corrugate2);
            Assert.IsTrue(corrugate1.Equals(corrugate2));
            Assert.IsFalse(corrugate1 != corrugate2);

            corrugate1.Width = 1;
            Assert.IsFalse(corrugate1 == corrugate2);
            Assert.IsFalse(corrugate1.Equals(corrugate2));
            Assert.IsTrue(corrugate1 != corrugate2);
            corrugate2.Width = 1;
            Assert.IsTrue(corrugate1 == corrugate2);
            Assert.IsTrue(corrugate1.Equals(corrugate2));
            Assert.IsFalse(corrugate1 != corrugate2);

            corrugate1.Thickness = 1;
            Assert.IsFalse(corrugate1 == corrugate2);
            Assert.IsFalse(corrugate1.Equals(corrugate2));
            Assert.IsTrue(corrugate1 != corrugate2);
            corrugate2.Thickness = 1;
            Assert.IsTrue(corrugate1 == corrugate2);
            Assert.IsTrue(corrugate1.Equals(corrugate2));
            Assert.IsFalse(corrugate1 != corrugate2);
        }
    }
}
