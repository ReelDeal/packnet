﻿using PackNet.Business.PickZone;
using PackNet.Common.Interfaces.Exceptions;

namespace BusinessTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;
    using PackNet.Common.Interfaces.DTO.PickZones;
    using PackNet.Data.PickArea;

    using Testing.Specificity;

    [TestClass]
    public class PickZoneManagerTests
    {
        private Mock<IPickZoneRepository> provider;
        private PickZones manager;
        private List<PickZone> pickZones;
        private Guid pz1Guid;
        private Guid pz2Guid;

        [TestInitialize]
        public void TestInitialize()
        {
            provider = SetUpProviderMock();
            manager = new PickZones(provider.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnPickzoneWithSpecificAlias()
        {
            provider.Setup(p => p.FindByAlias("PZ1")).Returns(new PickZone(){Alias = "PZ1"});
            provider.Setup(p => p.FindByAlias("PZ2")).Returns(new PickZone(){Alias = "PZ2"});

            var zone1 = manager.Find("PZ1");
            var zone2 = manager.Find("PZ2");
            var zone3 = manager.Find("ASD");

            Specify.That(zone1).Should.Not.BeNull();
            Specify.That(zone2).Should.Not.BeNull();
            Specify.That(zone3).Should.BeNull();

            Specify.That(zone1.Alias).Should.BeEqualTo("PZ1");
            Specify.That(zone2.Alias).Should.BeEqualTo("PZ2");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnAllPickZones()
        {
            var zones = manager.GetPickZones();

            Specify.That(zones).Should.Not.BeNull();
            Specify.That(zones.Count()).Should.BeEqualTo(2);
            Specify.That(zones.ElementAt(0).Alias).Should.BeEqualTo("PZ1");
            Specify.That(zones.ElementAt(1).Alias).Should.BeEqualTo("PZ2");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreatePickZone()
        {
            var zoneToCreate = new PickZone{Alias = "PZ3"};
            var zone = manager.Create(zoneToCreate);

            Specify.That(zone).Should.Not.BeNull();
            Specify.That(zone.Id).Should.Not.BeEqualTo(Guid.Empty);

            provider.Verify(m => m.Create(zoneToCreate), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(AlreadyPersistedException))]
        public void ShouldThrowErrorWhenAliasIsAlreadyAdded()
        {
            var zoneToCreate = new PickZone{Alias = "PZ2"};

            provider.Setup(p => p.FindByAlias(zoneToCreate.Alias)).Returns(new PickZone());
            try
            {
                var zone = manager.Create(zoneToCreate);
            }
            catch (Exception e)
            {
                Specify.That(e.Message.Contains("Could not Create Pick Zone, Alias is already in use")).Should.BeTrue();
                throw;
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(Exception))]
        public void ShouldThrowErrorWhenRepositoryIdIsAlreadyPopulated()
        {
            var zoneToCreate = new PickZone{Alias = "PZ2", Id= Guid.NewGuid()};
            try
            {
                var zone = manager.Create(zoneToCreate);
            }
            catch (Exception e)
            {
                Specify.That(e.Message.Contains("Invalid Pick Zone ID, Id is already populated")).Should.BeTrue();
                throw;
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdatePickZone()
        {
            var zoneToUpdate = manager.GetPickZones().First(p => p.Alias == "PZ1");
            var zone = manager.Update(zoneToUpdate);

            Specify.That(zone).Should.Not.BeNull();
            Specify.That(zone.Id).Should.Not.BeEqualTo(Guid.Empty);

            provider.Verify(m => m.Update(zoneToUpdate), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(Exception))]
        public void ShouldThrowExceptionWhenUpdatingWithEmptyGuid()
        {
            var zoneToUpdate = new PickZone{Alias = "PZ2", Id = Guid.Empty };

            try
            {
                var zone = manager.Update(zoneToUpdate);
            }
            catch (Exception e)
            {
                Specify.That(e.Message.Contains("Could not Update Pick Zone, Repository Id was Empty")).Should.BeTrue();
                throw;
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDeletePickZone()
        {
            var zoneToCreate = new PickZone{Alias = "PZ2"};
            manager.Delete(zoneToCreate);

            provider.Verify(m => m.Delete(zoneToCreate), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionWhenTryingToDeletePickZoneWithAssignedWork()
        {
            var zoneToDelete = new PickZone { Alias = "PZ2", NumberOfRequests = 2};
            try
            {
                manager.Delete(zoneToDelete);
            }
            catch (Exception e)
            {
                Specify.That(e.Message.Contains("Cannot delete pickzone with assigned work"));
            }

            provider.Verify(m => m.Delete(zoneToDelete), Times.Never);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDeletePickZones()
        {
            var zoneToCreates = new List<PickZone>() { new PickZone{Alias = "PZ2"}, new PickZone{Alias = "ASD"}, new PickZone{Alias = "123"} };
            manager.Delete(zoneToCreates);
            
            provider.Verify(m => m.Delete(zoneToCreates), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionWhenTryingToDeletePickZonesWhenAnyOfThePickZonesHasAssignedWork()
        {
            var zonesToDelete = new List<PickZone>()
            {
                new PickZone() { Alias = "PZ1", NumberOfRequests = 0 },
                new PickZone() { Alias = "PZ2", NumberOfRequests = 2 },
                new PickZone() { Alias = "PZ3", NumberOfRequests = 0 }
            };

            try
            {
                manager.Delete(zonesToDelete);
            }
            catch (Exception e)
            {
                Specify.That(e.Message.Contains("Cannot delete pickzone with assigned work"));
            }
            provider.Verify(m => m.Delete(zonesToDelete), Times.Never);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLocatePickzoneOnId()
        {
            var zone = manager.Find(pz2Guid);

            provider.Verify(m => m.Find(pz2Guid), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionWhenEditingNonExistantPacksizeId()
        {
            var zoneToUpdate = manager.GetPickZones().First(p => p.Alias == "PZ1");

            var zone = manager.Update(zoneToUpdate);
            Specify.That(zone).Should.Not.BeNull();

            zoneToUpdate = new PickZone { Alias = "PZ2123", Id = Guid.NewGuid() };

            zone = manager.Update(zoneToUpdate);
            Specify.That(zone).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionWhenTryingToUpdatePickZoneWithAssignedWork()
        {
            var model = pickZones[0];
            var zoneToUpdate = new PickZone {Id = model.Id, Alias = model.Alias + "_renamed"};
            try
            {
                var oldValue = model.NumberOfRequests;
                model.NumberOfRequests = 1;
                manager.Update(zoneToUpdate);
                model.NumberOfRequests = oldValue;
            }
            catch (Exception e)
            {
                Specify.That(e.Message.Equals("Cannot Update Alias for Pick Zone with assigned work")).Should.BeTrue();
            }

            provider.Verify(m => m.Delete(zoneToUpdate), Times.Never);
        }

        private Mock<IPickZoneRepository> SetUpProviderMock()
        {
            var providerMock = new Mock<IPickZoneRepository>();

            pz1Guid = Guid.NewGuid();
            pz2Guid = Guid.NewGuid();

            pickZones = new List<PickZone>() { new PickZone{Alias = "PZ1", Id = pz1Guid }, new PickZone{Alias = "PZ2", Id = pz2Guid } };

            providerMock.Setup(provider => provider.All()).Returns(pickZones);
            providerMock.Setup(provider => provider.Create(It.IsAny<PickZone>())).Callback<PickZone>((a) => a.Id = Guid.NewGuid());
            providerMock.Setup(provider => provider.Update(It.IsIn<PickZone>(pickZones))).Returns<PickZone>((a) => a);

            return providerMock;
        }
    }
}
