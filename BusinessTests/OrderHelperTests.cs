﻿using System.Collections.Generic;
using System.ComponentModel;

using PackNet.Business.PackagingDesigns;
using System.Linq;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using Testing.Specificity;

namespace BusinessTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using PackNet.Business.Orders;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;

    [TestClass]
    public class OrderHelperTests
    {
        private const int NormalDesign = 1;
        private const int LinesOutsideDesign = 2;

        [TestMethod]
        [TestCategory("Unit")]
        public void MergeDesignsSideBySide_ShouldReturnSingleDesign_ForSingleCarton()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign } };
            var corrugate = new Corrugate();
            var mergedDesign = OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);

            Specify.That(mergedDesign.Lines.Count()).Should.BeEqualTo(4);
            Specify.That(mergedDesign.Width).Should.BeLogicallyEqualTo(100);
            Specify.That(mergedDesign.Length).Should.BeLogicallyEqualTo(200);

            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 0, 0, 0, 200, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 100, 0, 100, 200, LineType.crease)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 0, 0, 100, 0, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 0, 200, 100, 200, LineType.crease)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnSingleDesignWithLinesOutsideModified_ForSingleCarton()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = LinesOutsideDesign } };
            var corrugate = new Corrugate();
            var mergedDesign = OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);

            Specify.That(mergedDesign.Lines.Count()).Should.BeEqualTo(3);
            Specify.That(mergedDesign.Width).Should.BeLogicallyEqualTo(300);
            Specify.That(mergedDesign.Length).Should.BeLogicallyEqualTo(300);

            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 0, 200, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 50, 300, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 300, 50, 300, 300, LineType.cut)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void MergeDesignsSideBySide_ShouldReturnMergedDesigns_ForMultipleCartons()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign }, new Carton() { DesignId = NormalDesign }, new Carton() { DesignId = NormalDesign } };
            var corrugate = new Corrugate();

            var mergedDesign = OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);
            Specify.That(mergedDesign.Lines.Count()).Should.BeEqualTo(12);
            Specify.That(mergedDesign.Width).Should.BeLogicallyEqualTo(300);
            Specify.That(mergedDesign.Length).Should.BeLogicallyEqualTo(200);
            //Lines from first design
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 0, 0, 0, 200, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 100, 0, 100, 200, LineType.crease)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 0, 0, 100, 0, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 0, 200, 100, 200, LineType.crease)).Should.BeEqualTo(1);
            //Lines from second design
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 100, 0, 100, 200, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 0, 200, 200, LineType.crease)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 100, 0, 200, 0, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 100, 200, 200, 200, LineType.crease)).Should.BeEqualTo(1);
            //Lines from third design
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 0, 200, 200, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 300, 0, 300, 200, LineType.crease)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 0, 300, 0, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 200, 300, 200, LineType.crease)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnMergedDesignWithLinesOutsideModified_ForMultipleCartons()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = LinesOutsideDesign }, new Carton() { DesignId = LinesOutsideDesign }, new Carton() { DesignId = LinesOutsideDesign } };
            var corrugate = new Corrugate();
            var mergedDesign = OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);

            Specify.That(mergedDesign.Lines.Count()).Should.BeEqualTo(3*3);
            Specify.That(mergedDesign.Width).Should.BeLogicallyEqualTo(300*3);
            Specify.That(mergedDesign.Length).Should.BeLogicallyEqualTo(300);

            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 0, 200, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 200, 50, 300, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 300, 50, 300, 300, LineType.cut)).Should.BeEqualTo(1);
            //Lines from second design
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 500, 0, 500, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 500, 50, 600, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 600, 50, 600, 300, LineType.cut)).Should.BeEqualTo(1);
            //Lines from third design
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 800, 0, 800, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 800, 50, 900, 50, LineType.cut)).Should.BeEqualTo(1);
            Specify.That(NumberOfMatchingLines(mergedDesign.Lines, 900, 50, 900, 300, LineType.cut)).Should.BeEqualTo(1);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree0()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree0} };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);

            designManager.Verify(d =>d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree0, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree0Flip()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree0_flip } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);

            designManager.Verify(d => d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree0_flip, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree90()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree90 } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object, true);

            designManager.Verify(d => d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree90, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree90Flip()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree90_flip } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object, true);

            designManager.Verify(d => d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree90_flip, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree180()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree180 } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);

            designManager.Verify(d => d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree180, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree180Flip()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree180_flip } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);

            designManager.Verify(d => d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree180_flip, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree270()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree270 } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object, true);

            designManager.Verify(d => d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree270, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConsiderRotationRestrictionWhenRotationIsDegree270Flip()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign, Rotation = OrientationEnum.Degree270_flip } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object, true);

            designManager.Verify(d => d.GetPhyscialDesignForCarton(cartons.First(), corrugate, OrientationEnum.Degree270_flip, 1), Times.Once, "GetPhysicalDesignForCarton should be called with correct rotation");
        }

        private int NumberOfMatchingLines(IEnumerable<PhysicalLine> lines, MicroMeter startX, MicroMeter startY, MicroMeter endX, MicroMeter endY, LineType type)
        {
            return
                lines.Count(
                    l =>
                        l.StartCoordinate.X == startX && l.StartCoordinate.Y == startY && l.EndCoordinate.X == endX &&
                        l.EndCoordinate.Y == endY && l.Type == type);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void MergeDesignsSideBySide_ShouldAskPackagingDesignManagerForDesignWithCorrectRotation()
        {
            var designManager = SetupDesignManager();
            var cartons = new[] { new Carton() { DesignId = NormalDesign } };
            var corrugate = new Corrugate();

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object);
            designManager.Verify(m => m.GetPhyscialDesignForCarton(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.Is<OrientationEnum>(e => e == OrientationEnum.Degree0), It.IsAny<int>()), Times.Once());

            OrderHelpers.MergeDesignsSideBySide(cartons, corrugate, true, designManager.Object, true);
            designManager.Verify(m => m.GetPhyscialDesignForCarton(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.Is<OrientationEnum>(e => e == OrientationEnum.Degree90), It.IsAny<int>()), Times.Once());
        }

        private Mock<IPackagingDesignManager> SetupDesignManager()
        {
            var mock = new Mock<IPackagingDesignManager>();
            mock.Setup(m => m.GetPhyscialDesignForCarton(It.Is<ICarton>(c => c.DesignId == NormalDesign), It.IsAny<Corrugate>(), It.IsAny<OrientationEnum>()))
                .Returns<ICarton, Corrugate, OrientationEnum>((a, b, c) =>
                {
                    var physicalDesign = new PhysicalDesign { Length = 200, Width = 100 };
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 200),
                        LineType.crease));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(100, 0), LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(100, 200),
                        LineType.crease));
                    return physicalDesign;
                });

            mock.Setup(m => m.GetPhyscialDesignForCarton(It.Is<ICarton>(c => c.DesignId == NormalDesign), It.IsAny<Corrugate>(), It.IsAny<OrientationEnum>(), It.IsAny<int>()))
                .Returns<ICarton, Corrugate, OrientationEnum, int>((a, b, c, d) =>
                {
                    var physicalDesign = new PhysicalDesign { Length = 200, Width = 100 };
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 200),
                        LineType.crease));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(100, 0), LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(100, 200),
                        LineType.crease));
                    return physicalDesign;
                });

            mock.Setup(m => m.CalculateUsage(It.Is<ICarton>(c => c.DesignId == NormalDesign), It.IsAny<Corrugate>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new PackagingDesignCalculation { Length = 200 });
            
            //Lines outside design carton
            mock.Setup(m => m.GetPhyscialDesignForCarton(It.Is<ICarton>(c => c.DesignId == LinesOutsideDesign), It.IsAny<Corrugate>(), It.IsAny<OrientationEnum>()))
                .Returns<ICarton, Corrugate, OrientationEnum>((a, b, c) =>
                {
                    var physicalDesign = new PhysicalDesign { Length = 300, Width = 300 };
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 50),
                        LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 50), new PhysicalCoordinate(400, 50),
                        LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(400, 300),
                        LineType.cut));
                    return physicalDesign;
                });
            mock.Setup(m => m.GetPhyscialDesignForCarton(It.Is<ICarton>(c => c.DesignId == LinesOutsideDesign), It.IsAny<Corrugate>(), It.IsAny<OrientationEnum>(), It.IsAny<int>()))
                .Returns<ICarton, Corrugate, OrientationEnum, int>((a, b, c, d) =>
                {
                    var physicalDesign = new PhysicalDesign { Length = 300, Width = 300 };
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 50),
                        LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 50), new PhysicalCoordinate(400, 50),
                        LineType.cut));
                    physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(400, 300),
                        LineType.cut));
                    return physicalDesign;
                });
            mock.Setup(m => m.CalculateUsage(It.Is<ICarton>(c => c.DesignId == LinesOutsideDesign), It.IsAny<Corrugate>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new PackagingDesignCalculation { Length = 300 });
            
            return mock;
        }
    }
}
