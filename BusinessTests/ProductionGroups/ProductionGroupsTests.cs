﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using Newtonsoft.Json;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.ProductionGroups;
using PackNet.Services;

using Testing.Specificity;

namespace BusinessTests.ProductionGroups
{
    [TestClass]
    public class ProductionGroupsTests
    {
        private PackNet.Business.ProductionGroups.ProductionGroups _productionGroups;
        private EventAggregator _eventAggregator;
        private Mock<IProductionGroupRepository> _mockRepo;
        private Mock<ILogger> _mockLogger;
        private ProductionGroup _newProductionGroup;
        private JsonSerializerSettings settings = SerializationSettings.GetJsonSerializerSettings();
        private IServiceLocator serviceLocator;
        [TestInitialize]
        public void Setup()
        {
            _eventAggregator = new EventAggregator();
            _mockRepo = new Mock<IProductionGroupRepository>();
            _mockLogger = new Mock<ILogger>();
            _newProductionGroup = new ProductionGroup();
            serviceLocator  = new ServiceLocator("", new List<IService>(), _mockLogger.Object);
            _productionGroups = new PackNet.Business.ProductionGroups.ProductionGroups(_eventAggregator, _eventAggregator, _mockRepo.Object, serviceLocator, _mockLogger.Object);
        }

        [TestMethod]
        [TestCategory("Bug 7355")]
        [TestCategory("Bug")]
        public void Should_Create_BroadcastsNewProductionGroups()
        {
            _newProductionGroup.Alias = "NewPg";
            _newProductionGroup.SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;
            IMessage<ProductionGroup> broadcast = null;
            _eventAggregator.GetEvent<IMessage<ProductionGroup>>()
                .Subscribe(pg => broadcast = pg);
            _productionGroups.Create(_newProductionGroup);

            Specify.That(broadcast).Should.Not.BeNull();
            Specify.That(broadcast.Data).Should.Not.BeNull();
            Specify.That(broadcast.Data).Should.BeEqualTo(_newProductionGroup);
            Specify.That(broadcast.MessageType).Should.BeEqualTo(ProductionGroupMessages.ProductionGroupCreated);
        }

        [TestMethod]
        public void Should_Create_AProductionGroup()
        {
            _newProductionGroup.Alias = "NewPg";
            _newProductionGroup.SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;

            _productionGroups.Create(_newProductionGroup);

            _mockRepo.Verify(p => p.Create(It.Is<ProductionGroup>(g => g.Alias == _newProductionGroup.Alias)), Times.Once);
        }

        [TestMethod]
        public void Should_Update_AProductionGroup()
        {
            _newProductionGroup.Id = new Guid();

            _newProductionGroup.SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;

            _productionGroups.Create(_newProductionGroup);

            _mockRepo.Setup(p => p.Find(It.Is<Guid>(id => id == _newProductionGroup.Id))).Returns(_newProductionGroup);

            _newProductionGroup.SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst;

            _productionGroups.Update(_newProductionGroup);

            _mockRepo.Verify(p => p.Update(It.Is<ProductionGroup>(g => g.Id == _newProductionGroup.Id)), Times.Once);
        }

        [TestMethod]
        public void Should_Delete_AProductionGroup()
        {
            _newProductionGroup.Id = new Guid();

            _productionGroups.Delete(_newProductionGroup);

            _mockRepo.Verify(p => p.Delete(It.Is<ProductionGroup>(g => g.Id == _newProductionGroup.Id)), Times.Once);
        }

        [TestMethod]
        public void Should_Find_AProductionGroup()
        {
            _newProductionGroup.Id = new Guid();

            _productionGroups.Find(_newProductionGroup.Id);

            _mockRepo.Verify(p => p.Find(It.Is<Guid>(g => g == _newProductionGroup.Id)), Times.Once);
        }

        [TestMethod]
        public void Should_Get_AllProductionGroups()
        {
            _newProductionGroup.Id = new Guid();

            _productionGroups.GetProductionGroups();

            _mockRepo.Verify(p => p.All(), Times.Once);
        }

        [TestMethod]
        public void Should_FindAProdutionGroup_ByMachineGroupId()
        {
            var machineGroup1 = new MachineGroup() { Id = Guid.NewGuid() };
            var machineGroup2 = new MachineGroup() { Id = Guid.NewGuid() };

            _newProductionGroup.Id = Guid.NewGuid();
            _newProductionGroup.ConfiguredMachineGroups.Add(machineGroup1.Id);

            var pg2 = new ProductionGroup() { Id = Guid.NewGuid() };

            var productionGroups = new List<ProductionGroup>() { _newProductionGroup, pg2 };

            _mockRepo.Setup(p => p.All()).Returns(productionGroups);
            _mockRepo.Setup(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup1.Id))).Returns(_newProductionGroup);
            _mockRepo.Setup(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup2.Id))).Returns(pg2);

            var productionGroupReturned = _productionGroups.GetProductionGroupForMachineGroup(machineGroup1.Id);

            Specify.That(productionGroupReturned).Should.Not.BeNull();

            Specify.That(productionGroupReturned).Should.BeEqualTo(_newProductionGroup);

            pg2.ConfiguredMachineGroups.Add(machineGroup2.Id);

            productionGroupReturned = _productionGroups.GetProductionGroupForMachineGroup(machineGroup2.Id);

            Specify.That(productionGroupReturned).Should.Not.BeNull();

            Specify.That(productionGroupReturned).Should.BeEqualTo(pg2);
        }

        [TestMethod]
        public void Should_FindAProductionGroup_ByCPGId()
        {
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "cpg1" };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid(), Alias = "cpg2" };

            var serializedCPG1 = JsonConvert.SerializeObject(new List<CartonPropertyGroup>() { cpg1 }, settings);
            var serializedCPG2 = JsonConvert.SerializeObject(new List<CartonPropertyGroup>() { cpg2 }, settings);

            var jsonData1 = new Dictionary<string, string> { { "ConfiguredCartonPropertyGroups", serializedCPG1 } };

            _newProductionGroup.Options = jsonData1;

            var jsonData2 = new Dictionary<string, string> { { "ConfiguredCartonPropertyGroups", serializedCPG2 } };

            var pg2 = new ProductionGroup() { Id = Guid.NewGuid(), Options = jsonData2 };

            var productionGroups = new List<ProductionGroup>() { _newProductionGroup, pg2 };

            _mockRepo.Setup(p => p.All()).Returns(productionGroups);

            var productionGroupReturned = _productionGroups.FindByCartonPropertyGroupId(Guid.NewGuid());

            Specify.That(productionGroupReturned).Should.BeNull();

            productionGroupReturned = _productionGroups.FindByCartonPropertyGroupAlias(string.Empty);
            
            Specify.That(productionGroupReturned).Should.BeNull();

            productionGroupReturned = _productionGroups.FindByCartonPropertyGroupId(cpg1.Id);

            Specify.That(productionGroupReturned).Should.Not.BeNull();
            Specify.That(productionGroupReturned.Id).Should.BeEqualTo(_newProductionGroup.Id);

            productionGroupReturned = _productionGroups.FindByCartonPropertyGroupAlias(cpg1.Alias);

            Specify.That(productionGroupReturned).Should.Not.BeNull();
            Specify.That(productionGroupReturned.Id).Should.BeEqualTo(_newProductionGroup.Id);

            productionGroupReturned = _productionGroups.FindByCartonPropertyGroupId(cpg2.Id);

            Specify.That(productionGroupReturned).Should.Not.BeNull();
            Specify.That(productionGroupReturned.Id).Should.BeEqualTo(pg2.Id);

            productionGroupReturned = _productionGroups.FindByCartonPropertyGroupAlias(cpg2.Alias);

            Specify.That(productionGroupReturned).Should.Not.BeNull();
            Specify.That(productionGroupReturned.Id).Should.BeEqualTo(pg2.Id);
        }

        [TestMethod]
        public void Should_FindAProductionGroup_ByAlias()
        {
            _newProductionGroup.Id = new Guid();
            _newProductionGroup.Alias = "PG1";

            var pg2 = new ProductionGroup() { Alias = "PG2" };

            var productionGroups = new List<ProductionGroup>() { _newProductionGroup, pg2 };

            _mockRepo.Setup(p => p.All()).Returns(productionGroups);

            var productionGroupReturned = _productionGroups.FindByAlias("PG");
            
            _mockRepo.Verify(p => p.All(), Times.Once);

            Specify.That(productionGroupReturned).Should.BeNull();

            productionGroupReturned = _productionGroups.FindByAlias("PG1");

            Specify.That(productionGroupReturned).Should.Not.BeNull();

            Specify.That(productionGroupReturned).Should.BeEqualTo(_newProductionGroup);
            
            Specify.That(productionGroupReturned.Id).Should.BeEqualTo(_newProductionGroup.Id);
        }

        [TestMethod]
        public void Should_FindProductionGroups_ByCorrugate()
        {
            _newProductionGroup.Id = Guid.NewGuid();

            var corrugate1 = new Corrugate() { Id = Guid.NewGuid() };
            var corrugate2 = new Corrugate() { Id = Guid.NewGuid() };
            var corrugateUnused = new Corrugate() { Id = Guid.NewGuid() };

            _newProductionGroup.ConfiguredCorrugates.Add(corrugate1.Id);
            _newProductionGroup.ConfiguredCorrugates.Add(corrugate2.Id);

            var pg2 = new ProductionGroup() { Id = Guid.NewGuid() };

            pg2.ConfiguredCorrugates.Add(corrugate1.Id);

            var productionGroups = new List<ProductionGroup>() { _newProductionGroup, pg2};

            _mockRepo.Setup(p => p.All()).Returns(productionGroups);

            var productionGroupReturned = _productionGroups.GetProductionGroupsForCorrugate(corrugateUnused.Id);

            Specify.That(productionGroupReturned.Count).Should.BeEqualTo(0);

            productionGroupReturned = _productionGroups.GetProductionGroupsForCorrugate(corrugate1.Id);

            Specify.That(productionGroupReturned).Should.Not.BeNull();

            Specify.That(productionGroupReturned.Count).Should.BeEqualTo(2);

            Specify.That(productionGroupReturned.FirstOrDefault(pg => pg.Id == _newProductionGroup.Id))
                .Should.BeEqualTo(_newProductionGroup);

            Specify.That(productionGroupReturned.FirstOrDefault(pg => pg.Id == pg2.Id)).Should.BeEqualTo(pg2);
            
            productionGroupReturned = _productionGroups.GetProductionGroupsForCorrugate(corrugate2.Id);

            Specify.That(productionGroupReturned.Count).Should.BeEqualTo(1);

            Specify.That(productionGroupReturned.FirstOrDefault(pg => pg.Id == _newProductionGroup.Id))
                .Should.BeEqualTo(_newProductionGroup);
        }

        [TestMethod]
        public void Should_RemoveACorrugate_FromProductionGroups()
        {
            _newProductionGroup.Id = Guid.NewGuid();
	        _newProductionGroup.Alias = "PG1";

            var corrugate1 = new Corrugate() { Id = Guid.NewGuid() };
            var corrugate2 = new Corrugate() { Id = Guid.NewGuid() };

            _newProductionGroup.ConfiguredCorrugates.Add(corrugate1.Id);
            _newProductionGroup.ConfiguredCorrugates.Add(corrugate2.Id);

            var pg2 = new ProductionGroup() { Id = Guid.NewGuid(), Alias = "PG2"};

            pg2.ConfiguredCorrugates.Add(corrugate1.Id);

            var productionGroups = new List<ProductionGroup>() { _newProductionGroup, pg2 };

            _mockRepo.Setup(p => p.All()).Returns(productionGroups);
            _mockRepo.Setup(p => p.Find(It.Is<Guid>(id => id == _newProductionGroup.Id))).Returns(_newProductionGroup);
            _mockRepo.Setup(p => p.Find(It.Is<Guid>(id => id == pg2.Id))).Returns(pg2);

            _productionGroups.RemoveCorrugateFromProductionGroups(corrugate1);

            _mockRepo.Verify(p => p.Update(It.IsAny<ProductionGroup>()), Times.Exactly(2));

            var productionGroupReturned = _productionGroups.GetProductionGroupsForCorrugate(corrugate1.Id);

            Specify.That(productionGroupReturned.Count).Should.BeEqualTo(0);

            productionGroupReturned = _productionGroups.GetProductionGroupsForCorrugate(corrugate2.Id);

            Specify.That(productionGroupReturned).Should.Not.BeNull();

            Specify.That(productionGroupReturned.Count).Should.BeEqualTo(1);

            Specify.That(productionGroupReturned.FirstOrDefault(pg => pg.Id == _newProductionGroup.Id)).Should.BeEqualTo(_newProductionGroup);
        }

        [TestMethod]
        public void Should_RemoveAMachineGroup_FromProductionGroup()
        {
            var machineGroup1 = new MachineGroup() { Id = Guid.NewGuid() };
            var machineGroup2 = new MachineGroup() { Id = Guid.NewGuid() };

            _newProductionGroup.Id = Guid.NewGuid();
			_newProductionGroup.Alias = "PG1";
            _newProductionGroup.ConfiguredMachineGroups.Add(machineGroup1.Id);

            var pg2 = new ProductionGroup() { Id = Guid.NewGuid(), Alias = "PG2" };
            pg2.ConfiguredMachineGroups.Add(machineGroup2.Id);

            var productionGroups = new List<ProductionGroup>() { _newProductionGroup, pg2 };

            _mockRepo.Setup(p => p.All()).Returns(productionGroups);
            _mockRepo.Setup(p => p.Find(It.Is<Guid>(id => id == _newProductionGroup.Id))).Returns(_newProductionGroup);
            _mockRepo.Setup(p => p.Find(It.Is<Guid>(id => id == pg2.Id))).Returns(pg2);
            _mockRepo.Setup(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup1.Id))).Returns(_newProductionGroup);
            _mockRepo.Setup(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup2.Id))).Returns(pg2);

            var productionGroupReturned = _productionGroups.GetProductionGroupForMachineGroup(machineGroup1.Id);

            _mockRepo.Verify(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup1.Id)), Times.Exactly(1));

            Specify.That(productionGroupReturned).Should.Not.BeNull();

            Specify.That(productionGroupReturned).Should.BeEqualTo(_newProductionGroup);

            productionGroupReturned = _productionGroups.GetProductionGroupForMachineGroup(machineGroup2.Id);

            _mockRepo.Verify(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup2.Id)), Times.Exactly(1));
            
            Specify.That(productionGroupReturned).Should.Not.BeNull();

            Specify.That(productionGroupReturned).Should.BeEqualTo(pg2);

            _productionGroups.RemoveMachineGroupFromProductionGroup(machineGroup1);

            _mockRepo.Verify(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup1.Id)), Times.Exactly(2));

            _mockRepo.Verify(p => p.Update(It.IsAny<ProductionGroup>()), Times.Exactly(1));

            _productionGroups.RemoveMachineGroupFromProductionGroup(machineGroup2);

            _mockRepo.Verify(p => p.FindByMachineGroup(It.Is<Guid>(id => id == machineGroup2.Id)), Times.Exactly(2));

            _mockRepo.Verify(p => p.Update(It.IsAny<ProductionGroup>()), Times.Exactly(2));
        }
    }
}