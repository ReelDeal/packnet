﻿using PackNet.Common.Eventing;

namespace BusinessTests.ProductionGroups
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

    [TestClass]
    public class ProductionGroupManagerTests
    {
        //private Guid machineGroup1Id = Guid.NewGuid();
        //private Guid machineGroup2Id = Guid.NewGuid();
        //private Guid machineGroup3Id = Guid.NewGuid();

        //private Guid corrugate1Id = Guid.NewGuid();
        //private Guid corrugate2Id = Guid.NewGuid();
        //private Guid c3Id = Guid.NewGuid();

        //private List<CommonProductionGroups.ProductionGroup> testProductionGroups;
        //private Mock<ICartonRequestManager> cartonRequestManagerMock;
        //private Mock<ICorrugates> corrugateManagerMock;
        //private object productionGroupThatChanged;
        //private ILogger logger;
        //private EventAggregator eventAggregator = new EventAggregator();
        //private Mock<ProductionGroup> productionGroup;

        //[TestInitialize]
        //public void TestInitialize()
        //{
        //    testProductionGroups = SetUpTestProductionGroups();
        //    corrugateManagerMock = new Mock<ICorrugates>();
        //    cartonRequestManagerMock = new Mock<ICartonRequestManager>();
        //    productionGroup = new Mock<ProductionGroup>();
        //    productionGroup.Setup(pg => pg.GetProductionGroups()).Returns(testProductionGroups);
        //    SetUpLogging();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ConstructorTest()
        //{
            //Assert.Fail("Jeff -- Make sure this functionality (in the entire file) gets into the PG service");
            //var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

            //Specify.That(productionGroupManager.GetType()).Should.BeEqualTo(typeof(ProductionGroupManager));
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldInitializeGroupsFromProductionGroupProvider()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    Specify.That(productionGroupManager.ProductionGroups.Count()).Should.BeEqualTo(4);
        //    Specify.That(productionGroupManager.ProductionGroups.ElementAt(0).Alias).Should.BeEqualTo("1");
        //    Specify.That(productionGroupManager.ProductionGroups.ElementAt(1).Alias).Should.BeEqualTo("2");
        //    Specify.That(productionGroupManager.ProductionGroups.ElementAt(2).Alias).Should.BeEqualTo("3");
        //    Specify.That(productionGroupManager.ProductionGroups.ElementAt(3).Alias).Should.BeEqualTo("4");
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldSetWaveSelectionAlgorithmConfigByDefault()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    Specify.That(productionGroupManager.ProductionGroups.ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldLoadCorrugatesForPg()
        //{
        //    //var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = new Guid("14d0d723-e5a8-4475-a951-017a0473d181") }, new Corrugate { Alias = "cor2", Id = new Guid("14d0d723-e5a8-4475-a951-017a0473d182") }, new Corrugate { Alias = "cor3", Id = new Guid("14d0d723-e5a8-4475-a951-017a0473d183") } };
        //    //corrugateManagerMock.Setup(c => c.GetCorrugates()).Returns(testCorrugates);

        //    //var productionGroupsProvider = new XmlProductionGroupsProvider(Environment.CurrentDirectory + @"\TestData\ProductionGroups.xml");
        //    //var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    //Specify.That(productionGroupManager.ProductionGroups.Count()).Should.BeEqualTo(2);
        //    //Specify.That(productionGroupManager.ProductionGroups.ElementAt(0).ConfiguredCorrugatesIds.Count()).Should.BeEqualTo(2);
        //    //Assert.AreEqual(productionGroupManager.ProductionGroups.ElementAt(0).ConfiguredCorrugatesIds.Count(), productionGroupManager.ProductionGroups.ElementAt(0).ConfiguredCorrugates.Count());
        //    //Assert.AreEqual(testCorrugates[0].Id, productionGroupManager.ProductionGroups.ElementAt(0).ConfiguredCorrugates.ElementAt(0).Id);
        //    //Assert.AreEqual(testCorrugates[1].Id, productionGroupManager.ProductionGroups.ElementAt(0).ConfiguredCorrugates.ElementAt(1).Id);
        //    //Assert.AreEqual(1, productionGroupManager.ProductionGroups.ElementAt(1).ConfiguredCorrugates.Count());
        //    Assert.Inconclusive(UnitTestStatusEnum.ProviderRefactor.ToString());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldUpdateProductionGroup()
        //{
        //    var testCorrugates = new List<Guid> { corrugate1Id, corrugate2Id };

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).ConfiguredMachineGroups.ElementAt(0)).Should.BeEqualTo(machineGroup1Id);
        //    Specify.That(providerMock.Object.GetProductionGroups().Last().SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.ElementAt(0).ConfiguredMachineGroups = new List<Guid> { machineGroup2Id };
        //    updatedGroups.ElementAt(1).ConfiguredCorrugates = testCorrugates;
        //    updatedGroups.Last().SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst;

        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).Machines.ElementAt(0).Id).Should.BeEqualTo(2);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).ProductionItems.ElementAt(0).CartonPropertyGroup).Should.BeEqualTo("A1");
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).ProductionItems.ElementAt(1).CartonPropertyGroup).Should.BeEqualTo("A2");
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).ConfiguredCorrugatesIds.ElementAt(0).Id).Should.BeEqualTo(testCorrugates[0].Id);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).ConfiguredCorrugatesIds.ElementAt(1).Id).Should.BeEqualTo(testCorrugates[1].Id);
        //    Specify.That(providerMock.Object.GetProductionGroups().Last().SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldSetListeningPortToNullWhenUpdateProductionGroupAndSelectionAlgorithmNotBoxLast()
        //{
        //    // Arrange
        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    var productionGroupManager = new ProductionGroupManager(providerMock.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.ElementAt(0).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort
        //            =
        //            1
        //    };
        //    updatedGroups.ElementAt(1).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort = 2
        //    };
        //    updatedGroups.ElementAt(2).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort
        //            =
        //            3
        //    };
        //    updatedGroups.ElementAt(3).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort
        //            =
        //            4
        //    };

        //    // Act
        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    // Assert
        //    Specify.That(providerMock.Object.GetProductionGroups().Any(p => p.SelectionAlgorithm == SelectionAlgorithmTypes.BoxLast)).Should.BeFalse();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort.HasValue).Should.BeFalse();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort.HasValue).Should.BeFalse();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(2).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort.HasValue).Should.BeFalse();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(3).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort.HasValue).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldSetShowTriggerInterfaceWhenUpdateProductionGroupAndSelectionAlgorithmIsBoxLastOrScanToQueue()
        //{
        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.ElementAt(0).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort
        //            =
        //            1
        //    };
        //    updatedGroups.ElementAt(0).SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;
        //    updatedGroups.ElementAt(1).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort
        //            =
        //            2
        //    };
        //    updatedGroups.ElementAt(2).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort
        //            =
        //            3
        //    };
        //    updatedGroups.ElementAt(2).SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate;
        //    updatedGroups.ElementAt(3).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort
        //            =
        //            4
        //    };



        //    updatedGroups.ElementAt(0).SelectionAlgorithmConfiguration.ShowTriggerInterface = true;
        //    updatedGroups.ElementAt(0).SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;
        //    updatedGroups.ElementAt(1).SelectionAlgorithmConfiguration.ShowTriggerInterface = true;
        //    updatedGroups.ElementAt(2).SelectionAlgorithmConfiguration.ShowTriggerInterface = true;
        //    updatedGroups.ElementAt(2).SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;
        //    updatedGroups.ElementAt(3).SelectionAlgorithmConfiguration.ShowTriggerInterface = true;

        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    // Act
        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    // Assert
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithmConfiguration.ShowTriggerInterface).Should.BeEqualTo(true);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithmConfiguration.ShowTriggerInterface).Should.BeEqualTo(false);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(2).SelectionAlgorithmConfiguration.ShowTriggerInterface).Should.BeEqualTo(true);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(3).SelectionAlgorithmConfiguration.ShowTriggerInterface).Should.BeEqualTo(false);
        //}


        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldSetListeningPortWhenUpdateProductionGroupAndSelectionAlgorithmIsBoxLast()
        //{
        //    // Arrange
        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    var updatedGroups = SetUpTestProductionGroups();
        //    updatedGroups.ElementAt(0).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort = 1
        //    };
        //    updatedGroups.ElementAt(0).SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;
        //    updatedGroups.ElementAt(1).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort = 2
        //    };
        //    updatedGroups.ElementAt(2).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort = 3
        //    };
        //    updatedGroups.ElementAt(2).SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast;
        //    updatedGroups.ElementAt(3).SelectionAlgorithmConfiguration.ScannerSettings = new ScannerSettings
        //    {
        //        ListeningPort = 4
        //    };

        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    // Act
        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    // Assert
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort).Should.BeEqualTo(1);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort.HasValue).Should.BeFalse();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(2).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort).Should.BeEqualTo(3);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(3).SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort.HasValue).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldThrowArgumentExceptionWhenUpdateProductionGroupAndSelectionAlgorithmIsBoxLastButPortAssignedToAnotherProductionGroup()
        //{
        //    // Arrange
        //    var listOfProductionGroups = new List<CommonProductionGroups.ProductionGroup>
        //        {
        //            new CommonProductionGroups.ProductionGroup("1") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = null}} },
        //            new CommonProductionGroups.ProductionGroup("2") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = 3000}} }
        //        };
        //    var providerMock = SetUpMockProductionGroupsProvider(listOfProductionGroups);
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    var updatedGroups = new List<CommonProductionGroups.ProductionGroup>
        //        {
        //            new CommonProductionGroups.ProductionGroup("1") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = 3000}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast},
        //            new CommonProductionGroups.ProductionGroup("2") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = 3000}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast }
        //        };
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    // Act
        //    ProductionGroupArgumentException exception = null;
        //    try
        //    {
        //        productionGroupManager.UpdateProductionGroups(updatedGroups);
        //    }
        //    catch (ProductionGroupArgumentException e)
        //    {
        //        exception = e;
        //    }

        //    // Assert
        //    Assert.IsNotNull(exception);
        //    Specify.That(exception.ProductionGroup.SelectionAlgorithmConfiguration.ScannerSettings.ListeningPort).Should.BeEqualTo(3000);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldNotThrowArgumentExceptionWhenUpdateProductionGroupAndTwoNonBoxLastProductionGroupsHaveDuplicatePort()
        //{
        //    // Arrange
        //    var listOfProductionGroups = new List<CommonProductionGroups.ProductionGroup>
        //        {
        //            new CommonProductionGroups.ProductionGroup("1") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = null}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst},
        //            new CommonProductionGroups.ProductionGroup("2") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = null}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst },
        //            new CommonProductionGroups.ProductionGroup("3") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = null}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst }
        //        };
        //    var providerMock = SetUpMockProductionGroupsProvider(listOfProductionGroups);
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    var updatedGroups = new List<CommonProductionGroups.ProductionGroup>
        //        {
        //            new CommonProductionGroups.ProductionGroup("1") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = null}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst},  // duplicate listening port in non-box-last 
        //            new CommonProductionGroups.ProductionGroup("2") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = null}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst }, // algorithms are ignored
        //            new CommonProductionGroups.ProductionGroup("3") { SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {ScannerSettings = new ScannerSettings {ListeningPort = 3000}}, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast }
        //        };
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    // Act
        //    ProductionGroupArgumentException exception = null;
        //    try
        //    {
        //        productionGroupManager.UpdateProductionGroups(updatedGroups);
        //    }
        //    catch (ProductionGroupArgumentException e)
        //    {
        //        exception = e;
        //    }

        //    // Assert
        //    Assert.IsNull(exception);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldSetDefaultAlgorithmSettingsWhenProductionGroupsAreUpdatedIfAlgorithmSettingsAreNull()
        //{
        //    var providerMock = SetUpMockProductionGroupsProvider();

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;
        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);
        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithmConfiguration).Should.Not.BeNull();
        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithmConfiguration.PauseMachineBetweenOrders).Should.BeFalse();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithmConfiguration).Should.Not.BeNull();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithmConfiguration.PauseMachineBetweenOrders).Should.BeTrue();

        //    var updatedGroups = SetUpTestProductionGroups();

        //    Specify.That(updatedGroups.First().SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);
        //    // Specify.That(updatedGroups.First().SelectionAlgorithmConfiguration).Should.BeNull();
        //    Specify.That(updatedGroups.ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);
        //    Specify.That(updatedGroups.ElementAt(1).SelectionAlgorithmConfiguration).Should.Not.BeNull();
        //    Specify.That(updatedGroups.ElementAt(1).SelectionAlgorithmConfiguration.PauseMachineBetweenOrders).Should.BeTrue();


        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);
        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithmConfiguration).Should.Not.BeNull();
        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithmConfiguration.PauseMachineBetweenOrders).Should.BeFalse();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithmConfiguration).Should.Not.BeNull();
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithmConfiguration.PauseMachineBetweenOrders).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesNotAllowChangeToSelectionAlgorithmWhenCartonsInQueue()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.First().SelectionAlgorithm = SelectionAlgorithmTypes.Order;

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesNotAllowChangeToCPGWhenCartonsInQueueForOrders()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);


        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "testCPG"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = new List<CommonProductionGroups.ProductionGroup>
        //                     {
        //                         new CommonProductionGroups.ProductionGroup("1")
        //                         {
        //                             SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
        //                             ConfiguredCorrugates = new List<Corrugate> { new Corrugate { Id = corrugate1Id } },
        //                             Machines = new List<MachineInformation> (),
        //                             ProductionItems = new List<ProductionItem>()
        //                         },
        //                         new CommonProductionGroups.ProductionGroup("2")
        //                         {
        //                             SelectionAlgorithm = SelectionAlgorithmTypes.Order,
        //                             ConfiguredCorrugates = new List<Corrugate> { new Corrugate { Id = corrugate2Id } },
        //                             Machines = new List<MachineInformation> (),
        //                             ProductionItems = new List<ProductionItem>()
        //                         }
        //                     };
        //    var updatedGroups = new List<CommonProductionGroups.ProductionGroup>
        //                     {
        //                         new CommonProductionGroups.ProductionGroup("1")
        //                         {
        //                             SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
        //                             ConfiguredCorrugates = new List<Corrugate> { new Corrugate { Id = corrugate1Id } },
        //                             Machines = new List<MachineInformation> (),
        //                             ProductionItems = new List<ProductionItem>()
        //                         },
        //                         new CommonProductionGroups.ProductionGroup("2")
        //                         {
        //                             SelectionAlgorithm = SelectionAlgorithmTypes.Order,
        //                             ConfiguredCorrugates = new List<Corrugate> { new Corrugate { Id = corrugate2Id } },
        //                             Machines = new List<MachineInformation> (),
        //                             ProductionItems = new List<ProductionItem>{new ProductionItem("testCPG", 0.0, 0.0)}
        //                         }
        //                     };

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.Last(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldRaiseFailedProductionGroupUpdateForGroup()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "3"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).Machines.ElementAt(0).Id).Should.BeEqualTo(1);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).ProductionItems.ElementAt(0).CartonPropertyGroup).Should.BeEqualTo("G4");
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).ProductionItems.ElementAt(1).CartonPropertyGroup).Should.BeEqualTo("G5");
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(2).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);

        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.ElementAt(0).Machines = new List<MachineInformation> { new MachineInformation(2) };
        //    updatedGroups.ElementAt(1).ProductionItems = new[] { new ProductionItem("A1", 1), new ProductionItem("A2", 1) };
        //    updatedGroups.ElementAt(1).ConfiguredCorrugates = testCorrugates;
        //    updatedGroups.ElementAt(2).SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst;

        //    var successfulPGUpdates = 0;
        //    CommonProductionGroups.ProductionGroup changedGroup = null;
        //    eventAggregator.GetEvent<Message<CommonProductionGroups.ProductionGroup>>()
        //        .Where(m => m.MessageType == ProductionGroupMessages.ProductionGroupUpdated)
        //        .Subscribe(m =>
        //            {
        //                if (m.Data.LastUpdateStatus == ProductionGroupChangedStatus.JobsExistCannotUpdateSelectionAlgorithm)
        //                    changedGroup = m.Data;
        //                else
        //                    successfulPGUpdates++;
        //            });

        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    Specify.That(successfulPGUpdates).Should.BeEqualTo(3, "Two of the three PG's should have been successfully updated.");
        //    Specify.That(changedGroup).Should.Not.BeNull("The pg with alias '4' should have raised the failure event.");
        //    Specify.That(changedGroup.Alias).Should.BeSameAs("3");
        //    Specify.That(changedGroup.LastUpdateStatus).Should.BeEqualTo(ProductionGroupChangedStatus.JobsExistCannotUpdateSelectionAlgorithm);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesAllowChangeOfMachinesWhenCartonsInQueue()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.First().Machines = new List<MachineInformation>();

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesAllowChangeOfCorrugatesWhenCartonsInQueue()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.First().ConfiguredCorrugates = new List<Corrugate>();

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesAllowCartonPropertyGroupWithCartonsInQueueToBeAssignedToAWaveGroupIfNotAssignedBefore()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    var unassigned = new ProductionItem("New", 1);

        //    updatedGroups.First().ProductionItems = new List<ProductionItem>(updatedGroups.First().ProductionItems) { unassigned };

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesAllowCartonPropertyGroupWithCartonsInQueueToBeRemovedFromAWaveGroupAndNotAssignedToOtherGroup()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.First().ProductionItems = new List<ProductionItem>(updatedGroups.First().ProductionItems.Take(2));

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesAllowCartonPropertyGroupWithCartonsInQueueToBeAssignedToAOrderGroupIfNotAssignedBefore()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "2"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.ElementAt(1).ProductionItems = new List<ProductionItem>(updatedGroups.ElementAt(1).ProductionItems) { new ProductionItem("New", 1) };

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.ElementAt(1), updatedGroups.ElementAt(1), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesNotAllowCartonPropertyGroupWithCartonsInQueueToBeRemovedFromAOrderGroupAndNotAssignedToOtherGroup()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "2"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.ElementAt(1).ProductionItems = new List<ProductionItem>(updatedGroups.ElementAt(1).ProductionItems.Take(1));

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.ElementAt(1), updatedGroups.ElementAt(1), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesAllowCartonPropertyGroupWithCartonsInQueueToBeMovedFromWaveGroupToOtherWaveGroup()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "4"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    var groupToMove = updatedGroups.First().ProductionItems.ElementAt(0);
        //    updatedGroups.First().ProductionItems = new List<ProductionItem>(updatedGroups.First().ProductionItems.Skip(1));
        //    updatedGroups.Last().ProductionItems = new List<ProductionItem>(updatedGroups.Last().ProductionItems) { groupToMove };

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesNotAllowCartonPropertyGroupWithCartonsInQueueToBeMovedFromWaveGroupToOrdersGroup()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    var groupToMove = updatedGroups.First().ProductionItems.ElementAt(0);
        //    updatedGroups.First().ProductionItems = new List<ProductionItem>(updatedGroups.First().ProductionItems.Skip(1));
        //    updatedGroups.ElementAt(1).ProductionItems = new List<ProductionItem>(updatedGroups.ElementAt(1).ProductionItems) { groupToMove };

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();

        //    result = productionGroupManager.CanChangeProductionGroup(origGroups.ElementAt(1), updatedGroups.ElementAt(1), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesNotAllowCartonPropertyGroupWithCartonsInQueueToBeMovedFromOrdersGroupToWaveGroup()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton> { new Carton() });
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "2"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().First().SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    var groupToMove = updatedGroups.ElementAt(1).ProductionItems.ElementAt(0);
        //    updatedGroups.ElementAt(1).ProductionItems = new List<ProductionItem>(updatedGroups.ElementAt(1).ProductionItems.Skip(1));
        //    updatedGroups.First().ProductionItems = new List<ProductionItem>(updatedGroups.First().ProductionItems) { groupToMove };

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.ElementAt(1), updatedGroups.ElementAt(1), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();

        //    result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesNotAllowCartonPropertyGroupWithCartonsInQueueToBeMovedFromOrdersGroupToOtherOrdersGroup()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "2"))).Returns(new List<ICarton> { new Carton() });
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "3"))).Returns(new List<ICarton> { new Carton() });
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(1).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);
        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(2).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.Order);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    var groupToMove = updatedGroups.ElementAt(1).ProductionItems.ElementAt(0);
        //    updatedGroups.ElementAt(1).ProductionItems = new List<ProductionItem>(updatedGroups.ElementAt(1).ProductionItems.Skip(1));
        //    updatedGroups.ElementAt(2).ProductionItems = new List<ProductionItem>(updatedGroups.ElementAt(2).ProductionItems) { groupToMove };

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.ElementAt(1), updatedGroups.ElementAt(1), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();

        //    result = productionGroupManager.CanChangeProductionGroup(origGroups.ElementAt(2), updatedGroups.ElementAt(2), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeFalse();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void CanChangeProductionGroup_DoesAllowChangeToSelectionAlgorithmWhenNoCartonsInQueue()
        //{
        //    var testCorrugates = new List<Corrugate> { new Corrugate { Alias = "cor1", Id = corrugate1Id }, new Corrugate { Alias = "cor1", Id = corrugate2Id } };

        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    corrugateManagerMock.Setup(m => m.GetCorrugates()).Returns(testCorrugates);

        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.Is<SearchCriteria>(s => s.SearchValue == "1"))).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    Specify.That(providerMock.Object.GetProductionGroups().ElementAt(0).SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.BoxFirst);

        //    var origGroups = SetUpTestProductionGroups();
        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.First().SelectionAlgorithm = SelectionAlgorithmTypes.Order;

        //    var result = productionGroupManager.CanChangeProductionGroup(origGroups.First(), updatedGroups.First(), origGroups, updatedGroups);
        //    Specify.That(result).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldSaveProductionGroupsOnPropertyChanged()
        //{
        //    var providerMock = SetUpMockProductionGroupsProvider();
        //    var productionGroupManager = new ProductionGroupManager(providerMock.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;
        //    var updatedGroups = SetUpTestProductionGroups();
        //    updatedGroups.ElementAt(0).Machines = new List<MachineInformation> { new MachineInformation(2) };

        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    providerMock.Verify(mock => mock.Upsert(It.IsAny<PackNet.Common.Interfaces.DTO.ProductionGroups.ProductionGroup>()));
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void MachineChangesToProductionGroupsShouldTriggerEvent()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    var updatedGroups = SetUpTestProductionGroups();
        //    updatedGroups.ElementAt(0).Machines = new List<MachineInformation> { new MachineInformation(2) };
        //    updatedGroups.ElementAt(0).ProductionItems.ElementAt(0).Modifier = 2.0;

        //    eventAggregator.GetEvent<Message<CommonProductionGroups.ProductionGroup>>()
        //        .Where(m => m.MessageType == ProductionGroupMessages.ProductionGroupUpdated)
        //        .Subscribe(m => OnProductionGroupChanged(m.Data));

        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    Specify.That(productionGroupThatChanged).Should.Not.BeNull();
        //    Specify.That(productionGroupThatChangedStatus)
        //        .Should.BeEqualTo(ProductionGroupChangedStatus.Success);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ModifierLevelChangesFromZeroToProductionGroupShouldTriggerEvent()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    cartonRequestManagerMock.Setup(c => c.GetCartonRequestBySearchCriteria(It.IsAny<SearchCriteria>())).Returns(new List<ICarton>());
        //    productionGroupManager.CartonRequestManager = cartonRequestManagerMock.Object;

        //    var updatedGroups = SetUpTestProductionGroups();

        //    updatedGroups.ElementAt(0).ProductionItems.ElementAt(0).Modifier = 2.0;

        //    productionGroupManager.ProductionGroups.ElementAt(0).ProductionItems.ElementAt(0).Modifier = 0;

        //    eventAggregator.GetEvent<Message<CommonProductionGroups.ProductionGroup>>()
        //        .Where(m => m.MessageType == ProductionGroupMessages.ProductionGroupUpdated)
        //        .Subscribe(m => OnProductionGroupChanged(m.Data));

        //    productionGroupManager.UpdateProductionGroups(updatedGroups);

        //    Specify.That(productionGroupThatChanged).Should.Not.BeNull();
        //    Specify.That(productionGroupThatChangedStatus)
        //        .Should.BeEqualTo(ProductionGroupChangedStatus.Success);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldReturnTheProductionGroupToWhichAGivenMachineIsAssigned()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    var productionGroupForMachine = productionGroupManager.GetProductionGroupForMachine(1);
        //    Specify.That(productionGroupForMachine.Alias).Should.BeEqualTo("1");
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldReturnNullIfMachineIsNotAssignedToAProductionGroup()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    var productionGroupForMachine = productionGroupManager.GetProductionGroupForMachine(3);
        //    Specify.That(productionGroupForMachine).Should.BeNull();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldReturnTheProductionGroupsThatCanProduceSpecifiedCartonPropertyGroup()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    var groups = productionGroupManager.GetProductionGroupWithCartonPropertyGroup("G1");
        //    Specify.That(groups.Count()).Should.BeEqualTo(1);
        //    groups = productionGroupManager.GetProductionGroupWithCartonPropertyGroup("G2");
        //    Specify.That(groups.Count()).Should.BeEqualTo(1);
        //    groups = productionGroupManager.GetProductionGroupWithCartonPropertyGroup("G3");
        //    Specify.That(groups.Count()).Should.BeEqualTo(1);
        //    groups = productionGroupManager.GetProductionGroupWithCartonPropertyGroup("Non");
        //    Specify.That(groups.Count()).Should.BeEqualTo(0);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldReturnTheProductionGroupThatIsAssignedDirectlyToTheCarton()
        //{
        //    var productionGroupAlias = "test";
        //    var carton = new Carton { ProductionGroupAlias = productionGroupAlias };
        //    testProductionGroups.Add(new CommonProductionGroups.ProductionGroup(productionGroupAlias));
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    var group = productionGroupManager.GetProductionGroupForCarton(carton);
        //    Specify.That(group).Should.Not.BeNull();
        //    Specify.That(group.Alias).Should.BeEqualTo(productionGroupAlias);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldReturnTheProductionGroupFromCPGAssignedToTheCarton()
        //{
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);
        //    var carton = new Carton { CartonPropertyGroupAlias = "G1" };

        //    var group = productionGroupManager.GetProductionGroupForCarton(carton);
        //    Specify.That(group).Should.Not.BeNull();
        //    Specify.That(group.Alias).Should.BeEqualTo("1");
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldRemoveAllMachineFromAllProductionGroupsIfDuplicateIsFound()
        //{
        //    //var testPGs = new List<CommonProductionGroups.ProductionGroup>
        //    //{
        //    //    new CommonProductionGroups.ProductionGroup("test1"){ Machines = new List<MachineInformation>{new MachineInformation(1), new MachineInformation(2)} }, 
        //    //    new CommonProductionGroups.ProductionGroup("test2"){ Machines = new List<MachineInformation>{new MachineInformation(3), new MachineInformation(2)} }
        //    //};
        //    //var mockProductionGroupsProvider = SetUpMockProductionGroupsProvider(testPGs);

        //    //var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    //Specify.That(productionGroupManager.ProductionGroups.FirstOrDefault(pg => pg.Machines.Any(m => m.Id == 1))).Should.Not.BeNull();
        //    //Specify.That(productionGroupManager.ProductionGroups.FirstOrDefault(pg => pg.Machines.Any(m => m.Id == 3))).Should.Not.BeNull();
        //    //Specify.That(productionGroupManager.ProductionGroups.FirstOrDefault(pg => pg.Machines.Any(m => m.Id == 2))).Should.BeNull();
        //    Assert.Inconclusive(UnitTestStatusEnum.FunctionalityResearchNeeded.ToString());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldRemoveAllCPGFromAllProductionGroupsIfDuplicateIsFound()
        //{
        //    //var testPGs = new List<CommonProductionGroups.ProductionGroup>
        //    //{
        //    //    new CommonProductionGroups.ProductionGroup("test1"){ ProductionItems = new List<ProductionItem> { new ProductionItem("test1", 0.0), new ProductionItem("test2", 0.0)}}, 
        //    //    new CommonProductionGroups.ProductionGroup("test2"){ ProductionItems = new List<ProductionItem> { new ProductionItem("test2", 0.0), new ProductionItem("test3", 0.0)}}
        //    //};
        //    //var mockProductionGroupsProvider = SetUpMockProductionGroupsProvider(testPGs);

        //    //var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    //Specify.That(productionGroupManager.ProductionGroups.FirstOrDefault(pg => pg.ProductionItems.Any(pi => pi.CartonPropertyGroup == "test1"))).Should.Not.BeNull();
        //    //Specify.That(productionGroupManager.ProductionGroups.FirstOrDefault(pg => pg.ProductionItems.Any(pi => pi.CartonPropertyGroup == "test3"))).Should.Not.BeNull();
        //    //Specify.That(productionGroupManager.ProductionGroups.FirstOrDefault(pg => pg.ProductionItems.Any(pi => pi.CartonPropertyGroup == "test2"))).Should.BeNull();
        //    Assert.Inconclusive(UnitTestStatusEnum.FunctionalityResearchNeeded.ToString());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldPublishAllProductionGroups_WhenPublsihConfigurations()
        //{
        //    // Arrange
        //    var productionGroupManager = new ProductionGroupManager(productionGroup.Object, corrugateManagerMock.Object, logger, eventAggregator);

        //    var publishedProductionGroups = new List<CommonProductionGroups.ProductionGroup>();

        //    eventAggregator.GetEvent<IMessage<CommonProductionGroups.ProductionGroup>>()
        //        .Where(x => x.MessageType == ProductionGroupMessages.ProductionGroupConfiguration)
        //        .Subscribe(x => publishedProductionGroups.Add(x.Data));

        //    // Act
        //    productionGroupManager.PublishConfiguration();

        //    Retry.For(() => publishedProductionGroups.Count == 4, TimeSpan.FromSeconds(2));

        //    // Assert
        //    Specify.That(publishedProductionGroups.Count).Should.BeEqualTo(4);
        //    Specify.That(publishedProductionGroups.Count(p => p.Alias == "1")).Should.BeEqualTo(1);
        //    Specify.That(publishedProductionGroups.Count(p => p.Alias == "2")).Should.BeEqualTo(1);
        //    Specify.That(publishedProductionGroups.Count(p => p.Alias == "3")).Should.BeEqualTo(1);
        //    Specify.That(publishedProductionGroups.Count(p => p.Alias == "4")).Should.BeEqualTo(1);
        //}

        //private Mock<ProductionGroup> SetUpMockProductionGroupsProvider(List<CommonProductionGroups.ProductionGroup> productionGroups = null)
        //{
        //    var mock = new Mock<ProductionGroup>();

        //    if (productionGroups != null)
        //        mock.Setup(provider => provider.GetProductionGroups()).Returns(productionGroups);
        //    else
        //        mock.Setup(provider => provider.GetProductionGroups()).Returns(testProductionGroups);
        //    return mock;
        //}

        //private void OnProductionGroupChanged(CommonProductionGroups.ProductionGroup productionGroup)
        //{
        //    productionGroupThatChanged = productionGroup;
        //}

        //private List<CommonProductionGroups.ProductionGroup> SetUpTestProductionGroups()
        //{
        //    var listOfProductionGroups = new List<CommonProductionGroups.ProductionGroup>
        //        {
        //            new CommonProductionGroups.ProductionGroup
        //                {
        //                    Alias = "1",
        //                    ConfiguredCorrugates = new List<Guid> { corrugate2Id },
        //                    ConfiguredMachineGroups = new List<Guid> { machineGroup1Id },
        //                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration(),
        //                },
        //            new CommonProductionGroups.ProductionGroup
        //                {
        //                    Alias = "2",
        //                    SelectionAlgorithm = SelectionAlgorithmTypes.Order,
        //                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {PauseMachineBetweenOrders = true}
        //                },
        //            new CommonProductionGroups.ProductionGroup
        //                {
        //                    Alias = "3",
        //                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration(),
        //                    SelectionAlgorithm = SelectionAlgorithmTypes.Order
        //                },
        //            new CommonProductionGroups.ProductionGroup
        //                {
        //                    Alias = "4",
        //                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration(),
        //                    SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst
        //                },
        //        };

        //    return listOfProductionGroups;
        //}

        //private void SetUpLogging()
        //{
        //    logger = new Mock<ILogger>(MockBehavior.Loose).Object;
        //}
    }
}