﻿namespace BusinessTests.CartonRequests
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Common.Interfaces.DTO.Carton;
    using Testing.Specificity;

    [TestClass]
    public class CartonRequestTests
    {
        private Guid machine1Id = Guid.NewGuid();

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalseWhenComparingToAnObjectThatIsNotAnEntity()
        {
            var carton = new Carton();
            var notACarton = new object();

            Specify.That(carton.Equals(notACarton)).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalseWhenComparingToAnEntityThatIsNull()
        {
            var carton = new Carton();

            Specify.That(carton.Equals(null)).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetHeightToZeroIfSetWithNegValue()
        {
            var cartonRequest = new Carton { Height = -5 };
            Specify.That(cartonRequest.Height).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetLengthToZeroIfSetWithNegValue()
        {
            var cartonRequest = new Carton { Length = -5 };
            Specify.That(cartonRequest.Length).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetWidthToZeroIfSetWithNegValue()
        {
            var cartonRequest = new Carton { Width = -6 };
            Specify.That(cartonRequest.Width).Should.BeEqualTo(0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateIdToDatabaseId()
        {
            // Arrange
            var request = new Carton();
            var originalId = Guid.NewGuid();

            // Act
            request.UpdateId(originalId);

            // Assert
            Specify.That(request.Id).Should.BeEqualTo(originalId);
        }

        [TestMethod]
        public void Should_DefaultToEmptyXValParameterList_IfNotSet()
        {
            var request = new Carton();
            Specify.That(request.XValues.Keys).Should.BeEmpty();
        }

        [TestMethod]
        public void Should_BeAbleTo_AssignXValues()
        {
            var request = new Carton();
            Specify.That(request.XValues.Keys).Should.BeEmpty();
            
            request.XValues.Add("X1", 5.0);
            Specify.That(request.XValues.Keys.Count(k => k.Equals("X1"))).Should.BeEqualTo(1);
            Specify.That(request.XValues["X1"]).Should.BeLogicallyEqualTo(5.0);
        }
    }
}