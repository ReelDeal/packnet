﻿namespace BusinessTests.CartonRequests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.Enums;

    using Testing.Specificity;

    [TestClass]
    public class ProductionStatusTests
    {
        private ProductionStatus productionStatus;

        [TestInitialize]
        public void TestInitialize()
        {
            productionStatus = new ProductionStatus();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHaveInitialStatusOfQueuedWhenConstructed()
        {
            Specify.That(productionStatus.Status).Should.BeEqualTo(ProductionStatusEnum.Queued);
        }
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetStatusWhenConstructed()
        {
            productionStatus = new ProductionStatus(ProductionStatusEnum.Completed);
            Specify.That(productionStatus.Status).Should.BeEqualTo(ProductionStatusEnum.Completed);
        }
    }   
}
