﻿namespace BusinessTests
{
    public enum UnitTestStatusEnum
    {
        ProviderRefactor = 0,
        FunctionalityResearchNeeded = 1
    }
}