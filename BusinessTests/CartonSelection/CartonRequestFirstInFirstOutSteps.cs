﻿using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Data.WMS;

namespace BusinessTests.CartonSelection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Moq;

    using PackNet.Business.Carton.TODO;
    using PackNet.Business.CartonSelection;
    using PackNet.Business.Classifications;
    using PackNet.Business.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;
    using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class CartonRequestFirstInFirstOutSteps
    {
        private Guid c1Id = Guid.NewGuid();
        private Guid c2Id = Guid.NewGuid();

        #region Fields
        private OrdersQueueRequestSelector selector;
        private Mock<ICartonRequestManager> cartonRequestManager;
        private IPacksizeCutCreaseMachine machine;
        private IList<IJobOrder> jobOrderList = new List<IJobOrder>();
        private Dictionary<int, List<IJobOrder>> ordersInMachineQueue = new Dictionary<int, List<IJobOrder>>();
        private IList<ICarton> validRequestNotSentToMachine = new List<ICarton>();
        private ICartonRequestOrder job;
        private Corrugate c1;
        private ProductionGroup productionGroup;
        private Guid machine2Id = Guid.NewGuid();
        private Guid machine1Id = Guid.NewGuid();

        #endregion

        [BeforeScenario("ManualOrderQueue")]
        public void BeforeScenario()
        {
            ordersInMachineQueue = new Dictionary<int, List<IJobOrder>>();
            var jobOrderManagerMock = new Mock<IJobOrdersManager>();
            jobOrderManagerMock.Setup(m => m.ProducibleOpenOrders).Returns(jobOrderList);
            jobOrderManagerMock.Setup(
                m => m.ShouldDispatchCartonForMachineAndOrder(It.IsAny<Guid>(), It.IsAny<IJobOrder>()))
                .Returns<int, IJobOrder>(
                    (machineId, jobOrder) =>
                    {
                        if (ordersInMachineQueue.ContainsKey(machineId) && productionGroup.SelectionAlgorithmConfiguration.PauseMachineBetweenOrders)
                        {
                            return ordersInMachineQueue[machineId].Any()
                                ? DispatchCartonCommand.Starve
                                : DispatchCartonCommand.Send;
                        }
                        return DispatchCartonCommand.Send;
                    })
                .Callback<int, IJobOrder>((machineId, jobOrder) =>
                {
                    if (ordersInMachineQueue.ContainsKey(machineId))
                    {
                        ordersInMachineQueue[machineId].Add(jobOrder);
                    }
                    else
                    {
                        ordersInMachineQueue.Add(machineId, new List<IJobOrder> {jobOrder});
                    }
                });
            //jobOrderManagerMock.Setup(m => m.MachineCurrentOrders).Returns(new ConcurrentDictionary<int, List<string>>());
            cartonRequestManager = new Mock<ICartonRequestManager>();
            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(validRequestNotSentToMachine);

            cartonRequestManager.Setup(m => m.CreateCartonRequestOrder(
                It.IsAny<IEnumerable<ICarton>>(),
                It.IsAny<CartonOnCorrugate>(),
                It.IsAny<IPacksizeCutCreaseMachine>(),
                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
                It.IsAny<bool>(),
                false,
                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
                It.IsAny<int?>(),
                It.IsAny<bool>()))
                .Returns<
                IEnumerable<ICarton>,
                Corrugate,
                IPacksizeCutCreaseMachine,
                PackNet.Common.Interfaces.DTO.Classifications.Classification,
                bool,
                bool,
                Func<DesignQueryParam, IPackagingDesign>,
                int?,
                bool>((a, b, c, e, f, g, h, i, j) => new CartonRequestOrder { Requests = a });

            selector = new MachineSpecificRequestSelector(
                jobOrderManagerMock.Object,
                cartonRequestManager.Object,
                new Mock<IClassificationsManager>().Object,
                new Mock<IPackagingDesignManager>().Object,
                new WmsConfiguration{ExternalPrint = false}, 
                new Mock<ILogger>(MockBehavior.Loose).Object);
        }

        [AfterScenario("ManualOrderQueue")]
        public void AfterScenario()
        {
        }

        [Given(@"a corrugate c(.*)")]
        public void GivenACorrugateC(int p0)
        {
            c1 = new Corrugate { Id = c1Id, Quality = 1 };
        }

        [Given(@"a machine loaded with c1")]
        public void GivenAMachineLoadedWithC1()
        {
            machine = new FusionMachine() { Tracks = new List<Track>
                {
                    new Track { TrackNumber = 1, LoadedCorrugate = c1 }
                }
            };
        }

        [Given(@"order1 optimal on corrugate c1 assigned to another machine")]
        public void GivenOrder1OptimalOnCorrugateC1AssignedToAnotherMachine()
        {
            var cr = new Carton { MachineId = Guid.NewGuid(), Custom = true, OrderId = "1" };
            validRequestNotSentToMachine.Add(cr);
            jobOrderList.Add(new JobOrder(cr) { OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = c1, TileCount = 1 } });
        }

        [Given(@"order(.*) with a quantity (.*) optimal tiled on corrugate c1 not optimal on my machine assigned to my machine")]
        public void GivenOrderWithAQuantityOptimalTiledOnCorrugateCNotOptimalOnMyMachineAssignedToMyMachine(string orderNumber, int quantity)
        {
            ICarton cr = null;
            for (int i = 0; i < quantity; i++)
            {
                cr = new Carton { MachineId = machine.Id, Custom = true, OrderId = orderNumber, Quantity = quantity };
                validRequestNotSentToMachine.Add(cr);
            }

            jobOrderList.Add(new JobOrder(cr)
            {
                OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = c1, TileCount = 2, ProducibleMachines = new Dictionary<Guid, string> { {machine2Id, "two"} } },
                ProductionGroup = productionGroup
            });
        }

        [Given(@"order(.*) with a quantity (.*) optimal tiled on corrugate c1 optimal on my machine assigned to my machine")]
        public void GivenOrderWithAQuantityOptimalTiledOnCorrugateC1OptimalOnMyMachineAssignedToMyMachine(string orderNumber, int quantity)
        {
            ICarton cr = null;
            for (int i = 0; i < quantity; i++)
            {
                cr = new Carton { MachineId = machine.Id, Custom = true, OrderId = orderNumber, Quantity = quantity };
                validRequestNotSentToMachine.Add(cr);
            }

            jobOrderList.Add(new JobOrder(cr)
            {
                OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = c1, TileCount = 2, ProducibleMachines = new Dictionary<Guid, string> { {machine1Id, "one"} }},
                ProductionGroup = productionGroup
            });
        }

        [Given(@"order(.*) with a quantity (.*) optimal tiled on corrugate c2 optimal on my machine assigned to my machine")]
        public void GivenOrderWithAQuantityOptimalTiledOnCorrugateC2OptimalOnMyMachineAssignedToMyMachine(string orderNumber, int quantity)
        {
            ICarton cr = null;
            for (int i = 0; i < quantity; i++)
            {
                cr = new Carton { MachineId = machine.Id, Custom = true, OrderId = orderNumber, Quantity = quantity };
                validRequestNotSentToMachine.Add(cr);
            }

            jobOrderList.Add(new JobOrder(cr)
            {
                OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = new Corrugate { Id = c2Id, Quality = 2 }, TileCount = 2, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } } },
                ProductionGroup = productionGroup
            });
        }

        [Given(@"order(.*) with a quantity (.*) optimal tiled on corrugate c2 assigned to my machine")]
        public void GivenOrderWithAQuantityOptimalTiledOnCorrugateC2AssignedToMyMachine(string orderNumber, int quantity)
        {
            ICarton cr = null;
            for (int i = 0; i < quantity; i++)
            {
                cr = new Carton { MachineId = machine.Id, Custom = true, OrderId = orderNumber, Quantity = quantity };
                validRequestNotSentToMachine.Add(cr);
            }

            jobOrderList.Add(new JobOrder(cr)
            {
                OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = new Corrugate { Id = c2Id, Quality = 2 }, TileCount = 2 },
                ProductionGroup = productionGroup
            });
        }

        [Given(@"order2 is not picked up by any other machine")]
        public void GivenOrder2IsNotPickedUpByAnyOtherMachine()
        {
            cartonRequestManager.Setup(
                m => m.RequestsOkToSendToMachineAndRegister(It.Is<IEnumerable<ICarton>>(cr => cr.ElementAt(0).OrderId == "2"), It.IsAny<Guid?>()))
                .Callback<IEnumerable<ICarton>, int?>((requests, machineId) =>
                {
                    foreach (ICarton cr in requests)
                    {
                        validRequestNotSentToMachine.Remove(cr);
                    }
                })
                .Returns(true);
        }

        [Given(@"order1 is picked up by another machine")]
        public void GivenOrder1IsPickedUpByAnotherMachine()
        {
            cartonRequestManager.Setup(
                m =>
                    m.RequestsOkToSendToMachineAndRegister(
                        It.Is<IEnumerable<ICarton>>(cr => cr.ElementAt(0).OrderId == "1"), It.IsAny<Guid?>()))
                .Returns(false)
                .Callback(() => ordersInMachineQueue[1].Clear());
        }

        [Given(@"no order is picked up by any other machine")]
        public void GivenNoOrderIsNotPickedUpByAnyOtherMachine()
        {
            cartonRequestManager.Setup(
                m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
                .Callback<IEnumerable<ICarton>, int?>((requests, machineId) =>
                {
                    foreach (ICarton cr in requests)
                    {
                        validRequestNotSentToMachine.Remove(cr);
                    }
                })
                .Returns(true);
        }

        [Given(@"a production group setup with pause between orders (.*)")]
        public void GivenAProductionGroupSetupWithPauseBetweenOrders(bool pauseBetween)
        {
            productionGroup = new ProductionGroup {
                SelectionAlgorithmConfiguration =
                    new SelectionAlgorithmConfiguration { PauseMachineBetweenOrders = pauseBetween }
            };
        }

        [When(@"next job is requested")]
        public void WhenNextJobIsRequested()
        {
            job = selector.GetNextJobForMachine(machine);
        }

        [Then(@"order2 is produced")]
        public void ThenOrder2IsProduced()
        {
            Specify.That(job.Requests.ElementAt(0).OrderId).Should.BeEqualTo("2");
        }

        [Then(@"order(.*) is produced without tiles")]
        public void ThenOrderIsProducedWithoutTiles(string orderNumber)
        {
            Specify.That(job.Requests.Count()).Should.BeEqualTo(1);
            Specify.That(job.Requests.ElementAt(0).OrderId).Should.BeEqualTo(orderNumber);
        }

        [Then(@"order(.*) is produced with tiles")]
        public void ThenOrderIsProducedWithTiles(string orderNumber)
        {
            Specify.That(job.Requests.Count()).Should.BeEqualTo(2);
            Specify.That(job.Requests.ElementAt(0).OrderId).Should.BeEqualTo(orderNumber);
        }

        [Then(@"no order is produced")]
        public void ThenNoOrderIsProduced()
        {
            Specify.That(job).Should.BeNull();
        }
    }
}
