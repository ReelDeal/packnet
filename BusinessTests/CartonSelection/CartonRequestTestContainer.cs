namespace BusinessTests.CartonSelection
{
    using System;

    public class CartonRequestTestContainer
    {
        public string SerialNumber { get; set; }

        public int CartonPropertyGroup { get; set; }

        public int Classification { get; set; }

        public int AreaOn { get; set; }

        public int TotalLengthOn { get; set; }

        public int TotalWidthOn { get; set; }

        public DateTime CreatedTime { get; set; }

        public int? CorrugateQuality { get; set; }
    }
}