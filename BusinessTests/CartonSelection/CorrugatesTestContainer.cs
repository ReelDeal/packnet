namespace BusinessTests.CartonSelection
{
    using PackNet.Common.Interfaces.DTO.Corrugates;

    public class CorrugatesTestContainer
    {
        public string Alias { get; set; }

        public double Width { get; set; }

        public int Quality { get; set; }

        public Corrugate CreateFanfold()
        {
            return new Corrugate{ Alias = Alias, Width = Width, Quality = Quality };
        }
    }
}