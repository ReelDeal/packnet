﻿//using PackNet.Business.PickZone;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.Orders;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Enums;
//    using PackNet.Common.Interfaces.Services;

//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;
//    using Testing.Specificity;

    [TestClass]
    public class SelectorFilterTests
    {

#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
        //        private WaveCartonRequestSelector selector;
//        private Mock<ICartonRequestManager> requestManager;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IMachinesManager> machinesManager;
//        private Mock<IPickZoneManager> pickAreaManager;
//        private Mock<IProductionGroupManager> productionGroupManager;
//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
//        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;
//        private Mock<IPackagingDesignManager> designManagerMock;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private Guid machine1Id = Guid.NewGuid();
//        private Guid machine2Id = Guid.NewGuid();
//        private Guid machine3Id = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            SetUpMachinesManagerMock();
//            SetUpRequestManagerMock();
//            SetUpClassificationsManagerMock();
//            SetUpPickAreaManagerMock();
//            SetUpProductionGroupManagerMock();
//            SetUpCartonPropertyGroupManagerMock();
//            SetupLongHeadPositioningValidationMock();
//            SetupDesignManagerMock();

//            SetupCorrugatesCalculationServiceMock();

//            selector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                requestManager.Object,
//                classificationsManager.Object,
//                pickAreaManager.Object,
//                machinesManager.Object,
//                productionGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManagerMock.Object,
//                new SelectorSettings(2, false, 40, 40, 0),
//                longHeadPositioningValidation.Object,
//                new WmsConfiguration(), 
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//        }

//        private void SetupCorrugatesCalculationServiceMock()
//        {
//            mockCorrugateCalculationService = new Mock<ICorrugateService>();

//            mockCorrugateCalculationService.Setup(
//               m =>
//                   m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                       It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//               .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//               ((a, b, c, d, e) =>
//               {
//                   return a.Width != null && a.Width.Value <= b.Width && a.CorrugateQuality == b.Quality;
//               });

//            mockCorrugateCalculationService.Setup(
//            m =>
//                m.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                    It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                    It.IsAny<bool>()))
//            .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//            ((a, b, c, d, e) => a.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Length);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null ? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width,
//                    Design = designManagerMock.Object.GetDesigns().First()
//                });

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetCartonRequestsWithoutStoppedStatus()
//        {
//            var result = selector.GetCartonRequestsToPickZonesWithoutStop(requestManager.Object.ValidRequestsInActivePickzones);
//            Specify.That(result.Count()).Should.BeEqualTo(4);
//            Specify.That(result.ElementAt(0).PickZone).Should.BeEqualTo("A1");
//            Specify.That(result.ElementAt(1).PickZone).Should.BeEqualTo("A2");
//            Specify.That(result.ElementAt(2).PickZone).Should.BeEqualTo("B1");
//            Specify.That(result.ElementAt(3).PickZone).Should.BeEqualTo("B1");

//            pickAreaManager.Object.PickZones.ElementAt(0).Status = ZoneStatus.Stop;
//            pickAreaManager.Object.PickZones.ElementAt(1).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(2).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(3).Status = ZoneStatus.Normal;
//            result = selector.GetCartonRequestsToPickZonesWithoutStop(requestManager.Object.ValidRequestsInActivePickzones);
//            Specify.That(result.Count()).Should.BeEqualTo(3);
//            Specify.That(result.ElementAt(0).PickZone).Should.BeEqualTo("A2");
//            Specify.That(result.ElementAt(1).PickZone).Should.BeEqualTo("B1");
//            Specify.That(result.ElementAt(2).PickZone).Should.BeEqualTo("B1");

//            pickAreaManager.Object.PickZones.ElementAt(0).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(1).Status = ZoneStatus.Stop;
//            pickAreaManager.Object.PickZones.ElementAt(2).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(3).Status = ZoneStatus.Normal;
//            result = selector.GetCartonRequestsToPickZonesWithoutStop(requestManager.Object.ValidRequestsInActivePickzones);
//            Specify.That(result.Count()).Should.BeEqualTo(3);
//            Specify.That(result.ElementAt(0).PickZone).Should.BeEqualTo("A1");
//            Specify.That(result.ElementAt(1).PickZone).Should.BeEqualTo("B1");
//            Specify.That(result.ElementAt(2).PickZone).Should.BeEqualTo("B1");

//            pickAreaManager.Object.PickZones.ElementAt(0).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(1).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(2).Status = ZoneStatus.Stop;
//            pickAreaManager.Object.PickZones.ElementAt(3).Status = ZoneStatus.Normal;
//            result = selector.GetCartonRequestsToPickZonesWithoutStop(requestManager.Object.ValidRequestsInActivePickzones);
//            Specify.That(result.Count()).Should.BeEqualTo(2);
//            Specify.That(result.ElementAt(0).PickZone).Should.BeEqualTo("A1");
//            Specify.That(result.ElementAt(1).PickZone).Should.BeEqualTo("A2");

//            pickAreaManager.Object.PickZones.ElementAt(0).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(1).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(2).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(3).Status = ZoneStatus.Stop;
//            result = selector.GetCartonRequestsToPickZonesWithoutStop(requestManager.Object.ValidRequestsInActivePickzones);
//            Specify.That(result.Count()).Should.BeEqualTo(4);
//            Specify.That(result.ElementAt(0).PickZone).Should.BeEqualTo("A1");
//            Specify.That(result.ElementAt(1).PickZone).Should.BeEqualTo("A2");
//            Specify.That(result.ElementAt(2).PickZone).Should.BeEqualTo("B1");
//            Specify.That(result.ElementAt(3).PickZone).Should.BeEqualTo("B1");

//            pickAreaManager.Object.PickZones.ElementAt(0).Status = ZoneStatus.Normal;
//            pickAreaManager.Object.PickZones.ElementAt(1).Status = ZoneStatus.Stop;
//            pickAreaManager.Object.PickZones.ElementAt(2).Status = ZoneStatus.Stop;
//            pickAreaManager.Object.PickZones.ElementAt(3).Status = ZoneStatus.Stop;
//            result = selector.GetCartonRequestsToPickZonesWithoutStop(requestManager.Object.ValidRequestsInActivePickzones);
//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.ElementAt(0).PickZone).Should.BeEqualTo("A1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetExpeditedCartonRequests()
//        {
//            var result = selector.FilterOnClassificationStatus(requestManager.Object.ValidRequestsInActivePickzones, ClassificationStatus.Expedite);
//            Specify.That(result.Count()).Should.BeEqualTo(0);

//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Normal;
//            result = selector.FilterOnClassificationStatus(requestManager.Object.ValidRequestsInActivePickzones, ClassificationStatus.Expedite);
//            Specify.That(result.Count()).Should.BeEqualTo(1);

//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Normal;
//            result = selector.FilterOnClassificationStatus(requestManager.Object.ValidRequestsInActivePickzones, ClassificationStatus.Expedite);
//            Specify.That(result.Count()).Should.BeEqualTo(2);

//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Expedite;
//            result = selector.FilterOnClassificationStatus(requestManager.Object.ValidRequestsInActivePickzones, ClassificationStatus.Expedite);
//            Specify.That(result.Count()).Should.BeEqualTo(0);

//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Expedite;
//            result = selector.FilterOnClassificationStatus(requestManager.Object.ValidRequestsInActivePickzones, ClassificationStatus.Expedite);
//            Specify.That(result.Count()).Should.BeEqualTo(3);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetCartonRequestsForMostExclusiveCorrugate()
//        {

//            var results = selector.GetCartonRequestOrdersThatFitsOnCorrugate(requestManager.Object.ValidRequestsInActivePickzones, new Corrugate { Width = 100, Quality = 1 }, new FusionMachine());
//            Specify.That(results.Count()).Should.BeEqualTo(0);

//            results = selector.GetCartonRequestOrdersThatFitsOnCorrugate(requestManager.Object.ValidRequestsInActivePickzones, new Corrugate { Width = 200, Quality = 1 }, new FusionMachine());
//            Specify.That(results.Count()).Should.BeEqualTo(2);
//            Specify.That(results.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(results.ElementAt(1).SerialNumber).Should.BeEqualTo("2");

//            results = selector.GetCartonRequestOrdersThatFitsOnCorrugate(requestManager.Object.ValidRequestsInActivePickzones, new Corrugate { Width = 300, Quality = 1 }, new FusionMachine());
//            Specify.That(results.Count()).Should.BeEqualTo(3);
//            Specify.That(results.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(results.ElementAt(1).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(results.ElementAt(2).SerialNumber).Should.BeEqualTo("3");

//            results = selector.GetCartonRequestOrdersThatFitsOnCorrugate(requestManager.Object.ValidRequestsInActivePickzones, new Corrugate { Width = 400, Quality = 1 }, new FusionMachine());
//            Specify.That(results.Count()).Should.BeEqualTo(4);
//            Specify.That(results.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(results.ElementAt(1).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(results.ElementAt(2).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(results.ElementAt(3).SerialNumber).Should.BeEqualTo("4");

//            results = selector.GetCartonRequestOrdersThatFitsOnCorrugate(requestManager.Object.ValidRequestsInActivePickzones, new Corrugate { Width = 400, Quality = 2, Id = Guid.NewGuid() }, new FusionMachine());
//            Specify.That(results.Count()).Should.BeEqualTo(0);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnPrintableRequests()
//        {
//            SetUpRequestManagerMockWithNoLabelRequests();

//            classificationsManager.Object.Classifications.ElementAt(0).IsPriorityLabel = false;
//            classificationsManager.Object.Classifications.ElementAt(1).IsPriorityLabel = true;

//            var machine = new Mock<IPacksizeCutCreaseMachine>();

//            //machine.Setup(m => m.Printers).Returns(new PrinterInformation[0]);
//            var results = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(results.Count()).Should.BeEqualTo(2);

//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(false);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(false);
//            results = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(results.Count()).Should.BeEqualTo(2);
//            Specify.That(results.ElementAt(0).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(results.ElementAt(1).SerialNumber).Should.BeEqualTo("4");

//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(false);
//            results = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(results.Count()).Should.BeEqualTo(3);
//            Specify.That(results.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(results.ElementAt(1).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(results.ElementAt(2).SerialNumber).Should.BeEqualTo("4");

//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(false);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            results = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(results.Count()).Should.BeEqualTo(3);
//            Specify.That(results.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(results.ElementAt(1).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(results.ElementAt(2).SerialNumber).Should.BeEqualTo("4");

//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            results = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(results.Count()).Should.BeEqualTo(4);
//            Specify.That(results.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(results.ElementAt(1).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(results.ElementAt(2).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(results.ElementAt(3).SerialNumber).Should.BeEqualTo("4");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldFilterOutRequestsWithStopClassification()
//        {
//            var requests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            ClassificationNumber = "1",
//                            PickZone = "D1",
//                            SerialNumber = "1",
//                            DesignId = 1,
//                            Length = 150,
//                            Width = 150
//                        },
//                    new Carton
//                        {
//                            ClassificationNumber = "2",
//                            PickZone = "D1",
//                            SerialNumber = "1",
//                            DesignId = 1,
//                            Length = 150,
//                            Width = 150
//                        },
//                    new Carton
//                        {
//                            ClassificationNumber = "3",
//                            PickZone = "D1",
//                            SerialNumber = "1",
//                            DesignId = 1,
//                            Length = 150,
//                            Width = 150
//                        },
//                    new Carton
//                        {
//                            ClassificationNumber = "4",
//                            PickZone = "D1",
//                            SerialNumber = "1",
//                            DesignId = 1,
//                            Length = 150,
//                            Width = 150
//                        }
//                };
//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Stop;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(3).Status = ClassificationStatus.Stop;

//            var result = selector.GetCartonRequestsWithNonStoppedClassification(requests);

//            Specify.That(result.Count()).Should.BeEqualTo(2);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldFilterOutRequestsToUnavailableCartonPropertyGroups()
//        {
//            var requests = new List<ICarton>
//                {
//                    new Carton { CartonPropertyGroupAlias = "G1", SerialNumber = "1" },
//                    new Carton { CartonPropertyGroupAlias = "G2", SerialNumber = "2" },
//                    new Carton { CartonPropertyGroupAlias = "G1", SerialNumber = "3" },
//                    new Carton { CartonPropertyGroupAlias = "G3", SerialNumber = "4" }
//                };

//            var machine = new Mock<IPacksizeCutCreaseMachine>();

//            machine.SetupGet(m => m.Id).Returns(Guid.NewGuid());
//            var result = selector.FilterOutUnavailableCartonPropertyGroupRequests(requests, machine.Object);
//            Specify.That(result.Count()).Should.BeEqualTo(3);
//            Specify.That(result.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(result.ElementAt(1).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(result.ElementAt(2).SerialNumber).Should.BeEqualTo("2");

//            machine.SetupGet(m => m.Id).Returns(Guid.NewGuid());
//            result = selector.FilterOutUnavailableCartonPropertyGroupRequests(requests, machine.Object);
//            Specify.That(result.Count()).Should.BeEqualTo(0);

//            machine.SetupGet(m => m.Id).Returns(Guid.NewGuid());
//            result = selector.FilterOutUnavailableCartonPropertyGroupRequests(requests, machine.Object);
//            Specify.That(result.Count()).Should.BeEqualTo(0);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldFilterOutRequestsWithStoppedCartonPropertyGroup()
//        {
//            var requests = new List<ICarton>
//                {
//                    new Carton { CartonPropertyGroupAlias = "G1", SerialNumber = "1" },
//                    new Carton { CartonPropertyGroupAlias = "G2", SerialNumber = "2" },
//                    new Carton { CartonPropertyGroupAlias = "G1", SerialNumber = "3" },
//                    new Carton { CartonPropertyGroupAlias = "G3", SerialNumber = "4" }
//                };

//            var groups = new List<CartonPropertyGroup>();
//            cartonPropertyGroupManager
//                .Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Stop))
//                .Returns(() => groups);

//            var result = selector.GetCartonRequestsToCartonPropertyGroupsWithoutStop(requests);
//            Specify.That(result.Count()).Should.BeEqualTo(4);
//            Specify.That(result.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(result.ElementAt(1).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(result.ElementAt(2).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(result.ElementAt(3).SerialNumber).Should.BeEqualTo("4");

//            var group = new Mock<CartonPropertyGroup>();
//            group.SetupGet(g => g.Alias).Returns("G1");
//            groups.Add(group.Object);

//            result = selector.GetCartonRequestsToCartonPropertyGroupsWithoutStop(requests);
//            Specify.That(result.Count()).Should.BeEqualTo(2);
//            Specify.That(result.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(result.ElementAt(1).SerialNumber).Should.BeEqualTo("4");

//            group = new Mock<CartonPropertyGroup>();
//            group.SetupGet(g => g.Alias).Returns("G2");
//            groups.Add(group.Object);

//            result = selector.GetCartonRequestsToCartonPropertyGroupsWithoutStop(requests);
//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.ElementAt(0).SerialNumber).Should.BeEqualTo("4");

//            group = new Mock<CartonPropertyGroup>();
//            group.SetupGet(g => g.Alias).Returns("G3");
//            groups.Add(group.Object);

//            Specify.That(selector.GetCartonRequestsToCartonPropertyGroupsWithoutStop(requests).Count()).Should.BeEqualTo(0);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void AllRequestsAreReturnedWhenFilterPrintableOnMachineWithBothPrinters()
//        {
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            var result = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(result.Count()).Should.BeEqualTo(4);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void OnlyHighPriorityClassificationsAreReturnedWhenFilterPrintableOnlyHighPrioPrinter()
//        {
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(false);
//            var result = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(result.Count()).Should.BeEqualTo(1);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void OnlyNormalPriorityClassificationsAreReturnedWhenFilterPrintableOnlyNormalPrioPrinter()
//        {
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(false);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            var result = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(result.Count()).Should.BeEqualTo(3);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void NoRequestsAreReturnedWhenFilterPrintableOnlyNormalPrioPrinter()
//        {
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(false);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(false);
//            var result = selector.FilterOutNonPrintableOnMachine(requestManager.Object.ValidRequestsInActivePickzones, machine.Object);
//            Specify.That(result.Count()).Should.BeEqualTo(0);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUseCorrugateForMachineWhenDuplicatesExists()
//        {
//            var result = selector.GetNextJobByTilingPartner(requestManager.Object.ValidRequestsInActivePickzones, new List<ICarton>(), machinesManager.Object.Machines.ElementAt(0), machinesManager.Object.Machines.ElementAt(0).Tracks.First().LoadedCorrugate);
//            Specify.That(result.Corrugate.Id).Should.Not.BeEqualTo(Guid.Empty);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnBestYeild()
//        {
//            var result = selector.GetNextJobByTilingPartner(requestManager.Object.ValidRequestsInActivePickzones, new List<ICarton>(), machinesManager.Object.Machines.ElementAt(0), machinesManager.Object.Machines.ElementAt(0).Tracks.First().LoadedCorrugate);
//            Specify.That(result.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo(requestManager.Object.ValidRequestsInActivePickzones.ElementAt(3).SerialNumber);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnSecondBestYeildIfFirstIsAlreadyTaken()
//        {
//            requestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.Is<IEnumerable<ICarton>>(r => r.ElementAt(0).SerialNumber == "4"), It.IsAny<Guid?>())).Returns(false);
//            var result = selector.GetNextJobByTilingPartner(requestManager.Object.ValidRequestsInActivePickzones, new List<ICarton>(), machinesManager.Object.Machines.ElementAt(0), machinesManager.Object.Machines.ElementAt(0).Tracks.First().LoadedCorrugate);
//            Specify.That(result.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo(requestManager.Object.ValidRequestsInActivePickzones.ElementAt(2).SerialNumber);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnNullIfNoRequestIsFound()
//        {
//            requestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(false);
//            var result = selector.GetNextJobByTilingPartner(requestManager.Object.ValidRequestsInActivePickzones, new List<ICarton>(), machinesManager.Object.Machines.ElementAt(0), new Corrugate());
//            Specify.That(result).Should.BeNull();
//        }

//        private void SetupDesignManagerMock()
//        {
//            var packagingDesignOne = new PackagingDesign { Name = "1", Id = 1, LengthOnZFold = "L", WidthOnZFold = "W" };
//            designManagerMock = new Mock<IPackagingDesignManager>();
//            designManagerMock.Setup(m => m.GetDesign(It.IsAny<DesignQueryParam>())).Returns(packagingDesignOne);
//            designManagerMock.Setup(m => m.GetDesigns()).Returns(new List<PackagingDesign>() { packagingDesignOne });
//        }

//        private void SetUpProductionGroupManagerMock()
//        {
//            var pg1 = new Mock<CommonProductionGroups.ProductionGroup>();
//            pg1.SetupGet(g => g.Alias).Returns("pg1");
//            //pg1.SetupGet(g => g.ProductionItems).Returns(new[] { new ProductionItem("G1", 1), new ProductionItem("G2", 1) });
//            //pg1.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(1) });

//            var pg2 = new Mock<CommonProductionGroups.ProductionGroup>();
//            pg2.SetupGet(g => g.Alias).Returns("pg2");
//            //pg2.SetupGet(g => g.ProductionItems).Returns(new ProductionItem[0]);
//            //pg2.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(1) });

//            productionGroupManager = new Mock<IProductionGroupManager>();
//            productionGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine1Id)).Returns(pg1.Object);
//            productionGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine2Id)).Returns(pg2.Object);
//            productionGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine3Id)).Returns((CommonProductionGroups.ProductionGroup)null);

//            productionGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.Is<string>(s => s == "G1" || s == "G2"))).Returns(new[] { pg1.Object });
//            productionGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.Is<string>(s => s != "G1" && s != "G2"))).Returns(new[] { pg1.Object });
//        }

//        private void SetUpCartonPropertyGroupManagerMock()
//        {
//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Stop)).Returns(new CartonPropertyGroup[0]);
//        }

//        private void SetupLongHeadPositioningValidationMock()
//        {
//            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();
//        }

//        private void SetUpPickAreaManagerMock()
//        {
//            var areas = Common.SetUpPickAreas();
//            pickAreaManager = new Mock<IPickZoneManager>();
//            pickAreaManager.Setup(manager => manager.PickZones).Returns(areas.SelectMany(pa => pa.PickZones));
//        }

//        private void SetUpRequestManagerMock()
//        {
//            IList<ICarton> requests = new List<ICarton>();
//            requests.Add(new Carton { ClassificationNumber = "1", PickZone = "A1", SerialNumber = "1", DesignId = 1, Length = 150, Width = 150, CorrugateQuality = 1, ShouldPrintLabel = true });
//            requests.Add(new Carton { ClassificationNumber = "2", PickZone = "A2", SerialNumber = "2", DesignId = 1, Length = 200, Width = 200, CorrugateQuality = 1, ShouldPrintLabel = true });
//            requests.Add(new Carton { ClassificationNumber = "2", PickZone = "B1", SerialNumber = "3", DesignId = 1, Length = 300, Width = 300, CorrugateQuality = 1, ShouldPrintLabel = true });
//            requests.Add(new Carton { ClassificationNumber = "4", PickZone = "B1", SerialNumber = "4", DesignId = 1, Length = 400, Width = 400, CorrugateQuality = 1, ShouldPrintLabel = true });

//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            requestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(requests);

//            requestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                    IEnumerable<ICarton>,
//                    Corrugate,
//                    IPacksizeCutCreaseMachine,
//                    PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                    bool,
//                    bool,
//                    Func<DesignQueryParam, IPackagingDesign>,
//                    int?,
//                    bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder
//                    {
//                        Requests = a,
//                        Corrugate = b,
//                        MachineId = c.Id,
//                        InternalPrinting = f,
//                        PackStation = h,
//                        SerialNo = CartonRequestOrder.CreateSerialNumber(a),
//                        Design = OrderHelpers.MergeDesignsSideBySide(mockCorrugateCalculationService.Object, a, b, c, e, g, i)
//                    });
//        }

//        private void SetUpRequestManagerMockWithNoLabelRequests()
//        {
//            IList<ICarton> requests = new List<ICarton>();
//            requests.Add(new Carton { ClassificationNumber = "1", PickZone = "A1", SerialNumber = "1", DesignId = 1, Length = 150, Width = 150, CorrugateQuality = 1, ShouldPrintLabel = true });
//            requests.Add(new Carton { ClassificationNumber = "2", PickZone = "A2", SerialNumber = "2", DesignId = 1, Length = 200, Width = 200, CorrugateQuality = 1, ShouldPrintLabel = true });
//            requests.Add(new Carton { ClassificationNumber = "2", PickZone = "B1", SerialNumber = "3", DesignId = 1, Length = 300, Width = 300, CorrugateQuality = 1, ShouldPrintLabel = false });
//            requests.Add(new Carton { ClassificationNumber = "4", PickZone = "B1", SerialNumber = "4", DesignId = 1, Length = 400, Width = 400, CorrugateQuality = 1, ShouldPrintLabel = false });

//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            requestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(requests);
//        }

//        private void SetUpMachinesManagerMock()
//        {
//            var c1Id = Guid.NewGuid();
//            var c2Id = Guid.NewGuid();
//            var c3Id = Guid.NewGuid();

//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.SetupGet(m => m.Id).Returns(machine1Id);

//            var machineCorrugates = new Corrugate[2];
//            machineCorrugates[0] = new Corrugate { Width = 400, Thickness = 1, Quality = 1, Id = c1Id };
//            machineCorrugates[1] = new Corrugate { Width = 500, Thickness = 1, Quality = 1, Id = c2Id };
//            machine.Setup(m => m.Tracks).Returns(new List<Track> 
//                {
//                    new Track { TrackNumber = 1, LoadedCorrugate = machineCorrugates.First()}, 
//                    new Track { TrackNumber = 2, LoadedCorrugate = machineCorrugates.Last()}, 
//                });

//            var corrugates = new List<Corrugate>();

//            var corrugateWithId = new Mock<Corrugate>();
//            corrugateWithId.SetupGet(c => c.Id).Returns(c3Id);
//            corrugateWithId.SetupGet(c => c.Width).Returns(400);
//            corrugateWithId.SetupGet(c => c.Thickness).Returns(1);
//            corrugateWithId.SetupGet(c => c.Quality).Returns(1);
//            corrugates.Add(corrugateWithId.Object);

//            corrugates.Add(new Corrugate { Width = 200, Thickness = 1, Quality = 1, Id = c3Id });

//            corrugates.AddRange(machineCorrugates);

//            machinesManager = new Mock<IMachinesManager>();
//            machinesManager.Setup(manager => manager.Machines).Returns(new[] { machine.Object });
//            //machinesManager.Setup(manager => manager.GetAllAvailableCorrugates()).Returns(corrugates);
//            //machinesManager.Setup(manager => manager.GetCorrugatesByProductionGroup(It.IsAny<CommonProductionGroups.ProductionGroup>())).Returns(corrugates.ToArray);
//        }

//        private void SetUpClassificationsManagerMock()
//        {
//            IList<PackNet.Common.Interfaces.DTO.Classifications.Classification> classifications = Common.SetUpClassifications();

//            classificationsManager = new Mock<IClassificationsManager>();
//            classificationsManager.Setup(manager => manager.Classifications).Returns(classifications);
//        }
    }
}