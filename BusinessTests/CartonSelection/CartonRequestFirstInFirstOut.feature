﻿Feature: Manual Orders Queue jobselection
	In order to get manual added order produced in an effiecent way
	As an operator
	I need the machines to produce according to a prioritized list

# Work in progress converting SpecFlow features to StoryQ
# New StoryQ test class is CartonRequestFirstInFirstOutFeature.cs

# Feature has been converted to StoryQ
# @ManualOrderQueue
#Scenario: Should produce order that are assigned to the specific machine
#	Given a corrugate c1
#	And a machine loaded with c1
#	And order1 optimal on corrugate c1 assigned to another machine
#	And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
#	And order2 is not picked up by any other machine
#	When next job is requested
#	Then order2 is produced

@ManualOrderQueue
Scenario: Should not produce order that are assigned to another machine
	Given a corrugate c1
	And a machine loaded with c1
	And order1 optimal on corrugate c1 assigned to another machine
	And no order is picked up by any other machine
	When next job is requested
	Then no order is produced

@ManualOrderQueue
Scenario: Should not produce orders that are assigned to the specific machine but with an unloaded corrugate
	Given a corrugate c1
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c2 optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then no order is produced

@ManualOrderQueue
Scenario: Should not produce orders that are assigned to the specific machine but already in queue
	Given a corrugate c1
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then order1 is produced without tiles
	When next job is requested
	Then no order is produced

@ManualOrderQueue
Scenario: Should tile within orders
	Given a corrugate c1
	And a machine loaded with c1
	And order1 with a quantity 2 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then order1 is produced with tiles

@ManualOrderQueue
Scenario: Should not tile between orders
	Given a corrugate c1
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then order1 is produced without tiles

@ManualOrderQueue
Scenario: Should pause between orders when production group is set to pause
	Given a corrugate c1
	And a production group setup with pause between orders true
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then order1 is produced without tiles
	When next job is requested
	Then no order is produced

@ManualOrderQueue
Scenario: Should not pause between orders when production group is set to not pause
	Given a corrugate c1
	And a production group setup with pause between orders false
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then order1 is produced without tiles
	When next job is requested
	Then order2 is produced without tiles

@ManualOrderQueue
Scenario: Should produce second order that are assigned to the specific machine when the first is picked up by another machine
	Given a corrugate c1
	And a production group setup with pause between orders true
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And order1 is picked up by another machine
	And order2 is not picked up by any other machine
	When next job is requested
	Then order2 is produced
	
@ManualOrderQueue
Scenario: Should not produce orders that are not optimal for my machine
	Given a corrugate c1
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c1 not optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then no order is produced
	
@ManualOrderQueue
Scenario: Should only produce orders that are optimal for my machine
	Given a corrugate c1
	And a machine loaded with c1
	And order1 with a quantity 1 optimal tiled on corrugate c1 not optimal on my machine assigned to my machine
	And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
	And no order is picked up by any other machine
	When next job is requested
	Then order2 is produced