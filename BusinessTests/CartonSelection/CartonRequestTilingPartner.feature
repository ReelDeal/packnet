﻿Feature: Carton request tiling
	In order to optimize throughput
	As a production manager
	I want requests to be tiled when possible

@CartonRequestTilingPartner
Scenario: Should select surge expedite tiling partner for surge expedite request when all requests are identical
	Given a number of corrugates
	| Alias | Width | Quality |
	| 1     | 500   | 6       |
	| 2     | 500   | 10      |
	And a number of classfications
	| Classification | Status   |
	| 1              | Expedite |
	| 2              | Normal   |
	| 3              | Hold     |
	| 4              | Stop     |
	And a number of tiling requests
	| SerialNumber | CartonPropertyGroup | Classification | AreaOn | TotalLengthOn | TotalWidthOn | CreatedTime | CorrugateQuality |
	| CSN3         | 1                   | 2              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSH4         | 1                   | 3              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSR5         | 1                   | 4              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSE1         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSE2         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 10               |
	And carton property group 1 has status Surge
	And carton property group 1 is next in sequence mix all the time
	And corrugates are exclusive in reverse order of addition
	When I get next job for machine
	Then I should get CSE1 and CSE2

Scenario: Should select surge expedite tiling partner for surge expedite request when created time differs
	Given a number of corrugates
	| Alias | Width | Quality |
	| 1     | 500   | 6       |
	| 2     | 500   | 10      |
	And a number of classfications
	| Classification | Status   |
	| 1              | Expedite |
	| 2              | Normal   |
	| 3              | Hold     |
	| 4              | Stop     |
	And a number of tiling requests
	| SerialNumber | CartonPropertyGroup | Classification | AreaOn | TotalLengthOn | TotalWidthOn | CreatedTime | CorrugateQuality |
	| CSN3         | 1                   | 2              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSH4         | 1                   | 3              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSR5         | 1                   | 4              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSE1         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSE2         | 1                   | 1              | 200    | 200           | 250          | 2012-02-01  | 6                |
	And carton property group 1 has status Surge
	And carton property group 1 is next in sequence mix all the time
	And corrugates are exclusive in reverse order of addition
	When I get next job for machine
	Then I should get CSE1 and CSE2

	Scenario: Should select surge expedite tiling partner for surge expedite request when area on differs
	Given a number of corrugates
	| Alias | Width | Quality |
	| 1     | 500   | 6       |
	| 2     | 500   | 10      |
	And a number of classfications
	| Classification | Status   |
	| 1              | Expedite |
	| 2              | Normal   |
	| 3              | Hold     |
	| 4              | Stop     |
	And a number of tiling requests
	| SerialNumber | CartonPropertyGroup | Classification | AreaOn | TotalLengthOn | TotalWidthOn | CreatedTime | CorrugateQuality |
	| CSN3         | 1                   | 2              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSH4         | 1                   | 3              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSR5         | 1                   | 4              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSE1         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSE2         | 1                   | 1              | 100    | 200           | 250          | 2012-01-01  | 6                |
	And carton property group 1 has status Surge
	And carton property group 1 is next in sequence mix all the time
	And corrugates are exclusive in reverse order of addition
	When I get next job for machine
	Then I should get CSE1 and CSE2

	Scenario: Should select surge expedite tiling partner for surge expedite request when total length on differs
	Given a number of corrugates
	| Alias | Width | Quality |
	| 1     | 500   | 6       |
	| 2     | 500   | 10      |
	And a number of classfications
	| Classification | Status   |
	| 1              | Expedite |
	| 2              | Normal   |
	| 3              | Hold     |
	| 4              | Stop     |
	And a number of tiling requests
	| SerialNumber | CartonPropertyGroup | Classification | AreaOn | TotalLengthOn | TotalWidthOn | CreatedTime | CorrugateQuality |
	| CSN3         | 1                   | 2              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSH4         | 1                   | 3              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSR5         | 1                   | 4              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSE1         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 6                |
	| CSE2         | 1                   | 1              | 200    | 100           | 250          | 2012-01-01  | 6                |
	And carton property group 1 has status Surge
	And carton property group 1 is next in sequence mix all the time
	And corrugates are exclusive in reverse order of addition
	When I get next job for machine
	Then I should get CSE1 and CSE2

	@CartonRequestTilingPartner
Scenario: Should not select surge expedite tiling partner for surge expedite request when corrugate quality differs, instead selects CSN3
	Given a number of corrugates
	| Alias | Width | Quality |
	| 1     | 500   | 6       |
	| 2     | 500   | 10      |
	And a number of classfications
	| Classification | Status   |
	| 1              | Expedite |
	| 2              | Normal   |
	| 3              | Hold     |
	| 4              | Stop     |
	And a number of tiling requests
	| SerialNumber | CartonPropertyGroup | Classification | AreaOn | TotalLengthOn | TotalWidthOn | CreatedTime | CorrugateQuality |
	| CSN3         | 1                   | 2              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSH4         | 1                   | 3              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSR5         | 1                   | 4              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSE1         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSE2         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 10               |
	And carton property group 1 has status Surge
	And carton property group 1 is next in sequence mix all the time
	And corrugates are exclusive in reverse order of addition
	When I get next job for machine
	Then I should get CSE1 and CSE2

@CartonRequestTilingPartner
Scenario: Should tile NonSurged expedite partners when no surged requests are available

	Given a number of corrugates
	| Alias | Width | Quality |
	| 1     | 500   | 6       |
	| 2     | 500   | 10      |
	And a number of classfications
	| Classification | Status   |
	| 1              | Expedite |
	| 2              | Normal   |
	| 3              | Hold     |
	| 4              | Stop     |
	And a number of tiling requests
	| SerialNumber | CartonPropertyGroup | Classification | AreaOn | TotalLengthOn | TotalWidthOn | CreatedTime | CorrugateQuality |
	| CSN3         | 1                   | 2              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSH4         | 1                   | 3              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSR5         | 1                   | 4              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSE1         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 10               |
	| CSE2         | 1                   | 1              | 200    | 200           | 250          | 2012-01-01  | 10               |
	And carton property group 1 has status Normal
	And carton property group 1 is next in sequence mix all the time
	And corrugates are exclusive in reverse order of addition
	When I get next job for machine
	Then I should get CSE1 and CSE2
