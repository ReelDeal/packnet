namespace BusinessTests.CartonSelection
{
    using Moq;

    using PackNet.Common.Interfaces.DTO.Classifications;
    using PackNet.Common.Interfaces.Enums;

    public class ClassificationTestContainer
    {
        public int Classification { get; set; }

        public ClassificationStatuses Status { get; set; }

        public Mock<Classification> CreateMock()
        {
            var mock = new Mock<Classification>();
            mock.Setup(c => c.Number).Returns(Classification.ToString);
            mock.Setup(c => c.Status).Returns(Status);
            return mock;
        }
    }
}