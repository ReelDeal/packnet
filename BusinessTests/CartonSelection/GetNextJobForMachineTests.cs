﻿//using PackNet.Business.PickZone;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.Orders;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.ComponentModel.FormulaEvaluator;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Enums;
//    using PackNet.Common.Interfaces.Services;

//    using Testing.Specificity;
//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

    [TestClass]
//    [DeploymentItem(@"CartonSelection\Designs", "Designs")]
    public class GetNextJobForMachineTests
    {
#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
        //        #region Fields

//        private ICartonRequestSelector requestSelector;

//        private Mock<ICartonRequestManager> cartonRequestManager;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IPickZoneManager> pickAreaManager;
//        private Mock<IMachinesManager> machinesManager;
//        private Mock<IProductionGroupManager> sequenceGroupManager;
//        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup;
//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;

//        private List<CartonPropertyGroup> stoppedCpgs;
//        private List<CartonPropertyGroup> surgedCpgs;
//        private List<CartonPropertyGroup> normalCpgs;

//        private IPacksizeCutCreaseMachine testMachine1;

//        private List<Corrugate> allCorrugates;

//        private Queue<string> groupNames;
//        private IPackagingDesignManager designManager;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private Guid machine1Id = Guid.NewGuid();
//        private Guid machine2Id = Guid.NewGuid();
//        private Guid machine3Id = Guid.NewGuid();
//        private Guid machine4Id = Guid.NewGuid();

//        #endregion

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            SetupProductionGroupManagerMock();

//            pickAreaManager = new Mock<IPickZoneManager>();
//            pickAreaManager.Setup(manager => manager.PickZones).Returns(Common.SetUpPickZones("A").Concat(Common.SetUpPickZones("B")));

//            classificationsManager = new Mock<IClassificationsManager>();
//            var classifications = Common.SetUpClassifications();
//            classificationsManager.Setup(manager => manager.Classifications).Returns(classifications);

//            SetupCorrugatesCalculationServiceMock();

//            stoppedCpgs = new List<CartonPropertyGroup>();
//            surgedCpgs = new List<CartonPropertyGroup>();
//            normalCpgs = new List<CartonPropertyGroup>();
//            SetupRequestsManager();

//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Stop)).Returns(
//                () => stoppedCpgs);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Surge)).Returns(
//                () => surgedCpgs);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Normal)).Returns(
//                () => normalCpgs);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>())).Returns(new CartonPropertyGroup());

//            allCorrugates = new List<Corrugate>();
//            machinesManager = new Mock<IMachinesManager>();
//            machinesManager.Setup(manager => manager.Machines).Returns(SetUpMachines());

//            testMachine1 = machinesManager.Object.Machines.ElementAt(0);

//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(testMachine1, new[] { sequenceGroup.Object })).Returns(allCorrugates);
//            designManager = new PackagingDesignManager(@"Designs", new Mock<ILogger>(MockBehavior.Loose).Object);
//            designManager.LoadDesigns();
//            requestSelector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                cartonRequestManager.Object,
//                classificationsManager.Object,
//                pickAreaManager.Object,
//                machinesManager.Object,
//                sequenceGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManager,
//                new SelectorSettings(2, true, 40, 40, 0),
//                new Mock<ILongHeadPositioningValidation>().Object,
//                new WmsConfiguration(),
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//        }

//        private void SetupCorrugatesCalculationServiceMock()
//        {
//            mockCorrugateCalculationService = new Mock<ICorrugateService>();

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Width != null && a.Width.Value < b.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Length);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null ? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width,
//                    Design = designManager.GetDesigns().First()
//                });

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));
//        }

//        private void SetupRequestsManager()
//        {
//            cartonRequestManager = new Mock<ICartonRequestManager>();

//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h, SerialNo = CartonRequestOrder.CreateSerialNumber(a) });
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void OnlyRequestsToStoppedCartonPropertyGroupsReturnsNull()
//        {
//            /*
//             * Machine 1 - FF 800(ex), FF 1200
//               Machine 2 - FF 1200
//             * 
//             *  CR1 : L500 W500 H500. SN:1 DesignId:1 Classification: 1, PickZone: A1, CartonPropertyGroup: G1
//                CR2 : L850 W850 H850. SN:2 DesignId:1 Classification: 1, PickZone: A2, CartonPropertyGroup: G2 (G2 in other ProductionGroup)
//             */

//            SetUpRequestWithDifferentCpg();

//            AddCartonPropertyGroupWithStoppedStatus("G1");

//            // Verify that null is returned
//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            Specify.That(nextJob).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CanNotTakeARequestForAStoppedCartonPropertyGroupAsPartner()
//        {
//            /*
//             *  Machine 1 - FF 800(ex), FF 1200
//                Machine 2 - FF 1200
//             * 
//             *  CR1 : L500 W500 H500. SN:1 DesignId:1 Classification: 1, CartonPropertyGroup: A1
//                CR2 : L850 W850 H850. SN:2 DesignId:1 Classification: 1, CartonPropertyGroup: A2
//             */

//            SetUpRequestSet1();

//            // 1. CPG A1 Stopped, A2 next to use
//            AddCartonPropertyGroupWithStoppedStatus("A1");
//            AddCartonPropertyGroupWithNormalStatus("A2");

//            // 2. Fit on 2:nd exclusive fanfold
//            SetListOfExclusiveFanfoldsForMachine(testMachine1, testMachine1.Tracks.Select(t => t.LoadedCorrugate));

//            // 4. Tiling - Non to tile with

//            // 5. Best yield
//            cartonRequestManager.Setup(cr => cr.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            var nextOrder = requestSelector.GetNextJobForMachine(testMachine1);

//            // Verify [CR2]
//            Specify.That(nextOrder.ContainsJob("2")).Should.BeTrue();
//            Specify.That(nextOrder.Requests.Count()).Should.BeEqualTo(1);
//            AssertThatCorrugatedWidthIsNotExceded(nextOrder);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CanNotTakeARequestForAStoppedPickZoneAsPartner()
//        {
//            /*
//             *  Machine 1 - FF 800(ex), FF 1200
//                Machine 2 - FF 1200
//             * 
//             *  CR1 : L500 W500 H500. SN:1 DesignId:1 Classification: 1, PickZone: A1, CartonPropertyGroup: A1
//                CR2 : L850 W850 H850. SN:2 DesignId:1 Classification: 1, PickZone: A2, CartonPropertyGroup: A2
//             */

//            SetUpRequestSet1();

//            AddCartonPropertyGroupWithNormalStatus("A1");
//            AddCartonPropertyGroupWithNormalStatus("A2");

//            // 1. PickZ A1 Stopped
//            SetPickZoneStatus("A1", ZoneStatus.Stop);

//            // 2. Fit on 2:nd exclusive fanfold
//            SetListOfExclusiveFanfoldsForMachine(testMachine1, testMachine1.Tracks.Select(t => t.LoadedCorrugate));

//            // 4. Tiling - Non to tile with
//            // 5. Best yield
//            cartonRequestManager.Setup(cr => cr.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            var nextOrder = requestSelector.GetNextJobForMachine(testMachine1);

//            // Verify [CR2]
//            Specify.That(nextOrder.ContainsJob("2")).Should.BeTrue();
//            Specify.That(nextOrder.Requests.Count()).Should.BeEqualTo(1);
//            AssertThatCorrugatedWidthIsNotExceded(nextOrder);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void IsWaveSelectorFiltersOutMachineSpecificJobs()
//        {
//            SetUpRequestSet8();

//            AddCartonPropertyGroupWithNormalStatus("A1");
//            AddCartonPropertyGroupWithNormalStatus("B1");
//            AddCartonPropertyGroupWithNormalStatus("B2");

//            SetListOfExclusiveFanfoldsForMachine(testMachine1, new[] { testMachine1.Tracks.Select(t => t.LoadedCorrugate).ElementAt(0) });

//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            Specify.That(nextJob.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(nextJob.Requests.ElementAt(1).SerialNumber).Should.BeEqualTo("3");

//            AssertThatCorrugatedWidthIsNotExceded(nextJob);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CartonPropertyGroupWithSurge()
//        {
//            /*
//             *  Machine 1 - FF 800(ex), FF 1200
//                Machine 2 - FF 1200
//             * 
//                CR1 : L500 W500 H500. SN:1 DesignId:1 Classification: 1, PickZone: A1, CartonPropertyGroup: A1
//                CR2 : L400 W400 H400. SN:2 DesignId:1 Classification: 1, PickZone: B1, CartonPropertyGroup: B1
//                CR3 : L400 W400 H400. SN:3 DesignId:1 Classification: 1, PickZone: B2, CartonPropertyGroup: B2
//                CR4 : L150 W150 H150. SN:4 DesignId:1 Classification: 1, PickZone: B2, CartonPropertyGroup: B2
//             */

//            SetUpRequestSet2();

//            // 1. CartonPropertyGroup A1 Stopped, CR2,CR3,CR4 are surged CPG B1,B2
//            AddCartonPropertyGroupWithStoppedStatus("A1");
//            AddCartonPropertyGroupWithSurgedStatus("B1");
//            AddCartonPropertyGroupWithSurgedStatus("B2");

//            // 2. CR2,CR3,CR4 fit on exclusive fanfold
//            SetListOfExclusiveFanfoldsForMachine(testMachine1, new[] { testMachine1.Tracks.Select(t => t.LoadedCorrugate).ElementAt(0) });

//            // 4. Tiling - [CR2-CR3], [CR3-CR4], [CR2-CR4], [CR3-CR4]

//            // 5. Best yield [CR2-CR3] -> [CR2-CR4] -> [CR3-CR4]
//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            // Verify [CR2-CR3]
//            Specify.That(nextJob.Requests.Count()).Should.BeEqualTo(2);
//            Specify.That(nextJob.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(nextJob.Requests.ElementAt(1).SerialNumber).Should.BeEqualTo("3");

//            AssertThatCorrugatedWidthIsNotExceded(nextJob);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void NoJobAvailableForMachineGivesNull()
//        {
//            /*
//             * Machine 1 - FF 800(ex), FF 1200
//               Machine 2 - FF 1200
//             * 
//             * No CR
//             */

//            cartonRequestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(new List<ICarton>());

//            // Verify that null is returned
//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            Specify.That(nextJob).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CanNotSelectItSelfAsTilingPartner()
//        {
//            /*
//             *  Machine 1 - FF 800(ex), FF 1200
//                Machine 2 - FF 1200
//             * 
//                CR1 : L500 W500 H500. SN:1 DesignId:1 Classification: 1, CartonPropertyGroup: A1
//             * 
//             */

//            SetUpRequestSet3();
//            SetListOfExclusiveFanfoldsForMachine(testMachine1, new[] { testMachine1.Tracks.Select(t => t.LoadedCorrugate).ElementAt(0) });
//            AddCartonPropertyGroupWithNormalStatus("A1");
//            cartonRequestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder
//                {
//                    Requests = a,
//                    Corrugate = b,
//                    MachineId = c.Id,
//                    InternalPrinting = f,
//                    PackStation = h,
//                    Design = OrderHelpers.MergeDesignsSideBySide(mockCorrugateCalculationService.Object, a, b, c, e, g, i)
//                });

//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            // Verify CR1
//            Specify.That(nextJob.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("1");

//            Specify.That(nextJob.Requests.Count()).Should.BeEqualTo(1);
//            AssertThatCorrugatedWidthIsNotExceded(nextJob);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void Expedited()
//        {
//            /*
//             *  Machine 1 - FF 800(ex), FF 1200
//                Machine 2 - FF 1200
//             * 
//                CR1 : L350 W350 H350. SN:1 DesignId:1 Classification: 1, CPG: A1
//                CR2 : L500 W500 H500. SN:2 DesignId:1 Classification: 1, CPG: B1
//                CR3 : L350 W350 H350. SN:3 DesignId:1 Classification: 2, CPG: B2
//                CR4 : L150 W150 H150. SN:4 DesignId:1 Classification: 3, CPG: B2
//             */

//            SetUpRequestSet4();

//            AddCartonPropertyGroupWithNormalStatus("A1");
//            AddCartonPropertyGroupWithNormalStatus("B1");
//            AddCartonPropertyGroupWithNormalStatus("B2");

//            // 1. PickZ Normal, Classification 2 set to Expedite
//            SetClassificationStatus("2", ClassificationStatus.Expedite, false);

//            // 2. CR3 fit on exclusive fanfold
//            SetListOfExclusiveFanfoldsForMachine(testMachine1, new[] { testMachine1.Tracks.Select(t => t.LoadedCorrugate).ElementAt(0) });

//            // 4. Tiling - [CR3-CR4], [CR3-CR2], [CR3-CR1]
//            // 5. Best yield [CR3-CR2] or [CR3-CR1]?
//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            // Verify [CR3-CR1]
//            Specify.That(nextJob.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(nextJob.Requests.ElementAt(1).SerialNumber).Should.BeEqualTo("1");

//            Specify.That(nextJob.Requests.Count()).Should.BeEqualTo(2);
//            AssertThatCorrugatedWidthIsNotExceded(nextJob);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnNullIfAllRequestAreTaken()
//        {
//            /*
//             *  Machine 1 - FF 800(ex), FF 1200
//                Machine 2 - FF 1200
//             * 
//                CR1 : L500 W500 H500. SN:1 DesignId:1 Classification: 1, CPG: G1
//                CR2 : L500 W500 H500. SN:2 DesignId:1 Classification: 1, CPG: G1
//                CR3 : L250 W250 H250. SN:3 DesignId:1 Classification: 1, CPG: G1
//             */

//            SetUpRequestSet6();

//            AddCartonPropertyGroupWithNormalStatus("G1");

//            // 1. exclusive fanfold
//            SetListOfExclusiveFanfoldsForMachine(testMachine1, new[] { testMachine1.Tracks.Select(t => t.LoadedCorrugate).ElementAt(0) });

//            // 2. Tiling
//            // 3. Best yield
//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            Specify.That(nextJob).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnSecondBestIfBestIsTaken()
//        {
//            /*
//             *  Machine 1 - FF 800(ex), FF 1200
//                Machine 2 - FF 1200
//             * 
//                CR1 : L800 W800 H800. SN:1 DesignId:1 Classification: 1, CPG: G1
//                CR2 : L700 W700 H700. SN:2 DesignId:1 Classification: 1, CPG: G1
//                CR3 : L600 W600 H600. SN:3 DesignId:1 Classification: 1, CPG: G1
//             */

//            SetUpRequestSet5();
//            AddCartonPropertyGroupWithNormalStatus("G1");
//            SetListOfExclusiveFanfoldsForMachine(testMachine1, new[] { testMachine1.Tracks.Select(t => t.LoadedCorrugate).ElementAt(0) });
//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);
//            Specify.That(nextJob.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotEvaluateInValidRequests()
//        {
//            SetUpRequestSet7();

//            SetListOfExclusiveFanfoldsForMachine(testMachine1, new[] { testMachine1.Tracks.Select(t => t.LoadedCorrugate).ElementAt(0) });

//            var nextJob = requestSelector.GetNextJobForMachine(testMachine1);

//            Specify.That(nextJob).Should.BeNull();
//        }

//        private void AddCartonPropertyGroupWithStoppedStatus(string alias)
//        {
//            AddCartonPropertyGroupToList(alias, stoppedCpgs);
//        }

//        private void AddCartonPropertyGroupWithSurgedStatus(string alias)
//        {
//            AddCartonPropertyGroupToList(alias, surgedCpgs);
//        }

//        private void AddCartonPropertyGroupWithNormalStatus(string alias)
//        {
//            AddCartonPropertyGroupToList(alias, normalCpgs);
//        }

//        private void AddCartonPropertyGroupToList(string alias, List<CartonPropertyGroup> list)
//        {
//            var group = new Mock<CartonPropertyGroup>();
//            group.SetupGet(g => g.Alias).Returns(alias);
//            list.Add(group.Object);
//            groupNames.Enqueue(alias);
//        }

//        private void SetListOfExclusiveFanfoldsForMachine(IPacksizeCutCreaseMachine machine, IEnumerable<object> fanfolds)
//        {
//            var exclusiveFanfolds = fanfolds.Cast<Corrugate>().ToList();

//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine, new[] { new CommonProductionGroups.ProductionGroup() })).Returns(exclusiveFanfolds);
//        }

//        private void SetPickZoneStatus(string pickZoneId, ZoneStatus zoneStatus)
//        {
//            var selectedPickZone = pickAreaManager.Object.PickZones.SingleOrDefault(pickZone => pickZone.PickZoneId == pickZoneId);

//            selectedPickZone.Status = zoneStatus;
//        }

//        private void AssertThatCorrugatedWidthIsNotExceded(ICartonRequestOrder order)
//        {
//            var corrugated = order.Corrugate;

//            Specify.That(order.Design.WidthOnZFold.Solve(new Dictionary<string, object>()) <= corrugated.Width).Should.BeTrue();
//        }

//        private void SetUpRequestWithDifferentCpg()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            ClassificationNumber = "1",
//                            PickZone = "A1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Length = 850,
//                            Width = 850,
//                            Height = 850,
//                            ClassificationNumber = "1",
//                            PickZone = "A2",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G2"
//                        }
//                };

//            cartonRequestManager
//                .Setup(manager => manager.ValidRequestsInActivePickzones)
//                .Returns(listOfRequests);
//            cartonRequestManager
//                .Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetUpRequestSet1()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            PickZone = "A1",
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "A1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Length = 850,
//                            Width = 850,
//                            Height = 850,
//                            PickZone = "A2",
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "A2"
//                        }
//                };

//            cartonRequestManager
//                .Setup(manager => manager.ValidRequestsInActivePickzones)
//                .Returns(listOfRequests);
//            cartonRequestManager
//                .Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder
//                {
//                    Requests = a,
//                    Corrugate = b,
//                    MachineId = c.Id,
//                    InternalPrinting = f,
//                    PackStation = h,
//                    SerialNo = CartonRequestOrder.CreateSerialNumber(a),
//                    Design = OrderHelpers.MergeDesignsSideBySide(mockCorrugateCalculationService.Object, a, b, c, e, g, i)
//                });
//        }

//        private void SetUpRequestSet2()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            ClassificationNumber = "1",
//                            PickZone = "A1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "A1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Length = 400,
//                            Width = 400,
//                            Height = 400,
//                            ClassificationNumber = "1",
//                            PickZone = "B1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "B1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "3",
//                            Length = 400,
//                            Width = 400,
//                            Height = 400,
//                            ClassificationNumber = "1",
//                            PickZone = "B2",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "B2"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "4",
//                            Length = 150,
//                            Width = 150,
//                            Height = 150,
//                            ClassificationNumber = "1",
//                            PickZone = "B2",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "B2"
//                        }
//                };

//            cartonRequestManager
//                .Setup(manager => manager.ValidRequestsInActivePickzones)
//                .Returns(listOfRequests);
//            cartonRequestManager
//                .Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder
//                {
//                    Requests = a,
//                    Corrugate = b,
//                    MachineId = c.Id,
//                    InternalPrinting = f,
//                    PackStation = h,
//                    SerialNo = CartonRequestOrder.CreateSerialNumber(a),
//                    Design = OrderHelpers.MergeDesignsSideBySide(mockCorrugateCalculationService.Object, a, b, c, e, g, i)
//                });
//        }

//        private void SetUpRequestSet3()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                    {
//                            SerialNumber = "1",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            ClassificationNumber = "1",
//                            CartonPropertyGroupAlias = "A1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true
//                    }
//                };

//            cartonRequestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(listOfRequests);
//            cartonRequestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetUpRequestSet4()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Length = 350,
//                            Width = 350,
//                            Height = 350,
//                            ClassificationNumber = "1",
//                            CartonPropertyGroupAlias = "A1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            ClassificationNumber = "1",
//                            CartonPropertyGroupAlias = "B1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "3",
//                            Length = 350,
//                            Width = 350,
//                            Height = 350,
//                            ClassificationNumber = "2",
//                            CartonPropertyGroupAlias = "B2",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "4",
//                            Length = 150,
//                            Width = 150,
//                            Height = 150,
//                            ClassificationNumber = "3",
//                            CartonPropertyGroupAlias = "B2",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true
//                        }
//                };

//            cartonRequestManager
//                .Setup(manager => manager.ValidRequestsInActivePickzones)
//                .Returns(listOfRequests);
//            cartonRequestManager
//                .Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder
//                {
//                    Requests = a,
//                    Corrugate = b,
//                    MachineId = c.Id,
//                    InternalPrinting = f,
//                    PackStation = h,

//                    SerialNo = CartonRequestOrder.CreateSerialNumber(a),
//                    Design = OrderHelpers.MergeDesignsSideBySide(mockCorrugateCalculationService.Object, a, b, c, e, g, i)
//                });
//        }

//        private void SetUpRequestSet5()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Length = 800,
//                            Width = 800,
//                            Height = 800,
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Length = 700,
//                            Width = 700,
//                            Height = 700,
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "3",
//                            Length = 600,
//                            Width = 600,
//                            Height = 600,
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        }
//                };

//            cartonRequestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(listOfRequests);
//            cartonRequestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(r => r.ElementAt(0).SerialNumber == "1"), It.IsAny<Guid?>())).Returns(false);
//            cartonRequestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(r => r.ElementAt(0).SerialNumber != "1"), It.IsAny<Guid?>())).Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetUpRequestSet6()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "3",
//                            Length = 250,
//                            Width = 250,
//                            Height = 250,
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        }
//                };

//            cartonRequestManager
//                .Setup(manager => manager.ValidRequestsInActivePickzones)
//                .Returns(listOfRequests);
//            cartonRequestManager
//                .Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Returns(false);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetUpRequestSet7()
//        {
//            var listOfRequests = new List<ICarton>();

//            // 2011-11-11: ValidRequestsInActivePickzones will never return Invalid requests
//            // listOfRequests.Add(new Carton() { SerialNumber = "1", Length = 500, Width = 500, Height = 500, ClassificationNumber = "1", PickZone = "A1", DesignId = 1, CorrugateQuality = 1, IsValid = false });
//            // listOfRequests.Add(new Carton() { SerialNumber = "2", Length = 850, Width = 850, Height = 850, ClassificationNumber = "1", PickZone = "A1", DesignId = 1, CorrugateQuality = 1, IsValid = false });
//            // listOfRequests.Add(new Carton() { SerialNumber = "3", Length = 280, Width = 280, Height = 280, ClassificationNumber = "1", PickZone = "A1", DesignId = 1, CorrugateQuality = 1, IsValid = false });
//            cartonRequestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(listOfRequests);
//            cartonRequestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetUpRequestSet8()
//        {
//            var listOfRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Length = 500,
//                            Width = 500,
//                            Height = 500,
//                            ClassificationNumber = "1",
//                            PickZone = "A1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "A1",
//                            MachineId = machine4Id,
//                            Custom = true
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Length = 400,
//                            Width = 400,
//                            Height = 400,
//                            ClassificationNumber = "1",
//                            PickZone = "B1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "B1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "3",
//                            Length = 400,
//                            Width = 400,
//                            Height = 400,
//                            ClassificationNumber = "1",
//                            PickZone = "B2",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "B2"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "4",
//                            Length = 150,
//                            Width = 150,
//                            Height = 150,
//                            ClassificationNumber = "1",
//                            PickZone = "B2",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "B2"
//                        }
//                };

//            cartonRequestManager
//                .Setup(manager => manager.ValidRequestsInActivePickzones)
//                .Returns(listOfRequests);
//            cartonRequestManager
//                .Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder
//                {
//                    Requests = a,
//                    Corrugate = b,
//                    MachineId = c.Id,
//                    InternalPrinting = f,
//                    PackStation = h,
//                    SerialNo = CartonRequestOrder.CreateSerialNumber(a),
//                    Design = OrderHelpers.MergeDesignsSideBySide(mockCorrugateCalculationService.Object, a, b, c, e, g, i)
//                });
//        }

//        private void SetupProductionGroupManagerMock()
//        {
//            sequenceGroup = new Mock<CommonProductionGroups.ProductionGroup>();
//            groupNames = new Queue<string>();

//            sequenceGroupManager = new Mock<IProductionGroupManager>();
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(sequenceGroup.Object);
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(new[] { sequenceGroup.Object });
//        }

//        private IEnumerable<ProductionItem> GetAllProductionItems()
//        {
//            var items = stoppedCpgs.Select(@group => new ProductionItem(@group.Alias, 1)).ToList();
//            items.AddRange(normalCpgs.Select(@group => new ProductionItem(@group.Alias, 1)));
//            items.AddRange(surgedCpgs.Select(@group => new ProductionItem(@group.Alias, 1)));

//            return items;
//        }

//        private string ProductionGroupMix(IEnumerable<string> groups, List<string> filter)
//        {
//            var groupsToSelectFrom = groups.Where(g => filter.Contains(g) == false);
//            var group = groupsToSelectFrom.FirstOrDefault();

//            if (string.IsNullOrEmpty(group))
//            {
//                group = string.Empty;
//            }

//            return group;
//        }

//        private void SetClassificationStatus(string number, ClassificationStatus classificationStatus, bool isPriorityLabel)
//        {
//            var classificationToChange = classificationsManager.Object.Classifications.SingleOrDefault(classification => classification.Number == number);

//            classificationToChange.Status = classificationStatus;
//            classificationToChange.IsPriorityLabel = isPriorityLabel;
//        }

//        private IEnumerable<IPacksizeCutCreaseMachine> SetUpMachines()
//        {
//            var listOfMachines = new List<IPacksizeCutCreaseMachine>();

//            var machine1 = new Mock<IPacksizeCutCreaseMachine>();

//            var fanfolds = new List<Corrugate>
//                {
//                    new Corrugate { Width = 800, Thickness = 1, Quality = 1 },
//                    new Corrugate { Width = 1200, Thickness = 1, Quality = 1 }
//                };
//            allCorrugates.AddRange(fanfolds);

//            machine1.SetupGet(machine => machine.Id).Returns(machine1Id);
//            machine1.Setup(machine => machine.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = fanfolds.First()},
//                new Track{ TrackNumber = 2, LoadedCorrugate = fanfolds.Last()},
//            });

//            //machine1.Setup(machine => machine.CurrentProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            var machine2 = new Mock<IPacksizeCutCreaseMachine>();

//            fanfolds = new List<Corrugate> { new Corrugate { Width = 1200, Thickness = 1, Quality = 1 } };
//            allCorrugates.AddRange(fanfolds);

//            machine2.SetupGet(machine => machine.Id).Returns(machine2Id);
//            machine2.Setup(machine => machine.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = fanfolds.First()},
//            });
//            //machine2.Setup(machine => machine.CurrentProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            listOfMachines.Add(machine1.Object);
//            listOfMachines.Add(machine2.Object);

//            return listOfMachines;
//        }
    }
}
