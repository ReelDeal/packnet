﻿Feature: Priority order of different carton properties
	In order to get my production right
	As a production manager
	I want the production order to be based on a few properties

@CartonRequestSelectorPriorityOrder
Scenario: Surged Carton Property Group is first priority
	Given I have a carton C1 to classification 1, carton property group 1, pickzone 1, suitable for corrugate 1
	And I have a carton C2 to classiciation 2, carton property group 2, pickzone 1, suitable for corrugate 2
	And carton property group 1 has surged status
	And carton property group 2 has normal status
	And classification 1 has expedite status
	And classification 2 has normal status
	And carton property group 2 is next in sequence mix
	And corrugate 2 is more exclusive than corrugate 1
	And C2 gives better yield than C1
	When next carton is requested by a machine
	Then C1 is returned as next carton

@CartonRequestSelectorPriorityOrder
Scenario: Expedited Classification is second priority
	Given I have a carton C1 to classification 1, carton property group 1, pickzone 1, suitable for corrugate 1
	And I have a carton C2 to classiciation 2, carton property group 2, pickzone 1, suitable for corrugate 2
	And carton property group 1 has normal status
	And carton property group 2 has normal status
	And classification 1 has expedite status
	And classification 2 has normal status
	And carton property group 2 is next in sequence mix
	And corrugate 2 is more exclusive than corrugate 1
	And C2 gives better yield than C1
	When next carton is requested by a machine
	Then C1 is returned as next carton
	
@CartonRequestSelectorPriorityOrder
Scenario: Carton Property Group mix is third priority
	Given I have a carton C1 to classification 1, carton property group 1, pickzone 1, suitable for corrugate 1
	And I have a carton C2 to classiciation 2, carton property group 2, pickzone 1, suitable for corrugate 2
	And carton property group 1 has normal status
	And carton property group 2 has normal status
	And classification 1 has normal status
	And classification 2 has normal status
	And carton property group 1 is next in sequence mix
	And corrugate 2 is more exclusive than corrugate 1
	And C2 gives better yield than C1
	When next carton is requested by a machine
	Then C1 is returned as next carton
	
@CartonRequestSelectorPriorityOrder
Scenario: Corrugate exclusiveness is fourth priority
	Given I have a carton C1 to classification 1, carton property group 1, pickzone 1, suitable for corrugate 1
	And I have a carton C2 to classiciation 2, carton property group 1, pickzone 1, suitable for corrugate 2
	And carton property group 1 has normal status
	And classification 1 has normal status
	And classification 2 has normal status
	And carton property group 1 is next in sequence mix
	And corrugate 2 is more exclusive than corrugate 1
	And C2 gives better yield than C1
	When next carton is requested by a machine
	Then C2 is returned as next carton
	
@CartonRequestSelectorPriorityOrder
Scenario: Yield is fifth priority
	Given I have a carton C1 to classification 1, carton property group 1, pickzone 1, suitable for corrugate 1
	And I have a carton C2 to classiciation 2, carton property group 1, pickzone 1, suitable for corrugate 1
	And carton property group 1 has normal status
	And classification 1 has normal status
	And classification 2 has normal status
	And carton property group 1 is next in sequence mix
	And corrugate 2 is more exclusive than corrugate 1
	And C2 gives better yield than C1
	When next carton is requested by a machine
	Then C2 is returned as next carton
	
@CartonRequestSelectorPriorityOrder
Scenario: Age is sixth priority
	Given I have a carton C1 to classification 1, carton property group 1, pickzone 1, suitable for corrugate 1
	And I have a carton C2 to classiciation 2, carton property group 1, pickzone 1, suitable for corrugate 1
	And carton property group 1 has normal status
	And classification 1 has normal status
	And classification 2 has normal status
	And carton property group 1 is next in sequence mix
	And corrugate 2 is more exclusive than corrugate 1
	When next carton is requested by a machine
	Then C1 is returned as next carton