﻿//using PackNet.Business.Corrugates;
//using PackNet.Business.PickZone;
//using PackNet.Common.Eventing;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.IO;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.PickZones;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Eventing;
//    using PackNet.Common.Interfaces.Services;
//    using PackNet.Common.Logging;
//    using PackNet.Common.Logging.NLogBackend;
//    using PackNet.Data.PickArea;
//    using PackNet.Data.ProductionGroups;

//    using Testing.Specificity;

//    [Ignore]
    [TestClass]
    public class PerfomanceTests
    {
#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
        //        private readonly TimeSpan tenSeconds = new TimeSpan(0, 0, 10);
//        private SelectorSettings settings;
//        private Mock<ICartonRequestManager> requestManagerMock;
//        private IPackagingDesignManager designManager;
//        private Mock<IClassificationsManager> classificationsManagerMock;
//        private IPickZoneManager pickZoneManager;
//        private IMachinesManager machinesManager;
//        private IProductionGroupManager sequenceGroupManager;
//        private IPacksizeCutCreaseMachine machine;
//        private ILogger logger;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private EventAggregator eventAggregator = new EventAggregator();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            SetupDesignManager();
//            SetupCartonSelectionSettings();
//            SetupMachinesManager();
//            SetupProductionGroupManager();
//            SetupClassificationsManager();
//            SetupPickAreaManager();
//            SetupRequestManager();
//            SetUpLogging();
//            mockCorrugateCalculationService = new Mock<ICorrugateService>();
//        }

//        [TestMethod]
//        [Description("Integration with environment")]
//        [TestCategory("Integration")]
//        public void RealWorldScenario()
//        {
//            for (int i = 0; i < 180000; i++)
//            {
//                var requests = GenerateRequests();
//                requestManagerMock.SetupGet(rm => rm.ValidRequestsInActivePickzones).Returns(requests);

//                var selector = new NonParallelWaveCartonRequestSelector(
//                    mockCorrugateCalculationService.Object,
//                    requestManagerMock.Object,
//                    classificationsManagerMock.Object,
//                    pickZoneManager,
//                    machinesManager,
//                    sequenceGroupManager,
//                    new Mock<ICartonPropertyGroups>().Object,
//                    designManager,
//                    settings,
//                    new Mock<ILongHeadPositioningValidation>().Object,
//                    new WmsConfiguration(),
//                    new Mock<ILogger>(MockBehavior.Loose).Object);

//                var start = DateTime.Now;
//                selector.GetNextJobForMachine(machine);
//            }
//        }

//        [TestMethod]
//        [Description("Integration with environment")]
//        [TestCategory("Integration")]
//        public void WorstCaseScenario()
//        {
//            var requests = GenerateRequests();
//            requestManagerMock.SetupGet(rm => rm.ValidRequestsInActivePickzones).Returns(requests);

//            var selector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                requestManagerMock.Object,
//                classificationsManagerMock.Object,
//                pickZoneManager,
//                machinesManager,
//                sequenceGroupManager,
//                new Mock<ICartonPropertyGroups>().Object,
//                designManager,
//                settings,
//                new Mock<ILongHeadPositioningValidation>().Object,
//                new WmsConfiguration(),
//                new Mock<ILogger>(MockBehavior.Loose).Object);

//            var start = DateTime.Now;
//            selector.GetNextJobForMachine(machine);
//            var elapsedTime = DateTime.Now - start;
//            Specify.That(elapsedTime < tenSeconds).Should.BeTrue();
//        }

//        private void SetupPickAreaManager()
//        {
//            pickZoneManager = new PickZoneManager(new Mock<IPickZoneRepository>().Object);
//        }

//        private void SetupRequestManager()
//        {
//            requestManagerMock = new Mock<ICartonRequestManager>();
//            requestManagerMock.Setup(rm => rm.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//        }

//        private IEnumerable<ICarton> GenerateRequests()
//        {
//            var requests = new List<ICarton>();
//            var files = Directory.GetFiles("Requests");

//            foreach (var file in files)
//            {
//                var lines = File.ReadAllLines(file);
//                foreach (var line in lines)
//                {
//                    var parts = line.Split(';');
//                    var request = new Carton
//                        {
//                            DesignId = 1,
//                            SerialNumber = parts[1],
//                            PickZone = parts[4],
//                            Length = double.Parse(parts[8].Replace('.', ',')),
//                            Width = double.Parse(parts[10].Replace('.', ',')),
//                            Height = double.Parse(parts[12].Replace('.', ',')),
//                            ClassificationNumber = parts[14],
//                            CorrugateQuality = parts[2] == "101" ? 2 : 1,
//                            IsValid = true,
//                            CartonPropertyGroupAlias = "G1"
//                        };
//                    requests.Add(request);
//                }
//            }

//            return requests;
//        }

//        private void SetupClassificationsManager()
//        {
//            classificationsManagerMock = new Mock<IClassificationsManager>();
//            classificationsManagerMock.SetupGet(cm => cm.Classifications).Returns(new[] { new PackNet.Common.Interfaces.DTO.Classifications.Classification("1") });
//        }

//        private void SetupMachinesManager()
//        {
//            machinesManager = new MachinesManager(new Mock<ICorrugates>().Object, eventAggregator, eventAggregator);
//            machine = machinesManager.Machines.Skip(2).First();
//        }

//        private void SetupProductionGroupManager()
//        {
//            var eventAggSubscriber = new Mock<IEventAggregatorSubscriber>();
//            var eventAggPublisher = new Mock<IEventAggregatorPublisher>();
//            var repo = new Mock<IProductionGroupRepository>();
//            var corrugates = new Mock<ICorrugates>();
//            sequenceGroupManager = new ProductionGroupManager(new ProductionGroup(eventAggSubscriber.Object, repo.Object, corrugates.Object, logger), null, logger, eventAggPublisher.Object);
//        }

//        private void SetupCartonSelectionSettings()
//        {
//            settings = new SelectorSettings(2, true, 40, 40, 0);
//        }

//        private void SetupDesignManager()
//        {
//            var design = new PackagingDesign { LengthOnZFold = ("L"), WidthOnZFold = ("W") };

//            var aline = new Line { StartX = "0", StartY = "0", EndX = "0", EndY = "L" };
//            design.Lines.Add(aline);

//            aline = new Line { StartX = "0", StartY = "0", EndX = "0", EndY = "W" };
//            design.Lines.Add(aline);

//            aline = new Line { StartX = "W", StartY = "0", EndX = "W", EndY = "L" };
//            design.Lines.Add(aline);

//            aline = new Line { StartX = "L", StartY = "0", EndX = "L", EndY = "W" };
//            design.Lines.Add(aline);

//            designManager = new PackagingDesignManager("Designs", new Mock<ILogger>(MockBehavior.Loose).Object);
//            designManager.LoadDesigns();
//        }

//        private void SetUpLogging()
//        {
//            LogManager.LoggingBackend = new NLogBackend("MachineManager", ".\\NLog.config");
//            logger = LogManager.GetLogFor("ProductionGroupManager");
//        }

//        private class PickAreaProvider : IPickAreaProvider
//        {
//            private readonly List<PickArea> pickareas;

//            public PickAreaProvider()
//            {
//                pickareas = new List<PickArea>();
//                var pickZoneNames = "0312;0022;0413;0114;0323;0212;0222;0314;0024;0023;0414;0214;0321;0111;0424;0224;0021;0113;0221;0311;0211;0112;0422;0223;0213;0412;0322;0423;0421;0313;0411;0324".Split(';');
//                var pickZones = pickZoneNames.Select(name => new PickZone(name)).ToList();

//                pickareas.Add(new PickArea("1", pickZones));
//            }

//            public IEnumerable<PickArea> PickAreas
//            {
//                get { return pickareas; }
//            }
//        }
    }
}
