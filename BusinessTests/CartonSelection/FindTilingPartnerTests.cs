﻿//using System;
//using PackNet.Business.Classifications;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Machines;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Services;

//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;
//    using Testing.Specificity;

    [TestClass]
    public class FindTilingPartnerTests
    {
#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
        //        private WaveCartonRequestSelector selector;
//        private IPacksizeCutCreaseMachine machine;
//        private Mock<IMachinesManager> machinesManagerMock;
//        private Mock<IProductionGroupManager> sequenceGroupManagerMock;
//        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;
//        private Mock<IPackagingDesignManager> designManagerMock;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private Mock<IClassificationsManager> classificationsMock;
//        private Guid machine1Id = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            var machineMock = new Mock<IPacksizeCutCreaseMachine>();
//            machineMock.Setup(m => m.Id).Returns(machine1Id);
//            machineMock.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = new Corrugate { Width = 900, Thickness = 1, Quality = 1 } },
//                new Track{ TrackNumber = 2, LoadedCorrugate = new Corrugate { Width = 510, Thickness = 1, Quality = 1 } },
//                new Track{ TrackNumber = 3, LoadedCorrugate = new Corrugate { Width = 2000, Thickness = 1, Quality = 1 } },
//                new Track{ TrackNumber = 4, LoadedCorrugate = new Corrugate { Width = 300, Thickness = 1, Quality = 1 } }, 
//                new Track{ TrackNumber = 5, LoadedCorrugate = new Corrugate { Width = 900, Thickness = 1, Quality = 2} }
//            });
//            machine = machineMock.Object;

//            var sequenceGroup = new CommonProductionGroups.ProductionGroup
//            {
//                //Machines = new List<MachineInformation> { new MachineInformation(1111) }
//            };
//            sequenceGroupManagerMock = new Mock<IProductionGroupManager>();
//            sequenceGroupManagerMock.Setup(m => m.GetProductionGroupForMachine(machine.Id)).Returns(sequenceGroup);
//            sequenceGroupManagerMock.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(new[] { sequenceGroup });

//            machinesManagerMock = new Mock<IMachinesManager>();
//            machinesManagerMock.SetupGet(m => m.Machines).Returns(new List<IPacksizeCutCreaseMachine> { machine });

//            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();
         
//            var packagingDesignOne = new PackagingDesign { Name = "1", Id = 1, LengthOnZFold = "L", WidthOnZFold = "W" };
//            designManagerMock = new Mock<IPackagingDesignManager>();
//            designManagerMock.Setup(m => m.GetDesign(It.IsAny<DesignQueryParam>())).Returns(packagingDesignOne);

//            SetupCorrugateCalculationsServiceMock();

//            SetupClassificationsMock();

//            selector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                new Mock<ICartonRequestManager>().Object,
//                classificationsMock.Object,
//                null,
//                machinesManagerMock.Object,
//                sequenceGroupManagerMock.Object,
//                null,
//                designManagerMock.Object,
//                new SelectorSettings(10, true, 40, 40, 0),
//                longHeadPositioningValidation.Object,
//                new WmsConfiguration(), 
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//        }

//        private void SetupClassificationsMock()
//        {
//            classificationsMock = new Mock<IClassificationsManager>();

//        }

//        private void SetupCorrugateCalculationsServiceMock()
//        {
//            mockCorrugateCalculationService = new Mock<ICorrugateService>();

//            mockCorrugateCalculationService.Setup(
//               m =>
//                   m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                       It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//               .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//               ((a, b, c, d, e) => a.Width != null && a.Width.Value < b.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Length);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null ? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width
//                });

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRemoveThatIsBetterOnOtherCorrugated()
//        {
//            var requests = new[]
//                {
//                    new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 } 
//                };
//            var result = selector.FindTilingPartners(
//                requests, new ICarton[0], machine, machine.Tracks.ElementAt(0).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(0);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTileFour()
//        {
//            var requests = new[] { new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 } };
//            var result = selector.FindTilingPartners(requests, GetSomePartners(), machine, machine.Tracks.ElementAt(2).LoadedCorrugate);

//            Specify.That(result.First().MainAndTiles.Count()).Should.BeEqualTo(4);
//        }
        
//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotTileWhenTilingPartnerCollectionIsNull()
//        {
//            var requests = new[] { new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 } };
//            var result = selector.FindTilingPartners(requests, null, machine, machine.Tracks.ElementAt(1).LoadedCorrugate);

//            Specify.That(result.First().MainAndTiles.Count()).Should.BeEqualTo(1);
//        }
        
//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotTileWhenTilingPartnerCollectionIsEmpty()
//        {
//            var requests = new[] { new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 } };
//            var result = selector.FindTilingPartners(requests, new ICarton[0], machine, machine.Tracks.ElementAt(1).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//        }
        
//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnEmptyListIfRequestIsMoreSuitbaleAsTiledOnOtherCorrugate()
//        {
//            var requests = new[] { new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 } };
//            var result = selector.FindTilingPartners(requests, GetSomePartners(), machine, machine.Tracks.ElementAt(1).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(0);
//        }
        
//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotReturnSelfOnToNarrowCorrugate()
//        {
//            var requests = new[] { new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 } };

//            var result = selector.FindTilingPartners(requests, null, machine, machine.Tracks.ElementAt(3).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(0);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnEmptyListForNoProducable()
//        {
//            var requests = new[] { new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 2 } };
//            var result = selector.FindTilingPartners(requests, GetSomePartners(), machine, machine.Tracks.ElementAt(1).LoadedCorrugate);
//            Specify.That(result.Count()).Should.BeEqualTo(0);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldOnlyTileEqual()
//        {
//            var requests = new[] { new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 1 } };

//            var result = selector.FindTilingPartners(requests, GetSomePartners(), machine, machine.Tracks.ElementAt(1).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.First().NumberOfCartons).Should.BeEqualTo(2);
//            Specify.That(result.First().MainAndTiles.ElementAt(0).DesignId).Should.BeEqualTo(1);
//            Specify.That(result.First().MainAndTiles.ElementAt(1).DesignId).Should.BeEqualTo(1);
//            Specify.That(result.First().Corrugate).Should.BeEqualTo(machine.Tracks.ElementAt(1));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldOnlyTileRequestsForSamePackStation()
//        {
//            var requests = new [] {new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 2, PackStation = 1 } };
//            var result = selector.FindTilingPartners(requests, GetSomeRequestsWherePackStationIsSet(), machine, machine.Tracks.ElementAt(4).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.First().NumberOfCartons).Should.BeEqualTo(3);
//            Specify.That(result.First().MainAndTiles.ElementAt(0).PackStation).Should.BeEqualTo(1);
//            Specify.That(result.First().Corrugate).Should.BeEqualTo(machine.Tracks.ElementAt(4));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotTilePartnersWithPackStationIdIfMainRequestsPackStationIsNull()
//        {
//            var requests = new[] { new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 2 } };
//            var result = selector.FindTilingPartners(requests, GetSomeRequestsWherePackStationIsSet(), machine, machine.Tracks.ElementAt(4).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.First().NumberOfCartons).Should.BeEqualTo(1);
//            Specify.That(result.First().MainAndTiles.ElementAt(0).PackStation.HasValue).Should.BeFalse();
//            Specify.That(result.First().Corrugate).Should.BeEqualTo(machine.Tracks.ElementAt(4));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTileWithDifferentPriorityPrinterRequestIfShouldPrintLabelIsFalseOnBothRequests()
//        {
//            var requests = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = false, StoredIsPriorityLabel = false } };
//            var tilingPartners = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = false, StoredIsPriorityLabel = true } };
//            var result = selector.FindTilingPartners(requests, tilingPartners, machine, machine.Tracks.ElementAt(0).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.ElementAt(0).MainAndTiles.Count()).Should.BeEqualTo(2);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotTileWithDifferentPriorityPrinterRequestIfShouldPrintLabelIsTrueOnBothRequests()
//        {
//            var requests = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = true, StoredIsPriorityLabel = false } };
//            var tilingPartners = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = true, StoredIsPriorityLabel = true } };
//            var result = selector.FindTilingPartners(requests, tilingPartners, machine, machine.Tracks.ElementAt(1).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.ElementAt(0).MainAndTiles.Count()).Should.BeEqualTo(1);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTileWithSamePriorityPrinterRequestIfShouldPrintLabelIsTrueOnBothRequests()
//        {
//            var requests = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = true, StoredIsPriorityLabel = true } };
//            var tilingPartners = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = true, StoredIsPriorityLabel = true } };
//            var result = selector.FindTilingPartners(requests, tilingPartners, machine, machine.Tracks.ElementAt(0).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.ElementAt(0).MainAndTiles.Count()).Should.BeEqualTo(2);

//            requests = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = true, StoredIsPriorityLabel = false } };
//            tilingPartners = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = true, StoredIsPriorityLabel = false } };
//            result = selector.FindTilingPartners(requests, tilingPartners, machine, machine.Tracks.ElementAt(0).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.ElementAt(0).MainAndTiles.Count()).Should.BeEqualTo(2);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotTileWithPrintLabelRequestWithNonPrintLabelRequest()
//        {
//            var requests = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = false, StoredIsPriorityLabel = true } };
//            var tilingPartners = new[] { new Carton { Length = 450, Width = 450, Height = 450, DesignId = 1, CorrugateQuality = 1, ShouldPrintLabel = true, StoredIsPriorityLabel = true } };
//            var result = selector.FindTilingPartners(requests, tilingPartners, machine, machine.Tracks.ElementAt(1).LoadedCorrugate);

//            Specify.That(result.Count()).Should.BeEqualTo(1);
//            Specify.That(result.ElementAt(0).MainAndTiles.Count()).Should.BeEqualTo(1);
//        }

//        private IEnumerable<ICarton> GetSomeRequestsWherePackStationIsSet()
//        {
//            return new ICarton[]
//                {
//                    new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 2, PackStation = 1 },
//                    new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 2, PackStation = 1 },
//                    new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 2, PackStation = 2 }
//                };
//        }
        
//        private IEnumerable<ICarton> GetSomePartners()
//        {
//            return new ICarton[]
//                {
//                    new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 },
//                    new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 },
//                    new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 },
//                    new Carton { Length = 500, Width = 500, Height = 500, DesignId = 1, CorrugateQuality = 1 },
//                    new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 1 },
//                    new Carton { Length = 200, Width = 200, Height = 200, DesignId = 1, CorrugateQuality = 2 }
//                };
//        }
    }
}
