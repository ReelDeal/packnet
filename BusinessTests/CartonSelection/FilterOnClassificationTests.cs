﻿//using PackNet.Business.PickZone;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Enums;
//    using PackNet.Common.Interfaces.Services;
//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

//    using Testing.Specificity;

    [TestClass]
    public class FilterOnClassificationTests
    {
#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
        //        private WaveCartonRequestSelector selector;
//        private IList<ICarton> requests;
//        private IList<Corrugate> corrugates;
//        private Mock<IMachinesManager> machinesManager;
//        private Mock<ICartonRequestManager> requestManager;
//        private Mock<IPickZoneManager> pickAreaManager;
//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
//        private ICarton request1;
//        private ICarton request2;
//        private ICarton request3;
//        private ICarton request4;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IProductionGroupManager> sequenceGroupManager;
//        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup;
//        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;
//        private Mock<IPackagingDesignManager> designManagerMock;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private Guid machine1Id = Guid.NewGuid();
//        private Guid machine2Id = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            SetupProductionGroupManagerMock();
//            SetUpMachinesManagerMock();
//            SetUpRequests();
//            SetupCartonRequestManagerMock();
//            SetupPickAreaManagerMock();
//            SetupClassificationsManagerMock();
//            SetupCartonPropertyGroupManagerMock();
//            SetupServerWrapperMock();
//            SetupDesignManagerMock();
//            SetupCorrugateServiceMock();

//            selector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                requestManager.Object,
//                classificationsManager.Object,
//                pickAreaManager.Object,
//                machinesManager.Object,
//                sequenceGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManagerMock.Object, 
//                new SelectorSettings(2, true, 40, 40, 0),
//                longHeadPositioningValidation.Object,
//                new WmsConfiguration(), 
//               new Mock<ILogger>(MockBehavior.Loose).Object);
//        }

//        private void SetupCorrugateServiceMock()
//        {
//            mockCorrugateCalculationService = new Mock<ICorrugateService>();

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>())).Returns(true);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Length);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width
//                });

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));
//        }

//        private void SetupDesignManagerMock()
//        {
//            var packagingDesignOne = new PackagingDesign { Name = "1", Id = 1, LengthOnZFold = "L", WidthOnZFold = "W" };
//            designManagerMock = new Mock<IPackagingDesignManager>();
//            designManagerMock.Setup(m => m.GetDesign(It.IsAny<DesignQueryParam>())).Returns(packagingDesignOne);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTakeExpeditedFirst()
//        {
//            SetUpRequestsForTakeExpeditedTest();
//            var result = selector.GetNextJobByClassification(requests, CartonPropertyGroupStatus.Normal, machinesManager.Object.Machines.First());
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("1");
//        }
        
//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTakeNormalJobIfExpeditedIsNotOptimalOnMachine()
//        {
//            SetCartonRequestWidthsForNotOptimalTest();
            
//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(3).Status = ClassificationStatus.Normal;

//            var result = selector.GetNextJobByClassification(requests, CartonPropertyGroupStatus.Normal, machinesManager.Object.Machines.First());
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTakeExpediteJobsFirst()
//        {
//            SetUpRequestsCorrugateDefaults();

//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Stop;
//            classificationsManager.Object.Classifications.ElementAt(3).Status = ClassificationStatus.Normal;

//            var result = selector.GetNextJobByClassification(requests, CartonPropertyGroupStatus.Normal, machinesManager.Object.Machines.First());
//            Specify.That(result.Requests.Count()).Should.BeEqualTo(1);
//            Specify.That(result.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTakeNormalJobsBeforeHoldJobs()
//        {
//            SetUpRequestsCorrugateDefaults();

//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Hold;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Stop;
//            classificationsManager.Object.Classifications.ElementAt(3).Status = ClassificationStatus.Stop;

//            var result = selector.GetNextJobByClassification(requests, CartonPropertyGroupStatus.Normal, machinesManager.Object.Machines.First());
//            Specify.That(result.Requests.Count()).Should.BeEqualTo(1);
//            Specify.That(result.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTakeHoldJobIfNoOtherJobCanBeProduced()
//        {
//            SetUpRequestsCorrugateDefaults();

//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Stop;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Stop;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Hold;
//            classificationsManager.Object.Classifications.ElementAt(3).Status = ClassificationStatus.Stop;
//            var result = selector.GetNextJobByClassification(requests, CartonPropertyGroupStatus.Normal, machinesManager.Object.Machines.First());
//            Specify.That(result.Requests.Count()).Should.BeEqualTo(1);
//            Specify.That(result.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("3");
//        }

//        private void SetupCartonRequestManagerMock()
//        {
//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            requestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetUpMachinesManagerMock()
//        {
//            var machineCorrugate = new[] { new Corrugate { Width = 1000 } };
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = machineCorrugate.First() },
//            });
//            machine.SetupGet(m => m.Id).Returns(machine1Id);

//            var machineCorrugate2 = new[] { new Corrugate { Width = 800 } };
//            var machine2 = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = machineCorrugate2.First() },
//            });
//            machine2.SetupGet(m => m.Id).Returns(machine2Id);

//            corrugates = new List<Corrugate>(machineCorrugate);
//            ((List<Corrugate>)corrugates).AddRange(machineCorrugate2);

//            machinesManager = new Mock<IMachinesManager>();
//            machinesManager.Setup(manager => manager.Machines).Returns(new List<IPacksizeCutCreaseMachine> { machine.Object, machine2.Object });
//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine.Object, new[] { sequenceGroup.Object })).Returns(machineCorrugate);
//            //machinesManager.Setup(manager => manager.GetAllAvailableCorrugates()).Returns(corrugates);
//            //machinesManager.Setup(manager => manager.GetCorrugatesByProductionGroup(It.IsAny<CommonProductionGroups.ProductionGroup>())).Returns(corrugates);
//        }

//        private void SetupPickAreaManagerMock()
//        {
//            pickAreaManager = new Mock<IPickZoneManager>();
//            pickAreaManager.Setup(manager => manager.PickZones).Returns(Common.SetUpPickAreas().SelectMany(p => p.PickZones));
//        }

//        private void SetupCartonPropertyGroupManagerMock()
//        {
//            var groups = new List<CartonPropertyGroup>();
//            var group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("1");
//            groups.Add(group.Object);
//            var group2 = new Mock<CartonPropertyGroup>();
//            group2.Setup(g => g.Alias).Returns("2");
//            groups.Add(group2.Object);
//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(group.Object);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("2")).Returns(group2.Object);

//            cartonPropertyGroupManager.Setup(
//                manager =>
//                    manager.GetCartonPropertyGroupsWithStatus(
//                        It.Is<CartonPropertyGroupStatus>(c => c.Equals(CartonPropertyGroupStatus.Stop)))).Returns(new List<CartonPropertyGroup>());

//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(It.Is<CartonPropertyGroupStatus>(c => (c.Equals(CartonPropertyGroupStatus.Normal) || c.Equals(CartonPropertyGroupStatus.Surge))))).Returns(groups);
//        }

//        private void SetupServerWrapperMock()
//        {
//            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();
//        }

//        private void SetupClassificationsManagerMock()
//        {
//            classificationsManager = new Mock<IClassificationsManager>();
//            classificationsManager.Setup(manager => manager.Classifications).Returns(Common.SetUpClassifications());
//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Expedite;
//        }

//        private void SetUpRequests()
//        {
//            requests = new List<ICarton>();
//            request1 = new Carton { SerialNumber = "1", CartonPropertyGroupAlias = "1", ClassificationNumber = "1", Length = 300, DesignId = 1 };
//            requests.Add(request1);

//            request2 = new Carton { SerialNumber = "2", CartonPropertyGroupAlias = "2", ClassificationNumber = "2", Length = 300, DesignId = 1 };
//            requests.Add(request2);

//            request3 = new Carton { SerialNumber = "3", CartonPropertyGroupAlias = "1", ClassificationNumber = "3", Length = 300, DesignId = 1 };
//            requests.Add(request3);

//            request4 = new Carton { SerialNumber = "4", CartonPropertyGroupAlias = "2", ClassificationNumber = "4", Length = 300, DesignId = 1 };
//            requests.Add(request4);
//        }

//        private void SetCartonRequestWidthsForNotOptimalTest()
//        {
//            request1.Width = 1000;
//            request2.Width = 800;
//        }

//        private void SetUpRequestsForTakeExpeditedTest()
//        {
//            request1.Width = 900;
//            request2.Width = 900;
//        }

//        private void SetUpRequestsCorrugateDefaults()
//        {
//            request1.Width = 900;
//            request2.Width = 900;
//            request3.Width = 900;
//            request4.Width = 900;
//        }

//        private void SetupProductionGroupManagerMock()
//        {
//            sequenceGroup = new Mock<CommonProductionGroups.ProductionGroup>();
//            var groupNames = new Queue<string>();
//            groupNames.Enqueue("1");
//            groupNames.Enqueue("2");
//            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns<List<string>>(l => ProductionGroupMix(groupNames, l));
//            //sequenceGroup.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(1), new MachineInformation(2) });

//            sequenceGroupManager = new Mock<IProductionGroupManager>();
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(sequenceGroup.Object);
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(new[] { sequenceGroup.Object });
//        }

//        private string ProductionGroupMix(IEnumerable<string> groupNames, List<string> filter)
//        {
//            var groupsToSelectFrom = groupNames.Where(g => filter.Contains(g) == false);
//            var group = groupsToSelectFrom.FirstOrDefault();

//            if (string.IsNullOrEmpty(group))
//            {
//                group = string.Empty;
//            }

//            return group;
//        }
    }
}
