﻿Feature: FusionTiling
	If two requests that may be tiled are sent to an iQ Fusion
	As a customer
	I want each request to be produced as a single out even if the iQ Fusion cannot tile them

@FusionTiling
Scenario: Validate that two identical requests fail to run as tiled jobs on two iQ Fusion, but will run as single out
	Given a machines manager with two iQ Fusion
	And a carton request selector
	When the next job for machine 1 is requested
	Then a job with 2 request for machine 1 is returned
	When the max tiling quantity is set to 1 for machine 1
	When the next job for machine 1 is requested
	Then no job is returned
	When the next job for machine 2 is requested
	Then a job with 2 request for machine 2 is returned
	When the max tiling quantity is set to 1 for machine 2
	When the next job for machine 1 is requested
	Then a job with 1 request for machine 1 is returned
	When the max tiling quantity is set to 0 for machine 1
	When the next job for machine 1 is requested
	Then a job with 1 request for machine 1 is returned
	When the max tiling quantity is set to 0 for machine 1
	When the next job for machine 1 is requested
	Then no job is returned
	When the next job for machine 2 is requested
	Then a job with 1 request for machine 2 is returned
	When the max tiling quantity is set to 0 for machine 2
	When the next job for machine 2 is requested
	Then a job with 1 request for machine 2 is returned
	When the max tiling quantity is set to 0 for machine 2
	When the next job for machine 2 is requested
	Then no job is returned
	When the next job for machine 1 is requested
	Then no job is returned