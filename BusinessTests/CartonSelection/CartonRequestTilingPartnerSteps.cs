﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.PickZone;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Data.WMS;

namespace BusinessTests.CartonSelection
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Moq;

    using PackNet.Business.Carton.TODO;
    using PackNet.Business.CartonPropertyGroup;
    using PackNet.Business.CartonSelection;
    using PackNet.Business.Classifications;
    using PackNet.Business.Machines;
    using PackNet.Business.PackagingDesigns;
    using PackNet.Business.ProductionGroups;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.PickZones;
    using PackNet.Common.Interfaces.DTO.Settings;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Services;

    using TechTalk.SpecFlow;
    using TechTalk.SpecFlow.Assist;

    using Testing.Specificity;
    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

    [Binding]
    public class CartonRequestTilingPartnerSteps
    {
        private IPackagingDesignManager designManager;
        private WaveCartonRequestSelector cartonRequestSelector;
        private Mock<ICartonRequestManager> cartonRequestManager;
        private Mock<IClassificationsManager> classificationsManager;
        private Mock<IPickZoneManager> pickAreaManager;
        private Mock<IMachinesManager> machinesManager;
        private Mock<IProductionGroupManager> sequenceGroupManager;
        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
        private List<ICarton> cartonRequestsInRepository;
        private List<PickZone> zones;
        private List<CartonPropertyGroup> groups;
        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup;
        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;
        private Mock<ICorrugateService> mockCorrugateCalculator;
        private Guid machine1Id = Guid.NewGuid();

        public void SetUpCartonRequestManagerMock()
        {
            cartonRequestsInRepository = new List<ICarton>();
            cartonRequestManager = new Mock<ICartonRequestManager>();
            cartonRequestManager
                .Setup(manager => manager.ValidRequestsInActivePickzones).Returns(() => cartonRequestsInRepository);
            cartonRequestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
                It.IsAny<CartonOnCorrugate>(),
                It.IsAny<IPacksizeCutCreaseMachine>(),
                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
                It.IsAny<int?>(),
                It.IsAny<bool>())).Returns<
                IEnumerable<ICarton>,
                Corrugate,
                IPacksizeCutCreaseMachine,
                PackNet.Common.Interfaces.DTO.Classifications.Classification,
                bool,
                bool,
                Func<DesignQueryParam, IPackagingDesign>,
                int?,
                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
        }

        public void SetUpClassificationsManagerMock()
        {
            ScenarioContext.Current.Add("Classifications", new List<PackNet.Common.Interfaces.DTO.Classifications.Classification>());
            classificationsManager = new Mock<IClassificationsManager>();
            classificationsManager.Setup(manager => manager.Classifications).Returns(() => (List<PackNet.Common.Interfaces.DTO.Classifications.Classification>)ScenarioContext.Current["Classifications"]);
        }

        public void SetUpPickAreaManagerMock()
        {
            zones = new List<PickZone>();
            var pickArea = new PickArea("1", zones);

            var pickAreas = new List<PickArea> { pickArea };

            pickAreaManager = new Mock<IPickZoneManager>();
            pickAreaManager.Setup(manager => manager.PickZones).Returns(() => zones);
        }

        public void SetUpCartonPropertyGroupManagerMock()
        {
            groups = new List<CartonPropertyGroup>();

            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
            cartonPropertyGroupManager
                .Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Normal))
                .Returns(() => groups.Where(g => g.Status == CartonPropertyGroupStatus.Normal));
            cartonPropertyGroupManager
                .Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Stop))
                .Returns(() => groups.Where(g => g.Status == CartonPropertyGroupStatus.Stop));
            cartonPropertyGroupManager
                .Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Surge))
                .Returns(() => groups.Where(g => g.Status == CartonPropertyGroupStatus.Surge));
        }

        public void SetUpMachinesManagerMock()
        {
            var machine = new Mock<IPacksizeCutCreaseMachine>();
            machine.Setup(m => m.Id).Returns(machine1Id);
            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(true);
            //machine.Setup(m => m.Corrugates).Returns(((List<Corrugate>)ScenarioContext.Current["Corrugates"]).ToArray());
            machinesManager = new Mock<IMachinesManager>();
            machinesManager.Setup(manager => manager.Machines).Returns(new[] { machine.Object });
        }

        public void SetUpProductionGroupManagerMock()
        {
            sequenceGroup = new Mock<CommonProductionGroups.ProductionGroup>();
            //sequenceGroup.Setup(sg => sg.ProductionItems).Returns(new[] { new ProductionItem("1", 1) });
            //sequenceGroup.Setup(sg => sg.Machines).Returns(new[] { new IPacksizeCutCreaseMachine(1) });

            // , new ProductionItem("2", 1) });
            sequenceGroupManager = new Mock<IProductionGroupManager>();
            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine1Id)).Returns(sequenceGroup.Object);
            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(new[] { sequenceGroup.Object });
        }

        [Given(@"a number of corrugates")]
        public void GivenANumberOfCorrugates(Table table)
        {
            AddCorrugatesToScenarioContext();
            var corrugatesData = table.CreateSet<CorrugatesTestContainer>();
            ((List<Corrugate>)ScenarioContext.Current["Corrugates"]).AddRange(corrugatesData.Select(corrugate => corrugate.CreateFanfold()));
            SetupCorrugateCalculatorMock();
            SetUpCartonRequestManagerMock();
            SetUpClassificationsManagerMock();
            SetUpPickAreaManagerMock();
            SetUpProductionGroupManagerMock();
            SetUpMachinesManagerMock();
            SetUpCartonPropertyGroupManagerMock();

            designManager = new PackagingDesignManager(Environment.CurrentDirectory + @"\Designs", new Mock<ILogger>(MockBehavior.Loose).Object);
            designManager.LoadDesigns();
        }

        private void SetupCorrugateCalculatorMock()
        {
            mockCorrugateCalculator = new Mock<ICorrugateService>();
        }

        public void AddCorrugatesToScenarioContext()
        {
            var corrugates = new List<Corrugate>();
            ScenarioContext.Current.Add("Corrugates", corrugates);
        }

        [Given(@"a number of tiling requests")]
        public void GivenANumberOfTilingRequests(Table table)
        {
            var cartonRequests = table.CreateSet<CartonRequestTestContainer>();
            var requests = cartonRequests.Select(CreateCartonRequestMock);
            ScenarioContext.Current.Add("requests", requests);
        }

        public Mock<ICarton> CreateCartonRequestMock(CartonRequestTestContainer request)
        {
            var carton = new Mock<ICarton>();
            carton.Setup(c => c.DesignId).Returns(1);
            carton.Setup(c => c.ClassificationNumber).Returns(request.Classification.ToString);
            carton.Setup(c => c.PickZone).Returns("1");
            carton.Setup(c => c.CartonPropertyGroupAlias).Returns(request.CartonPropertyGroup.ToString);
            carton.Setup(c => c.SerialNumber).Returns(request.SerialNumber);
            carton.Setup(c => c.Created).Returns(request.CreatedTime);
            mockCorrugateCalculator.Setup(c => c.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(request.AreaOn);
            mockCorrugateCalculator.Setup(c => c.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(request.TotalLengthOn);
            mockCorrugateCalculator.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), false)).Returns(true);
            mockCorrugateCalculator.Setup(r => r.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), false)).Returns(request.TotalWidthOn);
            carton.Setup(c => c.CorrugateQuality).Returns(request.CorrugateQuality);
            carton.Setup(c => c.GetMaximumNumberOfTilesOnMachine(It.IsAny<Guid>())).Returns(int.MaxValue);
            return carton;
        }

        [Given(@"carton property group (.*) has status (.*)")]
        public void GivenCartonPropertyGroupHasStatus(int cartonPropertyGroup, CartonPropertyGroupStatus status)
        {
            var group = new Mock<CartonPropertyGroup>();
            var cartonPropertyGroupAsString = cartonPropertyGroup.ToString(CultureInfo.InvariantCulture);
            group.Setup(g => g.Alias).Returns(cartonPropertyGroupAsString);
            group.Setup(g => g.Status).Returns(status);
            groups.Add(group.Object);
            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroupByAlias(cartonPropertyGroupAsString)).Returns(group.Object);
        }

        [Given(@"classification (.*) has status (.*)")]
        public void GivenClassificationHasStatus(int classification, ClassificationStatus status)
        {
            var classificationMock = new Mock<PackNet.Common.Interfaces.DTO.Classifications.Classification>();
            classificationMock.Setup(c => c.Number).Returns(classification.ToString);
            classificationMock.Setup(c => c.Status).Returns(status);
            ((List<PackNet.Common.Interfaces.DTO.Classifications.Classification>)ScenarioContext.Current["Classifications"]).Add(classificationMock.Object);
        }

        [Given(@"a number of classfications")]
        public void GivenANumberOfClassfications(Table table)
        {
            var classifications = table.CreateSet<ClassificationTestContainer>();
            var list = classifications.Select(classification => classification.CreateMock()).ToList();
            ((List<PackNet.Common.Interfaces.DTO.Classifications.Classification>)ScenarioContext.Current["Classifications"]).AddRange(
                list.Select(mock => mock.Object));
        }

        [Given(@"carton property group (.*) is next in sequence mix all the time")]
        public void GivenCartonPropertyGroupIsNextInSequenceMixAllTheTime(int cartonPropertyGroup)
        {
            Assert.Fail("Move mix out of production group and into selection algorithm");
            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(l => l.Count == 0))).Returns(cartonPropertyGroup.ToString);
            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(l => l.Count > 0))).Returns("0");
        }

        [Given(@"corrugates are exclusive in reverse order of addition")]
        public void GivenCorrugatesAreExclusiveInReverseOrderOfAddition()
        {
            var list = ((List<Corrugate>)ScenarioContext.Current["Corrugates"]).ToList();
            list.Reverse();
            machinesManager
                .Setup(manager => manager.GetCorrugatesByExclusiveness(It.IsAny<IPacksizeCutCreaseMachine>(), new[] { sequenceGroup.Object }))
                .Returns(() => list);
        }

        [When(@"I get next job for machine")]
        public void WhenIGetNextJobForMachine()
        {
            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();
            var listInMemoryLog = new Mock<ILogger>(MockBehavior.Loose).Object;
            cartonRequestSelector = new NonParallelWaveCartonRequestSelector(
                mockCorrugateCalculator.Object,
                cartonRequestManager.Object,
                classificationsManager.Object,
                pickAreaManager.Object,
                machinesManager.Object,
                sequenceGroupManager.Object,
                cartonPropertyGroupManager.Object,
                designManager,
                new SelectorSettings(2, true, 30, 20, 7200000),
                longHeadPositioningValidation.Object,
                new WmsConfiguration(), 
                listInMemoryLog);
            var requestMocks = (IEnumerable<Mock<ICarton>>)ScenarioContext.Current["requests"];
            cartonRequestsInRepository = requestMocks.Select(x => x.Object).ToList();
            var result =
                cartonRequestSelector.GetNextJobForMachine(machinesManager.Object.Machines.ElementAt(0));

            ScenarioContext.Current.Add("result", result);
        }

        [Then(@"I should get (.*) and (.*)")]
        public void ThenIShouldGet(string serialNumberOne, string serialNumberTwo)
        {
            var result = (CartonRequestOrder)ScenarioContext.Current["result"];
            foreach (var res in result.Requests)
            {
                Console.WriteLine(res.SerialNumber);
            }

            Specify.That(result.Requests.Any(x => x.SerialNumber == serialNumberOne)).Should.BeTrue();
            Specify.That(result.Requests.Any(x => x.SerialNumber == serialNumberTwo)).Should.BeTrue();
        }

        [Then(@"I should only get (.*)")]
        public void ThenIShouldOnlyGet(string serialNumberOne)
        {
            var result = (CartonRequestOrder)ScenarioContext.Current["result"];
            Specify.That(result.Requests.Count()).Should.BeEqualTo(1);
            Specify.That(result.Requests.All(x => x.SerialNumber == serialNumberOne)).Should.BeTrue();
        }
    }
}
