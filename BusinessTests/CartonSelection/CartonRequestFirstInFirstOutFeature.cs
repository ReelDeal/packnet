﻿using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Data.WMS;

namespace BusinessTests.CartonSelection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using PackNet.Business.Carton.TODO;
    using PackNet.Business.CartonSelection;
    using PackNet.Business.Classifications;
    using PackNet.Business.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;

    using StoryQ;

    using Testing.Specificity;

    // CartonRequestFirstInFirstOut.feature
    //Feature: Manual Orders Queue jobselection
    //    In order to get manual added order produced in an effiecent way
    //    As an operator
    //    I need the machines to produce according to a prioritized list

    //@ManualOrderQueue
    //Scenario: Should not produce order that are assigned to another machine
    //    Given a corrugate c1
    //    And a machine loaded with c1
    //    And order1 optimal on corrugate c1 assigned to another machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then no order is produced

    //@ManualOrderQueue
    //Scenario: Should not produce orders that are assigned to the specific machine but with an unloaded corrugate
    //    Given a corrugate c1
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c2 optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then no order is produced

    //@ManualOrderQueue
    //Scenario: Should not produce orders that are assigned to the specific machine but already in queue
    //    Given a corrugate c1
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then order1 is produced without tiles
    //    When next job is requested
    //    Then no order is produced

    //@ManualOrderQueue
    //Scenario: Should tile within orders
    //    Given a corrugate c1
    //    And a machine loaded with c1
    //    And order1 with a quantity 2 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then order1 is produced with tiles

    //@ManualOrderQueue
    //Scenario: Should not tile between orders
    //    Given a corrugate c1
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then order1 is produced without tiles

    //@ManualOrderQueue
    //Scenario: Should pause between orders when production group is set to pause
    //    Given a corrugate c1
    //    And a production group setup with pause between orders true
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then order1 is produced without tiles
    //    When next job is requested
    //    Then no order is produced

    //@ManualOrderQueue
    //Scenario: Should not pause between orders when production group is set to not pause
    //    Given a corrugate c1
    //    And a production group setup with pause between orders false
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then order1 is produced without tiles
    //    When next job is requested
    //    Then order2 is produced without tiles

    //@ManualOrderQueue
    //Scenario: Should produce second order that are assigned to the specific machine when the first is picked up by another machine
    //    Given a corrugate c1
    //    And a production group setup with pause between orders true
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And order1 is picked up by another machine
    //    And order2 is not picked up by any other machine
    //    When next job is requested
    //    Then order2 is produced

    //@ManualOrderQueue
    //Scenario: Should not produce orders that are not optimal for my machine
    //    Given a corrugate c1
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c1 not optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then no order is produced

    //@ManualOrderQueue
    //Scenario: Should only produce orders that are optimal for my machine
    //    Given a corrugate c1
    //    And a machine loaded with c1
    //    And order1 with a quantity 1 optimal tiled on corrugate c1 not optimal on my machine assigned to my machine
    //    And order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assigned to my machine
    //    And no order is picked up by any other machine
    //    When next job is requested
    //    Then order2 is produced

    [TestClass]
    public class CartonRequestFirstInFirstOutFeature
    {
        private Corrugate corrugate;
        private IPacksizeCutCreaseMachine machine;
        private ICollection<ICarton> cartonRequests;
        private ICartonRequestOrder job;

        private ICollection<ICarton> validRequestsNotSentToMachine;
        private ProductionGroup productionGroup;
        private ICollection<IJobOrder> jobOrders;
        private Dictionary<int, List<IJobOrder>> ordersInMachineQueue;
        private Mock<ICartonRequestManager> cartonRequestManager;
        private OrdersQueueRequestSelector selector;

        [TestInitialize]
        public void TestInitialize()
        {
            corrugate = null;
            machine = null;
            cartonRequests = new List<ICarton>();

            jobOrders = new List<IJobOrder>();
            ordersInMachineQueue = new Dictionary<int, List<IJobOrder>>();
            productionGroup = new ProductionGroup();

            var jobOrderManagerMock = new Mock<IJobOrdersManager>();
            jobOrderManagerMock.Setup(m => m.ProducibleOpenOrders).Returns(jobOrders);
            jobOrderManagerMock.Setup(
                m => m.ShouldDispatchCartonForMachineAndOrder(It.IsAny<Guid>(), It.IsAny<IJobOrder>()))
                .Returns<int, IJobOrder>(
                    (machineId, jobOrder) =>
                    {
                        if (ordersInMachineQueue.ContainsKey(machineId) &&
                            productionGroup.SelectionAlgorithmConfiguration.PauseMachineBetweenOrders)
                        {
                            return ordersInMachineQueue[machineId].Any()
                                ? DispatchCartonCommand.Starve
                                : DispatchCartonCommand.Send;
                        }
                        return DispatchCartonCommand.Send;
                    })
                .Callback<int, IJobOrder>((machineId, jobOrder) =>
                {
                    if (ordersInMachineQueue.ContainsKey(machineId))
                    {
                        ordersInMachineQueue[machineId].Add(jobOrder);
                    }
                    else
                    {
                        ordersInMachineQueue.Add(machineId, new List<IJobOrder> { jobOrder });
                    }
                });

            validRequestsNotSentToMachine = new List<ICarton>();
            cartonRequestManager = new Mock<ICartonRequestManager>();
            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(validRequestsNotSentToMachine);
            cartonRequestManager.Setup(m => m.CreateCartonRequestOrder(
                It.IsAny<IEnumerable<ICarton>>(),
                It.IsAny<CartonOnCorrugate>(),
                It.IsAny<IPacksizeCutCreaseMachine>(),
                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
                It.IsAny<int?>(),
                It.IsAny<bool>()))
                .Returns<
                    IEnumerable<ICarton>,
                    Corrugate,
                    IPacksizeCutCreaseMachine,
                    PackNet.Common.Interfaces.DTO.Classifications.Classification,
                    bool,
                    bool,
                    Func<DesignQueryParam, IPackagingDesign>,
                    int?,
                    bool>((a, b, c, e, f, g, h, i, j) => new CartonRequestOrder { Requests = a });

            selector = new MachineSpecificRequestSelector(
                jobOrderManagerMock.Object,
                cartonRequestManager.Object,
                new Mock<IClassificationsManager>().Object,
                new Mock<IPackagingDesignManager>().Object,
                new WmsConfiguration{ExternalPrint = false}, 
                new Mock<ILogger>(MockBehavior.Loose).Object);
        }

        [TestMethod]
        [TestCategory("Acceptance")]
        public void ShouldProduceOrderAssignedToSpecificMachine()
        {
            new Story("Manual Orders Queue Job Selection")
                .InOrderTo("Get a manual order produced in an efficient way")
                .AsA("Operator")
                .IWant("To produce according to a prioritized list")

                .WithScenario("Should produce orders that are assigned to a specific machine")
                .Given(ACorrugate)
                .And(AMachineLoadedWithCorrugate)
                .And(AnOrderAssignedToMachineAndValidToSend)
                .And(OrderIsOptimalToProduceOnCorrugateAndMachineWithTiles)
                .And(OrderIsNotPickedByAnyOtherMachine)
                .When(NextJobIsRequested)
                .Then(OrderIsProduced)

                .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        private void OrderIsNotPickedByAnyOtherMachine()
        {
            cartonRequestManager.Setup(
                m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
                .Callback<IEnumerable<ICarton>, int?>((requests, machineId) =>
                {
                    foreach (var cr in requests)
                    {
                        validRequestsNotSentToMachine.Remove(cr);
                    }
                })
                .Returns(true);
        }

        private void ACorrugate()
        {
            corrugate = new Corrugate { Id = Guid.NewGuid(), Quality = 1 };
        }

        private void AMachineLoadedWithCorrugate()
        {
            machine = new FusionMachine {
                Id = Guid.NewGuid(), 
                Alias = "one", 
                Tracks = new List<Track>
                {
                    new Track { TrackNumber = 1, LoadedCorrugate = corrugate }
                }
            };
        }

        private void AnOrderAssignedToMachineAndValidToSend()
        {
            var cartonRequest = new Carton {MachineId = machine.Id, Custom = true, OrderId = "1", Quantity = 1};
            cartonRequests.Add(cartonRequest);
            validRequestsNotSentToMachine.Add(cartonRequest);
        }

        private void OrderIsOptimalToProduceOnCorrugateAndMachineWithTiles()
        {
            jobOrders.Add(new JobOrder(cartonRequests.First())
            {
                OptimalCorrugateToUse =
                    new CartonOnCorrugate
                    {
                        Corrugate = corrugate,
                        TileCount = 2,
                        ProducibleMachines = new Dictionary<Guid, string> {{machine.Id, machine.Alias}}
                    },
                ProductionGroup = productionGroup
            });
        }

        private void NextJobIsRequested()
        {
            job = selector.GetNextJobForMachine(machine);
        }

        private void OrderIsProduced()
        {
            Specify.That(job.Requests.ElementAt(0).OrderId).Should.BeEqualTo(cartonRequests.First().OrderId);
        }
    }
}
