﻿//using PackNet.Common.Eventing;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Common.Interfaces.Utils;
//using PackNet.Data.Carton;
//using PackNet.Data.WMS;

//namespace BusinessTests.CartonSelection
//{
//    using System;
//    using System.Collections.Concurrent;
//    using System.Collections.Generic;
//    using System.Linq;
//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

//    using Testing.Specificity;

//    [TestClass]
//    public class OrdersQueueRequestSelectorTests
//    {
//        Guid c1Id = Guid.NewGuid();
//        Guid c2Id = Guid.NewGuid();
//        Guid c3Id = Guid.NewGuid();

//        private List<IJobOrder> orders;
//        private List<ICarton> requests;
//        private Mock<IJobOrderRepository> jobOrderRepositoryMock;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IPackagingDesignManager> designManager;
//        private Corrugate optimalCorrugate;

//        private CommonProductionGroups.ProductionGroup productionGroup;

//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManagerMock;
//        private EventAggregator eventAggregator;
//        private Guid machine1Id = Guid.NewGuid();
//        private Guid machine2Id = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            cartonPropertyGroupManagerMock = new Mock<ICartonPropertyGroups>();
//            classificationsManager = new Mock<IClassificationsManager>();
//            classificationsManager.Setup(cm => cm.Classifications).Returns(new[] { new PackNet.Common.Interfaces.DTO.Classifications.Classification(null) });

//            designManager = new Mock<IPackagingDesignManager>();
//            designManager.Setup(dm => dm.GetDesign(It.IsAny<DesignQueryParam>())).Returns(new PackagingDesign { Id = 1, LengthOnZFold = "20", WidthOnZFold = "10", Name = "test", Lines = new List<Line>() });

//            optimalCorrugate = new Corrugate { Width = 5.0, Quality = 4, Thickness = 4.0, Id = c1Id };
//            jobOrderRepositoryMock = new Mock<IJobOrderRepository>();
//            productionGroup = new CommonProductionGroups.ProductionGroup { Alias = "Test Grp 1", ConfiguredCorrugates = new List<Guid> { optimalCorrugate.Id } };


//            eventAggregator = new EventAggregator();

//            var order1 = new JobOrder
//            {
//                OrderId = "Order 1",
//                RemainingQty = 10,
//                ProductionSequence = 2,
//                ProductionGroup = productionGroup,
//                OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" }, { machine2Id, "two" } } }
//            };
//            var order2 = new JobOrder
//            {
//                OrderId = "Order 2",
//                RemainingQty = 10,
//                ProductionSequence = 1,
//                ProductionGroup = productionGroup,
//                OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" }, { machine2Id, "two" } } }
//            };
//            var order3 = new JobOrder
//                {
//                    OrderId = "order3",
//                    ProductionSequence = 3,
//                    ProductionGroup = productionGroup,
//                    RemainingQty = 10,
//                    OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" }, { machine2Id, "two" } } }
//                };
//            orders = new List<IJobOrder> { order1, order2, order3 };

//            var request1ForOrder1 = new Carton { OrderId = orders[0].OrderId, IsValid = true };
//            var request2ForOrder2 = new Carton { OrderId = orders[1].OrderId, IsValid = true };
//            var request3ForOrder3 = new Carton { OrderId = orders[2].OrderId, IsValid = true };
//            requests = new List<ICarton> { request1ForOrder1, request2ForOrder2, request3ForOrder3 };
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotSelectJobsThatAreNotProducibleOnMyMachine()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);

//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track{ TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(new[] {new JobOrder
//            {
//                    OrderId = "order3",
//                    ProductionSequence = 3,
//                    ProductionGroup = productionGroup,
//                    RemainingQty = 10,
//                    OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = optimalCorrugate, TileCount = 1, ProducibleMachines = new Dictionary<Guid, string> {{Guid.NewGuid(), "plus one"}}}
//                } });
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };

//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Specify.That(nextRequest).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldOnlySelectJobsThatAreProducibleOnMyMachine()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);

//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(new[] {
//                new JobOrder
//                {
//                    OrderId = "Order 2",
//                    ProductionSequence = 2,
//                    ProductionGroup = productionGroup,
//                    RemainingQty = 10,
//                    OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = optimalCorrugate, TileCount = 1, ProducibleMachines = new Dictionary<Guid, string> {{Guid.NewGuid(), "plus one"}}}
//                },
//                new JobOrder
//                {
//                    OrderId = "order3",
//                    ProductionSequence = 3,
//                    ProductionGroup = productionGroup,
//                    RemainingQty = 10,
//                    OptimalCorrugateToUse = new CartonOnCorrugate { Corrugate = optimalCorrugate, TileCount = 1, ProducibleMachines = new Dictionary<Guid, string> {{machine.Object.Id, "one"}}}
//                }});

//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//       .Setup(
//           m =>
//               m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                   It.IsAny<CartonOnCorrugate>(),
//                   It.IsAny<IPacksizeCutCreaseMachine>(),
//                   It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                   It.IsAny<bool>(),
//                   It.IsAny<bool>(),
//                   It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                   It.IsAny<int?>(),
//                   It.IsAny<bool>())).Returns
//       <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//           Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//               (a, b, c, d, e, f, g, h, i) =>
//               {
//                   requestedOrder.Requests = a;
//                   return requestedOrder;
//               });

//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Specify.That(nextRequest.Requests.ElementAt(0).OrderId).Should.BeEqualTo("order3");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void RequestIsSelectedBasedOnOrderPriority()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };


//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });

//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Assert.IsTrue(nextRequest.Requests.Contains(requests[0]));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void RequestSendsNextJobFromNextOrderWithHighestPriority()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            orders[0] = new JobOrder { ProductionSequence = 0, ProductionGroup = productionGroup, OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } } } };

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };
//            productionGroup.SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration();

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });

//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Assert.IsTrue(nextRequest.Requests.Contains(requests[1]));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldAssignInternalPrintingValueToSelectedRequest()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            orders[0] = new JobOrder { ProductionSequence = 0, ProductionGroup = productionGroup, OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } } } };

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object,
//                classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };
//            productionGroup.SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration();

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });

//            var request = selector.GetNextJobForMachine(machine.Object);
//            Specify.That(request.InternalPrinting).Should.BeFalse();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetNextJobForMachineRestrictsJobsToMachineCurrentlyWorkingOrder()
//        {
//            SetRequestsOrderId("order 1");
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            orders = new List<IJobOrder>{new JobOrder
//                         {
//                             OrderId = "order 1",
//                             ProductionSequence = 0, 
//                             ProductionGroup = productionGroup, 
//                             OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { {machine1Id, "one"} } },
//                             IsDistributable = false
//                         }};

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(
//                ordersManager.Object,
//                cartonRequestManager.Object,
//                classificationsManager.Object,
//                designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);

//            var machine1 = new Mock<IPacksizeCutCreaseMachine>();
//            machine1.Setup(m => m.Id).Returns(machine1Id);
//            machine1.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });

//            var machine2 = new Mock<IPacksizeCutCreaseMachine>();
//            machine2.Setup(m => m.Id).Returns(machine2Id);
//            machine2.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine1.Object.Id), new MachineInformation(machine2.Object.Id) };

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });

//            var nextRequest = selector.GetNextJobForMachine(machine1.Object);
//            Assert.AreEqual("order 1", nextRequest.Requests.First().OrderId, "Machine 1 should have started working on order 1");
//            nextRequest = selector.GetNextJobForMachine(machine2.Object);
//            Assert.IsNull(nextRequest, "Machine 2 should not get routed a job because order 1 is restricted to machine 1");
//        }

//        private void SetRequestsOrderId(string newOrderId)
//        {
//            foreach (var request in requests)
//            {
//                request.OrderId = newOrderId;
//            }
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetNextJobForMachineSendsSecondJobForNonDistributedOrder()
//        {
//            SetRequestsOrderId("order 1");
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            orders = new List<IJobOrder>
//            {
//                new JobOrder
//                {
//                    OrderId = "order 1",
//                    ProductionSequence = 0,
//                    Status = JobOrderStatus.InQueue,
//                    RemainingQty = 1,
//                    ProductionGroup = productionGroup,
//                    OptimalCorrugateToUse =
//                        new CartonOnCorrugate {TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { {machine1Id, "one"} } },
//                    IsDistributable = false
//                }
//            };

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders.ProducibleOrders());
//            var selector = new OrdersQueueRequestSelector(
//                ordersManager.Object,
//                cartonRequestManager.Object,
//                classificationsManager.Object,
//                designManager.Object,
//                                new WmsConfiguration { ExternalPrint = false },
//new Mock<ILogger>(MockBehavior.Loose).Object);

//            var machine1 = new Mock<IPacksizeCutCreaseMachine>();
//            machine1.Setup(m => m.Id).Returns(machine1Id);
//            machine1.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });

//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine1.Object.Id) };

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });

//            var nextRequest = selector.GetNextJobForMachine(machine1.Object);
//            Assert.AreEqual("order 1", nextRequest.Requests.First().OrderId, "Machine 1 should have started working on order 1");
//            nextRequest = selector.GetNextJobForMachine(machine1.Object);
//            Assert.AreEqual("order 1", nextRequest.Requests.First().OrderId, "Machine 1 should have started working on order 1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldSendTilesWhenOptimalCorrugateHasATiledValue()
//        {
//            orders.Clear();
//            orders.Add(new JobOrder
//                {
//                    OrderId = "Order 1",
//                    ProductionSequence = 0,
//                    ProductionGroup = productionGroup,
//                    OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 3, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } } }
//                });

//            var carton1 = new Carton
//                {
//                    OrderId = orders[0].OrderId,
//                    IsValid = true,
//                    ClassificationNumber = "class1"
//                };
//            var carton2 = new Carton
//                {
//                    OrderId = orders[0].OrderId,
//                    IsValid = true,
//                    ClassificationNumber = "class1"
//                };
//            var carton3 = new Carton
//                {
//                    OrderId = orders[0].OrderId,
//                    IsValid = true,
//                    ClassificationNumber = "class1"
//                };
//            var carton4 = new Carton
//                {
//                    OrderId = orders[0].OrderId,
//                    IsValid = true,
//                    ClassificationNumber = "class1"
//                };

//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine)
//                                .Returns(new List<ICarton> { carton1, carton2, carton3, carton4 });
//            cartonRequestManager.Setup(
//                m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.Setup(jom => jom.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object,
//                classificationsManager.Object, designManager.Object,
//                                new WmsConfiguration { ExternalPrint = false },
//new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };
//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//       .Setup(
//           m =>
//               m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                   It.IsAny<CartonOnCorrugate>(),
//                   It.IsAny<IPacksizeCutCreaseMachine>(),
//                   It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                   It.IsAny<bool>(),
//                   It.IsAny<bool>(),
//                   It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                   It.IsAny<int?>(),
//                   It.IsAny<bool>())).Returns
//       <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//           Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//               (a, b, c, d, e, f, g, h, i) =>
//               {
//                   requestedOrder.Requests = a;
//                   return requestedOrder;
//               });

//            //run test
//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Specify.That(nextRequest.Requests.Count()).Should.BeEqualTo(3, "optimal corrugate set to for tiling and we did not send the exptected number of cartons with the order.");

//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetNextRequestShouldCallIntoCrmToFlagCartonsSelectedAreGoingToMachine()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            orders.Clear();
//            orders.Add(new JobOrder { OrderId = "Order 1", ProductionSequence = 0, ProductionGroup = productionGroup, OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } } } });
//            orders.Add(new JobOrder { Created = DateTime.MinValue, OrderId = "Order 2", ProductionSequence = 0, ProductionGroup = productionGroup, OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } } } });

//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//         .Setup(
//             m =>
//                 m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                     It.IsAny<CartonOnCorrugate>(),
//                     It.IsAny<IPacksizeCutCreaseMachine>(),
//                     It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                     It.IsAny<bool>(),
//                     It.IsAny<bool>(),
//                     It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                     It.IsAny<int?>(),
//                     It.IsAny<bool>())).Returns
//         <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//             Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                 (a, b, c, d, e, f, g, h, i) =>
//                 {
//                     requestedOrder.Requests = a;
//                     return requestedOrder;
//                 });

//            IEnumerable<ICarton> requestsSentToMachine = null;
//            cartonRequestManager.Setup(
//                crm =>
//                    crm.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Callback<IEnumerable<ICarton>, int?>(
//                    (cr, machineId) =>
//                    {
//                        requestsSentToMachine = cr;
//                    }
//                ).Returns(true);
//            //run test
//            selector.GetNextJobForMachine(machine.Object);
//            foreach (var cartonRequest in requestedOrder.Requests)
//            {
//                Specify.That(requestsSentToMachine.Count(cr => cr.Id == cartonRequest.Id)).Should.BeEqualTo(1);
//            }
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void RequestNextOrderCyclesUntilTheOptimalCorrugateIsLoadedOnMachine()
//        {
//            var notOptimal = new CartonOnCorrugate
//            {
//                TileCount = 1,
//                Corrugate = new Corrugate { Alias = "corr1", Quality = 1, Thickness = 1, Id = c1Id, Width = 10 },
//                ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } }
//            };

//            var optimal = new CartonOnCorrugate
//            {
//                TileCount = 1,
//                Corrugate = new Corrugate { Alias = "corr2", Quality = 1, Thickness = 2, Id = c2Id, Width = 19 },
//                ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } }
//            };
//            orders[0].OptimalCorrugateToUse = notOptimal;
//            orders[1].OptimalCorrugateToUse = notOptimal;
//            orders[2].OptimalCorrugateToUse = optimal;

//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = notOptimal.Corrugate } });

//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };
//            productionGroup.SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration { PauseMachineBetweenOrders = false };

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });
//            //run first part of test.
//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Assert.IsNull(nextRequest);// we don't yet have the optimal corrugate loaded

//            // now setup optimal corrugate
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimal.Corrugate } });

//            //run test
//            nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Assert.IsTrue(nextRequest.Requests.First().OrderId == "order3");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetNextJobForMachineDoesntReturnResultForEmptyList()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(new List<ICarton>());
//            var ordersManager = new JobOrdersManagerWrapper(orders, jobOrderRepositoryMock.Object, cartonRequestManager.Object, cartonPropertyGroupManagerMock.Object, new Mock<IOptimalCorrugateCalculator>().Object, eventAggregator, eventAggregator, new Mock<IProductionGroupManager>().Object);
//            var selector = new OrdersQueueRequestSelector(ordersManager, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new FusionMachine
//            {
//                Tracks = new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = new Corrugate { Width = new MicroMeter(5), Quality = 4, Thickness = new MicroMeter(4) } } }
//            };
//            var nextRequest = selector.GetNextJobForMachine(machine);
//            Assert.IsNull(nextRequest);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetNextJobForMachineSkipsOrderWhenToldOrElseItGetsTheHoseAgain()
//        {
//            orders.Add(new JobOrder { Created = DateTime.MinValue, OrderId = "Order 2", ProductionSequence = 0, ProductionGroup = productionGroup, OptimalCorrugateToUse = new CartonOnCorrugate { TileCount = 1, Corrugate = optimalCorrugate, ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" } } } });

//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(orders);
//            ordersManager.Setup(om => om.ShouldDispatchCartonForMachineAndOrder(It.IsAny<Guid>(), It.Is<IJobOrder>(j => j.OrderId == "Order 1"))).Returns(DispatchCartonCommand.Skip);
//            ordersManager.Setup(om => om.ShouldDispatchCartonForMachineAndOrder(It.IsAny<Guid>(), It.Is<IJobOrder>(j => j.OrderId == "Order 2"))).Returns(DispatchCartonCommand.Send);
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };


//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });

//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Assert.IsNotNull(nextRequest.Requests.FirstOrDefault(c => c.OrderId == "Order 2"));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetNextJobForMachineHandlesNullFromJobManager()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(new List<ICarton> { new Carton() });
//            var ordersManager = new Mock<IJobOrdersManager>();
//            ordersManager.Setup(m => m.OpenOrders).Returns(new List<IJobOrder>());
//            var selector = new OrdersQueueRequestSelector(ordersManager.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//            var machine = new FusionMachine()
//            {
//                Tracks = new List<Track> { new Track{TrackNumber = 1, LoadedCorrugate = new Corrugate { Width = new MicroMeter(5), Quality = 4, Thickness = new MicroMeter(4) } } }
//            };
//            var nextRequest = selector.GetNextJobForMachine(machine);
//            Assert.IsNull(nextRequest);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotUpdateMachineCurrentOrdersWhenTheSameOrderIsSent()
//        {
//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);

//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });
//            //productionGroup.Machines = new List<MachineInformation> { new MachineInformation(machine.Object.Id) };

//            orders.Clear();
//            orders.Add(
//                new JobOrder
//                {
//                    OrderId = "Order 1",
//                    ProductionSequence = 0,
//                    ProductionGroup = productionGroup,
//                    OptimalCorrugateToUse =
//                        new CartonOnCorrugate
//                        {
//                            TileCount = 1,
//                            Corrugate = optimalCorrugate,
//                            ProducibleMachines = new Dictionary<Guid, string> { { machine1Id, "one" }, { machine2Id, "two " } }
//                        }
//                });

//            var ordersManagerMock = new Mock<IJobOrdersManager>();
//            // Setup to have machine already working on Order 2
//            var machineCurrentOrders =
//                new ConcurrentDictionary<int, List<string>>(
//                    new List<KeyValuePair<int, List<string>>>
//                    {
//                        new KeyValuePair<int, List<string>>(
//                            1,
//                            new List<string> { "Order 1" })
//                    });
//            //ordersManagerMock.Setup(m => m.MachineCurrentOrders).Returns(machineCurrentOrders);
//            ordersManagerMock.Setup(m => m.ProducibleOpenOrders).Returns(orders);
//            var selector = new OrdersQueueRequestSelector(ordersManagerMock.Object, cartonRequestManager.Object, classificationsManager.Object, designManager.Object,
//                new WmsConfiguration { ExternalPrint = false },
//                new Mock<ILogger>(MockBehavior.Loose).Object);

//            // Begin test
//            selector.GetNextJobForMachine(machine.Object);
//            List<string> ordersForMachine;
//            Assert.IsTrue(machineCurrentOrders.TryGetValue(1, out ordersForMachine), "machine was not added to the current orders collection");
//            Assert.IsNotNull(ordersForMachine, "machine was not added to the current orders collection");
//            Assert.IsTrue(ordersForMachine.Contains("Order 1"), "order should contain Order 1");
//            Assert.IsTrue(ordersForMachine.Count == 1, "order should contain only one Order 1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void GetNextJobForMachine_WhenMachineSpecificSelector_ReturnsMachineSpecificOrderForMyMachine()
//        {
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            machine.Setup(m => m.Tracks).Returns(new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = optimalCorrugate } });

//            var ordersManager = new Mock<IJobOrdersManager>();
//            var machineSpecificCarton = new Carton
//            {
//                SerialNumber = "Machine Specific",
//                OrderId = "Machine Specific Order",
//                MachineId = machine.Object.Id,
//                Custom = true
//            };
//            var notMachineSpecificCarton = new Carton
//            {
//                SerialNumber = "Not Machine Specific",
//                OrderId = "Not Machine Specific Order",
//                MachineId = machine.Object.Id,
//                Custom = false
//            };
//            var requests = new List<ICarton> { machineSpecificCarton, notMachineSpecificCarton };
//            ordersManager.SetupGet(om => om.ProducibleOpenOrders).Returns(new[]
//            {
//                new JobOrder
//                {
//                    OrderId = "Machine Specific Order",
//                    ProductionSequence = 1,
//                    ProductionGroup = productionGroup,
//                    RemainingQty = 10,
//                    OptimalCorrugateToUse =
//                        new CartonOnCorrugate
//                        {
//                            Corrugate = optimalCorrugate,
//                            TileCount = 1,
//                            ProducibleMachines = new Dictionary<Guid, string> {{machine.Object.Id, "one"}}
//                        },
//                    Carton = machineSpecificCarton
//                },
//                new JobOrder
//                {
//                    OrderId = "order",
//                    ProductionSequence = 2,
//                    ProductionGroup = productionGroup,
//                    RemainingQty = 10,
//                    OptimalCorrugateToUse =
//                        new CartonOnCorrugate
//                        {
//                            Corrugate = optimalCorrugate,
//                            TileCount = 1,
//                            ProducibleMachines = new Dictionary<Guid, string> {{machine.Object.Id, "one"}}
//                        },
//                    Carton = notMachineSpecificCarton
//                }
//            });

//            var cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(m => m.ValidRequestsNotSentToMachine).Returns(requests);
//            cartonRequestManager.Setup(
//                m =>
//                    m.RequestsOkToSendToMachineAndRegister(new List<ICarton> { machineSpecificCarton },
//                        machine.Object.Id)).Returns(true);

//            var selector = new MachineSpecificRequestSelector(
//                ordersManager.Object,
//                cartonRequestManager.Object,
//                classificationsManager.Object,
//                designManager.Object,
//                new WmsConfiguration{ExternalPrint = false}, 
//                new Mock<ILogger>().Object);

//            var requestedOrder = new CartonRequestOrder();
//            cartonRequestManager
//                .Setup(
//                    m =>
//                        m.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                            It.IsAny<CartonOnCorrugate>(),
//                            It.IsAny<IPacksizeCutCreaseMachine>(),
//                            It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<bool>(),
//                            It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                            It.IsAny<int?>(),
//                            It.IsAny<bool>())).Returns
//                <IEnumerable<ICarton>, Corrugate, IPacksizeCutCreaseMachine, PackNet.Common.Interfaces.DTO.Classifications.Classification, bool, bool,
//                    Func<DesignQueryParam, IPackagingDesign>, int?, bool>(
//                        (a, b, c, d, e, f, g, h, i) =>
//                        {
//                            requestedOrder.Requests = a;
//                            return requestedOrder;
//                        });

//            var nextRequest = selector.GetNextJobForMachine(machine.Object);
//            Specify.That(nextRequest.Requests.Single().OrderId).Should.BeEqualTo("Machine Specific Order");
//        }
//    }
//}
