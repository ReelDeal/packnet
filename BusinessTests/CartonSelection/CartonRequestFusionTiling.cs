﻿//using PackNet.Business.PickZone;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Services;
//    using PackNet.Common.Logging;

//    using TechTalk.SpecFlow;

//    using Testing.Specificity;
//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

//    [Binding]
    public class CartonRequestFusionTiling
    {
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
//        #region Fields
//        private WaveCartonRequestSelector cartonRequestSelector;
//        private Mock<ICartonRequestManager> cartonRequestManager;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IPickZoneManager> pickAreaManager;
//        private Mock<IMachinesManager> machinesManager;
//        private Mock<IProductionGroupManager> sequenceGroupManager;
//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
//        private IPackagingDesignManager designManager;
//        private ICartonRequestOrder selectedOrder;
//        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private Guid machine1Id = Guid.NewGuid();
//        private Guid machine2Id = Guid.NewGuid();

//        #endregion

//        [BeforeScenario("FusionTiling")]
//        public void BeforeScenario()
//        {
//            var backend = new Mock<ILoggingBackend>(MockBehavior.Loose);
//            backend.Setup(b => b.GetLog()).Returns(new Mock<ILogger>(MockBehavior.Loose).Object);
//            backend.Setup(b => b.GetLogFor(It.IsAny<string>())).Returns(new Mock<ILogger>(MockBehavior.Loose).Object);
//            LogManager.LoggingBackend = backend.Object;

//            pickAreaManager = new Mock<IPickZoneManager>();
//            SetupProductionGroupManagerMock();
//            SetUpCartonRequestManagerMock();
//            SetUpClassificationsManagerMock();
//            SetupCartonPropertyGroupManagerMock();

//            designManager = new PackagingDesignManager("Designs", new Mock<ILogger>(MockBehavior.Loose).Object);
//            designManager.LoadDesigns();
//            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();
//        }

//        [Given(@"a machines manager with two iQ Fusion")]
//        public void GivenAMachinesManagerWithTwoIqFusion()
//        {
//            SetUpMachinesManagerMock();
//        }

//        [Given(@"a carton request selector")]
//        public void GivenACartonRequestSelector()
//        {
//            cartonRequestSelector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                cartonRequestManager.Object,
//                classificationsManager.Object,
//                pickAreaManager.Object,
//                machinesManager.Object,
//                sequenceGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManager,
//                new SelectorSettings(10, true, 40, 40, 0),
//                longHeadPositioningValidation.Object,
//                new WmsConfiguration(), 
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//        }

//        [When(@"the next job for machine (.*) is requested")]
//        public void WhenTheNextJobForMachineXIsRequested(Guid machineId)
//        {
//            selectedOrder = cartonRequestSelector.GetNextJobForMachine(machinesManager.Object.Machines.Single(m => m.Id == machineId));
//        }

//        [Then(@"a job with (.*) request for machine (.*) is returned")]
//        public void ThenAJobWithXRequestForMachineYIsReturned(int numberOfRequests, int machineId)
//        {
//            Specify.That(selectedOrder.Requests.Count()).Should.BeEqualTo(numberOfRequests);
//            Specify.That(selectedOrder.MachineId).Should.BeEqualTo(machineId);
//        }

//        [When(@"the max tiling quantity is set to (.*) for machine (.*)")]
//        public void WhenTheMaxTilingQuantityIsSetToXForMachineY(int maxTiles, Guid machineId)
//        {
//            foreach (var request in selectedOrder.Requests)
//            {
//                request.FailsOnMachineWithNumberOfTiles(machineId, maxTiles + 1);
//            }
//        }

//        [Then(@"no job is returned")]
//        public void ThenNoJobIsReturned()
//        {
//            Specify.That(selectedOrder).Should.BeNull();
//        }

//        private void SetUpCartonRequestManagerMock()
//        {
//            var cartonRequests = new List<ICarton>
//                {
//                    new Carton
//                        {
//                            SerialNumber = "1",
//                            Height = 12,
//                            Length = 10,
//                            Width = 7,
//                            PickZone = "AZ1",
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            CartonPropertyGroupAlias = "1"
//                        },
//                    new Carton
//                        {
//                            SerialNumber = "2",
//                            Height = 12,
//                            Length = 10,
//                            Width = 7,
//                            PickZone = "AZ1",
//                            ClassificationNumber = "1",
//                            DesignId = 1,
//                            CorrugateQuality = 1,
//                            CartonPropertyGroupAlias = "1"
//                        }
//                };

//            cartonRequestManager = new Mock<ICartonRequestManager>();
            
//            SetupCorrugateCalculationService();

//            cartonRequestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(cartonRequests);
//            cartonRequestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetupCorrugateCalculationService()
//        {
//            mockCorrugateCalculationService = new Mock<ICorrugateService>();

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>())).Returns(true);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IMachineEnvironment, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IMachineEnvironment, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Length);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IMachineEnvironment, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null ? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width
//                });

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IMachineEnvironment, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));
//        }


//        private void SetUpMachinesManagerMock()
//        {
//            var allCorrugates = new List<Corrugate>();

//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Id).Returns(machine1Id);
//            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            //machine.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine.Setup(m => m.CurrentMachineProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            var corrugates = new List<Corrugate>
//                {
//                    new Corrugate { Quality = 1, Thickness = 0.16, Width = 16 },
//                    new Corrugate { Quality = 1, Thickness = 0.16, Width = 18 }
//                };

//            allCorrugates.AddRange(corrugates);

//            machine.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = corrugates.First() },
//                new Track{ TrackNumber = 2, LoadedCorrugate = corrugates.Last() }
//            });
//            var machine2 = new Mock<IPacksizeCutCreaseMachine>();
//            machine2.Setup(m => m.Id).Returns(machine2Id);
//            //machine2.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            //machine2.Setup(m => m.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine2.Setup(m => m.CurrentMachineProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            var corrugates2 = new List<Corrugate>
//                {
//                    new Corrugate { Quality = 1, Thickness = 0.16, Width = 20 },
//                    new Corrugate { Quality = 1, Thickness = 0.16, Width = 24 }
//                };

//            machine2.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = corrugates2.First() },
//                new Track{ TrackNumber = 2, LoadedCorrugate = corrugates2.Last() }
//            });
//            allCorrugates.AddRange(corrugates2);

//            machinesManager = new Mock<IMachinesManager>();
//            machinesManager.Setup(manager => manager.Machines).Returns(new[] { machine.Object, machine2.Object });
//            //machinesManager.Setup(manager => manager.GetAllAvailableCorrugates()).Returns(allCorrugates);
//            //machinesManager.Setup(manager => manager.GetCorrugatesByProductionGroup(It.IsAny<CommonProductionGroups.ProductionGroup>())).Returns(allCorrugates);

//            machinesManager.Setup(
//                manager => manager.GetCorrugatesByExclusiveness(machine.Object, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>()))
//                .Returns(corrugates);

//            machinesManager.Setup(
//                manager => manager.GetCorrugatesByExclusiveness(machine2.Object, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>()))
//                .Returns(corrugates2);
//        }

//        private void SetupCartonPropertyGroupManagerMock()
//        {
//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();

//            var cartonPropertyGroup = new Mock<CartonPropertyGroup>();
//            cartonPropertyGroup.Setup(g => g.Alias).Returns("1");
//            cartonPropertyGroup.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Normal);

//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(cartonPropertyGroup.Object);
//        }

//        private void SetupProductionGroupManagerMock()
//        {
//            var item = new ProductionItem("1", 1);
//            var sequenceGroup = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup.Setup(sg => sg.ProductionItems).Returns(new[] { item });
//            //sequenceGroup.Setup(sg => sg.GetNextItemInMix(It.Is<List<string>>(l => l.Count > 0))).Returns(string.Empty);
//            //sequenceGroup.Setup(sg => sg.GetNextItemInMix(It.Is<List<string>>(l => l.Count == 0))).Returns("1");
//            //sequenceGroup.Setup(sg => sg.Machines).Returns(new[] { new MachineInformation(1), new MachineInformation(2) });
            
//            sequenceGroupManager = new Mock<IProductionGroupManager>();
//            sequenceGroupManager.Setup(sg => sg.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(sequenceGroup.Object);
//            sequenceGroupManager.Setup(sg => sg.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(new[] { sequenceGroup.Object });
//        }

//        private void SetUpClassificationsManagerMock()
//        {
//            classificationsManager = new Mock<IClassificationsManager>();
//            classificationsManager.Setup(manager => manager.Classifications).Returns(new List<PackNet.Common.Interfaces.DTO.Classifications.Classification> { new PackNet.Common.Interfaces.DTO.Classifications.Classification("1") });
//        }
    }
}
