﻿//using PackNet.Business.PickZone;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.Diagnostics;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.PickZones;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Enums;
//    using PackNet.Common.Interfaces.Services;

//    using Testing.Specificity;
//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

    [TestClass]
    public class FilterOnCartonPropertyGroupStatusTests
    {
#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
//        private WaveCartonRequestSelector selector;
//        private IList<ICarton> requests;
//        private Mock<IMachinesManager> machinesManager;
//        private Mock<ICartonRequestManager> requestManager;
//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IPickZoneManager> pickAreaManager;
//        private Mock<IProductionGroupManager> sequenceGroupManager;
//        private ICarton request1;
//        private ICarton request2;
//        private Mock<CommonProductionGroups.ProductionGroup> productionGroup;
//        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;
//        private Mock<IPackagingDesignManager> designManagerMock;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private Guid machine1Id = Guid.NewGuid();
//        private Guid machine2Id = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            SetupProductionGroupManagerMock();
//            SetUpMachinesManagerMock();
//            SetUpRequests();
//            SetupClassificationManagerMock();
//            SetupCartonRequestManagerMock();
//            SetupCartonPropertyGroupManagerMock();
//            SetupPickAreaManagerMock();
//            SetupServerWrapperMock();
//            SetupDesignManagerMock();
//            mockCorrugateCalculationService = SetupCorrugateSelectionService();
//            //var designManager = new PackagingDesignManager("Data\\Packaging Designs");
//            //designManager.LoadDesigns();
//            var logger = new Mock<ILogger>();
//            logger.Setup(l => l.Log(It.IsAny<LogLevel>(), It.IsAny<string>())).Callback<LogLevel, string>((ll, s) => Trace.WriteLine(s));
//            selector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                requestManager.Object,
//                classificationsManager.Object,
//                pickAreaManager.Object,
//                machinesManager.Object,
//                sequenceGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManagerMock.Object,
//                new SelectorSettings(2, true, 40, 40, 0),
//                longHeadPositioningValidation.Object,
//                new WmsConfiguration(), 
//                logger.Object);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTakeSurgedJobFirst()
//        {
//            SetUpBucketBestFanfoldForTakeSurgedTest();
//            var result = selector.GetNextJobByCartonPropertyGroupStatus(requests, machinesManager.Object.Machines.First());
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("G2");
//            //productionGroup.Verify(g => g.GetNextItemInMix(It.IsAny<List<string>>()), Times.Once());
//            Assert.Fail("Move mix into selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldTakeNormalJobIfSurgedIsNotOptimalOnMachine()
//        {
//            SetUpBucketBestFanfoldForTakeNormalTest();
//            var result = selector.GetNextJobByCartonPropertyGroupStatus(requests, machinesManager.Object.Machines.First());
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("G1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldSortRequestsOnImportance()
//        {
//            var requests = SetUpRequestListForSortingTest();

//            Specify.That(requests.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(requests.ElementAt(1).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(requests.ElementAt(2).SerialNumber).Should.BeEqualTo("3");
//            Specify.That(requests.ElementAt(3).SerialNumber).Should.BeEqualTo("4");

//            var result = selector.SortRequestsByImportance(requests);

//            Specify.That(result.ElementAt(0).SerialNumber).Should.BeEqualTo("4");
//            Specify.That(result.ElementAt(1).SerialNumber).Should.BeEqualTo("2");
//            Specify.That(result.ElementAt(2).SerialNumber).Should.BeEqualTo("1");
//            Specify.That(result.ElementAt(3).SerialNumber).Should.BeEqualTo("3");
//        }
        
//        private void SetupCartonRequestManagerMock()
//        {
//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            requestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        private void SetUpMachinesManagerMock()
//        {
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = new Corrugate { Width = 1000 } },
//            });
//            machine.Setup(m => m.Id).Returns(machine1Id);

//            var machine2 = new Mock<IPacksizeCutCreaseMachine>();

//            machine.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = new Corrugate { Width = 800 } },
//            });
//            machine2.Setup(m => m.Id).Returns(machine2Id);

//            machinesManager = new Mock<IMachinesManager>();
//            machinesManager.Setup(manager => manager.Machines).Returns(new List<IPacksizeCutCreaseMachine> { machine.Object, machine2.Object });
//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine.Object, new[] { productionGroup.Object })).Returns(new List<Corrugate>{machine.Object.Tracks[0].LoadedCorrugate});
//        }

//        private void SetupCartonPropertyGroupManagerMock()
//        {
//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();

//            var groups = CreateCartonPropertyGroups();
//            cartonPropertyGroupManager.Setup(manager => manager.Groups).Returns(groups);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Surge)).Returns(new[] { groups.ElementAt(1) });

//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("G1")).Returns(groups.ElementAt(0));
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("G2")).Returns(groups.ElementAt(1));
//        }

//        private void SetupDesignManagerMock()
//        {
//            var packagingDesignOne = new PackagingDesign { Name = "1", Id = 1, LengthOnZFold = "L", WidthOnZFold = "W" };
//            designManagerMock = new Mock<IPackagingDesignManager>();
//            designManagerMock.Setup(m => m.GetDesign(It.IsAny<DesignQueryParam>())).Returns(packagingDesignOne);
//        }

//        private void SetupPickAreaManagerMock()
//        {
//            pickAreaManager = new Mock<IPickZoneManager>();
//            var zone = new PickZone(null);
//            pickAreaManager.Setup(manager => manager.PickZones).Returns(new[] { zone });
//        }

//        private void SetupServerWrapperMock()
//        {
//            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();
//        }

//        private void SetupProductionGroupManagerMock()
//        {
//            productionGroup = new Mock<CommonProductionGroups.ProductionGroup>();
//            var groupNames = new Queue<string>();
//            groupNames.Enqueue("G1");
//            groupNames.Enqueue("G2");
//            //productionGroup.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns<List<string>>(l => ProductionGroupMix(groupNames, l));
            
//            productionGroup.Setup(g => g.ConfiguredMachineGroups).Returns(new List<Guid>());

//            sequenceGroupManager = new Mock<IProductionGroupManager>();
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(productionGroup.Object);
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(new[] { productionGroup.Object });
//        }

//        private string ProductionGroupMix(IEnumerable<string> groupNames, List<string> filter)
//        {
//            var groupsToSelectFrom = groupNames.Where(g => filter.Contains(g) == false);
//            var group = groupsToSelectFrom.FirstOrDefault();

//            if (string.IsNullOrEmpty(group))
//            {
//                group = string.Empty;
//            }

//            return group;
//        }

//        private void SetUpRequests()
//        {
//            requests = new List<ICarton>();
//            request1 = new Carton { SerialNumber = "1", CartonPropertyGroupAlias = "G1", ClassificationNumber = "2", DesignId = 1, Length = 800 };
//            requests.Add(request1);

//            request2 = new Carton { SerialNumber = "2", CartonPropertyGroupAlias = "G2", ClassificationNumber = "1", DesignId = 1, Length = 800 };
//            requests.Add(request2);
//        }

//        private IEnumerable<CartonPropertyGroup> CreateCartonPropertyGroups()
//        {
//            var groups = new List<CartonPropertyGroup>();

//            var group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("G1");
//            group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Normal);
//            groups.Add(group.Object);

//            group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("G2");
//            group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Surge);
//            groups.Add(group.Object);

//            return groups;
//        }

//        private void SetupClassificationManagerMock()
//        {
//            classificationsManager = new Mock<IClassificationsManager>();
//            classificationsManager.Setup(manager => manager.Classifications).Returns(Common.SetUpClassifications());
//            classificationsManager.Object.Classifications.ElementAt(0).Status = ClassificationStatus.Expedite;
//            classificationsManager.Object.Classifications.ElementAt(1).Status = ClassificationStatus.Normal;
//            classificationsManager.Object.Classifications.ElementAt(2).Status = ClassificationStatus.Hold;
//            classificationsManager.Object.Classifications.ElementAt(3).Status = ClassificationStatus.Expedite;
//        }

//        private IEnumerable<ICarton> SetUpRequestListForSortingTest()
//        {
//            var requests = new List<ICarton>();

//            var request1 = new Carton { SerialNumber = "1", CartonPropertyGroupAlias = "G1", DesignId = 1, ClassificationNumber = "1" };
//            var request2 = new Carton { SerialNumber = "2", CartonPropertyGroupAlias = "G2", DesignId = 1, ClassificationNumber = "2" };    
//            var request3 = new Carton { SerialNumber = "3", CartonPropertyGroupAlias = "G1", DesignId = 1, ClassificationNumber = "3" };
//            var request4 = new Carton { SerialNumber = "4", CartonPropertyGroupAlias = "G2", DesignId = 1, ClassificationNumber = "4" };

//            requests.Add(request1);
//            requests.Add(request2);
//            requests.Add(request3);
//            requests.Add(request4);

//            return requests;
//        }

//        private void SetUpBucketBestFanfoldForTakeSurgedTest()
//        {
//            request1.Width = 900;
//            request2.Width = 900;
//        }

//        private void SetUpBucketBestFanfoldForTakeNormalTest()
//        {
//            request1.Width = 900;
//            request2.Width = 800;
//        }

//        private Mock<ICorrugateService> SetupCorrugateSelectionService()
//        {
//            var mock = new Mock<ICorrugateService>();

//            mock.Setup(
//               m =>
//                   m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                       It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//               .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//               ((a, b, c, d, e) => a.Width != null && a.Width.Value < b.Width);

//            mock.Setup(
//            m =>
//                m.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                    It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                    It.IsAny<bool>()))
//            .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//            ((a, b, c, d, e) => a.Width);

//            mock.Setup(
//                m =>
//                    m.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Length);

//            mock.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null ? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width
//                });

//            mock.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));

//            return mock;
//        }
    }
}
