﻿//using PackNet.Common.Interfaces.DTO.Messaging;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Common.Interfaces.Utils;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Concurrent;
//    using System.Collections.Generic;
//    using System.Linq;
//    using System.Threading;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Mappers;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Eventing;
//    using PackNet.Common.Interfaces.DTO;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.Classifications;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.Scanning;
//    using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
//    using PackNet.Common.Interfaces.Enums;
//    using PackNet.Common.Interfaces.Eventing;

//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;
//    using Testing.Specificity;

    [TestClass]
    public class ScanToQueueRequestSelectorTests
    {
#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
        //        private Guid corrugate1Id = Guid.NewGuid();
//        private Guid corrugate2Id = Guid.NewGuid();
//        private Guid corrugate3Id = Guid.NewGuid();

//        private Guid machineId = Guid.NewGuid();

//        private Scan2QueueTestGuy selector;
//        private List<CommonProductionGroups.ProductionGroup> productionGroups;
//        private IPacksizeCutCreaseMachine machine;
//        private List<Guid> configuredCorrugates;
//        private List<ICarton> requests;
//        private Mock<ILogger> logger;
//        private EventAggregator eventAggregator;
//        private Mock<IOptimalCorrugateCalculator> corrugateCalculator;
//        private Mock<IProductionGroupManager> productionGroupManager;
//        private Mock<IPackagingDesignManager> designManager;
//        private Mock<ICartonRequestManager> cartonRequestManager;
//        private Mock<IArticleImporter> articleImporter;
//        private Mock<ICustomJobImporter> customJobImporter;

//        [TestInitialize]
//        public void Setup()
//        {
//            machine = new FusionMachine()
//            {
//                Id = machineId,
//                Tracks = new List<Track>{ new Track{LoadedCorrugate = new Corrugate { Alias = "c1", Id = corrugate1Id, Quality = 1, Thickness = 1, Width = 1 } }}
//            };

//            configuredCorrugates = new List<Guid>
//            {
//                corrugate1Id, corrugate2Id, corrugate3Id,
//            };

//            productionGroups = new List<CommonProductionGroups.ProductionGroup>
//            {
//                new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "Scan2Queue",
//                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {
//                        ScannerSettings = new ScannerSettings
//                        {
//                            ListeningPort = 3001,
//                            ScanToQueueSelectorMode =  ScanToQueueSelectorModes.Custom
//                        }
//                    },
//                    SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
//                    ConfiguredCorrugates = configuredCorrugates,
//                    ConfiguredMachineGroups = new List<Guid>{Guid.NewGuid()}
//                }
//                ,
//                new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "Scan2Queue2",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
//                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {
//                        ScannerSettings = new ScannerSettings
//                        {
//                            ListeningPort = 3002,
//                            ScanToQueueSelectorMode =  ScanToQueueSelectorModes.Custom
//                        }
//                    }
//                },
//                new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "ArticleQueue",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
//                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration {
//                        ScannerSettings = new ScannerSettings
//                        {
//                            ListeningPort = 3003,
//                            ScanToQueueSelectorMode =  ScanToQueueSelectorModes.Article
//                        }
//                    }
//                }
//            };

//            requests = new List<ICarton>
//            {
//                new Carton
//                {
//                    SerialNumber = Guid.NewGuid().ToString(),
//                    ProductionGroupAlias = "Scan2Queue",
//                    IsValid = true
//                },
//            };

//            logger = new Mock<ILogger>();
//            eventAggregator = new EventAggregator();
//            corrugateCalculator = new Mock<IOptimalCorrugateCalculator>();
//            productionGroupManager = new Mock<IProductionGroupManager>();

//            designManager = new Mock<IPackagingDesignManager>();
//            cartonRequestManager = new Mock<ICartonRequestManager>();
//            articleImporter = new Mock<IArticleImporter>();
//            customJobImporter = new Mock<ICustomJobImporter>();

//            productionGroupManager
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupManager
//                .Setup(p => p.GetProductionGroupForMachine(machineId))
//                .Returns(productionGroups.First());

//            cartonRequestManager.Setup(
//    manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//        It.IsAny<CartonOnCorrugate>(),
//        It.IsAny<IPacksizeCutCreaseMachine>(),
//        It.IsAny<Classification>(),
//        It.IsAny<bool>(),
//        It.IsAny<bool>(),
//        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//        It.IsAny<int?>(),
//        It.IsAny<bool>())).Returns<
//            IEnumerable<ICarton>,
//            Corrugate,
//            IPacksizeCutCreaseMachine,
//            Classification,
//            bool,
//            bool,
//            Func<DesignQueryParam, IPackagingDesign>,
//            int?,
//            bool>(
//                (a, b, c, d, e, f, g, h, i) =>
//                    new CartonRequestOrder
//            {
//                Requests = a,
//                Corrugate = b,
//                MachineId = c.Id,
//                InternalPrinting = f,
//                PackStation = h
//            });

//            selector = new Scan2QueueTestGuy(eventAggregator, eventAggregator,
//                cartonRequestManager.Object, productionGroupManager.Object, designManager.Object,
//                corrugateCalculator.Object, articleImporter.Object, customJobImporter.Object, logger.Object);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ScanToQueueCartonRequestSelectorShouldReturnCorrectSelectionAlgorithmTypesValue()
//        {
//            Specify.That(selector.SelectionAlgorithm).Should.BeEqualTo(SelectionAlgorithmTypes.ScanToCreate);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishSelectionAlgorithmIfPgIsPresent()
//        {
//            var eventCalled = false;
//            eventAggregator.GetEvent<IMessage<Tuple<SelectionAlgorithmTypes, IScanTrigger>>>().Subscribe(data =>
//            {
//                eventCalled = true;
//                Specify.That(data.Data.Item1).Should.BeEqualTo(SelectionAlgorithmTypes.ScanToCreate);
//            });

//            eventAggregator.Publish(new Message<IScanTrigger>
//            {
//                MessageType = MessageTypes.GetSelectionAlgorithmForProductionGroup,
//                Data = new CreateCartonTrigger
//                {
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            });

//            Specify.That(eventCalled).Should.BeTrue();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotPublishSelectionAlgorithmIfPgIsNotPresent()
//        {
//            var eventCalled = false;
//            eventAggregator.GetEvent<IMessage<Tuple<SelectionAlgorithmTypes, IScanTrigger>>>().Subscribe(data =>
//            {
//                eventCalled = true;
//                Specify.That(data.Data.Item1).Should.BeEqualTo(SelectionAlgorithmTypes.ScanToCreate);
//            });

//            eventAggregator.Publish(new Message<IScanTrigger>
//                {
//                    MessageType = MessageTypes.GetSelectionAlgorithmForProductionGroup,
//                    Data = new CreateCartonTrigger
//                    {
//                        ProductionGroupAlias = "NOTAVALIDBOXLASTPG"
//                    }
//                });

//            Specify.That(eventCalled).Should.BeFalse();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldAddCartonRequestsToProductionGroupOnScan()
//        {
//            customJobImporter.Setup(m => m.ImportCustomJobs(201, 10, 11, 12, 5, "Scan2Queue")).Returns(new List<ICarton> {
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//            });

//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "201:10:11:12:5",
//                ProductionGroupAlias = "Scan2Queue",
//                TriggeredOn = DateTime.Now
//            });

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(5);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.DesignId.Equals(201))).Should.BeTrue();

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.Length.Equals(10.0))).Should.BeTrue();
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.Width.Equals(11.0))).Should.BeTrue();
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.Height.Equals(12.0))).Should.BeTrue();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldParseBothDecimalSeparators()
//        {
//            customJobImporter.Setup(m => m.ImportCustomJobs(201, 10.2, 11.2, 12.2, 5, "Scan2Queue")).Returns(new List<ICarton>
//            {
//                new Carton {DesignId = 201, Length = 10.2, Width = 11.2, Height = 12.2, ProductionGroupAlias = "Scan2Queue"},
//            });

//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "201:10.2:11.2:12.2:5",
//                ProductionGroupAlias = "Scan2Queue",
//                TriggeredOn = DateTime.Now
//            });

//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "201:10,2:11,2:12,2:5",
//                ProductionGroupAlias = "Scan2Queue",
//                TriggeredOn = DateTime.Now
//            });

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(2);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.DesignId.Equals(201))).Should.BeTrue();

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.Length.Equals(10.2))).Should.BeTrue();
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.Width.Equals(11.2))).Should.BeTrue();
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.Height.Equals(12.2))).Should.BeTrue();
//        }


//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldAppendNewCartonRequestsToProductionGroupQueueOnScan()
//        {
//            customJobImporter.Setup(m => m.ImportCustomJobs(201, 10, 11, 12, 5, "Scan2Queue")).Returns(new List<ICarton> {
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//            });

//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "201:10:11:12:5",
//                ProductionGroupAlias = "Scan2Queue",
//                TriggeredOn = DateTime.Now
//            });

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(5);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.All(cr => cr.DesignId.Equals(201))).Should.BeTrue();

//            customJobImporter.Setup(m => m.ImportCustomJobs(202, 10, 11, 12, 1, "Scan2Queue")).Returns(new List<ICarton> {
//                new Carton {DesignId = 202, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//            });

//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "202:10:11:12:1",
//                ProductionGroupAlias = "Scan2Queue",
//                TriggeredOn = DateTime.Now
//            });

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(6);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Last().DesignId).Should.BeEqualTo(202);

//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldLogErrorWhenScanningForANonExistingProductionGroup()
//        {
//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "201:10:11:12:5",
//                ProductionGroupAlias = "ABO",
//                TriggeredOn = DateTime.Now
//            });

//            var count = selector.PgQueues.Sum(pair => pair.Value.Item2.Count);

//            Specify.That(count).Should.BeEqualTo(0);

//            logger.Verify(l => l.Log(LogLevel.Error, It.IsAny<string>()));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldLogErrorWhenScanDataIsInvalid()
//        {
//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "201:1asd0:11:12:5",
//                ProductionGroupAlias = "ABO",
//                TriggeredOn = DateTime.Now
//            });

//            var count = selector.PgQueues.Sum(pair => pair.Value.Item2.Count);

//            Specify.That(count).Should.BeEqualTo(0);

//            logger.Verify(l => l.Log(LogLevel.Error, It.IsAny<string>()));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ScanToQueueSelectorSendsCartonToQueueWhenEventIsTriggered()
//        {
//            var productionGroups = new List<CommonProductionGroups.ProductionGroup>
//            {
//                new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "Scan2Queue",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast
//                }
//            };

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            customJobImporter.Setup(m => m.ImportCustomJobs(201, 10, 10, 10, 1, "Scan2Queue")).Returns(new List<ICarton>
//            {
//                new Carton
//                {
//                    SerialNumber = "MEGA REQUEST",
//                    ProductionGroupAlias = "Scan2Queue",
//                    IsValid = true
//                }
//            });

//            productionGroupManager
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            productionGroupManager
//                .Setup(p => p.GetProductionGroupForMachine(machineId))
//                .Returns(productionGroups.First());

//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);

//            //When event is triggered Store the request in the corresponding PG queue
//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                ProductionGroupAlias = productionGroups.First().Alias,
//                TriggeredOn = DateTime.UtcNow,
//                BarcodeData = "201:10:10:10:1"
//            });

//            Thread.Sleep(100);
//            //Call getNextJobForMachine
//            var returnedCartonOrder = selector.GetNextJobForMachine(machine);

//            //Make sure that the job gets returned
//            Specify.That(returnedCartonOrder.Requests.First().SerialNumber)
//                .Should.BeEqualTo("MEGA REQUEST");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishAndLogErrorMessageWhenIncorrectlyDelimitedDataIsScanned()
//        {
//            bool called = false;

//            var evilTrigger = new ScanToQueueTrigger
//            {
//                ProductionGroupAlias = productionGroups.First().Alias,
//                TriggeredOn = DateTime.UtcNow,
//                BarcodeData = "201☻10☻10☻10☻1"
//            };

//            eventAggregator.GetEvent<IMessage<ScanMessage>>().Subscribe(e =>
//            {
//                Specify.That(e.Data.Message).Should.Contain(evilTrigger.BarcodeData);
//                Specify.That(e.Data.Message).Should.Contain(":");
//                Specify.That(e.Data.Message).Should.Contain("Invalid delimiter");
//                Specify.That(e.Data.Message).Should.Contain("Scan2Queue");
//                Specify.That(e.Data.Message).Should.Contain(evilTrigger.ProductionGroupAlias);
//                called = true;
//            });

//            eventAggregator.Publish(evilTrigger);

//            Specify.That(called).Should.BeTrue();
//            logger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s =>
//                s.Contains("Invalid delimiter") &&
//                s.Contains(":") &&
//                s.Contains(evilTrigger.BarcodeData) &&
//                s.Contains(evilTrigger.ProductionGroupAlias)
//                )), Times.Once);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishAndLogErrorMessageWhenDataWithWrongNumberOfFIeldsIsScanned()
//        {
//            bool called = false;

//            var evilTrigger = new ScanToQueueTrigger
//            {
//                ProductionGroupAlias = productionGroups.First().Alias,
//                TriggeredOn = DateTime.UtcNow,
//                BarcodeData = "10:10:10:1"
//            };

//            eventAggregator.GetEvent<IMessage<ScanMessage>>().Subscribe(e =>
//            {
//                Specify.That(e.Data.Message).Should.Contain(evilTrigger.BarcodeData);
//                Specify.That(e.Data.Message).Should.Contain("Incorrect number of fields");
//                Specify.That(e.Data.Message).Should.Contain("Scan2Queue");
//                Specify.That(e.Data.Message).Should.Contain(evilTrigger.ProductionGroupAlias);
//                called = true;
//            });

//            eventAggregator.Publish(evilTrigger);

//            Specify.That(called).Should.BeTrue();
//            logger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s =>
//                s.Contains("Incorrect number of fields") &&
//                s.Contains(evilTrigger.BarcodeData) &&
//                s.Contains(evilTrigger.ProductionGroupAlias)
//                )), Times.Once);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishAndLogErrorMessageWhenScannedDataCannotBeParsed()
//        {
//            bool called = false;

//            var evilTrigger = new ScanToQueueTrigger
//            {
//                ProductionGroupAlias = productionGroups.First().Alias,
//                TriggeredOn = DateTime.UtcNow,
//                BarcodeData = "102sdwq:1ssss0:1220:1:ddd"
//            };

//            eventAggregator.GetEvent<IMessage<ScanMessage>>().Subscribe(e =>
//            {
//                Specify.That(e.Data.Message).Should.Contain(evilTrigger.BarcodeData);
//                Specify.That(e.Data.Message).Should.Contain("Could not read scanned data");
//                Specify.That(e.Data.Message).Should.Contain(evilTrigger.ProductionGroupAlias);
//                called = true;
//            });

//            eventAggregator.Publish(evilTrigger);

//            Specify.That(called).Should.BeTrue();
//            //logger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s =>
//            //    s.Contains("Incorrect number of fields") &&
//            //    s.Contains(evilTrigger.BarcodeData) &&
//            //    s.Contains(evilTrigger.ProductionGroupAlias)
//            //    )), Times.Once);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReInsertCartonsMarkedForReproduction()
//        {
//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };

//            productionGroupManager
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(0);
//            Specify.That(selector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(0);
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.FlaggedForReproduction,
//                Data = requests
//            });
            
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(3);
//            Specify.That(selector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(0);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[0].SerialNumber).Should.BeEqualTo("3");
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[1].SerialNumber).Should.BeEqualTo("2");
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[2].SerialNumber).Should.BeEqualTo("1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReInsertCartonsRetractedFromMachine()
//        {
//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };

//            productionGroupManager
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(0);
//            Specify.That(selector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(0);

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.RetractedFromMachine,
//                Data = requests
//            });
            
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(3);
//            Specify.That(selector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(0);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[0].SerialNumber).Should.BeEqualTo("3");
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[1].SerialNumber).Should.BeEqualTo("2");
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[2].SerialNumber).Should.BeEqualTo("1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRemoveCartonsFromPgQueueOnEvent()
//        {
//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };

//            productionGroupManager
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.RetractedFromMachine,
//                Data = requests
//            });

//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(3);
//            Specify.That(selector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(0);

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.CartonRemoved,
//                Data = requests.GetRange(1, 2)
//            });
            
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(1);
//            Specify.That(selector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(0);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[0].SerialNumber).Should.BeEqualTo("1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldDisregardsCartonRemovalOfNonExistingCarton()
//        {
//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };

//            var r2 = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "4",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "5",
//                    ProductionGroupAlias = "Scan2Queue"
//                },

//                new Carton {
//                    SerialNumber = "6",
//                    ProductionGroupAlias = "Scan2Queue2"
//            }
//            };

//            productionGroupManager
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.RetractedFromMachine,
//                Data = requests
//            });

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.CartonRemoved,
//                Data = r2
//            });
            
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(3);
//            Specify.That(selector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(0);
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[0].SerialNumber).Should.BeEqualTo("3");
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[1].SerialNumber).Should.BeEqualTo("2");
//            Specify.That(selector.PgQueues["Scan2Queue"].Item2[2].SerialNumber).Should.BeEqualTo("1");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldLoadDBPersistedCartonsIntoPGqueuesOnConstruction()
//        {
//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue2"
//                },
                                
//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };

//            cartonRequestManager.Setup(m => m.CartonRequests).Returns(requests);

//            var scan2QueueSelector = new Scan2QueueTestGuy(eventAggregator, eventAggregator,
//                cartonRequestManager.Object, productionGroupManager.Object, designManager.Object,
//                corrugateCalculator.Object, articleImporter.Object, customJobImporter.Object, logger.Object);

//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue"].Item2.Count).Should.BeEqualTo(2);
//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue2"].Item2.Count).Should.BeEqualTo(1);

//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue"].Item2.Contains(requests[0])).Should.BeTrue();
//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue"].Item2.Contains(requests[1])).Should.BeFalse();
//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue"].Item2.Contains(requests[2])).Should.BeTrue();

//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue2"].Item2.Contains(requests[0])).Should.BeFalse();
//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue2"].Item2.Contains(requests[1])).Should.BeTrue();
//            Specify.That(scan2QueueSelector.PgQueues["Scan2Queue2"].Item2.Contains(requests[2])).Should.BeFalse();

//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishDBPersistedCartonsOnConstruction()
//        {
//            var eventCalled = false;

//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },

//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue2"
//                },

//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };

//            cartonRequestManager.Setup(m => m.CartonRequests).Returns(requests);

//            eventAggregator.GetEvent<Message<IEnumerable<ICarton>>>().Subscribe(m =>
//            {
//                Specify.That(m.Data.Count()).Equals(3);
//                eventCalled = true;
//            });

//            var scan2QueueSelector = new Scan2QueueTestGuy(eventAggregator, eventAggregator,
//                cartonRequestManager.Object, productionGroupManager.Object, designManager.Object,
//                corrugateCalculator.Object, articleImporter.Object, customJobImporter.Object, logger.Object);

//            Specify.That(eventCalled).Should.BeTrue();
//        }


//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldAddProductionGroupQueueWhenNewProductionGroupIsChangedToScan2Queue()
//        {
//            eventAggregator.Publish(new Message<CommonProductionGroups.ProductionGroup>()
//            {
//                MessageType = ProductionGroupMessages.ProductionGroupUpdated,
//                Data = new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "SUPERGROUP",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
//                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration
//                    {
//                        ScannerSettings = new ScannerSettings
//                        {
//                            ScanToQueueSelectorMode = ScanToQueueSelectorModes.Article
//                        }
//                    }
//                }
//            });

//            Specify.That(selector.PgQueues.ContainsKey("SUPERGROUP")).Should.BeTrue();
//            Specify.That(selector.PgQueues["SUPERGROUP"].Item1).Should.BeEqualTo(ScanToQueueSelectorModes.Article);
//        }


//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateScanningModeWhenScan2QueueProductionGroupsIsPublished()
//        {
//            eventAggregator.Publish(new Message<CommonProductionGroups.ProductionGroup>()
//            {
//                MessageType = ProductionGroupMessages.ProductionGroupUpdated,
//                Data = new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "Scan2Queue",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
//                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration
//                    {
//                        ScannerSettings = new ScannerSettings
//                        {
//                            ScanToQueueSelectorMode = ScanToQueueSelectorModes.Article
//                        }
//                    }
//                }
//            });

//            Specify.That(selector.PgQueues["Scan2Queue"].Item1).Should.BeEqualTo(ScanToQueueSelectorModes.Article);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRemoveProductionGroupQueueIfScanningModeIsChangedFromScan2Queue()
//        {
//            eventAggregator.Publish(new Message<CommonProductionGroups.ProductionGroup>()
//            {
//                Data = new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "Scan2Queue",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
//                    SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration
//                    {
//                        ScannerSettings = new ScannerSettings
//                        {
//                            ScanToQueueSelectorMode = null
//                        }
//                    }
//                },
//                MessageType = ProductionGroupMessages.ProductionGroupUpdated
//            });

//            Specify.That(selector.PgQueues.ContainsKey("Scan2Queue")).Should.BeFalse();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldThrowErrorsWhenTryingToRemoveProductionGroupQueueWithQueuedCartons()
//        {
//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue2"
//                },
                                
//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };
//            productionGroupManager
//            .SetupGet(p => p.ProductionGroups)
//            .Returns(productionGroups);

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.RetractedFromMachine,
//                Data = requests
//            });

//            eventAggregator.Publish(
//                new Message<CommonProductionGroups.ProductionGroup>()
//                {
//                    Data = new CommonProductionGroups.ProductionGroup
//                    {
//                        Alias = "Scan2Queue",
//                        SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
//                        SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration
//                        {
//                            ScannerSettings = new ScannerSettings
//                            {
//                                ScanToQueueSelectorMode = null
//                            }
//                        }
//                    },
//                    MessageType = ProductionGroupMessages.ProductionGroupUpdated
//                });

//            logger.Verify(m => m.Log(LogLevel.Error, It.IsAny<string>()));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldLogErrorWhenTryingToCreateNonExistingArticle()
//        {
//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "1",
//                ProductionGroupAlias = "ArticleQueue",
//                TriggeredOn = DateTime.Now
//            });

//            cartonRequestManager.Verify(m => m.AddRequests(It.IsAny<IEnumerable<ICarton>>(), CartonRequestEventType.Added), Times.Never);
//            logger.Verify(m => m.Log(LogLevel.Error, string.Format("Unable to import article with ID: 1 for Production Group ArticleQueue, please verify its existance in the database")));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldCreateArticleRequestOnTrigger()
//        {
//            articleImporter.Setup(
//                imp =>
//                    imp.ImportArticleFromArticleId(It.Is<string>(s => s.Equals("1")),
//                        It.Is<string>(s => s.Equals("ArticleQueue")))).Returns(new Carton());

//            var trigger = new ScanToQueueTrigger
//            {
//                BarcodeData = "1",
//                ProductionGroupAlias = "ArticleQueue",
//                TriggeredOn = DateTime.Now
//            };

//            eventAggregator.Publish(trigger);

//            articleImporter.Verify(imp => imp.ImportArticleFromArticleId(It.Is<string>(s => s.Equals(trigger.BarcodeData)), It.Is<string>(s => s.Equals(trigger.ProductionGroupAlias))));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldAddTriggeredArticleRequestToQueue()
//        {
//            articleImporter.Setup(m => m.ImportArticleFromArticleId(It.IsAny<string>(), It.IsAny<string>()))
//                .Returns<string, string>((id, pg) => new Carton
//                {
//                    SerialNumber = Guid.NewGuid().ToString(),
//                    ArticleId = id,
//                    ProductionGroupAlias = pg
//                });

//            var trigger = new ScanToQueueTrigger
//            {
//                BarcodeData = "1",
//                ProductionGroupAlias = "ArticleQueue",
//                TriggeredOn = DateTime.Now
//            };

//            eventAggregator.Publish(trigger);

//            Specify.That(selector.PgQueues[trigger.ProductionGroupAlias].Item1).Should.BeEqualTo(ScanToQueueSelectorModes.Article);
//            Specify.That(selector.PgQueues[trigger.ProductionGroupAlias].Item2.First().ArticleId).Should.BeEqualTo(trigger.BarcodeData);
//            Specify.That(selector.PgQueues[trigger.ProductionGroupAlias].Item2.First().ProductionGroupAlias)
//                .Should.BeEqualTo(trigger.ProductionGroupAlias);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldLogIfNullArticleIOsReturned()
//        {
//            var evilTrigger = new ScanToQueueTrigger
//            {
//                BarcodeData = "666",
//                ProductionGroupAlias = "ArticleQueue",
//                TriggeredOn = DateTime.Now
//            };

//            eventAggregator.Publish(evilTrigger);

//            logger.Verify(m => m.Log(LogLevel.Error, string.Format("Unable to import article with ID: 666 for Production Group ArticleQueue, please verify its existance in the database")));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldLogErrorWhenUpdatingGroupWithoutValidScanningMode()
//        {
//            var group = new CommonProductionGroups.ProductionGroup
//            {
//                SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
//                SelectionAlgorithmConfiguration = new SelectionAlgorithmConfiguration
//                {
//                    ScannerSettings = new ScannerSettings
//                    {
//                        ScanToQueueSelectorMode = ScanToQueueSelectorModes.NotApplicable
//                    }
//                }
//            };

//            eventAggregator.Publish(new Message<CommonProductionGroups.ProductionGroup>()
//            {
//                Data = group,
//                MessageType = ProductionGroupMessages.ProductionGroupUpdated
//            });

//            logger.Verify(l => l.Log(LogLevel.Error, It.IsAny<string>()));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishProduciblesOnCreateCartonTrigger()
//        {
//            customJobImporter.Setup(m => m.ImportCustomJobs(201, 10, 11, 12, 5, "Scan2Queue")).Returns(new List<ICarton> {
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//                new Carton {DesignId = 201, Length = 10, Width = 11, Height = 12, ProductionGroupAlias = "Scan2Queue"},
//            });

//            var eventCalled = false;

//            AddThreeJobs();

//            eventAggregator.GetEvent<Message<IEnumerable<ICarton>>>().Subscribe(m =>
//            {
//                Specify.That(m.Data.Count()).Should.BeEqualTo(8);
//                Specify.That(m.MessageType).Should.BeEqualTo(MessageTypes.ScanToQueueRequestListUpdate);
//                Specify.That(m.ReplyTo).Should.BeNullOrEmpty();
//                eventCalled = true;
//            });

//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                BarcodeData = "201:10:11:12:5",
//                ProductionGroupAlias = "Scan2Queue",
//                TriggeredOn = DateTime.Now
//            });

//            Specify.That(eventCalled).Should.BeTrue();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishSuccessMessageWhenCustomImportIsSuccessfull()
//        {
//            bool called = false;

//            var goodTrigger = new ScanToQueueTrigger
//            {
//                ProductionGroupAlias = productionGroups.First().Alias,
//                TriggeredOn = DateTime.UtcNow,
//                BarcodeData = "201:10:10:10:1"
//            };

//            eventAggregator.GetEvent<IMessage<ScanMessage>>().Subscribe(e =>
//            {
//                Specify.That(e.Data.Message).Should.Contain(goodTrigger.BarcodeData);
//                Specify.That(e.Data.Message).Should.Contain("successfully");
//                Specify.That(e.Data.Message).Should.Contain("Imported barcode with data:");
//                Specify.That(e.Data.ProductionGroup).Should.Contain(goodTrigger.ProductionGroupAlias);
//                called = true;
//            });

//            eventAggregator.Publish(goodTrigger);

//            Specify.That(called).Should.BeTrue();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishSuccessMessageWhenArticleImportIsSuccessfull()
//        {
//            bool called = false;

//            this.articleImporter.Setup(
//         imp =>
//             imp.ImportArticleFromArticleId(It.Is<string>(s => s.Equals("1")),
//                 It.Is<string>(s => s.Equals("ArticleQueue")))).Returns(new Carton());

//            var goodTrigger = new ScanToQueueTrigger
//            {
//                ProductionGroupAlias = "ArticleQueue",
//                TriggeredOn = DateTime.UtcNow,
//                BarcodeData = "1"
//            };

//            eventAggregator.GetEvent<IMessage<ScanMessage>>().Subscribe(e =>
//            {
//                Specify.That(e.Data.Message).Should.Contain(goodTrigger.BarcodeData);
//                Specify.That(e.Data.Message).Should.Contain("successfully");
//                Specify.That(e.Data.Message).Should.Contain("Imported barcode with data:");
//                Specify.That(e.Data.ProductionGroup).Should.Contain(goodTrigger.ProductionGroupAlias);
//                called = true;
//            });

//            eventAggregator.Publish(goodTrigger);

//            Specify.That(called).Should.BeTrue();
//        }



//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishProduciblesWhenFillingMachineQueues()
//        {
//            var productionGroups = new List<CommonProductionGroups.ProductionGroup>
//            {
//                new CommonProductionGroups.ProductionGroup
//                {
//                    Alias = "Scan2Queue",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast
//                }
//            };

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            customJobImporter.Setup(m => m.ImportCustomJobs(201, 10, 10, 10, 1, "Scan2Queue"))
//                .Returns(new List<ICarton>
//                {
//                    new Carton
//                    {
//                        SerialNumber = "MEGA REQUEST",
//                        ProductionGroupAlias = "Scan2Queue",
//                        IsValid = true
//                    },
//                    new Carton
//                    {
//                        SerialNumber = "MEGA REQUEST1",
//                        ProductionGroupAlias = "Scan2Queue",
//                        IsValid = true
//                    },
//                    new Carton
//                    {
//                        SerialNumber = "MEGA REQUEST2",
//                        ProductionGroupAlias = "Scan2Queue",
//                        IsValid = true
//                    },

//                });

//            productionGroupManager
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            productionGroupManager
//                .Setup(p => p.GetProductionGroupForMachine(machineId))
//                .Returns(productionGroups.First());

//            cartonRequestManager.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);

//            //When event is triggered Store the request in the corresponding PG queue
//            eventAggregator.Publish(new ScanToQueueTrigger
//            {
//                ProductionGroupAlias = productionGroups.First().Alias,
//                TriggeredOn = DateTime.UtcNow,
//                BarcodeData = "201:10:10:10:1"
//            });


//            var eventCalled = false;

//            eventAggregator.GetEvent<Message<IEnumerable<ICarton>>>().Subscribe(m =>
//            {
//                Specify.That(m.Data.Count()).Should.BeEqualTo(2);
//                Specify.That(m.MessageType).Should.BeEqualTo(MessageTypes.ScanToQueueRequestListUpdate);
//                Specify.That(m.ReplyTo).Should.BeNullOrEmpty();
//                eventCalled = true;
//            });


//            Thread.Sleep(100);
//            //Call getNextJobForMachine
//            var returnedCartonOrder = selector.GetNextJobForMachine(machine);

//            //Make sure that the job gets returned
//            Specify.That(eventCalled).Should.BeTrue();
//        }

//        [TestCleanup]
//        public void TearDown()
//        {

//        }

//        public void AddThreeJobs()
//        {
//            var requests = new List<ICarton> {
//                new Carton {
//                    SerialNumber = "1",
//                    ProductionGroupAlias = "Scan2Queue"
//                },
                                
//                new Carton {
//                    SerialNumber = "2",
//                    ProductionGroupAlias = "Scan2Queue2"
//                },
                                
//                new Carton {
//                    SerialNumber = "3",
//                    ProductionGroupAlias = "Scan2Queue"
//                }
//            };

//            productionGroupManager
//            .SetupGet(p => p.ProductionGroups)
//            .Returns(productionGroups);
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.RetractedFromMachine,
//                Data = requests
//            });
//        }

//    }

//    public class Scan2QueueTestGuy : ScanToQueueRequestSelector
//    {
//        public Scan2QueueTestGuy(IEventAggregatorPublisher eventAggregatorPublisher, IEventAggregatorSubscriber eventAggregatorSubscriber, ICartonRequestManager cartonRequestManager, IProductionGroupManager productionGroupManager, IPackagingDesignManager designManager, IOptimalCorrugateCalculator optimalCorrugateCalculator, IArticleImporter articleImporter, ICustomJobImporter customJobImporter, ILogger logger)
//            : base(eventAggregatorPublisher, eventAggregatorSubscriber, cartonRequestManager, productionGroupManager, designManager, optimalCorrugateCalculator, articleImporter, customJobImporter, logger)
//        {
//        }

//        public ConcurrentDictionary<string /*ProductionGroupAlias*/, Tuple<ScanToQueueSelectorModes?, List<ICarton>>> PgQueues
//        {
//            get { return ProductionGroupQueues; }
//        }
    }
}
