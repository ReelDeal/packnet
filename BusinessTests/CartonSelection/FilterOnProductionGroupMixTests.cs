﻿//using PackNet.Business.PickZone;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Common.Interfaces.Services;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

//    using Testing.Specificity;

    [TestClass]
    public class FilterOnProductionGroupMixTests
    {
#if DEBUG
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
//        private WaveCartonRequestSelector selector;
//        private IList<ICarton> requests;
//        private Mock<ICartonRequestManager> requestManager;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IMachinesManager> machinesManager;
//        private Mock<IProductionGroupManager> sequenceGroupManager;
//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
//        private List<Corrugate> corrugates;
//        private Queue<string> groupNames;
//        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup;
//        private Mock<IPackagingDesignManager> designManagerMock;
//        private Mock<ICorrugateService> mockCorrugateCalculationService;
//        private Guid machine1Id = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            classificationsManager = new Mock<IClassificationsManager>();
//            corrugates = new List<Corrugate>();

//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(
//                manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            SetupRequestsManager();

//            SetupProductionGroupManagerMock();
//            SetUpMachinesManager();
//            SetupRequests();
//            SetUpCartonPropertyGroupManagerMock();
//            SetupDesignManagerMock();
//            SetupCorrugateCalculationsServiceMock();

//            selector = new NonParallelWaveCartonRequestSelector(
//                mockCorrugateCalculationService.Object,
//                requestManager.Object,
//                classificationsManager.Object,
//                new Mock<IPickZoneManager>().Object,
//                machinesManager.Object,
//                sequenceGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManagerMock.Object,
//                new SelectorSettings(2, true, 40, 40, 0),
//                new Mock<ILongHeadPositioningValidation>().Object,
//                new WmsConfiguration(), 
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//        }

//        private void SetupCorrugateCalculationsServiceMock()
//        {
//            mockCorrugateCalculationService = new Mock<ICorrugateService>();

//            mockCorrugateCalculationService.Setup(
//                           m =>
//                               m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                                   It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                           .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                           ((a, b, c, d, e) => a.Width != null && a.Width.Value < b.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalWidthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Width);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => a.Length);

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null ? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width
//                });

//            mockCorrugateCalculationService.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));
//        }

//        private void SetupRequestsManager()
//        {
//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(
//                manager =>
//                    manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>()))
//                .Returns(true);

//            requestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void NoProductionGroupShouldReturnNoJob()
//        {
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(
//                (CommonProductionGroups.ProductionGroup)null);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void EmptySequenceMixShouldReturnNoJob()
//        {
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void NoMatchingJobForSequenceMixShouldReturnNoJob()
//        {
//            SetUpSequenceMixWithoutAnyMatchingCartons();
//            cartonPropertyGroupManager.Setup(
//                manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>())).Returns(new CartonPropertyGroup());
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ReturnsCartonForFirstCartonPropertyGroup()
//        {
//            SetUpSequenceMixWithMatchingCartons();
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("1");
//            //sequenceGroup.Verify(g => g.UpdateItemMix(It.IsAny<string>()), Times.Once());
//            //sequenceGroup.Verify(g => g.UpdateItemMix("1"), Times.Once());
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ReturnsCartonForSecondCartonPropertyGroup()
//        {
//            SetUpSequenceMixWithMatchingCartons();
//            cartonPropertyGroupManager.Setup(
//                manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>())).Returns(new CartonPropertyGroup());
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequests => cartonRequests.Any(r => r.CartonPropertyGroupAlias == "1")), It.IsAny<Guid?>())).Returns(false);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("2");
//            //sequenceGroup.Verify(g => g.UpdateItemMix(It.IsAny<string>()), Times.Exactly(1));
//            //sequenceGroup.Verify(g => g.UpdateItemMix("2"), Times.Once());
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ReturnsCartonForSubstituteOfCartonPropertyGroupIfMainGroupIsNotAvailable()
//        {
//            SetUpSequenceMixWithMatchingCartonsAndSubstitute();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequests =>
//                        cartonRequests.Any(r => r.CartonPropertyGroupAlias == "1" || r.CartonPropertyGroupAlias == "2")), It.IsAny<Guid?>()))
//                .Returns(false);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("3");
//            //sequenceGroup.Verify(g => g.UpdateItemMix(It.IsAny<string>()), Times.Exactly(1));
//            //sequenceGroup.Verify(g => g.UpdateItemMix("2"), Times.Once());
//            cartonPropertyGroupManager.Verify(manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>()), Times.Exactly(3));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ReturnsNullIfRecursiveSubstitutionGroupsAndNoJobsForGroups()
//        {
//            SetupSequenceMixWithRecursiveSubstitureAndNoMatchingCartons();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequests =>
//                        cartonRequests.Any(r => r.CartonPropertyGroupAlias == "1" || r.CartonPropertyGroupAlias == "2")), It.IsAny<Guid?>()))
//                .Returns(false);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void SubstituteOfCartonPropertyGroupsShouldBeEvaluatedRecursively()
//        {
//            SetUpSequenceMixWithMatchingCartonsWithRecursiveSubstitutions();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequests =>
//                        cartonRequests.Any(r => r.CartonPropertyGroupAlias == "1" || r.CartonPropertyGroupAlias == "2")), It.IsAny<Guid?>()))
//                .Returns(false);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("3");
//            //sequenceGroup.Verify(g => g.UpdateItemMix(It.IsAny<string>()), Times.Once());
//            //sequenceGroup.Verify(g => g.UpdateItemMix("1"), Times.Once());
//            cartonPropertyGroupManager.Verify(manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>()), Times.Exactly(2));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldMoveToNextWhenNoSubstituteGroupIsAvailable()
//        {
//            SetUpSequenceMixWithMatchingCartonsWhereFirstHasSubstitute();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequests =>
//                        cartonRequests.Any(r => r.CartonPropertyGroupAlias == "1" || r.CartonPropertyGroupAlias == "3")), It.IsAny<Guid?>()))
//                .Returns(false);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("2");
//            //sequenceGroup.Verify(g => g.UpdateItemMix("2"), Times.Once());
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ReturnsNoJobIfNoCartonPropertyGroupIsOkToSendToMachine()
//        {
//            SetUpSequenceMixWithMatchingCartons();
//            cartonPropertyGroupManager.Setup(
//                manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>())).Returns(new CartonPropertyGroup());
//            requestManager.Setup(
//                manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(false);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result).Should.BeNull();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateMixForBothRequestsWhenProducingTiles()
//        {
//            var sequenceGroupMock = new Mock<CommonProductionGroups.ProductionGroup>();
//            var sequenceGroupManagerMock = new Mock<IProductionGroupManager>();
//            var cartonPropertyGroupManagerMock = new Mock<ICartonPropertyGroups>();
//            SetUpSequenceMixWithMatchingCartonsAndSubstituteForUpdateMixWhenProducingTiles(
//                sequenceGroupMock, sequenceGroupManagerMock, cartonPropertyGroupManagerMock);

//            var machineManagerMock = new Mock<IMachinesManager>();
//            SetUpMachinesManagerForUpdateMixWhenProducingTiles(machineManagerMock, sequenceGroupMock);

//            var cartonRequests = new List<Carton>();
//            var requestsManagerMock = new Mock<ICartonRequestManager>();
//            requestsManagerMock.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//            SetupRequestsForUpdateMixWhenProducingTiles(cartonRequests, requestsManagerMock);

//            var cartonRequestSelector = new NonParallelWaveCartonRequestSelector(
//                null,
//                requestsManagerMock.Object,
//                new Mock<IClassificationsManager>().Object,
//                new Mock<IPickZoneManager>().Object,
//                machineManagerMock.Object,
//                sequenceGroupManagerMock.Object,
//                cartonPropertyGroupManagerMock.Object,
//                designManagerMock.Object,
//                new SelectorSettings(2, true, 40, 40, 0),
//                new Mock<ILongHeadPositioningValidation>().Object,
//                new WmsConfiguration(), 
//                new Mock<ILogger>(MockBehavior.Loose).Object);

//            var result = cartonRequestSelector.GetNextJobByCartonPropertyGroupMix(
//                cartonRequests,
//                cartonRequests,
//                CartonPropertyGroupStatus.Normal,
//                machineManagerMock.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.Count()).Should.BeEqualTo(2);
//            //sequenceGroupMock.Verify(g => g.UpdateItemMix(It.IsAny<string>()), Times.Exactly(2));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateMixForMainItemForEachRequestsThatIsOfASubstituteItem()
//        {
//            SetupRequestSet2();
//            var corrugate = new Corrugate { Width = 1600 };
//            corrugates.Add(corrugate);
//            SetUpSequenceMixWithMatchingCartonsAndSubstitute();
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                requests,
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.Count()).Should.BeEqualTo(2);
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("3");
//            Specify.That(result.Requests.ElementAt(1).CartonPropertyGroupAlias).Should.BeEqualTo("3");
//            //sequenceGroup.Verify(g => g.UpdateItemMix("2"), Times.Exactly(2));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateMixForEachRequestAccordingToIndividualCartonPropertyGroup()
//        {
//            SetupRequestSet2();
//            var corrugate = new Corrugate { Width = 1600 };
//            corrugates.Add(corrugate);
//            requests.ElementAt(0).CartonPropertyGroupAlias = "1";
//            SetUpSequenceMixWithMatchingCartonsAndSubstitute();
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                requests,
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.Count()).Should.BeEqualTo(2);
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("1");
//            Specify.That(result.Requests.ElementAt(1).CartonPropertyGroupAlias).Should.BeEqualTo("3");
//            //sequenceGroup.Verify(g => g.UpdateItemMix("1"), Times.Once());
//            //sequenceGroup.Verify(g => g.UpdateItemMix("3"), Times.Once());
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotUpdateMixForSkippedCartonPropertyGroupsIfProducibleOnOtherMachine()
//        {
//            SetupRequests();
//            SetUpSequenceMixWithMatchingCartons();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequests => cartonRequests.Any(r => r.CartonPropertyGroupAlias == "1")), It.IsAny<Guid?>())).Returns(false);
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                requests,
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result.Requests.Count()).Should.BeEqualTo(1);
//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("2");
//            //sequenceGroup.Verify(g => g.UpdateItemMix("2"), Times.Once());
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateMixForSkippedCartonPropertyGroupsIfNoProducibleJobExists()
//        {
//            SetupRequestSet3();
//            SetUpSequenceMixWithMatchingCartons();
//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                requests,
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            Specify.That(result).Should.Not.BeNull();
//            //sequenceGroup.Verify(g => g.UpdateItemMix("1"), Times.Once());
//            //sequenceGroup.Verify(g => g.UpdateItemMix("2"), Times.Once());
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotUpdateMixForSkippedCartonPropertyGroupsIfProducibleJobExists()
//        {
//            SetupRequestSet4();
//            SetUpSequenceMixWithMatchingCartons();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequests => cartonRequests.Any(r => r.CartonPropertyGroupAlias == "1")), It.IsAny<Guid?>())).Returns(false);

//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                requests,
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));

//            Specify.That(result).Should.Not.BeNull();
//            //sequenceGroup.Verify(g => g.UpdateItemMix("1"), Times.Never());
//            //sequenceGroup.Verify(g => g.UpdateItemMix("2"), Times.Once());
//            //sequenceGroup.Verify(g => g.UpdateItemMix(It.IsAny<string>()), Times.Exactly(1));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateItemMixOnceOnHasRequestsForCartonPropertyGroup()
//        {
//            SetUpSequenceMixWithNoSubstitutions();
//            SetupRequestSet5();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequest =>
//                        cartonRequest.Any(
//                            request =>
//                            request.CartonPropertyGroupAlias == "1" || request.CartonPropertyGroupAlias == "2"
//                            || request.CartonPropertyGroupAlias == "3")), It.IsAny<Guid?>())).Returns(false);

//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));

//            Specify.That(result).Should.BeNull();
//            //sequenceGroup.Verify(g => g.UpdateItemMix("3"), Times.Once());
//            cartonPropertyGroupManager.Verify(
//                manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>()), Times.Exactly(3));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotUpdateSQMixWhenSurging()
//        {
//            SetUpSequenceMixWithNoSubstitutions();
//            SetupRequestSet5();
//            requestManager.Setup(
//                manager =>
//                manager.RequestsOkToSendToMachineAndRegister(
//                    It.Is<IEnumerable<ICarton>>(
//                        cartonRequest =>
//                        cartonRequest.Any(
//                            request =>
//                            request.CartonPropertyGroupAlias == "1" || request.CartonPropertyGroupAlias == "2"
//                            || request.CartonPropertyGroupAlias == "3")), It.IsAny<Guid?>())).Returns(false);

//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Surge,
//                machinesManager.Object.Machines.ElementAt(0));

//            Specify.That(result).Should.BeNull();
//            //sequenceGroup.Verify(g => g.UpdateItemMix(It.IsAny<string>()), Times.Never());
//            cartonPropertyGroupManager.Verify(
//                manager => manager.GetCartonPropertyGroupByAlias(It.IsAny<string>()), Times.Exactly(3));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateSQMixForStoppedCPGsWhenANormalJobIsProduced()
//        {
//            var numberOfSurgedGroups = 0;
//            var numberOfNormalGroups = 1;
//            var numberOfStoppedGroups = 2;

//            var numberOfCPGs = numberOfStoppedGroups + numberOfNormalGroups + numberOfSurgedGroups;

//            SetupSelectorForStoppedCPGTest(numberOfCPGs);
//            SetUpProductionMix(numberOfStoppedGroups, numberOfNormalGroups, numberOfSurgedGroups);
//            SetupRequestSet5();
//            /*
//                new Carton { CartonPropertyGroupAlias = "1", DesignId = 1, Length = 800, Width = 800 },
//                new Carton { CartonPropertyGroupAlias = "2", DesignId = 1, Length = 800, Width = 800 },
//                new Carton { CartonPropertyGroupAlias = "3", DesignId = 1, Length = 800, Width = 800 }
//             */

//            SetupRequestManagerWithXNumberOfCPGs(numberOfCPGs);

//            var result = selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));

//            //sequenceGroup.Verify(sg => sg.UpdateItemMix(It.IsAny<string>()), Times.Exactly((numberOfNormalGroups + numberOfStoppedGroups) * numberOfNormalGroups));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldUpdateSQMixForStoppedCPGsWhenANormalJobIsProducedWithRandomNumberOfJobs()
//        {
//            var rd = new Random(Guid.NewGuid().GetHashCode());

//            var numberOfSurgedGroups = 0;
//            var numberOfNormalGroups = rd.Next(1, 201);
//            var numberOfStoppedGroups = rd.Next(1, 201);

//            var numberOfCPGs = numberOfStoppedGroups + numberOfNormalGroups + numberOfSurgedGroups;

//            SetupSelectorForStoppedCPGTest(numberOfCPGs);
//            SetUpProductionMix(numberOfStoppedGroups, numberOfNormalGroups, numberOfSurgedGroups);
//            SetupRequestSet5();
//            /*
//                new Carton { CartonPropertyGroupAlias = "1", DesignId = 1, Length = 800, Width = 800 },
//                new Carton { CartonPropertyGroupAlias = "2", DesignId = 1, Length = 800, Width = 800 },
//                new Carton { CartonPropertyGroupAlias = "3", DesignId = 1, Length = 800, Width = 800 }
//             */

//            SetupRequestManagerWithXNumberOfCPGs(numberOfCPGs);

//            for (int i = 0; i < numberOfNormalGroups; i++)
//            {
//                selector.GetNextJobByCartonPropertyGroupMix(
//                requests,
//                new List<ICarton>(),
//                CartonPropertyGroupStatus.Normal,
//                machinesManager.Object.Machines.ElementAt(0));
//            }

//            //sequenceGroup.Verify(sg => sg.UpdateItemMix(It.IsAny<string>()), Times.Exactly((numberOfNormalGroups + numberOfStoppedGroups) * numberOfNormalGroups));
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        private void SetupRequestManagerWithXNumberOfCPGs(int numberOfCPGs)
//        {
//            var cpgs = new List<string>();

//            for (int i = 0; i < numberOfCPGs; i++)
//            {
//                cpgs.Add((i + 1).ToString());
//            }


//            requestManager.Setup(
//                manager =>
//                    manager.RequestsOkToSendToMachineAndRegister(
//                        It.Is<IEnumerable<ICarton>>(
//                            cartonRequest =>
//                                cartonRequest.Any(
//                                    request =>
//                                        cpgs.Contains(request.CartonPropertyGroupAlias))), It.IsAny<Guid?>())).Returns(true);
//            requestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
//        }

//        public void SetupSelectorForStoppedCPGTest(int numberOfSequenceItems)
//        {
//            corrugates = new List<Corrugate>();

//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(
//                manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);

//            SetupSequenceGroupManagerMockForStoppedCPGTest(numberOfSequenceItems);
//            SetUpMachinesManager();
//            SetupRequests();
//            SetUpCartonPropertyGroupManagerMock();

//            selector = new NonParallelWaveCartonRequestSelector(
//                null,
//                requestManager.Object,
//                classificationsManager.Object,
//                new Mock<IPickZoneManager>().Object,
//                machinesManager.Object,
//                sequenceGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManagerMock.Object,
//                new SelectorSettings(2, true, 40, 40, 0),
//                new Mock<ILongHeadPositioningValidation>().Object,
//                new WmsConfiguration(), 
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//        }

//        public void SetupSequenceGroupManagerMockForStoppedCPGTest(int numberOfSequenceItems)
//        {
//            var sequenceItemArray = new ProductionItem[numberOfSequenceItems];

//            for (int i = 0; i < numberOfSequenceItems; i++)
//            {
//                sequenceItemArray[i] = new ProductionItem((i + 1).ToString(), 1);
//            }

//            sequenceGroup = new Mock<CommonProductionGroups.ProductionGroup>();

//            groupNames = new Queue<string>();
//            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns<List<string>>(
//            //    l => ProductionGroupMix(groupNames, l));
//            //sequenceGroup.Setup(g => g.ProductionItems).Returns(sequenceItemArray);
//            //sequenceGroup.SetupGet(g => g.Machines).Returns(new[] { new MachineInformation(1) });

//            sequenceGroupManager = new Mock<IProductionGroupManager>();
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(
//                sequenceGroup.Object);
//            sequenceGroupManager.Setup(
//                manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(
//                    new[] { sequenceGroup.Object });
//            Assert.Fail("Move mix to selection algorithm");
//        }

//        private void SetUpMachinesManager()
//        {
//            corrugates.Add(new Corrugate { Width = 1000 });
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = new Corrugate { Width = 1000 } },
//            });
//            machine.SetupGet(m => m.Id).Returns(machine1Id);

//            machinesManager = new Mock<IMachinesManager>();

//            machinesManager.Setup(manager => manager.Machines).Returns(
//                new List<IPacksizeCutCreaseMachine> { machine.Object });

//            machinesManager.Setup(
//                manager => manager.GetCorrugatesByExclusiveness(machine.Object, new[] { sequenceGroup.Object })).Returns(
//                    corrugates);
//        }

//        private void SetupProductionGroupManagerMock()
//        {
//            groupNames = new Queue<string>();
//            sequenceGroup = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns<List<string>>(
//            //    l => ProductionGroupMix(groupNames, l));
//            //sequenceGroup.Setup(g => g.ProductionItems).Returns(
//            //    new[] { new ProductionItem("1", 1), new ProductionItem("2", 1) });
//            //sequenceGroup.SetupGet(g => g.Machines).Returns(new[] { new MachineInformation(1) });

//            sequenceGroupManager = new Mock<IProductionGroupManager>();
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(
//                sequenceGroup.Object);
//            sequenceGroupManager.Setup(
//                manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(
//                    new[] { sequenceGroup.Object });
//        }

//        private void SetupDesignManagerMock()
//        {
//            var packagingDesignOne = new PackagingDesign { Name = "1", Id = 1, LengthOnZFold = "L", WidthOnZFold = "W" };
//            designManagerMock = new Mock<IPackagingDesignManager>();
//            designManagerMock.Setup(m => m.GetDesign(It.IsAny<DesignQueryParam>())).Returns(packagingDesignOne);
//        }

//        private void SetUpCartonPropertyGroupManagerMock()
//        {
//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
//        }

//        private void SetupRequests()
//        {
//            requests = new List<ICarton>
//                {
//                    new Carton { CartonPropertyGroupAlias = "1", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "2", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "1", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "2", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "3", DesignId = 1, Length = 800, Width = 800 }
//                };

//            requestManager.Setup(r => r.ValidRequestsInActivePickzones).Returns(requests);
//        }

//        private void SetupRequestSet2()
//        {
//            requests = new List<ICarton>
//                {
//                    new Carton { CartonPropertyGroupAlias = "3", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "3", DesignId = 1, Length = 800, Width = 800 }
//                };

//            requestManager.Setup(r => r.ValidRequestsInActivePickzones).Returns(requests);
//        }

//        private void SetupRequestSet3()
//        {
//            requests = new List<ICarton>
//                {
//                    new Carton { CartonPropertyGroupAlias = "2", DesignId = 1, Length = 800, Width = 800 } 
//                };

//            requestManager.Setup(r => r.ValidRequestsInActivePickzones).Returns(requests);
//        }

//        private void SetupRequestSet4()
//        {
//            requests = new List<ICarton>
//                {
//                    new Carton { CartonPropertyGroupAlias = "1", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "2", DesignId = 1, Length = 800, Width = 800 }
//                };

//            requestManager.Setup(r => r.ValidRequestsInActivePickzones).Returns(requests);
//        }

//        private void SetupRequestSet5()
//        {
//            requests = new List<ICarton>
//                {
//                    new Carton { CartonPropertyGroupAlias = "1", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "2", DesignId = 1, Length = 800, Width = 800 },
//                    new Carton { CartonPropertyGroupAlias = "3", DesignId = 1, Length = 800, Width = 800 }
//                };
//        }

//        private void SetupRequestsForUpdateMixWhenProducingTiles(
//            ICollection<Carton> cartonRequests, Mock<ICartonRequestManager> requestManagerMock)
//        {
//            requestManagerMock.Setup(
//                manager =>
//                    manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<Guid?>())).Returns(true);

//            var request1 = new Carton { CartonPropertyGroupAlias = "CPG1", DesignId = 1, Length = 800, Width = 800 };
//            var request2 = new Carton { CartonPropertyGroupAlias = "CPG2", DesignId = 1, Length = 800, Width = 800 };
//            var request3 = new Carton { CartonPropertyGroupAlias = "CPG1", DesignId = 1, Length = 800, Width = 800 };
//            var request4 = new Carton { CartonPropertyGroupAlias = "CPG2", DesignId = 1, Length = 800, Width = 800 };
//            var request5 = new Carton { CartonPropertyGroupAlias = "CPG3", DesignId = 1, Length = 800, Width = 800 };

//            cartonRequests.Add(request1);
//            cartonRequests.Add(request2);
//            cartonRequests.Add(request3);
//            cartonRequests.Add(request4);
//            cartonRequests.Add(request5);

//            requestManagerMock.Setup(r => r.ValidRequestsInActivePickzones).Returns(cartonRequests);
//        }

//        private string ProductionGroupMix(IEnumerable<string> groups, IEnumerable<string> filter)
//        {
//            var groupsToSelectFrom = groups.Where(g => filter.Contains(g) == false);
//            var group = groupsToSelectFrom.FirstOrDefault();

//            if (string.IsNullOrEmpty(group))
//            {
//                group = string.Empty;
//            }

//            return group;
//        }

//        private void SetUpSequenceMixWithNoSubstitutions()
//        {
//            groupNames.Enqueue("1");
//            groupNames.Enqueue("2");
//            groupNames.Enqueue("3");

//            var group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("1");
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(group.Object);

//            group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("2");
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("2")).Returns(group.Object);

//            group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("3");
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("3")).Returns(group.Object);

//            var g3 = new Mock<CartonPropertyGroup>();
//            g3.Setup(s => s.Alias).Returns("3");

//            var g2 = new Mock<CartonPropertyGroup>();
//            g2.Setup(g => g.Alias).Returns("2");

//            var g1 = new Mock<CartonPropertyGroup>();
//            g1.Setup(g => g.Alias).Returns("1");

//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(g1.Object);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("2")).Returns(g2.Object);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("3")).Returns(g3.Object);
//        }

//        private void SetUpProductionMix(int stopped, int normal, int surged)
//        {
//            var stoppedGroups = new List<CartonPropertyGroup>();
//            var normalGroups = new List<CartonPropertyGroup>();
//            var surgedGroups = new List<CartonPropertyGroup>();

//            var numberOfStopped = 0;
//            var numberOfNormal = 0;
//            var numberOfSurged = 0;

//            for (int i = 0; i < (stopped + normal + surged); i++)
//            {
//                var group = new Mock<CartonPropertyGroup>();
//                var id = (i + 1).ToString();
//                groupNames.Enqueue(id);

//                if (numberOfStopped < stopped)
//                {
//                    group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Stop);
//                    stoppedGroups.Add(group.Object);
//                    numberOfStopped++;
//                }
//                else if (numberOfNormal < normal)
//                {
//                    group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Normal);
//                    normalGroups.Add(group.Object);
//                    numberOfNormal++;
//                }
//                else if (numberOfSurged < surged)
//                {
//                    group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Surge);
//                    surgedGroups.Add(group.Object);
//                    numberOfSurged++;
//                }

//                group.Setup(g => g.Alias).Returns(id);
//                cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias(id)).Returns(group.Object);
//            }

//            var allgroups = new List<CartonPropertyGroup>();

//            allgroups.AddRange(surgedGroups);
//            allgroups.AddRange(normalGroups);
//            allgroups.AddRange(stoppedGroups);

//            cartonPropertyGroupManager.Setup(manager => manager.Groups).Returns(allgroups);

//            cartonPropertyGroupManager.Setup(
//                manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Stop)).Returns(stoppedGroups);

//            cartonPropertyGroupManager.Setup(
//                manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Normal)).Returns(normalGroups);

//            cartonPropertyGroupManager.Setup(
//                manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Surge)).Returns(surgedGroups);
//        }

//        private void SetUpSequenceMixWithMatchingCartonsWithRecursiveSubstitutions()
//        {
//            SetUpSequenceMixWithMatchingCartons();

//            var g3 = new Mock<CartonPropertyGroup>();
//            g3.Setup(s => s.Alias).Returns("3");

//            var g2 = new Mock<CartonPropertyGroup>();
//            g2.Setup(g => g.Alias).Returns("2");
//            g2.Object.SubstitutionGroup = g3.Object;

//            var g1 = new Mock<CartonPropertyGroup>();
//            g1.Setup(g => g.Alias).Returns("1");
//            g1.Object.SubstitutionGroup = g2.Object;

//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(g1.Object);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("2")).Returns(g2.Object);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("3")).Returns(g3.Object);
//        }

//        private void SetUpSequenceMixWithMatchingCartonsWhereFirstHasSubstitute()
//        {
//            SetUpSequenceMixWithMatchingCartons();

//            var g3 = new Mock<CartonPropertyGroup>();
//            g3.Setup(s => s.Alias).Returns("3");

//            var g1 = new Mock<CartonPropertyGroup>();
//            g1.Setup(g => g.Alias).Returns("1");
//            g1.Object.SubstitutionGroup = g3.Object;

//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(g1.Object);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("3")).Returns(g3.Object);
//        }

//        private void SetUpSequenceMixWithMatchingCartonsAndSubstitute()
//        {
//            SetUpSequenceMixWithMatchingCartons();

//            var sub = new Mock<CartonPropertyGroup>();
//            sub.Setup(s => s.Alias).Returns("3");

//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("3")).Returns(sub.Object);

//            var group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("2");
//            group.Object.SubstitutionGroup = sub.Object;
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("2")).Returns(group.Object);

//            group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("1");
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(group.Object);
//        }

//        private void SetUpMachinesManagerForUpdateMixWhenProducingTiles(
//            Mock<IMachinesManager> machinesManagerMock,
//            Mock<CommonProductionGroups.ProductionGroup> sequenceGroupMock)
//        {
//            var corrugatesLocal = new List<Corrugate>();
//            var corrugate = new Corrugate { Width = 1600 };
//            corrugatesLocal.Add(corrugate);

//            corrugate = new Corrugate { Width = 1000 };
//            corrugatesLocal.Add(corrugate);

//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.Setup(m => m.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = corrugatesLocal.First()},
//                new Track{ TrackNumber = 2, LoadedCorrugate = corrugatesLocal.Last()},
//            });
//            machine.SetupGet(m => m.Id).Returns(machine1Id);

//            machinesManagerMock.Setup(manager => manager.Machines).Returns(new List<IPacksizeCutCreaseMachine> { machine.Object });

//            machinesManagerMock.Setup(
//                manager => manager.GetCorrugatesByExclusiveness(machine.Object, new[] { sequenceGroupMock.Object })).Returns(
//                    corrugatesLocal);
//        }

//        private void SetUpSequenceMixWithMatchingCartonsAndSubstituteForUpdateMixWhenProducingTiles(
//            Mock<CommonProductionGroups.ProductionGroup> sequenceGroupMock,
//            Mock<IProductionGroupManager> sequenceGroupManagerMock,
//            Mock<ICartonPropertyGroups> cartonPropertyGroupManagerMock)
//        {
//            var groupNamesLocal = new Queue<string>();
//            groupNamesLocal.Enqueue("CPG1");
//            groupNamesLocal.Enqueue("CPG2");

//            //sequenceGroupMock.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns<List<string>>(
//            //    l => ProductionGroupMix(groupNamesLocal, l));
//            //sequenceGroupMock.Setup(g => g.ProductionItems).Returns(
//            //    new[] { new ProductionItem("CPG1", 1), new ProductionItem("CPG2", 1) });
//            //sequenceGroupMock.SetupGet(g => g.Machines).Returns(new[] { new MachineInformation(1) });

//            sequenceGroupManagerMock.Setup(manager => manager.GetProductionGroupForMachine(It.IsAny<Guid>())).Returns(
//                sequenceGroupMock.Object);
//            sequenceGroupManagerMock.Setup(
//                manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(
//                    new[] { sequenceGroupMock.Object });

//            var sub = new Mock<CartonPropertyGroup>();
//            sub.Setup(s => s.Alias).Returns("CPG3");

//            cartonPropertyGroupManagerMock.Setup(manager => manager.GetCartonPropertyGroupByAlias("CPG3")).Returns(sub.Object);

//            var group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("CPG2");
//            group.Object.SubstitutionGroup = sub.Object;
//            cartonPropertyGroupManagerMock.Setup(manager => manager.GetCartonPropertyGroupByAlias("CPG2")).Returns(group.Object);

//            group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("CPG1");
//            cartonPropertyGroupManagerMock.Setup(manager => manager.GetCartonPropertyGroupByAlias("CPG1")).Returns(group.Object);
//        }

//        private void SetupSequenceMixWithRecursiveSubstitureAndNoMatchingCartons()
//        {
//            SetUpSequenceMixWithMatchingCartons();

//            var main = new Mock<CartonPropertyGroup>();
//            main.Setup(s => s.Alias).Returns("1");
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(main.Object);

//            var sub = new Mock<CartonPropertyGroup>();
//            sub.Setup(s => s.Alias).Returns("2");
//            sub.Object.SubstitutionGroup = main.Object;
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(sub.Object);

//            main.Object.SubstitutionGroup = sub.Object;
//        }

//        private void SetUpSequenceMixWithMatchingCartons()
//        {
//            groupNames.Enqueue("1");
//            groupNames.Enqueue("2");

//            var group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("1");
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("1")).Returns(group.Object);

//            group = new Mock<CartonPropertyGroup>();
//            group.Setup(g => g.Alias).Returns("2");
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("2")).Returns(group.Object);
//        }

//        private void SetUpSequenceMixWithoutAnyMatchingCartons()
//        {
//            groupNames.Enqueue("4");
//        }
    }
}
