﻿using PackNet.Business.PickZone;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Data.WMS;

namespace BusinessTests.CartonSelection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using PackNet.Business.Carton.TODO;
    using PackNet.Business.CartonPropertyGroup;
    using PackNet.Business.CartonSelection;
    using PackNet.Business.Classifications;
    using PackNet.Business.Machines;
    using PackNet.Business.PackagingDesigns;
    using PackNet.Business.ProductionGroups;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.PickZones;
    using PackNet.Common.Interfaces.DTO.Settings;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Services;
    using PackNet.Common.Logging;

    using TechTalk.SpecFlow;

    using Testing.Specificity;
    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

    [Binding]
    public class CartonRequestSelectorPriorityOrderSteps
    {
        #region Fields
        private WaveCartonRequestSelector cartonRequestSelector;
        private Mock<ICartonRequestManager> cartonRequestManager;
        private Mock<IClassificationsManager> classificationsManager;
        private Mock<IPickZoneManager> pickAreaManager;
        private Mock<IMachinesManager> machinesManager;
        private Mock<IProductionGroupManager> sequenceGroupManager;
        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup;
        private List<ICarton> cartonRequestsInRepository;
        private List<PackNet.Common.Interfaces.DTO.Classifications.Classification> classifications;
        private List<PickZone> zones;
        private List<CartonPropertyGroup> groups;
        private List<Corrugate> corrugates;
        private IPackagingDesignManager designManager;
        private ICartonRequestOrder selectedOrder;
        private Mock<ICarton> c1;
        private Mock<ICarton> c2;
        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;
        private Mock<ICorrugateService> mockCorrugateCalculationService;
        private Guid machine1Id = Guid.NewGuid();

        #endregion

        [BeforeScenario("CartonRequestSelectorPriorityOrder")]
        [DeploymentItem("Designs\\")]
        public void BeforeScenario()
        {
            SetUpCorrugates();
            SetUpCartonRequestManagerMock();
            SetUpClassificationsManagerMock();
            SetUpPickAreaManagerMock();
            SetUpProductionGroupManagerMock();
            SetUpMachinesManagerMock();
            SetUpCartonPropertyGroupManagerMock();

            mockCorrugateCalculationService = new Mock<ICorrugateService>();

            designManager = new PackagingDesignManager("Designs", new Mock<ILogger>(MockBehavior.Loose).Object);
            designManager.LoadDesigns();

            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();

            var backend = new Mock<ILoggingBackend>(MockBehavior.Loose);
            backend.Setup(b => b.GetLog()).Returns(new Mock<ILogger>(MockBehavior.Loose).Object);
            backend.Setup(b => b.GetLogFor(It.IsAny<string>())).Returns(new Mock<ILogger>(MockBehavior.Loose).Object);
            LogManager.LoggingBackend = backend.Object;

            cartonRequestSelector = new NonParallelWaveCartonRequestSelector(
                mockCorrugateCalculationService.Object,
                cartonRequestManager.Object,
                classificationsManager.Object,
                pickAreaManager.Object,
                machinesManager.Object,
                sequenceGroupManager.Object,
                cartonPropertyGroupManager.Object,
                designManager,
                new SelectorSettings(1, true, 30, 20, 7200000),
                longHeadPositioningValidation.Object,
                new WmsConfiguration(), 
                new Mock<ILogger>(MockBehavior.Loose).Object);
        }

        [AfterScenario("CartonRequestSelectorPriorityOrder")]
        public void AfterScenario()
        {
            cartonRequestsInRepository.Clear();
        }

        [Given(@"I have a carton C1 to classification 1, carton property group 1, pickzone 1, suitable for corrugate 1")]
        public void GivenIHaveACartonC1ToClassification1CartonPropertyGroup1Pickzone1SuitableForCorrugate1()
        {
            c1 = new Mock<ICarton>();
            c1.Setup(c => c.DesignId).Returns(1);
            c1.Setup(c => c.ClassificationNumber).Returns("1");
            c1.Setup(c => c.PickZone).Returns("1");
            c1.Setup(c => c.CartonPropertyGroupAlias).Returns("1");
            c1.Setup(c => c.SerialNumber).Returns("C1");
            c1.Setup(c => c.Created).Returns(DateTime.Now.Subtract(new TimeSpan(0, 0, 2)));
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "1"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(true);
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "2"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(false);
            c1.Setup(c => c.GetMaximumNumberOfTilesOnMachine(It.IsAny<Guid>())).Returns(int.MaxValue);
            cartonRequestsInRepository.Add(c1.Object);
        }

        [Given(@"I have a carton C2 to classiciation 2, carton property group 2, pickzone 1, suitable for corrugate 2")]
        public void GivenIHaveACartonC2ToClassiciation2CartonPropertyGroup2Pickzone1SuitableForCorrugate2()
        {
            c2 = new Mock<ICarton>();
            c2.Setup(c => c.DesignId).Returns(1);
            c2.Setup(c => c.ClassificationNumber).Returns("2");
            c2.Setup(c => c.PickZone).Returns("1");
            c2.Setup(c => c.CartonPropertyGroupAlias).Returns("2");
            c2.Setup(c => c.SerialNumber).Returns("C2");
            c2.Setup(c => c.Created).Returns(DateTime.Now);
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "1"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(true);
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "2"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(false);
            c2.Setup(c => c.GetMaximumNumberOfTilesOnMachine(It.IsAny<Guid>())).Returns(int.MaxValue);
            cartonRequestsInRepository.Add(c2.Object);
        }

        [Given(@"I have a carton C2 to classiciation 2, carton property group 1, pickzone 1, suitable for corrugate 2")]
        public void GivenIHaveACartonC2ToClassiciation2CartonPropertyGroup1Pickzone1SuitableForCorrugate2()
        {
            c2 = new Mock<ICarton>();
            c2.Setup(c => c.DesignId).Returns(1);
            c2.Setup(c => c.ClassificationNumber).Returns("2");
            c2.Setup(c => c.PickZone).Returns("1");
            c2.Setup(c => c.CartonPropertyGroupAlias).Returns("1");
            c2.Setup(c => c.SerialNumber).Returns("C2");
            c2.Setup(c => c.Created).Returns(DateTime.Now);
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "1"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(false);
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "2"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(true);
            c2.Setup(c => c.GetMaximumNumberOfTilesOnMachine(It.IsAny<Guid>())).Returns(int.MaxValue);
            cartonRequestsInRepository.Add(c2.Object);
        }

        [Given(@"I have a carton C2 to classiciation 2, carton property group 1, pickzone 1, suitable for corrugate 1")]
        public void GivenIHaveACartonC2ToClassiciation2CartonPropertyGroup1Pickzone1SuitableForCorrugate1()
        {
            c2 = new Mock<ICarton>();
            c2.Setup(c => c.DesignId).Returns(1);
            c2.Setup(c => c.ClassificationNumber).Returns("2");
            c2.Setup(c => c.PickZone).Returns("1");
            c2.Setup(c => c.CartonPropertyGroupAlias).Returns("1");
            c2.Setup(c => c.SerialNumber).Returns("C2");
            c2.Setup(c => c.Created).Returns(DateTime.Now);
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "1"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(true);
            mockCorrugateCalculationService.Setup(c => c.FitsOnCorrugate(It.IsAny<ICarton>(), It.Is<Corrugate>(co => co.Alias == "2"), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(false);
            c2.Setup(c => c.GetMaximumNumberOfTilesOnMachine(It.IsAny<Guid>())).Returns(int.MaxValue);
            cartonRequestsInRepository.Add(c2.Object);
        }

        [Given(@"classification 1 has expedite status")]
        public void GivenClassification1HasExpediteStatus()
        {
            var classification = new Mock<PackNet.Common.Interfaces.DTO.Classifications.Classification>();
            classification.Setup(c => c.Number).Returns("1");
            classification.Setup(c => c.Status).Returns(ClassificationStatus.Expedite);
            classifications.Add(classification.Object);
        }

        [Given(@"classification 1 has normal status")]
        public void GivenClassification1HasNormalStatus()
        {
            var classification = new Mock<PackNet.Common.Interfaces.DTO.Classifications.Classification>();
            classification.Setup(c => c.Number).Returns("1");
            classification.Setup(c => c.Status).Returns(ClassificationStatus.Normal);
            classifications.Add(classification.Object);
        }

        [Given(@"classification 2 has normal status")]
        public void GivenClassification2HasNormalStatus()
        {
            var classification = new Mock<PackNet.Common.Interfaces.DTO.Classifications.Classification>();
            classification.Setup(c => c.Number).Returns("2");
            classification.Setup(c => c.Status).Returns(ClassificationStatus.Normal);
            classifications.Add(classification.Object);
        }

        [Given(@"carton property group 1 has normal status")]
        public void GivenCartonPropertyGroup1HasNormalStatus()
        {
            var group = new Mock<CartonPropertyGroup>();
            group.Setup(g => g.Alias).Returns("1");
            group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Normal);
            groups.Add(group.Object);
            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroupByAlias("1")).Returns(group.Object);
        }

        [Given(@"carton property group 1 has surged status")]
        public void GivenCartonPropertyGroup1HasSurgedStatus()
        {
            var group = new Mock<CartonPropertyGroup>();
            group.Setup(g => g.Alias).Returns("1");
            group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Surge);
            groups.Add(group.Object);
            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroupByAlias("1")).Returns(group.Object);
        }

        [Given(@"carton property group 2 has normal status")]
        public void GivenCartonPropertyGroup2HasNormalStatus()
        {
            var group = new Mock<CartonPropertyGroup>();
            group.Setup(g => g.Alias).Returns("2");
            group.Setup(g => g.Status).Returns(CartonPropertyGroupStatus.Normal);
            groups.Add(group.Object);
            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroupByAlias("2")).Returns(group.Object);
        }

        [Given(@"carton property group 1 is next in sequence mix")]
        public void GivenCartonPropertyGroup1IsNextInSequenceMix()
        {
            Assert.Fail("Need to move mix out of pg");
            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(l => l.Count == 0))).Returns("1");
            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(l => l.Count > 0))).Returns("2");
        }

        [Given(@"carton property group 2 is next in sequence mix")]
        public void GivenCartonPropertyGroup2IsNextInSequenceMix()
        {
            Assert.Fail("Need to move mix out of pg");
            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(l => l.Count == 0))).Returns("2");
            //sequenceGroup.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(l => l.Count > 0))).Returns("1");
        }

        [Given(@"corrugate 2 is more exclusive than corrugate 1")]
        public void GivenCorrugate2IsMoreExclusiveThanCorrugate1()
        {
            var list = corrugates.ToList();
            list.Reverse();
            machinesManager
                .Setup(manager => manager.GetCorrugatesByExclusiveness(It.IsAny<IPacksizeCutCreaseMachine>(), new CommonProductionGroups.ProductionGroup[] { sequenceGroup.Object }))
                .Returns(() => list);
        }

        [Given(@"C2 gives better yield than C1")]
        public void GivenC2GivesBetterYieldThanC1()
        {
            mockCorrugateCalculationService.Setup(c => c.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(200);
            mockCorrugateCalculationService.Setup(c => c.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(200);
            mockCorrugateCalculationService.Setup(c => c.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(100);
            mockCorrugateCalculationService.Setup(c => c.CalculateTotalLengthOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), designManager.GetDesign, false)).Returns(200);
        }

        [When(@"next carton is requested by a machine")]
        public void WhenNextCartonIsRequestedByAMachine()
        {
            selectedOrder = cartonRequestSelector.GetNextJobForMachine(machinesManager.Object.Machines.ElementAt(0));
        }

        [Then(@"C1 is returned as next carton")]
        public void ThenC1IsReturnedAsNextCarton()
        {
            Specify.That(selectedOrder.Requests.Count()).Should.BeEqualTo(1);
            Specify.That(selectedOrder.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("C1");
        }

        [Then(@"C2 is returned as next carton")]
        public void ThenC2IsReturnedAsNextCarton()
        {
            Specify.That(selectedOrder.Requests.Count()).Should.BeEqualTo(1);
            Specify.That(selectedOrder.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("C2");
        }

        private void SetUpCartonRequestManagerMock()
        {
            cartonRequestsInRepository = new List<ICarton>();

            cartonRequestManager = new Mock<ICartonRequestManager>();
            cartonRequestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(() => cartonRequestsInRepository);
            cartonRequestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
            cartonRequestManager.SetupGet(manager => manager.CartonRequests).Returns(cartonRequestsInRepository);
            cartonRequestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
                It.IsAny<CartonOnCorrugate>(),
                It.IsAny<IPacksizeCutCreaseMachine>(),
                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
                It.IsAny<bool>(),
                It.IsAny<bool>(),
                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
                It.IsAny<int?>(),
                It.IsAny<bool>())).Returns<
                IEnumerable<ICarton>,
                Corrugate,
                IPacksizeCutCreaseMachine,
                PackNet.Common.Interfaces.DTO.Classifications.Classification,
                bool,
                bool,
                Func<DesignQueryParam, IPackagingDesign>,
                int?,
                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h });
        }

        private void SetUpClassificationsManagerMock()
        {
            classifications = new List<PackNet.Common.Interfaces.DTO.Classifications.Classification>();
            classificationsManager = new Mock<IClassificationsManager>();
            classificationsManager.Setup(manager => manager.Classifications).Returns(delegate { return classifications; });
        }

        private void SetUpPickAreaManagerMock()
        {
            zones = new List<PickZone>();
            var pickArea = new PickArea("1", zones);

            var pickAreas = new List<PickArea>();
            pickAreas.Add(pickArea);

            pickAreaManager = new Mock<IPickZoneManager>();
            pickAreaManager.Setup(manager => manager.PickZones).Returns(delegate { return pickAreas.SelectMany(pa => pa.PickZones); });
        }

        private void SetUpCartonPropertyGroupManagerMock()
        {
            groups = new List<CartonPropertyGroup>();

            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
            cartonPropertyGroupManager
                .Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Normal))
                .Returns(() => groups.Where(g => g.Status == CartonPropertyGroupStatus.Normal));
            cartonPropertyGroupManager
                .Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Stop))
                .Returns(() => groups.Where(g => g.Status == CartonPropertyGroupStatus.Stop));
            cartonPropertyGroupManager
                .Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Surge))
                .Returns(() => groups.Where(g => g.Status == CartonPropertyGroupStatus.Surge));
        }

        private void SetUpMachinesManagerMock()
        {
            var machine = new Mock<IPacksizeCutCreaseMachine>();
            machine.Setup(m => m.Id).Returns(machine1Id);
            //machine.Setup(m => m.IsEquippedWithNormalPriorityPrinter).Returns(true);
            machine.Setup(m => m.Tracks).Returns(new List<Track>{
                new Track{ TrackNumber = 1, LoadedCorrugate = corrugates.First() },
                new Track{ TrackNumber = 2, LoadedCorrugate = corrugates.Last() }
            });
            machinesManager = new Mock<IMachinesManager>();
            machinesManager.Setup(manager => manager.Machines).Returns(new[] { machine.Object });
            //machinesManager.Setup(manager => manager.GetAllAvailableCorrugates()).Returns(corrugates);
            //machinesManager.Setup(manager => manager.GetCorrugatesByProductionGroup(sequenceGroupManager.Object.GetProductionGroupForMachine(1))).Returns(corrugates);
        }

        private void SetUpProductionGroupManagerMock()
        {
            sequenceGroup = new Mock<CommonProductionGroups.ProductionGroup>();
            sequenceGroup.SetupGet(g => g.Alias).Returns("G1");
            //sequenceGroup.Setup(sg => sg.ProductionItems).Returns(new[] { new ProductionItem("1", 1), new ProductionItem("2", 1) });
            //sequenceGroup.SetupGet(sg => sg.Machines).Returns(new List<MachineInformation> { new MachineInformation(1) });

            sequenceGroupManager = new Mock<IProductionGroupManager>();
            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine1Id)).Returns(sequenceGroup.Object);
            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup(It.IsAny<string>())).Returns(new[] { sequenceGroup.Object });
        }

        private void SetUpCorrugates()
        {
            corrugates = new List<Corrugate> { new Corrugate { Alias = "1" }, new Corrugate { Alias = "2" } };
        }
    }
}
