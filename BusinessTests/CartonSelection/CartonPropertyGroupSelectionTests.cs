﻿//using PackNet.Business.PickZone;
//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.CartonSelection
{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using PackNet.Business.Carton.TODO;
//    using PackNet.Business.CartonPropertyGroup;
//    using PackNet.Business.CartonSelection;
//    using PackNet.Business.Classifications;
//    using PackNet.Business.Machines;
//    using PackNet.Business.PackagingDesigns;
//    using PackNet.Business.ProductionGroups;
//    using PackNet.Common.Interfaces.DTO.Carton;
//    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
//    using PackNet.Common.Interfaces.DTO.Corrugates;
//    using PackNet.Common.Interfaces.DTO.Machines;
//    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//    using PackNet.Common.Interfaces.DTO.Settings;
//    using PackNet.Common.Interfaces.Services;
//    using PackNet.Common.Utils;
//    using CommonProductionGroups = PackNet.Common.Interfaces.DTO.ProductionGroups;

//    using Testing.Specificity;

    [TestClass]
    public class CartonPropertyGroupSelectionTests
    {
#if DEBUG        
        [TestMethod]
        public void FindTilingPartnerShouldHandleNullTilingProspects()
        {
            Assert.Fail("Need to revisit with wave");
        }
#endif
//        private Mock<IMachinesManager> machinesManager;
//        private Mock<ICartonRequestManager> requestManager;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IPickZoneManager> pickAreaManager;
//        private Mock<IProductionGroupManager> sequenceGroupManager;
//        private Mock<ICartonRequestManager> cartonRequestManager;
//        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
//        private Mock<ICorrugateService> corrugateCalculationsServiceMock;
//        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup1;
//        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup2;
//        private Mock<CommonProductionGroups.ProductionGroup> sequenceGroup3;
//        private Mock<ILongHeadPositioningValidation> longHeadPositioningValidation;

//        private WaveCartonRequestSelector requestSelector;
//        private List<CartonPropertyGroup> stoppedCpgs;
//        private List<CartonPropertyGroup> surgedCpgs;
//        private List<CartonPropertyGroup> normalCpgs;
//        private Mock<IPackagingDesignManager> designManagerMock;
//        private Guid machine1Id = Guid.NewGuid();
//        private Guid machine2Id = Guid.NewGuid();
//        private Guid machine3Id = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            SetUpClassificationsManagerMock();
//            SetUpPickAreaManagerMock();
//            sequenceGroupManager = new Mock<IProductionGroupManager>();
//            SetUpCartonPropertyGroupManagerMock();
//            SetUpRequestManagerMock();
//            machinesManager = new Mock<IMachinesManager>();
//            longHeadPositioningValidation = new Mock<ILongHeadPositioningValidation>();
//            corrugateCalculationsServiceMock = new Mock<ICorrugateService>();

//            SetupDesignManagerMock();
//            var selectorSettings = new SelectorSettings(2, false, 40, 40, 0);

//            requestSelector = new NonParallelWaveCartonRequestSelector(
//                corrugateCalculationsServiceMock.Object,
//                requestManager.Object,
//                classificationsManager.Object,
//                pickAreaManager.Object,
//                machinesManager.Object,
//                sequenceGroupManager.Object,
//                cartonPropertyGroupManager.Object,
//                designManagerMock.Object,
//                selectorSettings,
//                longHeadPositioningValidation.Object,
//                new WmsConfiguration(), 
//                new Mock<ILogger>(MockBehavior.Loose).Object);
//        }


//        private void SetupDesignManagerMock()
//        {
//            var packagingDesignOne = new PackagingDesign { Name = "1", Id = 1, LengthOnZFold = "L", WidthOnZFold = "W" };
//            designManagerMock = new Mock<IPackagingDesignManager>();
//            designManagerMock.Setup(m => m.GetDesign(It.IsAny<DesignQueryParam>())).Returns(packagingDesignOne);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRunOnMachineTwoDueToCartonPropertyGroup()
//        {
//            SetUpProductionGroupManagerMock();
//            SetUpMachinesManagerMock();
//            SetupCorrugateCalculationService();

//            var machine = machinesManager.Object.Machines.ElementAt(1);
//            var result = requestSelector.GetNextJobForMachine(machine);

//            Specify.That(result.Requests.ElementAt(0).CartonPropertyGroupAlias).Should.BeEqualTo("G2");
//            machinesManager.Verify(m => m.GetCorrugatesByExclusiveness(machine, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>()), Times.Once());
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnNullOrderDueToCartonPropertyGroup()
//        {
//            SetUpProductionGroupManagerMock();
//            SetUpMachinesManagerMock();

//            var machine = machinesManager.Object.Machines.ElementAt(0);
//            var result = requestSelector.GetNextJobForMachine(machine);

//            Specify.That(result).Should.BeNull();
//            machinesManager.Verify(m => m.GetCorrugatesByExclusiveness(machine, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>()), Times.Never());
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnRequest1DueToCorrugateUsageAndCartonPropertyGroupOnOtherMachine()
//        {
//            SetUpProductionGroupManagerMockWithSameCartonPropertyGroupInMultipleProductionGroups();
//            SetUpMachinesManagerMockWithThreeMachines();
//            SetupCorrugateCalculationService();

//            var machine = machinesManager.Object.Machines.ElementAt(0);
//            var result = requestSelector.GetNextJobForMachine(machine);

//            Retry.For(() => result != null, TimeSpan.FromSeconds(5));
            
//            Specify.That(result).Should.Not.BeNull();
//            Specify.That(result.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("1");
//            machinesManager.Verify(m => m.GetCorrugatesByExclusiveness(machine, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>()), Times.Exactly(2));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldReturnRequest2DueToCorrugateUsageAndCartonPropertyGroupOnOtherMachine()
//        {
//            SetUpProductionGroupManagerMockWithSameCartonPropertyGroupInMultipleProductionGroups();
//            SetUpMachinesManagerMockWithThreeMachines();
//            SetupCorrugateCalculationService();

//            var machine = machinesManager.Object.Machines.ElementAt(1);
//            var result = requestSelector.GetNextJobForMachine(machine);

//            Specify.That(result).Should.Not.BeNull();
//            Specify.That(result.Requests.ElementAt(0).SerialNumber).Should.BeEqualTo("2");
//            machinesManager.Verify(m => m.GetCorrugatesByExclusiveness(machine, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>()), Times.Once());
//        }

//        private void SetUpRequestManagerMock()
//        {
//            IList<ICarton> requests = new List<ICarton>();
//            requests.Add(new Carton { SerialNumber = "1", Length = 500, Width = 500, Height = 500, ClassificationNumber = "1", PickZone = "A1", DesignId = 1, CorrugateQuality = 1, IsValid = true, CartonPropertyGroupAlias = "G2" });
//            requests.Add(new Carton { SerialNumber = "2", Length = 850, Width = 850, Height = 850, ClassificationNumber = "1", PickZone = "A1", DesignId = 1, CorrugateQuality = 1, IsValid = true, CartonPropertyGroupAlias = "G1" });

//            requestManager = new Mock<ICartonRequestManager>();
//            requestManager.Setup(manager => manager.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            requestManager.Setup(manager => manager.ValidRequestsInActivePickzones).Returns(requests);
//            requestManager.Setup(manager => manager.CartonRequests).Returns(requests);
//            requestManager.Setup(manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//                It.IsAny<CartonOnCorrugate>(),
//                It.IsAny<IPacksizeCutCreaseMachine>(),
//                It.IsAny<PackNet.Common.Interfaces.DTO.Classifications.Classification>(),
//                It.IsAny<bool>(),
//                It.IsAny<bool>(),
//                It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                It.IsAny<int?>(),
//                It.IsAny<bool>())).Returns<
//                IEnumerable<ICarton>,
//                Corrugate,
//                IPacksizeCutCreaseMachine,
//                PackNet.Common.Interfaces.DTO.Classifications.Classification,
//                bool,
//                bool,
//                Func<DesignQueryParam, IPackagingDesign>,
//                int?,
//                bool>((a, b, c, d, e, f, g, h, i) => new CartonRequestOrder { Requests = a, Corrugate = b, MachineId = c.Id, InternalPrinting = f, PackStation = h});
//        }

//        private void SetUpMachinesManagerMock()
//        {
//            var corrugates = new List<Corrugate>();

//            var machineList = new List<IPacksizeCutCreaseMachine>();

//            var machine1 = new Mock<IPacksizeCutCreaseMachine>();

//            var fanfolds = new List<Corrugate> { new Corrugate { Width = 600, Thickness = 1, Quality = 1 } };
//            corrugates.AddRange(fanfolds);

//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine1.Object, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>())).Returns(fanfolds);

//            machine1.SetupGet(machine => machine.Id).Returns(machine1Id);
//            machine1.Setup(machine => machine.Tracks).Returns( new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = fanfolds.First() }
//            });
//            //machine1.Setup(machine => machine.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine1.Setup(machine => machine.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            //machine1.Setup(machine => machine.CurrentProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            var machine2 = new Mock<IPacksizeCutCreaseMachine>();

//            fanfolds = new List<Corrugate> { new Corrugate { Width = 1200, Thickness = 1, Quality = 1 } };
//            corrugates.AddRange(fanfolds);

//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine2.Object, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>())).Returns(fanfolds);

//            machine2.SetupGet(machine => machine.Id).Returns(machine2Id);
//            machine2.Setup(machine => machine.Tracks).Returns( new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = fanfolds.First() }
//            });
//            //machine2.Setup(machine => machine.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine2.Setup(machine => machine.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            //machine2.Setup(machine => machine.CurrentProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            machineList.Add(machine1.Object);
//            machineList.Add(machine2.Object);

//            machinesManager.Setup(manager => manager.Machines).Returns(machineList);
//            //machinesManager.Setup(manager => manager.GetAllAvailableCorrugates()).Returns(corrugates);
//        }

//        private void SetUpMachinesManagerMockWithThreeMachines()
//        {
//            var corrugates = new List<Corrugate>();

//            var machineList = new List<IPacksizeCutCreaseMachine>();

//            var machine1 = new Mock<IPacksizeCutCreaseMachine>();

//            var fanfolds = new List<Corrugate>
//                {
//                    new Corrugate { Width = 900, Thickness = 1, Quality = 1 },
//                    new Corrugate { Width = 600, Thickness = 1, Quality = 1 }
//                };
//            corrugates.AddRange(fanfolds);

//            machine1.SetupGet(machine => machine.Id).Returns(machine1Id);
//            machine1.Setup(machine => machine.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = fanfolds.First() },
//                new Track{ TrackNumber = 2, LoadedCorrugate = fanfolds.Last() }
//            });
//            //machine1.Setup(machine => machine.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine1.Setup(machine => machine.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            //machine1.Setup(machine => machine.CurrentProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine1.Object, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>())).Returns(fanfolds);

//            var machine2 = new Mock<IPacksizeCutCreaseMachine>();

//            fanfolds = new List<Corrugate> { new Corrugate { Width = 860, Thickness = 1, Quality = 1 } };
//            corrugates.AddRange(fanfolds);

//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine2.Object, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>())).Returns(fanfolds);

//            machine2.SetupGet(machine => machine.Id).Returns(machine2Id);
//            machine2.Setup(machine => machine.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = fanfolds.First() },
//                new Track{ TrackNumber = 2, LoadedCorrugate = fanfolds.Last() }
//            });
//            //machine2.Setup(machine => machine.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine2.Setup(machine => machine.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            //machine2.Setup(machine => machine.CurrentProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            var machine3 = new Mock<IPacksizeCutCreaseMachine>();

//            fanfolds = new List<Corrugate> { new Corrugate { Width = 850, Thickness = 1, Quality = 1 } };
//            corrugates.AddRange(fanfolds);

//            machinesManager.Setup(manager => manager.GetCorrugatesByExclusiveness(machine3.Object, It.IsAny<IEnumerable<CommonProductionGroups.ProductionGroup>>())).Returns(fanfolds);

//            machine3.SetupGet(machine => machine.Id).Returns(machine3Id);
//            machine3.Setup(machine => machine.Tracks).Returns(new List<Track>{
//                new Track{ TrackNumber = 1, LoadedCorrugate = fanfolds.First() },
//                new Track{ TrackNumber = 2, LoadedCorrugate = fanfolds.Last() }
//            }); 
//            //machine3.Setup(machine => machine.IsEquippedWithHighPriorityPrinter).Returns(true);
//            //machine3.Setup(machine => machine.IsEquippedWithNormalPriorityPrinter).Returns(true);
//            //machine3.Setup(machine => machine.CurrentProductionStatus).Returns(MachineProductionStatuses.AutoProductionMode);

//            machineList.Add(machine1.Object);
//            machineList.Add(machine2.Object);
//            machineList.Add(machine3.Object);

//            machinesManager.Setup(manager => manager.Machines).Returns(machineList);
//        }

//        private void SetUpClassificationsManagerMock()
//        {
//            IList<PackNet.Common.Interfaces.DTO.Classifications.Classification> classifications = Common.SetUpClassifications();

//            classificationsManager = new Mock<IClassificationsManager>();
//            classificationsManager.Setup(manager => manager.Classifications).Returns(classifications);
//        }

//        private void SetUpPickAreaManagerMock()
//        {
//            var pickAreas = Common.SetUpPickAreas();
//            pickAreaManager = new Mock<IPickZoneManager>();
//            pickAreaManager.Setup(manager => manager.PickZones).Returns(Common.SetUpPickAreas().SelectMany(p => p.PickZones));
//        }

//        private void SetUpProductionGroupManagerMock()
//        {
//            sequenceGroup1 = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup1.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns(string.Empty);
//            sequenceGroup1.SetupGet(g => g.Alias).Returns("G1");
//            //sequenceGroup1.SetupGet(g => g.ProductionItems).Returns(new List<ProductionItem> { new ProductionItem("G1", 1) });
//            //sequenceGroup1.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(1) });

//            sequenceGroup2 = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup2.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns("G2");
//            sequenceGroup2.SetupGet(g => g.Alias).Returns("G2");
//            //sequenceGroup2.SetupGet(g => g.ProductionItems).Returns(new List<ProductionItem> { new ProductionItem("G2", 2) });
//            //sequenceGroup2.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(2) });

//            sequenceGroup3 = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup3.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns("G3");
//            sequenceGroup3.SetupGet(g => g.Alias).Returns("G3");
//            //sequenceGroup3.SetupGet(g => g.ProductionItems).Returns(new List<ProductionItem> { new ProductionItem("G3", 3) });

//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine1Id)).Returns(sequenceGroup1.Object);
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine2Id)).Returns(sequenceGroup2.Object);

//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup("G1")).Returns(new[] { sequenceGroup1.Object });
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup("G2")).Returns(new[] { sequenceGroup2.Object });
//        }

//        private void SetUpProductionGroupManagerMockWithSameCartonPropertyGroupInMultipleProductionGroups()
//        {
//            sequenceGroup1 = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup1.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(filter => filter.Count == 0))).Returns("G1");
//            //sequenceGroup1.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(filter => filter.Count == 1))).Returns("G2");
//            sequenceGroup1.SetupGet(g => g.Alias).Returns("SG1");
//            //sequenceGroup1.SetupGet(g => g.ProductionItems).Returns(new List<ProductionItem> { new ProductionItem("G1", 1), new ProductionItem("G2", 1) });
//            //sequenceGroup1.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(1) });

//            sequenceGroup2 = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup2.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(filter => filter.Count == 0))).Returns("G1");
//            //sequenceGroup2.Setup(g => g.GetNextItemInMix(It.Is<List<string>>(filter => filter.Count == 1))).Returns("G2");
//            sequenceGroup2.SetupGet(g => g.Alias).Returns("SG2");
//            //sequenceGroup2.SetupGet(g => g.ProductionItems).Returns(new List<ProductionItem> { new ProductionItem("G1", 1), new ProductionItem("G2", 1) });
//            //sequenceGroup2.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(2) });

//            sequenceGroup3 = new Mock<CommonProductionGroups.ProductionGroup>();
//            //sequenceGroup3.Setup(g => g.GetNextItemInMix(It.IsAny<List<string>>())).Returns("G3");
//            sequenceGroup3.SetupGet(g => g.Alias).Returns("SG3");
//            //sequenceGroup3.SetupGet(g => g.ProductionItems).Returns(new List<ProductionItem> { new ProductionItem("G3", 1) });
//            //sequenceGroup3.SetupGet(g => g.Machines).Returns(new List<MachineInformation> { new MachineInformation(3) });

//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine1Id)).Returns(sequenceGroup1.Object);
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine2Id)).Returns(sequenceGroup2.Object);
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupForMachine(machine3Id)).Returns(sequenceGroup3.Object);

//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup("G1")).Returns(new[] { sequenceGroup1.Object, sequenceGroup2.Object });
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup("G2")).Returns(new[] { sequenceGroup1.Object, sequenceGroup2.Object });
//            sequenceGroupManager.Setup(manager => manager.GetProductionGroupWithCartonPropertyGroup("G3")).Returns(new[] { sequenceGroup3.Object });
//        }

//        private void SetUpCartonPropertyGroupManagerMock()
//        {
//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Normal)).Returns(new List<CartonPropertyGroup>());

//            stoppedCpgs = new List<CartonPropertyGroup>();
//            surgedCpgs = new List<CartonPropertyGroup>();
//            normalCpgs = new List<CartonPropertyGroup>();
//            cartonRequestManager = new Mock<ICartonRequestManager>();
//            cartonRequestManager.Setup(cr => cr.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid?>())).Returns(true);
//            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Stop)).Returns(() => stoppedCpgs);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Surge)).Returns(() => surgedCpgs);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupsWithStatus(CartonPropertyGroupStatus.Normal)).Returns(() => normalCpgs);
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("G1")).Returns(new CartonPropertyGroup());
//            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroupByAlias("G2")).Returns(new CartonPropertyGroup());
//        }


//        private void SetupCorrugateCalculationService()
//        {
//            var machines = machinesManager.Object.Machines;

//            corrugateCalculationsServiceMock.Setup(m => m.FitsOnCorrugate(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(), It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>())).Returns(true);

//            corrugateCalculationsServiceMock.Setup(
//                m =>
//                    m.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => new CorrugateCalculation()
//                {
//                    Area = (a.Length == null ? 1 : a.Length.Value) * b.Width,
//                    Length = a.Length,
//                    Width = b.Width
//                });

//            corrugateCalculationsServiceMock.Setup(
//                m =>
//                    m.CalculateAreaOn(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(), It.IsAny<bool>()))
//                .Returns<ICarton, Corrugate, IPacksizeCutCreaseMachine, Func<DesignQueryParam, IPackagingDesign>, bool>
//                ((a, b, c, d, e) => (a.Length == null ? 1 : a.Length.Value) * (a.Width == null ? 1 : a.Width.Value));
//        }
    }
}
