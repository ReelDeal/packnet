﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.34014
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace BusinessTests.CartonSelection
{
    using TechTalk.SpecFlow;

    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [Microsoft.VisualStudio.TestTools.UnitTesting.TestClassAttribute()]
    public partial class ManualOrdersQueueJobselectionFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "CartonRequestFirstInFirstOut.feature"
#line hidden
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassInitializeAttribute()]
        public static void FeatureSetup(Microsoft.VisualStudio.TestTools.UnitTesting.TestContext testContext)
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "Manual Orders Queue jobselection", "In order to get manual added order produced in an effiecent way\r\nAs an operator\r\n" +
                    "I need the machines to produce according to a prioritized list", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.ClassCleanupAttribute()]
        public static void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestInitializeAttribute()]
        public virtual void TestInitialize()
        {
            if (((TechTalk.SpecFlow.FeatureContext.Current != null) 
                        && (TechTalk.SpecFlow.FeatureContext.Current.FeatureInfo.Title != "Manual Orders Queue jobselection")))
            {
                FeatureSetup(null);
            }
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCleanupAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should not produce order that are assigned to another machine")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldNotProduceOrderThatAreAssignedToAnotherMachine()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not produce order that are assigned to another machine", new string[] {
                        "ManualOrderQueue"});
#line 21
this.ScenarioSetup(scenarioInfo);
#line 22
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 23
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 24
 testRunner.And("order1 optimal on corrugate c1 assigned to another machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 26
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 27
 testRunner.Then("no order is produced", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should not produce orders that are assigned to the specific machine but with an u" +
            "nloaded corrugate")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldNotProduceOrdersThatAreAssignedToTheSpecificMachineButWithAnUnloadedCorrugate()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not produce orders that are assigned to the specific machine but with an u" +
                    "nloaded corrugate", new string[] {
                        "ManualOrderQueue"});
#line 30
this.ScenarioSetup(scenarioInfo);
#line 31
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 32
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 33
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c2 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 34
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 35
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 36
 testRunner.Then("no order is produced", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should not produce orders that are assigned to the specific machine but already i" +
            "n queue")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldNotProduceOrdersThatAreAssignedToTheSpecificMachineButAlreadyInQueue()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not produce orders that are assigned to the specific machine but already i" +
                    "n queue", new string[] {
                        "ManualOrderQueue"});
#line 39
this.ScenarioSetup(scenarioInfo);
#line 40
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 41
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 42
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 43
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 44
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 45
 testRunner.Then("order1 is produced without tiles", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 46
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 47
 testRunner.Then("no order is produced", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should tile within orders")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldTileWithinOrders()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should tile within orders", new string[] {
                        "ManualOrderQueue"});
#line 50
this.ScenarioSetup(scenarioInfo);
#line 51
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 52
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 53
 testRunner.And("order1 with a quantity 2 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 54
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 55
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 56
 testRunner.Then("order1 is produced with tiles", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should not tile between orders")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldNotTileBetweenOrders()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not tile between orders", new string[] {
                        "ManualOrderQueue"});
#line 59
this.ScenarioSetup(scenarioInfo);
#line 60
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 61
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 62
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 63
 testRunner.And("order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 64
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 65
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 66
 testRunner.Then("order1 is produced without tiles", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should pause between orders when production group is set to pause")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldPauseBetweenOrdersWhenProductionGroupIsSetToPause()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should pause between orders when production group is set to pause", new string[] {
                        "ManualOrderQueue"});
#line 69
this.ScenarioSetup(scenarioInfo);
#line 70
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 71
 testRunner.And("a production group setup with pause between orders true", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 72
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 73
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 74
 testRunner.And("order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 75
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 76
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 77
 testRunner.Then("order1 is produced without tiles", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 78
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 79
 testRunner.Then("no order is produced", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should not pause between orders when production group is set to not pause")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldNotPauseBetweenOrdersWhenProductionGroupIsSetToNotPause()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not pause between orders when production group is set to not pause", new string[] {
                        "ManualOrderQueue"});
#line 82
this.ScenarioSetup(scenarioInfo);
#line 83
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 84
 testRunner.And("a production group setup with pause between orders false", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 85
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 86
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 87
 testRunner.And("order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 88
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 89
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 90
 testRunner.Then("order1 is produced without tiles", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 91
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 92
 testRunner.Then("order2 is produced without tiles", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should produce second order that are assigned to the specific machine when the fi" +
            "rst is picked up by another machine")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldProduceSecondOrderThatAreAssignedToTheSpecificMachineWhenTheFirstIsPickedUpByAnotherMachine()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should produce second order that are assigned to the specific machine when the fi" +
                    "rst is picked up by another machine", new string[] {
                        "ManualOrderQueue"});
#line 95
this.ScenarioSetup(scenarioInfo);
#line 96
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 97
 testRunner.And("a production group setup with pause between orders true", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 98
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 99
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 100
 testRunner.And("order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 101
 testRunner.And("order1 is picked up by another machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 102
 testRunner.And("order2 is not picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 103
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 104
 testRunner.Then("order2 is produced", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should not produce orders that are not optimal for my machine")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldNotProduceOrdersThatAreNotOptimalForMyMachine()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should not produce orders that are not optimal for my machine", new string[] {
                        "ManualOrderQueue"});
#line 107
this.ScenarioSetup(scenarioInfo);
#line 108
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 109
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 110
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c1 not optimal on my machine " +
                    "assigned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 111
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 112
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 113
 testRunner.Then("no order is produced", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestMethodAttribute()]
        [Microsoft.VisualStudio.TestTools.UnitTesting.DescriptionAttribute("Should only produce orders that are optimal for my machine")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestPropertyAttribute("FeatureTitle", "Manual Orders Queue jobselection")]
        [Microsoft.VisualStudio.TestTools.UnitTesting.TestCategoryAttribute("ManualOrderQueue")]
        public virtual void ShouldOnlyProduceOrdersThatAreOptimalForMyMachine()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Should only produce orders that are optimal for my machine", new string[] {
                        "ManualOrderQueue"});
#line 116
this.ScenarioSetup(scenarioInfo);
#line 117
 testRunner.Given("a corrugate c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 118
 testRunner.And("a machine loaded with c1", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 119
 testRunner.And("order1 with a quantity 1 optimal tiled on corrugate c1 not optimal on my machine " +
                    "assigned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 120
 testRunner.And("order2 with a quantity 1 optimal tiled on corrugate c1 optimal on my machine assi" +
                    "gned to my machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 121
 testRunner.And("no order is picked up by any other machine", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 122
 testRunner.When("next job is requested", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 123
 testRunner.Then("order2 is produced", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
