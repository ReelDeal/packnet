﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Logging;
using PackNet.Data.Templates;

using Testing.Specificity;

namespace BusinessTests.Templates
{
    [TestClass]
    public class TemplatesTest
    {
        [TestMethod]
        [DeploymentItem("Data\\Labels\\DoubleBarcode.prn", "Data\\Labels\\DoubleBarcode.prn")]
        [DeploymentItem("Data\\Labels\\Template.prn", "Data\\Labels\\Template.prn")]
        public void ShouldGetInitialDataFromFileSystem()
        {
            var mockLogger = new Mock<ILogger>();
            var mockRepository = new Mock<ITemplateRepository>();

            var templateManager = new PackNet.Business.Templates.Templates(mockRepository.Object, mockLogger.Object);

            var filePaths = Directory.GetFiles(Path.Combine(Environment.CurrentDirectory, "Data\\Labels"), "*.prn");
            var data = templateManager.GetInitialData();

            Specify.That(filePaths.Count()).Should.BeEqualTo(data.Count());

            foreach (var filePath in filePaths)
            {
                var template = new Template
                {
                    Name = Path.GetFileNameWithoutExtension(filePath),
                    Content = File.ReadAllText(filePath)
                };

                Specify.That(data.FirstOrDefault(t => t.Name == template.Name && t.Content == template.Content)).Should.Not.BeNull();
            }
        }

        [TestMethod]
        [DeploymentItem("Data\\Labels\\DoubleBarcode.prn", "Data\\Labels\\DoubleBarcode.prn")]
        [DeploymentItem("Data\\Labels\\Template.prn", "Data\\Labels\\Template.prn")]
        public void ShouldGetAllFromRepository()
        {
            var expectedResult = new List<Template> { new Template() };
            var mockLogger = new Mock<ILogger>();
            var mockRepository = new Mock<ITemplateRepository>();

            mockRepository.Setup(c => c.All()).Returns(() => expectedResult);

            var templateManager = new PackNet.Business.Templates.Templates(mockRepository.Object, mockLogger.Object);

            var data = templateManager.GetAll();
            mockRepository.Verify();
            Specify.That(data.First()).Should.BeEqualTo(expectedResult.First());
        }

        [TestMethod]
        [DeploymentItem("Data\\Labels\\DoubleBarcode.prn", "Data\\Labels\\DoubleBarcode.prn")]
        [DeploymentItem("Data\\Labels\\Template.prn", "Data\\Labels\\Template.prn")]
        public void ShouldGetAllFromFileSystemWhenRepositoryIsEmptyAndSaveInRepository()
        {
            var mockLogger = new Mock<ILogger>();
            var mockRepository = new Mock<ITemplateRepository>();
            mockRepository.Setup(c => c.All()).Returns(() => new List<Template>());
            mockRepository.Setup(c => c.Create(It.IsAny<IEnumerable<Template>>()));
            var templateManager = new PackNet.Business.Templates.Templates(mockRepository.Object, mockLogger.Object);
            var data = templateManager.GetInitialData();


            var result = templateManager.GetAll();

            foreach (var template in result)
            {
                Specify.That(data.FirstOrDefault(t => t.Name == template.Name && t.Content == template.Content)).Should.Not.BeNull();
            }
        }

    }
}
