﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Utils;

namespace BusinessTests.IqFusionJobCreator
{
    using System.Linq;

    using PackNet.Common.Interfaces;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;

    public static class MachineInstructionCreatorTestHelper
    {
        private static readonly Guid machine1Id = Guid.NewGuid();

        public static Guid GetMachineId()
        {
            return machine1Id;
        }

        public static void SetCartonRequestCorrugate(IPacksizeCarton job, int corrugateWidth = 700, double corrugateThickness = 3)
        {
            job.CartonOnCorrugate.Corrugate = new Corrugate
            {
                Width = corrugateWidth,
                Thickness = corrugateThickness
            };
        }

        public static void SetRequestDimensions(IPacksizeCarton order, int length = 700, int width = 300, int height = 300)
        {
            order.Length = length;
            order.Width = width;
            order.Height = height;
        }

        public static void SetRequestDimensions(IPacksizeCarton order, IDictionary<string, double> xValues, int length = 700, int width = 300, int height = 300)
        {
            order.Length = length;
            order.Width = width;
            order.Height = height;

            xValues.ForEach(kv => order.XValues[kv.Key] = kv.Value);
        }

        public static IPacksizeCarton GetCartonRequest()
        {
            return new Carton
            {
                TrackNumber = 2,
                CartonOnCorrugate = new CartonOnCorrugate { Corrugate = new Corrugate() }
                //SerialNo = serialNumber,
                //Requests = new[] { new Carton() }
            };
        }

        public static FusionMachine GetMachine(string machineFile = "machineExample.xml")
        {
            var settings = FusionPhysicalMachineSettings.CreateFromFile(Networking.FindIpEndPoint("127.0.0.1", 999, true), machineFile);
            var longHeads = settings.LongHeadParameters.LongHeads;

            for (int i = 0; i < longHeads.Count(); i++)
            {
                longHeads.ElementAt(i).Position = 300 * (i + 1);
            }

            return new FusionMachine
            {
                PhysicalMachineSettings = settings,
                Tracks = new ConcurrentList<Track>()
            {
                new Track()
                {
                    TrackNumber = 1,
                    TrackOffset = 0
                },
                new Track()
                {
                    TrackNumber = 2,
                    TrackOffset = 350
                }                
            }};
        }
    }
}
