﻿using Moq;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Logging;

namespace BusinessTests.IqFusionJobCreator
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Business.CodeGenerator.InstructionList;
    using PackNet.Business.IqFusionJobCreator;
    using PackNet.Common.Interfaces;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Orders;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using System.Collections.Generic;
    using System.Linq;
    using Testing.Specificity;

    [TestClass]
    [DeploymentItem("TestData\\machineExample.xml")]
    [DeploymentItem("TestData\\PhysicalMachineSettings\\machineTheoretical_inches.xml")]
    [DeploymentItem("TestData\\0201-0000.ezd")]
    public class MachineInstructionCreatorTests
    {
        private FusionMachineInstructionCreator machineInstructionCreator;
        private IPacksizeCarton cartonRequest;
        private FusionMachine machine;
        private readonly Mock<ILogger> logger = new Mock<ILogger>();
        
        [TestInitialize]
        public void TestInitialize()
        {
            machine = MachineInstructionCreatorTestHelper.GetMachine();
            machineInstructionCreator = new FusionMachineInstructionCreator(logger.Object);            
            cartonRequest = MachineInstructionCreatorTestHelper.GetCartonRequest();
            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(cartonRequest);
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void CorrugateShouldMatchSetup()
        {
            Specify.That(cartonRequest.CartonOnCorrugate.Corrugate.Width).Should.BeLogicallyEqualTo(700);
            Specify.That(cartonRequest.CartonOnCorrugate.Corrugate.Thickness).Should.BeLogicallyEqualTo(3);
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void DimensionsShouldMatchSetup()
        {
            Specify.That(cartonRequest.Length).Should.BeLogicallyEqualTo(700);
            Specify.That(cartonRequest.Width).Should.BeLogicallyEqualTo(300);
            Specify.That(cartonRequest.Height).Should.BeLogicallyEqualTo(300);
            //Assert.Fail();
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void MachineShouldMatchSetup()
        {
            Specify.That((machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).TrackParameters.Tracks.Count()).Should.BeEqualTo(2);
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void JobPropertiesMatchesSetup()
        {
            Specify.That(cartonRequest.TrackNumber).Should.BeEqualTo(2);
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void ShouldGenerateInstructionListFromCartonRequest()
        {
            var instructionList = machineInstructionCreator.GetInstructionListForJob(cartonRequest, GetDesignFromFile(cartonRequest), machine.Tracks.Single(t => t.TrackNumber == cartonRequest.TrackNumber), machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);
            Specify.That(instructionList).Should.Not.BeNull();
        }
        
        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void ShouldGenerateFullInstructionListFromCartonRequest()
        {
            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 750);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(cartonRequest, 100, 200);
            var instructionList = machineInstructionCreator.GetInstructionListForJob(cartonRequest, GetDesignFromFile(cartonRequest), machine.Tracks.Single(t => t.TrackNumber == cartonRequest.TrackNumber), machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);
            Specify.That(instructionList).Should.Not.BeNull();
            Specify.That(instructionList.FirstOrDefault()).Should.BeInstanceOfType(typeof(InstructionMetaDataItem));
            Specify.That(instructionList.FirstOrDefault(item => item.GetType() == typeof(InstructionLongHeadPositionItem))).Should.Not.BeNull();
            Specify.That(instructionList.FirstOrDefault(item => item.GetType() == typeof(InstructionCrossHeadMovementItem))).Should.Not.BeNull();
            Specify.That(instructionList.FirstOrDefault(item => item.GetType() == typeof(InstructionFeedRollerItem))).Should.Not.BeNull();
            Specify.That(instructionList.LastOrDefault()).Should.BeInstanceOfType(typeof(InstructionEndMarkerItem));
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void ShouldReturnInvalidForNonProducibleOrder()
        {
            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 750);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(cartonRequest, 500, 50, 150);
            bool isValid = machineInstructionCreator.IsLongHeadPositioningValidFor(cartonRequest, GetDesignFromFile(cartonRequest), machine.Tracks.Single(t => t.TrackNumber == cartonRequest.TrackNumber), machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);

            Specify.That(isValid).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnInvalidForCarton_ThatCannotBeProduceOnBothTrackOfAFusion_OnCanProducePackagingFor()
        {
            machine = MachineInstructionCreatorTestHelper.GetMachine("machineTheoretical_inches.xml");
            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 29, 0.11);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(cartonRequest, 3, 4, 2);
            var producePackagingParameters = new CanProducePackagingForParameters
            {
                Corrugate = cartonRequest.CartonOnCorrugate.Corrugate,
                PackagingDesign = GetDesignFromFile(cartonRequest)
            };
            var maxLhPos = (machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters.MaximumPosition;
            bool isValid = machineInstructionCreator.CanProducePackagingFor(producePackagingParameters, machine.PhysicalMachineSettings as FusionPhysicalMachineSettings, 2, maxLhPos);

            Specify.That(isValid).Should.BeFalse();
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void ShouldReturnInvalidForNonProducibleOrderOnCanProducePackagingFor()
        {
            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 750);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(cartonRequest, 500, 50, 150);
            var producePackagingParameters = new CanProducePackagingForParameters               
            {
                Corrugate = cartonRequest.CartonOnCorrugate.Corrugate,
                PackagingDesign = GetDesignFromFile(cartonRequest)
            };

            var maxLhPos = (machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters.MaximumPosition;
            bool isValid = machineInstructionCreator.CanProducePackagingFor(producePackagingParameters, machine.PhysicalMachineSettings as FusionPhysicalMachineSettings, 2, maxLhPos);

            Specify.That(isValid).Should.BeFalse();
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void ShouldReturnValidForProducibleOrder()
        {
            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 750);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(cartonRequest, 100, 200);
            var isValid = machineInstructionCreator.IsLongHeadPositioningValidFor(cartonRequest, GetDesignFromFile(cartonRequest), machine.Tracks.Single(t => t.TrackNumber == cartonRequest.TrackNumber), machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);

            Specify.That(isValid).Should.BeTrue();
        }

        [TestMethod]
        [Description("Integration with DomainObjects Container")]
        [TestCategory("Integration")]
        public void ShouldReturnValidForProducibleOrderOnCanProducePackagingFor()
        {
            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 750);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(cartonRequest, 100, 200);

            var canProducePackagingForParameters = new CanProducePackagingForParameters
            {
                Corrugate = cartonRequest.CartonOnCorrugate.Corrugate,
                PackagingDesign = GetDesignFromFile(cartonRequest)
            };

            var maxLhPos = (machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters.MaximumPosition;
            var isValid = machineInstructionCreator.CanProducePackagingFor(canProducePackagingForParameters, machine.PhysicalMachineSettings as FusionPhysicalMachineSettings, 2, maxLhPos);

            Specify.That(isValid).Should.BeTrue();
        }

        [TestMethod]        
        [TestCategory("Integration")]
        public void ShouldReturnValidForProducibleOrderOnCanProducePackagingForWithXValueDesign()
        {
            var xValues = new Dictionary<string, double> { { "X1", 5.0 }, {"X2", 6.0 } };

            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 27);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(order: cartonRequest, xValues: xValues,
                length: 10, width: 11, height: 12);

            var canProducePackagingForParameters = new CanProducePackagingForParameters
            {
                Corrugate = cartonRequest.CartonOnCorrugate.Corrugate,
                PackagingDesign = ADesignWithX1WidthAndX2Length(cartonRequest)
            };

            var maxLhPos = (machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters.MaximumPosition;
            var isValid = machineInstructionCreator.CanProducePackagingFor(canProducePackagingForParameters, machine.PhysicalMachineSettings as FusionPhysicalMachineSettings, 2, maxLhPos);

            Specify.That(isValid).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldReturnValidForProducibleOrderOnCanProducePackagingForWithDesignWithXValuesInEveryFormula()
        {
            var xValues = new Dictionary<string, double> { { "X1", 5.0 }, { "X2", 6.0 }, { "X3", 7.0 }, { "X4", 8.0 }, { "X5", 6.0 }, { "X6", 6.0 }, { "X7", 6.0 } };

            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 27);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(order: cartonRequest, xValues: xValues,
                length: 10, width: 11, height: 12);

            var canProducePackagingForParameters = new CanProducePackagingForParameters
            {
                Corrugate = cartonRequest.CartonOnCorrugate.Corrugate,
                PackagingDesign = ADesignWithLinesAllOverThePlace(cartonRequest)
            };

            var maxLhPos = (machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters.MaximumPosition;
            var isValid = machineInstructionCreator.CanProducePackagingFor(canProducePackagingForParameters, machine.PhysicalMachineSettings as FusionPhysicalMachineSettings, 2, maxLhPos);

            Specify.That(isValid).Should.BeTrue();
        }

        [TestMethod]        
        [TestCategory("Integration")]
        public void ShouldBeABleToValidateLongheadPositionsUsingXValues()
        {
            var xValues = new Dictionary<string, double> { { "X1", 2.0 }, { "X2", 4.0 }, { "X3", 8.0 }, { "X4", 10.0 }, { "X5", 12.0 } };

            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW",
                LengthOnZFold = "L"
            };

            xValues.ForEach(kv => packagingDesign.Lines.Add(new Line
            {
                StartX = kv.Key,
                StartY = "0",
                Length = "L",
                Direction = LineDirection.vertical,
                Type = LineType.crease
            }));            

            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 30);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(order: cartonRequest, xValues: xValues,
                length: 10, width: 11, height: 12);

            bool isValid = machineInstructionCreator.IsLongHeadPositioningValidFor(cartonRequest,
                packagingDesign.AsPhysicalDesign(GetDesignParametes(cartonRequest)), machine.Tracks.Single(t => t.TrackNumber == cartonRequest.TrackNumber), machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);

            Specify.That(isValid).Should.BeFalse();   
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void LongheadValidationWithXValues()
        {
            var xValues = new Dictionary<string, double> { { "X1", 30 }, { "X2", 100 }, { "X3", 200 }, { "X4", 300 } };

            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW",
                LengthOnZFold = "L"
            };

            xValues.ForEach(kv => packagingDesign.Lines.Add(new Line
            {
                StartX = kv.Key,
                StartY = "0",
                Length = "L",
                Direction = LineDirection.vertical,
                Type = LineType.crease
            }));  

            MachineInstructionCreatorTestHelper.SetCartonRequestCorrugate(cartonRequest, 700);
            MachineInstructionCreatorTestHelper.SetRequestDimensions(order: cartonRequest, xValues: xValues);

            var isValid = machineInstructionCreator.IsLongHeadPositioningValidFor(cartonRequest,
                packagingDesign.AsPhysicalDesign(GetDesignParametes(cartonRequest)), machine.Tracks.Single(t => t.TrackNumber == cartonRequest.TrackNumber), machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);

            Specify.That(isValid).Should.BeTrue();
        }

        private static PhysicalDesign GetDesignFromFile(IPacksizeCarton carton)
        {
            return PackagingFile.Deserialize("0201-0000.ezd").Designs.First().AsPhysicalDesign(GetDesignParametes(carton));
        }

        private static DesignFormulaParameters GetDesignParametes(IPacksizeCarton carton)
        {
            var df = new DesignFormulaParameters
            {
                CartonHeight = carton.Height,
                CartonLength = carton.Length,
                CartonWidth = carton.Width,
                ZfoldThickness = carton.CartonOnCorrugate.Corrugate.Thickness,
                ZfoldWidth = carton.CartonOnCorrugate.Corrugate.Width
            };

            carton.XValues.ForEach(kv => df.XValues.Add(kv.Key, kv.Value));

            return df;
        }

        private PhysicalDesign ADesignWithLinesAllOverThePlace(IPacksizeCarton carton)
        {
            var packagingDesign = new PackagingDesign
            {
                WidthOnZFold = "FW+X5+X6+X7",
                LengthOnZFold = "L+X1+X2+X3+X4"
            };

            var line = new Line
            {
                StartX = "X1+X2",
                StartY = "X3+L",
                Length = "L+X4",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            };

            packagingDesign.Lines.Add(line);

            line = new Line
            {
                StartX = "X5-X6",
                StartY = "X7*L",
                Length = "FW+X7",
                Direction = LineDirection.horizontal,
                Type = LineType.cut
            };

            packagingDesign.Lines.Add(line);

            return packagingDesign.AsPhysicalDesign(GetDesignParametes(carton));
        }

        private PhysicalDesign ADesignWithX1WidthAndX2Length(IPacksizeCarton carton)
        {
            var packagingDesign = new PackagingDesign
            {
                Id = 666,
                WidthOnZFold = "X1",
                LengthOnZFold = "X2"
            };

            var line = new Line
            {
                StartX = "X1",
                StartY = "0",
                Length = "L",
                Direction = LineDirection.vertical,
                Type = LineType.cut
            };

            packagingDesign.Lines.Add(line);

            return packagingDesign.AsPhysicalDesign(GetDesignParametes(carton));
        }
    }
}
