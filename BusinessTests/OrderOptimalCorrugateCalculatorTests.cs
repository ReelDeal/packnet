﻿using System.Diagnostics;

using PackNet.Business.Corrugates;
using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;

namespace BusinessTests
{
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;
    using PackNet.Common.Interfaces.Services;

    using StoryQ;

    using Testing.Specificity;

    [TestClass]
    public class OrderOptimalCorrugateCalculatorTests
    {
        private Guid c1Id = Guid.NewGuid();
        private Guid c2Id = Guid.NewGuid();
        private Guid c10Id = Guid.NewGuid();

        private const string AnyPacksizeCustomer = "Any Packsize Customer";
        private List<Corrugate> corrugates;
        private List<IPacksizeCutCreaseMachine> machines;
        private List<ProductionGroup> productionGroup;

        private List<MachineGroup> machineGroups; 
        private IOptimalCorrugateCalculator optimalCorrugateCalculator;
        private Mock<IPackagingDesignManager> designManager;
        private Mock<IAggregateMachineService> machinesServiceMock;
        private Mock<IMachineGroupService> machineGroupService;
        private Mock<ILogger> loggerMock;
        private Mock<IUserNotificationService> userNotificationServiceMock;

        private Mock<IServiceLocator> serviceLocatorMock;

        private ICarton Carton { get; set; }
        private CartonOnCorrugate OptimalCartonOnCorrugate { get; set; }
        private PhysicalDesign packagingDesign;
        private PhysicalDesign packagingDesignRotated;
        private PhysicalDesign packagingDesignThatIsNotRotatable;
        private Guid machine1Id = Guid.NewGuid();
        private Guid machine2Id = Guid.NewGuid();

        private IOptimalCorrugateCalculator OptimalCorrugateCalculator
        {
            get { return optimalCorrugateCalculator; }
        }



        [TestInitialize]
        public void TestInitialize()
        {
            packagingDesign = new PhysicalDesign
            {
                Length = 100,
                Width = 200,
            };
            packagingDesign.Add(new PhysicalLine(new PhysicalCoordinate(5, 0), new PhysicalCoordinate(5, 10), LineType.cut));

            packagingDesignRotated = new PhysicalDesign
            {
                Length = 200,
                Width = 100
            };
            packagingDesignRotated.Add(new PhysicalLine(new PhysicalCoordinate(0, 5), new PhysicalCoordinate(10, 5), LineType.cut));

            packagingDesignThatIsNotRotatable = new PhysicalDesign
            {
                Length = 100,
                Width = 200
            };
            packagingDesignThatIsNotRotatable.Add(new PhysicalLine(new PhysicalCoordinate(5, 0), new PhysicalCoordinate(5, 10), LineType.cut));

            corrugates = new List<Corrugate>();
            machines = new List<IPacksizeCutCreaseMachine>();
            productionGroup = new List<ProductionGroup>();
            machineGroups = new List<MachineGroup>();

            machinesServiceMock = new Mock<IAggregateMachineService>();
            machineGroupService = new Mock<IMachineGroupService>();
            machinesServiceMock.Setup(m => m.Machines).Returns(machines);
            userNotificationServiceMock = new Mock<IUserNotificationService>();
            loggerMock = new Mock<ILogger>();

            serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(locator => locator.Locate<IAggregateMachineService>()).Returns(machinesServiceMock.Object);
            serviceLocatorMock.Setup(locator => locator.Locate<IMachineGroupService>()).Returns(machineGroupService.Object);
            serviceLocatorMock.Setup(locator => locator.Locate<IUserNotificationService>()).Returns(userNotificationServiceMock.Object);
            serviceLocatorMock.Setup(locator => locator.Locate<ILogger>()).Returns(loggerMock.Object);

            designManager = new Mock<IPackagingDesignManager>();
            designManager.Setup(dm => dm.GetPhyscialDesignForCarton(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<OrientationEnum>(), It.IsAny<int>()))
                .Returns<ICarton, Corrugate, bool>((c, corr, rotated) => rotated ? packagingDesignRotated : packagingDesign);
            designManager.Setup(m => m.HasDesignWithId(It.IsAny<int>())).Returns(true);

            optimalCorrugateCalculator = new OptimalCorrugateCalculator(designManager.Object,
                serviceLocatorMock.Object, machineGroupService.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 12282")]
        public void ShouldSendUserNotificationAndLogWhenTheDesignForACartonIsMissing()
        {
            var carton = new Carton() { Length = 10, Width = 10, Height = 10, DesignId = 201};

            var machineGuid = Guid.NewGuid();
            var mgId = Guid.NewGuid();

            machines.Add(new FusionMachine() { Id = machineGuid });
            machinesServiceMock.Setup(m => m.CanProduce(machineGuid, carton)).Returns(true);
            machineGroupService.Setup(m => m.FindByMachineId(machineGuid)).Returns(new MachineGroup() { Id = mgId });

            designManager.Setup(m => m.HasDesignWithId(carton.DesignId)).Returns(false);

            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(new List<Corrugate>(), machines, carton, 1);
            Specify.That(optimalCorrugate).Should.BeNull();

            loggerMock.Verify(m => m.Log(LogLevel.Warning, It.Is<string>(s => s.Contains("could not be created since a design with ID: 201"))));
            userNotificationServiceMock.Verify(m => m.SendNotificationToMachineGroup(NotificationSeverity.Error, It.Is<string>(s => s.Contains("could not be created since a design with ID: 201")), string.Empty, mgId));
        }

        [TestMethod]
        [TestCategory("Unit")]
        [Conditional("Debug")]
        public void ShouldReturnBestCorrugateWhenRotationIsConsidered()
        {
            machines.Add(new FusionMachine());
            var corrugate1 = new Corrugate { Id = c1Id, Quality = 1, Thickness = .16, Width = 34 };
            var corrugate2 = new Corrugate { Id = c2Id, Quality = 1, Thickness = .16, Width = 25 };
            var corrugateList = new List<Corrugate> { corrugate1, corrugate2 };

            var xValues = new Dictionary<string, double>();
            for (var i = 1; i < 21; i++)
                xValues.Add("X" + i, 0);
            var carton = new Carton() { Length = 10, Width = 10, Height = 10, DesignId = 201, XValues = xValues };

            var machineGuid = Guid.NewGuid();
            machines.Add(new FusionMachine() { Id = machineGuid });
            machinesServiceMock.Setup(m => m.CanProduce(machineGuid, carton)).Returns(true);

            // Should be able to fit 3 tiles rotated on corrugate 1 with 4 waste
            // Should be able to fit 1 tile with no rotation on corrugate 2 with 5 waste
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate1, 3, false))
                .Returns(GetCorrugateCalculation(10, 20));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate1, 3, true))
                .Returns(GetCorrugateCalculation(20, 10));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate2, 3, false))
                .Returns(GetCorrugateCalculation(10, 30));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate2, 3, true))
                .Returns(GetCorrugateCalculation(30, 10));


            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new FusionMachine { Id = m.Id }), carton, 3);
            Specify.That(optimalCorrugate.Corrugate.Equals(corrugate1)).Should.BeTrue();
            Specify.That(optimalCorrugate.TileCount).Should.BeEqualTo(3);
            Specify.That(optimalCorrugate.Rotated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnCartonOnCorrugateWithCorrectRotation()
        {
            var corrugate1 = new Corrugate { Id = c1Id, Quality = 1, Thickness = .16, Width = 34 };
            var corrugateList = new List<Corrugate> { corrugate1 };

            var carton = new Carton() { Length = 10, Width = 10, Height = 10, DesignId = 201, Rotation = OrientationEnum.Degree0};

            var machineGuid = Guid.NewGuid();
            machines.Add(new EmMachine() { Id = machineGuid });
            machinesServiceMock.Setup(m => m.CanProduce(machineGuid, carton)).Returns(true);

            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate1, 1, false))
                .Returns(GetCorrugateCalculation(10, 10));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate1, 1, true))
                .Returns(GetCorrugateCalculation(10, 10));

            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree0);

            carton.Rotation = OrientationEnum.Degree0_flip;
            optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree0_flip);

            carton.Rotation = OrientationEnum.Degree90;
            optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree90);

            carton.Rotation = OrientationEnum.Degree90_flip;
            optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree90_flip);

            carton.Rotation = OrientationEnum.Degree180;
            optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree180);

            carton.Rotation = OrientationEnum.Degree180_flip;
            optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree180_flip);

            carton.Rotation = OrientationEnum.Degree270;
            optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree270);

            carton.Rotation = OrientationEnum.Degree270_flip;
            optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new EmMachine { Id = m.Id }), carton, 1);
            Specify.That(optimalCorrugate.Rotation).Should.BeLogicallyEqualTo(OrientationEnum.Degree270_flip);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnBestCorrugateWhenTilingIsOnConsidered()
        {
            var corrugate1 = new Corrugate { Id = c2Id, Quality = 1, Thickness = .16, Width = 34 };  // 3 tiles will fit
            var corrugate2 = new Corrugate { Id = c1Id, Quality = 1, Thickness = .16, Width = 25 };  // 2 tiles will fit
            var corrugate3 = new Corrugate { Id = c10Id, Quality = 1, Thickness = .16, Width = 19 }; // 1 tile will fit
            var corrugate4 = new Corrugate { Id = c10Id, Quality = 1, Thickness = .16, Width = 64 }; // 3 tiles with more waste because max tiles is set to 3
            var corrugateList = new List<Corrugate> { corrugate2, corrugate3, corrugate1, corrugate4 };

            var xValues = new Dictionary<string, double>();
            for (var i = 1; i < 21; i++)
                xValues.Add("X" + i, 0);
            var carton = new Carton() { Length = 10, Width = 10, Height = 1, DesignId = 201, XValues = xValues };

            var machineGuid = Guid.NewGuid();
            machines.Add(new FusionMachine() { Id = machineGuid });
            machinesServiceMock.Setup(m => m.CanProduce(machineGuid, carton)).Returns(true);
            /*
             View of test
             Carton L on corrugate = 10" Carton Width on corrugate is 11"
             * Corrugate 19"        Corrugate 26"       Corrugate 34"               Corrugate 64"
             * |   +      |         |     +    |        |  +     +     +     |      |  +     +     +     +     +     +     |
             * |   +      |         |     +    |        |  +     +     +     |      |  +     +     +     +     +     +     |
             * |   +      |         |     +    |        |  +     +     +     |      |  +     +     +     +     +     +     |
             * |   +      |         |     +    |        |  +     +     +     |      |  +     +     +     +     +     +     |
             * |   +      |         |     +    |        |  +     +     +     |      |  +     +     +     +     +     +     |
             * |   +      |         |     +    |        |  +     +     +     |      |  +     +     +     +     +     +     |
             *  carton A=110        carton A=110        carton A=110                carton A=110     
             *  Corr Area Used=209  Corr Area Used=286  Corr Area Used=374          Corr Area Used=704 
             */

            //Carton is producible on all corrugates and should have the same calculation on each as far as the individual carton is concerned.
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<int>(), false))
                .Returns(this.GetCorrugateCalculation(10, 11));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), It.IsAny<int>(), true))
                .Returns(this.GetCorrugateCalculation(11, 10));

            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new FusionMachine { Id = m.Id }), carton, 3);
            Specify.That(optimalCorrugate.Corrugate.Equals(corrugate1)).Should.BeTrue();
            Specify.That(optimalCorrugate.TileCount).Should.BeEqualTo(3);
            Specify.That(optimalCorrugate.Rotated).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnCorrectTileCountBasedOnMaxTilesFromConfig()
        {
            var corrugate1 = new Corrugate { Id = c2Id, Quality = 1, Thickness = .16, Width = 34 };  // 3 tiles will fit
            var corrugateList = new List<Corrugate> { corrugate1 };

            var xValues = new Dictionary<string, double>();
            for (var i = 1; i < 21; i++)
                xValues.Add("X" + i, 0);
            var carton = new Carton() { Length = 10, Width = 10, Height = 1, DesignId = 201, XValues = xValues };

            var machineGuid = Guid.NewGuid();
            machines.Add(new FusionMachine() { Id = machineGuid });
            machinesServiceMock.Setup(m => m.CanProduce(machineGuid, carton)).Returns(true);

            //Carton is producible on all corrugates and should have the same calculation on each as far as the individual carton is concerned.
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 2, It.Is<bool>(b => b == false)))
                .Returns(this.GetCorrugateCalculation(10, 11));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 2, It.Is<bool>(b => b)))
                .Returns(this.GetCorrugateCalculation(11, 10));

            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines.Select(m => new FusionMachine { Id = m.Id }), carton, 2, true);
            Specify.That(optimalCorrugate.Corrugate.Equals(corrugate1)).Should.BeTrue();
            Specify.That(optimalCorrugate.TileCount).Should.BeEqualTo(2);
        }

        private PackagingDesignCalculation GetCorrugateCalculation(MicroMeter l, MicroMeter w)
        {
            return new PackagingDesignCalculation { Length = l, Width = w };
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 8195")]
        public void ShouldNotUsedCache_WhenDesignXValuesAreDifferent()
        {
            machines.Add(new EmMachine());
            var corrugate = new Corrugate { Id = c10Id, Quality = 1, Thickness = .16, Width = 600 };
            var corrugateList = new List<Corrugate> { corrugate };

            var carton1 = new Carton() { DesignId = 201, Length = 10, Width = 11, Height = 0 };
            carton1.XValues.Add("X1", 1);
            carton1.XValues.Add("X2", 1);

            var carton2 = new Carton() { DesignId = 201, Length = 11, Width = 11, Height = 0 };
            carton2.XValues.Add("X1", 1);
            carton2.XValues.Add("X2", 2);

            machinesServiceMock.Setup(m => m.CanProduce(It.IsAny<Guid>(), It.IsAny<IProducible>())).Returns(true);

            //Setup corrugate calculation for carton1
            designManager.Setup(dm => dm.CalculateUsage(carton1, corrugate, 1, false)).Returns(this.GetCorrugateCalculation(10, 11));
            designManager.Setup(dm => dm.CalculateUsage(carton1, corrugate, 1, true)).Returns(this.GetCorrugateCalculation(10, 11));
            
            //Setup corrugate calculation for carton2
            designManager.Setup(dm => dm.CalculateUsage(carton2, corrugate, 1, false)).Returns(this.GetCorrugateCalculation(11, 11));
            designManager.Setup(dm => dm.CalculateUsage(carton2, corrugate, 1, true)).Returns(this.GetCorrugateCalculation(11, 11));
            
            var coc1 = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines, carton1, 1);
            var coc2 = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines, carton2, 1);

            Specify.That(coc1.AreaOnCorrugate).Should.Not.BeLogicallyEqualTo(coc2.AreaOnCorrugate, "Cache should not be used if carton has different x-values");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPickBestCorrugateWhenTilingAndThereIsLessWaste()
        {
            machines.Add(new FusionMachine());
            var corrugate2 = new Corrugate { Id = c1Id, Quality = 1, Thickness = .16, Width = 22 };  // 2 tiles will fit
            var corrugate3 = new Corrugate { Id = c10Id, Quality = 1, Thickness = .16, Width = 24 }; // 1 tile will fit
            var corrugateList = new List<Corrugate> { corrugate2, corrugate3 };

            var xValues = new Dictionary<string, double>();
            for (var i = 1; i < 21; i++)
                xValues.Add("X" + i, 0);
            var carton = new Carton() { Length = 10, Width = 11, Height = 0, DesignId = 201, XValues = xValues };


            var machineGuid = Guid.NewGuid();
            machines.Add(new FusionMachine() { Id = machineGuid });
            machinesServiceMock.Setup(m => m.CanProduce(machineGuid, carton)).Returns(true);
            /*
             View of test
             Carton L on corrugate = 10" Carton Width on corrugate is 11"
             * Corrugate 22"        Corrugate 24"      
             * |  +      |         |   +    |       
             * |  +      |         |   +    |       
             * |  +      |         |   +    |       
             * |  +      |         |   +    |       
             * |  +      |         |   +    |       
             * |  +      |         |   +    |       
             *  carton A=110        carton A=110       
             */

            //Carton is producible on all corrugates and should have the same calculation on each as far as the individual carton is concerned.
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 2, false))
                .Returns(this.GetCorrugateCalculation(10, 11));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 2, true))
                .Returns(this.GetCorrugateCalculation(11, 10));

            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines, carton, 2);
            Specify.That(optimalCorrugate.Corrugate.Equals(corrugate2)).Should.BeTrue();

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPickMostOptimalCorrugateForSpecificRotation()
        {
            machines.Add(new EmMachine());
            var corrugate1 = new Corrugate { Id = c1Id, Quality = 1, Thickness = .16, Width = 6 };  // most optimal when not rotated
            var corrugate2 = new Corrugate { Id = c2Id, Quality = 1, Thickness = .16, Width = 20 };  // least optimal
            var corrugate3 = new Corrugate { Id = c10Id, Quality = 1, Thickness = .16, Width = 18 }; // most optimal when rotated
            var corrugateList = new List<Corrugate> { corrugate1, corrugate2, corrugate3 };

            var xValues = new Dictionary<string, double>();
            for (var i = 1; i < 21; i++)
                xValues.Add("X" + i, 0);
            var carton = new Carton() { Length = 15, Width = 5, Height = 0, DesignId = 201, XValues = xValues };
            carton.Rotation = OrientationEnum.Degree90;

            var machineGuid = Guid.NewGuid();
            machines.Add(new FusionMachine() { Id = machineGuid });
            machinesServiceMock.Setup(m => m.CanProduce(machineGuid, carton)).Returns(true);

            //Carton is producible on all corrugates and should have the same calculation on each as far as the individual carton is concerned.
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 1, false))
                .Returns(this.GetCorrugateCalculation(15, 5));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 1, true))
                .Returns(this.GetCorrugateCalculation(5, 15));

            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines, carton, 1);
            Specify.That(optimalCorrugate.Corrugate.Equals(corrugate3)).Should.BeTrue();

            carton.Rotation = OrientationEnum.Degree0;

            var optimalNotRotatedCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(corrugateList, machines, carton, 1);
            Specify.That(optimalNotRotatedCorrugate.Corrugate.Equals(corrugate1)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnNullForOptimalCorrugateWhenNotProducibleOnCorrugate()
        {
            var xValues = new Dictionary<string, double>();
            for (var i = 1; i < 21; i++)
                xValues.Add("X" + i, 0);
            var carton = new Carton() { Length = 0, Width = 0, Height = 0, DesignId = 201, XValues = xValues };

            var corrugate1 = new Corrugate
            {
                Alias = "blah",
                Id = c1Id,
                Quality = 1,
                Thickness = 10,
                Width = 40
            };
            var corrugate2 = new Corrugate
            {
                Alias = "two",
                Id = c2Id,
                Quality = 1,
                Thickness = 20,
                Width = 20
            };
            var configuredCorrugates = new List<Corrugate> { corrugate1,
                corrugate2};

            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate1, 1, false))
                .Returns(GetCorrugateCalculation(160, 100));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate2, 1, false))
                .Returns(GetCorrugateCalculation(100.6, 100));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate1, 1, true))
                .Returns(GetCorrugateCalculation(100, 160));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), corrugate2, 1, true))
                .Returns(GetCorrugateCalculation(100, 100.6));

            var optimalCorrugate = optimalCorrugateCalculator.GetOptimalCorrugate(configuredCorrugates, machines, carton, 1);
            Specify.That(optimalCorrugate).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CartonOnCorrugateHasNullOrZeroAreaCalculationWhenCartonDoesNotFitOnCorrugate()
        {
            var corrugate = new Corrugate { Width = 9 };
            var key = string.Empty;
            var calculation = new PackagingDesignCalculation { Width = 10, Length = 12 };
            var result = new CartonOnCorrugate(new Carton(), corrugate, key, calculation, OrientationEnum.Degree0);

            Specify.That(result.AreaOnCorrugate).Should.BeEqualTo(0d);
        }
        [TestMethod]
        [TestCategory("Unit")]
        public void CartonOnCorrugateHasMaxValueAreaCalculationWhenCartonDoesNotFitOnCorrugate()
        {
            var corrugate = new Corrugate { Width = 100 };
            var key = string.Empty;
            var calculation = new PackagingDesignCalculation { Width = 120, Length = 100 };
            var result = new CartonOnCorrugate(new Carton(), corrugate, key, calculation, OrientationEnum.Degree0);

            Specify.That(result.AreaOnCorrugate).Should.BeLogicallyEqualTo(0d);
        }
#if DEBUG
        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateWithTileCountOneIfTileCountTwoCantBeProducedOnAnyMachine()
        //{
        //    new Story("Optimal Corrugate Calculation").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of One")

        //        .Given(ANonTilingFusionMachineWithId, machine1Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 0)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ATilableCartonForProductionGroup, "1")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 30d })
        //        .And(CalculatedTileCountIs, 1)

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldOnlyEvaluateCorrugateWithMatchingQualityWhenCalculatingOptimalCorrugate()
        //{
        //    new Story("Optimal Corrugate Calculation").Tag("TFS-1854")
        //        .InOrderTo("To restrict boxes to a specific corrugate type")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("corrugate with matching quality to match")
        //        .WithScenario("corrugate quality 1")

        //        .Given(ANonTilingFusionMachineWithId, machine1Id)
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 1)
        //        .And(ACorrugateWithAWidthAndQualityOf, 20d, 2)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ACartonForProductionGroupWithCorrugateQualityOptimalOnCorrugate, "1", (int?)1, new Corrugate { Width = 20d, Quality = 2 })

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 1)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 30d, Quality = 1 })

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldOnlyEvaluateCorrugateWithMatchingQualityWhenCalculatingOptimalCorrugateWhenSecondCorrugateIsBest()
        //{
        //    new Story("Optimal Corrugate Calculation").Tag("TFS-1854")
        //        .InOrderTo("To restrict boxes to a specific corrugate type")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("corrugate with matching quality to match")
        //        .WithScenario("corrugate quality 2")

        //        .Given(ANonTilingFusionMachineWithId, machine1Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 20d, 1)
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 2)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ACartonForProductionGroupWithCorrugateQualityOptimalOnCorrugate, "1", (int?)2, new Corrugate { Width = 20d, Quality = 1 })

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 1)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 30d, Quality = 2 })

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}


        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldEvaluateAllCorrugateQualitiesWhenCalculatingOptimalCorrugateIfNoCorrugateQualityIsSpecified()
        //{
        //    new Story("Optimal Corrugate Calculation").Tag("TFS-1854")
        //        .InOrderTo("To restrict boxes to a specific corrugate type")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("corrugate with matching quality to match")
        //        .WithScenario("corrugate quality null")

        //        .Given(ANonTilingFusionMachineWithId, machine1Id)
        //        .And(ACorrugateWithAWidthAndQualityOf, 20d, 1)
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 2)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ACartonForProductionGroupWithCorrugateQualityOptimalOnCorrugate, "1", (int?)null, new Corrugate { Width = 20d, Quality = 1 })

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 1)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 20d, Quality = 1 })

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}
#endif
        
        private void AMachineWithIdAddedToMachineGroupWithAlias(Guid machineId, string mgAlias)
        {
            machineGroups.First(mg => mg.Alias == mgAlias).ConfiguredMachines.Add(machineId);
        }

       
        private void ATilableCartonForProductionGroup(Guid productionAlias)
        {
            var xValues = new Dictionary<string, double>();
            for (var i = 1; i < 21; i++)
                xValues.Add("X" + i, 0);
            var carton = new Carton() { Length = 10, Width = 10, Height = 4, DesignId = 201, XValues = xValues };
            carton.Restrictions.Add(new ProductionGroupSpecificRestriction(productionAlias));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 2, false))
                .Returns(GetCorrugateCalculation(10, 14));
            designManager.Setup(dm => dm.CalculateUsage(It.IsAny<ICarton>(), It.IsAny<Corrugate>(), 2, true))
                .Returns(GetCorrugateCalculation(14, 10));
            Carton = carton;
        }

       
        private void CalculatedTileCountIs(int expectedTileCount)
        {
            Specify.That(OptimalCartonOnCorrugate.TileCount).Should.BeEqualTo(expectedTileCount);
        }

        private void CalcualtedProducibleMachines(IEnumerable<Guid> machineIds)
        {
            Specify.That(OptimalCartonOnCorrugate.ProducibleMachines.Count()).Should.BeEqualTo(machineIds.Count());
            foreach (var id in machineIds)
            {
                Specify.That(OptimalCartonOnCorrugate.ProducibleMachines.ContainsKey(id)).Should.BeTrue();
            }
        }

        private void CalculatedCorrugateIsCorrect(Corrugate expectedCorrugate)
        {
            Specify.That(OptimalCartonOnCorrugate.Corrugate.Width).Should.BeEqualTo(expectedCorrugate.Width);
        }

        private void OptimalCorrugateNotFound()
        {
            Specify.That(OptimalCartonOnCorrugate).Should.BeNull();
        }

        private void OptimalCorrugateIsCalculatedWithMaxTileCount(int maxTileCount = 2)
        {
            try
            {
                var corrugatesIdsToUse = productionGroup.Single(p => p.Id == Carton.Restrictions.OfType<ProductionGroupSpecificRestriction>().Single().Value).ConfiguredCorrugates;
                var corrugatesToUse = corrugates.Where(c => corrugatesIdsToUse.Contains(c.Id));
                var machineGroupIds = productionGroup.Single(p => p.Id == Carton.Restrictions.OfType<ProductionGroupSpecificRestriction>().Single().Value).ConfiguredMachineGroups;
                var machineIdsToUse = machineGroups.Where(mg => machineGroupIds.Contains(mg.Id)).SelectMany(mgs => mgs.ConfiguredMachines);
                var machinesToUse = machines.Where(m => machineIdsToUse.Contains(m.Id));
                OptimalCartonOnCorrugate = OptimalCorrugateCalculator.GetOptimalCorrugate(corrugatesToUse, machinesToUse, Carton, maxTileCount);
            }
            catch 
            {
                OptimalCartonOnCorrugate = null;
            }
            
        }

        private void AProductionGroupWithAliasMachineGroupAndCorrugates(string alias, IEnumerable<MachineGroup> machineGroups, IEnumerable<Guid> corrugatesInGroup)
        {
            productionGroup.Add(new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = alias,
                ConfiguredCorrugates = new ConcurrentList<Guid>(corrugatesInGroup.ToList()),
                ConfiguredMachineGroups = new ConcurrentList<Guid>(machineGroups.Select(mg => mg.Id).ToList())
            });
        }

        private void ACorrugateWithAWidthAndQualityOf(double width, int quality)
        {
            corrugates.Add(new Corrugate { Width = width, Quality = quality, Id = Guid.NewGuid()});
        }

        private void AMachineGroupWithAlias(string alias)
        {
            machineGroups.Add(new MachineGroup(){Alias = alias, Id = Guid.NewGuid()});
        }

        private void ATilingFusionMachineWithId(Guid id)
        {
            machinesServiceMock.Setup(
                m => m.CanProduce(id, It.Is<ICarton>(p => p.ProduceOnMachineId == id))).Returns(true);

            machines.Add(new FusionMachine { Id = id });
        }

        private void ANonTilingFusionMachineWithId(Guid id)
        {
            machinesServiceMock.Setup(
                m => m.CanProduce(id, It.Is<IProducible>(p => this.ContainsTilingRestriction(p))))
                .Returns(false);
            machinesServiceMock.Setup(
                m => m.CanProduce(id, It.Is<IProducible>(p => this.ContainsTilingRestriction(p))))
                .Returns(true);

            machines.Add(new FusionMachine { Id = id });
        }

        private bool ContainsTilingRestriction(IProducible carton)
        {
            var restriction = carton.Restrictions.OfType<CanProduceWithTileCountRestriction>().FirstOrDefault();
            return restriction != null
                   && restriction.Value > 1;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void NoOptimalCorrugate()
        {
            var pgId = Guid.NewGuid();
            new Story("Optimal Corrugate Calculation").Tag("TFS-765")
                .InOrderTo("To limit corrugate waste")
                .AsA(AnyPacksizeCustomer)
                .IWant("Calculation optimal corrugate based on producibility")
                .WithScenario("Tile Count of One")

                .Given(ANonTilingFusionMachineWithId, machine1Id)
                .And(ANonTilingFusionMachineWithId, machine2Id)
                .And(AMachineGroupWithAlias, "MG1")
                .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
                .And(AMachineWithIdAddedToMachineGroupWithAlias, machine2Id, "MG1")
                .And(ACorrugateWithAWidthAndQualityOf, 9d, 0)
                .And(ACorrugateWithAWidthAndQualityOf, 8d, 0)
                .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
                .And(ATilableCartonForProductionGroup, pgId)

                .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

                .Then(OptimalCorrugateNotFound)

                .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

#if DEBUG

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateWithTileCountTwoOnAllMachinesIfTileCountTwoCanBeProducedOnAnyMachine()
        //{
        //    new Story("Optimal Corrugate Calculation").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of One")

        //        .Given(ATilingFusionMachineWithId, machine1Id)
        //        .And(ATilingFusionMachineWithId, machine2Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine2Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 0)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ATilableCartonForProductionGroup, "1")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 30d })
        //        .And(CalculatedTileCountIs, 2)
        //        .And(CalcualtedProducibleMachines, machines.Select(m => m.Id))

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}

        //private void ATilingFusionMachineWithIdThatCanNotRotate(Guid id)
        //{
        //    //machinesServiceMock.Setup(
        //    //    m => m.CanProduce(id, It.Is<IPacksizeCarton>(p => p.Design.LengthOnZFold == "14"))).Returns(false); // rotated length
        //    //machinesServiceMock.Setup(
        //    //    m => m.CanProduce(id, It.Is<IPacksizeCarton>(p => p.Design.LengthOnZFold == "10"))).Returns(true);

        //    machines.Add(new FusionMachine { Id = id });
        //}



        
        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateToSecondOptimalIfOptimalCorrugateRequiresTilingToBeOptimalAndNoMachineCanProduceTiled()
        //{
        //    new Story("Optimal Corrugate Calculation").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of One")

        //        .Given(ANonTilingFusionMachineWithId, machine1Id)
        //        .And(ANonTilingFusionMachineWithId, machine2Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine2Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 0)
        //        .And(ACorrugateWithAWidthAndQualityOf, 20d, 0)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ATilableCartonForProductionGroup, "1")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 20d })
        //        .And(CalculatedTileCountIs, 1)
        //        .And(CalcualtedProducibleMachines, machines.Select(m => m.Id))

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateWithTileCountTwoOnMachineOneIfTileCountTwoOnlyCanBeProducedOnMachineOne()
        //{
        //    new Story("Optimal Corrugate Calculation").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of Two")

        //        .Given(ATilingFusionMachineWithId, machine1Id)
        //        .And(ANonTilingFusionMachineWithId, machine2Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine2Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 0)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ATilableCartonForProductionGroup, "1")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 30d })
        //        .And(CalculatedTileCountIs, 2)
        //        .And(CalcualtedProducibleMachines, new[] { machine1Id })

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}


        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateWithoutRotationIfMostOptimal()
        //{
        //    new Story("Optimal Corrugate Calculation with Tiles = 2 without rotating design").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of Two")

        //        .Given(ATilingFusionMachineWithId, machine1Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 0)
        //        .And(ACorrugateWithAWidthAndQualityOf, 20d, 0)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ATilableCartonForProductionGroup, "1")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 20d })
        //        .And(CalculatedTileCountIs, 2)
        //        .And(CalcualtedProducibleMachines, machines.Select(m => m.Id))

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());

        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateWithRotationIfRotatedIsOptimal()
        //{
        //    new Story("Optimal Corrugate Calculation with Tiles = 2 without rotating design").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of Two")

        //        .Given(ATilingFusionMachineWithId, machine1Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 29d, 0)// Carton Not Rotated 2Tiles W=28 = 1" waste, Rotated W=20 = 9" waste
        //        .And(ACorrugateWithAWidthAndQualityOf, 22d, 0)// Carton Not Rotated 2Tiles W=28 = <not producible as tiled>, Rotated W=20 = 2" waste
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ATilableCartonForProductionGroup, "1")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 29d })
        //        .And(CalculatedTileCountIs, 2)
        //        .And(CartonIsNotRotated)
        //        .And(CalcualtedProducibleMachines, machines.Select(m => m.Id))

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateWithoutRotationIfRotatationIsOptimalAndNoMachineCanProduceItRotated()
        //{

        //    new Story("Optimal Corrugate Calculation with Tiles = 2 without rotating design").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of Two")

        //        .Given(ATilingFusionMachineWithIdThatCanNotRotate, machine1Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(ACorrugateWithAWidthAndQualityOf, 29d, 0)// Carton Not Rotated 2Tiles W=28 = 1" waste, Rotated W=20 = 9" waste
        //        .And(ACorrugateWithAWidthAndQualityOf, 20d, 0)// Carton Not Rotated 2Tiles W=28 = <not producible as tiled>, Rotated W=20 = 0" waste
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups, corrugates.Select(c => c.Id))
        //        .And(ATilableCartonForProductionGroup, "1")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 29d })
        //        .And(CalculatedTileCountIs, 2)
        //        .And(CartonIsNotRotated)
        //        .And(CalcualtedProducibleMachines, machines.Select(m => m.Id))

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldCalculateOptimalCorrugateToTheMostOptimalMachineWithinTheProductionGroup()
        //{

        //    new Story("Optimal Corrugate Calculation with multiple production groups and machine in second production group is more optimal to produce carton.").Tag("TFS-765")
        //        .InOrderTo("To limit corrugate waste")
        //        .AsA(AnyPacksizeCustomer)
        //        .IWant("Calculation optimal corrugate based on producibility")
        //        .WithScenario("Tile Count of Two")

        //        .Given(ATilingFusionMachineWithId, machine1Id)
        //        .And(ANonTilingFusionMachineWithId, machine2Id)
        //        .And(AMachineGroupWithAlias, "MG1")
        //        .And(AMachineGroupWithAlias, "MG2")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine1Id, "MG1")
        //        .And(AMachineWithIdAddedToMachineGroupWithAlias, machine2Id, "MG2")
        //        .And(ACorrugateWithAWidthAndQualityOf, 30d, 0)
        //        .And(ACorrugateWithAWidthAndQualityOf, 20d, 0)
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "2", machineGroups.Where(m => m.ConfiguredMachines.Contains(machine2Id)), corrugates.Select(c => c.Id))
        //        .And(AProductionGroupWithAliasMachineGroupAndCorrugates, "1", machineGroups.Where(m => m.ConfiguredMachines.Contains(machine1Id)), corrugates.Select(c => c.Id))
        //        .And(ATilableCartonWithoutRotationForProductionGroup, "2")

        //        .When(OptimalCorrugateIsCalculatedWithMaxTileCount, 2)

        //        .Then(CalculatedCorrugateIsCorrect, new Corrugate { Width = 20d })
        //        .And(CalculatedTileCountIs, 1)
        //        .And(CartonIsNotRotated)
        //        .And(CalcualtedProducibleMachines, machines.Where(m => m.Id == machine2Id).Select(m => m.Id))

        //        .ExecuteWithReport(MethodBase.GetCurrentMethod());
        //}
#endif
        private void CartonIsNotRotated()
        {
            Specify.That(OptimalCartonOnCorrugate.Rotated).Should.BeFalse();
        }

        private void CartonIsRotated()
        {
            Specify.That(OptimalCartonOnCorrugate.Rotated).Should.BeTrue();

        }
    }
}
