﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Machines.Fusion;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using System;
using System.Collections.Generic;
using System.Linq;
using Testing.Specificity;
using Track = PackNet.Common.Interfaces.DTO.PhysicalMachine.Track;

namespace BusinessTests.Machines
{
    [TestClass]
    public class FusionMachinesTest
    {
        private FusionMachines fusionMachines;

        private Mock<IFusionMachineRepository> repoMock;
        private Mock<IMachineCommunicatorFactory> machineCommunicatorFactoryMock;
        private Mock<ICodeGenerationService> codeGenMock;
        private Mock<IFusionCommunicator> communicatorMock;
        private FusionMachine machine;
        private Mock<IServiceLocator> serviceLocatorMock;
        private EventAggregator aggregator;
        private Mock<ILogger> loggerMock;

        [TestInitialize]
        public void Setup()
        {
            loggerMock = new Mock<ILogger>();
            aggregator = new EventAggregator();
            repoMock = new Mock<IFusionMachineRepository>();
            codeGenMock = new Mock<ICodeGenerationService>();
            machineCommunicatorFactoryMock = new Mock<IMachineCommunicatorFactory>();
            communicatorMock = new Mock<IFusionCommunicator>();
            serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(m => m.Locate<ICodeGenerationService>()).Returns(codeGenMock.Object);
            machine = new FusionMachine()
            {
                Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
                {
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackNumber = 1
                    },
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackNumber = 2
                    }
                },
                PhysicalMachineSettings = new FusionPhysicalMachineSettings()
                {
                    TrackParameters = new FusionTracksParametersTestGuy(new List<Track>()
                    {
                        new Track()
                        {
                            Number = 1
                        },
                        new Track()
                        {
                            Number = 2
                        }
                    })
                },
            };

            repoMock.Setup(m => m.All()).Returns(new List<FusionMachine>() { machine });

            machineCommunicatorFactoryMock.Setup(m => m.GetMachineCommunicator(machine)).Returns(communicatorMock.Object);

            communicatorMock.Setup(x => x.IsConnected).Returns(true);

            fusionMachines = new FusionMachines(repoMock.Object, machineCommunicatorFactoryMock.Object, serviceLocatorMock.Object, aggregator, loggerMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_Not_SyncNumberOfTracksWhenUpdatingMachine_AndIs_Not_Connected()
        {
            communicatorMock.Setup(x => x.IsConnected).Returns(false);

            machine.NumTracks = 2;

            fusionMachines.Update(machine);

            communicatorMock.Verify(m => m.SetNumberOfTracks(It.IsAny<int>()), Times.Never);
            communicatorMock.Verify(m => m.SetAlignments(), Times.Never);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SynchronizeNumTracksWithTracks_BeforePersisting()
        {
            var newMachine = new FusionMachine();

            machine.CopyTo(newMachine);

            machineCommunicatorFactoryMock.Setup(m => m.GetMachineCommunicator(newMachine)).Returns(communicatorMock.Object);
            
            newMachine.Id = machine.Id;

            //single -> multi
            machine.NumTracks = 1;
            machine.Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
            {
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 1},
            };

            newMachine.NumTracks = 2;
            fusionMachines.Update(newMachine);

            Specify.That(fusionMachines.GetMachines().First().Tracks).Should.Not.BeNull();
            Specify.That(fusionMachines.GetMachines().First().Tracks.Count).Should.BeEqualTo(2);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(0).TrackNumber).Should.BeEqualTo(1);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(1).TrackNumber).Should.BeEqualTo(2);
            

            //multi -> single
            machine.NumTracks = 2;
            machine.Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
            {
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 1},
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 2},
            };

            newMachine.NumTracks = 1;
            fusionMachines.Update(newMachine);

            Specify.That(fusionMachines.GetMachines().First().Tracks).Should.Not.BeNull();
            Specify.That(fusionMachines.GetMachines().First().Tracks.Count).Should.BeEqualTo(1);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(0).TrackNumber).Should.BeEqualTo(1);


            //1 -> 4 tracks
            machine.NumTracks = 1;
            machine.Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
            {
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 1},
            };

            newMachine.NumTracks = 4;
            fusionMachines.Update(newMachine);

            Specify.That(fusionMachines.GetMachines().First().Tracks).Should.Not.BeNull();
            Specify.That(fusionMachines.GetMachines().First().Tracks.Count).Should.BeEqualTo(4);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(0).TrackNumber).Should.BeEqualTo(1);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(1).TrackNumber).Should.BeEqualTo(2);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(2).TrackNumber).Should.BeEqualTo(3);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(3).TrackNumber).Should.BeEqualTo(4);

            //4 -> 1 track   
            machine.NumTracks = 4;
            machine.Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
            {
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 1},
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 2},
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 3},
                new PackNet.Common.Interfaces.DTO.Machines.Track() {TrackNumber = 4},
            };

            newMachine.NumTracks = 1;
            fusionMachines.Update(newMachine);

            Specify.That(fusionMachines.GetMachines().First().Tracks).Should.Not.BeNull();
            Specify.That(fusionMachines.GetMachines().First().Tracks.Count).Should.BeEqualTo(1);
            Specify.That(fusionMachines.GetMachines().First().Tracks.ElementAt(0).TrackNumber).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_PersistMachine_When_CorrugatesAreChanged_And_MachineIsOffline()
        {
            communicatorMock.Setup(x => x.IsConnected).Returns(false);

            machine.NumTracks = 2;

             machine.CurrentStatus = MachineStatuses.MachineOffline;

            fusionMachines.ConfigureTracks(machine);

            repoMock.Verify(x => x.Update(machine), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSyncCorrugateThicknessWhenConfiguringTracks_And_IsConnected()
        {
            machine.NumTracks = 2;

            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;

            var trackBasedCutCreaseMachine = new Mock<ITrackBasedCutCreaseMachine>();

            trackBasedCutCreaseMachine.Setup(m => m.Id).Returns(machine.Id);
            trackBasedCutCreaseMachine.Setup(m => m.Tracks).Returns(new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
            {
                new PackNet.Common.Interfaces.DTO.Machines.Track()
                {
                    TrackNumber = 1,
                    TrackOffset = 0,
                    LoadedCorrugate = new Corrugate()
                    {
                        Id = new Guid(),
                        Alias = "C1",
                        Quality = 1,
                        Thickness = 0.16,
                        Width = 22.01
                    }
                },
                new PackNet.Common.Interfaces.DTO.Machines.Track()
                {
                    TrackNumber = 2,
                    TrackOffset = 0,
                    LoadedCorrugate = new Corrugate()
                    {
                        Id = new Guid(),
                        Alias = "C2",
                        Quality = 2,
                        Thickness = 0.13,
                        Width = 10.01
                    }
                }
            });

            fusionMachines.ConfigureTracks(trackBasedCutCreaseMachine.Object);

            communicatorMock.Setup(m => m.SetCorrugateThicknesses(It.IsAny<IEnumerable<double>>()))
                .Callback<IEnumerable<double>>(l =>
                {
                    Specify.That(l.ElementAt(0)).Should.BeLogicallyEqualTo(0.16);
                    Specify.That(l.ElementAt(1)).Should.BeLogicallyEqualTo(0.13);
                });

            communicatorMock.Verify(m => m.SetCorrugateThicknesses(It.IsAny<IEnumerable<double>>()), Times.Once);
        }

        private void SetPaused(ref MachineStatuses value)
        {
            value = MachinePausedStatuses.MachinePaused;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSyncTrackParametersCorrectlyWhenMachineIsSingleTrack()
        {
            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;
            var trackBasedCutCreaseMachine = new Mock<ITrackBasedCutCreaseMachine>();

            trackBasedCutCreaseMachine.Setup(m => m.Id).Returns(machine.Id);
            trackBasedCutCreaseMachine.Setup(m => m.Tracks).Returns(new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
            {
                new PackNet.Common.Interfaces.DTO.Machines.Track()
                {
                    TrackNumber = 1,
                    TrackOffset = 0,
                    LoadedCorrugate = new Corrugate()
                    {
                        Id = new Guid(),
                        Alias = "C1",
                         Quality = 1,
                        Thickness = 0.16,
                        Width = 22.01
                    }
                }
            });
            machine.NumTracks = 1;

            communicatorMock.Setup(m => m.SetCorrugateThicknesses(It.IsAny<IEnumerable<double>>()))
                .Callback<IEnumerable<double>>(l => Specify.That(l.ElementAt(0)).Should.BeLogicallyEqualTo(0.16));
            communicatorMock.Setup(m => m.SetCorrugateWidths(It.IsAny<IEnumerable<PackNet.Common.Interfaces.DTO.Machines.Track>>()))
                .Callback<IEnumerable<PackNet.Common.Interfaces.DTO.Machines.Track>>(l => Specify.That(l.ElementAt(0).LoadedCorrugate.Width).Should.BeLogicallyEqualTo(22.01));

            fusionMachines.ConfigureTracks(trackBasedCutCreaseMachine.Object);

            communicatorMock.Verify(m => m.SetCorrugateWidths(It.IsAny<IEnumerable<PackNet.Common.Interfaces.DTO.Machines.Track>>()), Times.Once);
            communicatorMock.Verify(m => m.SetCorrugateThicknesses(It.IsAny<IEnumerable<double>>()), Times.Once);
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 7232")]
        public void Bug7232_UpdatingFusionMachine_Should_Not_ChangeTracks_When_Null()
        {
            var newMachine = new FusionMachine();

            machine.CopyTo(newMachine);

            machineCommunicatorFactoryMock.Setup(m => m.GetMachineCommunicator(newMachine)).Returns(communicatorMock.Object);

            communicatorMock.Setup(x => x.IsConnected).Returns(false);

            machine.NumTracks = 2;

            newMachine.Tracks = null;

            newMachine.Id = machine.Id;

            fusionMachines.Update(newMachine);

            Specify.That(fusionMachines.GetMachines().First().Tracks).Should.Not.BeNull();
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 7232")]
        public void Bug7232_UpdatingFusionMachine_Should_ChangeTracks_If_Present()
        {
            var newMachine = new FusionMachine();

            machine.CopyTo(newMachine);

            machineCommunicatorFactoryMock.Setup(m => m.GetMachineCommunicator(newMachine)).Returns(communicatorMock.Object);

            communicatorMock.Setup(x => x.IsConnected).Returns(false);

            machine.NumTracks = 2;

            newMachine.Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
            {
                new PackNet.Common.Interfaces.DTO.Machines.Track()
                {
                    LoadedCorrugate = new Corrugate(){Alias = "C1", Id = Guid.NewGuid()},
                    TrackNumber = 1,
                    TrackOffset = 23
                },
                new PackNet.Common.Interfaces.DTO.Machines.Track()
                {
                    
                    LoadedCorrugate = new Corrugate() {Alias = "C2", Id = Guid.NewGuid()},
                    TrackNumber = 2,
                    TrackOffset = 27
                }
            };

            newMachine.Id = machine.Id;
            newMachine.NumTracks = 2;

            fusionMachines.Update(newMachine);

            var tracks = fusionMachines.GetMachines().First().Tracks;

            Specify.That(tracks).Should.Not.BeNull();
            Specify.That(tracks[0].LoadedCorrugate).Should.BeEqualTo(newMachine.Tracks[0].LoadedCorrugate);
            Specify.That(tracks[0].TrackOffset).Should.BeEqualTo(newMachine.Tracks[0].TrackOffset);
            Specify.That(tracks[0].TrackNumber).Should.BeEqualTo(newMachine.Tracks[0].TrackNumber);
            Specify.That(tracks[1].LoadedCorrugate).Should.BeEqualTo(newMachine.Tracks[1].LoadedCorrugate);
            Specify.That(tracks[1].TrackOffset).Should.BeEqualTo(newMachine.Tracks[1].TrackOffset);
            Specify.That(tracks[1].TrackNumber).Should.BeEqualTo(newMachine.Tracks[1].TrackNumber);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void UpdateShouldNotReplaceCurrentMachineObject()
        {
            //If the refernce of the machine within EmMachines or FusionMachines is changed during the update all the observables will get disconnected to the MG and so on
            var updateCalled = false;
            var newMachine = new FusionMachine();

            machine.CopyTo(newMachine);

            machineCommunicatorFactoryMock.Setup(m => m.GetMachineCommunicator(newMachine)).Returns(communicatorMock.Object);

            communicatorMock.Setup(x => x.IsConnected).Returns(false);

            machine.NumTracks = 2;

            newMachine.Tracks = null;

            newMachine.Id = machine.Id;

            repoMock.Setup(m => m.Update(It.IsAny<FusionMachine>())).Callback<FusionMachine>(nm =>
            {
                updateCalled = true;
                Specify.That(nm).Should.BeSameAs(machine);
            });

            fusionMachines.Update(newMachine);

            Retry.For(() => updateCalled, TimeSpan.FromSeconds(1));

            Specify.That(updateCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void UpdateOnlySynchronizesMachineWhenTheCommunicatorIsConnected()
        {
            var synchronizeCalled = false;
            var newMachine = new FusionMachine();

            machine.CopyTo(newMachine);

            machineCommunicatorFactoryMock.Setup(m => m.GetMachineCommunicator(newMachine)).Returns(communicatorMock.Object);

            communicatorMock.Setup(x => x.IsConnected).Returns(false);

            communicatorMock.Setup(m => m.SynchronizeMachine(It.IsAny<bool>())).Callback<bool>(reload =>
            {
                synchronizeCalled = true;
                Specify.That(reload).Should.BeTrue();
            });

            fusionMachines.Update(newMachine);

            Retry.For(() => synchronizeCalled, TimeSpan.FromSeconds(1));

            Specify.That(synchronizeCalled).Should.BeFalse();
            
            communicatorMock.Setup(x => x.IsConnected).Returns(true);
            
            fusionMachines.Update(newMachine);

            Retry.For(() => synchronizeCalled, TimeSpan.FromSeconds(1));

            Specify.That(synchronizeCalled).Should.BeTrue();
        }
    }

    public class FusionTracksParametersTestGuy : FusionTracksParameters
    {
        public FusionTracksParametersTestGuy(List<Track> tracks)
            : base(tracks)
        {
        }
    }
}
