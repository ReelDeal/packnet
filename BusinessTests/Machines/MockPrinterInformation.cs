﻿using System;

namespace BusinessTests.Machines
{
    using PackNet.Common.Interfaces.DTO.Settings;

    public class MockPrinterInformation : PrinterInformation
    {
        public void SetId(Guid id)
        {
            base.Id = id;
        }

        public void SetSerialPort(string serialPort)
        {
            base.SerialPort = serialPort;
        }

        public void SetIsPriorityPrinter(bool priority)
        {
            base.IsPriorityPrinter = priority;
        }

        public void SetIpOrDnsNameAndPort(string ipOrDnsNameAndPort)
        {
            base.IpOrDnsNameAndPort = ipOrDnsNameAndPort;
        }
    }
}
