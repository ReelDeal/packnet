﻿namespace BusinessTests.Machines
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.DTO.Machines;

    using Testing.Specificity;

    [TestClass]
    public class LegacyTracksLimitParametersTest
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ConstructorTest()
        {
            var parameters = new LegacyTracksLimitParameters(1.0, 2.0, new List<double> {3.0, 4.0});
            Specify.That(parameters.MinPosition).Should.BeEqualTo(1.0);
            Specify.That(parameters.MaxPosition).Should.BeEqualTo(2.0);
            Specify.That(parameters.StartingPositions.Count()).Should.BeEqualTo(2);
            Specify.That(parameters.StartingPositions.ElementAt(0)).Should.BeEqualTo(3.0);
            Specify.That(parameters.StartingPositions.ElementAt(1)).Should.BeEqualTo(4.0);
        }
    }
}
