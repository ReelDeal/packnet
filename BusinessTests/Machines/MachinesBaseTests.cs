﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.Machines;
using System;
using System.Collections.Generic;
using System.Linq;
using Testing.Specificity;

namespace BusinessTests.Machines
{
    [TestClass]
    public class MachinesBaseTests
    {
        private Mock<IRepository<EmMachine>> emMachineRepoMock;
        private Mock<IEmMachineRepository> emRepoMock;
        private Mock<IMachineCommunicatorFactory> machineComFactoryMock;
        private Mock<IEmMachineCommunicator> machineComMock;
        private Mock<ILogger> loggerMock;
        private Mock<IServiceLocator> serviceLocatorMock;

        private MachinesBase<EmMachine> machineBase;
        private EventAggregator aggregator;
            
        [TestInitialize]
        public void Setup()
        {
            emMachineRepoMock = new Mock<IRepository<EmMachine>>();
            machineComFactoryMock = new Mock<IMachineCommunicatorFactory>();
            emRepoMock = new Mock<IEmMachineRepository>();
            machineComMock = new Mock<IEmMachineCommunicator>();
            loggerMock = new Mock<ILogger>();
            aggregator = new EventAggregator();
            serviceLocatorMock = new Mock<IServiceLocator>();
            machineBase = new EmMachines(emRepoMock.Object, machineComFactoryMock.Object, aggregator, aggregator, serviceLocatorMock.Object, loggerMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 8897")]
        public void ConfigureTracksShouldNotReestablishConnectionToMachine()
        {
            this.machineComFactoryMock.Setup(m => m.GetMachineCommunicator(It.IsAny<IMachine>())).Returns(machineComMock.Object);

            var machine = new EmMachine()
            {
                Id = Guid.NewGuid(),
                Tracks = new ConcurrentList<Track>()
                {
                    new Track(){TrackNumber = 1, TrackOffset = 1},
                    new Track(){TrackNumber = 2, TrackOffset = 1},
                    new Track(){TrackNumber = 3, TrackOffset = 1},
                },
            };

            this.machineBase.Create(machine);

            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;
            this.machineBase.ConfigureTracks(machine);

            this.machineComFactoryMock.Verify(m => m.GetMachineCommunicator(It.IsAny<IMachine>()), Times.Once); 
            //Happens once when the machien is added to the system
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddVersionRestrictionWhenUpdatingCapabilities()
        {
            machineComFactoryMock.Setup(m => m.GetMachineCommunicator(It.IsAny<IMachine>())).Returns(machineComMock.Object);
            var machine = new EmMachine()
            {
                Id = Guid.NewGuid(),
                PlcVersion = "BestVersion"
            };

            machineBase.Create(machine);
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("BestVersion"))).Should.Not.BeNull();
            
            machine.PlcVersion = "New Version";
            machineBase.Update(machine);
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("BestVersion"))).Should.BeNull();
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("New Version"))).Should.Not.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 10442")]
        public void ShouldUpdateCommunicatorWhenUpdatingMachine()
        {
            var tester = new MachinesBaseTester(emMachineRepoMock.Object, machineComFactoryMock.Object);
            var machine = new EmMachine()
            {
                Id = Guid.NewGuid(),
                PlcVersion = "BestVersion",
                IpOrDnsName = "123.123.123.123",
                Port = 123
            };

            tester.AddCommunicator(machine.Id, new Mock<IMachineCommunicator>().Object);
            tester.CreatedMachines.Add(machine);

            tester.Update(machine);

            machineComFactoryMock.Verify(
                m =>
                    m.UpdateWebRequestCreator(
                        It.Is<IPacksizeCutCreaseMachine>(
                            mach => mach.Id == machine.Id && mach.IpOrDnsName == machine.IpOrDnsName && mach.Port == machine.Port),
                        It.IsAny<IMachineCommunicator>()),
                Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateCapabilitiesWhenPlcVersionChanges()
        {
            machineComFactoryMock.Setup(m => m.GetMachineCommunicator(It.IsAny<IMachine>())).Returns(machineComMock.Object);
            var machine = new EmMachine()
            {
                Id = Guid.NewGuid(),
                PlcVersion = "BestVersion"
            };

            machineBase.Create(machine);
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("BestVersion"))).Should.Not.BeNull();

            machine.PlcVersion = "NewVersion";

            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("BestVersion"))).Should.BeNull();
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("NewVersion"))).Should.Not.BeNull();

            machineBase.Update(machine);
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("NewVersion"))).Should.Not.BeNull();

            machine.PlcVersion = "VersionV2";
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("NewVersion"))).Should.BeNull();
            Specify.That(machine.CurrentCapabilities.FirstOrDefault(c => c.DisplayValue.Contains("VersionV2"))).Should.Not.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ConfigureTracksShouldCallCorrectMethodBasedOnMachineStatus()
        {
            var machine = new EmMachine()
            {
                Id = Guid.NewGuid()
            };
            var tester = new MachinesBaseTester(emMachineRepoMock.Object, machineComFactoryMock.Object);
            tester.CreatedMachines.Add(machine);

            machine.CurrentStatus = MachineStatuses.MachineOffline;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(0);

            machine.CurrentStatus = MachineStatuses.MachineOnline;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(0);

            machine.CurrentStatus = MachineStatuses.MachineInitializing;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(0);

            machine.CurrentStatus = MachineErrorStatuses.MachineError;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(1);

            machine.CurrentStatus = MachineErrorStatuses.EmergencyStop;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(2);

            machine.CurrentStatus = MachineErrorStatuses.MachineNotConfigured;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(2);

            machine.CurrentStatus = MachinePausedStatuses.MachineChangingCorrugate;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(3);

            machine.CurrentStatus = PacksizePackagingMachineErrorStatuses.MachineInOutOfCorrugate;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(4);

            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;
            tester.ConfigureTracks(machine);
            Specify.That(tester.PoweredOffCorrugateUpdateCallCount).Should.BeEqualTo(1);
            Specify.That(tester.PoweredOnCorrugateUpdateCallCount).Should.BeEqualTo(5);
        }
    }

    public class MachinesBaseTester : MachinesBase<EmMachine>
    {
        public MachinesBaseTester(IRepository<EmMachine> repository, IMachineCommunicatorFactory machineCommunicatorFactory)
            : base(repository, machineCommunicatorFactory, new Mock<IServiceLocator>().Object, new Mock<ILogger>().Object)
        {
        }

        public int PoweredOffCorrugateUpdateCallCount { get; set; }
        public int PoweredOnCorrugateUpdateCallCount { get; set; }

        public new List<EmMachine> CreatedMachines
        {
            get { return base.CreatedMachines; }
        }

        public void AddCommunicator(Guid id, IMachineCommunicator communicator)
        {
            MachineComunication.TryAdd(id, communicator);
        }

        protected override void MachinePoweredOffCorrugateUpdate(EmMachine localMachine, ITrackBasedCutCreaseMachine machine)
        {
            PoweredOffCorrugateUpdateCallCount++;
        }

        protected override void MachinePoweredOnCorrugateUpdate(EmMachine localMachine, ITrackBasedCutCreaseMachine machine)
        {
            PoweredOnCorrugateUpdateCallCount++;
        }
    }
}
