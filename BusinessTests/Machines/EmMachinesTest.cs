﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.Machines;

using Testing.Specificity;

namespace BusinessTests.Machines
{
    [TestClass]
    public class EmMachinesTest
    {
        private EmMachine machine;
        private EmMachines emMachines;
        private EventAggregator aggregator;

        private Mock<IEmMachineCommunicator> machineCommunicatorMock;
        private Mock<IEmMachineRepository> emMachinesRepository;
        private Mock<IMachineCommunicatorFactory> communicatorFactory;
        private Mock<ILogger> loggerMock;
        private Mock<IServiceLocator> serviceLocator;

        [TestInitialize]
        public void Setup()
        {
            loggerMock = new Mock<ILogger>();
            machine = new EmMachine();
            aggregator = new EventAggregator();
            serviceLocator = new Mock<IServiceLocator>();
            emMachinesRepository = GetMachinesRepositoryMock();
            communicatorFactory = GetCommunicatorFactoryMock();
            emMachines = new EmMachines(emMachinesRepository.Object, communicatorFactory.Object, aggregator, aggregator, serviceLocator.Object, loggerMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void EmMachineShouldKeepLoadedCorrugatesWhenChangingNumTracks()
        {
            machine.Tracks = new ConcurrentList<Track>()
            {
                new Track(){ LoadedCorrugate = new Corrugate(){ Alias = "T1"}, TrackNumber = 1},
                new Track(){ LoadedCorrugate = new Corrugate(){ Alias = "T2"}, TrackNumber = 2},
                new Track(){ LoadedCorrugate = new Corrugate(){ Alias = "T3"}, TrackNumber = 3}
            };

            machine.NumTracks = 5;

            Specify.That(machine.Tracks.Count).Should.BeEqualTo(5);
            Specify.That(machine.Tracks.ElementAt(0).TrackNumber).Should.BeEqualTo(1);
            Specify.That(machine.Tracks.ElementAt(1).TrackNumber).Should.BeEqualTo(2);
            Specify.That(machine.Tracks.ElementAt(2).TrackNumber).Should.BeEqualTo(3);
            Specify.That(machine.Tracks.ElementAt(3).TrackNumber).Should.BeEqualTo(4);
            Specify.That(machine.Tracks.ElementAt(4).TrackNumber).Should.BeEqualTo(5);

            Specify.That(machine.Tracks.ElementAt(0).LoadedCorrugate.Alias).Should.BeEqualTo("T1");
            Specify.That(machine.Tracks.ElementAt(1).LoadedCorrugate.Alias).Should.BeEqualTo("T2");
            Specify.That(machine.Tracks.ElementAt(2).LoadedCorrugate.Alias).Should.BeEqualTo("T3");
            Specify.That(machine.Tracks.ElementAt(3).LoadedCorrugate).Should.BeNull();
            Specify.That(machine.Tracks.ElementAt(4).LoadedCorrugate).Should.BeNull();

            machine.NumTracks = 2;

            Specify.That(machine.Tracks.Count).Should.BeEqualTo(2);
            Specify.That(machine.Tracks.ElementAt(0).TrackNumber).Should.BeEqualTo(1);
            Specify.That(machine.Tracks.ElementAt(1).TrackNumber).Should.BeEqualTo(2);
            Specify.That(machine.Tracks.ElementAt(0).LoadedCorrugate.Alias).Should.BeEqualTo("T1");
            Specify.That(machine.Tracks.ElementAt(1).LoadedCorrugate.Alias).Should.BeEqualTo("T2");

            machine.NumTracks = 0;

            Specify.That(machine.Tracks.Count).Should.BeEqualTo(0); 
            
            machine.NumTracks = 2;

            Specify.That(machine.Tracks.Count).Should.BeEqualTo(2);
            Specify.That(machine.Tracks.ElementAt(0).TrackNumber).Should.BeEqualTo(1);
            Specify.That(machine.Tracks.ElementAt(1).TrackNumber).Should.BeEqualTo(2);
            Specify.That(machine.Tracks.ElementAt(0).LoadedCorrugate).Should.BeNull();
            Specify.That(machine.Tracks.ElementAt(1).LoadedCorrugate).Should.BeNull();

        }
        
        private Mock<IMachineCommunicatorFactory> GetCommunicatorFactoryMock()
        {
            var mock = new Mock<IMachineCommunicatorFactory>();

            machineCommunicatorMock = new Mock<IEmMachineCommunicator>();

            mock.Setup(m => m.GetMachineCommunicator(It.IsAny<IMachine>())).Returns(machineCommunicatorMock.Object);

            return mock;
        }

        private Mock<IEmMachineRepository> GetMachinesRepositoryMock()
        {
            var mock = new Mock<IEmMachineRepository>();

            mock.Setup(m => m.All()).Returns(new List<EmMachine> { machine });

            return mock;
        }
    }
}
