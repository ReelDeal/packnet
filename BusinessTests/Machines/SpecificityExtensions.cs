namespace BusinessTests.Machines //Machines
{
    using System;
    using System.Globalization;
    using System.Linq;

    using Testing.Specificity;

    public static class SpecificityExtensions
    {
        public static void HaveSeen(this NegativeConstraint<PropertyChangedWatcher> self, string propertyName)
        {
            HaveSeen(self, propertyName, null, new object[0]);
        }

        public static void HaveSeen(this NegativeConstraint<PropertyChangedWatcher> self, string propertyName, string message, params object[] parameters)
        {
            if (self.Value.Any(e => e.PropertyName == propertyName))
            {
                FailExtension("ShouldNotHaveSeen", message, parameters);
            }
        }

        public static void HaveSeen(this Constraint<PropertyChangedWatcher> self, string propertyName)
        {
            HaveSeen(self, propertyName, null, new object[0]);
        }

        public static void HaveSeen(this Constraint<PropertyChangedWatcher> self, string propertyName, string message, params object[] parameters)
        {
            if (self.Value.All(e => e.PropertyName != propertyName))
            {
                FailExtension("ShouldNotHaveSeen", message, parameters);
            }
        }

        public static ConstrainedValue<PropertyChangedWatcher> HaveSeen(this ConstrainedValue<PropertyChangedWatcher> self, string propertyName)
        {
            return HaveSeen(self, propertyName, null, new object[0]);
        }

        public static ConstrainedValue<PropertyChangedWatcher> HaveSeen(this ConstrainedValue<PropertyChangedWatcher> self, string propertyName, string message, params object[] parameters)
        {
            if (self.Value.Any(e => e.PropertyName == propertyName))
            {
                FailExtension("ShouldNotHaveSeen", message, parameters);
            }

            return self;
        }

        private static void FailExtension(string constraint, string message, object[] messageParameters)
        {
            FailExtension(constraint, null, message, messageParameters);
        }

        private static void FailExtension(string constraint, string reason, string message, object[] messageParameters)
        {
            if (!string.IsNullOrEmpty(message) && messageParameters != null && messageParameters.Length != 0)
            {
                message = string.Format(CultureInfo.CurrentCulture, message, messageParameters);
            }

            if (!string.IsNullOrEmpty(reason))
            {
                message = reason + " " + message;
            }
            throw new ConstraintFailedException(constraint, new Exception(message));
        }
    }
}