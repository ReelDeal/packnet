﻿using System;
using System.Net;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Machines;
using PackNet.Business.Settings;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Communication.Communicators.EM;
using PackNet.Communication.Communicators.Fusion;

using Testing.Specificity;

using PackNetServerSettings = PackNet.Common.Interfaces.DTO.Settings.PackNetServerSettings;

namespace BusinessTests.Machines
{
    [TestClass]
    [DeploymentItem(@"TestData\PhysicalMachineSettings", @"PhysicalMachineSettings")]
    public class MachineCommunicatorFactoryTests
    {

        private IMachineCommunicatorFactory factory;
        private Mock<IPackNetServerSettings> packnetServerSettingsMock;
        private Mock<ILogger> loggerMock;
        private Mock<IUserNotificationService> userNotificationMock;
        private EventAggregator aggregator;

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();
            loggerMock = new Mock<ILogger>();
            packnetServerSettingsMock = new Mock<IPackNetServerSettings>();
            userNotificationMock = new Mock<IUserNotificationService>();

            packnetServerSettingsMock.Setup(m => m.GetSettings()).Returns(new PackNetServerSettings()
            {
                MachineCallbackIpAddress = new IPAddress(1),
                MachineCallbackPort = 1
            });

            factory = new MachineCommunicatorFactory(packnetServerSettingsMock.Object, aggregator, aggregator, loggerMock.Object, userNotificationMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateFusionMachineCommunicatorWhenPassedAFusionMachine()
        {
            var fusionMachine = new FusionMachine()
            {
                PhysicalSettingsFilePath = @"PhysicalMachineSettings\machineTheoretical_inches.xml"
            };

            var communicator = factory.GetMachineCommunicator(fusionMachine);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator is FusionCommunicator).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 10442")]
        [TestCategory("Integration")]
        public void ShouldCallUpdateWebReqeustCreatorInComWhenUpdatingCom()
        {
            var fusionMachine = new FusionMachine()
            {
                PhysicalSettingsFilePath = @"PhysicalMachineSettings\machineTheoretical_inches.xml",
                IpOrDnsName = "123.123.123.123",
                Port = 123
            };

            var mock = new Mock<IMachineCommunicator>();

            factory.UpdateWebRequestCreator(fusionMachine, mock.Object);

            mock.Verify(m => m.UpdateWebRequestCreator(It.Is<IWebRequestCreator>(cr => cr.ServerAddress == "123.123.123.123:123")));
        }

        [TestMethod]
        [TestCategory("Unit")]
        [Ignore]
        //needs an EM physical machine settings file
        public void ShouldCreateEmMachineCommunicatorWhenPassedAEmMachine()
        {
            var emMachine = new EmMachine();

            var communicator = factory.GetMachineCommunicator(emMachine);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator is EmCommunicator).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        [ExpectedException(typeof(Exception))]
        public void ShouldThrowExceptionWhenPassedUnknownIMachine()
        {
            var communicator = factory.GetMachineCommunicator(null);
        }
    }
}
