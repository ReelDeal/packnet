﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Data.Machines;



using Testing.Specificity;

using TestUtils;

namespace BusinessTests.Machines
{
    using System.Reactive.Subjects;

    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;

    [TestClass]
    public class  MachineGroupsTests
    {
        private Mock<IMachineGroupRepository> machineGroupRepositoryMock;
        private Mock<ISelectionAlgorithmDispatchService> selectionAlgorithmServiceMock;
        private Mock<IAggregateMachineService> machineServiceMock;
        private Mock<IMachineGroupWorkflowDispatcher> machineGroupWorkflowDispatcherMock;
        private Mock<ILogger> loggerMock;
        private Mock<IUserNotificationService> userNotificationMock;
        private EventAggregator eventAggregator;

        private MachineGroupBl objectInTest;

        [TestInitialize]
        public void Init()
        {
            machineGroupRepositoryMock = new Mock<IMachineGroupRepository>();
            selectionAlgorithmServiceMock = new Mock<ISelectionAlgorithmDispatchService>();
            machineServiceMock = new Mock<IAggregateMachineService>();
            machineGroupWorkflowDispatcherMock = new Mock<IMachineGroupWorkflowDispatcher>();
            userNotificationMock = new Mock<IUserNotificationService>();
            loggerMock = new Mock<ILogger>();
            eventAggregator = new EventAggregator();
        }

        [TestMethod]
        public void ShouldNotBeAbleToUpdateMachineGroupIfMachineInUse()
        {
            //setup
            var mgId = Guid.NewGuid();
            var machineGroupInstances = new List<MachineGroup>
            {
                new MachineGroup { Id = mgId,Alias = "one",  MaxQueueLength = 2, CurrentProductionStatus = MachineProductionInProgressStatuses.ProductionInProgress}
            };
            machineGroupRepositoryMock.Setup(r => r.All()).Returns(machineGroupInstances);

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);
            try
            {
                //test
                objectInTest.Update(machineGroupInstances[0]);
                Specify.Failure("Expected exception to thrown.");
            }
            catch (MachineGroupException e)
            {
                Specify.That(e.MachineGroupId).Should.BeEqualTo(mgId);
                Specify.That(e.MessageType).Should.BeEqualTo(MachineGroupMessages.MachineGroupInProduction);
            }
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldNotSendJobsThatHasBeenRemovedWhileInQueue()
        {
            
            var mgId = Guid.NewGuid();
            var machineId = Guid.NewGuid();
            var machine = new EmMachine();
            machine.Id = machineId;
            machine.CurrentStatus = MachineStatuses.MachineOnline;
            
            var machineGroup = new MachineGroup { Id = mgId, Alias = "one", MaxQueueLength = 2, CurrentProductionStatus = MachineProductionInProgressStatuses.ProductionInProgress };
            machineGroup.ConfiguredMachines.Add(machineId);
            
            machineServiceMock.Setup(m => m.FindById(It.IsAny<Guid>())).Returns(machine);
            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { machine });

            var machineGroupInstances = new List<MachineGroup> { machineGroup };
            machineGroupRepositoryMock.Setup(r => r.All()).Returns(machineGroupInstances);
            
            var consoleLogger = new ConsoleLogger();

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, consoleLogger,
                eventAggregator, userNotificationMock.Object);
                        
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "key1", null, OrientationEnum.Degree0) { TileCount = 1 };            
            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc };
            var carton2 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "222", CartonOnCorrugate = coc };            
            var order1 = new Order(carton1, 1) { OrderId = "First order in the kit" };
            var order2 = new Order(carton2, 1) { OrderId = "Second order in the kit" };

            var kit = new Kit { Id = Guid.NewGuid(), ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1 }) };
            var kit2 = new Kit { Id = Guid.NewGuid(), ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order2 }) };

            var firstOrder = new Order(kit, 1) { OrderId = "First Kit order" };
            var secondOrder = new Order(kit2, 1) { OrderId = "Second Kit order" };
            bool dispatchCalled = false;

            // simulate blocking call, should really be replaced with real implementation
            machineGroupWorkflowDispatcherMock.Setup(
                d => d.DispatchCreateProducibleWorkflow(It.IsAny<MachineGroup>(), It.IsAny<IProducible>()))
                .Callback<MachineGroup, IProducible>(
                    (m, p) =>
                    {
                        dispatchCalled = true;
                        Retry.For(() => p.ProducibleStatus == ProducibleStatuses.ProducibleCompleted, TimeSpan.FromSeconds(10));
                    });

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            Specify.That(objectInTest.GetQueueCount(machineGroup)).Should.BeEqualTo(0);

            objectInTest.AddItemToQueue(machineGroup, firstOrder);
            objectInTest.AddItemToQueue(machineGroup, secondOrder);
            Specify.That(objectInTest.GetQueueCount(machineGroup)).Should.BeEqualTo(2);

            Specify.That(firstOrder.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.AddedToMachineGroupQueue);
            Specify.That(secondOrder.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.AddedToMachineGroupQueue);

            secondOrder.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

            Retry.For(() => dispatchCalled, TimeSpan.FromSeconds(4));
                        
            firstOrder.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            
            Retry.For(() => objectInTest.GetQueueCount(machineGroup) == 0, TimeSpan.FromSeconds(4));

            Specify.That(objectInTest.GetQueueCount(machineGroup)).Should.BeEqualTo(0);

            machineGroupWorkflowDispatcherMock.Verify(d => d.DispatchCreateProducibleWorkflow(It.Is<MachineGroup>(mg => mg.Id == mgId), It.Is<IProducible>(pr => pr.Id == firstOrder.Id)), Times.Once());
            machineGroupWorkflowDispatcherMock.Verify(d => d.DispatchCreateProducibleWorkflow(It.Is<MachineGroup>(mg => mg.Id == mgId), It.Is<IProducible>(pr => pr.Id == secondOrder.Id)), Times.Never);
        }

        [TestMethod]
        public void ShouldNotBeAbleToDeleteMachineGroupIfMachineInUse()
        {
            //setup
            var mgId = Guid.NewGuid();
            var machineGroupInstances = new List<MachineGroup>
            {
                new MachineGroup { Id = mgId,Alias = "one",  MaxQueueLength = 2, CurrentProductionStatus = MachineProductionInProgressStatuses.ProductionInProgress}
            };
            machineGroupRepositoryMock.Setup(r => r.All()).Returns(machineGroupInstances);

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);
            try
            {
                //test
                objectInTest.Delete(machineGroupInstances[0]);
                Specify.Failure("Expected exception to thrown.");
            }
            catch (MachineGroupException e)
            {
                Specify.That(e.MachineGroupId).Should.BeEqualTo(mgId);
                Specify.That(e.MessageType).Should.BeEqualTo(MachineGroupMessages.MachineGroupInProduction);
            }
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 19805")]
        [TestCategory("Integration")]
        public void SomethingSomething_PauseMachineGroup()
        {
            var logger = new ConsoleLogger();
            var machineId = Guid.NewGuid();
            var mgId = Guid.NewGuid();
            var machine = new EmMachine() { Alias = "M1", Id = machineId };
            machine.CurrentStatus = MachineStatuses.MachineOnline;
            machine.AddOrUpdateCapabilities(new List<ICapability> { new MachineGroupSpecificCapability(mgId) });

            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { machine });
            machineServiceMock.Setup(m => m.FindById(machineId)).Returns(machine);
            
            var mg = new MachineGroup
                     {
                         Id = mgId,
                         Alias = "one",
                         MaxQueueLength = 2,
                         CurrentProductionStatus = MachineProductionStatuses.ProductionIdle,
                         CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline,
                     };

            mg.ConfiguredMachines.Add(machineId);
            var machineGroupInstances = new List<MachineGroup> { mg };
            machineGroupRepositoryMock.Setup(r => r.All()).Returns(machineGroupInstances);


            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, logger,
                eventAggregator, userNotificationMock.Object);

            var order = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "NotImportant"}, 1);
            mg.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            objectInTest.AddItemToQueue(mg, order);
            order.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            
            objectInTest.ToggleMachineGroupPaused(mgId);
            Retry.For(() => mg.CurrentStatus == MachineGroupAvailableStatuses.ProduceCurrentAndPause, TimeSpan.FromSeconds(5));
            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.ProduceCurrentAndPause, "Since we are producing an item we should go to ProduceCurrentAndPause");
            
            objectInTest.ToggleMachineGroupPaused(mgId);
            Retry.For(() => mg.CurrentStatus == MachineGroupAvailableStatuses.ProduceCurrentAndPause, TimeSpan.FromSeconds(5));
            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.ProduceCurrentAndPause, "Should not be able to unpause machine while in ProduceCurrentAndPause");

            Retry.For(() => mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupPaused, TimeSpan.FromSeconds(5));
            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.MachineGroupPaused, "Since the MG is paused we should be able to unpause it.");
            objectInTest.ToggleMachineGroupPaused(mgId);

            Retry.For(() => mg.CurrentStatus == MachineGroupAvailableStatuses.MachineGroupOnline, TimeSpan.FromSeconds(5));
            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.MachineGroupOnline);
        }

        [TestMethod]
        public void ShouldAskForMoreWorkWhenCapabilitiesChangedOnMachineInGroup()
        {
            var eventRaised = false;
            //setup
            var m1 = new ZebraPrinter { Id = Guid.NewGuid() };
            m1.AddOrUpdateCapabilities(new List<ICapability> { new BasicCapability<string>("cap1") });

            var m1Machines = new ConcurrentList<Guid> { m1.Id };
            var mgId = Guid.NewGuid();
            var machineGroupInstances = new List<MachineGroup>
            {
                new MachineGroup { Id = mgId,Alias = "one", ConfiguredMachines = m1Machines, MaxQueueLength = 2}
            };
            machineGroupRepositoryMock.Setup(r => r.All()).Returns(machineGroupInstances);
            machineServiceMock.Setup(ms => ms.FindById(m1.Id)).Returns(m1);

            //verification setup
            eventAggregator.GetEvent<Message>()
                .Where(m => m.MessageType == MachineGroupMessages.GetWorkForMachineGroup)
                .DurableSubscribe(msg =>
                {
                    eventRaised = msg.MachineGroupId == mgId;
                }, loggerMock.Object);

            //test
            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);

            m1.AddOrUpdateCapabilities(new List<ICapability> { new BasicCapability<string>("cap3") });

            Specify.That(eventRaised).Should.BeTrue("Event never raised");

        }

        [TestMethod]
        public void ShouldRemoveCapabilitiesForMachine_IfNewCapabilitiesIsNull()
        {
            var machine1 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion1"
            };
            var machine2 = new ZebraPrinter()
            {
                Id = Guid.NewGuid(),
                Alias = "zebra1"
            };
            var machine3 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion2"
            };

            machine3.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine3cap"), new BasicCapability<string>("dup") });
            machine2.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("dup") });
            var template = new Template() { Id = Guid.NewGuid(), Name = "123Template", Content = "asd" };
            machine2.AddOrUpdateCapabilities(new List<BasicCapability<Template>> { new BasicCapability<Template>(template) });
            machine1.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine1cap") });
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id, machine2.Id }, MaxQueueLength = 2 };

            mg.UpdateMachineGroupCapabilities(machine1.Id, machine1.CurrentCapabilities);
            mg.UpdateMachineGroupCapabilities(machine2.Id, machine2.CurrentCapabilities);
            mg.UpdateMachineGroupCapabilities(machine3.Id, machine3.CurrentCapabilities);

            Specify.That(mg.CurrentCapabilities.Count()).Should.BeLogicallyEqualTo(4);
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine3cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<Template>(template))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine1cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("dup"))).Should.BeTrue();

            mg.UpdateMachineGroupCapabilities(machine2.Id, null);
            Specify.That(mg.CurrentCapabilities.Count()).Should.BeLogicallyEqualTo(3);
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine3cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<Template>(template))).Should.BeFalse();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine1cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("dup"))).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldBeWiredUpToAllMachinesStatusChangedAndUpdateMachineGroupStatus()
        {
            var machine1 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion1"
            };
            var machine2 = new ZebraPrinter()
            {
                Id = Guid.NewGuid(),
                Alias = "zebra1"
            };
            var machine3 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion2"
            };
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id, machine2.Id }, MaxQueueLength = 2 };

            machineGroupRepositoryMock.Setup(r => r.All()).Returns(new List<MachineGroup> { mg });
            machineServiceMock.Setup(ms => ms.FindById(machine1.Id)).Returns(machine1);
            machineServiceMock.Setup(ms => ms.FindById(machine2.Id)).Returns(machine2);
            machineServiceMock.Setup(ms => ms.FindById(machine3.Id)).Returns(machine3);
            machineServiceMock.Setup(ms => ms.Machines).Returns(new List<IMachine> { machine1, machine2, machine3 });

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);

            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupUnavailableStatuses.MachineGroupOffline);

            machine2.CurrentStatus = MachineStatuses.MachineOnline;
            machine3.CurrentStatus = MachineStatuses.MachineOnline;

            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupUnavailableStatuses.MachineGroupOffline);

            machine1.CurrentStatus = MachineStatuses.MachineOnline;

            //after all machines are online mg should go to paused
            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.MachineGroupPaused);
        }

        [TestMethod]
        public void ShouldBeWiredUpToAllMachinesProductionStatusChangedAndUpdateMachineGroupStatus()
        {
            var machine1 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion1"
            };
            var machine2 = new ZebraPrinter()
            {
                Id = Guid.NewGuid(),
                Alias = "zebra1"
            };
            var machine3 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion2"
            };
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id, machine2.Id }, MaxQueueLength = 2 };

            machineGroupRepositoryMock.Setup(r => r.All()).Returns(new List<MachineGroup> { mg });
            machineServiceMock.Setup(ms => ms.FindById(machine1.Id)).Returns(machine1);
            machineServiceMock.Setup(ms => ms.FindById(machine2.Id)).Returns(machine2);
            machineServiceMock.Setup(ms => ms.FindById(machine3.Id)).Returns(machine3);
            machineServiceMock.Setup(ms => ms.Machines).Returns(new List<IMachine> { machine1, machine2, machine3 });

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);
            
            machine2.CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;
            machine3.CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;

            Specify.That(mg.CurrentProductionStatus).Should.BeEqualTo(MachineProductionStatuses.ProductionIdle);

            machine1.CurrentProductionStatus = MachineProductionInProgressStatuses.ProductionInProgress;

            Specify.That(mg.CurrentProductionStatus).Should.BeEqualTo(MachineProductionInProgressStatuses.ProductionInProgress);
            machine3.CurrentProductionStatus = MachineProductionErrorStatuses.ProductionError;

            Specify.That(mg.CurrentProductionStatus).Should.BeEqualTo( MachineProductionErrorStatuses.ProductionError);
        }

        [TestMethod]
        public void ShouldSetMachineGroupToLightBarrierBrokenWhenMachineHasLightBarrierBroken()
        {
            var machine1 = new EmMachine()
            {
                Id = Guid.NewGuid(),
                Alias = "em"
            };
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id }, MaxQueueLength = 2 };

            machineGroupRepositoryMock.Setup(r => r.All()).Returns(new List<MachineGroup> { mg });
            machineServiceMock.Setup(ms => ms.FindById(machine1.Id)).Returns(machine1);
            machineServiceMock.Setup(ms => ms.Machines).Returns(new List<IMachine> { machine1 });

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);

            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupUnavailableStatuses.MachineGroupOffline);

            machine1.CurrentStatus = MachineStatuses.MachineOnline;

            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.MachineGroupPaused);

            machine1.CurrentStatus = MachinePausedStatuses.LightBarrierBroken;

            Specify.That(mg.CurrentStatus).Should.BeEqualTo(MachineGroupWarningStatuses.MachineGroupLightBarrierBroken);
        }

        [TestMethod]
        public void ShouldBeWiredUpToAllMachinesInstancesCapabilitiesChangedAndUpdateMachineGroupCapabilitiesToMatch()
        {
            var machine1 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion1"
            };
            var machine2 = new ZebraPrinter()
            {
                Id = Guid.NewGuid(),
                Alias = "zebra1"
            };
            var machine3 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion2"
            };

            IEnumerable<ICapability> currentCapabilities = null;
            machine3.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine3cap"), new BasicCapability<string>("dup") });
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id, machine2.Id }, MaxQueueLength = 2 };
            mg.CurrentCapabilitiesChangedObservable.Subscribe(cap => currentCapabilities = cap);

            machineGroupRepositoryMock.Setup(r => r.All()).Returns(new List<MachineGroup> { mg });
            machineServiceMock.Setup(ms => ms.FindById(machine1.Id)).Returns(machine1);
            machineServiceMock.Setup(ms => ms.FindById(machine2.Id)).Returns(machine2);
            machineServiceMock.Setup(ms => ms.FindById(machine3.Id)).Returns(machine3);

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);

            Thread.Sleep(100);
            Specify.That(mg.CurrentCapabilities.Count()).Should.BeEqualTo(2);
            Specify.That(currentCapabilities.Count()).Should.BeEqualTo(2);

            machine2.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine2cap"), new BasicCapability<string>("dup") });

            Thread.Sleep(100);
            Specify.That(mg.CurrentCapabilities.Count()).Should.BeEqualTo(3);
            Specify.That(currentCapabilities.Count()).Should.BeEqualTo(3);

            machine1.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine1cap") });

            Thread.Sleep(100);
            Specify.That(mg.CurrentCapabilities.Count()).Should.BeEqualTo(4);
            Specify.That(currentCapabilities.Count()).Should.BeEqualTo(4);

            machine3.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine3bcap") });

            Thread.Sleep(100);
            Specify.That(mg.CurrentCapabilities.Count()).Should.BeEqualTo(4);
            Specify.That(currentCapabilities.Count()).Should.BeEqualTo(4);

            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine3bcap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine2cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine1cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("dup"))).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldUpdateMachinesCapabilitiesForMachinesRemovedFromGroup()
        {
            var machine1 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion1"
            };
            var machine2 = new ZebraPrinter()
            {
                Id = Guid.NewGuid(),
                Alias = "zebra1"
            };
            var machine3 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion2"
            };

            IEnumerable<ICapability> currentCapabilities = null;

            machine1.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine1cap"), new BasicCapability<string>("duplicate") });
            machine2.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine2cap"), new BasicCapability<string>("duplicate") });
            machine3.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine3cap") });
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id, machine2.Id }, MaxQueueLength = 2 };
            mg.UpdateMachineGroupCapabilities(machine1.Id, machine1.CurrentCapabilities);
            mg.UpdateMachineGroupCapabilities(machine2.Id, machine2.CurrentCapabilities);
            mg.UpdateMachineGroupCapabilities(machine2.Id, machine3.CurrentCapabilities);
            
            mg.CurrentCapabilitiesChangedObservable.Subscribe(cap => currentCapabilities = cap);

            machineGroupRepositoryMock.Setup(r => r.All()).Returns(new List<MachineGroup> { mg });
            machineServiceMock.Setup(ms => ms.FindById(machine1.Id)).Returns(machine1);
            machineServiceMock.Setup(ms => ms.FindById(machine2.Id)).Returns(machine2);
            machineServiceMock.Setup(ms => ms.FindById(machine3.Id)).Returns(machine3);
            
            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);
            var mgUpdated = new MachineGroup { Id = mg.Id, ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id }, MaxQueueLength = 2 };

            objectInTest.Update(mgUpdated);
            Thread.Sleep(100);
            Specify.That(mg.CurrentCapabilities.Count()).Should.BeEqualTo(3);
            Specify.That(currentCapabilities.Count()).Should.BeEqualTo(3);

            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine3cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine1cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("duplicate"))).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldUpdateMachinesCapabilitiesForMachinesAddedToGroup()
        {
            var machine1 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion1"
            };
            var machine2 = new ZebraPrinter()
            {
                Id = Guid.NewGuid(),
                Alias = "zebra1"
            };
            var machine3 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion2"
            };

            IEnumerable<ICapability> currentCapabilities = null;

            machine1.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine1cap"), new BasicCapability<string>("duplicate") });
            machine2.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine2cap"), new BasicCapability<string>("duplicate") });
            machine3.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine3cap") });
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { }, MaxQueueLength = 2 };
            mg.CurrentCapabilitiesChangedObservable.Subscribe(cap => currentCapabilities = cap);

            machineGroupRepositoryMock.Setup(r => r.All()).Returns(new List<MachineGroup> { mg });
            machineServiceMock.Setup(ms => ms.FindById(machine1.Id)).Returns(machine1);
            machineServiceMock.Setup(ms => ms.FindById(machine2.Id)).Returns(machine2);
            machineServiceMock.Setup(ms => ms.FindById(machine3.Id)).Returns(machine3);

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);
            var mgUpdated = new MachineGroup { Id = mg.Id, ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id }, MaxQueueLength = 2 };

            objectInTest.Update(mgUpdated);
            Specify.That(mg.CurrentCapabilities.Count()).Should.BeEqualTo(3);
            Specify.That(currentCapabilities.Count()).Should.BeEqualTo(3, "expected the mg to raise capabilities changed observable");

            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine3cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine1cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("duplicate"))).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldSetInitialValuesOfAllMgCapabilities()
        {
            var machine1 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion1"
            };
            var machine2 = new ZebraPrinter()
            {
                Id = Guid.NewGuid(),
                Alias = "zebra1"
            };
            var machine3 = new FusionMachine
            {
                Id = Guid.NewGuid(),
                Alias = "fusion2"
            };


            machine1.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine1cap"), new BasicCapability<string>("duplicate") });
            machine2.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine2cap"), new BasicCapability<string>("duplicate") });
            machine3.AddOrUpdateCapabilities(new List<BasicCapability<string>> { new BasicCapability<string>("machine3cap") });
            var mg = new MachineGroup { Id = Guid.NewGuid(), ConfiguredMachines = new ConcurrentList<Guid> { machine1.Id, machine3.Id, machine2.Id }, MaxQueueLength = 2 };

            machineGroupRepositoryMock.Setup(r => r.All()).Returns(new List<MachineGroup> { mg });
            machineServiceMock.Setup(ms => ms.FindById(machine1.Id)).Returns(machine1);
            machineServiceMock.Setup(ms => ms.FindById(machine2.Id)).Returns(machine2);
            machineServiceMock.Setup(ms => ms.FindById(machine3.Id)).Returns(machine3);

            objectInTest = new MachineGroupBl(machineGroupRepositoryMock.Object, selectionAlgorithmServiceMock.Object,
                machineServiceMock.Object, eventAggregator, machineGroupWorkflowDispatcherMock.Object, loggerMock.Object,
                eventAggregator, userNotificationMock.Object);

            Specify.That(mg.CurrentCapabilities.Count()).Should.BeEqualTo(4);
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine3cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine1cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("machine2cap"))).Should.BeTrue();
            Specify.That(mg.CurrentCapabilities.Contains(new BasicCapability<string>("duplicate"))).Should.BeTrue();
        }
    }
}
