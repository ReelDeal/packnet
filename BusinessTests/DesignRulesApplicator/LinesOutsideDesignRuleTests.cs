﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.DesignRulesApplicator
{
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class LinesOutsideDesignRuleTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveVerticalLinesThatAreCompletelyOutsideDesignOnTopAndBottom()
        {
            var physicalDesign = new PhysicalDesign
            {
                Length = 400,
                Width = 400
            };
            //Top
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(-100, -10), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, -90), new PhysicalCoordinate(200, -1), LineType.cut));
            //Bottom
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 401), new PhysicalCoordinate(200, 500), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(500, 410), new PhysicalCoordinate(500, 500), LineType.cut));

            var ruleAppliedDesign = LinesOutsideDesignRule.ApplyTo(physicalDesign);
            Specify.That(ruleAppliedDesign.Lines.Any()).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldShortenVerticalLinesThatArePartiallyOutsideDesignOnTopAndBottom()
        {
            var physicalDesign = new PhysicalDesign
            {
                Length = 400,
                Width = 400
            };
            //Top
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(100, -100), new PhysicalCoordinate(100, 100), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, -90), new PhysicalCoordinate(200, 0), LineType.cut));
            //Bottom
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(100, 200), new PhysicalCoordinate(100, 500), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 400), new PhysicalCoordinate(200, 500), LineType.cut));

            var ruleAppliedDesign = LinesOutsideDesignRule.ApplyTo(physicalDesign);
            Specify.That(ruleAppliedDesign.Lines.Count()).Should.BeEqualTo(2, "Two of the lines get modified so that the length is 0 these lines should be removed");
            Specify.That(ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 100)).Should.BeEqualTo(1, "Line from -100 to 100 should get starting position at design edge");
            Specify.That(ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 200 && l.EndCoordinate.Y == 400)).Should.BeEqualTo(1, "Line from 200 to 500 should get end position at design edge");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveHorizontalLinesThatAreCompletelyOutsideDesignOnLeftAndRightSide()
        {
            var physicalDesign = new PhysicalDesign
            {
                Length = 400,
                Width = 400
            };
            //Left
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(-10, -100), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-90, 200), new PhysicalCoordinate(-1, 200), LineType.cut));
            //Right
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(401, 200), new PhysicalCoordinate(500, 200), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(410, 500), new PhysicalCoordinate(500, 500), LineType.cut));

            var ruleAppliedDesign = LinesOutsideDesignRule.ApplyTo(physicalDesign);
            Specify.That(ruleAppliedDesign.Lines.Any()).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldShortenHorizontalLinesThatArePartiallyOutsideDesignOnLeftAndRightSide()
        {
            var physicalDesign = new PhysicalDesign
            {
                Length = 400,
                Width = 400
            };
            //Left
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, 100), new PhysicalCoordinate(100, 100), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-90, 200), new PhysicalCoordinate(0, 200), LineType.cut));
            //Right
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(500, 100), LineType.cut));
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(400, 200), new PhysicalCoordinate(500, 200), LineType.cut));

            var ruleAppliedDesign = LinesOutsideDesignRule.ApplyTo(physicalDesign);
            Specify.That(ruleAppliedDesign.Lines.Count()).Should.BeEqualTo(2, "Two of the lines get modified so that the length is 0 these lines should be removed");
            Specify.That(ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 0 && l.EndCoordinate.X == 100)).Should.BeEqualTo(1, "Line from -100 to 100 should get starting position at design edge");
            Specify.That(ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 200 && l.EndCoordinate.X == 400)).Should.BeEqualTo(1, "Line from 200 to 500 should get end position at design edge");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldMoveHorizontalLinesThatAreOutisdeTheDesignOnTopAndBottom_ToDesignEdges()
        {
            var physicalDesign = new PhysicalDesign
            {
                Length = 400,
                Width = 400
            };
            //Lines outside on left side and top should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(-10, -100), LineType.cut));
            //Lines outside on left side and bottom should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, 500), new PhysicalCoordinate(-10, 500), LineType.cut));
            //Lines outside on right side and top should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(410, -100), new PhysicalCoordinate(500, -100), LineType.cut));
            //Lines outside on right side and bottom should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(410, 500), new PhysicalCoordinate(500, 500), LineType.cut));
            //Lines partially outside on left side and outside on top should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(200, -100), LineType.cut));
            //Lines partially outside on right side and outside on top should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, -100), new PhysicalCoordinate(500, -100), LineType.cut));
            //Lines partially outside on left side and outside on bottom should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, 500), new PhysicalCoordinate(200, 500), LineType.cut));
            //Lines partially outside on right side and outside on bottom should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(200, 500), new PhysicalCoordinate(500, 500), LineType.cut));
            //Lines partially outside on left and right side and outside on top should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(500, -100), LineType.cut));
            //Lines partially outside on left and right side and outside on bottom should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, 500), new PhysicalCoordinate(500, 500), LineType.cut));
            //Lines that are inside left and right and outside on top should be moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(100, -100), new PhysicalCoordinate(300, -100), LineType.cut));
            //Lines that are inside left and right and outside on bottom should be moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(100, 500), new PhysicalCoordinate(300, 500), LineType.cut));

            var ruleAppliedDesign = LinesOutsideDesignRule.ApplyTo(physicalDesign);

            Specify.That(ruleAppliedDesign.Lines.Count()).Should.BeEqualTo(8, "4 out of the 12 lines should be removed completely");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 0 && l.StartCoordinate.X == 0 && l.EndCoordinate.X == 200))
                .Should.BeEqualTo(1, "Line (-100,-100) to (200,-100) should have been shortened and moved new coordinates (0,0) to (200,0)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 0 && l.StartCoordinate.X == 200 && l.EndCoordinate.X == 400))
                .Should.BeEqualTo(1, "Line (200,-100) to (500,-100) should have been shortened and moved new coordinates (200,0) to (400,0)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 400 && l.StartCoordinate.X == 0 && l.EndCoordinate.X == 200))
                .Should.BeEqualTo(1, "Line (-100,500) to (200,500) should have been shortened and moved new coordinates (0,400) to (200,400)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 400 && l.StartCoordinate.X == 200 && l.EndCoordinate.X == 400))
                .Should.BeEqualTo(1, "Line (200,500) to (500,500) should have been shortened and moved new coordinates (200,400) to (400,400)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 0 && l.StartCoordinate.X == 0 && l.EndCoordinate.X == 400))
                .Should.BeEqualTo(1, "Line (-100,-100) to (500,-100) should have been shortened and moved new coordinates (0,0) to (400,0)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 400 && l.StartCoordinate.X == 0 && l.EndCoordinate.X == 400))
                .Should.BeEqualTo(1, "Line (-100,500) to (500,500) should have been shortened and moved new coordinates (0,400) to (400,400)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 0 && l.StartCoordinate.X == 100 && l.EndCoordinate.X == 300))
                .Should.BeEqualTo(1, "Line (100,-100) to (300,-100) should have been moved new coordinates (100,0) to (300,0)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.Y == 400 && l.StartCoordinate.X == 100 && l.EndCoordinate.X == 300))
                .Should.BeEqualTo(1, "Line (100,500) to (300,500) should have been moved new coordinates (100,400) to (300,400)");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldMoveVerticalLinesThatAreOutisdeTheDesignOnLeftAndRightSide_ToDesignEdges()
        {
            var physicalDesign = new PhysicalDesign
            {
                Length = 400,
                Width = 400
            };
            //Lines outside on top and left side should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(-100, -10), LineType.cut));
            //Lines outside on top and right side should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(500, -100), new PhysicalCoordinate(500, -10), LineType.cut));
            //Lines outside on bottom and left side should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, 410), new PhysicalCoordinate(-100, 500), LineType.cut));
            //Lines outside on bottom and right side should be removed
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(500, 410), new PhysicalCoordinate(500, 500), LineType.cut));
            //Lines partially outside on top and outside on left side should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(-100, 200), LineType.cut));
            //Lines partially outside on bottom and outside on left side should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, 200), new PhysicalCoordinate(-100, 500), LineType.cut));
            //Lines partially outside on top and outside on right side should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(500, -100), new PhysicalCoordinate(500, 200), LineType.cut));
            //Lines partially outside on bottom and outside on right side should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(500, 500), new PhysicalCoordinate(500, 200), LineType.cut));
            //Lines partially outside on top and bottom and outside on left side should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, -100), new PhysicalCoordinate(-100, 500), LineType.cut));
            //Lines partially outside on top and bottom and outside on right side should be shortened and moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(500, -100), new PhysicalCoordinate(500, 500), LineType.cut));
            //Lines that are inside top and bottom and outside on left side should be moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(-100, 100), new PhysicalCoordinate(-100, 300), LineType.cut));
            //Lines that are inside top and bottom and outside on right side should be moved
            physicalDesign.Add(new PhysicalLine(new PhysicalCoordinate(500, 100), new PhysicalCoordinate(500, 300), LineType.cut));

            var ruleAppliedDesign = LinesOutsideDesignRule.ApplyTo(physicalDesign);

            Specify.That(ruleAppliedDesign.Lines.Count()).Should.BeEqualTo(8, "4 out of the 12 lines should be removed completely");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 0 && l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 200))
                .Should.BeEqualTo(1, "Line (-100,-100) to (-100,200) should have been shortened and moved new coordinates (0,0) to (0,200)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 0 && l.StartCoordinate.Y == 200 && l.EndCoordinate.Y == 400))
                .Should.BeEqualTo(1, "Line (-100,200) to (-100,500) should have been shortened and moved new coordinates (0,200) to (0,400)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 400 && l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 200))
                .Should.BeEqualTo(1, "Line (500,-100) to (500,-100) should have been shortened and moved new coordinates (400,0) to (400,200)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 400 && l.StartCoordinate.Y == 200 && l.EndCoordinate.Y == 400))
                .Should.BeEqualTo(1, "Line (500,200) to (500,500) should have been shortened and moved new coordinates (400,200) to (400,400)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 0 && l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 400))
                .Should.BeEqualTo(1, "Line (-100,-100) to (-100,500) should have been shortened and moved new coordinates (0,0) to (0,400)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 400 && l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 400))
                .Should.BeEqualTo(1, "Line (500,-100) to (500,500) should have been shortened and moved new coordinates (400,0) to (400,400)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 0 && l.StartCoordinate.Y == 100 && l.EndCoordinate.Y == 300))
                .Should.BeEqualTo(1, "Line (-100,100) to (-100,300) should have been moved new coordinates (0,100) to (0,300)");
            Specify.That(
                ruleAppliedDesign.Lines.Count(l => l.StartCoordinate.X == 400 && l.StartCoordinate.Y == 100 && l.EndCoordinate.Y == 300))
                .Should.BeEqualTo(1, "Line (500,100) to (500,300) should have been moved new coordinates (400,100) to (400,300)");
        }
    }
}
