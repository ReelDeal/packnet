﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces;

namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class EMOverlappingRulesTests
    {
        #region TwoCompletelyOverlappingLines
        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingVerticalLinesOfTypesNunatabAndCut_ShouldResultInOneLineOfTypeNunatab()
        {
            var lines = GenerateTwoVerticalOverlappingLinesOfType(LineType.cut, LineType.nunatab);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.nunatab);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingHorizontalLinesOfTypesNunatabAndCut_ShouldResultInOneLineOfTypeNunatab()
        {
            var lines = GenerateTwoHorizontalOverlappingLinesOfType(LineType.nunatab, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.nunatab);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void LesserLineThatOverlapsTwoConnectedDominantLinesShouldBeRemoved()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(0, 300), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(0, 350), LineType.cut)
            };

            var ruleAppliedLines = new OverLappingLinesRule().ApplyTo(lines);

            ruleAppliedLines.OrderBy(l => l.StartCoordinate.Y).ForEach(Console.WriteLine);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(2);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 200 && l.Type == LineType.cut)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 200 && l.EndCoordinate.Y == 350 && l.Type == LineType.cut)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void LinesShouldNotBeMissedWhenPerforationOverlapsCutAndCreaseOverlapsPerforation()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(0, 300), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(0, 350), LineType.crease)
            };

            var ruleAppliedLines = new OverLappingLinesRule().ApplyTo(lines);

            ruleAppliedLines.OrderBy(l => l.StartCoordinate.Y).ForEach(Console.WriteLine);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(3);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 200 && l.Type == LineType.cut)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 200 && l.EndCoordinate.Y == 300 && l.Type == LineType.perforation)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 300 && l.EndCoordinate.Y == 350 && l.Type == LineType.crease)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void LineShouldBeSplitSeveralTimesIfItIsOverlappingMultipleDominantLines()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 700), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0,20), LineType.cut ),
                new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(0,320), LineType.cut ),
                new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(0,620), LineType.cut ),
            };

            var ruleAppliedLines = new OverLappingLinesRule().ApplyTo(lines);

            ruleAppliedLines.OrderBy(l => l.StartCoordinate.Y).ForEach(Console.WriteLine);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(6);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 20 && l.Type == LineType.cut)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 20 && l.EndCoordinate.Y == 300 && l.Type == LineType.crease)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 300 && l.EndCoordinate.Y == 320 && l.Type == LineType.cut)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 320 && l.EndCoordinate.Y == 600 && l.Type == LineType.crease)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 600 && l.EndCoordinate.Y == 620 && l.Type == LineType.cut)).Should.BeEqualTo(1);
            Specify.That(ruleAppliedLines.Count(l => l.StartCoordinate.Y == 620 && l.EndCoordinate.Y == 700 && l.Type == LineType.crease)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingVerticalLinesOfTypesCutAndPreforation_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoVerticalOverlappingLinesOfType(LineType.cut, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingHorizontalLinesOfTypesCutAndPreforation_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoHorizontalOverlappingLinesOfType(LineType.cut, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingVerticalLinesOfTypesCutAndCrease_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoVerticalOverlappingLinesOfType(LineType.cut, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingHorizontalLinesOfTypesCutAndCrease_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoHorizontalOverlappingLinesOfType(LineType.cut, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingVerticalLinesOfTypeCut_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoVerticalOverlappingLinesOfType(LineType.cut, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingHorizontalLinesOfTypeCut_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoHorizontalOverlappingLinesOfType(LineType.cut, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingVerticalLinesOfTypePerforationAndCrease_ShouldResultInTypePerforation()
        {
            var lines = GenerateTwoVerticalOverlappingLinesOfType(LineType.crease, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingHorizontalLinesOfTypePerforationAndCrease_ShouldResultInTypePerforation()
        {
            var lines = GenerateTwoHorizontalOverlappingLinesOfType(LineType.crease, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingVerticalLinesOfTypePerforation_ShouldResultInOneLineOfTypePerforation()
        {
            var lines = GenerateTwoVerticalOverlappingLinesOfType(LineType.perforation, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingHorizentalLinesOfTypePerforation_ShouldResultInOneLineOfTypePerforation()
        {
            var lines = GenerateTwoHorizontalOverlappingLinesOfType(LineType.perforation, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingVerticalLinesOfTypeCrease_ShouldResultInOneLineOfTypeCrease()
        {
            var lines = GenerateTwoVerticalOverlappingLinesOfType(LineType.crease, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void TwoCompletelyOverlappingHorizontalLinesOfTypeCrease_ShouldResultInOneLineOfTypeCrease()
        {
            var lines = GenerateTwoHorizontalOverlappingLinesOfType(LineType.crease, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }
        #endregion

        #region CompletelyOverlapping with longer dominant line
        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalNunatabLineCompletelyOverlappingACutLine_ShouldResultInOneLineOfTypeNunatab()
        {
            var lines = GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.nunatab, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.nunatab);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalNunatabLineCompletelyOverlappingACutLine_ShouldResultInOneLineOfTypeNunatab()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.nunatab, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.nunatab);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalCutLineCompletelyOverlappingAPerforationLine_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.cut, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalCutLineCompletelyOverlappingAPerforationLine_ShouldResultInOneLineOfTypeCut()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.cut, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalPerforationLineCompletelyOverlappingACreaseLine_ShouldResultInOneLineOfTypePerforation()
        {
            var lines = GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.perforation, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 1000));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalPerforationLineCompletelyOverlappingACreaseLine_ShouldResultInOneLineOfTypePerforation()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.perforation, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.First().Type).Should.BeLogicallyEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.First().StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.First().EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(1000, 0));
        }
        #endregion

        #region CompletelyOverlapping with shorter dominant line
        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalCutLineCompletelyOverlappingANunatabLine_ShouldResultInThreeLinesOfTypeCutNunatabCut()
        {
            var lines = GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.cut, LineType.nunatab);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            var firstCut = new PhysicalLine(
                lines.First().StartCoordinate,
                lines.Skip(1).First().StartCoordinate,
                LineType.cut);
            var nunatabLine = new PhysicalLine(
                lines.Skip(1).First().StartCoordinate,
                lines.Skip(1).First().EndCoordinate,
                LineType.nunatab);
            var secondCut = new PhysicalLine(
                lines.Skip(1).First().EndCoordinate,
                lines.First().EndCoordinate,
                LineType.cut);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(firstCut.Type) && l.StartCoordinate.X == firstCut.StartCoordinate.X
                        && l.StartCoordinate.Y == firstCut.StartCoordinate.Y
                        && l.EndCoordinate.X == firstCut.EndCoordinate.X
                        && l.EndCoordinate.Y == firstCut.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(nunatabLine.Type) && l.StartCoordinate.X == nunatabLine.StartCoordinate.X
                        && l.StartCoordinate.Y == nunatabLine.StartCoordinate.Y && l.EndCoordinate.X == nunatabLine.EndCoordinate.X
                        && l.EndCoordinate.Y == nunatabLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(secondCut.Type) && l.StartCoordinate.X == secondCut.StartCoordinate.X
                        && l.StartCoordinate.Y == secondCut.StartCoordinate.Y
                        && l.EndCoordinate.X == secondCut.EndCoordinate.X
                        && l.EndCoordinate.Y == secondCut.EndCoordinate.Y)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalCutLineCompletelyOverlappingANunatabLine_ShouldResultInThreeLinesOfTypeCutNunatabCut()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.cut, LineType.nunatab);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            var firstCut = new PhysicalLine(
                lines.First().StartCoordinate,
                lines.Skip(1).First().StartCoordinate,
                LineType.cut);
            var nunaLine = new PhysicalLine(
                lines.Skip(1).First().StartCoordinate,
                lines.Skip(1).First().EndCoordinate,
                LineType.nunatab);
            var secondCut = new PhysicalLine(
                lines.Skip(1).First().EndCoordinate,
                lines.First().EndCoordinate,
                LineType.cut);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(firstCut.Type) && l.StartCoordinate.X == firstCut.StartCoordinate.X
                        && l.StartCoordinate.Y == firstCut.StartCoordinate.Y
                        && l.EndCoordinate.X == firstCut.EndCoordinate.X
                        && l.EndCoordinate.Y == firstCut.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(nunaLine.Type) && l.StartCoordinate.X == nunaLine.StartCoordinate.X
                        && l.StartCoordinate.Y == nunaLine.StartCoordinate.Y && l.EndCoordinate.X == nunaLine.EndCoordinate.X
                        && l.EndCoordinate.Y == nunaLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(secondCut.Type) && l.StartCoordinate.X == secondCut.StartCoordinate.X
                        && l.StartCoordinate.Y == secondCut.StartCoordinate.Y
                        && l.EndCoordinate.X == secondCut.EndCoordinate.X
                        && l.EndCoordinate.Y == secondCut.EndCoordinate.Y)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalPerforationLineCompletelyOverlappingACutLine_ShouldResultInThreeLinesOfTypePerforationCutPerforation()
        {
            var lines = GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.perforation, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            var firstPerforation = new PhysicalLine(
                lines.First().StartCoordinate,
                lines.Skip(1).First().StartCoordinate,
                LineType.perforation);
            var cutLine = new PhysicalLine(
                lines.Skip(1).First().StartCoordinate,
                lines.Skip(1).First().EndCoordinate,
                LineType.cut);
            var secondPerforation = new PhysicalLine(
                lines.Skip(1).First().EndCoordinate,
                lines.First().EndCoordinate,
                LineType.perforation);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(firstPerforation.Type) && l.StartCoordinate.X == firstPerforation.StartCoordinate.X
                        && l.StartCoordinate.Y == firstPerforation.StartCoordinate.Y
                        && l.EndCoordinate.X == firstPerforation.EndCoordinate.X
                        && l.EndCoordinate.Y == firstPerforation.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(cutLine.Type) && l.StartCoordinate.X == cutLine.StartCoordinate.X
                        && l.StartCoordinate.Y == cutLine.StartCoordinate.Y && l.EndCoordinate.X == cutLine.EndCoordinate.X
                        && l.EndCoordinate.Y == cutLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(secondPerforation.Type) && l.StartCoordinate.X == secondPerforation.StartCoordinate.X
                        && l.StartCoordinate.Y == secondPerforation.StartCoordinate.Y
                        && l.EndCoordinate.X == secondPerforation.EndCoordinate.X
                        && l.EndCoordinate.Y == secondPerforation.EndCoordinate.Y)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalPerforationLineCompletelyOverlappingACutLine_ShouldResultInThreeLinesOfTypePerforationCutPerforation()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.perforation, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            var firstPerforation = new PhysicalLine(
                lines.First().StartCoordinate,
                lines.Skip(1).First().StartCoordinate,
                LineType.perforation);
            var cutLine = new PhysicalLine(
                lines.Skip(1).First().StartCoordinate,
                lines.Skip(1).First().EndCoordinate,
                LineType.cut);
            var secondPerforation = new PhysicalLine(
                lines.Skip(1).First().EndCoordinate,
                lines.First().EndCoordinate,
                LineType.perforation);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(firstPerforation.Type) && l.StartCoordinate.X == firstPerforation.StartCoordinate.X
                        && l.StartCoordinate.Y == firstPerforation.StartCoordinate.Y
                        && l.EndCoordinate.X == firstPerforation.EndCoordinate.X
                        && l.EndCoordinate.Y == firstPerforation.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(cutLine.Type) && l.StartCoordinate.X == cutLine.StartCoordinate.X
                        && l.StartCoordinate.Y == cutLine.StartCoordinate.Y && l.EndCoordinate.X == cutLine.EndCoordinate.X
                        && l.EndCoordinate.Y == cutLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(secondPerforation.Type) && l.StartCoordinate.X == secondPerforation.StartCoordinate.X
                        && l.StartCoordinate.Y == secondPerforation.StartCoordinate.Y
                        && l.EndCoordinate.X == secondPerforation.EndCoordinate.X
                        && l.EndCoordinate.Y == secondPerforation.EndCoordinate.Y)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalCreaseLineCompletelyOverlappingAPerforationLine_ShouldResultInThreeLinesOfTypeCreasePerforationCrease()
        {
            var lines = GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.crease, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            var firstLine = new PhysicalLine(lines.First().StartCoordinate, lines.Skip(1).First().StartCoordinate, LineType.crease);
            var secondLine = new PhysicalLine(
                lines.Skip(1).First().StartCoordinate,
                lines.Skip(1).First().EndCoordinate,
                LineType.perforation);
            var thirdLine = new PhysicalLine(lines.Skip(1).First().EndCoordinate, lines.First().EndCoordinate, LineType.crease);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(firstLine.Type) && l.StartCoordinate.X == firstLine.StartCoordinate.X
                        && l.StartCoordinate.Y == firstLine.StartCoordinate.Y && l.EndCoordinate.X == firstLine.EndCoordinate.X
                        && l.EndCoordinate.Y == firstLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(secondLine.Type) && l.StartCoordinate.X == secondLine.StartCoordinate.X
                        && l.StartCoordinate.Y == secondLine.StartCoordinate.Y && l.EndCoordinate.X == secondLine.EndCoordinate.X
                        && l.EndCoordinate.Y == secondLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(thirdLine.Type) && l.StartCoordinate.X == thirdLine.StartCoordinate.X
                        && l.StartCoordinate.Y == thirdLine.StartCoordinate.Y && l.EndCoordinate.X == thirdLine.EndCoordinate.X
                        && l.EndCoordinate.Y == thirdLine.EndCoordinate.Y)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalCreaseLineCompletelyOverlappingAPerforationLine_ShouldResultInThreeLinesOfTypeCreasePerforationCrease()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineOneCompletelyOverlapsLineTwo(LineType.crease, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            var firstLine = new PhysicalLine(lines.First().StartCoordinate, lines.Skip(1).First().StartCoordinate, LineType.crease);
            var secondLine = new PhysicalLine(
                lines.Skip(1).First().StartCoordinate,
                lines.Skip(1).First().EndCoordinate,
                LineType.perforation);
            var thirdLine = new PhysicalLine(lines.Skip(1).First().EndCoordinate, lines.First().EndCoordinate, LineType.crease);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(firstLine.Type) && l.StartCoordinate.X == firstLine.StartCoordinate.X
                        && l.StartCoordinate.Y == firstLine.StartCoordinate.Y && l.EndCoordinate.X == firstLine.EndCoordinate.X
                        && l.EndCoordinate.Y == firstLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(secondLine.Type) && l.StartCoordinate.X == secondLine.StartCoordinate.X
                        && l.StartCoordinate.Y == secondLine.StartCoordinate.Y && l.EndCoordinate.X == secondLine.EndCoordinate.X
                        && l.EndCoordinate.Y == secondLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(thirdLine.Type) && l.StartCoordinate.X == thirdLine.StartCoordinate.X
                        && l.StartCoordinate.Y == thirdLine.StartCoordinate.Y && l.EndCoordinate.X == thirdLine.EndCoordinate.X
                        && l.EndCoordinate.Y == thirdLine.EndCoordinate.Y)).Should.BeEqualTo(1);
        }
        #endregion

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalPerforationLineCompletelyOverlappingACutLine_StartingInSamePosition_ShouldResultInTwoLinesOfTypeCutPerforation()
        {
            var lines = GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwoAndStartsAtSamePosition(LineType.perforation, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines).ToList();

            var perforationLine = new PhysicalLine(
                lines.Last().EndCoordinate,
                lines.First().EndCoordinate,
                LineType.perforation);
            var cutLine = new PhysicalLine(
                lines.Last().StartCoordinate,
                lines.Last().EndCoordinate,
                LineType.cut);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(cutLine.Type) && l.StartCoordinate.X == cutLine.StartCoordinate.X
                        && l.StartCoordinate.Y == cutLine.StartCoordinate.Y && l.EndCoordinate.X == cutLine.EndCoordinate.X
                        && l.EndCoordinate.Y == cutLine.EndCoordinate.Y)).Should.BeEqualTo(1);

            Specify.That(
                linesAfterRuleIsApplied.Count(
                    l =>
                        l.Type.Equals(perforationLine.Type) && l.StartCoordinate.X == perforationLine.StartCoordinate.X
                        && l.StartCoordinate.Y == perforationLine.StartCoordinate.Y
                        && l.EndCoordinate.X == perforationLine.EndCoordinate.X
                        && l.EndCoordinate.Y == perforationLine.EndCoordinate.Y)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalPerforationLineThatStartsBeforeACutLineEnds_ShouldStartWhereTheCutLineEnds()
        {
            var lines = GenerateTwoVerticalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.cut, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).EndCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalPerforationLineThatStartsBeforeACutLineEnds_ShouldStartWhereTheCutLineEnds()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.cut, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).EndCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalCreaseLineThatStartsBeforeAPerforationLineEnds_ShouldStartWhereThePerforationLineEnds()
        {
            var lines = GenerateTwoVerticalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.perforation, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalCreaseLineThatStartsBeforeAPerforationLineEnds_ShouldStartWhereThePerforationLineEnds()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.perforation, LineType.crease);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().EndCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalCutLineThatStartsBeforeAPerforationLineEnds_ShouldStartWhereThCutLineStarts()
        {
            var lines = GenerateTwoVerticalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.perforation, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).StartCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalCutLineThatStartsBeforeAPerforationLineEnds_ShouldStartWhereThCutLineStarts()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.perforation, LineType.cut);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).StartCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalPerforationLineThatStartsBeforeACreaseLineEnds_ShouldStartWhereThPerforationLineStarts()
        {
            var lines = GenerateTwoVerticalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.crease, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalPerforationLineThatStartsBeforeACreaseLineEnds_ShouldStartWhereThPerforationLineStarts()
        {
            var lines = GenerateTwoHorizontalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType.crease, LineType.perforation);

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeLogicallyEqualTo(1);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).StartCoordinate).Should.BeLogicallyEqualTo(lines.First().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(lines.Last().StartCoordinate);
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(lines.Last().EndCoordinate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneCreaseLineOverlappedByTwoCutLines()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 500), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 300), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(0, 400), LineType.cut),
            };

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines).ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(4);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 100)).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y == 100 && l.EndCoordinate.Y == 300)).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y == 300 && l.EndCoordinate.Y == 400)).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y == 400 && l.EndCoordinate.Y == 500)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotAddZeroLengthLinesWhenSplittingOverlapped()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 500), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 300), LineType.cut),
            };

            var linesAfterRuleIsApplied = new OverLappingLinesRule().ApplyTo(lines).ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y == 100 && l.EndCoordinate.Y == 300)).Should.BeEqualTo(1);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y == 300 && l.EndCoordinate.Y == 500)).Should.BeEqualTo(1);
        }

        private IEnumerable<PhysicalLine> GenerateTwoVerticalOverlappingLinesOfType(LineType lineOneType, LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(0, 1000),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(0, 1000),
                           lineTwoType)
                   };
        }

        private IEnumerable<PhysicalLine> GenerateTwoHorizontalOverlappingLinesOfType(LineType lineOneType, LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(1000, 0),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(1000, 0),
                           lineTwoType)
                   };
        }

        private IEnumerable<PhysicalLine> GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwo(
            LineType lineOneType,
            LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(0, 1000),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 300),
                           new PhysicalCoordinate(0, 800),
                           lineTwoType)
                   };
        }

        private IEnumerable<PhysicalLine> GenerateTwoVerticalLines_WhereLineOneCompletelyOverlapsLineTwoAndStartsAtSamePosition(
            LineType lineOneType,
            LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(0, 1000),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(0, 800),
                           lineTwoType)
                   };
        }

        private IEnumerable<PhysicalLine> GenerateTwoHorizontalLines_WhereLineOneCompletelyOverlapsLineTwo(
            LineType lineOneType,
            LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(1000, 0),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(300, 0),
                           new PhysicalCoordinate(800, 0),
                           lineTwoType)
                   };
        }

        private IEnumerable<PhysicalLine> GenerateTwoVerticalLines_WhereLineTwoStartsBeforeLineOneEnds(
            LineType lineOneType,
            LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(0, 1000),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 300),
                           new PhysicalCoordinate(0, 1500),
                           lineTwoType)
                   };
        }

        private IEnumerable<PhysicalLine> GenerateTwoHorizontalLines_WhereLineTwoStartsBeforeLineOneEnds(
            LineType lineOneType,
            LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(1000, 0),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(300, 0),
                           new PhysicalCoordinate(1500, 0),
                           lineTwoType)
                   };
        }
    }
}
