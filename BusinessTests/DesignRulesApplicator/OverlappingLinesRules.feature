﻿@DesignRulesApplication
Feature: Rules for overlapping lines
	In order to make sure overlapping lines are handled the right way
	As an API User
	I want to test a design with a lot of overlapping lines

@overlappingLinesTest
Scenario: Remove completely overlapped lines
	Given overlapping lines where one line is completely overlapped by a more dominant line
	And one fefco201 design
	When rules are applied to overlapping design
	And rules are applied to fefco201 design
	Then the overlapped line is removed
	And the fefco201 design should not be affected

@overlappingLinesTest
Scenario: Remove one of two identical lines
	Given two identical overlapping lines
	When rules are applied to overlapping design
	Then one of the identical lines are removed
