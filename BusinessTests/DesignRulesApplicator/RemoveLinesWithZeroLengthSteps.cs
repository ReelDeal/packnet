﻿namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class RemoveLinesWithZeroLengthSteps
    {
        [Given(@"zero length lines")]
        public void GivenZeroLengthLines()
        {
            var design = new PhysicalDesign();
            var line1 = new PhysicalLine(new PhysicalCoordinate(10, 10), 
                new PhysicalCoordinate(10, 10), LineType.cut);

            var line2 = new PhysicalLine(new PhysicalCoordinate(20, 20),
                new PhysicalCoordinate(20, 20), LineType.cut);
            
            design.Add(line1);
            design.Add(line2);

            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"none zero length lines")]
        public void GivenNoneZeroLengthLines()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;

            var line1 = new PhysicalLine(new PhysicalCoordinate(10, 10), 
                new PhysicalCoordinate(20, 40), LineType.cut);

            var line2 = new PhysicalLine(new PhysicalCoordinate(0, 10),
                new PhysicalCoordinate(20, 20), LineType.cut);

            design.Add(line1);
            design.Add(line2);
        }

        [When(@"zero length rule is applied")]
        public void WhenZeroLengthRuleIsApplied()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;
            ScenarioContext.Current.Add("ZeroLengthLineRuleAppliedLines", new ZeroLengthLinesRule().ApplyTo(design.Lines));
        }

        [Then(@"the zero length lines are removed")]
        public void ThenTheZeroLengthLinesAreRemoved()
        {
            var rulesAppliedLines = ScenarioContext.Current["ZeroLengthLineRuleAppliedLines"] as IEnumerable<PhysicalLine>;
            Specify.That(rulesAppliedLines.Count()).Should.BeEqualTo(2);
        }
    }
}
