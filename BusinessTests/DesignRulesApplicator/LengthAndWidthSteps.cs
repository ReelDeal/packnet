﻿using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace BusinessTests.DesignRulesApplicator
{
    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class LengthAndWidthSteps
    {
        [When(@"rules applicator is used")]
        public void WhenRulesApplicatorIsUsed()
        {
            var rulesApplicator = FeatureContext.Current["DesignRulesApplicator"] as IDesignRulesApplicator;
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;

            var rulesAppliedDesign = rulesApplicator.ApplyRules(design, new FusionPerforationLinesRule(design.PerforationParameters), new IqNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0);
            ScenarioContext.Current.Add("RulesAppliedDesign", rulesAppliedDesign);
        }

        [Then(@"the length is the same on the rule applied design as on the measurement applied design")]
        public void ThenTheLengthIsTheSameOnTheRuleAppliedDesignAsOnTheMeasurementAppliedDesign()
        {
            var measurementAppliedDesign = ScenarioContext.Current["Design"] as PhysicalDesign;
            var rulesAppliedDesign = ScenarioContext.Current["RulesAppliedDesign"] as PhysicalDesign;
            Specify.That(rulesAppliedDesign.Length).Should.BeLogicallyEqualTo(measurementAppliedDesign.Length);
        }

        [Then(@"the width is the same on the rule applied design as on the measurement applied design")]
        public void ThenTheWidthIsTheSameOnTheRuleAppliedDesignAsOnTheMeasurementAppliedDesign()
        {
            var measurementAppliedDesign = ScenarioContext.Current["Design"] as PhysicalDesign;
            var rulesAppliedDesign = ScenarioContext.Current["RulesAppliedDesign"] as PhysicalDesign;
            Specify.That(rulesAppliedDesign.Width).Should.BeLogicallyEqualTo(measurementAppliedDesign.Width);
        }
    }
}
