﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;

namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class EMOverallRulesTest
    {
        [TestMethod]
        [TestCategory("Integration")]
        public void SingelOutDesignWithNunatabs_ShouldOnlyHaveCutsLines()
        {
            var design = GetDesignWidthNunaTabsOnAllSides();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 200 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count).Should.BeEqualTo(4);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeEqualTo(4);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldConvertAllNunaTabLinesToCut_WhenUsingWasteCutting_AndRunningSingleOut()
        {
            var design = new PhysicalDesign()
            {
                Length = 200,
                Width = 200
            };
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(200, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(200, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 20), LineType.nunatab));

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = 100,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0,
                Enable = true
            });

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 400 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied.Count).Should.BeEqualTo(6);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeEqualTo(6);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldConvertNunaTabLinesInIntersectionPointToCrease_WhenUsingWasteCutting_AndRunningTiledOut()
        {
            var design = new PhysicalDesign()
            {
                Length = 200,
                Width = 400
            };
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(200, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(200, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 20), LineType.nunatab));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(400, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 200), new PhysicalCoordinate(400, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 20), LineType.nunatab));

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = 100,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0,
                Enable = true
            });

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 600 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied.Count).Should.BeEqualTo(8);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeEqualTo(7);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void TiledDesignWithNunatabs_ShouldOnlyHaveNunatabLinesInIntersectionPointConvertedToCreaseLines()
        {
            var design = GetDesignWidthNunaTabsOnAllSides(2);

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(40), new EmNunatabLinesRule(), new Corrugate { Width = 400 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count).Should.BeEqualTo(8);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut))
                .Should.BeEqualTo(6,
                    "The edges of the box should be 4 cuts, 2 extra cuts in the intersection point of the tile where non nunatabs exists");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeEqualTo(2, "In the tile intersection point the nunatabs should merged and converted to creases");
            Specify.That(
                linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y == 0 && l.EndCoordinate.Y == 80))
                .Should.BeEqualTo(1,
                    "Two of the nunatabs from the original designs in the intersection point should have been merged and converted into on crease");
            Specify.That(
                linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y == 140 && l.EndCoordinate.Y == 170))
                .Should.BeEqualTo(1,
                    "The two last nunatabs from the original designs in the intersection point should have been merged and converted into on crease");
            Specify.That(
                linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y == 80 && l.EndCoordinate.Y == 140))
                .Should.BeEqualTo(1,
                    "The gap between the nuna tabs in the designs should be a cut line");
            Specify.That(
                linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y == 170 && l.EndCoordinate.Y == 200))
                .Should.BeEqualTo(1,
                    "The end of the line after the nunatabs should be a cut line");
        }

        private PhysicalDesign GetDesignWidthNunaTabsOnAllSides(int tileCount = 1)
        {
            /*
             *      0                    200
             *  0    __-________-______-___
             *      {                      |
             *      |                      }
             *      |                      }
             *      |                      |
             *      {                      }
             * 200  |-____________________-|
             * 
             */

            var design = new PhysicalDesign()
            {
                Length = 200,
                Width = 200 * tileCount
            };

            for (var i = 0; i < tileCount; i++)
            {
                design.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 0), new PhysicalCoordinate(20 + (200 * i), 0), LineType.cut));
                design.Add(new PhysicalLine(new PhysicalCoordinate(20 + (200 * i), 0), new PhysicalCoordinate(40 + (200 * i), 0), LineType.nunatab));
                design.Add(new PhysicalLine(new PhysicalCoordinate(40 + (200 * i), 0), new PhysicalCoordinate(170 + (200 * i), 0), LineType.cut));
                design.Add(new PhysicalLine(new PhysicalCoordinate(130 + (200 * i), 0), new PhysicalCoordinate(140 + (200 * i), 0), LineType.nunatab));
                design.Add(new PhysicalLine(new PhysicalCoordinate(170 + (200 * i), 0), new PhysicalCoordinate(180 + (200 * i), 0), LineType.nunatab));
                design.Add(new PhysicalLine(new PhysicalCoordinate(180 + (200 * i), 0), new PhysicalCoordinate(200 + (200 * i), 0), LineType.cut));

                design.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 200), new PhysicalCoordinate(190 + (200 * i), 200), LineType.cut));
                design.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 200), new PhysicalCoordinate(30 + (200 * i), 200), LineType.nunatab));
                design.Add(new PhysicalLine(new PhysicalCoordinate(190 + (200 * i), 200), new PhysicalCoordinate(200 + (200 * i), 200), LineType.nunatab));

                design.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 0), new PhysicalCoordinate(0 + (200 * i), 200), LineType.cut));
                design.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 0), new PhysicalCoordinate(0 + (200 * i), 20), LineType.nunatab));
                design.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 150), new PhysicalCoordinate(0 + (200 * i), 170), LineType.nunatab));

                design.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 0), new PhysicalCoordinate(200 + (200 * i), 20), LineType.cut));
                design.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 20), new PhysicalCoordinate(200 + (200 * i), 80), LineType.nunatab));
                design.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 80), new PhysicalCoordinate(200 + (200 * i), 200), LineType.cut));
                design.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 140), new PhysicalCoordinate(200 + (200 * i), 160), LineType.nunatab));
            };

            return design;
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void OverallRulesTestOneVerticalLine_ShouldReturnOneVerticalLineWithBothCutAndCrease()
        {
            var lines = new List<PhysicalLine>()
            {
                /*Expected output: Crease:0-550, Cut:550-950, Crease:950-1500, Cut:1500-2500, Crease:2500-3000*/
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 1500), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(0, 800), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(0, 800), LineType.crease),//overlapping lines
                new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(0, 800), LineType.cut), //zero length line
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 2000), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 2000), LineType.crease),//completely overlapping lines
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 2000), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 1900), new PhysicalCoordinate(0, 2500), LineType.cut), //Merge connected cut line
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 3000), LineType.crease), //Merge with overlapping cut line
            };

            var design = new PhysicalDesign();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });

            lines.ForEach(design.Add);

            design.Length = 3000d;

            design.Width = 100;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count).Should.BeLogicallyEqualTo(5);

            Specify.That(linesAfterRuleIsApplied.ElementAt(0).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 400));

            Specify.That(linesAfterRuleIsApplied.ElementAt(1).Type).Should.BeEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 400));
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1100));

            Specify.That(linesAfterRuleIsApplied.ElementAt(2).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1100));
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1500));

            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.Y.Equals(1500) && l.EndCoordinate.Y.Equals(2500))).Should.BeLogicallyEqualTo(1, "Expected one line to start at 1500");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.Y.Equals(1500)).Type).Should.BeLogicallyEqualTo(LineType.cut, "Expected cut line");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.Y.Equals(2500) && l.EndCoordinate.Y.Equals(3000))).Should.BeLogicallyEqualTo(1, "Expected one line to start at 2000");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.Y.Equals(2500)).Type).Should.BeLogicallyEqualTo(LineType.crease, "Expected crease line");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void FourVerticalLinesFourRulesOverallTest()
        {
            var lines = new List<PhysicalLine>()
            {
                /*Expected output: X = 0: Crease:0-550, Cut:550-950, Crease:950-1500
                 *                 X = 200: Cut:500-1500, Crease:1500-2000
                 *                 X = 300: Cut:0-2000, Crease:2000-4000
                 */
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 1500), LineType.perforation),//perforation lines converting
                
                new PhysicalLine(new PhysicalCoordinate(100, 1000), new PhysicalCoordinate(100, 1000), LineType.cut), //zero length line

                new PhysicalLine(new PhysicalCoordinate(200, 500), new PhysicalCoordinate(200, 1000), LineType.cut),
                
                new PhysicalLine(new PhysicalCoordinate(200, 600), new PhysicalCoordinate(200, 1500), LineType.cut),

                new PhysicalLine(new PhysicalCoordinate(200, 600), new PhysicalCoordinate(200, 2000), LineType.crease),//Completely overlapped by other lines

                new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 1000), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 1000), new PhysicalCoordinate(300, 2000), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 2000), new PhysicalCoordinate(300, 3000), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(300, 3000), new PhysicalCoordinate(300, 4000), LineType.crease),//Merge the connected same type line
            };

            var design = new PhysicalDesign();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 4000;

            design.Width = 300;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(100), new EmNunatabLinesRule(), new Corrugate { Width = 300 }, OrientationEnum.Degree0).Lines.ToList();
            
            Specify.That(linesAfterRuleIsApplied.Count).Should.BeLogicallyEqualTo(7);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 0)).Should.BeLogicallyEqualTo(3);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 100)).Should.BeLogicallyEqualTo(0);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 200)).Should.BeLogicallyEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 300)).Should.BeLogicallyEqualTo(2);

            Specify.That(linesAfterRuleIsApplied.ElementAt(0).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 100));

            Specify.That(linesAfterRuleIsApplied.ElementAt(1).Type).Should.BeEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 100));
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1400));

            Specify.That(linesAfterRuleIsApplied.ElementAt(2).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1400));
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1500));

            Specify.That(linesAfterRuleIsApplied.ElementAt(3).EndCoordinate.Y).Should.BeLogicallyEqualTo(1500d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(3).Type).Should.BeLogicallyEqualTo(LineType.cut);

            Specify.That(linesAfterRuleIsApplied.ElementAt(4).EndCoordinate.Y).Should.BeLogicallyEqualTo(2000d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(4).Type).Should.BeLogicallyEqualTo(LineType.crease);

            Specify.That(linesAfterRuleIsApplied.ElementAt(5).EndCoordinate.Y).Should.BeLogicallyEqualTo(2000d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(5).Type).Should.BeLogicallyEqualTo(LineType.cut);

            Specify.That(linesAfterRuleIsApplied.ElementAt(6).EndCoordinate.Y).Should.BeLogicallyEqualTo(4000d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(6).Type).Should.BeLogicallyEqualTo(LineType.crease);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndMergeWithConnectedCreases()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1300), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 1300), new PhysicalCoordinate(0, 1400), LineType.crease),
            };

            var design = new PhysicalDesign();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
            });
            lines.ForEach(design.Add);

            design.Length = 3000d;

            design.Width = 100;
            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(0) && l.EndCoordinate.Y.Equals(500)))
                .Should.BeLogicallyEqualTo(1, "Crease part in begining of perforation to be merged with previous crease line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.Y.Equals(500) && l.EndCoordinate.Y.Equals(900)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 500 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(900) && l.EndCoordinate.Y.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Crease part in end of perforation to be merged with next crease line");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndShouldNotMergeWithConnectedCuts()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 100), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1300), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 1300), new PhysicalCoordinate(0, 1400), LineType.cut),
            };

            var design = new PhysicalDesign();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 3000d;

            design.Width = 100;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(5);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y.Equals(0) && l.EndCoordinate.Y.Equals(100)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(100) && l.EndCoordinate.Y.Equals(500)))
                .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.Y.Equals(500) && l.EndCoordinate.Y.Equals(900)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 500 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(900) && l.EndCoordinate.Y.Equals(1300)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y.Equals(1300) && l.EndCoordinate.Y.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndShouldNotMergeWithCreasesIfCutLineInterceptsConnection()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(100, 0), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(200, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(1400, 0), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(1400, 0), new PhysicalCoordinate(1500, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(1500, 0), new PhysicalCoordinate(1600, 0), LineType.crease),
            };

            var design = new PhysicalDesign();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 100;

            design.Width = 1600;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1600 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(7);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(0) && l.EndCoordinate.X.Equals(100)))
                .Should.BeLogicallyEqualTo(1, "Crease with cut intercepting connection to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.X.Equals(100) && l.EndCoordinate.X.Equals(200)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(200) && l.EndCoordinate.X.Equals(600)))
                .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(600) && l.EndCoordinate.X.Equals(1000)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 600 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1000) && l.EndCoordinate.X.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.X.Equals(1400) && l.EndCoordinate.X.Equals(1500)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1500) && l.EndCoordinate.X.Equals(1600)))
                .Should.BeLogicallyEqualTo(1, "Crease with cut intercepting connection to perforation should not be merged");
        }


        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndShouldNotMergeWithCreasesIfNotDirectlyConnectedToPerforation()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(100, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(1400, 100), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(1500, 100), new PhysicalCoordinate(1600, 100), LineType.crease),
            };

            var design = new PhysicalDesign();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 100;

            design.Width = 1600;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1600 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(5);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(0) && l.EndCoordinate.X.Equals(100)))
                .Should.BeLogicallyEqualTo(1, "Crease not directly connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(200) && l.EndCoordinate.X.Equals(600)))
                .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(600) && l.EndCoordinate.X.Equals(1000)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 600 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1000) && l.EndCoordinate.X.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1500) && l.EndCoordinate.X.Equals(1600)))
                .Should.BeLogicallyEqualTo(1, "Crease not directly connected to perforation should not be merged");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void TwoPerforationLinesShouldBeConvertedIntoCutsAndCreases_AndShouldMergeWithCreasesInTheMiddle()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(1200, 100), new PhysicalCoordinate(2000, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(1200, 100), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(2000, 100), new PhysicalCoordinate(3200, 100), LineType.perforation),
            };

            var design = new PhysicalDesign();

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 100;

            design.Width = 3200;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 3200 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(5);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(0) && l.EndCoordinate.X.Equals(400)))
                        .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(400) && l.EndCoordinate.X.Equals(800)))
                .Should.BeLogicallyEqualTo(1, "perforation part of first perforation should be from 400 to 800");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(800) && l.EndCoordinate.X.Equals(2400)))
                .Should.BeLogicallyEqualTo(1, "End crease part of two preforation lines should be merged with the crease line in the middle");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(2400) && l.EndCoordinate.X.Equals(2800)))
                .Should.BeLogicallyEqualTo(1, "perforation part of the second perforation should be from 2400 to 2800");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(2800) && l.EndCoordinate.X.Equals(3200)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldAddWasteCuttingLinesTo201WithGlueFlap()
        {
            /*
               0,  150      400  550 1000
             0       _______
            50  ____|_ _ _ _|____     |
           250 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1000 |_ _ ¦_ _ _ _¦_ _ |    |
          1200 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1950 |____¦_______¦____|    | */

            var design = new PhysicalDesign()
            {
                Length = 1950,
                Width = 550
            };


            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(400, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(150, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(400, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(550, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(550, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1000), new PhysicalCoordinate(550, 1000), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1200), new PhysicalCoordinate(550, 1200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1950), new PhysicalCoordinate(550, 1950), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(0, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 1950), LineType.cut));

            var designApp = new DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = 600,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0,
                Enable = true
            });
            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(12+6/*6 waste cutting lines added*/);
            
        }

        private IEnumerable<PhysicalLine> GenerateTwoHorizontalLines_WhereLineTwoStartsBeforeLineOneEnds(LineType lineOneType, LineType lineTwoType)
        {
            return new List<PhysicalLine>()
                   {
                       new PhysicalLine(
                           new PhysicalCoordinate(0, 0),
                           new PhysicalCoordinate(1000, 0),
                           lineOneType),
                       new PhysicalLine(
                           new PhysicalCoordinate(300, 0),
                           new PhysicalCoordinate(1500, 0),
                           lineTwoType)
                   };
        }

    }
}
