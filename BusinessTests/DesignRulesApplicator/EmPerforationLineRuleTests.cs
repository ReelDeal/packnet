﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

using Testing.Specificity;

namespace BusinessTests.DesignRulesApplicator
{
    [TestClass]
    public class EmPerforationLineRuleTests
    {
        [TestMethod]
        public void ShouldConvertPerforationLineInto2CreasesAndAPerforationPart()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 500), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(500, 0), LineType.perforation)
            };

            var ruleAppliedLines = new EmPerforationLineRule(100).ApplyTo(lines);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(6);

            Specify.That(ruleAppliedLines.ElementAt(0).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(ruleAppliedLines.ElementAt(0).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(ruleAppliedLines.ElementAt(0).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 100));

            Specify.That(ruleAppliedLines.ElementAt(1).Type).Should.BeEqualTo(LineType.perforation);
            Specify.That(ruleAppliedLines.ElementAt(1).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 100));
            Specify.That(ruleAppliedLines.ElementAt(1).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 400));

            Specify.That(ruleAppliedLines.ElementAt(2).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(ruleAppliedLines.ElementAt(2).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 400));
            Specify.That(ruleAppliedLines.ElementAt(2).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 500));


            Specify.That(ruleAppliedLines.ElementAt(3).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(ruleAppliedLines.ElementAt(3).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(ruleAppliedLines.ElementAt(3).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(100, 0));

            Specify.That(ruleAppliedLines.ElementAt(4).Type).Should.BeEqualTo(LineType.perforation);
            Specify.That(ruleAppliedLines.ElementAt(4).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(100, 0));
            Specify.That(ruleAppliedLines.ElementAt(4).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(400, 0));

            Specify.That(ruleAppliedLines.ElementAt(5).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(ruleAppliedLines.ElementAt(5).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(400, 0));
            Specify.That(ruleAppliedLines.ElementAt(5).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(500, 0));
        }
    }
}
