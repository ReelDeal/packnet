﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;

using Testing.Specificity;

namespace BusinessTests.DesignRulesApplicator
{
    [TestClass]
    public class WasteAreaFinderTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetMergedWasteAreasFromAllSides()
        {
            /*
                0, 50   950 1000   1200
              0     _____
             50  __|     |__          |
                |           |         |
                |           |         |
            550 |__       __|         |
            600    |_____|            |*/

            var corrugateWidth = new MicroMeter(1200d);
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(0, 550), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(950, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(50, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(950, 50), new PhysicalCoordinate(1000, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(950, 0), new PhysicalCoordinate(950, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(1000, 50), new PhysicalCoordinate(1000, 550), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 550), new PhysicalCoordinate(50, 550), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 550), new PhysicalCoordinate(50, 600), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(950, 550), new PhysicalCoordinate(1000, 550), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(950, 550), new PhysicalCoordinate(950, 600), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 600), new PhysicalCoordinate(950, 600), LineType.cut),
            };
            var design = new PhysicalDesign(lines);
            design.Length = 600;
            design.Width = 1000;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 1000, MinimumWasteWidth = 0, MinimumWasteLength = 25 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
            {
                Console.WriteLine("Found: " + area);
            }

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(5);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 50 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 950 && a.TopLeft.Y == 0 && a.BottomRight.X == 1200 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 1000 && a.TopLeft.Y == 50 && a.BottomRight.X == 1200 && a.BottomRight.Y == 550)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 550 && a.BottomRight.X == 50 && a.BottomRight.Y == 600)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 950 && a.TopLeft.Y == 550 && a.BottomRight.X == 1200 && a.BottomRight.Y == 600)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindWasteAreasForDeepSideWasteWithDifferentWidth()
        {
            /*
                0, 50  100  200   300   500
              0  ____              _______ 
             50 |    |__          |       |
            100 |       |     ____|       |
            120 |       |____|            |
                |                         |
            550 |                         |
            600 |_________________________|*/

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(50, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 50), new PhysicalCoordinate(100, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(100, 50), new PhysicalCoordinate(100, 120), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(100, 120), new PhysicalCoordinate(200, 120), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(200, 120), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(300, 100), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 100), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(500, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(500, 0), new PhysicalCoordinate(500, 600), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(500, 600), LineType.cut)
            };
            var corrugateWidth = 500;
            var design = new PhysicalDesign(lines);
            design.Width = corrugateWidth;
            design.Length = 600;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 1000, MinimumWasteWidth = 0, MinimumWasteLength = 20 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(3);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 50 && a.TopLeft.Y == 0 && a.BottomRight.X == 300 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 100 && a.TopLeft.Y == 50 && a.BottomRight.X == 300 && a.BottomRight.Y == 100)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 100 && a.TopLeft.Y == 100 && a.BottomRight.X == 200 && a.BottomRight.Y == 120)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindWasteAreasInWeirdDesign()
        {
            /*
                0, 100 150  250    400
              0               _________ 
             50       __     |         |____
            100      |  |    |              |
            150  ____|  |    |              |
            200 |       |____|              |
                |                           |
            300 |___________________________| */

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(0, 300), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(100, 150), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(100, 50), new PhysicalCoordinate(100, 150), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(100, 50), new PhysicalCoordinate(150, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(150, 200), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(150, 200), new PhysicalCoordinate(250, 200), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(250, 0), new PhysicalCoordinate(250, 200), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(250, 0), new PhysicalCoordinate(400, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(500, 50), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(500, 50), new PhysicalCoordinate(500, 300), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(500, 300), LineType.cut)
            };
            var corrugateWidth = 500;
            var design = new PhysicalDesign(lines);
            design.Width = 500;
            design.Length = 300;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 1000, MinimumWasteWidth = 0, MinimumWasteLength = 25 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);
            
            Specify.That(wasteAreas.Count()).Should.BeEqualTo(4);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 250 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 400 && a.TopLeft.Y == 0 && a.BottomRight.X == 500 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 50 && a.BottomRight.X == 100 && a.BottomRight.Y == 150)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 150 && a.TopLeft.Y == 50 && a.BottomRight.X == 250 && a.BottomRight.Y == 200)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindWasteOn201WithGlueFlap()
        {
            /*
                0,  50       150  200  250
              0       _______
             25  ____|_ _ _ _|____     |
                |    ¦       ¦    |    |
             50 |_ _ ¦_ _ _ _¦_ _ |    |
                |    ¦       ¦    |    |
            100 |_ _ ¦_ _ _ _¦_ _ |    |
                |    ¦       ¦    |    |
            150 |_ _ ¦_ _ _ _¦_ _ |    |
                |    ¦       ¦    |    |
            200 |____¦_______¦____|    | */


            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(150, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(150, 25), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(200, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(0, 200), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(50, 200), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(150, 200), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 25), new PhysicalCoordinate(200, 200), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(200, 50), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(200, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(200, 150), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(200, 200), LineType.cut)
            };
            var corrugateWidth = 250;
            var design = new PhysicalDesign(lines);
            design.Length = 200;
            design.Width = 200;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 1000, MinimumWasteWidth = 0, MinimumWasteLength = 25 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(3);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 50 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 150 && a.TopLeft.Y == 0 && a.BottomRight.X == 250 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 25 && a.BottomRight.X == 250 && a.BottomRight.Y == 200)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindAndSplitLongWasteOn201WithGlueFlap()
        {
            /*
               0,  50       150  200  250
             0       _______
            25  ____|_ _ _ _|____     |
               |    ¦       ¦    |    |
            50 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           100 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           150 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           200 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           250 |____¦_______¦____|    | */

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(150, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(150, 25), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(200, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(0, 250), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(50, 250), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(150, 250), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 25), new PhysicalCoordinate(200, 250), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(200, 50), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(200, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(200, 150), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(200, 200), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(200, 250), LineType.cut)
            };
            var corrugateWidth = 250;
            var design = new PhysicalDesign(lines);
            design.Length = 250;
            design.Width = 200;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 200, MinimumWasteWidth = 0, MinimumWasteLength = 25 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(4);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 50 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 150 && a.TopLeft.Y == 0 && a.BottomRight.X == 250 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 25 && a.BottomRight.X == 250 && a.BottomRight.Y == 200)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 200 && a.BottomRight.X == 250 && a.BottomRight.Y == 250)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotGenerateWasteThatIsTooShort()
        {
            /*
               0,  50       150  200  250
             0  _________________     |
               |    ¦       ¦    |    |
           50  |    ¦       ¦    |    |
               |    ¦       ¦    |    |
           100 |    ¦       ¦    |    |
               |    ¦       ¦    |    |
           150 |    ¦       ¦    |    |
               |    ¦       ¦    |    |
           200 |    ¦       ¦    |    |
           215 |____¦_______¦____|    | */

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(200, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 215), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 215), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 215), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 215), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 215), new PhysicalCoordinate(200, 215), LineType.cut)
            };

            var corrugateWidth = 250;
            var design = new PhysicalDesign(lines);
            design.Length = 215;
            design.Width = 200;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 200, MinimumWasteWidth = 0, MinimumWasteLength = 25 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(2);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 0 && a.BottomRight.X == 250 && a.BottomRight.Y == 107.5)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 107.5 && a.BottomRight.X == 250 && a.BottomRight.Y == 215)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindAndSplitWideWasteOn201WithGlueFlap()
        {
            /*
               0,  50       150  200  250
             0       _______
            25  ____|_ _ _ _|____     |
               |    ¦       ¦    |    |
            50 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           100 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           150 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           200 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
           250 |____¦_______¦____|    | */

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(150, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(150, 25), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(200, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(0, 250), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(50, 250), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(150, 250), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 25), new PhysicalCoordinate(200, 250), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(200, 50), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(200, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(200, 150), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(200, 200), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(200, 250), LineType.cut)
            };
            var corrugateWidth = 250;
            var design = new PhysicalDesign(lines);
            design.Length = 250;
            design.Width = 200;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 30, MaximumWasteLength = 1000, MinimumWasteWidth = 0, MinimumWasteLength = 0 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(8);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 30 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 30 && a.TopLeft.Y == 0 && a.BottomRight.X == 50 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 150 && a.TopLeft.Y == 0 && a.BottomRight.X == 180 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 180 && a.TopLeft.Y == 0 && a.BottomRight.X == 200 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 0 && a.BottomRight.X == 230 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 230 && a.TopLeft.Y == 0 && a.BottomRight.X == 250 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 25 && a.BottomRight.X == 230 && a.BottomRight.Y == 250)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 230 && a.TopLeft.Y == 25 && a.BottomRight.X == 250 && a.BottomRight.Y == 250)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindWasteOn201WithGlueFlap_WithoutSplittingWasteAreas()
        {
            /*
               0,  150      400  550 1000
             0       _______
            50  ____|_ _ _ _|____     |
           250 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1000 |_ _ ¦_ _ _ _¦_ _ |    |
          1200 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1950 |____¦_______¦____|    | */

            var design = new PhysicalDesign()
            {
                Length = 1950,
                Width = 550
            };


            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(400, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(150, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(400, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(550, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(550, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1000), new PhysicalCoordinate(550, 1000), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1200), new PhysicalCoordinate(550, 1200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1950), new PhysicalCoordinate(550, 1950), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(0, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 1950), LineType.cut));
            var corrugateWidth = 1000;

            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(3);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 150 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 400 && a.TopLeft.Y == 0 && a.BottomRight.X == 1000 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 50 && a.BottomRight.X == 1000 && a.BottomRight.Y == 1950)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindWasteOn201WithGlueFlap_AndSplittingWasteAreas()
        {
            /*
               0,  150      400  550 1000
             0       _______
            50  ____|_ _ _ _|____     |
           250 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1000 |_ _ ¦_ _ _ _¦_ _ |    |
          1200 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1950 |____¦_______¦____|    | */

            var design = new PhysicalDesign()
            {
                Length = 1950,
                Width = 550
            };


            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(400, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(150, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(400, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(550, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(550, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1000), new PhysicalCoordinate(550, 1000), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1200), new PhysicalCoordinate(550, 1200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1950), new PhysicalCoordinate(550, 1950), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(0, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 1950), LineType.cut));
            var corrugateWidth = 1000;

            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, new WasteAreaParameters()
            {
                MaximumWasteLength = 600,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(7);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 150 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 400 && a.TopLeft.Y == 0 && a.BottomRight.X == 1000 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 50 && a.BottomRight.X == 1000 && a.BottomRight.Y == 250)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 250 && a.BottomRight.X == 1000 && a.BottomRight.Y == 850)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 850 && a.BottomRight.X == 1000 && a.BottomRight.Y == 1200)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 1200 && a.BottomRight.X == 1000 && a.BottomRight.Y == 1800)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 1800 && a.BottomRight.X == 1000 && a.BottomRight.Y == 1950)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotGenerateWasteThatIsTooNarrow()
        {
            /*
               0,  50       150  225  250
             0                      ____    
           25   ___________________|    |
               |                        |
           50  |                        |
               |                        |
           100 |                        |
               |                        |
           150 |                        |
               |                        |
           200 |                        |
           215 |________________________| */

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(225, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(225, 0), new PhysicalCoordinate(225, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(225, 0), new PhysicalCoordinate(250, 0), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(0, 215), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(250, 0), new PhysicalCoordinate(250, 215), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 215), new PhysicalCoordinate(200, 215), LineType.cut)
            };

            var corrugateWidth = 250;
            var design = new PhysicalDesign(lines);
            design.Length = 215;
            design.Width = 250;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 100, MaximumWasteLength = 100, MinimumWasteWidth = 30, MinimumWasteLength = 0 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(3);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 100 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 100 && a.TopLeft.Y == 0 && a.BottomRight.X == 162.5 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 162.5 && a.TopLeft.Y == 0 && a.BottomRight.X == 225 && a.BottomRight.Y == 25)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSplitWasteThatIsTooWideAndTooLong()
        {
            /*
               0,     100       200   300    415
             0  _________________             |
               |                 |            |
           100 |                 |            |
               |                 |            |
               |                 |            |
           200 |                 |            |
               |                 |            |
               |                 |            |
           300 |                 |            | 
               |                 |            |
               |                 |            |
           415 |_________________|            | */

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(200, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 415), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(200, 415), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 415), new PhysicalCoordinate(200, 415), LineType.cut)
            };

            var corrugateWidth = 415;
            var design = new PhysicalDesign(lines);
            design.Length = 415;
            design.Width = 200;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 100, MaximumWasteLength = 200, MinimumWasteWidth = 25, MinimumWasteLength = 25 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(9);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 0 && a.BottomRight.X == 300 && a.BottomRight.Y == 200)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 300 && a.TopLeft.Y == 0 && a.BottomRight.X == 357.5 && a.BottomRight.Y == 200)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 357.5 && a.TopLeft.Y == 0 && a.BottomRight.X == 415 && a.BottomRight.Y == 200)).Should.BeEqualTo(1);

            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 200 && a.BottomRight.X == 300 && a.BottomRight.Y == 307.5)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 300 && a.TopLeft.Y == 200 && a.BottomRight.X == 357.5 && a.BottomRight.Y == 307.5)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 357.5 && a.TopLeft.Y == 200 && a.BottomRight.X == 415 && a.BottomRight.Y == 307.5)).Should.BeEqualTo(1);

            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 307.5 && a.BottomRight.X == 300 && a.BottomRight.Y == 415)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 300 && a.TopLeft.Y == 307.5 && a.BottomRight.X == 357.5 && a.BottomRight.Y == 415)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 357.5 && a.TopLeft.Y == 307.5 && a.BottomRight.X == 415 && a.BottomRight.Y == 415)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFindWasteOn201WithGlueFlapAndCutLines_WithoutSplittingWasteAreas()
        {
            /*
               0,  150      400  550 1000
             0       _______
            50  ____|_ _ _ _|____     |
           250 |____¦_ _ _ _¦____|    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1000 |____¦_ _ _ _¦____|    |
          1200 |____¦_ _ _ _¦____|    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1950 |____¦_______¦____|    | */

            var design = new PhysicalDesign()
            {
                Length = 1950,
                Width = 550
            };


            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(400, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(150, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 50), new PhysicalCoordinate(400, 50), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 50), new PhysicalCoordinate(550, 50), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(150, 250), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 250), new PhysicalCoordinate(400, 250), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 250), new PhysicalCoordinate(550, 250), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1000), new PhysicalCoordinate(150, 1000), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 1000), new PhysicalCoordinate(400, 1000), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 1000), new PhysicalCoordinate(550, 1000), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1200), new PhysicalCoordinate(150, 1200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 1200), new PhysicalCoordinate(400, 1200), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 1200), new PhysicalCoordinate(550, 1200), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1950), new PhysicalCoordinate(550, 1950), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(0, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 1950), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 0), new PhysicalCoordinate(550, 1950), LineType.cut));
            var corrugateWidth = 1000;

            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(3);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 150 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 400 && a.TopLeft.Y == 0 && a.BottomRight.X == 1000 && a.BottomRight.Y == 50)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 50 && a.BottomRight.X == 1000 && a.BottomRight.Y == 1950)).Should.BeEqualTo(1);
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDecreaseWidthOnLeftSideOfTooShortWasteAreas_ToAllowForMergingWithOtherWasteAreas()
        {
            /*
               0,  150      400  550 700
             0  ____________
            90 |    ¦       |____     |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
           590 |    ¦       ¦____|    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
           990 |    ¦       ¦____|    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1500 |____¦_______¦____|    | */

            var design = new PhysicalDesign()
            {
                Length = 1500,
                Width = 550
            };


            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(400, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(400, 90), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 90), new PhysicalCoordinate(550, 90), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 90), new PhysicalCoordinate(550, 1500), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(550, 1500), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 1500), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 1500), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 90), new PhysicalCoordinate(400, 1500), LineType.crease));

            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 90), new PhysicalCoordinate(550, 90), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 590), new PhysicalCoordinate(550, 590), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(400, 990), new PhysicalCoordinate(550, 990), LineType.cut));


            var corrugateWidth = 700;

            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, new WasteAreaParameters()
            {
                MaximumWasteLength = 500,
                MinimumWasteLength = 100,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0,
                Enable = true
            });

            foreach (var wasteArea in wasteAreas)
                Console.WriteLine(wasteArea);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(4);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 0 && a.BottomRight.X == 700 && a.BottomRight.Y == 500)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 500 && a.BottomRight.X == 700 && a.BottomRight.Y == 990)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 990 && a.BottomRight.X == 700 && a.BottomRight.Y == 1245)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 550 && a.TopLeft.Y == 1245 && a.BottomRight.X == 700 && a.BottomRight.Y == 1500)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDecreaseWidthOnRightSideOfTooShortWasteAreas_ToAllowForMergingWithOtherWasteAreas()
        {
            /* Flipped
               0,  150      400  550 700
             0  ____________
            90 |    ¦       |____     |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
           590 |    ¦       ¦____|    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
           990 |    ¦       ¦____|    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
               |    ¦       ¦    |    |
          1500 |____¦_______¦____|    | */

            var design = new PhysicalDesign()
            {
                Length = 1500,
                Width = 550
            };


            design.Add(new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(700, 0), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 90), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 90), new PhysicalCoordinate(300, 90), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 90), new PhysicalCoordinate(150, 1500), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 1500), new PhysicalCoordinate(700, 1500), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(700, 0), new PhysicalCoordinate(700, 1500), LineType.cut));

            design.Add(new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 1500), LineType.crease));
            design.Add(new PhysicalLine(new PhysicalCoordinate(550, 90), new PhysicalCoordinate(550, 1500), LineType.crease));

            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 90), new PhysicalCoordinate(300, 90), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 590), new PhysicalCoordinate(300, 590), LineType.cut));
            design.Add(new PhysicalLine(new PhysicalCoordinate(150, 990), new PhysicalCoordinate(300, 990), LineType.cut));


            var corrugateWidth = 700;

            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, new WasteAreaParameters()
            {
                MaximumWasteLength = 500,
                MinimumWasteLength = 100,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0,
                Enable = true
            });

            foreach (var wasteArea in wasteAreas)
                Console.WriteLine(wasteArea);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(4);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 0 && a.BottomRight.X == 150 && a.BottomRight.Y == 500)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 500 && a.BottomRight.X == 150 && a.BottomRight.Y == 990)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 990 && a.BottomRight.X == 150 && a.BottomRight.Y == 1245)).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 0 && a.TopLeft.Y == 1245 && a.BottomRight.X == 150 && a.BottomRight.Y == 1500)).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDecreaseWidthFromBothSides_OnTooShortWasteArea_ToAllowForMerging()
        {
            /*
                0,  50   200  300  450     500
              0  ____                _______ 
             20 |    |____      ____|       |
                |         |    |            |
            150 |         |____|            |
                |                           |
                |                           |
            600 |___________________________|*/

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 600), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(50, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 20), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 20), new PhysicalCoordinate(200, 20), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(200, 20), new PhysicalCoordinate(200, 150), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(200, 150), new PhysicalCoordinate(300, 150), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 20), new PhysicalCoordinate(300, 150), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 20), new PhysicalCoordinate(450, 20), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(450, 0), new PhysicalCoordinate(450, 20), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(450, 0), new PhysicalCoordinate(500, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(500, 0), new PhysicalCoordinate(500, 600), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 600), new PhysicalCoordinate(500, 600), LineType.cut)
            };
            var corrugateWidth = 500;
            var design = new PhysicalDesign(lines);
            design.Width = corrugateWidth;
            design.Length = 600;

            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 1000, MaximumWasteLength = 1000, MinimumWasteWidth = 0, MinimumWasteLength = 30 };
            var wasteAreas = WasteAreaFinder.GetWasteAreasForPhysicalDesign(design.Lines, design.Length, corrugateWidth, wasteParams);

            foreach (var area in wasteAreas)
                Console.WriteLine(area);

            Specify.That(wasteAreas.Count()).Should.BeEqualTo(1);
            Specify.That(wasteAreas.Count(a => a.TopLeft.X == 200 && a.TopLeft.Y == 0 && a.BottomRight.X == 300 && a.BottomRight.Y == 150)).Should.BeEqualTo(1);
        }

    }
}

