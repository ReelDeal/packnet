﻿namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class MergeConnectedAndEqualLinesSteps
    {
        [Given(@"lines of same type")]
        public void GivenLinesOfSameType()
        {
            var design = new PhysicalDesign();
            var line1 = new PhysicalLine(new PhysicalCoordinate(0, 0), 
                new PhysicalCoordinate(0, 0), LineType.cut);
            var line2 = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(0, 0), LineType.cut);
            design.Add(line1);
            design.Add(line2);

            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"and start and end of the lines are the same coordinate")]
        public void GivenAndStartAndEndOfTheLinesAreTheSameCoordinate()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;
            design.Lines.ElementAt(0).StartCoordinate.X = 0;
            design.Lines.ElementAt(0).StartCoordinate.Y = 10;
            design.Lines.ElementAt(0).EndCoordinate.X = 10;
            design.Lines.ElementAt(0).EndCoordinate.Y = 10;

            design.Lines.ElementAt(1).StartCoordinate.X = 10;
            design.Lines.ElementAt(1).StartCoordinate.Y = 10;
            design.Lines.ElementAt(1).EndCoordinate.X = 20;
            design.Lines.ElementAt(1).EndCoordinate.Y = 10;
        }

        [When(@"merge connected and equal rule is applied")]
        public void WhenMergeConnectedAndEqualRuleIsApplied()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;

            ScenarioContext.Current.Add("MergeRuleAppliedLines", new MergeConnectedAndEqualLinesRule().ApplyTo(design.Lines));
        }

        [Then(@"the lines are merged into one")]
        public void ThenTheLinesAreMergedIntoOne()
        {
            var rulesAppliedLines = ScenarioContext.Current["MergeRuleAppliedLines"] as IEnumerable<PhysicalLine>;

            Specify.That(rulesAppliedLines.Count()).Should.BeEqualTo(1);
            Specify.That(rulesAppliedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(rulesAppliedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(20);
            Specify.That(rulesAppliedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(rulesAppliedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(10);
        }
    }
}
