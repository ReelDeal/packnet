﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces.DTO;
using Testing.Specificity;

namespace BusinessTests.DesignRulesApplicator
{
    [TestClass]
    public class WasteCuttingRuleTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddHorizontalCutLinesToWaste()
        {
            var rule = new WasteCuttingRule(new MicroMeter(1200d),  new MicroMeter(300d), new MicroMeter(300d));
            var lines = this.GetHorizontalLines().Union(this.GetVerticalLines()).ToList();
            var modifiedLines = rule.ApplyTo(lines).ToList();
            var wasteLines = modifiedLines.Except(lines).GetHorizontalLines().ToList();

            foreach (var line in wasteLines)
            {
                Console.WriteLine(line);
            }

            Specify.That(modifiedLines).Should.Not.BeNull();
            Specify.That(wasteLines.Count()).Should.BeEqualTo(4);
            Specify.That(wasteLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(100d);
            Specify.That(wasteLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(400d);
            Specify.That(wasteLines.ElementAt(2).StartCoordinate.Y).Should.BeLogicallyEqualTo(700d);
            Specify.That(wasteLines.ElementAt(3).StartCoordinate.Y).Should.BeLogicallyEqualTo(1000d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCutWasteFromTheRightMostVerticalLineOnTheCurrentYPosition()
        {
            var rule = new WasteCuttingRule(new MicroMeter(1200d), new MicroMeter(30d), new MicroMeter(30d));
            var lines = this.GetHorizontalLines().Union(this.GetVerticalLines()).ToList();
            var modifiedLines = rule.ApplyTo(lines).ToList();
            var wasteLines = modifiedLines.Except(lines).ToList();

            foreach (var physicalLine in wasteLines)
            {
                Console.WriteLine(physicalLine.StartCoordinate.Y);
            }

            Specify.That(wasteLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(30d);
            Specify.That(wasteLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(470d);
            Specify.That(wasteLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(50d);
            Specify.That(wasteLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(720d);
            Specify.That(wasteLines.ElementAt(2).StartCoordinate.Y).Should.BeLogicallyEqualTo(80d);
            Specify.That(wasteLines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(720d);
            Specify.That(wasteLines.ElementAt(3).StartCoordinate.Y).Should.BeLogicallyEqualTo(100d);
            Specify.That(wasteLines.ElementAt(3).StartCoordinate.X).Should.BeLogicallyEqualTo(720d);
            Specify.That(wasteLines.ElementAt(4).StartCoordinate.Y).Should.BeLogicallyEqualTo(130d);
            Specify.That(wasteLines.ElementAt(4).StartCoordinate.X).Should.BeLogicallyEqualTo(720d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCutWasteOnLeftSide()
        {
            var rule = new WasteCuttingRule(new MicroMeter(1200d), new MicroMeter(30d), new MicroMeter(30d));
            var lines = this.GetHorizontalLines().Union(this.GetVerticalLines()).ToList();
            var modifiedLines = rule.ApplyTo(lines).ToList();
            var wasteLines = modifiedLines.Except(lines).GetHorizontalLines().ToList();

            foreach (var line in wasteLines)
            {
                Console.WriteLine(line.StartCoordinate.X + "->" + line.EndCoordinate.X);
            }

            Specify.That(wasteLines.Count()).Should.BeEqualTo(43);

            Specify.That(wasteLines.ElementAt(41).StartCoordinate.Y).Should.BeLogicallyEqualTo(30d);
            Specify.That(wasteLines.ElementAt(41).StartCoordinate.X).Should.BeLogicallyEqualTo(0d);
            Specify.That(wasteLines.ElementAt(41).EndCoordinate.X).Should.BeLogicallyEqualTo(250d);
            Specify.That(wasteLines.ElementAt(42).StartCoordinate.Y).Should.BeLogicallyEqualTo(1180d);
            Specify.That(wasteLines.ElementAt(42).StartCoordinate.X).Should.BeLogicallyEqualTo(0d);
            Specify.That(wasteLines.ElementAt(42).EndCoordinate.X).Should.BeLogicallyEqualTo(250d);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCutWasteOnTiledBoxWithTopAndBottomWaste()
        {
            var rule = new WasteCuttingRule(new MicroMeter(1600d), new MicroMeter(300d), new MicroMeter(300d));
            var lines = this.GetHorizontalLines().Union(this.GetVerticalLines()).ToList();
            var tileLines = this.GetTiledLines(lines, 720d);
            var tiledDesignLines = lines.Union(tileLines);
            var modifiedLines = rule.ApplyTo(tiledDesignLines).ToList();
            var wasteLines = modifiedLines.Except(tiledDesignLines).ToList();

            var horizontalWasteLines = wasteLines.GetHorizontalLines().ToList();
            var verticalWasteLines = wasteLines.GetVerticalLines().ToList();

            wasteLines.ForEach(l => Console.WriteLine(l));

            Specify.That(horizontalWasteLines.Count()).Should.BeEqualTo(4);
            Specify.That(verticalWasteLines.Count()).Should.BeEqualTo(2);


            Specify.That(horizontalWasteLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(100d);
            Specify.That(horizontalWasteLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(400d);
            Specify.That(horizontalWasteLines.ElementAt(2).StartCoordinate.Y).Should.BeLogicallyEqualTo(700d);
            Specify.That(horizontalWasteLines.ElementAt(3).StartCoordinate.Y).Should.BeLogicallyEqualTo(1000d);

            Specify.That(horizontalWasteLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(1440d);
            Specify.That(horizontalWasteLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(1600d);

            Specify.That(verticalWasteLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(0d);
            Specify.That((verticalWasteLines.ElementAt(0).EndCoordinate.Y)).Should.BeLogicallyEqualTo(50d);
            Specify.That((verticalWasteLines.ElementAt(0).StartCoordinate.X)).Should.BeLogicallyEqualTo(720d);

            Specify.That(verticalWasteLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(1150d);
            Specify.That((verticalWasteLines.ElementAt(1).EndCoordinate.Y)).Should.BeLogicallyEqualTo(1200d);
            Specify.That((verticalWasteLines.ElementAt(1).StartCoordinate.X)).Should.BeLogicallyEqualTo(720d);
        }

        private IEnumerable<PhysicalLine> GetTiledLines(IEnumerable<PhysicalLine> lines, double designWidth)
        {
            var tiledLines = new List<PhysicalLine>();
            foreach (var line in lines)
            {
                double startX = line.StartCoordinate.X + designWidth;
                double endX = line.EndCoordinate.X + designWidth;
                var l =
                    new PhysicalLine(
                        new PhysicalCoordinate(new MicroMeter(startX), new MicroMeter((double)line.StartCoordinate.Y)),
                        new PhysicalCoordinate(new MicroMeter(endX), new MicroMeter((double)line.EndCoordinate.Y)),
                        line.Type);
                tiledLines.Add(l);
            }

            return tiledLines;
        }

        private IEnumerable<PhysicalLine> GetHorizontalLines()
        {
            var l1 = new PhysicalLine(new PhysicalCoordinate(0d, 50d), new PhysicalCoordinate(250d, 50d), LineType.cut);
            var l2 = new PhysicalLine(new PhysicalCoordinate(0d, 1150d), new PhysicalCoordinate(250d, 1150d), LineType.cut);
            var l3 = new PhysicalLine(new PhysicalCoordinate(250d, 0d), new PhysicalCoordinate(470d, 0d), LineType.cut);
            var l4 = new PhysicalLine(new PhysicalCoordinate(250d, 1200d), new PhysicalCoordinate(470d, 1200d), LineType.cut);
            var l5 = new PhysicalLine(new PhysicalCoordinate(470d, 50d), new PhysicalCoordinate(720d, 50d), LineType.cut);
            var l6 = new PhysicalLine(new PhysicalCoordinate(470d, 1150d), new PhysicalCoordinate(720d, 1150d), LineType.cut);
            var l7 = new PhysicalLine(new PhysicalCoordinate(0d, 100d), new PhysicalCoordinate(720d, 100d), LineType.crease);
            var l8 = new PhysicalLine(new PhysicalCoordinate(0d, 1100d), new PhysicalCoordinate(720d, 1100d), LineType.crease);

            return new List<PhysicalLine>() {l1, l2, l3, l4, l5, l6, l7, l8};
        }

        private IEnumerable<PhysicalLine> GetVerticalLines()
        {
            var l1 = new PhysicalLine(new PhysicalCoordinate(0d, 50d), new PhysicalCoordinate(0d, 1150d), LineType.cut);
            var l2 = new PhysicalLine(new PhysicalCoordinate(250d, 0d), new PhysicalCoordinate(250d, 50d), LineType.cut);
            var l3 = new PhysicalLine(new PhysicalCoordinate(250d, 50d), new PhysicalCoordinate(250d, 1150d), LineType.crease);
            var l4 = new PhysicalLine(new PhysicalCoordinate(250d, 1150d), new PhysicalCoordinate(250d, 1200d), LineType.cut);
            var l5 = new PhysicalLine(new PhysicalCoordinate(470d, 0d), new PhysicalCoordinate(470d, 50d), LineType.cut);
            var l6 = new PhysicalLine(new PhysicalCoordinate(470d, 50d), new PhysicalCoordinate(470d, 1150d), LineType.crease);
            var l7 = new PhysicalLine(new PhysicalCoordinate(470d, 1150d), new PhysicalCoordinate(470d, 1200d), LineType.cut);
            var l8 = new PhysicalLine(new PhysicalCoordinate(720d, 50d), new PhysicalCoordinate(720d, 1150d), LineType.cut);

            return new List<PhysicalLine>() {l1, l2, l3, l4, l5, l6, l7, l8};
        }


    }
}
