﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

using Testing.Specificity;

namespace BusinessTests.DesignRulesApplicator
{
    [TestClass]
    public class ConvertWasteSeparationLineTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldConvertWasteSeparationLinesToCutLines()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(1, 1), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(1, 1), LineType.separate),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(1, 1), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(1, 1), LineType.separate),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(1, 1), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(1, 1), LineType.separate),
            };


            var ruleAppliedLines = new WasteSeparationLinesRule().ApplyTo(lines);

            Specify.That(lines.Count).Should.BeEqualTo(ruleAppliedLines.Count());
            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(6);
            Specify.That(ruleAppliedLines.Any(l => l.Type == LineType.separate)).Should.BeFalse();
            Specify.That(ruleAppliedLines.Count(l => l.Type == LineType.crease)).Should.BeEqualTo(2);
            Specify.That(ruleAppliedLines.Count(l => l.Type == LineType.cut)).Should.BeEqualTo(4);

            Specify.That(ruleAppliedLines.ElementAt(0).Type == LineType.cut).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(0).StartCoordinate.Equals(new PhysicalCoordinate(0, 0))).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(0).EndCoordinate.Equals(new PhysicalCoordinate(1, 1))).Should.BeTrue();

            Specify.That(ruleAppliedLines.ElementAt(1).Type == LineType.cut).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(1).StartCoordinate.Equals(new PhysicalCoordinate(0, 0))).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(1).EndCoordinate.Equals(new PhysicalCoordinate(1, 1))).Should.BeTrue();

            Specify.That(ruleAppliedLines.ElementAt(2).Type == LineType.crease).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(2).StartCoordinate.Equals(new PhysicalCoordinate(0, 0))).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(2).EndCoordinate.Equals(new PhysicalCoordinate(1, 1))).Should.BeTrue();

            Specify.That(ruleAppliedLines.ElementAt(3).Type == LineType.cut).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(3).StartCoordinate.Equals(new PhysicalCoordinate(0, 0))).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(3).EndCoordinate.Equals(new PhysicalCoordinate(1, 1))).Should.BeTrue();

            Specify.That(ruleAppliedLines.ElementAt(4).Type == LineType.crease).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(4).StartCoordinate.Equals(new PhysicalCoordinate(0, 0))).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(4).EndCoordinate.Equals(new PhysicalCoordinate(1, 1))).Should.BeTrue();

            Specify.That(ruleAppliedLines.ElementAt(5).Type == LineType.cut).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(5).StartCoordinate.Equals(new PhysicalCoordinate(0, 0))).Should.BeTrue();
            Specify.That(ruleAppliedLines.ElementAt(5).EndCoordinate.Equals(new PhysicalCoordinate(1, 1))).Should.BeTrue();
        }
    }
}
