﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class EMConvertPerforationLinesTest
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void OneVerticalPerforationLine_ShouldBeConvertedIntoLinesOfTypeCutAndCrease()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 1500), LineType.perforation)
            };

            var linesAfterRuleIsApplied =
                new FusionPerforationLinesRule(new PerforationParameters() { CreaseLength = 400, CutLength = 400 }).ApplyTo(
                    lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.Y.Equals(0)))
                .Should.BeLogicallyEqualTo(1, "Expected one line to start at 0");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.Y.Equals(0)).Type)
                .Should.BeLogicallyEqualTo(LineType.crease, "Expected perforation crease line at edge of design");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.EndCoordinate.Y.Equals(1500)))
                .Should.BeLogicallyEqualTo(1, "Expected one line to end at 1500");
            Specify.That(linesAfterRuleIsApplied.First(l => l.EndCoordinate.Y.Equals(1500)).Type)
                .Should.BeLogicallyEqualTo(LineType.crease, "Expected perforation crease line at edge of design");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.Y.Equals(0)).EndCoordinate.Y >= 400)
                .Should.BeTrue("Vertical edge crease isn't at least <CreaseLength> long");
            Specify.That(linesAfterRuleIsApplied.Any(l => l.Type == LineType.cut))
                .Should.BeTrue("Expected vertical perforation cut lines");
            Specify.That(
                linesAfterRuleIsApplied.Where(l => l.Type == LineType.cut).All(l => l.EndCoordinate.Y - l.StartCoordinate.Y >= 400))
                .Should.BeTrue("All vertical perforation cut lines are not at least <CutLength> long.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OneHorizontalPerforationLine_ShouldBeConvertedIntoLinesOfTypeCutAndCrease()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(1500, 0), LineType.perforation)
            };

            var linesAfterRuleIsApplied =
                new FusionPerforationLinesRule(new PerforationParameters() { CreaseLength = 400, CutLength = 400 }).ApplyTo(
                    lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X.Equals(0)))
                .Should.BeLogicallyEqualTo(1, "Expected one line to end at 0");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.X.Equals(0)).Type)
                .Should.BeLogicallyEqualTo(LineType.crease, "Expected perforation crease line at edge of design");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.EndCoordinate.X.Equals(1500)))
                .Should.BeLogicallyEqualTo(1, "Expected one line to end at 1500");
            Specify.That(linesAfterRuleIsApplied.First(l => l.EndCoordinate.X.Equals(1500)).Type)
                .Should.BeLogicallyEqualTo(LineType.crease, "Expected perforation crease line at edge of design");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.X.Equals(0)).EndCoordinate.X >= 400)
                .Should.BeTrue("Horizontal edge crease isn't at least <CreaseLength> long");
            Specify.That(linesAfterRuleIsApplied.Any(l => l.Type == LineType.cut))
                .Should.BeTrue("Expected horizontal perforation cut lines");
            Specify.That(
                linesAfterRuleIsApplied.Where(l => l.Type == LineType.cut).All(l => l.EndCoordinate.X - l.StartCoordinate.X >= 400))
                .Should.BeTrue("All horizontal perforation cut lines are not at least <CutLength> long.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeConvertedIntoCutsAndCreases_AndShouldNotMergeWithCreasesConnected()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(200, 0), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(1400, 0), LineType.perforation),
            };

            var linesAfterRuleIsApplied =
                new FusionPerforationLinesRule(new PerforationParameters() { CreaseLength = 400, CutLength = 400 }).ApplyTo(
                    lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(4);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(0) && l.EndCoordinate.X.Equals(200)))
                .Should.BeLogicallyEqualTo(1, "Crease that connected to perforation should not be merged");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(200) && l.EndCoordinate.X.Equals(600)))
                .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.X.Equals(600) && l.EndCoordinate.X.Equals(1000)))
                .Should.BeLogicallyEqualTo(1, "Cut part of perforation should start at 600 and be 400 long");
            
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1000) && l.EndCoordinate.X.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoConnectedCreaseLine_WhenLengthOfPerforationLineIsShortThanTheCreaseLengthAndCutLength()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(3000, 100), new PhysicalCoordinate(3200, 100), LineType.perforation),
            };

            var linesAfterRuleIsApplied =
                new FusionPerforationLinesRule(new PerforationParameters() { CreaseLength = 400, CutLength = 400 }).ApplyTo(
                    lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3000) && l.EndCoordinate.X.Equals(3100)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");
            
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3100) && l.EndCoordinate.X.Equals(3200)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoConnectedCreaseLine_WhenPerforationLineIsShortThanCreaseLength_ButLongerThanCutLength()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(3000, 100), new PhysicalCoordinate(3200, 100), LineType.perforation),
            };

            var linesAfterRuleIsApplied =
                new FusionPerforationLinesRule(new PerforationParameters() { CreaseLength = 400, CutLength = 100 }).ApplyTo(
                    lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3000) && l.EndCoordinate.X.Equals(3100)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3100) && l.EndCoordinate.X.Equals(3200)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoCreaseLine_WhenPerforationLineIsShortThanCutLength_ButLongerThanCreaseLength()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(3000, 100), new PhysicalCoordinate(3200, 100), LineType.perforation),
            };

            var linesAfterRuleIsApplied =
                new FusionPerforationLinesRule(new PerforationParameters() { CreaseLength = 100, CutLength = 400 }).ApplyTo(
                    lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3000) && l.EndCoordinate.X.Equals(3100)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3100) && l.EndCoordinate.X.Equals(3200)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTwoCreaseLine_WhenPerforationLineIsShortThanCutLengthPlusTwoTimesCrease_ButLongerThanTwoTimesCreaseLength()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(3000, 100), new PhysicalCoordinate(3200, 100), LineType.perforation),
            };

            var linesAfterRuleIsApplied =
                new FusionPerforationLinesRule(new PerforationParameters() { CreaseLength = 80, CutLength = 90 }).ApplyTo(
                    lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(2);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3000) && l.EndCoordinate.X.Equals(3100)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(3100) && l.EndCoordinate.X.Equals(3200)))
                .Should.BeLogicallyEqualTo(1, "Perforation line should be converted to crease lines");
        }
    }
}