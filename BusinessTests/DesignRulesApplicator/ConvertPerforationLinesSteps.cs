﻿namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    public class ConvertPerforationLinesSteps
    {
        [Given(@"a simple and a more complex line of perforation type")]
        public void GivenASimpleAndAMoreComplexLineOfPerforationType()
        {
            var design20_20 = new PhysicalDesign();
            var design10_10 = new PhysicalDesign();
            var design30_20 = new PhysicalDesign();
            var design20_10 = new PhysicalDesign();

            var simplePerforationLine = new PhysicalLine(new PhysicalCoordinate(0, 0), 
                new PhysicalCoordinate(100, 0), LineType.perforation);

            var perforationLineWhereOneCreaseAndAPartOfACutIsOffset = new PhysicalLine(new PhysicalCoordinate(200, 0),
                new PhysicalCoordinate(310, 0), LineType.perforation);

            var smallPerforationLine = new PhysicalLine(new PhysicalCoordinate(350, 0),
                new PhysicalCoordinate(380, 0), LineType.perforation);

            var anotherComplexPerforationLine = new PhysicalLine(new PhysicalCoordinate(400, 0),
                new PhysicalCoordinate(510, 0), LineType.perforation);

            var perforationLineWithoutAnyRoomForACutLine = new PhysicalLine(new PhysicalCoordinate(600, 0),
                new PhysicalCoordinate(650, 0), LineType.perforation);

            var cutLine = new PhysicalLine(new PhysicalCoordinate(700, 0),
                new PhysicalCoordinate(750, 0), LineType.cut);

            var verticalPerforationLine = new PhysicalLine(new PhysicalCoordinate(1000, 10),
                new PhysicalCoordinate(1000, 110), LineType.perforation);

            design10_10.Add(simplePerforationLine);
            design30_20.Add(perforationLineWhereOneCreaseAndAPartOfACutIsOffset);
            design20_10.Add(anotherComplexPerforationLine);
            design20_20.Add(smallPerforationLine);
            design20_20.Add(perforationLineWithoutAnyRoomForACutLine);
            design20_20.Add(cutLine);
            design20_20.Add(verticalPerforationLine);

            ScenarioContext.Current["Design10_10"] = design10_10;
            ScenarioContext.Current["Design30_20"] = design30_20;
            ScenarioContext.Current["Design20_10"] = design20_10;
            ScenarioContext.Current["Design20_20"] = design20_20;
        }

        [When(@"perforation rules are applied")]
        public void WhenPerforationRulesAreApplied()
        {
            var design10_10 = ScenarioContext.Current["Design10_10"] as PhysicalDesign;
            var perforationLinesRule = new FusionPerforationLinesRule(new PerforationParameters { CutLength = 10, CreaseLength = 10 });
            ScenarioContext.Current.Add("RulesAppliedDesign10_10Lines", perforationLinesRule.ApplyTo(design10_10.Lines));

            var design30_20 = ScenarioContext.Current["Design30_20"] as PhysicalDesign;
            perforationLinesRule = new FusionPerforationLinesRule(new PerforationParameters { CutLength = 30, CreaseLength = 20 });
            ScenarioContext.Current.Add("RulesAppliedDesign30_20Lines", perforationLinesRule.ApplyTo(design30_20.Lines));

            var design20_10 = ScenarioContext.Current["Design20_10"] as PhysicalDesign;
            perforationLinesRule = new FusionPerforationLinesRule(new PerforationParameters { CutLength = 20, CreaseLength = 10 });
            ScenarioContext.Current.Add("RulesAppliedDesign20_10Lines", perforationLinesRule.ApplyTo(design20_10.Lines));

            var design20_20 = ScenarioContext.Current["Design20_20"] as PhysicalDesign;
            perforationLinesRule = new FusionPerforationLinesRule(new PerforationParameters { CutLength = 20, CreaseLength = 20 });
            ScenarioContext.Current.Add("RulesAppliedDesign20_20Lines", perforationLinesRule.ApplyTo(design20_20.Lines));
        }

        [Then(@"the second to last item must be a cut line if it fits")]
        public void ThenTheSecondToLastItemMustBeACutLineIfItFits()
        {
            var lines = ScenarioContext.Current["RulesAppliedDesign10_10Lines"] as IEnumerable<PhysicalLine>;

            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(9);
            Specify.That(lines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(2).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(3).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(4).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(5).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(6).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(7).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(8).Type).Should.BeLogicallyEqualTo(LineType.crease);
        }

        [Then(@"one crease and part of a cut line is converted to offset")]
        public void ThenOneCreaseAndPartOfACutLineIsConvertedToOffset()
        {
            var lines = ScenarioContext.Current["RulesAppliedDesign30_20Lines"] as IEnumerable<PhysicalLine>;

            Specify.That(lines.Count()).Should.BeEqualTo(3);
            Specify.That(lines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(200);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(240);
            Specify.That(lines.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(240);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(270);
            Specify.That(lines.ElementAt(2).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(270);
            Specify.That(lines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(310);
        }

        [Then(@"the first and last crease line is shortened if two full lines does not fit")]
        public void ThenTheFirstAndLastCreaseLineIsShortenedIfTwoFullLinesDoesNotFit()
        {
            var rulesAppliedLines = ScenarioContext.Current["RulesAppliedDesign20_20Lines"] as IEnumerable<PhysicalLine>;
            var lines = rulesAppliedLines.Where(l => l.StartCoordinate.X > 340).Where(l => l.EndCoordinate.X < 400).OrderBy(l => l.StartCoordinate.X);

            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(2);
            Specify.That(lines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(350);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(365);
            Specify.That(lines.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(365);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(380);
        }

        [Then(@"the first and last crease line is expanded if the last cut line does not fit")]
        public void ThenTheFirstAndLastCreaseLineIsExpandedIfTheLastCutLineDoesNotFit()
        {
            var evenMoreLines = ScenarioContext.Current["RulesAppliedDesign20_10Lines"] as IEnumerable<PhysicalLine>;

            Specify.That(evenMoreLines.Count()).Should.BeLogicallyEqualTo(7);
            Specify.That(evenMoreLines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(evenMoreLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(400);
            Specify.That(evenMoreLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(415);
            Specify.That(evenMoreLines.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(evenMoreLines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(415);
            Specify.That(evenMoreLines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(435);
            Specify.That(evenMoreLines.ElementAt(2).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(evenMoreLines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(435);
            Specify.That(evenMoreLines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(445);
            Specify.That(evenMoreLines.ElementAt(3).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(evenMoreLines.ElementAt(3).StartCoordinate.X).Should.BeLogicallyEqualTo(445);
            Specify.That(evenMoreLines.ElementAt(3).EndCoordinate.X).Should.BeLogicallyEqualTo(465);
            Specify.That(evenMoreLines.ElementAt(4).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(evenMoreLines.ElementAt(4).StartCoordinate.X).Should.BeLogicallyEqualTo(465);
            Specify.That(evenMoreLines.ElementAt(4).EndCoordinate.X).Should.BeLogicallyEqualTo(475);
            Specify.That(evenMoreLines.ElementAt(5).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(evenMoreLines.ElementAt(5).StartCoordinate.X).Should.BeLogicallyEqualTo(475);
            Specify.That(evenMoreLines.ElementAt(5).EndCoordinate.X).Should.BeLogicallyEqualTo(495);
            Specify.That(evenMoreLines.ElementAt(6).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(evenMoreLines.ElementAt(6).StartCoordinate.X).Should.BeLogicallyEqualTo(495);
            Specify.That(evenMoreLines.ElementAt(6).EndCoordinate.X).Should.BeLogicallyEqualTo(510);
        }

        [Then(@"the first and last crease line is expanded if no cut line fits")]
        public void ThenTheFirstAndLastCreaseLineIsExpandedIfNoCutLineFits()
        {
            var lines = ScenarioContext.Current["RulesAppliedDesign20_20Lines"] as IEnumerable<PhysicalLine>;

            var linesWithNoRoomForCutLine = lines.Where(l => l.StartCoordinate.X > 599).Where(l => l.EndCoordinate.X < 700).OrderBy(l => l.StartCoordinate.X);

            Specify.That(linesWithNoRoomForCutLine.Count()).Should.BeLogicallyEqualTo(2);
            Specify.That(linesWithNoRoomForCutLine.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(linesWithNoRoomForCutLine.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(600);
            Specify.That(linesWithNoRoomForCutLine.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(625);
            Specify.That(linesWithNoRoomForCutLine.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(linesWithNoRoomForCutLine.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(625);
            Specify.That(linesWithNoRoomForCutLine.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(650);
        }

        [Then(@"lines that are not perforation lines should not be affected")]
        public void ThenLinesThatAreNotPerforationLinesShouldNotBeAffected()
        {
            var rulesAppliedLines = ScenarioContext.Current["RulesAppliedDesign20_20Lines"] as IEnumerable<PhysicalLine>;
            var cutLine = rulesAppliedLines.Where(l => l.StartCoordinate.X > 699);

            Specify.That(cutLine.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(cutLine.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(700);
            Specify.That(cutLine.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(750);
        }

        [Then(@"vertical lines should be handled as well")]
        public void ThenVerticalLinesShouldBeHandledAsWell()
        {
            var rulesAppliedLines = ScenarioContext.Current["RulesAppliedDesign20_20Lines"] as IEnumerable<PhysicalLine>;
            var verticalLines = rulesAppliedLines.Where(l => l.StartCoordinate.Y > 9).OrderBy(l => l.StartCoordinate.Y);

            Specify.That(verticalLines.Count()).Should.BeLogicallyEqualTo(5);
            Specify.That(verticalLines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(verticalLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(verticalLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(30);
            Specify.That(verticalLines.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(verticalLines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(30);
            Specify.That(verticalLines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(verticalLines.ElementAt(2).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(verticalLines.ElementAt(2).StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(verticalLines.ElementAt(2).EndCoordinate.Y).Should.BeLogicallyEqualTo(70);
            Specify.That(verticalLines.ElementAt(3).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(verticalLines.ElementAt(3).StartCoordinate.Y).Should.BeLogicallyEqualTo(70);
            Specify.That(verticalLines.ElementAt(3).EndCoordinate.Y).Should.BeLogicallyEqualTo(90);
            Specify.That(verticalLines.ElementAt(4).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(verticalLines.ElementAt(4).StartCoordinate.Y).Should.BeLogicallyEqualTo(90);
            Specify.That(verticalLines.ElementAt(4).EndCoordinate.Y).Should.BeLogicallyEqualTo(110);
        }
    }
}
