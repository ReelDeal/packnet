﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class EMRemoveLinesWithZeroLengthTest
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ZeroLengthLines_ShouldBeRemoved()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 300), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(0, 300), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(0, 300), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(0, 300), LineType.perforation),

                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(300, 0), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 0), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 0), LineType.perforation)
            };

            var linesAfterRuleIsApplied = new ZeroLengthLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0), "Vertical line start coordinate is not correct");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 300), "Vertical line end coordinate is not correct");

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0), "Horizental line start coordinate is not correct");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(300, 0), "Horizental line end coordinate is not correct");
        }
    }
}
