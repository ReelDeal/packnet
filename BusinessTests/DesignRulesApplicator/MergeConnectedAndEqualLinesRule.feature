﻿@DesignRulesApplication
Feature: Rules for merging connected and equal lines
	In order to make sure that two lines of the same type ending where the next starting are combined into one line
	As an API User
	I want the rule to merge all connected lines into one

@MergeConnectedAndEqualLinesTest
Scenario: Merge first with second when first line starts before the second
	Given connected horizontal lines of same type where the first line starts before the second
	When rule is applied
	Then lines are merged into one horizontal line

@MergeConnectedAndEqualLinesTest
Scenario: Merge first with second when first line starts after the second
	Given connected horizontal lines of same type where the first line starts after the second
	When rule is applied
	Then lines are merged into one horizontal line

@MergeConnectedAndEqualLinesTest
Scenario: Do not merge when line type differs
	Given connected horizontal lines of different types
	When rule is applied
	Then both lines are left untouched

@MergeConnectedAndEqualLinesTest
Scenario: Merge connected vertical lines
	Given connected vertical lines of same type
	When rule is applied
	Then lines are merged into one vertical line

@MergeConnectedAndEqualLinesTest
Scenario: Merge both connected vertical and horizontal lines
	Given six horizontal and three vertial connected lines
	When rule is applied
	Then the lines are merged into five horizontal and two vertical lines