﻿namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    [Scope(Tag = "overlappingLinesTest")]
    public class OverlappingLinesRulesSteps
    {
        [Given(@"overlapping lines where one line is completely overlapped by a more dominant line")]
        public void GivenOverlappingLinesWhereOneLineIsCompletelyOverlappedByAMoreDominantLine()
        {
            var design = new PhysicalDesign();

            var dominantOverlappedLine = new PhysicalLine(new PhysicalCoordinate(10, 0), 
                new PhysicalCoordinate(15, 0), LineType.cut );

            var dominantOverlappingLine = new PhysicalLine(new PhysicalCoordinate(5, 0), 
                new PhysicalCoordinate(20, 0), LineType.cut );

            var lesserOverlappedLine = new PhysicalLine(new PhysicalCoordinate(10, 0),
                new PhysicalCoordinate(15, 0), LineType.perforation);

            var lesserOverlappingLine = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(40, 0), LineType.crease);

            var lesserOverlappingLine2 = new PhysicalLine(new PhysicalCoordinate(5, 0),
                new PhysicalCoordinate(20, 0), LineType.perforation);

            design.Add(dominantOverlappingLine);
            design.Add(dominantOverlappedLine);
            design.Add(lesserOverlappedLine);
            design.Add(lesserOverlappingLine);
            design.Add(lesserOverlappingLine2);

            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"one fefco201 design")]
        public void GivenOneFefco201Design()
        {
            var fefco201Design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(150, 0),
                new PhysicalCoordinate(150, 500), LineType.crease);
            fefco201Design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 0),
                new PhysicalCoordinate(550, 500), LineType.crease);
            fefco201Design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(700, 0),
                new PhysicalCoordinate(700, 500), LineType.cut);
            fefco201Design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 50),
                new PhysicalCoordinate(150, 50), LineType.cut);
            fefco201Design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(150, 50),
                new PhysicalCoordinate(550, 50), LineType.crease);
            fefco201Design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 50),
                new PhysicalCoordinate(700, 50), LineType.cut);
            fefco201Design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 450),
                new PhysicalCoordinate(150, 450), LineType.cut);
            fefco201Design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(150, 450),
                new PhysicalCoordinate(550, 450), LineType.crease);
            fefco201Design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(550, 450),
                new PhysicalCoordinate(700, 450), LineType.cut);
            fefco201Design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 500),
                new PhysicalCoordinate(700, 500), LineType.cut);
            fefco201Design.Add(line);

            ScenarioContext.Current["Fefco201Design"] = fefco201Design;

            // this.designTrack1.SetDesignLength(new MicroMeter(100));
            // this.designTrack1.Add(new TrackNumberItem(1));
        }

        [Given(@"two identical overlapping lines")]
        public void GivenTwoIdenticalOverlappingLines()
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(10, 0), 
                new PhysicalCoordinate(15, 0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(10, 0),
                new PhysicalCoordinate(15, 0), LineType.cut);
            design.Add(line);

            ScenarioContext.Current["Design"] = design;
        }

        [When(@"rules are applied to overlapping design")]
        public void WhenRulesAreAppliedToOverlappingDesign()
        {

            var design = ScenarioContext.Current["Design"] as PhysicalDesign;
            ScenarioContext.Current.Add("OverLappingLinesRuleAppliedLines", new OverLappingLinesRule().ApplyTo(design.Lines));
        }

        [When(@"rules are applied to fefco201 design")]
        public void WhenRulesAreAppliedToFefco201Design()
        {
            var fefco201Design = ScenarioContext.Current["Fefco201Design"] as PhysicalDesign;
            ScenarioContext.Current.Add("RulesAppliedFefco201Design", new OverLappingLinesRule().ApplyTo(fefco201Design.Lines));
        }

        [Then(@"the overlapped line is removed")]
        public void ThenTheOverlappedLineIsRemoved()
        {
            var rulesAppliedLines = ScenarioContext.Current["OverLappingLinesRuleAppliedLines"] as IEnumerable<PhysicalLine>;

            var lines = rulesAppliedLines.OrderBy(l => l.StartCoordinate.X);
            /*foreach (var l in lines)
            {
                System.Console.WriteLine("----------------");
                System.Console.WriteLine(l.GetPhysicalLineType());
                System.Console.WriteLine(l.GetPhysicalStartCoordinateX());
                System.Console.WriteLine(l.GetPhysicalStartCoordinateY());
                System.Console.WriteLine(l.GetPhysicalEndCoordinateX());
                System.Console.WriteLine(l.GetPhysicalEndCoordinateY());

            }*/
            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(3);

            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(5);
            Specify.That(lines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.crease);

            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(5);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(20);
            Specify.That(lines.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.cut);

            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(20);
            Specify.That(lines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(40);
            Specify.That(lines.ElementAt(2).Type).Should.BeLogicallyEqualTo(LineType.crease);
        }

        [Then(@"the fefco201 design should not be affected")]
        public void ThenTheFefco201DesignShouldNotBeAffected()
        {
            var rulesAppliedLines = ScenarioContext.Current["RulesAppliedFefco201Design"] as IEnumerable<PhysicalLine>;
            var physicalVertialLines = rulesAppliedLines.GetVerticalLines();
            var physicalHorizontalLines = rulesAppliedLines.GetHorizontalLines();

            /*foreach (var line in rulesAppliedFefco201Design.GetPhysicalLines())
            {
                System.Console.WriteLine(String.Format("X: {0}, {1} Y: {2}, {3}. {4}", line.GetPhysicalStartCoordinateX(), line.GetPhysicalEndCoordinateX(), line.GetPhysicalStartCoordinateY(),  
                                                        line.GetPhysicalEndCoordinateY(), line.GetPhysicalLineType().ToString()));
            }*/
            Specify.That(rulesAppliedLines.Count()).Should.BeLogicallyEqualTo(10);
            Specify.That(physicalVertialLines.Count()).Should.BeLogicallyEqualTo(3);
            Specify.That(physicalHorizontalLines.Count()).Should.BeLogicallyEqualTo(7);
        }

        [Then(@"one of the identical lines are removed")]
        public void ThenOneOfTheIdenticalLinesAreRemoved()
        {
            var rulesAppliedLines = ScenarioContext.Current["OverLappingLinesRuleAppliedLines"] as IEnumerable<PhysicalLine>;
            Specify.That(rulesAppliedLines.Count()).Should.BeEqualTo(1);
        }
    }
}
