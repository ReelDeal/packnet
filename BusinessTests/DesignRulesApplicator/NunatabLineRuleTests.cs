﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignRulesApplicator;
using PackNet.Common.Interfaces.DTO;

using Testing.Specificity;

namespace BusinessTests.DesignRulesApplicator
{
    [TestClass]
    public class NunatabLineRuleTests
    {
        #region EM nunatab tests
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReplaceNunatabLines_WithCutLinesForSingleOutOnEmMachines()
        {
            var lines = GetLinesForDesignWidthNunaTabsOnAllSides();

            var originalNunaTabLines = lines.Where(l => l.Type == LineType.nunatab).ToList();
            var ruleAppliedLines = new EmNunatabLinesRule().ApplyTo(lines);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(16);
            Specify.That(ruleAppliedLines.Count(l => l.Type == LineType.crease)).Should.BeEqualTo(0);
            Specify.That(originalNunaTabLines.Count(l => l.Type == LineType.cut)).Should.BeEqualTo(9, "We expect all nuna tab lines to be converted into cuts");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReplaceNunatabLinesWithCreaseLines_AtIntersectionPointWhenTilingOnEmMachines()
        {
            var lines = GetLinesForDesignWidthNunaTabsOnAllSides(2);

            var originalNunaTabLines = lines.Where(l => l.Type == LineType.nunatab).ToList();
            var ruleAppliedLines = new EmNunatabLinesRule().ApplyTo(lines);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(32);
            Specify.That(originalNunaTabLines.Count(l => l.Type == LineType.crease))
                .Should.BeEqualTo(4, "All nuna tab lines that are intersecting the next designs to be kept as nuna tab lines");
            Specify.That(originalNunaTabLines.Count(l => l.Type == LineType.cut))
                .Should.BeEqualTo(14, "All nuna tab lines that are not intersecting the next designs to be converted to cuts");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShoulReplaceNunatabLinesWithCreaseLines_AtIntersectionPointWhenTilingHorizontallyOnEmMachines()
        {
            var lines = GetLinesForDesignWidthNunaTabsOnAllSides(1, 2);

            var originalNunaTabLines = lines.Where(l => l.Type == LineType.nunatab).ToList();
            var ruleAppliedLines = new EmNunatabLinesRule().ApplyTo(lines);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(32);
            Specify.That(originalNunaTabLines.Count(l => l.Type == LineType.crease))
                .Should.BeEqualTo(5, "All nuna tab lines that are intersecting the next designs to be kept as nuna tab lines");
            Specify.That(originalNunaTabLines.Count(l => l.Type == LineType.cut))
                .Should.BeEqualTo(13, "All nuna tab lines that are not intersecting the next designs to be converted to cuts");
        }
        #endregion

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReplaceAllNunatabLinesWithCutLines_OnIqMachines()
        {
            var lines = GetLinesForDesignWidthNunaTabsOnAllSides(2);

            var originalNunaTabLines = lines.Where(l => l.Type == LineType.nunatab).ToList();
            var ruleAppliedLines = new IqNunatabLinesRule().ApplyTo(lines);

            Specify.That(ruleAppliedLines.Count()).Should.BeEqualTo(32);
            Specify.That(originalNunaTabLines.Count(l => l.Type == LineType.cut))
                .Should.BeEqualTo(18, "All nuna tab lines should be replace by cut lines");
        }
        #region iQ nunatab tests

        #endregion
        private IEnumerable<PhysicalLine> GetLinesForDesignWidthNunaTabsOnAllSides(int tileCount = 1, int horizontalTileCount = 1)
        {
            /*
             *      0                    200
             *  0    __-________-______-___
             *      {                      |
             *      |                      }
             *      |                      }
             *      |                      |
             *      {                      }
             * 200  |-____________________-|
             * 
             */

            var lines = new List<PhysicalLine>();
            for (int i = 0; i < tileCount; i++)
            {
                lines.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 0), new PhysicalCoordinate(20 + (200 * i), 0), LineType.cut));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(20 + (200 * i), 0), new PhysicalCoordinate(40 + (200 * i), 0), LineType.nunatab));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(40 + (200 * i), 0), new PhysicalCoordinate(170 + (200 * i), 0), LineType.cut));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(130 + (200 * i), 0), new PhysicalCoordinate(140 + (200 * i), 0), LineType.nunatab));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(170 + (200 * i), 0), new PhysicalCoordinate(180 + (200 * i), 0), LineType.nunatab));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(180 + (200 * i), 0), new PhysicalCoordinate(200 + (200 * i), 0), LineType.cut));

                lines.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 200), new PhysicalCoordinate(190 + (200 * i), 200), LineType.cut));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 200), new PhysicalCoordinate(30 + (200 * i), 200), LineType.nunatab));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(190 + (200 * i), 200), new PhysicalCoordinate(200 + (200 * i), 200), LineType.nunatab));

                lines.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 0), new PhysicalCoordinate(0 + (200 * i), 200), LineType.cut));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 0), new PhysicalCoordinate(0 + (200 * i), 20), LineType.nunatab));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(0 + (200 * i), 150), new PhysicalCoordinate(0 + (200 * i), 170), LineType.nunatab));

                lines.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 0), new PhysicalCoordinate(200 + (200 * i), 20), LineType.cut));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 20), new PhysicalCoordinate(200 + (200 * i), 80), LineType.nunatab));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 80), new PhysicalCoordinate(200 + (200 * i), 200), LineType.cut));
                lines.Add(new PhysicalLine(new PhysicalCoordinate(200 + (200 * i), 140), new PhysicalCoordinate(200 + (200 * i), 160), LineType.nunatab));
            };

            var originalLines = lines.ToList();
            for (var i = 1; i < horizontalTileCount; i++)
            {
                originalLines.ForEach(
                    l =>
                        lines.Add(new PhysicalLine(new PhysicalCoordinate(l.StartCoordinate.X, l.StartCoordinate.Y + (200 * i)),
                            new PhysicalCoordinate(l.EndCoordinate.X, l.EndCoordinate.Y + (200 * i)), l.Type)));
            }

            return lines;
        }
    }
}
