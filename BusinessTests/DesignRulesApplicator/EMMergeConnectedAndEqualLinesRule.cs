﻿namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class EMMergeConnectedAndEqualLinesRule
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void VerticalLinesThatStartAtTheEndPositionOfALineWithTheSameType_ShouldBeMergedIntoOne()
        {
            var lines = new List<PhysicalLine>()
                        {
                            new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 5000), LineType.crease),
                            new PhysicalLine(new PhysicalCoordinate(0, 5000), new PhysicalCoordinate(0, 10000), LineType.crease),
                            new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 5000), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(300, 5000), new PhysicalCoordinate(300, 10000), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(500, 0), new PhysicalCoordinate(500, 5000), LineType.perforation),
                            new PhysicalLine(new PhysicalCoordinate(500, 5000), new PhysicalCoordinate(500, 10000), LineType.perforation),
                            new PhysicalLine(new PhysicalCoordinate(700, 0), new PhysicalCoordinate(700, 5000), LineType.separate),
                            new PhysicalLine(new PhysicalCoordinate(700, 5000), new PhysicalCoordinate(700, 10000), LineType.separate),
                            new PhysicalLine(new PhysicalCoordinate(900, 0), new PhysicalCoordinate(900, 5000), LineType.nunatab),
                            new PhysicalLine(new PhysicalCoordinate(900, 5000), new PhysicalCoordinate(900, 10000), LineType.nunatab),
                        };

            var linesAfterRuleIsApplied = new MergeConnectedAndEqualLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull("List of lines is null");
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(5, "The actual number of lines is wrong.");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeLogicallyEqualTo(1, "There should only be one crease line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeLogicallyEqualTo(1, "There should only be one cut line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1, "There should only be one perforation line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.separate)).Should.BeLogicallyEqualTo(1, "There should only be one separation line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.nunatab)).Should.BeLogicallyEqualTo(1, "There should only be one nunatab line");

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0,0), "Expected the crease line to start at (0,0)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(300, 0), "Expected the cut line to start at (300,0)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(500, 0), "Expected the perforation line to start at (500,0)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.separate).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(700, 0), "Expected the separation line to start at (700,0)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.nunatab).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(900, 0), "Expected the nunatab line to start at (900,0)");

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 10000), "Expected the crease line to end at (0,10000)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(300, 10000), "Expected the cut line to end at (300,10000)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(500, 10000), "Expected the perforation line to end at (500,10000)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.separate).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(700, 10000), "Expected the separation line to end at (700,10000)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.nunatab).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(900, 10000), "Expected the nunatab line to end at (900,10000)");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void HorizontalLinesThatStartAtTheEndPositionOfALineWithTheSameType_ShouldBeMergedIntoOne()
        {
            
            var lines = new List<PhysicalLine>()
                        {
                            new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(5000, 0), LineType.crease),
                            new PhysicalLine(new PhysicalCoordinate(5000, 0), new PhysicalCoordinate(10000, 0), LineType.crease),
                            new PhysicalLine(new PhysicalCoordinate(0, 300), new PhysicalCoordinate(5000, 300), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(5000, 300), new PhysicalCoordinate(10000, 300), LineType.cut),
                            new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(5000, 500), LineType.perforation),
                            new PhysicalLine(new PhysicalCoordinate(5000, 500), new PhysicalCoordinate(10000, 500), LineType.perforation),
                            new PhysicalLine(new PhysicalCoordinate(0, 700), new PhysicalCoordinate(5000, 700), LineType.separate),
                            new PhysicalLine(new PhysicalCoordinate(5000, 700), new PhysicalCoordinate(10000, 700), LineType.separate),
                            new PhysicalLine(new PhysicalCoordinate(0, 900), new PhysicalCoordinate(5000, 900), LineType.nunatab),
                            new PhysicalLine(new PhysicalCoordinate(5000, 900), new PhysicalCoordinate(10000, 900), LineType.nunatab),
                        };

            var linesAfterRuleIsApplied = new MergeConnectedAndEqualLinesRule().ApplyTo(lines);

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull("List of lines is null");
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeLogicallyEqualTo(5, "The actual number of lines is wrong.");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease)).Should.BeLogicallyEqualTo(1, "There should only be one crease line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut)).Should.BeLogicallyEqualTo(1, "There should only be one cut line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation)).Should.BeLogicallyEqualTo(1, "There should only be one perforation line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.separate)).Should.BeLogicallyEqualTo(1, "There should only be one separation line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.nunatab)).Should.BeLogicallyEqualTo(1, "There should only be one nunatab line");

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 0), "Expected the crease line to start at (0,0)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 300), "Expected the cut line to start at (0,300)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 500), "Expected the perforation line to start at (0,500)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.separate).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 700), "Expected the separation line to start at (0,700)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.nunatab).StartCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(0, 900), "Expected the nunatab line to start at (0,900)");

            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.crease).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(10000, 0), "Expected the crease line to end at (10000,0)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.cut).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(10000, 300), "Expected the cut line to end at (10000,300)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.perforation).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(10000, 500), "Expected the perforation line to end at (10000,500)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.separate).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(10000, 700), "Expected the separation line to end at (10000,700)");
            Specify.That(linesAfterRuleIsApplied.First(l => l.Type == LineType.nunatab).EndCoordinate).Should.BeLogicallyEqualTo(new PhysicalCoordinate(10000, 900), "Expected the nunatab line to end at (10000,900)");
        }
    }
}
