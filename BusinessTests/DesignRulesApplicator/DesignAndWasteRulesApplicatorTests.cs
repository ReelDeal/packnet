﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.DesignRulesApplicator;
using PackNet.Business.Orders;
using PackNet.Business.PackagingDesigns;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

using Testing.Specificity;

namespace BusinessTests.DesignRulesApplicator
{
    [TestClass]
    public class DesignAndWasteRulesApplicatorTests
    {
        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("TestData\\ruleApplicatorTestDesign.ezd")]
        public void ShouldReturnRuleAppliedDesignWithWasteAreas()
        {
            var designManager = new PackagingDesignManager(Environment.CurrentDirectory, new Mock<ICachingService>().Object, new Mock<ILogger>().Object);
            designManager.LoadDesigns();
            var carton = new Carton() { Length = 300, Width = 200, Height = 500, DesignId = 6780001 };
            carton.XValues.Add("X1", 50);
            carton.XValues.Add("X2", 100);
            carton.XValues.Add("X3", 50);
            carton.XValues.Add("X4", 50);
            var corrugate = new Corrugate() { Width = 800, Thickness = 3};
            var design = OrderHelpers.MergeDesignsSideBySide(new[] { carton }, corrugate, true, designManager);
            var wasteParams = new WasteAreaParameters() { MaximumWasteWidth = 150, MaximumWasteLength = 250, MinimumWasteWidth = 50, MinimumWasteLength = 50, Enable = true};
            var ruleApplicator = new DesignAndWasteRulesApplicator(wasteParams);
            var ruleAppliedDesign = ruleApplicator.ApplyRules(design, new EmPerforationLineRule(50), new EmNunatabLinesRule(), corrugate, OrientationEnum.Degree0);

            Specify.That(ruleAppliedDesign).Should.Not.BeNull();
            Specify.That(ruleAppliedDesign.WasteAreas).Should.Not.BeNull();
            Specify.That(ruleAppliedDesign.WasteAreas.Count()).Should.BeEqualTo(9);
            Specify.That(ruleAppliedDesign.Lines.GetVerticalLines().Count()).Should.BeEqualTo(11);
            Specify.That(ruleAppliedDesign.Lines.GetHorizontalLines().Count()).Should.BeEqualTo(20);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void OverallRulesTestOneVerticalLine_ShouldReturnOneVerticalLineWithBothCutAndCrease()
        {
            var lines = new List<PhysicalLine>()
            {
                /*Expected output: Crease:0-550, Cut:550-950, Crease:950-1500, Cut:1500-2500, Crease:2500-3000*/
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 1500), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(0, 800), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(0, 800), LineType.crease),//overlapping lines
                new PhysicalLine(new PhysicalCoordinate(0, 800), new PhysicalCoordinate(0, 800), LineType.cut), //zero length line
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 2000), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 2000), LineType.crease),//completely overlapping lines
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 2000), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 1900), new PhysicalCoordinate(0, 2500), LineType.cut), //Merge connected cut line
                new PhysicalLine(new PhysicalCoordinate(0, 1500), new PhysicalCoordinate(0, 3000), LineType.crease), //Merge with overlapping cut line
            };

            var design = new PhysicalDesign();

            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });

            lines.ForEach(design.Add);

            design.Length = 3000d;

            design.Width = 100;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count).Should.BeLogicallyEqualTo(5);

            Specify.That(linesAfterRuleIsApplied.ElementAt(0).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 400));

            Specify.That(linesAfterRuleIsApplied.ElementAt(1).Type).Should.BeEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 400));
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1100));

            Specify.That(linesAfterRuleIsApplied.ElementAt(2).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1100));
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1500));

            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.Y.Equals(1500) && l.EndCoordinate.Y.Equals(2500))).Should.BeLogicallyEqualTo(1, "Expected one line to start at 1500");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.Y.Equals(1500)).Type).Should.BeLogicallyEqualTo(LineType.cut, "Expected cut line");

            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.Y.Equals(2500) && l.EndCoordinate.Y.Equals(3000))).Should.BeLogicallyEqualTo(1, "Expected one line to start at 2000");
            Specify.That(linesAfterRuleIsApplied.First(l => l.StartCoordinate.Y.Equals(2500)).Type).Should.BeLogicallyEqualTo(LineType.crease, "Expected crease line");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void FourVerticalLinesFourRulesOverallTest()
        {
            var lines = new List<PhysicalLine>()
            {
                /*Expected output: X = 0: Crease:0-550, Cut:550-950, Crease:950-1500
                 *                 X = 200: Cut:500-1500, Crease:1500-2000
                 *                 X = 300: Cut:0-2000, Crease:2000-4000
                 */
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 1500), LineType.perforation),//perforation lines converting
                
                new PhysicalLine(new PhysicalCoordinate(100, 1000), new PhysicalCoordinate(100, 1000), LineType.cut), //zero length line

                new PhysicalLine(new PhysicalCoordinate(200, 500), new PhysicalCoordinate(200, 1000), LineType.cut),
                
                new PhysicalLine(new PhysicalCoordinate(200, 600), new PhysicalCoordinate(200, 1500), LineType.cut),

                new PhysicalLine(new PhysicalCoordinate(200, 600), new PhysicalCoordinate(200, 2000), LineType.crease),//Completely overlapped by other lines

                new PhysicalLine(new PhysicalCoordinate(300, 0), new PhysicalCoordinate(300, 1000), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 1000), new PhysicalCoordinate(300, 2000), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(300, 2000), new PhysicalCoordinate(300, 3000), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(300, 3000), new PhysicalCoordinate(300, 4000), LineType.crease),//Merge the connected same type line
            };

            var design = new PhysicalDesign();

            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 4000;

            design.Width = 300;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(100), new EmNunatabLinesRule(), new Corrugate { Width = 300 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied.Count).Should.BeLogicallyEqualTo(7);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 0)).Should.BeLogicallyEqualTo(3);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 100)).Should.BeLogicallyEqualTo(0);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 200)).Should.BeLogicallyEqualTo(2);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.StartCoordinate.X == 300)).Should.BeLogicallyEqualTo(2);

            Specify.That(linesAfterRuleIsApplied.ElementAt(0).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 0));
            Specify.That(linesAfterRuleIsApplied.ElementAt(0).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 100));

            Specify.That(linesAfterRuleIsApplied.ElementAt(1).Type).Should.BeEqualTo(LineType.perforation);
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 100));
            Specify.That(linesAfterRuleIsApplied.ElementAt(1).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1400));

            Specify.That(linesAfterRuleIsApplied.ElementAt(2).Type).Should.BeEqualTo(LineType.crease);
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).StartCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1400));
            Specify.That(linesAfterRuleIsApplied.ElementAt(2).EndCoordinate).Should.BeEqualTo(new PhysicalCoordinate(0, 1500));

            Specify.That(linesAfterRuleIsApplied.ElementAt(3).EndCoordinate.Y).Should.BeLogicallyEqualTo(1500d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(3).Type).Should.BeLogicallyEqualTo(LineType.cut);

            Specify.That(linesAfterRuleIsApplied.ElementAt(4).EndCoordinate.Y).Should.BeLogicallyEqualTo(2000d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(4).Type).Should.BeLogicallyEqualTo(LineType.crease);

            Specify.That(linesAfterRuleIsApplied.ElementAt(5).EndCoordinate.Y).Should.BeLogicallyEqualTo(2000d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(5).Type).Should.BeLogicallyEqualTo(LineType.cut);

            Specify.That(linesAfterRuleIsApplied.ElementAt(6).EndCoordinate.Y).Should.BeLogicallyEqualTo(4000d);
            Specify.That(linesAfterRuleIsApplied.ElementAt(6).Type).Should.BeLogicallyEqualTo(LineType.crease);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndMergeWithConnectedCreases()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1300), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 1300), new PhysicalCoordinate(0, 1400), LineType.crease),
            };

            var design = new PhysicalDesign();

            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 3000d;

            design.Width = 100;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(3);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(0) && l.EndCoordinate.Y.Equals(500)))
                .Should.BeLogicallyEqualTo(1, "Crease part in begining of perforation to be merged with previous crease line");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.Y.Equals(500) && l.EndCoordinate.Y.Equals(900)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 500 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(900) && l.EndCoordinate.Y.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Crease part in end of perforation to be merged with next crease line");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndShouldNotMergeWithConnectedCuts()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 100), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 1300), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(0, 1300), new PhysicalCoordinate(0, 1400), LineType.cut),
            };

            var design = new PhysicalDesign();

            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 3000d;

            design.Width = 100;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1000 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(5);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y.Equals(0) && l.EndCoordinate.Y.Equals(100)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(100) && l.EndCoordinate.Y.Equals(500)))
                .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.Y.Equals(500) && l.EndCoordinate.Y.Equals(900)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 500 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.Y.Equals(900) && l.EndCoordinate.Y.Equals(1300)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.Y.Equals(1300) && l.EndCoordinate.Y.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndShouldNotMergeWithCreasesIfCutLineInterceptsConnection()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(100, 0), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(200, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(1400, 0), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(1400, 0), new PhysicalCoordinate(1500, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(1500, 0), new PhysicalCoordinate(1600, 0), LineType.crease),
            };

            var design = new PhysicalDesign();

            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 100;

            design.Width = 1600;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1600 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(7);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(0) && l.EndCoordinate.X.Equals(100)))
                .Should.BeLogicallyEqualTo(1, "Crease with cut intercepting connection to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.X.Equals(100) && l.EndCoordinate.X.Equals(200)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(200) && l.EndCoordinate.X.Equals(600)))
                .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(600) && l.EndCoordinate.X.Equals(1000)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 600 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1000) && l.EndCoordinate.X.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.cut && l.StartCoordinate.X.Equals(1400) && l.EndCoordinate.X.Equals(1500)))
                .Should.BeLogicallyEqualTo(1, "Cut connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1500) && l.EndCoordinate.X.Equals(1600)))
                .Should.BeLogicallyEqualTo(1, "Crease with cut intercepting connection to perforation should not be merged");
        }


        [TestMethod]
        [TestCategory("Integration")]
        public void PerforationLineShouldBeConvertedIntoCutsAndCreases_AndShouldNotMergeWithCreasesIfNotDirectlyConnectedToPerforation()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(100, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 100), new PhysicalCoordinate(1400, 100), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(1500, 100), new PhysicalCoordinate(1600, 100), LineType.crease),
            };

            var design = new PhysicalDesign();

            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 100;

            design.Width = 1600;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 1600 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(5);
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(0) && l.EndCoordinate.X.Equals(100)))
                .Should.BeLogicallyEqualTo(1, "Crease not directly connected to perforation should not be merged");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(200) && l.EndCoordinate.X.Equals(600)))
                .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(600) && l.EndCoordinate.X.Equals(1000)))
                .Should.BeLogicallyEqualTo(1, "Perforation part of perforation should start at 600 and be 400 long");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1000) && l.EndCoordinate.X.Equals(1400)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(1500) && l.EndCoordinate.X.Equals(1600)))
                .Should.BeLogicallyEqualTo(1, "Crease not directly connected to perforation should not be merged");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void TwoPerforationLinesShouldBeConvertedIntoCutsAndCreases_AndShouldMergeWithCreasesInTheMiddle()
        {
            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(1200, 100), new PhysicalCoordinate(2000, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(1200, 100), LineType.perforation),
                new PhysicalLine(new PhysicalCoordinate(2000, 100), new PhysicalCoordinate(3200, 100), LineType.perforation),
            };

            var design = new PhysicalDesign();

            var designApp = new PackNet.Business.DesignRulesApplicator.DesignRulesApplicator(new WasteAreaParameters()
            {
                MaximumWasteLength = MicroMeter.MaxValue,
                MinimumWasteLength = 0,
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0
            });
            lines.ForEach(design.Add);

            design.Length = 100;

            design.Width = 3200;

            var linesAfterRuleIsApplied = designApp.ApplyRules(design, new EmPerforationLineRule(400), new EmNunatabLinesRule(), new Corrugate { Width = 3200 }, OrientationEnum.Degree0).Lines.ToList();

            Specify.That(linesAfterRuleIsApplied).Should.Not.BeNull();
            Specify.That(linesAfterRuleIsApplied.Count()).Should.BeEqualTo(5);

            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(0) && l.EndCoordinate.X.Equals(400)))
                        .Should.BeLogicallyEqualTo(1, "Perforation should begin with crease part");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(400) && l.EndCoordinate.X.Equals(800)))
                .Should.BeLogicallyEqualTo(1, "perforation part of first perforation should be from 400 to 800");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(800) && l.EndCoordinate.X.Equals(2400)))
                .Should.BeLogicallyEqualTo(1, "End crease part of two preforation lines should be merged with the crease line in the middle");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.perforation && l.StartCoordinate.X.Equals(2400) && l.EndCoordinate.X.Equals(2800)))
                .Should.BeLogicallyEqualTo(1, "perforation part of the second perforation should be from 2400 to 2800");
            Specify.That(linesAfterRuleIsApplied.Count(l => l.Type == LineType.crease && l.StartCoordinate.X.Equals(2800) && l.EndCoordinate.X.Equals(3200)))
                .Should.BeLogicallyEqualTo(1, "Perforation should end with crease part");
        }

        private PhysicalDesign Get201WithDGlueFlap()
        {
            /*
               0,  50       150  200  250
             0       _______
            25  ____|_ _ _ _|____     |
               |    ¦       ¦    |    |
            50 |_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
            100|_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
            150|_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
            200|_ _ ¦_ _ _ _¦_ _ |    |
               |    ¦       ¦    |    |
            250|____¦_______¦____|    | */

            var lines = new List<PhysicalLine>()
            {
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(150, 0), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 0), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(150, 0), new PhysicalCoordinate(150, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(50, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(150, 25), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(200, 25), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 25), new PhysicalCoordinate(0, 250), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(50, 25), new PhysicalCoordinate(50, 250), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(150, 25), new PhysicalCoordinate(150, 250), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(200, 25), new PhysicalCoordinate(200, 250), LineType.cut),
                new PhysicalLine(new PhysicalCoordinate(0, 50), new PhysicalCoordinate(200, 50), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(200, 100), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 150), new PhysicalCoordinate(200, 150), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 200), new PhysicalCoordinate(200, 200), LineType.crease),
                new PhysicalLine(new PhysicalCoordinate(0, 250), new PhysicalCoordinate(200, 250), LineType.cut)
            };

            var design = new PhysicalDesign(lines);
            design.Length = 250;
            design.Width = 200;

            return design;
        }
    }
}
