﻿@DesignRulesApplication
Feature: Design Rules Application
	In order to produce a design in an efficent way
	As an API user
	I want to get a design with applied rules

Scenario: Remove lines with zero length
	Given zero length lines
	And none zero length lines
	When zero length rule is applied
	Then the zero length lines are removed

Scenario: Adjust overlapping lines
	Given overlapping lines
	And one line of perforation type
	And one line of cut type
	And one line of crease type
	And another line of perforation type
	When rules are applied
	Then the overlapping cut line dominates crease and perforation
	And the overlapping perforation dominates crease

Scenario: Merge connected equal lines
	Given lines of same type
	And and start and end of the lines are the same coordinate
	When merge connected and equal rule is applied
	Then the lines are merged into one

Scenario: Convert perforation lines
	Given a simple and a more complex line of perforation type
	When perforation rules are applied
	Then the second to last item must be a cut line if it fits
	And one crease and part of a cut line is converted to offset
	And the first and last crease line is shortened if two full lines does not fit
	And the first and last crease line is expanded if the last cut line does not fit
	And the first and last crease line is expanded if no cut line fits
	And lines that are not perforation lines should not be affected
	And vertical lines should be handled as well

Scenario: Rules are applied in importance order
	Given a design
	When all rules are applied
	Then the rules are applied in the following order; zero length, overlapping and connected

Scenario: Length and Width of the design should be same as the measurement applied design
	Given a design
	When rules applicator is used
	Then the length is the same on the rule applied design as on the measurement applied design
	And the width is the same on the rule applied design as on the measurement applied design
