﻿namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]
    [Scope(Tag = "MergeConnectedAndEqualLinesTest")]
    public class MergeConnectedAndEqualLinesRuleSteps
    {
        [Given(@"connected horizontal lines of same type where the first line starts before the second")]
        public void GivenConnectedHorizontalLinesOfSameTypeWhereTheFirstLineStartsBeforeTheSecond()
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(0, 0), 
                new PhysicalCoordinate(10, 0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(10, 0),
                new PhysicalCoordinate(20, 0), LineType.cut);
            design.Add(line);

            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"connected horizontal lines of same type where the first line starts after the second")]
        public void GivenConnectedHorizontalLinesOfSameTypeWhereTheFirstLineStartsAfterTheSecond()
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(10, 0),
                new PhysicalCoordinate(20, 0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(10, 0), LineType.cut);
            design.Add(line);

            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"connected horizontal lines of different types")]
        public void GivenConnectedHorizontalLinesOfDifferentTypes()
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(10, 0), 
                new PhysicalCoordinate(20, 0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(10, 0), LineType.crease);
            design.Add(line);

            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"connected vertical lines of same type")]
        public void GivenConnectedVerticalLinesOfSameType()
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(0, 10),
                new PhysicalCoordinate(0, 20), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(0, 10), LineType.cut);
            design.Add(line);
            
            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"six horizontal and three vertial connected lines")]
        public void GivenSixHorizontalAndThreeVertialConnectedLines()
        {
            var design = new PhysicalDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(20, 0),
                new PhysicalCoordinate(30, 0), LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(10, 0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(10, 0),
                new PhysicalCoordinate(20, 0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(40, 0),
                new PhysicalCoordinate(50, 0), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(60, 0),
                new PhysicalCoordinate(70, 0), LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(50, 0),
                new PhysicalCoordinate(60, 0), LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 10),
                new PhysicalCoordinate(0, 20), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(0, 10), LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(0, 20),
                new PhysicalCoordinate(0, 30), LineType.cut);
            design.Add(line);

            ScenarioContext.Current["Design"] = design;
        }

        [When(@"rule is applied")]
        public void WhenRuleIsApplied()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;
            ScenarioContext.Current.Add("MergeRulesAppliedLines", new MergeConnectedAndEqualLinesRule().ApplyTo(design.Lines));
        }

        [Then(@"lines are merged into one horizontal line")]
        public void ThenLinesAreMergedIntoOneHorizontalLine()
        {
            var rulesAppliedLines = ScenarioContext.Current["MergeRulesAppliedLines"] as IEnumerable<PhysicalLine>;

            Specify.That(rulesAppliedLines.Count()).Should.BeEqualTo(1);
            Specify.That(rulesAppliedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(rulesAppliedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(rulesAppliedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(20);
            Specify.That(rulesAppliedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(0);
        }

        [Then(@"both lines are left untouched")]
        public void ThenBothLinesAreLeftUntouched()
        {
            var rulesAppliedLines = ScenarioContext.Current["MergeRulesAppliedLines"] as IEnumerable<PhysicalLine>;
            Specify.That(rulesAppliedLines.Count()).Should.BeEqualTo(2);
        }

        [Then(@"lines are merged into one vertical line")]
        public void ThenLinesAreMergedIntoOneVerticalLine()
        {
            var rulesAppliedLines = ScenarioContext.Current["MergeRulesAppliedLines"] as IEnumerable<PhysicalLine>;

            Specify.That(rulesAppliedLines.Count()).Should.BeEqualTo(1);
            Specify.That(rulesAppliedLines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(rulesAppliedLines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(rulesAppliedLines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(rulesAppliedLines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(20);
        }

        [Then(@"the lines are merged into five horizontal and two vertical lines")]
        public void ThenTheLinesAreMergedIntoFiveHorizontalAndTwoVerticalLines()
        {
            var rulesAppliedLines = ScenarioContext.Current["MergeRulesAppliedLines"] as IEnumerable<PhysicalLine>;

            var lines = rulesAppliedLines.GetHorizontalLines();
            Specify.That(lines.Count()).Should.BeEqualTo(5);

            lines = rulesAppliedLines.GetVerticalLines();
            Specify.That(lines.Count()).Should.BeEqualTo(2);
        }
    }
}
