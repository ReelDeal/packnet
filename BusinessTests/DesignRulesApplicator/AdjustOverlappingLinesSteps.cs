﻿namespace BusinessTests.DesignRulesApplicator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.DesignRulesApplicator;
    using PackNet.Common.Interfaces.DTO;

    using TechTalk.SpecFlow;

    using Testing.Specificity;

    [Binding]    
    public class AdjustOverlappingLinesSteps
    {   
        [Given(@"overlapping lines")]
        public void GivenOverlappingLines()
        {
            var design = new PhysicalDesign();

            var line1 = new PhysicalLine(new PhysicalCoordinate(0, 0), 
                new PhysicalCoordinate(10, 0), LineType.cut);

            var line2 = new PhysicalLine(new PhysicalCoordinate(5, 0), 
                new PhysicalCoordinate(20, 0), LineType.cut);

            var line3 = new PhysicalLine(new PhysicalCoordinate(15, 0),
                new PhysicalCoordinate(40, 0), LineType.cut);

            var line4 = new PhysicalLine(new PhysicalCoordinate(30, 0),
                new PhysicalCoordinate(60, 0), LineType.cut);

            design.Add(line1);
            design.Add(line2);
            design.Add(line3);
            design.Add(line4);

            ScenarioContext.Current["Design"] = design;
        }

        [Given(@"one line of perforation type")]
        public void GivenOneLineOfPerforationType()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;

            design.Lines.ElementAt(0).Type = LineType.perforation;
        }

        [Given(@"one line of cut type")]
        public void GivenOneLineOfCutType()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;

            design.Lines.ElementAt(1).Type = LineType.cut;
        }

        [Given(@"one line of crease type")]
        public void GivenOneLineOfCreaseType()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;

            design.Lines.ElementAt(2).Type = LineType.crease;
        }

        [Given(@"another line of perforation type")]
        public void GivenAnotherLineOfPerforationType()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;

            design.Lines.ElementAt(3).Type = LineType.perforation;
        }

        [When(@"rules are applied")]
        public void WhenRulesAreApplied()
        {
            var design = ScenarioContext.Current["Design"] as PhysicalDesign;
            ScenarioContext.Current.Add("OverLappingLinesRuleAppliedLines", new OverLappingLinesRule().ApplyTo(design.Lines));
        }

        [Then(@"the overlapping cut line dominates crease and perforation")]
        public void ThenTheOverlappingCutLineDominatesCreaseAndPerforation()
        {
            var rulesAppliedLines = ScenarioContext.Current["OverLappingLinesRuleAppliedLines"] as IEnumerable<PhysicalLine>;
            var lines = rulesAppliedLines.OrderBy(l => l.StartCoordinate.X);

            // ( ... = perf; ___ = cut; --- = crease )
            // 0.....10 5______20 15-----40 30......60
            // ska vara 0....5_____20-----30.....60  efter regelapplikator
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(5);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(5);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(20);
            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(20);
        }

        [Then(@"the overlapping perforation dominates crease")]
        public void ThenTheOverlappingPerforationDominatesCrease()
        {
            var rulesAppliedLines = ScenarioContext.Current["OverLappingLinesRuleAppliedLines"] as IEnumerable<PhysicalLine>;
            var lines = rulesAppliedLines.OrderBy(l => l.StartCoordinate.X);

            Specify.That(lines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(30);
            Specify.That(lines.ElementAt(3).StartCoordinate.X).Should.BeLogicallyEqualTo(30);
        }
    }
}
