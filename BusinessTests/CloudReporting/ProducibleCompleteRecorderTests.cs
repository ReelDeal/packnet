﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.CloudReporting;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Utils;

using Testing.Specificity;

namespace BusinessTests.CloudReporting
{
    [TestClass]
    public class ProducibleCompleteRecorderTests
    {
        [TestMethod]
        public void ShouldWriteBasicInformation_ForAnyBoxProductionResult()
        {
            var loggerArgs = string.Empty;
            var loggerMock = new Mock<ILogger>();
            loggerMock.Setup(l => l.Log(LogLevel.Info, "BoxProductionResult {0}", It.IsAny<Object[]>())).Callback<LogLevel, string, Object[]>(
                (level, meesage, args) =>
                {
                    loggerArgs = args[0] as string;
                });

            var machineMock = CreateMachineMock();
            var aggregator = new EventAggregator();
            var recorder = new ProducibleCompleteRecorder(aggregator, loggerMock.Object);

            var corrugate = CreateCorrugate();
            var packagingDesignCalculation = new PackagingDesignCalculation() { Length = 100, Width = 1000 };
            var cartonProduced = CreateCarton(corrugate, packagingDesignCalculation, "CUID-1", string.Empty);

            var machineProductionData = CreateMachineProductionData(cartonProduced, machineMock);

            aggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machineMock.Object, machineProductionData)
            });
            Retry.UntilTrue(() => loggerArgs != string.Empty, TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(50));

            Specify.That(loggerArgs).Should.Contain(string.Format("\"MachineId\":\"{0}\"", machineMock.Object.Id));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"MachineName\":\"{0}\"", machineMock.Object.Alias));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"TimeToProduce\":{0}", TimeSpan.FromMilliseconds(machineProductionData.ProductionTime).TotalSeconds));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"IsSuccess\":{0}", (!machineProductionData.Failure).ToString().ToLower()));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"CorrugateWidth\":{0}", corrugate.Width));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"CorrugateThickness\":{0}", corrugate.Thickness));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"CorrugateQuality\":{0}", corrugate.Quality));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"CorrugateUsedWidth\":{0}", packagingDesignCalculation.Width));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"CorrugateUsedLength\":{0}", machineProductionData.FedLength));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"PackagingLength\":{0}", cartonProduced.Length));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"PackagingWidth\":{0}", cartonProduced.Width));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"PackagingHeight\":{0}", cartonProduced.Height));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"PackagingDesignId\":{0}", cartonProduced.DesignId));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"Producibles\":[\"{0}\"]", cartonProduced.CustomerUniqueId));
            Specify.That(loggerArgs).Should.Contain(string.Format("\"Username\":\"{0}\"", machineMock.Object.AssignedOperator));
        }

        [TestMethod]
        public void ShouldWriteArticleId_IfCartonIsCreatedFromArticle()
        {
            var loggerArgs = string.Empty;
            var loggerMock = new Mock<ILogger>();
            loggerMock.Setup(l => l.Log(LogLevel.Info, "BoxProductionResult {0}", It.IsAny<Object[]>())).Callback<LogLevel, string, Object[]>(
                (level, meesage, args) =>
                {
                    loggerArgs = args[0] as string;
                });

            var machineMock = CreateMachineMock();
            var aggregator = new EventAggregator();
            var recorder = new ProducibleCompleteRecorder(aggregator, loggerMock.Object);

            var corrugate = CreateCorrugate();
            var packagingDesignCalculation = new PackagingDesignCalculation() { Length = 100, Width = 1000 };
            var cartonProduced = CreateCarton(corrugate, packagingDesignCalculation, "CUID-1", "Article-1");

            var machineProductionData = CreateMachineProductionData(cartonProduced, machineMock);

            aggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machineMock.Object, machineProductionData)
            });
            Retry.UntilTrue(() => loggerArgs != string.Empty, TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(50));

            Specify.That(loggerArgs).Should.Contain(string.Format("\"ArticleName\":\"{0}\"", cartonProduced.ArticleId));
        }

        [TestMethod]
        public void ShouldWriteUsedCorrugateAsSumForAllTiles_WhenATiledJobIsCompleted()
        {
            var loggerArgs = string.Empty;
            var loggerMock = new Mock<ILogger>();
            loggerMock.Setup(l => l.Log(LogLevel.Info, "BoxProductionResult {0}", It.IsAny<Object[]>())).Callback<LogLevel, string, Object[]>(
                (level, meesage, args) =>
                {
                    loggerArgs = args[0] as string;
                });

            var machineMock = CreateMachineMock();
            var aggregator = new EventAggregator();
            var recorder = new ProducibleCompleteRecorder(aggregator, loggerMock.Object);

            var corrugate = CreateCorrugate();
            var packagingDesignCalculation = new PackagingDesignCalculation() { Length = 100, Width = 1000 };
            var cartonProduced = CreatTiledCarton(corrugate, packagingDesignCalculation, "CUID-1", "Article-1");

            var machineProductionData = CreateMachineProductionData(cartonProduced, machineMock);

            aggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machineMock.Object, machineProductionData)
            });
            Retry.UntilTrue(() => loggerArgs != string.Empty, TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(50));

            Specify.That(loggerArgs).Should.Contain(string.Format("\"CorrugateUsedWidth\":{0}", packagingDesignCalculation.Width * machineProductionData.ProducibleCount));
        }

        [TestMethod]
        public void ShouldWriteXValues_IfCartonHasXValues()
        {
            var loggerArgs = string.Empty;
            var loggerMock = new Mock<ILogger>();
            loggerMock.Setup(l => l.Log(LogLevel.Info, "BoxProductionResult {0}", It.IsAny<Object[]>())).Callback<LogLevel, string, Object[]>(
                (level, meesage, args) =>
                {
                    loggerArgs = args[0] as string;
                });

            var machineMock = CreateMachineMock();
            var aggregator = new EventAggregator();
            var recorder = new ProducibleCompleteRecorder(aggregator, loggerMock.Object);

            var corrugate = CreateCorrugate();
            var packagingDesignCalculation = new PackagingDesignCalculation() { Length = 100, Width = 1000 };
            var cartonProduced = CreateCartonWithXValues(corrugate, packagingDesignCalculation, "CUID-1", "Article-1");

            var machineProductionData = CreateMachineProductionData(cartonProduced, machineMock);

            aggregator.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machineMock.Object, machineProductionData)
            });
            Retry.UntilTrue(() => loggerArgs != string.Empty, TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(50));

            Specify.That(loggerArgs)
                .Should.Contain(string.Format("\"AdditionalDimensions\":{{\"X1\":{0},\"X2\":{1},\"OtherName\":{2}", cartonProduced.XValues["X1"],
                    cartonProduced.XValues["X2"], cartonProduced.XValues["OtherName"]));
        }

        private static MachineProductionData CreateMachineProductionData(IPacksizeCarton cartonProduced, Mock<IPacksizeCutCreaseMachine> machineMock)
        {
            var machineProductionData = new MachineProductionData
            {
                Carton = cartonProduced,
                ProductionTime = 1234567,
                FedLength = 123,
                Corrugate = cartonProduced.CartonOnCorrugate.Corrugate,
                Machine = machineMock.Object,
                CartonOnCorrugate = cartonProduced.CartonOnCorrugate,
                Failure = false,
                UserName = machineMock.Object.AssignedOperator,
                ProducibleCount = cartonProduced is TiledCarton ? (cartonProduced as TiledCarton).Tiles.Count() : 1,
                MachineGroup = cartonProduced.ProducedOnMachineGroupId
            };
            return machineProductionData;
        }

        private static Carton CreateCartonWithXValues(Corrugate corrugate, PackagingDesignCalculation packagingDesignCalculation,
            string customerId, string articleId)
        {
            var carton = CreateCarton(corrugate, packagingDesignCalculation, customerId, articleId);
            carton.XValues.Add("X1", 123.4);
            carton.XValues.Add("X2", 234.5);
            carton.XValues.Add("OtherName", 345.6);
            return carton;
        }

        private static TiledCarton CreatTiledCarton(Corrugate corrugate, PackagingDesignCalculation packagingDesignCalculation,
            string customerId, string articleId)
        {
            var tiled = new TiledCarton();
            var tiles = new List<ICarton>();
            tiles.Add(CreateCarton(corrugate, packagingDesignCalculation, customerId, articleId));
            tiles.Add(CreateCarton(corrugate, packagingDesignCalculation, customerId, articleId));
            tiles.Add(CreateCarton(corrugate, packagingDesignCalculation, customerId, articleId));
            tiled.AddTiles(tiles);
            return tiled;
        }

        private static Carton CreateCarton(Corrugate corrugate, PackagingDesignCalculation packagingDesignCalculation, string customerId, string articleId)
        {
            var cartonProduced = new Carton()
            {
                ArticleId = articleId,
                CartonOnCorrugate =
                    new CartonOnCorrugate(null, corrugate, "key", packagingDesignCalculation, OrientationEnum.Degree0),
                CorrugateQuality = 1,
                CustomerUniqueId = customerId,
                DesignId = 1234,
                Height = 200,
                Length = 300,
                Width = 400,
                TrackNumber = 2,
                ProducedOnMachineGroupId = Guid.NewGuid()
            };

            return cartonProduced;
        }

        private static Corrugate CreateCorrugate()
        {
            var corrugate = new Corrugate()
            {
                Width = 1234,
                Alias = "C1",
                Thickness = 1.2,
                Quality = 3
            };
            return corrugate;
        }

        private static Mock<IPacksizeCutCreaseMachine> CreateMachineMock()
        {
            var machineMock = new Mock<IPacksizeCutCreaseMachine>();
            machineMock.Setup(m => m.AssignedOperator).Returns("Magma");
            machineMock.Setup(m => m.Id).Returns(Guid.NewGuid());
            machineMock.Setup(m => m.Alias).Returns("NameOfMachine");
            return machineMock;
        }
    }
}
