﻿

namespace BusinessTests.DesignCompensator
{
    using System.Collections.Generic;
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Business.DesignCompensator;
    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class DesignLineRemoverTests
    {
        [TestMethod]
        public void LinesThatStartAndEndCloseToTheDesignEdge_ShouldBeRemoved()
        {
            MicroMeter minimumDistanceToCorrugateEdge = 5d;
            MicroMeter corrugateLength = 1000d;
            MicroMeter corrugateWidth = 1000d;
            MicroMeter trackOffset = 0;

            var lines = new List<PhysicalLine>()
                        {
                            new PhysicalLine(
                                new PhysicalCoordinate(0, 0),
                                new PhysicalCoordinate(0, corrugateLength),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(0, corrugateLength),
                                new PhysicalCoordinate(corrugateWidth - 1d, corrugateLength),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(0, 0),
                                new PhysicalCoordinate(corrugateWidth - 1d, 0),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(corrugateWidth - 1d, 0),
                                new PhysicalCoordinate(corrugateWidth - 1d, corrugateLength),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(
                                corrugateWidth - minimumDistanceToCorrugateEdge - 1d,
                                0),
                                new PhysicalCoordinate(
                                corrugateWidth - minimumDistanceToCorrugateEdge - 1d,
                                corrugateLength),
                                LineType.cut),
                        };

            var compensatedLines = DesignLineRemover.RemoveLinesCloseToDesignEdge(
                lines,
                corrugateLength,
                corrugateWidth,
                trackOffset,
                minimumDistanceToCorrugateEdge);

            Specify.That(compensatedLines.Count).Should.BeLogicallyEqualTo(1);
            Specify.That(compensatedLines.First().StartCoordinate.X).Should.BeLogicallyEqualTo(corrugateWidth - minimumDistanceToCorrugateEdge - 1d);
            Specify.That(compensatedLines.First().StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First().EndCoordinate.X).Should.BeLogicallyEqualTo(corrugateWidth - minimumDistanceToCorrugateEdge - 1d);
            Specify.That(compensatedLines.First().EndCoordinate.Y).Should.BeLogicallyEqualTo(corrugateLength);
        }
    }
}
