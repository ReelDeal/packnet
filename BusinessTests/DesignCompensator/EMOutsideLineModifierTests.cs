﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Business.DesignCompensator;
namespace BusinessTests.DesignCompensator
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Common.Interfaces.DTO;

    using Testing.Specificity;

    [TestClass]
    public class EMOutsideLineModifierTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void LinesThatStartAndEndOutsideTheDesign_ShouldBeModifiedToStartEndAtDesignMinMaxEdge()
        {
            var lines = new List<PhysicalLine>();

            // 400x300 square
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, -100d), new PhysicalCoordinate(0, 500d), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(-100d, 300d), new PhysicalCoordinate(500d, 300d), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(-100d, 0), new PhysicalCoordinate(500d, 0), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(400d, -100d), new PhysicalCoordinate(400d, 500d), LineType.cut));

            var compensatedHorizontalLines = new OutsideLineModifier(lines.GetHorizontalLines(), new PhysicalHorizontalLineModifier(), 400d).ModifyLines().ToList();
            var compensatedVerticalLines = new OutsideLineModifier(lines.GetVerticalLines(), new PhysicalVerticalLineModifier(), 300d).ModifyLines().ToList();

            Specify.That(compensatedHorizontalLines.Count).Should.BeLogicallyEqualTo(2);
            Specify.That(compensatedVerticalLines.Count).Should.BeLogicallyEqualTo(2);
            var allCompensatedLines = new List<PhysicalLine>();
            allCompensatedLines.AddRange(compensatedHorizontalLines);
            allCompensatedLines.AddRange(compensatedVerticalLines);

            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.X == 0)).Should.BeLogicallyEqualTo(3);
            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.X == 400d)).Should.BeLogicallyEqualTo(1);

            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.X == 0)).Should.BeLogicallyEqualTo(1);
            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.X == 400d)).Should.BeLogicallyEqualTo(3);

            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.Y == 0)).Should.BeLogicallyEqualTo(3);
            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.Y == 300d)).Should.BeLogicallyEqualTo(1);

            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.Y == 0)).Should.BeLogicallyEqualTo(1);
            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.Y == 300d)).Should.BeLogicallyEqualTo(3);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void LinesThatStartOrEndOutsideTheDesign_ShouldBeModifiedToStartEndAtDesignMinMaxEdge()
        {
            var lines = new List<PhysicalLine>();

            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(500d, 0), LineType.cut)); // Horizontal start inside end outside
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, -100d), new PhysicalCoordinate(0, 300d), LineType.cut)); // Vertical start outside end inside
            lines.Add(new PhysicalLine(new PhysicalCoordinate(400d, 0), new PhysicalCoordinate(400d, 500d), LineType.cut)); // Vertical start inside end outside          
            lines.Add(new PhysicalLine(new PhysicalCoordinate(-100d, 300d), new PhysicalCoordinate(400d, 300d), LineType.cut)); // Horizontal start outside end inside

            var compensatedHorizontalLines = new OutsideLineModifier(lines.GetHorizontalLines(), new PhysicalHorizontalLineModifier(), 400d).ModifyLines().ToList();
            var compensatedVerticalLines = new OutsideLineModifier(lines.GetVerticalLines(), new PhysicalVerticalLineModifier(), 300d).ModifyLines().ToList();

            Specify.That(compensatedHorizontalLines.Count).Should.BeLogicallyEqualTo(2);
            Specify.That(compensatedVerticalLines.Count).Should.BeLogicallyEqualTo(2);
            var allCompensatedLines = new List<PhysicalLine>();
            allCompensatedLines.AddRange(compensatedHorizontalLines);
            allCompensatedLines.AddRange(compensatedVerticalLines);

            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.X == 0)).Should.BeLogicallyEqualTo(3);
            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.X == 400d)).Should.BeLogicallyEqualTo(1);

            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.X == 0)).Should.BeLogicallyEqualTo(1);
            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.X == 400d)).Should.BeLogicallyEqualTo(3);

            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.Y == 0)).Should.BeLogicallyEqualTo(3);
            Specify.That(allCompensatedLines.Count(l => l.StartCoordinate.Y == 300d)).Should.BeLogicallyEqualTo(1);

            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.Y == 0)).Should.BeLogicallyEqualTo(1);
            Specify.That(allCompensatedLines.Count(l => l.EndCoordinate.Y == 300d)).Should.BeLogicallyEqualTo(3);
        }
    }
}
