﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;

using Testing.Specificity;

namespace BusinessTests.DesignCompensator
{
    [TestClass]
    public class EmLineOffsetterTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldOffsetHorizontalPositions_WhenGivenHorizontalAxis()
        {
            MicroMeter corrugateLength = 1000d;
            MicroMeter corrugateWidth = 1000d;
            MicroMeter trackOffset = 100d;

            var lines = new List<PhysicalLine>
            {
                            new PhysicalLine(
                                new PhysicalCoordinate(0d, 0d),
                                new PhysicalCoordinate(0d, corrugateLength),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(0d, corrugateLength),
                                new PhysicalCoordinate(corrugateWidth, corrugateLength),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(0d, 0d),
                                new PhysicalCoordinate(corrugateWidth, 0d),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(corrugateWidth, 0d),
                                new PhysicalCoordinate(corrugateWidth, corrugateLength),
                                LineType.cut)
                        };

            var offsetter = new LineOffsetter(lines, new PhysicalHorizontalLineModifier());
            var linesOffset = offsetter.AdjustOffset(trackOffset).ToArray();

            Specify.That(linesOffset.ElementAt(0).StartCoordinate.X).Should.BeEqualTo(trackOffset);
            Specify.That(linesOffset.ElementAt(0).EndCoordinate.X).Should.BeEqualTo(trackOffset);
            Specify.That(linesOffset.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(linesOffset.ElementAt(0).EndCoordinate.Y).Should.BeEqualTo(corrugateLength);

            Specify.That(linesOffset.ElementAt(1).StartCoordinate.X).Should.BeEqualTo(trackOffset);
            Specify.That(linesOffset.ElementAt(1).EndCoordinate.X).Should.BeEqualTo(corrugateWidth + trackOffset);
            Specify.That(linesOffset.ElementAt(1).StartCoordinate.Y).Should.BeEqualTo(corrugateLength);
            Specify.That(linesOffset.ElementAt(1).EndCoordinate.Y).Should.BeEqualTo(corrugateLength);

            Specify.That(linesOffset.ElementAt(2).StartCoordinate.X).Should.BeEqualTo(trackOffset);
            Specify.That(linesOffset.ElementAt(2).EndCoordinate.X).Should.BeEqualTo(corrugateWidth + trackOffset);
            Specify.That(linesOffset.ElementAt(2).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(linesOffset.ElementAt(2).EndCoordinate.Y).Should.BeLogicallyEqualTo(0);

            Specify.That(linesOffset.ElementAt(3).StartCoordinate.X).Should.BeEqualTo(corrugateWidth + trackOffset);
            Specify.That(linesOffset.ElementAt(3).EndCoordinate.X).Should.BeEqualTo(corrugateWidth + trackOffset);
            Specify.That(linesOffset.ElementAt(3).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(linesOffset.ElementAt(3).EndCoordinate.Y).Should.BeEqualTo(corrugateLength);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldOffsetVerticalPositions_WhenGivenVerticalAxis()
        {
            MicroMeter corrugateLength = 1000d;
            MicroMeter corrugateWidth = 1000d;
            MicroMeter trackOffset = 100d;

            var lines = new List<PhysicalLine>
            {
                            new PhysicalLine(
                                new PhysicalCoordinate(0d, 0d),
                                new PhysicalCoordinate(0d, corrugateLength),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(0, corrugateLength),
                                new PhysicalCoordinate(corrugateWidth, corrugateLength),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(0d, 0d),
                                new PhysicalCoordinate(corrugateWidth, 0d),
                                LineType.cut),
                            new PhysicalLine(
                                new PhysicalCoordinate(corrugateWidth, 0d),
                                new PhysicalCoordinate(corrugateWidth, corrugateLength),
                                LineType.cut)
                        };

            var offsetter = new LineOffsetter(lines, new PhysicalVerticalLineModifier());
            var linesOffset = offsetter.AdjustOffset(trackOffset).ToArray();

            Specify.That(linesOffset.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(linesOffset.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(linesOffset.ElementAt(0).StartCoordinate.Y).Should.BeEqualTo(trackOffset);
            Specify.That(linesOffset.ElementAt(0).EndCoordinate.Y).Should.BeEqualTo(corrugateLength + trackOffset);

            Specify.That(linesOffset.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(linesOffset.ElementAt(1).EndCoordinate.X).Should.BeEqualTo(corrugateWidth);
            Specify.That(linesOffset.ElementAt(1).StartCoordinate.Y).Should.BeEqualTo(corrugateLength + trackOffset);
            Specify.That(linesOffset.ElementAt(1).EndCoordinate.Y).Should.BeEqualTo(corrugateLength + trackOffset);

            Specify.That(linesOffset.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(linesOffset.ElementAt(2).EndCoordinate.X).Should.BeEqualTo(corrugateWidth);
            Specify.That(linesOffset.ElementAt(2).StartCoordinate.Y).Should.BeEqualTo(trackOffset);
            Specify.That(linesOffset.ElementAt(2).EndCoordinate.Y).Should.BeLogicallyEqualTo(trackOffset);

            Specify.That(linesOffset.ElementAt(3).StartCoordinate.X).Should.BeEqualTo(corrugateWidth);
            Specify.That(linesOffset.ElementAt(3).EndCoordinate.X).Should.BeEqualTo(corrugateWidth);
            Specify.That(linesOffset.ElementAt(3).StartCoordinate.Y).Should.BeEqualTo(trackOffset);
            Specify.That(linesOffset.ElementAt(3).EndCoordinate.Y).Should.BeEqualTo(corrugateLength + trackOffset);
        }
    }
}
