﻿using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;

using Testing.Specificity;

namespace BusinessTests.DesignCompensator
{
    [TestClass]
    public class WasteAreaCutLineCreatorTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddCutLinesForAllWasteAreasInRuleAppliedPhysicalDesign()
        {
            var design = new RuleAppliedPhysicalDesign();
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(400, 200)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(600, 200)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(200, 200), new PhysicalCoordinate(400, 400)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(400, 200), new PhysicalCoordinate(600, 400)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(200, 400), new PhysicalCoordinate(400, 600)));
            design.AddWasteArea(new WasteArea(new PhysicalCoordinate(400, 400), new PhysicalCoordinate(600, 600)));

            var lines = design.Lines.Union(WasteAreaCutLineCreator.GetWasteCuttingLinesForWasteAreas(design.WasteAreas));

            Specify.That(design.Lines.Count()).Should.BeEqualTo(0);
            Specify.That(lines.Count()).Should.BeEqualTo(24);
            Specify.That(lines.GetVerticalLines().Count(l => l.StartCoordinate.X == 400)).Should.BeEqualTo(6);
            Specify.That(lines.GetVerticalLines().Count(l => l.StartCoordinate.X == 600)).Should.BeEqualTo(3);
            Specify.That(lines.GetHorizontalLines().Count(l => l.StartCoordinate.Y == 200)).Should.BeEqualTo(4);
            Specify.That(lines.GetHorizontalLines().Count(l => l.StartCoordinate.Y == 400)).Should.BeEqualTo(4);
            Specify.That(lines.GetHorizontalLines().Count(l => l.StartCoordinate.Y == 600)).Should.BeEqualTo(2);
        }
    }
}
