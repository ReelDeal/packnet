﻿using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;
using StoryQ;
using Testing.Specificity;

namespace BusinessTests.DesignCompensator
{
    using DesignCompensator = PackNet.Business.DesignCompensator.DesignCompensator;

    [TestClass]
    public class DesignCompensatorTests
    {
        private RuleAppliedPhysicalDesign design;
        private CompensationParameters compensationParameters;
        private DesignCompensator compensator;
        private PhysicalDesign compensatedDesign;

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddCutLinesToCorrugateEdgeWhenLastLineInHorizontalSequenceIsACreaseLineForTrack1()
        {
            new Story("")
            .InOrderTo("Have as small waste pieces as possible")
            .AsA("User")
            .IWant("Cut waste into pieces")
            .WithScenario("Should produce small wastepieces")
            .Given(ADesignWithACreaslineOnTheLeftEdge)
            .And(ADesignCompensator)
            .And(ASetOfCompensationParametersForTrackNumber, 1)
            .When(ICompensateTheDesign)
            .Then(ACutLineIsAddedBetweenTheEndOfTheDesignAndTheEndOfTheCorrugate)
            .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddCutLinesToCorrugateEdgeWhenLastLineInHorizontalSequenceIsACreaseLineForTrack2()
        {
            new Story("")
            .InOrderTo("Have as small waste pieces as possible")
            .AsA("User")
            .IWant("Cut waste into pieces")
            .WithScenario("Should produce small wastepieces")
            .Given(ADesignWithACreaslineOnTheRightEdge)
            .And(ADesignCompensator)
            .And(ASetOfCompensationParametersForTrackNumber, 2)
            .When(ICompensateTheDesign)
            .Then(ACutLineIsAddedBetweenTheEndOfTheDesignAndTheEndOfTheCorrugate2)
            .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        private void ADesignCompensator()
        {
            compensator = new DesignCompensator();
        }

        private void ICompensateTheDesign()
        {
            compensatedDesign = compensator.Compensate(compensationParameters);
        }

        private void ASetOfCompensationParametersForTrackNumber(int trackNumber)
        {
            if (trackNumber == 1)
            {
                compensationParameters = new CompensationParameters(design, true, 300, 200, 0, 0, 0, 0, 0, 0);
            }
            else if (trackNumber == 2)
            {
                compensationParameters = new CompensationParameters(design, false, 400, 200, 0, 0, 0, 0, 0, 0);
            }
        }

        private void ADesignWithACreaslineOnTheLeftEdge()
        {
            design = new RuleAppliedPhysicalDesign
            {
                Length = 100,
                Width = 150
            };

            var line = new PhysicalLine(
                new PhysicalCoordinate(150, 50), new PhysicalCoordinate(200, 50),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(200, 50), new PhysicalCoordinate(300, 50),
                LineType.cut);
            design.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(150, 100), new PhysicalCoordinate(200, 100),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(200, 100), new PhysicalCoordinate(300, 100),
                LineType.cut);
            design.Add(line);
        }

        private void ADesignWithACreaslineOnTheRightEdge()
        {
            design = new RuleAppliedPhysicalDesign
            {
                Length = 100,
                Width = 150
            };

            var line = new PhysicalLine(new PhysicalCoordinate(400, 50),
                new PhysicalCoordinate(500, 50), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(500, 50),
                new PhysicalCoordinate(550, 50), LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(400, 100),
                new PhysicalCoordinate(500, 100), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(500, 100),
                new PhysicalCoordinate(550, 100), LineType.crease);
            design.Add(line);
        }


        private void ACutLineIsAddedBetweenTheEndOfTheDesignAndTheEndOfTheCorrugate2()
        {
            var lines = compensatedDesign.Lines.ToArray();
            var newLine1 = lines[5];
            var newLine2 = lines[2];

            Specify.That(compensatedDesign.Lines.Count()).Should.BeEqualTo(7);

            Specify.That(newLine1.Type).Should.BeEqualTo(LineType.cut);
            Specify.That(newLine1.StartCoordinate.X).Should.BeLogicallyEqualTo(550);
            Specify.That(newLine1.EndCoordinate.X).Should.BeLogicallyEqualTo(600);
            Specify.That(newLine1.StartCoordinate.Y).Should.BeLogicallyEqualTo(100);
            Specify.That(newLine1.EndCoordinate.Y).Should.BeLogicallyEqualTo(100);

            Specify.That(newLine2.Type).Should.BeEqualTo(LineType.cut);
            Specify.That(newLine2.StartCoordinate.X).Should.BeLogicallyEqualTo(550);
            Specify.That(newLine2.EndCoordinate.X).Should.BeLogicallyEqualTo(600);
            Specify.That(newLine2.StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(newLine2.EndCoordinate.Y).Should.BeLogicallyEqualTo(50);
        }

        private void ACutLineIsAddedBetweenTheEndOfTheDesignAndTheEndOfTheCorrugate()
        {
            var lines = compensatedDesign.Lines.ToArray();
            var newLine1 = lines[0];
            var newLine2 = lines[3];

            Specify.That(compensatedDesign.Lines.Count()).Should.BeEqualTo(7);
            Specify.That(newLine1.Type).Should.BeEqualTo(LineType.cut);
            Specify.That(newLine1.StartCoordinate.X).Should.BeLogicallyEqualTo(100);
            Specify.That(newLine1.EndCoordinate.X).Should.BeLogicallyEqualTo(150);
            Specify.That(newLine1.StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(newLine1.EndCoordinate.Y).Should.BeLogicallyEqualTo(50);

            Specify.That(newLine2.Type).Should.BeEqualTo(LineType.cut);
            Specify.That(newLine2.StartCoordinate.X).Should.BeLogicallyEqualTo(100);
            Specify.That(newLine2.EndCoordinate.X).Should.BeLogicallyEqualTo(150);
            Specify.That(newLine2.StartCoordinate.Y).Should.BeLogicallyEqualTo(100);
            Specify.That(newLine2.EndCoordinate.Y).Should.BeLogicallyEqualTo(100);
        }
    }
}
