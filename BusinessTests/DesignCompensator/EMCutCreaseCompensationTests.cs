﻿namespace BusinessTests.DesignCompensator
{
    using System.Collections.Generic;
    using System.Linq;
    using PackNet.Business.DesignCompensator;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using PackNet.Common.Interfaces.DTO;
    using Testing.Specificity;

    [TestClass]
    public class EMCutCreaseCompensationTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void VerticalCutLines_ShouldBeCompensatedWithCutCompensation()
        {
            const double cutCompensation = 5d;
            const double creaseCompensation = 5d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 10), new PhysicalCoordinate(0, 990), LineType.cut));

            var compensator = new EmLineCompensator(lines, new PhysicalVerticalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(1);

            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.Y).Should.BeLogicallyEqualTo(15d);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).EndCoordinate.Y).Should.BeLogicallyEqualTo(985d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotCompensateStartPosOrEndPos_OfVerticalLinesThatStartsOrEndsAtDesignStartAndEnd()
        {
            const double cutCompensation = 5d;
            const double creaseCompensation = 5d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(0, 500), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(0, 1000), LineType.crease));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(100, 1000), LineType.crease));

            var compensator = new EmLineCompensator(lines, new PhysicalVerticalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d).ToList();

            Specify.That(compensatedLines.Count).Should.BeLogicallyEqualTo(3);

            Specify.That(compensatedLines[0].StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines[0].EndCoordinate.Y).Should.BeLogicallyEqualTo(500 - cutCompensation);
            Specify.That(compensatedLines[1].StartCoordinate.Y).Should.BeLogicallyEqualTo(500 - cutCompensation);
            Specify.That(compensatedLines[1].EndCoordinate.Y).Should.BeLogicallyEqualTo(1000);
            Specify.That(compensatedLines[2].StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines[2].EndCoordinate.Y).Should.BeLogicallyEqualTo(1000);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotCompensateStartPosOrEndPos_OfHorizontalLinesThatStartsOrEndsAtDesignStartAndEnd()
        {
            const double cutCompensation = 5d;
            const double creaseCompensation = 5d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 0), new PhysicalCoordinate(500, 0), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(500, 0), new PhysicalCoordinate(1000, 0), LineType.crease));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(1000, 100), LineType.cut));

            var compensator = new EmLineCompensator(lines, new PhysicalHorizontalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d).ToList();

            Specify.That(compensatedLines.Count).Should.BeLogicallyEqualTo(3);

            Specify.That(compensatedLines[0].StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines[0].EndCoordinate.X).Should.BeLogicallyEqualTo(500 - cutCompensation);
            Specify.That(compensatedLines[1].StartCoordinate.X).Should.BeLogicallyEqualTo(500 - cutCompensation);
            Specify.That(compensatedLines[1].EndCoordinate.X).Should.BeLogicallyEqualTo(1000);
            Specify.That(compensatedLines[2].StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines[2].EndCoordinate.X).Should.BeLogicallyEqualTo(1000);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AVerticalCreaseBeforeAVerticalCutLines_ShouldBeExtendedToStartAtTheCutLineStartPosition_IfOnTheSameCoordinate()
        {
            const double cutCompensation = 5d;
            const double creaseCompensation = 10d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 10), new PhysicalCoordinate(0, 100), LineType.crease));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 100), new PhysicalCoordinate(0, 990), LineType.cut));

            var compensator = new EmLineCompensator(lines, new PhysicalVerticalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.Y).Should.BeLogicallyEqualTo(105);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).EndCoordinate.Y).Should.BeLogicallyEqualTo(985);

            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.Y).Should.BeLogicallyEqualTo(20);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).EndCoordinate.Y).Should.BeLogicallyEqualTo(105);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AVerticalCreaseBeforeAVerticalCutLines_ShouldNotBeExtendedToStartAtTheCutLineStartPosition_IfNotOnTheSameCoordinate()
        {
            const double cutCompensation = 5d;
            const double creaseCompensation = 10d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(10, 10), new PhysicalCoordinate(10, 100), LineType.crease));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(20, 100), new PhysicalCoordinate(20, 990), LineType.cut));

            var compensator = new EmLineCompensator(lines, new PhysicalVerticalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.X).Should.BeLogicallyEqualTo(20);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.Y).Should.BeLogicallyEqualTo(105);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).EndCoordinate.Y).Should.BeLogicallyEqualTo(985);

            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.X).Should.BeLogicallyEqualTo(10);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.Y).Should.BeLogicallyEqualTo(20);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).EndCoordinate.Y).Should.BeLogicallyEqualTo(90);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void StartingPositionOfVerticalCreaseLineAfterVerticalCutLines_ShouldBeDecreasedWithCutCompensation()
        {
            const double cutCompensation = 5;
            const double creaseCompensation = 10;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 10), new PhysicalCoordinate(0, 500), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 500), new PhysicalCoordinate(0, 900), LineType.crease));

            var compensator = new EmLineCompensator(lines, new PhysicalVerticalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.Y).Should.BeLogicallyEqualTo(15);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).EndCoordinate.Y).Should.BeLogicallyEqualTo(495);

            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.Y).Should.BeLogicallyEqualTo(495);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).EndCoordinate.Y).Should.BeLogicallyEqualTo(890);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void VerticalCreaseLines_ShouldBeCompensatedWithCreaseCompensation()
        {
            const double cutCompensation = 5;
            const double creaseCompensation = 5;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(0, 10), new PhysicalCoordinate(0, 900), LineType.crease));

            var compensator = new EmLineCompensator(lines, new PhysicalVerticalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(1);

            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.Y).Should.BeLogicallyEqualTo(15);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).EndCoordinate.Y).Should.BeLogicallyEqualTo(895);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void HorizontalCutLines_ShouldBeCompensatedWithCutCompensation()
        {
            const double cutCompensation = 5;
            const double creaseCompensation = 5;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(10, 0), new PhysicalCoordinate(900, 0), LineType.cut));

            var compensator = new EmLineCompensator(lines, new PhysicalHorizontalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(1);

            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.X).Should.BeLogicallyEqualTo(15);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).EndCoordinate.X).Should.BeLogicallyEqualTo(895);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AHorizontalCreaseBeforeAHorizontalCutLines_ShouldBeExtendedToStartAtTheCutLineStartPosition()
        {
            const double cutCompensation = 5d;
            const double creaseCompensation = 10d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(10, 0), new PhysicalCoordinate(100, 0), LineType.crease));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(100, 0), new PhysicalCoordinate(990, 0), LineType.cut));

            var compensator = new EmLineCompensator(lines, new PhysicalHorizontalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.X).Should.BeLogicallyEqualTo(20);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).EndCoordinate.X).Should.BeLogicallyEqualTo(105);

            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.X).Should.BeLogicallyEqualTo(105);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).EndCoordinate.X).Should.BeLogicallyEqualTo(985);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void StartingPositionOfHorizontalCreaseLineAfterHorizontalCutLines_ShouldBeDecreasedWithCutCompensation()
        {
            const double cutCompensation = 5;
            const double creaseCompensation = 10;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(10, 0), new PhysicalCoordinate(500, 0), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(500, 0), new PhysicalCoordinate(900, 0), LineType.crease));

            var compensator = new EmLineCompensator(lines, new PhysicalHorizontalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(2);

            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).StartCoordinate.X).Should.BeLogicallyEqualTo(15);
            Specify.That(compensatedLines.First(l => l.Type == LineType.cut).EndCoordinate.X).Should.BeLogicallyEqualTo(495);

            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.X).Should.BeLogicallyEqualTo(495);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).EndCoordinate.X).Should.BeLogicallyEqualTo(890);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void HorizontalCreaseLines_ShouldBeCompensatedWithCreaseCompensation()
        {
            const double cutCompensation = 5d;
            const double creaseCompensation = 5d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(10, 0), new PhysicalCoordinate(900, 0), LineType.crease));

            var compensator = new EmLineCompensator(lines, new PhysicalHorizontalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(1);

            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).StartCoordinate.X).Should.BeLogicallyEqualTo(15);
            Specify.That(compensatedLines.First(l => l.Type == LineType.crease).EndCoordinate.X).Should.BeLogicallyEqualTo(895);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void HorizontalCreaseLinesBetweenCutLines_ShouldBeCompensatedWithCutCompensation()
        {
            const double cutCompensation = 10d;
            const double creaseCompensation = 5d;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(10, 0), new PhysicalCoordinate(200, 0), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(400, 0), LineType.crease));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(600, 0), LineType.cut));

            var compensator = new EmLineCompensator(lines, new PhysicalHorizontalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15d);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(3);

            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 20 && l.StartCoordinate.Y == 0)).Should.Not.BeNull();
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 20 && l.StartCoordinate.Y == 0).EndCoordinate.X).Should.BeLogicallyEqualTo(190);
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 20 && l.StartCoordinate.Y == 0).Type).Should.BeLogicallyEqualTo(LineType.cut);

            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 190 && l.StartCoordinate.Y == 0)).Should.Not.BeNull();
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 190 && l.StartCoordinate.Y == 0).EndCoordinate.X).Should.BeLogicallyEqualTo(410);
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 190 && l.StartCoordinate.Y == 0).Type).Should.BeLogicallyEqualTo(LineType.crease);

            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 410 && l.StartCoordinate.Y == 0)).Should.Not.BeNull();
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 410 && l.StartCoordinate.Y == 0).EndCoordinate.X).Should.BeLogicallyEqualTo(590);
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 410 && l.StartCoordinate.Y == 0).Type).Should.BeLogicallyEqualTo(LineType.cut);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void HorizontalCutLinesBetweenCreaseLines_ShouldBeCompensatedWithCutCompensation()
        {
            const double cutCompensation = 10;
            const double creaseCompensation = 5;
            var lines = new List<PhysicalLine>();
            lines.Add(new PhysicalLine(new PhysicalCoordinate(10, 0), new PhysicalCoordinate(200, 0), LineType.crease));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(200, 0), new PhysicalCoordinate(400, 0), LineType.cut));
            lines.Add(new PhysicalLine(new PhysicalCoordinate(400, 0), new PhysicalCoordinate(600, 0), LineType.crease));

            var compensator = new EmLineCompensator(lines, new PhysicalHorizontalLineModifier(), 0, 1000);

            var compensatedLines = compensator.Compensate(cutCompensation, creaseCompensation, 15);

            Specify.That(compensatedLines.Count()).Should.BeLogicallyEqualTo(3);

            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 15 && l.StartCoordinate.Y == 0)).Should.Not.BeNull();
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 15 && l.StartCoordinate.Y == 0).EndCoordinate.X).Should.BeLogicallyEqualTo(210);
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 15 && l.StartCoordinate.Y == 0).Type).Should.BeLogicallyEqualTo(LineType.crease);

            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 210 && l.StartCoordinate.Y == 0)).Should.Not.BeNull();
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 210 && l.StartCoordinate.Y == 0).EndCoordinate.X).Should.BeLogicallyEqualTo(390);
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 210 && l.StartCoordinate.Y == 0).Type).Should.BeLogicallyEqualTo(LineType.cut);

            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 390 && l.StartCoordinate.Y == 0)).Should.Not.BeNull();
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 390 && l.StartCoordinate.Y == 0).EndCoordinate.X).Should.BeLogicallyEqualTo(595);
            Specify.That(compensatedLines.First(l => l.StartCoordinate.X == 390 && l.StartCoordinate.Y == 0).Type).Should.BeLogicallyEqualTo(LineType.crease);
        }
    }
}
