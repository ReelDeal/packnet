﻿/*namespace DesignCompensatorTests
{
    using System;
    using System.Linq;
    using Packsize.ComponentModel.Types;
    using Packsize.ContainerCore;
    using Packsize.DomainLogic.DesignCompensator;
    using Packsize.DomainObjects;
    using Packsize.DomainObjects.BaseItems;
    using Packsize.DomainObjects.Design;
    using Packsize.DomainObjects.Line;
    using Packsize.DomainObjects.Machine.Track;
    using Packsize.DomainObjects.PhysicalDesign;
    using Packsize.DomainObjects.PhysicalLine;
    using TechTalk.SpecFlow;
    using Testing.Specificity;

    [Binding]
    public class OptimizationCompensatorSteps
    {
        private Container design;
        private Container compensatedDesign;
        private Container track;
        private MicroMeter corrugateWidth;
        private MicroMeter cutCompensation;
        private MicroMeter minimumLineLength;
        //private MicroMeter trackOffset;
        private IOptimizationCompensator optimizationCompensator;
        private bool exceptionThrown;

        [Given(@"a rule applied design")]
        public void GivenARuleAppliedDesign()
        {
            this.design = new PhysicalDesignFactory().Create();
            this.design.SetDesignLength(100);
            this.design.SetDesignWidth(150);
        }

        [Given(@"a track")]
        public void GivenATrack()
        {
            this.track = new TrackFactory().Create();
        }

        [Given(@"a right fixed track")]
        public void GivenARightFixedTrack()
        {
            this.track = new TrackFactory().Create();
            this.track.SetIsTracksRightSideFixed(true);
        }

        [Given(@"a horizontal cut line")]
        public void GivenAHorizontalCutLine()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(10, 10, 90, 10, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a center alignment")]
        public void GivenACenterAlignment()
        {
            this.track.SetTrackAlignment(Alignment.Center);
        }

        [Given(@"a right alignment")]
        public void GivenARightAlignment()
        {
            this.track.SetTrackAlignment(Alignment.Right);
        }

        [Given(@"a horizontal cut line with its start connected to the left side of corrugate")]
        public void GivenAHorizontalCutLineWithItsStartConnectedToTheLeftSideOfCorrugate()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(0, 50, 100, 50, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a horizontal cut line with its end connected to the right side of corrugate")]
        public void GivenAHorizontalCutLineWithItsEndConnectedToTheRightSideOfCorrugate()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(10, 55, 200, 55, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a partially outside horizontal cut line")]
        public void GivenAPartiallyOutsideHorizontalCutLine()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(10, 20, 200, 20, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a completely outside horizontal cut line")]
        public void GivenACompletelyOutsideHorizontalCutLine()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(160, 20, 200, 20, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a horizontal line shorter or equal to the the two times the compensation")]
        public void GivenAHorizontalLineShorterOrEqualToTheTheTwoTimesTheCompensation()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(10, 10, 20, 10, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a cut compensation of five")]
        public void GivenACutCompensationOfFive()
        {
            this.cutCompensation = 5;
        }

        [Given(@"a design compensator")]
        public void GivenADesignCompensator()
        {
            this.optimizationCompensator = new OptimizationCompensator();
        }

        [Given(@"a minimum line length of one")]
        public void GivenAMinimumLineLengthOfOne()
        {
            this.minimumLineLength = 1;
        }

        [Given(@"a parameter design")]
        public void GivenAParameterDesign()
        {
            this.design = new DesignContainerFactory().Create();
        }

        [Given(@"a corrugate width of 200")]
        public void GivenACorrugateWidthOf200()
        {
            this.corrugateWidth = 200;
        }

        [Given(@"a vertical cut line")]
        public void GivenAVerticalCutLine()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(10, 10, 10, 90, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a completely outside veritical cut line")]
        public void GivenACompletelyOutsideVeriticalCutLine()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(150, 110, 150, 150, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a completely outside veritical cut line with negative coordinates")]
        public void GivenACompletelyOutsideVeriticalCutLineWithNegativeCoordinates()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(150, -50, 150, -20, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a partially outside veritical cut line with negative coordinate")]
        public void GivenAPartiallyOutsideVeriticalCutLineWithNegativeCoordinate()
        {
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(50, -20, 50, 50, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"lines at edges of the corrugate")]
        public void GivenLinesAtEdgesOfTheCorrugate()
        {
            //// Top left to bottom left corner
            var line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(0, 0, 0, 50, LineType.cut);
            this.design.AddPhysicalLine(line);
            line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(0, 60, 0, 100, LineType.cut);
            this.design.AddPhysicalLine(line);
            //// Top left to top right corner
            line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(10, 0, 150, 0, LineType.cut);
            this.design.AddPhysicalLine(line);
            //// Top right to bottom right corner
            line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(150, 0, 150, 100, LineType.cut);
            this.design.AddPhysicalLine(line);
            //// Bottom left to bottom right corner
            line = new PhysicalLineFactory().Create();
            line.SetPhysicalLine(0, 100, 150, 100, LineType.cut);
            this.design.AddPhysicalLine(line);
        }

        [Given(@"a track offset of 123")]
        public void GivenATrackOffsetOf123()
        {
            //this.trackOffset = 123;            
            this.track.SetTrackOffset(123);
        }

        [Given(@"a track offset of 300")]
        public void GivenATrackOffsetOf300()
        {
            this.track.SetTrackOffset(300);
        }

        [When(@"the design is compensated")]
        public void WhenTheDesignIsCompensated()
        {
            try
            {
                var parameters = new CompensationParameters(this.design, this.track, this.corrugateWidth, this.cutCompensation, this.cutCompensation, this.minimumLineLength);
                this.compensatedDesign = this.optimizationCompensator.Compensate(parameters);
            }
            catch (ArgumentException)
            {
                this.exceptionThrown = true;
            }
        }

        [Then(@"all lines at the corrugate edges is removed")]
        public void ThenAllLinesAtTheCorrugateEdgesIsRemoved()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();
            Specify.That(lines.Count()).Should.BeEqualTo(3);
        }

        [Then(@"the cut line is shortened according to the cut compensation")]
        public void ThenTheCutLineIsShortenedAccordingToTheCutCompensation()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();
            Specify.That(this.compensatedDesign.IsMatching(Templates.PhysicalDesign)).Should.BeTrue();
            Specify.That(lines.Count()).Should.BeEqualTo(2);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(15);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(85);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(10);
        }

        [Then(@"the line is minimum line length long")]
        public void ThenTheLineIsMinimumLineLengthLong()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();
            Specify.That(this.compensatedDesign.IsMatching(Templates.PhysicalDesign)).Should.BeTrue();
            Specify.That(lines.Count()).Should.BeEqualTo(2);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(14.5);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(15.5);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(10);
        }

        [Then(@"an exception is thrown")]
        public void ThenAnExceptionIsThrown()
        {
            Specify.That(this.exceptionThrown).Should.BeTrue();
        }

        [Then(@"a cut off line is added at design length")]
        public void ThenACutOffLineIsAddedAtDesignLength()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();
            Specify.That(lines.Count()).Should.BeEqualTo(1);
            Specify.That(lines.ElementAt(0).GetPhysicalLineType()).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(this.corrugateWidth);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(this.design.GetDesignLength().Dimension);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(this.design.GetDesignLength().Dimension);
        }

        [Then(@"the length is same as on the rule applied design")]
        public void ThenTheLengthIsSameAsOnTheRuleAppliedDesign()
        {
            Specify.That(this.compensatedDesign.GetDesignLength().Dimension).Should.BeLogicallyEqualTo(this.design.GetDesignLength().Dimension);
        }

        [Then(@"the width is same as on the rule applied design")]
        public void ThenTheWidthIsSameAsOnTheRuleAppliedDesign()
        {
            Specify.That(this.compensatedDesign.GetDesignWidth().Dimension).Should.BeLogicallyEqualTo(this.design.GetDesignWidth().Dimension);
        }

        [Then(@"the vertical line is unchanged")]
        public void ThenTheVerticalLineIsUnchanged()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();
            Specify.That(lines.Count()).Should.BeEqualTo(2);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(90);
        }

        [Then(@"all lines x coordinates are moved with 123")]
        public void ThenAllLinesXCoordinatesAreMovedWith123()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();
            Specify.That(lines.Count()).Should.BeEqualTo(3);
            //// Horizontal cut line
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(138);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(208);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(10);

            //// Vertical line
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(133);
            Specify.That(lines.ElementAt(1).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(133);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(1).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(90);

            Specify.That(lines.ElementAt(2).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(123);
            Specify.That(lines.ElementAt(2).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(123 + this.corrugateWidth);
            Specify.That(lines.ElementAt(2).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(this.design.GetDesignLength().Dimension);
            Specify.That(lines.ElementAt(2).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(this.design.GetDesignLength().Dimension);
        }

        [Then(@"outside lines are shortened or removed")]
        public void ThenOutsideLinesAreShortenedOrRemoved()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();

            foreach (var line in lines)
            {
                System.Console.WriteLine("start: " + line.GetPhysicalStartCoordinateX().ToString() + " end: " + line.GetPhysicalEndCoordinateX().ToString());
            }

            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(5);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(90);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(1).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(200);
            Specify.That(lines.ElementAt(2).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(2).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(90);
            Specify.That(lines.ElementAt(3).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(3).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(50);
        }

        [Then(@"the design should be center aligned")]
        public void ThenTheDesignShouldBeCenterAligned()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();

            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(35);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(115);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(35);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(1).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(90);
        }

        [Then(@"the design should be right aligned")]
        public void ThenTheDesignShouldBeRightAligned()
        {
            var lines = this.compensatedDesign.GetPhysicalLines();

            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(60);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(140);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(60);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateY()).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(1).GetPhysicalEndCoordinateY()).Should.BeLogicallyEqualTo(90);
        }

        [Then(@"the line edges connected to the design are lengthend")]
        public void ThenTheLineEdgesConnectedToTheDesignAreLengthend()
        {
            var lines = this.compensatedDesign.GetPhysicalLines().GetHorizontalPhysicalLines();

            foreach (var line in lines)
            {
                System.Console.WriteLine("start: " + line.GetPhysicalStartCoordinateX().ToString() + " end: " + line.GetPhysicalEndCoordinateX().ToString());
            }

            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(4);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(100);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(245);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(165);
            Specify.That(lines.ElementAt(1).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(300);
            Specify.That(lines.ElementAt(2).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(165);
            Specify.That(lines.ElementAt(2).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(235);
        }

    }
}*/