﻿using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;

using Testing.Specificity;

namespace BusinessTests.DesignCompensator
{
    [TestClass]
    public class OptimizationCompensatorTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHorizontallyOffsetByTrackOffset_WhenCompensated_WithRuleAppliedDesign_AndTrackAlignmentSetToLeft()
        {
            MicroMeter corrugateLength = 1000d;
            MicroMeter corrugateWidth = 750d;
            MicroMeter trackOffset = 100d;

            var verticalLineAt50X = new PhysicalLine(
                new PhysicalCoordinate(50d, 0d),
                new PhysicalCoordinate(50d, corrugateLength),
                LineType.cut);
            var verticalLineAt600X = new PhysicalLine(
                new PhysicalCoordinate(corrugateWidth - 150d, 0d),
                new PhysicalCoordinate(corrugateWidth - 150d, corrugateLength),
                LineType.cut);
            var horizontalLineAt50Y = new PhysicalLine(
                new PhysicalCoordinate(0d, 50d),
                new PhysicalCoordinate(corrugateWidth, 50d),
                LineType.cut);
            var horizontalLineAt950Y = new PhysicalLine(
                new PhysicalCoordinate(0d, corrugateLength - 50d),
                new PhysicalCoordinate(corrugateWidth, corrugateLength - 50d),
                LineType.cut);
            var lines = new List<PhysicalLine> { verticalLineAt50X, verticalLineAt600X, horizontalLineAt50Y, horizontalLineAt950Y };

            var physicalDesign = new RuleAppliedPhysicalDesign
            {
                Length = corrugateLength,
                Width = corrugateWidth
            };
            lines.ForEach(physicalDesign.Add);

            const bool isTrackRightSideFixed = false;
            var parameters = new CompensationParameters(physicalDesign, isTrackRightSideFixed, trackOffset, corrugateWidth, 0, 0, 0, 0, 0, 0);

            var compensator = new OptimizationCompensator();
            var linesOffset = compensator.Compensate(parameters).Lines.ToList();

            Specify.That(linesOffset.Count).Should.BeEqualTo(4);
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == 50d + trackOffset && l.StartCoordinate.Y == 0d &&
                        l.EndCoordinate.X == 50d + trackOffset && l.EndCoordinate.Y == corrugateLength))
                        .Should.BeEqualTo(1, "verticalLineAt50X should have been offset by trackOffset");
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == corrugateWidth - 150d + trackOffset && l.StartCoordinate.Y == 0d &&
                        l.EndCoordinate.X == corrugateWidth - 150d + trackOffset && l.EndCoordinate.Y == corrugateLength))
                        .Should.BeEqualTo(1, "verticalLineAt600X should have been offset by trackOffset");
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == trackOffset && l.StartCoordinate.Y == 50d &&
                        l.EndCoordinate.X == corrugateWidth + trackOffset && l.EndCoordinate.Y == 50d))
                        .Should.BeEqualTo(1, "horizontalLineAt50Y should have been offset by trackOffset");
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == trackOffset && l.StartCoordinate.Y == 950d &&
                        l.EndCoordinate.X == corrugateWidth + trackOffset && l.EndCoordinate.Y == 950d))
                        .Should.BeEqualTo(1, "horizontalLineAt950Y should have been offset by trackOffset");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHorizontallyOffsetByTrackOffsetAndDifferenceBetweenCorrugateWidthAndDesignWidth_WhenCompensated_WithRuleAppliedDesign_AndTrackAlignmentSetToRight()
        {
            MicroMeter corrugateLength = 1000d;
            MicroMeter corrugateWidth = 750d;
            MicroMeter trackOffset = 100d;
            MicroMeter designWidth = 600d;

            var verticalLineAt50X = new PhysicalLine(
                new PhysicalCoordinate(50d, 0d),
                new PhysicalCoordinate(50d, corrugateLength),
                LineType.cut);
            var verticalLineAt550X = new PhysicalLine(
                new PhysicalCoordinate(designWidth - 50d, 0d),
                new PhysicalCoordinate(designWidth - 50d, corrugateLength),
                LineType.cut);
            var horizontalLineAt50Y = new PhysicalLine(
                new PhysicalCoordinate(0d, 50d),
                new PhysicalCoordinate(designWidth, 50d),
                LineType.cut);
            var horizontalLineAt950Y = new PhysicalLine(
                new PhysicalCoordinate(0d, corrugateLength - 50d),
                new PhysicalCoordinate(designWidth, corrugateLength - 50d),
                LineType.cut);
            var lines = new List<PhysicalLine> { verticalLineAt50X, verticalLineAt550X, horizontalLineAt50Y, horizontalLineAt950Y };

            var physicalDesign = new RuleAppliedPhysicalDesign
            {
                Length = corrugateLength,
                Width = designWidth
            };
            lines.ForEach(physicalDesign.Add);

            const bool isTrackRightSideFixed = true;
            var parameters = new CompensationParameters(physicalDesign, isTrackRightSideFixed, trackOffset, corrugateWidth, 0, 0, 0, 0, 0, 0);

            var compensator = new OptimizationCompensator();
            var linesOffset = compensator.Compensate(parameters).Lines.ToList();

            var trackStartingPosition = trackOffset - corrugateWidth;
            var rightSideOffset = corrugateWidth - designWidth;
            Specify.That(linesOffset.Count).Should.BeEqualTo(4);
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == 50d + trackStartingPosition + rightSideOffset && l.StartCoordinate.Y == 0d &&
                        l.EndCoordinate.X == 50d + trackStartingPosition + rightSideOffset && l.EndCoordinate.Y == corrugateLength))
                        .Should.BeEqualTo(1, "verticalLineAt50X should have been offset by trackOffset");
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == designWidth - 50d + trackStartingPosition + rightSideOffset && l.StartCoordinate.Y == 0d &&
                        l.EndCoordinate.X == designWidth - 50d + trackStartingPosition + rightSideOffset && l.EndCoordinate.Y == corrugateLength))
                        .Should.BeEqualTo(1, "verticalLineAt550X should have been offset by trackOffset");
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == trackStartingPosition + rightSideOffset && l.StartCoordinate.Y == 50d &&
                        l.EndCoordinate.X == designWidth + trackStartingPosition + rightSideOffset && l.EndCoordinate.Y == 50d))
                        .Should.BeEqualTo(1, "horizontalLineAt50Y should have been offset by trackOffset");
            Specify.That(linesOffset.Count(l =>
                        l.StartCoordinate.X == trackStartingPosition + rightSideOffset && l.StartCoordinate.Y == 950d &&
                        l.EndCoordinate.X == designWidth + trackStartingPosition + rightSideOffset && l.EndCoordinate.Y == 950d))
                        .Should.BeEqualTo(1, "horizontalLineAt950Y should have been offset by trackOffset");
        }
    }
}
