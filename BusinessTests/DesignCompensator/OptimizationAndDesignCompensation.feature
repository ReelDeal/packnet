﻿Feature: Optimization and Design Compensator
	In order to produce a design where the cuts are exactly the right length
	As an API user
	I want to get a design with lines compensated for knife and crease dimensions

Scenario: make horizontal cut lines shorter
	Given a rule applied design
	And a track
	And a horizontal cut line
	And a cut compensation of five
	And a corrugate width of 200
	And a design compensator
	And an optimization compensator
	When the design is compensated for optimization and design
	Then the cut line is shortened according to the cut compensation

Scenario: attached horizontal crease lines should be lengthed if cut line is shortened
	Given a rule applied fefco design
	And a track
	And a track offset of 200
	And track has right side fixed
	And a cut compensation of five
	And a corrugate width of 200
	And a design compensator
	And an optimization compensator
	When the design is compensated for optimization and design
	Then the total number of lines should be 10
	And at index 0 a line from (0, 50) to (95, 50) with type cut
	And at index 1 a line from (95, 50) to (155, 50) with type crease
	And at index 2 a line from (155, 50) to (200, 50) with type cut
	And at index 3 a line from (0, 75) to (95, 75) with type cut
	And at index 4 a line from (95, 75) to (155, 75) with type crease
	And at index 5 a line from (155, 75) to (200, 75) with type cut
	And at index 6 a line from (50, 0) to (50, 150) with type cut
	And at index 7 a line from (100, 0) to (100, 150) with type crease
	And at index 8 a line from (150, 0) to (150, 150) with type crease
	And at index 9 a line from (0, 150) to (200, 150) with type cut

Scenario: make the tool create a mark for too short lines
	Given a rule applied design
	And a track
	And a design compensator
	And an optimization compensator
	And a cut compensation of five
	And a minimum line length of one
	And a corrugate width of 200
	And a horizontal line shorter or equal to the the two times the compensation
	When the design is compensated for optimization and design
	Then the line is minimum line length long

Scenario: compensated design should have the same length and width as rule applied
	Given a rule applied design
	And a track
	And a design compensator
	And an optimization compensator
	When the design is compensated for optimization and design
	Then the length is same as on the rule applied design
	And the width is same as on the rule applied design

Scenario: should add cut off line
	Given a rule applied design
	And a track
	And a design compensator
	And an optimization compensator
	And a corrugate width of 200
	When the design is compensated for optimization and design
	Then a cut off line is added at design length

Scenario: should not compensate vertical lines
	Given a rule applied design
	And a track
	And a design compensator
	And an optimization compensator
	And a vertical cut line
	And a corrugate width of 100
	And a cut compensation of five
	When the design is compensated for optimization and design
	Then the vertical line is unchanged

Scenario: should move lines so that positions are absolute positions on the machine
	Given a rule applied design
	And a track
	And a design compensator
	And an optimization compensator
	And a horizontal cut line
	And a vertical cut line
	And a cut compensation of five
	And a corrugate width of 200
	And a track offset of 123
	When the design is compensated for optimization and design
	Then the total number of lines should be 3
	And line at index 0 should start at 138 and end at 208 on x axis
	And line at index 0 should start at 10 and end at 10 on y axis
	And line at index 1 should start at 133 and end at 133 on x axis
	And line at index 1 should start at 10 and end at 90 on y axis
	And line at index 2 should start at 123 and end at 323 on x axis
	And line at index 2 should start at 150 and end at 150 on y axis

Scenario: should remove lines at corrugate edges
	Given a rule applied design
	And a track
	And a design compensator
	And an optimization compensator
	And a horizontal cut line
	And a corrugate width of 200
	And lines at edges of the corrugate
	And one line at design edge on waste side
	When the design is compensated for optimization and design
	Then all lines at the corrugate edges are removed leaving a total of 3 lines

Scenario: should right align design when right side is fixed
	Given a rule applied design
	And a track
	And a track offset of 200
	And track has right side fixed
	And a horizontal cut line
	And a vertical cut line
	And a design compensator
	And an optimization compensator
	And a corrugate width of 200
	When the design is compensated for optimization and design
	Then the design should be right aligned
