﻿using System;
using System.Linq;
using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using TechTalk.SpecFlow;
using Testing.Specificity;

namespace BusinessTests.DesignCompensator
{
    using DesignCompensator = PackNet.Business.DesignCompensator.DesignCompensator;

    [Binding]
    public class DesignCompensatorSteps
    {
        private RuleAppliedPhysicalDesign design;
        private RuleAppliedPhysicalDesign compensatedDesign;
        private Track track;
        private MicroMeter trackOffset;
        private MicroMeter corrugateWidth;
        private MicroMeter cutCompensation;
        private MicroMeter creaseCompensation;
        private MicroMeter minimumLineLength;
        private MicroMeter minimumDistanceCorrugateEdgeToVerticalLine;
        private IDesignCompensator compensator;
        private OptimizationCompensator optimizationCompensator;


        [Given(@"a rule applied design")]
        public void GivenARuleAppliedDesign()
        {
            design = new RuleAppliedPhysicalDesign
            {
                Length = 150,
                Width = 150
            };
        }

        [Given(@"a rule applied fefco design")]
        public void GivenARuleAppliedFefcoDesign()
        {
            GivenARuleAppliedDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(0, 0),
                new PhysicalCoordinate(0, 150), LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(50, 0),
                new PhysicalCoordinate(50, 150), LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(100, 0),
                new PhysicalCoordinate(100, 150), LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 50),
                new PhysicalCoordinate(50, 50), LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(50, 50),
                new PhysicalCoordinate(100, 50), LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(100, 50),
                new PhysicalCoordinate(150, 50), LineType.cut);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 75),
                new PhysicalCoordinate(50, 75), LineType.cut);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(50, 75),
                new PhysicalCoordinate(100, 75), LineType.crease);
            design.Add(line);
            line = new PhysicalLine(new PhysicalCoordinate(100, 75),
                new PhysicalCoordinate(150, 75), LineType.cut);
            design.Add(line);
        }

        [Given(@"a rule applied design with three crease lines across the design width")]
        public void GivenARuleAppliedFefcoCreaseDesign()
        {
            GivenARuleAppliedDesign();

            var line = new PhysicalLine(new PhysicalCoordinate(0, 10),
                new PhysicalCoordinate(150, 10), LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 50),
                new PhysicalCoordinate(150, 50), LineType.crease);
            design.Add(line);

            line = new PhysicalLine(new PhysicalCoordinate(0, 90),
                new PhysicalCoordinate(150, 90), LineType.crease);
            design.Add(line);
        }

        [Given(@"a track")]
        public void GivenATrack()
        {
            track = new Track();
        }

        [Given(@"track has right side fixed")]
        public void TrackHasRightSideFixed()
        {
            track.RightSideFixed = true;
        }

        [Given(@"a track offset of (.*)")]
        public void GivenATrackOffsetOf200(double offset)
        {
            trackOffset = offset;
        }

        [Given(@"a horizontal cut line")]
        public void GivenAHorizontalCutLine()
        {
            var line = new PhysicalLine(new PhysicalCoordinate(10, 10),
                new PhysicalCoordinate(90, 10), LineType.cut);
            design.Add(line);
        }

        [Given(@"a horizontal cut line with its start connected to the left side of corrugate")]
        public void GivenAHorizontalCutLineWithItsStartConnectedToTheLeftSideOfCorrugate()
        {
            var line = new PhysicalLine(new PhysicalCoordinate(0, 50),
                new PhysicalCoordinate(100, 50), LineType.cut);
            design.Add(line);
        }

        [Given(@"a horizontal cut line with its end connected to the right side of corrugate")]
        public void GivenAHorizontalCutLineWithItsEndConnectedToTheRightSideOfCorrugate()
        {
            var line = new PhysicalLine(new PhysicalCoordinate(10, 55),
                new PhysicalCoordinate(200, 55), LineType.cut);
            design.Add(line);
        }

        [Given(@"a partially outside horizontal line from \((.*), (.*)\) to \((.*), (.*)\) with type (.*)")]
        public void GivenAPartiallyOutsideHorizontalLineFromToWithType(int xstart, int ystart, int xend, int yend, LineType lineType)
        {
            AddLineToDesign(xstart, ystart, xend, yend, lineType);
        }

        // [Given(@"a partially outside horizontal cut line")]
        // public void GivenAPartiallyOutsideHorizontalCutLine()
        // {
        //    var line = new PhysicalLineFactory().Create();
        //    line.SetPhysicalLine(10, 20, 200, 20, LineType.cut);
        //    this.design.AddPhysicalLine(line);
        // }
        [Given(@"a completely outside horizontal line from \((.*), (.*)\) to \((.*), (.*)\) with type (.*)")]
        public void GivenACompletelyOutsideHorizontalLineFromToWithType(int xstart, int ystart, int xend, int yend, LineType lineType)
        {
            GivenAPartiallyOutsideHorizontalLineFromToWithType(xstart, ystart, xend, yend, lineType);
        }

        [Given(@"a horizontal line shorter or equal to the the two times the compensation")]
        public void GivenAHorizontalLineShorterOrEqualToTheTheTwoTimesTheCompensation()
        {
            GivenAPartiallyOutsideHorizontalLineFromToWithType(10, 10, 20, 10, LineType.cut);
        }

        [Given(@"a cut compensation of five")]
        public void GivenACutCompensationOfFive()
        {
            cutCompensation = 5;
        }

        [Given(@"a cut compensation of (.*)")]
        public void GivenACutCompensationOf(int compensation)
        {
            cutCompensation = compensation;
        }

        [Given(@"a crease compensation of (.*)")]
        public void GivenACreaseCompensationOf(int compensation)
        {
            creaseCompensation = compensation;
        }

        [Given(@"a design compensator")]
        public void GivenADesignCompensator()
        {
            compensator = new DesignCompensator();
        }

        [Given(@"an optimization compensator")]
        public void GivenAnOptimizationCompensator()
        {
            optimizationCompensator = new OptimizationCompensator();
        }

        [Given(@"a minimum line length of one")]
        public void GivenAMinimumLineLengthOfOne()
        {
            minimumLineLength = 1;
        }

        [Given(@"a minimum distance corrugate edge to vertical line of five")]
        public void GivenAMinimumDistanceCorrugateEdgeToVerticalLineOfFive()
        {
            minimumDistanceCorrugateEdgeToVerticalLine = 5;
        }

        [Given(@"a corrugate width of (.*)")]
        public void GivenACorrugateWidthOf(double width)
        {
            corrugateWidth = width;
        }

        [Given(@"a vertical cut line")]
        public void GivenAVerticalCutLine()
        {
            GivenAPartiallyOutsideHorizontalLineFromToWithType(10, 10, 10, 90, LineType.cut);
        }

        [Given(@"a vertical (.*) line from \((.*), (.*)\) to \((.*), (.*)\)")]
        public void GivenAVerticalCutLineFromTo(LineType lineType, int xstart, int ystart, int xend, int yend)
        {
            GivenAPartiallyOutsideHorizontalLineFromToWithType(xstart, ystart, xend, yend, lineType);
        }

        // [Given(@"a completely outside veritical cut line")]
        // public void GivenACompletelyOutsideVeriticalCutLine()
        // {
        //    var line = new PhysicalLineFactory().Create();
        //    line.SetPhysicalLine(150, 110, 150, 150, LineType.cut);
        //    this.design.AddPhysicalLine(line);            
        // }
        [Given(@"a completely outside vertical line from \((.*), (.*)\) to \((.*), (.*)\) with type (.*)")]
        public void GivenACompletelyOutsideVerticalLineFromToWithType(int xstart, int ystart, int xend, int yend, LineType lineType)
        {
            GivenAPartiallyOutsideHorizontalLineFromToWithType(xstart, ystart, xend, yend, lineType);
        }

        // [Given(@"a completely outside veritical cut line with negative coordinates")]
        // public void GivenACompletelyOutsideVeriticalCutLineWithNegativeCoordinates()
        // {
        //    var line = new PhysicalLineFactory().Create();
        //    line.SetPhysicalLine(150, -50, 150, -20, LineType.cut);
        //    this.design.AddPhysicalLine(line);
        // }
        [Given(@"a completely outside vertical line with negative coordinates from \((.*), (.*)\) to \((.*), (.*)\) with type (.*)")]
        public void GivenACompletelyOutsideVerticalLineWithNegativeCoordinatesFromToWithType(int xstart, int ystart, int xend, int yend, LineType lineType)
        {
            GivenAPartiallyOutsideHorizontalLineFromToWithType(xstart, ystart, xend, yend, lineType);
        }

        // [Given(@"a partially outside veritical cut line with negative coordinate")]
        // public void GivenAPartiallyOutsideVeriticalCutLineWithNegativeCoordinate()
        // {
        //    var line = new PhysicalLineFactory().Create();
        //    line.SetPhysicalLine(50, -20, 50, 50, LineType.cut);
        //    this.design.AddPhysicalLine(line);
        // }
        [Given(@"a partially outside vertical line with negative coordinate from \((.*), (.*)\) to \((.*), (.*)\) with type (.*)")]
        public void GivenAPartiallyOutsideVerticalLineWithNegativeCoordinateFromToWithType(int xstart, int ystart, int xend, int yend, LineType lineType)
        {
            //Are we not suppossed to use the values passed in as arguments
            GivenAPartiallyOutsideHorizontalLineFromToWithType(50, -20, 50, 50, LineType.cut);
        }

        [Given(@"lines at edges of the corrugate")]
        public void GivenLinesAtEdgesOfTheCorrugate()
        {
            AddLinesFromTopLeftToBottomLeftCorner();
            AddLineFromTopLeftToTopRightCorner();
            AddLineFromTopRightToBottomRightCorner();
            AddLineFromBottomLeftToBottomRightCorner();
        }

        public void AddLinesFromTopLeftToBottomLeftCorner()
        {
            AddLineToDesign(0, 0, 0, 50, LineType.cut);
            AddLineToDesign(0, 60, 0, 100, LineType.cut);
        }

        public void AddLineFromTopLeftToTopRightCorner()
        {
            AddLineToDesign(10, 0, corrugateWidth, 0, LineType.cut);
        }

        public void AddLineFromTopRightToBottomRightCorner()
        {
            AddLineToDesign(corrugateWidth, 0, corrugateWidth, 150, LineType.cut);
        }

        public void AddLineFromBottomLeftToBottomRightCorner()
        {
            AddLineToDesign(0, 150, 150, 150, LineType.cut);
        }

        [Given(@"one line at design edge on waste side")]
        public void OneLineAtDesignEdgeOnWasteSide()
        {
            // TODO: Consider track offset!
            AddLineToDesign(150, 0, 150, 100, LineType.cut);
        }

        [When(@"the design is compensated")]
        public void WhenTheDesignIsCompensated()
        {
            var parameters = new CompensationParameters(design, track.RightSideFixed, trackOffset, corrugateWidth, cutCompensation, creaseCompensation, minimumLineLength, minimumDistanceCorrugateEdgeToVerticalLine,0,0);
            compensatedDesign = compensator.Compensate(parameters);
        }

        [When(@"the design is compensated for optimization")]
        public void WhenTheDesignIsCompensatedForOptimization()
        {
            var parameters = new CompensationParameters(design, track.RightSideFixed, trackOffset, corrugateWidth, cutCompensation, creaseCompensation, minimumLineLength, minimumDistanceCorrugateEdgeToVerticalLine, 0, 0);
            compensatedDesign = optimizationCompensator.Compensate(parameters);
        }

        [When(@"the design is compensated for optimization and design")]
        public void WhenTheDesignIsCompensatedForOptimizationAndDesign()
        {
            var parameters = new CompensationParameters(design, track.RightSideFixed, trackOffset, corrugateWidth, cutCompensation, cutCompensation, minimumLineLength, minimumDistanceCorrugateEdgeToVerticalLine, 0, 0);
            var optimizationCompensation = optimizationCompensator.Compensate(parameters);
            parameters = new CompensationParameters(optimizationCompensation, track.RightSideFixed, trackOffset, corrugateWidth, cutCompensation, cutCompensation, minimumLineLength, minimumDistanceCorrugateEdgeToVerticalLine, 0, 0);
            compensatedDesign = compensator.Compensate(parameters);
        }

        [Then(@"all lines at the corrugate edges are removed leaving a total of (.*) lines")]
        public void ThenAllLinesAtTheCorrugateEdgesIsRemoved(int linesLeft)
        {
            Specify.That(compensatedDesign.Lines.Count()).Should.BeEqualTo(linesLeft);
        }

        [Then(@"the cut line is shortened according to the cut compensation")]
        public void ThenTheCutLineIsShortenedAccordingToTheCutCompensation()
        {
            var lines = compensatedDesign.Lines;
            Specify.That(lines.Count()).Should.BeEqualTo(2);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(15);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(85);
            Specify.That(lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(10);
        }

        [Then(@"the line is minimum line length long")]
        public void ThenTheLineIsMinimumLineLengthLong()
        {
            var lines = compensatedDesign.Lines;
            Specify.That(lines.Count()).Should.BeEqualTo(2);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(14.5);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(15.5);
            Specify.That(lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(10);
        }

        [Then(@"the total number of lines should be (.*)")]
        public void ThenTheTotalNumberOfLinesShouldBe(int lineCount)
        {
            Specify.That(compensatedDesign.Lines.Count()).Should.BeEqualTo(lineCount);
        }

        [Then(@"a cut off line is added at design length")]
        public void ThenACutOffLineIsAddedAtDesignLength()
        {
            var lines = compensatedDesign.Lines;
            Specify.That(lines.Count()).Should.BeEqualTo(1);
            Specify.That(lines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(corrugateWidth);
            Specify.That(lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(design.Length);
            Specify.That(lines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(design.Length);
        }

        [Then(@"the length is same as on the rule applied design")]
        public void ThenTheLengthIsSameAsOnTheRuleAppliedDesign()
        {
            Specify.That(compensatedDesign.Length).Should.BeLogicallyEqualTo(design.Length);
        }

        [Then(@"the width is same as on the rule applied design")]
        public void ThenTheWidthIsSameAsOnTheRuleAppliedDesign()
        {
            Specify.That(compensatedDesign.Width).Should.BeLogicallyEqualTo(design.Width);
        }

        [Then(@"the vertical line is unchanged")]
        public void ThenTheVerticalLineIsUnchanged()
        {
            var lines = compensatedDesign.Lines;
            Specify.That(lines.Count()).Should.BeEqualTo(2);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(90);
        }

        [Then(@"all lines x coordinates are moved with 123")]
        public void ThenAllLinesXCoordinatesAreMovedWith123()
        {
            var lines = compensatedDesign.Lines;
            //// Horizontal cut line
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(138);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(208);
            Specify.That(lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(10);

            //// Vertical line
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(133);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(133);
            Specify.That(lines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(90);

            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(123);
            Specify.That(lines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(123 + corrugateWidth);
            Specify.That(lines.ElementAt(2).StartCoordinate.Y).Should.BeLogicallyEqualTo(design.Length);
            Specify.That(lines.ElementAt(2).EndCoordinate.Y).Should.BeLogicallyEqualTo(design.Length);
        }

        [Then(@"outside lines are shortened or removed leaving a total of (.*) lines")]
        public void ThenOutsideLinesAreShortenedOrRemovedLeavingATotalOfLines(int linesLeft)
        {
            var lines = compensatedDesign.Lines;

            foreach (var line in lines)
            {
                Console.WriteLine(
                    "line of type {0} start: ({1}, {2})" + " end: ({3}, {4})",
                    line.Type,
                    line.StartCoordinate.X,
                    line.StartCoordinate.Y,
                    line.EndCoordinate.X,
                    line.EndCoordinate.Y);
            }

            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(linesLeft);
        }

        [Then(@"line at index (.*) should start at (.*) and end at (.*) on x axis")]
        public void LineAtIndexShouldStartAtAndEndAtOnXAxis(int index, int xstart, int xend)
        {
            var lines = compensatedDesign.Lines;
            Specify.That(lines.ElementAt(index).StartCoordinate.X).Should.BeLogicallyEqualTo(xstart);
            Specify.That(lines.ElementAt(index).EndCoordinate.X).Should.BeLogicallyEqualTo(xend);
        }

        [Then(@"line at index (.*) should start at (.*) and end at (.*) on y axis")]
        public void LineAtIndexShouldStartAtAndEndAtOnYAxis(int index, int ystart, int yend)
        {
            var lines = compensatedDesign.Lines;
            Specify.That(lines.ElementAt(index).StartCoordinate.Y).Should.BeLogicallyEqualTo(ystart);
            Specify.That(lines.ElementAt(index).EndCoordinate.Y).Should.BeLogicallyEqualTo(yend);
        }

        [Then(@"at index (.*) a line from \((.*), (.*)\) to \((.*), (.*)\) with type (.*)")]
        public void AndAtIndexALineFromXytoXyWithType(int index, int xstart, int ystart, int xend, int yend, LineType lineType)
        {
            var line = compensatedDesign.Lines.ElementAt(index);
            Specify.That(line.Type).Should.BeLogicallyEqualTo(lineType);
            Specify.That(line.StartCoordinate.X).Should.BeLogicallyEqualTo(xstart);
            Specify.That(line.StartCoordinate.Y).Should.BeLogicallyEqualTo(ystart);
            Specify.That(line.EndCoordinate.X).Should.BeLogicallyEqualTo(xend);
            Specify.That(line.EndCoordinate.Y).Should.BeLogicallyEqualTo(yend);
        }

        [Then(@"the line edges connected to the corrugate edge are not compensated")]
        public void ThenTheLineEdgesConnectedToTheCorrugateEdgeAreNotCompensated()
        {
            var lines = compensatedDesign.Lines;

            /*Specify.That(lines.Count()).Should.BeLogicallyEqualTo(4);
            Specify.That(lines.ElementAt(0).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(0).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(95);
            Specify.That(lines.ElementAt(1).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(15);
            Specify.That(lines.ElementAt(1).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(200);
            Specify.That(lines.ElementAt(2).GetPhysicalStartCoordinateX()).Should.BeLogicallyEqualTo(15);
            Specify.That(lines.ElementAt(2).GetPhysicalEndCoordinateX()).Should.BeLogicallyEqualTo(85);*/
            ScenarioContext.Current.Pending();
        }

        [Then(@"the design should be center aligned")]
        public void ThenTheDesignShouldBeCenterAligned()
        {
            var lines = compensatedDesign.Lines;

            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(35);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(115);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(35);
            Specify.That(lines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(90);
        }

        [Then(@"the design should be right aligned")]
        public void ThenTheDesignShouldBeRightAligned()
        {
            var lines = compensatedDesign.Lines;

            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(60);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(140);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(60);
            Specify.That(lines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(10);
            Specify.That(lines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(90);
        }

        [Then(@"the line edges connected to the design are lengthend")]
        public void ThenTheLineEdgesConnectedToTheDesignAreLengthend()
        {
            var lines = compensatedDesign.Lines.GetHorizontalLines();

            foreach (var line in lines)
            {
                Console.WriteLine("start: " + line.StartCoordinate.X + " end: " + line.EndCoordinate.X);
            }

            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(4);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(100);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(245);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(165);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(300);
            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(165);
            Specify.That(lines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(235);
        }

        [Then(@"the line edges connected to the design are lengthend without track alignment")]
        public void ThenTheLineEdgesConnectedToTheDesignAreLengthendWithoutTrackAlignment()
        {
            var lines = compensatedDesign.Lines.GetHorizontalLines();

            foreach (var line in lines)
            {
                Console.WriteLine("start: " + line.StartCoordinate.X + " end: " + line.EndCoordinate.X);
            }

            Specify.That(lines.Count()).Should.BeLogicallyEqualTo(4);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(5);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(245);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(165);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(300);
            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(165);
            Specify.That(lines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(235);
        }

        [Then(@"the cut lines are shortened and the crease lines are lenghtened")]
        public void ThenTheCutLinesAreShortenedAndTheCreaseLinesAreLenghtened()
        {
            var lines = compensatedDesign.Lines.GetHorizontalLines();

            foreach (var line in lines)
            {
                Console.WriteLine("y: " + line.StartCoordinate.Y + "start: " + line.StartCoordinate.X + " end: " + line.EndCoordinate.X + " type: " + line.Type);
            }

            Specify.That(lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(55);
            Specify.That(lines.ElementAt(0).EndCoordinate.X).Should.BeLogicallyEqualTo(95);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(95);
            Specify.That(lines.ElementAt(1).EndCoordinate.X).Should.BeLogicallyEqualTo(155);
            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(155);
            Specify.That(lines.ElementAt(2).EndCoordinate.X).Should.BeLogicallyEqualTo(195);

            Specify.That(lines.ElementAt(3).StartCoordinate.Y).Should.BeLogicallyEqualTo(75);
            Specify.That(lines.ElementAt(3).StartCoordinate.X).Should.BeLogicallyEqualTo(55);
            Specify.That(lines.ElementAt(3).EndCoordinate.X).Should.BeLogicallyEqualTo(95);
            Specify.That(lines.ElementAt(4).StartCoordinate.X).Should.BeLogicallyEqualTo(95);
            Specify.That(lines.ElementAt(4).EndCoordinate.X).Should.BeLogicallyEqualTo(155);
            Specify.That(lines.ElementAt(5).StartCoordinate.X).Should.BeLogicallyEqualTo(155);
            Specify.That(lines.ElementAt(5).EndCoordinate.X).Should.BeLogicallyEqualTo(195);

            lines = compensatedDesign.Lines.GetVerticalLines();

            foreach (var line in lines)
            {
                Console.WriteLine("x: " + line.StartCoordinate.X + "start: " + line.StartCoordinate.Y + " end: " + line.EndCoordinate.Y + " type: " + line.Type);
            }

            Specify.That(lines.ElementAt(0).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(0).EndCoordinate.Y).Should.BeLogicallyEqualTo(100);
            Specify.That(lines.ElementAt(0).StartCoordinate.X).Should.BeLogicallyEqualTo(50);
            Specify.That(lines.ElementAt(0).Type).Should.BeLogicallyEqualTo(LineType.cut);
            Specify.That(lines.ElementAt(1).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(1).EndCoordinate.Y).Should.BeLogicallyEqualTo(100);
            Specify.That(lines.ElementAt(1).StartCoordinate.X).Should.BeLogicallyEqualTo(100);
            Specify.That(lines.ElementAt(1).Type).Should.BeLogicallyEqualTo(LineType.crease);
            Specify.That(lines.ElementAt(2).StartCoordinate.Y).Should.BeLogicallyEqualTo(0);
            Specify.That(lines.ElementAt(2).EndCoordinate.Y).Should.BeLogicallyEqualTo(100);
            Specify.That(lines.ElementAt(2).StartCoordinate.X).Should.BeLogicallyEqualTo(150);
            Specify.That(lines.ElementAt(2).Type).Should.BeLogicallyEqualTo(LineType.crease);
        }

        private void AddLineToDesign(MicroMeter xstart, MicroMeter ystart, MicroMeter xend, MicroMeter yend, LineType lineType)
        {
            var line = new PhysicalLine(new PhysicalCoordinate(xstart, ystart),
                new PhysicalCoordinate(xend, yend), lineType);
            design.Add(line);
        }
    }
}
