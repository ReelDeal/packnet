﻿Feature: Optimization Compensator
	In order to produce a design where the cuts are exactly the right length
	As an API user
	I want to get a design with lines compensated for knife and crease dimensions

Scenario: compensated design should have the same length and width as rule applied
	Given a rule applied design
	And a track
	And an optimization compensator
	When the design is compensated for optimization
	Then the length is same as on the rule applied design
	And the width is same as on the rule applied design

Scenario: should move lines so that positions are absolute positions on the machine
	Given a rule applied design
	And a track
	And an optimization compensator
	And a horizontal cut line
	And a vertical cut line
	And a corrugate width of 200
	And a track offset of 123
	When the design is compensated for optimization
	Then the total number of lines should be 2
	And line at index 0 should start at 133 and end at 213 on x axis
	And line at index 0 should start at 10 and end at 10 on y axis
	And line at index 1 should start at 133 and end at 133 on x axis
	And line at index 1 should start at 10 and end at 90 on y axis

Scenario: should remove lines at corrugate edges
	Given a rule applied design
	And a track
	And an optimization compensator
	And a horizontal cut line
	And a corrugate width of 200
	And lines at edges of the corrugate
	And one line at design edge on waste side
	When the design is compensated for optimization
	Then all lines at the corrugate edges are removed leaving a total of 2 lines

Scenario: should remove lines are close to the corrugate edges
	Given a rule applied design
	And a track
	And an optimization compensator
	And a horizontal cut line
	And a corrugate width of 152
	And a vertical cut line from (4, 0) to (4, 100)
	And a vertical cut line from (147, 0) to (147, 100)
	And one line at design edge on waste side
	And lines at edges of the corrugate
	And a minimum distance corrugate edge to vertical line of five
	When the design is compensated for optimization
	Then all lines at the corrugate edges are removed leaving a total of 1 lines

Scenario: should right align design when right side steering is fixed
	Given a rule applied design
	And a track
	And a track offset of 200
	And track has right side fixed
	And a horizontal cut line
	And a vertical cut line
	And an optimization compensator
	And a corrugate width of 200
	When the design is compensated for optimization
	Then the design should be right aligned