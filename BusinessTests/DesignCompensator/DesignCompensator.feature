﻿Feature: Design Compensator
	In order to produce a design where the cuts are exactly the right length
	As an API user
	I want to get a design with lines compensated for knife and crease dimensions

Scenario: make horizontal cut lines shorter
	Given a rule applied design
	And a track
	And a horizontal cut line
	And a cut compensation of five
	And a corrugate width of 200
	And a design compensator
	When the design is compensated
	Then the cut line is shortened according to the cut compensation

Scenario: attached horizontal crease lines should be lengthed if cut line is shortened
	Given a rule applied fefco design
	And a track
	And a cut compensation of five
	And a corrugate width of 200
	And a design compensator
	When the design is compensated
	Then the total number of lines should be 10
	And at index 0 a line from (0, 50) to (45, 50) with type cut
	And at index 1 a line from (45, 50) to (105, 50) with type crease
	And at index 2 a line from (105, 50) to (200, 50) with type cut
	And at index 3 a line from (0, 75) to (45, 75) with type cut
	And at index 4 a line from (45, 75) to (105, 75) with type crease
	And at index 5 a line from (105, 75) to (200, 75) with type cut
	And at index 6 a line from (0, 0) to (0, 150) with type cut
	And at index 7 a line from (50, 0) to (50, 150) with type crease
	And at index 8 a line from (100, 0) to (100, 150) with type crease
	And at index 9 a line from (0, 150) to (200, 150) with type cut

Scenario: Should always add cutlines between edge of design and edge of corrugate
	Given a rule applied design with three crease lines across the design width
	And a track
	And a crease compensation of 10	
	And a corrugate width of 300
	And a design compensator
	When the design is compensated
	Then the total number of lines should be 7
	And at index 0 a line from (0, 10) to (150, 10) with type crease
	And at index 1 a line from (150, 10) to (300, 10) with type cut
	And at index 2 a line from (0, 50) to (150, 50) with type crease
	And at index 3 a line from (150, 50) to (300, 50) with type cut
	And at index 4 a line from (0, 90) to (150, 90) with type crease
	And at index 5 a line from (150, 90) to (300, 90) with type cut
	And at index 6 a line from (0, 150) to (300, 150) with type cut

Scenario: make the tool create a mark for too short lines
	Given a rule applied design
	And a track
	And a design compensator
	And a cut compensation of five
	And a minimum line length of one
	And a corrugate width of 200
	And a horizontal line shorter or equal to the the two times the compensation
	When the design is compensated
	Then the line is minimum line length long

Scenario: compensated design should have the same length and width as rule applied
	Given a rule applied design
	And a track
	And a design compensator
	When the design is compensated
	Then the length is same as on the rule applied design
	And the width is same as on the rule applied design

Scenario: should add cut off line
	Given a rule applied design
	And a track
	And a design compensator
	And a corrugate width of 200
	When the design is compensated
	Then a cut off line is added at design length

Scenario: should not compensate vertical lines
	Given a rule applied design
	And a track
	And a design compensator
	And a vertical cut line
	And a cut compensation of five
	When the design is compensated
	Then the vertical line is unchanged

Scenario: should lengthen cut lines that start or end or design edge
	Given a rule applied design
	And a track 
	And a track offset of 300
	And a corrugate width of 200
	And a design compensator
	And a cut compensation of five
	And a horizontal cut line with its start connected to the left side of corrugate
	And a horizontal cut line with its end connected to the right side of corrugate
	And a horizontal cut line
	When the design is compensated
	Then the total number of lines should be 4
	And line at index 0 should start at 5 and end at 95 on x axis
	And line at index 1 should start at 15 and end at 195 on x axis
	And line at index 2 should start at 15 and end at 85 on x axis
	And line at index 3 should start at 300 and end at 500 on x axis
