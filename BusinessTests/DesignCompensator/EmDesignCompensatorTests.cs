﻿using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Business.DesignCompensator;
using PackNet.Common.Interfaces.DTO;
using StoryQ;
using Testing.Specificity;

namespace BusinessTests.DesignCompensator
{
    [TestClass]
    public class EmDesignCompensatorTests
    {
        private RuleAppliedPhysicalDesign design;
        private CompensationParameters compensationParameters;
        private EmDesignCompensator compensator;
        private PhysicalDesign compensatedDesign;

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotExtendCrossHeadLinesToCutWaste()
        {
            new Story("")
            .InOrderTo("Produce the design without unnecessary cross head movements")
            .AsA("User")
            .IWant("Don't want waste to be cut into pieces when i move the cross head")
            .WithScenario("Should not cut waste")
            .Given(ADesignWithHorizontalCutsAndCreases)
            .And(ADesignCompensator)
            .And(ASetOfCompensationParametersWithTrackOffsetAndCutAndCreaseCompensation, 0d, 5d, 3d)
            .When(ICompensateTheDesign)
            .Then(WasteCuttingLinesShouldNotBeAdded)
            .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCompensateVerticalAndHorizontalLines()
        {
            new Story("")
            .InOrderTo("Produce a design with correct vertical and horizontal lines")
            .AsA("User")
            .IWant("I want lines start and end position to be adjusted to handle radius of cut and crease tool")
            .WithScenario("Should adjust both vertical and horizontal lines")
            .Given(ADesignWithBothVerticalAndHorizontalLines)
            .And(ADesignCompensator)
            .And(ASetOfCompensationParametersWithTrackOffsetAndCutAndCreaseCompensation, 0d, 5d, 3d)
            .When(ICompensateTheDesign)
            .Then(VerticalLinesShouldBeCompensated)
            .And(HorizontalLinesShouldBeCompensated)
            .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUseTrackOffsetWhenDetermining_IfStartAndEndPosOfLineAreAtDesignEdges()
        {
            new Story("")
            .InOrderTo("To get a clear cut between box and waste")
            .AsA("User")
            .IWant("I don't want lines start and end to get compensated on design edges")
            .WithScenario("Should consider track offset when determining if start and end are at design edges")
            .Given(ADesignWithHorizontalCutsAndCreasesAtDesignEdge)
            .And(ADesignCompensator)
            .And(ASetOfCompensationParametersWithTrackOffsetAndCutAndCreaseCompensation, 100d, 5d, 3d)
            .When(ICompensateTheDesign)
            .Then(HorizontalLinesShouldNotBeCompensated)
            .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddCutOffLine()
        {
            new Story("")
            .InOrderTo("Get my design out from the machine without feeding corrugate from the bale")
            .AsA("User")
            .IWant("I want a cut off line at the end of the design")
            .WithScenario("Should add cut off line at end of design")
            .Given(ADesignWithHorizontalCutsAndCreases)
            .And(ADesignCompensator)
            .And(ASetOfCompensationParametersWithTrackOffsetAndCutAndCreaseCompensation, 0d, 5d, 3d)
            .When(ICompensateTheDesign)
            .Then(AnNonCompensatedCutOffLineIsAdded)
            .ExecuteWithReport(MethodBase.GetCurrentMethod());
        }

        private void ADesignCompensator()
        {
            compensator = new EmDesignCompensator();
        }

        private void ICompensateTheDesign()
        {
            compensatedDesign = compensator.Compensate(compensationParameters);
        }

        private void ASetOfCompensationParametersWithTrackOffsetAndCutAndCreaseCompensation(double trackoffset, double cutComp, double creaseComp)
        {
            compensationParameters = new CompensationParameters(design, false, trackoffset, 200, cutComp, creaseComp, 0, 0, 0, 0);
        }

        private void ADesignWithHorizontalCutsAndCreasesAtDesignEdge()
        {
            design = new RuleAppliedPhysicalDesign
            {
                Length = 150,
                Width = 150
            };

            var line = new PhysicalLine(
                new PhysicalCoordinate(100, 50), new PhysicalCoordinate(250, 50),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(100, 100), new PhysicalCoordinate(250, 100),
                LineType.cut);
            design.Add(line);
        }

        private void ADesignWithHorizontalCutsAndCreases()
        {
            design = new RuleAppliedPhysicalDesign
            {
                Length = 150,
                Width = 150
            };

            var line = new PhysicalLine(
                new PhysicalCoordinate(10, 50), new PhysicalCoordinate(140, 50),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(10, 100), new PhysicalCoordinate(140, 100),
                LineType.cut);
            design.Add(line);
        }

        private void ADesignWithBothVerticalAndHorizontalLines()
        {
            design = new RuleAppliedPhysicalDesign
            {
                Length = 150,
                Width = 150
            };

            var line = new PhysicalLine(
                new PhysicalCoordinate(10, 50), new PhysicalCoordinate(50, 50),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(
                new PhysicalCoordinate(50, 50), new PhysicalCoordinate(100, 50),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(
                new PhysicalCoordinate(100, 50), new PhysicalCoordinate(140, 50),
                LineType.crease);
            design.Add(line);

            line = new PhysicalLine(
                new PhysicalCoordinate(50, 10), new PhysicalCoordinate(50, 50),
                LineType.cut);
            design.Add(line);
            line = new PhysicalLine(
                new PhysicalCoordinate(50, 50), new PhysicalCoordinate(50, 100),
                LineType.crease);
            design.Add(line);
            line = new PhysicalLine(
                new PhysicalCoordinate(50, 100), new PhysicalCoordinate(50, 140),
                LineType.cut);
            design.Add(line);
        }

        private void VerticalLinesShouldBeCompensated()
        {
            var lines = compensatedDesign.GetVerticalPhysicalLines().ToList();
            Specify.That(lines.Count).Should.BeEqualTo(3);

            Specify.That(lines[0].Type).Should.BeEqualTo(LineType.cut);
            Specify.That(lines[0].StartCoordinate.X).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[0].EndCoordinate.X).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[0].StartCoordinate.Y).Should.BeLogicallyEqualTo(10 + compensationParameters.CrossHeadCutCompensation
);
            Specify.That(lines[0].EndCoordinate.Y).Should.BeLogicallyEqualTo(50 - compensationParameters.CrossHeadCutCompensation);

            Specify.That(lines[1].Type).Should.BeEqualTo(LineType.crease);
            Specify.That(lines[1].StartCoordinate.X).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[1].EndCoordinate.X).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[1].StartCoordinate.Y).Should.BeLogicallyEqualTo(50 - compensationParameters.CrossHeadCutCompensation);
            Specify.That(lines[1].EndCoordinate.Y).Should.BeLogicallyEqualTo(100 + compensationParameters.CrossHeadCutCompensation);

            Specify.That(lines[2].Type).Should.BeEqualTo(LineType.cut);
            Specify.That(lines[2].StartCoordinate.X).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[2].EndCoordinate.X).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[2].StartCoordinate.Y).Should.BeLogicallyEqualTo(100 + compensationParameters.CrossHeadCutCompensation);
            Specify.That(lines[2].EndCoordinate.Y).Should.BeLogicallyEqualTo(140 - compensationParameters.CrossHeadCutCompensation);
        }

        private void HorizontalLinesShouldNotBeCompensated()
        {
            var lines = compensatedDesign.GetHorizontalPhysicalLines().ToList();
            Specify.That(lines.Count).Should.BeEqualTo(3);

            Specify.That(lines[0].Type).Should.BeEqualTo(LineType.crease);
            Specify.That(lines[0].StartCoordinate.X).Should.BeLogicallyEqualTo(100);
            Specify.That(lines[0].EndCoordinate.X).Should.BeLogicallyEqualTo(250);
            Specify.That(lines[0].StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[0].EndCoordinate.Y).Should.BeLogicallyEqualTo(50);

            Specify.That(lines[1].Type).Should.BeEqualTo(LineType.cut);
            Specify.That(lines[1].StartCoordinate.X).Should.BeLogicallyEqualTo(100);
            Specify.That(lines[1].EndCoordinate.X).Should.BeLogicallyEqualTo(250);
            Specify.That(lines[1].StartCoordinate.Y).Should.BeLogicallyEqualTo(100);
            Specify.That(lines[1].EndCoordinate.Y).Should.BeLogicallyEqualTo(100);
        }

        private void HorizontalLinesShouldBeCompensated()
        {
            var lines = compensatedDesign.GetHorizontalPhysicalLines().ToList();
            Specify.That(lines.Count).Should.BeEqualTo(4);

            Specify.That(lines[0].Type).Should.BeEqualTo(LineType.crease);
            Specify.That(lines[0].StartCoordinate.X).Should.BeLogicallyEqualTo(10 + compensationParameters.CrossHeadCreaseCompensation);
            Specify.That(lines[0].EndCoordinate.X).Should.BeLogicallyEqualTo(50 + compensationParameters.CrossHeadCutCompensation);
            Specify.That(lines[0].StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[0].EndCoordinate.Y).Should.BeLogicallyEqualTo(50);

            Specify.That(lines[1].Type).Should.BeEqualTo(LineType.cut);
            Specify.That(lines[1].StartCoordinate.X).Should.BeLogicallyEqualTo(50 + compensationParameters.CrossHeadCutCompensation);
            Specify.That(lines[1].EndCoordinate.X).Should.BeLogicallyEqualTo(100 - compensationParameters.CrossHeadCutCompensation);
            Specify.That(lines[1].StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[1].EndCoordinate.Y).Should.BeLogicallyEqualTo(50);

            Specify.That(lines[2].Type).Should.BeEqualTo(LineType.crease);
            Specify.That(lines[2].StartCoordinate.X).Should.BeLogicallyEqualTo(100 - compensationParameters.CrossHeadCutCompensation);
            Specify.That(lines[2].EndCoordinate.X).Should.BeLogicallyEqualTo(140 - compensationParameters.CrossHeadCreaseCompensation);
            Specify.That(lines[2].StartCoordinate.Y).Should.BeLogicallyEqualTo(50);
            Specify.That(lines[2].EndCoordinate.Y).Should.BeLogicallyEqualTo(50);
        }

        private void AnNonCompensatedCutOffLineIsAdded()
        {
            var lines = compensatedDesign.Lines.ToList();
            Specify.That(lines.Count).Should.BeEqualTo(3);

            Specify.That(lines[2].Type).Should.BeEqualTo(LineType.cut);
            Specify.That(lines[2].StartCoordinate.X).Should.BeLogicallyEqualTo(compensationParameters.TrackOffset);
            Specify.That(lines[2].EndCoordinate.X).Should.BeLogicallyEqualTo(200);
            Specify.That(lines[2].StartCoordinate.Y).Should.BeLogicallyEqualTo(150);
            Specify.That(lines[2].EndCoordinate.Y).Should.BeLogicallyEqualTo(150);
        }

        private void WasteCuttingLinesShouldNotBeAdded()
        {
            var lines = compensatedDesign.Lines.ToList();
            Specify.That(lines.Count).Should.BeEqualTo(3);

            Specify.That(lines.Take(2).Any(l => l.EndCoordinate.X > compensatedDesign.Width)).Should.BeFalse();
        }
    }
}
