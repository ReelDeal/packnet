﻿
using Moq;

using PackNet.Common.Interfaces.Logging;

namespace ImporterTests
{
    using System;
    using System.IO;
    using PackNet.Importer;
    using Testing.Specificity;  
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class PluginComposerTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("4034")]
        [TestCategory("4036")]
        [TestCategory("4037")]
        [DeploymentItem(@"ValidCartonPlugin\BoxLast\BoxLastCartonImport.xaml", @"ValidPlugins\BoxLast\")]
        [DeploymentItem(@"ValidCartonPlugin\BoxFirst\BoxFirstCartonImport.xaml", @"ValidPlugins\BoxFirst\")]
        [DeploymentItem(@"ValidCartonPlugin\Order\empty.txt", @"ValidPlugins\Order\")]
        public void ShouldNotThrowExceptionIfTheWorkflowExist_AndThrowWhenItDoesnt()
        {
            ImporterPluginComposer composer = null;
            try
            {
                composer = new ImporterPluginComposer("ValidPlugins", new Mock<ILogger>().Object);
                Assert.Fail("Exception not thrown");
            }
            catch (FileNotFoundException ex)
            {
                Specify.That(composer).Should.BeNull();
                Specify.That(ex.Message).Should.Contain("The import workflow for Label does not exist.");
            }
            catch (Exception ex)
            {
                Assert.Fail("Wrong exception. Expected " + typeof(FileNotFoundException) + " but got " + ex.GetType());
            }
        }
    }
}
