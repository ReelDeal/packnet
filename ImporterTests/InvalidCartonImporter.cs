﻿namespace ImporterTests.InvalidCartonPlugin
{
    using System.ComponentModel.Composition;
    using System.IO;

    using PackNet.Common.Interfaces;
    using PackNet.Common.Interfaces.Importing;

    [Export(typeof(IWorkflowImporter))]
    public class InvalidCartonImporter : IWorkflowImporter
    {
        public string Name { get { return Enums.ImportTypes.Carton.ToString(); } }

        public string Workflow
        {
            get { return Path.Combine(Enums.ImportTypes.Carton.ToString(), "CartonImport.xaml"); }
        }
    }
}