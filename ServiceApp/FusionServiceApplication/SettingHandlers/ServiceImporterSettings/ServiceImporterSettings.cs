﻿using System;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ServiceImporterSettings
{
    [XmlRoot(ElementName = "ServiceImporterSettings", Namespace = "http://Packsize.com")]
    [Serializable]
    public class ServiceImporterSettings : Configuration<ServiceImporterSettings>, IConfigFile
    {
        public static ServiceImporterSettings GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("FieldDelimiter")]
        public int FieldDelimiter { get; set; }

        [XmlElement("Fields")]
        public string Fields { get; set; }

        [XmlElement("FieldsToIgnore")]
        public string FieldsToIgnore { get; set; }

        [XmlElement("Headers")]
        public bool Headers { get; set; }

        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            FieldDelimiter = reloadedConfig.FieldDelimiter;
            Fields = reloadedConfig.Fields;
            FieldsToIgnore = reloadedConfig.FieldsToIgnore;
            Headers = reloadedConfig.Headers;
        }
    }
}
