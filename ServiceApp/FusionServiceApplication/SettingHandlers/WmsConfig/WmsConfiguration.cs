﻿using System;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.WmsConfig
{
    [XmlRoot(ElementName = "WmsConfiguration", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class WmsConfiguration : Configuration<WmsConfiguration>, IConfigFile
    {
        public static WmsConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);   
        }

        [XmlElement("Enabled")]
        public bool Enabled { get; set; }

        [XmlElement("ExternalPrint")]
        public bool ExternalPrint { get; set; }

        [XmlElement("PrinterAlertPort")]
        public int PrinterAlertPort { get; set; }

        [XmlElement("PrinterPollingTimeout")]
        public int PrinterPollingTimeout { get; set; }

        [XmlElement("PrinterPollingTimes")]
        public int PrinterPollingTimes { get; set; }

        [XmlElement("ReportProduced")]
        public bool ReportProduced { get; set; }

        [XmlElement("SequenceNumberLowerBound")]
        public int SequenceNumberLowerBound { get; set; }

        [XmlElement("SequenceNumberUpperBound")]
        public int SequenceNumberUpperBound { get; set; }

        [XmlElement("UseSocketCommunication")]
        public bool UseSocketCommunication { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            Enabled = reloadedConfig.Enabled;
            ExternalPrint = reloadedConfig.ExternalPrint;
            PrinterAlertPort = reloadedConfig.PrinterAlertPort;
            PrinterPollingTimeout = reloadedConfig.PrinterPollingTimeout;
            PrinterPollingTimes = reloadedConfig.PrinterPollingTimes;
            ReportProduced = reloadedConfig.ReportProduced;
            SequenceNumberLowerBound = reloadedConfig.SequenceNumberLowerBound;
            SequenceNumberUpperBound = reloadedConfig.SequenceNumberUpperBound;
            UseSocketCommunication = reloadedConfig.UseSocketCommunication;
            Version = reloadedConfig.Version;
        }
    }
}
