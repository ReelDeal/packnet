﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.PickZonesConfiguration
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class PickZone
    {
        [XmlElement("Alias")]
        public string Alias { get; set; }

        [XmlElement("Id")]
        public string Id { get; set; }

        public override string ToString()
        {
            return Alias;
        }
    }
}
