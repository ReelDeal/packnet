﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.PickZonesConfiguration
{

    [XmlRoot(ElementName = "PickAreaProvider", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class PickZonesConfiguration : Configuration<PickZonesConfiguration>, IConfigFile
    {
        public static PickZonesConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("PickAreas")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PickAreas PickAreas { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

        protected override void ConsistencyCheck()
        {
        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            PickAreas = reloadedConfig.PickAreas;
            Version = reloadedConfig.Version;
        }
    }
}
