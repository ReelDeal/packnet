﻿namespace FusionServiceApplication.SettingHandlers.PickZonesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class PickZones
    {
        [XmlElement("PickZone")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<PickZone> PickZone { get; set; }
    }
}
