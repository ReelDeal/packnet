﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.PickZonesConfiguration
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class PickArea
    {
        [XmlElement("Alias")]
        public string Alias { get; set; }
    
        [XmlElement("id")]
        public string Id { get; set; }

        [XmlElement("PickZones")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PickZones PickZones { get; set; }

        public override string ToString()
        {
            return Alias;
        }
    }
}
