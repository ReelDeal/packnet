﻿
namespace FusionServiceApplication.SettingHandlers.PickZonesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class PickAreas
    {
        [XmlElement("PickArea")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<PickArea> PickArea { get; set; }
    }
}
