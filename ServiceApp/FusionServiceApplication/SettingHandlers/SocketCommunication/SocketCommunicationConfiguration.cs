﻿using System;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.SocketCommunication
{
    [XmlRoot(ElementName = "SocketCommunicatorSettings", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class SocketCommunicationConfiguration : Configuration<SocketCommunicationConfiguration>, IConfigFile
    {
        public static SocketCommunicationConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("AddressFamily")]
        public string AddressFamily { get; set; }

        [XmlElement("EncodingName")]
        public string EncodingName { get; set; }

        [XmlElement("Host")]
        public string Host { get; set; }

        [XmlElement("KeepAliveInterval")]
        public int KeepAliveInterval { get; set; }

        [XmlElement("NumberOfRetries")]
        public int NumberOfRetries { get; set; }

        [XmlElement("Port")]
        public int Port { get; set; }

        [XmlElement("ProtocolType")]
        public string ProtocolType { get; set; }

        [XmlElement("ReceiveTimeout")]
        public int ReceiveTimeout { get; set; }

        [XmlElement("SocketType")]
        public string SocketType { get; set; }
        
        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            AddressFamily = reloadedConfig.AddressFamily;
            EncodingName = reloadedConfig.EncodingName;
            Host = reloadedConfig.Host;
            KeepAliveInterval = reloadedConfig.KeepAliveInterval;
            NumberOfRetries = reloadedConfig.NumberOfRetries;
            Port = reloadedConfig.Port;
            ProtocolType = reloadedConfig.ProtocolType;
            ReceiveTimeout = reloadedConfig.ReceiveTimeout;
            SocketType = reloadedConfig.SocketType;
           
        }
    }
}
