﻿namespace FusionServiceApplication.SettingHandlers
{
    public interface IConfigFile
    {
        void Serialize(string path);

        void Reload(string path);
    }
}
