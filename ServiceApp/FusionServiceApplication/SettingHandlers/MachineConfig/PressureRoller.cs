﻿using System.ComponentModel;

namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class PressureRoller
    {
        [XmlElement("leftRoller")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public RollerInstance LeftRoller { get; set; }

        [XmlElement("rightRoller")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public RollerInstance RightRoller { get; set; }
    }
}
