﻿namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Xml.Serialization;

    [Serializable]
    public class SwordsensorCompensations
    {
        [XmlElement("compensation")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [Browsable(false)]
        public Compensation[] Compensation { get; set; }

        [XmlIgnore]
        [Browsable(false)]
        public double[] Compensations
        {
            get
            {
                return Compensation.Select(compensation => double.Parse(compensation.Value, CultureInfo.InvariantCulture)).ToArray();
            }

            set
            {
                for (var i = 0; i < Math.Min(Compensation.Length, value.Length); i++)
                {
                    Compensation[i].Value = value[i].ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation1
        {
            get
            {
                return Compensation.Any() ? Compensation[0] : null;
            }

            set
            {
                InsertNewCompensationn(value, 1);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation2
        {
            get
            {
                return Compensation.Length > 1 ? Compensation[1] : null;
            }

            set
            {
                InsertNewCompensationn(value, 2);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation3
        {
            get
            {
                return Compensation.Length > 2 ? Compensation[2] : null;
            }

            set
            {
                InsertNewCompensationn(value, 3);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation4
        {
            get
            {
                return Compensation.Length > 3 ? Compensation[3] : null;
            }

            set
            {
                InsertNewCompensationn(value, 4);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation5
        {
            get
            {
                return Compensation.Length > 4 ? Compensation[4] : null;
            }

            set
            {
                InsertNewCompensationn(value, 5);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation6
        {
            get
            {
                return Compensation.Length > 5 ? Compensation[5] : null;
            }

            set
            {
                InsertNewCompensationn(value, 6);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation7
        {
            get
            {
                return Compensation.Length > 6 ? Compensation[6] : null;
            }

            set
            {
                InsertNewCompensationn(value, 7);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation8
        {
            get
            {
                return Compensation.Length > 7 ? Compensation[7] : null;
            }

            set
            {
                InsertNewCompensationn(value, 8);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation9
        {
            get
            {
                return Compensation.Length > 8 ? Compensation[8] : null;
            }

            set
            {
                InsertNewCompensationn(value, 8);
            }
        }

        [XmlIgnore]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Compensation Compensation10
        {
            get
            {
                return Compensation.Length > 9 ? Compensation[9] : null;
            }

            set
            {
                InsertNewCompensationn(value, 10);
            }
        }

        public void InsertNewCompensationn(Compensation newCompensation, int compensationNumber)
        {
            if (newCompensation != null)
            {
                newCompensation.Speed = compensationNumber * 10;
            }

            var compensations = Compensation;
            Compensation = new Compensation[10];

            for (var i = 0; i < compensations.Count(); i++)
            {
                Compensation[i] = compensations[i];
            }

            Compensation[compensationNumber - 1] = newCompensation;
        }
    }
}