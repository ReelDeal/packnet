﻿
namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    public class OnTheFly
    {
        [XmlAttribute("active")]
        [Description("Activates/Deactivates the on the fly functionallity on the machine")]
        public bool Active { get; set; }
    }
}
