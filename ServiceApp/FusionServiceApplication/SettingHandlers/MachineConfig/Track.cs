﻿using System.ComponentModel;

namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class Track
    {
        [XmlAttribute("number")]
        public int Number { get; set; }

        [XmlAttribute("rightSideFixed")]
        public bool RightSideFixed { get; set; }

        [XmlAttribute("outOfCorrugatePosition")]
        public double OutOfCorrugatePosition { get; set; }

        [XmlAttribute("leftSensorPlateToCorrugate")]
        public double LeftSensorPlateToCorrugate { get; set; }

        [XmlAttribute("rightSensorPlateToCorrugate")]
        public double RightSensorPlateToCorrugate { get; set; }

        [XmlElement("leftswordsensorCompensations")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public SwordsensorCompensations LeftswordsensorCompensations { get; set; }

        [XmlElement("rightswordsensorCompensations")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public SwordsensorCompensations RightswordsensorCompensations { get; set; }
    }
}
