﻿
using System.ComponentModel;

namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;
    
    [Serializable]
    public class Crosshead
    {
        [XmlAttribute(AttributeName = "minimumPosition")]
        public double CrossheadMinimumPosition { get; set; }

        [XmlAttribute(AttributeName = "maximumPosition")]
        public double CrossheadMaximumPosition { get; set; }

        [XmlAttribute("width")]
        [Description("The width of the crosshead housing")]
        public double Width { get; set; }

        [XmlAttribute("leftSideToTool")]
        public double LeftSideToTool { get; set; }

        [XmlAttribute(AttributeName = "longHeadSensorToTool")]  
        public double LongheadSensorToTool { get; set; }

        [XmlAttribute(AttributeName = "trackSensorToTool")]
        public double TrackSensorToTool { get; set; }

        [XmlAttribute("creaseWheelOffset")]
        public double CreaseWheelOffset { get; set; }

        [XmlAttribute(AttributeName = "speed")]
        public double CrossheadSpeedInPercent { get; set; }

        [XmlAttribute(AttributeName = "acceleration")]
        public double CrossheadAccelerationInPercent { get; set; }

        [XmlAttribute(AttributeName = "maximumSpeed")]
        public double CrossheadMaximumSpeedInUnits { get; set; }

        [XmlAttribute(AttributeName = "maximumAcceleration")]
        public double CrossheadMaximumAccelerationInUnits { get; set; }

        [XmlAttribute(AttributeName = "maximumDeceleration")]
        public double CrossheadMaximumDecelerationInUnits { get; set; }
    
        [XmlAttribute("knifeActivationDelay")]
        public int KnifeActivationDelay { get; set; }

        [XmlAttribute("knifeDeactivationDelay")]
        public int KnifeDeactivationDelay { get; set; }

        [XmlAttribute("cutCompensation")]
        public double CutCompensation { get; set; }

        [XmlAttribute("creaseCompensation")]
        public double CreaseCompensation { get; set; }

        [XmlAttribute("minimumLineLength")]
        public double MinimumLineLength { get; set; }

        [XmlAttribute(AttributeName = "homingPosition")]
        public double HomingPosition { get; set; }

        [XmlAttribute(AttributeName = "scaling")]
        public double Scaling { get; set; }

    }
}
