﻿
namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class EventNotifier
    {

        [XmlAttribute("address")] 
        public string Address { get; set; }

        [XmlAttribute("port")]
        public string Port { get; set; }
    }
}
