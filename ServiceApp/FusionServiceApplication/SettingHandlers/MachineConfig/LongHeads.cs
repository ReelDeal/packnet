﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    [Serializable]
    public class LongHeads
    {
        [XmlAttribute("minimumPosition")]
        public double MinimumPosition { get; set; }

        [XmlAttribute("maximumPosition")]
        public double MaximumPosition { get; set; }

        [XmlAttribute("connectionDelay")]
        public int ConnectionDelay { get; set; }

        [XmlAttribute("width")]
        public double Width { get; set; }

        [XmlAttribute("leftSideToSensorPin")]
        public double LeftSideToSensorPin { get; set; }

        [XmlAttribute("positioningSpeed")]
        public double PositioningSpeed { get; set; }
        
        [XmlAttribute("positioningAcceleration")]
        public double PositioningAcceleration { get; set; }

        [XmlElement("longhead")]
        [Browsable(false)]
        public Longhead[] Longhead { get; set; }


        [XmlIgnore]
        [Browsable(true), ReadOnly(false), TypeConverter(typeof(ExpandableObjectConverter))]
        public Longhead Longhead1
        {
            get
            {
                return Longhead.Any() ? Longhead[0] : null;
            }
            set
            {
                InsertNewLonghead(value, 1);
            }
        }

        [XmlIgnore]
        [Browsable(true), ReadOnly(false), TypeConverter(typeof(ExpandableObjectConverter))]
        public Longhead Longhead2
        {
            get
            {
                return Longhead.Count() > 1 ? Longhead[1] : null;
            }
            set
            {
                InsertNewLonghead(value, 2);
            }
        }

        [XmlIgnore]
        [Browsable(true), ReadOnly(false), TypeConverter(typeof(ExpandableObjectConverter))]
        public Longhead Longhead3
        {
            get
            {
                return Longhead.Count() > 2 ? Longhead[2] : null;
            }
            set
            {
                InsertNewLonghead(value, 3);
            }
        }

        [XmlIgnore]
        [Browsable(true), ReadOnly(false), TypeConverter(typeof(ExpandableObjectConverter))]
        public Longhead Longhead4
        {
            get
            {
                return Longhead.Count() > 3 ? Longhead[3] : null;
            }
            set
            {
                InsertNewLonghead(value, 4);
            }
        }

        [XmlIgnore]
        [Browsable(true), ReadOnly(false), TypeConverter(typeof(ExpandableObjectConverter))]
        public Longhead Longhead5
        {
            get
            {
                return Longhead.Count() > 4 ? Longhead[4] : null;
            }
            set
            {
                InsertNewLonghead(value, 5);
            }
        }

        [XmlIgnore]
        [Browsable(true), ReadOnly(false), TypeConverter(typeof(ExpandableObjectConverter))]
        public Longhead Longhead6
        {
            get
            {
                return Longhead.Count() > 5 ? Longhead[5] : null;
            }
            set
            {
                InsertNewLonghead(value, 6);
            }
        }

        [XmlIgnore]
        [Browsable(true), ReadOnly(false), TypeConverter(typeof(ExpandableObjectConverter))]
        public Longhead Longhead7
        {
            get
            {
                return Longhead.Count() > 6 ? Longhead[6] : null;
            }
            set
            {
                InsertNewLonghead(value, 7);
            }
        }

        public void InsertNewLonghead(Longhead newLonghead, int longheadNumber)
        {
            if(newLonghead != null)
                newLonghead.Number = longheadNumber;

            var longheads = Longhead;
            Longhead = new Longhead[7];
            
            for (var i = 0; i < longheads.Count(); i++)
            {
                Longhead[i] = longheads[i];
            }
            Longhead[longheadNumber - 1] = newLonghead;
        }
    
    }
}
