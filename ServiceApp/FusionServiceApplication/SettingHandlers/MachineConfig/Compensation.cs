﻿
namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel;

    [Serializable]
    public class Compensation
    {
        [ReadOnly(true), Browsable(true)]
        [XmlAttribute("speed")]
        public int Speed { get; set; }
        
        [XmlText]
        public string Value { get; set; }
    }
}
