﻿
namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class FeedRoller
    {
        [XmlAttribute(AttributeName = "outfeedLength")]
        public double OutfeedLength { get; set; }

        [XmlAttribute(AttributeName = "loweringOffset")]
        public double LoweringOffset { get; set; }

        [XmlAttribute(AttributeName = "cleancutlength")]
        public double CleanCutLength { get; set; }

        [XmlAttribute(AttributeName = "speed")]
        public double SpeedInPercent { get; set; }

        [XmlAttribute(AttributeName = "acceleration")]
        public double RollerAccelerationInPercent { get; set; }
        
        [XmlAttribute(AttributeName = "scaling")]
        public double Scaling { get; set; }

        [XmlAttribute(AttributeName = "toolsGearRatio")]
        public double ToolsGearRatio { get; set; }
    }
}
