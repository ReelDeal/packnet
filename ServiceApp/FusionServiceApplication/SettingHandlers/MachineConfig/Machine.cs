﻿
namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "machine")]
    [Serializable]
    public class Machine : Configuration<Machine>, IConfigFile
    {
        static Machine()
        {
            DefaultFilename = DefaultFilePath;
        }

        public static string DefaultFilePath
        {
            get
            {
                return CoreSettings.Instance.PhysicalMachineSettingsPath;
            }
        }

        [XmlAttribute("isRightSideOffset")]
        [Description("Is machine right sided")]
        public bool IsMachineRightOffset { get; set; }

        [XmlElement("onthefly")]
        [Description("Activates/Deactivates on the fly functionallity on the machine")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [ReadOnly(true), Browsable(true)]
        public OnTheFly OnTheFly { get; set; }


        [XmlElement("crosshead")]
        [Description("Crosshead settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [ReadOnly(true), Browsable(true)]
        public Crosshead Crosshead { get; set; }

        [XmlElement("longheads")]
        [Description("Longheads settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [ReadOnly(true), Browsable(true)]
        public LongHeads LongHeads { get; set; }

        [XmlElement("tracks")]
        [Description("Track settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [ReadOnly(true), Browsable(true)]
        public Tracks Tracks { get; set; }

        [XmlElement("feedroller")]
        [Description("Feedroller settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [ReadOnly(true), Browsable(true)]
        public FeedRoller FeedRoller { get; set; }

        [XmlElement("pressurerollers")]
        [Description("Pressure roller settings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [ReadOnly(true), Browsable(true)]
        public PressureRoller PressureRoller { get; set; }


        public static Machine GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        public void Reload(string path)
        {
            Reload();
        }

        protected static new Machine Deserialize()
        {
            return Deserialize(DefaultFilePath);
        }


        protected override void ConsistencyCheck()
        {
            
        }
    }
}
