﻿using System.ComponentModel;

namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class Tracks
    {
        [XmlAttribute("trackActivateDelay")]
        public int TrackActivationDelay { get; set; }

        [XmlAttribute("trackSideSteeringTolerance")]
        public double TrackSideSteeringTolerance { get; set; }

        [XmlAttribute("trackOffsetTolerance")]
        public double TrackOffsetTolerance { get; set; }

        [XmlAttribute("trackLatchDistance")]
        public double TrackLatchDistance { get; set; }

        [XmlElement("track")]
        [Browsable(false)]
        public Track[] Track { get; set; }


        [Browsable(true),ReadOnly(true), TypeConverter(typeof(ExpandableObjectConverter))]
        public Track Track1
        {
            get
            {
                return Track[0];
            }
        }

        [Browsable(true), ReadOnly(true), TypeConverter(typeof(ExpandableObjectConverter))]
        public Track Track2
        {
            get
            {
                return Track[1];
            }
        }
    }
}
