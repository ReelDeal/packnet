﻿

namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel;

    using Utilities.Enums;

    [Serializable]
    public class Longhead
    {
        [XmlAttribute("number")]
        [ReadOnly(true), Browsable(true)]
        public int Number { get; set; }

        [XmlAttribute("leftSideToTool")]
        public double LeftSideToTool { get; set; }

        [XmlAttribute("type")]
        public LongheadType Type { get; set; }
    }
}
