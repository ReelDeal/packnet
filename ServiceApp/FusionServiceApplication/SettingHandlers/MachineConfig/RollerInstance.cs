﻿namespace FusionServiceApplication.SettingHandlers.MachineConfig
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class RollerInstance
    {
        [XmlAttribute("disconnectPosition")]
        public double DisconnectPosition { get; set; }
    }
}
