﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers
{
    using Utilities.Logging;

    [Serializable]
    public abstract class Configuration<T> where T : Configuration<T>, new()
    {
        private static T instance;
        private static FileSystemWatcher configFileWatcher;
        private static object guard = new object();
        private static object serializationGuard = new object();

        public static object Guard
        {
            get { return guard; }
        }

        [DebuggerHidden]
        public static T Instance
        {
            get
            {
                lock (guard)
                {
                    if (instance == null)
                    {
                        MakeSureStaticFieldsAreInitilizad();
                        instance = Deserialize();
                        SetupConfigfileWatcher();
                    }

                    return instance;
                }
            }

            protected set
            {
                instance = value;
            }
        }

        protected static string DefaultFilename { get; set; }

        public static T Deserialize(string path)
        {
            XmlSerializer mySerializer;
            T config;

            mySerializer = new XmlSerializer(typeof(T));

            lock (serializationGuard)
            {
                if (File.Exists(path))
                {
                    FileInfo f = new FileInfo(path);
                    XmlTextReader fs = new XmlTextReader(path);
                    try
                    {
                        config = (T)mySerializer.Deserialize(fs);
                        mySerializer = null;
                        fs.Close();
                    }
                    catch (Exception e)
                    {
                        fs.Close();
                        throw e;
                    }

                    config.ConsistencyCheck();
                }
                else
                {
                    config = new T();
                    config.Serialize();
                }
            }

            DefaultFilename = path;
            return config;
        }

        public void Serialize()
        {
            Serialize(DefaultFilename);
        }

        public void Serialize(string filename)
        {
            Thread t = new Thread(PerformSerialize);
            t.IsBackground = true;
            t.Start(filename);
        }

        public void SynchronousSerialize(string filename)
        {
            PerformSerialize(filename);
        }

        protected static T Deserialize()
        {
            return Deserialize(DefaultFilename);
        }

        protected abstract void ConsistencyCheck();

        private static void MakeSureStaticFieldsAreInitilizad()
        {
            T t = new T();
        }

        private static void OnConfigChanged(object sender, FileSystemEventArgs e)
        {
            instance = Deserialize();
        }

        private static void SetupConfigfileWatcher()
        {
            configFileWatcher = new FileSystemWatcher(".", DefaultFilename);
            configFileWatcher.Changed += new FileSystemEventHandler(OnConfigChanged);
            configFileWatcher.EnableRaisingEvents = true;
        }

        private void PerformSerialize(object filename)
        {
            lock (serializationGuard)
            {
                try
                {
                    if (configFileWatcher != null)
                    {
                        configFileWatcher.EnableRaisingEvents = false;
                    }

                    var s = new XmlSerializer(typeof(T));
                    TextWriter w = new StreamWriter((string)filename);
                    s.Serialize(w, this);
                    w.Close();
                    w.Dispose();

                    if (configFileWatcher != null)
                    {
                        configFileWatcher.WaitForChanged(WatcherChangeTypes.Changed, 1000);
                        configFileWatcher.EnableRaisingEvents = true;
                    }
                }
                catch (IOException ex)
                {
                    Logging.WriteToLogFile(ex.ToString());
                }
            }
        }
    }
}
