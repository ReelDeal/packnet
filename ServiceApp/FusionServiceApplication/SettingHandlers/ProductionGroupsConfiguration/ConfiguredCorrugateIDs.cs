﻿namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    public class ConfiguredCorrugateIds
    {
        [XmlElement("Corrugate")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ItemCollection<ProductionGroupCorrugate> Corrugate { get; set; }
    }
}
