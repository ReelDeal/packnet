﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ProductionGroupCorrugate
    {
        [XmlElement("Id")]
        public string Id { get; set; }

        public override string ToString()
        {
            return Id;
        }
    }
}
