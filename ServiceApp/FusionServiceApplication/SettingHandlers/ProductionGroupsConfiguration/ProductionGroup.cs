﻿
namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ProductionGroup
    {
        [XmlElement("Alias")]
        public string Alias { get; set; }

        [XmlElement("MaximumTrailingQuantity")]
        public int MaximumTrailingQuantity { get; set; }

        [XmlElement("Machines")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ProductionGroupMachines Machines { get; set; }

        [XmlElement("ProductionItems")]
        public ProductionItems ProductionItems { get; set; }

        [XmlElement("ConfiguredCorrugateIDs")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ConfiguredCorrugateIds ConfiguredCorrugateIds { get; set; }

        public override string ToString()
        {
            return Alias;
        }
    }

}
