﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ProductionItems
    {
        [XmlElement("ProductionItem")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<ProductionItem> ProductionItem { get; set; }
    }
}
