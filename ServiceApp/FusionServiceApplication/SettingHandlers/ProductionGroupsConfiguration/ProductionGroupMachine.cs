﻿
namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ProductionGroupMachine
    {
        [XmlElement("Id")]
        public string Id { get; set; }

        public override string ToString()
        {
            return Id;
        }
    }
}
