﻿
namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    public class ProductionGroupMachines
    {
        [XmlElement("Machine")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<ProductionGroupMachine> Machine { get; set; }
    }
}
