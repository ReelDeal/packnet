﻿namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ProductionGroups
    {
        [XmlElement("ProductionGroup")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<ProductionGroup> ProductionGroup { get; set; }
    }
}
