﻿namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ProductionItem
    {
        [XmlElement("CartonPropertyGroup")]
        public string CartonPropertyGroup { get; set; }

        [XmlElement("MixMultiplier")]
        public double MixMultiplier { get; set; }

        [XmlElement("MixQuantity")]
        public double MixQuantity { get; set; }

        public override string ToString()
        {
            return CartonPropertyGroup;
        }
    }
}
