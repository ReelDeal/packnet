﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ProductionGroupsConfiguration
{
    [XmlRoot(ElementName = "ProductionGroupsProvider", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class ProductionGroupsConfiguration : Configuration<ProductionGroupsConfiguration>, IConfigFile
    {
        public static ProductionGroupsConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("ProductionGroups")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ProductionGroups ProductionGroups { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

        protected override void ConsistencyCheck()
        {
        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            ProductionGroups = reloadedConfig.ProductionGroups;
            Version = reloadedConfig.Version;
        }
    }
}
