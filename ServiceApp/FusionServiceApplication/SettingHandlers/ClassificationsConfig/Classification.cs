﻿namespace FusionServiceApplication.SettingHandlers.ClassificationsConfig
{
    using System;
    using System.Xml.Serialization;
    using System.ComponentModel;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Classification
    {

        [XmlElement("Alias")]
        public string Alias { get; set; }

        [XmlElement("Number")]
        public string Number { get; set; }

        [XmlElement("PriorityLabel")]
        public bool PriorityLabel { get; set; }

        [XmlElement("Status")]
        public string Status { get; set; }

        public override string ToString()
        {
            return Alias;
        }
    }
}
