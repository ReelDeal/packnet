﻿namespace FusionServiceApplication.SettingHandlers.ClassificationsConfig
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "ClassificationsProvider", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class ClassificationsConfig : Configuration<ClassificationsConfig>, IConfigFile
    {
        public static ClassificationsConfig GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("Classifications")]
        public Classifications Classifications { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

        protected override void ConsistencyCheck()
        {
            
        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            Classifications = reloadedConfig.Classifications;
            Version = reloadedConfig.Version;
        }
    }
}
