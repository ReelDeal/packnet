﻿namespace FusionServiceApplication.SettingHandlers.ClassificationsConfig
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Classifications
    {
        [XmlElement("Classification")]
        [TypeConverter(typeof(ExpandableObjectConverter)), Browsable(true)]
        public ItemCollection<Classification> Classificaion { get; set; } 
    }
}
