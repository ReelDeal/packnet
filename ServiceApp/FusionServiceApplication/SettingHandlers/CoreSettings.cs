﻿using System;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers
{
    [XmlRoot]
    [Serializable]
    public class CoreSettings : Configuration<CoreSettings>
    {
        static CoreSettings()
        {
            DefaultFilename = DefaultFilePath;
        }

        [ReadOnly(true)]
        public string EventNotifierAdress { get; set; }

        [ReadOnly(true)]
        public string EventNotifierPort { get; set; }

        [ReadOnly(true)]
        public string PhysicalMachineSettingsPath { get; set; }

        [ReadOnly(true)]
        public string MachineManagerDataFolderPath { get; set; }

        [ReadOnly(true)]
        public string DefaultConnectionAddress { get; set; }

        protected static new CoreSettings Deserialize()
        {
            return Deserialize(DefaultFilePath);
        }

        public static string DefaultFilePath {
            get
            {
                return Path.Combine(Directory.GetCurrentDirectory(), "XmlFiles", "Settings",
                    "IqFusionServiceAppSettings.xml");
            }
        }

        public static CoreSettings GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {
        
        }
    }
}
