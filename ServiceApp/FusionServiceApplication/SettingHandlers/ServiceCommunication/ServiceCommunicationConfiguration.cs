﻿using System;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ServiceCommunication
{
    [XmlRoot(ElementName = "ServiceCommunicatorSettings", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class ServiceCommunicationConfiguration : Configuration<ServiceCommunicationConfiguration>, IConfigFile
    {

        public static ServiceCommunicationConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("Url")]
        public string Url { get; set; }

       
        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            Url = reloadedConfig.Url;
        }
    }
}
