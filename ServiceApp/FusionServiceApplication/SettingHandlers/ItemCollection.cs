﻿namespace FusionServiceApplication.SettingHandlers
{
    using System;
    using System.Collections;
    using System.ComponentModel;
    using System.Text;

    public class ItemCollection<T> : CollectionBase, ICustomTypeDescriptor where T : new()
    {
        public void Add(T item)
        {
            List.Add(item);
        }

        public void Remove(T item)
        {
            List.Remove(item);
        }

        public T this[int index]
        {
            get
            {
              /*  if (!Compare(this.List[Count - 1], new T()))
                {
                    this.Add(new T());

                    //TODO HÄR MÅSTE EN UPDATE SKE
                }*/

                return (T)List[index];
            }
        }

        #region ICustomTypeDescriptor impl

        public String GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }

        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }

        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            return GetProperties();
        }

        public PropertyDescriptorCollection GetProperties()
        {
            var pds = new PropertyDescriptorCollection(null);

        /*    if (Compare(this.List[this.List.Count - 1],new object()))
                this.List.Add(new object());*/

            for (var i = 0; i < List.Count; i++)
            {
                var pd = new ItemCollectionPropertyDescriptor<T>(this, i);
                pds.Add(pd);
            }
            return pds;
        }

        static bool Compare<T1>(T1 x, T1 y) where T1 : class
        {
            return x.Equals(y);
        }

        #endregion
    }

    [TypeConverter(typeof(ExpandableObjectConverter)), Browsable(true)]
    public class ItemCollectionPropertyDescriptor<T> : PropertyDescriptor where T : new()
    {
        private readonly ItemCollection<T> collection;
        private readonly int index = -1;

        public ItemCollectionPropertyDescriptor(ItemCollection<T> coll, int idx) :
            base("#" + idx, null)
        {
            collection = coll;
            index = idx;
        }

        public override AttributeCollection Attributes
        {
            get
            {
                return new AttributeCollection(null);
            }
        }

        public override bool CanResetValue(object component)
        {
            return true;
        }

        public override Type ComponentType
        {
            get
            {
                return collection.GetType();
            }
        }

        public override string DisplayName
        {
            get
            {
                var item = collection[index];
                return item.ToString();
            }
        }

        public override string Description
        {
            get
            {
                var item = collection[index];
                var sb = new StringBuilder();
                sb.Append(item);
                return sb.ToString();
            }
        }

        public override object GetValue(object component)
        {
            return collection[index];
        }

        public override bool IsReadOnly
        {
            get { return false; }
        }

        public override string Name
        {
            get { return collection[index].ToString(); }
        }

        public override Type PropertyType
        {
            get { return collection[index].GetType(); }
        }

        public override void ResetValue(object component)
        {
        }

        public override bool ShouldSerializeValue(object component)
        {
            return true;
        }

        public override void SetValue(object component, object value)
        {
            //   this.collection[index] = value;
        }
    }
}
