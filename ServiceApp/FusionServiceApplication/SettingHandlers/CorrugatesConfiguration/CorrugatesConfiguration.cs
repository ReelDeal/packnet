﻿using System.ComponentModel;

namespace FusionServiceApplication.SettingHandlers.CorrugatesConfiguration
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "CorrugateSupplier", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class CorrugatesConfiguration : Configuration<CorrugatesConfiguration>, IConfigFile
    {
        public static CorrugatesConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("Corrugates")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Corrugates Corrugates { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            Corrugates = reloadedConfig.Corrugates;
            Version = reloadedConfig.Version;
        }
    }
}
