﻿namespace FusionServiceApplication.SettingHandlers.CorrugatesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    public class Corrugates
    {
        [XmlElement("Corrugate")]
        [TypeConverter(typeof(ExpandableObjectConverter)), Browsable(true)]
        public ItemCollection<Corrugate> Corrugate { get; set; }
    }
}
