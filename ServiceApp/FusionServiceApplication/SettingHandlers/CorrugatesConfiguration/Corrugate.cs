﻿namespace FusionServiceApplication.SettingHandlers.CorrugatesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Corrugate
    {
        [XmlElement("Alias")]
        public string Alias { get; set; }

        [XmlElement("Id")]
        public string Id { get; set; }
        
        [XmlElement("Width")]
        public double Width { get; set; }

        [XmlElement("Thickness")]
        public double Thickness { get; set; }

        [XmlElement("Quality")]
        public int Quality { get; set; }

        public override string ToString()
        {
            return Alias;
        }
    }
}
