﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.CartonPropertyGroupConfiguration
{
    [XmlRoot(ElementName = "CartonPropertyGroupProvider", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class CartonPropertyGroupConfiguration : Configuration<CartonPropertyGroupConfiguration>, IConfigFile
    {
        public static CartonPropertyGroupConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            var newInstance = Deserialize(path);
            return newInstance;
        }

        [XmlElement("CartonPropertyGroups")]
        [Browsable(true), TypeConverter(typeof(ExpandableObjectConverter)), ReadOnly(true)]
        public CartonPropertyGroups CartonPropertyGroups { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            CartonPropertyGroups = reloadedConfig.CartonPropertyGroups;
            Version = reloadedConfig.Version;
        }
    }
}
