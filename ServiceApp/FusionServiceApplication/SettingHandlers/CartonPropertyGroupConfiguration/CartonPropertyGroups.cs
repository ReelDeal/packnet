﻿namespace FusionServiceApplication.SettingHandlers.CartonPropertyGroupConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    public class CartonPropertyGroups
    {
        [XmlElement("CartonPropertyGroup")]
        [Browsable(true), TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<CartonPropertyGroup> CartonPropertyGroupsCollection
        {
            get;
            set;
        }
    }
}
