﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.CartonPropertyGroupConfiguration
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class CartonPropertyGroup
    {
        [XmlElement("Alias")]
        public string Alias { get; set; }

        [XmlElement("Condition")]
        public string Condition { get; set; }

        [XmlElement("DefaultMixQuantity")]
        public double DefaultMixQuantity { get; set; }

        [XmlElement("Status")]
        public string Status { get; set; }

        [XmlElement("SubstitutionGroup")]
        public string SubstitutionGroup { get; set; }

        public override string ToString()
        {
            return Alias;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as CartonPropertyGroup;
            if (other == null)
                return false;

            return Alias == other.Alias && Condition == other.Condition &&
                   Math.Abs(DefaultMixQuantity - other.DefaultMixQuantity) < 0.0001
                   && Status == Status && SubstitutionGroup == SubstitutionGroup;

        }
    }
}
