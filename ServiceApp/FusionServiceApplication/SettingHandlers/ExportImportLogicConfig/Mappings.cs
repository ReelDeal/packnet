﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ExportImportLogicConfig
{
    [Serializable]
    public class Mappings
    {
        [XmlElement("Map")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<Map> Map { get; set; }

    }
}
