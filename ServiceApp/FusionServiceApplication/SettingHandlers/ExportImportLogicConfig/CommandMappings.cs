﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ExportImportLogicConfig
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class CommandMappings
    {
        [XmlElement("CommandMapping")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<CommandMapping> CommandMapping { get; set; }
    }
}
