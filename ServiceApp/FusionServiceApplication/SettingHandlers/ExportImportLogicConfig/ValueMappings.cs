﻿
namespace FusionServiceApplication.SettingHandlers.ExportImportLogicConfig
{
    using System.ComponentModel;
    using System.Xml.Serialization;

    public class ValueMappings
    {
        [XmlElement("MappingTable")]
        [TypeConverter(typeof(ExpandableObjectConverter)), Browsable(true)]
        public ItemCollection<MappingTable> MappingTable { get; set; }
    }
}
