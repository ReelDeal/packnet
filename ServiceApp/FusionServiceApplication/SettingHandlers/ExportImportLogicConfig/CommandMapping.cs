﻿namespace FusionServiceApplication.SettingHandlers.ExportImportLogicConfig
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class CommandMapping
    {
        [XmlElement("ExternalCommand")]
        public string ExternalCommand { get; set; }

        [XmlElement("InternalCommand")]
        public string InternalCommand { get; set; }

        public override string ToString()
        {
            return "Command mapping";
        }
    }
}
