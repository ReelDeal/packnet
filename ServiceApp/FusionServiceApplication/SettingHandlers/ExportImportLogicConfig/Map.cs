﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ExportImportLogicConfig
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Map
    {
        [XmlAttribute("from")]
        public string From { get; set; }

        [XmlAttribute("to")]
        public string To { get; set; }

        public override string ToString()
        {
            return "Map";
        }
    }
}
