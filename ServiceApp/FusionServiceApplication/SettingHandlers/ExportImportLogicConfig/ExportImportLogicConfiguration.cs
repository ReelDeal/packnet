﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ExportImportLogicConfig
{
    [XmlRoot(ElementName = "ImportExportSettings", Namespace = "http://Packsize.com")]
    [Serializable]
    public class ExportLogicConfiguration : Configuration<ExportLogicConfiguration>, IConfigFile
    {
        public static ExportLogicConfiguration   GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("CommandColumnName")]
        public string CommandColumnName { get; set; }

        [XmlElement("CommandMappings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CommandMappings CommandMappings { get; set; }

        [XmlElement("ValueMappings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ValueMappings ValueMappings { get; set; }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            CommandColumnName = reloadedConfig.CommandColumnName;
            CommandMappings = reloadedConfig.CommandMappings;
            ValueMappings = reloadedConfig.ValueMappings;
        }
    }
}
