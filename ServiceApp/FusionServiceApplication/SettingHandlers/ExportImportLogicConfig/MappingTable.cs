﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ExportImportLogicConfig
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class MappingTable
    {
        [XmlAttribute("field")]
        public string Field { get; set; }

        [XmlAttribute("default")]
        public string Default { get; set; }

        [XmlElement("Mappings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Mappings Mappings { get; set; }

        public override string ToString()
        {
            return "Mapping";
        }
    }
}
