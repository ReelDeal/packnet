﻿using System;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ComponentsConfig
{
    [Serializable]
    public class CodeGeneratorSettings
    {
        [XmlElement("MinimumLineDistanceToCorrugateEdge")]
        public double MinimumLineDistanceToCorrugateEdge { get; set; }
    }
}
