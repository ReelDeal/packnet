﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ComponentsConfig
{
    [XmlRoot(ElementName = "ComponentsConfigurations", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class ComponentsConfiguration : Configuration<ComponentsConfiguration>, IConfigFile
    {
        public static ComponentsConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("HistoryConnectionString")]
        public string HistoryConnectionString { get; set; }

        [XmlElement("NumberOfRequestsToSurgedGroup")]
        public int NumberOfRequestsToSurgedGroup { get; set; }

        [XmlElement("NumberOfValidRequestsBeforeTryingToProduce")]
        public int NumberOfValidRequestsBeforeTryingToProduce { get; set; }

        [XmlElement("RequestProducedExpireDays")]
        public int RequestProducedExpireDays { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }
        
        [XmlElement("CodeGeneratorSettings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public CodeGeneratorSettings CodeGeneratorSettings { get; set; }

        [XmlElement("SelectorSettings")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public SelectorSettings SelectorSettings { get; set; }

        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            CodeGeneratorSettings = reloadedConfig.CodeGeneratorSettings;
            HistoryConnectionString = reloadedConfig.HistoryConnectionString;
            NumberOfRequestsToSurgedGroup = reloadedConfig.NumberOfRequestsToSurgedGroup;
            NumberOfValidRequestsBeforeTryingToProduce = reloadedConfig.NumberOfValidRequestsBeforeTryingToProduce;
            RequestProducedExpireDays = reloadedConfig.RequestProducedExpireDays;
            SelectorSettings = reloadedConfig.SelectorSettings;
            Version = reloadedConfig.Version;
        }
    }

}
