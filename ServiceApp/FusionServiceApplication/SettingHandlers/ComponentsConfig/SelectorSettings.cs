﻿using System;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.ComponentsConfig
{
    [Serializable]
    public class SelectorSettings
    {
        [XmlElement("AlignTilesInEnd")]
        public bool AlignTilesInEnd { get; set; }

        [XmlElement("MaxNumberOfTiles")]
        public int MaxNumberOfTiles { get; set; }

        [XmlElement("NunaTabLength")]
        public double NunaTabLength { get; set; }

        [XmlElement("NunaTabOffset")]
        public double NunaTabOffset { get; set; }
    }
}
