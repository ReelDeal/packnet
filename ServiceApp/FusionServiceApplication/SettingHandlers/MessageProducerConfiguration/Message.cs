﻿using System.ComponentModel;

namespace FusionServiceApplication.SettingHandlers.MessageProducerConfiguration
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Message
    {
        [XmlAttribute("type")]
        public string Type { get; set; }

        [XmlElement("format")]
        public string Format { get; set; }

        public override string ToString()
        {
            return "Message item";
        }
    }
}
