﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.MessageProducerConfiguration
{
    [Serializable]
    public class Answers
    {
        [XmlElement("answer")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<Answer> Answer { get; set; }
    }
}
