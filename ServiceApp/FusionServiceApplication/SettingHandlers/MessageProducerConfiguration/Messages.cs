﻿namespace FusionServiceApplication.SettingHandlers.MessageProducerConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    public class Messages
    {
        [XmlElement("message")] 
        [TypeConverter(typeof (ExpandableObjectConverter))] 
        public ItemCollection<Message> Message { get; set; } 
    }
}
