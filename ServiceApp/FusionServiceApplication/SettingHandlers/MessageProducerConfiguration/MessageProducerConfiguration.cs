﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.MessageProducerConfiguration
{
    [XmlRoot(ElementName = "configuration")]
    [Serializable]
    public class MessageProducerConfiguration : Configuration<MessageProducerConfiguration>, IConfigFile
    {
        public static MessageProducerConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("messages")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Messages Messages { get; set; }

        [XmlElement("answers")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Answers Answers { get; set; }

        protected override void ConsistencyCheck()
        {
        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            Answers = reloadedConfig.Answers;
            Messages = reloadedConfig.Messages;
        }
    }
}
