﻿namespace FusionServiceApplication.SettingHandlers.MessageProducerConfiguration
{
    using System.ComponentModel;
    using System.Xml.Serialization;
    using System;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Answer
    {
        [XmlElement("regex")]
        public string Regex { get; set; }

        [XmlAttribute("type")]
        public string Type { get; set; }

        public override string ToString()
        {
            return "Answer item";
        }
    }
}
