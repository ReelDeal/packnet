﻿namespace FusionServiceApplication.SettingHandlers.ReportsConfig
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "ReportConfiguration", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class ReportsConfiguration : Configuration<ReportsConfiguration>, IConfigFile
    {
        public static ReportsConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("ExcelReportTemplate")]
        public string ExcelReportTemplate { get; set; }

        [XmlElement("HistoryDB")]
        public string HistoryDB { get; set; }

        [XmlElement("LastNumberOfDays")]
        public int LastNumberOfDays { get; set; }

        [XmlElement("ReportDestinationPath")]
        public string ReportDestinationPath { get; set; }

        [XmlElement("ServerHostUri")]
        public string ServerHostUri { get; set; }

        [XmlElement("Url")]
        public string Url { get; set; }


        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            ExcelReportTemplate = reloadedConfig.ExcelReportTemplate;
            HistoryDB = reloadedConfig.HistoryDB;
            LastNumberOfDays = reloadedConfig.LastNumberOfDays;
            ReportDestinationPath = reloadedConfig.ReportDestinationPath;
            ServerHostUri = reloadedConfig.ServerHostUri;
            Url = reloadedConfig.Url;
        }
    }
}
