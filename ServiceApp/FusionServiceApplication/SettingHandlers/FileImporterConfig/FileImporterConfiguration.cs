﻿namespace FusionServiceApplication.SettingHandlers.FileImporterConfig
{
    using System;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "FileImporterSettings", Namespace = "http://Packsize.com")]
    [Serializable]
    public class FileImporterConfiguration : Configuration<FileImporterConfiguration>, IConfigFile
    {
        public static FileImporterConfiguration GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("DeleteImportedFiles")]
        public bool DeleteImportedFiles { get; set; }

        [XmlElement("Extension")]
        public string Extension { get; set; }

        [XmlElement("FieldDelimiter")]
        public int FieldDelimiter { get; set; }

        [XmlElement("Fields")]
        public string Fields { get; set; }

        [XmlElement("FieldsToIgnore")]
        public string FieldsToIgnore { get; set; }

        [XmlElement("Headers")]
        public bool Headers { get; set; }

        [XmlElement("NumberOfRetriesOnLockedFile")]
        public int NumberOfRetriesOnLockedFile { get; set; }

        [XmlElement("Path")]
        public string Path { get; set; }

        public static void Reload()
        {
            lock (Guard)
            {
                Instance = Deserialize();
            }
        }

        protected override void ConsistencyCheck()
        {

        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            DeleteImportedFiles = reloadedConfig.DeleteImportedFiles;
            Extension = reloadedConfig.Extension;
            FieldDelimiter = reloadedConfig.FieldDelimiter;
            Fields = reloadedConfig.Fields;
            FieldsToIgnore = reloadedConfig.FieldsToIgnore;
            Headers = reloadedConfig.Headers;
            NumberOfRetriesOnLockedFile = reloadedConfig.NumberOfRetriesOnLockedFile;
            Path = reloadedConfig.Path;
        }
    }
}
