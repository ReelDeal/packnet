﻿namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Printer
    { 
        [XmlElement("Id")]
        public string Id { get; set; }
        
        [XmlElement("IsPriorityPrinter")]
        public bool IsPriorityPrinter { get; set; }
        
        [XmlElement("Port")]
        public string Port { get; set; }

        public override string ToString()
        {
            return Id;
        }
    }
}
