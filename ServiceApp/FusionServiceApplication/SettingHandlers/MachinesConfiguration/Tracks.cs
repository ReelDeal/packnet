﻿namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [XmlInclude(typeof(LegacyMachineTrackInfo))]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Tracks
    {
        [XmlElement("TrackInfo")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<TrackInfo> TrackInfo { get; set; }
    }
}
