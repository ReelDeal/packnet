﻿
namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [XmlInclude(typeof(Machine))]
    [XmlType(TypeName = "LegacyMachineTrackInfo", IncludeInSchema = true, Namespace = "http://Packsize.MachineManager.com")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class LegacyMachineTrackInfo : TrackInfo
    {
    }
}
