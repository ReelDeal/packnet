﻿namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class MachineManagerCallbackIp
    {
        [XmlElement("IpOrDnsName")]
        public string IpOrDnsName { get; set; }
    
        [XmlElement("Port")]
        public int Port { get; set; }
    }
}
