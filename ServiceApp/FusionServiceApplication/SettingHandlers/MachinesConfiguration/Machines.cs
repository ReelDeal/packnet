﻿using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    [XmlInclude(typeof(IqFusionMachine))]
    [XmlInclude(typeof(EMMachine))]
    public class Machines
    {
        [XmlElement("Machine")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ItemCollection<Machine> Machine { get; set; }
    }

}
