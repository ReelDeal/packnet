﻿using System;

namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [XmlInclude(typeof(Machine))]
    [XmlType(TypeName = "IqFusionMachine", IncludeInSchema = true, Namespace = "http://Packsize.MachineManager.com")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class IqFusionMachine : Machine
    {
    }
}
