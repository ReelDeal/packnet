﻿using System;

namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    [XmlInclude(typeof(Machine))]
    [XmlType(TypeName = "EM", IncludeInSchema = true, Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class EMMachine : Machine
    {
    }
}
