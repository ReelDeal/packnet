﻿namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [XmlRoot(ElementName = "MachinesSupplier", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class MachinesSupplier : Configuration<MachinesSupplier>, IConfigFile
    {
        public static MachinesSupplier GetInstanceFromSpecifiedPath(string path)
        {
            return Deserialize(path);
        }

        [XmlElement("MachineManagerCallbackIp")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public MachineManagerCallbackIp MachineManagerCallbackIp { get; set; }

        [XmlElement("Machines")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Machines Machines { get; set; }

        [XmlElement("Version")]
        public string Version { get; set; }

        protected override void ConsistencyCheck()
        {
            
        }

        public void Reload(string path)
        {
            var reloadedConfig = Deserialize(path);

            MachineManagerCallbackIp = reloadedConfig.MachineManagerCallbackIp;
            Machines = reloadedConfig.Machines;
            Version = reloadedConfig.Version;
        }
    }
}
