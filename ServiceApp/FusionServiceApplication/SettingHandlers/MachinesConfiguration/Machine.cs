﻿namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System.ComponentModel;
    using System.Xml.Serialization;

    using Utilities.Enums;

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Machine
    {
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public MachinesConfigurationTypes MachinesConfigurationTypes { get; set; }

        [XmlElement("Alias")]
        public string Alias { get; set; }

        [XmlElement("Id")]
        public string Id { get; set; }

        [XmlElement("Printers")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Printers Printers { get; set; }

        [XmlElement("Tracks")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Tracks MachinesConfigurationTracks { get; set; }

        [XmlElement("IpOrDnsName")]
        public string IpOrDnsName { get; set; }

        [XmlElement("PhysicalSettingsFilePath")]
        public string PhysicalSettingsFilePath { get; set; }


        public override string ToString()
        {
            return Alias;
        }
    }
}
