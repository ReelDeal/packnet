﻿
namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    [Serializable]
    public class Printers
    {
        [XmlElement("Printer")]
        [TypeConverter(typeof (ExpandableObjectConverter))]
        public ItemCollection<Printer> Printer { get; set; }
    }
}
