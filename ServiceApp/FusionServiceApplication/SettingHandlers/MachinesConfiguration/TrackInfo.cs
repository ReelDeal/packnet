﻿namespace FusionServiceApplication.SettingHandlers.MachinesConfiguration
{
    using System;
    using System.ComponentModel;
    using System.Xml.Serialization;

    using Utilities.Enums;

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class TrackInfo
    {
        [XmlAttribute(AttributeName = "type", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public MachinesConfigurationTypes TrackinfoType { get; set; }

        [XmlElement("Id")]
        public string Id { get; set; }

        [XmlElement("CorrugateId")]
        public string CorrugateId { get; set; }

        [XmlElement("Offset")]
        public double Offset { get; set; }
    }
}
