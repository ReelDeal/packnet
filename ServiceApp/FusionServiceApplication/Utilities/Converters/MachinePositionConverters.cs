﻿namespace FusionServiceApplication.Utilities.Converters
{
    using System;

    public static class MachinePositionConverters
    {
        public static Tuple<int, int> ConvertMachinePos(double pos)
        {
            return new Tuple<int, int>(GetDMorInchPart(Math.Round(pos, 1)), GetInchOrMMoverHundredPart(Math.Round(pos, 1)));
        }

        private static short GetDMorInchPart(double microMeter)
        {
            return (short)(microMeter / 100);
        }

        private static short GetInchOrMMoverHundredPart(double microMeter)
        {
            return (short)((microMeter % 100) * 100);
        }
    }
}
