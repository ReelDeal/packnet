﻿namespace FusionServiceApplication.Utilities.InstructionListStuff
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Forms;

    using Converters;
    using Enums;

    public static class InstructionListFromCustom
    {
        public static bool GetFeedInstruction(List<string> instruction, List<string> realIl)
        {
            try
            {
                var convertedLength = MachinePositionConverters.ConvertMachinePos(double.Parse(instruction[1]));
                realIl.Add(((InstructionType)Enum.Parse(typeof(InstructionType), instruction[0])).ToString("d"));
                realIl.Add(convertedLength.Item1.ToString(CultureInfo.InvariantCulture));
                realIl.Add(convertedLength.Item2.ToString(CultureInfo.InvariantCulture));
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to convert length of FeedInstruction instruction");
                return false;
            }
        }

        public static bool GetCrossheadInstruction(List<string> instruction, List<string> realIl)
        {
            try
            {
                var convertedLength = MachinePositionConverters.ConvertMachinePos(double.Parse(instruction[3]));
                realIl.Add(((InstructionType)Enum.Parse(typeof(InstructionType), instruction[0])).ToString("d"));
                realIl.Add(instruction[1]);
                realIl.Add(instruction[2]);
                realIl.Add(convertedLength.Item1.ToString(CultureInfo.InvariantCulture));
                realIl.Add(convertedLength.Item2.ToString(CultureInfo.InvariantCulture));
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to convert length of CHpositioning instruction");
                return false;
            }
        }

        public static bool GetMetadataInstruction(List<string> instruction, List<string> realIl)
        {
            try
            {
                var convertedLength = MachinePositionConverters.ConvertMachinePos(double.Parse(instruction[1]));

                realIl.Add(((InstructionType)Enum.Parse(typeof(InstructionType), instruction[0])).ToString("d"));
                realIl.Add(convertedLength.Item1.ToString(CultureInfo.InvariantCulture));
                realIl.Add(convertedLength.Item2.ToString(CultureInfo.InvariantCulture));
                realIl.Add(instruction[2]);
                realIl.Add(instruction[3]);
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to convert length of metadata instruction");
                return false;
            }
        }

        public static void GetKnifeInstruction(List<string> realIl, List<string> instruction)
        {
            realIl.Add(((InstructionType)Enum.Parse(typeof(InstructionType), instruction[0])).ToString("d"));
            realIl.Add(instruction[1]);
        }

        public static bool GetLhPositioningInstruction(List<double> currentLhPositions, List<string> instruction, List<string> realIl)
        {
            var convertedPositions = GetSolutionForLongHeadPositions(currentLhPositions, instruction);

            if (convertedPositions == null)
            {
                return false;
            }

            realIl.Add(((InstructionType)Enum.Parse(typeof(InstructionType), instruction[0])).ToString("d"));
            realIl.AddRange(convertedPositions);
            return true;
        }

        private static IEnumerable<string> GetSolutionForLongHeadPositions(List<double> currentLhPositions, IList<string> instruction)
        {
            var wantedPositions = new List<double>();

            if (currentLhPositions.Count(longheadPosition => Math.Abs(longheadPosition - 0) > 0.00001) != instruction.Count - 1)
            {
                throw new Exception("Longhead count missmatch : GetSolutionForLonHeadPositions");
            }

            for (var i = 1; i < instruction.Count; i++)
            {
                if (instruction[i] == string.Empty)
                {
                    break;
                }

                try
                {
                    wantedPositions.Add(double.Parse(instruction[i]));
                }
                catch (Exception)
                {
                    MessageBox.Show("One of the positions cant be parsed");
                    return null;
                }
            }

            var positioningSolution = new List<Tuple<int, double>>();
            LongheadPositioningStuff.FindLongheadMovementSolution(
                                                                     currentLhPositions, 
                                                                     wantedPositions,
                                                                     Enumerable.Range(1, wantedPositions.Count).ToList(), 
                                                                     positioningSolution,
                                                                     currentLhPositions.Last() > 100);

            if (positioningSolution.Count != wantedPositions.Count)
            {
                MessageBox.Show("Unable to finde LHMovementSolution");
                return null;
            }

            var convertedPositions = new List<string>();

            foreach (var longheadPosition in positioningSolution)
            {
                convertedPositions.Add(longheadPosition.Item1.ToString(CultureInfo.InvariantCulture));

                var convPos = MachinePositionConverters.ConvertMachinePos(longheadPosition.Item2);

                convertedPositions.Add(convPos.Item1.ToString(CultureInfo.InvariantCulture));
                convertedPositions.Add(convPos.Item2.ToString(CultureInfo.InvariantCulture));
            }

            return convertedPositions;
        }
    }
}
