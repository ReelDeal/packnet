﻿namespace FusionServiceApplication.Utilities.InstructionListStuff
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Constants;

    public static class LongheadPositioningStuff
    {
        public static List<Tuple<int, double>> FindLongheadMovementSolution(List<double> startPositions, List<double> endPositions, List<int> longheadsLeft, List<Tuple<int, double>> currentSolution, bool metricMachine)
        {
            if (longheadsLeft.All(lhs => lhs == -1))
            {
                return currentSolution;
            }

            var currentPositions = new List<double>(startPositions);

            for (var i = 0; i < startPositions.Count; i++)
            {
                if (longheadsLeft[i] == -1)
                {
                    continue;
                }

                if (CheckIfCollision(startPositions[i], endPositions[i], currentPositions, i, metricMachine))
                {
                    continue;
                }

                currentSolution.Add(new Tuple<int, double>(longheadsLeft[i], endPositions[i]));
                currentPositions[i] = endPositions[i];
                longheadsLeft[i] = -1;

                var purposedSolution = FindLongheadMovementSolution(currentPositions, endPositions, longheadsLeft, currentSolution, metricMachine);
                if (purposedSolution != null)
                {
                    return purposedSolution;
                }
            }

            return null;
        }

        private static bool CheckIfCollision(double startPosition, double endPosition, IEnumerable<double> currentLongheadPositions, int longheadNumber, bool metricMachine)
        {
            var positions = new List<double>(currentLongheadPositions);
            positions.RemoveAt(longheadNumber);

            if (startPosition > endPosition)
            {
                return positions.Any(pos => pos > endPosition - (PhysicalMachineProperties.GetLongheadSize(metricMachine) / 2) && pos < startPosition);
            }

            return positions.Any(pos => pos < endPosition + (PhysicalMachineProperties.GetLongheadSize(metricMachine) / 2) && pos > startPosition);
        }
    }
}
