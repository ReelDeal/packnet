﻿namespace FusionServiceApplication.Utilities.InstructionListStuff
{
    public static class InstructionListMarkers
    {
        public const string TrueMarker = "1";
        public const string FalseMarker = "0";
        public const string KnifeRaiseMarker = "0";
        public const string KnifeCutMarker = "3";
        public const string FeedMarker = "4";
        public const string LongheadPositioningMarker = "2";
        public const string KnifeMarker = "3";
        public const string StartMarker = "31000";
        public const string StopMarker = "32000";
        public const string MetadataMarker = "5";

        public const string CrossheadMarker = "1";
    }
}
