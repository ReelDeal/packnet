﻿namespace FusionServiceApplication.Utilities.Enums
{
    using System;
    using System.ComponentModel;
    using System.Linq;

    public static class EnumDescriptionGetter
    {
        public static string[] GetDescriptions(Type enumType)
        {
            var values = Enum.GetValues(enumType).Cast<object>();

            var descriptions = from value in values
                select
                    value.GetType()
                        .GetMember(value.ToString())[0]
                        .GetCustomAttributes(true)
                        .OfType<DescriptionAttribute>()
                        .First()
                        .Description;

            return descriptions.ToArray();
        }
    }
}
