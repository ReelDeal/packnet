﻿namespace FusionServiceApplication.Utilities.Enums
{
    using System.ComponentModel;

    public enum ErrorCodes
    {
        [Description("No Errors")]
        NoErrors = 0,
        [Description("Emergency Stop")]
        EmergencyStop = 1,
        [Description("Rollaxis Error")]
        RollaxisError = 2,
        [Description("Toolaxis Error")]
        ToolaxisError = 3,
        [Description("Connection Error")]
        ConnectionError = 4,
        [Description("Out of Corrugate")]
        OutOfCorrugate = 5,
        [Description("Paper Jam")]
        PaperJam = 6,
        [Description("Currogate Mismatch")]
        CorrugateMismatch = 7,
        [Description("Long head Quantity Error")]
        LongHeadQuantityError = 8,
        [Description("Long head Positioning Error")]
        LongHeadPositionError = 9,
        [Description("Track offset Moved")]
        TrackOffsetMoved = 10,
        [Description("Track Quantity Error")]
        TrackQuantityError = 11,
        [Description("Instruction list Error")]
        InstructionListError = 12,
        [Description("Unhandled Exception")]
        UnhandledException = 13,
        [Description("Change Corrugate Mode")]
        ChangeCorrugateMode = 14
    }
}
