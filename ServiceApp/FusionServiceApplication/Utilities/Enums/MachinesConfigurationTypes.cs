﻿namespace FusionServiceApplication.Utilities.Enums
{
    using System.Xml.Serialization;

    [XmlType(Namespace = "http://Packsize.MachineManager.com", IncludeInSchema = true)]
    public enum MachinesConfigurationTypes
    {       
        Unknown,
        EM,
        IqFusionMachine,
        LegacyMachineTrackInfo
    }
}
