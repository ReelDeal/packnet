﻿namespace FusionServiceApplication.Utilities.Enums
{
    using System.ComponentModel;

    public enum NetworkMode
    {
        [Description("Static IP")]
        StaticIp = 0,
        [Description("DHCP")]
        DHCP = 1,
    }
}
