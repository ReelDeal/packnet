﻿namespace FusionServiceApplication.Utilities.Enums
{
    using System.ComponentModel;

    public enum NetworkScannerViewingMode
    {
        [Description("All")]
        All = 0,
        [Description("Only Packsize")]
        AllPacksize = 1,
        [Description("Only iQ Fusion")]
        FusionOnly = 2,
    }

 
}
