﻿namespace FusionServiceApplication.Utilities.Enums
{
    public enum InstructionType
    {
        MetadataInstruction = 5,
        Knife = 3,
        LHpositioning = 2,
        CHpositioning = 1,
        FeedInstruction = 4,
    }
}
