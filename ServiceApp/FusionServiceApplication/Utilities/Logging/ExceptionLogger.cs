﻿namespace FusionServiceApplication.Utilities.Logging
{
    using System;
    using System.Windows.Forms;
    using System.Windows.Threading;

    public static class ExceptionLogger
    {
        public static void UnhandledExceptionHandler(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Unhandled ExceptionOccured, please see log for more info");
            Logging.WriteToLogFile(e.ToString());
        }

        public static void UnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Unhandled ExceptionOccured, please see log for more info");
            Logging.WriteToLogFile(e.ToString());
        }
    }
}
