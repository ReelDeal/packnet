﻿namespace FusionServiceApplication.Utilities.Logging
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Text;

    public static class Logging
    {
        public static void WriteToLogFile(string message, string filePath = @".\ErrorLog")
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }

            var outputFile = File.Open(filePath, FileMode.Append);

            var encoding = new ASCIIEncoding();
            var bytes = encoding.GetBytes(DateTime.Now + " : " + message.ToString(CultureInfo.InvariantCulture) + Environment.NewLine);

            outputFile.Write(bytes, 0, bytes.Length);

            outputFile.Close();
        }
    }
}