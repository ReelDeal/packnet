﻿namespace FusionServiceApplication.Utilities.Constants
{
    public static class PhysicalMachineProperties
    {
        private const double MinimumPackageLengthMm = 2.54;
        private const double MinimumPackageLengthInch = 1;

        private const double MaximumPackageLengthMm = 1000;
        private const double MaximumPackageLengthInch = 39.3;

        private const int LongheadSizeMm = 76;
        private const int LongheadSizeInch = 3;

        public static double GetLongheadSize(bool metricMachine)
        {
            return metricMachine ? LongheadSizeMm : LongheadSizeInch;
        }

        public static double GetMinimumPackageLength(bool metricMachine)
        {
            return metricMachine ? MinimumPackageLengthMm : MinimumPackageLengthInch;
        }

        public static double GetMaximumPackageLength(bool metricMachine)
        {
            return metricMachine ? MaximumPackageLengthMm : MaximumPackageLengthInch;
        }
    }
}
