﻿using PackNet.Communication.Fusion;
using PackNet.Communication.PLCBase;

namespace FusionServiceApplication.Utilities.Communication
{
    using System;
    using System.Collections.Generic;

    public interface IIqFusionCommunicator
    {
        #region properties

        EventHandler OnConnectionLostHandler { get; set; }

        EventHandler OnConnectionEstablishedHandler { get; set; }

        EventHandler<VariableChangedEventArgs> OnVariableChangedHandler { get; set; }

        bool Connected { get; }

        bool Run4Ever { get; set; }

        bool LongHeadPositionsReady { get; set; }

        bool RedLEDActive { get; set; }

        bool WhiteLEDActive { get; set; }

        bool GreenLEDActive { get; set; }
        
        bool LatcherActive { get; set; }

        bool RightRollerActive { get; set; }

        bool LeftRollerActive { get; set; }

        bool KnifeActive { get; set; }

        bool TrackSensorControlMode { get; set; }

        bool CrossHeadNotMoving { get; }

        bool IoControl { get; set; }

        bool ServiceMode { get; set; }

        string TrackSensorControlModeSpeed { get; set; }

        double[] TrackLatchPositions { get; }

        bool RollerActivated { set; }

        bool PlcReadyForTransfer { get; set; }

        bool CustomInstructionList { set; }

        string[] TransferBufferInstructionList { set; }

        int TransferBufferId { get; set; }

        bool GetLongHeadPositions { get; set; }

        IqFusionMachineVariables VariableMap { get; }

        List<double> CurrentLongHeadPositions { get; }

        bool CalibratingSwordSensorCompensations { get; set; }

        bool ClearNonVolatileCommunicationOptions { set; }

        int NumberOfLongheads { get; }

        string EventNotifierAdress { set; }

        string EventNotifierPort { set; }

        float RightRollerDisconnectPosition { get; set; }

        float LeftRollerDisconnectPosition { get; set; }

        string RollerAcceleration { set; }

        string RollerCleanCutLength { set; }

        string RollerGearRatio { set; }

        string RollerLoweringOffset { set; }

        string RollerOutFeedLength { set; }

        string RollerSpeedInPrecent { set; }

        string RollerAccelerationInPrecent { set; }

        string RollerAxisScaling { set; }

        string CrossHeadAccelerationInPercent { set; }

        float CrossHeadHomingPosition { get; set; }

        string CrossHeadMaximumAcceleration { set; }

        string CrossHeadMaximumDeceleration { set; }

        string CrossHeadMaximumPosition { set; }

        double CrossHeadMax { get; }

        double CrossHeadMin { get; }

        bool IsMetricMachine { get; }

        string CrossheadMinimumPosition { set; }

        string CrossHeadMaximumSpeed { set; }

        string CrossHeadTrackSensorToTool { set; }

        string CrossHeadSpeedInPrecent { set; }

        string ToolAxisScaling { set; }

        double[] SwordSensorCompensationLeft1 { get; set; }

        double[] SwordSensorCompensationLeft2 { get; set; }

        double[] SwordSensorCompensationRight1 { get; set; }

        double[] SwordSensorCompensationRight2 { get; set; }

        double OutOfCorrugatePosition1 { get; }

        double OutOfCorrugatePosition2 { get; }

        string DefaultGateway { get; set; }

        string SubnetMask { get; set; }

        string HostName { get; set; }

        bool Dhcp { get; set; }

        string StaticIp { get; set; }

        bool ExecuteNetworkSettings { set; }

        string CurrentHostName { get; }

        int CurrentNetworkMode { get; }

        string CurrentIpAddress { get; }

        string CurrentSubnetMask { get; }

        string CurrentGateway { get; }

        string MachineMacAddress { get; }

        string MachinePlcVersion { get; }

        double TrackSideSteeringTolerance { get; set; }
        
        double TrackOffsetTolerance { get; set; }
        
        int TrackActivationDelay { get; set; }
        
        double TrackLatchDistance { get; set; }

        #endregion

        #region methods

        bool Connect();

        bool Disconnect();

        void WriteValue(PacksizePlcVariable var, object value);

        T ReadValue<T>(PacksizePlcVariable var);

        #endregion

    }
}
