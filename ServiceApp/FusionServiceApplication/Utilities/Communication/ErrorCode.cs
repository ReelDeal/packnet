﻿namespace FusionServiceApplication.Utilities.Communication
{
    using System.ComponentModel;
    using System.Linq;

    using Enums;

    public class ErrorCode
    {
        private object value;
        private string[] args;

        public ErrorCode()
        {
        }

        public ErrorCode(string name, object value, string[] args)
        {
            Name = name;
            this.value = value;
            this.args = args;
        }

        public string Name { get; set; }

        public object Value
        {
            get { return value; }
            set { this.value = value; }
        }

        public string[] Args
        {
            get { return args; }
            set { args = value; }
        }

        public static string GetEnumDescription(object enumValue)
        {
            var fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                var attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);

                if (attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return string.Empty;
        }

        public override string ToString()
        {
            int errorValue;
            string outPutText;

            if (int.TryParse(value.ToString(), out errorValue))
            {
                outPutText = GetEnumDescription((ErrorCodes)errorValue) + " : ";
                return Args == null ? outPutText : Args.Aggregate(outPutText, (current, arg) => current + (arg + ", "));
            }

            outPutText = value + " : ";
            return Args.Aggregate(outPutText, (current, arg) => current + (arg + ", "));
        }
    }
}
