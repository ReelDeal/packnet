﻿using PackNet.Common.Interfaces.Logging;
using PackNet.Communication;
using PackNet.Communication.Fusion;
using PackNet.Communication.PLCBase;
using PackNet.Communication.WebRequestCreator;

namespace FusionServiceApplication.Utilities.Communication
{
    using Properties;
    using Logging;
    using PackNet.Common.Logging;
    using PackNet.Common.Logging.NLogBackend;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Windows.Threading;

    internal  delegate void WriteToMachineDelegate(PacksizePlcVariable var, object arg);

    public class IqFusionCommunicator : IIqFusionCommunicator
    {
        private readonly JsonCommunicator machineCommunicator;
        private readonly MachineRESTService restServer;
        private readonly IqFusionMachineVariables variableMap;
        private readonly WebRequestCreator webRequestCreator;
        private readonly IPostRequestPopulator postRequestCreator;
        private readonly ILogger logger;

        public IqFusionCommunicator(string ip)
        {
            restServer = new MachineRESTService(logger);
            variableMap = new IqFusionMachineVariables();
            postRequestCreator = new PostRequestPopulator();
            webRequestCreator = new WebRequestCreator(ip, postRequestCreator);
            LogManager.LoggingBackend = new NLogBackend("FusionServiceApplication", Settings.Default.NLogConfiguration);
            logger = LogManager.GetLogForApplication();
            machineCommunicator = new JsonCommunicator(webRequestCreator, variableMap,
                logger);
        }

        public EventHandler OnConnectionLostHandler { get; set; }

        public EventHandler OnConnectionEstablishedHandler { get; set; }

        public EventHandler<VariableChangedEventArgs> OnVariableChangedHandler { get; set; }

        public bool Connected { get; private set; }

        public bool Run4Ever
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.Run4Ever); }
            set { TryWriteToPlc(variableMap.Run4Ever, value); }
        }

        public bool LongHeadPositionsReady
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.LongHeadPositionsReady); }
            set { TryWriteToPlc(variableMap.LongHeadPositionsReady, value); }
        }

        public bool RedLEDActive
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.RedLEDState); }
            set { TryWriteToPlc(variableMap.RedLEDState, value); }
        }

        public bool WhiteLEDActive
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.WhiteLEDState); }

            set { TryWriteToPlc(variableMap.WhiteLEDState, value); }
        }

        public bool GreenLEDActive
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.GreenLEDState); }

            set { TryWriteToPlc(variableMap.GreenLEDState, value); }
        }

        public bool LatcherActive
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.ActivateLatcher); }
            set { TryWriteToPlc(variableMap.ActivateLatcher, value); }
        }

        public bool RightRollerActive
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.ActivateRightPressureRoller); }
            set { TryWriteToPlc(variableMap.ActivateRightPressureRoller, value); }
        }

        public bool LeftRollerActive
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.ActivateLeftPressureRoller); }
            set { TryWriteToPlc(variableMap.ActivateLeftPressureRoller, value); }
        }

        public bool KnifeActive
        {
            get
            {
                return machineCommunicator.ReadValue<int>(variableMap.KnifeCommand) == 3;
            }

            set
            {
                if (KnifeActive == value)
                {
                    return;
                }

                TryWriteToPlc(variableMap.KnifeCommand, KnifeActive ? 0 : 3);
                TryWriteToPlc(variableMap.KnifeExecuteCommand, 1);
            }
        }

        public bool TrackSensorControlMode
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.TrackSensorControl); }
            set { TryWriteToPlc(variableMap.TrackSensorControl, value); }
        }

        public bool CrossHeadNotMoving
        {
            get
            {
                return !machineCommunicator.ReadValue<bool>(variableMap.Calibrate) &&
                       !machineCommunicator.ReadValue<bool>(variableMap.TrackSensorControl) &&
                       !machineCommunicator.ReadValue<bool>(variableMap.Run4Ever);
            }
        }

        public bool IoControl
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.IOControllTestState); }
            set { TryWriteToPlc(variableMap.IOControllTestState, value); }
        }

        public bool ServiceMode
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.SetServiceMode); }
            set { TryWriteToPlc(variableMap.SetServiceMode, value); }
        }

        public string TrackSensorControlModeSpeed
        {
            get
            {
                return machineCommunicator.ReadValue<string>(variableMap.TrackSensorControlModeSpeed);
            }

            set
            {
                try
                {
                    value = Math.Min(Math.Max(int.Parse(value), 1), 100).ToString(CultureInfo.InvariantCulture);
                }
                catch (Exception)
                {
                    value = "1";
                }

                TryWriteToPlc(variableMap.TrackSensorControlModeSpeed, value);
            }
        }

        public double[] TrackLatchPositions
        {
            get { return machineCommunicator.ReadValue<double[]>(VariableMap.LastLatchPosition); }
        }

        public bool RollerActivated
        {
            set { TryWriteToPlc(variableMap.LowerRoller, value); }
        }

        public bool PlcReadyForTransfer
        {
            get { return TryReadFromPlc<bool>(variableMap.PlcReadyForTransfer); }
            set { TryWriteToPlc(variableMap.PlcReadyForTransfer, value); }
        }

        public bool CustomInstructionList
        {
            set { TryWriteToPlc(variableMap.CustomInstructionList, value); }
        }

        public string[] TransferBufferInstructionList
        {
            set { TryWriteToPlc(variableMap.TransferBufferInstructionList, value); }
        }

        public int TransferBufferId
        {
            get { return machineCommunicator.ReadValue<int>(variableMap.TransferBufferId); }
            set { TryWriteToPlc(variableMap.TransferBufferId, value); }
        }

        public bool GetLongHeadPositions
        {
            set { TryWriteToPlc(variableMap.GetLongHeadPositions, value); }
            get { throw new NotImplementedException(); }
        }

        public IqFusionMachineVariables VariableMap
        {
            get { return variableMap; }
        }

        public List<double> CurrentLongHeadPositions
        {
            get { return machineCommunicator.ReadValue<double[]>(variableMap.LongHeadPositions).ToList(); }
        }

        public bool CalibratingSwordSensorCompensations
        {
            get { return machineCommunicator.ReadValue<bool>(variableMap.Calibrate); }
            set { TryWriteToPlc(variableMap.Calibrate, value); }
        }

        public bool ClearNonVolatileCommunicationOptions
        {
            set { TryWriteToPlc(variableMap.ClearNonVolatile, value); }
        }

        public int NumberOfLongheads
        {
            get { return machineCommunicator.ReadValue<int>(variableMap.NumberOfLongHeads); }
        }

        public string EventNotifierAdress
        {
            set { TryWriteToPlc(variableMap.EventNotifierAddress, value); }
        }

        public string EventNotifierPort
        {
            set { TryWriteToPlc(variableMap.EventNotifierPort, value); }
        }

        public float RightRollerDisconnectPosition
        {
            get { return machineCommunicator.ReadValue<float>(variableMap.RightRollerDisconnectPosition); }
            set { TryWriteToPlc(variableMap.RightRollerDisconnectPosition, value); }
        }

        public float LeftRollerDisconnectPosition
        {
            get { return machineCommunicator.ReadValue<float>(variableMap.LeftRollerDisconnectPosition); }
            set { TryWriteToPlc(variableMap.LeftRollerDisconnectPosition, value); }
        }

        public string RollerAcceleration
        {
            set { TryWriteToPlc(variableMap.FeedRollerAccelerationInPercent, value); }
        }

        public string RollerCleanCutLength
        {
            set { TryWriteToPlc(variableMap.FeedRollerCleanCutLength, value); }
        }

        public string RollerGearRatio
        {
            set { TryWriteToPlc(variableMap.FeedRollerGearRatio, value); }
        }

        public string RollerLoweringOffset
        {
            set { TryWriteToPlc(variableMap.FeedRollerLoweringOffset, value); }
        }

        public string RollerOutFeedLength
        {
            set { TryWriteToPlc(variableMap.FeedRollerOutFeedLength, value); }
        }

        public string RollerSpeedInPrecent
        {
            set { TryWriteToPlc(variableMap.FeedRollerSpeedInPercent, value); }
        }

        public string RollerAccelerationInPrecent
        {
            set { TryWriteToPlc(variableMap.FeedRollerAccelerationInPercent, value); }
        }

        public string RollerAxisScaling
        {
            set { TryWriteToPlc(variableMap.RollAxisScaling, value); }
        }

        public string CrossHeadAccelerationInPercent
        {
            set { TryWriteToPlc(variableMap.CrossHeadAccelerationInPercent, value); }
        }

        public float CrossHeadHomingPosition
        {
            get { return machineCommunicator.ReadValue<float>(variableMap.CrossHeadHomingPosition); }
            set { TryWriteToPlc(variableMap.CrossHeadHomingPosition, value); }
        }

        public string CrossHeadMaximumAcceleration
        {
            set { TryWriteToPlc(variableMap.CrossHeadMaximumAcceleration, value); }
        }

        public string CrossHeadMaximumDeceleration
        {
            set { TryWriteToPlc(variableMap.CrossHeadMaximumDeceleration, value); }
        }

        public string CrossHeadMaximumPosition
        {
            set { TryWriteToPlc(variableMap.CrossHeadMaximumPosition, value); }
        }

        public double CrossHeadMax
        {
            get { return machineCommunicator.ReadValue<double>(variableMap.CrossHeadMaximumPosition); }
        }

        public double CrossHeadMin
        {
            get { return machineCommunicator.ReadValue<double>(variableMap.CrossHeadMinimumPosition); }
        }

        public bool IsMetricMachine
        {
            get { return machineCommunicator.ReadValue<double>(variableMap.ToolAxisScaling) > 1000; }
        }

        public string CrossheadMinimumPosition
        {
            set { TryWriteToPlc(variableMap.CrossHeadMinimumPosition, value); }
        }

        public string CrossHeadMaximumSpeed
        {
            set { TryWriteToPlc(variableMap.CrossHeadMaximumSpeed, 100 * int.Parse(value)); }
        }

        public string CrossHeadTrackSensorToTool
        {
            set { TryWriteToPlc(variableMap.CrossHeadTrackSensorToTool, value); }
        }

        public string CrossHeadSpeedInPrecent
        {
            set { TryWriteToPlc(variableMap.CrossHeadSpeedInPercent, value); }
        }

        public string ToolAxisScaling
        {
            set { TryWriteToPlc(variableMap.ToolAxisScaling, value); }
        }

        public double OutOfCorrugatePosition1
        {
            get { return machineCommunicator.ReadValue<double[]>(variableMap.TrackOutOfCorrugatePositions).ElementAt(0); }
        }

        public double OutOfCorrugatePosition2
        {
            get { return machineCommunicator.ReadValue<double[]>(variableMap.TrackOutOfCorrugatePositions).ElementAt(1); }
        }

        public double[] SwordSensorCompensationLeft1
        {
            get { return machineCommunicator.ReadValue<double[]>(variableMap.SensorCompensationLeft1); }
            set { TryWriteToPlc(variableMap.TrackSensorCompensationLeft1, value);}
        }

        public double[] SwordSensorCompensationLeft2
        {
            get { return machineCommunicator.ReadValue<double[]>(variableMap.SensorCompensationLeft2); }
            set { TryWriteToPlc(variableMap.TrackSensorCompensationLeft2, value); }
        }

        public double[] SwordSensorCompensationRight1
        {
            get { return machineCommunicator.ReadValue<double[]>(variableMap.SensorCompensationRight1); }
            set { TryWriteToPlc(variableMap.TrackSensorCompensationRight1, value);}
        }

        public double[] SwordSensorCompensationRight2
        {
            get { return machineCommunicator.ReadValue<double[]>(variableMap.SensorCompensationRight2); }
            set { TryWriteToPlc(variableMap.TrackSensorCompensationRight2, value); }
        }

        public string DefaultGateway
        {
            get { throw new NotImplementedException(); }
            set { TryWriteToPlc(variableMap.DefaultGateWay, value); }
        }

        public string SubnetMask
        {
            get { throw new NotImplementedException(); }
            set { TryWriteToPlc(variableMap.SubnetMask, value); }
        }

        public string HostName
        {
            get { throw new NotImplementedException(); }
            set { TryWriteToPlc(variableMap.HostName, value); }
        }

        public bool Dhcp
        {
            get { throw new NotImplementedException(); }
            set { TryWriteToPlc(variableMap.DHCP, value.ToString()); }
        }

        public string StaticIp
        {
            get { throw new NotImplementedException(); }
            set { TryWriteToPlc(variableMap.StaticIp, value); }
        }

        public string[] ErrorCode1Arguments
        {
            get { return TryReadFromPlc<string[]>(variableMap.ErrorCode1Arguments); }
            set { throw new NotImplementedException(); }
        }

        public string[] ErrorCode2Arguments
        {
            get { return TryReadFromPlc<string[]>(variableMap.ErrorCode2Arguments); }
            set { throw new NotImplementedException(); }
        }

        public string[] ErrorCode3Arguments
        {
            get { return TryReadFromPlc<string[]>(variableMap.ErrorCode3Arguments); }
            set { throw new NotImplementedException(); }
        }

        public string[] ErrorCode4Arguments
        {
            get { return TryReadFromPlc<string[]>(variableMap.ErrorCode4Arguments); }
            set { throw new NotImplementedException(); }
        }

        public string[] ErrorCode5Arguments
        {
            get { return TryReadFromPlc<string[]>(variableMap.ErrorCode5Arguments); }
            set { throw new NotImplementedException(); }
        }

        public bool ExecuteNetworkSettings
        {
            set { TryWriteToPlc(variableMap.ExecuteNetworkSettings, value); }
        }

        public string CurrentHostName
        {
            get
            {
                return TryReadFromPlc<string>(variableMap.CurrentHostname);
            }
        }

        public int CurrentNetworkMode
        {
            get
            {
                return TryReadFromPlc<int>(variableMap.CurrentNetworkMode);
            }
        }

        public string CurrentIpAddress
        {
            get
            {
                return TryReadFromPlc<string>(variableMap.CurrentIpAddress);
            }
        }

        public string CurrentSubnetMask
        {
            get
            {
                return TryReadFromPlc<string>(variableMap.CurrentSubnetMask);
            }
        }

        public string CurrentGateway
        {
            get
            {
                return TryReadFromPlc<string>(variableMap.CurrentGateway);
            }
        }

        public string MachineMacAddress
        {
            get
            {
                var mac = TryReadFromPlc<int[]>(variableMap.MachineMacAddress);
                var output = string.Empty;
                for (var index = 0; index < mac.Length - 1; index++)
                {
                    var bit = mac[index];
                    output += bit + ":";
                }
                output += mac.Last();

                return output;
            }
        }

        public string MachinePlcVersion
        {
            get
            {
                return TryReadFromPlc<string>(variableMap.MachinePlcVersion);
            }
        }

        public double TrackSideSteeringTolerance
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                TryWriteToPlc(VariableMap.TrackSideSteeringTolerance, value);
            }
        }

        public double TrackOffsetTolerance
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                TryWriteToPlc(variableMap.TrackOffsetTolerance, value);    
            }
        }

        public int TrackActivationDelay
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                TryWriteToPlc(variableMap.TrackActivationDelay, value);
            }
        }

        public double TrackLatchDistance
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                TryWriteToPlc(variableMap.TrackLatchMinimumDistance, value);
            }
        }

        public int NrOfFeedRollerDownAfterLhRepositioning 
        {
            get
            {
                return TryReadFromPlc<int>(variableMap.NrOfFeedRollerOutOfPositionAfterLongheadRepos);
            } 
        }

        public int NrOfLongheadPositioningRetries
        {
            get
            {
                //return this.TryReadFromPlc<int>(this.variableMap.NrOfLongheadPositioningRetries);
                return int.MinValue;
            }
        }

        public List<ErrorCode> GetMachineErrors()
        {
            var machineErrors = new List<ErrorCode>();

            if (
                !GetErrorAndPutInList(VariableMap.ErrorCode1, VariableMap.ErrorCode1Arguments,
                                           ref machineErrors))
            {
                return machineErrors;
            }

            if (
                !GetErrorAndPutInList(VariableMap.ErrorCode2, VariableMap.ErrorCode2Arguments,
                                           ref machineErrors))
            {
                return machineErrors;
            }

            if (
                !GetErrorAndPutInList(VariableMap.ErrorCode3, VariableMap.ErrorCode3Arguments,
                                           ref machineErrors))
            {
                return machineErrors;
            }

            if (
                !GetErrorAndPutInList(VariableMap.ErrorCode4, VariableMap.ErrorCode4Arguments,
                                           ref machineErrors))
            {
                return machineErrors;
            }

            return !GetErrorAndPutInList(VariableMap.ErrorCode5, VariableMap.ErrorCode5Arguments,
                                              ref machineErrors) ? machineErrors : machineErrors;
        }

        private bool GetErrorAndPutInList(PacksizePlcVariable code, PacksizePlcVariable args,
                                         ref List<ErrorCode> machineErrors)
        {
            var e1 = GetError(code, args);

            if (e1 == null)
                return false;

            machineErrors.Add(e1);
            return true;
        }

        private ErrorCode GetError(PacksizePlcVariable code, PacksizePlcVariable args)
        {
            var e = machineCommunicator.ReadValue<int>(code);
            if (e != 0)
            {
                var eArgs = machineCommunicator.ReadValue<string[]>(args);
                return new ErrorCode(code.VariableName, e, eArgs);
            }
            return null;
        }

        public bool Connect()
        {
            WireEvents();
            try
            {
                machineCommunicator.Connect();
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile("Unable to connect: " + e.Message);
                return false;
            }

            return true;
        }

        public bool Disconnect()
        {
            if (!Connected)
            {
                return true;
            }

            try
            {
                ResetMachine();
                machineCommunicator.Disconnect();
            }
            catch (Exception)
            {
                return false;
            }

            UnWireEvents();
            return true;
        }

        public void WriteValue(PacksizePlcVariable var, object value)
        {
            TryWriteToPlc(var, value); }

        public T ReadValue<T>(PacksizePlcVariable var)
        {
            return machineCommunicator.ReadValue<T>(var);
        }

        private void WireEvents()
        {
            machineCommunicator.VariableChanged += OnVariableChanged;
            machineCommunicator.ConnectionLost += OnConnectionLost;
            machineCommunicator.Connected += OnConnectionEstablished;
        }

        private void UnWireEvents()
        {
            machineCommunicator.VariableChanged -= OnVariableChanged;
            machineCommunicator.ConnectionLost -= OnConnectionLost;
            machineCommunicator.Connected -= OnConnectionEstablished;
        }

        private void ResetMachine()
        {
            var properties = typeof(IqFusionCommunicator).GetProperties();
            foreach (var property in properties.Where(p => p.PropertyType == typeof(bool) && p.CanWrite))
            {
                if (!Connected)
                {
                    return;
                }

                try
                {
                    var setMethod = property.GetSetMethod();
                    if (setMethod != null && setMethod.IsPublic && !property.Name.Equals("Dhcp"))
                    {
                        setMethod.Invoke(this, new object[] { false });
                    }
                }
                catch (Exception e)
                {
                    Logging.WriteToLogFile("Unable to reset variable when disconnecting : " + e);
                }
            }
        }

        private T TryReadFromPlc<T>(PacksizePlcVariable variable)
        {
            try
            {
                return machineCommunicator.ReadValue<T>(variable);
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile("Unable to read from " + variable.VariableName + Environment.NewLine +
                                               "Trace:" + e.Message);
            }

            return default(T);
        }

        private void TryWriteToPlc(PacksizePlcVariable variable, object value)
        {
            try
            {
                WriteToMachineDelegate d = machineCommunicator.WriteValue;
                var call = Dispatcher.CurrentDispatcher.BeginInvoke(d, new[] {variable, value});
                
                if (call.Wait(new TimeSpan(0,0,0,5)) != DispatcherOperationStatus.Completed)
                    throw new Exception("Timeout when writing to " + variable);
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile("Unable to write to " + variable.VariableName + Environment.NewLine +
                                               "Trace:" + e.Message);
            }
        }

        private void OnVariableChanged(object sender, VariableChangedEventArgs e)
        {
            if (OnVariableChangedHandler != null)
            {
                OnVariableChangedHandler.Invoke(sender, e);
            }
        }

        private void OnConnectionLost(object sender, EventArgs e)
        {
            Connected = false;

            if (restServer != null)
            {
                restServer.CloseHost();
            }

            if (OnConnectionLostHandler != null)
            {
                OnConnectionLostHandler.Invoke(sender, e);
            }
        }

        private void OnConnectionEstablished(object sender, EventArgs e)
        {
            Connected = true;

            if (restServer != null)
            {
                restServer.OpenHost();
            }

            if (OnConnectionEstablishedHandler != null)
            {
                OnConnectionEstablishedHandler.Invoke(sender, e);
            }
        }
    }
}
