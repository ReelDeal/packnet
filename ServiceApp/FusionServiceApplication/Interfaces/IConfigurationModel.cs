﻿namespace FusionServiceApplication.Interfaces
{
    using Utilities.Communication;
    using Utilities.Enums;

    public interface IConfigurationModel
    {
        IIqFusionCommunicator MachineCommunicator { get; set; }
        
        string CurrentHostname
        {
            get;
            set;
        }

        NetworkMode CurrentNetworkMode
        {
            get;
            set;
        }

        string CurrentIpAddress
        {
            get;
            set;
        }

        string CurrentGateway
        {
            get;
            set;
        }

        string CurrentSubnetMask
        {
            get;
            set;
        }

        bool HasChangesBeenMade { get; set; }
        string MachineMacAddress { get; }
        string MachinePlcVersion { get; }

        void ClearNonVolatileCommunicationOptions();
        
        void SetNetworkSettingsInMachine();

        void UpdateNetworkValuesFromMachine();
    }
}
