﻿using System.Windows;

namespace FusionServiceApplication.PresentationModels
{
    public class TrackSensorControlModePresentationModel
    {
        public TrackSensorControlModePresentationModel()
        {
            SpeedValueBoxIsEnabled = false;
            SpeedValueBoxVisibility = Visibility.Hidden;
            TrackSensorControlModeButtonContent = "Start track sensor control mode";
            TrackSensorControlModeButtonMargin = new Thickness(3, 3, 3, 3);
        }

        public bool SpeedValueBoxIsEnabled { get; private set; }

        public Visibility SpeedValueBoxVisibility { get; private set; }

        public Thickness TrackSensorControlModeButtonMargin { get; private set; }

        public string TrackSensorControlModeButtonContent { get; set; }

        public void SetForTracksensorControlMode()
        {
            SpeedValueBoxIsEnabled = true;
            SpeedValueBoxVisibility = Visibility.Visible;
            TrackSensorControlModeButtonContent = "Stop";
            TrackSensorControlModeButtonMargin = new Thickness(3, 3, 3, 40);
        }

        public void Reset()
        {
            SpeedValueBoxIsEnabled = false;
            SpeedValueBoxVisibility = Visibility.Hidden;
            TrackSensorControlModeButtonContent = "Start track sensor control mode";
            TrackSensorControlModeButtonMargin = new Thickness(3, 3, 3, 3);
        }
    }
}
