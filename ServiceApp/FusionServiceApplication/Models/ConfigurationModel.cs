﻿using System.Net;

namespace FusionServiceApplication.Models
{
    using System;
    using System.Net.NetworkInformation;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Windows;

    using Utilities.Communication;
    using Utilities.Enums;

    using Interfaces;
    using Viewmodels;

    public class ConfigurationModel : IConfigurationModel
    {
        private readonly WriteToLogDelegate writeToLog;
        
        private string currentHostname;
        private string currentIpAddress;
        private string currentGateway;
        private string currentSubnetMask;
        private NetworkMode currentNetworkMode;

        public ConfigurationModel()
        {
            HasChangesBeenMade = false;
        }

        public ConfigurationModel(IIqFusionCommunicator machineCommunicator, WriteToLogDelegate writeToLog)
        {
            HasChangesBeenMade = false;
            this.writeToLog = writeToLog;
            MachineCommunicator = machineCommunicator;
            UpdateNetworkValuesFromMachine();
        }

        #region Properties
    
        public IIqFusionCommunicator MachineCommunicator { get; set; }

        public string CurrentHostname
        {
            get
            {
                return currentHostname;
            }
            set
            {
                currentHostname = value;
            }
        }

        public NetworkMode CurrentNetworkMode
        {
            get
            {
                return currentNetworkMode;
            }
            set
            {
                currentNetworkMode = value;
            }
        }

        public string CurrentIpAddress
        {
            get
            {
                return currentIpAddress;
            }
            set
            {
                currentIpAddress = value;
            }
        }

        public string CurrentGateway
        {
            get
            {
                return currentGateway;
            }
            set
            {
                currentGateway = value;
            }
        }

        public string CurrentSubnetMask
        {
            get
            {
                return currentSubnetMask;
            }
            set
            {
                currentSubnetMask = value;
            }
        }

        public bool HasChangesBeenMade { get; set; }

        public string MachineMacAddress
        {
            get
            {
                return MachineCommunicator.MachineMacAddress;
            }
        }

        public string MachinePlcVersion
        {
            get
            {
                return MachineCommunicator.MachinePlcVersion;
            }
        }

        #endregion

        public void ClearNonVolatileCommunicationOptions()
        {
            MachineCommunicator.ClearNonVolatileCommunicationOptions = true;
            writeToLog("Network settings is reset, please power cycle machine to take effect");
            MachineCommunicator.Disconnect();
        }
        
        public void SetNetworkSettingsInMachine()
        {
            if (!RelevantSettingsValid())
                return;

            if(currentNetworkMode != (NetworkMode)MachineCommunicator.CurrentNetworkMode)
                MachineCommunicator.Dhcp = currentNetworkMode == NetworkMode.DHCP;

                if (currentHostname != string.Empty &&
                    currentHostname != MachineCommunicator.CurrentHostName)
                {
                        MachineCommunicator.HostName = currentHostname;
                }
            
            
            if (currentNetworkMode != NetworkMode.DHCP)
            {
                if (currentIpAddress != string.Empty &&
                    currentIpAddress != MachineCommunicator.CurrentIpAddress)
                {
                    MachineCommunicator.StaticIp = currentIpAddress;
                }

                if (currentSubnetMask != string.Empty &&
                    currentSubnetMask != MachineCommunicator.CurrentSubnetMask)
                {
                        MachineCommunicator.SubnetMask = currentSubnetMask;
                }

                if (currentGateway != string.Empty &&
                    currentGateway != MachineCommunicator.CurrentGateway)
                {
                        MachineCommunicator.DefaultGateway = currentGateway;
                }
            }

            try
            {
                MachineCommunicator.ExecuteNetworkSettings = true;
                writeToLog("Network Set in machine, Disconnecting");
                writeToLog("Please power cycle the machine");
                MachineCommunicator.Disconnect();
            }
            catch (Exception)
            {
                writeToLog("Network Set in machine, Disconnecting");
                writeToLog("Please power cycle the machine");
                MachineCommunicator.Disconnect();
            }
        }

        private bool RelevantSettingsValid()
        {
            var errorMessage = new StringBuilder();

            if (currentNetworkMode == NetworkMode.DHCP)
            {
                if (!CheckValidHostname(currentHostname))
                {
                    errorMessage.Append("Hostname is not valid, ");
                }
            }
            else
            {
                if (!CheckValidHostname(currentHostname))
                {
                    errorMessage.Append("Hostname is not valid, ");
                }

                if (!CheckIfValidIpAddress(currentIpAddress))
                {
                    errorMessage.Append("IP-Address is not valid, ");
                }
                else if (CheckIfIpIsAlreadyInUse(currentIpAddress))
                {
                    errorMessage.Append("IP-Address is already in use, ");

                }

                if (!CheckIfValidIpAddress(currentSubnetMask))
                {
                    errorMessage.Append("Subnet mask is not valid, ");
                }

                if (!CheckIfValidIpAddress(currentGateway))
                {
                    errorMessage.Append("Gateway is not valid, ");
                }
            }

            if (errorMessage.ToString() != string.Empty)
            {
                errorMessage.Insert(0, "Error while setting network settings: ");
                errorMessage.Append("Aborting!");
                MessageBox.Show(errorMessage.ToString());
                return false;
            }
            return true;
        }

        private static bool CheckIfValidIpAddress(string s)
        {
            return Regex.IsMatch(s, "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
        }

        private static bool CheckValidHostname(string s)
        {
            return Regex.IsMatch(s, @"^[a-zA-Z0-9\-]+$");
        }

        private static bool CheckIfIpIsAlreadyInUse(string s)
        {
            try
            {
                var reply = (new Ping()).Send(s, 500);
                return reply != null && reply.Status.Equals(IPStatus.Success);
            }
            catch (Exception)
            {
                return false;
            }
        }


        public void UpdateNetworkValuesFromMachine()
        {
            currentSubnetMask = MachineCommunicator.CurrentSubnetMask;
            currentNetworkMode = (NetworkMode)MachineCommunicator.CurrentNetworkMode;
            currentIpAddress = MachineCommunicator.CurrentIpAddress;
            currentGateway = MachineCommunicator.CurrentGateway;
            currentHostname = MachineCommunicator.CurrentHostName;

            if (!CheckIfValidIpAddress(currentIpAddress))
            {
                try
                {
                    currentIpAddress = Dns.GetHostEntry(currentHostname).AddressList[0].ToString();
                }
                catch (Exception)
                {
                    currentIpAddress = (currentNetworkMode == NetworkMode.StaticIp ? "Could not resolve" : string.Empty);
                }
            }

            if (!CheckIfValidIpAddress(currentSubnetMask))
            {
                try
                {
                    throw new Exception();
                }
                catch (Exception)
                {
                    currentSubnetMask = (currentNetworkMode == NetworkMode.StaticIp ? "Could not resolve" : string.Empty);
                }
            }

            if (!CheckIfValidIpAddress(currentGateway))
            {
                try
                {
                    throw new Exception();
                }
                catch (Exception)
                {
                    currentGateway = (currentNetworkMode == NetworkMode.StaticIp
                        ? "Could not resolve"
                        : string.Empty);
                }
            }
        }
    }
}
