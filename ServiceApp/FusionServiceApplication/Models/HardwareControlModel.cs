﻿namespace FusionServiceApplication.Models
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Threading;

    using Utilities.Communication;
    using Utilities.Logging;

    using Microsoft.Win32;

    using Viewmodels;

    public class HardwareControlModel : IDisposable
    {
        public readonly IIqFusionCommunicator MachineCommunicator;
        private readonly WriteToLogDelegate writeToLog;

        private double[] lastLatchPosition;
        private Thread trackLoggingThread;

        #region ctor

        public HardwareControlModel(WriteToLogDelegate writeToLog, IIqFusionCommunicator machineCommunicator)
        {
            this.writeToLog = writeToLog;
            MachineCommunicator = machineCommunicator;

            if (MachineCommunicator != null)
            {
                MachineCommunicator.OnConnectionLostHandler += OnConnectionToMachineLost;
            }
        }

        #endregion

        #region properties

        public string TrackSensorControlModeSpeed
        {
            get { return MachineCommunicator.TrackSensorControlModeSpeed; }
            set { MachineCommunicator.TrackSensorControlModeSpeed = value; }
        }

        public bool RedLEDState
        {
            get { return MachineCommunicator.RedLEDActive; }
            set { MachineCommunicator.RedLEDActive = value; }
        }

        public bool WhiteLEDState
        {
            get { return MachineCommunicator.WhiteLEDActive; }
            set { MachineCommunicator.WhiteLEDActive = value; }
        }

        public bool GreenLEDState
        {
            get { return MachineCommunicator.GreenLEDActive; }
            set { MachineCommunicator.GreenLEDActive = value; }
        }

        public bool LatcherState
        {
            get { return MachineCommunicator.LatcherActive; }
            set { MachineCommunicator.LatcherActive = value; }
        }

        public bool RightRollerState
        {
            get { return MachineCommunicator.RightRollerActive; }
            set { MachineCommunicator.RightRollerActive = value; }
        }

        public bool LeftRollerState
        {
            get { return MachineCommunicator.LeftRollerActive; }
            set { MachineCommunicator.LeftRollerActive = value; }
        }

        public bool KnifeState
        {
            get { return MachineCommunicator.KnifeActive; }
            set { MachineCommunicator.KnifeActive = value; }
        }

        public bool TrackSensorControlModeState
        {
            get { return MachineCommunicator.TrackSensorControlMode; }
            set { MachineCommunicator.TrackSensorControlMode = value; }
        }

        public bool CrossHeadNotMoving
        {
            get
            {
                return !MachineCommunicator.Run4Ever && 
                       !MachineCommunicator.TrackSensorControlMode &&
                       !MachineCommunicator.CalibratingSwordSensorCompensations;
            }
        }

        public bool StartLightControl
        {
            set { MachineCommunicator.IoControl = value; }
        }

        protected double EPSILON
        {
            get { return 0.0000000000000000001; }
        }

        #endregion
        
        #region methods

        public void ToggleLight(string light)
        {
            StartLightControl = true;

            switch (light)
            {
                case "RedLED":
                    RedLEDState = !RedLEDState;
                    break;
                case "WhiteLED":
                    WhiteLEDState = !WhiteLEDState;
                    break;
                case "GreenLED":
                    GreenLEDState = !GreenLEDState;
                    break;
            }
        }

        public void LowerRoller()
        {
            try
            {
                MachineCommunicator.RollerActivated = true;
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile(e.ToString());
                writeToLog("Unable to preform command, aborting");
            }
        }

        public void ToggleTrackSensorControl()
        {
            var saveDiag = new SaveFileDialog();

            if (!TrackSensorControlModeState)
            {
                if (!saveDiag.ShowDialog().Equals(true))
                {
                    TrackSensorControlModeState = false;
                    return;
                }
            }

            TrackSensorControlModeState = !TrackSensorControlModeState;

            if (TrackSensorControlModeState)
            {
                trackLoggingThread = new Thread(LogTrackLatchPositions);
                trackLoggingThread.Start(saveDiag.FileName);
            }
            else
            {
                if (trackLoggingThread != null)
                {
                    trackLoggingThread.Abort();
                }
            }
        }

        public void SetTrackSensorControlModeSpeed()
        {
            try
            {
                TrackSensorControlModeSpeed = Math.Min(Math.Max(int.Parse(TrackSensorControlModeSpeed), 1), 100).ToString(CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                writeToLog("Unable to set speed on crosshead, value not an integer?");
            }
        }

        public void Dispose()
        {
            if (trackLoggingThread != null)
            {
                trackLoggingThread.Abort();
            }
        }

        private void LogTrackLatchPositions(object filename)
        {
            lastLatchPosition = new double[100];

            while (true)
            {
                try
                {
                    var latchStack = new Stack<double>();
                    var latches = MachineCommunicator.TrackLatchPositions;

                    foreach (var t in latches.TakeWhile(t => Math.Abs(t - lastLatchPosition[0]) > EPSILON && !(Math.Abs(t - 0) < EPSILON)))
                    {
                        latchStack.Push(t);
                    }

                    while (latchStack.Count != 0)
                    {
                        var entry = latchStack.Pop();

                        Logging.WriteToLogFile(entry.ToString(CultureInfo.InvariantCulture), filename as string);

                        writeToLog(entry.ToString(CultureInfo.InvariantCulture));
                    }

                    lastLatchPosition = latches;
                }
                catch (Exception e)
                {
                    Logging.WriteToLogFile(e.ToString());
                    writeToLog("Unable to read latch positions, aborting");
                    return;
                }
            }
        }

        #endregion
        
        #region events

        private void OnConnectionToMachineLost(object sender, EventArgs e)
        {
            Dispose();
        }

        #endregion
    }
}
