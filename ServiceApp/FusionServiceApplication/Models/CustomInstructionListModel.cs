﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using FusionServiceApplication.Viewmodels;

namespace FusionServiceApplication.Models
{
    using Utilities.Communication;

    public class CustomInstructionListModel
    {
        private readonly IIqFusionCommunicator machineCommunicator;
        private readonly ObservableCollection<List<string>> currentInstructionList;
        private readonly WriteToLogDelegate writeToLog;

        #region ctor

        public CustomInstructionListModel(IIqFusionCommunicator machineCommunicator, WriteToLogDelegate writeToLog)
        {
            this.machineCommunicator = machineCommunicator;
            this.writeToLog = writeToLog;
            currentInstructionList = new ObservableCollection<List<string>>();
        }

        #endregion

        #region properties

        public ObservableCollection<List<string>> CurrentInstructionList
        {
            get { return currentInstructionList; }
        }

        public List<string> SelectedInstruction { get; set; }


        #endregion

        #region methods


        #endregion

        public void StartExecution(string pathToCodeFile)
        {
            throw new NotImplementedException();
        }

        public void StopExecution()
        {
            throw new NotImplementedException();
        }
    }
}
