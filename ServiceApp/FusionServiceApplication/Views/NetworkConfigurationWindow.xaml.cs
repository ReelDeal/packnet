﻿namespace FusionServiceApplication.Views
{
    using System;
    using System.ComponentModel;
    using Viewmodels;

    /// <summary>
    /// Interaction logic for NetworkConfigurationWindow.xaml
    /// </summary>
    public partial class NetworkConfigurationWindow
    {
        public NetworkConfigurationWindow()
        {
            InitializeComponent();
        }

        public NetworkConfigurationWindow(NetworkConfigurationWindowViewModel model)
        {
            InitializeComponent();

            DataContext = model;
        }

        private void OnClose(object sender, CancelEventArgs e)
        {
            var model = DataContext as NetworkConfigurationWindowViewModel;

            if (model != null)
            {
                model.WindowOnCloseDelegate(this);
            }
            else
            {
                throw new Exception("ViewModel for NetworkConfigurationWindow == NULL");
            }
        }
    }
}
