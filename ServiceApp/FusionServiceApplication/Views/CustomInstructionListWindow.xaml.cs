﻿namespace FusionServiceApplication.Views
{
    using System;
    using System.ComponentModel;
    using System.Windows.Controls;

    using Viewmodels;

    /// <summary>
    /// Interaction logic for CustomInstructionListWindow.xaml
    /// </summary>
    public partial class CustomInstructionListWindow
    {
        private string textBoxTemp = string.Empty;

        public CustomInstructionListWindow(CustomInstructionListViewModel viewModel)
        {
            InitializeComponent();
            DataContext = viewModel;
        }

        private void InstructionEntryBoxGotFocus(object sender, EventArgs args)
        {
            var textBox = sender as TextBox;
            
            if (textBox == null || !textBox.IsEnabled)
            {
                return;
            }
            
            textBoxTemp = textBox.Text;
            textBox.Text = string.Empty;
        }

        private void InstructionEntryBoxLostFocus(object sender, EventArgs args)
        {
            var textBox = sender as TextBox;

            if (textBox == null || !textBox.IsEnabled)
            {
                return;
            }

            if (textBox.Text == string.Empty)
            {
                textBox.Text = textBoxTemp;
            }
        }

        private void OnClose(object sender, CancelEventArgs e)
        {
            var model = DataContext as CustomInstructionListViewModel;

            if (model != null)
            {
                model.WindowOnCloseDelegate(this);
            }
            else
            {
                throw new Exception("ViewModel for CustomInstructionListWindow == NULL");
            }
        }
    }
}
