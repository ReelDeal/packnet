﻿using System;
using System.ComponentModel;
using System.Windows;

using FusionServiceApplication.Viewmodels;

namespace FusionServiceApplication.Views
{
    /// <summary>
    /// Interaction logic for NetworkScannerWindow.xaml
    /// </summary>
    public partial class NetworkScannerWindow : Window
    {
        public NetworkScannerWindow()
        {
            InitializeComponent();
        }

        private void OnClose(object sender, CancelEventArgs e)
        {
            var model = DataContext as NetworkScannerViewModel;

            if (model != null)
            {
                model.WindowOnCloseDelegate(this);
                model.Dispose();
            }
            else
            {
                throw new Exception("ViewModel for NetworkScanner == NULL");
            }
        }
    }
}
