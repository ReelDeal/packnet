﻿using System;
using System.ComponentModel;
using System.Windows;

using FusionServiceApplication.Viewmodels;

namespace FusionServiceApplication.Views
{
    /// <summary>
    /// Interaction logic for ConfigEditor.xaml
    /// </summary>
    public partial class ConfigEditor : Window
    {
        public ConfigEditor()
        {
            InitializeComponent();
        }

        private void ConfigEditor_OnClosing(object sender, CancelEventArgs e)
        {
            var model = DataContext as ConfigEditorViewModel;

            if (model != null)
            {
                model.WindowOnCloseDelegate(this);
            }
            else
            {
                throw new Exception("ViewModel for ConfigEditorViewModel == NULL");
            }
        }
    }
}
