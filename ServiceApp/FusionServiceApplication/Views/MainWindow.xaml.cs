﻿namespace FusionServiceApplication.Views
{
    using System;
    using System.Windows;

    using Viewmodels;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void FusionServiceApplicationOnClose(object sender, EventArgs e)
        {
            var viewModel = DataContext as MainWindowViewModel;
            if (viewModel != null)
            {
                viewModel.DisconnectMachineCommand.Execute(null);
                viewModel.CloseWindowCommand.Execute(null);
            }

            if (Application.Current != null)
            {
                Application.Current.Shutdown(0);
            }
        }
    }
}
