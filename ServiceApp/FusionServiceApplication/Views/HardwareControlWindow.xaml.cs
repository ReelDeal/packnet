﻿namespace FusionServiceApplication.Views
{
    using System;
    using System.ComponentModel;

    using Viewmodels;

    public partial class HardwareControlWindow
    {
        public HardwareControlWindow(HardwareControlWindowViewModel hardwareControlWindowViewModel)
        {
            InitializeComponent();

            DataContext = hardwareControlWindowViewModel;
        }

        private void OnClose(object sender, CancelEventArgs e)
        {
            var model = DataContext as HardwareControlWindowViewModel;

            if (model != null)
            {
                model.WindowOnCloseDelegate(this);
            }
            else
            {
                throw new Exception("ViewModel for HardwareControlWindow == NULL");
            }

            model.AbortThreads();
        }
    }
}
