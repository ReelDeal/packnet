﻿namespace FusionServiceApplication.Managers
{
    using System;
    using System.Threading;
    using Helpers;
    using Utilities.Communication;
    using Utilities.Logging;
    using Viewmodels;
    using Views;
    using Onyx.Windows;

    public delegate void UpdatePackageStatistics(int producedOnTrack, double crossheadDistanceMoved, int nrOfLongheads);

    public delegate void UpdateLastSentPackageTime();

    public class Run4EverManager : IDisposable
    {
        private readonly IqFusionCommunicator machineCommunicator;
        private readonly WriteToLogDelegate writeToLog;

        private readonly Run4EverHandler run4EverHandler;
        private readonly Run4EverStatisticsViewModel statisticsViewModel;
        private readonly Run4EverStatisticsWindow statisticsWindow;

        private Thread workerThread;

        private DateTime startTime;
        private DateTime lastSentPackageTime;

        private DateTime lastLog;

        public Run4EverManager(IqFusionCommunicator machineCommunicator, WriteToLogDelegate writeToLog, View baseView)
        {
            this.machineCommunicator = machineCommunicator;
            this.writeToLog = writeToLog;
            lastLog = DateTime.Now;
            run4EverHandler = new Run4EverHandler(UpdatePackageStatistics, this.machineCommunicator);

            statisticsViewModel = new Run4EverStatisticsViewModel(baseView);
            statisticsWindow = new Run4EverStatisticsWindow { DataContext = statisticsViewModel };
        }

        public event UpdateLastSentPackageTime OnNewPackageSentToMachine;

        public string CurrentRunTime
        {
            get
            {
                return (lastSentPackageTime - startTime).ToString();
            }
        }

        public int NrOfPackagesSent 
        {
            get
            {
                return statisticsViewModel.NrOfBoxesProduced;
            }
        }

        public void Dispose()
        {
            if (workerThread != null && workerThread.IsAlive)
            {
                workerThread.Abort(0);
            }

            if (statisticsWindow != null)
            {
                statisticsWindow.Close();
            }
        }

        public void Start()
        {
            startTime = DateTime.Now;
            Run4Ever();

            if (statisticsWindow != null)
            {
                if (!statisticsWindow.IsVisible)
                {
                    statisticsWindow.Show();
                }
                else
                {
                    statisticsWindow.Focus();
                }
            }
        }

        public void End()
        {
            Run4Ever();
            Dispose();
        }

        private void Run4Ever()
        {
            if (machineCommunicator == null || !machineCommunicator.Connected)
            {
                writeToLog("Unable to start Run4Ever, No connection");
                return;
            }

            if (machineCommunicator.Run4Ever)
            {
                writeToLog("Stopping Run4Ever");

                machineCommunicator.Run4Ever = false;

                if (workerThread != null)
                {
                    workerThread.Abort(0);
                    workerThread = null;
                }
            }
            else
            {
                writeToLog("Starting Run4Ever");

                machineCommunicator.LongHeadPositionsReady = false;
                machineCommunicator.Run4Ever = true;

                workerThread = new Thread(() => run4EverHandler.Run4Ever());
                workerThread.Start();
            }
        }

        private void UpdatePackageStatistics(int producedOnTrack, double crossheadDistanceMoved, int nrOfLongheads)
        {
            lastSentPackageTime = DateTime.Now;

            if (statisticsViewModel != null)
            {
                statisticsViewModel.NrOfBoxesProduced++;
                statisticsViewModel.CrossheadDistanceMoved += crossheadDistanceMoved;
                statisticsViewModel.LongheadMovements += nrOfLongheads;
                statisticsViewModel.NrOfFeedRollerDownAfterLhRepositioning =
                    machineCommunicator.NrOfFeedRollerDownAfterLhRepositioning;

                statisticsViewModel.NrOfLongheadPositioningRetries =
                    machineCommunicator.NrOfLongheadPositioningRetries;

                if (producedOnTrack == 1)
                {
                    statisticsViewModel.LeftPressureRollerLifts++;
                }
                else
                {
                    statisticsViewModel.RightPressureRollerLifts++;
                }
            }

            if (Math.Abs(DateTime.Now.Minute - lastLog.Minute) > 15)
            {
                if (statisticsViewModel != null)
                {
                    Logging.WriteToLogFile(
                        string.Format(
                            "Boxes produced: {0}, Crosshead movement distance: {1}, LongheadMovements {2}, Right pressure roller lifts {3}, Left pressure roller lifts {4}",
                            statisticsViewModel.NrOfBoxesProduced,
                            statisticsViewModel.CrossheadDistanceMoved,
                            statisticsViewModel.LongheadMovements,
                            statisticsViewModel.RightPressureRollerLifts,
                            statisticsViewModel.LeftPressureRollerLifts),
                        @".\Run4EverStatistics.txt");
                }

                lastLog = DateTime.Now;
            }

            OnNewPackageSentToMachine.Invoke();
        }
    }
}
