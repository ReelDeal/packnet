﻿
namespace FusionServiceApplication.Managers
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Threading;
    using System.Windows;

    using Utilities.Communication;
    using Utilities.Logging;

    using Helpers;
    using Viewmodels;

    class MachineCalibrationHandler : IDisposable
    {
        private readonly IqFusionCommunicator machineCommunicator;
        private readonly WriteToLogDelegate writeToLog;

        public MachineCalibrationHandler(IqFusionCommunicator machineCommunicator, WriteToLogDelegate writeToLog)
        {
            this.machineCommunicator = machineCommunicator;
            this.writeToLog = writeToLog;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void CalibrateTracks()
        {
            if (
                MessageBox.Show("Make sure that no corrugate is loaded.", "No corrugate loaded?",
                    MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                if (machineCommunicator.CalibratingSwordSensorCompensations)
                {
                    writeToLog("Unable to preform action (Already calibrating)");
                    return;
                }

                if (!CheckIfOkToMove())
                {
                    return;
                }

                machineCommunicator.CalibratingSwordSensorCompensations = true;

                if (!machineCommunicator.CalibratingSwordSensorCompensations)
                {
                    writeToLog("Unable to set RUN_MACHINE_CALIBRATION variable in the PLC");
                    return;
                }

                writeToLog("Started machine calibration");

                new Thread(WaitForCalibrationDone).Start();
            }
        }


        private bool CheckIfOkToMove()
        {
            if (machineCommunicator.LatcherActive)
            {
                writeToLog("Command not avaliable, latcher is activated");
                return false;
            }

            return true;
        }

        private void WaitForCalibrationDone()
        {
            try
            {
                while (machineCommunicator.CalibratingSwordSensorCompensations)
                {
                    Thread.Sleep(1000);
                }

                //  this.CalibrateOutOfCorrugatePositions();

                writeToLog("Done");
                FinishCalibration();
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile(e.ToString());
                machineCommunicator.CalibratingSwordSensorCompensations = false;
                writeToLog("Exception occured, aborting calibration");
            }
        }

        private void CalibrateOutOfCorrugatePositions()
        {
            machineCommunicator.TrackSensorControlMode = true;
            Thread.Sleep(10000);
            machineCommunicator.TrackSensorControlMode = false;

            var latchPositions = machineCommunicator.TrackLatchPositions;


            var track1Latches = latchPositions.OrderBy(latch => latch).Take(2);
            var track2Latches = latchPositions.OrderByDescending(latch => latch).Take(2);

            var track1OutOfCorrugate = track1Latches.First() + ((track1Latches.First() - track1Latches.Last()) / 2);
            var track2OutOfCorruage = track2Latches.Last() + ((track2Latches.First() - track2Latches.Last()) / 2);

            //TODO skriv ooc till maskin xmlen borde serialiseras

        }

        private void FinishCalibration()
        {
            var sensorCompensationLeft1 = machineCommunicator.SwordSensorCompensationLeft1;
            var sensorCompensationLeft2 = machineCommunicator.SwordSensorCompensationLeft2;
            var sensorCompenstaionRight1 = machineCommunicator.SwordSensorCompensationRight1;
            var sensorCompenstaionRight2 = machineCommunicator.SwordSensorCompensationRight1;

            writeToLog("Track 1 Left sensor compensations: ");
            foreach (var d in sensorCompensationLeft1)
            {
                writeToLog(d.ToString(CultureInfo.InvariantCulture));
            }

            writeToLog("Track 2 Left sensor compensations: ");
            foreach (var d in sensorCompensationLeft2)
            {
                writeToLog(d.ToString(CultureInfo.InvariantCulture));
            }

            writeToLog("Track 1 Right sensor compensations: ");
            foreach (var d in sensorCompenstaionRight1)
            {
                writeToLog(d.ToString(CultureInfo.InvariantCulture));
            }

            writeToLog("Track 2 Right sensor compensations: ");

            foreach (var d in sensorCompenstaionRight2)
            {
                writeToLog(d.ToString(CultureInfo.InvariantCulture));
            }

            MachineXmlHandler.WriteToMachineXml(sensorCompensationLeft1, sensorCompensationLeft2, sensorCompenstaionRight1, sensorCompenstaionRight2, writeToLog);

            writeToLog("Calibration completed");
        }
    }
}
