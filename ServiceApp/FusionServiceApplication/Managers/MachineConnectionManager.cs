﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using FusionServiceApplication.Helpers;
using FusionServiceApplication.Viewmodels;
using PackNet.Communication.PLCBase;

namespace FusionServiceApplication.Managers
{
    using SettingHandlers;
    using Utilities.Communication;
    using Utilities.Logging;

    public class MachineConnectionManager : IDisposable
    {
        private IqFusionCommunicator machineCommunicator;
        private readonly WriteToLogDelegate writeToLog;
        private Thread connectionThread;
        private ObservableCollection<ErrorCode> errorCodes;

        private string machineIp = CoreSettings.Instance.DefaultConnectionAddress;

        private object connectingLock = new object();

        public event EventHandler OnCleanUpRequestedEvent;
        public event EventHandler OnDisconnectedEvent;
        public event EventHandler OnConnectedEvent;
        public event EventHandler OnMachineErrorsUpdatedNeeded;


        public MachineConnectionManager(WriteToLogDelegate writeToLog)
        {
            this.writeToLog = writeToLog;
            errorCodes = new ObservableCollection<ErrorCode>();
        }

        public MachineConnectionManager(WriteToLogDelegate writeToLog, string ipAddress)
        {
            this.writeToLog = writeToLog;
            errorCodes = new ObservableCollection<ErrorCode>();
            machineIp = ipAddress;
        }

        public ObservableCollection<ErrorCode> ErrorCodes
        {
            get
            {
                return errorCodes;
            }

            set
            {
                errorCodes = value;
                
            }
        }

        public void Dispose()
        {
            if (machineCommunicator != null && machineCommunicator.Connected)
            {
                Disconnect();
                OnDisconnectedEvent.Invoke(null, null);
            }
        }

        public IqFusionCommunicator MachineCommunicator
        {
            get { return machineCommunicator; }
        }

        public string MachineIp
        {
            get { return machineIp; }
            set { machineIp = value; }
        }

        public bool HasConnection
        {
            get { return machineCommunicator != null && machineCommunicator.Connected; }
        }

        public void Connect()
        {
            if (machineCommunicator != null && machineCommunicator.Connected)
            {
                writeToLog("Already connected");
                return;
            }

            lock (connectingLock)
            {
                var wantedIpAddress = Networking.GetIp(machineIp);

                if (!Networking.AbleToReachHost(wantedIpAddress))
                {
                    writeToLog(
                        "Unable to reach host on"
                        + ((string.IsNullOrEmpty(wantedIpAddress) ? " hostname: " : " ip: ") + machineIp));
                    Logging.WriteToLogFile(
                        "Unable to reach host on ip: "
                        + ((string.IsNullOrEmpty(wantedIpAddress) ? " hostname: " : " ip: ") + machineIp));
                    return;
                }

                writeToLog("Trying to connect on: " + machineIp);

                machineCommunicator = new IqFusionCommunicator(wantedIpAddress);

                WireEvents();

                connectionThread = new Thread(o => ConnectionThread());
                connectionThread.Start();
            }
        }

        private bool TryToConnect()
        {
            var wasConnectionMade = false;
            try
            {
                wasConnectionMade = machineCommunicator.Connect();
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile("unable to connect: " + e.Message);
            }
            finally
            {
                if (!wasConnectionMade)
                {
                    machineCommunicator.Disconnect();
                }
            }

            return wasConnectionMade;
        }

        private void ConnectionThread()
        {
            try
            {
                if (TryToConnect())
                {
                    return;
                }
                writeToLog("Unable to connect: See error log for details");
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile(e.ToString());
            }
        }

        public void Disconnect()
        {
            if (machineCommunicator == null || !machineCommunicator.Connected)
            {
                writeToLog("Not connected");
                return;
            }
            machineCommunicator.Disconnect();
        }


        private void WireEvents()
        {
            machineCommunicator.OnConnectionEstablishedHandler += OnConnected;
            machineCommunicator.OnConnectionLostHandler += OnConnectionLost;
            machineCommunicator.OnVariableChangedHandler += OnVariableChanged;
        }

        private void OnConnected(object obj, EventArgs eventArgs)
        {
            if (!PhyscialMachineSettingsHelper.SyncMachine(machineCommunicator))
            {
                writeToLog("Unable to synchronize machine | check error log");
                Disconnect();
                return;
            }

            machineCommunicator.ServiceMode = true;
            GetInitialErrors();

            var handler = OnConnectedEvent;
            if (handler != null)
            {
                handler.Invoke(null, null);
            }
            writeToLog("Connection established");
        }

        private void GetInitialErrors()
        {
            var errors = machineCommunicator.GetMachineErrors();
            if (App.Current.Dispatcher.CheckAccess() == false)
            {
                App.Current.Dispatcher.Invoke(addErrors, ErrorCodes, errors);
            }
            else
            {
                addErrors(ErrorCodes, errors);
            }
            var handler = OnMachineErrorsUpdatedNeeded;
            if (handler != null)
            {
                handler.Invoke(null, null);
            }
        }

        private delegate void AddErrors(ObservableCollection<ErrorCode> errorCodes, IEnumerable<ErrorCode> errorsToAdd);

        private readonly AddErrors addErrors = delegate(ObservableCollection<ErrorCode> errorCodes, IEnumerable<ErrorCode> errorsToAdd)
            {
                foreach (ErrorCode error in errorsToAdd)
                {
                    errorCodes.Add(error);
                }
            };

        private void OnConnectionLost(object obj, EventArgs eventArgs)
        {
            if (Application.Current == null)
            {
                return;
            }
            Application.Current.Dispatcher.BeginInvoke(
                                                  DispatcherPriority.Background,
                                                  new Action(CleanUp));

            writeToLog("Connection lost, if this is unexpected. Check the error log");
        }

        private void OnVariableChanged(object sender, VariableChangedEventArgs e)
        {
            if (e.VariableName == machineCommunicator.VariableMap.ErrorCode1.VariableName ||
                e.VariableName == machineCommunicator.VariableMap.ErrorCode2.VariableName ||
                e.VariableName == machineCommunicator.VariableMap.ErrorCode3.VariableName ||
                e.VariableName == machineCommunicator.VariableMap.ErrorCode4.VariableName ||
                e.VariableName == machineCommunicator.VariableMap.ErrorCode5.VariableName)
            {
                var errorCode = Convert.ToUInt16(e.Value);
                if (errorCode == 0)
                {
                    OnErrorCleared(e.VariableName);
                }
                else
                {
                    var args = new string[] { };

                    if (e.VariableName == machineCommunicator.VariableMap.ErrorCode1.VariableName)
                    {
                        args = machineCommunicator.ErrorCode1Arguments;
                    }
                    else if (e.VariableName == machineCommunicator.VariableMap.ErrorCode2.VariableName)
                    {
                        args = machineCommunicator.ErrorCode2Arguments;
                    }
                    else if (e.VariableName == machineCommunicator.VariableMap.ErrorCode3.VariableName)
                    {
                        args = machineCommunicator.ErrorCode3Arguments;
                    }
                    else if (e.VariableName == machineCommunicator.VariableMap.ErrorCode4.VariableName)
                    {
                        args = machineCommunicator.ErrorCode4Arguments;
                    }
                    else if (e.VariableName == machineCommunicator.VariableMap.ErrorCode5.VariableName)
                    {
                        args = machineCommunicator.ErrorCode5Arguments;
                    }

                    OnErrorOccured(e.VariableName, e.Value, args);
                }
                var handler = OnMachineErrorsUpdatedNeeded;
                if (handler != null)
                {
                    handler.Invoke(null, null);
                }
            }
        }

        private void OnErrorOccured(string name, object value, string[] args)
        {
            Application.Current.Dispatcher.Invoke(
                                                  DispatcherPriority.Background,
                                                  new Action(() => errorCodes.Add(new ErrorCode(name, value, args))));
        }

        private void OnErrorCleared(string name)
        {
            if (ErrorCodes.Count <= 0)
            {
                return;
            }

            try
            {
                var index = ErrorCodes.First(err => err.Name.Equals(name));

                if (index != null)
                {
                    Application.Current.Dispatcher.Invoke(
                                                          DispatcherPriority.Background,
                                                          new Action(() => ErrorCodes.RemoveAt(ErrorCodes.IndexOf(index))));
                }
            }
            catch (Exception)
            {
            }
        }

        private void CleanUp()
        {
            if (machineCommunicator != null && machineCommunicator.Connected)
            {
                machineCommunicator.Disconnect();
                machineCommunicator = null;
            }

            var handler = OnCleanUpRequestedEvent;
            if (handler != null)
            {
                handler.Invoke(null, null);
            }

            handler = OnDisconnectedEvent;
            if (handler != null)
            {
                handler.Invoke(null,null);
            }
        }
    }
}
