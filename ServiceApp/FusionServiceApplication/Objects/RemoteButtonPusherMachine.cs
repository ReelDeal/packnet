﻿namespace FusionServiceApplication.Objects
{
    using SettingHandlers.MachinesConfiguration;

    public class RemoteButtonPusherMachine
    {
        public RemoteButtonPusherMachine(Machine machineInstance)
        {
            MachineInstance = machineInstance;
        }

        public Machine MachineInstance { get; set; }

        public override string ToString()
        {
            return MachineInstance.Alias;
        }
    }
}
