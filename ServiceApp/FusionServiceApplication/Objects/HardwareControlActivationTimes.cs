﻿namespace FusionServiceApplication.Objects
{
    using System;

    public class HardwareControlActivationTimes
    {
        public HardwareControlActivationTimes()
        {
            LastLatchTime = new DateTime();
            LastRightRollerActivateTime = new DateTime();
            LastLeftRollerActivateTime = new DateTime();
            LastKnifeActivation = new DateTime();
        }

        public DateTime LastLatchTime { get; set; }

        public DateTime LastRightRollerActivateTime { get; set; }

        public DateTime LastLeftRollerActivateTime { get; set; }

        public DateTime LastKnifeActivation { get; set; }  
    }
}
