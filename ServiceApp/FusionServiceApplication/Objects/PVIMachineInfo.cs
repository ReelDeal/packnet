﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace FusionServiceApplication.Objects
{
    using BR.AN.PviServices;

    using Exception = Exception;

    public class PVIMachineInfo
    {
        private readonly List<Variable> snmpVariables;

        public PVIMachineInfo(List<Variable> snmpVariables)
        {
            this.snmpVariables = snmpVariables;
        }

        public string MacAddress
        {
            get
            {
                var macVariable = snmpVariables.First(var => var.Name.Equals("macAddress"));

                return macVariable.Value;
            }
        }

        public string IpAddress
        {
            get
            {
                var ipVariable = snmpVariables.First(var => var.Name.Equals("ipAddress"));

                return ipVariable.Value;
            }
        }

        public string Gateway
        {
            get
            {
                return string.Empty;
            }
        }

        public string SubnetMask
        {
            get
            {
                return string.Empty;
            }
        }

        public string PlcVersion
        {
            get
            {
                try
                {
                    var uri = new Uri(@"http://" + IpAddress + @"/GetVariable?Main:PLC_VERSION");
                    WebRequest request = WebRequest.Create(uri);
                    WebResponse response = request.GetResponse();

                    var responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var reader = new StreamReader(responseStream);

                        var content = reader.ReadToEnd();
                        content = content.Replace("{\"Main:PLC_VERSION\":\"", "");
                        content = content.Replace("\"}", "");
                        return content;
                    }
                    throw new Exception("Unable to get response stream");
                }
                catch (Exception)
                {
                    return "Unknown";
                }

            }
        }
    }
}
