﻿using PackNet.Communication.PLCBase;

namespace FusionServiceApplication.Objects
{
    public class PlcVariableEntry
    {
        private readonly string variableName;

        public PlcVariableEntry()
        {
            variableName = string.Empty;
            Value = string.Empty;
//            this.UpdateInPlc = false;
            PlcVariable = new PacksizePlcVariable();
        }

        public PlcVariableEntry(PacksizePlcVariable variable, string variableValue, bool update)
        {
            variableName = variable.VariableName;
            Value = variableValue;
       //     this.UpdateInPlc = update;
            PlcVariable = variable;
        }

        public PlcVariableEntry(PacksizePlcVariable variable)
        {
            PlcVariable = variable;
            variableName = variable.VariableName;
        }

        public PacksizePlcVariable PlcVariable { get; set; }

        public string Variable
        {
            get { return variableName; }
        }

        public string Value { get; set; }

   //     public bool UpdateInPlc { get; set; }
    }
}
