﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BR.AN.PviServices;
using Exception = System.Exception;

namespace FusionServiceApplication.Objects
{
    using System.Windows;

    class PviManagerHandler : IDisposable
    {
        private Service snmpService;

        private readonly List<PVIMachineInfo> machineInfo = new List<PVIMachineInfo>();

        #region ctor

        public PviManagerHandler()
        {
            ConnectPVIService();
        }

        public void Dispose()
        {
            snmpService.Remove();
            snmpService.Disconnect();
            snmpService.Dispose();
            snmpService = null;
        }

        #endregion

        #region properties

        public bool SearchPreformed { get; private set; }

        public ObservableCollection<PVIMachineInfo> MachineInfo
        {
            get
            {
                return new ObservableCollection<PVIMachineInfo>(machineInfo);
            }
        }

        #endregion

        #region methods
        public void ConnectPVIService()
        {
            try
            {
                if (snmpService == null)
                {
                    snmpService = new Service("LocalSNMPBrowser");
                    snmpService.SNMP.ResponseTimeout = 500;
                    snmpService.Connected += OnServiceConnected;
                    snmpService.Connect();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Unable to load PVI Manager, please download from br-automation.com and install");
            }
        }

        public void SearchForDevices()
        {
            SearchPreformed = false;
            snmpService.SNMP.SearchCompleted += OnSearchCompleted;
            snmpService.SNMP.Error += OnErrorWhenSearchingForDevices;
            snmpService.SNMP.Search();
        }

        private void ReadMachineVariables()
        {
            foreach (NetworkAdapter nwAdapt in snmpService.SNMP.NetworkAdapters.Values)
            {
                nwAdapt.Variables.ValuesRead += OnMachineVariablesRead;
                nwAdapt.Variables.Read();
            }
            SearchPreformed = true;
        }
        
        private void GetStationVariableList()
        {
            if (snmpService.SNMP.NetworkAdapters.Count == 0)
            {
                SearchPreformed = true;
                return;
            }

            snmpService.SNMP.NetworkAdapters.SearchCompleted += OnStationVariablesListGettingCompleted;
            snmpService.SNMP.NetworkAdapters.Error += OnErrorWhileGettingStationVariablesList;
            snmpService.SNMP.NetworkAdapters.Search();
        }

        #endregion

        #region eventhandlers

        private void OnServiceConnected(object sender, PviEventArgs e)
        {
        }

        private void OnSearchCompleted(object sender, ErrorEventArgs e)
        {
            GetStationVariableList();
        }

        private void OnErrorWhenSearchingForDevices(object sender, ErrorEventArgs e)
        {
            throw new Exception("Error while searching for devices: " + e.ErrorInfo);
        }

        private void OnMachineVariablesRead(object sender, ErrorEventArgs e)
        {
            ((SNMPVariableCollection)sender).ValuesRead -= OnMachineVariablesRead;

            var variables = ((SNMPVariableCollection) sender).Values.Cast<Variable>().ToList();

            var machineInfoObject = new PVIMachineInfo(variables);

            machineInfo.Add(machineInfoObject);
        }

        private void OnStationVariablesListGettingCompleted(object sender, ErrorEventArgs e)
        {
            ReadMachineVariables();
        }

        private void OnErrorWhileGettingStationVariablesList(object sender, ErrorEventArgs e)
        {
            throw new Exception("Error while searching for variable list: " + e.ErrorInfo);
        }

        #endregion;

    }
}
