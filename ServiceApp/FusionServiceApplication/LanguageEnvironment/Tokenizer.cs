﻿

namespace FusionServiceApplication.LanguageEnvironment
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Exceptions;
    using Factories;

    public class Tokenizer
    {
        private IList<string> fileLines = new List<string>(); 

        private readonly TokenFactory tokenFactory = new TokenFactory();

        public void loadFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("File not found", filePath);
            }

            var sr = new StreamReader(new FileStream(filePath, FileMode.Open));
            fileLines = sr.ReadToEnd().Split('\n').ToList();
            sr.Close();

            if (fileLines.Count <= 0 || (fileLines.Count == 1 && fileLines[0].Equals(string.Empty)))
            {
                throw new NoFileContentsException();
            }
        }

        public IList<string> FileLines
        {
            get
            {
                return fileLines;
            }
        }

        public List<Token> getTokens()
        {
            if (fileLines.Count <= 0)
            {
                throw new FileNotLoadedException();
            }

            var tokens = new List<Token>();

            var segmentStarted = false;

            for (var index = 0; index < fileLines.Count; index++)
            {
                var line = fileLines[index];

                if (tokenFactory.isComment(line) || tokenFactory.isEmpty(line))
                {
                    continue;
                }

                if (tokenFactory.isStartToken(line) && segmentStarted == false)
                {
                    segmentStarted = true;
                }

                if (segmentStarted)
                {
                    var nextToken = tokenFactory.getInstance(line);
                    nextToken.LineNumber = index + 1;

                    tokens.Add(nextToken);
                }

                if (tokenFactory.isEndToken(line) && segmentStarted)
                {
                    segmentStarted = false;
                }
            }

            /*
            for (var i = 0; i < this.FileLines.Count; i++)
            {
                if (!this.tokenFactory.isToken(this.fileLines[i]))
                {
                    continue;
                }

                var nextToken = this.tokenFactory.getInstance(this.fileLines[i]);
                nextToken.LineNumber = i + 1;

                tokens.Add(nextToken);
            }*/

            return tokens;
        }
    }
}
