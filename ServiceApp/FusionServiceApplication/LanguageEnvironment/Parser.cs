﻿
namespace FusionServiceApplication.LanguageEnvironment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Exceptions;
    using Factories;
    using Utilities.InstructionListStuff;

    public class Parser
    {
        private readonly IList<Token> loggingBlock = new List<Token>();

        private readonly IList<Token> machineInfoBlock = new List<Token>(); 

        private readonly IList<IList<Token>> declarationBlocks = new List<IList<Token>>();

        private readonly IList<IList<Token>> executionBlocks = new List<IList<Token>>();

        private int instructionListIterator;

        public IList<IList<Token>> DeclarationBlocks
        {
            get
            {
                return declarationBlocks;
            }
        }

        public IList<IList<Token>> ExecutionBlocks
        {
            get
            {
                return executionBlocks;
            }
        }

        public IList<Token> LoggingBlock
        {
            get
            {
                return loggingBlock;
            }
        }

        public IList<Token> MachineInfoBlock
        {
            get
            {
                return machineInfoBlock;
            }
        }

        public string CurrentPackageName
        {
            get
            {
                return declarationBlocks[instructionListIterator][0].Arguments[0];
            }
        }

        public string[] getNextInstructionList()
        {
            var declarationBlock = declarationBlocks[instructionListIterator];
            var executionBlock =
                executionBlocks.First(
                    x =>
                    x.First(token => token.TokenId == TokenId.BEGIN_PACKAGE).Arguments[0].Equals(
                        declarationBlock.First(token => token.TokenId.Equals(TokenId.DECLARE_PACKAGE)).Arguments[0]));

            if (executionBlock == null)
            {
                throw new CouldNotFindExecutionalCounterPartException();
            }

            var nextInstructionList = MetadataInstructionFactory.getInstance(declarationBlock).ToInstructionEntry();
            try
            {
                for (var i = 1; i < executionBlock.Count - 1; i++)
                {
                    nextInstructionList.AddRange(InstructionFactory.getInstance(executionBlock[i]).ToInstructionEntry());
                }

                nextInstructionList.Add(InstructionListMarkers.StopMarker);
            }
            catch (Exception)
            {
                instructionListIterator = (instructionListIterator + 1) % declarationBlocks.Count;
                throw;
            }

            instructionListIterator = (instructionListIterator + 1) % declarationBlocks.Count;


            return nextInstructionList.ToArray();
        }

        public void parseTokens(IList<Token> tokens)
        {
            parseBlocks(tokens);

            setupMachineInfo();
            setupLogging();

            checkBlocks();

            instructionListIterator = 0;
        }

        private void setupLogging()
        {
            //TODO FIXA
        }

        private void setupMachineInfo()
        {
            var tracksToken = machineInfoBlock.FirstOrDefault(x => x.TokenId.Equals(TokenId.TRACKS));
            var longheadNrToken = machineInfoBlock.FirstOrDefault(x => x.TokenId.Equals(TokenId.LONGHEADS));
            var metricToken = machineInfoBlock.FirstOrDefault(x => x.TokenId.Equals(TokenId.METRIC));

            try
            {

                if (tracksToken != null)
                {
                    MachineInfo.Instance.Tracks = int.Parse(tracksToken.Arguments[0]);
                }

                if (longheadNrToken != null)
                {
                    MachineInfo.Instance.LongheadCount = int.Parse(longheadNrToken.Arguments[0]);
                }

                if (metricToken != null)
                {
                    MachineInfo.Instance.Metric = bool.Parse(metricToken.Arguments[0]);
                }

            }
            catch (Exception)
            {
                throw new UnableToParseMachineDeclarationException();
            }
        }

        private void parseBlocks(IEnumerable<Token> tokens)
        {
            var tokensToParse = new List<Token>(tokens);

            parseMachineInfoBlock(tokensToParse);
            parseLoggingBlock(tokensToParse);
            parseDeclarationBlocks(tokensToParse);
            parseExecutionalBlocks(tokensToParse);
        }

        private void parseLoggingBlock(List<Token> tokensToParse)
        {
            if (tokensToParse.FirstOrDefault(x => x.TokenId.Equals(TokenId.LOGGING)) == null)
            {
                return;
            }

            tokensToParse.ToList()
                      .GetRange(
                          tokensToParse.IndexOf(
                              tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.LOGGING))),
                          tokensToParse.IndexOf(
                              tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.END_LOGGING)))
                          - tokensToParse.IndexOf(tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.LOGGING))) + 1).ForEach(loggingBlock.Add);
            
            foreach (var token in loggingBlock)
            {
                tokensToParse.Remove(token);
            }

            if (tokensToParse.FirstOrDefault(x => x.TokenId.Equals(TokenId.LOGGING)) != null)
            {
                throw new MultipleLoggingDeclarationsException();
            }
        }

        private void parseMachineInfoBlock(List<Token> tokensToParse)
        {
            if (tokensToParse.FirstOrDefault(x => x.TokenId.Equals(TokenId.MACHINE_DECLARE)) == null)
            {
                return;
            }

            tokensToParse.ToList()
                         .GetRange(
                             tokensToParse.IndexOf(
                                 tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.MACHINE_DECLARE))),
                             tokensToParse.IndexOf(
                                 tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.END_MACHINE_DECLARE)))
                             - tokensToParse.IndexOf(tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.MACHINE_DECLARE))) + 1).ForEach(machineInfoBlock.Add);

            foreach (var token in machineInfoBlock)
            {
                tokensToParse.Remove(token);
            }

            if (tokensToParse.FirstOrDefault(x => x.TokenId.Equals(TokenId.MACHINE_DECLARE)) != null)
            {
                throw new MultipleMachineDeclarationsException();
            }
        }

        private void parseExecutionalBlocks(List<Token> tokensToParse)
        {
            if (tokensToParse.Count == 0)
            {
                throw new NoExecutionalBlocksInFileException();
            }

            while (true)
            {
                var declarationBlock = tokensToParse.ToList()
                    .GetRange(
                        tokensToParse.IndexOf(
                            tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.BEGIN_PACKAGE))),
                        tokensToParse.IndexOf(
                            tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.END_PACKAGE)))
                        - tokensToParse.IndexOf(tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.BEGIN_PACKAGE))) + 1);

                foreach (var token in declarationBlock)
                {
                    tokensToParse.Remove(token);
                }

                ExecutionBlocks.Add(declarationBlock);

                if (tokensToParse.ToList().FirstOrDefault(x => x.TokenId.Equals(TokenId.BEGIN_PACKAGE))
                    == null)
                {
                    break;
                }
            }
        }

        private void parseDeclarationBlocks(List<Token> tokensToParse)
        {
            List<Token> declarationBlocksPart;

            try
            {
                declarationBlocksPart =
                    tokensToParse.ToList()
                        .GetRange(
                            tokensToParse.IndexOf(tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.DECLARE))),
                            tokensToParse.IndexOf(
                                tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.END_DECLARE)))
                            - tokensToParse.IndexOf(
                                tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.DECLARE))) + 1);

                tokensToParse.RemoveRange(
                    tokensToParse.IndexOf(tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.DECLARE))),
                    tokensToParse.IndexOf(tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.END_DECLARE)))
                    - tokensToParse.IndexOf(tokensToParse.ToList().First(x => x.TokenId.Equals(TokenId.DECLARE))) + 1);
            }
            catch (Exception)
            {
                throw new NoDeclarationSegmentInFileException();
            }


            getDeclarationBlocks(declarationBlocksPart);
        }

        private void getDeclarationBlocks(IList<Token> declarationBlocksPart)
        {
            declarationBlocksPart.RemoveAt(0);
            declarationBlocksPart.RemoveAt(declarationBlocksPart.Count - 1);
         
            if (declarationBlocksPart.Count == 0)
            {
                throw new NoDeclarationBlocksInFileException();
            }

            while (true)
            {
                var declarationBlock = declarationBlocksPart.ToList()
                    .GetRange(
                        declarationBlocksPart.IndexOf(
                            declarationBlocksPart.ToList().First(x => x.TokenId.Equals(TokenId.DECLARE_PACKAGE))),
                        declarationBlocksPart.IndexOf(
                            declarationBlocksPart.ToList().First(x => x.TokenId.Equals(TokenId.END_DECLARE_PACKAGE)))
                        - declarationBlocksPart.IndexOf(declarationBlocksPart.ToList().First(x => x.TokenId.Equals(TokenId.DECLARE_PACKAGE))) + 1);

                foreach (var token in declarationBlock)
                {
                    declarationBlocksPart.Remove(token);
                }

                declarationBlocks.Add(declarationBlock);

                if (declarationBlocksPart.ToList().FirstOrDefault(x => x.TokenId.Equals(TokenId.DECLARE_PACKAGE))
                    == null)
                {
                    break;
                }
            }
        }

        private void checkBlocks()
        {
            if (executionBlocks.Count == 0)
            {
                throw new NoExecutionalBlocksInFileException();
            }

            if (declarationBlocks.Count == 0)
            {
                throw new NoDeclarationBlocksInFileException();
            }

            if (declarationBlocks.Count != executionBlocks.Count)
            {
                throw new BlockCountMissmatchException();
            }

            foreach (var block in declarationBlocks)
            {
                var counterpart =
                    executionBlocks.FirstOrDefault(
                        x =>
                        x.First().TokenId.Equals(TokenId.BEGIN_PACKAGE)
                        && block.First().TokenId.Equals(TokenId.DECLARE_PACKAGE)
                        && x[0].Arguments[0] == block[0].Arguments[0]);

                if (counterpart == null)
                {
                    throw new NoExecutionalCounterpartToDeclarationException();
                }
            }
        }
    }

}
