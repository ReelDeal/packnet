﻿namespace FusionServiceApplication.LanguageEnvironment
{
    using System.Collections.Generic;

    using Utilities.Enums;

    public interface IInstructionItem
    {
        
        List<string> ToInstructionEntry();

        InstructionType InstructionType { get; }
        
        string Arguments { get; }
    }
}
