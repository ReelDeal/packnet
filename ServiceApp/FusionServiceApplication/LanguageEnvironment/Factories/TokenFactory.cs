﻿namespace FusionServiceApplication.LanguageEnvironment.Factories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Exceptions;

    public class TokenFactory
    {
        public bool isToken(string line)
        {
            TokenId id;
            
            var items = trim(line).Split(' ');
        
            return Enum.TryParse(items[0], out id);
        }

        private string trim(string line)
        {
            line = line.TrimStart(new[] { '\t', ' ', '\n', '\r' });
            line = line.TrimEnd(new[] { '\t', ' ', '\n', '\r' });

            return line;
        }

        public Token getInstance(string line)
        {
            TokenId id;

            var items = trim(line).Split(' ').ToList();

            if (!Enum.TryParse(items[0], out id))
            {
                throw new CouldNotParseTokenException(line);
            }
            
            return new Token(id, items.GetRange(1, items.Count - 1));
        }

        public bool isStartToken(string line)
        {
            return tokenIsEither(line, new List<TokenId> { TokenId.DECLARE, TokenId.BEGIN_PACKAGE });
        }

        public bool isEndToken(string line)
        {
            return tokenIsEither(line, new List<TokenId> { TokenId.END_DECLARE, TokenId.END_PACKAGE });
        }

        private bool tokenIsEither(string line, ICollection<TokenId> tokens)
        {
            TokenId id;

            var items = trim(line).Split(' ');

            if (string.IsNullOrEmpty(items[0]))
            {
                return false;
            }

            if (!Enum.TryParse(items[0], out id))
            {
                throw new CouldNotParseTokenException(line);
            }

            return tokens.Contains(id);
        }

        public bool isComment(string line)
        {
            line = trim(line);
            return line.StartsWith("//");
        }

        public bool isEmpty(string line)
        {
            line = trim(line);
            return string.IsNullOrEmpty(line);
        }
    }
}
