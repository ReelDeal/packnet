﻿
namespace FusionServiceApplication.LanguageEnvironment.Factories
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Exceptions;
    using Instructions;

    class MetadataInstructionFactory
    {
        public static IInstructionItem getInstance(IList<Token> declarationBlock)
        {
            var packageLength = 0.0;
            var track = 1;

            foreach (var token in declarationBlock)
            {
                switch (token.TokenId)
                {
                    case TokenId.PACKAGELENGTH:
                        try
                        {
                            packageLength = double.Parse(token.Arguments.First());
                        }
                        catch (Exception)
                        {
                            throw new UnableToCreateMetadataInstructionException("Unable to parse packagelength");
                        }

                        break;
                    case TokenId.TRACK:
                        try
                        {
                            track = int.Parse(token.Arguments.First());
                        }
                        catch (Exception)
                        {
                            throw new UnableToCreateMetadataInstructionException("Unable to parse track");
                        }

                        break;
                }
            }

            return new MetadataInstruction(packageLength, track);
        }
    }
}
