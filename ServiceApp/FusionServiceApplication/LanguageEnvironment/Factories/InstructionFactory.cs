﻿namespace FusionServiceApplication.LanguageEnvironment.Factories
{
    using Exceptions;
    using Instructions;

    class InstructionFactory
    {

        public static IInstructionItem getInstance(Token token)
        {
            switch (token.TokenId)
            {
                case TokenId.ACTIVATEKNIFE:
                    return new KnifeInstruction(token.Arguments, true);
                case TokenId.DEACTIVATEKNIFE:
                    return new KnifeInstruction(token.Arguments, false);
                case TokenId.FEED:
                    return new FeedRollerInstruction(token.Arguments);
                case TokenId.MOVECROSSHEAD:
                    return new CrossheadInstruction(token.Arguments);
                case TokenId.POSITIONLONGHEADS:
                    return new LongheadInstruction(token.Arguments);
                case TokenId.LIFTROLLER:
                    return new CrossheadInstruction(token.Arguments, true);
                default:
                    throw new CouldNotParseTokenException(token);
            }
        }
    }
}
