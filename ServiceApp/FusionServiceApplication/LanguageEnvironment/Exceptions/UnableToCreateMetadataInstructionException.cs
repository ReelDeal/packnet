﻿
namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class UnableToCreateMetadataInstructionException : Exception
    {
        public UnableToCreateMetadataInstructionException(string message)
            : base(message)
        {
            
        }

        public UnableToCreateMetadataInstructionException(Exception exception)
            : base(exception.Message)
        {
            
        }
    }
}
