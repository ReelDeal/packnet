﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class NoExecutionalBlocksInFileException : Exception
    {
    }
}
