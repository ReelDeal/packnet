﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class UnableToCreateCrossheadInstructionException : Exception
    {
        public UnableToCreateCrossheadInstructionException(Exception e)
            : base(e.Message)

        {

        }
    }
}
