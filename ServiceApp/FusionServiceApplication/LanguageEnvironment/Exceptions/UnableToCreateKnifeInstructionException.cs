﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class UnableToCreateKnifeInstructionException : Exception
    {
        public UnableToCreateKnifeInstructionException(Exception e) : base(e.Message)
        {
            
        }
    }
}
