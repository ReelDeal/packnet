﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class UnableToCreateFeedRollerInstructionException : Exception
    {
        public UnableToCreateFeedRollerInstructionException(Exception e) : base(e.Message)
        {
            
        }
    }
}
