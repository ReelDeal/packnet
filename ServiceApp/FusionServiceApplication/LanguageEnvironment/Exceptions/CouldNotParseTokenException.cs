﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class CouldNotParseTokenException : Exception
    {
        public CouldNotParseTokenException(string line)
            : base("Line contents: " + line)
        {

        }

        public CouldNotParseTokenException(Token token)
            : base("Token: " + token.TokenId + " Line number: " + token.LineNumber)
        {

        }
    }
}
