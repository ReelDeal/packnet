﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class NoExecutionalCounterpartToDeclarationException : Exception
    {
    }
}
