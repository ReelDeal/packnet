﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class ToManyArgumentsException : Exception
    {
        public ToManyArgumentsException(IInstructionItem item)
            : base("Item: " + item.InstructionType + " Arguments: " + item.Arguments)
        {
            
        }
    }
}
