﻿namespace FusionServiceApplication.LanguageEnvironment.Exceptions
{
    using System;

    public class NoDeclarationBlocksInFileException : Exception
    {
    }
}
