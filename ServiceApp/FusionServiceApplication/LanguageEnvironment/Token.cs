﻿namespace FusionServiceApplication.LanguageEnvironment
{
    using System.Collections.Generic;

    public class Token
    {
        private readonly TokenId tokenId;

        private readonly IList<string> argumentsList;


        public Token(TokenId tokenId, List<string> argumentsList)
        {
            this.tokenId = tokenId;
            this.argumentsList = argumentsList;
        }

        public List<string> toFusionInstruction()
        {
            return null;
        }

        public TokenId TokenId
        {
            get
            {
                return tokenId;
            }
        }

        public IList<string> Arguments
        {
            get
            {
                return argumentsList;
            }
        }

        public int LineNumber { get; set; }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var comparerToken = (Token)obj;

            if (comparerToken == null) 
            {
                return false;
            }

            if (tokenId != comparerToken.tokenId)
            {
                return false;
            }

            return argumentsList == comparerToken.argumentsList;
        }
    }
}
