﻿namespace FusionServiceApplication.LanguageEnvironment
{
    using System.Threading;

    using Utilities.Communication;

    public class FusionTestingLanguageHandler
    {
        private readonly IqFusionCommunicator machineCommunicator;

        private Tokenizer tokenizer;
        private Parser parser;

        public FusionTestingLanguageHandler(IqFusionCommunicator machineCommunicator)
        {
            this.machineCommunicator = machineCommunicator;

            tokenizer = new Tokenizer();
            parser = new Parser();
        }

        public void ExecuteFile(string file)
        {
            if (file.Equals(string.Empty) || machineCommunicator.Equals(null))
            {
                return;
            }

            tokenizer.loadFile(file);
            parser.parseTokens(tokenizer.getTokens());

            while (true)
            {
                if (!machineCommunicator.PlcReadyForTransfer)
                {
                    Thread.Sleep(1000);
                    continue;
                }
                
                MachineInfo.Instance.CurrentLongheadPositions = machineCommunicator.CurrentLongHeadPositions;
                machineCommunicator.TransferBufferInstructionList = parser.getNextInstructionList();
                machineCommunicator.TransferBufferId++;
            }
        }
    }
}
