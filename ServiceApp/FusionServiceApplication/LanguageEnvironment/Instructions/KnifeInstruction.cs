﻿

namespace FusionServiceApplication.LanguageEnvironment.Instructions
{
    using System;
    using System.Collections.Generic;

    using Exceptions;
    using Utilities.Enums;
    using Utilities.InstructionListStuff;

    class KnifeInstruction : IInstructionItem
    {
        private readonly bool shouldActivate;

        public KnifeInstruction(IList<string> argsList, bool shouldActivate)
        {
            if (argsList.Count > 0)
            {
                throw new ToManyArgumentsException(this);
            }

            try
            {
                this.shouldActivate = shouldActivate;
            }
            catch (Exception e)
            {
                throw new UnableToCreateKnifeInstructionException(e);
            }
        }

        public List<string> ToInstructionEntry()
        {
            return new List<string>
                       {
                           InstructionListMarkers.StartMarker,
                           InstructionListMarkers.KnifeMarker,
                           shouldActivate ? 
                           InstructionListMarkers.KnifeCutMarker : 
                           InstructionListMarkers.KnifeRaiseMarker
                       };
        }

        public InstructionType InstructionType 
        {
            get
            {
                return InstructionType.Knife;
            }
        }

        public string Arguments
        {
            get
            {
                return " Should activate: " + shouldActivate;
            }
        }
    }
}
