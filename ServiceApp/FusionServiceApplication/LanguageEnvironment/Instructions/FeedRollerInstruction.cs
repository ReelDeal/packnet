﻿

namespace FusionServiceApplication.LanguageEnvironment.Instructions
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Exceptions;
    using Utilities.Converters;
    using Utilities.Enums;
    using Utilities.InstructionListStuff;

    class FeedRollerInstruction : IInstructionItem
    {
        private readonly double feedLength;

        public FeedRollerInstruction(IList<string> argsList)
        {
            if (argsList.Count > 1)
            {
                throw new ToManyArgumentsException(this);
            }

            try
            {
                if (argsList[0] == "REST")
                {
                    //rest feed
                    return;
                }
                feedLength = double.Parse(argsList[0]);
            }
            catch (Exception e)
            {
                throw new UnableToCreateFeedRollerInstructionException(e);
            }
        }

        public List<string> ToInstructionEntry()
        {
            var position = MachinePositionConverters.ConvertMachinePos(feedLength);

            return new List<string>
                       {
                           InstructionListMarkers.StartMarker,
                           InstructionListMarkers.FeedMarker,
                           position.Item1.ToString(CultureInfo.InvariantCulture),
                           position.Item2.ToString(CultureInfo.InvariantCulture)
                       };
        }

        public InstructionType InstructionType {
            get
            {
                return InstructionType.FeedInstruction;
            }
        }

        public string Arguments
        {
            get
            {
                return " Length: " + feedLength;
            }
        }
    }
}
