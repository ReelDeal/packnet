﻿

namespace FusionServiceApplication.LanguageEnvironment.Instructions
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Exceptions;
    using Utilities.Converters;
    using Utilities.Enums;
    using Utilities.InstructionListStuff;

    class MetadataInstruction : IInstructionItem
    {
        private readonly int track;

        private readonly double fullPackageLength;

        public MetadataInstruction(IList<string> argsList)
        {
            if (argsList.Count > 2)
            {
                throw new ToManyArgumentsException(this);
            }

            try
            {
                track = int.Parse(argsList[0]);
                fullPackageLength = double.Parse(argsList[1]);
            }
            catch (Exception e)
            {
                throw new UnableToCreateMetadataInstructionException(e);
            }
        }

        public MetadataInstruction(double packageLength, int track)
        {
            fullPackageLength = packageLength;
            this.track = track;
        }

        public List<string> ToInstructionEntry()
        {
            var convertedLength =
                MachinePositionConverters.ConvertMachinePos(fullPackageLength);

            return new List<string>
                       {
                           InstructionListMarkers.StartMarker,
                           InstructionListMarkers.MetadataMarker,
                           convertedLength.Item1.ToString(CultureInfo.InvariantCulture),
                           convertedLength.Item2.ToString(CultureInfo.InvariantCulture),
                           track.ToString(CultureInfo.InvariantCulture),
                           InstructionListMarkers.FalseMarker
                       };
        }

        public InstructionType InstructionType {
            get
            {
                return InstructionType.MetadataInstruction;
            } 
        }

        public string Arguments
        {
            get
            {
                return "Track: " + track + " Package length: " + fullPackageLength;
            }
        }
    }
}
