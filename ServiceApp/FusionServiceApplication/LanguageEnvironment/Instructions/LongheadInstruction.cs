﻿

namespace FusionServiceApplication.LanguageEnvironment.Instructions
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;

    using Exceptions;
    using Utilities.Converters;
    using Utilities.Enums;
    using Utilities.InstructionListStuff;

    class LongheadInstruction : IInstructionItem
    {
        private readonly List<double> longheadPositions = new List<double>();
        
        public LongheadInstruction(ICollection<string> argsList)
        {
            if (argsList.Count > MachineInfo.Instance.LongheadCount)
            {
                throw new ToManyArgumentsException(this);
            }

            try
            {
                foreach (var argument in argsList)
                {
                    longheadPositions.Add(double.Parse(argument, CultureInfo.InvariantCulture));
                }
            }
            catch (Exception e)
            {
                throw new UnableToCreateMetadataInstructionException(e);
            }
        }


        public List<string> ToInstructionEntry()
        {
            var instructionEntry = new List<string>
                                       {
                                           InstructionListMarkers.StartMarker,
                                           InstructionListMarkers.LongheadPositioningMarker
                                       };

            var workingMovementSolution =
                LongheadPositioningStuff.FindLongheadMovementSolution(
                    MachineInfo.Instance.CurrentLongheadPositions.GetRange(0, longheadPositions.Count),
                    longheadPositions,
                    Enumerable.Range(1, longheadPositions.Count).ToList(),
                    new List<Tuple<int, double>>(),
                    true);

            if (workingMovementSolution == null)
            {
                throw new CouldNotFindLongheadMovementSolutionException();
            }

            for (var i = 0; i < workingMovementSolution.Count(); i++)
            {
                instructionEntry.Add(workingMovementSolution[i].Item1.ToString(CultureInfo.InvariantCulture));
                var convertedPosition = MachinePositionConverters.ConvertMachinePos(workingMovementSolution[i].Item2);
                instructionEntry.Add(convertedPosition.Item1.ToString(CultureInfo.InvariantCulture));
                instructionEntry.Add(convertedPosition.Item2.ToString(CultureInfo.InvariantCulture));
            }

            return instructionEntry;
        }

        public InstructionType InstructionType
        {
            get
            {
                return InstructionType.LHpositioning;
            }
        }

        public string Arguments
        {
            get
            {
                var sb = new StringBuilder();

                sb.Append("Positions: ");

                for (var i = 0; i < longheadPositions.Count; i++)
                {
                    sb.Append(" LH" + i + ": ");
                    sb.Append(longheadPositions[i]);
                }

                return sb.ToString();
            }
        }
    }
}
