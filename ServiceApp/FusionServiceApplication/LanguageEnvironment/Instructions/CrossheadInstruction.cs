﻿

namespace FusionServiceApplication.LanguageEnvironment.Instructions
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;

    using Exceptions;
    using Utilities.Converters;
    using Utilities.Enums;
    using Utilities.InstructionListStuff;

    class CrossheadInstruction : IInstructionItem
    {
        private readonly double position;

        private readonly bool shouldLiftPr;
        
        public CrossheadInstruction(IList<string> argsList, bool souldLift = false)
        {
            if (argsList.Count > (souldLift ? 0 : 1))
            {
                throw new ToManyArgumentsException(this);
            }

            try
            {
                if (souldLift)
                {
                    shouldLiftPr = true;

                    //SÄTT POSITION BASERAT PÅ VILKET TRACK SOM SKA KÖRAS
                    return;
                }

                position = double.Parse(argsList[0], CultureInfo.InvariantCulture);
            }
            catch (Exception e)
            {
                throw new UnableToCreateCrossheadInstructionException(e);
            }
        }

        public List<string> ToInstructionEntry()
        {
            var machinePosition =
                MachinePositionConverters.ConvertMachinePos(position);

            return new List<string>
                       {
                           InstructionListMarkers.StartMarker,
                           InstructionListMarkers.CrossheadMarker,
                           InstructionListMarkers.FalseMarker,
                           shouldLiftPr ? 
                           InstructionListMarkers.TrueMarker : 
                           InstructionListMarkers.FalseMarker,
                           machinePosition.Item1.ToString(CultureInfo.InvariantCulture),
                           machinePosition.Item2.ToString(CultureInfo.InvariantCulture)
                       };
        }

        public InstructionType InstructionType 
        {
            get
            {
                return InstructionType.CHpositioning;
            }
        }

        public string Arguments
        {
            get
            {
                return "Position : " + position + " Should lift roller: " + shouldLiftPr;
            }
        }
    }
}
