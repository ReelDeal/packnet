﻿namespace FusionServiceApplication.LanguageEnvironment
{
    using System.Collections.Generic;

    public class MachineInfo
    {
        private static MachineInfo instance;

        public MachineInfo(int LongheadCount = 7, int Tracks = 2)
        {
            this.LongheadCount = LongheadCount;
            this.Tracks = Tracks;
        }

        public static MachineInfo Instance
        {
            get
            {
                return instance ?? (instance = new MachineInfo());
            }
        }

        public int LongheadCount { get; set; }

        public int Tracks { get; set; }

        public List<double> CurrentLongheadPositions { get; set; }
        
        public bool Metric { get; set; }
    }
}
