﻿namespace FusionServiceApplication.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class LoggerWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo info)
        {
            return (double)value - 330;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo info)
        {
            return (double)value + 330;
        }
    }
}
