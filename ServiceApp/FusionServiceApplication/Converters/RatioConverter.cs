﻿namespace FusionServiceApplication.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class RatioConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo info)
        {
            return (double)value * (double)parameter;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo info)
        {
            return (double)value / (double)parameter;
        }
    }
}
