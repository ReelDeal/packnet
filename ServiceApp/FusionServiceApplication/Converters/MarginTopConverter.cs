﻿namespace FusionServiceApplication.Converters
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;

    public class MarginTopConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo info)
        {
            return new Thickness(0, (double)value + (double)parameter, 0, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo info)
        {
            throw new NotSupportedException();
        }
    }
}
