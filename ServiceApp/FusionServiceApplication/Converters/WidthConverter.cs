﻿namespace FusionServiceApplication.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class WidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo info)
        {
            return (double)value / 2;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo info)
        {
            return (double)value * 2;
        }
    }
}
