﻿namespace FusionServiceApplication.Converters
{
    using System;
    using System.Globalization;
    using System.Windows.Data;

    public class InverseBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo info)
        {
            if (targetType != typeof(bool))
            {
                throw new InvalidOperationException("The target must be a boolean");
            }

            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo info)
        {
            throw new NotSupportedException();
        }
    }
}
