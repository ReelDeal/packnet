﻿
namespace FusionServiceApplication.Helpers
{
    using SettingHandlers.MachineConfig;
    using Microsoft.Win32;
    using Viewmodels;
    using System.IO;

    public static class MachineXmlHandler
    {
        public static void WriteToMachineXml(double[] sensorCompensationLeft1, double[] sensorCompensationLeft2,
            double[] sensorCompenstaionRight1, double[] sensorCompensationRight2, WriteToLogDelegate writeToLog)
        {
            bool? diagResult = false;
            var diag = new OpenFileDialog {Title = "Select machine config file"};


            if (writeToLog != null)
                diagResult = diag.ShowDialog();
            else
            {
                diag.FileName = Path.Combine(Directory.GetCurrentDirectory(), "machine.xml");
            }


            if (diagResult == true || writeToLog == null)
            {
                if (writeToLog != null)
                {
                    writeToLog("Writing to machine xml");
                }

                var machineConfig = Machine.Deserialize(diag.FileName);

                WriteSensorCompensationsToXml(sensorCompensationLeft1, sensorCompensationLeft2, sensorCompenstaionRight1, sensorCompensationRight2, machineConfig);

                //TODO serializerar inte som den ska
                machineConfig.Serialize(diag.FileName);
            }
            else
            {
                if (writeToLog != null)
                {
                    writeToLog("Couldn't find machine xml aborting...");
                }
            }
        }

        private static void WriteSensorCompensationsToXml(double[] sensorCompensationLeft1, double[] sensorCompensationLeft2, double[] sensorCompensationRight1, double[] sensorCompensationRight2, Machine machineConfig)
        {
            machineConfig.Tracks.Track[0].LeftswordsensorCompensations.Compensations = sensorCompensationLeft1;
            machineConfig.Tracks.Track[1].LeftswordsensorCompensations.Compensations = sensorCompensationLeft2;
            machineConfig.Tracks.Track[0].RightswordsensorCompensations.Compensations = sensorCompensationRight1;
            machineConfig.Tracks.Track[1].RightswordsensorCompensations.Compensations = sensorCompensationRight2;
        }
    }
}
