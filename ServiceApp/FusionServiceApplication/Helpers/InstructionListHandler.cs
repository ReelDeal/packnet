﻿namespace FusionServiceApplication.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;

    using Utilities.Constants;
    using Utilities.Converters;
    using Utilities.InstructionListStuff;

    public class InstructionListHandler
    {
        private readonly Random randomGenerator;

        private readonly bool isMetricMachine;

        private double totalCrossheadDistanceMoved;

        private double lastCrossheadPosition;

        private readonly double crossheadMaxPosition;

        private readonly double crossheadMinPosition;

        private readonly double lhMaxPosition;

        private readonly double lhMinPosition;

        private int trackForNextBox;

        public InstructionListHandler(bool isMetricMachine, double crossheadMax, double crossheadMin, double lhMaxPosition, double lhMinPosition)
        {
            this.isMetricMachine = isMetricMachine;
            randomGenerator = new Random(DateTime.Now.Millisecond);

            crossheadMaxPosition = crossheadMax;
            crossheadMinPosition = crossheadMin;

            this.lhMaxPosition = lhMaxPosition;
            this.lhMinPosition = lhMinPosition;
        }

        public double TotalCrossheadDistanceMoved
        {
            get
            {
                return totalCrossheadDistanceMoved;
            }
        }

        public int TrackForNextBox
        {
            get
            {
                return trackForNextBox;
            }
        }

        public string[] GetNewInstructionList(int numberOfLongheads, List<double> currentLhPositions)
        {
            double packageLengthDm;
            double packageLengthHundredsMm;

            var newInstructionList = new List<string>();
            totalCrossheadDistanceMoved = 0;

            newInstructionList.AddRange(GetNewMetaDataInstruction(out packageLengthDm, out packageLengthHundredsMm));
            if (numberOfLongheads > 0)
            {
                newInstructionList.AddRange(GetNewLongHeadInstruction(lhMaxPosition, lhMinPosition, numberOfLongheads, currentLhPositions));
            }

            newInstructionList.AddRange(GetNewKnifeInstruction(true));
            newInstructionList.AddRange(GetNewFeedInstruction(packageLengthDm, packageLengthHundredsMm, out packageLengthDm, out packageLengthHundredsMm, false));
            newInstructionList.AddRange(GetNewCrossHeadInstruction(crossheadMaxPosition, crossheadMinPosition, false)); 
            newInstructionList.AddRange(GetNewKnifeInstruction(false));
            newInstructionList.AddRange(GetNewCrossHeadInstruction(crossheadMaxPosition, crossheadMinPosition, false));
            newInstructionList.AddRange(GetNewKnifeInstruction(false));
            newInstructionList.AddRange(GetNewCrossHeadInstruction(crossheadMaxPosition, crossheadMinPosition, false));
            newInstructionList.AddRange(GetNewKnifeInstruction(true)); 
            newInstructionList.AddRange(GetNewFeedInstruction(packageLengthDm, packageLengthHundredsMm, out packageLengthDm, out packageLengthHundredsMm, true));
            newInstructionList.AddRange(GetNewCrossHeadInstruction(crossheadMaxPosition, crossheadMinPosition, false));
            newInstructionList.AddRange(GetNewKnifeInstruction(false));
            newInstructionList.AddRange(GetNewCrossHeadInstruction(crossheadMaxPosition, crossheadMinPosition, false));
            newInstructionList.AddRange(GetNewKnifeInstruction(false));
            newInstructionList.AddRange(GetNewCrossHeadInstruction(crossheadMaxPosition, crossheadMinPosition, false));
            newInstructionList.AddRange(GetNewKnifeInstruction(true));
            newInstructionList.AddRange(GetNewFeedInstruction(packageLengthDm, packageLengthHundredsMm, out packageLengthDm, out packageLengthHundredsMm, true));
            newInstructionList.AddRange(GetNewCrossHeadInstruction(crossheadMaxPosition, crossheadMinPosition, true));
            newInstructionList.AddRange(GetStopMarker());

            
            WriteListToLog(newInstructionList);
            return newInstructionList.ToArray();
        }

        public string[] GetNewMetaDataInstruction(out double packageLengthDm, out double packageLengthHundredsMm)
        {
            var packageLength = GetDoubleInInterval(PhysicalMachineProperties.GetMinimumPackageLength(isMetricMachine), PhysicalMachineProperties.GetMaximumPackageLength(isMetricMachine));

            var track = (randomGenerator.Next() % 2 == 0) ? 1 : 2;
            
            var convertedLength = MachinePositionConverters.ConvertMachinePos(packageLength);
            
            packageLengthDm = convertedLength.Item1;
            packageLengthHundredsMm = convertedLength.Item2;
            
            trackForNextBox = track;

            return new[] { InstructionListMarkers.StartMarker, InstructionListMarkers.MetadataMarker, packageLengthDm.ToString(CultureInfo.InvariantCulture), packageLengthHundredsMm.ToString(CultureInfo.InvariantCulture), track.ToString(CultureInfo.InvariantCulture), "0" }; // last one is for printing labels, which it shouldnt
        }

        public string[] GetNewKnifeInstruction(bool forcedRelease)
        {
            return forcedRelease
                       ? new[]
                             {
                                 InstructionListMarkers.StartMarker, InstructionListMarkers.KnifeMarker,
                                 InstructionListMarkers.KnifeRaiseMarker
                             }

                       : new[]
                             {
                                 InstructionListMarkers.StartMarker, InstructionListMarkers.KnifeCutMarker,
                                 (randomGenerator.Next() % 2 == 0
                                      ? InstructionListMarkers.KnifeCutMarker
                                      : InstructionListMarkers.KnifeRaiseMarker)
                             };
        }

        public string[] GetNewLongHeadInstruction(double machineMax, double machineMin, int numberOfLongheads, List<double> currentPositions)
        {
            var longheadPositions = GetNewLongheadPositions(machineMax, machineMin, numberOfLongheads);
            var workingMovementSolution = 
                LongheadPositioningStuff.FindLongheadMovementSolution(
                    currentPositions.GetRange(0, numberOfLongheads),
                    longheadPositions.ToList(),
                    Enumerable.Range(1, numberOfLongheads).ToList(),
                    new List<Tuple<int, double>>(),
                    isMetricMachine);
            return CreateLongheadInstructionBasedOnWorkingMovementSolution(numberOfLongheads, workingMovementSolution);
        }

        private string[] CreateLongheadInstructionBasedOnWorkingMovementSolution(
            int numberOfLongheads,
            IList<Tuple<int, double>> workingMovementSolution)
        {
            var finalLongheadPositioning = new List<string>();
            for (var i = 0; i < numberOfLongheads; i++)
            {
                finalLongheadPositioning.Add(workingMovementSolution[i].Item1.ToString(CultureInfo.InvariantCulture));

                var convertedPos = MachinePositionConverters.ConvertMachinePos(workingMovementSolution[i].Item2);

                finalLongheadPositioning.Add(convertedPos.Item1.ToString(CultureInfo.InvariantCulture));
                finalLongheadPositioning.Add(convertedPos.Item2.ToString(CultureInfo.InvariantCulture));
            }

            finalLongheadPositioning.InsertRange(
                0,
                new List<string>
                    {
                        InstructionListMarkers.StartMarker,
                        InstructionListMarkers.LongheadPositioningMarker
                    });

            return finalLongheadPositioning.ToArray();
        }

        private IEnumerable<double> GetNewLongheadPositions(double machineMax, double machineMin, int numberOfLongheads)
        {
            var longheadPositions = new double[numberOfLongheads];
            var longheadInterval = (machineMax - machineMin) / numberOfLongheads;

            for (var i = 0; i < numberOfLongheads; i++)
            {
                var minOfInterval = machineMax - ((i + 1) * longheadInterval)
                                    + (PhysicalMachineProperties.GetLongheadSize(isMetricMachine) / 2);
                var maxOfInterval = machineMax - (i * longheadInterval)
                                    - (PhysicalMachineProperties.GetLongheadSize(isMetricMachine) / 2);
                var nextLongheadPosition = GetDoubleInInterval(minOfInterval, maxOfInterval);
                longheadPositions[i] = nextLongheadPosition;
            }

            return longheadPositions.Reverse().ToArray();
        }

        public string[] GetNewFeedInstruction(double packageLengthDmRange, double packageLengtHundredsMmRange, out double packagelengthDmLeft, out double packageLengthHundredsMmLeft, bool finalFeed)
        {
            if (finalFeed)
            {
                packagelengthDmLeft = 0;
                packageLengthHundredsMmLeft = 0;
                return new[]
                           {
                               InstructionListMarkers.StartMarker, InstructionListMarkers.FeedMarker, packageLengthDmRange.ToString(CultureInfo.InvariantCulture),
                               packageLengtHundredsMmRange.ToString(CultureInfo.InvariantCulture)
                           };
            }

            var feedDm = GetDoubleInInterval(0, packageLengthDmRange);
            var feedMm = GetDoubleInInterval(0, packageLengtHundredsMmRange);

            packagelengthDmLeft = packageLengthDmRange - feedDm;
            packageLengthHundredsMmLeft = packageLengtHundredsMmRange - feedMm;

            return new[]
                       {
                           InstructionListMarkers.StartMarker, InstructionListMarkers.FeedMarker, feedDm.ToString(CultureInfo.InvariantCulture),
                           feedMm.ToString(CultureInfo.InvariantCulture)
                       };
        }

        public string[] GetNewCrossHeadInstruction(double machineMax, double machineMin, bool disconnectRoller)
        {
            var crossheadPosition = Math.Max(randomGenerator.NextDouble() * machineMax, machineMin);

            if (disconnectRoller)
            {
                crossheadPosition = machineMax - (machineMax > 1000 ? 25 : 1);
            }

            var convertedPos = MachinePositionConverters.ConvertMachinePos(crossheadPosition);

            totalCrossheadDistanceMoved += Math.Abs(crossheadPosition - lastCrossheadPosition);
            lastCrossheadPosition = crossheadPosition;

            return new[]
                       {
                           InstructionListMarkers.StartMarker, 
                           InstructionListMarkers.TrueMarker,
                           InstructionListMarkers.FalseMarker,
                           disconnectRoller
                               ? InstructionListMarkers.TrueMarker
                               : InstructionListMarkers.FalseMarker,
                           convertedPos.Item1.ToString(CultureInfo.InvariantCulture),
                           convertedPos.Item2.ToString(CultureInfo.InvariantCulture)
                       };
        }

        private double GetDoubleInInterval(double minOfInterval, double maxOfInterval)
        {
            var constant = randomGenerator.NextDouble();
            return minOfInterval + (constant * Math.Abs(maxOfInterval - minOfInterval));
        }

        private static void WriteListToLog(IEnumerable<string> instructionList)
        {
            var outputFile = File.Open(Path.Combine(Directory.GetCurrentDirectory(), "InstructionLists"), FileMode.Append);
            var encoding = new ASCIIEncoding();

            foreach (var bytes in instructionList.Select(instructionEntry => encoding.GetBytes(instructionEntry.ToString(CultureInfo.InvariantCulture) + (instructionEntry.Equals(InstructionListMarkers.StopMarker) ? Environment.NewLine : " "))))
            {
                outputFile.Write(bytes, 0, bytes.Length);
            }

            outputFile.Close();
        }

        private IEnumerable<string> GetStopMarker()
        {
            return new[] { InstructionListMarkers.StopMarker };
        }
    }
}