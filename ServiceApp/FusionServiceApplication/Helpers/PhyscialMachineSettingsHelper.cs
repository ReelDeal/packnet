﻿using PackNet.Communication.Fusion;

namespace FusionServiceApplication.Helpers
{
    using System;
    using System.Net;

    using Utilities.Communication;
    using Utilities.Logging;

    using SettingHandlers;
    using SettingHandlers.MachineConfig;

    public static class PhyscialMachineSettingsHelper
    {
        public static bool SyncMachine(IIqFusionCommunicator com)
        {
            try
            {
                SyncCrossheadParameters(com);
                SyncRollerParameters(com);
                SyncRollerDiscPositions(com);
                SyncTrackCompensations(com);
                SyncEventNotifier(com);
                return true;
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile("Unable to synchronize machine | " + e.Message);
                return false;
            }
        }

        private static void SyncTrackCompensations(IIqFusionCommunicator com)
        {
            com.TrackSideSteeringTolerance = Machine.Instance.Tracks.TrackSideSteeringTolerance;
            com.TrackOffsetTolerance = Machine.Instance.Tracks.TrackOffsetTolerance;
            com.TrackActivationDelay = Machine.Instance.Tracks.TrackActivationDelay;
            com.TrackLatchDistance = Machine.Instance.Tracks.TrackLatchDistance;
            
            
            com.SwordSensorCompensationLeft1 = Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensations;
            com.SwordSensorCompensationLeft2 = Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensations;

            com.SwordSensorCompensationRight1 = Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensations;
            com.SwordSensorCompensationRight2 = Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensations;
        }

        public static IPEndPoint GetEventNotifierAdress()
        {
            return Networking.FindIpEndPoint(CoreSettings.Instance.EventNotifierAdress, CoreSettings.Instance.EventNotifierPort);
        }

        private static void SyncRollerDiscPositions(IIqFusionCommunicator com)
        {
            com.RightRollerDisconnectPosition = (float)Machine.Instance.PressureRoller.RightRoller.DisconnectPosition;
            com.LeftRollerDisconnectPosition = (float)Machine.Instance.PressureRoller.LeftRoller.DisconnectPosition;
        }

        private static void SyncRollerParameters(IIqFusionCommunicator com)
        {
            var map = new IqFusionMachineVariables();
            com.WriteValue(map.FeedRollerAccelerationInPercent, Machine.Instance.FeedRoller.RollerAccelerationInPercent);
            com.WriteValue(map.FeedRollerCleanCutLength, Machine.Instance.FeedRoller.CleanCutLength);
            com.WriteValue(map.FeedRollerGearRatio, Machine.Instance.FeedRoller.ToolsGearRatio);
            com.WriteValue(map.FeedRollerLoweringOffset, Machine.Instance.FeedRoller.LoweringOffset);
            com.WriteValue(map.FeedRollerOutFeedLength, Machine.Instance.FeedRoller.OutfeedLength);
            com.WriteValue(map.FeedRollerSpeedInPercent, Machine.Instance.FeedRoller.SpeedInPercent);
            com.WriteValue(map.RollAxisScaling, Machine.Instance.FeedRoller.Scaling);
        }

        private static void SyncEventNotifier(IIqFusionCommunicator com)
        {
            com.EventNotifierAdress = CoreSettings.Instance.EventNotifierAdress;
            com.EventNotifierPort = CoreSettings.Instance.EventNotifierPort;
        }

        private static void SyncCrossheadParameters(IIqFusionCommunicator com)
        {
            var map = new IqFusionMachineVariables();

            com.WriteValue(map.CrossHeadAccelerationInPercent, Machine.Instance.Crosshead.CrossheadAccelerationInPercent);
            com.WriteValue(map.CrossHeadHomingPosition, Machine.Instance.Crosshead.HomingPosition);
            com.WriteValue(map.CrossHeadMaximumAcceleration, 100 * Machine.Instance.Crosshead.CrossheadMaximumAccelerationInUnits);
            com.WriteValue(map.CrossHeadMaximumDeceleration, 100*Machine.Instance.Crosshead.CrossheadMaximumDecelerationInUnits);
            com.WriteValue(map.CrossHeadMaximumPosition, Machine.Instance.Crosshead.CrossheadMaximumPosition);
            com.WriteValue(map.CrossHeadMinimumPosition, Machine.Instance.Crosshead.CrossheadMinimumPosition);
            com.WriteValue(map.CrossHeadMaximumSpeed, 100 * Machine.Instance.Crosshead.CrossheadMaximumSpeedInUnits);
            com.WriteValue(map.CrossHeadTrackSensorToTool, Machine.Instance.Crosshead.TrackSensorToTool);
            com.WriteValue(map.CrossHeadSpeedInPercent, Machine.Instance.Crosshead.CrossheadSpeedInPercent);
            com.WriteValue(map.ToolAxisScaling, Machine.Instance.Crosshead.Scaling);
            com.WriteValue(map.CrossHeadSensorToKnife, Machine.Instance.Crosshead.LongheadSensorToTool);
        }
    }
}
