﻿namespace FusionServiceApplication.Helpers
{
    using System;
    using System.Threading;
    using Managers;
    using SettingHandlers.MachineConfig;
    using Utilities.Communication;
    using Utilities.Logging;

    public class Run4EverHandler
    {
        private readonly InstructionListHandler instructionListHandler;

        private readonly UpdatePackageStatistics updateStatistics;

        private readonly IqFusionCommunicator machineCommunicator;
        
        public Run4EverHandler(UpdatePackageStatistics updateStatisticsDelegate, IqFusionCommunicator machineCommunicator)
        {
            updateStatistics = updateStatisticsDelegate;
            this.machineCommunicator = machineCommunicator;
            instructionListHandler = new InstructionListHandler(
                machineCommunicator.IsMetricMachine,
                machineCommunicator.CrossHeadMax,
                machineCommunicator.CrossHeadMin,
                Machine.Instance.LongHeads.MaximumPosition,
                Machine.Instance.LongHeads.MinimumPosition);
        }

        public void Run4Ever()
        {
            while (!machineCommunicator.LongHeadPositionsReady)
            {
                Thread.Sleep(1000);
            }

            var numberOfLongheads = machineCommunicator.NumberOfLongheads;

            while (true)
            {
                try
                {
                    var nextInstructionList = instructionListHandler.GetNewInstructionList(numberOfLongheads, machineCommunicator.CurrentLongHeadPositions);

                    while (!machineCommunicator.PlcReadyForTransfer)
                    {
                        Thread.Sleep(5000);
                    }

                    machineCommunicator.TransferBufferId++;
                    machineCommunicator.TransferBufferInstructionList = nextInstructionList;
                    machineCommunicator.PlcReadyForTransfer = false;

                    updateStatistics(
                        instructionListHandler.TrackForNextBox,
                        instructionListHandler.TotalCrossheadDistanceMoved,
                        numberOfLongheads);
                }
                catch (Exception e)
                {
                    Logging.WriteToLogFile(e.ToString());
                    return;
                }
            }
        }
    }
}
