﻿namespace FusionServiceApplication.Helpers
{
    using System;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Text.RegularExpressions;

    public static class Networking
    {
        public static string GetIp(string s)
        {
            var validIpAddressRegex =
                new Regex(
                    "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");

            if (validIpAddressRegex.IsMatch(s))
            {
                return s;
            }

            try
            {
                var address = Dns.GetHostEntry(s);
                return address.AddressList[0].ToString();
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static bool AbleToReachHost(string address)
        {
            if (string.IsNullOrEmpty(address))
            {
                return false;
            }

            var pingReply = (new Ping()).Send(address, 500);

            if (pingReply != null && !pingReply.Status.Equals(IPStatus.Success))
            {
                return false;
            }

            return true;
        }

        public static IPEndPoint FindIpEndPoint(string address, string port)
        {
            return PackNet.Common.Utils.Networking.FindIpEndPoint(address, int.Parse(port), false);
        }
    }
}