﻿namespace FusionServiceApplication.Viewmodels
{
    using Onyx.Windows;

    internal class Run4EverStatisticsViewModel : ViewModel
    {
        private int nrOfBoxesProduced;
        
        private double crossheadDistanceMoved;
        
        private int longheadMovements;
        
        private int leftPressureRollerLifts;
        
        private int rightPressureRollerLifts;

        private int nrOfFeedRollerDownAfterLhRepositioning;

        private int nrOfLongheadPositioningRetries;

        public Run4EverStatisticsViewModel(View view) : base(view)
        {
        }

        public int NrOfBoxesProduced
        {
            get
            {
                return nrOfBoxesProduced;
            }
            
            set
            {
                nrOfBoxesProduced = value;
                OnPropertyChanged("NrOfBoxesProduced");
            }
        }

        public double CrossheadDistanceMoved
        {
            get
            {
                return crossheadDistanceMoved;
            }

            set
            {
                crossheadDistanceMoved = value;
                OnPropertyChanged("CrossheadDistanceMoved");
            }
        }

        public int LongheadMovements
        {
            get
            {
                return longheadMovements;
            }
            
            set
            {
                longheadMovements = value;
                OnPropertyChanged("LongheadMovements");
            }
        }

        public int LeftPressureRollerLifts
        {
            get
            {
                return leftPressureRollerLifts;
            }

            set
            {
                leftPressureRollerLifts = value;
                OnPropertyChanged("LeftPressureRollerLifts");
            }
        }

        public int RightPressureRollerLifts
        {
            get
            {
                return rightPressureRollerLifts;
            }
            
            set
            {
                rightPressureRollerLifts = value;
                OnPropertyChanged("RightPressureRollerLifts");
            }
        }

        public int NrOfFeedRollerDownAfterLhRepositioning
        {
            get
            {
                return nrOfFeedRollerDownAfterLhRepositioning;
            }

            set
            {
                nrOfFeedRollerDownAfterLhRepositioning = value;
                OnPropertyChanged("NrOfFeedRollerDownAfterLhRepositioning");
            }
        }

        public int NrOfLongheadPositioningRetries
        {
            get
            {
                return nrOfLongheadPositioningRetries;
            }

            set
            {
                nrOfLongheadPositioningRetries = value;
                OnPropertyChanged("NrOfLongheadPositioningRetries");
            }
        }
    }
}
