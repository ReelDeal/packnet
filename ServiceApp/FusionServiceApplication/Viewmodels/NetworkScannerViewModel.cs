﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using FusionServiceApplication.Helpers;
using FusionServiceApplication.Objects;
using Onyx.Windows;

namespace FusionServiceApplication.Viewmodels
{
    public class NetworkScannerViewModel : ViewModel, IDisposable
    {
        private RelayCommand scanNetworkCommand;
        
        private bool currentlyScanning;
        
        private Visibility loadImageVisibility = Visibility.Hidden;

        private Thread waitForScannerToCompleteThread;

        private ControlCloseDelegate windowOnCloseDelegate;

        private readonly PviManagerHandler pviManagerHandler;

        public NetworkScannerViewModel(View view, ControlCloseDelegate windowClose)
            : base(view)
        {
            windowOnCloseDelegate = windowClose;
            pviManagerHandler = new PviManagerHandler();
        }

        public void Dispose()
        {
            pviManagerHandler.Dispose();
            
            if (waitForScannerToCompleteThread != null)
                waitForScannerToCompleteThread.Abort(0);
        }

        public bool CurrentlyScanning
        {
            get
            {
                return currentlyScanning;
            }

            set
            {
                currentlyScanning = value;
                LoadImageVisibility = value ? Visibility.Visible : Visibility.Hidden;
                OnPropertyChanged("CurrentlyScanning");
            }
        }

        public Visibility LoadImageVisibility
        {
            get
            {
                return loadImageVisibility;
            }

            set
            {
                loadImageVisibility = value;
                OnPropertyChanged("LoadImageVisibility");
            }
        }

        public ControlCloseDelegate WindowOnCloseDelegate
        {
            get
            {
                if (windowOnCloseDelegate == null)
                {
                    throw new Exception("WindowOnCloseDelegate == NULL");
                }

                return windowOnCloseDelegate;
            }

            set
            {
                windowOnCloseDelegate = value;
            }
        }

        public RelayCommand ScanNetworkCommand
        {
            get
            {
                return scanNetworkCommand ??
                       (scanNetworkCommand = new RelayCommand(delegate
                                                                   {
                                                                       pviManagerHandler.SearchForDevices();
                                                                       CurrentlyScanning = true;
                                                                       waitForScannerToCompleteThread =
                                                                           new Thread(WaitUntilScanIsFinished);
                                                                       waitForScannerToCompleteThread.Start();
                                                                       
                                                                   },
                           delegate { return true; }));
            }
        }

        private void WaitUntilScanIsFinished()
        {
            while (pviManagerHandler.SearchPreformed == false)
                Thread.Sleep(100);

            CurrentlyScanning = false;
            OnPropertyChanged("ActiveAdresses");
            waitForScannerToCompleteThread.Abort(0);
        }

        public List<PVIMachineInfo> ActiveAdresses
        {
            get
            {
                if (pviManagerHandler == null)
                    return null;

                return pviManagerHandler.MachineInfo.ToList();
            }
        }
    }
}


