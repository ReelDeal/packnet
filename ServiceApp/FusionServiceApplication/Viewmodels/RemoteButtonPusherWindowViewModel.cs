﻿using PackNet.Communication.Fusion;
using PackNet.Communication.PLCBase;
using PackNet.Communication.WebRequestCreator;
using View = Onyx.Windows.View;

namespace FusionServiceApplication.Viewmodels
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows.Forms;

    using Helpers;
    using Objects;
    using Properties;
    using SettingHandlers;
    using SettingHandlers.MachinesConfiguration;
    using Utilities.Logging;

    using Onyx.Windows;
    using View = View;

    public class RemoteButtonPusherWindowViewModel : ViewModel
    {
        private readonly List<IWebRequestCreator> requestHandlers;

        private RelayCommand whiteButtonPressed;

        private RelayCommand redButtonPressed;

        private RelayCommand blueButtonPressed;

        public RemoteButtonPusherWindowViewModel(View view)
            : base(view)
        {
            Machines = new List<RemoteButtonPusherMachine>();
            requestHandlers = new List<IWebRequestCreator>();

            LoadMachines();
        }

        public RemoteButtonPusherMachine CurrentlySelectedMachine { get; set; }

        public List<RemoteButtonPusherMachine> Machines { get; set; } 

        public RelayCommand WhiteButtonPressed
        {
            get
            {
                return whiteButtonPressed
                       ?? (whiteButtonPressed =
                           new RelayCommand(delegate { WhiteButton(); }, delegate { return true; }));
            }
        }

        public RelayCommand RedButtonPressed
        {
            get
            {
                return redButtonPressed
                       ?? (redButtonPressed =
                           new RelayCommand(delegate { RedButton(); }, delegate { return true; }));
            }
        }

        public RelayCommand BlueButtonPressed
        {
            get
            {
                return blueButtonPressed
                       ?? (blueButtonPressed =
                           new RelayCommand(delegate { BlueButton(); }, delegate { return true; }));
            }
        }

        private void WhiteButton()
        {
            try
            {
                var currentRequestHandler =
                    requestHandlers.First(
                        requestHandler =>
                        requestHandler.ServerAddress.Equals(CurrentlySelectedMachine.MachineInstance.IpOrDnsName));

                var request =
                    currentRequestHandler.CreatePostRequest(
                        new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "SET_PAUSE_MODE", 2),
                        true);

                request.GetResponse();
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile(e.Message);
            }
        }

        private void RedButton()
        {
            try
            {
                var currentRequestHandler =
                    requestHandlers.First(
                        requestHandler =>
                        requestHandler.ServerAddress.Equals(CurrentlySelectedMachine.MachineInstance.IpOrDnsName));

                var request =
                    currentRequestHandler.CreatePostRequest(
                        new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CLEAR_MACHINE_ERRORS", 2),
                        true);
                
                request.GetResponse();
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile(e.Message);
            }
        }

        private void BlueButton()
        {
            try
            {
                var currentRequestHandler =
                    requestHandlers.First(
                        requestHandler =>
                        requestHandler.ServerAddress.Equals(CurrentlySelectedMachine.MachineInstance.IpOrDnsName));

                var request =
                    currentRequestHandler.CreatePostRequest(
                        new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "DO_BOX_RELEASE_OUT_FEED", 2),
                        true);
                
                request.GetResponse();
            }
            catch (Exception e)
            {
                Logging.WriteToLogFile(e.Message);
            }
        }

        private void LoadMachines()
        {
            var machines = new MachinesSupplier();

            if (Directory.Exists(CoreSettings.Instance.MachineManagerDataFolderPath))
            {
                machines =
                    MachinesSupplier.GetInstanceFromSpecifiedPath(
                        CoreSettings.Instance.MachineManagerDataFolderPath + "\\MachinesConfiguration.xml");
            }
            else
            {
                var openFileDialog = new OpenFileDialog();

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        machines =
                        MachinesSupplier.GetInstanceFromSpecifiedPath(openFileDialog.FileName);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(Resources.UnableToOpenMachinesConfigFile);
                        return;
                    }
                }
            }

            foreach (var m in from Machine m in machines.Machines.Machine where m.IpOrDnsName != null select m)
            {
                Machines.Add(new RemoteButtonPusherMachine(m));
                requestHandlers.Add(new WebRequestCreator(m.IpOrDnsName, new PostRequestPopulator()));
            }
        }
    }
}
