﻿

namespace FusionServiceApplication.Viewmodels
{
    using System;
    using System.Threading;
    using System.Windows;

    using Utilities.Communication;

    using Helpers;
    using Models;
    using Objects;
    using Onyx.Windows;
    using PresentationModels;

    public class HardwareControlWindowViewModel : ViewModel
    {
        #region fields

        private readonly HardwareControlActivationTimes activationTimes = new HardwareControlActivationTimes();
        private readonly WriteToLogDelegate writeToLog;
        private readonly Thread updateOutputStatesThread;

        private readonly HardwareControlModel model;

        private RelayCommand toggleRedLedCommand;
        private RelayCommand toggleWhiteLedCommand;
        private RelayCommand toggleGreenLedCommand;

        private RelayCommand activateRightPressureRollerCommand;
        private RelayCommand activateLeftPressureRollerCommand;
        private RelayCommand toggleCutKnifeCommand;
        private RelayCommand activateLatcherCommand;
        private RelayCommand lowerRollerCommand;

        private RelayCommand toggleTrackSensorControlCommand;
        private RelayCommand setTrackSensorControlModeSpeedCommand;
        
        private ControlCloseDelegate windowOnCloseDelegate;

        #endregion

        #region ctor

        public HardwareControlWindowViewModel(View view, WriteToLogDelegate writeToLogDelegate, IqFusionCommunicator machineCommunicator, ControlCloseDelegate onControlCloseDelegate)
            : base(view)
        {
            writeToLog = writeToLogDelegate;

            model = new HardwareControlModel(writeToLogDelegate, machineCommunicator);

            windowOnCloseDelegate = onControlCloseDelegate;
            updateOutputStatesThread = new Thread(UpdateOutputStates);
            updateOutputStatesThread.Start();
            TrackControlModePresentationModel = new TrackSensorControlModePresentationModel();

            InitiateView();
        }

        #endregion

        #region properties

        public TrackSensorControlModePresentationModel TrackControlModePresentationModel { get; private set; }

        public Thickness TrackSensorControlModeButtonMargin
        {
            get { return TrackControlModePresentationModel.TrackSensorControlModeButtonMargin; }
        }

        public string TrackSensorControlModeButtonContent
        {
            get { return TrackControlModePresentationModel.TrackSensorControlModeButtonContent; }
        }

        public bool SpeedValueBoxIsEnabled
        {
            get { return TrackControlModePresentationModel.SpeedValueBoxIsEnabled; }
        }

        public Visibility SpeedValueBoxVisibility
        {
            get { return TrackControlModePresentationModel.SpeedValueBoxVisibility; }
        }

        public EventHandler OutPutSignalStateChangedEventHandler { get; set; }

        public ControlCloseDelegate WindowOnCloseDelegate
        {
            get
            {
                if (windowOnCloseDelegate == null)
                {
                    throw new Exception("WindowOnCloseDelegate == NULL");
                }

                return windowOnCloseDelegate;
            }

            set
            {
                windowOnCloseDelegate = value;
            }
        }

        public RelayCommand ToggleGreenLEDCommand
        {
            get
            {
                return toggleGreenLedCommand ??
                       (toggleGreenLedCommand = new RelayCommand(
                                                                      delegate { model.ToggleLight("GreenLED"); },
                                                                      delegate { return true; }));
            }
        }

        public RelayCommand ToggleRedLEDCommand
        {
            get
            {
                return toggleRedLedCommand ??
                       (toggleRedLedCommand = new RelayCommand(
                                                                    delegate { model.ToggleLight("RedLED"); },
                                                                    delegate { return true; }));
            }
        }

        public RelayCommand ToggleWhiteLEDCommand
        {
            get
            {
                return toggleWhiteLedCommand ??
                       (toggleWhiteLedCommand = new RelayCommand(
                                                                      delegate { model.ToggleLight("WhiteLED"); },
                                                                      delegate { return true; }));
            }
        }

        public RelayCommand ActivateRightPressureRollerCommand
        {
            get
            {
                return activateRightPressureRollerCommand ??
                       (activateRightPressureRollerCommand =
                        new RelayCommand(
                                         delegate { ActivatePressureRoller("right"); },
                                         delegate { return true; }));
            }
        }

        public RelayCommand ActivateLeftPressureRollerCommand
        {
            get
            {
                return activateLeftPressureRollerCommand ??
                       (activateLeftPressureRollerCommand =
                        new RelayCommand(
                                         delegate { ActivatePressureRoller("left"); },
                                         delegate { return true; }));
            }
        }

        public RelayCommand ToggleCutKnifeCommand
        {
            get
            {
                return toggleCutKnifeCommand ??
                       (toggleCutKnifeCommand = new RelayCommand(
                                                                      delegate { ToggleKnife(); },
                                                                      delegate { return true; }));
            }
        }

        public RelayCommand ActivateLatcherCommand
        {
            get
            {
                return activateLatcherCommand ??
                       (activateLatcherCommand = new RelayCommand(
                                                                       delegate { ActivateLatcher(); },
                                                                       delegate { return true; }));
            }
        }

        public RelayCommand LowerRollerCommand
        {
            get
            {
                return lowerRollerCommand ??
                       (lowerRollerCommand = new RelayCommand(
                                                                   delegate { LowerRoller(); },
                                                                   delegate { return true; }));
            }
        }

        public RelayCommand ToggleTrackSensorControlCommand
        {
            get
            {
                return toggleTrackSensorControlCommand ??
                       (toggleTrackSensorControlCommand =
                        new RelayCommand(
                                         delegate { model.ToggleTrackSensorControl(); SetTrackSensorControlModeIndicator();},
                                         delegate { return true; }));
            }
        }

        public RelayCommand SetTrackSensorControlModeSpeedCommand
        {
            get
            {
                return setTrackSensorControlModeSpeedCommand ??
                       (setTrackSensorControlModeSpeedCommand =
                        new RelayCommand(
                                         delegate { model.SetTrackSensorControlModeSpeed(); },
                                         delegate { return true; }));
            }
        }

        public string TrackSensorControlModeSpeed
        {
            get
            {
                return model.TrackSensorControlModeSpeed;
            }

            set
            {
                model.TrackSensorControlModeSpeed = value;
                OnPropertyChanged("TrackSensorControlModeSpeed");
            }
        }

        public bool RedLEDState
        {
            get
            {
                return model.RedLEDState;
            }

            set
            {
                model.RedLEDState = value;
                OnPropertyChanged(() => RedLEDState);
            }
        }

        public bool WhiteLEDState
        {
            get
            {
                return model.WhiteLEDState;
            }

            set
            {
                model.WhiteLEDState = value;
            }
        }

        public bool GreenLEDState
        {
            get
            {
                return model.GreenLEDState;
            }

            set
            {
                model.GreenLEDState = value;
                OnPropertyChanged(() => GreenLEDState);
            }
        }

        public bool LatcherState
        {
            get
            {
                return model.LatcherState;
            }

            set
            {
                model.LatcherState = value;
                OnPropertyChanged("LatcherState");
            }
        }

        public bool RightRollerState
        {
            get
            {
                return model.RightRollerState;
            }

            set
            {
                model.RightRollerState = value;
                OnPropertyChanged("RightRollerState");
            }
        }

        public bool LeftRollerState
        {
            get
            {
                return model.LeftRollerState;
            }

            set
            {
                model.LeftRollerState = value;
                OnPropertyChanged("LeftRollerState");
            }
        }

        public bool KnifeState
        {
            get
            {
                return model.KnifeState;
            }

            set
            {
                model.KnifeState = value;
                OnPropertyChanged("KnifeState");
            }
        }

        public bool TrackSensorControlModeState
        {
            get
            {
                return model.TrackSensorControlModeState;
            }

            set
            {
                model.TrackSensorControlModeState = value;

                if (OutPutSignalStateChangedEventHandler != null)
                {
                    OutPutSignalStateChangedEventHandler.Invoke(null, null);
                }
            }
        }

        private bool CrossHeadNotMoving
        {
            get { return model.CrossHeadNotMoving; }
        }

        #endregion

        #region methods

        private void InitiateView()
        {
            KnifeState = KnifeState;
            LatcherState = LatcherState;
            RightRollerState = RightRollerState;
            LeftRollerState = LeftRollerState;

            SetTrackSensorControlModeIndicator();
        }

        private void SetTrackSensorControlModeIndicator()
        {
            if (TrackSensorControlModeState)
            {
                TrackControlModePresentationModel.SetForTracksensorControlMode();
            }
            else
            {
                TrackControlModePresentationModel.Reset();
            }

            OnPropertyChanged(() => TrackSensorControlModeButtonContent);
            OnPropertyChanged(() => TrackSensorControlModeButtonMargin);
            OnPropertyChanged(() => SpeedValueBoxIsEnabled);
            OnPropertyChanged(() => SpeedValueBoxVisibility);
        }

        public void AbortThreads()
        {
            model.Dispose();

            ////todo stäng av track sensor control mode
            if (updateOutputStatesThread != null)
            {
                updateOutputStatesThread.Abort(0);
            }
        }

        private void LowerRoller()
        {
            model.LowerRoller();
        }

        private void ActivateLatcher()
        {
            if (!CrossHeadNotMoving)
            {
                writeToLog("Unable to latch, crosshead moving");
                return;
            }

            LatcherState = !LatcherState;

            if (LatcherState)
            {
                activationTimes.LastLatchTime = DateTime.Now;
            }
        }

        private void ToggleKnife()
        {
            KnifeState = !KnifeState;

            if (KnifeState)
            {
                activationTimes.LastKnifeActivation = DateTime.Now;
            }
        }

        private void ActivatePressureRoller(string roller)
        {
            switch (roller)
            {
                case "left":
                    LeftRollerState = !LeftRollerState;

                    if (LeftRollerState)
                    {
                        activationTimes.LastLeftRollerActivateTime = DateTime.Now;
                    }

                    break;
                case "right":
                    RightRollerState = !RightRollerState;

                    if (RightRollerState)
                    {
                        activationTimes.LastRightRollerActivateTime = DateTime.Now;
                    }

                    break;
            }
        }

        private void UpdateOutputStates()
        {
            var breakPoint = new TimeSpan(0, 0, 1, 0);

            while (true)
            {
                if (LatcherState &&
                    DateTime.Now - activationTimes.LastLatchTime > breakPoint)
                {
                    LatcherState = false;
                }

                if (KnifeState &&
                    DateTime.Now - activationTimes.LastKnifeActivation > breakPoint)
                {
                    KnifeState = false;
                }

                if (RightRollerState &&
                    DateTime.Now - activationTimes.LastRightRollerActivateTime > breakPoint)
                {
                    RightRollerState = false;
                }

                if (LeftRollerState &&
                    DateTime.Now - activationTimes.LastLeftRollerActivateTime > breakPoint)
                {
                    LeftRollerState = false;
                }

                Thread.Sleep(1000);
            }
        }

        #endregion
    }
}