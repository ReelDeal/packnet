﻿namespace FusionServiceApplication.Viewmodels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Threading;

    using Helpers;
    using Managers;
    using Utilities.Communication;
    using Utilities.Logging;
    using Views;

    using Onyx.Windows;

    public delegate void WriteToLogDelegate(string message);

    public delegate void WriteToLogFileDelegate(string message);

    public delegate void ControlCloseDelegate(Window currentWindow);
    
    internal class MainWindowViewModel : ViewModel, IDisposable
    {
        #region fields

        private readonly List<Window> currentlyOpenWindows;
        private readonly ObservableCollection<string> logMessages;

        private readonly ControlCloseDelegate onControlCloseDelegate;
        private readonly WriteToLogDelegate writeToLog;

        private RelayCommand closeWindowCommand;
        private RelayCommand connectToMachineCommand;
        private RelayCommand disconnectMachine;
        private RelayCommand launchConfigurationWindowCommand;
        private RelayCommand launchCustomInstructionListWindowCommand;
        private RelayCommand launchHardwareControlWindowCommand;
        private RelayCommand launchNetworkScannerWindowCommand;
        private RelayCommand clearLogCommand;
        private RelayCommand run4EverCommand;
        private RelayCommand reloadPhysicalMachineSettingsCommand;
        private RelayCommand launchConfigEditorWindow;
        private RelayCommand launchMachinePlcWatcherCommand;
        private RelayCommand calibrateTracksCommand;
        private RelayCommand launchRemotePlcButtonPusher;

        private Run4EverManager run4EverManager;
        private MachineConnectionManager machineConnectionManager;
        private MachineCalibrationHandler machineCalibrationHandler;

        #endregion

        #region ctor

        public MainWindowViewModel(View view)
            : base(view)
        {
            logMessages = new ObservableCollection<string>();
            currentlyOpenWindows = new List<Window>();
            writeToLog = WriteToLog;
            onControlCloseDelegate = OnControlWindowClose;

            machineConnectionManager = new MachineConnectionManager(writeToLog);
            WireEvents();

            Application.Current.DispatcherUnhandledException += ExceptionLogger.UnhandledExceptionHandler;
            AppDomain.CurrentDomain.UnhandledException += ExceptionLogger.UnhandledExceptionHandler;
        }

        #endregion

        #region properties

        public ObservableCollection<string> LogMessages
        {
            get { return logMessages; }
        }

        public string Title
        {
            get { return "Iq Fusion service application " + Assembly.GetExecutingAssembly().GetName().Version; }
        }

        public string CurrentRun4EverRuntime
        {
            get
            {
                return run4EverManager != null ? run4EverManager.CurrentRunTime : string.Empty;
            }
        }

        public string Run4EverButtonText
        {
            get { return run4EverManager == null ? "RUN 4-EVER" : "Runtime: " + CurrentRun4EverRuntime; }
        }

        public ObservableCollection<ErrorCode> ErrorCodes
        {
            get
            {
                return machineConnectionManager == null ? null : machineConnectionManager.ErrorCodes;
            }

            set
            {
                machineConnectionManager.ErrorCodes = value;
                OnPropertyChanged("ErrorCodes");
            }
        }

        public RelayCommand ReloadPhysicalMachineSettingsCommand
        {
            get
            {
                return reloadPhysicalMachineSettingsCommand ??
                       (reloadPhysicalMachineSettingsCommand =
                           new RelayCommand(
                               delegate
                               {
                                   if (machineConnectionManager.MachineCommunicator != null
                                       && machineConnectionManager.MachineCommunicator.Connected)
                                   {
                                       writeToLog("Synchornizing machine settings, please wait");
                                       Dispatcher.CurrentDispatcher.BeginInvoke(
                                           (Action)(() =>
                                               {
                                                   PhyscialMachineSettingsHelper.SyncMachine(
                                                       machineConnectionManager.MachineCommunicator);
                                                   writeToLog("Done");
                                               }),
                                           DispatcherPriority.SystemIdle);
                                   }
                                   else
                                   {
                                       WriteToLog("Unable to reload machine settings, no connection");
                                   }
                               },
                               delegate { return true; }));
            }
        }

        public RelayCommand ConnectToMachineCommand
        {
            get
            {
                return connectToMachineCommand ??
                       (connectToMachineCommand = new RelayCommand(
                           delegate
                           {
                               if (machineConnectionManager == null)
                               {
                                   machineConnectionManager = new MachineConnectionManager(writeToLog);
                               }

                               Dispatcher.CurrentDispatcher.BeginInvoke((Action)(() => machineConnectionManager.Connect()));
                           },
                           delegate { return true; }));
            }
        }

        public RelayCommand DisconnectMachineCommand
        {
            get
            {
                return disconnectMachine ??
                       (disconnectMachine = new RelayCommand(
                           delegate
                           {
                               Dispatcher.CurrentDispatcher.BeginInvoke(
                                   (Action)(() => machineConnectionManager.Disconnect()));
                           },
                           delegate { return true; }));
            }
        }

        public RelayCommand LaunchRemotePlcButtonPusher
        {
            get
            {
                return launchRemotePlcButtonPusher
                       ?? (launchRemotePlcButtonPusher =
                           new RelayCommand(
                               delegate { LaunchRemotePlcButtonPusherWindow(); },
                               delegate { return true; }));
            }
        }

        public RelayCommand LaunchConfigurationWindowCommand
        {
            get
            {
                return launchConfigurationWindowCommand
                       ?? (launchConfigurationWindowCommand =
                           new RelayCommand(delegate { LaunchConfigurationWindow(); }, delegate { return true; }));
            }
        }

        public RelayCommand LaunchNetworkScannerWindowCommand
        {
            get
            {
                return launchNetworkScannerWindowCommand
                       ?? (launchNetworkScannerWindowCommand =
                           new RelayCommand(delegate { LaunchNetworkScanner(); }, delegate { return true; }));
            }
        }

        public RelayCommand LaunchCustomInstructionListWindowCommand
        {
            get
            {
                return launchCustomInstructionListWindowCommand
                       ?? (launchCustomInstructionListWindowCommand =
                           new RelayCommand(
                               delegate { LaunchCustomInstrucionListWindow(); },
                               delegate { return true; }));
            }
        }

        public RelayCommand LaunchHardwareControlWindowCommand
        {
            get
            {
                return launchHardwareControlWindowCommand
                       ?? (launchHardwareControlWindowCommand =
                           new RelayCommand(delegate { LaunchHardwareControlWindow(); }, delegate { return true; }));
            }
        }

        public RelayCommand LaunchConfigEditorWindow
        {
            get
            {
                return launchConfigEditorWindow
                       ?? (launchConfigEditorWindow =
                           new RelayCommand(delegate { LaunchConfigEditor(); }, delegate { return true; }));
            }
        }

        public RelayCommand Run4EverCommand
        {
            get
            {
                if (run4EverCommand != null)
                {
                    return run4EverCommand;
                }

                run4EverCommand = new RelayCommand(
                    delegate
                        {
                            if (run4EverManager == null)
                            {
                                run4EverManager =
                                    new Run4EverManager(
                                        machineConnectionManager.MachineCommunicator,
                                        writeToLog,
                                        View);
                                run4EverManager.OnNewPackageSentToMachine += OnNewRun4EverPackageSent;
                                run4EverManager.Start();
                            }
                            else
                            {
                                if (run4EverManager.NrOfPackagesSent > 0)
                                {
                                    writeToLog("Total Run-4-ever session runtime: " + CurrentRun4EverRuntime);
                                }

                                run4EverManager.End();
                                run4EverManager = null;
                                OnPropertyChanged("Run4EverButtonText");
                            }
                        },
                    delegate { return true; });

                return run4EverCommand;
            }
        }

        public string MachineIp
        {
            get
            {
                if (machineConnectionManager != null)
                {
                    return machineConnectionManager.MachineIp;
                }

                machineConnectionManager = new MachineConnectionManager(writeToLog);
                WireEvents();
                return machineConnectionManager.MachineIp;
            }

            set
            {
                if (machineConnectionManager == null)
                {
                    machineConnectionManager = new MachineConnectionManager(writeToLog);
                    WireEvents();
                }

                machineConnectionManager.MachineIp = value;
            }
        }

        public RelayCommand CloseWindowCommand
        {
            get
            {
                return closeWindowCommand ??
                       (closeWindowCommand = new RelayCommand(
                           delegate { OnCloseWindow(); },
                           delegate { return true; }));
            }
        }

        public RelayCommand ClearLogCommand
        {
            get
            {
                return clearLogCommand ?? (clearLogCommand = new RelayCommand(
                    delegate { logMessages.Clear(); },
                    delegate { return true; }));
            }
        }

        public RelayCommand LaunchMachinePlcWatcherCommand
        {
            get
            {
                return launchMachinePlcWatcherCommand
                       ?? (launchMachinePlcWatcherCommand =
                           new RelayCommand(
                               delegate { LaunchMachinePlcWatcherWindow(); },
                               delegate { return true; }));
            }
        }

        public RelayCommand CalibrateTracksCommand
        {
            get
            {
                return calibrateTracksCommand ??
                       (calibrateTracksCommand = new RelayCommand(
                           delegate
                           {
                               if (machineCalibrationHandler == null)
                               {
                                   machineCalibrationHandler =
                                       new MachineCalibrationHandler(
                                           machineConnectionManager.MachineCommunicator,
                                           writeToLog);
                               }

                               machineCalibrationHandler.CalibrateTracks();
                           },
                                                          delegate { return true; }));
            }
        }
        
        public bool CurrentlyConnected
        {
            get
            {
                return machineConnectionManager.HasConnection;
            }
        }

        #endregion

        #region methods

        public void Dispose()
        {
            if (run4EverManager != null)
            {
                run4EverManager.Dispose();
                run4EverManager = null;
            }

            if (machineConnectionManager != null)
            {
                var lastIp = machineConnectionManager.MachineIp;
                machineConnectionManager.Dispose();
                machineConnectionManager = new MachineConnectionManager(writeToLog, lastIp);
            }

            CloseOpenWindows();
        }

        private void WriteToLog(string message)
        {
            if (Application.Current == null)
            {
                return;
            }

            Application.Current.Dispatcher.Invoke(
                DispatcherPriority.Normal,
                new Action(() => logMessages.Add(message)));
        }

        private void LaunchHardwareControlWindow()
        {
            if (machineConnectionManager == null || !machineConnectionManager.HasConnection)
            {
                WriteToLog("Unable to open hardware control tool, No connection");
                return;
            }

            var hardwarewindow =
                currentlyOpenWindows.FirstOrDefault(window => window.GetType() == typeof(HardwareControlWindow));
            if (hardwarewindow != null)
            {
                hardwarewindow.Focus();
                return;
            }

            var hwcw =
                new HardwareControlWindow(new HardwareControlWindowViewModel(
                    View,
                    writeToLog,
                    machineConnectionManager.MachineCommunicator,
                    onControlCloseDelegate));
            hwcw.Show();

            currentlyOpenWindows.Add(hwcw);
        }

        private void LaunchCustomInstrucionListWindow()
        {
            if (machineConnectionManager == null || !machineConnectionManager.HasConnection)
            {
                WriteToLog("Unable to open custom instruction list tool, No connection");
                return;
            }

            var cilWindow =
                currentlyOpenWindows.FirstOrDefault(
                    window => window.GetType() == typeof(CustomInstructionListWindow));
            if (cilWindow != null)
            {
                cilWindow.Focus();
                return;
            }

            var cilw =
                new CustomInstructionListWindow(new CustomInstructionListViewModel(
                    View,
                    machineConnectionManager.MachineCommunicator,
                    writeToLog,
                    onControlCloseDelegate));
            cilw.Show();

            currentlyOpenWindows.Add(cilw);
        }

        private void LaunchConfigurationWindow()
        {
            if (machineConnectionManager == null || !machineConnectionManager.HasConnection)
            {
                WriteToLog("Unable to open configuration tool, No connection");
                return;
            }

            var cfgWindow =
                currentlyOpenWindows.FirstOrDefault(
                    window => window.GetType() == typeof(NetworkConfigurationWindow));
            if (cfgWindow != null)
            {
                cfgWindow.Focus();
                return;
            }

            var cfgw =
                new NetworkConfigurationWindow(new NetworkConfigurationWindowViewModel(
                    View,
                    machineConnectionManager.MachineCommunicator,
                    writeToLog,
                    onControlCloseDelegate));
            cfgw.Show();

            currentlyOpenWindows.Add(cfgw);
        }

        private void LaunchNetworkScanner()
        {
            var networkScannerWindow =
                currentlyOpenWindows.FirstOrDefault(window => window.GetType() == typeof(NetworkScannerWindow));
            if (networkScannerWindow != null)
            {
                networkScannerWindow.Focus();
                return;
            }

            networkScannerWindow =
                new NetworkScannerWindow
                {
                    DataContext =
                        new NetworkScannerViewModel(View, onControlCloseDelegate)
                };
            networkScannerWindow.Show();

            currentlyOpenWindows.Add(networkScannerWindow);
        }

        private void LaunchConfigEditor()
        {
            var configEditorWindow =
                currentlyOpenWindows.FirstOrDefault(window => window.GetType() == typeof(ConfigEditor));
            if (configEditorWindow != null)
            {
                configEditorWindow.Focus();
                return;
            }

            configEditorWindow = new ConfigEditor
                                 {
                                     DataContext = new ConfigEditorViewModel(View, onControlCloseDelegate)
                                 };
            configEditorWindow.Show();

            currentlyOpenWindows.Add(configEditorWindow);
        }

        private void LaunchMachinePlcWatcherWindow()
        {
            var plcWatcherWindow =
                currentlyOpenWindows.FirstOrDefault(window => window.GetType() == typeof(PlcVariableWatcher));

            if (plcWatcherWindow != null)
            {
                plcWatcherWindow.Focus();
                return;
            }

            plcWatcherWindow = new PlcVariableWatcher
                               {
                                   DataContext =
                                       new PlcVariableWatcherViewModel(
                                       View, 
                                       onControlCloseDelegate, 
                                       machineConnectionManager.MachineCommunicator, 
                                       WriteToLog)
                               };
        
            plcWatcherWindow.Show();

            currentlyOpenWindows.Add(plcWatcherWindow);
        }

        private void LaunchRemotePlcButtonPusherWindow()
        {
            var remoteButtonPusherWindow =
                currentlyOpenWindows.FirstOrDefault(window => window.GetType() == typeof(RemoteButtonPusherWindow));

            if (remoteButtonPusherWindow != null)
            {
                remoteButtonPusherWindow.Focus();
                return;
            }

            remoteButtonPusherWindow = new RemoteButtonPusherWindow
                               {
                                   DataContext = new RemoteButtonPusherWindowViewModel(View)
                               };
        
            remoteButtonPusherWindow.Show();

            currentlyOpenWindows.Add(remoteButtonPusherWindow);
        }

        private void CloseOpenWindows()
        {
            for (int index = 0; index < currentlyOpenWindows.Count; index++)
            {
                currentlyOpenWindows[index].Close();
            }
        }

        #endregion

        #region EventHandlers

        private void WireEvents()
        {
            machineConnectionManager.OnCleanUpRequestedEvent += CleanUpRequested;
            machineConnectionManager.OnDisconnectedEvent += OnMachineCommunicatorDisconnected;
            machineConnectionManager.OnConnectedEvent += OnMachineCommunicatorConnected;
            machineConnectionManager.OnMachineErrorsUpdatedNeeded += OnMachineErrorsUpdateNeeded;
        }

        private void CleanUpRequested(object sender, EventArgs e)
        {
            Dispose();
        }

        private void OnCloseWindow()
        {
            Dispose();
        }

        private void OnControlWindowClose(Window currentWindow)
        {
            currentlyOpenWindows.Remove(currentWindow);
        }

        private void OnNewRun4EverPackageSent()
        {
            OnPropertyChanged("Run4EverButtonText");
        }

        private void OnMachineCommunicatorConnected(object sender, EventArgs e)
        {
            OnPropertyChanged("CurrentlyConnected");
        }

        private void OnMachineCommunicatorDisconnected(object sender, EventArgs e)
        {
            OnPropertyChanged("MachineIp");
            OnPropertyChanged("ErrorCodes");
            OnPropertyChanged("CurrentlyConnected");
        }

        private void OnMachineErrorsUpdateNeeded(object sender, EventArgs e)
        {
            OnPropertyChanged("ErrorCodes");
        }

        #endregion
    }
}