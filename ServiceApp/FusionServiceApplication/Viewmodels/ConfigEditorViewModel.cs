﻿using FusionServiceApplication.SettingHandlers.MachineConfig;
using View = Onyx.Windows.View;

namespace FusionServiceApplication.Viewmodels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.IO;
    using System.Windows.Forms;

    using Helpers;
    using SettingHandlers;
    using SettingHandlers.CartonPropertyGroupConfiguration;
    using SettingHandlers.ClassificationsConfig;
    using SettingHandlers.ComponentsConfig;
    using SettingHandlers.CorrugatesConfiguration;
    using SettingHandlers.ExportImportLogicConfig;
    using SettingHandlers.FileImporterConfig;
    using SettingHandlers.MachinesConfiguration;
    using SettingHandlers.MessageProducerConfiguration;
    using SettingHandlers.PickZonesConfiguration;
    using SettingHandlers.ProductionGroupsConfiguration;
    using SettingHandlers.ReportsConfig;
    using SettingHandlers.ServiceCommunication;
    using SettingHandlers.ServiceImporterSettings;
    using SettingHandlers.SocketCommunication;
    using SettingHandlers.WmsConfig;
    using Usercontrols;

    using Onyx.Windows;

    using Machine = Machine;
    using View = View;

    public class ConfigEditorViewModel : ViewModel
    {
        private RelayCommand loadConfigFileCommand;
        private RelayCommand loadFileImporterConfiguration;
        private RelayCommand loadComponentsConfiguration;
        private RelayCommand loadCartonPropertyGroupsConfiguration;
        private RelayCommand loadClassificationsConfiguration;
        private RelayCommand loadCorrugatesConfiguration;
        private RelayCommand loadExportImportLogicConfiguration;
        private RelayCommand loadMachinesConfiguration;
        private RelayCommand loadReportsConfiguration;
        private RelayCommand loadServiceCommunicatorConfiguration;
        private RelayCommand loadServiceImporterConfiguration;
        private RelayCommand loadSocketCommunicationConfiguration;
        private RelayCommand loadWMSConfiguration;
        private RelayCommand loadMessageProducerConfiguration;
        private RelayCommand loadPickZonesConfiguration;
        private RelayCommand loadSequenceGroupsConfiguration;

        private RelayCommand closeAllConfigFilesCommand;
        private RelayCommand closeCurrentConfigFileCommand;

        private RelayCommand saveConfigFileCommand;
        private RelayCommand saveAsConfigFileCommand;

        private RelayCommand reloadAllConfigFilesCommand;
        private RelayCommand reloadCurrentConfigFileCommand;

        #region ctor

        public ConfigEditorViewModel(View view, ControlCloseDelegate onWindowCloseDelegate)
            : base(view)
        {
            CurrentMachineConfigs = new List<IConfigFile>();
            ConfigEditorPropertyGrids = new ObservableCollection<WpfPropertyGrid>();
            CurrentMachineConfigDefaultLocations = new List<string>();
            WindowOnCloseDelegate = onWindowCloseDelegate;

            LoadFromMachineManagerFolderIfPossible();
        }
        
        #endregion

        #region properties

        public ControlCloseDelegate WindowOnCloseDelegate { get; set; }

        public List<IConfigFile> CurrentMachineConfigs { get; set; }

        public WpfPropertyGrid CurrentlySelectedPropertyGrid { get; set; }

        public List<string> CurrentMachineConfigDefaultLocations { get; set; } 
        
        public ObservableCollection<WpfPropertyGrid> ConfigEditorPropertyGrids
        { 
            get; 
            set; 
        }

        public RelayCommand ReloadCurrentConfigFileCommand
        {
            get
            {
                return reloadCurrentConfigFileCommand ?? (reloadCurrentConfigFileCommand =
                                                               new RelayCommand(
                                                                   delegate { ReloadCurrentConfigFile(); },
                                                                   delegate { return true; }));
            }
        }

        public RelayCommand ReloadAllConfigFilesCommand
        {
            get
            {
                return reloadAllConfigFilesCommand ?? (reloadAllConfigFilesCommand = new RelayCommand(
                                                                                                   delegate
                                                                                                       {
                                                                                                           CloseAllConfigFilesCommand
                                                                                                               .Execute(
                                                                                                                   null);
                                                                                                           LoadFromMachineManagerFolderIfPossible
                                                                                                               ();
                                                                                                       },
                                                                                                   delegate { return true; }));
            }
        }

        public RelayCommand CloseCurrentConfigFileCommand
        {
            get
            {
                return closeCurrentConfigFileCommand ?? (closeCurrentConfigFileCommand =
                                                              new RelayCommand(
                                                                  delegate { CloseCurrentConfigFile(); },
                                                                  delegate { return true; }));
            }
        }

        public RelayCommand CloseAllConfigFilesCommand
        {
            get
            {
                return closeAllConfigFilesCommand ?? (closeAllConfigFilesCommand = new RelayCommand(
                                                                                                 delegate
                                                                                                     {
                                                                                                         ConfigEditorPropertyGrids
                                                                                                             .Clear();
                                                                                                         CurrentMachineConfigs
                                                                                                             .Clear();
                                                                                                         CurrentMachineConfigDefaultLocations
                                                                                                             .Clear();

                                                                                                         OnPropertyChanged
                                                                                                                      ("CurrentMachineConfigs");
                                                                                                              },
                    delegate { return true; }));
            }
        }

        public RelayCommand LoadSequenceGroupsConfiguration
        {
            get
            {
                return loadSequenceGroupsConfiguration ?? (loadSequenceGroupsConfiguration =
                                                                new RelayCommand(
                                                                    delegate { LoadProductionGroupsConfig(); },
                                                                    delegate { return true; }));
            }
        }


        public RelayCommand LoadPickZonesConfiguration
        {
            get
            {
                return loadPickZonesConfiguration
                       ?? (loadPickZonesConfiguration =
                           new RelayCommand(delegate { LoadPickZonesConfig(); }, delegate { return true; }));
            }
        }


        public RelayCommand LoadMessageProducerConfiguration
        {
            get
            {
                return loadMessageProducerConfiguration ?? (loadMessageProducerConfiguration =
                                                                 new RelayCommand(
                                                                     delegate { LoadMessageProducerConfig(); },
                                                                     delegate { return true; }));
            }
        }

        public RelayCommand LoadWMSConfiguration
        {
            get
            {
                return loadWMSConfiguration ?? (loadWMSConfiguration =
                                                     new RelayCommand(delegate { LoadWMSConfig(); }, delegate { return true; }));
            }
        }

        public RelayCommand LoadSocketCommunicationConfiguration
        {
            get
            {
                return loadSocketCommunicationConfiguration ?? (loadSocketCommunicationConfiguration =
                                                                     new RelayCommand(
                                                                         delegate
                                                                             {
                                                                                 LoadSocketCommunicationConfig();
                                                                             },
                                                                         delegate { return true; }));
            }
        }
        
        public RelayCommand LoadServiceImporterConfiguration
        {
            get
            {
                return loadServiceImporterConfiguration ?? (loadServiceImporterConfiguration =
                                                                 new RelayCommand(
                                                                     delegate { LoadServiceImporterConfig(); },
                                                                     delegate { return true; }));
            }
        }


        public RelayCommand LoadServiceCommunicatorConfiguration
        {
            get
            {
                return loadServiceCommunicatorConfiguration ?? (loadServiceCommunicatorConfiguration =
                                                                     new RelayCommand(
                                                                         delegate
                                                                             {
                                                                                 LoadServiceCommunicatorConfig();
                                                                             },
                                                                         delegate { return true; }));
            }
        }


        public RelayCommand LoadReportsConfiguration
        {
            get
            {
                return loadReportsConfiguration
                       ?? (loadReportsConfiguration =
                           new RelayCommand(delegate { LoadReportsConfig(); }, delegate { return true; }));
            }
        }
        
        public RelayCommand LoadMachinesConfiguration
        {
            get
            {
                return loadMachinesConfiguration
                       ?? (loadMachinesConfiguration =
                           new RelayCommand(delegate { LoadMachinesConfig(); }, delegate { return true; }));
            }
        }
        
        public RelayCommand LoadExportImportLogicConfiguration
        {
            get
            {
                return loadExportImportLogicConfiguration ?? (loadExportImportLogicConfiguration =
                                                                   new RelayCommand(
                                                                       delegate { LoadExportImportLogicConfig(); },
                                                                       delegate { return true; }));
            }
        }
        
        public RelayCommand LoadCorrugatesConfiguration
        {
            get
            {
                return loadCorrugatesConfiguration
                       ?? (loadCorrugatesConfiguration =
                           new RelayCommand(delegate { LoadCorrugatesConfig(); }, delegate { return true; }));
            }
        }
        
        public RelayCommand LoadClassificationsConfiguration
        {
            get
            {
                return loadClassificationsConfiguration ?? (loadClassificationsConfiguration =
                                                                 new RelayCommand(
                                                                     delegate { LoadClassificationsConfig(); },
                                                                     delegate { return true; }));
            }
        }
        
        public RelayCommand LoadCartonPropertyGroupsConfiguration
        {
            get
            {
                return loadCartonPropertyGroupsConfiguration ?? (loadCartonPropertyGroupsConfiguration =
                                                                      new RelayCommand(
                                                                          delegate
                                                                              {
                                                                                  LoadCartonPropertyGroupsConfig();
                                                                              },
                                                                          delegate { return true; }));
            }
        }

        public RelayCommand LoadComponentsConfiguration
        {
            get
            {
                return loadComponentsConfiguration
                       ?? (loadComponentsConfiguration =
                           new RelayCommand(delegate { LoadComponentsConfig(); }, delegate { return true; }));
            }
        }

        public RelayCommand LoadFileImporterConfiguration
        {
            get
            {
                return loadFileImporterConfiguration ?? (loadFileImporterConfiguration =
                                                              new RelayCommand(
                                                                  delegate { LoadFileImporterConfigFile(); },
                                                                  delegate { return true; }));
            }
        }
        
        public RelayCommand LoadConfigFileCommand
        {
            get
            {
                return loadConfigFileCommand
                       ?? (loadConfigFileCommand =
                           new RelayCommand(delegate { LoadConfigFile(); }, delegate { return true; }));
            }
        }

        public RelayCommand SaveConfigFileCommand
        {
            get
            {
                return saveConfigFileCommand
                       ?? (saveConfigFileCommand =
                           new RelayCommand(delegate { SaveConfigFile(); }, delegate { return true; }));
            }
        }

        public RelayCommand SaveAsConfigFileCommand
        {
            get
            {
                return saveAsConfigFileCommand
                       ?? (saveAsConfigFileCommand =
                           new RelayCommand(delegate { SaveAsConfigFile(); }, delegate { return true; }));
            }
        }
        
        #endregion

        #region methods

        private void LoadConfigFile()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, Machine.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadComponentsConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, ComponentsConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadFileImporterConfigFile()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, FileImporterConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadCartonPropertyGroupsConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, CartonPropertyGroupConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadClassificationsConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, ClassificationsConfig.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadCorrugatesConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, CorrugatesConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadExportImportLogicConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, ExportLogicConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadMachinesConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, MachinesSupplier.GetInstanceFromSpecifiedPath);
            }
        }


        private void LoadReportsConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, ReportsConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadServiceCommunicatorConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, ServiceCommunicationConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadServiceImporterConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {

                TryLoadConfigFile(loadFileDialog.FileName, ServiceImporterSettings.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadSocketCommunicationConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, SocketCommunicationConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadWMSConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, WmsConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadPickZonesConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, PickZonesConfiguration.GetInstanceFromSpecifiedPath);
            }

        }

        private void LoadMessageProducerConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, MessageProducerConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void LoadProductionGroupsConfig()
        {
            var loadFileDialog = new OpenFileDialog();

            if (loadFileDialog.ShowDialog() == DialogResult.OK)
            {
                TryLoadConfigFile(loadFileDialog.FileName, ProductionGroupsConfiguration.GetInstanceFromSpecifiedPath);
            }
        }

        private void InsertNewIConfig(IConfigFile newlyLoadedConfigFile, string pathToFile)
        {
            var propertyGridForConfig = new WpfPropertyGrid
            {
                SelectedObject = newlyLoadedConfigFile,
                HelpVisible = true,
                TabHeader = Path.GetFileName(pathToFile)
            };

            CurrentMachineConfigs.Add(newlyLoadedConfigFile);
            ConfigEditorPropertyGrids.Add(propertyGridForConfig);

            CurrentMachineConfigDefaultLocations.Add(pathToFile);
            OnPropertyChanged("CurrentMachineConfigs");
        }

        private void CloseCurrentConfigFile()
        {
            if (CurrentMachineConfigs.Count > 0)
            {
                var currentlySelectedMachineConfigFile = CurrentlySelectedPropertyGrid.SelectedObject as IConfigFile;
                var indexOfCurrent = CurrentMachineConfigs.IndexOf(currentlySelectedMachineConfigFile);

                if (indexOfCurrent >= 0)
                {
                    CurrentMachineConfigs.RemoveAt(indexOfCurrent);
                    CurrentMachineConfigDefaultLocations.RemoveAt(indexOfCurrent);
                    ConfigEditorPropertyGrids.RemoveAt(indexOfCurrent);
                    OnPropertyChanged("CurrentMachineConfigs");
                }
            }
        }

        private void SaveConfigFile()
        {
            if (CurrentMachineConfigs.Count > 0)
            {
                var currentlySelectedMachineConfigFile = CurrentlySelectedPropertyGrid.SelectedObject as IConfigFile;

                var defaultPath = CurrentMachineConfigDefaultLocations[CurrentMachineConfigs.IndexOf(currentlySelectedMachineConfigFile)];

                if (currentlySelectedMachineConfigFile != null)
                    currentlySelectedMachineConfigFile.Serialize(defaultPath);
            }
        }

        private void SaveAsConfigFile()
        {
            if (CurrentMachineConfigs == null)
            {
                MessageBox.Show("You dont have a machine config loaded, aborting");
                return;
            }

            var saveFileDialog = new SaveFileDialog();

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                var currentlySelectedMachineConfigFile = CurrentlySelectedPropertyGrid.SelectedObject as IConfigFile;

                if (currentlySelectedMachineConfigFile != null)
                    currentlySelectedMachineConfigFile.Serialize(saveFileDialog.FileName);
            }
        }

        private void ReloadCurrentConfigFile()
        {
            if (CurrentMachineConfigs.Count > 0)
            {
                var currentlySelectedMachineConfigFile = CurrentlySelectedPropertyGrid.SelectedObject as IConfigFile;
                var indexOfCurrent = CurrentMachineConfigs.IndexOf(currentlySelectedMachineConfigFile);

                if (indexOfCurrent >= 0)
                {
                    (ConfigEditorPropertyGrids[indexOfCurrent].SelectedObject as IConfigFile).Reload(CurrentMachineConfigDefaultLocations[indexOfCurrent]);
                    OnPropertyChanged("ConfigEditorPropertyGrids");
                }
            }
        }

        private void LoadFromMachineManagerFolderIfPossible()
        {
            var machineManagerPath = CoreSettings.Instance.MachineManagerDataFolderPath;

            if (!Directory.Exists(machineManagerPath))
            {
                return;
            }

            TryLoadConfigFile(machineManagerPath + "\\CartonPropertyGroupConfiguration.xml", CartonPropertyGroupConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\Classifications.xml", ClassificationsConfig.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\ComponentsConfiguration.xml", ComponentsConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\CorrugatesConfiguration.xml", CorrugatesConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\ExportLogicConfiguration.xml", ExportLogicConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\FileImporterConfiguration.xml", FileImporterConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\FileImporterConfiguration_forOrders.xml", FileImporterConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\FileImporterConfiguration_forWave.xml", FileImporterConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\FileImporterConfiguration_kuk.xml", FileImporterConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\FileImporterConfiguration_org.xml", FileImporterConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\ImportLogicConfiguration.xml", ExportLogicConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\MachinesConfiguration.xml", MachinesSupplier.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\MessageProducer.xml", MessageProducerConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\PickZones.xml", PickZonesConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\ProductionGroups.xml", ProductionGroupsConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\ReportConfiguration.xml", ReportsConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\ServiceCommunication.xml", ServiceCommunicationConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\ServiceImporterConfiguration.xml", ServiceImporterSettings.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\SocketCommunication.xml", SocketCommunicationConfiguration.GetInstanceFromSpecifiedPath);
            TryLoadConfigFile(machineManagerPath + "\\WmsConfiguration.xml", WmsConfiguration.GetInstanceFromSpecifiedPath);
        }

        private void TryLoadConfigFile(string path, Func<string, IConfigFile> getInstanceFromSpecifiedPath)
        {
            try
            {
                var instance = getInstanceFromSpecifiedPath(path);
                if (instance != null)
                {
                    InsertNewIConfig(instance, path);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to load config file on path: " + path + " " + e.Message);
            }
        }

        #endregion
    }
}
