﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using FusionServiceApplication.Helpers;
using FusionServiceApplication.Objects;
using Onyx.Windows;

namespace FusionServiceApplication.Viewmodels
{
    using Utilities.Communication;
    using Utilities.Logging;

    class PlcVariableWatcherViewModel : ViewModel
    {
        private Thread plcWatcherUpdateThread;
        private readonly Thread plcWatcherThread; 
        
        private ControlCloseDelegate windowOnCloseDelegate;
        
        private RelayCommand updateMachineValuesCommand;
        
        public readonly WriteToLogDelegate WriteToLog;
        

        public PlcVariableWatcherViewModel(View view, ControlCloseDelegate onWindowClose, IqFusionCommunicator machineCommunicator, WriteToLogDelegate writeToLog) : base(view)
        {
            plcWatcherThread = new Thread(o => SetUpPlcWatcher());
            plcWatcherThread.Start();

            windowOnCloseDelegate = onWindowClose;
            MachineCommunicator = machineCommunicator;
            WriteToLog = writeToLog;
        }

        public ObservableCollection<PlcVariableEntry> PlcVariableEntries { get; private set; }

        public IqFusionCommunicator MachineCommunicator { get; private set; }

        public RelayCommand UpdateMachineValuesCommand
        {
            get
            {
                return updateMachineValuesCommand ??
                       (updateMachineValuesCommand = new RelayCommand(
                                                                           delegate { LaunchUpdateWatcherThread(); },
                                                                           delegate { return true; }));
            }
        }

        public ControlCloseDelegate WindowOnCloseDelegate
        {
            get
            {
                if (windowOnCloseDelegate == null)
                {
                    throw new Exception("WindowOnCloseDelegate == NULL");
                }

                if (plcWatcherThread != null)
                {
                    plcWatcherThread.Abort();
                }

                if (plcWatcherUpdateThread != null)
                {
                    plcWatcherUpdateThread.Abort();
                }

                return windowOnCloseDelegate;
            }

            set
            {
                windowOnCloseDelegate = value;
            }
        }


        private void LaunchUpdateWatcherThread()
        {
            plcWatcherUpdateThread = new Thread(o => UpdateMachineValues());
            plcWatcherUpdateThread.Start();
        }

        private void UpdateMachineValues()
        {
            foreach (var plcVariableEntry in PlcVariableEntries)
            {
              /*  if (plcVariableEntry.UpdateInPlc)
                {
                    try
                    {
                        this.MachineCommunicator.WriteValue(plcVariableEntry.PlcVariable, plcVariableEntry.Value);
                        plcVariableEntry.UpdateInPlc = false;
                    }
                    catch (Exception)
                    {
                        this.WriteToLog("Unable to write to PLC variable " + plcVariableEntry.Variable);
                    }

                    plcVariableEntry.UpdateInPlc = false;
                }
                else*/
                {
                    try
                    {
                        plcVariableEntry.Value = MachineCommunicator.ReadValue<object>(plcVariableEntry.PlcVariable).ToString();
                    }
                    catch (Exception e)
                    {
                        Logging.WriteToLogFile(e.ToString());
                    }
                }
            }

            plcWatcherUpdateThread.Abort();
        }

        public void SetUpPlcWatcher()
        {
            PlcVariableEntries = new ObservableCollection<PlcVariableEntry>();

            foreach (var packsizePlcVariable in MachineCommunicator.VariableMap.AsEnumerable())
            {
                PlcVariableEntries.Add(new PlcVariableEntry(packsizePlcVariable, string.Empty, false));
            }

            foreach (var plcVariableEntry in PlcVariableEntries)
            {
                try
                {
                    plcVariableEntry.Value =
                        MachineCommunicator.ReadValue<object>(plcVariableEntry.PlcVariable).ToString();
                }
                catch (Exception)
                {
                }
            }

            Thread.CurrentThread.Abort();
        }

    }
}
