﻿using FusionServiceApplication.Models;
using View = Onyx.Windows.View;

namespace FusionServiceApplication.Viewmodels
{
    using System;
    using System.Windows.Forms;

    using Utilities.Communication;

    using Helpers;

    using Onyx.Windows;

    using View = View;

    public class CustomInstructionListViewModel : ViewModel
    {
        #region fields

        private RelayCommand openLoadFileDialogCommand;
        private RelayCommand startExecutionCommand;
        private RelayCommand stopExecutionCommand;
        
        private ControlCloseDelegate windowOnCloseDelegate;
        private readonly CustomInstructionListModel model;

        #endregion

        #region ctor

        public CustomInstructionListViewModel(View view) : base(view)
        {
        }

        public CustomInstructionListViewModel(View view, IIqFusionCommunicator communicator, WriteToLogDelegate writeToLogDelegate, ControlCloseDelegate onControlCloseDelegate)
            : base(view)
        {
            model = new CustomInstructionListModel(communicator, writeToLogDelegate);
            windowOnCloseDelegate = onControlCloseDelegate;
        }

        #endregion

        #region properties

        public ControlCloseDelegate WindowOnCloseDelegate
        {
            get
            {
                if (windowOnCloseDelegate == null)
                {
                    throw new Exception("WindowOnCloseDelegate == NULL");
                }

                return windowOnCloseDelegate;
            }

            set
            {
                windowOnCloseDelegate = value;
            }
        }

        public string PathToCodeFile { get; set; }

        public RelayCommand StartExecutionCommand
        {
            get
            {
                return startExecutionCommand
                       ?? (startExecutionCommand =
                           new RelayCommand(delegate { StartExecution(); }, delegate { return true; }));
            }
        }

        public RelayCommand StopExecutionCommand
        {
            get
            {
                return stopExecutionCommand
                       ?? (stopExecutionCommand =
                           new RelayCommand(delegate { StopExecution(); }, delegate { return true; }));
            }
        }

        public RelayCommand OpenLoadFileDialogCommand
        {
            get
            {
                return openLoadFileDialogCommand
                       ?? (openLoadFileDialogCommand =
                           new RelayCommand(delegate { LoadFile(); }, delegate { return true; }));
            }
        }


        #endregion

        #region methods

        private void LoadFile()
        {
            var fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                PathToCodeFile = fileDialog.FileName;
                OnPropertyChanged("PathToCodeFile");
            }
        }

        private void StartExecution()
        {
            model.StartExecution(PathToCodeFile);
        }

        private void StopExecution()
        {
            model.StopExecution();
        }

        #endregion
    }
}
