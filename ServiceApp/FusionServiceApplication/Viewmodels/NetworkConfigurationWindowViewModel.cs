﻿using FusionServiceApplication.Interfaces;
using View = Onyx.Windows.View;

namespace FusionServiceApplication.Viewmodels
{
    using System;

    using Utilities.Communication;
    using Utilities.Enums;

    using Helpers;
    using Models;

    using Onyx.Windows;
    using View = View;

    public class NetworkConfigurationWindowViewModel : ViewModel
    {
        #region fields

        public readonly WriteToLogDelegate WriteToLog;
        private readonly IConfigurationModel model;

        private ControlCloseDelegate windowOnCloseDelegate;

        private RelayCommand setNetworkSettingsInPlc;
        private RelayCommand clearNonVolatileCommand;
        private RelayCommand retreiveCurrentNetworkSettingsFromPLC;


        #endregion

        #region ctor

        public NetworkConfigurationWindowViewModel(View view)
            : base(view)
        {
        }

        public NetworkConfigurationWindowViewModel(View view, IqFusionCommunicator com, WriteToLogDelegate writeToLogDelegate, ControlCloseDelegate onControlCloseDelegate)
            : base(view)
        {
            WriteToLog = writeToLogDelegate;
            windowOnCloseDelegate = onControlCloseDelegate;
            model = new ConfigurationModel(com, WriteToLog);
        }

        #endregion

        #region properties

        public ControlCloseDelegate WindowOnCloseDelegate
        {
            get
            {
                if (windowOnCloseDelegate == null)
                {
                    throw new Exception("WindowOnCloseDelegate == NULL");
                }

                return windowOnCloseDelegate;
            }

            set
            {
                windowOnCloseDelegate = value;
            }
        }

        public string HostName 
        {
            get
            {
                return model.CurrentHostname;
            }
            set
            {
                model.CurrentHostname = value;
                model.HasChangesBeenMade = true;
                OnPropertyChanged("HostName");
                OnPropertyChanged("ChangesMade");
            }
        }

        public string StaticIp
        {
            get
            {
                return model.CurrentIpAddress;
            }
            set
            {
                model.CurrentIpAddress = value;
                model.HasChangesBeenMade = true;
                OnPropertyChanged("StaticIp");
                OnPropertyChanged("ChangesMade");
            }
        }

        public string SubnetMask
        {
            get
            {
                return model.CurrentSubnetMask;
            }
            set
            {
                model.CurrentSubnetMask = value;
                model.HasChangesBeenMade = true;
                OnPropertyChanged("SubnetMask"); 
                OnPropertyChanged("ChangesMade");
            }
        }

        public string DefaultGateway
        {
            get
            {
                return model.CurrentGateway;
            }
            set
            {
                model.CurrentGateway = value;
                model.HasChangesBeenMade = true;
                OnPropertyChanged("DefaultGateway");
                OnPropertyChanged("ChangesMade");
            }
        }

        public int NetworkMode
        {
            get
            {
                return (int)model.CurrentNetworkMode;
            }
            set
            {
                model.CurrentNetworkMode = (NetworkMode) value;
                model.HasChangesBeenMade = true;
                OnPropertyChanged("NetworkMode");
                OnPropertyChanged("ChangesMade");
                OnPropertyChanged("DhcpIsNotSelected");
            }
        }

        public string MachineMacAddress
        {
            get
            {
                return model.MachineMacAddress;
            }
        }

        public string MachinePlcVersion
        {
            get
            {
                return model.MachinePlcVersion;
            }
        }

        public bool ChangesMade
        {
            get
            {
                return model.HasChangesBeenMade;
            }
            set
            {
                model.HasChangesBeenMade = value;
            }
        }

        public RelayCommand RetreiveCurrentNetworkSettingsFromPLC
        {
            get
            {
                return retreiveCurrentNetworkSettingsFromPLC ??
                       (retreiveCurrentNetworkSettingsFromPLC = new RelayCommand(
                           delegate
                           {
                               model.UpdateNetworkValuesFromMachine();
                               OnPropertyChanged("HostName");
                               OnPropertyChanged("StaticIp");
                               OnPropertyChanged("NetworkMode");
                               OnPropertyChanged("DefaultGateway");
                               OnPropertyChanged("SubnetMask");
                           },
                           delegate { return true; }));
            }
        }

        public RelayCommand SetNetworkSettingsInPlc
        {
            get
            {
                return setNetworkSettingsInPlc ??
                       (setNetworkSettingsInPlc = new RelayCommand(
                           delegate
                           {
                               model.SetNetworkSettingsInMachine();
                               model.HasChangesBeenMade = false;
                           },
                                                                 delegate { return true; }));
            }
        }

        public RelayCommand ClearNonVolatileCommand
        {
            get
            {
                return clearNonVolatileCommand ??
                       (clearNonVolatileCommand = new RelayCommand(
                                                                        delegate { ClearNonVolatileCommunicationOptions(); },
                                                                        delegate { return true; }));
            }   
        }

        public bool DhcpIsNotSelected
        {
            get
            {
                return model.CurrentNetworkMode != Utilities.Enums.NetworkMode.DHCP;
            }
        }

        #endregion

        #region methods

        private void ClearNonVolatileCommunicationOptions()
        {
            model.ClearNonVolatileCommunicationOptions();
        }
        
        #endregion
    }
}
