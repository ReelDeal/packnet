﻿namespace FusionServiceApplicationTests
{
    using System.Collections.Generic;
    using System.Linq;

    using FusionServiceApplication.Helpers;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class Run4EverTests
    {

        private InstructionListHandler instructionListHandler;

        [TestInitialize]
        public void TestSetup()
        {
            instructionListHandler = new InstructionListHandler(true, 1388, 4, 1370, 10);
        }

        [TestMethod]
        public void TestGetNewInstructionList()
        {
            var instructionList = instructionListHandler.GetNewInstructionList(6, new List<double> { 100, 200, 300, 400, 500, 600 });

            Assert.IsNotNull(instructionList);
            Assert.AreEqual(102, instructionList.Count());
        }
    }
}
