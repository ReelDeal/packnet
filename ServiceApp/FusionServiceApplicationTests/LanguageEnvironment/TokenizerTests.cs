﻿
namespace FusionServiceApplicationTests.LanguageEnvironment
{
    using System.IO;

    using FusionServiceApplication.LanguageEnvironment;
    using FusionServiceApplication.LanguageEnvironment.Exceptions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using TestUtils;

    [TestClass]
    [DeploymentItem(@"LanguageEnvironment\TestFiles\", "TestFiles")]
    public class TokenizerTests
    {
        private Tokenizer tokenizer;

        [TestInitialize]
        public void Setup()
        {
            tokenizer = new Tokenizer();
        }
        
        [TestMethod]
        public void TestSuccessfulLoad()
        {
            tokenizer.loadFile(@"./TestFiles/TestFile1.txt");
            Assert.AreEqual(43, tokenizer.FileLines.Count);
        }

        [TestMethod]
        public void TestGetTokens()
        {
            AssertUtil.Throws<FileNotLoadedException>( () => tokenizer.getTokens());
        
            LoadFile();

            var tokens = tokenizer.getTokens();
            Assert.AreEqual(32, tokens.Count);
        }

        [TestMethod]
        public void TestFileNotFoundLoad()
        {
            AssertUtil.Throws<FileNotFoundException>( () => tokenizer.loadFile("adsasdasd"));          
        }

        private void LoadFile()
        {
            tokenizer.loadFile(@"./TestFiles/TestFile1.txt");
        }
    }
}
