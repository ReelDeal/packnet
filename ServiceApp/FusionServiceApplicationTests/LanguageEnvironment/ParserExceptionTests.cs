﻿namespace FusionServiceApplicationTests.LanguageEnvironment
{
    using System.Collections.Generic;

    using FusionServiceApplication.LanguageEnvironment;
    using FusionServiceApplication.LanguageEnvironment.Exceptions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using TestUtils;

    [TestClass]
    [DeploymentItem(@"LanguageEnvironment\TestFiles\", "TestFiles")]
    public class ParserExceptionTests
    {
        private Parser parser;

        private Tokenizer tokenizer;

        [TestInitialize]
        public void TestSetup()
        {
            tokenizer = new Tokenizer();
            parser = new Parser();
        }

        [TestMethod]
        public void TestNoExecutionalCounterpart()
        {
            tokenizer.loadFile(@"./TestFiles/TestFile4.txt");
            AssertUtil.Throws<NoExecutionalCounterpartToDeclarationException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestMultipleMachineDeclarations()
        {
            tokenizer.loadFile(@"./TestFiles/MultipleMachineDeclarations.txt");

            AssertUtil.Throws<MultipleMachineDeclarationsException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestMultipleLoggingDeclarations()
        {
            tokenizer.loadFile(@"./TestFiles/MultipleLoggingDeclarations.txt");

            AssertUtil.Throws<MultipleLoggingDeclarationsException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestNoExecutionalBlocks()
        {
            tokenizer.loadFile(@"./TestFiles/NoExecutionalBlocks.txt");

            AssertUtil.Throws<NoExecutionalBlocksInFileException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestNoDeclarationBlocks()
        {
            tokenizer.loadFile(@"./TestFiles/NoDeclarationBlocks.txt");

            AssertUtil.Throws<NoDeclarationBlocksInFileException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestNoDeclarationSegment()
        {
            tokenizer.loadFile(@"./TestFiles/NoDeclarationSegment.txt");
            AssertUtil.Throws<NoDeclarationSegmentInFileException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestBlockCountMissmatch()
        {
            tokenizer.loadFile(@"./TestFiles/BlockCountMissmatchDeclaration.txt");
            AssertUtil.Throws<BlockCountMissmatchException>(
                () => parser.parseTokens(tokenizer.getTokens()));

            tokenizer.loadFile(@"./TestFiles/BlockCountMissmatchPackage.txt");
            AssertUtil.Throws<BlockCountMissmatchException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestNoExecutionalCounterpartToDeclarationException()
        {
            tokenizer.loadFile(@"./TestFiles/NoExecutionalCounterpartToDeclaration.txt");
            AssertUtil.Throws<NoExecutionalCounterpartToDeclarationException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestCouldNotFindLongheadSolutionException()
        {
            tokenizer.loadFile(@"./TestFiles/NoLongheadSolution.txt");

            parser.parseTokens(tokenizer.getTokens());
            MachineInfo.Instance.CurrentLongheadPositions = new List<double> { 100, 200, 300, 400, 500, 600, 700 };

            AssertUtil.Throws<CouldNotFindLongheadMovementSolutionException>(
                () => parser.getNextInstructionList());
        }

        [TestMethod]
        public void TestCouldNotParseTokenException()
        {
            tokenizer.loadFile(@"./TestFiles/CouldNotParseToken.txt");

            AssertUtil.Throws<CouldNotParseTokenException>(
                () => parser.parseTokens(tokenizer.getTokens()));
        }

        [TestMethod]
        public void TestNoFileContentsException()
        {
            AssertUtil.Throws<NoFileContentsException>(
                () => tokenizer.loadFile(@"./TestFiles/NoFileContents.txt"));
        }

        [TestMethod]
        public void TestToManyArgumentsException()
        {
            tokenizer.loadFile(@"./TestFiles/ToManyArguments.txt");

            parser.parseTokens(tokenizer.getTokens());
            MachineInfo.Instance.CurrentLongheadPositions = new List<double> { 100, 200, 300, 400, 500, 600, 700 };

             Assert.AreEqual(8, parser.ExecutionBlocks.Count);

            var currentName = parser.CurrentPackageName;
            AssertUtil.Throws<ToManyArgumentsException>(() => parser.getNextInstructionList());

             Assert.AreNotEqual(currentName, parser.CurrentPackageName);
            currentName = parser.CurrentPackageName;
            AssertUtil.Throws<ToManyArgumentsException>( () => parser.getNextInstructionList());


             Assert.AreNotEqual(currentName, parser.CurrentPackageName);
            currentName = parser.CurrentPackageName;
            AssertUtil.Throws<ToManyArgumentsException>( () => parser.getNextInstructionList());

             Assert.AreNotEqual(currentName, parser.CurrentPackageName);
            currentName = parser.CurrentPackageName;
            AssertUtil.Throws<ToManyArgumentsException>( () => parser.getNextInstructionList());

             Assert.AreNotEqual(currentName, parser.CurrentPackageName);
            currentName = parser.CurrentPackageName;
            AssertUtil.Throws<ToManyArgumentsException>( () => parser.getNextInstructionList());

             Assert.AreNotEqual(currentName, parser.CurrentPackageName);
            currentName = parser.CurrentPackageName;
            AssertUtil.Throws<ToManyArgumentsException>( () => parser.getNextInstructionList());

             Assert.AreNotEqual(currentName, parser.CurrentPackageName);
            currentName = parser.CurrentPackageName;
            AssertUtil.Throws<ToManyArgumentsException>( () => parser.getNextInstructionList());

             Assert.AreNotEqual(currentName, parser.CurrentPackageName);
            AssertUtil.Throws<ToManyArgumentsException>(() => parser.getNextInstructionList());
            
        }

        private void LoadFile4()
        {
            tokenizer.loadFile(@"./TestFiles/TestFile4.txt");
        }
    }
}
