﻿namespace FusionServiceApplicationTests.LanguageEnvironment
{
    using System.Collections.Generic;
    using System.Linq;

    using FusionServiceApplication.LanguageEnvironment;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [DeploymentItem("LanguageEnvironment\\TestFiles\\TestFile1.txt", "TestFiles")]
    [DeploymentItem("LanguageEnvironment\\TestFiles\\TestFile2.txt", "TestFiles")]
    [DeploymentItem("LanguageEnvironment\\TestFiles\\TestFile3.txt", "TestFiles")]
    public class ParserTests
    {
        private Parser parser;

        private Tokenizer tokenizer;

        [TestInitialize]
        public void Setup()
        {
            parser = new Parser(); 
            tokenizer = new Tokenizer();
            MachineInfo.Instance.CurrentLongheadPositions = new List<double> { 100, 300, 500, 700, 900, 1100, 1300 };
        }

        [TestMethod]
        public void TestParseTokens()
        {
            loadTestFile1();

            parser.parseTokens(tokenizer.getTokens());
        }

        [TestMethod]
        public void TestParseDeclarationBlocks()
        {
            loadTestFile1();

            parser.parseTokens(tokenizer.getTokens());

            Assert.IsTrue(parser.LoggingBlock.Any());
            Assert.IsTrue(parser.MachineInfoBlock.Any());

            Assert.AreEqual(2, parser.LoggingBlock.Count);
            Assert.AreEqual(5, parser.MachineInfoBlock.Count);

            Assert.AreEqual(2, parser.DeclarationBlocks.Count);

            Assert.AreEqual("1", parser.DeclarationBlocks[0][0].Arguments[0]);
            Assert.AreEqual("2", parser.DeclarationBlocks[1][0].Arguments[0]);
            
        }

        [TestMethod]
        public void TestParseExecutionalBlocks()
        {
            loadTestFile1();

            parser.parseTokens(tokenizer.getTokens());

            Assert.AreEqual(2, parser.ExecutionBlocks.Count);

            Assert.AreEqual("1", parser.ExecutionBlocks[0][0].Arguments[0]);
            Assert.AreEqual("2", parser.ExecutionBlocks[1][0].Arguments[0]);
        }

        [TestMethod]
        public void TestGetInstructionList()
        {
            loadTestFile1();

            parser.parseTokens(tokenizer.getTokens());

            var instructionList = parser.getNextInstructionList();

            Assert.IsNotNull(instructionList);
            Assert.IsTrue(instructionList.Any());

            var instructionList2 = parser.getNextInstructionList();

            Assert.IsNotNull(instructionList2);
            Assert.IsTrue(instructionList2.Any());
            Assert.AreNotEqual(instructionList, instructionList2);

            var instructionList3 = parser.getNextInstructionList();

            CollectionAssert.AreEqual(instructionList, instructionList3);

            var instructionList4 = parser.getNextInstructionList();

            CollectionAssert.AreEqual(instructionList2, instructionList4);
        }

        [TestMethod]
        public void TestInstructionList()
        {
            loadTestFile3();

            parser.parseTokens(tokenizer.getTokens());

            var instructionList = parser.getNextInstructionList();

            Assert.IsNotNull(instructionList);
            Assert.IsTrue(instructionList.Any());
            Assert.AreEqual(102, instructionList.Count());

            Assert.AreEqual("32000", instructionList.Last());
            Assert.AreNotEqual("32000", instructionList[instructionList.Count() - 2]);

            var comparisonList = new[]
                                     {
                                         "31000", "5", "15", "0", "1", "0", "31000", "2", "1", "1", "0", "2", "2", "0",
                                         "3", "3", "0", "4", "4", "0", "5", "5", "0", "6", "6", "0", "31000", "3", "3",
                                         "31000", "4", "1", "5000", "31000", "1", "0", "0", "1", "5000", "31000", "3", "0",
                                         "31000", "1", "0", "0", "1", "5000", "31000", "3", "3", "31000", "1", "0", "0",
                                         "1", "5000", "31000", "3", "3", "31000", "4", "1", "5000", "31000", "1", "0", "0",
                                         "1", "5000", "31000", "3", "0", "31000", "1", "0", "0", "1", "5000", "31000", "3",
                                         "3", "31000", "1", "0", "0", "1", "5000", "31000", "3", "0", "31000", "4", "0",
                                         "0", "31000", "1", "0", "1", "0", "0", "32000"
                                     };

            CollectionAssert.AreEqual(comparisonList, instructionList);
        }

        private void loadTestFile3()
        {
            tokenizer.loadFile(@"./TestFiles/TestFile3.txt");
        }

        private void loadTestFile2()
        {
            tokenizer.loadFile(@"./TestFiles/TestFile2.txt");
        }

        private void loadTestFile1()
        {
            tokenizer.loadFile(@"./TestFiles/TestFile1.txt");
        }
    }
}
