﻿
namespace FusionServiceApplicationTests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using FusionServiceApplication.Utilities.InstructionListStuff;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class FusionServiceApplicationTests
    {
        [TestMethod]
        public void CheckFindLongHeadSolution()
        {
            var startPositions = new List<double>{70,140,210,280,350,420};
            var endPositions = new List<double>{1000, 1070, 1140, 1210, 1280, 1350};
            var solution = new List<Tuple<int,double>>();
            
            LongheadPositioningStuff.FindLongheadMovementSolution(startPositions, endPositions,Enumerable.Range(1,6).ToList(), solution, true);

            Assert.AreEqual(solution[0].Item1, 6);
            Assert.AreEqual(solution[1].Item1, 5);
            Assert.AreEqual(solution[2].Item1, 4);
            Assert.AreEqual(solution[3].Item1, 3);
            Assert.AreEqual(solution[4].Item1, 2);
            Assert.AreEqual(solution[5].Item1, 1);

            Assert.AreEqual(solution[0].Item2, 1350);
            Assert.AreEqual(solution[1].Item2, 1280);
            Assert.AreEqual(solution[2].Item2, 1210);
            Assert.AreEqual(solution[3].Item2, 1140);
            Assert.AreEqual(solution[4].Item2, 1070);
            Assert.AreEqual(solution[5].Item2, 1000);

            startPositions = new List<double> {500,570,640,710,780,850};
            endPositions = new List<double> {70, 140, 210, 1000, 1070, 1140};
            solution = new List<Tuple<int, double>>();

            LongheadPositioningStuff.FindLongheadMovementSolution(startPositions, endPositions, Enumerable.Range(1,6).ToList(),  solution, true);

            Assert.AreEqual(solution[0].Item1, 1);
            Assert.AreEqual(solution[1].Item1, 2);
            Assert.AreEqual(solution[2].Item1, 3);
            Assert.AreEqual(solution[3].Item1, 6);
            Assert.AreEqual(solution[4].Item1, 5);
            Assert.AreEqual(solution[5].Item1, 4);

            Assert.AreEqual(solution[0].Item2, 70);
            Assert.AreEqual(solution[1].Item2, 140);
            Assert.AreEqual(solution[2].Item2, 210);
            Assert.AreEqual(solution[3].Item2, 1140);
            Assert.AreEqual(solution[4].Item2, 1070);
            Assert.AreEqual(solution[5].Item2, 1000);
            

            startPositions = new List<double>{900,970,1040,1110,1180,1250};
            endPositions = new List<double>{70,140,210,280,350,420};
            solution = new List<Tuple<int, double>>();

            LongheadPositioningStuff.FindLongheadMovementSolution(startPositions, endPositions, Enumerable.Range(1, 6).ToList(), solution, true);


            Assert.AreEqual(solution[0].Item1, 1);
            Assert.AreEqual(solution[1].Item1, 2);
            Assert.AreEqual(solution[2].Item1, 3);
            Assert.AreEqual(solution[3].Item1, 4);
            Assert.AreEqual(solution[4].Item1, 5);
            Assert.AreEqual(solution[5].Item1, 6);

            Assert.AreEqual(solution[0].Item2, 70);
            Assert.AreEqual(solution[1].Item2, 140);
            Assert.AreEqual(solution[2].Item2, 210);
            Assert.AreEqual(solution[3].Item2, 280);
            Assert.AreEqual(solution[4].Item2, 350);
            Assert.AreEqual(solution[5].Item2, 420);



            startPositions = new List<double> { 100, 300, 500 , 700, 900, 1000};
            endPositions = new List<double> { 290 , 400, 510, 890, 950, 1200};
            solution = new List<Tuple<int, double>>();

            LongheadPositioningStuff.FindLongheadMovementSolution(startPositions, endPositions,Enumerable.Range(1,6).ToList(),
                                                                      solution, true);

            Assert.AreEqual(solution[0].Item1, 2);
            Assert.AreEqual(solution[1].Item1, 1);
            Assert.AreEqual(solution[2].Item1, 3);
            Assert.AreEqual(solution[3].Item1, 5);
            Assert.AreEqual(solution[4].Item1, 4);
            Assert.AreEqual(solution[5].Item1, 6);

            Assert.AreEqual(solution[0].Item2, 400);
            Assert.AreEqual(solution[1].Item2, 290);
            Assert.AreEqual(solution[2].Item2, 510);
            Assert.AreEqual(solution[3].Item2, 950);
            Assert.AreEqual(solution[4].Item2, 890);
            Assert.AreEqual(solution[5].Item2, 1200);

        }


        [TestMethod]
        public void TestGetInstructionListForCustomInstructionList()
        {
        }
    }
}
