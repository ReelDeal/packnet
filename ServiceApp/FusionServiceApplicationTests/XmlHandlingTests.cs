﻿using PackNet.Communication;
using PackNet.Communication.Fusion;
using PackNet.Communication.PLCBase;

namespace FusionServiceApplicationTests
{
    using System;
    using System.Globalization;
    using System.IO;
    using System.Threading;
    using System.Xml;

    using FusionServiceApplication.Helpers;
    using FusionServiceApplication.SettingHandlers;
    using FusionServiceApplication.SettingHandlers.MachineConfig;
    using FusionServiceApplication.Utilities.Communication;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    [TestClass]
    public class XmlHandlingTests
    {
        [TestMethod]
        [DeploymentItem(@"machine.xml")]
        public void TestWriteToXml()
        {
            var generator = new Random();

            double[] sensorCompensationLeft1 = { generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble() };
            double[] sensorCompensationRight1 = { generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble() };
            double[] sensorCompensationLeft2 = { generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble() };
            double[] sensorCompensationRight2 = { generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble(), generator.NextDouble() };

            MachineXmlHandler.WriteToMachineXml(sensorCompensationLeft1, sensorCompensationLeft2, sensorCompensationRight1, sensorCompensationRight2, null);

            Thread.Sleep(5000);

            var doc = new XmlDocument();

            doc.Load(Path.Combine(Directory.GetCurrentDirectory(), "machine.xml"));

            {
                //check sword sensor positionsvar 

                var tracks = doc.SelectNodes("machine/tracks/track");
                var track1 = (tracks.Item(0).Attributes.GetNamedItem("number").Value == "1")
                             ? tracks.Item(0)
                             : tracks.Item(1);
                var track2 = (track1 == tracks.Item(0)) ? tracks.Item(1)
                           : tracks.Item(0);

                { // track1
                    var leftSensorCompensation = track1.SelectSingleNode("leftswordsensorCompensations");
                    var rightSensorCompensation = track1.SelectSingleNode("rightswordsensorCompensations");

                    var leftSensorCompensationCompensationNodes = leftSensorCompensation.SelectNodes("compensation");
                    var rightSensorCompensationCompensationNodes = rightSensorCompensation.SelectNodes("compensation");

                    for (var i = 0; i < 10; i++)
                    {
                        var compensationNodeL = leftSensorCompensationCompensationNodes.Item(i);
                        var compensationNodeR = rightSensorCompensationCompensationNodes.Item(i);

                        Assert.AreEqual(sensorCompensationLeft1[i].ToString(CultureInfo.InvariantCulture), compensationNodeL.InnerText);
                        Assert.AreEqual(sensorCompensationRight1[i].ToString(CultureInfo.InvariantCulture), compensationNodeR.InnerText);
                    }
                }

                { //track2
                    
                    var leftSensorCompensation = track2.SelectSingleNode("leftswordsensorCompensations");
                    var rightSensorCompensation = track2.SelectSingleNode("rightswordsensorCompensations");

                    var leftSensorCompensationCompensationNodes = leftSensorCompensation.SelectNodes("compensation");
                    var rightSensorCompensationCompensationNodes = rightSensorCompensation.SelectNodes("compensation");

                    for (var i = 0; i < 10; i++)
                    {
                        var compensationNodeL = leftSensorCompensationCompensationNodes.Item(i);
                        var compensationNodeR = rightSensorCompensationCompensationNodes.Item(i);

                        Assert.AreEqual(sensorCompensationLeft2[i].ToString(CultureInfo.InvariantCulture), compensationNodeL.InnerText);
                        Assert.AreEqual(sensorCompensationRight2[i].ToString(CultureInfo.InvariantCulture), compensationNodeR.InnerText);
                    }
                }
            }

        }

        [TestMethod]
        [DeploymentItem(@"machine.xml")]
        public void TestSyncMachine()
        {
            var map = new IqFusionMachineVariables();
            var communicationMoq = new Mock<IIqFusionCommunicator>();

            PhyscialMachineSettingsHelper.SyncMachine(communicationMoq.Object);

     //       VerifyCrossheadSync(communicationMoq, map);
     //       VerifyRollerSync(communicationMoq, map);
        //    VerifyRollerDiscPositions(communicationMoq, map);
            VerifyEventNotifierSync(communicationMoq);
        }

        private static void VerifyEventNotifierSync(Mock<IIqFusionCommunicator> communicationMoq)
        {
            communicationMoq.VerifySet(x => x.EventNotifierAdress = "10.1.1.1");
            communicationMoq.VerifySet(x => x.EventNotifierPort = "9998");
        }

        private static void VerifyRollerDiscPositions(Mock<IIqFusionCommunicator> communicationMoq, IPackagingMachineVariableMap map)
        {
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.RightRollerDisconnectPosition.VariableName)), "55.81"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.LeftRollerDisconnectPosition.VariableName)), "-1.14"), Times.Once());     
        }

        private static void VerifyRollerSync(Mock<IIqFusionCommunicator> communicationMoq, IPackagingMachineVariableMap map)
        {
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.FeedRollerAccelerationInPercent.VariableName)), "25"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.FeedRollerCleanCutLength.VariableName)), "2.362"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.FeedRollerGearRatio.VariableName)), "1.0"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.FeedRollerLoweringOffset.VariableName)), "3.543"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.FeedRollerOutFeedLength.VariableName)), "1.378"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.FeedRollerSpeedInPercent.VariableName)), "66"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.RollAxisScaling.VariableName)), "309"), Times.Once());
        }

        private static void VerifyCrossheadSync(Mock<IIqFusionCommunicator> communicationMoq, IPackagingMachineVariableMap map)
        {
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadAccelerationInPercent.VariableName)), "100"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadHomingPosition.VariableName)), "54.71"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadMaximumAcceleration.VariableName)), 39300), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadMaximumDeceleration.VariableName)), 39300), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadMaximumPosition.VariableName)), "54.409"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadMinimumPosition.VariableName)), "0.2"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadMaximumSpeed.VariableName)), 7000), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadTrackSensorToTool.VariableName)), "0.079"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadSpeedInPercent.VariableName)), "100"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.ToolAxisScaling.VariableName)), "295"), Times.Once());
            communicationMoq.Verify(x => x.WriteValue(It.Is<PacksizePlcVariable>(y => y.VariableName.Contains(map.CrossHeadSensorToKnife.VariableName)), "-0.630"), Times.Once());
        }

        [TestMethod]
        [DeploymentItem(@"XmlFiles\", "XmlFiles")]
        public void TestSerializedConfig()
        {
            Assert.AreEqual(CoreSettings.Instance.EventNotifierAdress, "10.1.1.1");
            Assert.AreEqual(CoreSettings.Instance.PhysicalMachineSettingsPath, @".\XmlFiles\PhysicalMachineSettings\machine3.xml");
            Assert.AreEqual(CoreSettings.Instance.EventNotifierPort, "9998");
        }

        [TestMethod]
        [Ignore]
        public void TestSerializedCrosshead()
        {
            Assert.AreEqual(Machine.Instance.Crosshead.HomingPosition, 54.70);
            Assert.AreEqual(Machine.Instance.Crosshead.LongheadSensorToTool, -0.630);
            Assert.AreEqual(Machine.Instance.Crosshead.Scaling, 295);
            Assert.AreEqual(Machine.Instance.Crosshead.CrossheadSpeedInPercent, 100);
            Assert.AreEqual(Machine.Instance.Crosshead.CrossheadMinimumPosition, 0);
            Assert.AreEqual(Machine.Instance.Crosshead.CrossheadMaximumSpeedInUnits, 70);
            Assert.AreEqual(Machine.Instance.Crosshead.CrossheadMaximumPosition, 54.65);
            Assert.AreEqual(Machine.Instance.Crosshead.CrossheadMaximumDecelerationInUnits, 393);
            Assert.AreEqual(Machine.Instance.Crosshead.CrossheadMaximumAccelerationInUnits, 393);
            Assert.AreEqual(Machine.Instance.Crosshead.CrossheadAccelerationInPercent, 100);
        }

        [TestMethod]
        [Ignore]
        public void TestSerializedFeedRoller()
        {
            Assert.AreEqual(Machine.Instance.FeedRoller.CleanCutLength, 4.0);
            Assert.AreEqual(Machine.Instance.FeedRoller.LoweringOffset, 3.6);
            Assert.AreEqual(Machine.Instance.FeedRoller.OutfeedLength, 1.58);
            Assert.AreEqual(Machine.Instance.FeedRoller.RollerAccelerationInPercent, 25);
            Assert.AreEqual(Machine.Instance.FeedRoller.Scaling, 309);
            Assert.AreEqual(Machine.Instance.FeedRoller.SpeedInPercent, 66);
            Assert.AreEqual(Machine.Instance.FeedRoller.ToolsGearRatio, 1.0);
        }

        [TestMethod]
        [Ignore]
        public void TestSerializedTracks()
        {
            TestTrackOneCompensations();
            TestTrackTwoCompensations();
        }

        private static void TestTrackOneCompensations()
        {
            var value = "0.230";
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[0].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[1].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[2].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[3].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[4].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[5].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[6].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[7].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[8].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].LeftswordsensorCompensations.Compensation[9].Value, value);

            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[0].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[1].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[2].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[3].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[4].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[5].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[6].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[7].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[8].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[0].RightswordsensorCompensations.Compensation[9].Value, value);
        }

        private static void TestTrackTwoCompensations()
        {
            var value = "0.230";
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[0].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[1].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[2].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[3].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[4].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[5].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[6].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[7].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[8].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].LeftswordsensorCompensations.Compensation[9].Value, value);

            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[0].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[1].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[2].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[3].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[4].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[5].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[6].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[7].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[8].Value, value);
            Assert.AreEqual(Machine.Instance.Tracks.Track[1].RightswordsensorCompensations.Compensation[9].Value, value);
        }
    }
}
