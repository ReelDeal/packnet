﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Net;

using ExternalZebraPrintIntegration.L10N;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.ExternalSystems;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ExternalSystems;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Network;

namespace ExternalZebraPrintIntegration
{
    public class Connection
    {
        public Connection()
        {
            SequenceNumber = 1;
            SyncObject = new object();
        }

        public object SyncObject { get; private set; }
        public int SequenceNumber { get; set; }
        public SocketCommunicator Socket { get; set; }
    }

    [Export(typeof(IService))]
    public class ExternalZebraPrintIntegrationService : IExternalZebraPrintIntegrationService
    {
        private readonly ILogger logger;
        private const char STX = '\x0002';
        private const char ETX = '\x0003';
        private const int MIN_SEQUENCE_NUMBER = 1;
        private const int MAX_SEQUENCE_NUMBER = 99999;
        private readonly string ACK = STX + "{0}ACK" + ETX;
        private readonly string PIPEDACK = STX + "{0}|ACK|" + ETX;

        private readonly string NACK = STX + "{0}NACK" + ETX;
        private readonly string PIPEDNACK = STX + "{0}|NACK|" + ETX;

        private object requestNumberLock = new object();
        //[SequenceNumber]KPA
        private readonly string KEEP_ALIVE_MESSAGE = STX + "{0}KPA" + ETX;

        //[SequenceNumber]RLP;[SerialNumber];[Length];[Width];[Height];[PrinterId];[ProduceCount]
        private string REQUEST_LABEL_PRINT_MESSAGE
        {
            get
            {
                return STX + requestLabelPrintMessage + ETX;
            }
        }

        public string RequestLabelPrintMessage
        {
            get { return requestLabelPrintMessage; }
            set
            {
                requestLabelPrintMessage = value;
            }
        }

        //[SequenceNumber]RCP;[SerialNumber];[Length];[Width];[Height];[ProduceCount]
        private string CARTON_PRODUCED_MESSAGE
        {
            get
            {
                return STX + cartonProducedMessage + ETX;
            }
        }

        public string CartonProducedMessage
        {
            get { return cartonProducedMessage; }
            set { cartonProducedMessage = value; }
        }

        private TimeSpan KeepAliveInterval = TimeSpan.FromSeconds(5);

        private ConcurrentDictionary<Guid, Tuple<ExternalCustomerSystem, Connection>> connections = new ConcurrentDictionary<Guid, Tuple<ExternalCustomerSystem, Connection>>();
        private IAggregateMachineService machineService;
        private string requestLabelPrintMessage = "{0}RLP;{1};{2};{3};{4};{5};{6}";
        private string cartonProducedMessage = "{0}RCP;{1};{2};{3};{4};{5}";
        private IUserNotificationService notificationService;
        private IMachineGroupService machineGroupService;

        [ImportingConstructor]
        public ExternalZebraPrintIntegrationService(IServiceLocator serviceLocator, ILogger logger)
        {
            this.logger = logger;
            serviceLocator.RegisterAsService(this);
            machineService = serviceLocator.Locate<IAggregateMachineService>();
            notificationService = serviceLocator.Locate<IUserNotificationService>();
            machineGroupService = serviceLocator.Locate<IMachineGroupService>();
        }

        public void SendLabelRequest(ExternalCustomerSystem externalCustomerSystem, IPrintable printable)
        {
            try
            {
                logger.Log(
                    LogLevel.Info,
                    Strings.ExternalZebraPrintIntegrationService_SendLabelRequest_Handling_external_print_request_for_producible_
                    + printable);
                var restriction = printable.Restrictions.FirstOrDefault(r => r is LabelForCartonRestriction);
                if (restriction == null)
                {
                    logger.Log(
                        LogLevel.Warning,
                        "Printable " + printable.CustomerUniqueId
                        + " did not contain LabelForCartonRestriction, RLP can not be sent to WMS");
                    return;
                }

                var printer = machineService.FindById(printable.ProduceOnMachineId);

                var connectionInfo = GetConnection(externalCustomerSystem);
                var connection = connectionInfo.Item2;


                var sequenceNumberString = GetSequenceNumber(connection);
                var carton = ((LabelForCartonRestriction)restriction).Value;


                var reproducedCount = printable.History.Count(ph => ph.NewStatus == ProducibleStatuses.ProducibleCompleted);

                //[SequenceNumber]RLP;[SerialNumber];[Length];[Width];[Height];[PrinterId];[ProduceCount]
                var message = String.Format(
                    REQUEST_LABEL_PRINT_MESSAGE,
                    sequenceNumberString,
                    carton.CustomerUniqueId,
                    carton.Length,
                    carton.Width,
                    carton.Height,
                    printer.Alias,
                    reproducedCount + 1);


                var response = string.Empty;
                lock (connection.SyncObject)
                {

                    logger.Log(
                        LogLevel.Info,
                        "Sending RLP Request to WMS (" + connection.Socket.EndPoint.Address + "): " + message);
                    var sw = Stopwatch.StartNew();
                    connection.Socket.Send(message, 1);
                    response = connection.Socket.ReceiveUntilDelimiter(ETX);

                    logger.Log(
                        LogLevel.Info,
                        "Sent RLP Request (" + sw.ElapsedMilliseconds + " ms) to WMS (" + connection.Socket.EndPoint.Address + "): " + message);
                }
                if (response == String.Format(ACK, sequenceNumberString) || response == String.Format(PIPEDACK, sequenceNumberString))
                {
                    //much success

                    printable.ProducibleStatus = ExternalPrintProducibleStatuses.LabelRequested;
                }
                else if (response == String.Format(NACK, sequenceNumberString) || response == String.Format(PIPEDNACK, sequenceNumberString))
                {
                    var machineGroup = machineGroupService.FindByMachineId(printable.ProduceOnMachineId);
                    printable.ProducibleStatus = ExternalPrintProducibleFailedStatus.LabelRequestFailed;
                    var msg = "Label Request Failed: Failed to send command (received NACK) to WMS (" + connection.Socket.EndPoint.Address + "): " + message;
                    notificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, msg, "", machineGroup.Id);
                    throw new Exception(msg);
                }
                else
                {
                    //Unexpected response
                    var machineGroup = machineGroupService.FindByMachineId(printable.ProduceOnMachineId);
                    printable.ProducibleStatus = ExternalPrintProducibleFailedStatus.LabelRequestFailed;
                    var msg = "Label Request Failed: Failed to send command to WMS (" + connection.Socket.EndPoint.Address + "): " + message + " response: "
                        + response;
                    notificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, msg, "", machineGroup.Id);
                    throw new Exception(msg);
                }
            }

            catch (Exception e)
            {
                var machineGroup = machineGroupService.FindByMachineId(printable.ProduceOnMachineId);
                printable.ProducibleStatus = ExternalPrintProducibleFailedStatus.LabelRequestFailed;
                var msg = "Label Request Failed: Failed to send label request to WMS (" + externalCustomerSystem.IpOrDnsName + "): " + e.Message;
                logger.Log(LogLevel.Error, msg);
                notificationService.SendNotificationToMachineGroup(NotificationSeverity.Error, msg, "", machineGroup.Id);

                throw;
            }
        }

        public void SendProductionComplete(ExternalCustomerSystem externalCustomerSystem, IProducible producible)
        {
            var connectionInfo = GetConnection(externalCustomerSystem);
            var connection = connectionInfo.Item2;
            var sequenceNumberString = GetSequenceNumber(connection);
            var carton = producible as ICarton;

            if (carton == null)
            {
                logger.Log(LogLevel.Warning, "Producible " + producible.CustomerUniqueId + " is not an ICarton, carton production data can not be sent to WMS");
                return;
            }

            var reproducedCount = producible.History.Count(ph => ph.NewStatus == ProducibleStatuses.ProducibleCompleted);

            //[SequenceNumber]RCP;[SerialNumber];[Length];[Width];[Height];[NextLabelRequestNumber]
            var message = String.Format(CARTON_PRODUCED_MESSAGE, sequenceNumberString, carton.CustomerUniqueId, carton.Length, carton.Width, carton.Height, reproducedCount + 1);
            lock (connection.SyncObject)
            {
                logger.Log(LogLevel.Info, "Sending RCP Request: " + message);
                var sw = Stopwatch.StartNew();
                connection.Socket.Send(message, 1);
                var response = connection.Socket.ReceiveLines();
                logger.Log(LogLevel.Info, "Sent RCP Request (" + sw.ElapsedMilliseconds + " ms): " + message);
                if (response == String.Format(ACK, sequenceNumberString) || response == String.Format(PIPEDACK, sequenceNumberString))
                {
                    //much success
                }
                else if (response == String.Format(NACK, sequenceNumberString) || response == String.Format(PIPEDNACK, sequenceNumberString))
                {
                    throw new Exception("Failed to send command (received NACK) " + message);
                }
                else
                {
                    //Unexpected response
                    throw new Exception("Failed to send command " + message);
                }
            }

        }

        private Tuple<ExternalCustomerSystem, Connection> GetConnection(ExternalCustomerSystem externalCustomerSystem)
        {
            var tuple = connections.GetOrAdd(externalCustomerSystem.Id, system =>
            {
                var endpoint = new IPEndPoint(IPAddress.Parse(externalCustomerSystem.IpOrDnsName), externalCustomerSystem.Port);
                var connection = new Connection();
                connection.Socket = new SocketCommunicatorWithHeartBeat(endpoint, logger, 1000, 1000, Convert.ToInt32(KeepAliveInterval.TotalMilliseconds),
                    (socket => HeartBeat(connection)));
                logger.Log(LogLevel.Debug, "connection to external system " + externalCustomerSystem.Id + " created");
                return new Tuple<ExternalCustomerSystem, Connection>(externalCustomerSystem, connection);
            });
            if (externalCustomerSystem.IpOrDnsName != tuple.Item2.Socket.EndPoint.Address.ToString() &&
                externalCustomerSystem.Port != tuple.Item2.Socket.EndPoint.Port)
            {
                logger.Log(LogLevel.Debug, "Connection to external system " + externalCustomerSystem.Id + " changed, re-establishing connection");
                Tuple<ExternalCustomerSystem, Connection> junk;
                connections.TryRemove(externalCustomerSystem.Id, out junk);
                return GetConnection(externalCustomerSystem);
            }
            return tuple;
        }

        private bool HeartBeat(Connection connection)
        {
            lock (connection.SyncObject)
            {
                try
                {
                    var sequenceNumberString = GetSequenceNumber(connection);
                    var sw = Stopwatch.StartNew();
                    var result = connection.Socket.SendReceiveUntilDelimiter(GetKeepAliveMessage(sequenceNumberString), ETX);
                    if (result == String.Format(ACK, sequenceNumberString) || result == String.Format(PIPEDACK, sequenceNumberString))
                    {
                        logger.Log(LogLevel.Trace, "Successful heartbeat (" + sw.ElapsedMilliseconds + " ms) from WMS (" + connection.Socket.EndPoint.Address + ") for sequence number " + sequenceNumberString);
                        return true;
                    }
                    logger.Log(LogLevel.Trace, "Failed heartbeat (" + sw.ElapsedMilliseconds + " ms) from WMS (" + connection.Socket.EndPoint.Address + ") for sequence number " + sequenceNumberString);
                    return false;
                }
                catch (Exception e)
                {
                    logger.Log(LogLevel.Warning, "Failed to contact WMS (" + connection.Socket.EndPoint.Address + "): " + e);
                    return false;
                }
            }
        }

        private string GetKeepAliveMessage(string sequenceNumber)
        {
            return String.Format(KEEP_ALIVE_MESSAGE, sequenceNumber);
        }

        /// <summary>
        /// Returns string representing the sequence number and increments the current counter 
        /// </summary>
        /// <returns></returns>
        private string GetSequenceNumber(Connection connection)
        {
            if (connection.SequenceNumber > MAX_SEQUENCE_NUMBER)
                connection.SequenceNumber = MIN_SEQUENCE_NUMBER;
            var ret = connection.SequenceNumber.ToString("00000");
            connection.SequenceNumber++;
            return ret;
        }

        public void Dispose()
        {
        }

        public string Name { get { return "StaplesExternalPrintService"; } }
    }
}
