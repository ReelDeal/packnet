﻿using PackNet.Common.Interfaces.DTO.ExternalSystems;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace ExternalZebraPrintIntegration
{
    public interface IExternalZebraPrintIntegrationService : IService
    {
        void SendLabelRequest(ExternalCustomerSystem externalCustomerSystem, IPrintable printable);

        void SendProductionComplete(ExternalCustomerSystem externalCustomerSystem, IProducible producible);

        string RequestLabelPrintMessage { get; set; }
        string CartonProducedMessage { get; set; }
    }
}