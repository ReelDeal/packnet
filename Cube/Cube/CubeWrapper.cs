﻿using PackNet.Common.Interfaces.Logging;
using PackNet.Cube.L10N;

namespace PackNet.Cube
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Serialization;

    using CubingClassLibrary;

    using DTO;
    using Enums;

    /// <summary>
    /// A unit testable wrapper around Fastfetch CubingClass
    /// </summary>
    public class CubeWrapper
    {
        private readonly ILogger logger;

        private readonly CubingClass cubingClass;
        /// <summary>
        /// The quality and speed of determining a set of boxes can be adjusted.  
        /// The value of the speed control ranges from 1 to 6, indicating slow to fast speed and good to best quality respectively
        /// </summary>
        public CubeSpeed SpeedControl
        {
            get
            {
                return (CubeSpeed)Enum.Parse(typeof(CubeSpeed), cubingClass.MaxVBMethods.ToString(CultureInfo.InvariantCulture));
            }
            set
            {
                cubingClass.MaxVBMethods = (int)value;
                logger.Log(LogLevel.Trace, String.Format(Strings.CubeWrapper_SpeedControl_Speed_Control_set_to___0___1___, value, (int)value));
            }
        }
        /// <summary>
        /// Best fit – assumes items can be rearranged after picking to reduce the occupied space in each box. <b/>
        /// Pick sequence – assumes items are placed into the shipping box in the order they are picked and will not be rearranged after placement to reduce the occupied space.
        /// </summary>
        public CubeMethod CubeMethod
        {
            get
            {
                return (CubeMethod)Enum.Parse(typeof(CubeMethod), cubingClass.CubeMethod);
            }
            set
            {
                cubingClass.CubeMethod = value.ToString();
                logger.Log(LogLevel.Trace, String.Format(Strings.CubeWrapper_CubeMethod_Cube_Method_set_to___0__, cubingClass.CubeMethod));
            }
        }
        /// <summary>
        /// Is an optional value to be added to the maximum dimension of some order item to control the box sizes used for selection 
        /// </summary>
        public float DeltaValue
        {
            get
            {
                return ModGlobalWrapper.DeltaValue;
            }
            set
            {
                ModGlobalWrapper.DeltaValue = value;
                logger.Log(LogLevel.Trace, String.Format(Strings.CubeWrapper_DeltaValue_Delta_Value_set_to___0__, ModGlobalWrapper.DeltaValue));
            }
        }
        /// <summary>
        /// Is an optional value specifying the maximum dimension of all boxes to be used for selection
        /// </summary>
        public float MaxDimension
        {
            get
            {
                return ModGlobalWrapper.MaxDimension;
            }
            set
            {
                ModGlobalWrapper.MaxDimension = value;
                logger.Log(LogLevel.Trace, String.Format(Strings.CubeWrapper_MaxDimension_Max_Dimension_set_to___0__, ModGlobalWrapper.MaxDimension));
            }
        }
        /// <summary>
        /// No Documentation Available
        /// </summary>
        public bool AuxileryField1Exists
        {
            get
            {
                return ModGlobalWrapper.AuxileryField1Exists;
            }
            set
            {
                ModGlobalWrapper.AuxileryField1Exists = value;
                logger.Log(LogLevel.Trace, String.Format(Strings.CubeWrapper_AuxileryField1Exists_Auxilery_Field_1_Exists_set_to___0__, ModGlobalWrapper.AuxileryField1Exists));
            }
        }
        /// <summary>
        /// No Documentation Available
        /// </summary>
        public bool AuxileryField2Exists
        {
            get
            {
                return ModGlobalWrapper.AuxileryField2Exists;
            }
            set
            {
                ModGlobalWrapper.AuxileryField2Exists = value;
                logger.Log(LogLevel.Trace, String.Format(Strings.CubeWrapper_AuxileryField2Exists_Auxilery_Field_2_Exists_set_to___0__, ModGlobalWrapper.AuxileryField2Exists));
            }
        }
        /// <summary>
        /// Result set of BoxListGenerator, or predefined Box List
        /// </summary>
        public List<BoxDefinition> BoxDefinitionList
        {
            get
            {
                return clsPublicLists.BoxDefinitionList.ToBoxDefinitions().ToList();
            }
            set
            {
                clsPublicLists.BoxDefinitionList = value.ToFastFetchBoxDefinitions().ToList();
                var stopWatch = Stopwatch.StartNew();
                var mi = typeof(clsPublicLists).GetMethod("SortBoxDefinitionList", BindingFlags.Static | BindingFlags.NonPublic);
                mi.Invoke(null, null);

                logger.Log(LogLevel.Info, String.Format(Strings.CubeWrapper_BoxDefinitionList_Sorting_Box_Definition_List_of_count__0__took__1__milliseconds, clsPublicLists.BoxDefinitionList.Count, stopWatch.ElapsedMilliseconds));
            }
        }
        /// <summary>
        /// Cube Algorithm Process
        /// </summary>
        /// <param name="logger">Logging Process for Debug and troubleshooting</param>
        public CubeWrapper(ILogger logger)
        {
            this.logger = logger;
            //Hacking the shit out of fast fetch by constructing without calling ctor... Use a decompiler to determine what needs to happen (resharper)
            cubingClass = (CubingClass)FormatterServices.GetUninitializedObject(typeof(CubingClass));
            cubingClass.ListLock = new object();
            cubingClass.DebugFlag = false;
            SpeedControl = CubeSpeed.Regular;
            CubeMethod = CubeMethod.BestFit;
            DeltaValue = 0.0f;
            MaxDimension = 999f;
            AuxileryField1Exists = false;
            AuxileryField2Exists = false;
            BoxDefinitionList = new List<BoxDefinition>();
        }

        /// <summary>
        /// Solve the box list
        /// </summary>
        /// <param name="orderItemList">List of Items to be Cubed</param>
        /// <returns></returns>
        public IEnumerable<CubingResult> Calculate(IEnumerable<OrderLineItem> orderItemList)
        {
            var stopWatch = Stopwatch.StartNew();
            var result = orderItemList.GroupBy(oi => oi.OrderId).Select(
                p =>
                {
                    var errors = "";
                    var order = new clsOrder(p.Key);
                    foreach (var orderItem in p)
                    {

                        order.AddOrderItem(ItemId: orderItem.Id,
                            TraversalSeq: "1",
                            Dimension1: orderItem.Length,
                            Dimension2: orderItem.Width,
                            Dimension3: orderItem.Height,
                            Weight: orderItem.Weight,
                            Quantity: orderItem.Quantity,
                            OrientationDimension1: orderItem.Orientation1,
                            OrientationDimension2: orderItem.Orientation2,
                            CanNest: orderItem.ItemCanBeNested,
                            NestingGrowthPercent: orderItem.NestingGrowthPercentage,
                            NestingLevelMaximum: orderItem.MaximumNumberOfNestedItems,
                            NestingAxis: orderItem.NestingAxis,
                            CanContain: orderItem.ItemCanContainOtherItems,
                            ContainDimX: orderItem.VoidFillDimentionX,
                            ContainDimY: orderItem.VoidFillDimentionY,
                            ContainDimZ: orderItem.VoidFillDimentionZ);

                    }
                    logger.Log(LogLevel.Info, String.Format(Strings.CubeWrapper_Calculate_Cubing_order__0__with__1__items__Method___2___Quality__3_, order.OrderID, p.Count(), CubeMethod.ToString(), SpeedControl.ToString()));

                    cubingClass.CubeOrder(ref order);
                    logger.Log(LogLevel.Debug, ToString());

                    if (!cubingClass.CubeSucceeded)
                    {
                        errors = cubingClass.GetSavedExceptions();
                        logger.Log(LogLevel.Error, String.Format(Strings.CubeWrapper_Calculate_Cubing_failed_for_order__0___Error___1_, order.OrderID, errors));
                    }
                    else
                    {
                        logger.Log(LogLevel.Info, String.Format(Strings.CubeWrapper_Calculate_Cubing_succeeded_for_order__0___Selected_boxes__1_,
                            order.OrderID, order.SelectedBoxList.Select(b => b.StringDimension).Aggregate((r, n) => r + ", " + n)));

                    }
                    logger.Log(LogLevel.Debug, String.Format(Strings.CubeWrapper_Calculate_Order___0___solved__Cartons__1_, order.OrderID, order.SelectedBoxList.Count));
                    foreach (var carton in order.SelectedBoxList)
                    {
                        logger.Log(LogLevel.Debug, String.Format(Strings.CubeWrapper_Calculate_Order___0____Carton__1__wasted_volume_percentage___2___, order.OrderID, carton.BoxID, carton.WastedVolumePercent.ToString("0.00")));
                    }

                    return new CubingResult { Order = order.ToCubeOrder(), Succeeded = cubingClass.CubeSucceeded, Errors = errors, AvailableBoxCount = BoxDefinitionList.Count};
                }).ToList();
            logger.Log(LogLevel.Info, String.Format(Strings.CubeWrapper_Calculate_Cubing_with_speed__0___box_list_count__1___completed_in__2__milliseconds, SpeedControl, BoxDefinitionList.Count(), stopWatch.ElapsedMilliseconds));
            return result;
        }

        /// <summary>
        /// String representation of the Cube Wrapper
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format(Strings.CubeWrapper_ToString_Cubing_Method___0___Speed_Control___1___Delta_Value___2___Max_Dimension___3___Box_List_Count__4_,
                CubeMethod, SpeedControl, DeltaValue, MaxDimension, BoxDefinitionList.Count);
        }
    }

    static class ModGlobalWrapper
    {
        private static Assembly assembly;
        private static Type type;

        static ModGlobalWrapper()
        {
            assembly = Assembly.GetAssembly(typeof(CubingClass));
            type = assembly.GetType("CubingClassLibrary.ModGlobal");
        }

        public static bool AuxileryField1Exists
        {
            get
            {
                var pi = type.GetProperty("bAuxField1IsAKey", BindingFlags.Static | BindingFlags.Public);
                var val = pi.GetValue(type, null);
                return val != null && (bool)val;
            }
            set
            {
                var pi = type.GetProperty("bAuxField1IsAKey", BindingFlags.Static | BindingFlags.Public);
                pi.SetValue(type, Convert.ChangeType(value, pi.PropertyType), null);
            }
        }

        public static bool AuxileryField2Exists
        {
            get
            {
                var pi = type.GetProperty("bAuxField2IsAKey", BindingFlags.Static | BindingFlags.Public);
                var val = pi.GetValue(type, null);
                return val != null && (bool)val;
            }
            set
            {
                var pi = type.GetProperty("bAuxField2IsAKey", BindingFlags.Static | BindingFlags.Public);
                pi.SetValue(type, Convert.ChangeType(value, pi.PropertyType), null);
            }
        }

        public static float MaxDimension
        {
            get
            {
                var f = type.GetField("MaxDim", BindingFlags.Static | BindingFlags.NonPublic);
                return (float)f.GetValue(type);
            }
            set
            {
                var f = type.GetField("MaxDim", BindingFlags.Static | BindingFlags.NonPublic);
                f.SetValue(type, value);
            }
        }

        public static float DeltaValue
        {
            get
            {
                var f = type.GetField("DeltaValue", BindingFlags.Static | BindingFlags.NonPublic);
                return (float)f.GetValue(type);
            }
            set
            {
                var f = type.GetField("DeltaValue", BindingFlags.Static | BindingFlags.NonPublic);
                f.SetValue(type, value);
            }
        }
    }
}
