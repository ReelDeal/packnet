﻿namespace PackNet.Cube.DTO
{
    /// <summary>
    /// Box representation used in a cube result
    /// </summary>
    public class SelectedBox
    {
        /// <summary>
        /// Unique Identifier for the box
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Definition of the selected box
        /// </summary>
        public BoxDefinition BoxDefinition { get; set; }

        /// <summary>
        /// Box type of the selected box
        /// </summary>
        public string BoxType { get; set; }

        /// <summary>
        /// Length of all contents
        /// </summary>
        public float ContentLength { get; set; }

        /// <summary>
        /// Width of all contents
        /// </summary>
        public float ContentWidth { get; set; }

        /// <summary>
        /// Height of all contents
        /// </summary>
        public float ContentHeight { get; set; }

        /// <summary>
        /// Volume of all contents
        /// </summary>
        public float ContentVolume { get; set; }

        /// <summary>
        /// Used Volume inside the Box
        /// </summary>
        public float UsedVolume { get; set; }

        /// <summary>
        /// Wasted Volume inside the Box
        /// </summary>
        public float WastedVolume { get; set; }

        /// <summary>
        /// Percent of Wasted Volume inside the Box
        /// </summary>
        public float WastedVolumePercent { get; set; }

        /// <summary>
        /// Total Weight of all items in the box
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// String representation of the box
        /// </summary>
        public string StringDimension
        {
            get { return BoxDefinition.StringDimension; }
        }
    }
}
