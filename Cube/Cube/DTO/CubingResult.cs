namespace PackNet.Cube.DTO
{
    /// <summary>
    /// The result of the Cubing process
    /// </summary>
    public class CubingResult
    {
        /// <summary>
        /// Order that was cubed
        /// </summary>
        public CubeOrder Order { get; set; }
        
        /// <summary>
        /// True if the Cubing completed successfully, false if it failed
        /// </summary>
        public bool Succeeded { get; set; }

        /// <summary>
        /// Any errors that may occur durring the cubing process
        /// </summary>
        public string Errors { get; set; }

        /// <summary>
        /// The number of boxes that were considered in the cubing attempt
        /// </summary>
        public int AvailableBoxCount { get; set; }
    }
}