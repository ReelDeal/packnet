﻿namespace PackNet.Cube.DTO
{
    /// <summary>
    /// An item to be Cubed
    /// </summary>
    public class OrderLineItem
    {
        /// <summary>
        /// Creates an OrderLineItem
        /// </summary>
        public OrderLineItem()
        {
            //TODO: document orientation
            Orientation1 = 9;
            Orientation2 = 9;
        }

        //TODO: Document traversal sequence
        /// <summary>
        /// Not sure what this is
        /// </summary>
        public string TraversalSequence { get; set; }

        /// <summary>
        /// Order id that the line item belongs to
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// Unique identifier for the line item
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Length of the item to be cubed
        /// </summary>
        public float Length { get; set; }

        /// <summary>
        /// Width of the item to be cubed
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        /// Height of the item to be cubed
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// Weight of the item to be cubed
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// Quantity of the item to be cubed
        /// </summary>
        public int Quantity { get; set; }

        //TODO: What are valid values for orientation???
        /// <summary>
        /// Orientation restriction for the item to be cubed. Restricts how an item must be placed into the box. For example, some items must be place “this side up” to avoid damage during shipping. 
        /// </summary>
        public byte Orientation1 { get; set; }

        /// <summary>
        /// Orientation restriction for the item to be cubed. Restricts how an item must be placed into the box. For example, some items must be place “this side up” to avoid damage during shipping. 
        /// </summary>
        public byte Orientation2 { get; set; }

        /// <summary>
        /// True if the item can be packaged inside another like item, Example: Two garbage cans can be stacked, one inside each other
        /// </summary>
        public bool ItemCanBeNested { get; set; }

        /// <summary>
        /// The amount the item will grow when the item is nested. Example: When two garbage cans are stacked, one inside each other, the end result is a item that is "X" percent larger then the original item
        /// </summary>
        public float NestingGrowthPercentage { get; set; }

        /// <summary>
        /// The maximum number of items that can be nested together
        /// </summary>
        public int MaximumNumberOfNestedItems { get; set; }

        /// <summary>
        /// The axis that the nesting will occur on
        /// </summary>
        public int NestingAxis { get; set; }

        /// <summary>
        /// True if the item has a void that can be filled with another item. Note: Item must fit entirely inside the void
        /// </summary>
        public bool ItemCanContainOtherItems { get; set; }

        /// <summary>
        /// X Dimention that defines the void that can be filled by another item
        /// </summary>
        public float VoidFillDimentionX { get; set; }

        /// <summary>
        /// Y Dimention that defines the void that can be filled by another item
        /// </summary>
        public float VoidFillDimentionY { get; set; }

        /// <summary>
        /// Z Dimention that defines the void that can be filled by another item
        /// </summary>
        public float VoidFillDimentionZ { get; set; }

        /// <summary>
        /// Group Category for Mix inclusion of exclusion
        /// </summary>
        public string ItemType { get; set; }

        //TODO: What is this? Use it or make it private
        /// <summary>
        /// N/A
        /// </summary>
        public string AuxiliaryField1 { get; set; }

        /// <summary>
        /// N/A
        /// </summary>
        public string AuxiliaryField2 { get; set; }

        /// <summary>
        /// Sets the Length, Width, and Height of this item
        /// </summary>
        /// <param name="length">Item Length</param>
        /// <param name="width">Item Width</param>
        /// <param name="height">Item Height</param>
        public void SetDimensions(float length, float width, float height)
        {
            Length = length;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// String representation of the line item
        /// </summary>
        public string StringDimension
        {
            get
            {
                return Length + " X " + Width + " X " + Height;
            }
        }
    }
}
