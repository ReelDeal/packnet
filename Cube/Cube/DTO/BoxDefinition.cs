﻿namespace PackNet.Cube.DTO
{
    using System.Diagnostics;

    using Enums;

    /// <summary>
    /// Represents a usable carton in PackNet.Cube 
    /// </summary>
    [DebuggerDisplay("Id = {Id}, Length = {Length}, Width = {Width}, Height = {Height}, BoxType = {BoxType}")]
    public class BoxDefinition
    {
        /// <summary>
        /// Creates a BoxDefinition
        /// </summary>
        public BoxDefinition()
        {
            BoxType = BoxType.RSC;
        }

        /// <summary>
        /// Unique id for a box
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Length of the box
        /// </summary>
        public float Length { get; set; }

        /// <summary>
        /// Width of the box
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        /// Height of the box
        /// </summary>
        public float Height { get; set; }

        /// <summary>
        /// Maximum weight of the box
        /// </summary>
        public float MaxWeight { get; set; }

        /// <summary>
        /// Percentage of the box that can be filled
        /// </summary>
        public float MaxFillPercentage { get; set; }

        /// <summary>
        /// Type of the box
        /// </summary>
        public BoxType BoxType { get; set; }

        /// <summary>
        /// String representation of the box definition (Length X Width X Height)
        /// </summary>
        public string StringDimension
        {
            get
            {
                return Length + " X " + Width + " X " + Height;
            }
        }
    }
}
