﻿namespace PackNet.Cube.DTO
{
    using System.Collections.Generic;

    /// <summary>
    /// Represents a order to be cubed. Orders can contain multiple line items and can result in multiple box solutions
    /// </summary>
    public class CubeOrder
    {
        /// <summary>
        /// Order identifier for a group of items
        /// </summary>
        public string OrderId { get; set; }

        /// <summary>
        /// Percentage of the carton to cube to. Example: Setting the value to 90 leave a 10% void in the cube solution.
        /// </summary>
        public float FillPercentage { get; set; }

        /// <summary>
        /// A list of items to be cubed
        /// </summary>
        public IEnumerable<OrderLineItem> LineItems { get; set; }

        /// <summary>
        /// Unmodified list of the items that were cubed
        /// </summary>
        public IEnumerable<OrderLineItem> OriginalLineItems { get; set; }

        /// <summary>
        /// Items that could not be boxed
        /// </summary>
        public IEnumerable<OrderLineItem> UnboxedLineItems { get; set; }

        /// <summary>
        /// The volume of all items in the solution
        /// </summary>
        public float Volume { get; set; }

        /// <summary>
        /// Weight of all items in the solution
        /// </summary>
        public float Weight { get; set; }

        /// <summary>
        /// A list of the selected items that need to be used to pack the items
        /// </summary>
        public IEnumerable<SelectedBox> SelectedBoxes { get; set; }
    }
}
