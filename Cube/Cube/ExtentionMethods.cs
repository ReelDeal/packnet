﻿namespace PackNet.Cube
{
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using CubingClassLibrary;

    using Newtonsoft.Json;

    using DTO;

    /// <summary>
    /// Extension methods for PackNet.Cube
    /// </summary>
    public static class ExtentionMethods
    {
        /// <summary>
        /// Converts a BoxDefinition to the fast fetch definition
        /// </summary>
        /// <param name="boxDefinition"></param>
        /// <returns></returns>
        public static clsBoxDefinition ToFastFetchBoxDefinition(this BoxDefinition boxDefinition)
        {
            var tmp = new clsBoxDefinition(new []
                                                       {
                                                           boxDefinition.Id, 
                                                           boxDefinition.Length.ToString(CultureInfo.InvariantCulture), 
                                                           boxDefinition.Width.ToString(CultureInfo.InvariantCulture), 
                                                           boxDefinition.Height.ToString(CultureInfo.InvariantCulture),
                                                           boxDefinition.MaxWeight.ToString(CultureInfo.InvariantCulture),
                                                           boxDefinition.MaxFillPercentage.ToString(CultureInfo.InvariantCulture)
                                                           //boxDefinition.BoxType.ToString()
                                                       });
            return tmp;
        }

        //internal because no api user needs to know about fastfetch (clsBoxDefinition)
        internal static IEnumerable<clsBoxDefinition> ToFastFetchBoxDefinitions(this IEnumerable<BoxDefinition> boxDefinitions)
        {
            return boxDefinitions.Select(b => b.ToFastFetchBoxDefinition());
        }

        //internal because no api user needs to know about fastfetch (clsBoxDefinition)
        private static BoxDefinition ToBoxDefinition(this clsBoxDefinition fastFetchBoxDefinition)
        {
            return new BoxDefinition
                   {
                       Id = fastFetchBoxDefinition.BoxID,
                       //BoxType = (BoxType)Enum.Parse(typeof(BoxType), fastFetchBoxDefinition.BoxType),
                       Length = fastFetchBoxDefinition.Dimension[0],
                       Width = fastFetchBoxDefinition.Dimension[1],
                       Height = fastFetchBoxDefinition.Dimension[2],
                       MaxFillPercentage = fastFetchBoxDefinition.MaxFillPercentage,
                       MaxWeight = fastFetchBoxDefinition.MaxWeight
                   };
        }

        //internal because no api user needs to know about fastfetch (clsBoxDefinition)
        internal static IEnumerable<BoxDefinition> ToBoxDefinitions(this IEnumerable<clsBoxDefinition> fastFetchBoxDefinitions)
        {
            return fastFetchBoxDefinitions.Select(b => b.ToBoxDefinition());
        }

        //internal because no api user needs to know about fastfetch (clsOrderItem)
        internal static OrderLineItem ToOrderLineItem(this clsOrderItem fromOrderItem)
        {
            var item = new OrderLineItem
                       {
                           AuxiliaryField1 = fromOrderItem.AuxiliaryField1,
                           AuxiliaryField2 = fromOrderItem.AuxiliaryField2,
                           Height = fromOrderItem.Dimension[2],
                           Id = fromOrderItem.OrderItemID,
                           ItemCanBeNested = fromOrderItem.CanNest,
                           ItemCanContainOtherItems = (bool)fromOrderItem.CanContain,
                           ItemType = fromOrderItem.ItemType,
                           Length = fromOrderItem.Dimension[0],
                           NestingAxis = fromOrderItem.NestAxis,
                           NestingGrowthPercentage = fromOrderItem.NestGrowthPercent,
                           MaximumNumberOfNestedItems = fromOrderItem.MaxNestDepth,
                           Orientation1 = fromOrderItem.DimensionOrientation[0],
                           Orientation2 = fromOrderItem.DimensionOrientation[1],
                           Quantity = fromOrderItem.OriginalQuantity,
                           TraversalSequence = fromOrderItem.TraversalSequence,
                           VoidFillDimentionX = fromOrderItem.ContainDimension[0],
                           VoidFillDimentionY = fromOrderItem.ContainDimension[1],
                           VoidFillDimentionZ = fromOrderItem.ContainDimension[2],
                           Weight = fromOrderItem.Weight,
                           Width = fromOrderItem.Dimension[1]
                       };
            //item.OrderId = fromOrderItem.;
            return item;
        }

        //internal because no api user needs to know about fastfetch (clsOrderItem)
        internal static IEnumerable<OrderLineItem> ToOrderLineItems(this IEnumerable<clsOrderItem> fromOrderItems)
        {
            return fromOrderItems.Select(o => o.ToOrderLineItem());
        }

        //internal because no api user needs to know about fastfetch (ClsRealBox)
        internal static SelectedBox ToSelectedBox(this ClsRealBox realBox)
        {
            var box = new SelectedBox
                      {
                          Id = realBox.BoxID,
                          BoxType = realBox.BoxType,
                          ContentLength = realBox.ContentDimensions[0],
                          ContentWidth = realBox.ContentDimensions[1],
                          ContentHeight = realBox.ContentDimensions[2],
                          ContentVolume = realBox.ContentVolume,
                          UsedVolume = realBox.UsedVolume,
                          WastedVolume = realBox.WastedVolume,
                          WastedVolumePercent = realBox.WastedVolumePercent,
                          Weight = realBox.Weight,
                          BoxDefinition = new BoxDefinition
                                          {
                                              Id = realBox.BoxID, 
                                              Length = realBox.OriginalDimension[0], 
                                              Width = realBox.OriginalDimension[1],
                                              Height = realBox.OriginalDimension[2],
                                              //BoxType = (BoxType)Enum.Parse(typeof(BoxType),realBox.BoxType)
                                          }
                      };
            return box;
        }

        //internal because no api user needs to know about fastfetch (ClsRealBox)
        internal static IEnumerable<SelectedBox> ToSelectedBoxes(this IEnumerable<ClsRealBox> fromBoxes)
        {
            return fromBoxes.Select(b => b.ToSelectedBox());
        }

        //internal because no api user needs to know about fastfetch (clsOrder)
        internal static CubeOrder ToCubeOrder(this clsOrder fromOrder)
        {
            var order = new CubeOrder
                        {
                            OrderId = fromOrder.OrderID,
                            FillPercentage = fromOrder.OrderFillPercentage,
                            LineItems = fromOrder.OrderItemList.ToOrderLineItems(),
                            OriginalLineItems = fromOrder.OriginalItemList.ToOrderLineItems(),
                            SelectedBoxes = fromOrder.SelectedBoxList.ToSelectedBoxes(),
                            UnboxedLineItems = fromOrder.UnboxedOrderItems(fromOrder).ToOrderLineItems(),
                            Volume = fromOrder.Volume,
                            Weight = fromOrder.Weight
                        };
            return order;
        }

        /// <summary>
        /// Converts a json representation of a list of cubing results to a CLR List of CubingResult
        /// </summary>
        /// <param name="jsonString"></param>
        /// <returns></returns>
        public static List<CubingResult> CubingResultsListFromJson(this string jsonString)
        {
            return JsonConvert.DeserializeObject<List<CubingResult>>(jsonString);
        }
    }

    
}
