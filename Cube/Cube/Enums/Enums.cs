﻿namespace PackNet.Cube.Enums
{
    /// <summary>
    /// Design used to pack each Item
    /// </summary>
    public enum BoxType
    {
        /// <summary>
        /// A box type that was not set
        /// </summary>
        Unknown = 0,
        /// <summary>
        /// Represents a standard RSC
        /// </summary>
        RSC = 1,

        /// <summary>
        /// Represents a standard envelope
        /// </summary>
        Envelope = 2
    }

    /// <summary>
    /// Cube method used to create a solution
    /// </summary>
    public enum CubeMethod
    {
        /// <summary>
        /// The default selection, Selects a carton based on least void in the carton
        /// </summary>
        BestFit,

        /// <summary>
        /// Creates a solution based on the order of items to be packed in the carton
        /// </summary>
        PickSequence
    }

    /// <summary>
    /// Cube speed used to create the solution
    /// </summary>
    public enum CubeSpeed
    {
        /// <summary>
        /// Fastest match lookup
        /// </summary>
        Fastest = 1,
        /// <summary>
        /// Speed and Quality Control, the greatest value the slowest but more accurate solution 
        /// </summary>
        Faster = 2,
        /// <summary>
        /// Speed and Quality Control, the greatest value the slowest but more accurate solution 
        /// </summary>
        Regular = 3,
        /// <summary>
        /// Speed and Quality Control, the greatest value the slowest but more accurate solution 
        /// </summary>
        GoodMatch = 4,
        /// <summary>
        /// Speed and Quality Control, the greatest value the slowest but more accurate solution 
        /// </summary>
        BetterMatch = 5,

        /// <summary>
        /// Best match lookup (slower)
        /// </summary>
        BestMatch = 6,

    }
}
