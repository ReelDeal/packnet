﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Logging;
using PackNet.Cube.L10N;

namespace PackNet.Cube
{
    using System;
    using System.Diagnostics;
    using System.Diagnostics.Contracts;

    using DTO;
    using Enums;

    /// <summary>
    /// This creates the Boxes file needed by the Cubing Algorithm based in a set of conditions as explained in detail in the paramenters documentation
    /// </summary>
    public static class BoxListGenerator
    {
        /// <summary>
        /// The Logger used by the BoxListGenerator
        /// </summary>
        public static ILogger Logger { get; set; }

        /// <summary>
        /// This set limits on Box List Result Set based on a RSC Box Type and Max Corrugated Width Loaded in Machine
        /// </summary>
        /// <param name="width">Width of the Box used in fromulat to calculaate max corrugated dim</param>
        /// <param name="height">Heigth of the Box used in fromulat to calculaate max corrugated dim</param>
        /// <param name="maxCorrugateWidth">Widest corrugated loaded in the machine</param>
        /// <returns></returns>
        private static bool RscMaxCorrugateFilter(float width, float height, float maxCorrugateWidth)
        {
            var rscWidth = (height + width) + (4.8 * .16);
            if (rscWidth > maxCorrugateWidth)
                return false;
            return true;
        }
        
        //TODO: Review boxType tag
        /// <summary>
        /// Final Constructed BoxList used by cubing algorithm with Filter
        /// </summary>
        /// <param name="minimumDimensions">Smallest possible Box considered for selection</param>
        /// <param name="maxDimensions">Biggest possible Box considered for selection</param>
        /// <param name="increment">Dimentional growth in Length,Width and Heigth</param>
        /// <param name="maxCorrugateWidth">Widest Corrugated Loaded in ALL available machines</param>
        /// <param name="maxWeight">Maximum weight allowed per Box</param>
        /// <param name="fillPercentage">Percent that the Box will be set as Full, Example if fillPercentage = 80, The box will have 20% void space</param>
        /// <param name="boxType">Box Design, Items</param>
        /// <returns></returns>
        public static List<BoxDefinition> GenerateWithRscMaxCorrugateFilter(
            BoxDefinition minimumDimensions,
            BoxDefinition maxDimensions,
            float increment,
            float maxCorrugateWidth,
            float maxWeight = 1f,
            float fillPercentage = 1f,
            BoxType boxType = BoxType.RSC)
        {
            var stopWatch = Stopwatch.StartNew();
            var boxList = Generate(minimumDimensions, maxDimensions, increment, (l, w, h) => RscMaxCorrugateFilter(w, h, maxCorrugateWidth), maxWeight, fillPercentage, boxType);
            if(Logger != null)
                Logger.Log(LogLevel.Info, String.Format(Strings.BoxListGenerator_GenerateWithRscMaxCorrugateFilter_Generated_box_list_with_RSC_max_corrugate_width_filter___0__results_in__1__milliseconds, boxList.Count(), stopWatch.ElapsedMilliseconds) );
            return boxList;
        }

        /// <summary>
        /// Final Constructed BoxList used by cubing algorithm
        /// </summary>
        /// <param name="minimumDimensions">Smallest possible Box considered for selection</param>
        /// <param name="maxDimensions">Biggest possible Box considered for selection</param>
        /// <param name="increment">Dimentional growth in Length,Width and Heigth</param>
        /// <param name="maxWeight">Maximum weight allowed per Box</param>
        /// <param name="fillPercentage">Percent that the Box will be set as Full, Example if fillPercentage = 80, The box will have 20% void space</param>
        /// <param name="boxType">Box Design, Items</param>
        /// <returns></returns>
        public static List<BoxDefinition> Generate(
            BoxDefinition minimumDimensions,
            BoxDefinition maxDimensions,
            float increment,
            float maxWeight = 1f,
            float fillPercentage = 1f,
            BoxType boxType = BoxType.RSC)
        {
            var stopWatch = Stopwatch.StartNew();
            var boxList = Generate(minimumDimensions, maxDimensions, increment, (l, w, h) => true, maxWeight, fillPercentage, boxType);
            if (Logger != null)
                Logger.Log(LogLevel.Info, String.Format(Strings.BoxListGenerator_GenerateWithRscMaxCorrugateFilter_Generated_box_list_with_RSC_max_corrugate_width_filter___0__results_in__1__milliseconds, boxList.Count(), stopWatch.ElapsedMilliseconds));
            return boxList;
        }

        /// <summary>
        /// Generates a box list from a given min and max box definition
        /// </summary>
        /// <param name="minimumDimensions">Smallest possible Box considered for selection</param>
        /// <param name="maxDimensions">Biggest possible Box considered for selection</param>
        /// <param name="increment">Dimentional growth in Length,Width and Heigth</param>
        /// <param name="filterPredicate">Limits the Box creation result set to fit in the widest corrugated available</param>
        /// <param name="maxWeight">Maximum weight allowed per Box</param>
        /// <param name="fillPercentage">Percent that the Box will be set as Full, Example if fillPercentage = 80, The box will have 20% void space</param>
        /// <param name="boxType">Box Design, Items</param>
        /// <returns></returns>
        public static List<BoxDefinition> Generate(BoxDefinition minimumDimensions, BoxDefinition maxDimensions, float increment, Func<float, float, float, bool> filterPredicate,  float maxWeight = 1f, float fillPercentage = 1f, BoxType boxType = BoxType.RSC)
        {
            Contract.Requires<ArgumentException>(minimumDimensions != null);
            Contract.Requires<ArgumentException>(maxDimensions != null);
            Contract.Requires<ArgumentException>(filterPredicate != null);
            
            Contract.Requires<ArgumentException>(minimumDimensions.Length > 0);
            Contract.Requires<ArgumentException>(minimumDimensions.Width > 0);
            Contract.Requires<ArgumentException>(minimumDimensions.Height > 0);
            Contract.Requires<ArgumentException>(maxDimensions.Length > 0);
            Contract.Requires<ArgumentException>(maxDimensions.Width > 0);
            Contract.Requires<ArgumentException>(maxDimensions.Height > 0);
            Contract.Requires<ArgumentException>(increment > 0);
            Contract.Requires<ArgumentException>(maxWeight > 0);
            Contract.Requires<ArgumentException>(fillPercentage > 0);

            Contract.Requires<ArgumentException>(minimumDimensions.Length <= maxDimensions.Length);
            Contract.Requires<ArgumentException>(minimumDimensions.Width <= maxDimensions.Width);
            Contract.Requires<ArgumentException>(minimumDimensions.Height <= maxDimensions.Height);
            Contract.Requires<ArgumentException>(minimumDimensions.Length < maxDimensions.Length || minimumDimensions.Width < maxDimensions.Width || maxDimensions.Height < maxDimensions.Height, "At least one max dimension has to be greater then the corrisponding min dimension");

            var decimalIncrement = increment;
            var lengthList = Increment(minimumDimensions.Length, maxDimensions.Length, decimalIncrement);
            var widthList = Increment(minimumDimensions.Width, maxDimensions.Width, decimalIncrement);
            var heightList = Increment(minimumDimensions.Height, maxDimensions.Height, decimalIncrement);

            var boxId = 1;
            var orderedList = from length in lengthList
                              from width in widthList
                              from height in heightList
                              where filterPredicate(length,width,height)
                              select new BoxDefinition
                                     {
                                         Id = (boxId++).ToString(),
                                         Length = length,
                                         Width = width,
                                         Height = height,
                                         MaxWeight = maxWeight,
                                         MaxFillPercentage = fillPercentage,
                                         BoxType = boxType
                                     };
            var result = orderedList.ToList();
            return result;
        }

        /// <summary>
        /// Helper that yeilds a number incrementer
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="increment"></param>
        /// <returns></returns>
        public static IEnumerable<decimal> Increment(decimal from, decimal to, decimal increment)
        {
            Contract.Requires(from <= to, "'from' value must be less then or equal to 'to' value");
            Contract.Requires<ArgumentException>(increment > 0, "'increment' value must be greater then 0");
            
            from = from - increment;
            while (from < to)
                yield return (from += increment);
        }

        /// <summary>
        /// Returns Increment value used in Box File creation
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="increment"></param>
        /// <returns></returns>
        public static IEnumerable<float> Increment(float from, float to, float increment)
        {
            return Increment((decimal)from, (decimal)to, (decimal)increment).Select(d=>(float)d);
        }

        /// <summary>
        /// Returns Increment value used in Box File creation
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="increment"></param>
        /// <returns></returns>
        public static IEnumerable<double> Increment(double from, double to, double increment)
        {
            return Increment((decimal)from, (decimal)to, (decimal)increment).Select(d => (double)d);
        }
    }
}
