﻿
using CloudReportingAutomationTests.PageObjects;
using CloudReportingAutomationTests.Utilities;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Threading;
using Testing.Specificity;

namespace CloudReportingAutomationTests.PageTests
{
    [CodedUITest]
    public class HomePageTests : TestBase
    {
        /// <summary>
        /// Verifies the reports links exist and work.
        /// </summary>
        [TestMethod]
        [TestCategory("UIAutomation")]
        public void VerifyReportsLinksExistAndWork()
        {
            TestUtilities.LoginAsAdmin();
            var page = TestUtilities.GetPage<Home>(String.Empty);

            //first lets verify the links exist
            Retry.For(() => page.cartonVolumeReportLink.Exists, TimeSpan.FromSeconds(10));
            Retry.For(() => page.MachineUtilizationReportLink.Exists, TimeSpan.FromSeconds(10));
            Retry.For(() => page.OperatorEfficiencyReportLink.Exists, TimeSpan.FromSeconds(10));

            //now lets check these links actually take us to the correct reports pages
            Retry.For(() => page.cartonVolumeReportLink.Click(), TimeSpan.FromSeconds(10));
            Thread.Sleep(TimeSpan.FromSeconds(5));
            Retry.For(() => Specify.That(page.homeH2.InnerText.Contains("Carton Volume Report")).Should.BeTrue("Carton Volume's Report didn't show"), TimeSpan.FromSeconds(10));

            Retry.For(() => page.homeMenu.Click(), TimeSpan.FromSeconds(10));
            Retry.For(() => page.homeLink.Click(), TimeSpan.FromSeconds(10));
            Retry.For(() => page.MachineUtilizationReportLink.Click(), TimeSpan.FromSeconds(10));
            Thread.Sleep(TimeSpan.FromSeconds(5));
            Retry.For(() => Specify.That(page.homeH2.InnerText.Contains("Machine Utilization Report")).Should.BeTrue("Machine Utilization's Report didn't show"), TimeSpan.FromSeconds(10));

            Retry.For(() => page.homeLink.Click(), TimeSpan.FromSeconds(10));
            Retry.For(() => page.OperatorEfficiencyReportLink.Click(), TimeSpan.FromSeconds(10));
            Thread.Sleep(TimeSpan.FromSeconds(5));
            Retry.For(() => Specify.That(page.homeH2.InnerText.Contains("Operator Efficiency Report")).Should.BeTrue("Operator Efficiency's Report didn't show"), TimeSpan.FromSeconds(10));
        }
    }
}
