﻿
using System;

using CloudReportingAutomationTests.PageObjects;
using CloudReportingAutomationTests.Utilities;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Testing.Specificity;

namespace CloudReportingAutomationTests.PageTests
{
    [CodedUITest]
    class ChangePasswordTests : TestBase
    {
        [TestMethod]
        [Ignore]
        [TestCategory("UIAutomation")]
        public void LoginWithValidUserAndPassword()
        {
            TestUtilities.LoginAsAdmin();
            var page = TestUtilities.GetPage<ChangePassword>(String.Empty);

            Retry.For(() => page.SaveButton.Exists, TimeSpan.FromSeconds(60));
            Specify.That(page.SaveButton.Exists).Should.BeTrue("Login didn't appear");

            Retry.For(() => page.NewPasswordEdit.Enabled, TimeSpan.FromSeconds(60));
            Specify.That(page.NewPasswordEdit.Enabled).Should.BeTrue("NewPassword field was not enabled");
            page.NewPasswordEdit.SetText("newpassword");

            Retry.For(() => page.NewPasswordConfirmationEdit.Enabled, TimeSpan.FromSeconds(60));
            Specify.That(page.NewPasswordConfirmationEdit.Enabled).Should.BeTrue("NewPassword field was not enabled");
            page.NewPasswordConfirmationEdit.SetText("newpassword");

            Retry.For(() => page.SaveButton.Click(), TimeSpan.FromSeconds(60));
            Retry.For(() => Specify.That(page.AlertSuccess.Exists).Should.BeTrue("Password success alert didn't appear"), TimeSpan.FromSeconds(10));
        }
    }
}
