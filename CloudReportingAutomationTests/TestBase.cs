﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CloudReportingAutomationTests
{
    [TestClass]
    public abstract class TestBase
    {
        protected virtual void BeforeTestInitialize() { }
        protected virtual void AfterTestInitialize() { }

        protected virtual void TestInitializeBeforeStartProcess() { }

        protected virtual void BeforeTestCleanup() { }
        protected virtual void AfterTestCleanup() { }
        protected virtual void BeforeClassCleanup() { }
        protected virtual void AfterClassCleanup() { }

        [TestCleanup]
        public void TestCleanup()
        {
            BeforeTestCleanup();
            // If we need to do a common test clean up, code will go here
            if (Playback.IsSessionStarted)
                Playback.Cleanup();
            AfterTestCleanup();
        }

        [ClassCleanup]
        public void Cleanup()
        {
            BeforeClassCleanup();
            // If we need to do a common test clean up, code will go here
            AfterClassCleanup();
        }

        [TestInitialize]
        public void TestInitialize()
        {
            BeforeTestInitialize();
            if (!Playback.IsInitialized)
            {
                Playback.Initialize();
            }

            BrowserWindow.CurrentBrowser = ConfigurationManager.AppSettings["CurrentBrowser"];
            AfterTestInitialize();
        }
    }
}
