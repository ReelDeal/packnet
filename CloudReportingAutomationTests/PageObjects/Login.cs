﻿using CUITe.Controls.HtmlControls;

namespace CloudReportingAutomationTests.PageObjects
{
    public class Login : CUITe_DynamicBrowserWindow
    {
        public Login()
            : base("Packsize Reporting")
        {
        }

        public CUITe_HtmlEdit UsernameEdit { get { return Get<CUITe_HtmlEdit>("id=username"); } }
        public CUITe_HtmlEdit PasswordEdit { get { return Get<CUITe_HtmlEdit>("id=password"); } }
        public CUITe_HtmlButton LoginButton { get { return Get<CUITe_HtmlButton>("type=submit"); } }
        public CUITe_HtmlHeading4 LoginH4 { get { return Get<CUITe_HtmlHeading4>(); } }

    }
}
