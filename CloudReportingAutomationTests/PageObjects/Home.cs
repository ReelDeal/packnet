﻿
using CUITe.Controls.HtmlControls;

namespace CloudReportingAutomationTests.PageObjects
{
    public class Home : CUITe_DynamicBrowserWindow
    {
        public Home()
            : base("Packsize Reporting")
        {
             sWindowTitle = "Packsize Reporting";
            SearchProperties[PropertyNames.Name] = sWindowTitle;
        }

        public CUITe_HtmlHeading2 homeH2 { get { return Get<CUITe_HtmlHeading2>(); } }

        //dashboard links
        public CUITe_HtmlUnorderedList leftMenu { get { return Get<CUITe_HtmlUnorderedList>("class=accordionMenu"); } }
        public CUITe_HtmlLabel homeMenu { get { return leftMenu.Get<CUITe_HtmlLabel>("InnerText~Home"); } }
        public CUITe_HtmlHyperlink homeLink { get { return leftMenu.Get<CUITe_HtmlHyperlink>("href~main"); } }
        public CUITe_HtmlLabel companiesMenu { get { return leftMenu.Get<CUITe_HtmlLabel>("InnerText~Report Admin"); } }
        public CUITe_HtmlHyperlink companiesLink { get { return leftMenu.Get<CUITe_HtmlHyperlink>("href~company"); } }

        //reports links
        public CUITe_HtmlHyperlink cartonVolumeReportLink { get { return Get<CUITe_HtmlHyperlink>("href~cartonvolume"); } }
        public CUITe_HtmlHyperlink MachineUtilizationReportLink { get { return Get<CUITe_HtmlHyperlink>("href~machines"); } }
        public CUITe_HtmlHyperlink OperatorEfficiencyReportLink { get { return Get<CUITe_HtmlHyperlink>("href~operators"); } }

        //top menu links
        public CUITe_HtmlDiv adminTopMenu { get { return Get<CUITe_HtmlDiv>("class=item dropdown username"); } }
        public CUITe_HtmlHyperlink adminTopMenuLink { get { return adminTopMenu.Get<CUITe_HtmlHyperlink>("TagName=a;TagInstance=1"); } }
        public CUITe_HtmlHyperlink changePasswordLink { get { return Get<CUITe_HtmlHyperlink>("href~password"); } }
        public CUITe_HtmlHyperlink logOutLink { get { return Get<CUITe_HtmlHyperlink>("href~logout"); } }

    }
}
