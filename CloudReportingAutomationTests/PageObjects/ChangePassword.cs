﻿
using CUITe.Controls.HtmlControls;

namespace CloudReportingAutomationTests.PageObjects
{
    public class ChangePassword : CUITe_DynamicBrowserWindow
    {
        public ChangePassword()
            : base("Packsize Reporting")
        {
            sWindowTitle = "Packsize Reporting";
            SearchProperties[PropertyNames.Name] = sWindowTitle;
        }

        public CUITe_HtmlHeading2 ChangePasswordH2 { get { return Get<CUITe_HtmlHeading2>(); } }

        public CUITe_HtmlEdit NewPasswordEdit { get { return Get<CUITe_HtmlEdit>("name=password"); } }
        public CUITe_HtmlEdit NewPasswordConfirmationEdit { get { return Get<CUITe_HtmlEdit>("name=confirmPassword"); } }
        public CUITe_HtmlButton SaveButton { get { return Get<CUITe_HtmlButton>("type=submit"); } }
        public CUITe_HtmlDiv AlertSuccess { get { return Get<CUITe_HtmlDiv>("class=alert alert-success ng-scope"); } }
    }
}
