﻿using CloudReportingAutomationTests.PageObjects;

using CUITe.Controls.HtmlControls;
using System;
using System.Configuration;

namespace CloudReportingAutomationTests.Utilities
{
    class TestUtilities
    {
        public static string CloudReportingUrl { get { return ConfigurationManager.AppSettings["CloudReportingUrl"]; } }
        public static string AdminUsername { get { return ConfigurationManager.AppSettings["AdminUsername"]; } }
        public static string AdminPassword { get { return ConfigurationManager.AppSettings["AdminPassword"]; } }

        /// <summary>
        /// Creates the page.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="pageUrl">The page URL.</param>
        /// <returns></returns>
        public static T CreatePage<T>(string pageUrl) where T : CUITe_DynamicBrowserWindow, new()
        {
            var mw =
              CUITe_BrowserWindow.Launch<T>(string.Format("{0}/{1}", CloudReportingUrl, pageUrl));
            mw.Maximized = true;

            return mw;
        }

        /// <summary>
        /// Use to navigate to an specific page after you have logged in.
        /// </summary>
        /// <param name="pageUrl"> we are navigating to</param>
        public static T GetPage<T>(string pageUrl) where T : CUITe_DynamicBrowserWindow, new()
        {
            return CUITe_BrowserWindow.GetBrowserWindow<T>();
        }

        /// <summary>
        /// Use to navigate to an specific page after you have logged in.
        /// </summary>
        /// <param name="pageUrl"> we are navigating to</param>
        public static T NavigateToPage<T>(string pageUrl) where T : CUITe_DynamicBrowserWindow, new()
        {
            var mw = CUITe_BrowserWindow.GetBrowserWindow<T>();
            mw.NavigateToUrl(String.Format("{0}/{1}", CloudReportingUrl, pageUrl));
            return mw;
        }

        public static void LoginAsAdmin()
        {
            var page = CreatePage<Login>(string.Empty);
            Retry.For(() => page.LoginButton.Exists, TimeSpan.FromSeconds(60));
            Retry.For(() => page.UsernameEdit.Enabled, TimeSpan.FromSeconds(60));
            page.UsernameEdit.SetText(AdminUsername);
            page.PasswordEdit.SetText(AdminPassword);
            Retry.For(() => page.LoginButton.Click(), TimeSpan.FromSeconds(60));
        }
    }
}
