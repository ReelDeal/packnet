﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SharpSnmpLib.Full.ForkedForBrDevices;
using SharpSnmpLib.Full.ForkedForBrDevices.BrCustomizationConstants;

namespace BrDeviceDiscoveryServiceTests
{
    [Ignore]
    [TestClass]
    public class BrDeviceSnmpAdministratorTests
    {
        BrDeviceSnmpAdministrator brDeviceSnmpAdministrator;

        [TestInitialize]
        public void TestInitialize()
        {
            brDeviceSnmpAdministrator = new BrDeviceSnmpAdministrator();
        }

        [TestMethod]
        public void DiscoverDevicesShouldFindAtLeastOneMachine()
        {
            var results = brDeviceSnmpAdministrator.DiscoverDevices();
            Assert.IsTrue(results.Count > 0, "no machines found!");
        }

        [TestMethod]
        public void DiscoverDevicesShouldFindPackLabTestPlc()
        {
            var results = brDeviceSnmpAdministrator.DiscoverDevices();
            var labMachine =
                results.FirstOrDefault(
                    a =>
                        a.DeviceAttributes[BrDeviceOidEnumeration.MacAddress].EndsWith("a8",
                            StringComparison.InvariantCultureIgnoreCase));

            Assert.IsNotNull(labMachine);
        }
    }
}