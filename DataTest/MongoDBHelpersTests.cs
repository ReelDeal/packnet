﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;

using Testing.Specificity;


namespace DataTest
{
    [TestClass]
    public class MongoDBHelpersTests
    {
        [TestMethod]
        public void DeepCopyCopiesAnObjectBasedOnBsonSettings()
        {
            MongoDbHelpers.TryRegisterSerializer<ProducibleStatuses>(new EnumerationSerializer());
            MongoDbHelpers.TryRegisterSerializer<ErrorProducibleStatuses>(new EnumerationSerializer());
            MongoDbHelpers.TryRegisterSerializer<InProductionProducibleStatuses>(new EnumerationSerializer());
            MongoDbHelpers.TryRegisterSerializer<NotInProductionProducibleStatuses>(new EnumerationSerializer());
            var carton = new Carton
            {
                Id = Guid.NewGuid(),
            };

            var order = new Order(carton, 4);
            var newOrder = MongoDbHelpers.DeepCopy(order);
            Specify.That(order).Should.Not.BeSameAs(newOrder);
            Specify.That(order.Id).Should.BeEqualTo(newOrder.Id);
            Specify.That(order.Producible).Should.Not.BeSameAs(newOrder.Producible);
            Specify.That(((Carton)order.Producible).Id).Should.BeEqualTo(((Carton)newOrder.Producible).Id);
        }
    }
}
