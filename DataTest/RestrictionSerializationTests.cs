﻿using System;
using System.Diagnostics;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Data;
using PackNet.Data.Producibles;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;

namespace DataTest
{

    [TestClass]
    public class RestrictionSerializationTests
    {
        [TestMethod]
        public void BasicRestrictionSerializationTest()
        {
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<string>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<int>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Guid>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<Corrugate>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterSerializer<MicroMeter>(new MicroMeterSerializer());

            var repo = new BoxLastRepository();
            repo.DeleteAll();
            var carton = new Carton();
            var id = Guid.NewGuid();
            carton.Id = id;
            carton.CustomerUniqueId = id.ToString();
            var expectedGuid = Guid.NewGuid();
            var expectedCorrugate = new Corrugate { Alias = "test", Id = Guid.NewGuid() };
            carton.Restrictions.Add(new BasicRestriction<string>("testString"));
            carton.Restrictions.Add(new BasicRestriction<int>(123));
            carton.Restrictions.Add(new BasicRestriction<Guid>(expectedGuid));
            CartonOnCorrugate cartonOnCorrugate = new CartonOnCorrugate(new Carton(), expectedCorrugate, string.Empty, null, OrientationEnum.Degree0);
            carton.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(cartonOnCorrugate));
            var bl = new BoxLastProducible { Producible = carton, ProductionGroupAlias = carton.ProductionGroupAlias };
            repo.Create(bl);
            var test = repo.Find(id);
            test.Producible.Restrictions.All(r =>
            {
                var s = r as BasicRestriction<string>;
                if (s != null)
                    return s.Value == "testString";

                var i = r as BasicRestriction<int>;
                if (i != null)
                    return i.Value == 123;

                var g = r as BasicRestriction<Guid>;
                if (g != null)
                    return g.Value == expectedGuid;

                var c = r as MustProduceWithOptimalCorrugateRestriction;
                if (c != null)
                    return c.Value.Corrugate.Alias == expectedCorrugate.Alias && c.Value.Corrugate.Id == expectedCorrugate.Id;
                return false;
            });

            repo.Delete(bl);
        }

        [TestMethod]
        public void AllAndAllRestrictionSerializationTest()
        {
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<string>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<int>>(new RestrictionDiscriminatorConvention());

            var repo = new BoxLastRepository();
            repo.DeleteAll();
            var carton = new Carton();
            var id = Guid.NewGuid();
            carton.Id = id;
            carton.CustomerUniqueId = id.ToString();
            var expectedGuid = Guid.NewGuid();
            var expectedCorrugate = new Corrugate { Alias = "test", Id = Guid.NewGuid() };
            carton.Restrictions.Add(new AllRequiredRestriction { Restrictions = { new BasicRestriction<string>("one"), new BasicRestriction<int>(123) } });
            carton.Restrictions.Add(new AnyRequiredRestriction { Restrictions = { new BasicRestriction<string>("two"), new BasicRestriction<int>(321) } });

            var bl = new BoxLastProducible { Producible = carton, ProductionGroupAlias = carton.ProductionGroupAlias };
            repo.Create(bl);
            var test = repo.Find(id);
            test.Producible.Restrictions.All(r =>
            {
                var s = r as AllRequiredRestriction;
                if (s != null)
                    return s.Restrictions.Count == 2
                           && (s.Restrictions[0] as BasicRestriction<string>).Value == "one"
                           && (s.Restrictions[1] as BasicRestriction<int>).Value == 123;

                var x = r as AnyRequiredRestriction;
                if (x != null)
                    return x.Restrictions.Count == 2
                           && (x.Restrictions[0] as BasicRestriction<string>).Value == "two"
                           && (x.Restrictions[1] as BasicRestriction<int>).Value == 321;
                return false;
            });

            repo.Delete(bl);
        }

        [TestMethod]
        public void NullRestrictionSerializationTest()
        {
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<string>>(new RestrictionDiscriminatorConvention());
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<int>>(new RestrictionDiscriminatorConvention());

            var repo = new BoxLastRepository();
            repo.DeleteAll();
            var carton = new Carton();
            var id = Guid.NewGuid();
            carton.Id = id;
            carton.CustomerUniqueId = id.ToString();
            var expectedGuid = Guid.NewGuid();
            var expectedCorrugate = new Corrugate { Alias = "test", Id = Guid.NewGuid() };
            carton.Restrictions.Add(new AllRequiredRestriction { Restrictions = { new BasicRestriction<string>("one"), new BasicRestriction<int>(123) } });
            carton.Restrictions.Add(null);
            carton.Restrictions.Add(new AnyRequiredRestriction { Restrictions = { new BasicRestriction<string>("two"), new BasicRestriction<int>(321) } });

            var bl = new BoxLastProducible { Producible = carton, ProductionGroupAlias = carton.ProductionGroupAlias };
            repo.Create(bl);
            var test = repo.Find(id);
            test.Producible.Restrictions.All(r =>
            {
                var s = r as AllRequiredRestriction;
                if (s != null)
                    return s.Restrictions.Count == 2
                           && (s.Restrictions[0] as BasicRestriction<string>).Value == "one"
                           && (s.Restrictions[1] as BasicRestriction<int>).Value == 123;

                var x = r as AnyRequiredRestriction;
                if (x != null)
                    return x.Restrictions.Count == 2
                           && (x.Restrictions[0] as BasicRestriction<string>).Value == "two"
                           && (x.Restrictions[1] as BasicRestriction<int>).Value == 321;
                return false;
            });

            repo.Delete(bl);
        }
    }
}
