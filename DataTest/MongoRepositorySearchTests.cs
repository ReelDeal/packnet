﻿using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Data.Producibles;

using Testing.Specificity;

namespace DataTest
{
    [TestClass]
    public class MongoRepositorySearchTests
    {
        private BoxLastRepository repo;
        private BoxLastProducible item6;
        private BoxLastProducible item5;
        private BoxLastProducible item4;
        private BoxLastProducible item3;
        private BoxLastProducible item2;
        private BoxLastProducible item1;

        [TestInitialize]
        public void Init()
        {
            repo = new BoxLastRepository();
            repo.DeleteAll();
            item1 = new BoxLastProducible(new Carton { CustomerUniqueId = "test1" });
            item2 = new BoxLastProducible(new Carton { CustomerUniqueId = "test2" }) {TimeTriggered = DateTime.UtcNow.AddHours(-1)};
            item3 = new BoxLastProducible(new Carton { CustomerUniqueId = "not3" }) { TimeTriggered = DateTime.UtcNow.AddDays(-10) };
            item4 = new BoxLastProducible(new Carton { CustomerUniqueId = "test4" });
            item5 = new BoxLastProducible(new Carton { CustomerUniqueId = "not5" });
            item6 = new BoxLastProducible(new Carton { CustomerUniqueId = "test6" });
            repo.Create(item1);
            repo.Create(item2);
            repo.Create(item3);
            repo.Create(item4);
            repo.Create(item5);
            repo.Create(item6);
        }


        [TestCleanup]
        public void TestCleanup()
        {
            repo.Delete(item1);
            repo.Delete(item2);
            repo.Delete(item3);
            repo.Delete(item4);
            repo.Delete(item5);
            repo.Delete(item6);
        }

        [TestMethod]
        public void SearchForCustomerUniqueId()
        {
            var results = repo.Search("{CustomerUniqueId: /test6/}");
            Specify.That(results.Count()).Should.BeEqualTo(1);
            Specify.That(results.Any(p => p.CustomerUniqueId == item6.CustomerUniqueId)).Should.BeTrue();
        }

        [TestMethod]
        public void SearchForPartialCustomerUniqueId()
        {
            var results = repo.Search("{CustomerUniqueId: /test/}");
            Specify.That(results.Count()).Should.BeEqualTo(4);
            Specify.That(results.All(p => p.CustomerUniqueId.Substring(0, 4) == "test")).Should.BeTrue();
        }

        [TestMethod]
        [Ignore]
        public void SearchForDateTimeRange()
        {
            var results = repo.Search("{Created:{$gte:ISODate('2015-03-29'), $lt: ISODate('2015-04-05T18:59:27.182Z')}}");
            Specify.That(results.Count()).Should.BeEqualTo(6);
        }
    }
}
