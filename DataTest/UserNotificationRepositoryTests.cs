﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Messaging;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.UserNotifications;

using Testing.Specificity;

namespace DataTest
{
	[TestClass]
	public class UserNotificationRepositoryTests
	{
		private UserNotificationRepository repository;

		private readonly Guid machineGroup1Id= Guid.NewGuid();
		private readonly Guid machineGroup2Id = Guid.NewGuid();

		private readonly Guid user1Id = Guid.NewGuid();
		private readonly Guid user2Id = Guid.NewGuid();
        private ConcurrentList<Guid> notifiedUsers;

	    [TestInitialize]
		public void Initialize()
		{
            notifiedUsers = new ConcurrentList<Guid>(new[] { user1Id, user2Id });

			repository = new UserNotificationRepository();

			// Notifications generated now
			repository.Create(new UserNotification()
			{
				Message = "System Notification",
				NotificationType = NotificationType.System,
				Severity = NotificationSeverity.Info,
				CreatedDate = DateTime.UtcNow,
				NotifiedUsers = notifiedUsers
			});

			repository.Create(new UserNotification()
			{
				Message = "Machine Group Notification",
				NotificationType = NotificationType.MachineGroup,
				Severity = NotificationSeverity.Success,
				CreatedDate = DateTime.UtcNow,
                NotifiedMachineGroups = new ConcurrentList<Guid>(new[] { machineGroup1Id }),
				NotifiedUsers = notifiedUsers
			});

			repository.Create(new UserNotification()
			{
				Message = "Production Group Notification",
				NotificationType = NotificationType.ProductionGroup,
				Severity = NotificationSeverity.Success,
				CreatedDate = DateTime.UtcNow,
                NotifiedMachineGroups = new ConcurrentList<Guid>(new[] { machineGroup1Id, machineGroup2Id }),
				NotifiedUsers = notifiedUsers
			});

			// Notification generated before the 24 hour limit
			repository.Create(new UserNotification()
			{
				Message = "System Notification",
				NotificationType = NotificationType.System,
				Severity = NotificationSeverity.Success,
				CreatedDate = DateTime.UtcNow.AddHours(-23).AddMinutes(-59),
				NotifiedUsers = notifiedUsers
			});

			// Notification generated after the 24 hour limit
			repository.Create(new UserNotification()
			{
				Message = "System Notification",
				NotificationType = NotificationType.System,
				Severity = NotificationSeverity.Warning,
				CreatedDate = DateTime.UtcNow.AddDays(-1),
				NotifiedUsers = notifiedUsers
			});
		}

		[TestCleanup]
		public void CleanUp()
		{
			repository.DeleteAll();
		}

		[TestMethod]
		public void DismissAllByUserDoesNotDelete()
		{
			IEnumerable<UserNotification> result;

			// Dismiss all notifications of type "System"
			repository.DismissAllByUser(user1Id);

			// Dismiss shouldn't delete rows from the database
			result = repository.FindByUserId(user1Id);

			Specify.That(result.Count()).Should.BeEqualTo((int)repository.Count());
		}

        [TestMethod]
        public void DismissAllByUserNotWorkingMachineGroup()
        {
            IEnumerable<UserNotification> result;

            // Dismiss all notifications of type "System"
            repository.DismissAllByUser(user1Id);

            // User1 should only have 2 active notifications, one of "Machine Group" type and another one of "Production Group" type
            result = repository.FindActiveByUserId(user1Id);

            Specify.That(result.Count()).Should.BeEqualTo(2);
            Specify.That(result.Where(u => u.NotificationType == NotificationType.MachineGroup).ToList().Count).Should.BeEqualTo(1);
            Specify.That(result.Where(u => u.NotificationType == NotificationType.ProductionGroup).ToList().Count).Should.BeEqualTo(1);

            // User2 should have active notifications
            result = repository.FindActiveByUserId(user2Id);

            Specify.That(result.Count()).Should.BeEqualTo((int)repository.Count() - 1);
        }

        //[TestMethod]
        //bug:9371 shows that this fixes it.  Leaving commented out because it takes way too long to run.
        //public void ShouldHandleLargeDataSet()
        //{
        //    IEnumerable<UserNotification> result;


        //    // Notification generated after the 24 hour limit
        //    ParallelEnumerable.Range(1, 1999999).ForEach(i => repository.Create(new UserNotification()
        //    {
        //        Message = "System Notification " + i,
        //        NotificationType = NotificationType.System,
        //        Severity = NotificationSeverity.Warning,
        //        CreatedDate = DateTime.UtcNow,
        //        NotifiedUsers = notifiedUsers
        //    }));


        //    // User1 should only have 2 active notifications, one of "Machine Group" type and another one of "Production Group" type
        //    result = repository.FindActiveByUserId(user1Id);

        //    Specify.That(result.Count()).Should.BeEqualTo(10000);

        //}

		[TestMethod]
		public void DismissAllByUserWorkingOnMachineGroup()
		{
			IEnumerable<UserNotification> result;

			// Dismiss all notifications for the currently assigned Machine Group (in this case, machineGroup2)
			repository.DismissAllByUser(machineGroup2Id, user1Id);

			// User1 should only have 1 active notifications, the one sent exclusively to the machineGroup1
			result = repository.FindActiveByUserId(user1Id);

			Specify.That(result.Count()).Should.BeEqualTo(1);
			Specify.That(result.Where(u => u.NotifiedMachineGroups.Contains(machineGroup1Id)).ToList().Count).Should.BeEqualTo(1);

			// User2 should have active notifications
			result = repository.FindActiveByUserId(user2Id);

			Specify.That(result.Count()).Should.BeEqualTo((int)repository.Count() - 1);
		}

		[TestMethod]
		public void DismissByUser()
		{
			IEnumerable<UserNotification> result;

			// Obtain all active notifications and pick one to dismiss
			result = repository.FindActiveByUserId(user1Id);

			var notification = result.First();

			repository.DismissByUser(notification.Id, user1Id);

			// Refresh active notifications
			result = repository.FindActiveByUserId(user1Id);

			// Verify that the dismissed notification is not returned
			Specify.That(result.Where(u => u.Id == notification.Id).ToList().Count).Should.BeEqualTo(0);

			// Verify that only one notification was flagged as dismissed by the user
			result = repository.All();

			Specify.That(result.Where(u => u.Id == notification.Id && u.DismissedByUsers.Contains(user1Id)).ToList().Count).Should.BeEqualTo(1);
			Specify.That(result.Where(u => u.DismissedByUsers.Contains(user1Id)).ToList().Count).Should.BeEqualTo(1);
		}

		[TestMethod]
		public void FindActiveByUserId()
		{
			// Only notifications that have less than 24 hours of creation are returned
			var result = repository.FindActiveByUserId(user1Id);

			Specify.That(result.Count()).Should.BeEqualTo((int)repository.Count() - 1);
		}

		[TestMethod]
		public void FindByUserId()
		{
			// Returns all notifications regardless if they were dismissed and without creation date limit
			var result = repository.FindByUserId(user1Id);

			Specify.That(result.Count()).Should.BeEqualTo((int)repository.Count());
		}

		[TestMethod]
		public void SearchUserNotificationsFilteredByAllFilters()
		{
			IEnumerable<UserNotification> result;
			List<NotificationType> notificationTypes = new List<NotificationType>();
			List<NotificationSeverity> notificationSeverities = new List<NotificationSeverity>();

			// Search for all System notifications of Success severity
			notificationTypes.Add(NotificationType.System);
			notificationSeverities.Add(NotificationSeverity.Success);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(1);
		}

		[TestMethod]
		public void SearchUserNotificationsFilteredByDate()
		{
			IEnumerable<UserNotification> result;
			List<NotificationType> notificationTypes = new List<NotificationType>();
			List<NotificationSeverity> notificationSeverities = new List<NotificationSeverity>();

			// Search for notifications up to 1 hour older
			result = repository.SearchUserNotifications(DateTime.UtcNow.AddHours(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo((int)repository.Count() - 2);

			// Search for notifications from all day yesterday until now
			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddDays(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo((int)repository.Count());
		}

		[TestMethod]
		public void SearchUserNotificationsFilteredByNotificationType()
		{
			IEnumerable<UserNotification> result;
			List<NotificationType> notificationTypes = new List<NotificationType>();
			List<NotificationSeverity> notificationSeverities = new List<NotificationSeverity>();

			// Search for all Machine Group notifications
			notificationTypes.Clear();
			notificationTypes.Add(NotificationType.MachineGroup);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(1);

			// Search for all Production Group notifications
			notificationTypes.Clear();
			notificationTypes.Add(NotificationType.ProductionGroup);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(1);

			// Search for all System notifications
			notificationTypes.Clear();
			notificationTypes.Add(NotificationType.System);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(3);

			// Search for all Machine Group and Production Group notifications
			notificationTypes.Clear();
			notificationTypes.Add(NotificationType.MachineGroup);
			notificationTypes.Add(NotificationType.ProductionGroup);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(2);
		}

		[TestMethod]
		public void SearchUserNotificationsFilteredByNotificationSeverity()
		{
			IEnumerable<UserNotification> result;
			List<NotificationType> notificationTypes = new List<NotificationType>();
			List<NotificationSeverity> notificationSeverities = new List<NotificationSeverity>();

			// Search for all Error notifications
			notificationSeverities.Clear();
			notificationSeverities.Add(NotificationSeverity.Error);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(0);

			// Search for all Info notifications
			notificationSeverities.Clear();
			notificationSeverities.Add(NotificationSeverity.Info);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(1);

			// Search for all Success notifications
			notificationSeverities.Clear();
			notificationSeverities.Add(NotificationSeverity.Success);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(3);

			// Search for all Warning notifications
			notificationSeverities.Clear();
			notificationSeverities.Add(NotificationSeverity.Warning);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(1);

			// Search for all Info and Warning notifications
			notificationSeverities.Clear();
			notificationSeverities.Add(NotificationSeverity.Info);
			notificationSeverities.Add(NotificationSeverity.Warning);

			result = repository.SearchUserNotifications(DateTime.UtcNow.Date.AddMonths(-1), DateTime.UtcNow, notificationTypes, notificationSeverities);

			Specify.That(result.Count()).Should.BeEqualTo(2);
		}
	}
}
