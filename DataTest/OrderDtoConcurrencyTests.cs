﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.Orders;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Data.Orders;
using PackNet.Data.Repositories.MongoDB;

using TestUtils;

namespace DataTest
{
    /// <summary>
    /// This test reproduces the issue resulting in this exception from TFS 10256.
    /// 2015-08-03 11:20:43.2101|ERROR|10448(23)|PackNet.Common.Interfaces.ExtensionMethods.ObservableExtensions+<>c__DisplayClass1`1.<DurableSubscribe>b__0|Message: Error in durable subscription. PackNet.Common.Interfaces.Enums.ProducibleStates.NotInProductionProducibleStatuses, Exception: System.InvalidOperationException: Collection was modified; enumeration operation may not execute.
    /// at System.Collections.Generic.List`1.Enumerator.MoveNextRare()
    /// at System.Collections.Generic.List`1.Enumerator.MoveNext()
    /// at MongoDB.Bson.Serialization.Serializers.EnumerableSerializerBase`1.Serialize(BsonWriter bsonWriter, Type nominalType, Object value, IBsonSerializationOptions options)
    /// at MongoDB.Bson.Serialization.BsonClassMapSerializer.SerializeMember(BsonWriter bsonWriter, Object obj, BsonMemberMap memberMap)
    /// at MongoDB.Bson.Serialization.BsonClassMapSerializer.Serialize(BsonWriter bsonWriter, Type nominalType, Object value, IBsonSerializationOptions options)
    /// at MongoDB.Driver.MongoCollection.Save(Type nominalType, Object document, MongoInsertOptions options)
    /// at PackNet.Data.Repositories.MongoDB.MongoDbRepository`1.Update(T objectToUpdate) in c:\Development\PackNet\Data\Repositories\MongoDB\MongoDbRepository.cs:line 130
    /// at PackNet.Business.Orders.Orders.Update(Order order) in c:\Development\PackNet\Business\Orders\Orders.cs:line 37
    /// at PackNet.Services.SelectionAlgorithm.Orders.OrdersSelectionAlgorithmService.<>c__DisplayClass6d.<GetNextOrderAndProducibleInQueue>b__69(ProducibleStatuses ps) in c:\Development\PackNet\Services\SelectionAlgorithm\Orders\OrdersSelectionAlgorithmService.cs:line 623
    /// at PackNet.Common.Interfaces.ExtensionMethods.ObservableExtensions.<>c__DisplayClass1`1.<DurableSubscribe>b__0(T a) in c:\Development\PackNet\CommonInterfaces\ExtensionMethods\ObservableExtensions.cs:line 21 
    /// </summary>
    [TestClass]
    public class OrderDtoConcurrencyTests
    {
        //Order order;
        //IOrdersRepository ordersRepository;
        //IOrders orders;

        //[Ignore]
        //[TestMethod]
        //[ExpectedException(typeof(AggregateException))]
        //public void UpdateUsingRepo_ShouldFail_WhenOrderHasChildCollectionsBeingModifiedUsingUpdateWithRetries()
        //{
        //    order = new Order(new Carton(), 100);
        //    ordersRepository = new OrdersRepository();
        //    //orders = new Orders(ordersRepository);
        //    List<Task> tasks = new List<Task>();

        //    order.ProducibleStatusObservable.Subscribe(ps =>
        //    {
        //        ordersRepository.Update(order);
        //    });

        //    Enumerable.Range(1, 100).AsParallel().ForEach(i =>
        //    {
        //        tasks.Add(Task.Factory.StartNew(() =>
        //        {
        //            order.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
        //            order.Produced.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

        //            order.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

        //            order.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
        //        }));
        //    });

        //    Task.WaitAll(tasks.ToArray());
        //}

        //[TestMethod]
        //public void UpdateWithRetriesUsingRepo_ShouldSucceed_WhenOrderHasChildCollectionsBeingModifiedUsingUpdateWithRetries()
        //{
        //    order = new Order(new Carton(), 100);
        //    ordersRepository = new OrdersRepository();
        //    //orders = new Orders(ordersRepository);
        //    List<Task> tasks = new List<Task>();

        //    order.ProducibleStatusObservable.Subscribe(ps =>
        //    {
        //        ordersRepository.UpdateWithRetries(order);
        //    });

        //    Enumerable.Range(1, 100).AsParallel().ForEach(i =>
        //    {
        //        tasks.Add(Task.Factory.StartNew(() =>
        //        {
        //            order.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
        //            order.Produced.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

        //            order.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

        //            order.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
        //        }));
        //    });

        //    Task.WaitAll(tasks.ToArray());
        //}

        //[Ignore]
        //[TestMethod]
        //[ExpectedException(typeof(AggregateException))]
        //public void UpdateUsingBiz_ShouldFail_WhenOrderHasChildCollectionsBeingModifiedUsingUpdateWithRetries()
        //{
        //    order = new Order(new Carton(), 100);
        //    ordersRepository = new OrdersRepository();
        //    orders = new Orders(ordersRepository);
        //    List<Task> tasks = new List<Task>();

        //    order.ProducibleStatusObservable.Subscribe(ps =>
        //    {
        //        orders.Update(order);
        //    });

        //    Enumerable.Range(1, 100).AsParallel().ForEach(i =>
        //    {
        //        tasks.Add(Task.Factory.StartNew(() =>
        //        {
        //            order.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
        //            order.Produced.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

        //            order.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

        //            order.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
        //        }));
        //    });

        //    Task.WaitAll(tasks.ToArray());
        //}

        //[TestMethod]
        //public void UpdateWithRetriesUsingBiz_ShouldSucceed_WhenOrderHasChildCollectionsBeingModifiedUsingUpdateWithRetries()
        //{
        //    order = new Order(new Carton(), 100);
        //    ordersRepository = new OrdersRepository();
        //    orders = new Orders(ordersRepository);
        //    List<Task> tasks = new List<Task>();

        //    order.ProducibleStatusObservable.Subscribe(ps =>
        //    {
        //        orders.UpdateWithRetries(order);
        //    });

        //    Enumerable.Range(1, 100).AsParallel().ForEach(i =>
        //    {
        //        tasks.Add(Task.Factory.StartNew(() =>
        //        {
        //            order.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
        //            order.Produced.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

        //            order.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

        //            order.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
        //            order.Failed.Add(Guid.NewGuid());
        //            order.Producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
        //        }));
        //    });

        //    Task.WaitAll(tasks.ToArray());
        //}
    }
}
