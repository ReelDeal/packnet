﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Data;
using PackNet.Data.Carton;
using PackNet.Data.Repositories.MongoDB;

using Testing.Specificity;

namespace DataTest
{
    [TestClass]
    public class LabelPersistentTest
    {
        [TestInitialize]
        public void Setup()
        {
            MongoDbHelpers.TryRegisterDiscriminatorConvention<BasicRestriction<string>>(new RestrictionDiscriminatorConvention());
        }

        [TestMethod]
        public void ShouldSerializeAndDeserialize()
        {
            var subject = new Label
            {
                Id = Guid.NewGuid(),
                PrintData = new Dictionary<string, string>
                {
                    { "test", "test" }
                }
            };

            subject.Restrictions.Add(new BasicRestriction<string>("Test Restriction"));
            subject.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
           
            var repository = new LabelRepository();
            repository.Create(subject);

            var result = repository.Find(subject.Id);
            Specify.That(result).Should.Not.BeNull();
            Specify.That(result.PrintData).Should.BeInstanceOfType(typeof(Dictionary<string, string>));
            Specify.That(result.Restrictions[0]).Should.BeInstanceOfType(typeof(BasicRestriction<string>));
            Specify.That(result.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);

            repository.Delete(subject.Id);
            result = repository.Find(subject.Id);
            Specify.That(result).Should.BeNull();
        }
    }
}
