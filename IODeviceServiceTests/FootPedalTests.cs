﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Utils;
using PackNet.IODeviceService;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Enums;
using PackNet.IODeviceService.FootPedalRelated;
using PackNet.IODeviceService.Intefaces;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

using Testing.Specificity;

namespace IODeviceServiceTests
{
    [TestClass]
    public class FootPedalTests
    {
        private readonly Subject<bool> comMockStatusChangedSubject = new Subject<bool>();
        private Mock<IAggregateMachineService> aggregateMachineService;
        private EventAggregator aggregator;

        private Mock<IIoDeviceCommunicatorFactory> comFactory;
        private Mock<IIoDeviceCommunicator> comMock;

        FootPedalService footPedalService;
        private Mock<ILogger> loggerMock;
        private Mock<IMachineGroupService> machineGroupServiceMock;
        private IInputMessageHandler messageHandler;
        private Mock<IFootPedalRepository> repoMock;
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;

        [TestInitialize]
        public void Setup()
        {
            loggerMock = new Mock<ILogger>();
            aggregator = new EventAggregator();
            serviceLocatorMock = GetServiceLocatorMock();
            aggregateMachineService = new Mock<IAggregateMachineService>();
            repoMock = GetRepoMock();
            machineGroupServiceMock = new Mock<IMachineGroupService>();
            comFactory = GetComFactoryMock();

            messageHandler = new InputMessageHandler(serviceLocatorMock.Object);

            serviceLocatorMock.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(aggregator);
            serviceLocatorMock.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);
            serviceLocatorMock.Setup(m => m.Locate<IAggregateMachineService>()).Returns(aggregateMachineService.Object);
            serviceLocatorMock.Setup(m => m.Locate<IMachineGroupService>()).Returns(machineGroupServiceMock.Object);
            serviceLocatorMock.Setup(m => m.Locate<ILogger>()).Returns(loggerMock.Object);
            serviceLocatorMock.Setup(m => m.Locate<IUICommunicationService>()).Returns(GetUiCommunicationServiceMock());
        }

        private Mock<IFootPedalRepository> GetRepoMock()
        {
            var mock = new Mock<IFootPedalRepository>();

            mock.Setup(m => m.Update(It.IsAny<FootPedal>())).Returns<FootPedal>(fp => fp);
            mock.Setup(m => m.Create(It.IsAny<FootPedal>())).Callback<FootPedal>(fp => fp.Id = Guid.NewGuid());

            return mock;
        }

        private Mock<IIoDeviceCommunicatorFactory> GetComFactoryMock()
        {
            var mock = new Mock<IIoDeviceCommunicatorFactory>();

            comMock = new Mock<IIoDeviceCommunicator>();
            comMock.Setup(m => m.IsConnected).Returns(false);
            comMock.Setup(m => m.ConnectionStatusChangedObservable).Returns(comMockStatusChangedSubject.AsObservable());

            mock.Setup(m => m.GetCommunicatorForMachine(It.IsAny<Guid>())).Returns(comMock.Object);

            return mock;
        }

        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(aggregator);
            mock.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);
            mock.Setup(m => m.Locate<ILogger>()).Returns(loggerMock.Object);

            return mock;
        }

        private IUICommunicationService GetUiCommunicationServiceMock()
        {
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();

            return uiCommunicationServiceMock.Object;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToCreateFootPedalWithTheSameAliasAsAnother()
        {
            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            var eventCalled = false;
            var createCalled = false;
            var logCalled = false;
            var machineID = Guid.NewGuid();

            repoMock.Setup(m => m.WithAlias(It.IsAny<string>()))
                .Returns(new List<FootPedal> { new FootPedal { Alias = "FOOTPEDAL", MachineId = machineID } });
            repoMock.Setup(m => m.Create(It.IsAny<FootPedal>())).Callback(() => { createCalled = true; });

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<FootPedal>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<FootPedal>, bool, bool>(
                    (m, b, b2) =>
                    {
                        var message = m as ResponseMessage<FootPedal>;
                        Specify.That(message).Should.Not.BeNull();
                        Specify.That(message.Result).Should.BeEqualTo(ResultTypes.Exists);
                        eventCalled = true;
                    });

            loggerMock.Setup(m => m.Log(LogLevel.Error, It.IsAny<string>()))
                .Callback<LogLevel, string>((l, m) => { logCalled = true; });

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.CreateFootPedalAccessory,
                Data = new FootPedal
                {
                    Alias = "FOOTPEDAL",
                    MachineId = machineID
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
            Specify.That(logCalled).Should.BeTrue();
            Specify.That(createCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleMGPausePlayListenerAccordingToMachineStatus()
        {
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal>
            {
                new FootPedal
                {
                    Id = Guid.NewGuid(),
                    Alias = "FOOTPEDAL",
                    MachineId = machineId,
                    MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
                }
            });

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid(), CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline });

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            aggregator.GetEvent<Message<MachineGroup>>().DurableSubscribe(m =>
            {
                Specify.That(m.MessageType).Should.BeEqualTo(MachineGroupMessages.ChangeMachineGroupStatus);
                eventCalled = true;
            }, loggerMock.Object);

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();

            comMockStatusChangedSubject.OnNext(true);

            eventCalled = false;
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();

            comMockStatusChangedSubject.OnNext(false);

            eventCalled = false;
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();

            comMockStatusChangedSubject.OnNext(true);

            eventCalled = false;
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotPublishMachineGroupChangeMessageWhenMGIsNotAvaliable()
        {
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal>
            {
                new FootPedal
                {
                    Id = Guid.NewGuid(),
                    Alias = "FOOTPEDAL",
                    MachineId = machineId,
                    MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
                }
            });

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid(), CurrentStatus = MachineGroupUnavailableStatuses.MachineGroupError});

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            aggregator.GetEvent<Message<MachineGroup>>().DurableSubscribe(m =>
            {
                Specify.That(m.MessageType).Should.BeEqualTo(MachineGroupMessages.ChangeMachineGroupStatus);
                eventCalled = true;
            }, loggerMock.Object);

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();

            comMockStatusChangedSubject.OnNext(true);

            eventCalled = false;
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleFootPedalTriggerListenerAccordingToMachineStatus()
        {
            var eventCalled = false;
            var machineId = Guid.NewGuid();
            var fp = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                FootPedalTrigger = new AccessoryPort { Module = 1, Port = 1 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { fp });

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid() });

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            fp.AccessoryStatusObservable.Subscribe(s =>
            {
                if (s == AccessoryStatuses.SignalReceived)
                    eventCalled = true;
            });

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();

            comMockStatusChangedSubject.OnNext(true);

            eventCalled = false;
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();

            comMockStatusChangedSubject.OnNext(false);

            eventCalled = false;
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();

            comMockStatusChangedSubject.OnNext(true);

            eventCalled = false;
            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveTriggerListenerWhenMachineGoesOffline()
        {
            var statusChanged = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                FootPedalTrigger = new AccessoryPort { Module = 1, Port = 1 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal });

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid() });

            comMock.Setup(m => m.IsConnected).Returns(true);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);
            
            Retry.For(() => footpedal.CurrentStatus == AccessoryStatuses.WaitingForSignal, TimeSpan.FromSeconds(1));
            Specify.That(footpedal.CurrentStatus).Should.BeEqualTo(AccessoryStatuses.WaitingForSignal);

            comMockStatusChangedSubject.OnNext(false);

            Retry.For(() => footpedal.CurrentStatus == AccessoryStatuses.Idle, TimeSpan.FromSeconds(1));
            Specify.That(footpedal.CurrentStatus).Should.BeEqualTo(AccessoryStatuses.Idle);

            footpedal.AccessoryStatusObservable.Subscribe(s => { statusChanged = true; });

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };
            aggregator.Publish(message);

            Retry.For(() => statusChanged, TimeSpan.FromSeconds(1));
            Specify.That(statusChanged).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddMgPausePlayListenerWhenCreatingFootPedal()
        {
            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            var statusChanged = false;
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
            };

            footpedal.AccessoryStatusObservable.Subscribe(s =>
            {
                Specify.That(s).Should.BeEqualTo(AccessoryStatuses.WaitingForSignal);
                statusChanged = true;
            });

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid(), CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline});

            comMock.Setup(m => m.IsConnected).Returns(true);

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.CreateFootPedalAccessory,
                Data = footpedal
            });

            Retry.For(() => statusChanged, TimeSpan.FromSeconds(1));

            aggregator.GetEvent<Message<MachineGroup>>().DurableSubscribe(m =>
            {
                Specify.That(m.MessageType).Should.BeEqualTo(MachineGroupMessages.ChangeMachineGroupStatus);
                eventCalled = true;
            }, loggerMock.Object);

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddMgPausePlayListenerWhenUpdatingFootPedal()
        {
            var statusChanged = false;
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                FootPedalTrigger = new AccessoryPort { Module = 1, Port = 1 }
            };

            footpedal.AccessoryStatusObservable.Subscribe(s =>
            {
                Specify.That(s).Should.BeEqualTo(AccessoryStatuses.WaitingForSignal);
                statusChanged = true;
            });

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal });
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns(footpedal);

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid(), CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline });

            comMock.Setup(m => m.IsConnected).Returns(true);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            footpedal.MachineGroupPausePlay = footpedal.FootPedalTrigger;
            footpedal.FootPedalTrigger = null;

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.UpdateFootPedalAccessory,
                Data = footpedal
            });

            Retry.For(() => statusChanged, TimeSpan.FromSeconds(1));

            aggregator.GetEvent<Message<MachineGroup>>().DurableSubscribe(m =>
            {
                Specify.That(m.MessageType).Should.BeEqualTo(MachineGroupMessages.ChangeMachineGroupStatus);
                eventCalled = true;
            }, loggerMock.Object);

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveMgPausePlayListenerWhenUpdatingFootPedal()
        {
            var statusChanged = false;
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal });
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns(footpedal);

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid() });

            comMock.Setup(m => m.IsConnected).Returns(true);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            footpedal.FootPedalTrigger = footpedal.MachineGroupPausePlay;
            footpedal.MachineGroupPausePlay = null;

            footpedal.AccessoryStatusObservable.Subscribe(s =>
            {
                Specify.That(s).Should.BeEqualTo(AccessoryStatuses.Idle);
                statusChanged = true;
            });

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.UpdateFootPedalAccessory,
                Data = footpedal
            });

            Retry.For(() => statusChanged, TimeSpan.FromSeconds(1));

            aggregator.GetEvent<Message<MachineGroup>>().DurableSubscribe(m =>
            {
                Specify.That(m.MessageType).Should.BeEqualTo(MachineGroupMessages.ChangeMachineGroupStatus);
                eventCalled = true;
            }, loggerMock.Object);

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddTriggerListenerWhenCreatingFootPedal()
        {
            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            var statusChanged = false;
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                FootPedalTrigger = new AccessoryPort { Module = 1, Port = 1 }
            };

            footpedal.AccessoryStatusObservable.Subscribe(s =>
            {
                if(s == AccessoryStatuses.WaitingForSignal)
                    statusChanged = true;
                if (s == AccessoryStatuses.SignalReceived)
                    eventCalled = true;
            });

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid() });

            comMock.Setup(m => m.IsConnected).Returns(true);

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.CreateFootPedalAccessory,
                Data = footpedal
            });

            Retry.For(() => statusChanged, TimeSpan.FromSeconds(1));

            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddTriggerListenerWhenUpdatingFootPedal()
        {
            var statusChanged = false;
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
            };

            footpedal.AccessoryStatusObservable.Subscribe(s =>
            {
                if (s == AccessoryStatuses.WaitingForSignal)
                    statusChanged = true;
                if (s == AccessoryStatuses.SignalReceived)
                    eventCalled = true;
            });

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal });
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns(footpedal);

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid() });

            comMock.Setup(m => m.IsConnected).Returns(true);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            footpedal.FootPedalTrigger = footpedal.MachineGroupPausePlay;
            footpedal.MachineGroupPausePlay = null;

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.UpdateFootPedalAccessory,
                Data = footpedal
            });

            Retry.For(() => statusChanged, TimeSpan.FromSeconds(1));
            
            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveTriggerListenerWhenRemovingFootPedal()
        {
            var statusChanged = false;
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                FootPedalTrigger = new AccessoryPort { Module = 1, Port = 1 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal });
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns(footpedal);

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid() });

            comMock.Setup(m => m.IsConnected).Returns(true);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            footpedal.FootPedalTrigger = footpedal.MachineGroupPausePlay;
            footpedal.MachineGroupPausePlay = null;

            footpedal.AccessoryStatusObservable.Subscribe(s =>
            {
                if(s == AccessoryStatuses.WaitingForSignal)
                    statusChanged = true;
                if (s == AccessoryStatuses.SignalReceived)
                    eventCalled = true;
            });

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.UpdateFootPedalAccessory,
                Data = footpedal
            });

            Retry.For(() => statusChanged, TimeSpan.FromSeconds(1));
            
            var message = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(message);

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToCreateFootPedalOnTheSameModulePortCombinationAsAnother()
        {
            var eventCalled = false;
            var createCalled = false;
            var logCalled = false;
            var machineID = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineID,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal });

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            repoMock.Setup(m => m.Create(It.IsAny<FootPedal>())).Callback(() => { createCalled = true; });

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<FootPedal>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<FootPedal>, bool, bool>(
                    (m, b, b2) =>
                    {
                        var message = m as ResponseMessage<FootPedal>;
                        Specify.That(message).Should.Not.BeNull();
                        Specify.That(message.Result).Should.BeEqualTo(ResultTypes.Fail);
                        Specify.That(message.Message).Should.BeEqualTo("AccessoryModuleAndPortInUse");
                        eventCalled = true;
                    });

            loggerMock.Setup(m => m.Log(LogLevel.Error, It.IsAny<string>()))
                .Callback<LogLevel, string>((l, m) => { logCalled = true; });

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.CreateFootPedalAccessory,
                Data = new FootPedal
                {
                    Alias = "FOOTPEDAL1",
                    MachineId = machineID,
                    FootPedalTrigger = new AccessoryPort { Port = 1, Module = 1 }
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
            Specify.That(logCalled).Should.BeTrue();
            Specify.That(createCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToUpdateAccessoryToAModuleAndPortThatIsUsedByAnotherAccessory()
        {
            var eventCalled = false;
            var createCalled = false;
            var logCalled = false;
            var machineID = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineID,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
            };

            var existing = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL1",
                MachineId = machineID,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 2 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal, existing });
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns<Guid>(g => g.Equals(footpedal.Id) ? footpedal : existing);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            repoMock.Setup(m => m.Create(It.IsAny<FootPedal>())).Callback(() => { createCalled = true; });

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<FootPedal>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<FootPedal>, bool, bool>(
                    (m, b, b2) =>
                    {
                        var message = m as ResponseMessage<FootPedal>;
                        Specify.That(message).Should.Not.BeNull();
                        Specify.That(message.Result).Should.BeEqualTo(ResultTypes.Fail);
                        Specify.That(message.Message).Should.BeEqualTo("AccessoryModuleAndPortInUse");
                        eventCalled = true;
                    });

            loggerMock.Setup(m => m.Log(LogLevel.Error, It.IsAny<string>()))
                .Callback<LogLevel, string>((l, m) => { logCalled = true; });

            footpedal.MachineGroupPausePlay.Port = 2;

            aggregator.Publish(new Message<FootPedal>
            {
                MessageType = AccessoryMessages.UpdateFootPedalAccessory,
                Data = footpedal
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
            Specify.That(logCalled).Should.BeTrue();
            Specify.That(createCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveListenersWhenDeletingFootPedal()
        {
            var statusChanged = false;
            var eventCalled = false;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
            };

            var footpedal1 = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL1",
                MachineId = machineId,
                FootPedalTrigger = new AccessoryPort { Module = 1, Port = 2 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal, footpedal1 });
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns<Guid>(g => g.Equals(footpedal.Id) ? footpedal : footpedal1);

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid() });

            comMock.Setup(m => m.IsConnected).Returns(true);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);
            
            Retry.For(() => footpedal.CurrentStatus.Equals(AccessoryStatuses.WaitingForSignal), TimeSpan.FromSeconds(1));
            Retry.For(() => footpedal1.CurrentStatus.Equals(AccessoryStatuses.WaitingForSignal), TimeSpan.FromSeconds(1));

            aggregator.GetEvent<Message<MachineGroup>>().DurableSubscribe(m =>
            {
                Specify.That(m.MessageType).Should.BeEqualTo(MachineGroupMessages.ChangeMachineGroupStatus);
                eventCalled = true;
            }, loggerMock.Object);

            footpedal1.AccessoryStatusObservable.Subscribe(s => { statusChanged = true; });

            footPedalService.DeleteFootPedalsForMachine(machineId);

            var mgPausePlayMessage = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            var triggerMessage = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 2, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(mgPausePlayMessage);
            aggregator.Publish(triggerMessage);

            Retry.For(() => eventCalled || statusChanged, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();
            Specify.That(statusChanged).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddOneMgPausePlayListenerForEachMgPausePlayFootPedalWhenCommunicatorComeOnline()
        {
            var callCount = 0;
            var machineId = Guid.NewGuid();

            var footpedal = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL",
                MachineId = machineId,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 1 }
            };

            var footpedal1 = new FootPedal
            {
                Id = Guid.NewGuid(),
                Alias = "FOOTPEDAL1",
                MachineId = machineId,
                MachineGroupPausePlay = new AccessoryPort { Module = 1, Port = 2 }
            };

            repoMock.Setup(m => m.All()).Returns(new List<FootPedal> { footpedal, footpedal1 });
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns<Guid>(g => g.Equals(footpedal.Id) ? footpedal : footpedal1);

            machineGroupServiceMock.Setup(m => m.FindByMachineId(It.IsAny<Guid>()))
                .Returns(new MachineGroup { Id = Guid.NewGuid(), CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline });

            comMock.Setup(m => m.IsConnected).Returns(false);

            footPedalService = new FootPedalService(serviceLocatorMock.Object, repoMock.Object, messageHandler, comFactory.Object);

            comMockStatusChangedSubject.OnNext(true);

            Retry.For(() => footpedal.CurrentStatus.Equals(AccessoryStatuses.WaitingForSignal), TimeSpan.FromSeconds(1));
            Retry.For(() => footpedal1.CurrentStatus.Equals(AccessoryStatuses.WaitingForSignal), TimeSpan.FromSeconds(1));

            aggregator.GetEvent<Message<MachineGroup>>().DurableSubscribe(m =>
            {
                Specify.That(m.MessageType).Should.BeEqualTo(MachineGroupMessages.ChangeMachineGroupStatus);
                callCount++;
            }, loggerMock.Object);

            var m1 = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 1, InputEventTypes.PositiveEdge)
            };

            var m2 = new Message<InputEvent>
            {
                Data = new InputEvent(machineId, 1, 2, InputEventTypes.PositiveEdge)
            };

            aggregator.Publish(m1);
            aggregator.Publish(m2);

            Retry.For(() => callCount == 2, TimeSpan.FromSeconds(1));
            Specify.That(callCount).Should.BeEqualTo(2);
        }
    }
}