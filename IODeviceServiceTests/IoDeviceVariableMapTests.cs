﻿using PackNet.IODeviceService.Communication;

namespace IODeviceServiceTests
{
    using Testing.Specificity;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class IoDeviceVariableMapTests
    {
        private const string CyclicName = "IODevice";
        
        [TestMethod]
        [TestCategory("Unit")]
        public void VerifyCallBackMappings()
        {
            Specify.That(IoDeviceVariableMap.Instance.IoDeviceCallbackIp.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.IoDeviceCallbackIp.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.IoDeviceCallbackIp.VariableName).Should.BeEqualTo("ServerIp");

            Specify.That(IoDeviceVariableMap.Instance.IoDeviceCallbackPort.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.IoDeviceCallbackPort.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.IoDeviceCallbackPort.VariableName).Should.BeEqualTo("ServerPort");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void VerifyOutputCommandMappings()
        {
            Specify.That(IoDeviceVariableMap.Instance.OutputCommand.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.OutputCommand.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.OutputCommand.VariableName).Should.BeEqualTo("CurrentOutputCommand");
            
            Specify.That(IoDeviceVariableMap.Instance.OutputCommandExecute.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.OutputCommandExecute.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.OutputCommandExecute.VariableName).Should.BeEqualTo("CurrentOutputCommand.Execute");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void VerifyInputCommandMappings()
        {
            Specify.That(IoDeviceVariableMap.Instance.InputCommandExecute.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandExecute.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandExecute.VariableName).Should.BeEqualTo("CurrentInputCommand.Execute");

            Specify.That(IoDeviceVariableMap.Instance.InputCommandInputIndex.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandInputIndex.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandInputIndex.VariableName).Should.BeEqualTo("CurrentInputCommand.Command.InputIndex");

            Specify.That(IoDeviceVariableMap.Instance.InputCommandModuleIndex.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandModuleIndex.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandModuleIndex.VariableName).Should.BeEqualTo("CurrentInputCommand.Command.ModuleNumber");

            Specify.That(IoDeviceVariableMap.Instance.InputCommandType.TaskName).Should.BeEqualTo(CyclicName);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandType.NotifyChanges).Should.BeEqualTo(false);
            Specify.That(IoDeviceVariableMap.Instance.InputCommandType.VariableName).Should.BeEqualTo("CurrentInputCommand.Command.Type");
        }
    }
}
