﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.CodeActivities;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions;

using Testing.Specificity;

namespace IODeviceServiceTests.CodeActivities
{
    [TestClass]
    public class FeedConveyorTests
    {
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IConveyorService> conveyorServiceMock;
        private Mock<IUserNotificationService> userNotificationServiceMock;
        private Mock<ILogger> loggerMock;
        
        private readonly Dictionary<string, PickZone> pickZones = new Dictionary<string, PickZone>();
        private readonly List<Conveyor> conveyors = new List<Conveyor>();
        
        private readonly EmMachine machine = new EmMachine() { Id = Guid.NewGuid() };

        [TestInitialize]
        public void Setup()
        {
            SetupPickZones();
            SetupConveyors();
            loggerMock = new Mock<ILogger>();
            userNotificationServiceMock = GetUserNotificationServiceMock();
            conveyorServiceMock = ConveyorServiceMock();
            serviceLocatorMock = GetServiceLocatorMock();
        }

        private void SetupConveyors()
        {
            conveyors.Add(new Conveyor() { Id = Guid.NewGuid(), MoveCapabilities = new List<ICapability>() { new ConveyorPickzoneCapability(new AccessoryPort()) { PickZone = pickZones["AZ1"] } } });
            conveyors.Add(new Conveyor() { Id = Guid.NewGuid(), MoveCapabilities = new List<ICapability>() { new ConveyorPickzoneCapability(new AccessoryPort()) { PickZone = pickZones["AZ2"] } } });
            conveyors.Add(new Conveyor() { Id = Guid.NewGuid(), ConveyorType = ConveyorType.WasteConveyor, MachineId = machine.Id });
        }

        private void SetupPickZones()
        {
            pickZones.Add("AZ1", new PickZone
            {
                Id = Guid.NewGuid(),
                Alias = "AZ1"
            });

            pickZones.Add("AZ2", new PickZone()
            {
                Id = Guid.NewGuid(),
                Alias = "AZ2"
            });
        }

        private Mock<IUserNotificationService> GetUserNotificationServiceMock()
        {
            var mock = new Mock<IUserNotificationService>();
            return mock;
        }

        private Mock<IConveyorService> ConveyorServiceMock()
        {
            var mock = new Mock<IConveyorService>();

            mock.Setup(m => m.GetConveyorsForMachineGroup(It.IsAny<Guid>())).Returns(conveyors);
            mock.Setup(m => m.GetConveyorsForMachine(It.IsAny<Guid>())).Returns<Guid>(g =>
                conveyors.Where(c => c.MachineId == g));

            return mock;
        }

        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<ILogger>()).Returns(loggerMock.Object);
            mock.Setup(m => m.Locate<IUserNotificationService>()).Returns(userNotificationServiceMock.Object);
            mock.Setup(m => m.Locate<IConveyorService>()).Returns(conveyorServiceMock.Object);

            return mock;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFeedBoxToCorrectPickZone()
        {
            var eventCalled = false;
            var producible = new Carton();
            producible.Restrictions.Add(new ConveyorPickzoneRestriction() { Value = pickZones["AZ2"] });

            var machineProductionData = new MachineProductionData()
            {
                Carton = new Carton() { TrackNumber = 1},
                MachineGroup = Guid.NewGuid()
            };

            conveyorServiceMock.Setup(m => m.FeedConveyorToPickZone(It.Is<Conveyor>(c => ReferenceEquals(c, conveyors.ElementAt(1))), It.IsAny<TimeSpan>(), It.Is<PickZone>(p => p.Alias == "AZ2")))
                .Callback<IConveyor, TimeSpan, PickZone>(
                    (c, t, p) =>
                    {
                        eventCalled = true;
                    });

            var arguments = new Dictionary<string, object>
            {
                { "Producible", producible },
                { "ServiceLocator", serviceLocatorMock.Object },
                { "TimeInMs", 100 }
            };

            var completed = false;
            var wfApp = new WorkflowApplication(new FeedProducibleToPickZone(), arguments)
            {
                Completed = e =>
                {
                    completed = true;
                }
            };

            wfApp.Run();

            Retry.For(() => completed, TimeSpan.FromSeconds(1));
            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));

            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");
            Specify.That(eventCalled).Should.BeTrue("Feed was not called with the correct conveyor");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldWaitForConveyorStatus()
        {
            var conveyor = new Conveyor() { CurrentStatus = AccessoryStatuses.Feeding };

            var workflowArguments = new Dictionary<string, object>()
            {
                { "Accessory", conveyor },
                { "Status", AccessoryStatuses.Idle },
                { "ServiceLocator", serviceLocatorMock.Object }
            };

            var completed = false;
            var wfApp = new WorkflowApplication(new WaitForAccessorieStatus(), workflowArguments)
            {
                Completed = e =>
                {
                    completed = true;
                }
            };

            var thread = new Thread(wfApp.Run);

            thread.Start();
            Thread.Sleep(100);
            Specify.That(completed).Should.BeFalse();
            Thread.Sleep(100);
            conveyor.CurrentStatus = AccessoryStatuses.Idle;

            Retry.For(() => completed, TimeSpan.FromSeconds(1));
            
            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFeedWasteConveyor()
        {
            var eventCalled = false;
            var producible = new Carton() { ProduceOnMachineId = machine.Id };

            conveyorServiceMock.Setup(m => m.FeedConveyor(It.Is<Conveyor>(c => ReferenceEquals(c, conveyors.ElementAt(2))), It.IsAny<TimeSpan>()))
                .Callback<IConveyor, TimeSpan>(
                    (c, t) =>
                    {
                        eventCalled = true;
                    });

            var arguments = new Dictionary<string, object>
            {
                { "Producible", producible },
                { "ServiceLocator", serviceLocatorMock.Object },
                { "TimeInMs", 100 }
            };

            var completed = false;
            var wfApp = new WorkflowApplication(new FeedWasteConveyor(), arguments)
            {
                Completed = e =>
                {
                    completed = true;
                }
            };

            wfApp.Run();

            Retry.For(() => completed, TimeSpan.FromSeconds(1));
            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));

            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");
            Specify.That(eventCalled).Should.BeTrue("Feed was not called with the correct conveyor");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogErrorWhenAWasteConveyorCantBeFound()
        {
            var eventCalled = false;
            var producible = new Carton() { ProduceOnMachineId = Guid.NewGuid() };

            loggerMock.Setup(m => m.Log(LogLevel.Warning, It.IsAny<string>())).Callback<LogLevel, string>((l, s) =>
            {
                eventCalled = s.Contains("Unable to find a Waste conveyor for Machine with Id");
            });

            var arguments = new Dictionary<string, object>
            {
                { "Producible", producible },
                { "ServiceLocator", serviceLocatorMock.Object },
                { "TimeInMs", 100 }
            };

            var completed = false;
            var wfApp = new WorkflowApplication(new FeedWasteConveyor(), arguments)
            {
                Completed = e =>
                {
                    completed = true;
                }
            };

            wfApp.Run();

            Retry.For(() => completed, TimeSpan.FromSeconds(1));
            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));

            Specify.That(completed).Should.BeTrue("The workflow didn't finish.");
            Specify.That(eventCalled).Should.BeTrue("Log was not called");
        }
    }
}
