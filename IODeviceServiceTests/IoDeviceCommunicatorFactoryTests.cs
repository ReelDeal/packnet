﻿using System;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.IODeviceService;
using Testing.Specificity;

namespace IODeviceServiceTests
{
    [TestClass]
    public class IoDeviceCommunicatorFactoryTests
    {
        private Mock<IAggregateMachineService> mockAggregateMachineService;
        private Mock<IServiceLocator> mockServiceLocator;
        private FusionMachine machine;
        private Guid machineId;
        private EventAggregator subscriber;
        private IEventAggregatorPublisher publisher;

        [TestInitialize]
        public void Setup()
        {
            machineId = new Guid("31eedff2-b69d-4079-b9d1-b8b9f4ca84fb");

            machine = new FusionMachine
            {
                Id = machineId,
                PhysicalMachineSettings = new FusionPhysicalMachineSettings()
                {
                    EventNotifierIp = new IPEndPoint(1, 1)
                },
                Port = 80,
                IpOrDnsName = "127.0.0.1"
            };

            mockAggregateMachineService = new Mock<IAggregateMachineService>();
            mockAggregateMachineService.Setup(s => s.FindById(It.IsAny<Guid>())).Returns(machine);

            subscriber = new EventAggregator();
            publisher = subscriber as IEventAggregatorPublisher;

            mockServiceLocator = new Mock<IServiceLocator>();
            mockServiceLocator.Setup(m => m.Locate<IAggregateMachineService>()).Returns(mockAggregateMachineService.Object);
            mockServiceLocator.Setup(m => m.Locate<ILogger>()).Returns(new Mock<ILogger>().Object);
            mockServiceLocator.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(subscriber);
            mockServiceLocator.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(publisher);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateANewIoDeviceCommunicatorIfItDoesNotExist()
        {
            machine.Port = 80;
            machine.IpOrDnsName = "127.0.0.1";
          
            var factory = new IoDeviceCommunicatorFactory(mockServiceLocator.Object);

            var communicator = factory.GetCommunicatorForMachine(machineId);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("127.0.0.1");
            Specify.That(communicator.Port).Should.BeEqualTo(80);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateIpOfCommunicatorIfMachineIpHasChanged()
        {            
            var factory = new IoDeviceCommunicatorFactory(mockServiceLocator.Object);

            var communicator = factory.GetCommunicatorForMachine(machineId);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("127.0.0.1");
            Specify.That(communicator.Port).Should.BeEqualTo(80);

            machine.Port = 85;

            communicator = factory.GetCommunicatorForMachine(machineId);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("127.0.0.1");
            Specify.That(communicator.Port).Should.BeEqualTo(85);

            machine.IpOrDnsName = "10.9.9.24";

            communicator = factory.GetCommunicatorForMachine(machineId);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("10.9.9.24");
            Specify.That(communicator.Port).Should.BeEqualTo(85);

            machine.IpOrDnsName = "10.9.9.50";
            machine.Port = 90;

            communicator = factory.GetCommunicatorForMachine(machineId);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("10.9.9.50");
            Specify.That(communicator.Port).Should.BeEqualTo(90);
        }
    }
}
