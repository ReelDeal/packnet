﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Utils;
using PackNet.IODeviceService;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Enums;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

using Testing.Specificity;

namespace IODeviceServiceTests
{
    [TestClass]
    public class ConveyorTests
    {
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IAggregateMachineService> aggregateMachineService;
        private Mock<IConveyorRepository> repoMock;
        private Mock<ILogger> loggerMock;
        private Mock<IMachineGroupService> machineGroupServiceMock;
        private Mock<IIoDeviceCommunicatorFactory> comFactory;
        private EventAggregator aggregator;

        ConveyorService conveyorService;

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();
            serviceLocatorMock = new Mock<IServiceLocator>();
            aggregateMachineService = new Mock<IAggregateMachineService>();
            repoMock = new Mock<IConveyorRepository>();
            machineGroupServiceMock = new Mock<IMachineGroupService>();
            comFactory = new Mock<IIoDeviceCommunicatorFactory>();

            loggerMock = new Mock<ILogger>();

            serviceLocatorMock.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(aggregator);
            serviceLocatorMock.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);
            serviceLocatorMock.Setup(m => m.Locate<IAggregateMachineService>()).Returns(aggregateMachineService.Object);
            serviceLocatorMock.Setup(m => m.Locate<IMachineGroupService>()).Returns(machineGroupServiceMock.Object);
            serviceLocatorMock.Setup(m => m.Locate<ILogger>()).Returns(loggerMock.Object);
            serviceLocatorMock.Setup(m => m.Locate<IUICommunicationService>()).Returns(GetUiCommunicationServiceMock());
        }

        private IUICommunicationService GetUiCommunicationServiceMock()
        {
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();

            return uiCommunicationServiceMock.Object;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ConveyorServiceShouldRegisterCorrectMessages()
        {
            conveyorService = new ConveyorService(serviceLocatorMock.Object, repoMock.Object, comFactory.Object);
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<Conveyor>(AccessoryMessages.CreateConveyorAccessory));
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<Conveyor>(AccessoryMessages.UpdateConveyorAccessory));
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<Conveyor>(AccessoryMessages.DeleteConveyorAccessory));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToCreateConveyorWithTheSameAliasAsAnother()
        {
            conveyorService = new ConveyorService(serviceLocatorMock.Object, repoMock.Object, comFactory.Object);
            var eventCalled = false;
            var createCalled = false;
            var logCalled = false;
            var machineID = Guid.NewGuid();

            repoMock.Setup(m => m.WithAlias(It.IsAny<string>())).Returns(new List<Conveyor>(){ new Conveyor() { Alias = "CONVEYOR", MachineId = machineID}});
            repoMock.Setup(m => m.Create(It.IsAny<Conveyor>())).Callback(() =>
            {
                createCalled = true;
            });

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<Conveyor>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<Conveyor>, bool, bool>(
                    (m, b, b2) =>
                    {
                        var message = m as ResponseMessage<Conveyor>;
                        Specify.That(message).Should.Not.BeNull();
                        Specify.That(message.Result).Should.BeEqualTo(ResultTypes.Exists);
                        eventCalled = true;
                    });

            loggerMock.Setup(m => m.Log(LogLevel.Error, It.IsAny<string>())).Callback<LogLevel, string>((l, m) =>
            {
                logCalled = true;
            });

            aggregator.Publish(new Message<Conveyor>
            {
                MessageType = AccessoryMessages.CreateConveyorAccessory,
                Data = new Conveyor()
                {
                    Alias = "CONVEYOR",
                    MachineId = machineID
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
            Specify.That(logCalled).Should.BeTrue();
            Specify.That(createCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToCreateConveyorWithPortThatIsInUse()
        {
            var eventCalled = false;
            var createCalled = false;
            var logCalled = false;
            var machineId = Guid.NewGuid();

            var currentConveyor = new Conveyor
            {
                Id = Guid.NewGuid(),
                Alias = "CONVEYOR",
                MachineId = machineId,
                MoveCapabilities = new List<ICapability>() { new ConveyorMoveCapability(new AccessoryPort(){ Module = 1, Port = 1})}
            };

            repoMock.Setup(m => m.All()).Returns(new List<Conveyor> { currentConveyor });

            conveyorService = new ConveyorService(serviceLocatorMock.Object, repoMock.Object, comFactory.Object);

            repoMock.Setup(m => m.Create(It.IsAny<Conveyor>())).Callback(() => { createCalled = true; });

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<Conveyor>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<Conveyor>, bool, bool>(
                    (m, b, b2) =>
                    {
                        var message = m as ResponseMessage<Conveyor>;
                        Specify.That(message).Should.Not.BeNull();
                        Specify.That(message.Result).Should.BeEqualTo(ResultTypes.Fail);
                        Specify.That(message.Message).Should.BeEqualTo("AccessoryModuleAndPortInUse");
                        eventCalled = true;
                    });

            loggerMock.Setup(m => m.Log(LogLevel.Error, It.IsAny<string>()))
                .Callback<LogLevel, string>((l, m) => { logCalled = true; });

            aggregator.Publish(new Message<Conveyor>
            {
                MessageType = AccessoryMessages.CreateConveyorAccessory,
                Data = new Conveyor
                {
                    Alias = "CONVEYOR1",
                    MachineId = machineId,
                    MoveCapabilities = new List<ICapability>() { new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 1 }) }
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
            Specify.That(logCalled).Should.BeTrue();
            Specify.That(createCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPropagateConveyorCapabilitiesToMachine()
        {
            var machineId = Guid.NewGuid();

            var emMachine = new EmMachine()
            {
                Id = machineId
            };

            aggregateMachineService.Setup(m => m.FindById(It.IsAny<Guid>())).Returns(emMachine);
            conveyorService = new ConveyorService(serviceLocatorMock.Object, repoMock.Object, comFactory.Object);

            aggregator.Publish(new Message<Conveyor>
            {
                MessageType = AccessoryMessages.CreateConveyorAccessory,
                Data = new Conveyor
                {
                    Alias = "CONVEYOR1",
                    MachineId = machineId,
                    MoveCapabilities = new List<ICapability>() { new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 1 }) }
                }
            });

            Retry.For(() => emMachine.CurrentCapabilities.Any(), TimeSpan.FromSeconds(1));
            Specify.That(emMachine.CurrentCapabilities.Count()).Should.BeLogicallyEqualTo(1);

            var capability = emMachine.CurrentCapabilities.FirstOrDefault(c => c is ConveyorMoveCapability) as ConveyorMoveCapability;
            Specify.That(capability).Should.Not.BeNull();
            Specify.That(capability.Value.Module == 1 && capability.Value.Port == 1).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveObsoleteCapabilitiesFromMachine()
        {
            var machineId = Guid.NewGuid();

            var emMachine = new EmMachine()
            {
                Id = machineId,
            };
            emMachine.AddOrUpdateCapabilities(new List<ICapability>()
            {
                new ConveyorMoveCapability(new AccessoryPort()),
                new ConveyorMoveCapability(new AccessoryPort()),
                new ConveyorMoveCapability(new AccessoryPort())
            });

            aggregateMachineService.Setup(m => m.FindById(It.IsAny<Guid>())).Returns(emMachine);
            conveyorService = new ConveyorService(serviceLocatorMock.Object, repoMock.Object, comFactory.Object);

            aggregator.Publish(new Message<Conveyor>
            {
                MessageType = AccessoryMessages.CreateConveyorAccessory,
                Data = new Conveyor
                {
                    Alias = "CONVEYOR1",
                    MachineId = machineId,
                    MoveCapabilities = new List<ICapability>() { new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 1 }) }
                }
            });

            Retry.For(() => emMachine.CurrentCapabilities.Any(), TimeSpan.FromSeconds(1));
            Specify.That(emMachine.CurrentCapabilities.Count()).Should.BeLogicallyEqualTo(1);

            var capability = emMachine.CurrentCapabilities.FirstOrDefault(c => c is ConveyorMoveCapability) as ConveyorMoveCapability;
            Specify.That(capability).Should.Not.BeNull();
            Specify.That(capability.Value.Module == 1 && capability.Value.Port == 1).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateCapabilitiesOfMachineWhenUpdatingConveyor()
        {
            var machineId = Guid.NewGuid();

            var emMachine = new EmMachine()
            {
                Id = machineId,
            };

            var localConveyor = new Conveyor
            {
                Alias = "CONVEYOR1",
                MachineId = machineId,
                MoveCapabilities =
                    new List<ICapability>()
                    {
                        new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 1 }),
                        new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 2 }),
                        new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 3 }),
                        new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 4 })
                    }
            };

            aggregateMachineService.Setup(m => m.FindById(It.IsAny<Guid>())).Returns(emMachine);
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns(new Conveyor());
            repoMock.Setup(m => m.All()).Returns(new List<Conveyor> { localConveyor });
            conveyorService = new ConveyorService(serviceLocatorMock.Object, repoMock.Object, comFactory.Object);

            aggregator.Publish(new Message<Conveyor>
            {
                MessageType = AccessoryMessages.UpdateConveyorAccessory,
                Data = new Conveyor
                {
                    Alias = "CONVEYOR1",
                    MachineId = machineId,
                    MoveCapabilities = new List<ICapability>() { new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 1 }) }
                }
            });

            Retry.For(() => emMachine.CurrentCapabilities.Any(), TimeSpan.FromSeconds(1));
            Specify.That(emMachine.CurrentCapabilities.Count()).Should.BeLogicallyEqualTo(1);

            var capability = emMachine.CurrentCapabilities.FirstOrDefault(c => c is ConveyorMoveCapability) as ConveyorMoveCapability;
            Specify.That(capability).Should.Not.BeNull();
            Specify.That(capability.Value.Module == 1 && capability.Value.Port == 1).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateCapabilitiesOfMachineWhenDeletingConveyor()
        {
            var machineId = Guid.NewGuid();

            var emMachine = new EmMachine()
            {
                Id = machineId,
            };
            emMachine.AddOrUpdateCapabilities(new List<ICapability>()
            {
                new ConveyorMoveCapability(new AccessoryPort()),
                new ConveyorMoveCapability(new AccessoryPort()),
                new ConveyorMoveCapability(new AccessoryPort())
            });

            aggregateMachineService.Setup(m => m.FindById(It.IsAny<Guid>())).Returns(emMachine);
            repoMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns(new Conveyor());
            conveyorService = new ConveyorService(serviceLocatorMock.Object, repoMock.Object, comFactory.Object);

            aggregator.Publish(new Message<Conveyor>
            {
                MessageType = AccessoryMessages.DeleteConveyorAccessory,
                Data = new Conveyor
                {
                    Alias = "CONVEYOR1",
                    MachineId = machineId,
                    MoveCapabilities = new List<ICapability>() { new ConveyorMoveCapability(new AccessoryPort() { Module = 1, Port = 1 }) }
                }
            });

            Retry.For(() => emMachine.CurrentCapabilities.Any() == false, TimeSpan.FromSeconds(1));
            Specify.That(emMachine.CurrentCapabilities.Count()).Should.BeLogicallyEqualTo(0);
        }
    }
}
