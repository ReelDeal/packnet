﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using Newtonsoft.Json;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.IODeviceService.Communication;
using PackNet.IODeviceService.DTO;
using PackNet.IODeviceService.Intefaces;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

using Testing.Specificity;

namespace IODeviceServiceTests
{
    [TestClass]
    public class IoDeviceCommunicatorTests
    {
        private IoDeviceCommunicator communicator;

        private Mock<ILogger> loggerMock;
        private Mock<IWebRequestCreator> webRequestCreatorMock;
        private List<Tuple<PacksizePlcVariable, object>> writtenValues;
        private IEventAggregatorSubscriber subscriber;
        private IEventAggregatorPublisher publisher;
        private FusionMachine machine;

        [TestInitialize]
        public void Setup()
        {
            loggerMock = new Mock<ILogger>();
            webRequestCreatorMock = GetWebRequestCreatorMock();
            subscriber = new EventAggregator();
            publisher = subscriber as IEventAggregatorPublisher;
            writtenValues = new List<Tuple<PacksizePlcVariable, object>>();
            machine = new FusionMachine()
            {
                Id = Guid.NewGuid(),
                PhysicalMachineSettings = new FusionPhysicalMachineSettings()
                {
                    EventNotifierIp = new IPEndPoint(1, 1)
                }

            };
            communicator = new IoDeviceCommunicator(machine, webRequestCreatorMock.Object, IoDeviceVariableMap.Instance, loggerMock.Object, subscriber, publisher);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void EnableOutputWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 3};
            communicator.EnableOutputSignal(accessory);

            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.OutputCommand, OutputTypes.Enable, 2, 1, 0, 0)).Should.BeTrue();
            Specify.That(VerifyLastWrittenValue(IoDeviceVariableMap.Instance.OutputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void DisableOutputWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 3 };
            communicator.DisableOutputSignal(accessory);

            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.OutputCommand, OutputTypes.Disable, 2, 1, 0, 0)).Should.BeTrue();
            Specify.That(VerifyLastWrittenValue(IoDeviceVariableMap.Instance.OutputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void EnableOutputTemporarilyWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 3 };
            communicator.EnableOutputSignalTemporarily(accessory, TimeSpan.FromSeconds(5));

            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.OutputCommand, OutputTypes.TemporaryEnable, 2, 1, 0, 5000)).Should.BeTrue();
            Specify.That(VerifyLastWrittenValue(IoDeviceVariableMap.Instance.OutputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void DisableOutputTemporarilyWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 3 };
            communicator.DisableOutputSignalTemporarily(accessory, TimeSpan.FromSeconds(5));

            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.OutputCommand, OutputTypes.TemporaryDisable, 2, 1, 0, 5000)).Should.BeTrue();
            Specify.That(VerifyLastWrittenValue(IoDeviceVariableMap.Instance.OutputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OsicillateOutputWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 3 };
            communicator.OscillateOutputSignal(accessory, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(3));

            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.OutputCommand, OutputTypes.OscillatingSignal, 2, 1, 5000, 3000)).Should.BeTrue();
            Specify.That(VerifyLastWrittenValue(IoDeviceVariableMap.Instance.OutputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AddSinglePositiveEdgeListenerWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 2 };
            communicator.AddSinglePositiveEdgeListener(accessory);

            Specify.That(writtenValues.Count).Should.BeEqualTo(4);
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandType, InputTypes.SinglePositiveEdge)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandInputIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandModuleIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AddSingleNegativeEdgeListenerWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 2 };
            communicator.AddSingleNegativeEdgeListener(accessory);

            Specify.That(writtenValues.Count).Should.BeEqualTo(4);
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandType, InputTypes.SingleNegativeEdge)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandInputIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandModuleIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AddMultiplePositiveEdgeListenerWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 2 };
            communicator.AddMultiplePositiveEdgeListener(accessory);

            Specify.That(writtenValues.Count).Should.BeEqualTo(4);
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandType, InputTypes.MultiplePositiveEdge)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandInputIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandModuleIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AddMultipleNegativeEdgeListenerWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 2 };
            communicator.AddMultipleNegativeEdgeListener(accessory);

            Specify.That(writtenValues.Count).Should.BeEqualTo(4);
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandType, InputTypes.MultipleNegativeEdge)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandInputIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandModuleIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void RemoveInputListenerWritesCorrectValuesToPlc()
        {
            var accessory = new AccessoryPort() { Module = 2, Port = 2 };
            communicator.RemoveInputListener(accessory);

            Specify.That(writtenValues.Count).Should.BeEqualTo(4);
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandType, InputTypes.RemoveMultipleEdge)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandInputIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandModuleIndex, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(IoDeviceVariableMap.Instance.InputCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishInputEvent_WhenInputMessageIsReceivedFromPlc()
        {
            InputEvent receivedInputEvent = null;
            subscriber.GetEvent<IMessage<InputEvent>>().DurableSubscribe(m => receivedInputEvent = m.Data);

            Specify.That(receivedInputEvent).Should.BeNull();

            var inputFromPlc = new PlcInputEvent();
            inputFromPlc.InputIndex = 1;
            inputFromPlc.Type = 0;

            var eventArgs = new VariableChangedEventArgs(IoDeviceVariableMap.Instance.InputEventMessageContainer.VariableName, JsonConvert.SerializeObject(inputFromPlc));

            publisher.Publish(new Message<VariableChangedEventArgs>() {Data = eventArgs} );

            Thread.Sleep(100);

            Specify.That(receivedInputEvent).Should.Not.BeNull();
            Specify.That(receivedInputEvent.Port).Should.BeEqualTo(2);
            Specify.That(receivedInputEvent.Sender).Should.BeEqualTo(machine.Id);
            Specify.That(receivedInputEvent.Type).Should.BeEqualTo(InputEventTypes.PositiveEdge);
        }

        private bool VerifyLastWrittenValue(PacksizePlcVariable variable, object value)
        {
            var lastWrite = writtenValues.Last();
            return lastWrite.Item1.Equals(variable) && lastWrite.Item2.Equals(value);
        }

        private bool VerifyWrittenValue(PacksizePlcVariable variable, OutputTypes type, int outputIndex, int moduleNumber, int t1, int t2)
        {
            var variableWritten = writtenValues.Any(writtenValue => writtenValue.Item1.Equals(variable));
            if (!variableWritten)
            {
                Trace.WriteLine("Variable " + variable + " was not written to plc");
                return false;
            }

            var correctValueWritten = writtenValues.Any(writtenValue =>
            {
                var correctVariable = writtenValue.Item1.Equals(variable);

                var value = writtenValue.Item2 as OutputCommandTrigger;

                if (value == null)
                    return false;

                return value.Command.Type == type &&
                       value.Command.OutputIndex == outputIndex &&
                       value.Command.ModuleNumber == moduleNumber &&
                       value.Command.T1 == t1 &&
                       value.Command.T2 == t2 &&
                       correctVariable;

            });

            if (!correctValueWritten)
            {
                Trace.WriteLine("Variable " + variable + " was written but the value was not correct");
            }
            return correctValueWritten;
        }

        private bool VerifyWrittenValue(PacksizePlcVariable variable, object value)
        {
            var variableWritten = writtenValues.Any(writtenValue => writtenValue.Item1.Equals(variable));
            if (!variableWritten)
            {
                Trace.WriteLine("Variable " + variable + " was not written to plc");
                return false;
            }

            var correctValueWritten = writtenValues.Any(writtenValue => writtenValue.Item1.Equals(variable) && writtenValue.Item2.Equals(value));
            if (!correctValueWritten)
            {
                Trace.WriteLine("Variable " + variable + " was written but the value " + value + " was not equal");
            }
            return correctValueWritten;
        }

        private Mock<IWebRequestCreator> GetWebRequestCreatorMock()
        {
            var mock = new Mock<IWebRequestCreator>();

            mock.Setup(m => m.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), It.IsAny<object>()))
                .Returns<PacksizePlcVariable, object>((variable, value) =>
                {
                    writtenValues.Add(new Tuple<PacksizePlcVariable, object>(variable, value));
                    return new WebRequestTester(true);
                });

            mock.Setup(m => m.CreateGetRequest(It.IsAny<PacksizePlcVariable>())).Returns<PacksizePlcVariable>(v => v == IoDeviceVariableMap.Instance.OutputCommandExecute || v == IoDeviceVariableMap.Instance.InputCommandExecute ? new WebRequestTester(false) : new WebRequestTester(true));

            mock.Setup(m => m.ServerAddress).Returns("127.0.0.1");
            return mock;
        }

        public class WebRequestTester : WebRequest
        {
            readonly bool value;

            public WebRequestTester(bool value)
            {
                this.value = value;
            }

            public override WebResponse GetResponse()
            {
                return new WebResponseTester(value);
            }
        }

        public class WebResponseTester : WebResponse
        {
            readonly bool value;

            public WebResponseTester(bool value)
            {
                this.value = value;

            }

            public override Stream GetResponseStream()
            {
                return ("{"+value.ToString().ToLower()+"}").ToStream();
            }

            public override void Close()
            {
            }
        }

        public class PlcInputEvent
        {
            public int InputIndex { get; set; }
            public int Type { get; set; }
        }
    }
}
