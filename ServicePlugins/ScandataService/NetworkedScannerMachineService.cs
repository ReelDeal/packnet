﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;

namespace PackNet.ScandataService
{
    [Export(typeof(IMachineService<IMachine>))]
    public class NetworkedScannerMachineService : INetworkedScannerMachineService
    {
        private readonly INetworkScannerMachines networkScannerMachines;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IServiceLocator serviceLocator;
        private readonly ILogger logger;
        private IAggregateMachineService machineService;
        private IMachineGroupService machineGroupService;

        protected Subject<BarcodeScannerMachine> machineErrorOccuredObservable = new Subject<BarcodeScannerMachine>();

        public string Name { get { return "BarcodeScannerMachineService"; } }

        private IMachineGroupService MachineGroupService
        {
            get { return machineGroupService ?? (machineGroupService = serviceLocator.Locate<IMachineGroupService>()); }
        }

        [ImportingConstructor]
        public NetworkedScannerMachineService(IServiceLocator serviceLocator, ILogger logger)
        {
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            networkScannerMachines = new NetworkScannerMachines(new NetworkScannerRepository(), serviceLocator, publisher, logger);
            this.serviceLocator = serviceLocator;
            this.logger = logger;
            machineErrorOccuredObservable = new Subject<BarcodeScannerMachine>();

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<BarcodeScannerMachine>(NetworkScannerMachineMessages.CreateNetworkScanner);
            NetworkScannerMachineMessages.CreateNetworkScanner
                .OnMessage<BarcodeScannerMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        BarcodeScannerMachine createdBarcodeScannerMachine = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachinePortConfiguration(msg.Data);

                            if (result == ResultTypes.Success)
                            {
                                createdBarcodeScannerMachine = Create(msg.Data);
                            }
                        }
                        catch (Exception e)
                        {
                            logger.Log(LogLevel.Error, "CreateNetworkScanner threw an exception", e.ToString());
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }

                        uiCommunicationService.SendMessageToUI(new ResponseMessage<BarcodeScannerMachine>
                        {
                            MessageType = MachineMessages.MachineCreated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = createdBarcodeScannerMachine
                        });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<BarcodeScannerMachine>(NetworkScannerMachineMessages.DeleteNetworkScanner);
            NetworkScannerMachineMessages.DeleteNetworkScanner
                .OnMessage<BarcodeScannerMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        try
                        {
                            Delete(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is RelationshipExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<BarcodeScannerMachine>
                        {
                            MessageType = MachineMessages.MachineDeleted,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = msg.Data
                        });
                        publisher.Publish(new Message { MessageType = MachineMessages.GetMachines });
                    }
                );

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<BarcodeScannerMachine>(NetworkScannerMachineMessages.UpdateNetworkScanner);
            NetworkScannerMachineMessages.UpdateNetworkScanner
                .OnMessage<BarcodeScannerMachine>(subscriber, logger,
                    msg =>
                    {
                        var result = ResultTypes.Success;
                        BarcodeScannerMachine updatedBarcodeScannerMachine = null;
                        try
                        {
                            result = machineService.ValidateNetworkMachinePortConfiguration(msg.Data);

                            if (result == ResultTypes.Success)
                                updatedBarcodeScannerMachine = Update(msg.Data);
                        }
                        catch (Exception e)
                        {
                            result = (e is ItemExistsException) ? ResultTypes.Exists : ResultTypes.Fail;
                        }
                        uiCommunicationService.SendMessageToUI(new ResponseMessage<BarcodeScannerMachine>
                        {
                            MessageType = MachineMessages.MachineUpdated,
                            Result = result,
                            ReplyTo = msg.ReplyTo,
                            Data = updatedBarcodeScannerMachine
                        });
                    }
                );

            serviceLocator.ServiceAddedObservable.Subscribe(ServiceAdded);
        }

        private void ServiceAdded(IService service)
        {
            if (service is IAggregateMachineService)
            {
                machineService = service as IAggregateMachineService;
            }
        }

        public void Dispose()
        {

        }

        public IEnumerable<BarcodeScannerMachine> Machines { get { return networkScannerMachines.GetMachines(); } }

        public void Produce(Guid machineId, IProducible producible)
        {
            throw new NotImplementedException();
        }

        [BsonIgnore]
        [JsonIgnore]
        public IObservable<BarcodeScannerMachine> MachineErrorOccuredObservable { get { return machineErrorOccuredObservable.AsObservable(); } }

        public bool CanProduce(Guid machineId, IProducible producible)
        {
            throw new NotImplementedException();
        }

        public BarcodeScannerMachine Create(BarcodeScannerMachine machine)
        {
            machine = networkScannerMachines.Create(machine);
            return machine;
        }

        public void Delete(BarcodeScannerMachine machine)
        {
            networkScannerMachines.Delete(machine);
        }

        public BarcodeScannerMachine Update(BarcodeScannerMachine machine)
        {
            return networkScannerMachines.Update(machine);
        }

        public IEnumerable<BarcodeScannerMachine> GetMachines()
        {
            return networkScannerMachines.GetMachines();
        }

        public BarcodeScannerMachine Find(Guid machineId)
        {
            return networkScannerMachines.Find(machineId);
        }
    }
}
