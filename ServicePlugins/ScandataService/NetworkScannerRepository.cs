﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;

namespace PackNet.ScandataService
{
    public class NetworkScannerRepository : MongoDbRepository<BarcodeScannerMachine>, INetworkScannerRepository 
    {
        public NetworkScannerRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "BarcodeScanners")
        {
        }
    }
}
