﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reactive.Linq;

using PackNet.Common;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Scanning;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.Network;
using PackNet.Common.Utils;
using PackNet.ScandataService.L10N;
using PackNet.ScandataService.Scanning;

namespace PackNet.ScandataService
{
    [Export(typeof(IService))]
    public class ScanDataTranslatorService : IScanDataTranslatorService
    {
        private readonly IMachineGroupService machineGroupService;
        private readonly IProductionGroupService productionGroupService;
        private readonly IEventAggregatorPublisher eventAggregatorPublisher;
        private readonly IServiceLocator serviceLocator;
        private readonly IUserNotificationService userNotificationService;
        private readonly ILogger logger;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorSubscriber eventAggregatorSubscriber;
        private IAggregateMachineService machineService;
        private IBoxLastSelectionAlgorithmService boxLastSelectionAlgorithmService;
        private IScanToCreateSelectionAlgorithmService scanToCreateSelectionAlgorithmService;
        private const string scanDataWorkflowPath = "%PackNetWorkflows%\\ScanData";

        [ImportingConstructor]
        public ScanDataTranslatorService(IServiceLocator serviceLocator, ILogger logger)
        {
            this.serviceLocator = serviceLocator;
            this.logger = logger;

            machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            productionGroupService = serviceLocator.Locate<IProductionGroupService>();
            eventAggregatorPublisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            eventAggregatorSubscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            serviceLocator.TryLocate(out machineService);
            serviceLocator.TryLocate(out boxLastSelectionAlgorithmService);
            serviceLocator.TryLocate(out scanToCreateSelectionAlgorithmService);
            
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<CreateCartonTrigger>(CartonMessages.CreateCartonTrigger);
            eventAggregatorSubscriber.GetEvent<IMessage<CreateCartonTrigger>>()
                .Where(m => m.MessageType == CartonMessages.CreateCartonTrigger)
                .DurableSubscribe(msg => OnCreateCartonTrigger(msg.Data), logger);

            eventAggregatorSubscriber.GetEvent<SocketListenerMessage>()
                .DurableSubscribe(SocketListenerMessageReceived, logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(NetworkScannerMachineMessages.GetScanDataWorkflows);
            eventAggregatorSubscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == NetworkScannerMachineMessages.GetScanDataWorkflows)
                .DurableSubscribe(msg =>
                {
                    var path = DirectoryHelpers.ReplaceEnvironmentVariables(scanDataWorkflowPath);
                    logger.Log(LogLevel.Trace, Strings.ScanDataTranslatorService_ScanDataTranslatorService_Loading_ScanData_workflows_from_path__0_, path);
                    var payload = Directory.EnumerateFiles(path, "*.xaml")
                        .Select(f =>
                                new KeyValuePair<string, string>(Path.GetFileNameWithoutExtension(f),
                                    Path.Combine(scanDataWorkflowPath, Path.GetFileName(f)))).ToList();

                    uiCommunicationService.SendMessageToUI(new ResponseMessage<List<KeyValuePair<string, string>>>
                    {
                        MessageType = NetworkScannerMachineMessages.ScanDataWorkflows,
                        ReplyTo = msg.ReplyTo,
                        Data = payload
                    });
                }, logger);

            serviceLocator.ServiceAddedObservable.Subscribe(LoadService);
            serviceLocator.RegisterAsService(this);
        }

        private void LoadService(IService serv)
        {
            var blService = serv as IBoxLastSelectionAlgorithmService;
            if (blService != null)
            {
                boxLastSelectionAlgorithmService = blService;
                return;
            }

            var s2cService = serv as IScanToCreateSelectionAlgorithmService;
            if (s2cService != null)
            {
                scanToCreateSelectionAlgorithmService = s2cService;
                return;
            }

            var mservice = serv as IAggregateMachineService;
            if (mservice != null)
            {
                machineService = mservice;
            }
        }

        private void SocketListenerMessageReceived(SocketListenerMessage msg)
        {
            var machine = machineService.Machines.FirstOrDefault(m => m.Id == msg.MachineId) as BarcodeScannerMachine;
            if (machine == null)
            {
                logger.Log(LogLevel.Error, "Invalid machine id {0}, scanner could not be looked up", msg.MachineId);
                return;
            }

            var mg = machineGroupService.FindByMachineId(msg.MachineId);

            if (mg == null)
            {
                userNotificationService.SendNotificationToSystem(NotificationSeverity.Error,
                    string.Format("Barcode Scanner Configuration Error: Scanner {0} is not currently assigned to a machine group",
                        machine.Alias), "");
                return;
            }
            var pg = productionGroupService.GetProductionGroupForMachineGroup(mg.Id);

            if (pg == null)
            {
                userNotificationService.SendNotificationToSystem(NotificationSeverity.Error,
                    string.Format(
                        "Barcode Scanner Configuration Error: Scanner {0} is a member of machine group {1} but that machine group is not currently assigned to a production group",
                        machine.Alias, mg.Alias), "");
                return;
            }
            var cleanMessage = SanitizeBarcode(msg.Message);

            var trigger = new Message<CreateCartonTrigger>
            {
                MessageType = MessageTypes.ScanTriggerSuccess,
                MachineGroupId = mg.Id,
                Data = new CreateCartonTrigger
                {
                    ProductionGroupAlias = pg.Alias,
                    BarcodeData = cleanMessage,
                    TriggeredOn = msg.MessageRecievedTime
                }
            };
            
            if (machine.NotifyBarcodeToUIOnManualMode && mg.ProductionMode == MachineGroupProductionModes.ManualProductionMode)
            {
                // Send message to the UI
                uiCommunicationService.SendMessageToUI(trigger);
            }
            else
            {
                OnCreateCartonTrigger(trigger.Data);
            }
        }

        private static string SanitizeBarcode(string message)
        {
            return message.Replace("\r", String.Empty).Replace("\n", string.Empty);
        }

        private void OnCreateCartonTrigger(IScanTrigger trigger)
        {
            var pg = productionGroupService.FindByAlias(trigger.ProductionGroupAlias);
            var scanTriggerType = typeof(ScanTrigger);
            var workflow = Path.Combine(Path.GetDirectoryName(scanTriggerType.Assembly.Location), @"DispatchScanTrigger.xaml");

            var inArgs = new Dictionary<string, object>()
            {
                {"Publisher",eventAggregatorPublisher},
                {"ServiceLocator",serviceLocator},
                {"ProductionGroup", pg},
                {"SelectionAlgorithm", pg.SelectionAlgorithm},
                {"ScanInfo",trigger}
            };

            WorkflowLifetime lt = null;
            var traceService = serviceLocator.Locate<IWorkflowTrackingService>();
            if (traceService != null)
            {
                var additionalInfo = new Dictionary<string, object>
                {
                    {"ProductionGroup", pg},
                    {"ScanInfo",trigger}
                };
                lt = traceService.Factory(ScanDataWorkflowType.ScanDataWorkflow, workflow, additionalInfo);
            }

            WorkflowHelper.InvokeWorkFlow(logger, workflow, scanTriggerType.Assembly, inArgs, userNotificationService, lt);
        }

        public void Dispose()
        {
        }

        public string Name { get { return "ScanDataTranslatorService"; } }
    }

    public class ScanDataWorkflowType : WorkflowTypes
    {
        public static readonly ScanDataWorkflowType ScanDataWorkflow = new ScanDataWorkflowType("ScanDataWorkflow");

        protected ScanDataWorkflowType(string name)
            : base(0, name)
        {
        }

        protected ScanDataWorkflowType(int value, string name)
            : base(value, name)
        {
        }
    }
}
