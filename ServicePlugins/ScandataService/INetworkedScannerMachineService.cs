﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.ScandataService
{
    public interface INetworkedScannerMachineService : IMachineService<BarcodeScannerMachine>
    {
        BarcodeScannerMachine Create(BarcodeScannerMachine machine);
        void Delete(BarcodeScannerMachine machine);
        BarcodeScannerMachine Update(BarcodeScannerMachine machine);

        IEnumerable<BarcodeScannerMachine> GetMachines();

        BarcodeScannerMachine Find(Guid machineId);
    }
}