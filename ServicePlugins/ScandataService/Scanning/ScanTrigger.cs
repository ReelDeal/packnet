﻿using System;

using PackNet.Common.Interfaces.DTO.Scanning;

namespace PackNet.ScandataService.Scanning
{
	public class ScanTrigger : IScanTrigger
	{
		public DateTime TriggeredOn { get; set; }
		public string ProductionGroupAlias { get; set; }
		public string BarcodeData { get; set; } //TODO: Rename this to CustomerUniqueId//
	}
}