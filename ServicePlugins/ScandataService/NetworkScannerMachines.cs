﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Network.Scanners;

namespace PackNet.ScandataService
{
    public class NetworkScannerMachines : INetworkScannerMachines
    {
        private readonly INetworkScannerRepository barcodeScannerRepository;
        protected readonly IServiceLocator serviceLocator;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private readonly ConcurrentList<BarcodeScannerMachine> createdMachines = new ConcurrentList<BarcodeScannerMachine>();
        private readonly ConcurrentDictionary<Guid, GenericNetworkScannerListener> listeners = new ConcurrentDictionary<Guid, GenericNetworkScannerListener>();

        public NetworkScannerMachines(INetworkScannerRepository barcodeScannerRepository,
                                IServiceLocator serviceLocator,
                                IEventAggregatorPublisher publisher,
                                ILogger logger)
        {
            this.barcodeScannerRepository = barcodeScannerRepository;
            this.serviceLocator = serviceLocator;
            this.publisher = publisher;
            this.logger = logger;

            createdMachines.AddRange(barcodeScannerRepository.All());
            createdMachines.ForEach(StartMachineCommunication);
        }

        private void TearDownMachineCommunication(INetworkConnectedMachine machine)
        {
            GenericNetworkScannerListener listener;
            if (listeners.TryRemove(machine.Id, out listener))
            {
                listener.Dispose();
            }
        }

        private void StartMachineCommunication(INetworkConnectedMachine machine)
        {
            var listener = new GenericNetworkScannerListener(machine.Port, machine.Id, publisher, logger);
            listener.Start();
            machine.CurrentStatus = MachineStatuses.MachineOnline;
            listeners.TryAdd(machine.Id, listener);
        }

        public BarcodeScannerMachine Create(BarcodeScannerMachine machine)
        {
            if (FindByAlias(machine.Alias) != null)
                throw new ItemExistsException("Invalid Machine, Machine Name already exists");

            barcodeScannerRepository.Create(machine);
            createdMachines.Add(machine);
            StartMachineCommunication(machine);

            return machine;
        }

        public void Delete(BarcodeScannerMachine machine)
        {
            var machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            var machineGroup = machineGroupService.FindByMachineId(machine.Id);

            if (machineGroup == null)
            {
                RemoveCreatedMachine(machine);
                barcodeScannerRepository.Delete(machine);
                TearDownMachineCommunication(machine);
            }
            else
            {
                throw new RelationshipExistsException(string.Format("Unable to delete, Machine '{0}' is related to the Machine Group '{1}'", machine.Alias, machineGroup.Alias));
            }
        }

        public BarcodeScannerMachine Update(BarcodeScannerMachine machine)
        {
            BarcodeScannerMachine existingMachine = FindByAlias(machine.Alias);

            if (existingMachine != null && existingMachine.Id != machine.Id)
                throw new ItemExistsException("Invalid Machine, Machine Name already exists");

            RemoveCreatedMachine(machine);
            createdMachines.Add(machine);
            TearDownMachineCommunication(machine);
            StartMachineCommunication(machine);
            return barcodeScannerRepository.Update(machine);
        }

        public BarcodeScannerMachine Find(Guid machineId)
        {
            var createdMachine = createdMachines.FirstOrDefault(m => m.Id == machineId);
            if (createdMachine != null)
                return createdMachine;

            return barcodeScannerRepository.Find(machineId);
        }

        public BarcodeScannerMachine FindByAlias(string alias)
        {
            return GetMachines().FirstOrDefault(g => g.Alias != null && g.Alias.ToLowerInvariant() == alias.ToLowerInvariant());
        }

        public IEnumerable<BarcodeScannerMachine> GetMachines()
        {
            return createdMachines;
        }

        private void RemoveCreatedMachine(BarcodeScannerMachine machine)
        {
            var createdMachine = createdMachines.FirstOrDefault(m => m.Id == machine.Id);
            if (createdMachine != null)
            {
                createdMachines.Remove(createdMachine);
            }
        }
    }
}