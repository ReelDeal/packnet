﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.ScandataService
{
    public interface INetworkScannerRepository : IRepository<BarcodeScannerMachine>
    {
    }
}