﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.ScandataService
{
    public interface INetworkScannerMachines
    {
        BarcodeScannerMachine Create(BarcodeScannerMachine machine);
        void Delete(BarcodeScannerMachine machine);
        BarcodeScannerMachine Update(BarcodeScannerMachine machine);
        BarcodeScannerMachine Find(Guid machineId);
        BarcodeScannerMachine FindByAlias(string alias);

        IEnumerable<BarcodeScannerMachine> GetMachines();
    }
}