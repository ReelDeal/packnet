﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Scanning;
using PackNet.Common.Interfaces.DTO.ScanToCreate;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Plugin.ArticleService;
using PackNet.Plugin.ArticleService.Enums;

namespace PackNet.ScandataService.WorkFlowCodeActivities
{

    public sealed class PublishScanToCreateImport : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<IEnumerable<string>> ScanDataParts { get; set; }
        [RequiredArgument]
        public InArgument<Article<IProducible>> Article { get; set; }
        [RequiredArgument]
        public InArgument<Guid> ProductionGroupId { get; set; }
        [RequiredArgument]
        public InArgument<IScanTrigger> ScanTrigger { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);
            var scanData = context.GetValue(ScanTrigger);
            var scanDataParts = context.GetValue(ScanDataParts).ToList();
            var article = context.GetValue(Article);
            var productionGroupId = context.GetValue(ProductionGroupId);

            var publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            var quantity = scanDataParts.Count() == 1 ? 1 : Int32.Parse(scanDataParts.ElementAt(1));
            var producibles = new List<IProducible>();
            
            var scanToCreateProducible = new ScanToCreateProducible(article.Producible, quantity);
            scanToCreateProducible.BarcodeData = scanData.BarcodeData;
            scanToCreateProducible.Restrictions.Add(new ProductionGroupSpecificRestriction(productionGroupId));
            scanToCreateProducible.CustomerUniqueId = article.Alias;
            producibles.Add(scanToCreateProducible);
            

            var msg = new ImportMessage<IEnumerable<IProducible>>
            {
                Data = producibles,
                ImportType = ImportTypes.ScanToCreate,
                SelectionAlgorithmType = SelectionAlgorithmTypes.ScanToCreate,
                ProductionGroupId = productionGroupId,
                MessageType = MessageTypes.DataImported
            };

            publisher.Publish(msg);
        }
    }
}
