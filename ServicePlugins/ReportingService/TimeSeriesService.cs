﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel.Composition;
//using System.Linq;
//using System.Reactive.Linq;

//using Newtonsoft.Json;

//using PackNet.Common.Interfaces;
//using PackNet.Common.Interfaces.DTO.Messaging;
//using PackNet.Common.Interfaces.Enums;
//using PackNet.Common.Interfaces.Eventing;
//using PackNet.Common.Interfaces.ExtensionMethods;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Services;
//using PackNet.ReportingService.DTO;
//using PackNet.ReportingService.Repositories;

//namespace PackNet.ReportingService
//{
//    [Export(typeof(IService))]
//    public class TimeSeriesService : IService
//    {
//        private readonly ILogger logger;

//        private TimeSeriesRecorder recorder;

//        private readonly ITimeSeriesDataRepository repository;
//        private readonly ITimeSeriesConfigurationRepository configurationRepository;
//        private readonly IUICommunicationService uiCommunicationService;

//        public string Name { get { return "TimeSeriesService"; } }

//        [ImportingConstructor]
//        public TimeSeriesService(IServiceLocator serviceLocator)
//        {           
//            repository = new TimeSeriesDataRepository();
//            recorder = new TimeSeriesRecorder(serviceLocator.Locate<IEventAggregatorSubscriber>(), repository, this.logger);

//            configurationRepository = new TimeSeriesConfigurationRepository();
            
//            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
//            logger = serviceLocator.Locate<ILogger>();

//            SetupListeners();
//        }

//        private void SetupListeners()
//        {
//            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(TimeSeriesMessages.ConfigurationRequest);
//            uiCommunicationService.UIEventObservable.Where(e => e.Item1 == TimeSeriesMessages.ConfigurationRequest)
//                .DurableSubscribe(
//                    msg =>
//                    {
//                        var data = JsonConvert.DeserializeObject<Message<Guid>>(msg.Item2);

//                        uiCommunicationService.SendMessageToUI(new Message<TimeSeriesConfiguration>()
//                        {
//                            Data = configurationRepository.Find(data.Data),
//                            MessageType = TimeSeriesMessages.UiResponseLookupTable[(TimeSeriesMessages)msg.Item1]
//                        });
//                    }, logger);

//            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(TimeSeriesMessages.NewConfiguration);
//            uiCommunicationService.UIEventObservable.Where(e => e.Item1 == TimeSeriesMessages.NewConfiguration)
//                .DurableSubscribe(msg =>
//                {
//                    var data = JsonConvert.DeserializeObject<Message<Guid>>(msg.Item2);

//                    var config = new TimeSeriesConfiguration()
//                    {
//                        Id = data.Data
//                    };
//                    configurationRepository.Create(config);

//                    uiCommunicationService.SendMessageToUI(new Message<TimeSeriesConfiguration>()
//                    {
//                        Data = config,
//                        MessageType = TimeSeriesMessages.UiResponseLookupTable[(TimeSeriesMessages)msg.Item1]
//                    });
//                }, logger);

//            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<TimeSeriesConfiguration>(TimeSeriesMessages.ConfigurationUpdate);
//            uiCommunicationService.UIEventObservable.Where(e => e.Item1 == TimeSeriesMessages.ConfigurationUpdate)
//                .DurableSubscribe(
//                    msg =>
//                    {
//                        var data = JsonConvert.DeserializeObject<Message<TimeSeriesConfiguration>>(msg.Item2);
//                        var configObject = configurationRepository.Update(data.Data);

//                        if (configObject != null)
//                        {
//                            PublishReportToUi(TimeSeriesMessages.UiResponseLookupTable[(TimeSeriesMessages)msg.Item1], configObject);
//                        }

//                    }, logger);

//            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(TimeSeriesMessages.WasteReport);
//            uiCommunicationService.UIEventObservable
//                .Where(e => e.Item1 == TimeSeriesMessages.WasteReport)
//                .DurableSubscribe(msg =>
//                {
//                    var data = JsonConvert.DeserializeObject<Message<Guid>>(msg.Item2);

//                    var config = configurationRepository.Find(data.Data);
//                    if (config != null)
//                    {
//                        PublishReportToUi(msg.Item1, config);
//                    }

//                }, logger);
//        }

//        private void PublishReportToUi(Enumeration messageType, TimeSeriesConfiguration configuration)
//        {
//            IEnumerable<TimeSeriesDataPoint> data = repository.FindByType(TimeSeriesMessages.TypeLookup[configuration.DataType]).ToList();
//            var dataPoints = new List<UiTimeSeriesDataPoint>();

//            foreach (var line in configuration.Lines)
//            {
//                    var linePoints = new List<UiTimeSeriesDataPoint>();
//                    var lineData = data.ToList().AsEnumerable();

//                    foreach (var filter in line.Filters)
//                    {
//                        var key = filter.Type;
//                        var value = filter.Value;

//                        if (String.IsNullOrWhiteSpace(key) || String.IsNullOrWhiteSpace(value))
//                        {
//                            continue;
//                        }
                        
//                        lineData =
//                            lineData.Where(
//                                dataPoint =>
//                                    dataPoint.Filters.ContainsKey(key) && String.Equals(dataPoint.Filters[key], value, StringComparison.CurrentCultureIgnoreCase));
//                    }

//                    lineData.ForEach(p => linePoints.Add(new UiTimeSeriesDataPoint(p)
//                    {
//                        Data = p.Data[configuration.DataType],
//                        Line = line.LineName
//                    }));

//                    DateTime parsedDate;
//                    if (String.IsNullOrWhiteSpace(configuration.FromDate) == false && DateTime.TryParse(configuration.FromDate, out parsedDate))
//                    {
//                        linePoints = linePoints.Where(d => d.CreationTime >= parsedDate).ToList();
//                    }

//                    dataPoints.AddRange(linePoints.OrderBy(d => d.CreationTime));

//            }

//            var graph = new TimeSeriesGraph(configuration.Id, dataPoints);

//            uiCommunicationService.SendMessageToUI(new Message<TimeSeriesGraph>()
//            {   
//                Data = graph,
//                MessageType = TimeSeriesMessages.UiResponseLookupTable[(TimeSeriesMessages)messageType]
//            });
//        }

//        public void Dispose()
//        {
            
//        }
//    }
//}