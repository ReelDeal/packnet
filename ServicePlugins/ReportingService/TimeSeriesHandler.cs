﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.Repositories;

namespace PackNet.ReportingService
{
    public class TimeSeriesHandler
    {
        private readonly ITimeSeriesDataRepository repository;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ILogger logger;

        public TimeSeriesHandler(ITimeSeriesDataRepository repository, IEventAggregatorSubscriber subscriber, ILogger logger)
        {
            this.repository = repository;
            this.subscriber = subscriber;
            this.logger = logger;

            SetupWasteCompression();
        }

        private void SetupWasteCompression()
        {
            //This will be removed, this is for a unit test, fix simulation of time in RX
            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .Take(1)
                .DurableSubscribe(m => CompressData(TimeSeriesMessages.Waste), logger);

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .Throttle(TimeSpan.FromHours(1))
                .DurableSubscribe(m => CompressData(TimeSeriesMessages.Waste), logger);
        }

        protected void CompressData(TimeSeriesMessages type)
        {
            var data = repository.FindByType(type);

            if (data == null || data.Any() == false)
                return;

            //Had to add 1 year so the daytime subtraction would work whe doing the groupings, otherwise one would get an unusable year for data from the current year.
            var yearlyData = GetYearlyGroupings(data, DateTime.Now.AddYears(1));
            var lastYearMonthly = GetMonthlyGroupings(yearlyData.First().Select(g => g), DateTime.Now.AddYears(1));
            var lastMonthDaily = GetDailyGroupings(lastYearMonthly.First().Select(g => g), DateTime.Now.AddYears(1));

            var newData = new List<TimeSeriesDataPoint>();

            yearlyData.Skip(1).ForEach(year =>
            {
                var currentYearData = year.Select(tsd => tsd);
                newData.Add(new TimeSeriesDataPoint()
                {
                    Data = GetAverage(currentYearData),
                    CreationTime = new DateTime(currentYearData.First().CreationTime.Year, 6, 15)
                });
            });

            lastYearMonthly.Skip(1).ForEach(month =>
            {
                var currentMonthData = month.Select(tsd => tsd);
                newData.Add(new TimeSeriesDataPoint()
                {
                    Data = GetAverage(currentMonthData),
                    CreationTime =
                        new DateTime(currentMonthData.First().CreationTime.Year, currentMonthData.First().CreationTime.Month, 15)
                });
            });

            lastMonthDaily.Skip(1).ForEach(month =>
            {
                var currentDay = month.Select(tsd => tsd);
                newData.Add(new TimeSeriesDataPoint()
                {
                    Data = GetAverage(currentDay),
                    CreationTime =
                        new DateTime(currentDay.First().CreationTime.Year, currentDay.First().CreationTime.Month,
                            currentDay.First().CreationTime.Day)
                });
            });

            newData.AddRange(lastMonthDaily.First().Select(d => d));

            repository.Delete(data);
            repository.Create(newData);
        }

        public static IDictionary<string, double> GetAverage(IEnumerable<TimeSeriesDataPoint> timeSeriesDataPoint)
        {
            var outputDict = new Dictionary<string, double>();
            var timeSeriesDataPoints = timeSeriesDataPoint as TimeSeriesDataPoint[] ?? timeSeriesDataPoint.ToArray();

            foreach (var dataPoint in timeSeriesDataPoints.SelectMany(tdp => tdp.Data))
            {
                if (outputDict.ContainsKey(dataPoint.Key) == false)
                {
                    outputDict.Add(dataPoint.Key, dataPoint.Value);
                }
                else
                {
                    outputDict[dataPoint.Key] += dataPoint.Value;
                }
            }

            for (var i = 0; i < outputDict.Count; i++)
            {
                var element = outputDict.ElementAt(i);
                outputDict[element.Key] /= timeSeriesDataPoints.Count(p => p.Data.ContainsKey(element.Key));      
            }

            return outputDict;
        }

        public static IEnumerable<IGrouping<int, TimeSeriesDataPoint>> GetYearlyGroupings(IEnumerable<TimeSeriesDataPoint> data,
            DateTime startTime)
        {
            return data.GroupBy(d => (new DateTime(1, 1, 1) + (startTime - d.CreationTime)).Year);
        }

        public static IEnumerable<IGrouping<int, TimeSeriesDataPoint>> GetMonthlyGroupings(IEnumerable<TimeSeriesDataPoint> data,
            DateTime startTime)
        {
            return data.GroupBy(d => (new DateTime(1, 1, 1) + (startTime - d.CreationTime)).Month);
        }

        public static IEnumerable<IGrouping<int, TimeSeriesDataPoint>> GetDailyGroupings(IEnumerable<TimeSeriesDataPoint> data,
            DateTime startTime)
        {
            return data.GroupBy(d => (new DateTime(1, 1, 1) + (startTime - d.CreationTime)).Day);
        }
    }
}