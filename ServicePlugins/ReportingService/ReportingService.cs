﻿using System;
using System.ComponentModel.Composition;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.Repositories;

namespace PackNet.ReportingService
{
    [Export(typeof(IService))]
    public class ReportingService : IService
    {
        private readonly IServiceLocator serviceLocator;

        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IUICommunicationService uiCommunicationService;
        
        // ReSharper disable NotAccessedField.Local
        private readonly MetricRecorder metricRecorder; //This is really needed, even though resharper doesn't think so
        // ReSharper restore NotAccessedField.Local
        private ILogger logger;

        //private TimeSeriesService timeSeriesService;
        
        [ImportingConstructor]
        public ReportingService(IServiceLocator serviceLocator)
        {
            this.serviceLocator = serviceLocator;
            logger = serviceLocator.Locate<ILogger>();
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            metricRecorder = new MetricRecorder(
                subscriber,
                publisher,
                serviceLocator,
                new MetricsRepository<string>(),
                new MetricsRepository<KeyValuePair<string, string>>(),
                new MetricsRepository<IDictionary<string, string>>(),
                new SlidingWindowMetricsRepository(),
                logger);
        }

        public string Name
        {
            get { return "ReportingService"; }
        }

        public void Dispose()
        {
        }
    }
}
