﻿using System;
using System.Collections.Generic;

namespace PackNet.ReportingService.DTO
{
    public class TimeSeriesGraph
    {
        public TimeSeriesGraph(Guid graphId, List<UiTimeSeriesDataPoint> dataPoints)
        {
            GraphId = graphId;
            DataPoints = dataPoints;
        }
        
        public Guid GraphId { get; set; }

        public List<UiTimeSeriesDataPoint> DataPoints { get; set; }
    }
}
