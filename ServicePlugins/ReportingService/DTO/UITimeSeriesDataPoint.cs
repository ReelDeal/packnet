﻿using System;
using System.Collections.Generic;

namespace PackNet.ReportingService.DTO
{
    public class UiTimeSeriesDataPoint
    {
        public double Data { get; set; }
        
        public IDictionary<string, string> Filters { get; set; }

        public TimeSeriesMessages Messages { get; set; }

        public DateTime CreationTime { get; set; }

        public string Line { get; set; }

        public UiTimeSeriesDataPoint()
        {
            CreationTime = DateTime.Now;
        }

        public UiTimeSeriesDataPoint(TimeSeriesDataPoint timeSeriesDataPoint)
        {
            CreationTime = timeSeriesDataPoint.CreationTime;
            Filters = timeSeriesDataPoint.Filters;
            Messages = Messages;
        }
    }
}