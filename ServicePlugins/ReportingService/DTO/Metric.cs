﻿using System;
using System.Diagnostics;

using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.WorkflowTracking;

namespace PackNet.ReportingService.DTO
{
    using MongoDB.Bson.Serialization.Attributes;

    public class Metric<T> : IPersistable
    {
        private T value;

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }
        
        /// <summary>
        /// Gets or sets the machine.
        /// </summary>
        /// <value>
        /// The machine.
        /// </value>
        public string Machine { get; set; }
        
        /// <summary>
        /// Gets or sets the type of the metric.
        /// </summary>
        /// <value>
        /// The type of the metric.
        /// </value>
        public MetricType MetricType { get; set; }
        
        /// <summary>
        /// Gets the creation time.
        /// </summary>
        /// <value>
        /// The creation time.
        /// </value>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime CreationTime { get; protected set; }
        
        /// <summary>
        /// Gets the last update time.
        /// </summary>
        /// <value>
        /// The last update time.
        /// </value>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastUpdateTime { get; protected set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public T Value
        {
            get { return value; }
            private set
            {
                this.value = value;
                //Taking this out because serializer andother things use the setter and sets the value leaving us so we can't address the issue when server was not running at midnight but the day hs elapsed
                //LastUpdateTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Metric{T}"/> class.
        /// </summary>
        public Metric()
        {
            CreationTime = DateTime.Now;
        }

        /// <summary>
        /// Resets the metric to the specified default value.
        /// </summary>
        /// <param name="defaultValue">The default value.</param>
        internal void Reset(T defaultValue)
        {
            CreationTime = DateTime.Now;
            Value = defaultValue;
        }

        /// <summary>
        /// Determines whether the metric has expired.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public bool HasExpired(DateTime startDate, DateTime endDate)
        {
            return HasExpired(startDate, endDate, DateTime.Now);
        }

        /// <summary>
        /// Determines whether the metric has expired.
        /// </summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="now">Pass in DateTime.Now or for additional edge testing purposes, your manipulated now value.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException">startDate;endDate must be greater than startDate.</exception>
        private bool HasExpired(DateTime startDate, DateTime endDate, DateTime now)
        {
            Debug.WriteLine("HasExpired - startDate.Ticks: {0} endDate.Ticks: {1} now.Ticks: {2}", startDate.Ticks, endDate.Ticks, now.Ticks);
            
            //Clearly bad data was passed in - abort, abort!
            if (startDate > endDate)
            {
                throw new ArgumentOutOfRangeException("startDate", "endDate must be greater than startDate.");
            }

            //If *now* is GTE the endDate for the metric it's clearly expired
            if (now >= endDate)
            {
                return true;
            }

            return false;
        }

        public T UpdateValue(T newValue)
        {
            Value = newValue;
            LastUpdateTime = DateTime.Now;
            return Value;
        }
    }
}
