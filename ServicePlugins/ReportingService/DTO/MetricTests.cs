﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.ReportingService.DTO;

namespace ReportingServiceTests
{
    using Testing.Specificity;

    [TestClass]
    public class MetricTests
    {
        [TestInitialize]
        public void Setup()
        {
            
        }

        /// <summary>
        /// Utility method for invoking a private method in Metric class so that we can simulate rolling over second, minute, hour, day, month, year, etc.
        /// </summary>
        /// <param name="metric"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="spoofedNow"></param>
        /// <returns></returns>
        private bool InvokeHasExpiredOnPrivateMethodInMetricClass(Metric<KeyValuePair<string, string>> metric, DateTime startDate, DateTime endDate, DateTime spoofedNow)
        {
            const string methodName = "HasExpired";
            Type typeOfMetric = metric.GetType();
            MethodInfo methodInfo = typeOfMetric.GetMethod(methodName, BindingFlags.Instance | BindingFlags.NonPublic);
            object instanceOfMetric = Activator.CreateInstance(typeOfMetric);
            object result = false;

            if (methodInfo != null)
            {
                result = methodInfo.Invoke(instanceOfMetric, new object[]{startDate, endDate, spoofedNow});
            }

            if (result == null)
            {
                throw new Exception("No value obtained in reflected invocation.");
            }

            return Convert.ToBoolean(result);
        }

        [TestMethod]
        public void MetricShouldNotExpire_WhenEndDateIsThirtySecondsPriorToMidnightOfNextDay()
        {
            var startDate = DateTime.Today;
            var endDate = startDate.AddDays(1);
            var spoofedNow = endDate.AddSeconds(-30);
            var metric = new Metric<KeyValuePair<string, string>>();
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
        }

        [TestMethod]
        public void MetricShouldExpire_WhenEndDateIsThirtySecondsPostMidnightOfNextDay()
        {
            var startDate = DateTime.Today;
            var endDate = startDate.AddDays(1);
            var spoofedNow = endDate.AddSeconds(30);
            var metric = new Metric<KeyValuePair<string, string>>();
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeTrue();
        }

        [TestMethod]
        public void MetricShouldNotExpire_WhenEndDateIsThirtySecondsPriorToMidnightOfNexWeek()
        {
            var startDate = DateTime.Today;
            var endDate = startDate.AddDays(7);
            var spoofedNow = endDate.AddSeconds(-30);
            var metric = new Metric<KeyValuePair<string, string>>();
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
        }

        [TestMethod]
        public void MetricShouldExpire_WhenEndDateIsThirtySecondsPostMidnightOfNextWeek()
        {
            var startDate = DateTime.Today;
            var endDate = startDate.AddDays(7);
            var spoofedNow = endDate.AddSeconds(30);
            var metric = new Metric<KeyValuePair<string, string>>();
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeTrue();
        }

        [TestMethod]
        public void MetricShouldNotExpire_WhenEndDateIsThirtySecondsPriorToMidnightOfNextMonth()
        {
            var startDate = DateTime.Today;
            var endDate = startDate.AddMonths(1);
            var spoofedNow = endDate.AddSeconds(-30);
            var metric = new Metric<KeyValuePair<string, string>>();
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
        }

        [TestMethod]
        public void MetricShouldExpire_WhenEndDateIsThirtySecondsPostMidnightOfNextMonth()
        {
            var startDate = DateTime.Today;
            var endDate = startDate.AddDays(7);
            var spoofedNow = endDate.AddMonths(1);
            var metric = new Metric<KeyValuePair<string, string>>();
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeTrue();
        }
        
        [TestMethod]
        public void MetricShouldNotBeExpired_WhenEndDateHasNotPassed()
        {
            var metric = new Metric<KeyValuePair<string, string>>();
            DateTime startDate = metric.CreationTime;
            DateTime endDate = startDate.AddMilliseconds(100);
            Specify.That(metric.HasExpired(startDate, endDate)).Should.BeFalse();
        }

        [TestMethod]
        public void MetricShouldNotBeExpired_WhenEndDateHasNotPassedUsingSpoofedNow()
        {
            var metric = new Metric<KeyValuePair<string, string>>();
            DateTime startDate = metric.CreationTime;
            DateTime endDate = startDate.AddMilliseconds(100);
            DateTime spoofedNow = DateTime.Now;
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
        }

        [TestMethod]
        public void MetricShouldBeExpired_WhenEndDatePassed()
        {
            var metric = new Metric<KeyValuePair<string, string>>();
            DateTime startDate = metric.CreationTime;
            DateTime endDate = startDate.AddMilliseconds(100);
            Specify.That(metric.HasExpired(startDate, endDate)).Should.BeFalse();
            Thread.Sleep(500);
            Specify.That(metric.HasExpired(startDate, endDate)).Should.BeTrue();
        }

        [TestMethod]
        public void MetricShouldBeExpired_WhenEndDatePassedUsingSpoofedNow()
        {
            var metric = new Metric<KeyValuePair<string, string>>();
            DateTime startDate = metric.CreationTime;
            DateTime spoofedNow = DateTime.Now;
            DateTime endDate = startDate.AddSeconds(1);
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
            Thread.Sleep(2000);
            spoofedNow = DateTime.Now;
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeTrue();
        }

        [TestMethod]
        public void DailyMetricStartDateAndEndDateShouldResetAfterPassingMidnight()
        {
            var metric = new Metric<KeyValuePair<string, string>>();
            metric.MetricType = MetricType.ProducibleImportCountByDay;
            DateTime startDate = DateTime.Today;
            DateTime spoofedNow = startDate.AddSeconds(1);
            DateTime endDate = DateTime.Today.AddSeconds(2);
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
            spoofedNow = spoofedNow.AddSeconds(2);
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeTrue();
        }

        [TestMethod]
        public void WeeklyMetricStartDateAndEndDateShouldResetAfterPassingMidnightAtWeeksEnd()
        {
            var metric = new Metric<KeyValuePair<string, string>>();
            metric.MetricType = MetricType.ProducibleImportCountByWeek;
            DateTime startDate = DateTime.Today;
            DateTime spoofedNow = startDate.AddDays(7).AddSeconds(-1);
            DateTime endDate = DateTime.Today.AddDays(7);
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
            spoofedNow = spoofedNow.AddSeconds(2);
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeTrue();
        }

        [TestMethod]
        public void OverallMetricStartDateAndEndDateShouldNotResetAfterPassingMidnightAtWeeksEnd()
        {
            var metric = new Metric<KeyValuePair<string, string>>();
            metric.MetricType = MetricType.ProducibleImportCount;
            DateTime startDate = DateTime.Today;
            DateTime spoofedNow = startDate.AddDays(7).AddSeconds(-1);
            DateTime endDate = DateTime.Today.AddDays(7);
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeFalse();
            spoofedNow = spoofedNow.AddSeconds(2);
            Specify.That(InvokeHasExpiredOnPrivateMethodInMetricClass(metric, startDate, endDate, spoofedNow)).Should.BeTrue();
        }

    }
}