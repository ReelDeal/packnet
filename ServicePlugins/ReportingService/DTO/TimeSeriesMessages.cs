﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.ReportingService.DTO
{
    public class TimeSeriesMessages : MessageTypes
    {
        //Types
        public static readonly TimeSeriesMessages Waste = new TimeSeriesMessages("WasteDataPoint");

        //Waste related keys
        public static readonly TimeSeriesMessages WasteValue = new TimeSeriesMessages("WasteValue");
        public static readonly TimeSeriesMessages WastePercentage = new TimeSeriesMessages("WastePercentage");
        public static readonly TimeSeriesMessages ProductionTime = new TimeSeriesMessages("ProductionTime");

        //Filter keys
        public static readonly string TiledBox = "TiledBox";
        public static readonly string RotatedBox = "RotatedBox";
        public static readonly string FailedBox = "FailedBox";
        public static readonly string Type = "Type";
        public static readonly string Machine = "Machine";
        public static readonly string Operator = "Operator";
        public static readonly string Corrugate = "Corrugate";

        //UI messages
        public static readonly TimeSeriesMessages WasteReport = new TimeSeriesMessages("WasteReport");
        public static readonly TimeSeriesMessages WasteReportResponse = new TimeSeriesMessages("WasteReportResponse");

        //Configuration messages
        public static readonly TimeSeriesMessages ConfigurationRequest = new TimeSeriesMessages("ConfigurationRequest");
        public static readonly TimeSeriesMessages ConfigurationRequestResponse = new TimeSeriesMessages("ConfigurationRequestResponse");

        public static readonly TimeSeriesMessages ConfigurationUpdate = new TimeSeriesMessages("ConfigurationUpdate");
        public static readonly TimeSeriesMessages ConfigurationUpdateResponse = new TimeSeriesMessages("ConfigurationUpdateResponse");

        public static readonly TimeSeriesMessages NewConfiguration = new TimeSeriesMessages("NewConfiguration");


        public static readonly Dictionary<string ,TimeSeriesMessages> TypeLookup = new Dictionary<string, TimeSeriesMessages>()
        {
            {"WasteValue", WasteValue},
            {"WastePercentage", WastePercentage},
            {"ProductionTime", ProductionTime}
        }; 

        public static readonly Dictionary<TimeSeriesMessages, TimeSeriesMessages> UiTypeLookupTable =
            new Dictionary<TimeSeriesMessages, TimeSeriesMessages>()
            {
                { WasteReport, Waste }
            };

        public static readonly Dictionary<TimeSeriesMessages, TimeSeriesMessages> UiResponseLookupTable =
            new Dictionary<TimeSeriesMessages, TimeSeriesMessages>()
            {
                { WasteReport, WasteReportResponse },
                { ConfigurationRequest, ConfigurationRequestResponse },
                { NewConfiguration, ConfigurationRequestResponse },
                { ConfigurationUpdate, ConfigurationUpdateResponse },
                { ConfigurationUpdateResponse, WasteReportResponse },
            };

        public TimeSeriesMessages(string name) : base(GetAll().Count() ,name)
        {
        }
    }
}