﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.ReportingService.DTO
{
    public class TimeSeriesConfiguration : IPersistable
    {
        public TimeSeriesConfiguration()
        {
            Lines = new List<TimeSeriesLine>();
        }

        public string DataType { get; set; }

        public string FromDate { get; set; }

        public List<TimeSeriesLine> Lines { get; set; }

        public Guid Id { get; set; }
    }

    public class Filter
    {
        public string Type { get; set; }

        public string Value { get; set; }
    }

    public class TimeSeriesLine
    {
        public string LineName { get; set; }

        public List<Filter> Filters { get; set; } 
    }
}
