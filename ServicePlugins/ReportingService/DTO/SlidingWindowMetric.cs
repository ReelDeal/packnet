﻿using System;

namespace PackNet.ReportingService.DTO
{
    using System.Collections.Generic;
    using System.Linq;

    using MongoDB.Bson.Serialization.Attributes;

    public class SlidingWindowMetric : Metric<KeyValuePair<string, string>>
    {
        private List<DateTime> entries;

        /// <summary>
        /// Gets or sets the type of the metric.
        /// </summary>
        /// <value>
        /// The type of the metric.
        /// </value>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public List<DateTime> Entries
        {
            get
            {
                return this.entries;
            }
            private set
            {
                this.entries = value;
            }
        }
        
        /// <summary>
        /// Specifies how far back in time values in the colledtion will be used
        /// </summary>
        public TimeSpan Window { get; set; }

        /// <summary>
        /// The date and time of the last purge.
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime LastPurge { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SlidingWindowMetric"/> class.
        /// </summary>
        public SlidingWindowMetric() : base()
        {
            entries = new List<DateTime>();
            LastPurge = DateTime.Now;
        }

        public void UpdateValue(string defaultKey)
        {
            var value = "0";
            if (entries.Any())
            {
                value = entries.Count(dt => dt >= (DateTime.Now.Subtract(Window)) && dt <= DateTime.Now).ToString();
            }

            //Value = new KeyValuePair<string, string>(defaultKey, value);
            UpdateValue(new KeyValuePair<string, string>(defaultKey, value));
        }

        /// <summary>
        /// Add a new entry
        /// </summary>
        /// <param name="date">The date the event was fired</param>
        /// <param name="val">The value to add</param>
        public void AddEntry(DateTime date, string val)
        {
            entries.Add(date);
            this.UpdateValue(val);
        }

        /// <summary>
        /// Resets the metric to the specified default value.
        /// </summary>
        /// <param name="defaultValue">The default value.</param>
        internal new void Reset(KeyValuePair<string, string> defaultValue)
        {
            CreationTime = DateTime.Now;
            entries.Clear();
            this.UpdateValue(defaultValue.Key);
        }

        /// <summary>
        /// Purges entries that are outside the size of the window. 
        /// Sets the default key if no entries are left after the purge.
        /// </summary>
        /// <param name="defaultValue">The default key.</param>
        internal void Purge(string defaultKey)
        {
            LastPurge = DateTime.Now;
            entries = entries.Where(dt => dt >= DateTime.Now.Subtract(Window) && dt <= DateTime.Now).ToList();
            this.UpdateValue(defaultKey);
        }
    }
}
