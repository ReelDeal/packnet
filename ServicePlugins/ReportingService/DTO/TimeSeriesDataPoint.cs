﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.ReportingService.DTO
{
    public class TimeSeriesDataPoint : IPersistable
    {
        public IDictionary<string, double> Data { get; set; }

        public Guid Id { get; set; }

        public IDictionary<string, string> Filters { get; set; }

        public TimeSeriesMessages DataType { get; set; }

        public DateTime CreationTime { get; set; }

        public TimeSeriesDataPoint()
        {
            CreationTime = DateTime.Now;
        }
    }
}