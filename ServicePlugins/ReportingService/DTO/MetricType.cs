﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.ReportingService.DTO
{
    public class MetricType : MessageTypes
    {
        public static readonly MetricType ProducibleImportCount = new MetricType("ProducibleImportCount");
        public static readonly MetricType ProducibleImportCountByDay = new MetricType("ProducibleImportCountByDay");
        public static readonly MetricType ProducibleImportCountByWeek = new MetricType("ProducibleImportCountByWeek");

        public static readonly MetricType ProducibleCreatedCount = new MetricType("ProducibleCreatedCount");
        public static readonly MetricType ProducibleCreatedCountByDay = new MetricType("ProducibleCreatedCountByDay");
        public static readonly MetricType ProducibleCreatedCountByWeek = new MetricType("ProducibleCreatedCountByWeek");

        public static readonly MetricType ProducibleFailedCount = new MetricType("ProducibleFailedCount");
        public static readonly MetricType ProducibleFailedCountByDay = new MetricType("ProducibleFailedCountByDay");
        public static readonly MetricType ProducibleFailedCountByWeek = new MetricType("ProducibleFailedCountByWeek");

        public static readonly MetricType AverageWastePercentage = new MetricType("AverageWastePercentage");
        public static readonly MetricType AverageWastePercentageByDay = new MetricType("AverageWastePercentageByDay");
        public static readonly MetricType AverageWastePercentageByWeek = new MetricType("AverageWastePercentageByWeek");

        public static readonly MetricType AverageProductionTime = new MetricType("AverageProductionTime");
        public static readonly MetricType AverageProductionTimeByDay = new MetricType("AverageProductionTimeByDay");
        public static readonly MetricType AverageProductionTimeByWeek = new MetricType("AverageProductionTimeByWeek");

        public static readonly MetricType ProducibleRotatedCount = new MetricType("ProducibleRotatedCount");
        public static readonly MetricType ProducibleRotatedCountByDay = new MetricType("ProducibleRotatedCountByDay");
        public static readonly MetricType ProducibleRotatedCountByWeek = new MetricType("ProducibleRotatedCountByWeek");

        public static readonly MetricType CartonCountByMachine = new MetricType("CartonCountByMachine");
        public static readonly MetricType CartonCountByMachineByDay = new MetricType("CartonCountByMachineByDay");
        public static readonly MetricType CartonCountByMachineByWeek = new MetricType("CartonCountByMachineByWeek");

        public static readonly MetricType CartonCountByUser = new MetricType("CartonCountByUser");
        public static readonly MetricType CartonCountByUserByDay = new MetricType("CartonCountByUserByDay");
        public static readonly MetricType CartonCountByUserByWeek = new MetricType("CartonCountByUserByWeek");

        public static readonly MetricType CartonCountByCorrugate = new MetricType("CartonCountByCorrugate");
        public static readonly MetricType CartonCountByCorrugateByDay = new MetricType("CartonCountByCorrugateByDay");
        public static readonly MetricType CartonCountByCorrugateByWeek = new MetricType("CartonCountByCorrugateByWeek");

        public static readonly MetricType CorrugateConsumption = new MetricType("CorrugateConsumption");
        public static readonly MetricType CorrugateConsumptionByDay = new MetricType("CorrugateConsumptionByDay");
        public static readonly MetricType CorrugateConsumptionByWeek = new MetricType("CorrugateConsumptionByWeek");

        public static readonly MetricType AverageWastePercentageByMachine = new MetricType("AverageWastePercentageByMachine");
        public static readonly MetricType AverageWastePercentageByMachineByDay = new MetricType("AverageWastePercentageByMachineByDay");
        public static readonly MetricType AverageWastePercentageByMachineByWeek = new MetricType("AverageWastePercentageByMachineByWeek");

        public static readonly MetricType AverageWastePercentageByCorrugate = new MetricType("AverageWastePercentageByCorrugate");
        public static readonly MetricType AverageWastePercentageByCorrugateByDay = new MetricType("AverageWastePercentageByCorrugateByDay");
        public static readonly MetricType AverageWastePercentageByCorrugateByWeek = new MetricType("AverageWastePercentageByCorrugateByWeek");

        public static readonly MetricType CartonCountByCPG = new MetricType("CartonCountByCPG");
        public static readonly MetricType CartonCountByCPGByHour = new MetricType("CartonCountByCPGByHour");
        public static readonly MetricType CartonCountByCPGBySlidingHour = new MetricType("CartonCountByCPGBySlidingHour");
    
        public static readonly MetricType CartonCountByCPGByDay = new MetricType("CartonCountByCPGByDay");
        public static readonly MetricType CartonCountByCPGByWeek = new MetricType("CartonCountByCPGByWeek");
        public static readonly MetricType CartonCountByCPGByMonth = new MetricType("CartonCountByCPGByMonth");
        
        protected MetricType(string name) : base(name) {}
    }
}
