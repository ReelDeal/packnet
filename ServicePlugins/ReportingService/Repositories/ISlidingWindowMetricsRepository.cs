﻿using System;

using PackNet.Common.Interfaces.Repositories;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public interface ISlidingWindowMetricsRepository : IRepository<SlidingWindowMetric>
    {
        SlidingWindowMetric FindByType(MetricType metricType);

        SlidingWindowMetric FindByType(MetricType metricType, Func<SlidingWindowMetric, bool> predicate);
    }
}