﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Repositories;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public interface ITimeSeriesDataRepository : IRepository<TimeSeriesDataPoint>
    {
        IEnumerable<TimeSeriesDataPoint> FindByType(TimeSeriesMessages type);
    }
}