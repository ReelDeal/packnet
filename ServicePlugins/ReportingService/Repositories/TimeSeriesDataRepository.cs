﻿using System.Collections.Generic;
using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public class TimeSeriesDataRepository : MongoDbRepository<TimeSeriesDataPoint>, ITimeSeriesDataRepository
    {
        private static bool serializerRegistered;
        private static object syncObject = new object();

        public TimeSeriesDataRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "TimeSeriesData")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                {
                    return;
                }

                MongoDbHelpers.TryRegisterSerializer<MetricType>(new EnumerationSerializer());

                serializerRegistered = true;
            }
        }

        public IEnumerable<TimeSeriesDataPoint> FindByType(TimeSeriesMessages metricType)
        {
            return Collection.AsQueryable().Where(m => m.DataType == metricType);
        }
    }
}