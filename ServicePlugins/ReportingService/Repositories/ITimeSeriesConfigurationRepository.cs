﻿using PackNet.Common.Interfaces.Repositories;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public interface ITimeSeriesConfigurationRepository : IRepository<TimeSeriesConfiguration>
    {
    }
}
