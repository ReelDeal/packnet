﻿using System;

using PackNet.Common.Interfaces.Repositories;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public interface IMetricsRepository<T> : IRepository<Metric<T>>
    {
        Metric<T> FindByType(MetricType metricType);

        Metric<T> FindByType(MetricType metricType, Func<Metric<T>, bool> predicate);
    }
}