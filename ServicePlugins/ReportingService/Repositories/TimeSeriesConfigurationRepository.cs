﻿using System;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public class TimeSeriesConfigurationRepository : MongoDbRepository<TimeSeriesConfiguration>, ITimeSeriesConfigurationRepository
    {
        public TimeSeriesConfigurationRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "TimeSeriesConfigurations")
        {
        }

        public void Create(TimeSeriesConfiguration data, Guid id)
        {
        }
    }
}
