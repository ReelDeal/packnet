﻿using System;
using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public class MetricsRepository<T> : MongoDbRepository<Metric<T>>, IMetricsRepository<T>
    {
        private static bool serializerRegistered;
        private static object syncObject = new object();

        public MetricsRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Metrics")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                {
                    return;
                }
                MongoDbHelpers.TryRegisterSerializer<MetricType>(new EnumerationSerializer());
                serializerRegistered = true;
            }
        }

        public Metric<T> FindByType(MetricType metricType)
        {
            return Collection.AsQueryable().FirstOrDefault(m => m.MetricType == metricType);
        }

        public Metric<T> FindByType(MetricType metricType, Func<Metric<T>, bool> predicate)
        {
            return Collection.AsQueryable().Where(m => m.MetricType == metricType)
                .ToList()
                .FirstOrDefault(predicate);
        }
    }
}
