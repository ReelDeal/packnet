﻿using System;
using System.Linq;

using MongoDB.Driver.Linq;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using PackNet.ReportingService.DTO;

namespace PackNet.ReportingService.Repositories
{
    public class SlidingWindowMetricsRepository : MongoDbRepository<SlidingWindowMetric>, ISlidingWindowMetricsRepository
    {
        private static bool serializerRegistered;
        private static object syncObject = new object();

        public SlidingWindowMetricsRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "SlidingMetrics")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                {
                    return;
                }
                MongoDbHelpers.TryRegisterSerializer<MetricType>(new EnumerationSerializer());
                serializerRegistered = true;
            }
        }

        public SlidingWindowMetric FindByType(MetricType metricType)
        {
            return Collection.AsQueryable().FirstOrDefault(m => m.MetricType == metricType);
        }

        public SlidingWindowMetric FindByType(MetricType metricType, Func<SlidingWindowMetric, bool> predicate)
        {
            return Collection.AsQueryable().Where(m => m.MetricType == metricType)
                .ToList()
                .FirstOrDefault(predicate);
        }
    }
}
