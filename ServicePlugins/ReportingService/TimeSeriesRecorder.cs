﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.Repositories;

namespace PackNet.ReportingService
{
    public class TimeSeriesRecorder
    {
        private TimeSeriesHandler handler;
        private readonly ITimeSeriesDataRepository repository;
        private readonly IEventAggregatorSubscriber subscriber;

        private readonly ILogger logger;

        public TimeSeriesRecorder(IEventAggregatorSubscriber subscriber, ITimeSeriesDataRepository repository, ILogger logger)
        {
            this.subscriber = subscriber;
            this.logger = logger;

            this.repository = repository;
            handler = new TimeSeriesHandler(repository, this.subscriber, this.logger);

            SetupRecorders();
        }

        private void SetupRecorders()
        {
            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m => repository.Create(new TimeSeriesDataPoint
                {
                    DataType = TimeSeriesMessages.Waste,
                    Data = ScrapeData(m.Data.Item2),
                    Filters =
                        new Dictionary<string, string>
                        {
                            { TimeSeriesMessages.Machine, m.Data.Item1.Id.ToString() },
                            { TimeSeriesMessages.Operator, m.Data.Item1.AssignedOperator },
                            { TimeSeriesMessages.Corrugate, m.Data.Item2.CartonOnCorrugate.Corrugate.Id.ToString() },
                            { TimeSeriesMessages.TiledBox, (m.Data.Item2.ProducibleCount > 1).ToString()},
                            { TimeSeriesMessages.RotatedBox, (m.Data.Item2.CartonOnCorrugate.Rotated).ToString()},
                            { TimeSeriesMessages.FailedBox, (m.Data.Item2.Failure).ToString()},
                        }
                }), logger);
        }

        private IDictionary<string, double> ScrapeData(IMachineProductionData productionData)
        {
            var wasteValue = productionData.CartonOnCorrugate.CartonLengthOnCorrugate * productionData.CartonOnCorrugate.Corrugate.Width -
                             productionData.CartonOnCorrugate.AreaOnCorrugate;

            var wastePercentage = productionData.CartonOnCorrugate.AreaOnCorrugate / productionData.CartonOnCorrugate.CartonLengthOnCorrugate *
                                  productionData.CartonOnCorrugate.Corrugate.Width;
        
            return new Dictionary<string, double>
            {
                { TimeSeriesMessages.WasteValue.DisplayName, wasteValue },
                { TimeSeriesMessages.WastePercentage.DisplayName, wastePercentage }
            };
        }
    }
}