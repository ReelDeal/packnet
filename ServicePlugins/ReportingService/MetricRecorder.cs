﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Timers;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.ReportingService.DTO;
using PackNet.ReportingService.L10N;
using PackNet.ReportingService.Properties;
using PackNet.ReportingService.Repositories;

namespace PackNet.ReportingService
{

    using Timer = System.Timers.Timer;

    /// <summary>
    /// Listens to events in the system and records metrics in the database 
    /// </summary>
    public class MetricRecorder
    {
        public const string AverageKey = "Key";
        public const string AverageValue = "Value";
        private const string AverageSum = "Sum";
        private const string AverageCount = "Count";

        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly ILogger logger;
        private readonly IMetricsRepository<string> basicMetricRepository;
        private readonly IMetricsRepository<KeyValuePair<string, string>> advancedMetricRepository;
        private readonly ISlidingWindowMetricsRepository slidingWindowMetricsRepository;
        /// <summary>
        /// The average metric repository. First Item is the value to display. Second Item the Sum and third item the count.
        /// </summary>
        private readonly IMetricsRepository<IDictionary<string, string>> averageMetricRepository;
        private readonly ICorrugateService corrugateService;
        private readonly IAggregateMachineService machineService;
        private readonly IUserService userService;
        private readonly ICartonPropertyGroupService cpgService;
        private readonly int metricThrottleTimeInSeconds;
        //Do not change this dayChangeNotifier of type System.Timers.Timer to another type. The timer wrapper used throughout the other source is not reliable enough for this class.
        private readonly Timer dayChangeNotifier;
        private DateTime lastDayNotified;

        private static readonly List<MetricType> byDayMetricTypes = new List<MetricType>
        {
            MetricType.ProducibleImportCountByDay,
            MetricType.AverageProductionTimeByDay,
            MetricType.ProducibleCreatedCountByDay,
            MetricType.ProducibleFailedCountByDay,
            MetricType.AverageWastePercentageByDay,
            MetricType.AverageProductionTimeByDay,
            MetricType.ProducibleRotatedCountByDay,
            MetricType.CartonCountByMachineByDay,
            MetricType.CartonCountByUserByDay,
            MetricType.CartonCountByCorrugateByDay,
            MetricType.CorrugateConsumptionByDay,
            MetricType.AverageWastePercentageByMachineByDay,
            MetricType.AverageWastePercentageByCorrugateByDay,
            MetricType.CartonCountByCPGByDay
        };

        private static readonly List<MetricType> byWeekMetricTypes = new List<MetricType>
        {
            MetricType.ProducibleImportCountByWeek,
            MetricType.AverageProductionTimeByWeek,
            MetricType.ProducibleImportCountByWeek,
            MetricType.ProducibleCreatedCountByWeek,
            MetricType.ProducibleFailedCountByWeek,
            MetricType.AverageWastePercentageByWeek,
            MetricType.AverageProductionTimeByWeek,
            MetricType.ProducibleRotatedCountByWeek,
            MetricType.CartonCountByMachineByWeek,
            MetricType.CartonCountByUserByWeek,
            MetricType.CartonCountByCorrugateByWeek,
            MetricType.CorrugateConsumptionByWeek,
            MetricType.AverageWastePercentageByMachineByWeek,
            MetricType.AverageWastePercentageByCorrugateByWeek
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="MetricRecorder"/> class.
        /// </summary>
        /// <param name="subscriber">The subscriber.</param>
        /// <param name="publisher">The publisher.</param>
        /// <param name="serviceLocator">The service locator.</param>
        /// <param name="basicMetricRepository">The basic metric repository.</param>
        /// <param name="advancedMetricRepository">The advanced metric repository.</param>
        /// <param name="averageMetricRepository">The average metric repository.</param>
        /// <param name="slidingWindowMetricsRepository">The sliding window metrics repository.</param>
        /// <param name="logger">The logger.</param>
        public MetricRecorder(IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher,
            IServiceLocator serviceLocator,
            IMetricsRepository<string> basicMetricRepository,
            IMetricsRepository<KeyValuePair<string, string>> advancedMetricRepository,
            IMetricsRepository<IDictionary<string, string>> averageMetricRepository,
            ISlidingWindowMetricsRepository slidingWindowMetricsRepository,
            ILogger logger)
        {
            this.subscriber = subscriber;
            this.publisher = publisher;
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            this.logger = logger;
            this.basicMetricRepository = basicMetricRepository;
            this.advancedMetricRepository = advancedMetricRepository;
            this.averageMetricRepository = averageMetricRepository;
            this.slidingWindowMetricsRepository = slidingWindowMetricsRepository;
            corrugateService = serviceLocator.Locate<ICorrugateService>();
            machineService = serviceLocator.Locate<IAggregateMachineService>();
            userService = serviceLocator.Locate<IUserService>();
            cpgService = serviceLocator.Locate<ICartonPropertyGroupService>();

            metricThrottleTimeInSeconds = Settings.Default.MetricThrottleTimeInSeconds;

            if (metricThrottleTimeInSeconds <= 0)
                metricThrottleTimeInSeconds = 2;

            RecordImportedItems();
            RecordProducedItems();
            RecordFailedProducibles();
            RecordRotatedItems();
            RecordAverageProductionTime();
            RecordAverageWastePercentage();
            RecordAverageWastePercentageByMachine();
            RecordAverageWastePercentageByCorrugate();
            RecordCorrugateConsumption();
            RecordCartonCountByCorrugate();
            RecordCartonCountByMachine();
            RecordCartonCountByUser();
            RecordCartonCountByCPG();

            //This needs to fire last to prevent a race and an overzealous zero-out condition in the UI
            dayChangeNotifier = new Timer();
            dayChangeNotifier.Elapsed += CheckForNewDayOrWeekAndNotify;
            //PLEASE DO NOT change the interval to 1000 ms - it's not a high enough resolution to pick up the day change every time. Leave it at 500 ms.
            dayChangeNotifier.Interval = 500;
            dayChangeNotifier.Enabled = true;
            dayChangeNotifier.Start();
        }

        private void CheckForNewDayOrWeekAndNotify(object sender, ElapsedEventArgs e)
        {
            DateTime now = DateTime.Now;
            dayChangeNotifier.Stop();

            if (lastDayNotified == default(DateTime))
            {
                lastDayNotified = now.Date;
            }

            //Check if it's a new day and that we have not already notified for the day. Make no assumptions about the new day being greater than the old (month turnover).
            if (now.Hour == 0 && now.Minute == 0 && now.Second == 0 && lastDayNotified.Day != now.Date.Day)
            {
                publisher.Publish(new Message { MessageType = MessageTypes.DateChanged });
                lastDayNotified = now.Date;
            }

            dayChangeNotifier.Start();
        }

        /// <summary>
        /// If server was not running @ midnight so the timer was missed and metrics are old we need to send out the message for the day change so UI can deal with it
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="metric"></param>
        private void HandleMetricDayTurnover<T>(Metric<T> metric)
        {
            DateTime localNow = DateTime.Now;
            DateTime lastUpdateLocal = metric.LastUpdateTime.ToLocalTime();

            // ReSharper disable once LocalizableElement
            logger.Log(LogLevel.Trace, "HandleMetricDayTurnoverForBasicAndAverageMetrics: localNow: {0} lastUpdateLocal: {1}.", localNow, lastUpdateLocal);

            if (localNow.Date > lastUpdateLocal.Date)
            {
                publisher.Publish(new Message { MessageType = MessageTypes.DateChanged });
                lastDayNotified = localNow;
            }
        }

        /// <summary>
        /// Send the default value for the by day or by week metric to the UI and persist to the database.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="metric"></param>
        /// <param name="defaultValue"></param>
        /// <param name="metricRepositoryReference">This needs to be passed to avoid issues with the generics and type identification</param>
        /// <param name="isThrottledMessage">Indicates if the message is throttled</param>
        private void SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase<T>(Metric<T> metric, T defaultValue, IMetricsRepository<T> metricRepositoryReference, bool isThrottledMessage = true)
        {
            //If the day has turned over for a day metric or the week has turned over for a week metric then handle it
            if (ShouldResetByDayMetric(metric.MetricType, metric.LastUpdateTime.ToLocalTime())
                || ShouldResetByWeekMetric(metric.MetricType, metric.LastUpdateTime.ToLocalTime()))
            {
                metric.UpdateValue(defaultValue);
                SendMetricToUserInterface(metric, isThrottledMessage);
                metricRepositoryReference.Update(metric);
            }
        }

        private bool ShouldResetByDayMetric(MetricType metricType, DateTime lastUpdated)
        {
            var lastDayRestarted = new DateTime(lastUpdated.Year, lastUpdated.Month, lastUpdated.Day, 0, 0, 0);
            if (byDayMetricTypes.Contains(metricType) && lastDayRestarted != DateTime.Today)
                return true;
            return false;
        }

        private bool ShouldResetByWeekMetric(MetricType metricType, DateTime lastUpdated)
        {
            var weekStartDate = GetTheStartDayOfThisWeek();

            // Do not user Calendar Year Week. When the year changes it gets reseted.
            return byWeekMetricTypes.Contains(metricType) && lastUpdated.Date < weekStartDate.Date;
        }

        private void RecordImportedItems()
        {
            RecordByDayWeekAndAll(MetricType.ProducibleImportCountByDay, MetricType.ProducibleImportCountByWeek, MetricType.ProducibleImportCount, RecordImportedItemsBy);
        }

        private void RecordImportedItemsBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";
            var metric = GetMetric(basicMetricRepository, metricType, defaultValue);

            if (metric.HasExpired(startDate, endDate))
            {
                metric.Reset(defaultValue);
                basicMetricRepository.Update(metric);
            }

            SendMetricToUserInterface(metric);

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => SendMetricToUserInterface(metric), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    //We know that the day changed per this message so send default to UI and persist
                    SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultValue, basicMetricRepository);
                });

            subscriber.GetEvent<ImportMessage<IEnumerable<IProducible>>>()
                .Where(msg => msg.MessageType != CartonMessages.ReproduceCarton && msg.MessageType != MessageTypes.Restaged)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var count = m.Data.Count(p => !(p is Order));
                    //Get the quantity from orders
                    var orderCount = m.Data
                        .Where(p => p is Order)
                        .Cast<Order>()
                        .Sum(p => p.OriginalQuantity);

                    count += orderCount;

                    if (count == 0)
                    {
                        return; //nothing to update
                    }

                    int existingValue;

                    // Important! A potential issue may occur if the recording of the imported items occurred very close to midnight and the
                    // adjust of these items (due to overwrite or skip) occurs after midnight.
                    if (m.MessageType == MessageTypes.DataOverwrittenOrSkipped)
                        count = count * -1;

                    if (int.TryParse(metric.Value, out existingValue))
                    {
                        metric.UpdateValue((existingValue + count).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        metric.UpdateValue(count.ToString(CultureInfo.InvariantCulture));
                    }

                    basicMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            HandleMetricDayTurnover(metric);
        }

        private void RecordProducedItems()
        {
            RecordByDayWeekAndAll(MetricType.ProducibleCreatedCountByDay, MetricType.ProducibleCreatedCountByWeek, MetricType.ProducibleCreatedCount, RecordProducedItemsBy);
        }

        private void RecordProducedItemsBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";
            var metric = GetMetric(basicMetricRepository, metricType, defaultValue);

            if (metric.HasExpired(startDate, endDate))
            {
                metric.Reset(defaultValue);
                basicMetricRepository.Update(metric);
            }

            SendMetricToUserInterface(metric);

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => SendMetricToUserInterface(metric), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    //We know that the day changed per this message so send default to UI and persist
                    SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultValue, basicMetricRepository);
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    int existingValue;
                    if (int.TryParse(metric.Value, out existingValue))
                    {
                        metric.UpdateValue((existingValue + (m.Data.Item2.Failure ? 0 : m.Data.Item2.ProducibleCount)).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        metric.UpdateValue(existingValue.ToString(CultureInfo.InvariantCulture));
                    }
                    basicMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            HandleMetricDayTurnover(metric);
        }

        private void RecordFailedProducibles()
        {
            RecordByDayWeekAndAll(MetricType.ProducibleFailedCountByDay, MetricType.ProducibleFailedCountByWeek, MetricType.ProducibleFailedCount, RecordFailedProduciblesBy);
        }

        private void RecordFailedProduciblesBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";
            var metric = GetMetric(basicMetricRepository, metricType, defaultValue);

            if (metric.HasExpired(startDate, endDate))
            {
                metric.Reset(defaultValue);
                basicMetricRepository.Update(metric);
            }

            SendMetricToUserInterface(metric);

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => SendMetricToUserInterface(metric), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    //We know that the day changed per this message so send default to UI and persist
                    SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultValue, basicMetricRepository);
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    int existingValue;
                    if (int.TryParse(metric.Value, out existingValue))
                    {
                        metric.UpdateValue((existingValue + (m.Data.Item2.Failure ? m.Data.Item2.ProducibleCount : 0)).ToString(CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        metric.UpdateValue(existingValue.ToString(CultureInfo.InvariantCulture));
                    }
                    basicMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            HandleMetricDayTurnover(metric);
        }

        private void RecordAverageWastePercentage()
        {
            RecordByDayWeekAndAll(MetricType.AverageWastePercentageByDay, MetricType.AverageWastePercentageByWeek, MetricType.AverageWastePercentage, RecordAverageWastePercentageBy);
        }

        private void RecordAverageWastePercentageBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            var defaultValue = GetAverageMetricValue("0", "0", "0");
            var metric = GetMetric(averageMetricRepository, metricType, defaultValue);

            if (metric.HasExpired(startDate, endDate))
            {
                metric.Reset(defaultValue);
                averageMetricRepository.Update(metric);
            }

            SendMetricToUserInterface(metric);

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => SendMetricToUserInterface(metric), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    //We know that the day changed per this message so send default to UI and persist
                    SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultValue, averageMetricRepository);
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    double sumValues;
                    int count;

                    var data = m.Data.Item2;
                    var wastePercent = WastePercent(data);

                    if (Double.TryParse(metric.Value[AverageSum], out sumValues) && int.TryParse(metric.Value[AverageCount], out count))
                    {
                        // TODO: Test accuracy when tiling.
                        count += m.Data.Item2.ProducibleCount;
                        sumValues += wastePercent;
                        var averageWaste = sumValues / count;
                        metric.UpdateValue(GetAverageMetricValue(averageWaste.ToString("0.##"), sumValues.ToString("0.##"), count.ToString()));
                    }
                    else
                    {
                        metric.UpdateValue(GetAverageMetricValue(wastePercent.ToString("0.##"), wastePercent.ToString("0.##"), m.Data.Item2.ProducibleCount.ToString()));
                    }

                    averageMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            HandleMetricDayTurnover(metric);
        }

        private void RecordAverageProductionTime()
        {
            RecordByDayWeekAndAll(MetricType.AverageProductionTimeByDay, MetricType.AverageProductionTimeByWeek, MetricType.AverageProductionTime, RecordAverageProductionTimeBy);
        }

        private void RecordAverageProductionTimeBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string timeFormat = @"hh\:mm\:ss";
            var defaultValue = GetAverageMetricValue(TimeSpan.Zero.ToString(), TimeSpan.Zero.ToString(), "0");

            var metric = GetMetric(averageMetricRepository, metricType, defaultValue);

            if (metric.HasExpired(startDate, endDate))
            {
                metric.Reset(defaultValue);
                averageMetricRepository.Update(metric);
            }

            SendMetricToUserInterface(metric);

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => SendMetricToUserInterface(metric), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    //We know that the day changed per this message so send default to UI and persist
                    SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultValue, averageMetricRepository);
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    TimeSpan sumValues;
                    int count;
                    var newTime = TimeSpan.FromMilliseconds(m.Data.Item2.ProductionTime);

                    if (TimeSpan.TryParse(metric.Value[AverageSum], out sumValues) && int.TryParse(metric.Value[AverageCount], out count))
                    {
                        sumValues += newTime;
                        count += m.Data.Item2.ProducibleCount;

                        var averageTicks = sumValues.Ticks / count;
                        long longAverageTicks = Convert.ToInt64(averageTicks);

                        metric.UpdateValue(GetAverageMetricValue(new TimeSpan(longAverageTicks).ToString(timeFormat),
                            sumValues.ToString(timeFormat), count.ToString()));

                    }
                    else
                    {
                        metric.UpdateValue(GetAverageMetricValue(newTime.ToString(timeFormat), newTime.ToString(timeFormat),
                            m.Data.Item2.ProducibleCount.ToString()));
                    }
                    averageMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            HandleMetricDayTurnover(metric);
        }

        private void RecordRotatedItems()
        {
            RecordByDayWeekAndAll(MetricType.ProducibleRotatedCountByDay, MetricType.ProducibleRotatedCountByWeek, MetricType.ProducibleRotatedCount, RecordRotatedItemsBy);
        }

        private void RecordRotatedItemsBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";
            var metric = GetMetric(basicMetricRepository, metricType, defaultValue);

            if (metric.HasExpired(startDate, endDate))
            {
                metric.Reset(defaultValue);
                basicMetricRepository.Update(metric);
            }

            SendMetricToUserInterface(metric);

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => SendMetricToUserInterface(metric), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    //We know that the day changed per this message so send default to UI and persist
                    SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultValue, basicMetricRepository);
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    int existingValue;
                    if (int.TryParse(metric.Value, out existingValue))
                        metric.UpdateValue(
                            (existingValue + (m.Data.Item2.CartonOnCorrugate.Rotated ? m.Data.Item2.ProducibleCount : 0)).ToString(
                                CultureInfo.InvariantCulture));
                    else
                        metric.UpdateValue(existingValue.ToString(CultureInfo.InvariantCulture));
                    basicMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            HandleMetricDayTurnover(metric);
        }

        private void RecordCartonCountByMachine()
        {
            RecordByDayWeekAndAll(MetricType.CartonCountByMachineByDay, MetricType.CartonCountByMachineByWeek, MetricType.CartonCountByMachine, RecordCartonCountByMachineBy);
        }

        private void RecordCartonCountByMachineBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";
            var allMachines = machineService.Machines;
            var metrics = allMachines
                .Where(m => m is IPacksizeCutCreaseMachine)
                .Select(c =>
                {
                    var metric = GetMetric(advancedMetricRepository, metricType,
                        new KeyValuePair<string, string>(c.Alias, defaultValue),
                        (m) => m.Value.Key == c.Alias);

                    if (metric.HasExpired(startDate, endDate))
                    {
                        metric.Reset(new KeyValuePair<string, string>(c.Alias, defaultValue));
                        advancedMetricRepository.Update(metric);
                    }

                    return metric;
                })
                .OrderBy(m => m.Value.Key)
                .ToList();

            metrics.ForEach(m => SendMetricToUserInterface(m, false));

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => metrics.ForEach(m => SendMetricToUserInterface(m, false)), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    metrics.ForEach(metric =>
                    {
                        //We know that the day changed per this message so send default to UI and persist
                        SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, new KeyValuePair<string, string>(metric.Value.Key, defaultValue), advancedMetricRepository, false);
                    });
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var data = m.Data.Item2;
                    var metric = metrics.FirstOrDefault(cm => cm.Value.Key == data.Machine.Alias.ToString());
                    if (metric == null)
                    {
                        metric = new Metric<KeyValuePair<string, string>>
                        {
                            MetricType = metricType,
                        };
                        metric.UpdateValue(new KeyValuePair<string, string>(data.Machine.Alias.ToString(), m.Data.Item2.ProducibleCount.ToString()));
                        metrics.Add(metric);
                    }
                    else
                    {
                        double existingValue;
                        var newValue = 0D;
                        if (double.TryParse(metric.Value.Value, out existingValue))
                            newValue = (existingValue + m.Data.Item2.ProducibleCount);
                        metric.UpdateValue(new KeyValuePair<string, string>(metric.Value.Key, newValue.ToString()));
                    }

                    advancedMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric, false);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            metrics.ForEach(HandleMetricDayTurnover);
        }

        private void RecordCartonCountByUser()
        {
            RecordByDayWeekAndAll(MetricType.CartonCountByUserByDay, MetricType.CartonCountByUserByWeek, MetricType.CartonCountByUser, RecordCartonCountByUserBy);
        }

        private void RecordCartonCountByUserBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";
            var allUsers = userService.Users;
            var metrics = allUsers
                .Select(c =>
                {
                    var metric = GetMetric(advancedMetricRepository, metricType,
                        new KeyValuePair<string, string>(c.UserName, defaultValue),
                        (m) => m.Value.Key == c.UserName);

                    if (metric.HasExpired(startDate, endDate))
                    {
                        metric.Reset(new KeyValuePair<string, string>(c.UserName, defaultValue));
                        advancedMetricRepository.Update(metric);
                    }
                    SendMetricToUserInterface(metric, false);
                    return metric;
                })
                .ToList();


            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => metrics.ForEach(m => SendMetricToUserInterface(m, false)), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    metrics.ForEach(metric =>
                    {
                        //We know that the day changed per this message so send default to UI and persist
                        SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, new KeyValuePair<string, string>(metric.Value.Key, defaultValue), advancedMetricRepository, false);
                    });
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var data = m.Data.Item2;
                    var metric = metrics.FirstOrDefault(cm => cm.Value.Key == data.UserName.ToString());
                    if (metric == null)
                    {
                        metric = new Metric<KeyValuePair<string, string>>
                        {
                            MetricType = metricType
                        };
                        metric.UpdateValue(new KeyValuePair<string, string>(data.UserName.ToString(), m.Data.Item2.ProducibleCount.ToString()));
                        metrics.Add(metric);
                    }
                    else
                    {
                        double existingValue;
                        var newValue = 0D;

                        if (double.TryParse(metric.Value.Value, out existingValue))
                        {
                            newValue = (existingValue + m.Data.Item2.ProducibleCount);
                        }

                        metric.UpdateValue(new KeyValuePair<string, string>(metric.Value.Key, newValue.ToString()));
                    }

                    advancedMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric, false);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            metrics.ForEach(HandleMetricDayTurnover);
        }

        private void RecordCartonCountByCorrugate()
        {
            RecordByDayWeekAndAll(MetricType.CartonCountByCorrugateByDay, MetricType.CartonCountByCorrugateByWeek, MetricType.CartonCountByCorrugate, RecordCartonCountByCorrugateBy);
        }

        private void RecordCartonCountByCorrugateBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";
            var allCorrugates = corrugateService.Corrugates;
            var corrugateMetrics = allCorrugates
                .Select(c =>
                {
                    var metric = GetMetric(advancedMetricRepository, metricType,
                        new KeyValuePair<string, string>(c.Alias, defaultValue),
                        (m) => m.Value.Key == c.Alias);

                    if (metric.HasExpired(startDate, endDate))
                    {
                        metric.Reset(new KeyValuePair<string, string>(c.Alias, defaultValue));
                        advancedMetricRepository.Update(metric);
                    }
                    SendMetricToUserInterface(metric, false);
                    return metric;
                })
                .ToList();

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => corrugateMetrics.ForEach(m => SendMetricToUserInterface(m, false)), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    corrugateMetrics.ForEach(metric =>
                    {
                        //We know that the day changed per this message so send default to UI and persist
                        SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, new KeyValuePair<string, string>(metric.Value.Key, defaultValue), advancedMetricRepository, false);
                    });
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var data = m.Data.Item2;
                    var metric = corrugateMetrics.FirstOrDefault(cm => cm.Value.Key == data.Corrugate.Alias.ToString());
                    if (metric == null)
                    {
                        metric = new Metric<KeyValuePair<string, string>>
                        {
                            MetricType = metricType,
                        };
                        metric.UpdateValue(new KeyValuePair<string, string>(data.Corrugate.Alias.ToString(), m.Data.Item2.ProducibleCount.ToString()));
                        corrugateMetrics.Add(metric);
                    }
                    else
                    {
                        double existingValue;
                        var newValue = 0D;
                        if (double.TryParse(metric.Value.Value, out existingValue))
                        {
                            newValue = (existingValue + m.Data.Item2.ProducibleCount);
                        }
                        metric.UpdateValue(new KeyValuePair<string, string>(metric.Value.Key, newValue.ToString()));
                    }

                    advancedMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric, false);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            corrugateMetrics.ForEach(HandleMetricDayTurnover);
        }

        private void RecordCorrugateConsumption()
        {
            RecordByDayWeekAndAll(MetricType.CorrugateConsumptionByDay, MetricType.CorrugateConsumptionByWeek, MetricType.CorrugateConsumption, RecordCorrugateConsumptionBy);
        }

        private void RecordCorrugateConsumptionBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";

            var allCorrugates = corrugateService.Corrugates;
            var corrugateMetrics = allCorrugates
                .Select(c =>
                {
                    var metric = GetMetric(advancedMetricRepository, metricType,
                        new KeyValuePair<string, string>(c.Alias, defaultValue),
                        (m) => m.Value.Key == c.Alias);

                    if (metric.HasExpired(startDate, endDate))
                    {
                        metric.Reset(new KeyValuePair<string, string>(c.Alias, defaultValue));
                        advancedMetricRepository.Update(metric);
                    }

                    SendMetricToUserInterface(metric, false);
                    return metric;
                })
                .ToList();

            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => corrugateMetrics.ForEach(m => SendMetricToUserInterface(m, false)), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    corrugateMetrics.ForEach(metric =>
                    {
                        //We know that the day changed per this message so send default to UI and persist
                        SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, new KeyValuePair<string, string>(metric.Value.Key, defaultValue), advancedMetricRepository, false);
                    });
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var data = m.Data.Item2;
                    var metric = corrugateMetrics.FirstOrDefault(cm => cm.Value.Key == data.Corrugate.Alias.ToString());
                    if (metric == null)
                    {
                        metric = new Metric<KeyValuePair<string, string>>
                        {
                            MetricType = metricType,
                        };
                        metric.UpdateValue(new KeyValuePair<string, string>(data.Corrugate.Alias.ToString(), m.Data.Item2.FedLength.ToString()));
                        corrugateMetrics.Add(metric);
                    }
                    else
                    {
                        double existingValue;
                        var newValue = 0D;
                        if (double.TryParse(metric.Value.Value, out existingValue))
                        {
                            newValue = (existingValue + m.Data.Item2.FedLength);
                        }
                        metric.UpdateValue(new KeyValuePair<string, string>(metric.Value.Key, newValue.ToString()));
                    }

                    advancedMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric, false);
                }, logger);
        }

        private void RecordAverageWastePercentageByMachine()
        {
            RecordByDayWeekAndAll(MetricType.AverageWastePercentageByMachineByDay, MetricType.AverageWastePercentageByMachineByWeek, MetricType.AverageWastePercentageByMachine, RecordAverageWastePercentageByMachineBy);
        }

        private void RecordAverageWastePercentageByMachineBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";

            var allMachines = machineService.Machines;
            var metrics = allMachines
                .Where(m => m is IPacksizeCutCreaseMachine)
                .Select(c =>
                {
                    var metric = GetMetric(averageMetricRepository, metricType,
                        GetAverageMetricValue(c.Alias, defaultValue, defaultValue, defaultValue),
                        (m) => m.Value[AverageKey] == c.Alias);

                    if (metric.HasExpired(startDate, endDate))
                    {
                        metric.Reset(GetAverageMetricValue(c.Alias, defaultValue, defaultValue, defaultValue));
                        averageMetricRepository.Update(metric);
                    }
                    SendMetricToUserInterface(metric, false);
                    return metric;
                })
                .ToList();


            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => metrics.ForEach(m => SendMetricToUserInterface(m, false)), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    metrics.ForEach(metric =>
                    {
                        var defaultDictionaryValue = GetAverageMetricValue(metric.Value[AverageKey], defaultValue, defaultValue, defaultValue);
                        //We know that the day changed per this message so send default to UI and persist
                        SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultDictionaryValue, averageMetricRepository, false);
                    });
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var data = m.Data.Item2;
                    var wastePercent = WastePercent(data);

                    var metric = metrics.FirstOrDefault(cm => cm.Value[AverageKey] == data.Machine.Alias.ToString());
                    if (metric == null)
                    {
                        metric = new Metric<IDictionary<string, string>>
                        {
                            MetricType = metricType,
                        };
                        metric.UpdateValue(GetAverageMetricValue(data.Machine.Alias.ToString(), wastePercent.ToString("0.##"), wastePercent.ToString("0.##"), m.Data.Item2.ProducibleCount.ToString()));
                        metrics.Add(metric);
                    }
                    else
                    {
                        double sumValues;
                        int count;
                        if (Double.TryParse(metric.Value[AverageSum], out sumValues) &&
                            int.TryParse(metric.Value[AverageCount], out count))
                        {
                            sumValues += wastePercent;
                            count += data.ProducibleCount;

                            var averageWaste = sumValues / count;
                            metric.UpdateValue(GetAverageMetricValue(metric.Value[AverageKey], averageWaste.ToString("0.##"),
                                sumValues.ToString("0.##"), count.ToString()));
                        }
                        else
                        {
                            metric.UpdateValue(GetAverageMetricValue(metric.Value[AverageKey], wastePercent.ToString("0.##"),
                                wastePercent.ToString("0.##"), m.Data.Item2.ProducibleCount.ToString()));
                        }
                    }

                    averageMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric, false);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            metrics.ForEach(HandleMetricDayTurnover);
        }

        private void RecordAverageWastePercentageByCorrugate()
        {
            RecordByDayWeekAndAll(MetricType.AverageWastePercentageByCorrugateByDay,
                MetricType.AverageWastePercentageByCorrugateByWeek, MetricType.AverageWastePercentageByCorrugate,
                RecordAverageWastePercentageByCorrugateBy);
        }

        private void RecordAverageWastePercentageByCorrugateBy(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartAndEndDate)
        {
            const string defaultValue = "0";

            var allCorrugates = corrugateService.Corrugates;
            var metrics = allCorrugates
                .Select(c =>
                {
                    var metric = GetMetric(averageMetricRepository, metricType,
                       GetAverageMetricValue(c.Alias, defaultValue, defaultValue, defaultValue),
                        (m) => m.Value[AverageKey] == c.Alias);

                    if (metric.HasExpired(startDate, endDate))
                    {
                        metric.Reset(GetAverageMetricValue(c.Alias, defaultValue, defaultValue, defaultValue));
                        averageMetricRepository.Update(metric);
                    }

                    SendMetricToUserInterface(metric, false);
                    return metric;
                })
                .ToList();


            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => metrics.ForEach(m => SendMetricToUserInterface(m, false)), logger);

            subscriber.GetEvent<Message>()
                .Where(msg => msg.MessageType == MessageTypes.DateChanged)
                .DurableSubscribe(m =>
                {
                    metrics.ForEach(metric =>
                    {
                        var defaultDictionaryValue = GetAverageMetricValue(metric.Value[AverageKey], defaultValue, defaultValue, defaultValue);
                        //We know that the day changed per this message so send default to UI and persist
                        SendDefaultValueToUiForDailyAndWeeklyMetricAndPersistToDatabase(metric, defaultDictionaryValue, averageMetricRepository, false);
                    });
                });

            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted && !m.Data.Item2.Failure)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var data = m.Data.Item2;
                    var wastePercent = WastePercent(data);

                    var metric = metrics.FirstOrDefault(cm => cm.Value[AverageKey] == data.Corrugate.Alias.ToString());
                    if (metric == null)
                    {
                        metric = new Metric<IDictionary<string, string>>
                        {
                            MetricType = metricType,
                        };
                        metric.UpdateValue(GetAverageMetricValue(data.Corrugate.Alias.ToString(), wastePercent.ToString("0.##"), wastePercent.ToString("0.##"), m.Data.Item2.ProducibleCount.ToString()));
                        metrics.Add(metric);
                    }
                    else
                    {
                        double sumValues;
                        int count;
                        if (Double.TryParse(metric.Value[AverageSum], out sumValues) &&
                            int.TryParse(metric.Value[AverageCount], out count))
                        {

                            sumValues += wastePercent;
                            count += data.ProducibleCount;

                            var averageWaste = sumValues / count;
                            metric.UpdateValue(GetAverageMetricValue(metric.Value[AverageKey], averageWaste.ToString("0.##"), sumValues.ToString("0.##"), count.ToString()));
                        }
                        else
                        {
                            metric.UpdateValue(GetAverageMetricValue(metric.Value[AverageKey], wastePercent.ToString("0.##"), wastePercent.ToString("0.##"), m.Data.Item2.ProducibleCount.ToString()));
                        }
                    }

                    averageMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric, false);
                }, logger);

            //Check for a day change on start-up and handle it so that if server was not running at midnight the house of cards doesn't collapse
            metrics.ForEach(HandleMetricDayTurnover);
        }

        private double WastePercent(IMachineProductionData data)
        {
            var corrugateWidth = data.Corrugate.Width;
            var usedWidth = data.CartonOnCorrugate.CartonWidthOnCorrugate*data.CartonOnCorrugate.TileCount;
            double wastePercent;
            try
            {
                wastePercent = (corrugateWidth - usedWidth) / (corrugateWidth) * 100;
                if (corrugateWidth < usedWidth)
                {
                    logger.Log(LogLevel.Error,
                        Strings.MetricRecorder_WastePercent_MachineProductionCompleted_data_incorrect__corrugate_Width__0___used_Width___1___Wasted_Percentage_over_100_,
                        corrugateWidth, usedWidth);
                    wastePercent = 0;
                }
            }
            catch (DivideByZeroException)
            {
                wastePercent = 0;
            }
            return wastePercent;
        }

        private void RecordCartonCountByCPG()
        {
            RecordByHour(MetricType.CartonCountByCPGByHour, RecordCartonCountByCPG);
            RecordBySlidingHour(MetricType.CartonCountByCPGBySlidingHour, RecordSlidingCartonCountByCPG);
            RecordByDay(MetricType.CartonCountByCPGByDay, RecordCartonCountByCPG);
            RecordByWeek(MetricType.CartonCountByCPGByWeek, RecordCartonCountByCPG);
            RecordByMonth(MetricType.CartonCountByCPGByMonth, RecordCartonCountByCPG);
            RecordAll(MetricType.CartonCountByCPG, RecordCartonCountByCPG);
        }

        private void RecordCartonCountByCPG(MetricType metricType, DateTime startDate, DateTime endDate, Func<Tuple<DateTime, DateTime>> getNewStartEndDates)
        {
            const string defaultValue = "0";
            var allCpgs = cpgService.Groups;
            var metrics = allCpgs
                .Select(c =>
                {
                    var metric = GetMetric(advancedMetricRepository, metricType,
                        new KeyValuePair<string, string>(c.Alias, defaultValue),
                        (m) => m.Value.Key == c.Alias);

                    if (metric.HasExpired(startDate, endDate))
                    {
                        var startEndDate = getNewStartEndDates.Invoke();
                        startDate = startEndDate.Item1;
                        endDate = startEndDate.Item2;
                        metric.Reset(new KeyValuePair<string, string>(c.Alias, defaultValue));
                        advancedMetricRepository.Update(metric);
                    }
                    SendMetricToUserInterface(metric);
                    return metric;
                })
                .ToList();


            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(e => metrics.ForEach(
                    m => SendMetricToUserInterface(m)), logger);

            subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(m => m.MessageType == CartonPropertyGroupMessages.CartonPropertyGroupCreated)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(
                    m =>
                    {
                        var metric = new Metric<KeyValuePair<string, string>>
                        {
                            MetricType = metricType,
                        };
                        metric.UpdateValue(new KeyValuePair<string, string>(m.Data.Alias, "0"));
                        metrics.Add(metric);
                        advancedMetricRepository.Update(metric);
                        SendMetricToUserInterface(metric);
                    }, logger);

            subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(m => m.MessageType == CartonPropertyGroupMessages.CartonPropertyGroupDeleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(
                    m =>
                    {
                        var metric = metrics.FirstOrDefault(cm => cm.Value.Key == m.Data.Alias);
                        if (metric != null)
                        {
                            var updatedMetric = new Metric<KeyValuePair<string, string>>
                            {
                                MetricType = metricType,
                            };
                            updatedMetric.UpdateValue(new KeyValuePair<string, string>(
                                                        m.Data.Alias + " (Deleted)",
                                                        metric.Value.ToString()));

                            metrics.Remove(metric);
                            advancedMetricRepository.Delete(metric);
                            SendMetricToUserInterface(updatedMetric);
                        }
                    }, logger);

            subscriber.GetEvent<Message<CartonPropertyGroup>>()
                .Where(m => m.MessageType == CartonPropertyGroupMessages.CartonPropertyGroupUpdated)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(
                    m =>
                    {
                        var metric = metrics.FirstOrDefault(cm => cm.Value.Key == m.Data.Alias);
                        if (metric != null)
                        {
                            metric.Reset(new KeyValuePair<string, string>(metric.Value.Key, defaultValue));
                            advancedMetricRepository.Update(metric);
                            SendMetricToUserInterface(metric);
                        }
                    }, logger);


            subscriber.GetEvent<Message<string>>()
                .Where(m => m.MessageType == CartonPropertyGroupMessages.ProducibleCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var metric = metrics.FirstOrDefault(cm => cm.Value.Key == m.Data);
                    if (metric == null)
                    {
                        metric = new Metric<KeyValuePair<string, string>>
                        {
                            MetricType = metricType,
                        };
                        metric.UpdateValue(new KeyValuePair<string, string>(m.Data, "1"));
                        metrics.Add(metric);
                    }
                    else
                    {
                        if (metric.HasExpired(startDate, endDate))
                        {
                            var startEndDate = getNewStartEndDates.Invoke();
                            startDate = startEndDate.Item1;
                            endDate = startEndDate.Item2;
                            metric.Reset(new KeyValuePair<string, string>(metric.Value.Key, defaultValue));
                        }

                        int existingValue;
                        var newValue = 0;
                        if (int.TryParse(metric.Value.Value, out existingValue))
                            newValue = existingValue + 1;
                        metric.UpdateValue(new KeyValuePair<string, string>(metric.Value.Key, newValue.ToString()));
                    }

                    advancedMetricRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);
        }

        private void RecordSlidingCartonCountByCPG(MetricType metricType, TimeSpan windowSize, TimeSpan purgeInterval)
        {
            const string defaultValue = "0";
            var allCpgs = cpgService.Groups;
            var metrics = allCpgs.Select(
                c =>
                {
                    var metric = GetSlidingMetric(slidingWindowMetricsRepository, metricType, windowSize,
                        new KeyValuePair<string, string>(c.Alias, defaultValue), (m) => m.Value.Key == c.Alias);
                    metric.Purge(c.Alias);
                    slidingWindowMetricsRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                    return metric;
                }).ToList();


            //Wire up events
            uiCommunicationService.UIEventObservable
                .Where(e => e.Item1 == metricType)
                .DurableSubscribe(
                    e => metrics.ForEach(
                        met =>
                        {
                            met.UpdateValue(met.Value.Key);
                            SendMetricToUserInterface(met);
                        }), logger);

            subscriber.GetEvent<Message<string>>()
                .Where(m => m.MessageType == CartonPropertyGroupMessages.ProducibleCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var metric = metrics.FirstOrDefault(cm => cm.Value.Key == m.Data);
                    if (metric == null)
                    {
                        metric = new SlidingWindowMetric()
                        {
                            MetricType = metricType,
                            Window = windowSize,
                        };
                        metric.AddEntry(DateTime.Now, m.Data);
                        metrics.Add(metric);
                    }
                    else
                        metric.AddEntry(m.Created, m.Data);

                    slidingWindowMetricsRepository.Update(metric);
                    SendMetricToUserInterface(metric);
                }, logger);

            subscriber.GetEvent<Message<string>>()
                .Where(m => m.MessageType == CartonPropertyGroupMessages.ProducibleCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .Sample(purgeInterval, subscriber.Scheduler)
                .DurableSubscribe(
                    m =>
                    {
                        var metric = metrics.FirstOrDefault(cm => cm.Value.Key == m.Data);
                        if (metric != null)
                        {
                            metric.Purge(m.Data);
                            slidingWindowMetricsRepository.Update(metric);
                        }
                    }, logger);
        }

        private static Metric<T> GetMetric<T>(IMetricsRepository<T> repository, MetricType metricType, T defaultValue)
        {
            var metric = repository.FindByType(metricType);
            if (metric != null)
            {
                return metric;
            }

            metric = new Metric<T>
            {
                MetricType = metricType,
            };
            metric.UpdateValue(defaultValue);
            repository.Create(metric);
            return metric;
        }

        private static Metric<T> GetMetric<T>(IMetricsRepository<T> repository, MetricType metricType, T defaultValue,
            Func<Metric<T>, bool> predicate)
        {
            var metric = repository.FindByType(metricType, predicate);
            if (metric != null)
            {
                return metric;
            }

            metric = new Metric<T>
            {
                MetricType = metricType,
            };
            metric.UpdateValue(defaultValue);
            repository.Create(metric);
            return metric;
        }

        private static SlidingWindowMetric GetSlidingMetric(ISlidingWindowMetricsRepository repository, MetricType metricType, TimeSpan windowSize, KeyValuePair<string, string> defaultValue,
            Func<SlidingWindowMetric, bool> predicate)
        {
            var metric = repository.FindByType(metricType, predicate);
            if (metric != null)
            {
                return metric;
            }

            metric = new SlidingWindowMetric
            {
                MetricType = metricType,
                Window = windowSize,
            };
            metric.Reset(defaultValue);
            repository.Create(metric);
            return metric;
        }

        private void SendMetricToUserInterface<T>(Metric<T> metric, bool isThrottledMessage = true)
        {
            var message = new Message<Metric<T>>
            {
                MessageType = metric.MetricType,
                Data = metric,
            };

            if (isThrottledMessage)
            {
                uiCommunicationService.SendThrottledMessageToUI(message, TimeSpan.FromSeconds(metricThrottleTimeInSeconds));
            }
            else
            {
                uiCommunicationService.SendMessageToUI(message);
            }
        }

        /// <summary>
        /// Calls the methods Record By Day, By Week and All using the delegate passed.
        /// </summary>
        /// <param name="metricTypeDaily">The metric type daily.</param>
        /// <param name="metricTypeWeekly">The metric type weekly.</param>
        /// <param name="metricTypeAll">The metric type all.</param>
        /// <param name="action">The delegate.</param>
        private void RecordByDayWeekAndAll(MetricType metricTypeDaily, MetricType metricTypeWeekly, MetricType metricTypeAll, Action<MetricType, DateTime, DateTime, Func<Tuple<DateTime, DateTime>>> action)
        {
            RecordAll(metricTypeAll, action);
            RecordByDay(metricTypeDaily, action);
            RecordByWeek(metricTypeWeekly, action);
        }

        /// <summary>
        /// Records all.
        /// </summary>
        /// <param name="metricType">Type of the metric.</param>
        /// <param name="action">The action.</param>
        private void RecordAll(MetricType metricType, Action<MetricType, DateTime, DateTime, Func<Tuple<DateTime, DateTime>>> action)
        {
            action.Invoke(metricType, DateTime.MinValue, DateTime.MaxValue, null);
        }

        /// <summary>
        /// Records by hour.
        /// </summary>
        /// <param name="metricType">Type of the metric.</param>
        /// <param name="action">The action.</param>
        private void RecordByHour(MetricType metricType, Action<MetricType, DateTime, DateTime, Func<Tuple<DateTime, DateTime>>> action)
        {
            var startEndDate = this.GetStartEndForHour();
            action.Invoke(metricType, startEndDate.Item1, startEndDate.Item2, GetStartEndForHour);
        }

        private void RecordBySlidingHour(MetricType metricType, Action<MetricType, TimeSpan, TimeSpan> action)
        {
            action.Invoke(metricType, TimeSpan.FromHours(1), TimeSpan.FromMinutes(30));
        }

        private Tuple<DateTime, DateTime> GetStartEndForHour()
        {
            var startDate = DateTime.Now;
            startDate = startDate.Subtract(new TimeSpan(0, 0, startDate.Minute, startDate.Second, startDate.Millisecond));
            var endDate = startDate.AddHours(1).Subtract(TimeSpan.FromSeconds(1));
            return new Tuple<DateTime, DateTime>(startDate, endDate);
        }

        /// <summary>
        /// Records by day.
        /// </summary>
        /// <param name="metricType">Type of the metric.</param>
        /// <param name="action">The action.</param>
        private void RecordByDay(MetricType metricType, Action<MetricType, DateTime, DateTime, Func<Tuple<DateTime, DateTime>>> action)
        {
            var startEndDate = this.GetStartEndForDay();
            action.Invoke(metricType, startEndDate.Item1, startEndDate.Item2, GetStartEndForDay);
        }

        private Tuple<DateTime, DateTime> GetStartEndForDay()
        {
            var startDate = DateTime.Today;
            var endDate = startDate.AddDays(1);
            return new Tuple<DateTime, DateTime>(startDate, endDate);
        }

        /// <summary>
        /// Records by week.
        /// </summary>
        /// <param name="metricType">Type of the metric.</param>
        /// <param name="action">The action.</param>
        private void RecordByWeek(MetricType metricType, Action<MetricType, DateTime, DateTime, Func<Tuple<DateTime, DateTime>>> action)
        {
            var startEndDate = GetStartEndForWeek();
            action.Invoke(metricType, startEndDate.Item1, startEndDate.Item2, GetStartEndForWeek);
        }

        private Tuple<DateTime, DateTime> GetStartEndForWeek()
        {
            var startDate = GetTheStartDayOfThisWeek();
            var endDate = startDate.AddDays(7);
            return new Tuple<DateTime, DateTime>(startDate, endDate);
        }

        /// <summary>
        /// Gets the start day of this week.
        /// </summary>
        /// <returns></returns>
        private DateTime GetTheStartDayOfThisWeek()
        {
            var diff = DateTime.Today.DayOfWeek - DayOfWeek.Monday;

            if (diff < 0)
            {
                diff += 7;
            }

            return DateTime.Today.AddDays(-1 * diff);
        }

        /// <summary>
        /// Records by month.
        /// </summary>
        /// <param name="metricType">Type of the metric.</param>
        /// <param name="action">The action.</param>
        private void RecordByMonth(MetricType metricType, Action<MetricType, DateTime, DateTime, Func<Tuple<DateTime, DateTime>>> action)
        {
            var startEndDate = GetStartEndForMonth();

            action.Invoke(metricType, startEndDate.Item1, startEndDate.Item2, GetStartEndForMonth);
        }

        private Tuple<DateTime, DateTime> GetStartEndForMonth()
        {
            var startDate = GetTheStartDayOfThisMonth();
            var endDate = GetEndDayOfMonth(startDate);
            return new Tuple<DateTime, DateTime>(startDate, endDate);
        }

        /// <summary>
        /// Gets the start day of this month.
        /// </summary>
        /// <returns></returns>
        private DateTime GetTheStartDayOfThisMonth()
        {
            var now = DateTime.Now;
            return new DateTime(now.Year, now.Month, 1);
        }

        /// <summary>
        /// Gets the end day of this month.
        /// </summary>
        /// <returns></returns>
        private DateTime GetEndDayOfMonth(DateTime startDate)
        {
            var daysToAdd = DateTime.DaysInMonth(startDate.Year, startDate.Month);
            return startDate.AddDays(daysToAdd).Subtract(TimeSpan.FromSeconds(1));
        }

        private static IDictionary<string, string> GetAverageMetricValue(string key, string value, string sum, string count)
        {
            var result = GetAverageMetricValue(value, sum, count);
            result.Add(AverageKey, key);
            return result;
        }
        private static IDictionary<string, string> GetAverageMetricValue(string value, string sum, string count)
        {
            return new Dictionary<string, string>
            {
                {AverageValue, value},
                {AverageSum, sum},
                {AverageCount, count},
            };
        }
    }
}
