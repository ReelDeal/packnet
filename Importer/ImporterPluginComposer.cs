﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Plugins;

namespace PackNet.Importer
{
    /// <summary>
    /// Composes all IWorkflowImporters from the given plugin directory
    /// </summary>
    public class ImporterPluginComposer
    {
        [ImportMany]
        public IEnumerable<IWorkflowImporter> Importers { get; private set; }

        public ImporterPluginComposer(string pluginDirectory, ILogger logger)
        {
            var assembly = Assembly.GetExecutingAssembly();

            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(assembly));
            var batch = new CompositionBatch();
            batch.AddExportedValue(logger);
            batch.AddPart(this);
            MefComposer.Compose(pluginDirectory, batch, catalog);

            //Validate the plug-ins
            Importers.ForEach(i => i.Validate(pluginDirectory));
        }
    }
}
