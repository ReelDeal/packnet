﻿using System.ComponentModel.Composition;
using System.IO;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Importer.ImportPlugins.Label
{
    [Export(typeof(IWorkflowImporter))]
    public class LabelImporter : IWorkflowImporter
    {
        public ProducibleTypes ProducibleType { get { return ProducibleTypes.Label; } }
        public ImportTypes ImportType { get { return ImportTypes.Label; } }

        public string Workflow
        {
            //todo: use the env variable?
            get { return Path.Combine(ImportType.ToString(), "ImportLabels.xaml"); }
        }

        [ImportingConstructor]
        public LabelImporter(ILogger logger)
        {
            logger.Log(LogLevel.Info, "Importer plug-in '{0}' loaded", ImportType);
        }

        public void Validate(string pluginDirectory)
        {
            var fileName = Path.Combine(pluginDirectory, Workflow);
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("The import workflow for " + ImportType + " does not exist.", fileName);
            }


        }
    }

}