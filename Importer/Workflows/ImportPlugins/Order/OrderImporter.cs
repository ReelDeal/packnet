﻿using PackNet.Common.Interfaces.Importing;
using System.ComponentModel.Composition;
using System.IO;

using PackNet.Common.Interfaces.Logging;

namespace PackNet.Importer.ImportPlugins.Order
{
    using Common.Interfaces.Enums;

    [Export(typeof(IWorkflowImporter))]
    public class OrderImporter : IWorkflowImporter
    {
        public ProducibleTypes ProducibleType { get { return ProducibleTypes.Order; } }
        public ImportTypes ImportType { get { return ImportTypes.Order; } }
        
        public string Workflow
        {
            get { return Path.Combine(ImportType.ToString(), "OrderImport.xaml"); }
        }

        [ImportingConstructor]
        public OrderImporter(ILogger logger)
        {
            logger.Log(LogLevel.Info, "Importer plug-in '{0}' loaded", ImportType);
        }

        public void Validate(string pluginDirectory)
        {
            var fileName = Path.Combine(pluginDirectory, Workflow);
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("The import workflow for " + ImportType + " does not exist.", fileName);
            }
        }
    }
}
