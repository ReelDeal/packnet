﻿using System;
using System.ComponentModel.Composition;
using System.IO;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Importer.L10N;

namespace PackNet.Importer.Workflows.ImportPlugins.BoxFirst
{
    [Export(typeof(IWorkflowImporter))]
    public class BoxFirstCartonImporter : IWorkflowImporter
    {
        public ProducibleTypes ProducibleType { get { return ProducibleTypes.BoxFirstProducible; } }
        public ImportTypes ImportType { get { return ImportTypes.BoxFirst; } }

        public string Workflow
        {
            get { return Path.Combine(ImportType.ToString(), "BoxFirstCartonImport.xaml"); }
        }

        [ImportingConstructor]
        public BoxFirstCartonImporter(ILogger logger)
        {
            logger.Log(LogLevel.Info, "Importer plug-in '{0}' loaded", ImportType);
        }

        public void Validate(string pluginDirectory)
        {
            var fileName = Path.Combine(pluginDirectory, Workflow);
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException(String.Format(Strings.BoxFirstCartonImporter_Validate_The_import_workflow_for__0__does_not_exist_,  ImportType), fileName);
            }
        }
    }
}