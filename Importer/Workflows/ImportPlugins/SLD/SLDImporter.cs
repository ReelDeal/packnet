﻿using System.ComponentModel.Composition;
using System.IO;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Importer.Workflows.ImportPlugins.SLD
{
    [Export(typeof(IWorkflowImporter))]
    public class SLDImporter : IWorkflowImporter
    {
        public ProducibleTypes ProducibleType { get { return ProducibleTypes.SLD; } }
        public ImportTypes ImportType { get { return ImportTypes.SLD; } }
        
        public string Workflow
        {
            get { return Path.Combine(ImportType.ToString(), "SLDImport.xaml"); }
        }

        [ImportingConstructor]
        public SLDImporter(ILogger logger)
        {
            logger.Log(LogLevel.Info, "Importer plug-in '{0}' loaded", ImportType);
        }

        public void Validate(string pluginDirectory)
        {
            var fileName = Path.Combine(pluginDirectory, Workflow);
            if (!File.Exists(fileName))
            {
                throw new FileNotFoundException("The import workflow for " + ImportType + " does not exist.", fileName);
            }
        }
    }
}
