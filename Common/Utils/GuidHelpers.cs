﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

namespace PackNet.Common.Utils
{
    public static class GuidHelpers
    {
        /// <summary>
        /// Converts the given guid to an array of four 32-bit integers. The conversion is performed
        /// by converting the guid to a byte array, then grouping the bytes by four, and finally by 
        /// converting the four bytes to a 32-bit integer.
        /// </summary>
        /// <param name="guid">Guid to convert</param>
        /// <returns>Array length of four of 32=bit integers</returns>
        public static int[] ConvertGuidToIntArray(Guid guid)
        {
            var byteArray = guid.ToByteArray();
            var guidBlocks = new Int32[4];

            for (var i = 0; i < (float)byteArray.Length / 4; i++)
            {
                guidBlocks[i] = BitConverter.ToInt32(byteArray.Skip(i * 4).Take(4).ToArray(), 0);
            }

            return guidBlocks;
        }

        /// <summary>
        /// Converts an array of four 32-bit signed integers to a guid.
        /// </summary>
        /// <param name="intArray">32-bit integer array to convert</param>
        /// <returns>Guid if array is convertible, an empty guid otherwise</returns>
        public static Guid ConvertIntArrayToGuid(int[] intArray)
        {
            if (intArray.Length != 4)
            {
                return Guid.Empty;
            }

            var bytes = new List<byte>();
            foreach (var i in intArray)
            {
                bytes.AddRange(BitConverter.GetBytes(i));
            }
            return new Guid(bytes.ToArray());
        }

        public static Guid GetGuidFromPlcData(object e)
        {
            var plcData = e.ToString();

            if (plcData == "[]")
            {
                return Guid.Empty;
            }

            // We receive an array of ints
            if (plcData.Contains(','))
            {
                var data = JsonConvert.DeserializeObject<Int32[]>(plcData);

                if (data != null && data.Count() != 4)
                {
                    throw new Exception("Guid from plc should be an int32 array with 4 elements, it isnt please fix");
                }

                return ConvertIntArrayToGuid(data);
            }

            //we receive the GUID
            return JsonConvert.DeserializeObject<Guid>(plcData);
        }

    }
}
