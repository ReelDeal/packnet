using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using PackNet.Common.Interfaces;

namespace PackNet.Common.Utils
{
    /// <summary>
    /// This class uses a pseudo que which is really a list with locking semantics for thread safety
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BlockingQueue<T> : IDisposable
    {
        public int MaxItems { get; private set; }
        private readonly BlockingCollection<T> blockingCollection;
        private bool disposed;

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="maxItems">Max number of items that can be added to the queue.</param>
        public BlockingQueue(int maxItems)
        {
            //TODO: Enforce the max items or use an exposed enumerator on framework's blocking collection class instead of a list with locking semantics
            MaxItems = maxItems;
            blockingCollection = new BlockingCollection<T>(maxItems);
        }

        /// <summary>
        /// Is there room on the queue? At the moment with using a list this is not really enforced. See the pending to-do in ctor.
        /// </summary>
        public bool CanAddItem
        {
            get
            {
                if (disposed)
                    return false;

                return !disposed && blockingCollection.Count < MaxItems;
            }
        }

        public IEnumerable<T> GetAll()
        {
            if (disposed)
                return null;

            return blockingCollection.ToList();
        }

        public int Count
        {
            get
            {
                if (disposed)
                    return 0;

                return disposed ? 0 : blockingCollection.Count;
            }
        }

        public void Dispose()
        {
            disposed = true;
        }

        /// <summary>
        /// Add an item to the queue
        /// </summary>
        /// <param name="producible"></param>
        public bool Add(T producible)
        {
            if (disposed)
                return false;

            return blockingCollection.TryAdd(producible,TimeSpan.FromMilliseconds(100));
        }

        /// <summary>
        /// Look at the next item in the collection without removing it, emulating the behavior of a traditional FIFO queue's Peek methodology.
        /// </summary>
        public T Peek()
        {
            if (disposed)
            {
                return default(T);
            }

            T result = blockingCollection.FirstOrDefault();
            return result;
        }

        /// <summary>
        /// Take the item from the collection, emulating the behavior of a traditional FIFO queue's Take methodology.
        /// </summary>
        /// <returns></returns>
        public T Take()
        {
            if (disposed)
            {
                return default(T);
            }

            T result = blockingCollection.FirstOrDefault();

            if (result != null)
            {
                blockingCollection.Take();
            }

            return result;
        }

        /// <summary>
        /// Used for debugging/logging so you can see what's actually in the underlying collection.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (disposed)
                return string.Empty;

            return "BlockingQueueItems:" + string.Join("**", blockingCollection);
        }

        /// <summary>
        /// Peak at the next item like it's a queue (FIFO) and wait until something is really there.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public T PeekAndWait(CancellationToken token)
        {
            if (disposed || token.IsCancellationRequested)
                return default(T);

            T result = Peek();

            while (result == null)
            {
                if (token.IsCancellationRequested || disposed)
                {
                    return default(T);
                }

                Thread.Sleep(50);
                result = Peek();
            }

            return result;
        }
    }
}