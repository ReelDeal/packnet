﻿namespace PackNet.Common.Utils
{
    using System;

    [Serializable]
    public class InvalidPropertyNameException : Exception
    {

        public string PropertyName { get; private set; }
        public object Holder { get; private set; }

        public InvalidPropertyNameException(object holder, string propertyName)
            : base()
        {
            PropertyName = propertyName;
            Holder = holder;
        }
    }
}
