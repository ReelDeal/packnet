﻿namespace PackNet.Common.Utils
{
    using System;
    using System.Reflection;
    using System.Text.RegularExpressions;

    public static class FieldReplacement
    {

        #region Fields
        #endregion

        #region Ctor
        #endregion

        #region Properties
        #endregion

        #region Methods

        public static string ReplaceFieldWithData(object data, string field, string magicField)
        {
            string fieldData = string.Empty;
            if (!ObjectModifier.TryGetValue(data, field, ref fieldData))
            {
                var job = string.Empty;
                if (!ObjectModifier.TryGetValue(data, magicField, ref job))
                {
                    throw new InvalidPropertyNameException(data, field);
                }
                Type t = data.GetType();
                PropertyInfo p = t.GetProperty(magicField);

                object jobData = p.GetValue(data, null);

                if (jobData == null || !ObjectModifier.TryGetValue(jobData, field, ref fieldData))
                {
                    throw new InvalidPropertyNameException(data, field);
                }
            }

            return fieldData;
        }

        public static string RegexReplaceObjectFields(
            object dataObject, 
            string fields, 
            string regex,
            string fieldStartingDelimeter = "[", 
            string fieldEndingDelimeter = "]")
        {
            var result = fields;
            Regex regEx = new Regex(regex);
            Match match;

            var regexpMatch = true;
            while (regexpMatch)
            {
                match = regEx.Match(result);
                regexpMatch = match.Success;

                if (regexpMatch)
                {
                    var field = match.Value.Replace(fieldStartingDelimeter, "").Replace(fieldEndingDelimeter, "");
                    result = result.Replace(match.Value, ReplaceFieldWithData(dataObject, field, "Job"));
                }
            }

            return result;
        }

        #endregion
    }
}
