﻿namespace PackNet.Common.Utils
{
    using System.Xml;

    public class WareWolfSettings
    {
        private XmlNode root;

        public WareWolfSettings(string path)
        {
            XmlDocument config = new XmlDocument();
            config.Load(path);
            root = config.SelectSingleNode("WareWolfConfig");
        }

        public string Version
        {
            get
            {
                XmlNode version = root.SelectSingleNode("//Version");
                if (version != null)
                {
                    return version.InnerText;
                }

                return "Unknown";
            }
        }

        public string DataRoot
        {
            get
            {
                XmlNode data = root.SelectSingleNode("//DataRoot");
                if (data != null)
                {
                    return data.InnerText;
                }

                return "Unknown";
            }
        }

        public string RuntimeDatabasePath
        {
            get
            {
                XmlNode db = root.SelectSingleNode("Database");
                if (db != null)
                {
                    db = db.SelectSingleNode("ArticleDatabase");
                    if (db != null)
                    {
                        return db.InnerText;
                    }
                }

                return string.Empty;
            }
        }

        public string HistoryDatabasePath
        {
            get
            {
                XmlNode db = root.SelectSingleNode("Database");
                if (db != null)
                {
                    db = db.SelectSingleNode("HistoryDatabase");
                    if (db != null)
                    {
                        return db.InnerText;
                    }
                }

                return string.Empty;
            }
        }

        public string ArticleDatabasePath
        {
            get
            {
                XmlNode db = root.SelectSingleNode("Database");
                if (db != null)
                {
                    db = db.SelectSingleNode("ArticleDatabase");
                    if (db != null)
                    {
                        return db.InnerText;
                    }
                }

                return string.Empty;
            }
        }

        public string QueueDatabasePath
        {
            get
            {
                XmlNode db = root.SelectSingleNode("Database");
                if (db != null)
                {
                    db = db.SelectSingleNode("QueueDatabase");
                    if (db != null)
                    {
                        return db.InnerText;
                    }
                }

                return string.Empty;
            }
        }

        public string SystemDatabasePath
        {
            get
            {
                XmlNode db = root.SelectSingleNode("Database");
                if (db != null)
                {
                    db = db.SelectSingleNode("SystemDatabase");
                    if (db != null)
                    {
                        return db.InnerText;
                    }
                }

                return string.Empty;
            }
        }

        public string ActivationCodeFile
        {
            get
            {
                XmlNode code = root.SelectSingleNode("ActivationCodeFile");
                if (code != null)
                {
                    return code.InnerText;
                }

                return string.Empty;
            }
        }

        public string MachinePath
        {
            get
            {
                XmlNode machine = root.SelectSingleNode("MachineConfig");
                if (machine != null)
                {
                    return machine.InnerText;
                }

                return string.Empty;
            }
        }

        public string IpAddress
        {
            get
            {
                XmlNode com = root.SelectSingleNode("PLCCommunication");
                if (com != null)
                {
                    com = com.SelectSingleNode("IPAddress");
                    if (com != null)
                    {
                        return com.InnerText;
                    }
                }

                return "192.168.0.5";
            }
        }
    }
}
