﻿//To ensure compatibility with Vista and Win7, add the following manifest to the .NET parent process:

//<?xml version="1.0" encoding="utf-8" standalone="yes"?>
//<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
//  <v3:trustInfo xmlns:v3="urn:schemas-microsoft-com:asm.v3">
//    <v3:security>
//      <v3:requestedPrivileges>
//        <v3:requestedExecutionLevel level="asInvoker" uiAccess="false" />
//      </v3:requestedPrivileges>
//    </v3:security>
//  </v3:trustInfo>
//  <compatibility xmlns="urn:schemas-microsoft-com:compatibility.v1">
//    <!-- We specify these, in addition to the UAC above, so we avoid Program Compatibility Assistant in Vista and Win7 -->
//    <!-- We try to avoid PCA so we can use Windows Job Objects -->
//    <!-- See http://stackoverflow.com/questions/3342941/kill-child-process-when-parent-process-is-killed -->

//    <application>
//      <!--The ID below indicates application support for Windows Vista -->
//      <supportedOS Id="{e2011457-1546-43c5-a5fe-008deee3d3f0}"/>
//      <!--The ID below indicates application support for Windows 7 -->
//      <supportedOS Id="{35138b9a-5d96-4fbd-8e2d-a2440225f93a}"/>
//    </application>
//  </compatibility>
//</assembly>

namespace PackNet.Common.Utils.JobObjects
{
    using System;
    using System.Diagnostics;
    using System.Runtime.InteropServices;

    public class Job : IDisposable
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        static extern IntPtr CreateJobObject(IntPtr a, string lpName);

        [DllImport("kernel32.dll")]
        static extern bool SetInformationJobObject(IntPtr hJob, JobObjectInfoType infoType, IntPtr lpJobObjectInfo, UInt32 cbJobObjectInfoLength);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AssignProcessToJobObject(IntPtr job, IntPtr process);

        private IntPtr handle;
        private bool disposed;

        public Job()
        {
            handle = CreateJobObject(IntPtr.Zero, null);

            var info = new JOBOBJECT_BASIC_LIMIT_INFORMATION
            {
                LimitFlags = 0x2000
            };

            var extendedInfo = new JOBOBJECT_EXTENDED_LIMIT_INFORMATION
            {
                BasicLimitInformation = info
            };

            int length = Marshal.SizeOf(typeof(JOBOBJECT_EXTENDED_LIMIT_INFORMATION));
            IntPtr extendedInfoPtr = Marshal.AllocHGlobal(length);
            Marshal.StructureToPtr(extendedInfo, extendedInfoPtr, false);

            if (!SetInformationJobObject(handle, JobObjectInfoType.ExtendedLimitInformation, extendedInfoPtr, (uint)length))
                throw new Exception(string.Format("Unable to set information.  Error: {0}", Marshal.GetLastWin32Error()));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing) { }

            Close();
            disposed = true;
        }

        public void Close()
        {
            CloseHandle(handle);
            handle = IntPtr.Zero;
        }

        public bool AddProcess(IntPtr processHandle)
        {
            var result = AssignProcessToJobObject(handle, processHandle);
            if (!result)
                throw new Exception(string.Format("Could not assign process to job. Error: {0}", Marshal.GetLastWin32Error()));
            
            return result;
        }

        public bool AddProcess(int processId)
        {
            return AddProcess(Process.GetProcessById(processId));
        }

        public bool AddProcess(Process process)
        {
            return AddProcess(process.Handle);
        }
    }

    #region Helper classes

    [StructLayout(LayoutKind.Sequential)]
    struct IO_COUNTERS
    {
        public UInt64 ReadOperationCount;
        public UInt64 WriteOperationCount;
        public UInt64 OtherOperationCount;
        public UInt64 ReadTransferCount;
        public UInt64 WriteTransferCount;
        public UInt64 OtherTransferCount;
    }


    [StructLayout(LayoutKind.Sequential)]
    struct JOBOBJECT_BASIC_LIMIT_INFORMATION
    {
        public Int64 PerProcessUserTimeLimit;
        public Int64 PerJobUserTimeLimit;
        public UInt32 LimitFlags;
        public UIntPtr MinimumWorkingSetSize;
        public UIntPtr MaximumWorkingSetSize;
        public UInt32 ActiveProcessLimit;
        public UIntPtr Affinity;
        public UInt32 PriorityClass;
        public UInt32 SchedulingClass;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct SECURITY_ATTRIBUTES
    {
        public UInt32 nLength;
        public IntPtr lpSecurityDescriptor;
        public Int32 bInheritHandle;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct JOBOBJECT_EXTENDED_LIMIT_INFORMATION
    {
        public JOBOBJECT_BASIC_LIMIT_INFORMATION BasicLimitInformation;
        public IO_COUNTERS IoInfo;
        public UIntPtr ProcessMemoryLimit;
        public UIntPtr JobMemoryLimit;
        public UIntPtr PeakProcessMemoryUsed;
        public UIntPtr PeakJobMemoryUsed;
    }

    public enum JobObjectInfoType
    {
        AssociateCompletionPortInformation = 7,
        BasicLimitInformation = 2,
        BasicUIRestrictions = 4,
        EndOfJobTimeInformation = 6,
        ExtendedLimitInformation = 9,
        SecurityLimitInformation = 5,
        GroupInformation = 11
    }

    #endregion
}
