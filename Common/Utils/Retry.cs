﻿
// Stolen from https://github.com/TestStack/White
// https://github.com/TestStack/White/blob/master/src/TestStack.White/Utility/Retry.cs
// License info:
//  https://github.com/TestStack/White/blob/master/LICENSE.txt
//  https://github.com/TestStack/White/blob/master/LICENSE-MIT.txt
//  https://github.com/TestStack/White/blob/master/LICENSE-APACHE.txt

using System.Collections.Generic;

namespace PackNet.Common.Utils
{
    using System;
    using System.Threading;

    public static class Retry
    {
        private static readonly TimeSpan DefaultRetryInterval = TimeSpan.FromMilliseconds(200);

        /// <summary>
        /// Retries until action does not throw an exception
        /// </summary>
        /// <param name="action">The operation to perform.</param>
        /// <param name="retryFor">The duration before timing outs.</param>
        public static void For(Action action, TimeSpan retryFor)
        {
            var startTime = DateTime.UtcNow;
            while (DateTime.UtcNow.Subtract(startTime).TotalMilliseconds < retryFor.TotalMilliseconds)
            {
                try
                {
                    action();
                    return;
                }
                catch (Exception)
                {
                    Thread.Sleep(DefaultRetryInterval);
                }
            }

            action();
        }

        /// <summary>
        /// Retries until method returns true
        /// </summary>
        /// <param name="getMethod">The operation to perform.</param>
        /// <param name="retryFor">The duration before timing out.</param>
        /// <param name="retryInterval">The time to sleep betwen retries.</param>
        public static bool For(Func<bool> getMethod, TimeSpan retryFor, TimeSpan? retryInterval = null)
        {
            return For(getMethod, g => !g, retryFor, retryInterval);
        }

        public static bool UntilTrue(Func<bool> getMethod, TimeSpan retryFor, TimeSpan? retryInterval = null)
        {
            return For(getMethod, g => !g, retryFor, retryInterval);
        }


        /// <summary>
        /// Retries until method returns a non-default value
        /// </summary>
        /// <param name="getMethod">The operation to perform.</param>
        /// <param name="retryFor">The duration before timing out.</param>
        /// <param name="retryInterval">The time to sleep betwen retries.</param>
        public static T For<T>(Func<T> getMethod, TimeSpan retryFor, TimeSpan? retryInterval = null)
        {
            //If T is a value type, by default we should retry if the value is default
            //Reference types will return false, so our predicate will always pass
            return For(getMethod, IsValueTypeAndDefault, retryFor, retryInterval);
        }

        /// <summary>
        /// Retries as long as predicate is satisfied
        /// </summary>
        /// <param name="getMethod">The operation to perform.</param>
        /// <param name="shouldRetry">The predicate used for retry.</param>
        /// <param name="retryFor">The duration before timing out.</param>
        /// <param name="retryInterval">The time to sleep betwen retries.</param>
        public static T For<T>(Func<T> getMethod, Predicate<T> shouldRetry, TimeSpan retryFor, TimeSpan? retryInterval = null)
        {
            var startTime = DateTime.UtcNow;
            T element;
            while (DateTime.UtcNow.Subtract(startTime).TotalMilliseconds < retryFor.TotalMilliseconds)
            {
                try
                {
                    element = getMethod();
                }
                catch (Exception)
                {
                    Thread.Sleep(retryInterval ?? DefaultRetryInterval);
                    continue;
                }

                if (!typeof(T).IsValueType && element != null && !shouldRetry(element))
                    return element;

                //Making it safe for bool and value types and reference types
                if (typeof(T) == typeof(bool) && !shouldRetry(element))
                    return element;

                if (typeof(T) != typeof(bool) &&
                    !IsReferenceTypeAndIsNull(element) &&
                    !shouldRetry(element))
                {
                    return element;
                }

                Thread.Sleep(retryInterval ?? DefaultRetryInterval);
            }

            element = getMethod();
            return element;
        }

        private static bool IsReferenceTypeAndIsNull<T>(T element)
        {
            return (!(typeof(T).IsValueType) && ReferenceEquals(element, null));
        }

        private static bool IsValueTypeAndDefault<T>(T element)
        {
            return (typeof(T).IsValueType && element.Equals(default(T)));
        }

        /// <summary>
        /// Try some action, if it does not succeed then wait for the specified interval and then try again.  Repeat for the specified retry count.
        /// </summary>
        /// <param name="action">Something to try to perform.</param>
        /// <param name="retryInterval">duration to wait from time of failure until trying again.  Uses Thread.Sleep </param>
        /// <param name="retryCount">How many times do you want to attempt to perform the action</param>
        public static void Do(Action action, TimeSpan retryInterval, int retryCount = 3)
        {
            Do<object>(() =>
            {
                action();
                return null;
            }, retryInterval, retryCount);
        }

        public static void Do(Action action, int retryCount = 3)
        {
            Do<object>(() =>
            {
                action();
                return null;
            }, TimeSpan.FromMilliseconds(20), retryCount);
        }

        /// <summary>
        /// Try some function, if it does not succeed then wait for the specified interval and then try again.  Repeat for the specified retry count.
        /// </summary>
        /// <param name="action">Something to try to perform.</param>
        /// <param name="retryInterval">duration to wait from time of failure until trying again.  Uses Thread.Sleep </param>
        /// <param name="retryCount">How many times do you want to attempt to perform the action</param>
        /// <returns>Defined generic type that your function returns</returns>
        /// <exception cref="System.AggregateException"></exception>
        public static T Do<T>(Func<T> action, TimeSpan retryInterval, int retryCount = 3)
        {
            var exceptions = new List<Exception>();

            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                    Thread.Sleep(retryInterval);
                }
            }

            throw new AggregateException(exceptions);
        }

        public static bool DoUntilTrue(Func<bool> action, TimeSpan retryInterval, int retryCount = 3, bool throwEx = true)
        {
            var exceptions = new List<Exception>();

            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    var result = action();
                    if (!result)
                    {
                        Thread.Sleep(retryInterval);
                        continue;
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                    Thread.Sleep(retryInterval);
                }
            }
            if (throwEx)
                throw new AggregateException(exceptions);

            return false;
        }
    }
}

