using System;
using System.Diagnostics;

namespace PackNet.Common.Utils
{
    /// <summary>
    /// Simple wrapper around System.Timers.Timer
    /// </summary>
    public class Timer : IDisposable
    {
        public string TimerLabel { get; private set; }
        private System.Timers.Timer timer;
        //Please Leave this here for debugging until we are solid
        private bool isDebug = false;

        public Timer(Action callbackAction, TimeSpan triggerInterval) :
            this("unlabeled timer", callbackAction, triggerInterval){}

        public Timer(string timerLabel, Action callbackAction, TimeSpan triggerInterval)
        {
            TimerLabel = timerLabel;
            timer = new System.Timers.Timer(triggerInterval.TotalMilliseconds)
            {
                AutoReset = true
            };
            timer.Elapsed += (sender, args) => callbackAction();
        }

        //Please Leave this here for debugging until we are solid
        private void DebugOutput(string message)
        {
            if (!isDebug)
                return;

            Debug.WriteLine(message);
            Console.WriteLine(message);
        }

        public void Start()
        {
            DebugOutput(TimerLabel + " starting");
            timer.Start();
        }

        public void Stop()
        {
            DebugOutput(TimerLabel + " stopping");

            if (timer != null)
            {
                timer.Stop(); 
            }
        }

        public void Dispose()
        {
            DebugOutput(TimerLabel + " disposing");
            
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
        }
    }
}