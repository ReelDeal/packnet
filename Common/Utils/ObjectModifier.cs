namespace PackNet.Common.Utils
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization.Formatters.Binary;

    public static class ObjectModifier
    {
        public static void SetValue(object obj, string propertyName, object propertyValue)
        {
            Type t = obj.GetType();
            PropertyInfo p = t.GetProperty(propertyName);
            Type propertyType = null;
            if (p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                propertyType = Nullable.GetUnderlyingType(p.PropertyType);
                if (propertyValue == null || string.IsNullOrEmpty(propertyValue.ToString()))
                {
                    p.SetValue(obj, null, null);
                    return;
                }
            }
            else
            {
                if (propertyValue == null || string.IsNullOrEmpty(propertyValue.ToString()))
                {
                    return;
                }

                propertyType = p.PropertyType;
            }

            NumberFormatInfo m_numberFormatInfo = (NumberFormatInfo)CultureInfo.InstalledUICulture.NumberFormat.Clone();
            m_numberFormatInfo.NumberDecimalSeparator = ".";
            if (p.PropertyType != typeof(string) && propertyValue.GetType() == typeof(string))
            {
                propertyValue = ((string)propertyValue).Replace(',', '.');
            }

            if (propertyType.IsEnum)
            {
                p.SetValue(obj, Enum.Parse(propertyType, propertyValue.ToString(), true), null);
            }
            else
            {
                PropertyDescriptor pd = TypeDescriptor.GetProperties(obj)[propertyName];
                if (pd.Converter.CanConvertFrom(propertyValue.GetType()))
                {
                    p.SetValue(obj, pd.Converter.ConvertFrom(null, CultureInfo.InvariantCulture, propertyValue), null);
                }
                else
                {
                    p.SetValue(obj, Convert.ChangeType(propertyValue, propertyType, m_numberFormatInfo), null);
                }
            }
        }

        public static string GetValue(object obj, string propertyName)
        {
            Type t = obj.GetType();
            PropertyInfo p = t.GetProperty(propertyName);
            if (p == null)
            {
                throw new InvalidPropertyNameException(obj, propertyName);
            }

            object val = p.GetValue(obj, null);

            return val == null ? string.Empty : val.ToString();
        }

        public static bool TryGetValue(object obj, string field, ref string fieldData)
        {
            try
            {
                fieldData = GetValue(obj, field);
                return true;
            }
            catch (InvalidPropertyNameException)
            {
                return false;
            }
        }

        /// <summary>
        /// Creates a clone of the object. Note that any delegates will not be cloned.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static T CloneObject<T>(T source) where T : class
        {
            if (source == null)
            {
                return null;
            }

            MemoryStream memory = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(memory, source);
            memory.Position = 0;
            return (T)formatter.Deserialize(memory);
        }
    }
}
