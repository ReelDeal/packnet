﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;

using PackNet.Common.Interfaces;
using PackNet.Common.L10N;

namespace PackNet.Common.Utils
{
    public static class DirectoryHelpers
    {
        public static void Copy(string source, string target)
        {
            Copy(source, target, false);
        }

        public static void Copy(string source, string target, bool overwrite)
        {
            if (Directory.Exists(target) == false)
            {
                Directory.CreateDirectory(target);
            }

            foreach (var file in Directory.GetFiles(source))
            {
                var fileName = Path.GetFileName(file);
                if (string.IsNullOrEmpty(fileName)) continue;

                if (overwrite || File.Exists(Path.Combine(target, fileName)) == false)
                {
                    File.Copy(file, Path.Combine(target, fileName), overwrite);
                }
            }

            foreach (var subDir in Directory.GetDirectories(source))
            {
                var fileName = Path.GetFileName(subDir);
                if (string.IsNullOrEmpty(fileName)) continue;

                Copy(subDir, Path.Combine(target, fileName), overwrite);
            }
        }

        /// <summary>
        /// This method validates if a directory path is accessible. It is a more effective check than Directory.Exists() method.
        /// </summary>
        /// <param name="path">Directory path to check</param>
        /// <returns>True if directory path is accessible, false otherwise</returns>
        /// <remarks>
        /// Credit http://stackoverflow.com/questions/11709862/check-if-directory-is-accessible-in-c
        /// </remarks>
        public static bool CanRead(string path)
        {
            var readAllow = false;
            var readDeny = false;

            DirectorySecurity accessControlList;
            try
            {
                accessControlList = Directory.GetAccessControl(path);
            }
            catch (UnauthorizedAccessException)
            {
                return false;
            }
            catch (IOException)
            {
                return false;
            }
            if (accessControlList == null) return false;

            var accessRules = accessControlList.GetAccessRules(true, true, typeof(SecurityIdentifier));

            foreach (var rule in accessRules.Cast<FileSystemAccessRule>().Where(rule => (FileSystemRights.Read & rule.FileSystemRights) == FileSystemRights.Read))
            {
                switch (rule.AccessControlType)
                {
                    case AccessControlType.Allow:
                        readAllow = true;
                        break;
                    case AccessControlType.Deny:
                        readDeny = true;
                        break;
                }
            }

            return readAllow && !readDeny;
        }

        private static Regex envVarSearchExpression = new Regex(@"%([^%]+)%", RegexOptions.Compiled);
        /// <summary>
        /// Resolves all environement variables in the supplied path. Nested environment variables are recursively resolved.
        /// </summary>
        /// <param name="path">The file path that requires resolving.</param>
        /// <returns>A string with the environment variables delimited with % resolved to their values.</returns>
        public static string ReplaceEnvironmentVariables(string path)
        {
            Match match = envVarSearchExpression.Match(path);

            if (match.Groups.Count == 0)
            {
                return path;
            }

            for (int i = 1; i < match.Groups.Count; i++)
            {
                string varName = match.Groups[i].Value;
                string varValue = Environment.GetEnvironmentVariable(varName, EnvironmentVariableTarget.Process);
                
                if (varValue == null)
                {
                    varValue = Environment.GetEnvironmentVariable(varName, EnvironmentVariableTarget.User);
                
                    if (varValue == null)
                    {
                        varValue = Environment.GetEnvironmentVariable(varName, EnvironmentVariableTarget.Machine);
                    
                        if (varValue == null)
                        {
                            throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, Strings.EnvironmentVariableNotDefinedErrorText, varName));
                        }
                    }
                }

                path = ReplaceEnvironmentVariables(path.Replace("%" + varName + "%", varValue));
            }

            return path;
        }
    }
}
