using System;
using System.Runtime.InteropServices;
using PackNet.Common.HttpNamespaceReserver.Structs;

namespace PackNet.Common.HttpNamespaceReserver.Facades
    {
    public class HttpApiFacade : IHttpApiFacade
    {
        const int HttpInitializeConfig = 2;

        public int InitializeHttp()
        {
            return HttpInitialize(new HttpApiVersion(1, 0), HttpInitializeConfig, IntPtr.Zero);
        }

        [DllImport("httpapi.dll")]
        static extern int HttpInitialize(HttpApiVersion version, int flags, IntPtr mustBeZero);

        public int TerminateHttp()
        {
            return HttpTerminate(HttpInitializeConfig, IntPtr.Zero);
        }

        [DllImport("httpapi.dll")]
        static extern int HttpTerminate(int flags, IntPtr mustBeZero);

        
        public int DeleteServiceConfigAcl(DeleteParams deleteParams)
        {
            var set = deleteParams.ConfigInfo;
            var configInfoLength = Marshal.SizeOf(set.GetType());
            return HttpDeleteServiceConfigurationAcl(deleteParams.ServiceHandle, deleteParams.ConfigId, ref set, configInfoLength, deleteParams.Overlapped);
        }

        [DllImport("httpapi.dll", ExactSpelling = true, EntryPoint = "HttpDeleteServiceConfiguration")]
        static extern int HttpDeleteServiceConfigurationAcl(IntPtr mustBeZero, int configId, [In] ref HttpServiceConfigUrlaclSet configInfo, int configInfoLength, IntPtr mustBeZero2);

        public int SetServiceConfigAcl(AddParams addParams)
        {
            var set = addParams.ConfigInfo;
            var configInfoLength = Marshal.SizeOf(set.GetType());
            return HttpSetServiceConfigurationAcl(addParams.ServiceHandle, addParams.ConfigId, ref set, configInfoLength, addParams.Overlapped);
        }

        [DllImport("httpapi.dll", ExactSpelling = true, EntryPoint = "HttpSetServiceConfiguration")]
        static extern int HttpSetServiceConfigurationAcl(IntPtr mustBeZero, int configId, [In] ref HttpServiceConfigUrlaclSet configInfo, int configInfoLength, IntPtr mustBeZero2);

        public uint HttpQueryServiceConfig(QueryParams queryParams)
        {
            var returnSize = queryParams.ReturnSize;
            var result = HttpQueryServiceConfiguration(queryParams.ServiceHandle, queryParams.ConfigId, queryParams.QueryPointer, (uint)Marshal.SizeOf(queryParams.Query), queryParams.OutputConfigInfoPointer, returnSize, ref returnSize, queryParams.Overlapped);
            queryParams.ReturnSize = returnSize;
            return result;
        }

        [DllImport("Httpapi.dll", ExactSpelling = true, EntryPoint = "HttpQueryServiceConfiguration")]
        static extern uint HttpQueryServiceConfiguration(IntPtr serviceHandle, int configId,
                                                         IntPtr pInputConfigInfo, uint inputConfigLength,
                                                         IntPtr pOutputConfigInfo, uint outputConfigInfoLength,
                                                         ref uint pReturnLength, IntPtr pOverlapped);
    }
}