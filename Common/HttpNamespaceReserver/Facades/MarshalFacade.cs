using System;
using System.Runtime.InteropServices;

namespace PackNet.Common.HttpNamespaceReserver.Facades
{
    public class MarshalFacade : IMarshalFacade
    {
        public void StructureToPtr(object query, IntPtr pointer, bool removeOld)
        {
            Marshal.StructureToPtr(query, pointer, false);
        }

        public T PtrToStructure<T>(IntPtr pointer)
        {
            return (T)Marshal.PtrToStructure(pointer, typeof(T));
        }

        public void DestroyStructure<T>(IntPtr pointer)
        {
            Marshal.DestroyStructure(pointer, typeof(T));
        }

        public void FreeHGlobal(IntPtr pointer)
        {
            Marshal.FreeHGlobal(pointer);
        }

        public IntPtr AllocHGlobal(IntPtr pointer)
        {
            return Marshal.AllocHGlobal(pointer);
        }

        public IntPtr AllocHGlobal(int cb)
        {
            return Marshal.AllocHGlobal(cb);
        }

        public int SizeOf(object structure)
        {
            return Marshal.SizeOf(structure);
        }
    }
}