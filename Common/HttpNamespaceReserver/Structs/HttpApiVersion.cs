using System.Runtime.InteropServices;

namespace PackNet.Common.HttpNamespaceReserver.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct HttpApiVersion
    {
        public HttpApiVersion(short maj, short min)
        {
            Major = maj;
            Minor = min;
        }

        public readonly short Major;
        public readonly short Minor;
    }
}