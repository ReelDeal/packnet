using System.Runtime.InteropServices;

namespace PackNet.Common.HttpNamespaceReserver.Structs
{
    public struct HttpServiceConfigUrlaclKey
    {
        [MarshalAs(UnmanagedType.LPWStr)]
        public string PUrlPrefix;
    }
}