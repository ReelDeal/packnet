using System.Runtime.InteropServices;

namespace PackNet.Common.HttpNamespaceReserver.Structs
{
    public struct HttpServiceConfigUrlaclParam
    {
        [MarshalAs(UnmanagedType.LPWStr)]
        public string PStringSecurityDescriptor;
    }
}