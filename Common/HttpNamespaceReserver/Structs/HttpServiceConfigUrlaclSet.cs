using System.Runtime.InteropServices;
using System.Security.Principal;

namespace PackNet.Common.HttpNamespaceReserver.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct HttpServiceConfigUrlaclSet
    {
        public HttpServiceConfigUrlaclKey KeyDesc;
        public HttpServiceConfigUrlaclParam ParamDesc;

        public static HttpServiceConfigUrlaclSet CreateConfigInfo(string urlPrefix, string accountName)
        {
            var sid = new NTAccount(accountName).Translate(typeof(SecurityIdentifier)).ToString();
            var sddl = string.Format("D:(A;;GX;;;{0})", sid);
            HttpServiceConfigUrlaclSet configInfo;
            configInfo.KeyDesc.PUrlPrefix = urlPrefix;
            configInfo.ParamDesc.PStringSecurityDescriptor = sddl;
            return configInfo;
        }
    }
}