namespace PackNet.Common.HttpNamespaceReserver.Structs
{
    public enum HttpServiceConfigQueryType
    {
        HttpServiceConfigQueryExact,
        HttpServiceConfigQueryNext,
        HttpServiceConfigQueryMax
    }
}