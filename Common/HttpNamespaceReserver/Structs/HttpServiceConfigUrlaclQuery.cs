using System.Runtime.InteropServices;

namespace PackNet.Common.HttpNamespaceReserver.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct HttpServiceConfigUrlaclQuery
    {
        public HttpServiceConfigQueryType QueryDesc;
        public HttpServiceConfigUrlaclKey KeyDesc;
        public uint dwToken;
    }
}