using System;

namespace PackNet.Common.HttpNamespaceReserver
{
    public interface IMarshalFacade
    {
        void StructureToPtr(object query, IntPtr pointer, bool removeOld);
        T PtrToStructure<T>(IntPtr pointer);
        void DestroyStructure<T>(IntPtr pointer);
        void FreeHGlobal(IntPtr pointer);
        IntPtr AllocHGlobal(IntPtr pointer);
        IntPtr AllocHGlobal(int cb);
        int SizeOf(object structure);
    }
}