using System;

using PackNet.Common.HttpNamespaceReserver.Structs;

namespace PackNet.Common.HttpNamespaceReserver
{
    public class AddParams : DeleteParams
    {
        public AddParams(IntPtr serviceHandle, int configId, HttpServiceConfigUrlaclSet configInfo, IntPtr overlapped) 
            : base(serviceHandle, configId, configInfo, overlapped) {}
    }
}