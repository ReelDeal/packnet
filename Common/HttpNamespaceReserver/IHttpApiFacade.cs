

namespace PackNet.Common.HttpNamespaceReserver
{
    public interface IHttpApiFacade
    {
        int DeleteServiceConfigAcl(DeleteParams deleteParams);
        int SetServiceConfigAcl(AddParams addParams);
        int InitializeHttp();
        int TerminateHttp();
        uint HttpQueryServiceConfig(QueryParams queryParams);
    }
}