using System;
using System.Collections.Generic;

using PackNet.Common.HttpNamespaceReserver.Structs;

namespace PackNet.Common.HttpNamespaceReserver
{
    public class NamespaceReserver
    {
        private readonly IHttpCommandFactory httpCommandFactory;
        const int HttpServiceConfigUrlAclInfo = 2;

        public NamespaceReserver(IHttpCommandFactory httpCommandFactory)
        {
            this.httpCommandFactory = httpCommandFactory;
        }

        public Dictionary<string, string> GetAclEntries()
        {
            var queryParams = new QueryParams(IntPtr.Zero, 2, IntPtr.Zero, IntPtr.Zero, 0, IntPtr.Zero, 0);
            var cmd = httpCommandFactory.GetCommand(queryParams);
            cmd.Execute();
            return queryParams.Result;
        }

        public void ReserveNamespace(string urlPrefix, string accountName)
        {
            var configInfo = HttpServiceConfigUrlaclSet.CreateConfigInfo(urlPrefix, accountName);
            var addDeleteParams = new AddParams(IntPtr.Zero, HttpServiceConfigUrlAclInfo, configInfo, IntPtr.Zero)
                                       {
                                           AccountName = accountName,
                                           UrlPrefix = urlPrefix
                                       };
            var cmd = httpCommandFactory.GetCommand(addDeleteParams);
            cmd.Execute();
        }

        public void RemoveNamespaceReservation(string urlPrefix, string accountName)
        {
            var configInfo = HttpServiceConfigUrlaclSet.CreateConfigInfo(urlPrefix, accountName);
            var addDeleteParams = new DeleteParams(IntPtr.Zero, HttpServiceConfigUrlAclInfo, configInfo, IntPtr.Zero)
                                       {
                                           AccountName = accountName,
                                           UrlPrefix = urlPrefix
                                       };
            var cmd = httpCommandFactory.GetCommand(addDeleteParams);
            cmd.Execute();
        }
    }
}