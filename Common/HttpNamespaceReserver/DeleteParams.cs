using System;
using PackNet.Common.HttpNamespaceReserver.Structs;

namespace PackNet.Common.HttpNamespaceReserver
{
    public class DeleteParams
    {
        private readonly IntPtr serviceHandle;
        private readonly int configId;
        private readonly IntPtr overlapped;
        
        public DeleteParams(IntPtr serviceHandle, int configId, HttpServiceConfigUrlaclSet configInfo, IntPtr overlapped)
        {
            this.serviceHandle = serviceHandle;
            this.configId = configId;
            ConfigInfo = configInfo;
            this.overlapped = overlapped;
        }

        public HttpServiceConfigUrlaclSet ConfigInfo { get; private set; }

        public IntPtr ServiceHandle
        {
            get { return serviceHandle; }
        }

        public int ConfigId
        {
            get { return configId; }
        }
        
        public IntPtr Overlapped
        {
            get { return overlapped; }
        }

        public string UrlPrefix { get; set; }
        public string AccountName { get; set; }
    }
}