using System;
using PackNet.Common.HttpNamespaceReserver.Structs;

namespace PackNet.Common.HttpNamespaceReserver.CommandsAndQueries
{
    public class GetAllReservationsQuery : ICommand
    {
        private readonly QueryParams queryParams;
        private readonly IHttpApiFacade facade;
        private readonly IMarshalFacade marshalFacade;

        public GetAllReservationsQuery(QueryParams queryParams, IHttpApiFacade facade, IMarshalFacade marshalFacade)
        {
            this.queryParams = queryParams;
            this.facade = facade;
            this.marshalFacade = marshalFacade;
        }

        public void Execute()
        {
            var sizeOf = marshalFacade.SizeOf(queryParams.Query);
            queryParams.QueryPointer = marshalFacade.AllocHGlobal(sizeOf);
            GetAllReservations(queryParams);
        }

        public void GetAllReservations(QueryParams queryParams)
        {
            try
            {
                var queryStatus = PerformQuery(queryParams);
                if (queryStatus != ErrorNoMoreItems)
                {
                    throw new HttpApiException("HttpQueryServiceConfiguration", (int)queryStatus);
                }
            }
            finally
            {
                marshalFacade.FreeHGlobal(queryParams.QueryPointer);
            }
        }


        private uint PerformQuery(QueryParams queryParams)
        {
            var query = queryParams.Query;
            uint queryStatus = NoError;
            for (query.dwToken = 0; true; query.dwToken++)
            {
                marshalFacade.StructureToPtr(query, queryParams.QueryPointer, false);
                try
                {
                    queryStatus = facade.HttpQueryServiceConfig(queryParams);
                    if (queryStatus == ErrorNoMoreItems)
                    {
                        break;
                    }
                    if (queryStatus != ErrorInsufficientBuffer)
                    {
                        throw new HttpApiException("HttpQueryServiceConfiguration", (int)queryStatus);
                    }
                    QueryHttpApi(queryParams);
                }
                finally
                {
                    marshalFacade.DestroyStructure<HttpServiceConfigUrlaclQuery>(queryParams.QueryPointer);
                    queryParams.ReturnSize = 0;
                }
            }
            return queryStatus;
        }

        private void QueryHttpApi(QueryParams qParams)
        {
            qParams.OutputConfigInfoPointer = marshalFacade.AllocHGlobal((IntPtr) qParams.ReturnSize);
            try
            {
                var queryStatus = facade.HttpQueryServiceConfig(qParams);

                if (queryStatus == NoError)
                {
                    var config = marshalFacade.PtrToStructure<HttpServiceConfigUrlaclSet>(qParams.OutputConfigInfoPointer);
                    qParams.Result.Add(config.KeyDesc.PUrlPrefix, config.ParamDesc.PStringSecurityDescriptor);
                }
            }
            finally
            {
                marshalFacade.FreeHGlobal(qParams.OutputConfigInfoPointer);
            }
        }

        public const uint ErrorNoMoreItems = 259;
        public const uint ErrorInsufficientBuffer = 122;
        public const uint NoError = 0;
    }
}