namespace PackNet.Common.HttpNamespaceReserver.CommandsAndQueries
{
    public class HttpCommandWrapper : ICommand
    {
        private readonly ICommand commandToExecute;
        private readonly IHttpApiFacade facade;

        public HttpCommandWrapper(ICommand commandToExecute, IHttpApiFacade facade)
        {
            this.commandToExecute = commandToExecute;
            this.facade = facade;
        }

        public void Execute()
        {
            InitializeHttp();
            try
            {
                commandToExecute.Execute();
            }
            finally
            {
                TerminateHttp();
            }   
        }


        private void InitializeHttp()
        {
            var errorCode = facade.InitializeHttp();
            if (0 != errorCode) throw new HttpApiException("HttpInitialize", errorCode);
        }

        private void TerminateHttp()
        {
            var errorCode = facade.TerminateHttp();
            if (0 != errorCode) throw new HttpApiException("HttpTerminate", errorCode);
        }
    }
}