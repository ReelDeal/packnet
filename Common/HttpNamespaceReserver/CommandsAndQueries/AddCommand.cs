

namespace PackNet.Common.HttpNamespaceReserver.CommandsAndQueries
{
    public class AddCommand : ICommand
    {
        private readonly AddParams addParams;
        private readonly IHttpApiFacade facade;

        public AddCommand(AddParams addParams, IHttpApiFacade facade)
        {
            this.addParams = addParams;
            this.facade = facade;
        }

        public void Execute()
        {
            facade.DeleteServiceConfigAcl(addParams);
            var errorCode = facade.SetServiceConfigAcl(addParams);
            if (0 != errorCode) throw new HttpApiException("HttpSetServiceConfigurationAcl", errorCode);
        }
    }
}