namespace PackNet.Common.HttpNamespaceReserver.CommandsAndQueries
{
    public class DeleteCommand : ICommand
    {
        private readonly DeleteParams addDeleteParams;
        private readonly IHttpApiFacade facade;

        public DeleteCommand(DeleteParams addDeleteParams, IHttpApiFacade facade)
        {
            this.addDeleteParams = addDeleteParams;
            this.facade = facade;
        }

        public void Execute()
        {
            var errorCode = facade.DeleteServiceConfigAcl(addDeleteParams);
            if (0 != errorCode) throw new HttpApiException("HttpDeleteServiceConfigurationAcl", errorCode);
        }
    }
}