using System;
using System.Runtime.InteropServices;

namespace PackNet.Common.HttpNamespaceReserver.CommandsAndQueries
{
    public class HttpApiException : Exception
    {
        public HttpApiException(string fcn, int errorCode) : base(string.Format("{0} failed: {1}", fcn, GetWin32ErrorMessage(errorCode))) {}

        private static string GetWin32ErrorMessage(int errorCode)
        {
            int hr = HresultFromWin32(errorCode);
            var x = Marshal.GetExceptionForHR(hr);
            return x.Message;
        }

        private static int HresultFromWin32(int errorCode)
        {
            if (errorCode <= 0) return errorCode;
            return (int) ((0x0000FFFFU &
                           ((uint) errorCode)) | (7U << 16) |
                          0x80000000U);
        }
    }
}