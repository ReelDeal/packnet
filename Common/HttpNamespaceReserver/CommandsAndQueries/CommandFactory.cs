using PackNet.Common.HttpNamespaceReserver.Facades;

namespace PackNet.Common.HttpNamespaceReserver.CommandsAndQueries
{
    public class CommandFactory : IHttpCommandFactory
    {
        private readonly IHttpApiFacade httpApiFacade;

        public CommandFactory(IHttpApiFacade httpApiFacade)
        {
            this.httpApiFacade = httpApiFacade;
        }

        public ICommand GetCommand(AddParams parameters)
        {
            var command = new AddCommand(parameters, httpApiFacade);
            return Wrap(command);
        }

        public ICommand GetCommand(DeleteParams parameters)
        {
            var command = new DeleteCommand(parameters, httpApiFacade);
            return Wrap(command);
        }

        public ICommand GetCommand(QueryParams queryParams)
        {
            var query = new GetAllReservationsQuery(queryParams, httpApiFacade, new MarshalFacade());
            return Wrap(query);
        }

        private HttpCommandWrapper Wrap(ICommand command)
        {
            return new HttpCommandWrapper(command, httpApiFacade);
        }
    }
}