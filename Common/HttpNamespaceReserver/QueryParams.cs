using System;
using System.Collections.Generic;
using PackNet.Common.HttpNamespaceReserver.Structs;


namespace PackNet.Common.HttpNamespaceReserver
{
    public class QueryParams
    {
        private readonly IntPtr serviceHandle;
        private readonly int configId;
        private readonly IntPtr inputConfigInfoPointer;
        private readonly IntPtr overlapped;
        public uint ReturnSize { get; set; }
        public HttpServiceConfigUrlaclQuery Query { get; set; }
        public Dictionary<string, string> Result { get; set; }

        public QueryParams(IntPtr serviceHandle, int configId, IntPtr inputConfigInfoPointer, IntPtr outputConfigInfoPointer, uint outputConfigInfoLength, IntPtr overlapped, uint returnSize)
        {
            this.serviceHandle = serviceHandle;
            this.configId = configId;
            this.inputConfigInfoPointer = inputConfigInfoPointer;
            OutputConfigInfoPointer = outputConfigInfoPointer;
            OutputConfigInfoLength = outputConfigInfoLength;
            this.overlapped = overlapped;
            ReturnSize = returnSize;
            Query = new HttpServiceConfigUrlaclQuery{QueryDesc = HttpServiceConfigQueryType.HttpServiceConfigQueryNext};
            Result = new Dictionary<string, string>();

        }

        public IntPtr ServiceHandle
        {
            get { return serviceHandle; }
        }

        public int ConfigId
        {
            get { return configId; }
        }

        public IntPtr InputConfigInfoPointer
        {
            get { return inputConfigInfoPointer; }
        }

        public IntPtr OutputConfigInfoPointer { get; set; }

        public uint OutputConfigInfoLength { get; set; }

        public IntPtr Overlapped
        {
            get { return overlapped; }
        }

        public IntPtr QueryPointer { get; set; }
    }
}