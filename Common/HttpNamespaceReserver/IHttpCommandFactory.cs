

namespace PackNet.Common.HttpNamespaceReserver
{
    public interface IHttpCommandFactory
    {
        ICommand GetCommand(QueryParams queryParams);
        ICommand GetCommand(AddParams parameters);
        ICommand GetCommand(DeleteParams parameters);
    }
}