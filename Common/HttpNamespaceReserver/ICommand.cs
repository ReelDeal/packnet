namespace PackNet.Common.HttpNamespaceReserver
{
    public interface ICommand
    {
        void Execute();
    }
}