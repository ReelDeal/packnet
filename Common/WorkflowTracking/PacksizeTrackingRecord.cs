using System;
using System.Activities;
using System.Activities.Tracking;
using System.Diagnostics;

namespace PackNet.Common.WorkflowTracking
{
    public class PacksizeTrackingRecord : CustomTrackingRecord
    {
        public PacksizeTrackingRecord(ActivityContext context, string name, string details)
            : base(context.WorkflowInstanceId, name, TraceLevel.Info)
        {
            Details = details;
        }
        public string Details { get; set; }

        public override string ToString()
        {
            return string.Format("PacksizeTrackingRecord {0}, Detials:{1}", base.ToString(),Details);
        }
    }
}