﻿using System.Activities.Tracking;

namespace PackNet.Common.WorkflowTracking
{
    public class JsonTrackingRecord : TrackingRecord
    {
        public JsonTrackingRecord(TrackingRecord record, string json)
            : base(record)
        {
            OriginalTrackingRecord = json;
        }

        public string OriginalTrackingRecord { get; set; }

        protected override TrackingRecord Clone()
        {
            return this;
        }
    }
}