﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowTracking
{

    public class PacksizeActivityTrackingRecordWrapper : CustomTrackingRecord
    {
        private readonly Dictionary<string, object> arguments = new Dictionary<string, object>();
        private Dictionary<string, object> variables = new Dictionary<string, object>();

        public PacksizeActivityTrackingRecordWrapper(ActivityStateRecord asr, bool trackArgumentsAndVariables)
            : base(asr.InstanceId, "PacksizeActivityTrackingRecordWrapper", asr.Level)
        {
            try
            {
                ActivityOrig = asr.Activity;
                StateOrig = asr.State;
                EventTimeOrig = asr.EventTime;
                RecordNumberOrig = asr.RecordNumber;
                if (trackArgumentsAndVariables)
                {
                    asr.Arguments.ForEach(kv =>
                    {
                        if (!(kv.Value is IService) && !(kv.Value is IServiceLocator))
                            try
                            {
                                IEnumerable<object> copy = null;
                                if (kv.Value is IEnumerable<object>)
                                    // having issues here with collection modified because the objects we get here are ref.
                                    copy = ((IEnumerable<object>)kv.Value).ToList();

                                arguments.Add(kv.Key, copy ?? kv.Value);
                                Arguments += "," + JsonConvert.SerializeObject(
                                    new
                                    {
                                        kv.Key,
                                        Value = copy ?? kv.Value
                                    });
                            }
                            catch
                            {
                                //invalidOperationException 
                                arguments.Add(kv.Key, "Failed to serialize");
                                Arguments += "," + JsonConvert.SerializeObject(
                                    new
                                    {
                                        kv.Key,
                                        Value = "Failed to serialize"
                                    });
                            }
                    });
                    asr.Variables.ForEach(kv =>
                    {
                        if (!(kv.Value is IService) && !(kv.Value is IServiceLocator))
                            try
                            {
                                IEnumerable<object> copy = null;
                                if (kv.Value is IEnumerable<object>)
                                    copy = ((IEnumerable<object>)kv.Value).ToList();

                                variables.Add(kv.Key, copy ?? kv.Value);
                                Variables += "," + JsonConvert.SerializeObject(
                                    new
                                    {
                                        kv.Key,
                                        Value = copy ?? kv.Value
                                    });
                            }
                            catch
                            {
                                variables.Add(kv.Key, "Failed to serialize");
                                Variables += "," + JsonConvert.SerializeObject(
                                    new
                                    {
                                        kv.Key,
                                        Value = "Failed to serialize"
                                    });
                            }
                    });
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("Failed to send message to server " + e);
            }
        }

        public long RecordNumberOrig { get; set; }
        public DateTime EventTimeOrig { get; set; }
        public ActivityInfo ActivityOrig { get; set; }
        public string StateOrig { get; set; }
        [JsonIgnore]
        public string Arguments { get; set; }
        [JsonIgnore]
        public string Variables { get; set; }
        [BsonIgnore]
        public Dictionary<string, object> ArgumentsOrig { get { return arguments; } }
        [BsonIgnore]
        public Dictionary<string, object> VariablesOrig { get { return variables; } }
    }
}
