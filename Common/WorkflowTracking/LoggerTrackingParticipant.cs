﻿using System;
using System.Activities.Tracking;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;

namespace PackNet.Common.WorkflowTracking
{
    public class LoggerTrackingParticipant : TrackingParticipant
    {
        private ILogger logger;
        public LoggerTrackingParticipant()
        {
            logger = LogManager.GetLogFor("WorklfowTracking");
        }
        protected override void Track(TrackingRecord record, TimeSpan timeout)
        {
            if (record != null)
            {
                logger.Log(LogLevel.Info, record.ToString());
            }
        }
    }
}