﻿using System.Reactive.Concurrency;

namespace PackNet.Common.Eventing
{
    using System;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;

    using Interfaces.Eventing;

    public class EventAggregator : IEventAggregatorPublisher, IEventAggregatorSubscriber
    {
        private IScheduler scheduler;
        private readonly ISubject<object> subject = new Subject<object>();

        public IObservable<T> GetEvent<T>()
        {
            return subject.AsObservable().OfType<T>();
        }

        public IScheduler Scheduler
        {
            get { return scheduler ?? System.Reactive.Concurrency.Scheduler.Default; }

            set { scheduler = value; }
        }

        public void Publish<TEvent>(TEvent publishedEvent)
        {
            subject.OnNext(publishedEvent);
        }
        
        public void Dispose()
        {
        }

        public string Name { get { return "EventAggregator"; } }
    }
}
