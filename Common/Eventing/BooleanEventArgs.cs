﻿using System;

namespace PackNet.Common.Eventing
{
    public class BooleanEventArgs : EventArgs
    {
        public BooleanEventArgs(bool value)
        {
            Value = value;
        }

        public bool Value { get; private set; }
    }
}
