﻿namespace PackNet.Common.Eventing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Interfaces.Eventing;

    [Obsolete("Use IEventAggregator")]
    public abstract class EventPublisher
    {
        protected IEnumerable<IPublisher> publishers;

        public void Publish(object data, DateTime eventTime, IEnumerable<IPublisher> publishers)
        {
            foreach (var publisher in publishers)
            {
                publisher.Publish(data, eventTime);
            }
        }

        public virtual void JobDone(object data, DateTime eventTime)
        {
            Publish(
                data,
                eventTime,
                publishers.Where(publisher => publisher.Type == PublisherType.JobDone));
        }

        public virtual void PackagingDone(object data, DateTime eventTime)
        {
            Publish(
                data,
                eventTime,
                publishers.Where(publisher => publisher.Type == PublisherType.PackagingDone));
        }
    }
}
