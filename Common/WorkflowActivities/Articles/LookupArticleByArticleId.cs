﻿using System.Activities;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities.Articles
{

    public sealed class LookupArticleByArticleId//<T> 
        : CodeActivity 
        //where T : IProducible, IPersistable
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<string> ArticleId { get; set; }
        //TODO: use typed article
        //public OutArgument<Article<T>> FoundArticle { get; set; }
        public OutArgument<Article> FoundArticle { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);
            var articleService = serviceLocator.Locate<IArticleService>();
            var articleId = context.GetValue(ArticleId);
            var foundArticle = articleService.Find(articleId);
            FoundArticle.Set(context, foundArticle);
        }
    }
}
