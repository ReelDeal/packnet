﻿using System;
using System.Activities;
using System.Linq;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;

namespace PackNet.Common.WorkflowActivities
{
    public sealed class ProducibleRestrictionsRemoveShouldWaitForBoxRelease
       : CodeActivity
    {
        public InArgument<ICarton> Producible { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var producible = Producible.Get(context); 
            
            producible.Restrictions.RemoveAll(r =>
                    r is BasicRestriction<string> &&
                    ((BasicRestriction<string>)r).Value == CartonRestrictions.ShouldWaitForBoxRelease);
        }
    }
}