﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLog;
using NLog.Fluent;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

using LogLevel = PackNet.Common.Interfaces.Logging.LogLevel;

namespace PackNet.Common.WorkflowActivities
{
    public class OrderProduciblesByExclusiveness : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }
        

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var sw = Stopwatch.StartNew();
            var serviceLocator = ServiceLocator.Get(context);
            var machineGroupId = MachineGroupId.Get(context);
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>();

            var pgService = serviceLocator.Locate<IProductionGroupService>();
            var mgService = serviceLocator.Locate<IMachineGroupService>();
            var machineGroup = mgService.FindByMachineGroupId(machineGroupId);
            var machineService = serviceLocator.Locate<IAggregateMachineService>();
            var logger = serviceLocator.Locate<ILogger>();

            var currentPg = pgService.ProductionGroups.FirstOrDefault(pg => pg.ConfiguredMachineGroups.Contains(machineGroupId));

            if (currentPg == null)
                throw new Exception("Machine Group does not belong to a Production Group");
            
            var mgsInCurrentPg = mgService.MachineGroups.Where(mg => currentPg.ConfiguredMachineGroups.Contains(mg.Id)).ToList();
            var machines = machineService.Machines.Where(m => m is IPacksizeCutCreaseMachine).ToList();
            var machinesInMachineGroups = mgsInCurrentPg.SelectMany(mg => machines.Where(m => mg.ConfiguredMachines.Contains(m.Id))).Cast<IPacksizeCutCreaseMachine>().ToList();
            var comparer = new CorrugateExclusivenessComparer(machinesInMachineGroups);

            var corrugatesInProductionGroup = machinesInMachineGroups.SelectMany(mg => mg.Tracks).Where(t => t.LoadedCorrugate != null).Select(t => t.LoadedCorrugate);
            var orderedCorrugates = corrugatesInProductionGroup.OrderBy(c => c, comparer).Distinct().ToList();
            var machineIdsInMachineGroup = mgsInCurrentPg.First(m => m.Id == machineGroupId).ConfiguredMachines;

            try
            {
            var orderedProducibles = producibles.OrderBy(p => GetMostExclusiveCorrugateThatCanBeProducedOnMachineGroup(orderedCorrugates, p, machineIdsInMachineGroup), comparer).ToList();
            logger.Log(LogLevel.Debug, "Ordered by exclusiveness took " + sw.ElapsedMilliseconds + " ms");
            return orderedProducibles;
        }
            catch (ApplicationException)
            {
                logger.Log(LogLevel.Info, "{0} cannot produce any of the items in staging. Producible Count ={1} ", machineGroup, producibles.Count());
                return new List<IProducible>();
            }
        }

        private Corrugate GetMostExclusiveCorrugateThatCanBeProducedOnMachineGroup(IEnumerable<Corrugate> orderedCorrugates, BoxFirstProducible producible, IEnumerable<Guid> machinesInMachineGroup)
        {
            foreach (var corrugate in orderedCorrugates)
            {
                if (producible.CartonOnCorrugates.Any(c => c.Corrugate.Id == corrugate.Id && MachineGroupCanProduceCartonOnCorrugate(c, machinesInMachineGroup)))
                    return corrugate;
            }
            throw new ApplicationException("No producibles can be produced by machine group");
        }

        private bool MachineGroupCanProduceCartonOnCorrugate(CartonOnCorrugate c, IEnumerable<Guid> machinesInMachineGroup)
        {
            return machinesInMachineGroup.Any(m => c.ProducibleMachines.ContainsKey(m));
        }
    }
}
