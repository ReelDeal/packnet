﻿using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using System.Activities;
using System.Collections.Generic;

namespace PackNet.Common.WorkflowActivities
{
    public class GetAllProducibles : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IBoxFirstSelectionAlgorithmService> selectionService { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var service = selectionService.Get(context);
            var statuses = new List<ProducibleStatuses>()
            {
                NotInProductionProducibleStatuses.ProducibleStaged, 
                NotInProductionProducibleStatuses.ProducibleImported, 
                NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction, 
                ErrorProducibleStatuses.NotProducible
            };
            var producibles = service.GetAllProduciblesWithStatuses(statuses);
            return producibles;
        }
    }
}
