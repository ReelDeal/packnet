﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Producible;

using System.Activities;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// This will add a dummy template only populating the name.  Later 
    /// when the label is send to the printer we lookup the real template and merge
    /// </summary>
    public class AddZebraTemplateRestriction : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IPrintable> Label { get; set; }

        [RequiredArgument]
        public InArgument<string> TemplateName { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var label = Label.Get(context);
            //todo: consider loading the real template using the service locator
            var template = new Template() { Name = TemplateName.Get(context)};
            label.Restrictions.Add(new BasicRestriction<Template>(template));

        }
    }
}
