﻿using System;
using System.Activities;
using System.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.ScanToCreate;
using PackNet.Common.Interfaces.Enums.ProducibleStates;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Validates the new Scan to Create producible and sets the status accordingly.
    /// A Carton without a Carton on Corrugate will be set to Not producible
    /// 
    /// If everything goes as expected the status of the producible will be set to staged
    /// </summary>
    /// <remarks>The AllRequiredRestriction means that the machine group must satisfy all of the restrictions to be qualified to work the producible.</remarks>
    public sealed class ValidateScanToCreateProducibleAndSetStatusAccordingly : CodeActivity
    {
        [RequiredArgument]
        public InArgument<ScanToCreateProducible> Producible { get; set; }
        
        protected override void Execute(CodeActivityContext context)
        {
            var scanToCreateProducible = Producible.Get(context);
            
            var producible = scanToCreateProducible.Producible;

            if (producible == null)
                return;

            if (producible is ICarton)
            {
                ValidateCarton(producible as ICarton);
            }
            else if (producible is Label)
            {
                ValidateLabel(producible as Label);
            }
            else if(producible is Kit)
            {
                ValidateKit(producible as Kit);
            }
            else
            {
                throw new ArgumentException("ValidateScanToCreateProducibleAndSetStatusAccordingly called with unknown producible type");
            }

            scanToCreateProducible.ProducibleStatus = producible.ProducibleStatus;
        }

        private static void ValidateKit(Kit kit)
        {
            if (kit == null)
                return;

            if (kit.ItemsToProduce == null)
                return;

            var cartons = kit.ItemsToProduce.OfType<ICarton>();
            var labels = kit.ItemsToProduce.OfType<Label>();
            var kits = kit.ItemsToProduce.OfType<Kit>();
            var orders = kit.ItemsToProduce.OfType<Order>();

            cartons.ForEach(ValidateCarton);
            labels.ForEach(ValidateLabel);
            kits.ForEach(ValidateKit);
            orders.ForEach(ValidateOrder);
        }

        private static void ValidateCarton(ICarton carton)
        {
            if (carton.CartonOnCorrugate == null)
                carton.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
        }

        private static void ValidateLabel(Label label)
        {
            //Currently no validation for labels
        }

        private static void ValidateOrder(Order order)
        {
            if (order.Producible.ProducibleStatus is ErrorProducibleStatuses)
                order.ProducibleStatus = order.Producible.ProducibleStatus;
        }
    }
}