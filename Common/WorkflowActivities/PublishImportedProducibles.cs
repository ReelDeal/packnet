﻿using System;
using System.Activities;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Publishes a imported message on the event bus
    /// </summary>
    public sealed class PublishImportedProducibles : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorPublisher> Publisher { get; set; }
        
        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> ObjectToPublish { get; set; }
        
        public InArgument<SelectionAlgorithmTypes> SelectionAlgorithmType { get; set; }

        public InArgument<Guid> ProductionGroupId { get; set; }

        public InArgument<Guid> MachineGroupId { get; set; }
        
        [RequiredArgument]
        public InArgument<ImportTypes> ImportType { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var sat = SelectionAlgorithmType.Get(context);
            var pgId = ProductionGroupId.Get(context);
            var mgId = MachineGroupId.Get(context);

            var message = new ImportMessage<IEnumerable<IProducible>>
            {
                Data = ObjectToPublish.Get(context),
                ImportType = ImportType.Get(context),
                SelectionAlgorithmType = sat,
                ProductionGroupId = pgId,
                MachineGroupId = mgId
            };
            var publisher = Publisher.Get(context);
            publisher.Publish(message);
        }
    }
}