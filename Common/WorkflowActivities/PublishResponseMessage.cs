﻿using System;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using System.Activities;

namespace PackNet.Common.WorkflowActivities
{
    using PackNet.Common.Interfaces.Services;

    /// <summary>
    /// Publishes a message on the event bus
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PublishResponseMessage<T> : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorPublisher> Publisher { get; set; }

        [RequiredArgument]
        public InArgument<T> ObjectToPublish { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        [RequiredArgument]
        public InArgument<string> ReplyTo { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new ResponseMessage<T>
            {
                Data = ObjectToPublish.Get(context),
                MessageType = MessageType.Get(context),
                ReplyTo = ReplyTo.Get(context)
            };
            var publisher = Publisher.Get(context);
            publisher.Publish(message);
        }
    }

    /// <summary>
    /// Publishes a message on the event bus
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PublishResponseMessageToUI<T> : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<T> ObjectToPublish { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        [RequiredArgument]
        public InArgument<string> ReplyTo { get; set; }

        [RequiredArgument]
        public InArgument<ResultTypes> ResultType { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new ResponseMessage<T>
            {
                Data = ObjectToPublish.Get(context),
                MessageType = MessageType.Get(context),
                ReplyTo = ReplyTo.Get(context),
                Result = ResultType.Get(context)
            };
            var serviceLocator = ServiceLocator.Get(context);
            var uiComsService = serviceLocator.Locate<IUICommunicationService>();
            uiComsService.SendMessageToUI(message);
        }
    }

    /// <summary></summary>
    public sealed class PublishResponseMessageToUI : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        
        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        [RequiredArgument]
        public InArgument<string> ReplyTo { get; set; }

        [RequiredArgument]
        public InArgument<ResultTypes> ResultType { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new ResponseMessage
            {
                MessageType = MessageType.Get(context),
                ReplyTo = ReplyTo.Get(context),
                Result = ResultType.Get(context)
            };
            var serviceLocator = ServiceLocator.Get(context);
            var uiComsService = serviceLocator.Locate<IUICommunicationService>();
            uiComsService.SendMessageToUI(message);
        }
    }

    /// <summary>
    /// Publishes a message on the event bus
    /// </summary>
    public sealed class PublishResponseMessage : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorPublisher> Publisher { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        [RequiredArgument]
        public InArgument<string> ReplyTo { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new ResponseMessage
            {
                MessageType = MessageType.Get(context),
                ReplyTo = ReplyTo.Get(context)
            };
            var publisher = Publisher.Get(context);
            publisher.Publish(message);
        }
    }
}