﻿using System;
using System.Activities;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Use this activity to get the job that a machine group is working on that is first in it's list of jobs
    /// that it could be working on.
    /// </summary>
    public sealed class MachineGroupServiceGetFirstJobInQueue : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<MachineGroup> MachineGroup { get; set; }
        [RequiredArgument]
        public OutArgument<IProducible> Producible { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var mgs = serviceLocator.Locate<IMachineGroupService>();
            var item = mgs.GetProducibleFirstInQueue(MachineGroup.Get(context));
            Producible.Set(context, item);
        }
    }
}