﻿using System.Activities;

namespace PackNet.Common.WorkflowActivities.Parsing
{
    public class ParseInt : CodeActivity<int?>
    {
        public InArgument<string> StringToParse { get; set; }

        protected override int? Execute(CodeActivityContext context)
        {
            int outInt;
            var stringToParse = StringToParse.Get(context);

            if (int.TryParse(stringToParse, out outInt))
            {
                return outInt;
            }

            return null;
        }
    }
}