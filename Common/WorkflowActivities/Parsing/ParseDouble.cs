﻿using System.Activities;

namespace PackNet.Common.WorkflowActivities.Parsing
{
    public class ParseDouble : CodeActivity<double?>
    {
        public InArgument<string> StringToParse { get; set; }

        protected override double? Execute(CodeActivityContext context)
        {
            double val;
            var stringToParse = StringToParse.Get(context);
            if (double.TryParse(stringToParse, out val))
            {
                return val;
            }

            return null;
        }
    }
}