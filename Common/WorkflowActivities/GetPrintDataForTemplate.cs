﻿using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public class GetPrintDataForTemplate : CodeActivity<Dictionary<string, string>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<string> TemplateName { get; set; }

        [RequiredArgument]
        public InArgument<Dictionary<string, string>> ImportedPrintData { get; set; }

        protected override Dictionary<string, string> Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var templateName = TemplateName.Get(context);
            var importedPrintData = ImportedPrintData.Get(context);


            if (importedPrintData == null || importedPrintData.Any() == false)
                return importedPrintData;

            var printDataForTemplate = new Dictionary<string, string>();
            var templateService = serviceLocator.Locate<ITemplateService>();

            var template = templateService.FindByName(templateName);
            if (template == null)
                return printDataForTemplate;

            foreach (var printDataField in importedPrintData.Keys.Where(printDataField => template.Content.Contains("{" + printDataField + "}")))
            {
                printDataForTemplate.Add(printDataField, importedPrintData[printDataField]);
            }
           
            return printDataForTemplate;
        }
    }
}

