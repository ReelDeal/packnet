using System;
using System.Activities;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public class DropFileValueLookup<T> : DropFileValueLookup
    {
        [RequiredArgument]
        public OutArgument<T> ParsedOutputValue { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            base.Execute(context);
            var foundValue = OutputValue.Get(context);
            if (!String.IsNullOrWhiteSpace(foundValue))
            {
                try
                {
                    var t = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
                    ParsedOutputValue.Set(context, (T)Convert.ChangeType(OutputValue.Get(context), t));
                }
                catch (Exception)
                {
                    var errorText = String.Format("Error in file importer: Value for column '{0}' was found (value '{1}') in drop file but could not be converted to '{2}'", stringToLookup, foundValue, typeof(T).Name);
                    OutputValue.Set(context, DefaultValue.Get(context));

                    Log(context, logger, Interfaces.Logging.LogLevel.Info, errorText);
                    NotifyUser(context, notificationService, Interfaces.Enums.NotificationSeverity.Error, errorText);
                }
            }
        }

    }

    public class DropFileValueOrderLookup<T> : DropFileValueOrderLookup
    {
        [RequiredArgument]
        public OutArgument<T> ParsedOutputValue { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            base.Execute(context);
            var foundValue = OutputValue.Get(context);
            if (!String.IsNullOrWhiteSpace(foundValue))
            {
                try
                {
                    var t = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
                    ParsedOutputValue.Set(context, (T)Convert.ChangeType(OutputValue.Get(context), t));
                }
                catch (Exception)
                {
                    var errorText = String.Format("Error in file importer: Value for column '{0}' was found (value '{1}') in drop file but could not be converted to '{2}'", stringToLookup, foundValue, typeof(T).Name);
                    OutputValue.Set(context, DefaultValue.Get(context));

                    Log(context, logger, Interfaces.Logging.LogLevel.Info, errorText);
                    NotifyUser(context, notificationService, Interfaces.Enums.NotificationSeverity.Error, errorText);
                }
            }
        }
                
    }
    
    public class DropFileValueLookup : CodeActivity
    {
        protected IUserNotificationService notificationService;
        protected ILogger logger;
        protected IServiceLocator serviceLocator;
        protected string stringToLookup;

        [RequiredArgument]
        public OutArgument<string> OutputValue { get; set; }

        [RequiredArgument]
        public InArgument<string> PropertyToRetrieve { get; set; }

        [RequiredArgument]
        public InArgument<object> Dynamic { get; set; }

        [RequiredArgument]
        public InArgument<string> DefaultValue { get; set; }

        [RequiredArgument]
        public InArgument<bool> LogMissing { get; set; }

        [RequiredArgument]
        public InArgument<bool> SendNotification { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        public InArgument<LogLevel?> LogLevel { get; set; }
        public InArgument<NotificationSeverity> NotificationSeverity { get; set; }

        public InArgument<string> ErrorText { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            serviceLocator = ServiceLocator.Get(context);
            logger = serviceLocator.Locate<ILogger>();
            notificationService = serviceLocator.Locate<IUserNotificationService>();

            stringToLookup = PropertyToRetrieve.Get(context);
            var obj = Dynamic.Get<object>(context);

            var dynamicItem = obj as ExpandoObject;
            if (obj == null)
                throw new ArgumentException("'Dynamic' must be a ExpandoObject");

            var dictionary = dynamicItem as IDictionary<string, Object>;

            //Handle if the header is missing //ignore casing
            var key = dictionary.Keys.FirstOrDefault(c => c.ToLowerInvariant() == stringToLookup.ToLowerInvariant());
            if (key == null)
            {
                OutputValue.Set(context, DefaultValue.Get(context));
                var errorText = String.Format("Error in file importer: Header '{0}' was not found, default value '{1}' used", stringToLookup, DefaultValue.Get(context) ?? "null");
                Log(context, logger, Interfaces.Logging.LogLevel.Info, errorText);
                NotifyUser(context, notificationService, Interfaces.Enums.NotificationSeverity.Error, errorText);
                OutputValue.Set(context, DefaultValue.Get(context));
                return;
            }

            object value;
            if (!dictionary.TryGetValue(key, out value))
            {
                var errorText = String.Format("Error in file importer: Value for header '{0}' was not found, default value '{1}' used", stringToLookup, DefaultValue.Get(context) ?? "null");
                OutputValue.Set(context, DefaultValue.Get(context));

                Log(context, logger, Interfaces.Logging.LogLevel.Info, errorText);
                NotifyUser(context, notificationService, Interfaces.Enums.NotificationSeverity.Error, errorText);
                return;
            }

            OutputValue.Set(context, value as string);
        }

        protected void NotifyUser(CodeActivityContext context, IUserNotificationService notificationService, Interfaces.Enums.NotificationSeverity notificationSeverity, string errorText)
        {
            if (SendNotification.Get(context))
            {
                var userDefinedNotificationSeverity = NotificationSeverity.Get(context);
                if (userDefinedNotificationSeverity == null)
                {
                    userDefinedNotificationSeverity = notificationSeverity;
                }
                notificationService.SendNotificationToSystem(userDefinedNotificationSeverity, errorText, "");
            }
        }

        protected void Log(CodeActivityContext context, ILogger logger, Interfaces.Logging.LogLevel logLevel, string errorText)
        {
            if (LogMissing.Get(context))
            {
                var userDefinedLogLevel = LogLevel.Get(context);
                if (userDefinedLogLevel == null)
                {
                    userDefinedLogLevel = logLevel;
                }
                logger.Log(userDefinedLogLevel.Value, errorText);
            }
        }
    }
    
    public class DropFileValueOrderLookup : CodeActivity
    {
        protected IUserNotificationService notificationService;
        protected ILogger logger;
        protected IServiceLocator serviceLocator;
        protected string stringToLookup;

        [RequiredArgument]
        public OutArgument<string> OutputValue { get; set; }

        [RequiredArgument]
        public OutArgument<bool> OutputError { get; set; }

        [RequiredArgument]
        public InArgument<string> PropertyToRetrieve { get; set; }

        [RequiredArgument]
        public InArgument<object> Dynamic { get; set; }

        [RequiredArgument]
        public InArgument<string> DefaultValue { get; set; }

        [RequiredArgument]
        public InArgument<bool> LogMissing { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        public InArgument<LogLevel?> LogLevel { get; set; }
        public InArgument<NotificationSeverity> NotificationSeverity { get; set; }

        public InArgument<string> ErrorText { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            serviceLocator = ServiceLocator.Get(context);
            logger = serviceLocator.Locate<ILogger>();
            notificationService = serviceLocator.Locate<IUserNotificationService>();

            stringToLookup = PropertyToRetrieve.Get(context);
            var obj = Dynamic.Get<object>(context);

            var dynamicItem = obj as ExpandoObject;
            if (obj == null)
                throw new ArgumentException("'Dynamic' must be a ExpandoObject");

            var dictionary = dynamicItem as IDictionary<string, Object>;

            //Handle if the header is missing //ignore casing
            var key = dictionary.Keys.FirstOrDefault(c => c.ToLowerInvariant() == stringToLookup.ToLowerInvariant());
            if (key == null)
            {
                OutputValue.Set(context, DefaultValue.Get(context));
                var errorText = String.Format("Error in file importer new: Header '{0}' was not found, default value '{1}' used", stringToLookup, DefaultValue.Get(context) ?? "null");
                Log(context, logger, Interfaces.Logging.LogLevel.Info, errorText);
                OutputValue.Set(context, DefaultValue.Get(context));
                OutputError.Set(context, true);
                return;
            }

            object value;
            if (!dictionary.TryGetValue(key, out value))
            {
                var errorText = String.Format("Error in file importer: Value for header '{0}' was not found, default value '{1}' used", stringToLookup, DefaultValue.Get(context) ?? "null");
                OutputValue.Set(context, DefaultValue.Get(context));
                OutputError.Set(context, true);
                Log(context, logger, Interfaces.Logging.LogLevel.Info, errorText);
                return;
            }

            OutputError.Set(context, false);
            OutputValue.Set(context, value as string);
        }

        protected void NotifyUser(CodeActivityContext context, IUserNotificationService notificationService, Interfaces.Enums.NotificationSeverity notificationSeverity, string errorText)
        {
            var userDefinedNotificationSeverity = NotificationSeverity.Get(context);
            if (userDefinedNotificationSeverity == null)
            {
                userDefinedNotificationSeverity = notificationSeverity;
            }
            notificationService.SendNotificationToSystem(userDefinedNotificationSeverity, errorText, "");
        }

        protected void Log(CodeActivityContext context, ILogger logger, Interfaces.Logging.LogLevel logLevel, string errorText)
        {
            if (LogMissing.Get(context))
            {
                var userDefinedLogLevel = LogLevel.Get(context);
                if (userDefinedLogLevel == null)
                {
                    userDefinedLogLevel = logLevel;
                }
                logger.Log(userDefinedLogLevel.Value, errorText);
            }
        }
    }
}