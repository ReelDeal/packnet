﻿using System.Activities;

using PackNet.Common.Interfaces.DTO.ScanToCreate;

namespace PackNet.Common.WorkflowActivities
{
    public class SetCustomerUniqueIdOfScanToCreateProducible : CodeActivity
    {
        public InArgument<ScanToCreateProducible> Producible { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var scanToCreateProducible = Producible.Get(context);

            if (scanToCreateProducible == null)
                return;

            if (string.IsNullOrEmpty(scanToCreateProducible.CustomerUniqueId))
                scanToCreateProducible.CustomerUniqueId = scanToCreateProducible.Id.ToString();

            var producible = scanToCreateProducible.Producible;

            if (producible == null)
                return;

            if (string.IsNullOrEmpty(producible.CustomerUniqueId))
                producible.CustomerUniqueId = scanToCreateProducible.Id.ToString();
        }
    }
}