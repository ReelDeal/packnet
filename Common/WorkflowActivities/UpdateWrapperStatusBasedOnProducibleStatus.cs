﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{

    public sealed class UpdateWrapperStatusBasedOnProducibleStatus : CodeActivity
    {
        public InArgument<IProducibleWrapper> Wrapper { get; set; }

        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            var w = context.GetValue(Wrapper);
            var locator = context.GetValue(ServiceLocator);
            var logger = locator.Locate<ILogger>();

            logger.Log(LogLevel.Debug, "UpdateWrapperStatus for wrapper with id {0} with status {1}. ProducibleStatus is {2}", w.CustomerUniqueId, w.ProducibleStatus, w.Producible.ProducibleStatus);
            if (w.Producible.ProducibleStatus != w.ProducibleStatus)
                w.ProducibleStatus = w.Producible.ProducibleStatus;
        }
    }
}
