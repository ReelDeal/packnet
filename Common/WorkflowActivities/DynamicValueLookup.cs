﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Try and find the value you specified in the dynamic object and return it as OutputValue or
    /// Throws ArgumentException if value is not found.
    /// </summary>
    public sealed class DynamicValueLookup : CodeActivity
    {
        [RequiredArgument]
        public OutArgument<string> OutputValue { get; set; }
        [RequiredArgument]
        public InArgument<string> PropertyToRetrieve { get; set; }
        [RequiredArgument]
        public InArgument<Object> Dynamic { get; set; }

        protected override void Execute(CodeActivityContext context)
        {

            var stringToLookup = PropertyToRetrieve.Get(context);
            var obj = Dynamic.Get<object>(context);

            var dynamicItem = obj as ExpandoObject;
            if (obj == null)
                throw new ArgumentException("'Dynamic' must be a ExpandoObject");

            var dictionary = dynamicItem as IDictionary<string, Object>;
            //ignore casing
            var key = dictionary.Keys.FirstOrDefault(c => c.ToLowerInvariant() == stringToLookup.ToLowerInvariant());
            if (key == null)
                throw new ArgumentException("dynamic object didn't contain a property named '" + stringToLookup + "'");

            object value;
            if (!dictionary.TryGetValue(key, out value))
                throw new ArgumentException("dynamic object didn't contain a property named '" + stringToLookup + "'");

            OutputValue.Set(context, value as string);
        }
    }
}
