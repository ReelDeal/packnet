﻿using System.Collections.Generic;
using System.Activities;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Add the AllRequiredRestriction with the passed RestrictionsToAdd add to the producible.
    /// </summary>
    /// <remarks>The AllRequiredRestriction means that the machine group must satisfy all of the restrictions to be qualified to work the producible.</remarks>
    public sealed class AddAllRestriction : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }
        [RequiredArgument]
        public InArgument<List<IRestriction>> RestrictionsToAdd{ get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new AllRequiredRestriction();
            message.Restrictions.AddRange(RestrictionsToAdd.Get(context));
            Producible.Get(context).Restrictions.Add(message);
        }
    }
}