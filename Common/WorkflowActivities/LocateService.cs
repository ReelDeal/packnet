﻿using System.Activities;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public sealed class LocateService<T> : CodeActivity where T: IService
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public OutArgument<T> Service { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var service = serviceLocator.Locate<T>();
            Service.Set(context, service);
        }
    }
}