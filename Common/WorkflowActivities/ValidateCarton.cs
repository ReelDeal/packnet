﻿using System.Activities;
using System.Collections.Generic;
using System.Reflection;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public class ValidateCarton : CodeActivity
    {
        private InArgument<string> _validateCartonPath = new InArgument<string>("ImportPlugins\\ValidateCarton.xaml");
        public InArgument<string> ValidateCartonPath
        {
            get { return _validateCartonPath; }
            set { _validateCartonPath = value; }
        }

        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        public InArgument<object> ItemToValidate { get; set; }
        private InArgument<string> _validationResultDictionaryKey = new InArgument<string>("cartonIsValid");
        public InArgument<string> ValidationResultDictionaryKey
        {
            get { return _validationResultDictionaryKey; }
            set
            {
                _validationResultDictionaryKey = value;
            }
        }
        public OutArgument<bool> IsValidResult { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var path = ValidateCartonPath.Get(context);
            var serviceLocator = ServiceLocator.Get(context);
            var itemToValidate = ItemToValidate.Get(context);
            var userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            var logger = serviceLocator.Locate<ILogger>();
            var validationKey = ValidationResultDictionaryKey.Get(context);
            var arguments = new Dictionary<string, object>()
            {
                {"serviceLocator",serviceLocator},
                {"dynamicObject", itemToValidate}
            };

            //todo:This should be moved into ExecuteXamlWorkflow nested workflow.
            var result = WorkflowHelper.InvokeWorkFlowAndWaitForResult(path, Assembly.GetExecutingAssembly(), arguments, userNotificationService, logger);

            if (result.ContainsKey(validationKey))
            {
                var isValid = (bool)result[validationKey];
                IsValidResult.Set(context, isValid);
            }
        }
    }
}