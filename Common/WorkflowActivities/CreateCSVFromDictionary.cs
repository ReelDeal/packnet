﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace PackNet.Common.WorkflowActivities
{
    using Interfaces;

    public sealed class CreateCSVFromDictionary : CodeActivity
    {
        [RequiredArgument]
        public InArgument<ICollection<Dictionary<string, string>>> Dictionaries { get; set; }

        [RequiredArgument]
        public InArgument<bool> UseHeaders { get; set; }

        [RequiredArgument]
        public InArgument<char> Separator { get; set; }

        [RequiredArgument]
        public OutArgument<string> CSVString { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var dictionaries = context.GetValue(this.Dictionaries);
            var useHeaders = context.GetValue(this.UseHeaders);
            var separator = context.GetValue(this.Separator);
            if (dictionaries.Count > 0)
            {
                var headers = dictionaries.SelectMany(d=>d.Keys).Distinct();
                var stringBuilder = new StringBuilder();

                if (useHeaders)
                    stringBuilder.AppendLine(string.Join(separator.ToString(), headers));

                dictionaries.ForEach(d =>
                {
                    foreach (var header in headers)
                    {
                        if (d.ContainsKey(header))
                        {
                            stringBuilder.Append(d[header]);
                        }
                        stringBuilder.Append(separator.ToString());
                    }
                    stringBuilder.Remove(stringBuilder.Length - 1, 1); //Remove the extra seperator
                    stringBuilder.AppendLine();
                });

                CSVString.Set(context, stringBuilder.ToString());
            }
            
        }
    }
}
