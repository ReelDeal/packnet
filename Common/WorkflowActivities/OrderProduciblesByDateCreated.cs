﻿using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.WorkflowActivities
{
    public class OrderProduciblesByDateCreated : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            return Producibles.Get(context).Cast<BoxFirstProducible>().OrderBy(c => c.Created).ToList();
        }
    }
}
