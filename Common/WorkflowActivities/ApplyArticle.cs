﻿using System.Activities;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Common.WorkflowActivities
{
    public class ApplyArticle : CodeActivity<ICarton>
    {
        public InArgument<ICarton> Carton { get; set; }
        public InArgument<Article> Article { get; set; }

        protected override ICarton Execute(CodeActivityContext context)
        {
            var carton = Carton.Get(context);
            var article = Article.Get(context);

            carton.ApplyArticle(article);

            return carton;
        }
    }
}