﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

namespace PackNet.Common.WorkflowActivities
{
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.Services;

    public sealed class CreateOrUpdateArticle : CodeActivity
    {
        public InArgument<Article> Article { get; set; }
        public InArgument<IArticleService> ArticleService { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var article = context.GetValue(this.Article);
            var articleService = context.GetValue(this.ArticleService);

            if (articleService.Find(article.ArticleId) == null)
                articleService.Create(article);
            else
                articleService.Update(article.ArticleId, article, false);
        }
    }
}
