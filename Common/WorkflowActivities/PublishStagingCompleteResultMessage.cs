﻿using System;
using System.Activities;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Publishes a staging complete message on the event bus
    /// </summary>
    public sealed class PublishStagingCompleteResultMessage : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorPublisher> Publisher { get; set; }

        [RequiredArgument]
        public InArgument<SelectionAlgorithmTypes> SelectionAlgorithmType { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new Message<SelectionAlgorithmTypes>
            {
                MessageType = SelectionAlgorithmMessages.StagingComplete,

                Data = SelectionAlgorithmType.Get(context)
            };
            var publisher = Publisher.Get(context);
            publisher.Publish(message);
        }
    }
}