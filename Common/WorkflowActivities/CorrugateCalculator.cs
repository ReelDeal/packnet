﻿using System.Activities;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public class CorrugateCalculator : CodeActivity
    {
        [RequiredArgument]
        public InArgument<ICorrugateService> Calculator { get; set; }
        [RequiredArgument]
        public InArgument<IEnumerable<Corrugate>> Corrugates { get; set; }
        [RequiredArgument]
        public InArgument<IEnumerable<IPacksizeCutCreaseMachine>> Machines { get; set; }
        [RequiredArgument]
        public InArgument<ICarton> Carton { get; set; }
        [RequiredArgument]
        public InArgument<int> TilingCount { get; set; }
        [RequiredArgument]
        public OutArgument<CartonOnCorrugate> SelectedCorrugate { get; set; }


        protected override void Execute(CodeActivityContext context)
        {
            var calcInstance = Calculator.Get(context);
            var corrugates = Corrugates.Get(context);
            var machines = Machines.Get(context);
            var carton = Carton.Get(context);
            var tilingCount = TilingCount.Get(context);

            var optimal = calcInstance.GetOptimalCorrugate(corrugates, machines, carton, tilingCount);
            SelectedCorrugate.Set(context, optimal);
        }
    }
}