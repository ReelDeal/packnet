﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities.Notification
{

    public sealed class SendUiNotificationToProductionGroupAndLog : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<Guid> ProductionGroupId { get; set; }
        [RequiredArgument]
        public InArgument<NotificationSeverity> NotificationSeverity { get; set; }
        [RequiredArgument]
        public InArgument<string> Text { get; set; }
        
        public InArgument<object> TextArgument1 { get; set; }
        public InArgument<object> TextArgument2 { get; set; }
        public InArgument<object> TextArgument3 { get; set; }
        public InArgument<object> TextArgument4 { get; set; }
        public InArgument<object> TextArgument5 { get; set; }
        public InArgument<object> TextArgument6 { get; set; }
        public InArgument<object> TextArgument7 { get; set; }
        public InArgument<object> TextArgument8 { get; set; }
        public InArgument<object> TextArgument9 { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var logger = serviceLocator.Locate<ILogger>();
            var notificationService = serviceLocator.Locate<IUserNotificationService>();
            var publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            var additionalInfo = CheckForTextArgs(context);
            var severity = NotificationSeverity.Get(context);
            var text = Text.Get(context);

            if (text.Contains("{0}"))
                text = string.Format(text, additionalInfo);

            var id = ProductionGroupId.Get(context);
            DoLog(logger, severity, text, id);
            //use the service
            notificationService.SendNotificationToProductionGroup(severity,text,string.Join("(':^)~", additionalInfo),id);
          
        }

        private void DoLog(ILogger logger, NotificationSeverity severity, string text, Guid id)
        {
            LogLevel level;
            if (severity == PackNet.Common.Interfaces.Enums.NotificationSeverity.Error)
                level = LogLevel.Error;
            else if (severity == PackNet.Common.Interfaces.Enums.NotificationSeverity.Warning)
                level = LogLevel.Warning;
            else
                level = LogLevel.Info;

            logger.Log(level, string.Format("Notification sent to ProductionGroupId:{0} with text:{1}", id,text));
        }

        private object[] CheckForTextArgs(CodeActivityContext context)
        {
            var list = new List<object>();
            addIfValid(TextArgument1.Get(context), list);
            addIfValid(TextArgument2.Get(context), list);
            addIfValid(TextArgument3.Get(context), list);
            addIfValid(TextArgument4.Get(context), list);
            addIfValid(TextArgument5.Get(context), list);
            addIfValid(TextArgument6.Get(context), list);
            addIfValid(TextArgument7.Get(context), list);
            addIfValid(TextArgument8.Get(context), list);
            addIfValid(TextArgument9.Get(context), list); 
            return list.ToArray();
        }

        private static void addIfValid(object s, List<object> list)
        {
            if (s != null && !string.IsNullOrWhiteSpace(s.ToString()))
            {
                list.Add(s);
            }
        }
    }
}
