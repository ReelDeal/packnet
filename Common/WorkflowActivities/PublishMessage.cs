﻿using System;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using System.Activities;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Publishes a message on the event bus
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PublishMessage<T> : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorPublisher> Publisher { get; set; }

        [RequiredArgument]
        public InArgument<T> ObjectToPublish { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        public InArgument<Guid?> MachineGroupId { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new Message<T>
            {
                Data = ObjectToPublish.Get(context),
                MessageType = MessageType.Get(context),
                MachineGroupId = MachineGroupId.Get(context)
            };
            var publisher = Publisher.Get(context);
            publisher.Publish(message);
        }
    }

    /// <summary>
    /// Publishes a message on the event bus
    /// </summary>
    public sealed class PublishMessage : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorPublisher> Publisher { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        public InArgument<Guid?> MachineGroupId { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new Message
            {
                MessageType = MessageType.Get(context),
                MachineGroupId = MachineGroupId.Get(context)
            };
            var publisher = Publisher.Get(context);
            publisher.Publish(message);
        }
    }
}