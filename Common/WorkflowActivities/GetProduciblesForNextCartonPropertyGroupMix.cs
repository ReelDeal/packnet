﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public class GetProduciblesForNextCartonPropertyGroupMix : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> AvailableProducibles { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var producibles = Producibles.Get(context).ToList();
            var availableProducibles = AvailableProducibles.Get(context).ToArray();
            var machineGroupId = MachineGroupId.Get(context);

            if (producibles.Any() == false)
                return producibles;

            var cpgService = serviceLocator.Locate<ICartonPropertyGroupService>();
            var productionGroupService = serviceLocator.Locate<IProductionGroupService>();

            var productionGroup = productionGroupService.GetProductionGroupForMachineGroup(machineGroupId);

            var cpg = cpgService.GetNextCPGToDispatchWorkTo(productionGroup, producibles, availableProducibles);
            var produciblesForNextCpgInMix = producibles.Where(p => ProducibleIsAssignedToCpg(p, cpg));

            return produciblesForNextCpgInMix.ToList();
        }

        private static bool ProducibleIsAssignedToCpg(IProducible producible, CartonPropertyGroup cpg)
        {
            var cpgForProducible = producible.Restrictions.OfType<BasicRestriction<CartonPropertyGroup>>().FirstOrDefault();
            if (cpgForProducible == null)
                return false;
            return cpgForProducible.Value.Alias == cpg.Alias;
        }
    }
}

