﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Activities;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Search producibles restrictins for ProductionGroupSpecificRestriction.
    /// </summary>
    public sealed class GetProductionGroupFromRestrictions : CodeActivity<ProductionGroup>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        /// <summary>
        /// The producible should have a restriction of type ProductionGroupSpecificRestriction assigned to the restriction list.
        /// </summary>
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }

        protected override ProductionGroup Execute(CodeActivityContext context)
        {
            var locator = ServiceLocator.Get(context);
            var pgService = locator.Locate<IProductionGroupService>();
            var producible = Producible.Get(context);
            var pgRestriction = producible.Restrictions.FirstOrDefault(r => r is ProductionGroupSpecificRestriction);

            if (pgRestriction == null)
                return null;
            return
                pgService.ProductionGroups.FirstOrDefault(pg => pg.Id == ((ProductionGroupSpecificRestriction)pgRestriction).Value);

        }
    }
}
