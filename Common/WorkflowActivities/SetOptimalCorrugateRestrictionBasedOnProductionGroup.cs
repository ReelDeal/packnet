﻿using System.Collections.Generic;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using System.Activities;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Machines;

using LogLevel = PackNet.Common.Interfaces.Logging.LogLevel;

namespace PackNet.Common.WorkflowActivities
{
    using PackNet.Common.Interfaces.DTO.Orders;

    //todo: add unit tests
    public sealed class SetOptimalCorrugateRestrictionBasedOnProductionGroup : CodeActivity
    {
        /// <summary>
        /// BoxLastProducible that the optimal corrugate will be assigned to.
        /// </summary>
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }

        /// <summary>
        /// Production group alias
        /// </summary>
        [RequiredArgument]
        public InArgument<string> ProductionGroupAlias { get; set; }

        /// <summary>
        /// Service used to find the production group for the machine group
        /// </summary>
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        /// <summary>
        /// The number of tiles to use when calculating the optimal corrugate.
        /// The default of one will be used if no value is given.
        /// </summary>
        private InArgument<int> allowedTileCount = 1;
        public InArgument<int> AllowedTileCount { get { return allowedTileCount; } set { allowedTileCount = value; } }

        /// <summary>
        /// The system will cache previous optimal corrugate calculations to reuse them.
        /// If this variable is set to true then the cached value will be ignored and the
        /// optimal corrugate will be recalculated.
        /// </summary>
        private InArgument<bool> recalculateCachedItem = false;
        public InArgument<bool> RecalculateCachedItem { get { return recalculateCachedItem; } set { recalculateCachedItem = value; } }

        private IEnumerable<Corrugate> corrugatesInProductionGroup;

        private IEnumerable<IPacksizeCutCreaseMachine> machinesInProductionGroup;

        private ICorrugateService corrugateService;

        private ILogger logger;

        /// <summary>
        /// Finds the optimal corrugate for the given carton based on its production group,
        /// the machines in that production group and the corrugates assigned to those machines.
        /// </summary>
        /// <param name="context">Workflow context</param>
        /// <remarks>
        /// If the production group does not exist, or if no machines are assigned to it,
        /// or if no corrugates are assigned to the machines then the optimal corrugate 
        /// restriction will not be set.
        /// </remarks>
        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            logger = serviceLocator.Locate<ILogger>();
            corrugateService = serviceLocator.Locate<ICorrugateService>();

            var maxTiles = AllowedTileCount.Get(context);
            var productionGroupService = serviceLocator.Locate<IProductionGroupService>();
            var producible = Producible.Get(context);
            var productionGroupAlias = ProductionGroupAlias.Get(context);
            var recalculateCache = RecalculateCachedItem.Get(context);

            if (producible == null)
            {
                logger.Log(LogLevel.Warning,
                    "SetOptimalCorrugateRestrictionBasedOnProductionGroup Workflow Code Activity called with a null producible.",
                    productionGroupAlias);
                return;
            }

            if (producible is IPrintable)
            {
                logger.Log(LogLevel.Info, "Producible {0} is an IPrintable - skipping optimal corrugate selection", producible.Id);
                return;
            }

            var cartonProductionGroup = productionGroupService.FindByAlias(productionGroupAlias);
            if (cartonProductionGroup == null)
            {
                logger.Log(LogLevel.Warning,
                    "SetOptimalCorrugateRestrictionBasedOnProductionGroup Workflow Code Activity could not find ProductionGroup for ProductionGroupAlias:{0}",
                    productionGroupAlias);
                return;
            }

            var machineGroupService = serviceLocator.Locate<IMachineGroupService>();

            List<MachineGroup> machineGroupsInProductionGroup =
                machineGroupService
                    .MachineGroups.Where(mg => cartonProductionGroup.ConfiguredMachineGroups.Contains(mg.Id))
                    .ToList();

            IAggregateMachineService machineService = serviceLocator.Locate<IAggregateMachineService>();
            machinesInProductionGroup =
                machineGroupsInProductionGroup.SelectMany(
                    mg =>
                        machineService
                            .Machines.Where(m => mg.ConfiguredMachines.Contains(m.Id))
                            .OfType<IPacksizeCutCreaseMachine>()).ToList();

            IEnumerable<Track> tracks = machinesInProductionGroup.SelectMany(machine => machine.Tracks);

            corrugatesInProductionGroup =
                tracks.Select(track => track.LoadedCorrugate).Where(corrugate => corrugate != null).DistinctBy(c => c.Id).ToList();

            if (!machinesInProductionGroup.Any() || !corrugatesInProductionGroup.Any())
            {
                logger.Log(LogLevel.Warning,
                    "SetOptimalCorrugateRestrictionBasedOnProductionGroup did not find machine in the PG or corrugates.",
                    productionGroupAlias);
                return;
            }

            var foundCorrugate = false;

            if (producible is ICarton)
            {
                foundCorrugate = SetOptimalCorrugateForCarton(producible as ICarton, maxTiles, recalculateCache);
            }
            else
            {
                var producibles = producible as Kit;
                if (producibles != null)
                {
                    if (producibles.ItemsToProduce == null)
                    {
                        return;
                    }
                    if (producibles.ItemsToProduce.All(p => p is Order))
                        maxTiles = 0;

                    maxTiles = producibles.ItemsToProduce.OfType<Order>().Aggregate(maxTiles, (current, order) => current + order.RemainingQuantity);

                    foundCorrugate =
                        producibles.ItemsToProduce.SelectMany(i => i.FindAllICarton())
                            .Where(p => p != null)
                            .All(p => SetOptimalCorrugateForCarton(p, maxTiles, recalculateCache));
                    producibles.ItemsToProduce.OfType<Order>().ForEach(o => o.Restrictions.ReplaceAll(o.Producible.Restrictions));
                    producibles.UpdateMyRestrictions();
                }
            }

            if (foundCorrugate)
            {
                producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged;
            }
            else
            {
                producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
            }
        }

        private bool SetOptimalCorrugateForCarton(ICarton producible, int maxTiles, bool recalcCache)
        {
            
            var optimalCorrugate = corrugateService.GetOptimalCorrugate(
                corrugatesInProductionGroup,
                machinesInProductionGroup,
                producible,
                maxTiles,
                recalcCache);
            if (optimalCorrugate != null)
            {
                producible.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(optimalCorrugate));
                producible.CartonOnCorrugate = optimalCorrugate;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
