﻿using System.Activities;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities.Producibles
{
    //todo: find out how to add these to a collection int he toolbox.
    public sealed class CreateLicensePlateLabel : CodeActivity
    {
        [RequiredArgument]
        // [DefaultValue(ServiceLocator)]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
       
        /// <summary>
        /// Customer Unique Id
        /// </summary>
        [RequiredArgument]
        public InArgument<string> CustomerUniqueId{ get; set; }

        public OutArgument<Label> Label
        {
            get; set;
        }

        protected override void Execute(CodeActivityContext context)
        {
            var templateService = ServiceLocator.Get(context).Locate<ITemplateService>();
            var template = templateService.FindByName("DefaultLicensePlate");
            var label = new Label();
            var cuid = CustomerUniqueId.Get(context);
            var printData = new Dictionary<string, string> { { "CustomerUniqueId", cuid } };
            label.PrintData = printData;
            label.Restrictions.Add(new BasicRestriction<Template>(template));
            label.CustomerUniqueId = cuid;
            Label.Set(context,label);
        }

    }
}
