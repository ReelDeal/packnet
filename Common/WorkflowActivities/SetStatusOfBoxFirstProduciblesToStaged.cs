﻿using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

namespace PackNet.Common.WorkflowActivities
{
    public class SetStatusOfBoxFirstProduciblesToStaged : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
 
        protected override void Execute(CodeActivityContext context)
        {
            var locator = ServiceLocator.Get(context);
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>();
            var boxFirstService = locator.Locate<IBoxFirstSelectionAlgorithmService>();

            //todo: consider moving selected out of NotInProductionProducibleStatuses
            boxFirstService.UpdateProducibleStatuses(producibles.Where(p => 
                p.CartonOnCorrugates != null 
                && p.CartonOnCorrugates.Any() 
                && ((p.ProducibleStatus is NotInProductionProducibleStatuses &&
                         p.ProducibleStatus != NotInProductionProducibleStatuses.ProducibleSelected &&
                         p.ProducibleStatus != NotInProductionProducibleStatuses.AddedToMachineGroupQueue)
                        || p.ProducibleStatus is ErrorProducibleStatuses)).ToList()
                , NotInProductionProducibleStatuses.ProducibleStaged);

            boxFirstService.UpdateProducibleStatuses(producibles.Where(p => p.CartonOnCorrugates == null || !p.CartonOnCorrugates.Any()).ToList(), ErrorProducibleStatuses.NotProducible);
        }
    }
}
