﻿using System;
using System.Activities;
using System.Linq;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;

namespace PackNet.Common.WorkflowActivities
{
    public sealed class PacksizeCutCreaseMachineReleaseBox
       : CodeActivity
    {
        private static IEventAggregatorPublisher PUBLISHER;
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }
        [RequiredArgument]
        public InArgument<MachineGroup> MachineGroup { get; set; }
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var mg = MachineGroup.Get(context);
            var sl = ServiceLocator.Get(context);
            var producible = Producible.Get(context); //TODO: Refactor, should be generic
            var logger = sl.Locate<ILogger>();
            //now remove the the restriction to hold the box
            //new BasicRestriction<string>(CartonRestrictions.ShouldWaitForBoxRelease)            
            producible.Restrictions.Remove(new BasicRestriction<string>(CartonRestrictions.ShouldWaitForBoxRelease));

            PUBLISHER = PUBLISHER ?? sl.Locate<IEventAggregatorPublisher>();
            var message = new Message<IProducible>
            {
                MachineGroupId = mg.Id,
                Data = producible,
                MessageType = CartonMessages.ReleaseBox
            };
            logger.Log(LogLevel.Trace, "Releasing box {0} on mg {1}", producible, mg);
            PUBLISHER.Publish(message);
        }
    }
}