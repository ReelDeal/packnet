﻿using System;
using System.Collections.Generic;
using System.Activities;
using System.Linq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{

    public sealed class GetProduciblesWithHighestClassification : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }  

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var locator = ServiceLocator.Get(context);
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>();
            
            //Dont evaluate anything if the list is empty
            if (!producibles.Any())
                return producibles;

            var classificationService = locator.Locate<IClassificationService>();

            var allClassifications = classificationService.Classifications.ToList();

            var grouped = allClassifications.GroupBy(c => c.Status).ToList();
            var expeditedGroup = grouped.FirstOrDefault(c => c.Key == ClassificationStatuses.Expedite);
            var normalGroup = grouped.FirstOrDefault(c => c.Key == ClassificationStatuses.Normal);
            var lastGroup = grouped.FirstOrDefault(c => c.Key == ClassificationStatuses.Last);

            if (expeditedGroup != null)
            {
                var expediteAliases = expeditedGroup.Select(c => c.Alias);
                var expedited = producibles.Where(p => ProducibleHasClassification(p, expediteAliases));
                if (expedited.Any())
                    return expedited.ToList();
            }
            if (normalGroup != null)
            {
                var normalAliases = normalGroup.Select(c => c.Alias);
                var normal = producibles.Where(p => ProducibleHasClassification(p, normalAliases));
                if (normal.Any())
                    return normal.ToList();
            }
            if (lastGroup != null)
            {
                var lastAliases = lastGroup.Select(c => c.Alias);
                var last = producibles.Where(p => ProducibleHasClassification(p, lastAliases));
                if (last.Any())
                    return last.ToList(); 
            }

            return new List<IProducible>();
        }

        private bool ProducibleHasClassification(BoxFirstProducible p, IEnumerable<string> classificationAliases)
        {
            var classification = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();
            if (classification == null)
                return false;

            return classificationAliases.Contains(classification.Alias);
        }
    }
}
