﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public class FilterUnstagedAndStoppedProducibles : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            
            var producibles = Producibles.Get(context);
            
            var stagedProducibles = producibles.GetByStatus(NotInProductionProducibleStatuses.ProducibleStaged);
            if (!stagedProducibles.Any())
                return stagedProducibles;

            var machineGroupId = MachineGroupId.Get(context);
            var locator = ServiceLocator.Get(context);
            var cpgService = locator.Locate<ICartonPropertyGroupService>();
            var classificationService = locator.Locate<IClassificationService>();
            var pickZoneService = locator.Locate<IPickZoneService>();
            var pgService = locator.Locate<IProductionGroupService>();
            var userNotificationService = locator.Locate<IUserNotificationService>();

            var pgId = Guid.Empty;
            var pg = pgService.GetProductionGroupForMachineGroup(machineGroupId);
            if (pg != null)
            {
                pgId = pg.Id;
            }

            var produciblesFilteredByCpg = FilterStoppedCartonPropertyGroups(cpgService, stagedProducibles);
            if (!produciblesFilteredByCpg.Any())
            {
                userNotificationService.SendNotificationToProductionGroup(NotificationSeverity.Warning, string.Format("No active CPGs in your production group contains jobs. {0} producibles could become available if enabling a stopped CPG.", stagedProducibles.Count()), "", pgId);
                return produciblesFilteredByCpg;
            }

            var produciblesFilteredByCpgAndClassification = FilterStoppedClassifications(classificationService, produciblesFilteredByCpg);
            if (!produciblesFilteredByCpgAndClassification.Any())
            {
                userNotificationService.SendNotificationToProductionGroup(NotificationSeverity.Warning, string.Format("No active classifications in your production group contains jobs. {0} producibles could become available if enabling a stopped classification.", produciblesFilteredByCpg.Count()), "", pgId);
                return produciblesFilteredByCpgAndClassification;
            }
            
            var produciblesFilteredByCpgAndClassificationAndPickZone = FilterStoppedPickZones(pickZoneService, produciblesFilteredByCpgAndClassification);
            if (!produciblesFilteredByCpgAndClassificationAndPickZone.Any())
            {
                userNotificationService.SendNotificationToProductionGroup(NotificationSeverity.Warning, string.Format("No active pick zones in your production group contains jobs. {0} producibles could become available if enabling a stopped Pick Zone", produciblesFilteredByCpgAndClassification.Count()), "", pgId);
                return produciblesFilteredByCpgAndClassificationAndPickZone;
            }

            return produciblesFilteredByCpgAndClassificationAndPickZone;
        }

        private IEnumerable<IProducible> FilterStoppedCartonPropertyGroups(ICartonPropertyGroupService cpgService, IEnumerable<IProducible> producibles)
        {
            var stoppedPropertyGroupsAliases = cpgService.GetCartonPropertyGroupsByStatus(CartonPropertyGroupStatuses.Stop).Select(cpg => cpg.Alias);
            var produciblesFilteredByCPG = producibles.Where(p => BelongsToUnstoppedCartonPropertyGroup(p, stoppedPropertyGroupsAliases)).ToList();
            return produciblesFilteredByCPG;
        }

        private IEnumerable<IProducible> FilterStoppedClassifications(IClassificationService classificationService, IEnumerable<IProducible> producibles)
        {
            var stoppedClassificationAliases = classificationService.GetClassificationsByStatus(ClassificationStatuses.Stop).Select(cls => cls.Alias);
            var produciblesFilteredByClassification = producibles.Where(p => BelongsToUnstoppedClassification(p, stoppedClassificationAliases)).ToList();
            return produciblesFilteredByClassification;
        }

        private IEnumerable<IProducible> FilterStoppedPickZones(IPickZoneService pickZoneService, IEnumerable<IProducible> producibles)
        {
            var stoppedPickZoneIds = pickZoneService.GetPickZonesByStatus(PickZoneStatuses.Stop).Select(pz => pz.Id);
            var produciblesFilteredByPickZone = producibles.Where(p => BelongsToUnstoppedPickZone(p, stoppedPickZoneIds)).ToList();
            return produciblesFilteredByPickZone;
        }

        private bool BelongsToUnstoppedCartonPropertyGroup(IProducible p, IEnumerable<string> stoppedPropertyGroupsAliases)
        {
            var cpg = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>();

            if (cpg == null)
                return true;

            return stoppedPropertyGroupsAliases.Contains(cpg.Alias) == false;
        }

        private bool BelongsToUnstoppedClassification(IProducible p, IEnumerable<string> stoppedClassificationAliases)
        {
            var cls = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<Classification>();

            if (cls == null)
                return true;

            return stoppedClassificationAliases.Contains(cls.Alias) == false;
        }

        private bool BelongsToUnstoppedPickZone(IProducible p, IEnumerable<Guid> stoppedPickZoneIds)
        {
            var pz = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<PickZone>();

            if (pz == null)
                return true;

            return stoppedPickZoneIds.Contains(pz.Id) == false;
        }
    }
}
