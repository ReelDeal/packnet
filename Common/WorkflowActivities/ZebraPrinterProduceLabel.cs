﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;

namespace PackNet.Common.WorkflowActivities
{
	public sealed class ZebraPrinterProduceLabel
		: CodeActivity
	{
		[RequiredArgument]
		public InArgument<IProducible> Producible { get; set; }
		[RequiredArgument]
		public InArgument<MachineGroup> MachineGroup { get; set; }
		[RequiredArgument]
		public InArgument<IServiceLocator> ServiceLocator { get; set; }

		protected override void Execute(CodeActivityContext context)
		{
			var mg = MachineGroup.Get(context);
			var sl = ServiceLocator.Get(context);
            var label = Producible.Get(context);
            if (sl == null)
                throw new ArgumentException("ServiceLocator is null or not passed to ZebraPrinterProduceLabel.");

            var logger = sl.Locate<ILogger>();
			if (!(label is IPrintable))//todo fill out exception.
				throw new ArgumentException("item is not a label");

			var machineService = sl.Locate<IAggregateMachineService>();
			var restrictionResolverService = sl.Locate<IRestrictionResolverService>();
            var zebraPrinters = new List<ZebraPrinter>();
            //get list of zebraprinters
			mg.ConfiguredMachines.ForEach(m =>
			{
				var zm = machineService.FindById(m);
				//todo: do this for real not just the first zebra in the group.
                if (zm is ZebraPrinter)
                    zebraPrinters.Add(zm as ZebraPrinter);
            });

            //ensure that no other label printer in the group is working on a job because we never want two labels on the printer at the same time.
            if (zebraPrinters.Any(p => p.IsPeelOff && p.CurrentProductionStatus is MachineProductionInProgressStatuses))
            {
                logger.Log(LogLevel.Info, "ZebraPrinterProduceLabel waiting until all printers are idle before sending label {0} to {1}", label, mg);
                var waitCount = 0;
                while (zebraPrinters.Any(p => p.CurrentProductionStatus is MachineProductionInProgressStatuses))
                {
                    waitCount++;
                    Thread.Sleep(200);
                    if(waitCount % 5 == 0)
                        logger.Log(LogLevel.Info, "{2} ZebraPrinterProduceLabel waiting until all printers are idle before sending label {0} to {1}", label, mg, waitCount);
                }
			}

            // send label to first that meets restrictions
            var printerToCompleteWork = zebraPrinters.FirstOrDefault(
                zm => zm.CurrentProductionStatus == MachineProductionStatuses.ProductionIdle
                && zm.CurrentStatus == MachineStatuses.MachineOnline 
                && label.Restrictions.All(r => restrictionResolverService.Resolve(r, zm.CurrentCapabilities.ToList()))
                );
            if (printerToCompleteWork == null)
                throw new ApplicationException(string.Format("No printer in machine group is idle, online and able to produce label with restrictions. {0}, {1}",
                    mg, label));

            label.ProduceOnMachineId = printerToCompleteWork.Id;
            
            var task = Task.Factory.StartNew(() => machineService.Produce(printerToCompleteWork.Id, label));
            task.ContinueWith((t) =>
            {
                if (t.IsFaulted)
                    logger.Log(LogLevel.Error, "Error: failed to print with error " + t.Exception);
            });
        }
	}
}