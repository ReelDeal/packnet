﻿using System;
using System.Activities;

using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities.Designs
{

    public sealed class DesignExists : CodeActivity<bool>
    {
        public InArgument<int> DesignId { get; set; }
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override bool Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var packagingDesignService = serviceLocator.Locate<IPackagingDesignService>();
            return !String.IsNullOrWhiteSpace(packagingDesignService.GetDesignName(DesignId.Get(context)));
        }
    }
}
