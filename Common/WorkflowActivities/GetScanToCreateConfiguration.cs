﻿using System.Activities;

using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;

namespace PackNet.Common.WorkflowActivities
{
    public class GetScanToCreateConfiguration : CodeActivity<ScanToCreateSelectionAlgorithmConfiguration>
    {
        public InArgument<ProductionGroup> ProductionGroup { get; set; }

        protected override ScanToCreateSelectionAlgorithmConfiguration Execute(CodeActivityContext context)
        {
            return ProductionGroup.Get(context).ScanToCreateConfiguration();
        }
    }
}