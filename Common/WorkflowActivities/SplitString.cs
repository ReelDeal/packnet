﻿using System;
using System.Activities;

namespace PackNet.Common.WorkflowActivities
{
    public class SplitString : CodeActivity<string[]>
    {
        public InArgument<String> StringToSplit { get; set; }
        public InArgument<Char> CharacterToSplitWith { get; set; }

        protected override string[] Execute(CodeActivityContext context)
        {
            var theString = StringToSplit.Get(context);
            var theChar = CharacterToSplitWith.Get(context);
            var result = theString.Split(theChar);

            return result;
        }
    }
}