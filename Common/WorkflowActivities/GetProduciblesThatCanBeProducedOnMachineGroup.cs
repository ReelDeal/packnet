﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Activities;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

namespace PackNet.Common.WorkflowActivities
{
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.Enums;

    public sealed class GetProduciblesThatCanBeProducedOnMachineGroup : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; } 

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var machineGroupId = MachineGroupId.Get(context);
            var userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>();
            if (!producibles.Any())
            {
                var pgService = serviceLocator.Locate<IProductionGroupService>();
                var pg = pgService.GetProductionGroupForMachineGroup(machineGroupId);
                if (pg != null && !pg.ConfiguredCorrugates.Any())
                    userNotificationService.SendNotificationToProductionGroup(NotificationSeverity.Warning, string.Format("Production group '{0}' does not have any configured corrugates", pg.Alias), string.Empty, pg.Id);
                else
                    userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Info, "There are no available producibles for your machine group.", "", machineGroupId);
                return producibles;
            }

            var machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            var boxFirstService = serviceLocator.Locate<IBoxFirstSelectionAlgorithmService>();
            var machineGroup = machineGroupService.FindByMachineGroupId(machineGroupId);

            var filteredByClassification = boxFirstService.FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(machineGroup, producibles).Cast<BoxFirstProducible>().ToList();
            if (!filteredByClassification.Any())
            {
                userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Warning, "This machine group cannot produce any of the remaining producibles. No printer is available.", "", machineGroupId);
                return filteredByClassification;
            }
            
            var filteredByClassificationAndOptimalCorrugate = boxFirstService.GetProduciblesWhereMachineGroupContainsMostOptimalCorrugate(machineGroup, filteredByClassification).ToList();
            if (!filteredByClassificationAndOptimalCorrugate.Any())
            {
                SendNoCorrugateMessageToUI(userNotificationService, machineGroupId, filteredByClassification.Count(p => !p.CartonOnCorrugates.Any()), filteredByClassification.Count(p => p.CartonOnCorrugates.Any()), filteredByClassification.SelectMany(b => b.CartonOnCorrugates).Distinct());
                return filteredByClassificationAndOptimalCorrugate;
            }

            return filteredByClassificationAndOptimalCorrugate;
        }

        private void SendNoCorrugateMessageToUI(IUserNotificationService service, Guid machineGroupId, int withoutCorrugate, int withCorrugate, IEnumerable<CartonOnCorrugate> uniqueCartonOnCorrugates)
        {
            if (withoutCorrugate > 0)
            {
                service.SendNotificationToMachineGroup(
                    NotificationSeverity.Warning,
                    string.Format("This machine group can't produce {0} of the remaining producibles because the optimal corrugate doesn't exist in the system. This could be because of tool configurations or wrong machine type.", withoutCorrugate),
                    "",
                    machineGroupId);
            }

            if (withCorrugate > 0)
            {
                service.SendNotificationToMachineGroup(
                    NotificationSeverity.Warning,
                    string.Format(
                        "This machine group doesn't contain the optimal corrugate for {0} of the remaining producibles. Optimal corrugates are: {1}",
                        withCorrugate, string.Join(", ", uniqueCartonOnCorrugates.Select(coc => coc.Corrugate.Alias + (coc.Rotated ? " with rotation" : " without rotation" + " and " + coc.TileCount + " tiles")).Distinct())), string.Empty,
                    machineGroupId);
            }
        }
    }
}
