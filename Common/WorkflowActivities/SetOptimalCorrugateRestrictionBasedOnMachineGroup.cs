﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using System.Activities;
using System.Linq;

namespace PackNet.Common.WorkflowActivities
{
    public sealed class SetOptimalCorrugateRestrictionBasedOnMachineGroup : CodeActivity
    {
        /// <summary>
        /// Machine group whose assigned production will be used to optimize against
        /// </summary>
        [RequiredArgument]
        public InArgument<MachineGroup> MachineGroup { get; set; }

        /// <summary>
        /// Service used to find the PacksizeCutCrease machines in the machine group.
        /// </summary>
        [RequiredArgument]
        public InArgument<IMachineService<IMachine>> MachineService { get; set; }

        /// <summary>
        /// Service used to find the optimal corrugate
        /// </summary>
        [RequiredArgument]
        public InArgument<ICorrugateService> CorrugateService { get; set; }

        /// <summary>
        /// Carton that the optimal corrugate will be assigned to.
        /// </summary>
        [RequiredArgument]
        public InArgument<ICarton> Carton { get; set; }

        /// <summary>
        /// The number of tiles to use when calculating the optimal corrugate.
        /// The default of one will be used if no value is given.
        /// </summary>
        private InArgument<int> allowedTileCount = 1;
        public InArgument<int> AllowedTileCount { get { return allowedTileCount; } set { allowedTileCount = value; } }

        /// <summary>
        /// The system will cache previous optimal corrugate calculations to reuse them.
        /// If this variable is set to true then the cached value will be ignored and the
        /// optimal corrugate will be recalculated.
        /// </summary>
        private InArgument<bool> recalculateCachedItem = false;
        public InArgument<bool> RecalculateCachedItem { get { return recalculateCachedItem; } set { recalculateCachedItem = value; } }

        protected override void Execute(CodeActivityContext context)
        {
            var machinesInMachineGroup = MachineService.Get(context).Machines
                .Where(m => MachineGroup.Get(context).ConfiguredMachines.Contains(m.Id))
                .OfType<IPacksizeCutCreaseMachine>().ToList();

            var corrugatesInMachineGroup = machinesInMachineGroup
                .SelectMany(machine => machine.Tracks)
                .Select(track => track.LoadedCorrugate)
                .Where(corrugate => corrugate != null)
                .DistinctBy(c => c.Id).ToList();

            if (!machinesInMachineGroup.Any() || !corrugatesInMachineGroup.Any())
                return;

            var carton = Carton.Get(context);
            var optimalCorrugate = CorrugateService.Get(context).GetOptimalCorrugate(
                corrugatesInMachineGroup,
                machinesInMachineGroup,
                carton,
                AllowedTileCount.Get(context),
                RecalculateCachedItem.Get(context));

            carton.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(optimalCorrugate));
            carton.CartonOnCorrugate = optimalCorrugate;
        }
    }
}
