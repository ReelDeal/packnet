﻿using System;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using System.Activities;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Publishes a message on the event bus
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class PublishMessageToUi<T> : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IUICommunicationService> UiCommunicationService { get; set; }

        [RequiredArgument]
        public InArgument<T> ObjectToPublish { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        public InArgument<Guid?> MachineGroupId { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new Message<T>
            {
                Data = ObjectToPublish.Get(context),
                MessageType = MessageType.Get(context),
                MachineGroupId = MachineGroupId.Get(context)
            };
            var uiCommunicationService = UiCommunicationService.Get(context);
            uiCommunicationService.SendMessageToUI(message);
        }
    }

    /// <summary>
    /// Publishes a message on the event bus
    /// </summary>
    public sealed class PublishMessageToUi : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IUICommunicationService> UiCommunicationService { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        public InArgument<Guid?> MachineGroupId { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var message = new Message
            {
                MessageType = MessageType.Get(context),
                MachineGroupId = MachineGroupId.Get(context)
            };
            var uiCommunicationService = UiCommunicationService.Get(context);
            uiCommunicationService.SendMessageToUI(message);
        }
    }
}