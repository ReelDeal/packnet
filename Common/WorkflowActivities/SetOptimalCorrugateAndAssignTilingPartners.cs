﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;
using PackNet.Common.WorkflowActivities.Notification;

namespace PackNet.Common.WorkflowActivities
{
    public class SetOptimalCorrugateAndAssignTilingPartners : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var locator = ServiceLocator.Get(context);
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>();
            var boxFirstService = locator.Locate<IBoxFirstSelectionAlgorithmService>();
          
            return boxFirstService.AssignOptimalCorrugateAndTilingPartnersForProducibles(producibles);
        }
    }
}
