﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

namespace PackNet.Common.WorkflowActivities
{

    public sealed class GetNextJobToProduce : CodeActivity<IProducible>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        protected override IProducible Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var machineGroupId = MachineGroupId.Get(context);
            var producibles = Producibles.Get(context);
            var machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            var boxFirstService = serviceLocator.Locate<IBoxFirstSelectionAlgorithmService>();
            var machineGroup = machineGroupService.FindByMachineGroupId(machineGroupId);

            return boxFirstService.PrepareNextJobForMachineGroup(machineGroup, producibles);
        }
    }
}
