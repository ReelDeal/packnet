﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Activities;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public sealed class GetProduciblesWithHighestCpg : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var locator = ServiceLocator.Get(context);
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>().ToList();
            var cpgService = locator.Locate<ICartonPropertyGroupService>();

            var cpgs = cpgService.Groups.ToList();
            var grouped = cpgs.GroupBy(cpg => cpg.Status).ToList();
            var surgedGroup = grouped.FirstOrDefault(g => g.Key == CartonPropertyGroupStatuses.Surge);
            var normalGroup = grouped.FirstOrDefault(g => g.Key == CartonPropertyGroupStatuses.Normal);

            if (surgedGroup != null)
            {
                var surgedCpgAliases = surgedGroup.Select(c => c.Alias);
                var surged = producibles.Where(p => ProducibleHasCartonProperyGroup(p, surgedCpgAliases)).ToList();
                if (surged.Any())
                    return surged;
            }
            if (normalGroup != null)
            {
                var normalCpgAliases = normalGroup.Select(c => c.Alias);
                var normal = producibles.Where(p => ProducibleHasCartonProperyGroup(p, normalCpgAliases)).ToList();
                if (normal.Any())
                    return normal;
            }

            return new List<IProducible>();
        }

        private bool ProducibleHasCartonProperyGroup(BoxFirstProducible p, IEnumerable<string> cpgAliases)
        {
            var cpg = p.Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>();
            if (cpg == null)
                return false;

            return cpgAliases.Contains(cpg.Alias);
        }
    }
}
