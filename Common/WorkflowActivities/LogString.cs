﻿using System.Collections.Generic;
using System.Activities;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

using LogLevel = PackNet.Common.Interfaces.Logging.LogLevel;

namespace PackNet.Common.WorkflowActivities
{
    public sealed class LogString : CodeActivity
    {
        static ILogger logger;

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<LogLevel> Level { get; set; }
        [RequiredArgument]
        public InArgument<string> Message { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            logger = logger ?? ServiceLocator.Get(context).Locate<ILogger>();
            logger.Log(Level.Get(context), Message.Get(context));
        }
    }
}