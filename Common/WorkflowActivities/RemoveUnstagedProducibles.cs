﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Filter out all items that are not in production or selected for production.
    /// </summary>
    public class ReturnNotInProductionProducibleStatusesProducibles : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        
        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var producibles = Producibles.Get(context);
            var stagedProducibles =
                producibles.Where(
                    p =>
                        (p.ProducibleStatus is NotInProductionProducibleStatuses &&
                         p.ProducibleStatus != NotInProductionProducibleStatuses.ProducibleSelected)
                        || p.ProducibleStatus is ErrorProducibleStatuses).ToList();
            return stagedProducibles;
        }
    }
}