﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    public class OrderByClassificationStatusAndCpgStatus : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IEnumerable<BoxFirstProducible>> Producibles { get; set; }

        [RequiredArgument]
        public InArgument<ICartonPropertyGroupService> CartonPropertyGroupService { get; set; }

        [RequiredArgument]
        public InArgument<IClassificationService> ClassificationService { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            //var cpgService = CartonPropertyGroupService.Get(context);
            //var classService = ClassificationService.Get(context);
            var producibles = Producibles.Get(context);
            //var groupStatuses = GetStatusForCartonPropertyGroups(cpgService);
            ////var cpgComparer = new CartonPropertyGroupStatusComparer();
            //var sortedProducibles = producibles.OrderBy(p => groupStatuses[p.CartonPropertyGroupId], cpgComparer).ToList();

            return producibles;
        }

        private IDictionary<Guid, CartonPropertyGroupStatuses> GetStatusForCartonPropertyGroups(ICartonPropertyGroupService cpgService)
        {
            var groupsWithStatus = cpgService.Groups.Select(g => new Tuple<Guid, CartonPropertyGroupStatuses>(g.Id, g.Status));
            return groupsWithStatus.ToDictionary(k => k.Item1, k => k.Item2);
        }
    }
}
