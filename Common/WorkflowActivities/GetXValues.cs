﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Activities;

using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities
{
    using System.Dynamic;
    using System.Globalization;

    public sealed class GetXValues : CodeActivity
    {
        [RequiredArgument]
        public InArgument<Object> Dynamic { get; set; }

        [RequiredArgument]
        public OutArgument<IDictionary<string, double>> ParsedOutputValue { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<int> DesignId { get; set; }

        // If your activity returns a value, derive from CodeActivity<TResult>
        // and return the value from the Execute method.
        protected override void Execute(CodeActivityContext context)
        {
            var obj = Dynamic.Get<object>(context);
            var dynamicItem = obj as ExpandoObject;
            if (obj == null)
                throw new ArgumentException("'Dynamic' must be a ExpandoObject");

            var dictionary = dynamicItem as IDictionary<string, Object>;
            if(dictionary == null)
                throw new ArgumentException("'Dynamic' could not be casted to a dictionary");

            IDictionary<string, double> xValues = new Dictionary<string, double>();
            for (var i = 1; i <= 100; i++)
            {
                var key = dictionary.Keys.FirstOrDefault(k => k.ToLowerInvariant() == "x" + i);
                object value = null;
                double parsedValue = 0;
                if (key != null && dictionary.TryGetValue(key, out value) && value != null && double.TryParse(value.ToString(), NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
                {
                    xValues.Add("X" + i, parsedValue);
                }
            }

            //Validate that we had x values passed in for the design if it is expected

            var serviceLocator = ServiceLocator.Get(context);
            var packagingDesignService = serviceLocator.Locate<IPackagingDesignService>();
            var design = packagingDesignService.GetDesignFromId(DesignId.Get(context));

            foreach (var xValue in design.DesignParameters)
            {
                if (!xValues.Keys.Any(x => x.ToUpper() == xValue.Name.ToUpper()))
                {
                    throw new DesignXValueFailureException(design.WidthOnZFold, design.Id);
                }
            }

            ParsedOutputValue.Set(context, xValues);
        }
    }
}
