﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    /// <summary>
    /// Bookmarks the workflow and waits for a message of payload T and messageType before resuming
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public sealed class WaitForPacksizeMessage<T> : NativeActivity<T> where T : IMessage
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorSubscriber> Subscriber { get; set; }

        [RequiredArgument]
        public InArgument<MessageTypes> MessageType { get; set; }

        [RequiredArgument]
        public InArgument<ILogger> Logger { get; set; }

        [RequiredArgument]
        public OutArgument<T> ResultMessage { get; set; }

        private ILogger logger;

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForPacksizeMessageExtension<T>());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            var subscriber = Subscriber.Get(context);
            var messageType = MessageType.Get(context);
            logger = Logger.Get(context);

            var bookmark = context.CreateBookmark("WaitForPacksizeMessage" + messageType.DisplayName, BookmarkResumed);
            var extension = context.GetExtension<WaitForPacksizeMessageExtension<T>>();
            extension.WaitForMessage(bookmark, subscriber, messageType, logger);
        }

        protected override void Cancel(NativeActivityContext context)
        {
            var extension = context.GetExtension<WaitForPacksizeMessageExtension<T>>();
            extension.DisposeSubscription(logger);
            base.Cancel(context);
        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            ResultMessage.Set(context, (T)value);
            var logger = Logger.Get(context);
            logger.Log(LogLevel.Debug, "Bookmark '{0}' resumed with data '{1}'.", bookmark.Name, (T)value);
        }
    }

    class WaitForPacksizeMessageExtension<T> : PacksizeWorkflowInstanceExtension where T : IMessage
    {
        /// <summary>
        /// Waits for the message, resumes the bookmark with the message
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="subscriber"></param>
        /// <param name="messageType"></param>
        /// <param name="logger"></param>
        public void WaitForMessage(Bookmark bookmark, IEventAggregatorSubscriber subscriber, MessageTypes messageType, ILogger logger)
        {
            subscription = subscriber.GetEvent<T>()
                .Where(msg => msg.MessageType == messageType)
                .DurableSubscribe(msg =>
                {
                    subscription.Dispose();
                    var ias = instance.BeginResumeBookmark(bookmark, msg, null, null);

                    if (ias.IsCompleted)
                    {
                        var result = instance.EndResumeBookmark(ias);
                        logger.Log(LogLevel.Debug, "Message '{0}' received, Resuming bookmark '{1}' with result '{0}'", messageType, bookmark.Name, result);
                    }

                }, logger);
        }
    }
}
