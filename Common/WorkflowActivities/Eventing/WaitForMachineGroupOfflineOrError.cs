﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    /// <summary>
    /// Bookmarks the workflow and waits for a status before resuming
    /// </summary>
    public sealed class WaitForMachineGroupOfflineOrError : NativeActivity<MachineGroupStatuses>
    {
        private ILogger logger;
        
        [RequiredArgument]
        public InArgument<MachineGroup> MachineGroup { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForMachineGroupOfflineOrErrorStatusExtension());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            var sl = ServiceLocator.Get(context);

            var machineGroup = MachineGroup.Get(context);
            logger = sl.Locate<ILogger>();
            if (machineGroup.CurrentStatus is MachineGroupUnavailableStatuses)
            {
                Result.Set(context, machineGroup.CurrentStatus);
                logger.Log(LogLevel.Debug, "{0} Already in expected status.", machineGroup);
                return;
            }

            /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
             of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
            var bookmark = context.CreateBookmark(
                "WaitForAnyMachineGroupNotOnlineStatus" + machineGroup.Alias + Guid.NewGuid(),
                BookmarkResumed);
            var extension = context.GetExtension<WaitForMachineGroupOfflineOrErrorStatusExtension>();
            extension.WaitForStatus(bookmark, machineGroup, logger);

        }

        protected override void Cancel(NativeActivityContext context)
        {
            var extension = context.GetExtension<WaitForMachineGroupOfflineOrErrorStatusExtension>();
            extension.DisposeSubscription(logger);
            base.Cancel(context);
        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            Result.Set(context, (MachineGroupStatuses)value);
            logger.Log(LogLevel.Debug, "Bookmark '{0}' resumed with data '{1}'.", bookmark.Name, (MachineGroupStatuses)value);
        }
    }

    class WaitForMachineGroupOfflineOrErrorStatusExtension : PacksizeWorkflowInstanceExtension
    {
        /// <summary>
        /// Waits for the message, resumes the bookmark with the message
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="logger"></param>
        /// <param name="machine"></param>
        public void WaitForStatus(Bookmark bookmark, MachineGroup machine, ILogger logger)
        {
            var doneLocker = new object();
            var done = false;

            subscription = machine.CurrentStatusChangedObservable
                .Where(ms => ms is MachineGroupUnavailableStatuses)
                .DurableSubscribe(ms =>
                {
                    lock (doneLocker)
                    {
                        if (!done)
                        {
                            subscription.Dispose();
                            done = true;
                            var ias = instance.BeginResumeBookmark(bookmark, ms, null, null);

                            if (ias.IsCompleted)
                            {
                                var result = instance.EndResumeBookmark(ias);
                                logger.Log(LogLevel.Debug,
                                    "MachineGroup '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                                    machine.Alias,
                                    machine.Id, ms, bookmark.Name, result);
                            }
                        }
                    }
                }, logger);
            
            //last minute race check
            if (machine.CurrentStatus is MachineGroupUnavailableStatuses)
            {
                lock (doneLocker)
                {
                    if (!done)
                    {
                        subscription.Dispose();
                        done = true;
                        var ias = instance.BeginResumeBookmark(bookmark, machine.CurrentStatus, null, null);

                        if (ias.IsCompleted)
                        {
                            var result = instance.EndResumeBookmark(ias);
                            logger.Log(LogLevel.Debug,
                                "MachineGroup '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'", machine.Alias,
                                machine.Id, machine.CurrentStatus, bookmark.Name, result);
                        }                        
                    }
                }
            }
        }
    }
}
