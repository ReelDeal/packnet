﻿//using System;
//using System.Activities;
//using System.Activities.Hosting;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reactive.Linq;
//using System.Threading;
//using System.Threading.Tasks;

//using PackNet.Common.Interfaces.Enums.ProducibleStates;
//using PackNet.Common.Interfaces.ExtensionMethods;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Producible;
//using PackNet.Common.Interfaces.Services;

//using LogLevel = PackNet.Common.Interfaces.Logging.LogLevel;

//namespace PackNet.Common.WorkflowActivities.Eventing
//{
//    /// <summary>
//    /// Bookmarks the workflow and waits for a status before resuming.  If the status is already in expected state then we return right away.
//    /// </summary>
//    public sealed class WaitForProducibleStatusType<T> : NativeActivity<ProducibleStatuses>
//    {
//        private static ILogger logger;
//        private static object locker = new object();
//        [RequiredArgument]
//        public InArgument<IProducible> Producible { get; set; }

//        [RequiredArgument]
//        public InArgument<IServiceLocator> ServiceLocator { get; set; }

//        protected override void CacheMetadata(NativeActivityMetadata metadata)
//        {
//            base.CacheMetadata(metadata);
//            metadata.AddDefaultExtensionProvider(() => new WaitForProducibleStatusTypeExtension());
//        }

//        protected override bool CanInduceIdle
//        {
//            get { return true; }
//        }

//        protected override void Execute(NativeActivityContext context)
//        {

//            var producible = Producible.Get(context);

//            lock (locker)
//            {
//                if (logger == null)
//                {
//                    var sl = ServiceLocator.Get(context);
//                    if (sl == null)
//                        throw new ArgumentException("ServiceLocator is null or not passed to WaitForProducibleStatus.");
//                    logger = sl.Locate<ILogger>();

//                    if (logger == null)
//                        throw new ArgumentException("Logger not found in service locator");
//                }
//            }

//            if (producible.ProducibleStatus is T)
//            {
//                Result.Set(context, producible.ProducibleStatus);
//                logger.Log(LogLevel.Debug, "{0} Already in expected status.", producible);
//                return;
//            }
//            /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
//     of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
//            var bookmark = context.CreateBookmark(
//                string.Format("WaitForProducibleStatus-ProducibleId:{0}-Status:{1}-Type:{2}:::{3}"
//                    , producible.Id, typeof(T), producible.ProducibleType.DisplayName, Guid.NewGuid())
//                , BookmarkResumed);
//            var extension = context.GetExtension<WaitForProducibleStatusTypeExtension>();
//            extension.WaitForStatus(bookmark, producible, typeof(T), logger);

//        }

//        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
//        {
//            Result.Set(context, (ProducibleStatuses)value);
//        }
//    }

//    internal class WaitForProducibleStatusTypeExtension : IWorkflowInstanceExtension
//    {
//        private WorkflowInstanceProxy instance;
//        public IEnumerable<object> GetAdditionalExtensions()
//        {
//            return null;
//        }

//        public void SetInstance(WorkflowInstanceProxy instance)
//        {
//            this.instance = instance;
//        }

//        /// <summary>
//        /// Waits for the message, resumes the bookmark with the message
//        /// </summary>
//        /// <param name="bookmark"></param>
//        /// <param name="producible"></param>
//        /// <param name="t"></param>
//        /// <param name="logger"></param>
//        public void WaitForStatus(Bookmark bookmark, IProducible producible, Type t, ILogger logger)
//        {
//            var locker = new object();
//            var done = false;


//            IDisposable o = null;
//            o = producible.ProducibleStatusObservable
//                .Where(ps => ps.GetType() == t)
//                .DurableSubscribe(ps =>
//                {
//                    o.Dispose();
//                    lock (locker)
//                    {
//                        if (!done)
//                        {
//                            done = true;
//                            var ias = instance.BeginResumeBookmark(bookmark, ps, null, null);

//                            if (ias.IsCompleted)
//                            {
//                                var result = instance.EndResumeBookmark(ias);
//                                logger.Log(LogLevel.Debug, "{0} resumed-a with data '{1}'.  Result:{2}", bookmark.Name,
//                                    (ProducibleStatuses)ps, result);
//                            }
//                        }
//                    }
//                }, logger);


//            //last minute race check
//            if (producible.ProducibleStatus.GetType() == t)
//            {
//                o.Dispose();
//                lock (locker)
//                {
//                    if (!done)
//                    {
//                        done = true;
//                        var ias = instance.BeginResumeBookmark(bookmark, producible.ProducibleStatus, null, null);

//                        if (ias.IsCompleted)
//                        {
//                            var result = instance.EndResumeBookmark(ias);
//                            logger.Log(LogLevel.Debug, "{0} resumed-b with data '{1}'.  Result:{2}", bookmark.Name,
//                                (ProducibleStatuses)producible.ProducibleStatus, result);
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
