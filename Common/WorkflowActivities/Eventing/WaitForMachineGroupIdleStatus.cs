﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Reactive.Linq;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    /// <summary>
    /// Bookmarks the workflow and waits for a status before resuming
    /// </summary>
    public sealed class WaitForMachineGroupIdleStatus : NativeActivity
    {
        private ILogger logger;

        [RequiredArgument]
        public InArgument<MachineGroup> MachineGroup { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForMachineGroupIdleStatusExtension());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            var sl = ServiceLocator.Get(context);

            var machineGroup = MachineGroup.Get(context);
            logger = sl.Locate<ILogger>();
            if (machineGroup.CurrentProductionStatus == MachineProductionStatuses.ProductionIdle || machineGroup.CurrentStatus == MachineProductionStatuses.ProductionCompleted)
            {
                logger.Log(LogLevel.Debug, "{0} Already in expected status '{1}' resuming.", machineGroup.LogIdentifiers(),
                machineGroup.CurrentStatus);
                return;
            }

            //if (machine.CurrentStatus != MachineGroupAvailableStatuses.MachineGroupOnline)
            //{
            //    logger.Log(LogLevel.Debug, "{0} Already in expected status '{1}' resuming.", machine.LogIdentifiers(),
            //        machine.CurrentStatus);
            //    return;
            //}
            /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
         of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
            var bookmark = context.CreateBookmark("WaitForMachineGroupIdleStatus" + machineGroup.Alias + Guid.NewGuid(),
                BookmarkResumed);
            var extension = context.GetExtension<WaitForMachineGroupIdleStatusExtension>();
            extension.WaitForStatus(bookmark, machineGroup, logger);

        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            logger.Log(LogLevel.Debug, "Bookmark '{0}' resumed with data '{1}'.", bookmark.Name, (MachineProductionStatuses)value);
        }
    }

    class WaitForMachineGroupIdleStatusExtension : IWorkflowInstanceExtension
    {
        private WorkflowInstanceProxy instance;
        public IEnumerable<object> GetAdditionalExtensions()
        {
            return null;
        }

        public void SetInstance(WorkflowInstanceProxy instance)
        {
            this.instance = instance;
        }

        /// <summary>
        /// Waits for the message, resumes the bookmark with the message
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="logger"></param>
        /// <param name="machineGroup"></param>
        public void WaitForStatus(Bookmark bookmark, MachineGroup machineGroup, ILogger logger)
        {
            var doneLocker = new object();
            var done = false;
            IDisposable o = null;

            o = machineGroup.CurrentProductionStatusObservable // kolla att detta ar ratt observable
                .Where(ms => ms == MachineProductionStatuses.ProductionIdle || machineGroup.CurrentStatus == MachineProductionStatuses.ProductionCompleted)
                .DurableSubscribe(ms =>
                {
                    lock (doneLocker)
                        if (!done)
                        {
                            o.Dispose();
                            done = true;
                            var ias = instance.BeginResumeBookmark(bookmark, ms, null, null);

                            if (ias.IsCompleted)
                            {
                                var result = instance.EndResumeBookmark(ias);
                                logger.Log(LogLevel.Debug,
                                    "MachineGroup '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'", machineGroup.Alias,
                                    machineGroup.Id, ms, bookmark.Name, result);
                            }
                        }
                }, logger);

            //o = machineGroup.CurrentStatusChangedObservable
            //    .Where(ms => ms != MachineGroupAvailableStatuses.MachineGroupOnline)
            //    .DurableSubscribe(ms =>
            //    {
            //        lock (doneLocker)
            //            if (!done)
            //            {
            //                o.Dispose();
            //                done = true;
            //                var ias = instance.BeginResumeBookmark(bookmark, ms, null, null);

            //                if (ias.IsCompleted)
            //                {
            //                    var result = instance.EndResumeBookmark(ias);
            //                    logger.Log(LogLevel.Debug,
            //                        "MachineGroup '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'", machineGroup.Alias,
            //                        machineGroup.Id, ms, bookmark.Name, result);
            //                }
            //            }
            //    }, logger);

            //last minute race check
            if (machineGroup.CurrentProductionStatus == MachineProductionStatuses.ProductionIdle || machineGroup.CurrentStatus == MachineProductionStatuses.ProductionCompleted)
            {
                lock (doneLocker)
                    if (!done)
                    {
                        o.Dispose();
                        done = true;
                        var ias = instance.BeginResumeBookmark(bookmark, machineGroup.CurrentStatus, null, null);

                        if (ias.IsCompleted)
                        {
                            var result = instance.EndResumeBookmark(ias);
                            logger.Log(LogLevel.Debug, "MachineGroup '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                                machineGroup.Alias, machineGroup.Id, machineGroup.CurrentStatus, bookmark.Name, result);
                        }
                    }
            }
        }
    }
}
