﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    /// <summary>
    /// Bookmarks the workflow and waits for a status before resuming
    /// </summary>
    public sealed class WaitForAnyProducibleErrorStatus : NativeActivity<ErrorProducibleStatuses>
    {
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }

        [RequiredArgument]
        public InArgument<ILogger> Logger { get; set; }

        [RequiredArgument]
        public OutArgument<ErrorProducibleStatuses> ResultStatus { get; set; }

        private ILogger logger;

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForAnyProducibleErrorStatusExtension());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            var producible = Producible.Get(context);
            logger = Logger.Get(context);

            if (producible.ProducibleStatus is ErrorProducibleStatuses)
            {
                ResultStatus.Set(context, producible.ProducibleStatus);
                logger.Log(LogLevel.Debug, "{0} Already in expected status.", producible);
                return;
            }
            /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
             of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
            var bookmark = context.CreateBookmark(
               string.Format("WaitForAnyProducibleErrorStatus-Id:{0}-Type:{1}:::", producible.Id, producible.ProducibleType.DisplayName, Guid.NewGuid())
               , BookmarkResumed);
            var extension = context.GetExtension<WaitForAnyProducibleErrorStatusExtension>();
            extension.WaitForStatus(bookmark, producible, logger);
        }

        protected override void Cancel(NativeActivityContext context)
        {
            var extension = context.GetExtension<WaitForAnyProducibleErrorStatusExtension>();
            extension.DisposeSubscription(logger);
            base.Cancel(context);
        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            ResultStatus.Set(context, (ErrorProducibleStatuses)value);
            var logger = Logger.Get(context);
            logger.Log(LogLevel.Debug, "Bookmark '{0}' resumed with data '{1}'.", bookmark.Name, (ErrorProducibleStatuses)value);
        }
    }

    class WaitForAnyProducibleErrorStatusExtension : PacksizeWorkflowInstanceExtension
    {
        /// <summary>
        /// Waits for the message, resumes the bookmark with the message
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="producible"></param>
        /// <param name="logger"></param>
        public void WaitForStatus(Bookmark bookmark, IProducible producible, ILogger logger)
        {
            var locker = new object();
            var done = false;

            subscription = producible.ProducibleStatusObservable
                .Where(ps => ps is ErrorProducibleStatuses)
                .Subscribe(ps =>
                {
                    subscription.Dispose();
                    lock (locker)
                    {
                        if (!done)
                        {
                            done = true;
                            var ias = instance.BeginResumeBookmark(bookmark, ps as ErrorProducibleStatuses, null, null);

                            if (ias.IsCompleted)
                            {
                                var result = instance.EndResumeBookmark(ias);
                                logger.Log(LogLevel.Debug,
                                "Producible '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                                producible.ProducibleType.DisplayName, producible.Id, ps.DisplayName, bookmark.Name, result);
                            }
                        }
                    }
                });

            //last minute race check
            if (producible.ProducibleStatus is ErrorProducibleStatuses)
            {
                subscription.Dispose();
                lock (locker)
                {
                    if (!done)
                    {
                        done = true;
                        var ias = instance.BeginResumeBookmark(bookmark, producible.ProducibleStatus as ErrorProducibleStatuses, null, null);

                        if (ias.IsCompleted)
                        {
                            var result = instance.EndResumeBookmark(ias);
                            logger.Log(LogLevel.Debug,
                            "Producible '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                            producible.ProducibleType.DisplayName, producible.Id, producible.ProducibleStatus.DisplayName, bookmark.Name, result);
                        }
                    }
                }
            }
        }
    }
}
