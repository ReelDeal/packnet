﻿using System;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    public abstract class PacksizeWorkflowInstanceExtension : IWorkflowInstanceExtension
    {
        protected WorkflowInstanceProxy instance;
        protected IDisposable subscription;

        public virtual IEnumerable<object> GetAdditionalExtensions()
        {
            return null;
        }

        public virtual void SetInstance(WorkflowInstanceProxy instance)
        {
            this.instance = instance;
        }

        public virtual void DisposeSubscription(ILogger logger)
        {
            if (subscription != null)
            {
                logger.Log(LogLevel.Trace, "Disposing subscription for " + this);
                subscription.Dispose();
            }
        }
    }
}
