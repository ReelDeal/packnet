﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    /// <summary>
    /// Bookmarks the workflow and waits for a status before resuming
    /// </summary>
    public sealed class WaitForAnyMachineErrorStatus : NativeActivity<MachineErrorStatuses>
    {
        [RequiredArgument]
        public InArgument<IMachine> Machine { get; set; }

        [RequiredArgument]
        public InArgument<ILogger> Logger { get; set; }

        public OutArgument<MachineErrorStatuses> ResultStatus { get; set; }

        private ILogger logger;

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForAnyMachineErrorStatusExtension());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            var machine = Machine.Get(context);
            logger = Logger.Get(context);

            if (machine.CurrentStatus is MachineErrorStatuses)
            {
                Result.Set(context, machine.CurrentStatus as MachineErrorStatuses);
                logger.Log(LogLevel.Debug, "{0} Already in expected status '{1}' resuming.", machine, machine.CurrentStatus);
                return;
            }
            /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
         of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
            var bookmark = context.CreateBookmark("WaitForAnyMachineErrorStatus" + machine.Alias + Guid.NewGuid(),
                BookmarkResumed);
            var extension = context.GetExtension<WaitForAnyMachineErrorStatusExtension>();
            extension.WaitForStatus(bookmark, machine, logger);

        }

        protected override void Cancel(NativeActivityContext context)
        {
            var extension = context.GetExtension<WaitForAnyMachineErrorStatusExtension>();
            extension.DisposeSubscription(logger);
            base.Cancel(context);
        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            ResultStatus.Set(context, (MachineErrorStatuses)value);
            var logger = Logger.Get(context);
            logger.Log(LogLevel.Debug, "Bookmark '{0}' resumed with data '{1}'.", bookmark.Name, (MachineErrorStatuses)value);
        }
    }

    class WaitForAnyMachineErrorStatusExtension : PacksizeWorkflowInstanceExtension
    {
        /// <summary>
        /// Waits for the message, resumes the bookmark with the message
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="logger"></param>
        /// <param name="machine"></param>
        public void WaitForStatus(Bookmark bookmark, IMachine machine, ILogger logger)
        {
            var doneLocker = new object();
            var done = false;

            subscription = machine.CurrentStatusChangedObservable
                .Where(ms => ms is MachineErrorStatuses)
                .DurableSubscribe(ms =>
                {
                    lock (doneLocker)
                        if (!done)
                        {
                            subscription.Dispose();
                            done = true;
                            var ias = instance.BeginResumeBookmark(bookmark, ms as MachineErrorStatuses, null, null);

                            if (ias.IsCompleted)
                            {
                                var result = instance.EndResumeBookmark(ias);
                                logger.Log(LogLevel.Debug, "Machine '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                                    machine.Alias, machine.Id, ms, bookmark.Name, result);
                            }
                        }

                }, logger);
            //Thread race check one more time
            if (machine.CurrentStatus is MachineErrorStatuses)
            {
                lock (doneLocker)
                    if (!done)
                    {
                        subscription.Dispose();
                        done = true;
                        var ias = instance.BeginResumeBookmark(bookmark, machine.CurrentStatus as MachineErrorStatuses, null, null);

                        if (ias.IsCompleted)
                        {
                            var result = instance.EndResumeBookmark(ias);
                            logger.Log(LogLevel.Debug, "Machine '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                                machine.Alias, machine.Id, machine.CurrentStatus, bookmark.Name, result);
                        }
                    }
            }
        }
    }
}
