﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

using LogLevel = PackNet.Common.Interfaces.Logging.LogLevel;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    /// <summary>
    /// Bookmarks the workflow and waits for a status before resuming.  If the status is already in expected state then we return right away.
    /// </summary>
    public sealed class WaitForProducibleStatusForAll : NativeActivity<ProducibleStatuses>
    {
        private static ILogger logger;
        private static object locker = new object();
        private IEnumerable<IProducible> producibles;
        private ProducibleStatuses producibleStatuses;
        private Guid bookmarkGuid;

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        [RequiredArgument]
        public InArgument<ProducibleStatuses> ProducibleStatus { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForProducibleStatusExtension());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {

            producibles = Producibles.Get(context);
            producibleStatuses = ProducibleStatus.Get(context);
            
            lock (locker)
            {
                if (logger == null)
                {
                    var sl = ServiceLocator.Get(context);
                    if (sl == null)
                        throw new ArgumentException("ServiceLocator is null or not passed to WaitForProducibleStatus.");
                    logger = sl.Locate<ILogger>();

                    if (logger == null)
                        throw new ArgumentException("Logger not found in service locator");
                }
            }
            
            if (!producibles.Any())
                throw new ArgumentException("No producibles passed to WaitForProducibleStatusForAll.");

            if (producibles.All(p=>p.ProducibleStatus == producibleStatuses))
            {
                Result.Set(context, producibleStatuses);

                logger.Log(LogLevel.Trace,
                    "WaitForProducibleStatusForAll - Already in expected status '{1}' resuming. Producibles:[{0}] ",
                    string.Join(",", producibles.Select(p=>p).ToList()), (ProducibleStatuses)producibleStatuses);

            }
            else
            {
                /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
         of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
               var firstP = producibles.FirstOrDefault(p=>p.ProducibleStatus != producibleStatuses);
                bookmarkGuid = Guid.NewGuid();
                var bookmark = context.CreateBookmark(
                    CreateBookMarkName(firstP)
                    , BookmarkResumed);


                logger.Log(LogLevel.Trace,
                    "WaitForProducibleStatusForAll - STARTING- Waiting for status '{1}' on these Producibles:[{0}] BookmarkID:::{2}",
                    string.Join(",", producibles.Select(p => p).ToList()), (ProducibleStatuses)producibleStatuses, bookmarkGuid);

                var extension = context.GetExtension<WaitForProducibleStatusExtension>();
                extension.WaitForStatus(bookmark, firstP, producibleStatuses, logger);
            }
        }

        protected override void Cancel(NativeActivityContext context)
        {
            var extension = context.GetExtension<WaitForProducibleStatusExtension>();
            extension.DisposeSubscription(logger);
            base.Cancel(context);
        }

        private string CreateBookMarkName(IProducible producible)
        {
            return string.Format("WaitForProducibleStatusForAll-ProducibleIds:{0}-Status:{1}-Type:{2}:::{3}"
                , producible.Id, producibleStatuses.DisplayName, producible.ProducibleType.DisplayName, bookmarkGuid);
        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            if (producibles.All(p => p.ProducibleStatus == producibleStatuses))
            {
                Result.Set(context, (ProducibleStatuses)value);
                logger.Log(LogLevel.Trace,
                    "WaitForProducibleStatusForAll - COMPLETE - BookmarkName:{0}",
                    bookmark);
            }
            else
            {
                var nextProducible = producibles.FirstOrDefault(p => p.ProducibleStatus != producibleStatuses);
                var nextBookmark = context.CreateBookmark(
                    CreateBookMarkName(nextProducible)
                    , BookmarkResumed);

                logger.Log(LogLevel.Trace,
                    "WaitForProducibleStatusForAll - WaitingForNextItemInList - BookmarkName:{0}",
                    nextBookmark);

                var extension = context.GetExtension<WaitForProducibleStatusExtension>();
                extension.WaitForStatus(nextBookmark, nextProducible, producibleStatuses, logger);
            }
        }
    }

}
