﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.WorkflowActivities.Eventing
{
    /// <summary>
    /// Bookmarks the workflow and waits for a status before resuming
    /// </summary>
    public sealed class WaitForMachineStatus : NativeActivity<MachineStatuses>
    {
        [RequiredArgument]
        public InArgument<IMachine> Machine { get; set; }

        [RequiredArgument]
        public InArgument<MachineStatuses> MachineStatus { get; set; }

        [RequiredArgument]
        public InArgument<ILogger> Logger { get; set; }

        public OutArgument<MachineStatuses> ResultStatus { get; set; }

        private ILogger logger;

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForMachineStatusExtension());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            var machine = Machine.Get(context);
            var machineStatus = MachineStatus.Get(context);
            logger = Logger.Get(context);
            if (machine.CurrentStatus == machineStatus)
            {
                ResultStatus.Set(context, machine.CurrentStatus);
                logger.Log(LogLevel.Debug, "{0} Already in expected status.", machine);
                return;
            }
            /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
             of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
            var bookmark = context.CreateBookmark("WaitForMachineStatus" + machine.Alias + machineStatus.DisplayName + Guid.NewGuid(), BookmarkResumed);
            var extension = context.GetExtension<WaitForMachineStatusExtension>();
            extension.WaitForStatus(bookmark, machine, machineStatus, logger);
        }

        protected override void Cancel(NativeActivityContext context)
        {
            var extension = context.GetExtension<WaitForMachineStatusExtension>();
            extension.DisposeSubscription(logger);
            base.Cancel(context);
        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
            ResultStatus.Set(context, (MachineStatuses)value);
            var logger = Logger.Get(context);
            logger.Log(LogLevel.Debug, "Bookmark '{0}' resumed with data '{1}'.", bookmark.Name, (MachineStatuses)value);
        }
    }

    class WaitForMachineStatusExtension : PacksizeWorkflowInstanceExtension
    {
        /// <summary>
        /// Waits for the message, resumes the bookmark with the message
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="machineStatus"></param>
        /// <param name="logger"></param>
        /// <param name="machine"></param>
        public void WaitForStatus(Bookmark bookmark, IMachine machine, MachineStatuses machineStatus, ILogger logger)
        {
            var doneLock = new object();
            var done = false;

            subscription = machine.CurrentStatusChangedObservable
                .Where(ms => ms == machineStatus)
                .DurableSubscribe(ms =>
                {
                    subscription.Dispose();
                    lock (doneLock)
                    {
                        if (!done)
                        {
                            done = true;
                            var ias = instance.BeginResumeBookmark(bookmark, ms, null, null);

                            if (ias.IsCompleted)
                            {
                                var result = instance.EndResumeBookmark(ias);
                                logger.Log(LogLevel.Debug, "Machine '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                                    machine.Alias, machine.Id, ms, bookmark.Name, result);
                            }
                        }
                    }
                }, logger);


            //last minute race check
            if (machine.CurrentStatus == machineStatus)
            {
                subscription.Dispose();
                lock (doneLock)
                {
                    if (!done)
                    {
                        done = true;
                        var ias = instance.BeginResumeBookmark(bookmark, machine.CurrentStatus, null, null);

                        if (ias.IsCompleted)
                        {
                            var result = instance.EndResumeBookmark(ias);
                            logger.Log(LogLevel.Debug, "Machine '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                            machine.Alias, machine.Id, machine.CurrentStatus, bookmark.Name, result);
                        }                        
                    }
                }
            }
        }
    }
}
