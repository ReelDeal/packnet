﻿namespace PackNet.Common.WorkflowActivities
{
    using System.Activities;
    using Interfaces.Eventing;

    public sealed class EventAggregatorPublishActivity<T> : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEventAggregatorPublisher> Publisher { get; set; }
        [RequiredArgument]
        public InArgument<T> ObjectToPublish { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var publisher = Publisher.Get(context);
            var objectToPublish = ObjectToPublish.Get(context);
            publisher.Publish(objectToPublish);
        }
    }
}
