﻿using System;
using System.Collections.Generic;
using System.Activities;
using System.Linq;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Add the AllRequiredRestriction with the passed RestrictionsToAdd add to the producible.
    /// </summary>
    /// <remarks>The AllRequiredRestriction means that the machine group must satisfy all of the restrictions to be qualified to work the producible.</remarks>
    public sealed class AddBoxAndProvidePossibleTilingPartnersAsFeedback : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> AvailableProducibles { get; set; }

        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        [RequiredArgument]
        public InArgument<List<IProducible>> ItemsToProduce { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var serviceLocator = ServiceLocator.Get(context);
            var availableProducibles = AvailableProducibles.Get(context);
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>();
            var itemsToProduce = ItemsToProduce.Get(context);
            var machineGroupId = MachineGroupId.Get(context);
            var boxFirstService = serviceLocator.Locate<IBoxFirstSelectionAlgorithmService>();

            itemsToProduce.Add(producibles.First());
            var boxFirstProducible = itemsToProduce.First() as BoxFirstProducible;
            var availableTilingPartners = boxFirstProducible.PossibleTilingPartners.Where(a => a.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged).Except(itemsToProduce.Cast<BoxFirstProducible>()).Intersect(availableProducibles);
            var tileCountToUse = boxFirstService.GetTileCountToUse(machineGroupId, boxFirstProducible, availableTilingPartners);

            if (itemsToProduce.Count < tileCountToUse)
                return availableTilingPartners;

            return new List<IProducible>();
        }
    }
}