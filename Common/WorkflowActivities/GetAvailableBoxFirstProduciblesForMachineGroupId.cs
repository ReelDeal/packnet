﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

namespace PackNet.Common.WorkflowActivities
{
    using PackNet.Common.Interfaces.Enums;

    public class GetAvailableBoxFirstProduciblesForMachineGroupId : CodeActivity<IEnumerable<IProducible>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        protected override IEnumerable<IProducible> Execute(CodeActivityContext context)
        {
            var locator = ServiceLocator.Get(context);
            var machineGroupId = MachineGroupId.Get(context);
            var boxFirstService = locator.Locate<IBoxFirstSelectionAlgorithmService>();

            return boxFirstService.GetAllStagedJobsForMachineGroup(machineGroupId);
        }
    }
}
