﻿using System.Activities;
using System.Activities.Tracking;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.WorkflowTracking;

namespace PackNet.Common.WorkflowActivities
{
    /// <summary>
    /// Add the AllRequiredRestriction with the passed RestrictionsToAdd add to the producible.
    /// </summary>
    /// <remarks>The AllRequiredRestriction means that the machine group must satisfy all of the restrictions to be qualified to work the producible.</remarks>
    public sealed class LogWithFormat : CodeActivity
    {
        static ILogger logger;

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        [RequiredArgument]
        public InArgument<LogLevel> Level { get; set; }
        [RequiredArgument]
        public InArgument<string> MessageStringFormat { get; set; }
        [RequiredArgument]
        public InArgument<List<object>> Parameters { get; set; }
        //todo add named string values to pass insted of a list collection and change to native activity so we can validate that the entered number of string args is correct w/ the number or parameters passed.

        protected override void Execute(CodeActivityContext context)
        {
            var message = string.Format( MessageStringFormat.Get(context),Parameters.Get(context).ToArray());
            
            context.Track(new PacksizeTrackingRecord(context, "LogWithFormat", message));

            logger = logger ?? ServiceLocator.Get(context).Locate<ILogger>();
            logger.Log(Level.Get(context),message);
        }
    }
}