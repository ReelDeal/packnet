﻿using System.Activities;

using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Common.WorkflowActivities.Evo
{

    public sealed class AssignXValue : CodeActivity
    {
        public InArgument<ICarton> Carton { get; set; }

        public InArgument<double> Value { get; set; }

        public InArgument<string> XValueName { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            // Obtain the runtime value of the Text input argument
            var xValueName = context.GetValue(XValueName);
            var value = context.GetValue(Value);
            var carton = context.GetValue(Carton);

            if (carton.XValues.ContainsKey(xValueName))
            {
                carton.XValues[xValueName] = value;
                return;
            }

            carton.XValues.Add(xValueName, value);
        }
    }
}
