﻿using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Common.WorkflowActivities.Evo
{
    public sealed class SortLWH : CodeActivity
    {
        [RequiredArgument]
        public InArgument<ICarton> Carton { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var producible = context.GetValue(Carton);

            if (producible == null)
                return;

            var sorted = new List<double>() { producible.Length, producible.Height, producible.Width }.OrderByDescending(c => c);

            producible.Length = sorted.ElementAt(0);
            producible.Width = sorted.ElementAt(1);
            producible.Height = sorted.ElementAt(2);
        }
    }
}
