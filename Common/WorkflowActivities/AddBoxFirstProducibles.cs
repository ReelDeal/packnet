﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.SelectionAlgorithms;

namespace PackNet.Common.WorkflowActivities
{
    public class AddBoxFirstProducibles : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IEnumerable<IProducible>> Producibles { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var producibles = Producibles.Get(context).Cast<BoxFirstProducible>();
            var locator = ServiceLocator.Get(context);
            var boxFirstService = locator.Locate<IBoxFirstSelectionAlgorithmService>();

            boxFirstService.AddProducibles(producibles);
        }
    }
}
