﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Reflection;

using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;

namespace PackNet.Common.WorkflowActivities
{
    //todo: remove usages of this call instaed use executeXamlWorkflow
    
    public class InvokeWorkflow : CodeActivity
    {
        public InArgument<string> WorkflowPath { get; set; }
        public InArgument<IServiceLocator> ServiceLocator { get; set; }
        public InArgument<Dictionary<String, Object>> InArguments { get; set; }
        public InArgument<string> AssemblyName { get; set; }
        public OutArgument<Dictionary<String, Object>> Results { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var path = WorkflowPath.Get(context);
            var serviceLocator = ServiceLocator.Get(context);
            var arguments = InArguments.Get(context);
            var userNotificationService = serviceLocator.Locate<IUserNotificationService>();
            var logger = serviceLocator.Locate<ILogger>();
            var assemblyName = AssemblyName.Get(context);

            var workflowPath = DirectoryHelpers.ReplaceEnvironmentVariables(path);
            logger.Log(LogLevel.Info, "Invoking Workflow {0}", workflowPath);

            var assembly = assemblyName != null ? Assembly.Load(assemblyName) : Assembly.GetExecutingAssembly();
            
            var result = WorkflowHelper.InvokeWorkFlowAndWaitForResult(workflowPath, assembly, arguments, userNotificationService, logger);

            if (!(result is Dictionary<String, Object>))
            {
                var tempDict = new Dictionary<String, Object>(result);
                result = tempDict;
            }

            Results.Set(context, result);

        }
    }
}