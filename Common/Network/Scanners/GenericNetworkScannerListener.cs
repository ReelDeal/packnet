﻿using System;

using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Network.Scanners
{
    public class GenericNetworkScannerListener : ObservableTcpListener
    {
        public GenericNetworkScannerListener(int port, Guid machineId, IEventAggregatorPublisher publisher, ILogger logger)
            : base(port, machineId, publisher, logger, RespondToMessage)
        {
        }

        /// <summary>
        /// Responds to a network scanner when a scan is performed, if the scanner requires a response. Currently we only support the Motorola 2090 so we have the value hardcoded
        /// </summary>
        /// <param name="arg">The message received from the scanner</param>
        /// <returns>The response to send to the scanner, if necessary</returns>
        private static string RespondToMessage(SocketListenerMessage arg)
        {
            var message = arg.Message.Replace("\r", string.Empty).Replace("\n", string.Empty);
            switch (ParsePrefix(arg))
            {
                default:
                    return String.Format("1{0} received and routed#", message);
            }
        }

        private static string ParsePrefix(SocketListenerMessage message)
        {
            //We aren't using prefixes yet
            return string.Empty;
        }
    }
}