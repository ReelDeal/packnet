﻿using System;

namespace PackNet.Common.Network
{
    public class SocketListenerMessage
    {
        public SocketListenerMessage(string message, string messageSender, Guid machineId)
        {
            Message = message;
            MessageSender = messageSender;
            MachineId = machineId;
            MessageRecievedTime = DateTime.UtcNow;
        }

        public DateTime MessageRecievedTime { get; private set; }
        public Guid MachineId { get; private set; }
        public string Message { get; private set; }
        public string MessageSender { get; private set; }

        public override string ToString()
        {
            return String.Format("{0} sent message {1} at {2}. Listening on behalf of {3}", MessageSender, Message.Replace("\r", string.Empty).Replace("\n", string.Empty),
                MessageRecievedTime.ToLongTimeString(), MachineId.ToString("B"));
        }
    }
}