﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Reactive.Subjects;
using System.Text;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Networking;

namespace PackNet.Common.Network
{
    public abstract class ObservableStream<T> : IObservableStream<T>
    {
        private readonly Stream stream;
        private readonly ILogger logger;
        private readonly int receiveBufferSize;
        private readonly Subject<string> streamSubject = new Subject<string>();
        private readonly Subject<T> publicSubject = new Subject<T>();
        private readonly byte[] buffer;
        private readonly Encoding encoding;
        private readonly IDisposable subscriptionDisposable;


        protected ILogger Logger { get { return logger; } }

        /// <summary>
        /// Creates an instance of the ObservableStream class
        /// </summary>
        /// <param name="stream">The stream to read/write from</param>
        /// <param name="logger"></param>
        /// <param name="encoding">The encoding to use.  If none is specified UTF-8 will be used</param>
        /// <param name="receiveBufferSize">The size of the buffer that will be read from the stream. Setting this to large could cause performance issues because we have to copy into a smaller array if all the bytes cannot be read</param>
        protected ObservableStream(Stream stream, ILogger logger, Encoding encoding = null, int receiveBufferSize = 2048)
        {
            Contract.Requires(stream != null);
            Contract.Requires(stream.CanRead);

            this.stream = stream;
            this.logger = logger;
            this.receiveBufferSize = receiveBufferSize;
            buffer = new byte[this.receiveBufferSize];
            this.encoding = encoding ?? Encoding.UTF8;
            subscriptionDisposable = streamSubject.Subscribe(StreamMessageReceived, OnError, OnCompleted);
        }

        public abstract void StreamMessageReceived(string message);

        public virtual void Dispose()
        {
            subscriptionDisposable.Dispose();
            streamSubject.Dispose();
            stream.Dispose();
        }

        /// <summary>
        /// Writes and flushes the payload to the stream
        /// </summary>
        /// <param name="payload">What to write to the stream</param>
        public void WriteToStream(string payload)
        {
            var payloadBytes = encoding.GetBytes(payload);
            stream.Write(payloadBytes, 0, payloadBytes.Length);
            stream.Flush();
        }

        private void PublishRead(byte[] bytesRead)
        {
            var message = encoding.GetString(bytesRead);

            logger.Log(LogLevel.Debug, String.Format("Message received from stream was {0} ", message));

            streamSubject.OnNext(message);
        }

        protected void BeginRead()
        {
            try
            {
                stream.BeginRead(buffer, 0, buffer.Length, EndRead, null);
            }
            catch (Exception ex)
            {
                streamSubject.OnError(ex);
            }
        }

        private void EndRead(IAsyncResult ar)
        {
            logger.Log(LogLevel.Debug, "Receiving message from stream");

            try
            {
                int bytesRead = stream.EndRead(ar);

                if (bytesRead > 0)
                {
                    // If we don't read bufferSize bytes, copy array to smaller size array.
                    // This way, the consumer of the service always sees only full arrays of data.
                    // This could have a resource tax if stream is constantantly returning < bufferSize bytes.
                    // This will normally only happen on the last read, but if happening more, you can tune the bufferSize.
                    if (bytesRead < receiveBufferSize)
                    {
                        logger.Log(LogLevel.Debug, String.Format("Resizing buffer, Received bytes length {0} buffer size {1}", bytesRead, buffer.Length));
                        var newBuf = new byte[bytesRead];
                        Array.Copy(buffer, newBuf, bytesRead);
                        PublishRead(newBuf);
                    }
                    else
                    {
                        PublishRead(buffer);
                    }

                    //Recurse until we reach the end of the stream
                    BeginRead();
                }
                else
                {
                    streamSubject.OnCompleted();
                }
            }
            catch (Exception ex)
            {
                streamSubject.OnError(ex);
            }
        }

        public void OnNext(T value)
        {
            publicSubject.OnNext(value);
        }

        public void OnError(Exception error)
        {
            publicSubject.OnError(error);
        }

        public void OnCompleted()
        {
            publicSubject.OnCompleted();
        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            return publicSubject.Subscribe(observer);
        }
    }
}