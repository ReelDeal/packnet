using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Networking;

namespace PackNet.Common.Network
{
    public class SocketCommunicatorWithHeartBeat : SocketCommunicator, ISocketCommunicatorWithHeartBeat
    {
        private readonly int waitTimeBetweenHeartbeatsInMilliseconds;
        private readonly Func<SocketCommunicatorWithHeartBeat, bool> userDefinedIsAliveTest;
        private readonly ILogger logger;
        private readonly Subject<Tuple<bool, string>> heatbeatSubject = new Subject<Tuple<bool, string>>();
        private readonly Stopwatch lastMessageSent = new Stopwatch();
        private readonly CancellationTokenSource cts;

        /// <summary>
        /// Message is Tuple of bool,string where bool is whether we are connected.  String is a message for more information.  Probably the exception
        /// </summary>
        public IObservable<Tuple<bool, string>> HeartbeatObservable { get { return heatbeatSubject.AsObservable(); } }

        /// <summary>
        /// Create this bad boy.
        /// </summary>
        /// <param name="ipEndPoint"></param>
        /// <param name="logger"></param>
        /// <param name="sendTimeoutInMilliseconds"></param>
        /// <param name="receiveTimeoutInMilliseconds"></param>
        /// <param name="waitTimeBetweenHeartbeatsInMilliseconds"></param>
        /// <param name="userDefinedIsAliveTest">a function that you can run in addition to us just checking if the socket is alive.  If you throw and exception it will be caught.</param>
        public SocketCommunicatorWithHeartBeat(IPEndPoint ipEndPoint, ILogger logger,
            int sendTimeoutInMilliseconds = 3000,
            int receiveTimeoutInMilliseconds = 3000,
            int waitTimeBetweenHeartbeatsInMilliseconds = 2000, Func<SocketCommunicatorWithHeartBeat, bool> userDefinedIsAliveTest = null) :
            base(logger, ipEndPoint, sendTimeoutInMilliseconds, receiveTimeoutInMilliseconds)
        {
            heatbeatSubject = new Subject<Tuple<bool, string>>();
            this.waitTimeBetweenHeartbeatsInMilliseconds = waitTimeBetweenHeartbeatsInMilliseconds;
            this.logger = logger;
            this.userDefinedIsAliveTest = userDefinedIsAliveTest;

            cts = new CancellationTokenSource();
            StartKeepAlive();
        }

        public override void Disconnect()
        {
            cts.Cancel(true);
            base.Disconnect();
        }

        public override void Send(string message, int retryTimes = 5)
        {
            base.Send(message, retryTimes);
            ResetKeepAlive();
        }

        public override string ReceiveLines(int linesToRead = 1, int retryTimes = 5)
        {
            var result = base.ReceiveLines(linesToRead, retryTimes);
            ResetKeepAlive();
            return result;

        }
        public override string SendReceiveLines(string message, int linesToRead = 1, int sendRetryTimes = 5, int receiveRetryTimes = 5)
        {
            var result = base.SendReceiveLines(message, linesToRead, sendRetryTimes, receiveRetryTimes);
            ResetKeepAlive();
            return result;
        }

        private void ResetKeepAlive()
        {
            lastMessageSent.Restart();
        }

        private void StartKeepAlive()
        {
            if (disposed)
                return;

            ResetKeepAlive();
            var task = Task.Factory.StartNew(() =>
            {
                var isAlive = false;
                var error = string.Empty;
                // stay alive until told to end.
                while (!cts.IsCancellationRequested && !disposed)
                {
                    try
                    {
                        error = string.Empty;
                        Thread.Sleep(waitTimeBetweenHeartbeatsInMilliseconds);
                        if (!cts.IsCancellationRequested &&
                            lastMessageSent.ElapsedMilliseconds >= waitTimeBetweenHeartbeatsInMilliseconds)
                        {
                            if (!isAlive)
                                ReconnectSocket(); 

                            isAlive = socket.IsConnected();

                            //only call user defined keepalive code if we have an active socket.
                            if (isAlive && userDefinedIsAliveTest != null)
                                isAlive = userDefinedIsAliveTest(this);

                            ResetKeepAlive();
                        }
                    }
                    catch (SocketException e)
                    {
                        isAlive = false;

                        if (e.SocketErrorCode == SocketError.TimedOut)
                            error =
                                string.Format(
                                    "Communication to socket timed out. Ensure that server hosting {0}:{1} is turned on and firewalls are open.  LastTimeSocketWasCheckedInMS:{2}",
                                     EndPoint.Address, EndPoint.Port, lastMessageSent.ElapsedMilliseconds);
                        else
                            error = string.Format(
                                "CheckSocket exception communicating to {0}:{1} with exception:{2} and errorCode:{3}.  LastTimeSocketWasCheckedInMS:{3}",
                                 EndPoint.Address, EndPoint.Port, e.ToString(), e.ErrorCode, lastMessageSent.ElapsedMilliseconds);

                        logger.Log(LogLevel.Trace,error);
                    }
                    catch (Exception ex)
                    {
                        isAlive = false;

                        logger.LogException(LogLevel.Warning,
                            "Unhandled Exception while running StartKeepAlive in SocketCommunicatorWithHeartBeat at IPAddress:{0}, Port:{1}. LastTimeSocketWasCheckedInMS:{2}",
                            ex, EndPoint.Address, EndPoint.Port, lastMessageSent.ElapsedMilliseconds);
                    }
                    finally
                    {
                        heatbeatSubject.OnNext(new Tuple<bool, string>(isAlive, error));
                    }
                }
                logger.Log(LogLevel.Trace, "Exiting StartKeepAlive for {0}:{1}.  Last message sent:{2}", EndPoint.Address, EndPoint.Port, lastMessageSent.ElapsedMilliseconds);
            }, cts.Token);
            task.ContinueWith((t) =>
            {
                if (t.IsFaulted)
                    logger.Log(LogLevel.Error, "SocketCommunicatorWithHeartBeat for EndPoint " + EndPoint + " failed with error " + t.Exception.Message);
                logger.Log(LogLevel.Error, "SocketCommunicatorWithHeartBeat stopped for EndPoint " + EndPoint);
                StartKeepAlive();
            });
        }

        public override void ReconnectSocket()
        {
            try
            {
                base.ReconnectSocket();
            }
            catch (Exception)
            {
               logger.Log(LogLevel.Trace, "ReconnectSocket threw an exception. This is expected if it goes offline. {0}:{1}",EndPoint.Address, EndPoint.Port);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (cts != null)
            {
                cts.Cancel(false);
            }
            base.Dispose(disposing);
        }
    }
}