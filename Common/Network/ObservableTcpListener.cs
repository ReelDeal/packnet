﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Networking;

namespace PackNet.Common.Network
{
    public class ObservableTcpListener : IObservableTcpListener
    { 
        private int port;
        private readonly Guid machineId;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private readonly Func<SocketListenerMessage, string> responseToMessage;
        private TcpListener tcpListener;
        private bool bindCalled = false;
        private IDisposable disposableSubscription;

        public Func<string, string> ResponseOnConnection { get; set; }
        public Guid MachineId { get { return machineId; } }

        public ObservableTcpListener(int port, Guid machineId, IEventAggregatorPublisher publisher, ILogger logger, Func<SocketListenerMessage, string> responseToMessage = null)
        {
            this.port = port;
            this.machineId = machineId;
            this.publisher = publisher;
            this.logger = logger;
            this.responseToMessage = responseToMessage;

        }

        public void Bind()
        {
            bindCalled = true;
            logger.Log(LogLevel.Debug, String.Format("Binding to port {0}", port));
            tcpListener = new TcpListener(IPAddress.Any, port);
            logger.Log(LogLevel.Debug, string.Format("Starting TCP Listener"));
            tcpListener.Start();
        }

        public void Start()
        {
            if (!bindCalled)
                Bind();

            AcceptClient();
        }

        private void AcceptClient()
        {
            Func<IObservable<TcpClient>> accept = Observable.FromAsyncPattern<TcpClient>(tcpListener.BeginAcceptTcpClient,
                tcpListener.EndAcceptTcpClient);

            disposableSubscription = accept().Where(o => o.Connected)
                 .ObserveOn(NewThreadScheduler.Default)
                 .Subscribe(ClientConnected);
        }

        private void ClientConnected(TcpClient client)
        {
            AcceptClient();

            logger.Log(LogLevel.Debug, string.Format("Process Message called. Client {0} connected", client.Client.RemoteEndPoint));

            var cli = new ObservableTcpClient(client, machineId, logger);

            cli.Subscribe(x => HandleMessage(x, cli), x => OnError(x, cli), () => OnComplete(cli));
        }

        private void OnComplete(ObservableTcpClient cli)
        {
            logger.Log(LogLevel.Debug, String.Format("Client {0} has completed", cli.Client.Client.RemoteEndPoint));
            cli.Dispose();
        }

        private void OnError(Exception obj, ObservableTcpClient cli)
        {
            logger.Log(LogLevel.Error, String.Format("Client {0} threw exception {1}", cli.Client.Client.RemoteEndPoint, obj));
        }

        private void HandleMessage(SocketListenerMessage socketMessage, ObservableTcpClient cli)
        {
            logger.Log(LogLevel.Debug, String.Format("Received message from stream. Message: {0}", socketMessage));

            if (responseToMessage != null)
            {
                var responseMessage = responseToMessage(socketMessage);

                cli.WriteToStream(responseMessage);
            }

            publisher.Publish(socketMessage);
        }

        public void Stop()
        {
            logger.Log(LogLevel.Info, String.Format("Stop has been called"));
            //TODO: Harvest exceptions and log them
            if (disposableSubscription != null)
                disposableSubscription.Dispose();

            tcpListener.Stop();

            bindCalled = false;
        }

        public void Dispose()
        {
            disposableSubscription.Dispose();
            tcpListener.Stop();
            tcpListener = null;
        }

        public int Port
        {
            get { return port; }
            set { port = value; }
        }
    }
}