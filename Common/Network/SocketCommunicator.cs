﻿using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Networking;
using PackNet.Common.Utils;
using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace PackNet.Common.Network
{
    public class SocketCommunicator : ISocketCommunicator
    {
        protected Socket socket;
        private NetworkStream stream;
        private StreamWriter StreamWriter;
        private StreamReader StreamReader;
        protected bool disposed;
        private readonly ILogger logger;
        private readonly int sendTimeout;
        private readonly int receiveTimeout;
        private volatile object locker = new object();
        
        public IPEndPoint EndPoint { get; private set; }

        /// <summary>
        /// String-based wrapper over a System.Net.Sockets.Socket. Used to send and receive strings to the underlying socket.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="ipEndPoint">IP EndPoint and port to connect the socket to</param>
        /// <param name="sendTimeoutInMilliseconds">Send timeout in milliseconds</param>
        /// <param name="receiveTimeoutInMilliseconds">Receive timeout in milliseconds</param>
        public SocketCommunicator(ILogger logger, IPEndPoint ipEndPoint, int sendTimeoutInMilliseconds = 3000,
            int receiveTimeoutInMilliseconds = 3000)
        {
            EndPoint = ipEndPoint;
            this.logger = logger;
            sendTimeout = sendTimeoutInMilliseconds;
            receiveTimeout = receiveTimeoutInMilliseconds;
        }

        /// <summary>
        /// Connect to the underlying socket and establishes a stream writer and stream reader for communication
        /// Connect from experience is taking about 20 seconds to return when the socket is not available.
        /// </summary>
        protected void InitializeSocketCommunication()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp)
            {
                ReceiveTimeout = receiveTimeout,
                SendTimeout = sendTimeout,
                NoDelay = true,
                ExclusiveAddressUse = true
            };
            logger.Log(LogLevel.Trace, EndPoint + " connecting to socket");
            socket.Connect(EndPoint);
            logger.Log(LogLevel.Trace, EndPoint + " connected to socket");
            stream = new NetworkStream(socket);
            StreamWriter = new StreamWriter(stream);
            StreamReader = new StreamReader(stream);
        }

        /// <summary>
        /// Sends message to the connected client.  If the communication lines are not open we try to open them here.
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="retryTimes"></param>
        /// <exception cref="System.AggregateException">Thrown if the socket is faulted. If you want auto-reconnect use CTOR default parameter</exception>
        public virtual void Send(string message, int retryTimes = 5)
        {
            Retry.Do(() =>
            {
                logger.Log(LogLevel.Trace, EndPoint + " Wire level sending " + message + " over socket with retry count " + retryTimes);
                InternalSendMessage(message);
                logger.Log(LogLevel.Trace, EndPoint + " Wire level sent " + message + " over socket");
            }, TimeSpan.FromMilliseconds(20), retryTimes);
        }

        /// <summary>
        /// Internals the send message.
        /// </summary>
        /// <param name="message">The message.</param>
        protected virtual void InternalSendMessage(string message)
        {
            try
            {
                lock (locker)
                {
                    if (!IsConnected())
                    {
                        ReconnectSocket();
                    }

                    StreamWriter.Write(message);
                    StreamWriter.Flush();
                    logger.Log(LogLevel.Trace, "SocketCommunicator sent message '{0}'", message);
                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Debug, EndPoint + " Failed to send message " + message +" on socket:" + e);
                //TODO: don't disconnect on every error. If the socket is connected why reconnect? Error could be related to dirty networks
                socket.Disconnect(true);
                throw;
            }

        }

        /// <summary>
        /// Receives message from the client, reading one line at a time
        /// </summary>
        /// <param name="linesToRead">Optional parameter setting the number of lines to read</param>
        /// <param name="retryTimes"></param>
        /// <returns>String representation of the bytes read from the client</returns>
        public virtual string ReceiveLines(int linesToRead = 1, int retryTimes = 5)
        {
            var stringBuilder = new StringBuilder();

            lock (locker)
            {
                try
                {
                    Contract.Requires(linesToRead > 0);

                    if (!IsConnected())
                    {
                        logger.Log(LogLevel.Trace, EndPoint + " not connected... reconnecting");
                        ReconnectSocket();
                    }

                    logger.Log(LogLevel.Trace, EndPoint + " reading " + linesToRead + " from socket");
                    for (var i = 0; i < linesToRead; i++)
                    {
                        Retry.Do(() =>
                        {
                            var line = StreamReader.ReadLine();
                            logger.Log(LogLevel.Trace, EndPoint + " read " + line + " from socket");
                            stringBuilder.Append(line);
                        }, retryTimes);
                    }

                    var result = stringBuilder.ToString();
                    logger.Log(LogLevel.Trace, EndPoint + "SocketCommunicator received message '{0}'", result);
                    return result;
                }
                catch (Exception e)
                {
                    logger.Log(LogLevel.Debug, EndPoint + "Failed to read socket:" + e);
                    //TODO: don't disconnect on every error. If the socket is connected why reconnect? Error could be related to dirty networks
                    socket.Disconnect(true);
                    throw;
                }
            }
        }

        /// <summary>
        /// Receives the until delimiter.
        /// </summary>
        /// <param name="delimiter">The delimiter.</param>
        /// <param name="retryTimes">The retry times.</param>
        /// <returns></returns>
        public string ReceiveUntilDelimiter(string delimiter, int retryTimes = 5)
        {
            var stringBuilder = new StringBuilder();
            try
            {
                lock (locker)
                {
                    if (!IsConnected())
                    {
                        ReconnectSocket();
                    }

                    Retry.Do(() =>
                    {
                        stringBuilder.Clear();
                        logger.Log(LogLevel.Trace, EndPoint + " reading until '" + delimiter + "' from socket");
                        stringBuilder.Append(StreamReader.ReadUntil(delimiter));
                        stringBuilder.Append(delimiter); //Add delimeter so you have the entire string
                    }, retryTimes);

                    var result = stringBuilder.ToString();
                    logger.Log(LogLevel.Trace, EndPoint + " SocketCommunicator received message '{0}'", result);
                    return result;
                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Debug, EndPoint + " Failed to read socket: " + e);
                logger.Log(LogLevel.Debug, EndPoint + " Read contents: " + stringBuilder.ToString());
                //TODO: don't disconnect on every error. If the socket is connected why reconnect? Error could be related to dirty networks
                socket.Disconnect(true);
                throw;
            }
        }

        /// <summary>
        /// Receives the until delimiter.
        /// </summary>
        /// <param name="delimiter">The delimiter.</param>
        /// <param name="retryTimes">The retry times.</param>
        /// <returns></returns>
        public string ReceiveUntilDelimiter(char delimiter, int retryTimes = 5)
        {
            return ReceiveUntilDelimiter(delimiter.ToString(), retryTimes);
        }

        /// <summary>
        /// A more in-depth check of if the socket is really connected and available.
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            try
            {
                return socket != null && socket.Connected;
            }
            catch (SocketException)
            {
                return false;
            }
        }

        /// <summary>
        /// Sends message and receives response from client
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="linesToRead">Optional parameter setting the number of lines to read</param>
        /// <param name="sendRetryTimes">The send retry times.</param>
        /// <param name="receiveRetryTimes">The receive retry times.</param>
        /// <returns>
        /// String representation of the bytes read from the client
        /// </returns>
        public virtual string SendReceiveLines(string message, int linesToRead = 1, int sendRetryTimes = 5, int receiveRetryTimes = 5)
        {
            Contract.Requires(linesToRead > 0);
            Contract.Requires(sendRetryTimes > 0);
            Contract.Requires(receiveRetryTimes > 0);
            Contract.Requires(message != null);
            Contract.Requires(message.Length > 0);

                lock (locker)
                {
                    Send(message, sendRetryTimes);
                    return ReceiveLines(linesToRead, receiveRetryTimes);
                }
        }

        /// <summary>
        /// Sends the receive until delimiter.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="delimiter">The delimiter.</param>
        /// <param name="sendRetryTimes">The send retry times.</param>
        /// <param name="receiveRetryTimes">The receive retry times.</param>
        /// <returns></returns>
        public string SendReceiveUntilDelimiter(string message, string delimiter, int sendRetryTimes = 5, int receiveRetryTimes = 5)
        {
            Contract.Requires(sendRetryTimes > 0);
            Contract.Requires(receiveRetryTimes > 0);
            Contract.Requires(message != null);
            Contract.Requires(message.Length > 0);
            Contract.Requires(delimiter != null);
            Contract.Requires(delimiter.Length > 0);

            lock (locker)
            {
                Send(message, sendRetryTimes);
                return ReceiveUntilDelimiter(delimiter, receiveRetryTimes);
            }
        }

        /// <summary>
        /// Sends the receive until delimiter.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="delimiter">The delimiter.</param>
        /// <param name="sendRetryTimes">The send retry times.</param>
        /// <param name="receiveRetryTimes">The receive retry times.</param>
        /// <returns></returns>
        public string SendReceiveUntilDelimiter(string message, char delimiter, int sendRetryTimes = 5, int receiveRetryTimes = 5)
        {
            return SendReceiveUntilDelimiter(message, delimiter.ToString(), sendRetryTimes, receiveRetryTimes);
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="SocketCommunicator"/> class.
        /// </summary>
        ~SocketCommunicator()
        {
            Dispose(false);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                DisposeOfUnmanagedResources();
            }

            disposed = true;
        }

        /// <summary>
        /// Flushes and disposes the underlying streams, closes the socket then creates a new instance of the socket
        /// </summary>
        public virtual void ReconnectSocket()
        {
            // Clean up all socket resources
            DisposeOfUnmanagedResources();

            InitializeSocketCommunication();
        }

        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        public virtual void Disconnect()
        {
            if (socket != null)
                socket.Disconnect(true);
        }

        /// <summary>
        /// Disposes of unmanaged resources.
        /// </summary>
        private void DisposeOfUnmanagedResources()
        {
            if (StreamWriter != null)
            {
                StreamWriter.Dispose();
            }

            if (StreamReader != null)
            {
                StreamReader.Dispose();
            }

            if (stream != null)
            {
                stream.Dispose();
            }

            if (socket != null)
            {
                socket.Dispose();
            }
        }
    }
}

