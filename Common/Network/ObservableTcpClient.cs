﻿using System;
using System.Net.Sockets;

using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Network
{
    public class ObservableTcpClient : ObservableStream<SocketListenerMessage>
    {
        private readonly TcpClient client;
        private readonly Guid machineId;


        public TcpClient Client { get { return client; } }

        public ObservableTcpClient(TcpClient client, Guid machineId, ILogger logger)
            : base(client.GetStream(), logger, receiveBufferSize: client.ReceiveBufferSize)
        {
            this.client = client;
            this.machineId = machineId;
            BeginRead();
            
        }

        /// <summary>
        /// This method is called when we received a message from a network scanner
        /// </summary>
        /// <param name="message"></param>
        public override void StreamMessageReceived(string message)
        {
            var remoteIp = client.Client.RemoteEndPoint.ToString();
            Logger.Log(LogLevel.Info, string.Format("Received message {0} from ipaddress {1}", message, remoteIp));
            OnNext(new SocketListenerMessage(message, remoteIp, machineId));
        }
    }
}