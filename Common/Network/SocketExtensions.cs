﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;

namespace PackNet.Common.Network
{
    public static class SocketExtensions
    {
        public static String ReadUntil(this StreamReader sr, String delim)
        {
            var sb = new StringBuilder();
            bool found = false;

            while (!found && !sr.EndOfStream)
            {
                for (int i = 0; i < delim.Length; i++)
                {
                    var c = (char)sr.Read();
                    sb.Append(c);

                    if (c != delim[i])
                        break;

                    if (i == delim.Length - 1)
                    {
                        sb.Remove(sb.Length - delim.Length, delim.Length);
                        found = true;
                    }
                }
            }

            return sb.ToString();
        }

        public static bool IsConnected(this Socket socket)
        {
            if (socket == null)
            {
                Debug.WriteLine("socket null");
                return false;
            }

            if (!socket.Connected)
            {
                return false;
            }

            // This is how you can determine whether a socket is still connected. 
            bool blockingState = socket.Blocking;

            try
            {
                socket.Blocking = false;
                socket.Send(new byte[1], 0, 0);
                return true;
            }
            catch (SocketException e)
            {
                // 10035 == WSAEWOULDBLOCK 
                Trace.WriteLine(e.NativeErrorCode.Equals(10035)
                    ? "Still Connected, but the Send would block"
                    : string.Format("Disconnected: error code {0}!", e.NativeErrorCode));

                return false;
            }
            finally
            {
                socket.Blocking = blockingState;
            }
        }
    }
}
