﻿using System;
using System.IO;

namespace PackNet.Common.Plugins
{
    using System.ComponentModel.Composition.Hosting;

    /// <summary>
    /// Provides a quick and easy way to use MEF to load plugins. Derive a class that specifies import attributes and call compose
    /// </summary>
    public static class MefComposer
    {
        /// <summary>
        /// Compose the catalog with the current assembly and a plugin directory
        /// </summary>
        /// <param name="pluginPath">Full path to the plugin directory</param>
        /// <param name="batch">The composition batch that contains any needed imports</param>
        /// <param name="aggregateCatalog"></param>
        /// <param name="recursivelyAdd"></param>
        public static void Compose(string pluginPath, CompositionBatch batch, AggregateCatalog aggregateCatalog,
            bool recursivelyAdd = true)
        {
            if (!Directory.Exists(pluginPath))
                Directory.CreateDirectory(pluginPath);

            if (recursivelyAdd)
            {
                AddDirectoryToCatalog(new DirectoryInfo(pluginPath), aggregateCatalog);
            }
            else
            {
                var directoryCatalog = new DirectoryCatalog(pluginPath);
                aggregateCatalog.Catalogs.Add(directoryCatalog);
            }

            var container = new CompositionContainer(aggregateCatalog);
            container.Compose(batch);
        }

        private static void AddDirectoryToCatalog(DirectoryInfo di, AggregateCatalog catalog)
        {
            var dirs = di.GetDirectories();

            foreach (var directoryInfo in dirs)
            {
                AddDirectoryToCatalog(directoryInfo, catalog);
            }
            AppDomain.CurrentDomain.AppendPrivatePath(di.FullName);
            catalog.Catalogs.Add(new DirectoryCatalog(di.FullName));
        }
    }
}