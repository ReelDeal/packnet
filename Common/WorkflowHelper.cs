﻿using System;
using System.Activities;
using System.Activities.Expressions;
using System.Activities.Tracking;
using System.Activities.XamlIntegration;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xaml;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;

namespace PackNet.Common
{
    public static class WorkflowHelper
    {
        private static readonly ConcurrentDictionary<string, Activity> CachedActivities = new ConcurrentDictionary<string, Activity>();

        public static void PurgeActivityCache()
        {
            CachedActivities.Clear();
        }

        private static Activity CheckCacheForActivity(string path)
        {
            Activity cachedActivity = null;
            if (CachedActivities.ContainsKey(path))
            {
                CachedActivities.TryGetValue(path, out cachedActivity);
            }

            return cachedActivity;
        }

        /// <summary>
        /// Asynchronously starts a work flow
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="workflow">This can be a path with Environment variables present or a fully qualified path.</param>
        /// <param name="assembly"></param>
        /// <param name="inArgs"></param>
        /// <param name="userNotificationService"></param>
        /// <param name="workflowLifetime"></param>
        /// <returns></returns>
        public static void InvokeWorkFlow(ILogger logger, string workflow, Assembly assembly, Dictionary<string, object> inArgs, IUserNotificationService userNotificationService, WorkflowLifetime workflowLifetime = null)
        {
            Activity activity = null;
            var workflowPath = DirectoryHelpers.ReplaceEnvironmentVariables(workflow);
            try
            {
                activity = GetActivity(workflowPath, assembly);
            }
            catch (FileNotFoundException e)
            {
                PublishAndLogWorkflowError(workflowPath, userNotificationService, logger, e);
                return;
            }
            catch (InvalidWorkflowException e)
            {
                PublishAndLogWorkflowError(workflowPath, userNotificationService, logger, e);
                return;
            }

            //todo figure out if we should know if this has been called before.
            var stopwatch = new Stopwatch();

            // If workflow is Aborted, Canceled or Faulted then OnError is published, which halts the sequence,
            // so it should be treated as a fatal error by handlers
            stopwatch.Start();
            var wfApp = new WorkflowApplication(activity, inArgs)
            {
                Aborted = e =>
                {
                    logger.Log(LogLevel.Error, "Workflow:{0} unexpectedly aborted:{1}", ((DynamicActivity)activity).Name, e.Reason);

                    if (workflowLifetime != null)
                        workflowLifetime.StopTracking();
                },
                Completed = e =>
                {
                    stopwatch.Stop();
                    switch (e.CompletionState)
                    {
                        case ActivityInstanceState.Canceled:
                            logger.Log(LogLevel.Error, "Workflow:{0} took {2}(ms) unexpectedly canceled:{1}", ((DynamicActivity)activity).Name,
                                e.TerminationException, stopwatch.ElapsedMilliseconds);
                            break;
                        case ActivityInstanceState.Faulted:
                            logger.Log(LogLevel.Error, "Workflow:{0} took {2}(ms) unexpectedly faulted:{1}", ((DynamicActivity)activity).Name,
                                e.TerminationException, stopwatch.ElapsedMilliseconds);
                            break;
                        case ActivityInstanceState.Closed:
                            logger.Log(LogLevel.Trace, "Completed workflow:{0} took {1}(ms)", new[] { ((DynamicActivity)activity).Name }, stopwatch.ElapsedMilliseconds);
                            break;
                    }

                    if (workflowLifetime != null)
                        workflowLifetime.StopTracking();
                },
                OnUnhandledException = (WorkflowApplicationUnhandledExceptionEventArgs e) =>
                    {
                        var message = string.Format("Workflow:{0} OnUnhandledException:{1} ExceptionSource:{2}",
                            ((DynamicActivity)activity).Name, e.UnhandledException.Message, e.ExceptionSource.DisplayName);
                        logger.Log(LogLevel.Error, message);

                        if (workflowLifetime != null)
                            workflowLifetime.StopTracking();
                        // Instruct the runtime to terminate the workflow.
                        return UnhandledExceptionAction.Cancel;

                        // Other choices are UnhandledExceptionAction.Abort and 
                        // UnhandledExceptionAction.Cancel
                    }
            };

            try
            {
                if (workflowLifetime != null)
                    workflowLifetime.StartTracking();
                wfApp.Run();
            }
            catch (InvalidWorkflowException e)
            {
                PublishAndLogWorkflowError(workflowPath, userNotificationService, logger, e);
                if (workflowLifetime != null)
                    workflowLifetime.StopTracking();
                throw;
            }

            logger.Log(LogLevel.Trace, "Started workflow:{0}", new[] { ((DynamicActivity)activity).Name });
        }

        /// <summary>
        /// Invokes a workflow and waits for a return value
        /// </summary>
        /// <param name="workflowPath"></param>
        /// <param name="assembly"></param>
        /// <param name="inArgs"></param>
        /// <param name="userNotificationService"></param>
        /// <param name="logger"></param>
        /// <param name="workflowLifetime"></param>
        /// <returns></returns>
        public static IDictionary<string, object> InvokeWorkFlowAndWaitForResult(string workflowPath, Assembly assembly, Dictionary<string, object> inArgs, IUserNotificationService userNotificationService, ILogger logger, WorkflowLifetime workflowLifetime = null)
        {
            Activity activity = null;
            var path = DirectoryHelpers.ReplaceEnvironmentVariables(workflowPath);

            var stopWatch = Stopwatch.StartNew();
            try
            {
                activity = GetActivity(path, assembly);
            }
            catch (FileNotFoundException e)
            {
                PublishAndLogWorkflowError(workflowPath, userNotificationService, logger, e);
                return null;
            }
            catch (InvalidWorkflowException e)
            {
                PublishAndLogWorkflowError(workflowPath, userNotificationService, logger, e);
                return null;
            }

            var invoker = new WorkflowInvoker(activity);

            logger.Log(LogLevel.Debug, string.Format("InvokeWorkFlowAndWaitForResult Starting Workflow {0}", path));
            
            IDictionary<string, object> result;
            try
            {
                if (workflowLifetime != null)
                    workflowLifetime.StartTracking();

                result = invoker.Invoke(inArgs);
            }
            catch (InvalidWorkflowException e)
            {
                PublishAndLogWorkflowError(workflowPath, userNotificationService, logger, e);
                return null;
            }
            finally
            {
                if (workflowLifetime != null)
                    workflowLifetime.StopTracking();
            }

            stopWatch.Stop();
            logger.Log(LogLevel.Debug, string.Format("InvokeWorkFlowAndWaitForResult for Workflow {0} took " + stopWatch.ElapsedMilliseconds + "(ms)", path));
            return result;
        }

        public static void PublishAndLogWorkflowError(string workflowPath, IUserNotificationService userNotificationService, ILogger logger,
            Exception e)
        {
            var sb = new StringBuilder();

            sb.Append(String.Format("Failed to run XAML File '{0}'. Working from: {1}. Message: {2}", workflowPath,
                Directory.GetCurrentDirectory(), e.Message));

            userNotificationService.SendNotificationToSystem(NotificationSeverity.Error, sb.ToString(), e.ToString());

            sb.Append(string.Format(" Stack trace: {0}", e.StackTrace));

            logger.Log(LogLevel.Error, sb.ToString);
        }

        /// <summary>
        /// Find the workflow activity and compile it.
        /// </summary>
        /// <param name="workflowPath">This should be a real path to a file.</param>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static Activity GetActivity(string workflowPath, Assembly assembly)
        {
            Activity activity = null;

            activity = CheckCacheForActivity(workflowPath);

            if (activity != null)
            {
                return activity;
            }

            var settings = new XamlXmlReaderSettings
            {
                LocalAssembly = assembly,
            };

            var reader = new XamlXmlReader(workflowPath, settings);

            var activitySettings = new ActivityXamlServicesSettings
            {
                CompileExpressions = true
            };


            activity = ActivityXamlServices.Load(reader, activitySettings);
            CompileExpressions(activity);


            CachedActivities.TryAdd(workflowPath, activity);

            return activity;
        }

        public static void CompileExpressions(DynamicActivity dynamicActivity)
        {
            // activityName is the Namespace.Type of the activity that contains the
            // C# expressions. For Dynamic Activities this can be retrieved using the
            // name property , which must be in the form Namespace.Type.
            string activityName = dynamicActivity.Name;

            // Split activityName into Namespace and Type.Append _CompiledExpressionRoot to the type name
            // to represent the new type that represents the compiled expressions.
            // Take everything after the last . for the type name.
            string activityType = activityName.Split('.').Last() + "_CompiledExpressionRoot";
            // Take everything before the last . for the namespace.
            string activityNamespace = string.Join(".", activityName.Split('.').Reverse().Skip(1).Reverse());

            // Create a TextExpressionCompilerSettings.
            TextExpressionCompilerSettings settings = new TextExpressionCompilerSettings
            {
                Activity = dynamicActivity,
                Language = "C#",
                ActivityName = activityType,
                ActivityNamespace = activityNamespace,
                RootNamespace = null,
                GenerateAsPartialClass = false,
                AlwaysGenerateSource = true,
                ForImplementation = true
            };

            // Compile the C# expression.
            TextExpressionCompilerResults results =
                new TextExpressionCompiler(settings).Compile();

            // Any compilation errors are contained in the CompilerMessages.
            if (results.HasErrors)
            {
                throw new Exception("Compilation failed.");
            }

            // Create an instance of the new compiled expression type.
            ICompiledExpressionRoot compiledExpressionRoot =
                Activator.CreateInstance(results.ResultType,
                    new object[] { dynamicActivity }) as ICompiledExpressionRoot;

            // Attach it to the activity.
            CompiledExpressionInvoker.SetCompiledExpressionRootForImplementation(
                dynamicActivity, compiledExpressionRoot);
        }

        /// <summary>
        /// Found code on msdn when the build server was filing to invoke workflows that have code snippets in it... 
        /// https://msdn.microsoft.com/en-us/library/29110be7-f4e3-407e-8dbe-78102eb21115(v=vs.110)#CodeWorkflows
        /// Call this before invoking all our worklfows.
        /// </summary>
        /// <param name="activity"></param>
        public static void CompileExpressions(Activity activity)
        {
            // activityName is the Namespace.Type of the activity that contains the
            // C# expressions.
            string activityName = activity.GetType().ToString();

            // Split activityName into Namespace and Type.Append _CompiledExpressionRoot to the type name
            // to represent the new type that represents the compiled expressions.
            // Take everything after the last . for the type name.
            string activityType = activityName.Split('.').Last() + "_CompiledExpressionRoot";
            // Take everything before the last . for the namespace.
            string activityNamespace = string.Join(".", activityName.Split('.').Reverse().Skip(1).Reverse());

            // Create a TextExpressionCompilerSettings.
            var settings = new TextExpressionCompilerSettings
            {
                Activity = activity,
                Language = "C#",
                ActivityName = activityType,
                ActivityNamespace = activityNamespace,
                RootNamespace = null,
                GenerateAsPartialClass = false,
                AlwaysGenerateSource = true,
                ForImplementation = false
            };

            // Compile the C# expression.
            var results = new TextExpressionCompiler(settings).Compile();

            // Any compilation errors are contained in the CompilerMessages.
            if (results.HasErrors)
            {
                throw new Exception("Compilation failed.");
            }

            // Create an instance of the new compiled expression type.
            var compiledExpressionRoot =
                Activator.CreateInstance(results.ResultType, new object[] { activity }) as ICompiledExpressionRoot;

            // Attach it to the activity.
            CompiledExpressionInvoker.SetCompiledExpressionRoot(
                activity, compiledExpressionRoot);
        }
    }
}