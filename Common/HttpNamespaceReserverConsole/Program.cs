﻿using System;
using System.Linq;
using PackNet.Common.HttpNamespaceReserver;
using PackNet.Common.HttpNamespaceReserver.Facades;
using PackNet.Common.HttpNamespaceReserver.CommandsAndQueries;

namespace PackNet.Common.HttpNamespaceReserverConsole
{
    public class HttpNamespaceReserver
    {
        static void Main(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine(
                    "Usage: reserveHttpNamespace " +
                    "prefix account" + "Add/Remove");
                return;
            }
            try
            {
                var reserver = new NamespaceReserver(new CommandFactory(new HttpApiFacade()));
                var before = reserver.GetAclEntries();
                var isRemove = args[2] == "Remove";
                if(isRemove)
                {
                    reserver.RemoveNamespaceReservation(args[0], args[1]);
                    Console.WriteLine("Success!");
                    var after = reserver.GetAclEntries();
                    var removed = before.Except(after);
                    Console.WriteLine("Removed values:");
                    foreach (var keyValuePair in removed)
                    {
                        Console.WriteLine("KEY: " + keyValuePair.Key + " VALUE: " + keyValuePair.Value);
                    }
                }
                else
                {
                    reserver.ReserveNamespace(args[0], args[1]);
                    Console.WriteLine("Success!");
                    var after = reserver.GetAclEntries();
                    var added = after.Except(before);
                    Console.WriteLine("Added values:");
                    foreach (var keyValuePair in added)
                    {
                        Console.WriteLine("KEY: " + keyValuePair.Key + " VALUE: " + keyValuePair.Value);
                    }
                }

            }
            catch (Exception x)
            {
                Console.WriteLine(x.Message);
            }
        }
    }
}
