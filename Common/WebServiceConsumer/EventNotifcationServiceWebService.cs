﻿namespace PackNet.Common.WebServiceConsumer
{
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading;
    using System.Web.Services;
    using System.Web.Services.Description;
    using System.Web.Services.Protocols;
    using System.Xml;
    using System.Xml.Serialization;

    /// <remarks/>
    [GeneratedCode("wsdl", "2.0.50727.3038")]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    [WebServiceBinding(Name = "EventNotifcationServiceWebServiceSoap", Namespace = "http://localhost")]
    public partial class EventNotifcationServiceWebService : SoapHttpClientProtocol
    {

        private SendOrPostCallback SendEventNotificationOperationCompleted;

        /// <remarks/>
        public EventNotifcationServiceWebService()
        {
            Url = "http://localhost/EventNotifcationService.asmx";
        }

        /// <remarks/>
        public event SendEventNotificationCompletedEventHandler SendEventNotificationCompleted;

        /// <remarks/>
        [SoapDocumentMethod("SendEventNotificaton", RequestNamespace = "http://localhost", ResponseNamespace = "http://localhost", Use = SoapBindingUse.Literal, ParameterStyle = SoapParameterStyle.Bare)]
        [return: XmlText()]
        [return: XmlAnyElement()]
        public XmlNode[] SendEventNotification([XmlText()] [XmlAnyElement()] XmlNode[] input)
        {
            object[] results = Invoke("SendEventNotification", new object[] {
                        input});
            return ((XmlNode[])(results[0]));
        }

        /// <remarks/>
        public IAsyncResult BeginSendEventNotification(XmlNode[] input, AsyncCallback callback, object asyncState)
        {
            return BeginInvoke("SendEventNotification", new object[] {
                        input}, callback, asyncState);
        }

        /// <remarks/>
        public XmlNode[] EndSendEventNotification(IAsyncResult asyncResult)
        {
            object[] results = EndInvoke(asyncResult);
            return ((XmlNode[])(results[0]));
        }

        /// <remarks/>
        public void SendEventNotificationAsync(XmlNode[] input)
        {
            SendEventNotificationAsync(input, null);
        }

        /// <remarks/>
        public void SendEventNotificationAsync(XmlNode[] input, object userState)
        {
            if ((SendEventNotificationOperationCompleted == null))
            {
                SendEventNotificationOperationCompleted = new SendOrPostCallback(OnSendEventNotificationOperationCompleted);
            }
            InvokeAsync("SendEventNotification", new object[] {
                        input}, SendEventNotificationOperationCompleted, userState);
        }

        private void OnSendEventNotificationOperationCompleted(object arg)
        {
            if ((SendEventNotificationCompleted != null))
            {
                InvokeCompletedEventArgs invokeArgs = ((InvokeCompletedEventArgs)(arg));
                SendEventNotificationCompleted(this, new SendEventNotificationCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }

        /// <remarks/>
        public new void CancelAsync(object userState)
        {
            base.CancelAsync(userState);
        }
    }

    /// <remarks/>
    [GeneratedCode("wsdl", "2.0.50727.3038")]
    public delegate void SendEventNotificationCompletedEventHandler(object sender, SendEventNotificationCompletedEventArgs e);

    /// <remarks/>
    [GeneratedCode("wsdl", "2.0.50727.3038")]
    [DebuggerStepThrough()]
    [DesignerCategory("code")]
    public partial class SendEventNotificationCompletedEventArgs : AsyncCompletedEventArgs
    {

        private object[] results;

        internal SendEventNotificationCompletedEventArgs(object[] results, Exception exception, bool cancelled, object userState) :
            base(exception, cancelled, userState)
        {
            this.results = results;
        }

        /// <remarks/>
        public XmlNode[] Result
        {
            get
            {
                RaiseExceptionIfNecessary();
                return ((XmlNode[])(results[0]));
            }
        }
    }
}
