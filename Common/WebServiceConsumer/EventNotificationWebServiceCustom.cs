﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace PackNet.Common.WebServiceConsumer
{
    public partial class EventNotifcationServiceWebService : IEventNotificationConsumer
    {
        public EventNotifcationServiceWebService(string url)
        {
            Url = url;
        }

        public string SendEventNotification(string message)
        {
            XmlNode[] reply = SendEventNotification(CreateRequest(message));
            return ResolveAnswer(reply);
        }

        private XmlNode[] CreateRequest(string message)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(String.Format("<SendEventNotification><message>{0}</message></SendEventNotification>", message));
            return CreateNodeList(doc);
        }

        private XmlNode[] CreateNodeList(XmlDocument doc)
        {
            List<XmlNode> lst = new List<XmlNode>();
            foreach (XmlNode node in doc.SelectNodes("/"))
            {
                lst.Add(node);
            }

            return lst.ToArray();
        }

        private string ResolveAnswer(XmlNode[] reply)
        {
            if (reply.ElementAt(0).HasChildNodes)
            {
                return reply.ElementAt(0).FirstChild.InnerText;
            }
            
            return String.Empty;
        }
    }
}
