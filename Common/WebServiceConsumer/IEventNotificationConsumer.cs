﻿using System.Xml;

namespace PackNet.Common.WebServiceConsumer
{
    public interface IEventNotificationConsumer
    {
        XmlNode[] SendEventNotification(XmlNode[] input);

        string SendEventNotification(string message);
    }
}