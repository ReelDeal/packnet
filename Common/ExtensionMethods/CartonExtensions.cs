﻿using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace PackNet.Common.ExtensionMethods
{
    using Interfaces.DTO;
    using Interfaces.DTO.Carton;

    public static class CartonExtensions
    {
        /// <summary>
        /// Assigns Article properties to an existing Carton request
        /// </summary>
        /// <param name="carton"></param>
        /// <param name="article"></param>
        /// <returns></returns>
        public static void ApplyArticle(this ICarton carton, Article article)
        {
            carton.Width = article.Width;
            carton.Length = article.Length;
            carton.Height = article.Height;
            carton.CorrugateQuality = article.CorrugateQuality;
            carton.DesignId = article.DesignId;
            carton.ArticleId = article.ArticleId;
            // TODO: Make sure this is tested
            if (article.XValues != null)
            {
                article.XValues.ForEach(kv => carton.XValues.Add(kv.Key, kv.Value));
            }
        }

        public static DesignFormulaParameters GetDesignFormulaParameters(this ICarton carton, Corrugate corrugate)
        {
            var dpf = new DesignFormulaParameters
            {
                CartonHeight = carton.Height,
                CartonLength = carton.Length,
                CartonWidth = carton.Width,
                ZfoldThickness = corrugate.Thickness,
                ZfoldWidth = corrugate.Width
            };

            carton.XValues.ForEach(x => dpf.XValues.Add(x.Key, x.Value));

            return dpf;
        }

    }
}
