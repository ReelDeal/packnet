﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PackNet.Common.ExtensionMethods
{
    using System;
    using System.Globalization;

    public static class ObjectExtensions
    {
        public static IDictionary<string, string> AsDictionary(this object source,
            BindingFlags bindingAttr = BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary
                (
                    propInfo => propInfo.Name,
                    propInfo => propInfo.GetValue(source, null) == null ? null : Convert.ToString(propInfo.GetValue(source, null), CultureInfo.InvariantCulture)

                );
        }
    }
}