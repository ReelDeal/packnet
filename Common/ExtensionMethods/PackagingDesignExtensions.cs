﻿using System.Linq;

using PackNet.Common.Interfaces;

namespace PackNet.Common.ExtensionMethods
{
    using System;
    using System.Collections.Generic;

    using ComponentModel.FormulaEvaluator;
    using Interfaces.DTO;
    using Interfaces.DTO.PackagingDesigns;

    public static class PackagingDesignExtensions
    {
        public static Func<string, IDictionary<string, object>, MicroMeter> GetWidthOnZFoldFactory = (a, b) => FormulaEvaluator.Solve<MicroMeter>(a, b);
        public static MicroMeter GetWidthOnZfold(this IPackagingDesign design, DesignFormulaParameters p)
        {

            try
            {
                return GetWidthOnZFoldFactory(
                    design.WidthOnZFold,
                    design.PopulateParameters(p));
            }
            catch (Exception e)
            {
                throw new ApplicationException(string.Format("ZFold width evaluation failed for DesignId:{2} formula:'{0}' with message: {1}", design.WidthOnZFold, e.Message, design.Id));
            }
        }

        public static Func<string, IDictionary<string, object>, MicroMeter> GetLengthOnZFoldFactory = (a, b) => FormulaEvaluator.Solve<MicroMeter>(a, b);

        public static MicroMeter GetLengthOnZfold(this IPackagingDesign design, DesignFormulaParameters p)
        {
            try
            {
                return GetLengthOnZFoldFactory(design.LengthOnZFold, design.PopulateParameters(p));
            }
            catch (Exception e)
            {
                throw new ApplicationException(string.Format("ZFold length evaluation failed for DesignId:{2} formula:'{0}' with message: {1}", design.WidthOnZFold, e.Message, design.Id));
            }
        }


        public static void ReplaceFormulasWithCurrentEvaluation(this PackagingDesign design, IDictionary<string, object> parameters)
        {
            design.WidthOnZFold = design.WidthOnZFold.Solve(parameters).ToString();
            design.MaximumWidthOnZFold = string.IsNullOrEmpty(design.MaximumWidthOnZFold)
                ? design.WidthOnZFold
                : design.MaximumWidthOnZFold.Solve(parameters).ToString();
            design.LengthOnZFold = design.LengthOnZFold.Solve(parameters).ToString();


            foreach (Line l in design.Lines)
            {
                l.Length = l.Length.Solve(parameters).ToString();

                l.StartX = l.StartX.Solve(parameters).ToString();
                l.StartY = l.StartY.Solve(parameters).ToString();

                if (l.Direction == LineDirection.vertical)
                {
                    l.EndX = !string.IsNullOrEmpty(l.EndX)
                                 ? l.EndX.Solve(parameters).ToString()
                                 : l.StartX;
                    l.EndY = !string.IsNullOrEmpty(l.EndY)
                                 ? l.EndY.Solve(parameters).ToString()
                                 : (l.StartY.Solve(parameters) + l.Length.Solve(parameters)).ToString();
                }
                else
                {
                    l.EndX = !string.IsNullOrEmpty(l.EndX)
                                 ? l.EndX.Solve(parameters).ToString()
                                 : (l.StartX.Solve(parameters) + l.Length.Solve(parameters)).ToString();
                    l.EndY = !string.IsNullOrEmpty(l.EndY)
                                 ? l.EndY.Solve(parameters).ToString()
                                 : l.StartY;
                }
            }
        }

        /// <summary>
        /// Apply measures to a PackagingDesign to convert it to a PhysicalDesign
        /// </summary>
        /// <param name="design">The design to transform</param>
        /// <param name="p">The design formula parameters containing design information</param>
        /// <param name="tileCount">The number of cartons to produce at the same time. <c>1</c> tile is one carton.</param>
        /// <param name="isRotated">Indicates whether or not this design will be rotated</param>
        /// <returns>A measurement applied physical design</returns>
        public static PhysicalDesign AsPhysicalDesign(this IPackagingDesign design, DesignFormulaParameters p, int tileCount = 1, bool isRotated = false)
        {
            try
            {
                var parameters = design.PopulateParameters(p);           
                design.AdjustLinesForElasticLines(isRotated, p, tileCount);

                var measurementAppliedDesign = new PhysicalDesign
                {
                    Width = design.WidthOnZFold.Solve(parameters),
                    Length = design.LengthOnZFold.Solve(parameters)
                };

                var lines = GetPhysicalLines(design.Lines, parameters);
                lines.ForEach(measurementAppliedDesign.Add);

                return measurementAppliedDesign;
            }
            catch (Exception)
            {
                 throw new ApplicationException(string.Format("Evaluation failed for formula {0}, DesignId:{1} is not supported because it has X Values", design.WidthOnZFold, design.Id));
            }
        }

        private static void AdjustLinesForElasticLines(this IPackagingDesign design, bool isRotated, DesignFormulaParameters formulaParameters, int tileCount)
        {
            if (!design.IsElasticLineDesign)
                return;
            
            var parameters = design.PopulateParameters(formulaParameters);
            
            var orderedElasticLines = design.Lines.OfType<ElasticLine>().OrderBy(l => l.StartX.Solve(parameters));

            var currentX = 0.0;
            var widthOnCorrugate = design.WidthOnZFold.Solve(parameters);
            var elasticlineMaxWidthDict = new Dictionary<MicroMeter, MicroMeter>();

            foreach (var line in orderedElasticLines)
            {
                var lineStartX = line.StartX.Solve(parameters);
                var lineEndX = string.IsNullOrEmpty(line.EndX) ? lineStartX + line.Length.Solve(parameters) : line.EndX.Solve(parameters);
                var lineY = line.StartY.Solve(parameters);

                if (elasticlineMaxWidthDict.ContainsKey(lineY) == false)
                    elasticlineMaxWidthDict[lineY] = 0;

                var maxAllowedWidth = (formulaParameters.ZfoldWidth - (widthOnCorrugate * tileCount)) * (line.SwellPercent / (MicroMeter)tileCount) / 100;

                if (maxAllowedWidth < lineEndX - lineStartX || isRotated)
                {
                    var origLine = line.Clone() as Line;
                    line.EndX = isRotated ? line.StartX : line.StartX + "+" + maxAllowedWidth;
                    line.Length = isRotated ? "0" : maxAllowedWidth.ToString();
                    if (currentX < lineEndX)
                        design.AdjustConnectedOnLineChange(origLine, line, parameters);

                    if (!isRotated)
                        elasticlineMaxWidthDict[lineY] += maxAllowedWidth;
                }
                else
                {
                    elasticlineMaxWidthDict[lineY] += lineEndX - lineStartX;
                }

                currentX = lineEndX;
            }

            design.WidthOnZFold += "+" + elasticlineMaxWidthDict.Max(kvp => kvp.Value);
        }

        private static void AdjustConnectedOnLineChange(
            this IPackagingDesign design,
            Line origLine,
            Line modifiedLine,
            IDictionary<string, object> formulaParameters)
        {
            var diff = origLine.Length.Solve(formulaParameters) - modifiedLine.Length.Solve(formulaParameters);
            var newEndX = modifiedLine.EndX.Solve(formulaParameters);

            foreach (var line in design.Lines.Where(line => line.StartX.Solve(formulaParameters) > newEndX))
            {
                line.StartX += "-" + diff;
                line.EndX = line.Direction == LineDirection.horizontal ? line.StartX + "+" + line.Length : line.StartX;                
            }

        }


        private static IEnumerable<PhysicalLine> GetPhysicalLines(
            IEnumerable<Line> lines,
            IDictionary<string, object> parameters)
        {
            var physicalLines = new List<PhysicalLine>();
            foreach (var line in lines)
            {
                var lineLength = line.Length.Solve(parameters);
                var startXCoord = line.StartX.Solve(parameters);
                var startYCoord = line.StartY.Solve(parameters);

                MicroMeter endXCoord, endYCoord;

                if (line.Direction == LineDirection.horizontal)
                {
                    endXCoord = startXCoord + lineLength;
                    endYCoord = startYCoord;
                }
                else
                {
                    endXCoord = startXCoord;
                    endYCoord = startYCoord + lineLength;
                }

                var physicalLine = new PhysicalLine(new PhysicalCoordinate(startXCoord, startYCoord),
                    new PhysicalCoordinate(endXCoord, endYCoord), line.Type);


                physicalLines.Add(physicalLine);
            }
            return physicalLines;
        }

        /// <summary>
        /// Returns a list with the user configurable parameters in the design, note that it filters out "FT" and "FW" because we dont want human hands to touch them
        /// </summary>
        /// <param name="design"></param>
        /// <returns></returns>
        public static List<DesignParameter> GetUsedParameters(this IPackagingDesign design)
        {
            var parameters = new List<IEnumerable<string>>
            {
                FormulaEvaluator.GetFormulaParameters(design.WidthOnZFold),
                FormulaEvaluator.GetFormulaParameters(design.MaximumWidthOnZFold),
                FormulaEvaluator.GetFormulaParameters(design.LengthOnZFold),
                GetDistinctParametersFromLines(design.Lines)
            };

            var designParameters = new List<string>();

            parameters.SelectMany(p => p).Where(p => !p.Equals("FW") && !p.Equals("FT") && !p.Equals("L") && !p.Equals("W") && !p.Equals("H")).Distinct().ForEach(designParameters.Add);

            return designParameters.Select(p => new DesignParameter { Alias = p, Name = p }).ToList();
        }

        private static IEnumerable<string> GetDistinctParametersFromLines(IEnumerable<Line> lines)
        {
            return lines.Select(line => line.GetUsedParameters()).SelectMany(l => l);
        }
    }
}
