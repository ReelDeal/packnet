﻿using System.Collections.Generic;
using System.Linq;
using PackNet.Common.ComponentModel.FormulaEvaluator;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace PackNet.Common.ExtensionMethods
{
    public static class LineExtensions
    {
        public static IEnumerable<string> GetUsedParameters(this Line line)
        {
            var lineParameters = new List<IEnumerable<string>>
            {
                FormulaEvaluator.GetFormulaParameters(line.StartX),
                FormulaEvaluator.GetFormulaParameters(line.StartY),
                FormulaEvaluator.GetFormulaParameters(line.Length)            
            };

            return lineParameters.SelectMany(l => l);
        }
    }
}
