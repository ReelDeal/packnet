﻿using System;
using System.Net;
using System.Net.Sockets;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Network;

namespace PackNet.Common.Communication
{
    public class SocketWrapper : ISocketWrapper
    {
        private readonly Socket socket;

        public SocketWrapper(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType)
        {
            socket = new Socket(addressFamily, socketType, protocolType);
        }

        public SocketWrapper(Socket socket)
        {
            this.socket = socket;
        }

        ~SocketWrapper()
        {
            Dispose(false);
        }

        public bool IsBound
        {
            get
            {
                return socket.IsBound;
            }
        }

        public bool IsConnected
        {
            get
            {
                return socket.Connected;
                //Use the enhanced IsConnected check
                //return socket.IsConnected();
            }
        }

        public int ReceiveTimeout
        {
            get { return socket.ReceiveTimeout; }
            set { socket.ReceiveTimeout = value; }
        }

        public int SendTimeout
        {
            get
            {
                return socket.SendTimeout;
            }

            set
            {
                socket.SendTimeout = value;
            }
        }

        public void Listen(int backlog)
        {
            socket.Listen(backlog);
        }

        public void Connect(string host, int port)
        {
            socket.Connect(host, port);
        }

        public IAsyncResult BeginConnect(IPAddress address, int port, AsyncCallback requestCallback, object state)
        {
            return socket.BeginConnect(address, port, requestCallback, state);
        }

        public int Send(byte[] buffer)
        {
            return socket.Send(buffer);
        }

        public int Receive(byte[] buffer)
        {
            return socket.Receive(buffer);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (socket != null)
                {
                    if (socket.Connected)
                    {
                        socket.Disconnect(false);
                    }

                    socket.Dispose();
                }
            }
        }
    }
}
