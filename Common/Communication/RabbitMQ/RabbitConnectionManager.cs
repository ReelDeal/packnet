﻿using System;
using System.Diagnostics;
using System.Reactive.Linq;
using System.Text;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Properties;
using PackNet.Common.Utils;

using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace PackNet.Common.Communication.RabbitMQ
{
    public class RabbitConnectionManager : IDisposable
    {
        private IConnection connection = null;
        private Timer connectTimer = null;
        private object connectionLock = new object();

        public const string SERVER = "Server"; //This is the name of the server routing key
        public const string FANOUT_EXCHANGE = "PackNet-FanOut"; //This exchange must exist or nothing works
        public const string DIRECT_EXCHANGE = "PackNet-Direct"; //This exchange must exist or nothing works
        public RabbitConnectionManager()
        {
            connectTimer = new Timer(Connect, TimeSpan.FromSeconds(5));
            Connect();
        }

        private void Connect()
        {
            lock (connectionLock)
            {
                connectTimer.Stop();
                if (connection != null && connection.CloseReason != null) //CloseReson is set when connection has closed
                {
                    connection.ConnectionShutdown -= connection_ConnectionShutdown;
                    connection.Dispose();
                    connection = null;
                }
                else if (connection != null)
                {
                    return; //Connection is still valid
                }

                try
                {
                    var factory = new ConnectionFactory
                    {
                        HostName = Settings.Default.MessageQueueIPAddress,
                        AutomaticRecoveryEnabled = true,
                        TopologyRecoveryEnabled = true
                    };

                    connection = factory.CreateConnection();
                    connection.ConnectionShutdown += connection_ConnectionShutdown;
                    connection.CallbackException += (sender, e) =>
                    {
                        throw new NotImplementedException();
                    };
                    connection.ConnectionBlocked += (sender1, e1) =>
                    {
                        throw new NotImplementedException();
                    };
                    //Create the exchanges
                    using (var channel = CreateChannel())
                    {
                        ExecuteWithChannel(CreateFanoutExchange, channel);
                        ExecuteWithChannel(CreateDirectExchange, channel);
                    }
                }
                catch (Exception)
                {
                    connectTimer.Start();
                }
            }
        }

        public IModel CreateChannel()
        {
            if (connection == null)
            {
                return null;
            }

            try
            {
                var channel = connection.CreateModel();
                //connection.AutoClose = true;
                return channel;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void CloseChannel(IModel channel)
        {
            if (channel == null)
                return;
            channel.Dispose();
        }

        private void ExecuteWithChannel(Action<IModel> action, IModel existingChannel = null)
        {
            if (existingChannel == null)
            {
                using (var channel = CreateChannel())
                {
                    if (channel != null)
                        action(channel);
                }
            }
            else
            {
                action(existingChannel);
            }
        }

        private void CreateFanoutExchange(IModel channel)
        {
            channel.ExchangeDeclare(FANOUT_EXCHANGE, ExchangeType.Fanout, true /* keep after server restart*/, false /* delete if no one is using*/, null /* no additional args*/);
        }

        private void CreateDirectExchange(IModel channel)
        {
            channel.ExchangeDeclare(DIRECT_EXCHANGE, ExchangeType.Direct, true /* keep after server restart*/, false /* delete if no one is using*/, null /* no additional args*/);
        }

        /// <summary>
        /// Transforms the event from the consumer into a readable message. Removes channel when disposed 
        /// </summary>
        /// <param name="routingKey"></param>
        /// <returns></returns>
        public Tuple<IObservable<string>, Action> GetMessageObservable(string routingKey)
        {
            var channel = CreateChannel();
            channel.ModelShutdown += (sender, e) =>
            {
                throw new NotImplementedException();
            };
            channel.CallbackException += (sender1, e1) =>
            {
                throw new NotImplementedException();
            };
            var queueName = channel.QueueDeclare().QueueName;
            if (routingKey != SERVER)
                channel.QueueBind(queueName, FANOUT_EXCHANGE, "", null);
            channel.QueueBind(queueName, DIRECT_EXCHANGE, routingKey, null);
            var consumer = new EventingBasicConsumer(channel);
            consumer.ConsumerCancelled += (sender2, e2) =>
            {
                throw new NotImplementedException();
            };
            consumer.Unregistered += (sender3, e3) =>
            {
                throw new NotImplementedException();
            };
            consumer.Shutdown += (sender4, e4) =>
            {
                throw new NotImplementedException();
            };
            consumer.Received += (sender5, e5) =>
            {
                Trace.WriteLine("RABBITMQ Received " + e5.RoutingKey + " " + Encoding.UTF8.GetString(e5.Body));
            };
            var observable = Observable.FromEventPattern<BasicDeliverEventArgs>(x => consumer.Received += x,
                x =>
                {
                    //I couldn't figure out why this wasnt called when the subscription was disposed so I added the Action to destroy the channel and unbind the consumer
                    channel.QueueUnbind(queueName, FANOUT_EXCHANGE, "", null);
                    channel.QueueUnbind(queueName, DIRECT_EXCHANGE, routingKey, null);
                    consumer.Received -= x;
                    channel.Dispose();
                }).Select(x => Encoding.UTF8.GetString(x.EventArgs.Body));
            channel.BasicConsume(queueName, true /*no ack required*/, consumer);

            return new Tuple<IObservable<string>, Action>(observable, () =>
            {
                if (routingKey != SERVER)
                    channel.QueueUnbind(queueName, FANOUT_EXCHANGE, "", null);
                channel.QueueUnbind(queueName, DIRECT_EXCHANGE, routingKey, null);
                channel.Dispose();
            });
        }

        public void SendToServer(string type, string message)
        {
            using (var channel = CreateChannel())
            {
                channel.BasicPublish(DIRECT_EXCHANGE, SERVER, null, Encoding.UTF8.GetBytes(message));
            }
        }

        public void SendToServer(IMessage message)
        {
            using (var channel = CreateChannel())
            {
                try
                {
                    channel.BasicPublish(DIRECT_EXCHANGE, SERVER, null,
                        Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Failed to send message to server " + e);
                }
            }
        }

        public void SendToClient(IMessage message)
        {
            using (var channel = CreateChannel())
            {
                try
                {
                    channel.BasicPublish(DIRECT_EXCHANGE, message.ReplyTo, null, Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message)));
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Failed to send message to server " + e);
                }
            }
        }

        public void SendToClient(string replyTo, byte[] message)
        {
            using (var channel = CreateChannel())
            {
                channel.BasicPublish(DIRECT_EXCHANGE, replyTo, null, message);
            }
        }

        public void SendToAll(string message)
        {
            using (var channel = CreateChannel())
            {
                channel.BasicPublish(FANOUT_EXCHANGE, "", null, Encoding.UTF8.GetBytes(message));
            }
        }

        private void connection_ConnectionShutdown(object sender, ShutdownEventArgs e)
        {
            if (e.Initiator != ShutdownInitiator.Application)
                connectTimer.Start();
        }

        public void Dispose()
        {
            //TODO: clean up
        }
    }
}