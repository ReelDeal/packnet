﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;

using zlib;

using SerializationSettings = PackNet.Common.Interfaces.Converters.SerializationSettings;

namespace PackNet.Common.Communication.RabbitMQ
{
    /// <summary>
    /// RabbitSubscriber should be nothing more then a glorified event dispatcher, passing messages to the event aggregator 
    /// </summary>
    public class RabbitSubscriber : IUICommunicationService
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private ConcurrentDictionary<string, IDisposable> existingRegistrations = new ConcurrentDictionary<string, IDisposable>();
        private readonly Subject<Tuple<MessageTypes, string>> eventSubject;
        private readonly ConcurrentDictionary<string /*MessageTypes.ToString() + double milliseconds*/, Subject<IMessage>> throttledEventSubjects;
        private RabbitConnectionManager rabbitConnectionManager;
        private IDisposable disposable;

        public JsonSerializerSettings JsonSerializerSettings { get; private set; }

        public string Name { get { return "UICommunicationService"; } }

        /// <summary>
        /// Observable stream of Tuple representing the MessageType of the payload and the serialized json payload of the event coming from the user interface
        /// </summary>
        public IObservable<Tuple<MessageTypes, string>> UIEventObservable
        {
            get { return eventSubject.AsObservable(); }
        }

        public RabbitSubscriber(RabbitConnectionManager rabbitConnectionManager,
            IEventAggregatorPublisher publisher,
            ILogger logger)
        {
            eventSubject = new Subject<Tuple<MessageTypes, string>>();
            throttledEventSubjects = new ConcurrentDictionary<string, Subject<IMessage>>();

            this.rabbitConnectionManager = rabbitConnectionManager;
            this.publisher = publisher;
            this.logger = logger;

            JsonSerializerSettings = SerializationSettings.GetJsonSerializerSettings();
            SetupRabbitSubscriber();

            /* ConnectionAlive */
            //This is basically a ping/pong for the UI. Before the login screen is displayed it sends a ConnectionAlive query
            UIEventObservable.Where(e => e.Item1 == MessageTypes.ConnectionAlive)
                .DurableSubscribe(e =>
                {
                    SendMessageToUI(new ResponseMessage
                    {
                        MessageType = MessageTypes.ConnectionAlive,
                        Result = ResultTypes.Success
                    });
                    Trace.WriteLine("Server sent ConnectionAlive");
                },
                    logger)
                    ;

        }

        /// <summary>
        /// Adds a converter to the serializer settings
        /// </summary>
        /// <param name="converter"></param>
        public void AddJsonConverter(JsonConverter converter)
        {
            JsonSerializerSettings.Converters.Add(converter);
        }

        /// <summary>
        /// Registers the message type and T with the message listener, every time a message is seen with the given message type... the payload will be 
        /// de-serialized into T and published on the internal event aggregator to be consumed by a service 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="messageType"></param>
        /// <returns></returns>
        public IDisposable RegisterUIEventWithInternalEventAggregator<T>(MessageTypes messageType)
        {
            //Ensure that only one message handler is registered
            var uniqueMessageName = typeof(T).FullName + "-" + messageType.DisplayName;
            IDisposable disposable = null;
            if (existingRegistrations.TryGetValue(uniqueMessageName, out disposable))
                return disposable;
            disposable = UIEventObservable.Where(e => e.Item1 == messageType)
                .DurableSubscribe(e =>
                {
                    try
                    {
                        var msg = JsonConvert.DeserializeObject<Message<T>>(e.Item2, JsonSerializerSettings);

                        logger.Log(LogLevel.Trace, "{0}", msg);

                        publisher.Publish(msg);
                    }
                    catch (Exception ex)
                    {
                        logger.LogException(LogLevel.Error, "Deserialization error.  Message of Type:{0}, Item1:{1} Item2:{2}", ex, uniqueMessageName, e.Item1, e.Item2);
                    }
                }, logger);
            existingRegistrations.TryAdd(uniqueMessageName, disposable);
            return disposable;
        }

        /// <summary>
        /// Registers a message type with the message listener, every time a message is seen with the given message type 
        /// the message will be published on the internal event aggregator to be consumed by a service 
        /// </summary>
        /// <param name="messageType"></param>
        /// <returns></returns>
        public IDisposable RegisterUIEventWithInternalEventAggregator(MessageTypes messageType)
        {
            return RegisterUIEventWithInternalEventAggregator<Message>(messageType);
        }

        /// <summary>
        /// Send a message to the user interface
        /// </summary>
        /// <param name="message">Message to send to user interface</param>
        /// <param name="publishInternally"></param>
        public void SendMessageToUI(IMessage message, bool publishInternally = false)
        {
            try
            {
                var jsonSerializedObject = JsonConvert.SerializeObject(message, JsonSerializerSettings);
                logger.Log(LogLevel.Trace, "{0} is lenth of {1}", jsonSerializedObject.Length, jsonSerializedObject);
                if (String.IsNullOrEmpty(message.ReplyTo))
                {
                    rabbitConnectionManager.SendToAll(jsonSerializedObject);
                }
                else
                {
                    rabbitConnectionManager.SendToClient(message.ReplyTo, Encoding.UTF8.GetBytes(jsonSerializedObject));
                }
            }
            catch (OutOfMemoryException)
            {
                TryStreamSerialization(message, JsonSerializerSettings);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Failed to serialize object: " + e.ToString());
            }

            if (publishInternally)
                publisher.Publish(message);
        }

        private void TryStreamSerialization(IMessage message, JsonSerializerSettings serializerSettings)
        {
            using (var ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms))
                {
                    try
                    {
                        var serializer = new JsonSerializer();
                        serializerSettings.Converters.ForEach(s => serializer.Converters.Add(s));
                        serializer.Serialize(sw, message);
                        ms.Position = 0;

                        rabbitConnectionManager.SendToClient(message.ReplyTo, ms.GetBuffer());
                    }
                    catch (Exception e)
                    {
                        logger.Log(LogLevel.Error, "Failed to serialize stream: " + e.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Send a message to the user interface
        /// </summary>
        /// <typeparam name="T">Type of payload serialized in the message</typeparam>
        /// <param name="message">Message to send to user interface</param>
        /// <param name="publishInternally">if set to <c>true</c> the message is also publish internally.</param>
        /// <param name="compress">Set to false only during development.</param>
        public void SendMessageToUI<T>(IMessage<T> message, bool publishInternally = false, bool compress = true)
        {
            try
            {
                if (compress)
                {
                    message.Compressed = true;
                    var jsonSerializedObject = JsonConvert.SerializeObject(message.Data, JsonSerializerSettings);

                    IMessage stringMessage;
                    var responseMessage = message as IResponseMessage;
                    if (responseMessage != null)
                    {
                        stringMessage = new ResponseMessage<string>
                        {
                            Compressed = true,
                            MessageType = responseMessage.MessageType,
                            MachineGroupId = responseMessage.MachineGroupId,
                            MachineGroupName = responseMessage.MachineGroupName,
                            ReplyTo = responseMessage.ReplyTo,
                            UserName = responseMessage.UserName,
                            Data = Compress(jsonSerializedObject),
                            Result = responseMessage.Result,
                            Message = responseMessage.Message
                        };
                    }
                    else
                    {
                        stringMessage = new Message<string>
                        {
                            Compressed = true,
                            MessageType = message.MessageType,
                            MachineGroupId = message.MachineGroupId,
                            MachineGroupName = message.MachineGroupName,
                            ReplyTo = message.ReplyTo,
                            UserName = message.UserName,
                            Data = Compress(jsonSerializedObject),
                        };
                    }


                    SendMessageToUI(stringMessage);
                    if (publishInternally)
                    {
                        //We don't want to publish the compressed message
                        publisher.Publish(message);
                    }
                }
                else
                    SendMessageToUI((IMessage)message, publishInternally);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Failed to send message to server" + e);
            }
        }

        public void SendThrottledMessageToUI<T>(IMessage<T> message, TimeSpan throttledTime)
        {
            Subject<IMessage> subject;
            if (!throttledEventSubjects.TryGetValue(message.MessageType.ToString() + throttledTime.TotalMilliseconds, out subject))
            {
                subject = new Subject<IMessage>();
                subject.Sample(throttledTime).Subscribe(m => SendMessageToUI(m as IMessage<T>));
                throttledEventSubjects.TryAdd(message.MessageType.ToString() + throttledTime.TotalMilliseconds, subject);
            }
            subject.OnNext(message);
        }

        public static string Compress(string text)
        {
            var inBytes = Encoding.UTF8.GetBytes(text);
            var compressed = CompressData(inBytes);
            var ret = Convert.ToBase64String(compressed);
            return ret;
        }

        public static string Decompress(string text)
        {
            var nonencoded = Convert.FromBase64String(text);
            var uncompressed = DecompressData(nonencoded);
            return Encoding.UTF8.GetString(uncompressed);
        }

        public static byte[] CompressData(byte[] inData)
        {
            using (var outMemoryStream = new MemoryStream())
            {
                using (var outZStream = new ZOutputStream(outMemoryStream, zlibConst.Z_DEFAULT_COMPRESSION))
                {
                    using (var inMemoryStream = new MemoryStream(inData))
                    {
                        {
                            CopyStream(inMemoryStream, outZStream);
                            outZStream.finish();
                            return outMemoryStream.ToArray();
                        }
                    }
                }
            }
        }

        public static byte[] DecompressData(byte[] inData)
        {
            using (var outMemoryStream = new MemoryStream())
            {
                using (var outZStream = new ZOutputStream(outMemoryStream))
                {
                    using (var inMemoryStream = new MemoryStream(inData))
                    {
                        CopyStream(inMemoryStream, outZStream);
                        outZStream.finish();
                        return outMemoryStream.ToArray();
                    }
                }
            }
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[2000];
            int len;
            while ((len = input.Read(buffer, 0, 2000)) > 0)
            {
                output.Write(buffer, 0, len);
            }
            output.Flush();
        }

        private void SetupRabbitSubscriber()
        {
            logger.Log(LogLevel.Debug, "Setting up rabbit subscriber");
            var observable = rabbitConnectionManager.GetMessageObservable("Server");
            disposable = observable.Item1.Subscribe(s =>
            {
                var task = Task.Factory.StartNew(() =>
                {
                    //TODO: Get the type
                    var message = JsonConvert.DeserializeObject<Message>(s);
                    eventSubject.OnNext(new Tuple<MessageTypes, string>(message.MessageType, s));
                });
                task.ContinueWith((t) =>
                {
                    if (t.IsFaulted)
                        logger.Log(LogLevel.Error, "Error processing rabbit message " + t.Exception.Message);
                });

            });
        }

        public void Dispose()
        {
            disposable.Dispose();
        }
    }
}
