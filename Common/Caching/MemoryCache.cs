﻿using System;
using System.Runtime.Caching;
using PackNet.Common.Interfaces.Caching;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Caching
{
    public class MemoryCache : IMemoryCache
    {
        private readonly ILogger logger;
        private readonly System.Runtime.Caching.MemoryCache Cache;
        private static readonly Object SyncLock = new object();

        public MemoryCache(ILogger logger, string cacheName)
        {
            this.logger = logger;
            Cache = new System.Runtime.Caching.MemoryCache(cacheName);
        }

        public bool TryGet<T>(string cacheKey, out T item)
        {
            lock (SyncLock)
            {
                if (Cache.Contains(cacheKey))
                {
                    item = GetItem<T>(cacheKey);
                    return true;
                }
                else
                {
                    item = default(T);
                    return false;
                }

            }

        }

        public T GetItem<T>(string cacheKey)
        {
            lock (SyncLock)
                return (T)Cache.Get(cacheKey);
        }

        public void CacheItem(string cacheKey, object obj)
        {
            lock (SyncLock)
                Cache.Set(cacheKey, obj, GetDefaultPolicy());
        }

        private CacheItemPolicy GetDefaultPolicy()
        {
            return new CacheItemPolicy() { Priority = CacheItemPriority.NotRemovable, AbsoluteExpiration = DateTimeOffset.Now.AddDays(1), UpdateCallback = CacheItemUpdated };
        }

        private const string CacheItemUpdatedFormat = "CacheItemUpdated - Key: {0}, RemovedReason: {1}, Source: {2}, AbsoluteExpiration: {3}, Priority: {4}, SlidingExpiration: {5}";
        private void CacheItemUpdated(CacheEntryUpdateArguments arguments)
        {
            logger.Log(LogLevel.Debug, String.Format(CacheItemUpdatedFormat, arguments.Key, arguments.RemovedReason, arguments.Source, arguments.UpdatedCacheItemPolicy.AbsoluteExpiration, arguments.UpdatedCacheItemPolicy.Priority, arguments.UpdatedCacheItemPolicy.SlidingExpiration));
        }
    }
}