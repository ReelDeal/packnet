﻿using CsvHelper;
using CsvHelper.Configuration;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace PackNet.Common.FileHandling.DynamicImporter
{
	public class FileEnumerator : IEnumerator<object>, /* Foreach */ IEnumerable<object> /* Linq support */
	{
		private readonly int secondsToRetry;
		private readonly ILogger logger;
        private StreamReader streamReader;
		private ExpandoObject currentRow;

	    private readonly string inputString;
		private readonly string fileFullPath = string.Empty;
		
        private string[] headerFields;
		private string[] headers;

		private bool disposedValue;
        private readonly CsvConfiguration csvConfiguration;
        private readonly IUserNotificationService notificationService;
	    private int rowCounter;
	    private bool headersRetreivedFromInput = false;
        /// <summary>
        /// Enumerates data in the given file returning a Dictionary&lt;string, expando object&gt;
        /// </summary>
        /// <param name="fileFullPath">Full path of the file to enumerate</param>
        /// <param name="delimiter">Field separator (e.g., ',' or ';' or ':')</param>
        /// <param name="logger">Log messages</param>
        /// <param name="notificationService">The notification service.</param>
        public FileEnumerator(string fileFullPath, char delimiter, ILogger logger, IUserNotificationService notificationService)
            : this(fileFullPath, notificationService, logger, delimiter: delimiter)
		{
		}

        public FileEnumerator(IUserNotificationService notificationService, ILogger logger, string[] headerFields = null, char delimiter = ',', char comment = '\'', int secondsToRetry = 15)
	    {
            this.logger = logger;
            this.headerFields = headerFields ?? new string[0];
            this.secondsToRetry = secondsToRetry;
            this.notificationService = notificationService;
            csvConfiguration = new CsvConfiguration { Delimiter = delimiter.ToString(CultureInfo.InvariantCulture), Comment = comment };
	    }

	    /// <summary>
	    /// Enumerates data in the given file returning a Dictionary&lt;string, expando object&gt;
	    /// </summary>
	    /// <param name="fileFullPath">Full path of the file to enumerate</param>
	    /// <param name="notificationService">The notification service.</param>
	    /// <param name="logger">Log messages</param>
	    /// <param name="headerFields">String array of fields that are contained in the file</param>
	    /// <param name="delimiter">Field separator (e.g., ',' or ';' or ':')</param>
	    /// <param name="comment">Comment indicator (e.g., '\' or '-' or '~')</param>
	    /// <param name="secondsToRetry">Number of seconds to retry a locked file</param>
	    public FileEnumerator(string fileFullPath, IUserNotificationService notificationService, ILogger logger,
	        string[] headerFields = null, char delimiter = ',', char comment = '\'', int secondsToRetry = 15)
	        : this(notificationService, logger, headerFields, delimiter, comment, secondsToRetry)
	    {
	        this.fileFullPath = fileFullPath;
	    }

        public FileEnumerator(ILogger logger, string input, IUserNotificationService notificationService, char delimiter = ',', char comment = '\'', string[] headerFields = null)
            : this(notificationService, logger, headerFields, delimiter, comment)
	    {
            inputString = input;
	    }

		/// <summary>
		/// Returns enumerator
		/// </summary>
		/// <returns></returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

	    private void InitializeStreamReader()
	    {
            Retry.For(
              () =>
              {
                  var stream = string.IsNullOrEmpty(fileFullPath) ? inputString.ToStream() : new FileStream(fileFullPath, FileMode.Open, FileAccess.Read);
                  streamReader = new StreamReader(stream);
              }, TimeSpan.FromSeconds(secondsToRetry));
	    }

		/// <summary>
		/// Opens an enumerator for the file stream designated in the full file path injected into the constructor.
		/// </summary>
		/// <returns>An enumerator for the file stream</returns>
		public IEnumerator<object> GetEnumerator()
		{
		    try
		    {
		        InitializeStreamReader();

                rowCounter = 1;
                if (headerFields.Length == 0)
                {
                    rowCounter = 2;

                    var headerRow = streamReader.ReadLine();
		            if (headerRow == null)
		                return this;

		            if (headerRow.Trim().StartsWith(csvConfiguration.Comment.ToString(CultureInfo.InvariantCulture)))
		            {
		                if (streamReader != null)
		                    streamReader.Dispose();

		                notificationService.SendNotificationToSystem(NotificationSeverity.Error,
		                    string.Format(
		                        "No headers in import file {0}. Headers may not be defined correctly in File Import settings or the header line in the file may be commented out.",
		                        Path.GetFileNameWithoutExtension(fileFullPath)), string.Empty);
		                throw new InvalidOperationException(
		                    "No headers defined in constructor nor in file (header line may be mistakenly commented)");
		            }

                    headerFields = ParseSingleLine(headerRow);
                    headersRetreivedFromInput = true;
                }
                else if (headersRetreivedFromInput)
                {
                    streamReader.ReadLine();
                }

		        headers = new string[headerFields.Length];
		        for (var i = 0; i <= headerFields.Length - 1; i++)
		        {
		            headers[i] = GetSafeFieldName(headerFields[i]);
		        }
		        ValidateHeader(headers);
		    }
		    catch (UnauthorizedAccessException unauthorizedAccessException)
		    {
		        logger.Log(LogLevel.Error, "Error reading file:{0}, exception:{1}", fileFullPath, unauthorizedAccessException);
		        Debug.WriteLine("GetEnumerator exception {0}", new object[] { unauthorizedAccessException.Message });

		        if (streamReader != null)
		        {
		            streamReader.Dispose();
		        }

		        throw;
		    }
		    catch (InvalidCharacterException invalidCharacterException)
            {
                notificationService.SendNotificationToSystem(NotificationSeverity.Error,
                    string.Format(
                        "No headers in import file {0}.",
                        Path.GetFileNameWithoutExtension(fileFullPath)), string.Empty);
                logger.Log(LogLevel.Error, "Error reading file:{0}. No headers in import file, exception:{1}", fileFullPath, invalidCharacterException);

                throw;
		    }
		    catch (Exception exception)
		    {
		        logger.Log(LogLevel.Error, "Error reading file:{0}, exception:{1}", fileFullPath, exception);
		        Debug.WriteLine("GetEnumerator exception {0}", new object[] { exception.Message });

		        if (streamReader != null)
		        {
		            streamReader.Dispose();
		        }

		        throw;
		    }
		    return this;
		}

		/// <summary>
		/// Transforms the input string by trimming the leading and trailing whitespace and replacing inner spaces with an underscore.
		/// </summary>
		/// <param name="input">String to transform.</param>
		/// <returns>Transformed string.</returns>
		private static string GetSafeFieldName(string input)
		{
			return input.Trim().Replace(" ", "_");
		}

		/// <summary>
		/// Returns the row that is currently pointed to by the iterator.
		/// </summary>
		public object Current
		{
			get { return currentRow; }
		}

		/// <summary>
		/// Iterates through all lines of a file stream. 
		/// </summary>
		/// <returns>True if current line has data, false if current line is null or length is less than or equal to zero.</returns>
		public bool MoveNext()
		{
			var line = streamReader.ReadLine();

			if (line == null || line.Length <= 0)
			{
			    var hasMore = streamReader.Peek();
                if (hasMore != -1)
			        return MoveNext();

				logger.Log(LogLevel.Trace, "Null or blank line found. Stopped processing file: {0}", this.fileFullPath);
				return false;
			}

			if (line.StartsWith(csvConfiguration.Comment.ToString(CultureInfo.InvariantCulture)))
				return MoveNext();

			logger.Log(LogLevel.Trace, "Importing line:{0}", line);

			currentRow = new ExpandoObject();
			var currentRowDictionary = currentRow as IDictionary<string, Object>;
			var rowValues = ParseSingleLine(line);
			for (var i = 0; i <= headers.Length - 1; i++)
			{
				try
				{
					currentRowDictionary.Add(headers[i], rowValues[i]);
				}
				catch (IndexOutOfRangeException)
				{
					currentRowDictionary.Add(headers[i], null); // handle when row does not contain a value for each header, set value to null
				}
			}

		    currentRowDictionary.Add("rowNumber", rowCounter);
		    rowCounter++;

			if (rowValues.Length > headers.Length)
			{
				// Log when the customer has given us more data values than the headers (additional data values are ignored, not imported)
				logger.Log(LogLevel.Info, "Imported line contains more values ({0}) than expected headers ({1}). Line: {2}", rowValues.Length, headers.Length, line);
			}

			return true;
		}

		/// <summary>
		/// Parses the given string using a CsvParser <see cref="CsvParser"/>, parsing it based on the CsvConfiguration created in the constructor.
		/// </summary>
		/// <param name="line">Line with delimiters </param>
		/// <returns>Array of fields parsed by the CsvParser</returns>
		private string[] ParseSingleLine(string line)
		{
			var c = new CsvParser(new StringReader(line), csvConfiguration);
			var rowValues = c.Read();
			return rowValues;
		}

		/// <summary>
		/// Validates the header fields can be converted into a valid C# property name based on 
		/// the C# Lexical Specification http://msdn.microsoft.com/en-us/library/aa664670.aspx
		/// </summary>
		/// <param name="headers">IEnumerable headers to validate</param>
		/// <remarks>
		/// For further information see Regular Expression - Character Classes http://msdn.microsoft.com/en-us/library/20bw873z(v=vs.110).aspx
		/// </remarks>
		private void ValidateHeader(IEnumerable<string> headers)
		{
			foreach (var field in headers)
			{
				// Valdiate the first character, which should be an underscore or letter
				const string startCharacterPattern = @"[^\p{Lu}\p{Ll}\p{Lt}\p{Lm}\p{Lo}\p{Nl}]";
				if (Regex.IsMatch(field.Substring(0, 1), startCharacterPattern))
				{
					if (streamReader != null) streamReader.Dispose();
					throw new InvalidCharacterException("Field {0} beginning character must be alpha or underscore", field);
				}
				// Validate the rest of the header, valid characters are letters, numbers, connecting characters, combining characters and some formatting characters
				const string fullCharacterPattern = @"[^\p{Lu}\p{Ll}\p{Lt}\p{Lm}\p{Lo}\p{Nl}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\p{Cf}]";
				if (Regex.IsMatch(field, fullCharacterPattern))
				{
					if (streamReader != null) streamReader.Dispose();
					throw new InvalidCharacterException("Field {0} is invalid as it contains special characters", field);
				}
			}
		}

		/// <summary>
		/// Closes the file stream then calls GetEnumerator
		/// </summary>
		public void Reset()
		{
			streamReader.Close();

			GetEnumerator();
		}

		~FileEnumerator()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing && streamReader != null)
				{
					streamReader.Dispose();
				}
				currentRow = null;
			}
			disposedValue = true;
		}
	}
}
