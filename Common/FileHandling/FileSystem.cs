﻿using System;
using System.IO;

using PackNet.Common.Interfaces.FileHandling;

namespace PackNet.Common.FileHandling
{
    public class FileSystem : IFileSystem
    {
        public DirectoryInfo CreateTemporaryDirectory()
        {
            var dirPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());

            var di = new DirectoryInfo(dirPath);

            if (!di.Exists)
            {
                di.Create();
            }

            return di;
        }

        public string GetTimestampedFileName(string fileExtension)
        {
            var now = DateTime.UtcNow;

            return String.Format("{0}.{1}", now.ToString("yyyyMMdd-HHmmss"), fileExtension);
        }
    }
}