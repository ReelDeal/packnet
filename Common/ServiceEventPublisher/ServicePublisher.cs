﻿using System;
using System.Linq;
using System.Runtime.Serialization;

using PackNet.Common.Utils;
using PackNet.Common.WebServiceConsumer;

namespace PackNet.Common.ServiceEventPublisher
{
    using Interfaces.Eventing;

    [DataContract(Name = "ServicePublisher", Namespace = "http://Packsize.com")]
    public class ServicePublisher : IPublisher
    {
        public ServicePublisher()
        {
            Enabled = false;
            Type = PublisherType.JobDone;
            FieldDelimiter = ";";
            Fields = "Title;Width;Length;Height";
            Url = "http://localhost/EventNotifcationService.asmx";
        }

        [DataMember(Name = "Enabled")]
        public virtual bool Enabled { get; set; }

        [DataMember(Name = "Type")]
        public virtual PublisherType Type { get; set; }
                
        [DataMember(Name = "FieldDelimiter")]
        public virtual string FieldDelimiter { get; set; }

        [DataMember(Name = "Fields")]
        public virtual string Fields { get; set; }

        [DataMember(Name = "Url")]
        public virtual string Url { get; set; }

        public IEventNotificationConsumer Service { get; set; }
        
        public void Publish(object data, DateTime eventDate)
        {
            if (Enabled)
            {
                SendMessageToWebService(CreateDataLine(data, eventDate));
            }
        }

        private string CreateDataLine(object data, DateTime eventDate)
        {
            string dataLine = FieldDelimiter + eventDate;

            foreach (string field in Fields.Split(FieldDelimiter.ToArray()))
            {
                string fieldData = FieldReplacement.ReplaceFieldWithData(data, field, "Job");

                dataLine += FieldDelimiter + fieldData;
            }

            return dataLine.Substring(FieldDelimiter.Length);
        }

        private void SendMessageToWebService(string message)
        {
            if (Service == null)
            {
                Service = new EventNotifcationServiceWebService(Url);
            }

            string reply = Service.SendEventNotification(message);
        }
    }
}
