﻿using System;
using System.Collections.Generic;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.ServiceEventPublisher
{
    using Interfaces.Eventing;

    public class ServiceEventPublisher : EventPublisher
    {
        private readonly ILogger logger;

        public ServiceEventPublisher(IEnumerable<IPublisher> publishers, ILogger logger)
        {
            this.publishers = publishers;
            this.logger = logger;
        }

        public override void JobDone(object data, DateTime eventTime)
        {
            try
            {
                base.JobDone(data, eventTime);
            }
            catch (Exception ex)
            {
                LogException(ex);
            }            
        }

        public override void PackagingDone(object data, DateTime eventTime)
        {
            try
            {
                base.PackagingDone(data, eventTime);
            }            
            catch (Exception ex)
            {
                LogException(ex);                
            }
        }

        private void LogException(Exception ex)
        {
            if (ex is NullReferenceException)
            {
                logger.Log(LogLevel.Warning, "Null reference in publisher");
            }
            else
            {
                logger.Log(LogLevel.Warning, "Could not read queue item properties.");
            }
        }
    }
}
