﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using PackNet.Common.ComponentModel.Configuration;
using PackNet.Common.WebServiceConsumer;

namespace PackNet.Common.ServiceEventPublisher
{
    using Interfaces.Eventing;

    [DataContract(Name = "ServiceEventPublisherSettings", Namespace = "http://Packsize.com")]
    public class ServiceEventPublisherSettings : XmlConfiguration<ServiceEventPublisherSettings>
    {
        #region Ctor
        
        public ServiceEventPublisherSettings()
        {
            var jobPublisher = new ServicePublisher();
            var packagingPublisher = new ServicePublisher();
            packagingPublisher.Type = PublisherType.PackagingDone;

            var publishers = new List<IPublisher>();
            publishers.Add(jobPublisher);
            publishers.Add(packagingPublisher);
            Publishers = publishers;            
        }
        
        #endregion

        #region Properties

        [DataMember(Name = "Publishers")]
        public virtual IEnumerable<IPublisher> Publishers { get; set; }
                
        public IEventNotificationConsumer EventNotificationConsumer { get; set; }
        
        #endregion
    }
}
