﻿using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Logging
{
    public static class LogManager
    {
        private static ILoggingBackend loggingBackend;

        public static ILoggingBackend LoggingBackend
        {
            get
            {
                return loggingBackend;
            }

            set
            {
                loggingBackend = value;
            }
        }

        public static ILogger GetLogForApplication()
        {
            return loggingBackend.GetLog();
        }

        public static ILogger GetLogFor(string logName)
        {
            return loggingBackend.GetLogFor(logName);
        }
    }
}
