﻿using System;

using NLog;
using NLog.Config;

using PackNet.Common.Interfaces.Logging;

using LogLevel = PackNet.Common.Interfaces.Logging.LogLevel;

namespace PackNet.Common.Logging.NLogBackend
{
    using LogManager = NLog.LogManager;

    public class NLogImplementation : ILogger
    {
        private Logger nLogger;

        public NLogImplementation(string loggerName, string configurationPath)
        {
            var configuration = new XmlLoggingConfiguration(configurationPath);

            LogManager.Configuration = configuration;

            LogName = loggerName;
            nLogger = LogManager.GetLogger(loggerName);
        }

        public string LogName { get; private set; }

        public string Name { get { return "NLogLoggerService"; } }

        public void Log(LogLevel logLevel, string logMessage, params object[] args)
        {
            string str;

            try
            {
                str = args.Length > 0 ? string.Format(logMessage, args) : logMessage;
            }
            catch (Exception exception)
            {
                str =
                    string.Format("Message you attempted to log threw an exception. Message format: {0} args: {1} exception: {2}",
                        logMessage.Replace("{", "("),
                        string.Join(",", args),
                        exception);
            }

            if (logLevel == LogLevel.Info)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Info, LogName, str));
            }
            else if (logLevel == LogLevel.Debug)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Debug, LogName, str));
            }
            else if (logLevel == LogLevel.Trace)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Trace, LogName, str));
            }
            else if (logLevel == LogLevel.Warning)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Warn, LogName, str));
            }
            else if (logLevel == LogLevel.Error)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Error, LogName, str));
            }
            else if (logLevel == LogLevel.Fatal)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Fatal, LogName, str));
            }
        }

        public void Log<T>(LogLevel logLevel, string id, string logMessage, params object[] args)
        {
            var str = args.Length > 0 ? string.Format(logMessage, args) : logMessage;
            Log(logLevel, "{0}:{1} - {2}", typeof(T).Name, id, str);
        }

        public void Log(LogLevel logLevel, Func<string> logMessage)
        {
            var shouldLog = false;
            switch (logLevel)
            {
                case LogLevel.Trace:
                    shouldLog = nLogger.IsTraceEnabled;
                    break;
                case LogLevel.Debug:
                    shouldLog = nLogger.IsDebugEnabled;
                    break;
                case LogLevel.Info:
                    shouldLog = nLogger.IsInfoEnabled;
                    break;
                case LogLevel.Warning:
                    shouldLog = nLogger.IsWarnEnabled;
                    break;
                case LogLevel.Error:
                    shouldLog = nLogger.IsErrorEnabled;
                    break;
            }
            if (shouldLog)
                Log(logLevel, logMessage());
        }

        public void Log(LogLevel logLevel, string logMessage)
        {
            Log(logLevel, logMessage, new object[0]);
        }
        
        public void LogException(LogLevel logLevel, string logMessage, Exception exception, params object[] args)
        {
            var str = args.Length > 0 ? string.Format(logMessage, args) : logMessage;
            if (logLevel == LogLevel.Info)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Info, LogName, String.Format("Message: {0}, Exception: {1}", str, exception)));
            }
            else if (logLevel == LogLevel.Debug)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Debug, LogName, String.Format("Message: {0}, Exception: {1}", str, exception)));
            }
            else if (logLevel == LogLevel.Trace)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Trace, LogName, String.Format("Message: {0}, Exception: {1}", str, exception)));
            }
            else if (logLevel == LogLevel.Warning)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Warn, LogName, String.Format("Message: {0}, Exception: {1}", str, exception)));
            }
            else if (logLevel == LogLevel.Error)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Error, LogName, String.Format("Message: {0}, Exception: {1}", str, exception)));
            }
            else if (logLevel == LogLevel.Fatal)
            {
                nLogger.Log(GetType(), new LogEventInfo(NLog.LogLevel.Fatal, LogName, String.Format("Message: {0}, Exception: {1}", str, exception)));
            }
        }

        public void Dispose()
        {
            //don't care
        }
    }
}
