﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Logging.NLogBackend
{

    public class NLogBackend : ILoggingBackend
    {
        private readonly Dictionary<string, NLogImplementation> loggs;
        private readonly string defaultName;

        private readonly string configurationPath;

        public NLogBackend(string loggerName, string configurationPath)
        {
            defaultName = loggerName;
            this.configurationPath = configurationPath;
            var log = new NLogImplementation(loggerName, configurationPath);

            loggs = new Dictionary<string, NLogImplementation> { { defaultName, log } };
        }

        public ILogger GetLog()
        {
            return loggs[defaultName];
        }

        public ILogger GetLogFor(string logName)
        {
            NLogImplementation existingLog;
            if (loggs.TryGetValue(logName, out existingLog))
            {
                return existingLog;
            }

            var newLog = new NLogImplementation(logName, configurationPath);
            loggs.Add(logName, newLog);

            return newLog;
        }
    }
}
