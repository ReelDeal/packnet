﻿namespace PackNet.Common.ComponentModel.Exceptions
{
    using System;

    public class InvalidFormulaException : Exception
    {
        public InvalidFormulaException(string formula) : base(string.Format("The formula [{0}] is invalid.", formula))
        {
            Formula = formula;
        }

        public string Formula { get; private set; }
    }
}
