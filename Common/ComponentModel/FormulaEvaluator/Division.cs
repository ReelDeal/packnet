﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class Division : NumericOperator
    {
        public override object Evaluate(double denominator, double nominator)
        {
            return denominator / nominator;
        }
    }
}
