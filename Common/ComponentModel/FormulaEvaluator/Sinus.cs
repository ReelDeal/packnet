﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal class Sinus : NumericFunction
    {
        public override object Evaluate(double parameter)
        {
            return Math.Sin(parameter * Math.PI / 180);
        }
    }
}
