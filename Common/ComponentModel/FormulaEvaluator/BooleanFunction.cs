﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal abstract class BooleanFunction : Function
    {
        public override object Evaluate(object parameter)
        {
            if (parameter is bool)
            {
                return Evaluate((bool)parameter);
            }

            throw new ArgumentException("Argument type not accepted");
        }

        public abstract object Evaluate(bool parameter);
    }
}
