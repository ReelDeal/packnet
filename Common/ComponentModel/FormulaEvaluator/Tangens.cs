﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal class Tangens : NumericFunction
    {
        public override object Evaluate(double parameter)
        {
            return Math.Tan(parameter * Math.PI / 180);
        }
    }
}
