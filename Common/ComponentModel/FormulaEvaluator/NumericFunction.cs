﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal abstract class NumericFunction : Function
    {
        public override object Evaluate(object parameter)
        {
            if (parameter is double)
            {
                return Evaluate((double)parameter);
            }

            throw new ArgumentException("Argument type not accepted");
        }

        public abstract object Evaluate(double parameter);
    }
}
