﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class Subtraction : NumericOperator
    {
        public override object Evaluate(double left, double right)
        {
            return left - right;
        }
    }
}
