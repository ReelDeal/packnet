﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class Addition : NumericOperator
    {
        public override object Evaluate(double left, double right)
        {
            return left + right;
        }
    }
}
