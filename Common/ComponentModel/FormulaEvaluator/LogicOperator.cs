﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal abstract class LogicOperator : Operator
    {
        /// <summary>
        /// Strings will be uppercased then compared
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public override object Evaluate(object left, object right)
        {
            if (left is double && right is double)
            {
                return Evaluate((double)left, (double)right);
            }
            if (left is string && right is string)
            {
                return Evaluate(((string)left).ToUpper(), ((string)right).ToUpper());
            }

            throw new ArgumentException("Argument type not accepted");
        }

        public abstract object Evaluate(double left, double right);

        public abstract object Evaluate(string left, string right);
    }
}
