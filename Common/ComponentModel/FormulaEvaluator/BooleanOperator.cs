﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal abstract class BooleanOperator : Operator
    {
        public override object Evaluate(object left, object right)
        {
            if (left is bool && right is bool)
            {
                return Evaluate((bool)left, (bool)right);
            }

            throw new ArgumentException("Argument type not accepted");
        }

        public abstract object Evaluate(bool left, bool right);
    }
}
