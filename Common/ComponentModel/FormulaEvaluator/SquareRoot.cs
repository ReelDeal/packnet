﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal class SquareRoot : NumericFunction
    {
        public override object Evaluate(double parameter)
        {
            return Math.Sqrt(parameter);
        }
    }
}
