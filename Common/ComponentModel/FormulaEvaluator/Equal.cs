﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class Equal : LogicOperator
    {
        public override object Evaluate(double left, double right)
        {
            return left == right;
        }

        public override object Evaluate(string left, string right)
        {
            return left == right;
        }
    }
}
