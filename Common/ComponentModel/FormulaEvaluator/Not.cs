﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class Not : BooleanFunction
    {
        public override object Evaluate(bool parameter)
        {
            return !parameter;
        }
    }
}
