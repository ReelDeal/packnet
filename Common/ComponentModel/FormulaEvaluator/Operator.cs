﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal abstract class Operator
    {
        public static Operator Create(string operatorCharacter)
        {
            switch (operatorCharacter)
            {
                case "*":
                    return new Multiplication();

                case "/":
                    return new Division();

                case "-":
                    return new Subtraction();

                case "+":
                    return new Addition();

                case "^":
                    return new Power();

                case "<":
                    return new LessThan();

                case ">":
                    return new GreaterThan();

                case "<=":
                    return new LessThanOrEqual();

                case ">=":
                    return new GreaterThanOrEqual();

                case "==":
                    return new Equal();

                case "!=":
                    return new NotEqual();

                case "&&":
                    return new And();

                case "||":
                    return new Or();

                default:
                    throw new ArgumentException(operatorCharacter + " is not a valid operator character.");
            }
        }

        public abstract object Evaluate(object left, object right);
    }
}
