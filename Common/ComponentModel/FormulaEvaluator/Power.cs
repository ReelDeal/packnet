﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal class Power : NumericOperator
    {
        public override object Evaluate(double baseValue, double exponent)
        {
            return Math.Pow(baseValue, exponent);
        }
    }
}
