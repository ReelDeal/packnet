﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class And : BooleanOperator
    {
        public override object Evaluate(bool left, bool right)
        {
            return left && right;
        }
    }
}
