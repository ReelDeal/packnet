﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal abstract class NumericOperator : Operator
    {
        public override object Evaluate(object left, object right)
        {
            if (left is double && right is double)
            {
                return Evaluate((double)left, (double)right);
            }

            throw new ArgumentException("Argument type not accepted");
        }

        public abstract object Evaluate(double left, double right);
    }
}
