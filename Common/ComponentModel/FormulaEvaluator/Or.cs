﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class Or : BooleanOperator
    {
        public override object Evaluate(bool left, bool right)
        {
            return left || right;
        }
    }
}
