﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using Interfaces.DTO;

    public static class FormulaEvaluatorHelper
    {
        public static MicroMeter AsMicroMeter(this string value)
        {
            double x;
            int y;
            if (double.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out x))
                return (MicroMeter)x;

            if (int.TryParse(value, NumberStyles.Float, CultureInfo.InvariantCulture, out y))
                return (MicroMeter) y;

            return null;
        }

        public static MicroMeter Solve(this string formula, IDictionary<string, object> param)
        {
            try
            {
                if (string.IsNullOrEmpty(formula))
                    return (MicroMeter)0;

                return FormulaEvaluator.Solve<MicroMeter>(formula, param);
            }
            catch (Exception e)
            {
                throw new ApplicationException(
                    string.Format(
                        "ZFold line evaluation failed formula:'{0}' with message: {1}",
                        formula, e.Message));
            }
        }
    }
}