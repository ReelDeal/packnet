﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal abstract class Function
    {
        internal static Function Create(string functionName)
        {
            switch (functionName)
            {
                case "SIN":
                    return new Sinus();

                case "COS":
                    return new Cosine();

                case "TAN":
                    return new Tangens();

                case "SQR":
                    return new SquareRoot();

                case "POW":
                    return new OldPower();

                case "!":
                    return new Not();

                default:
                    throw new ArgumentException(functionName + " is not a valid function name.");
            }
        }

        public abstract object Evaluate(object parameter);
    }
}
