﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal class OldPower : NumericFunction
    {
        public override object Evaluate(double parameter)
        {
            return Math.Pow(parameter, 2);
        }
    }
}
