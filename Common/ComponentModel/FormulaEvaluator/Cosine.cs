﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;

    internal class Cosine : NumericFunction
    {
        public override object Evaluate(double parameter)
        {
            return Math.Cos(parameter * Math.PI / 180);
        }
    }
}
