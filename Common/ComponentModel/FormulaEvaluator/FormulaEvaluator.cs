﻿using System.Text.RegularExpressions;

using PackNet.Common.Interfaces;

namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Exceptions;
    using Interfaces.DTO;

    public static class FormulaEvaluator
    {
        #region Fields

        #region Constants
        private const string DoubleQuote = "\"";
        private const string Sinus = "SIN";
        private const string Cosine = "COS";
        private const string Tangens = "TAN";
        private const string SquareRoot = "SQR";
        private const string Addition = "+";
        private const string Subtraction = "-";
        private const string Division = "/";
        private const string Multiplication = "*";
        private const string Power = "^";
        private const string OldPower = "POW";
        private const string LeftParanthesis = "(";
        private const string RightParanthesis = ")";
        private const string LessThan = "<";
        private const string LessThanOrEqual = "<=";
        private const string GreaterThan = ">";
        private const string GreaterThanOrEqual = ">=";
        private const string Not = "!";
        private const string And = "&&";
        private const string Or = "||";
        private const string Equal = "==";
        private const string NotEqual = "!=";

        #endregion

        #region Function and token information

        private static readonly string[] validTokens =
        { 
            Sinus, 
            Cosine, 
            Tangens, 
            SquareRoot, 
            Addition, 
            Division, 
            Multiplication, 
            Power,
            OldPower,
            LeftParanthesis, 
            RightParanthesis, 
            LessThan, 
            GreaterThan, 
            Not, 
            Subtraction,
            And, 
            Or, 
            Equal,
            NotEqual,
            GreaterThanOrEqual, 
            LessThanOrEqual, 
            DoubleQuote
        };

        private static readonly string[] tokensPrecedeMinusMeansNegative =
        {
            Addition,
            Division,
            Multiplication,
            Power,
            OldPower,
            LeftParanthesis,
            LessThan, 
            GreaterThan,
            Subtraction,
            And,
            Or,
            Equal,
            NotEqual,
            GreaterThanOrEqual,
            LessThanOrEqual,
        };

        private static readonly string[] rightAssociativeOperators = { Power, SquareRoot };
        
        private static readonly string[] operators =
        { 
            Addition, 
            Division, 
            Multiplication, 
            Power,  
            LessThan, 
            GreaterThan, 
            Subtraction,
            And, 
            Or, 
            Equal,
            NotEqual,
            GreaterThanOrEqual, 
            LessThanOrEqual,
        };

        private static readonly Dictionary<string, int> precedence = new Dictionary<string, int> {
            { LeftParanthesis, 1 },
            { RightParanthesis, 1 },
            { Or, 2 },
            { And, 3 },
            { Not, 4 },
            { LessThanOrEqual, 6 },
            { GreaterThanOrEqual, 6 },
            { LessThan, 6 },
            { GreaterThan, 6 },
            { NotEqual, 5 },
            { Equal, 5 },
            { Addition, 7 },
            { Subtraction, 7 }, 
            { Multiplication, 8 },
            { Division, 8 },
            { SquareRoot, 9 },
            { Power, 9 },
            { OldPower, 9 },
            { Cosine, 10 },
            { Sinus, 10 },
            { Tangens, 10 },
        };

        #endregion
        
        private static NumberFormatInfo numberFormatInfo;
        
        #endregion

        #region Constructor

        static FormulaEvaluator()
        {
            // Make sure a dot (.) is always used as decimal separator, regardless of system locale:
            // http://www.codeproject.com/csharp/floatparse.asp?msg=1507032
            CultureInfo ci = CultureInfo.InstalledUICulture;
            numberFormatInfo = (NumberFormatInfo)ci.NumberFormat.Clone();
            numberFormatInfo.NumberDecimalSeparator = ".";
        }

        #endregion

        #region Methods

        public static void Validate(string infix)
        {
            if (string.IsNullOrEmpty(infix))
            {
                return;
            }

            if (infix.StartsWith("+") || infix.StartsWith("-"))
            {
                infix = infix.Insert(0, "0");
            }

            Stack<string> output = new Stack<string>();
            List<string> tokensInRPN = ToRPN(infix);

            foreach (string token in tokensInRPN)
            {
                if (validTokens.Contains(token))
                {
                    if (operators.Contains(token))
                    {
                        if (output.Count < 2)
                        {
                            throw new InvalidFormulaException("To few parameters in formula: " + infix);
                        }

                        output.Pop();
                    }
                    else
                    {
                        if (output.Count < 1)
                        {
                            throw new InvalidFormulaException("To few parameters in formula: " + infix);
                        }
                    }
                }
                else
                {
                    output.Push(token);
                }
            }
        }

        /// <summary>
        /// Given some equation try to solve it based on parameters and the string represetation of the equation.
        /// ***When compaing with string values white spaces will be removed and case will not be considered.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="infix"></param>
        /// <returns></returns>
        public static T Solve<T>(string infix) where T : struct
        {
            return Solve<T>(infix, new Dictionary<string, object>());
        }

        /// <summary>
        /// Given some equation try to solve it based on parameters and the string represetation of the equation.
        /// ***When compaing with string values white spaces will be removed and case will not be considered.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="infix"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static T Solve<T>(string infix, IDictionary<string, object> values) where T : struct
        {
            if (infix == string.Empty || infix.StartsWith("+") || infix.StartsWith("-"))
            {
                infix = infix.Insert(0, "0");
            }

            infix = infix.Replace(" ", "" );

            Stack<object> output = new Stack<object>();
            
            List<string> tokensInRPN = ToRPN(infix);
            
            foreach (string token in tokensInRPN)
            {
                if (validTokens.Contains(token))
                {
                    if (operators.Contains(token))
                    {
                        if (output.Count < 2)
                        {
                            throw new ArgumentException("To few parameters");
                        }

                        object arg2 = output.Pop();
                        object arg1 = output.Pop();
                        Operator op = Operator.Create(token);
                        output.Push(op.Evaluate(arg1, arg2));
                    }
                    else
                    {
                        if (output.Count < 1)
                        {
                            throw new ArgumentException("To few parameters");
                        }

                        object arg = output.Pop();
                        Function func = Function.Create(token);
                        output.Push(func.Evaluate(arg));
                    }
                }
                else
                {
                    output.Push(SolveLeaf(token, values));
                }
            }

            object value = output.Pop();
            if (typeof(T) == typeof(MicroMeter))
            {
                return (T)(object)new MicroMeter((double)value);
            }
            
            return (T)(object)value;
        }

        #endregion

        #region Implementation

        private static object SolveLeaf(string token, IDictionary<string, object> values)
        {
            // Parse leaf, which can be one of following:
            //    Parameter only, e.g. L
            //    Number only, e.g. 0.5
            //    Number and Parameter, e.g. 0.5L
            token = token.Replace(',', '.');
            string paramPart = string.Empty;
            string numPart = string.Empty;
            string stringPart = string.Empty;

            bool paramStarted = false;
            bool stringStarted = false;
            int negativeMultiplier = 1;

            foreach (char current in token)
            {
                if (stringStarted)
                {
                    if (current == '"')
                    {
                        stringStarted = false;
                    }
                    else
                    {
                        stringPart += current.ToString();
                    }
                }
                else if (current == '-')
                {
                    negativeMultiplier = -1;
                }
                else if (current == '"')
                {
                    stringStarted = true;
                }
                else if ((current >= '0' && current <= '9') || current == '.')
                {
                    if (paramStarted)
                    {
                        paramPart += current.ToString();
                    }
                    else
                    {
                        numPart += current.ToString();
                    }
                }
                else if ((current >= 'A' && current <= 'Z') || (current >= 'a' && current <= 'z'))
                {
                    if (!paramStarted)
                    {
                        paramStarted = true;
                    }

                    paramPart += current.ToString();
                }
                else if(current != ' ')
                {
                    throw new ArgumentException("Invalid Parameter Exception (current is : " + current + ")");
                }
            }

            double num = numPart == string.Empty ? 1 : double.Parse(numPart, numberFormatInfo);
            double doubleParam;

            if (string.IsNullOrEmpty(stringPart) == false)
            {
                return stringPart;
            }
            if (string.IsNullOrEmpty(paramPart))
            {
                doubleParam = 1;
            }
            else
            {
                var param = GetParameterActualValue(values, paramPart);
                if (param is string)
                {
                    return ((string)param).Replace(" ", string.Empty);
                }
                if (param is int)
                {
                    doubleParam = Convert.ToDouble(param);
                }
                else if(param is MicroMeter)
                {
                    doubleParam = (MicroMeter)Convert.ChangeType(param, typeof(MicroMeter));

                }
                else
                    doubleParam = (double)param;
            }

            return num * doubleParam * negativeMultiplier;
        }

        private static object GetParameterActualValue(IDictionary<string, object> values, string parameter)
        {
            object parameterValue;
            values.TryGetValue(parameter, out parameterValue);

            if (parameterValue == null)
            {
                throw new ArgumentException("Invalid Parameter Exception");
            }

            return parameterValue;
            
        }

        private static List<string> ToRPN(string infix)
        {
            List<string> tokens = Tokenize(infix);
            List<string> output = new List<string>();
            Stack<string> operandStack = new Stack<string>();
            for (int i = 0; i < tokens.Count; i++)
            {
                if (validTokens.Contains(tokens[i]))
                {
                    switch (tokens[i])
                    {
                        case LeftParanthesis:
                            operandStack.Push(tokens[i]);
                            break;
                        case RightParanthesis:
                            while (operandStack.Peek() != LeftParanthesis)
                            {
                                output.Add(operandStack.Pop());
                            }

                            operandStack.Pop();
                            break;
                        default:
                            if (operandStack.Count > 0
                                && ((!rightAssociativeOperators.Contains(tokens[i]) && precedence[tokens[i]] <= precedence[operandStack.Peek()])
                                || (rightAssociativeOperators.Contains(tokens[i]) && precedence[tokens[i]] < precedence[operandStack.Peek()])))
                            {
                                output.Add(operandStack.Pop());
                                i--;
                            }
                            else
                            {
                                operandStack.Push(tokens[i]);
                            }

                            break;
                    }
                }
                else
                {
                    output.Add(tokens[i]);
                }
            }

            while (operandStack.Count > 0)
            {
                output.Add(operandStack.Pop());
            }

            return output;
        }

        private static List<string> Tokenize(string infix)
        {
            List<string> tokens = new List<string>();
            int startPos = 0;
            int i = 0;
            while (i < infix.Length)
            {
                string token = GetNextOperator(infix, i);
                if (token != null)
                {
                    if (startPos != i)
                    {
                        tokens.Add(infix.Substring(startPos, i - startPos));
                    }
                    
                    tokens.Add(token.ToUpper());
                    i += token.Length;
                    startPos = i;
                }
                else
                {
                    i++;
                }
            }

            if (startPos != infix.Length)
            {
                tokens.Add(infix.Substring(startPos, infix.Length - startPos));
            }

            return tokens;
        }

        private static string GetNextOperator(string infix, int i)
        {
            //if (infix[i] == '\"')
            //{
            //    return null;
            //}
            // Since - is both an operator and a negative sign the operator needs to be handled special
            if (infix[i] == '-')
            {
                if (IsNegativeSign(infix, i))
                {
                    return null;
                }
                return new string(infix[i], 1);
            }
            if (i + 2 < infix.Length
                && validTokens.Contains(infix.Substring(i, 3).ToUpper()))
            {
                return infix.Substring(i, 3);
            }
            if (i + 1 < infix.Length
                && validTokens.Contains(infix.Substring(i, 2)))
            {
                return infix.Substring(i, 2);
            }
            if (DoubleQuote.Contains(infix.Substring(i, 1)))
            {
                var closingDoubleQuote = infix.IndexOf(DoubleQuote, i + 1);
                if (closingDoubleQuote < 1)
                    throw new InvalidFormulaException("Formula does not have ending double quote for literal comparison: " + infix);

                return infix.Substring(i, closingDoubleQuote - (i - 1));
            }
            if (validTokens.Contains(infix.Substring(i, 1)))
            {
                return new string(infix[i], 1);
            }

            return null;
        }

        private static bool IsNegativeSign(string infix, int i)
        {
            return i - 1 < 0
                || (i > 1 && tokensPrecedeMinusMeansNegative.Contains(infix.Substring(i - 2, 2))
                || (i > 0 && tokensPrecedeMinusMeansNegative.Contains(infix.Substring(i - 1, 1))));
        }

        /// <summary>
        /// Returns all parameters used in a given formula, does not remove duplicates
        /// </summary>
        /// <param name="formula"></param>
        /// <returns>IEnumerable with used parameters as strings</returns>
        public static IEnumerable<string> GetFormulaParameters(string formula)
        {
            var splitters = new string[]
            {
                DoubleQuote, Sinus, Cosine, Tangens, SquareRoot, Addition, Subtraction, Division,
                Multiplication, Power, OldPower, LeftParanthesis, RightParanthesis, LessThan,
                LessThanOrEqual, GreaterThan, GreaterThanOrEqual, Not, And, Or, Equal, NotEqual
            };

            var XvalRegex = new Regex("[X][0-9]*$");
            var splitedFormula = formula.Split(splitters, StringSplitOptions.None);

            var xPara = new List<string>();

            splitedFormula.Where(parameter =>
                XvalRegex.Match(parameter).Success).ForEach(p => xPara.Add(p.Substring(p.IndexOf("X"))));

            var parameters = splitedFormula.Where(parameter => parameter.Equals("L")
                || parameter.Equals("W")
                || parameter.Equals("H")
                || parameter.Equals("FT")
                || parameter.Equals("FW"));
            
            return parameters.Concat(xPara);
        }

        #endregion
    }
}
