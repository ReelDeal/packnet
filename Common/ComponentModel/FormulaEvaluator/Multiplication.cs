﻿namespace PackNet.Common.ComponentModel.FormulaEvaluator
{
    internal class Multiplication : NumericOperator
    {
        public override object Evaluate(double left, double right)
        {
            return left * right;
        }
    }
}
