﻿namespace PackNet.Common.ComponentModel.Configuration
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Threading;
    using System.Xml;

    [DataContract(Name = "XmlConfiguration", Namespace = "http://Packsize.com")]
    public class XmlConfiguration<T>
    {
        public static T Deserialize(string path)
        {
            return Deserialize(path, Type.EmptyTypes);
        }

        public static T Deserialize(string path, IEnumerable<Type> knownTypes)
        {
            var stream = new FileStream(path, FileMode.OpenOrCreate);
            return DeserializeFromStream(stream, knownTypes);
        }

        public static T DeserializeFromStream(Stream stream, IEnumerable<Type> knownTypes)
        {
            var originalCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            try
            {
                var reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas());
                var serializer = new DataContractSerializer(typeof(T), knownTypes);
                var supplier = (T)serializer.ReadObject(reader, false);
                return supplier;
            }
            finally
            {
                stream.Close();
                Thread.CurrentThread.CurrentCulture = originalCulture;
            }
        }

        public void Serialize(string path)
        {
            var tempFile = Path.Combine(Path.GetTempPath(), Path.GetTempFileName());
            var currentCulture = Thread.CurrentThread.CurrentCulture;

            using (var fileStream = new FileStream(tempFile, FileMode.Create))
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                try
                {
                    SerializeToStream(fileStream);

                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = currentCulture;
                }
            }

            File.Copy(tempFile, path, true);
        }

        public void SerializeToStream(Stream stream)
        {
            var xmlWriterSettings = new XmlWriterSettings { Indent = true, NewLineHandling = NewLineHandling.Entitize };
            var xmlWriter = XmlWriter.Create(stream, xmlWriterSettings);
            var serializer = new DataContractSerializer(typeof(T));
            serializer.WriteObject(xmlWriter, this);
            xmlWriter.Flush();
        }
    }
}
