﻿using System;
using System.Collections.Generic;
using System.IO;

using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO.PrintingMachines;

namespace PackNet.Common.Printing
{
    public class PrintableLabelFormatter : IPrintableLabelFormatter
    {
        private const string BarcodeLabelFormat = "^XA^FO40,50^BY3^A0N,100,80^BCN,200,Y,N,N^FD{0}^FS^XZ";
        private const string PlainTextLabelFormat = "^XA^FO50,200^ADN100,30^FD{0}^FS^XZ";
        
        /// <summary>
        /// Template label read from .\Data\Labels\Template.prn. Internal modifier for testing.
        /// </summary>
        public static string TemplateLabel;

        static PrintableLabelFormatter()
        {
            ReadTemplateLabel();
        }

        /// <summary>
        /// Takes a printable and returns a ZPL label formatted based on the printable format. Only accepts a dictionary&lt;string,string&gt;
        /// </summary>
        /// <param name="printable">Item to print</param>
        /// <returns>String representation of the printable in ZPL format, empty string if undefined format</returns>
        /// <exception cref="ArgumentException">Thrown when non-numeric data given</exception>
        public string ConvertToPrintableFormat(Printable printable)
        {
            if (printable == null)
            {
                throw new ArgumentException("null printable given");
            }

            if (printable.PrintData == null)
            {
                throw new ArgumentException("printable with null data given");
            }

            // todo: how do we handle labels configured with a barcode that does not accept special characters moving forward? 
            //// check that the printable has valid data (the original label used was configured with a barcode that did not accept non-alphanumeric characters)
            //var r = new Regex("^[a-zA-Z0-9]*$");
            //if (!r.IsMatch(printable.PrintData.ToString()))
            //{
            //    throw new ArgumentException("this formatter only accepts alphanumeric data");
            //}

            return TemplateLabel.Inject((Dictionary<string, string>)printable.PrintData);
        }

        private static void ReadTemplateLabel()
        {
            var templateLabelPath = Path.Combine(Environment.CurrentDirectory, "Data\\Labels\\Template.prn");
            var fileInfo = new FileInfo(templateLabelPath);
            if (!fileInfo.Exists) throw new ApplicationException(string.Format("Template.prn does not exist in {0}", templateLabelPath));

            using (var sr = new StreamReader(fileInfo.Open(FileMode.Open, FileAccess.Read)))
            {
                TemplateLabel = sr.ReadToEnd();
            }
        }
    }
}