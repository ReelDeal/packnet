﻿using System;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;

namespace PackNet.Common.Printing
{
    using System.Diagnostics;
    using System.Drawing;
    using System.Drawing.Printing;
    using System.Reactive.Linq;
    using System.Reactive.Subjects;
    using System.Threading.Tasks;

    using Interfaces.Enums;

    public class WindowsPrinter : BaseStatusReportingMachine<MachineStatuses>, IPrinter
    {
        private readonly PrinterSettings printerSettings;
        private readonly Subject<PrintJobStatus> printJobStatusChangedSubject = new Subject<PrintJobStatus>();

        public Task CurrentTask = null;

        public MachineTypes MachineType { get { return PrintMachineTypes.WindowsPrinter; } }
        
        public string Description { get; set; }

        public void Print(Printable itemToPrint)
        {
            CurrentTask = Task.Factory.StartNew(
                () =>
                {
                    var doc = new PrintDocument { PrintController = new StandardPrintController(), PrinterSettings = printerSettings };
                    doc.PrintPage += (sender, e) => {
                        e.Graphics.DrawString(itemToPrint.PrintData.ToString(), SystemFonts.DefaultFont, Brushes.Black, new PointF(e.PageBounds.Width / 2, e.PageBounds.Height / 2));
                    };
                    doc.EndPrint += (sender, args) =>
                    {
                        printJobStatusChangedSubject.OnNext(
                            new PrintJobStatus
                            {
                                JobStatus = ProducibleStatuses.ProducibleCompleted, //TODO: detect windows print failures
                                PrinterId = Id,
                                ItemToPrint = itemToPrint
                            });
                    };

                    doc.Print();
                });
            CurrentTask.ContinueWith(
                (t) =>
                {
                    if(t.IsFaulted)
                        Debug.WriteLine(":( faulted task: " + t.Exception.Message);
                });
        }

        public IObservable<PrinterStatus> PrinterStatusChangedObservable { get; private set; }

        public IObservable<PrintJobStatus> PrintJobStatusChangedObservable { get { return printJobStatusChangedSubject.AsObservable(); } }

        public WindowsPrinter(string printerName)
        {
            Id = Guid.NewGuid();
            Alias = printerName;
            printerSettings = new PrinterSettings { PrinterName = printerName};
        }
    }
}
