﻿using PackNet.Common.Interfaces.DTO.PrintingMachines;

namespace PackNet.Common.Printing
{
    public interface IPrintableLabelFormatter
    {
        string ConvertToPrintableFormat(Printable printable);
    }
}