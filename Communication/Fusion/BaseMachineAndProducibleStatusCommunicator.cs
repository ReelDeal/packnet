using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Machines;

using Shielded;

namespace PackNet.Communication.Fusion
{
    public class BaseMachineAndProducibleStatusCommunicator
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly IMachine machine;

        public BaseMachineAndProducibleStatusCommunicator(IEventAggregatorPublisher publisher,IMachine machine)
        {
            this.publisher = publisher;
            this.machine = machine;
        }

        protected void SendMachineStatusChangedMessage(MachineStatuses newStatus)
        {
            if (machine.CurrentStatus.Value == newStatus)
                return;

            Shield.InTransaction(() => machine.CurrentStatus.Modify((ref MachineStatuses ms) => ms = newStatus));

            //todo: remove this call from here and move to fusionMachine service to listed to fusion machines status observable then publish the machine status message to the UI and/or EA
            var message = new Message<IMachine>
            {
                MessageType = MachineMessages.MachineStatusChanged,
                Data = machine
            };
            publisher.Publish(message);
        }

        protected void SendMachineProductionStatusChangedMessage(MachineProductionStatuses newStatus)
        {
            if (machine.CurrentProductionStatus.Value == newStatus)
                return;

            Shield.InTransaction<MachineProductionStatuses>(() => machine.CurrentProductionStatus.Value = newStatus);
        }
    }
}