﻿namespace PackNet.Communication
{
    public class NotificationVariable
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public NotificationVariable()
        {
        }

        public NotificationVariable(string name)
        {
            Name = name;
        }
    }
}
