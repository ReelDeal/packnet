﻿using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;
using PackNet.Communication.PLCBase;
using PackNet.Communication.Properties;

namespace PackNet.Communication
{
    public class PLCHeartBeatCommunicator : IPLCHeartBeatCommunicator
    {
        private const int DefaultPingTimeInSeconds = 5;
        private const int DefaultConnectTimeInSeconds = 3;

        private long currentPcLifeSign;
        private bool? lastIsConnectedStatusSent;
        private bool isConnected;
        private bool disposed;
        private readonly SimplePLCCommunicator simpleCommunicator;
        private readonly Timer pingTimer;
        private readonly Timer connectionTimer;
        private readonly ILogger logger;
        private readonly IProviderCommunicatorVariableMap variableMap;
        private readonly IEventAggregatorPublisher publisher;

        public PLCHeartBeatCommunicator(IWebRequestCreator webRequestCreator, IProviderCommunicatorVariableMap variableMap, IEventAggregatorPublisher publisher, ILogger logger, TimeSpan pingTime = default(TimeSpan), TimeSpan connectRetryTime = default(TimeSpan))
        {
            this.logger = logger;
            this.variableMap = variableMap;
            this.publisher = publisher;
            simpleCommunicator = new SimplePLCCommunicator(webRequestCreator, logger);

            if (pingTime == default(TimeSpan))
                pingTime = TimeSpan.FromSeconds(DefaultPingTimeInSeconds);

            if (connectRetryTime == default(TimeSpan))
                connectRetryTime = TimeSpan.FromSeconds(DefaultConnectTimeInSeconds);

            pingTimer = new Timer("pingTimer for " + simpleCommunicator.PlcIpAddressWithPort, VerifyMachineCommunication, pingTime);
            connectionTimer = new Timer("connectionTimer for " + simpleCommunicator.PlcIpAddressWithPort, Connect, connectRetryTime);
            connectionTimer.Start();
        }

        public bool IsConnected
        {
            get { return isConnected; }
            set
            {
                isConnected = value;

                //It's populated and without change - we're done
                if (lastIsConnectedStatusSent.HasValue && lastIsConnectedStatusSent.Value == value)
                {
                    return;
                }

                lastIsConnectedStatusSent = value;
                PublishConnectionStatus();
            }
        }

        public void WriteValue<T>(PacksizePlcVariable variable, T value, int retryCount = -1)
        {
            logger.Log(LogLevel.Trace, "WriteValue<T>(PacksizePlcVariable {0}, T {1}, {2})", variable, value, retryCount);

            if (!IsConnected)
            {
                logger.Log(LogLevel.Trace, "Machine is not connected, Can't write value");
                return;
            }

            try
            {
                simpleCommunicator.WriteValue(variable, value, retryCount);
            }
            catch (ConnectionException)
            {
                SignalReconnect();
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, exception.ToString());
            }
        }

        public T ReadValue<T>(PacksizePlcVariable variable, int retryCount = -1)
        {
            logger.Log(LogLevel.Trace, "ReadValue<T>(PacksizePlcVariable {0}, T {1})", variable, retryCount);

            try
            {
                return simpleCommunicator.ReadValue<T>(variable, retryCount);
            }
            catch (ConnectionException)
            {
                SignalReconnect();
                throw;
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Error, exception.ToString());
                return default(T);
            }
        }

        public bool UpdateWebRequestCreator(IWebRequestCreator newWebRequestCreator)
        {
            if (simpleCommunicator.UpdateWebRequestCreator(newWebRequestCreator))
            {
                SignalReconnect();
                return true;
            }

            return false;
        }

        public string PlcIpAddressWithPort { get { return simpleCommunicator.PlcIpAddressWithPort; } }

        private void Connect()
        {
            logger.Log(LogLevel.Trace, "Connect()");
            connectionTimer.Stop();
            pingTimer.Stop();

            try
            {
                simpleCommunicator.ReadValue<bool>(variableMap.ConnectionStatus, Settings.Default.MachineConnectRetryCount);
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Trace, "Failed to connect to {0}: {1}", PlcIpAddressWithPort, exception);
                //Restart the timer connect in the HandleFailedConnection method - not all over the place.
                HandleFailedConnection();
                return;
            }

            logger.Log(LogLevel.Debug, "Connected to machine " + PlcIpAddressWithPort);
            pingTimer.Start(); //leave the connect timer OFF but start the ping timer
            IsConnected = true;
            RegisterNotificationVariables();
            logger.Log(LogLevel.Debug, "Ping timer started for machine " + PlcIpAddressWithPort);
        }

        private void HandleFailedConnection()
        {
            logger.Log(LogLevel.Trace, "connectionTimer.Start()");
            connectionTimer.Start();
            IsConnected = false;
        }

        private void PublishConnectionStatus()
        {
            logger.Log(LogLevel.Trace, "PublishConnectionStatus()");

            var message = new Message<Tuple<string, bool>>
            {
                MessageType = SimplePLCMessages.SimplePLCConnectionStatus,
                Data = new Tuple<string, bool>(PlcIpAddressWithPort, IsConnected)
            };
            logger.Log(LogLevel.Trace, "Connection status: {0} IsConnected = {1}", PlcIpAddressWithPort, IsConnected);
            publisher.Publish(message);
        }

        /// <summary>
        /// Signals the connect timer to start running
        /// </summary>
        private void SignalReconnect()
        {
            logger.Log(LogLevel.Trace, "SignalReconnect()");
            IsConnected = false;
            pingTimer.Stop();
            connectionTimer.Start();
        }

        private void RegisterNotificationVariables()
        {
            logger.Log(LogLevel.Trace, "RegisterNotificationVariables()");
            const string colon = ":";

            List<NotificationVariable> notificationVariables = variableMap.AsEnumerable()
                .Where(plcVar => plcVar != null && plcVar.NotifyChanges)
                .Select(plcVar => new NotificationVariable(string.Concat(plcVar.TaskName, colon, plcVar.VariableName))).ToList();

            WriteValue(variableMap.EventNotificationRegistrationBuffer, notificationVariables);
            WriteValue(variableMap.EventNotificationVariablesCounter, notificationVariables.Count);
        }

        /// <summary>
        /// Issues a write command to verify that we can still communicate... Signals the Connect timer to run if communication fails
        /// </summary>
        private void VerifyMachineCommunication()
        {
            connectionTimer.Stop(); //Make sure the connection timer is not running
            logger.Log(LogLevel.Trace, "VerifyMachineCommunication()");

            try
            {
                WriteValue(variableMap.PcLifeSign, currentPcLifeSign++, Settings.Default.MachineVerifyConnectRetryCount);
            }
            catch (Exception exception)
            {
                logger.Log(LogLevel.Warning, "PLC connection failed for {0}: {1}", PlcIpAddressWithPort, exception);
                SignalReconnect();
            }
        }

        public void Dispose()
        {
            if (disposed) return;

            disposed = true;

            IsConnected = false;

            if (pingTimer != null)
            {
                pingTimer.Stop();
                pingTimer.Dispose();
            }

            if (connectionTimer != null)
            {
                connectionTimer.Stop();
                connectionTimer.Dispose();
            }
        }
    }
}
