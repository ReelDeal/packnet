using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Machines;



namespace PackNet.Communication.Communicators
{
    public class BaseMachineAndProducibleStatusCommunicator
    {
        private readonly IEventAggregatorPublisher publisher;
        private readonly IMachine machine;

        public BaseMachineAndProducibleStatusCommunicator(IEventAggregatorPublisher publisher, IMachine machine)
        {
            this.publisher = publisher;
            this.machine = machine;
        }

        protected void SendMachineStatusChangedMessage(MachineStatuses newStatus)
        {
            if (machine.CurrentStatus == newStatus)
                return;

            machine.CurrentStatus = newStatus;
        }

        protected void SendMachineProductionStatusChangedMessage(MachineProductionStatuses newStatus)
        {
            if (machine.CurrentProductionStatus == newStatus)
                return;

            machine.CurrentProductionStatus = newStatus;
        }
    }
}