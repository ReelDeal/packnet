﻿namespace PackNet.Communication.Communicators
{
    public class PlcMachineError
    {
        public ushort Code { get; set; }

        public double[] Arguments { get; set; }

        public int Position { get; set; }
    }
}
