﻿using System;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Exceptions;

namespace PackNet.Communication.Communicators
{
    public abstract class MachineCommunicatorPlcVariableHandler : IDisposable
    {
        protected readonly IPLCHeartBeatCommunicator Communicator;

        protected MachineCommunicatorPlcVariableHandler(IPLCHeartBeatCommunicator communicator)
        {
            Communicator = communicator;
        }

        /// <summary>
        /// Set variable in machine
        /// </summary>
        /// <param name="variable"></param>
        /// <param name="value"></param>
        /// <exception cref="ConnectionException">Can happen if connection is lost or timed out.</exception>
        public void SetVariableValue<T>(PacksizePlcVariable variable, T value)
        {
            Communicator.WriteValue(variable, value);
        }

        public T GetVariableValue<T>(PacksizePlcVariable variable)
        {
            return Communicator.ReadValue<T>(variable);
        }

        public void UpdateWebRequestCreator(IWebRequestCreator newWebRequestCreator)
        {
            Communicator.UpdateWebRequestCreator(newWebRequestCreator);
        }
        
        
        public  void Dispose()
        {
            if (Communicator != null)
                Communicator.Dispose();
        }
    }
}
