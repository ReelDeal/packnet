﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Communication.Communicators.Fusion;
using PackNet.Communication.PLCBase;
using PackNet.Communication.Properties;
using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace PackNet.Communication.Communicators
{
    public abstract class PacksizeMachineCommunicatorBase : PacksizeMachineErrorHandler, IDisposable
    {
        protected readonly IEventAggregatorPublisher publisher;

        private readonly IDisposable variableChangedDisposable;
        private readonly IDisposable plcConnectDisconnectDisposable;
        private readonly IPackagingMachineVariableMap variableMap;
        private readonly ILogger logger;
        protected IPacksizeCarton Order;
        private object orderLock = new object();
        private readonly Stopwatch dispatchToStartedStopwatch;
        private readonly IDisposable releaseBoxDisposible;

        protected IPacksizeCutCreaseMachine machine;
        private MachineStatuses lastStatus;

        protected PacksizeMachineCommunicatorBase(IPacksizeCutCreaseMachine machine, 
            IEventAggregatorPublisher publisher, 
            IEventAggregatorSubscriber subscriber, 
            IPLCHeartBeatCommunicator communicator, 
            IPackagingMachineVariableMap variableMap, 
            ILogger logger)
            : base(variableMap, communicator)
        {

            this.machine = machine;
            this.publisher = publisher;
            this.variableMap = variableMap;
            this.logger = logger;

            //Listen for a message to release the box we are working on right now.
            releaseBoxDisposible = subscriber.GetEvent<Message<IProducible>>()
                .Where(m => m.MessageType == CartonMessages.ReleaseBox && Order != null && m.Data.Id == Order.Id)
                .DurableSubscribe(m => ReleaseBox(), logger);

            //WARNING: VariableChanged is fired from the rest service... It always needs to be on a new thread
            variableChangedDisposable = subscriber.GetEvent<Message<VariableChangedEventArgs>>()
                .Where(m => m.Data.Address == Communicator.PlcIpAddressWithPort)
                .ObserveOn(NewThreadScheduler.Default)//WARNING: VariableChanged is fired from the rest service... It always needs to be on a new thread
                .DurableSubscribe(m => OnVariableChanged(m.Data), logger);

            //PLC connect/disconnect handler
            plcConnectDisconnectDisposable = subscriber.GetEvent<Message<Tuple<string, bool>>>()
                .Where(m => m.MessageType == SimplePLCMessages.SimplePLCConnectionStatus &&
                 m.Data.Item1 == Communicator.PlcIpAddressWithPort)
                 .Subscribe(m =>
                 {

                     if (m.Data.Item2)
                     {
                         //TODO: This appears to be a remnant of the old coomunication... The sim doesn't work without it. We need to figure out if it is needed for the real machine
                         SynchronizeMachine();
                     }

                     // Calling MachineIsInError() when the machine is not yet connected causes a ConnectionExcewption in PLCHeartBeat which causes this subscriber to dispose and we lose further machine styatus updates
                     // Could fix by swallowing connectionexcewptionns in here, or making this a durable subscribe. See bug 10553.

                     //var machineErrorOrInit = MachineIsInError() ? MachineErrorStatuses.MachineError : MachineStatuses.MachineInitializing;
                     //logger.Log(LogLevel.Debug, "SimplePLCConnectionStatus: " + machineErrorOrInit + " connected status" + m.Data.Item2);
                     //UpdateMachineStatus(m.Data.Item2 ? machineErrorOrInit : MachineStatuses.MachineOffline);

                     UpdateMachineStatus(m.Data.Item2 ? MachineIsInError() ? MachineErrorStatuses.MachineError : MachineStatuses.MachineInitializing : MachineStatuses.MachineOffline);

                 });

            subscriber.GetEvent<IMessage<PackNetServerSettings>>()
                .Where(m => m.MessageType == PackNetServerSettingsMessages.ServerSettingsUpdated)
                .DurableSubscribe(msg =>
                {
                    var task = Task.Factory.StartNew(() => UpdateEventNotifier(msg));
                    task.ContinueWith((t) =>
                    {
                        if (t.IsFaulted)
                            logger.Log(LogLevel.Error, "Error: Failed to update system settings " + t.Exception.Message);
                    });
                }, logger);

            
            dispatchToStartedStopwatch = new Stopwatch();
        }

        protected bool WaitUntilReadyToSynchronize(bool isReloadMachineConfig)
        {
            var sleepCounter = 0;

            if (isReloadMachineConfig == false)
            {
                return true;
            }

            SetMachinePause(true);
            while (!GetIsMachinePaused() && !GetIsMachineInErrorState())
            {
                Thread.Sleep(500);
                sleepCounter++;

                if (sleepCounter > 30)
                {
                    throw new MachineConfigurationException()
                    {
                        ErrorMessageType = MachineMessages.UnableToPauseMachine,
                        MachineAlias = machine.Alias,
                        TrackNumbers = new ConcurrentList<int>()
                    };
                }
            }

            return true;
        }

        private void UpdateEventNotifier(IMessage<PackNetServerSettings> msg)
        {
            if (msg.Data != null)
            {
                //Only update machines if callback has changed
                if (machine.PhysicalMachineSettings.EventNotifierIp.Address == msg.Data.MachineCallbackIpAddress &&
                    machine.PhysicalMachineSettings.EventNotifierIp.Port == msg.Data.MachineCallbackPort)
                {
                    return;
                }

                Networking.CheckIpAddressAndWarnIfNotBoundToThisMachine(msg.Data.MachineCallbackIpAddress, logger);
                machine.PhysicalMachineSettings.EventNotifierIp.Address = msg.Data.MachineCallbackIpAddress;
                machine.PhysicalMachineSettings.EventNotifierIp.Port = msg.Data.MachineCallbackPort;
                SetEventNotificationParameters(machine.PhysicalMachineSettings.EventNotifierIp);
            }
        }

        private bool MachineIsInError()
        {
            var machineError = GetMachineError();

            if (machineError == null)
            {
                machine.Errors.Clear();
                return false;
            }

            machine.Errors.Clear();
            machine.Errors.Add(ErrorFactory.Create(machineError.Code, machineError.Arguments, machineError.Position));

            logger.Log(LogLevel.Warning, "Machine " + machine + " is in error state: " + String.Join(", ", machine.Errors));

            return true;
        }

        public bool IsConnected { get { return Communicator.IsConnected; } }

        public abstract void SynchronizeMachine(bool isReloadMachineConfig = false);

        protected void UpdateMachinePlcVersion()
        {
            machine.PlcVersion = Communicator.ReadValue<string>(variableMap.MachinePlcVersion);
        }
        
        protected void SetUnitsOfMeasurement(string unit)
        {
            try
            {
                SetVariableValue(variableMap.IsInches, unit.ToLower().Equals("inch"));
            }
            catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
            {
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
        }
        
        public void SetCorrugateWidths(IEnumerable<Track> tracks)
        {
            try
            {
                SetVariableValue(variableMap.TrackWidths, tracks.Select(t => t.LoadedCorrugate == null ? 0.0 : (double)t.LoadedCorrugate.Width).ToArray());
            }
            catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
            {
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
        }

        public virtual void OnVariableChanged(VariableChangedEventArgs e)
        {
            if (e.IsOfType(variableMap.StartupProcedureCompleted))
            {
                SetMachineStatusBasedOnErrorStatus();
                InvokeStartupProcedureDoneEvent(e);
            }
            else if (ChangedVariableIsAnErrorCode(e))
            {                
                var errorCode = Convert.ToUInt16(e.Value);
                if (errorCode == 0)
                {
                    if (machine.CurrentStatus == MachinePausedStatuses.LightBarrierBroken)
                    {
                        machine.Errors.Clear();
                        UpdateMachineStatus(lastStatus);
                        return;
                    }

                    if (machine.CurrentStatus != MachineErrorStatuses.MachineError &&
                        machine.CurrentStatus != MachineErrorStatuses.EmergencyStop)
                    {
                        return;
                    }

                    SetMachineStatusBasedOnErrorStatus();
                }
                else
                {
                    lastStatus = machine.CurrentStatus;
                    var error = ErrorFactory.Create(errorCode, GetErrorArguments(e.VariableName), GetErrorPositionInList(e.VariableName));

                    machine.Errors.Clear();
                    machine.Errors.Add(ErrorFactory.Create(errorCode, GetErrorArguments(e.VariableName), GetErrorPositionInList(e.VariableName)));

                    logger.Log(LogLevel.Warning, "Machine " + machine + " is in error state: " + String.Join(", ", machine.Errors));

                    switch ((PacksizeMachineErrorCodes)errorCode)
                    {
                        case PacksizeMachineErrorCodes.EmergencyStop:
                            UpdateMachineStatus(MachineErrorStatuses.EmergencyStop);
                            break;
                        case PacksizeMachineErrorCodes.LightBarrierBroken:
                        case PacksizeMachineErrorCodes.InfeedLightBarrierBroken:
                            UpdateMachineStatus(MachinePausedStatuses.LightBarrierBroken);
                            break;
                        case PacksizeMachineErrorCodes.ConnectionError:
                            UpdateMachineStatus(MachineErrorStatuses.MachineError);
                            if (IsConnected)
                            {
                                SynchronizeMachine();
                            }
                            break;
                        default:
                            UpdateMachineStatus(MachineErrorStatuses.MachineError);
                            break;
                    }
                    
                    LogError(error);
                }
            }
            else if (e.IsOfType(variableMap.MachinePaused))
            {
                if (machine.CurrentStatus != MachineStatuses.MachineOnline &&
                    machine.CurrentStatus != MachinePausedStatuses.MachinePaused)
                {
                    return;
                }

                var newStatus = Convert.ToBoolean(e.Value) ? MachinePausedStatuses.MachinePaused : MachineStatuses.MachineOnline;
                UpdateMachineStatus(newStatus);
            }
            else if (e.IsOfType(variableMap.OutOfCorrugate))
            {
                if (!IsMachineInitializing())
                {
                    SetOutOfCorrugateStatus();
                }
            }
            else if (ChangedVariableIsChangeCorrugateMode(e))
            {
                SetChangeCorrugateStatus(Convert.ToBoolean(e.Value));
            }
            else if (e.IsOfType(variableMap.ProductionHistoryItemId))
            {
                var guid = GuidHelpers.GetGuidFromPlcData(e.Value);

                if (guid.Equals(Guid.Empty))
                {
                    logger.Log(LogLevel.Warning, "Guid from PLC was not convertable. {0} on {1}", e.Value.ToString() ,machine);
                    return;
                }

                NotifyPackagingCompleted(guid);
            }
            else if (e.IsOfType(variableMap.RemovedProductionItemId1) || e.IsOfType(variableMap.RemovedProductionItemId2))
            {
                var guid = GuidHelpers.GetGuidFromPlcData(e.Value);

                if (guid.Equals(Guid.Empty))
                    return;

                NotifyRemovedProductionItem(new List<Guid> { guid });
            }
            else if (e.IsOfType(variableMap.StartedProductionItemId))
            {
                var guid = GuidHelpers.GetGuidFromPlcData(e.Value);

                if (guid.Equals(Guid.Empty))
                    return;

                NotifyStartedProductionItem(guid);
            }
            else if (e.IsOfType(variableMap.WaitingForBoxReleaseId))
            {
                var guid = GuidHelpers.GetGuidFromPlcData(e.Value);

                if (guid.Equals(Guid.Empty))
                    return;

                NotifyWaitingForBoxRelease(guid);
            }
            else if (e.IsOfType(variableMap.InServiceMode)) 
            {
                if (machine.CurrentStatus != MachinePausedStatuses.MachinePaused &&
                    (machine.CurrentStatus is MachineErrorStatuses) == false &&
                    machine.CurrentStatus != MachinePausedStatuses.MachineService)
                    return;

                if (Convert.ToBoolean(e.Value))
                    UpdateMachineStatus(MachinePausedStatuses.MachineService);
                else
                    UpdateMachineStatus(MachineErrorStatuses.MachineError);
            }
            else if (e.IsOfType(variableMap.InCalibrationMode))
            {
                if (machine.CurrentStatus != MachinePausedStatuses.MachineService &&
                    machine.CurrentStatus != MachinePausedStatuses.MachineCalibrating)
                    return;

                UpdateMachineStatus(Convert.ToBoolean(e.Value)
                    ? MachinePausedStatuses.MachineCalibrating
                    : MachinePausedStatuses.MachineService);
            }
            else
            {
                logger.Log(
                    LogLevel.Warning,
                    string.Format(
                        "Unexpected Variable Changed value. VariableChangedEventArgs: VariableName:{0}, Value:{1}, Address:{2}",
                        e.VariableName, e.Value, e.Address));
            }
        }

        private void LogError(MachineError error)
        {
            if (error.Error == PacksizePackagingMachineErrorStatuses.InstructionList)
            {
                var id = GetVariableValue<int[]>(variableMap.FailingInstructionListId);
                var instructionList = GetVariableValue<Int16[]>(variableMap.FailingInstructionList);

                logger.Log(LogLevel.Error,
                    string.Format("Machine Error Occured on Machine {0}, Error: {1}, Id of failing instructionList {2}, Instruction List [{3}]",machine.Alias, error,
                        GuidHelpers.ConvertIntArrayToGuid(id), string.Join(", ", instructionList)));
                return;
            }

            logger.Log(LogLevel.Error, string.Format("Machine Error Occured on Machine {0}: {1}", machine.Alias, error));
        }

        private void SetMachineStatusBasedOnErrorStatus()
        {
            UpdateMachineStatus(MachineIsInError()
                ? machine.Errors.First().Error == MachineErrorStatuses.EmergencyStop
                    ? MachineErrorStatuses.EmergencyStop
                    : MachineErrorStatuses.MachineError
                : MachineStatuses.MachineInitializing);
        }

        private void SetOutOfCorrugateStatus()
        {
            UpdateMachineStatus(PacksizePackagingMachineErrorStatuses.MachineInOutOfCorrugate);
        }

        private void SetChangeCorrugateStatus(bool changingCorrugate)
        {
            UpdateMachineStatus(changingCorrugate ? MachinePausedStatuses.MachineChangingCorrugate : MachinePausedStatuses.MachineInitializing);
        }

        private void InvokeStartupProcedureDoneEvent(VariableChangedEventArgs e)
        {
            if (e == null)
            {
                throw new ArgumentNullException("e");
            }
            var startupValue = Convert.ToBoolean(e.Value);
            if (startupValue)
            {
                GetSettingsFromMachine();

                var message = new Message<IMachine>
                {
                    MessageType = MachineMessages.StartupProcedureCompleted,
                    Data = machine
                };
                publisher.Publish(message);

                logger.Log(LogLevel.Info, "{0} Initialization completed, commanding machine to pause", machine);

                UpdateMachineStatus(IsMachinePaused()
                    ? MachinePausedStatuses.MachinePaused
                    : MachineStatuses.MachineOnline);
            }
            else
            {
                logger.Log(LogLevel.Info, "{0} Initialization requested", machine);
                UpdateMachineStatus(MachineStatuses.MachineInitializing);
            }
        }

        protected abstract void GetSettingsFromMachine();

        protected static MicroMeter GetLongHeadToolPositionFromSensorPosition(LongHead longHead, MicroMeter sensorOffset, double sensorPosition)
        {
            return sensorPosition - sensorOffset + longHead.LeftSideToTool;
        }

        public bool IsMachinePaused()
        {
            return GetVariableValue<bool>(variableMap.MachinePaused);
        }

        public bool IsMachineInitializing()
        {
            return !GetVariableValue<bool>(variableMap.StartupProcedureCompleted);
        }

        protected void SendMachineProductionStatusChangedMessage(MachineProductionStatuses newStatus)
        {
            machine.CurrentProductionStatus = newStatus;
        }

        protected void UpdateMachineStatus(MachineStatuses newStatus)
        {
            if (machine.CurrentStatus == newStatus)
                return;

            machine.CurrentStatus = newStatus;
            //fail any job we're working on if we are offline or ran out of corrugate
            if (newStatus == MachineStatuses.MachineOffline || newStatus == PacksizePackagingMachineErrorStatuses.MachineInOutOfCorrugate)
            {
                if (Order != null)
                {
                    lock (orderLock)
                    {
                        logger.Log(LogLevel.Warning, "Machine '{0}' went into '{1}' while item was in progress, Producible '{2}' removed",
                            machine.Alias, newStatus, Order.CustomerUniqueId);
                        Order.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;

                        PublishMachineProductionCompleted(Order, TimeSpan.Zero, 0, PackagingStatus.Failure);

                        Order = null;
                        logger.Log(LogLevel.Warning, "Machine {0} order cleared", machine);
            }
                }
            }

            //todo: remove this call from here and move to fusionMachine service to listed to fusion machines status observable then publish the machine status message to the UI and/or EA
            var message = new Message<IMachine>
            {
                MessageType = MachineMessages.MachineStatusChanged,
                Data = machine
            };
            publisher.Publish(message);
        }

        protected void NotifyStartedProductionItem(Guid id)
        {
            if (Order == null)
            {
                logger.Log(LogLevel.Error, "Machine {0} reported packaging {1} started when unexpected.", machine, id);
                return;
            }

            if (Order.Id != id)
            {
                logger.Log(LogLevel.Error, "Machine {0} reported packaging {1} started when id's do not match: Expected:{2} Received:{1}.", machine, id, Order.Id);
                return;
            }
            dispatchToStartedStopwatch.Stop();
            logger.Log(LogLevel.Debug, "It took {0} ms for the machine {2} to start production of producible: {1}", dispatchToStartedStopwatch.ElapsedMilliseconds, Order, machine);
            SendMachineProductionStatusChangedMessage(MachineProductionInProgressStatuses.ProductionInProgress);
                Order.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
        }

        protected void NotifyWaitingForBoxRelease(Guid id)
        {
            if (Order == null)
            {
                logger.Log(LogLevel.Warning, "Machine {0} reported packaging {1} waiting for box release when unexpected.", machine, id);
                return;
            }
            if (Order.Id.Equals(id))
            {
                Order.ProducibleStatus = InProductionProducibleStatuses.ProducibleWaitingForTrigger;
                logger.Log(LogLevel.Debug, "Machine {1} is reporting that it is waiting for a trigger on producible: {0}", Order, machine);
            }
            else
            {
                logger.Log(LogLevel.Warning, "Machine {1} is reporting that it is waiting for a trigger on producible with Id: {0}, Id does not match Order in Communicator", id, machine);
            }
        }

        protected void NotifyRemovedProductionItem(IEnumerable<Guid> ids)
        {
            if (Order == null)
            {
                logger.Log(LogLevel.Warning, "Machine {0} reported packaging {1} removed when unexpected.", machine, string.Join(", ", ids));
                return;
            }
            if (!ids.Contains(Order.Id))
            {
                logger.Log(LogLevel.Warning, "Machine {0} reported packaging {1} removed when id's do not match: Expected:{2} Received:{1}.", machine, string.Join(", ", ids));
                return;
            }
            Order.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
            SendMachineProductionStatusChangedMessage(MachineProductionStatuses.ProductionIdle);
            ids.ForEach(id =>
            publisher.Publish(new Message<Guid>
            {
                MessageType = ProductionStatusMessages.ProductionCancelled,
                Data = id
            }));
        }

        protected void NotifyPackagingCompleted(Guid id)
        {
            lock (orderLock)
            {
            if (Order == null)
            {
                    logger.Log(LogLevel.Error, "Machine {0} reported packaging {1} complete when unexpected.", machine, id);
                    throw new Exception(String.Format("Machine {0} reported packaging {1} complete when unexpected.", machine, id));
            }

            if (Order.Id != id)
            {
                    logger.Log(LogLevel.Error, "Machine {0} reported packaging {1} complete when id's do not match: Expected:{2} Received:{1}.", machine, id, Order.Id);
                    throw new Exception(String.Format("Machine {0} reported packaging {1} complete when id's do not match: Expected:{2} Received:{1}.", machine, id, Order.Id));
            }

            var corrugateUsage = Communicator.ReadValue<double>(variableMap.ProductionHistoryItemCorrugateUsage);
                var completionTime =
                    TimeSpan.FromMilliseconds(Communicator.ReadValue<long>(variableMap.ProductionHistoryItemCompletionTime));
            
            PackagingStatus completionStatus;
                if (
                    PackagingStatus.TryParse(
                        Communicator.ReadValue<int>(variableMap.ProductionHistoryItemCompletionStatus).ToString(),
                        out completionStatus) == false)
                    logger.Log(LogLevel.Error, String.Format("Unable to parse Completion status of producible {0}", Order.Id),
                        Order);

                logger.Log(LogLevel.Debug, "Producible {0} packaging status is: {1} on machine {2}", Order,
                    completionStatus.ToString(), machine);
            var order = Order;
            Order = null;

                machine.CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;

            order.ProducibleStatus = completionStatus == PackagingStatus.Completed
                ? ProducibleStatuses.ProducibleCompleted
                : ErrorProducibleStatuses.ProducibleFailed;
            
                PublishMachineProductionCompleted(order, completionTime, corrugateUsage, completionStatus);

            SendMachineProductionStatusChangedMessage(MachineProductionStatuses.ProductionCompleted);
            SendMachineProductionStatusChangedMessage(MachineProductionStatuses.ProductionIdle);
        }
        }

        private void PublishMachineProductionCompleted(IPacksizeCarton order, TimeSpan completionTime, double corrugateUsage,
            PackagingStatus completionStatus)
        {
            publisher.Publish(new Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>
            {
                MessageType = MachineMessages.MachineProductionCompleted,
                Data = new Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>(machine, new MachineProductionData
                {
                    Carton = order,
                    ProductionTime = completionTime.TotalMilliseconds,
                    FedLength = corrugateUsage,
                    Corrugate = order.CartonOnCorrugate.Corrugate,
                    Machine = machine,
                    CartonOnCorrugate = order.CartonOnCorrugate,
                    Failure = completionStatus == PackagingStatus.Failure,
                    UserName = machine.AssignedOperator,
                    ProducibleCount = order.CartonOnCorrugate.TileCount,
                    MachineGroup = order.ProducedOnMachineGroupId
                })
            });
        }

        protected bool GetIsMachinePaused()
        {
            return GetVariableValue<bool>(variableMap.MachineInPausedState);
        }

        protected void SetMachinePause(bool pauseMachine)
        {
            SetVariableValue(variableMap.MachinePaused, pauseMachine);
        }

        public void SetNumberOfTracks(int numTracks)
        {
            SetVariableValue(variableMap.NumberOfTracks, numTracks);
        }

        protected bool ChangedVariableIsChangeCorrugateMode(VariableChangedEventArgs e)
        {
            return e.IsOfType(variableMap.ChangeCorrugate);
        }

        public void AddProductionItemToQueue(IProducible producible, short[] instructionList)
        {
            try
            {
                var sw = Stopwatch.StartNew();
                var shouldWaitForBoxRelease =
                    producible.Restrictions.Any(
                        r =>
                            r is BasicRestriction<string> &&
                            ((BasicRestriction<string>)r).Value == CartonRestrictions.ShouldWaitForBoxRelease);
                
                if (Settings.Default.SingleWriteToPlc)
                {
                    Communicator.WriteValue(variableMap.TransferBuffer,
                        new PLCProducible(GuidHelpers.ConvertGuidToIntArray(producible.Id), instructionList,
                            shouldWaitForBoxRelease, true));
                }
                else
                {
                    Communicator.WriteValue(variableMap.TransferBuffer,
                        new PLCProducible(GuidHelpers.ConvertGuidToIntArray(producible.Id), instructionList,
                            shouldWaitForBoxRelease, false));
                    Communicator.WriteValue(variableMap.ReadyForProcessing, true);
                }

                sw.Stop();
                logger.Log(LogLevel.Debug, () => String.Format("It took {0} ms for the machine to receive production info for producible: {1}", sw.ElapsedMilliseconds, producible));
                dispatchToStartedStopwatch.Restart();
            }
            catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
            {
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
            catch (AggregateException)
            {
                //unable to transfer
                var message = String.Format("{1} Failed to add production item {0} to queue.  PLC reporting not that it's not ready for transfer.", producible, machine);
                logger.Log(LogLevel.Error, message);
                throw new ApplicationException(message);
            }
        }

        /// <summary>
        /// Notify the PLC to release the box.
        /// </summary>
        public void ReleaseBox()
        {
            try
            {
                logger.Log(LogLevel.Debug, "Releasing Box - {0}", Order);
                Communicator.WriteValue(variableMap.ReleaseBox, true);
            }
            catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
            {
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
        }

        /// <summary>
        /// Command the machine to pause.  Nothing is done here you will need to wire up to the event MachinePaused in this class.
        /// </summary>
        public void PauseMachine()
        {
            try
            {
                SetVariableValue(variableMap.MachinePaused, true);
            }
            catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
            {
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
        }

        protected void InitVariables()
        {
            try
            {
                SetVariableValue(variableMap.StartupProcedureCompleted, false);
            }
            catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
            {
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
        }

        protected void SetEventNotificationParameters(IPEndPoint eventNotificationParameters)
        {
            SetVariableValue(variableMap.EventNotifierAddress, eventNotificationParameters.Address.ToString());
            SetVariableValue(variableMap.EventNotifierPort, eventNotificationParameters.Port);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public new void Dispose()
        {
            if (plcConnectDisconnectDisposable != null)
                plcConnectDisconnectDisposable.Dispose();

            if (variableChangedDisposable != null)
                variableChangedDisposable.Dispose();

            if (Communicator != null)
                Communicator.Dispose();

            if (releaseBoxDisposible != null)
                releaseBoxDisposible.Dispose();

            base.Dispose();
            logger.Log(LogLevel.Debug, "Communicator " + machine.Alias + " disposed");
        }

        /// <summary>
        /// Produces the specified producible.
        /// </summary>
        /// <param name="producible">The producible.</param>
        /// <param name="instructionList">The instruction list.</param>
        /// <exception cref="System.Exception">Machine can only create IPacksizeCarton (for now)</exception>
        public virtual void Produce(IProducible producible, IEnumerable<IInstructionItem> instructionList)
        {
            if (!IsMachineInitializing())
            {
                lock (orderLock)
                {
                Order = producible as IPacksizeCarton;
                if (Order == null)
                    throw new Exception("Machine can only create IPacksizeCarton (for now)");

                if (Order.TrackNumber <= 0)
                {
                    Order.TrackNumber =
                        machine.Tracks.Where(t => t.LoadedCorrugate.Id == Order.CartonOnCorrugate.Corrugate.Id)
                            .Select(t => t.TrackNumber).Single();
                }
                    logger.Log(LogLevel.Warning, "Sent producible {0} to Machine {1}", Order,
                        machine);
                AddProductionItemToQueue(Order, instructionList.GetPlcInstructionArray());
                Order.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            }
            }
            else
            {
                logger.Log(LogLevel.Warning, "Cannot produce while the machine is initializing");
            }
        }
    }
}