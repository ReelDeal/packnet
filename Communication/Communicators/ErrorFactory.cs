﻿using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;
using System.Globalization;
using System.Linq;

namespace PackNet.Communication.Communicators
{
    public static class ErrorFactory
    {
        public static MachineError Create(ushort errorCode, double[] errorArguments, int getErrorPositionInList)
        {
            var error = new MachineError();
            switch ((PacksizeMachineErrorCodes)errorCode)
            {
                case PacksizeMachineErrorCodes.EmergencyStop:
                    error.Error = MachineErrorStatuses.EmergencyStop;
                    break;
                case PacksizeMachineErrorCodes.RollAxisError:
                    error.Error = PacksizePackagingMachineErrorStatuses.RollAxis;
                    break;
                case PacksizeMachineErrorCodes.ToolAxisError:
                    error.Error = PacksizePackagingMachineErrorStatuses.ToolAxis;
                    break;
                case PacksizeMachineErrorCodes.ConnectionError:
                    error.Error = PacksizePackagingMachineErrorStatuses.ConnectionProblem;
                    break;
                case PacksizeMachineErrorCodes.OutOfCorrugate:
                    error.Error = PackagingMachineErrorStatuses.OutOfCorrugate;
                    break;
                case PacksizeMachineErrorCodes.PaperJam:
                    error.Error = PacksizePackagingMachineErrorStatuses.PaperJam;
                    break;
                case PacksizeMachineErrorCodes.CorrugateMismatch:
                    error.Error = PacksizePackagingMachineErrorStatuses.CorrugateMismatch;
                    break;
                case PacksizeMachineErrorCodes.LongHeadQuantity:
                    error.Error = PacksizePackagingMachineErrorStatuses.LongHeadQuantity;
                    break;
                case PacksizeMachineErrorCodes.LongHeadPosition:
                    error.Error = PacksizePackagingMachineErrorStatuses.LongHeadPosition;
                    break;
                case PacksizeMachineErrorCodes.TrackOffset:
                    error.Error = PacksizePackagingMachineErrorStatuses.TrackOffset;
                    break;
                case PacksizeMachineErrorCodes.TrackQuantity:
                    error.Error = PacksizePackagingMachineErrorStatuses.TrackQuantity;
                    break;
                case PacksizeMachineErrorCodes.InstructionList:
                    error.Error = PacksizePackagingMachineErrorStatuses.InstructionList;
                    break;
                case PacksizeMachineErrorCodes.MachineError:
                    error.Error = MachineErrorStatuses.MachineError;
                    break;
                case PacksizeMachineErrorCodes.AirPressureLow:
                    error.Error = PacksizePackagingMachineErrorStatuses.AirPressureLow;
                    break;
                case PacksizeMachineErrorCodes.MovablePressureRollerOutOfPosition:
                    error.Error = PacksizePackagingMachineErrorStatuses.MovablePressureRollerOutOfPosition;
                    break;
                case PacksizeMachineErrorCodes.LightBarrierBroken:
                    error.Error = MachinePausedStatuses.LightBarrierBroken;
                    break;
                case PacksizeMachineErrorCodes.ReferenceQuantityMissmatch:
                    error.Error = PacksizePackagingMachineErrorStatuses.ReferenceQuantityMissmatch;
                    break;
                case PacksizeMachineErrorCodes.LongHeadPositionOutsideOfTolerance:
                    error.Error = PacksizePackagingMachineErrorStatuses.LongHeadPositionOutsideOfTolerance;
                    break;
                case PacksizeMachineErrorCodes.CrossHeadPositionOutsideOfTolerance:
                    error.Error = PacksizePackagingMachineErrorStatuses.CrossHeadPositionOutsideOfTolerance;
                    break;
                case PacksizeMachineErrorCodes.PositionLatchError:
                    error.Error = PacksizePackagingMachineErrorStatuses.PositionLatchError;
                    break;
                case PacksizeMachineErrorCodes.InfeedLightBarrierBroken:
                    error.Error = MachinePausedStatuses.LightBarrierBroken;
                    break;
                case PacksizeMachineErrorCodes.CorrugateGuideQuantityError:
                    error.Error = PacksizePackagingMachineErrorStatuses.CorrugateGuideQuantity;
                    break;
                default:
                    error.Error = PacksizePackagingMachineErrorStatuses.GetUnknownErrorCode(errorCode);
                    break;
            }

            if (errorArguments != null && errorArguments.Any())
                error.Arguments = errorArguments.Select(a => a.ToString(CultureInfo.InvariantCulture)).ToList();

            return error;

        }
    }
}
