﻿namespace PackNet.Communication.Communicators
{
    //Task names are bounded to a maximum of 10 characters
    public enum PacksizeMachineTasks
    {
        Main,
        PCCommunic,
        VariableEv,
    }

    public enum IqFusionTasks
    {
        PCCommunic,
        Main,
        RestPostSe,
        IOControll,
        VariableEv,
    }

    public enum EmTasks
    {
        Main,
        PCCommunic,
		RestPostSe,
        VariableEv,
    }
}