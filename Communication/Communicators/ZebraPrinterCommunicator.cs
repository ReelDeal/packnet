using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Networking;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ExternalSystems;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.ExternalSystems;
using PackNet.Common.Network;
using PackNet.Common.Utils;
using PackNet.Communication.Properties;

namespace PackNet.Communication.Printer
{
    public class ZebraPrinterCommunicator
    {
        private readonly ZebraPrinter printer;
        private readonly ISocketCommunicatorWithHeartBeat socket;
        private readonly object lockObject = new object();
        private bool connectionAvailableToPrinter = false;
        private readonly ILogger logger;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IServiceLocator serviceLocator;
        private readonly BlockingQueue<IPrintable> workingQueue = new BlockingQueue<IPrintable>(999);
        private CancellationTokenSource workerThreadCancellationToken;

        protected ZebraPrinterCommunicator(ZebraPrinter printer, ILogger logger, IEventAggregatorPublisher publisher, IServiceLocator serviceLocator, ISocketCommunicatorWithHeartBeat socket)
        {
            this.printer = printer;
            this.logger = logger;
            this.publisher = publisher;
            this.serviceLocator = serviceLocator;
            this.socket = socket;

            printer.CurrentStatus = MachineStatuses.MachineOffline;

            SetupHeartBeatSubscription();
        }

        public ZebraPrinterCommunicator(ZebraPrinter printer, ILogger logger, IEventAggregatorPublisher publisher, IServiceLocator serviceLocator)
        {
            this.printer = printer;
            this.logger = logger;
            this.publisher = publisher;
            this.serviceLocator = serviceLocator;

            printer.CurrentStatus = MachineStatuses.MachineOffline;

            socket = new SocketCommunicatorWithHeartBeat(new IPEndPoint(IPAddress.Parse(printer.IpOrDnsName), printer.Port), logger, Settings.Default.PrinterSocketSendTimeout, Settings.Default.PrinterSocketSendTimeout, Settings.Default.PrinterSocketHeartbeatWaitTime, AdditionalHeartBeatCheck);
            SetupHeartBeatSubscription();
        }

        private void SetupHeartBeatSubscription()
        {
            socket.HeartbeatObservable.DurableSubscribe(isAlive =>
            {
                //AdditionalHeartBeatCheck is used to set the machine errors.  
                if (isAlive.Item1 != connectionAvailableToPrinter)
                {
                    connectionAvailableToPrinter = isAlive.Item1;

                    if (connectionAvailableToPrinter)
                    {
                        printer.CurrentStatus = MachineStatuses.MachineInitializing;

                        if (workerThreadCancellationToken != null && !workerThreadCancellationToken.IsCancellationRequested)
                            workerThreadCancellationToken.Cancel();
                        workerThreadCancellationToken = new CancellationTokenSource();
                        SendPrinterSettings();
                        ClearQueue();
                        var task = Task.Factory.StartNew(ExecutePrintLoop);
                        task.ContinueWith(
                                t =>
                                {
                                    if (t.IsFaulted && t.Exception != null)
                                    {
                                        logger.LogException(LogLevel.Error, string.Format("Error ExecutePrintLoop failed.  Producibles:[{0}]", workingQueue.ToString()), t.Exception);
                                    }
                                }, workerThreadCancellationToken.Token, TaskContinuationOptions.OnlyOnFaulted, TaskScheduler.Default);

                        printer.CurrentStatus = MachineStatuses.MachineOnline;
                    }
                    else
                    {
                        try
                        {
                            printer.Errors.Clear();
                            printer.CurrentStatus = MachineStatuses.MachineOffline;

                            ClearQueue();
                            if (workerThreadCancellationToken != null && !workerThreadCancellationToken.IsCancellationRequested)
                                workerThreadCancellationToken.Cancel();
                        }
                        catch (Exception e)
                        {
                            logger.LogException(LogLevel.Error, string.Format("Error while clearing machine queue.", workingQueue.ToString()), e);
                        }
                    }
                    publisher.Publish(new Message<Tuple<Guid, string>>
                    {
                        MessageType = MachineMessages.MachineErrorOccurred,
                        Data = new Tuple<Guid, string>(printer.Id, isAlive.Item2)
                    });
                }
            }, logger);
        }

        private bool AdditionalHeartBeatCheck(SocketCommunicatorWithHeartBeat arg)
        {
            var hs = GetConsitantHostStatus();
            var error = hs.GetErrorStatus();
            if (error != null)
            {
                printer.Errors.Clear();
                printer.Errors.Add(new MachineError(error)); //Set error before status because a callback will be fired
                logger.Log(LogLevel.Warning, "Machine " + printer + " is in error state: " + String.Join(", ", printer.Errors));
                if (printer.CurrentStatus != error)
                    printer.CurrentStatus = error;
                printer.CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;
                
                return true;
            }

            if (!(printer.CurrentProductionStatus is MachineProductionInProgressStatuses))
            {
                var unexpected = hs.GetUnexpectedLabelStatus();
                
                if (unexpected != null)
                {
                    printer.Errors.Clear();
                    printer.Errors.Add(new MachineError(unexpected)); //Set error before status because a callback will be fired
                    logger.Log(LogLevel.Warning, "Machine " + printer + " is in error state: " + String.Join(", ", printer.Errors));
                    if (printer.CurrentStatus != unexpected)
                        printer.CurrentStatus = unexpected;
                    printer.CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;
                    return true;
                }
            }

            printer.Errors.Clear();

            if (printer.CurrentStatus != MachineStatuses.MachineInitializing &&
                printer.CurrentStatus != MachineStatuses.MachineOnline)
                printer.CurrentStatus = MachineStatuses.MachineOnline;

            return true;
        }

        /// <summary>
        /// Formats and sends the job to print. This will only allow one print job at a time. It will wait until the label is removed from the printer.
        /// This method is threaded and will return the identifier immediately.
        /// </summary>
        /// <param name="producible">The item that should be printed</param>
        /// <returns>The unique identifier for the print job</returns>
        public void Print(IProducible producible)
        {
            var itemToPrint = (IPrintable)producible;

            lock (lockObject)
            {
                if (itemToPrint == null)
                {
                    throw new ArgumentException("null printable sent to " + printer);
                }

                if (itemToPrint.PrintData == null)
                {
                    throw new ArgumentException(string.Format("null printable data is not allowed.  {0}, Producible:{1}", printer, itemToPrint));
                }

                itemToPrint.ProduceOnMachineId = printer.Id;

                if (!workingQueue.Add(itemToPrint))
                {
                    // Printer is printing another item
                    itemToPrint.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
                    logger.Log(LogLevel.Warning, string.Format("{0} queue is full. Removing {2} WorkQueue:[{1}]", printer, workingQueue.ToString(), itemToPrint));
                }
                else
                {
                    logger.Log(LogLevel.Info, string.Format("Added printable {0} to print queue on printer {1}", itemToPrint, printer));
                }
            }
        }

        /// <summary>
        /// Executes print by validating printer is ready, pauses the printer and verifies, sends label and verifies it in queue, starts printer then validates label is peeled off.
        /// </summary>
        protected virtual void ExecutePrintLoop()
        {
            while (!workerThreadCancellationToken.IsCancellationRequested)
            {

                if (printer.CurrentStatus != MachineStatuses.MachineOnline)// don't exit if we get an error like head open etc.
                {
                    logger.Log(LogLevel.Debug, "Waiting for printer to come online. {0}", printer);
                    Thread.Sleep(Settings.Default.PrinterExecutePrintLoopTimeout);
                    continue;//do not return here...
                }

                var sw = Stopwatch.StartNew();

                logger.Log(LogLevel.Debug, "Waiting for label to be added to queue. {0}", printer);
                var itemToPrint = workingQueue.PeekAndWait(workerThreadCancellationToken.Token);

                if (itemToPrint == null) //The only time we should return from peek and wait with a null is when the token has ben cancelled.
                {
                    continue;
                }

                logger.Log(LogLevel.Debug, "Received item to print: {0} on printer {1}", itemToPrint, printer);

                try
                {
                    //last minute check to see that printer is ready for us to send work.
                    var hs = GetConsitantHostStatus();
                    var error = hs.GetErrorStatus();
                    var unexpected = hs.GetUnexpectedLabelStatus();
                    if (error != null && unexpected != null)
                    {
                        if (error != null)
                        {
                            logger.Log(LogLevel.Debug, "Last minute check for printer " + printer.Alias + " Error = " + error);
                        }
                        else
                        {
                            logger.Log(LogLevel.Debug, "Last minute check for printer " + printer.Alias + " Error = " + unexpected);
                        }
                        continue;
                    }

                    if (printer.CurrentStatus != MachineStatuses.MachineOnline)
                    {
                        logger.Log(LogLevel.Debug, "Printer is not online, waiting for machine to come online. {0}", printer);
                        continue;
                    }

                    if (itemToPrint.ProducibleStatus != InProductionProducibleStatuses.ProducibleSentToMachine)
                    {
                        //an outside source said we are complete, label sync for example when we have sent the label and any other failure occurs we say the job is complete
                        logger.Log(LogLevel.Trace, "*** an outside source changed label status removing from printer queue without action. {0} on {1}", itemToPrint, printer);
                        continue;
                    }

                    itemToPrint.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
                    printer.CurrentProductionStatus = MachineProductionInProgressStatuses.ProductionInProgress;

                    logger.Log(LogLevel.Trace, "{1} Begin Printing Item {0}", itemToPrint, printer);

                    if (!PausePrinterAndVerify(itemToPrint))
                    {
                        itemToPrint.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
                        logger.Log(LogLevel.Error, "Failed to pause printer {1} and verify label {0}", itemToPrint, printer);
                        continue;
                    }
                    var isLabelInQueue = false;
                    var externalPrintRestriction = itemToPrint.Restrictions.SingleOrDefault(r => r is ExternalPrintRestriction);
                    if (externalPrintRestriction != null)
                    {
                        logger.Log(LogLevel.Debug, printer + " using External Print for label {0}", itemToPrint);
                        var externalSystemService = serviceLocator.Locate<IExternalCustomerSystemService>();
                        externalSystemService.SendMessage(((ExternalPrintRestriction)externalPrintRestriction).Value,
                            new Message<IPrintable>
                            {
                                MessageType = ExternalCustomerSystemMessages.ExternalPrint,
                                Data = itemToPrint
                            });
                        //if we never verify an aggregate exception is thrown.
                        Retry.Do(() =>
                        {
                            isLabelInQueue = VerifyInQueue(itemToPrint);
                            return isLabelInQueue;//return from lamda
                        }, TimeSpan.FromMilliseconds(Settings.Default.PrinterVerifyLabelInQueueRetryIntervalMilliseconds), printer.RetryCount);
                    }
                    else
                    {
                        // Send the label request and verify the printer queued it
                        isLabelInQueue = SendLabelAndVerifyInQueue(itemToPrint); //exception thrown in not in state.
                    }

                    if (!isLabelInQueue)
                    {
                        logger.Log(LogLevel.Debug, printer.Alias + " failed to verify that printer received item to print. {0}", itemToPrint);

                        if (externalPrintRestriction == null)
                        {
                            itemToPrint.ProducibleStatus = ZebraErrorProducibleStatus.FillPrinterQueueNotConfirmed;
                            itemToPrint.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
                        }
                        else
                        {
                            itemToPrint.ProducibleStatus = ZebraErrorProducibleStatus.LabelRequestedButNotConfirmedInPrinterQueue;
                            itemToPrint.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
                        }
                    }
                    else // all good let's continue.
                    {
                        if (!StartPrinterAndVerifyNotPaused(itemToPrint))
                        {
                            logger.Log(LogLevel.Debug, printer.Alias + " failed to start printer. {0}", itemToPrint);
                            itemToPrint.ProducibleStatus = ZebraErrorProducibleStatus.FailedToStart;
                            itemToPrint.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;// we need to go to failed here because the Selection Algorithm services don't know about Zebra error types.
                        }
                        else
                        {
                            itemToPrint.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
                            printer.CurrentProductionStatus = ZebraInProductionProductionStatus.WaitingForPeelOff;

                            // Verify that the label is peeled off
                            if (WaitForLabelToHaveBeenPeeledOff(itemToPrint))
                                itemToPrint.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                        }
                    }
                }
                catch (Exception e)
                {
                    itemToPrint.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
                    printer.CurrentStatus = MachineStatuses.MachineOffline;
                    logger.LogException(LogLevel.Error, "Unhandled printing exception in ExecutePrint for {0}, on {1})", e, itemToPrint, printer);
                }
                finally
                {
                    workingQueue.Take();

                    sw.Stop();
                    logger.Log(LogLevel.Debug, "End ExecutePrint: {0}ms ({1}) {1}", sw.ElapsedMilliseconds, sw.ElapsedTicks);

                    printer.CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;
                }
            }
        }


        /// <summary>
        /// Polls the printer's host status to validate that a label has been peeled off. Double checks in case we poll a 
        /// printer while the last label is printing.
        /// </summary>
        /// <param name="itemToPrint"></param>
        private bool WaitForLabelToHaveBeenPeeledOff(IPrintable itemToPrint)
        {
            // enter infinite loop (scary) until the label is peeled off
            logger.Log(LogLevel.Debug, printer.Alias + " Waiting for peeloff of label " + itemToPrint);

            var isPrinterFinishedWithLabels = !printer.IsPeelOff;
            while (!isPrinterFinishedWithLabels)
            {
                var hs = GetConsitantHostStatus();
                var error = hs.GetErrorStatus();

                if (error != null)
                {
                    printer.CurrentStatus = error;
                }

                //Check our printer status                
                if (printer.CurrentStatus != MachineStatuses.MachineOnline)
                {
                    logger.Log(LogLevel.Warning, "Printer {0} is not online while label hanging.  Setting label to complete {1}", printer, itemToPrint);
                    itemToPrint.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                    return false;
                }

                var stillInProduction = itemToPrint.ProducibleStatus is InProductionProducibleStatuses;
                if (!stillInProduction)
                {
                    //an outside source said we are complete, label sync for example when we have sent the label and any other failure occurs we say the job is complete
                    logger.Log(LogLevel.Trace, "*** an outside source said we are complete, label sync for example when we have sent the label and any other failure occurs we say the job is complete {0} on {1}", itemToPrint, printer);
                    return false;
                }

                isPrinterFinishedWithLabels = !hs.HasLabelsInQueue && !hs.LabelWaitingForPeelOff;
                logger.Log(LogLevel.Debug, "{2} {0} IsPrinterFinishedWithLabels First check: {1}", itemToPrint, isPrinterFinishedWithLabels, printer);

                // Double check printer finished in case we polled the printer while it was printing
                if (isPrinterFinishedWithLabels)
                {
                    Thread.Sleep(printer.LabelSecondCheckWaitTime);
                    hs = GetHostStatus();
                    isPrinterFinishedWithLabels = !hs.HasLabelsInQueue && !hs.LabelWaitingForPeelOff;
                    logger.Log(LogLevel.Debug, "{2} {0} IsPrinterFinishedWithLabels Second check: {1}", itemToPrint, isPrinterFinishedWithLabels, printer);
                }
            }
            return true;
        }

        /// <summary>
        /// Formats the label to ZPL, sends the label request then verifies its in queue.
        /// </summary>
        /// <param name="itemToPrint">Printable item to publish events on</param>
        /// <returns>True if label sent and verified in queue</returns>
        private bool SendLabelAndVerifyInQueue(IPrintable itemToPrint)
        {
            var printText = itemToPrint.PrintData as string;

            if (printText == null)
            {
                itemToPrint.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
                logger.Log(LogLevel.Warning, "Failed to create print data for {0} on {1}. Data was not a string.", itemToPrint, printer);
                return false;
            }

            // Send label request to printer
            logger.Log(LogLevel.Debug, printer.Alias + " printer is sending label");
            socket.Send(printText, 1);
            var result = false;
            //retry the specified number of times to validate that the printer is ready.
            Retry.Do(() =>
            {
                result = VerifyInQueue(itemToPrint);
                return result;
            }, TimeSpan.FromMilliseconds(Settings.Default.PrinterVerifyLabelInQueueRetryIntervalMilliseconds), printer.RetryCount);
            return result;
        }

        private bool VerifyInQueue(IPrintable itemToPrint)
        {
            logger.Log(LogLevel.Debug, printer.Alias + " waiting for label to become queued");
            var printJobQueued = Retry.DoUntilTrue(() =>
            {
                var hs = GetHostStatus();
                return hs.HasLabelsInQueue;
            }, TimeSpan.FromMilliseconds(printer.PollInterval), printer.RetryCount, false);

            logger.Log(LogLevel.Debug, printer.Alias + (printJobQueued ? " label is queued on printer" : " label was not queued within configured printer retry and timeout settings"));

            if (!printJobQueued)
            {
                //itemToPrint.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;  do not set this status here because this method is used in a retry.
                logger.Log(LogLevel.Debug, "Failed to verify items sent to printer, we will retry. {0} on {1}", itemToPrint,
                    printer);

                return false;
            }

            return true;
        }

        /// <summary>
        /// Send the pause command and verify that the printer is paused
        /// </summary>
        /// <param name="itemToPrint">Printable item to publish events on</param>
        /// <returns>true is printer is paused and false otherwise</returns>
        private bool PausePrinterAndVerify(IPrintable itemToPrint)
        {
            logger.Log(LogLevel.Debug, printer.Alias + " sending pause command");
            socket.Send(Zebra.Commands.PrinterPause);

            logger.Log(LogLevel.Debug, printer.Alias + " waiting for printer to become paused");
            var printerPaused = Retry.DoUntilTrue(() =>
            {
                var hs = GetHostStatus();
                return hs.Paused;
            }, TimeSpan.FromMilliseconds(printer.PollInterval), printer.RetryCount, false);

            logger.Log(LogLevel.Debug, printer.Alias + (printerPaused ? " printer is paused" : " printer did not pause"));

            return printerPaused;
        }


        private void SendPrinterSettings()
        {
            var realPrinterPeeloffSetting = Retry.DoUntilTrue(() =>
            {
                var hs = GetHostStatus();
                return hs.PeelOffMode;
            }, TimeSpan.FromMilliseconds(printer.PollInterval), printer.RetryCount, false);

            if (realPrinterPeeloffSetting != printer.IsPeelOff)
                socket.Send(printer.IsPeelOff ? Zebra.Commands.SetToPeelOff : Zebra.Commands.SetToTearOff);

        }

        /// <summary>
        /// Sends command to the printer to start the print job and verifies that it started
        /// </summary>
        /// <param name="itemToPrint">Printable item to publish events on</param>
        /// <returns>true if the printer started the print job and false otherwise</returns>
        private bool StartPrinterAndVerifyNotPaused(IPrintable itemToPrint)
        {
            socket.Send(Zebra.Commands.PrinterStart);

            try
            {
                var printerStarted = Retry.DoUntilTrue(() =>
                {
                    var hs = GetHostStatus();
                    return !hs.Paused;
                }, TimeSpan.FromMilliseconds(printer.PollInterval), printer.RetryCount, false);

                if (!printerStarted)
                {
                    logger.Log(LogLevel.Debug, printer.Alias + " failed to start print job");
                    return false;
                }
            }
            catch (Exception)
            {
                logger.Log(LogLevel.Debug, printer.Alias + " failed to start due to exception");
                return false;
            }
            return true;
        }

        /// <summary>
        /// We should only clear the queue when connection has been lost to the printer.  Printer error status should leave these items in queue.
        /// </summary>
        private void ClearQueue()
        {
            var item = workingQueue.Peek();
            while (item != null)
            {
                item.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
                workingQueue.Take();
                item = workingQueue.Peek();
            }
        }

        private HostStatus GetHostStatus()
        {
            var hostStatus = socket.SendReceiveLines(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead);

            var hs = new HostStatus(hostStatus);
            return hs;
        }

        private HostStatus GetConsitantHostStatus()
        {
            var count = 0;
            while (count < printer.RetryCount)
            {
                var firstStatus = socket.SendReceiveLines(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead);
                Thread.Sleep(printer.LabelSecondCheckWaitTime); //This is clunky... Could use a refactor
                var secondStatus = socket.SendReceiveLines(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead);
                if (firstStatus == secondStatus)
                {
                    Thread.Sleep(printer.LabelSecondCheckWaitTime); //This is clunky... Could use a refactor
                    var thirdStatus = socket.SendReceiveLines(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead);
                    if (secondStatus == thirdStatus)
                    {
                        return new HostStatus(firstStatus);
                    }
                }

                count++;
                Thread.Sleep(printer.PollInterval);
            }

            return null;
        }

        public void Dispose()
        {
            if (socket != null)
                socket.Dispose();

            if (workerThreadCancellationToken != null && !workerThreadCancellationToken.IsCancellationRequested)
                workerThreadCancellationToken.Cancel(false);
        }

    }
}