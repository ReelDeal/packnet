﻿namespace PackNet.Communication.Communicators
{
    using System;

    using PackNet.Common.Interfaces.Enums;

    public class PlcMachineWarning
    {
        public PlcMachineWarning(Guid machineId, PacksizeMachineWarningCodes warning, double[] arguments)
        {
            this.MachineId = machineId;
            this.WarningType = warning;
            this.Arguments = arguments;
        }

        public PacksizeMachineWarningCodes WarningType { get; private set; }

        public double[] Arguments { get; private set; }

        public Guid MachineId { get; private set; }
    }
}
