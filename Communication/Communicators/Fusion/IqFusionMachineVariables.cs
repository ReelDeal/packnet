﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Communication.Communicators.Fusion
{
    //TODO: this needs work. There is nothing variable about this class. It appears that id can be static const
    public class IqFusionMachineVariables : PacksizeBaseMachineVariableMap, IFusionMachineVaribleMap
    {
        public new static IqFusionMachineVariables Instance;

        static IqFusionMachineVariables()
        {
            Instance = new IqFusionMachineVariables();
        }

        private IqFusionMachineVariables()
        {
            MapAxisParameters();
            MapKnifeParameters();
            
            MapFusionFeedRollerParameters();
            MapFusionTrackParameters();
            MapFusionCrossHeadParameters();
            MapFusionServiceParameters();
            MapFusionStatusParameters();
        }

        #region CrossHeadParameters
        
        public PacksizePlcVariable CrossHeadSpeedInPercent { get; private set; }

        public PacksizePlcVariable CrossHeadAccelerationInPercent { get; private set; }

        public PacksizePlcVariable CrossHeadSensorToKnife { get; private set; }

        public PacksizePlcVariable CrossHeadMaximumSpeed { get; private set; }

        public PacksizePlcVariable CrossHeadMaximumAcceleration { get; private set; }

        public PacksizePlcVariable CrossHeadMaximumDeceleration { get; private set; }

        public PacksizePlcVariable CrossHeadHomingPosition { get; private set; }

        public PacksizePlcVariable CrossHeadTrackSensorToTool { get; private set; }

         private void MapFusionCrossHeadParameters()
        {
            CrossHeadSpeedInPercent = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CH_SETTINGS.SpeedInPercent");
            CrossHeadAccelerationInPercent = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CH_SETTINGS.AccelerationInPercent");
            CrossHeadSensorToKnife = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CH_SETTINGS.SensorToKnifeOffset");
            CrossHeadMaximumSpeed = new PacksizePlcVariable(string.Empty, "chAxis.move.basis.parameter.v_pos");
            CrossHeadMaximumAcceleration = new PacksizePlcVariable(string.Empty, "chAxis.move.basis.parameter.a1_pos");
            CrossHeadMaximumDeceleration = new PacksizePlcVariable(string.Empty, "chAxis.move.basis.parameter.a2_pos");
            CrossHeadTrackSensorToTool = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CH_SETTINGS.TrackSensorToKnifeOffset");
            CrossHeadHomingPosition = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CH_SETTINGS.HomingPosition");
        }
        #endregion

        #region knife
            
        public PacksizePlcVariable KnifeCommand { get; private set; }

        public PacksizePlcVariable KnifeExecuteCommand { get; private set; }

        public PacksizePlcVariable ActivateLatcher { get; private set; }

         private void MapKnifeParameters()
        {
            KnifeCommand = new PacksizePlcVariable(string.Empty, "knifeCommand");
            KnifeExecuteCommand = new PacksizePlcVariable(string.Empty, "knifeExecuteCommand");
            ActivateLatcher = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "LongHeadConnection");
        }

        #endregion

        #region TrackParameters

        public PacksizePlcVariable TrackOffset { get; private set; }

        public PacksizePlcVariable TrackSensorPlateToCorrugateTrack1 { get; private set; }
        
        public PacksizePlcVariable TrackSensorPlateToCorrugateTrack2 { get; private set; }

        public PacksizePlcVariable TrackActivationDelay { get; private set; }

        public PacksizePlcVariable TrackIsRightSideFixed { get; private set; }

        public PacksizePlcVariable TrackSensorCompensationLeftTrack1 { get; private set; }

        public PacksizePlcVariable TrackSensorCompensationRightTrack1 { get; private set; }

        public PacksizePlcVariable TrackSensorCompensationLeftTrack2 { get; private set; }

        public PacksizePlcVariable TrackSensorCompensationRightTrack2 { get; private set; }

        public PacksizePlcVariable TrackOutOfCorrugatePositions { get; private set; }

        public PacksizePlcVariable TrackSideSteeringTolerance { get; private set; }

        public PacksizePlcVariable TrackOffsetTolerance { get; private set; }

        public PacksizePlcVariable TrackLatchMinimumDistance { get; private set; }

        public PacksizePlcVariable TrackSensorControl { get; private set; }

        public PacksizePlcVariable LastLatchPosition { get; private set; }

        public PacksizePlcVariable TrackSensorControlModeSpeed { get; private set; }
        
        public PacksizePlcVariable CorrugateThicknesses { get; private set; }
        
        private void MapFusionTrackParameters()
        {
            TrackOffset = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.TrackStartingPositions", true);
            TrackSensorPlateToCorrugateTrack1 = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorPlateToCorrugateTrack1");
            TrackSensorPlateToCorrugateTrack2 = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorPlateToCorrugateTrack2");

            TrackActivationDelay = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.ActivationDelay");
            TrackIsRightSideFixed = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.IsRightSideFixed");
            
            //If changes are made to these, changes are needed in the MachineCommandServiceVariableMap.cs too
            TrackSensorCompensationLeftTrack1 = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorCompensationLeft.Track1");
            TrackSensorCompensationRightTrack1 = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorCompensationRight.Track1");
            TrackSensorCompensationLeftTrack2 = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorCompensationLeft.Track2");
            TrackSensorCompensationRightTrack2 = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorCompensationRight.Track2");
            
            TrackOutOfCorrugatePositions = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.OutOfCorrugatePositions");
            TrackSideSteeringTolerance = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SideSteeringTolerance");
            TrackOffsetTolerance = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.OffsetTolerance");
            TrackLatchMinimumDistance = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.LatchMinimumDistance");
            ActivateRightPressureRoller = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.PressureRollerActivation[0]");
            ActivateLeftPressureRoller = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.PressureRollerActivation[1]");
            TrackSensorControl = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorControlMode");
            LastLatchPosition = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.LastLatchPosition");
            TrackSensorControlModeSpeed = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.SensorControlModeSpeed");

            CorrugateThicknesses = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "TRACK_SETTINGS.CorrugateThicknesses");
        }

        #endregion

        #region RollerParameters

        public PacksizePlcVariable FeedRollerLoweringOffset { get; private set; }

        public PacksizePlcVariable FeedRollerCleanCutLength { get; private set; }

        public PacksizePlcVariable FeedRollerOutFeedLength { get; private set; }

        public PacksizePlcVariable FeedRollerSpeedInPercent { get; private set; }

        public PacksizePlcVariable FeedRollerAccelerationInPercent { get; private set; }

        public PacksizePlcVariable FeedRollerGearRatio { get; private set; }

        public PacksizePlcVariable LeftRollerDisconnectPosition { get; private set; }

        public PacksizePlcVariable RightRollerDisconnectPosition { get; private set; }

        public PacksizePlcVariable ActivateRightPressureRoller { get; private set; }

        public PacksizePlcVariable ActivateLeftPressureRoller { get; private set; }

        public PacksizePlcVariable LowerRoller { get; private set; }
        
        private void MapFusionFeedRollerParameters()
        {
            FeedRollerSpeedInPercent = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.SpeedInPercent");
            FeedRollerAccelerationInPercent = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.AccelerationInPercent");
            FeedRollerLoweringOffset = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.LoweringOffset");
            FeedRollerCleanCutLength = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.CleanCutLength");
            FeedRollerOutFeedLength = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.OutFeedLength");
            FeedRollerGearRatio = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.GearRatio");
            LeftRollerDisconnectPosition = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.LeftRollerDisconnectPosition");
            RightRollerDisconnectPosition = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.RightRollerDisconnectPosition");
            LowerRoller = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "FEED_ROLLER_SETTINGS.LowerRoller");
        }

        #endregion

        #region StatusParameters
        
        public PacksizePlcVariable GreenLEDState { get; private set; }

        public PacksizePlcVariable Calibrate { get; private set; }

        public PacksizePlcVariable RedLEDState { get; private set; }

        public PacksizePlcVariable WhiteLEDState { get; private set; }

        public PacksizePlcVariable IOControllTestState { get; private set; }

        public PacksizePlcVariable SetMachinePause { get; private set; }

        private void MapFusionStatusParameters()
        {
            Calibrate = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "RUN_CALIBRATION");
            GreenLEDState = new PacksizePlcVariable(IqFusionTasks.IOControll.ToString(), "GreenLEDSignal");
            RedLEDState = new PacksizePlcVariable(IqFusionTasks.IOControll.ToString(), "RedLEDSignal");
            WhiteLEDState = new PacksizePlcVariable(IqFusionTasks.IOControll.ToString(), "WhiteLEDSignal");
            IOControllTestState = new PacksizePlcVariable(IqFusionTasks.IOControll.ToString(), "InServiceMode");
            SetMachinePause = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "SET_PAUSE_MODE");
        }

        #endregion
        
        #region AxisParameters

        public PacksizePlcVariable ToolAxisScaling { get; private set; }

        public PacksizePlcVariable RollAxisScaling { get; private set; }

        private void MapAxisParameters()
        {
            ToolAxisScaling = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "chAxis.encoder_if.parameter.scaling.load.units");
            RollAxisScaling = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "rollAxis.encoder_if.parameter.scaling.load.units");
        }

        #endregion
        
        #region ServiceParameters
        public PacksizePlcVariable SetServiceMode { get; private set; }
        public PacksizePlcVariable IODebug { get; private set; }
        public PacksizePlcVariable Run4Ever { get; private set; }
        public PacksizePlcVariable CustomInstructionList { get; private set; }
        public PacksizePlcVariable LongHeadPositionsReady { get; private set; }
        public PacksizePlcVariable GetLongHeadPositions { get; private set; }
        public PacksizePlcVariable ClearNonVolatile { get; private set; }
        public PacksizePlcVariable NrOfFeedRollerOutOfPositionAfterLongheadRepos { get; private set; }

        protected void MapFusionServiceParameters()
        {
            SetServiceMode = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "GoToServiceMode");
            IODebug = new PacksizePlcVariable(string.Empty, "iodebug");
            Run4Ever = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "Run4Ever");
            CustomInstructionList = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CustomInstructionList");
            LongHeadPositionsReady = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "LongHeadPositionsReady");
            GetLongHeadPositions = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "GetLongHeadPositions");
            ClearNonVolatile = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "ClearNonVolatile");
            NrOfFeedRollerOutOfPositionAfterLongheadRepos = new PacksizePlcVariable(string.Empty, "FeedRollerNotCompletlyUpCount");
        }
        
        #endregion


        public override IEnumerable<PacksizePlcVariable> AsEnumerable()
        {
            var info = typeof(IqFusionMachineVariables).GetProperties();

            return info.Select(pi => (PacksizePlcVariable)pi.GetValue(this, null)).ToList();
        }
    }
}
