﻿using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Communication.Communicators.Fusion
{
    public interface IFusionMachineVaribleMap : IPackagingMachineVariableMap
    {
        PacksizePlcVariable GreenLEDState { get; }

        PacksizePlcVariable RedLEDState { get; }

        PacksizePlcVariable WhiteLEDState { get; }

        PacksizePlcVariable LeftRollerDisconnectPosition { get; }

        PacksizePlcVariable RightRollerDisconnectPosition { get; }

        PacksizePlcVariable ActivateRightPressureRoller { get; }

        PacksizePlcVariable ActivateLeftPressureRoller { get; }

        PacksizePlcVariable LowerRoller { get; }
     
        PacksizePlcVariable CrossHeadSpeedInPercent { get; }

        PacksizePlcVariable CrossHeadAccelerationInPercent { get; }

        PacksizePlcVariable CrossHeadSensorToKnife { get; }

        PacksizePlcVariable CrossHeadMaximumSpeed { get; }

        PacksizePlcVariable CrossHeadMaximumAcceleration { get; }

        PacksizePlcVariable CrossHeadMaximumDeceleration { get; }

        PacksizePlcVariable CrossHeadHomingPosition { get; }

        PacksizePlcVariable CrossHeadTrackSensorToTool { get; }

        PacksizePlcVariable KnifeCommand { get; }

        PacksizePlcVariable KnifeExecuteCommand { get; }

        PacksizePlcVariable ActivateLatcher { get; }
        
        PacksizePlcVariable TrackOffset { get; }

        PacksizePlcVariable TrackSensorPlateToCorrugateTrack1 { get; }

        PacksizePlcVariable TrackSensorPlateToCorrugateTrack2 { get; }

        PacksizePlcVariable TrackActivationDelay { get; }

        PacksizePlcVariable TrackIsRightSideFixed { get; }

        PacksizePlcVariable TrackSensorCompensationLeftTrack1 { get; }

        PacksizePlcVariable TrackSensorCompensationRightTrack1 { get; }

        PacksizePlcVariable TrackSensorCompensationLeftTrack2 { get; }

        PacksizePlcVariable TrackSensorCompensationRightTrack2 { get; }

        PacksizePlcVariable TrackOutOfCorrugatePositions { get; }

        PacksizePlcVariable TrackSideSteeringTolerance { get; }

        PacksizePlcVariable TrackOffsetTolerance { get; }

        PacksizePlcVariable TrackLatchMinimumDistance { get; }

        PacksizePlcVariable TrackSensorControl { get; }

        PacksizePlcVariable LastLatchPosition { get; }

        PacksizePlcVariable FeedRollerLoweringOffset { get; }

        PacksizePlcVariable FeedRollerCleanCutLength { get; }
        
        PacksizePlcVariable FeedRollerSpeedInPercent { get; }

        PacksizePlcVariable FeedRollerAccelerationInPercent { get; }

        PacksizePlcVariable FeedRollerGearRatio { get; }

        PacksizePlcVariable FeedRollerOutFeedLength { get; }


        /// <summary>
        /// The last carton id that was transferred, The Id GUID is split into 4 parts since the biggest INT type in the plc is a DINT which is 32 bits
        /// </summary>
        
        PacksizePlcVariable ToolAxisScaling { get; }

        PacksizePlcVariable RollAxisScaling { get; }
        
        PacksizePlcVariable IOControllTestState { get; }
        
        PacksizePlcVariable SetMachinePause { get; }

        PacksizePlcVariable CorrugateThicknesses { get; }
    }
}
