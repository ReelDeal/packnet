﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Machines;

namespace PackNet.Communication.Communicators.Fusion
{
    public static class InstructionListExtensions
    {
        public static short[] GetPlcInstructionArray(this IEnumerable<IInstructionItem> list)
        {
            var plcInstructionList = new List<short>();

            foreach (var item in list)
            {
                var itemPlcArray = item.ToPlcArray();
                plcInstructionList.AddRange(itemPlcArray);
            }

            return plcInstructionList.ToArray();
        }
    }
}
