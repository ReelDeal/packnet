﻿using System;

namespace PackNet.Communication.Communicators.Fusion
{
    public class ChangingCorrugateEventArgs : EventArgs
    {
        public ChangingCorrugateEventArgs(bool changingCorrugate)
        {
            ChangingCorrugate = changingCorrugate;
        }

        public bool ChangingCorrugate { get; private set; }
    }
}
