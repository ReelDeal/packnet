﻿namespace PackNet.Communication.Communicators.Fusion
{
    //Task names are bounded to a maximum of 10 characters
    public enum IqFusionTasks
    {
        PCCommunic,
        Main,
		SetStruct,
        IOControll,
        HttpClient,
    }
}