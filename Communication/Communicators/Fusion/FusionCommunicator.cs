using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines.Fusion;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PackNet.Communication.Communicators.Fusion
{
    public class FusionCommunicator : PacksizeMachineCommunicatorBase, IFusionCommunicator
    {
        private const int Acp10McScaling = 100;

        private readonly IFusionMachineVaribleMap variableMap;

        private readonly ILogger logger;

        public FusionCommunicator(FusionMachine machine, IEventAggregatorPublisher publisher, IEventAggregatorSubscriber subscriber, IPLCHeartBeatCommunicator communicator, IFusionMachineVaribleMap variableMap, ILogger logger)
            : base(machine, publisher, subscriber, communicator, variableMap, logger)
        {
            this.variableMap = variableMap;
            this.logger = logger;
        }

        public override void SynchronizeMachine(bool isReloadMachineConfig = false)
        {
            if (WaitUntilReadyToSynchronize(isReloadMachineConfig))
            {
                try
                {
                    var machineSettings = machine.PhysicalMachineSettings as FusionPhysicalMachineSettings;
                    // ensure that the first thing we do is setup the communication channel with .Server
                    SetVariableValue(variableMap.RunPrestart, false);
                    SetEventNotificationParameters(machineSettings.EventNotifierIp);
                    SetUnitsOfMeasurement(machineSettings.Unit);
                    InitVariables();

                    SetTrackParameters(machineSettings.TrackParameters, machine.NumTracks);
                    SetLongHeadParameters(machineSettings.LongHeadParameters);
                    SetCrossHeadParameters(machineSettings.CrossHeadParameters);

                    SetCorrugateWidths(machine.Tracks);
                    SetCorrugateThicknesses(
                        machine.Tracks.Select(t => t.LoadedCorrugate == null ? 0.0 : (double)t.LoadedCorrugate.Thickness).ToList());

                    SetFeedRollerParamters(machineSettings.FeedRollerParameters);
                    SetHomingPosition(machineSettings.CrossHeadParameters.HomingPosition);
                    ClearMachineErrors();
                    SetVariableValue(variableMap.RunPrestart, true);
                    UpdateMachinePlcVersion();
                }
                catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
                {
                    logger.LogException(LogLevel.Warning, "ConnectionException", ce);
                }
            }
        }

        protected override void GetSettingsFromMachine()
        {
            GetTrackOffsetsFromMachine(machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);
            GetFusionLongHeadSettingsFromMachine(machine.PhysicalMachineSettings as FusionPhysicalMachineSettings);
        }

        private void GetTrackOffsetsFromMachine(FusionPhysicalMachineSettings machineSettings)
        {
            var offsets = GetVariableValue<double[]>(variableMap.TrackOffset);
            for (var i = 0; i < offsets.Where(v => Math.Abs(v) > 0.000001).Count(); i++)
            {
                machine.Tracks.Single(t => t.TrackNumber == i + 1).TrackOffset = offsets[i];
            }
        }

        private void GetFusionLongHeadSettingsFromMachine(FusionPhysicalMachineSettings machineSettings)
        {
            var positions = GetVariableValue<double[]>(variableMap.LongHeadPositions);

            if (positions == null)
            {
                return;
            }
            var sensorOffset = machineSettings.LongHeadParameters.LeftSideToSensorPin;

            for (var i = 0; i < positions.Where(v => Math.Abs(v) > 0.000001).Count(); i++)
            {

                var compensatedPosition =
                    GetLongHeadToolPositionFromSensorPosition(
                        machineSettings.LongHeadParameters.LongHeads.ElementAt(i),
                        sensorOffset, positions[i]);
                machineSettings.LongHeadParameters
                    .LongHeads
                    .ElementAt(i)
                    .Position = compensatedPosition;
            }
        }

        /// <summary>
        /// Sets the change corrugate.
        /// </summary>
        /// <param name="value">if set to <c>true</c> Machine is changing corrugate.</param>
        public void SetChangeCorrugate(bool value)
        {
            try
            {
                SetVariableValue(variableMap.ChangeCorrugate, value);
            }
            catch (ConnectionException ce) //Don't throw this exception... No one cares
            {
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
        }

        public void SetCorrugateThicknesses(IEnumerable<double> trackThicknesses)
        {
            SetVariableValue(variableMap.CorrugateThicknesses, trackThicknesses.ToArray());
        }

        public void SetAlignments()
        {
            SetVariableValue(variableMap.TrackIsRightSideFixed, new[] { machine.NumTracks == 2, false });
        }

        private void SetTrackParameters(FusionTracksParameters parameters, int numberOfTracks)
        {
            //TODO we should determine number of tracks based on the number of loaded corrugates and the plc should ignore the correct feed guides so one can only have 1 corrugate on a multi track machine if you don't have another corrugate
            SetVariableValue(variableMap.NumberOfTracks, numberOfTracks);

            var track1 = parameters.Tracks.SingleOrDefault(t => t.Number == 1);
            if (track1 != null)
            {
                track1.RightSideFixed = numberOfTracks == 2;
                var sensorPlateTrack1 = new double[] { track1.LeftSensorPlateToCorrugate, track1.RightSensorPlateToCorrugate };
                SetVariableValue(variableMap.TrackSensorPlateToCorrugateTrack1, sensorPlateTrack1);
                SetVariableValue(variableMap.TrackSensorCompensationLeftTrack1, track1.LeftswordsensorCompensations.Select(c => c.Compensation).ToArray());
                SetVariableValue(variableMap.TrackSensorCompensationRightTrack1, track1.RightswordsensorCompensations.Select(c => c.Compensation).ToArray());
            }

            var track2 = parameters.Tracks.SingleOrDefault(t => t.Number == 2);
            if (track2 != null)
            {
                track2.RightSideFixed = false;
                var sensorPlateTrack2 = new double[] { track2.LeftSensorPlateToCorrugate, track2.RightSensorPlateToCorrugate };
                SetVariableValue(variableMap.TrackSensorPlateToCorrugateTrack2, sensorPlateTrack2);
                SetVariableValue(variableMap.TrackSensorCompensationLeftTrack2,
                    track2.LeftswordsensorCompensations.Select(c => c.Compensation).ToArray());
                SetVariableValue(variableMap.TrackSensorCompensationRightTrack2,
                    track2.RightswordsensorCompensations.Select(c => c.Compensation).ToArray());
            }

            if (numberOfTracks == 2)
            {
                SetVariableValue(variableMap.TrackIsRightSideFixed, new[] { track1.RightSideFixed, track2.RightSideFixed });
                SetVariableValue(variableMap.TrackOutOfCorrugatePositions, new double[]
                {
                    track1.OutOfCorrugatePosition,
                    track2.OutOfCorrugatePosition
                });
            }
            else
            {
                SetVariableValue(variableMap.TrackIsRightSideFixed, new[] { false, false });
                SetVariableValue(variableMap.TrackOutOfCorrugatePositions, new double[]
                {
                    track2.OutOfCorrugatePosition,
                    double.MaxValue
                });
            }

            SetVariableValue(variableMap.TrackActivationDelay, parameters.TrackActivateDelay);
            SetVariableValue(variableMap.TrackSideSteeringTolerance, (double)parameters.TrackSideSteeringTolerance);
            SetVariableValue(variableMap.TrackOffsetTolerance, (double)parameters.TrackOffsetTolerance);
            SetVariableValue(variableMap.TrackLatchMinimumDistance, (double)parameters.TrackLatchDistance);
            SetVariableValue(variableMap.CorrugateWidthTolerance, (double)parameters.CorrugateWidthTolerance);
        }

        private void SetLongHeadParameters(FusionLongHeadParameters parameters)
        {
            SetVariableValue(variableMap.NumberOfLongHeads, parameters.LongHeads.Count());
            SetVariableValue(variableMap.LongHeadConnectionDelay, parameters.ConnectionDelay);
        }

        private void SetCrossHeadParameters(FusionCrossHeadParameters parameters)
        {
            SetVariableValue(variableMap.CrossHeadMinimumPosition, (double)parameters.MinimumPosition);
            SetVariableValue(variableMap.CrossHeadMaximumPosition, (double)parameters.MaximumPosition);
            SetVariableValue(variableMap.CrossHeadSpeedInPercent, parameters.Speed);
            SetVariableValue(variableMap.CrossHeadAccelerationInPercent, parameters.Acceleration);
            SetVariableValue(variableMap.CrossHeadSensorToKnife, (double)parameters.LongHeadSensorToTool);
            SetVariableValue(variableMap.CrossHeadTrackSensorToTool, (double)parameters.TrackSensorToTool);
            SetVariableValue(variableMap.CrossHeadMaximumAcceleration,
                                  parameters.MaximumAcceleration * Acp10McScaling);
            SetVariableValue(variableMap.CrossHeadMaximumDeceleration,
                                  parameters.MaximumDeceleration * Acp10McScaling);
            SetVariableValue(variableMap.CrossHeadMaximumSpeed, parameters.MaximumSpeed * Acp10McScaling);
        }

        private void SetFeedRollerParamters(FusionFeedRollerParameters feedRoller)
        {
            SetVariableValue(variableMap.FeedRollerOutFeedLength, (double)feedRoller.OutFeedLength);
            SetVariableValue(variableMap.FeedRollerLoweringOffset, (double)feedRoller.LoweringOffset);
            SetVariableValue(variableMap.FeedRollerCleanCutLength, (double)feedRoller.CleanCutLength);
            SetVariableValue(variableMap.FeedRollerSpeedInPercent, feedRoller.PercentMovementSpeed);
            SetVariableValue(variableMap.FeedRollerGearRatio, feedRoller.ToolsGearRatio);
            SetVariableValue(variableMap.FeedRollerAccelerationInPercent, feedRoller.Acceleration);
        }

        private void SetHomingPosition(double pos)
        {
            SetVariableValue(variableMap.CrossHeadHomingPosition, pos);
        }

        /// <summary>
        /// End connections to machines and cleanup resources.
        /// </summary>
        public void Shutdown()
        {
            Dispose();
        }
    }
}
