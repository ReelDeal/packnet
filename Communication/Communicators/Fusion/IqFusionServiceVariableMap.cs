﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Communication.Communicators.Fusion
{
    public class IqFusionServiceVariableMap
    {

        public PacksizePlcVariable SetServiceMode { get; private set; }

        public PacksizePlcVariable IODebug { get; private set; }

        public PacksizePlcVariable Run4Ever { get; private set; }

        public PacksizePlcVariable CustomInstructionList { get; private set; }

        public PacksizePlcVariable LongHeadPositionsReady { get; private set; }

        public PacksizePlcVariable GetLongHeadPositions { get; private set; }

        public PacksizePlcVariable ClearNonVolatile { get; private set; }

        public PacksizePlcVariable NrOfFeedRollerOutOfPositionAfterLongheadRepos { get; private set; }

        public PacksizePlcVariable MachinePlcVersion { get; private set; }

        protected void MapServiceParameters()
        {
            SetServiceMode = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "GoToServiceMode");
            IODebug = new PacksizePlcVariable(string.Empty, "iodebug");
            Run4Ever = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "Run4Ever");
            CustomInstructionList = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "CustomInstructionList");
            LongHeadPositionsReady = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "LongHeadPositionsReady");
            GetLongHeadPositions = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "GetLongHeadPositions");
            ClearNonVolatile = new PacksizePlcVariable(IqFusionTasks.SetStruct.ToString(), "ClearNonVolatile");
            NrOfFeedRollerOutOfPositionAfterLongheadRepos = new PacksizePlcVariable(string.Empty, "FeedRollerNotCompletlyUpCount");
            MachinePlcVersion = new PacksizePlcVariable(IqFusionTasks.Main.ToString(), "PLC_VERSION");
        }
    }
}
