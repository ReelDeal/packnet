﻿using System;
using System.Linq;

using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Communication.Communicators
{
    public abstract class PacksizeMachineErrorHandler : MachineCommunicatorPlcVariableHandler, IDisposable
    {
        private readonly IPackagingMachineVariableMap variableMap;

        protected PacksizeMachineErrorHandler(IPackagingMachineVariableMap variableMap, IPLCHeartBeatCommunicator communicator)
            : base(communicator)
        {
            this.variableMap = variableMap;
        }

        protected PlcMachineError GetMachineError()
        {
            var errorCodeVariables = variableMap.AsEnumerable().Where(v => v != null && v.VariableName.Contains("ERROR_CODES[") && v.VariableName.Contains("].Code")).ToList();

            foreach (var errorCodeVariable in errorCodeVariables)
            {
                var errorCode = GetVariableValue<ushort>(errorCodeVariable);
                if (errorCode != 0)
                {
                    return new PlcMachineError()
                    {
                        Code = errorCode,
                        Arguments = GetErrorArguments(errorCodeVariable.VariableName),
                        Position = GetErrorPositionInList(errorCodeVariable.VariableName)
                    };
                }
            }

            return null;
        }
        
        protected double[] GetErrorArguments(string variableName)
        {
            if (variableName == variableMap.ErrorCode1.VariableName)
            {
                return GetVariableValue<double[]>(variableMap.ErrorCode1Arguments);
            }
            if (variableName == variableMap.ErrorCode2.VariableName)
            {
                return GetVariableValue<double[]>(variableMap.ErrorCode2Arguments);
            }
            if (variableName == variableMap.ErrorCode3.VariableName)
            {
                return GetVariableValue<double[]>(variableMap.ErrorCode3Arguments);
            }
            if (variableName == variableMap.ErrorCode4.VariableName)
            {
                return GetVariableValue<double[]>(variableMap.ErrorCode4Arguments);
            }

            return GetVariableValue<double[]>(variableMap.ErrorCode5Arguments);
        }

        // todo error list is 10 long now
        protected int GetErrorPositionInList(string variableName)
        {
            if (variableName == variableMap.ErrorCode1.VariableName)
            {
                return 1;
            }
            if (variableName == variableMap.ErrorCode2.VariableName)
            {
                return 2;
            }
            if (variableName == variableMap.ErrorCode3.VariableName)
            {
                return 3;
            }
            if (variableName == variableMap.ErrorCode4.VariableName)
            {
                return 4;
            }

            return 5;
        }

        protected bool ChangedVariableIsAnErrorCode(VariableChangedEventArgs e)
        {
            return e.IsOfType(variableMap.ErrorCode1) ||
                   e.IsOfType(variableMap.ErrorCode2) ||
                   e.IsOfType(variableMap.ErrorCode3) ||
                   e.IsOfType(variableMap.ErrorCode4) ||
                   e.IsOfType(variableMap.ErrorCode5);
        }

        protected void ClearMachineErrors()
        {
            SetVariableValue(variableMap.ClearMachineErrors, true);
        }

        protected bool GetIsMachineInErrorState()
        {
            return GetVariableValue<int>(variableMap.ErrorCode1) != 0 ||
                   GetVariableValue<int>(variableMap.ErrorCode2) != 0 ||
                   GetVariableValue<int>(variableMap.ErrorCode3) != 0 ||
                   GetVariableValue<int>(variableMap.ErrorCode4) != 0 ||
                   GetVariableValue<int>(variableMap.ErrorCode5) != 0;
        }
        
        public new void Dispose()
        {  
            base.Dispose();
        }
    }
}
