﻿namespace PackNet.Communication.Communicators.Fusion
{
    //Task names are bounded to a maximum of 10 characters
    public enum EmTasks
    {
        PCCommunic,
		RestPostSe,
        VariableEv,
        Main
    }
}