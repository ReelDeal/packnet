﻿using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PackNet.Communication.Communicators.EM
{
    public class EmCommunicator : PacksizeMachineCommunicatorBase, IEmMachineCommunicator
    {
        private double ACP10Scaling;
        private readonly IEmMachineVariableMap variableMap;
        private readonly ILogger logger;

        private EmMachine Machine { get { return machine as EmMachine; } }

        public EmCommunicator(EmMachine machine,
            IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber,
            IPLCHeartBeatCommunicator communicator,
            IEmMachineVariableMap variableMap,
            ILogger logger)
            : base(machine, publisher, subscriber, communicator, variableMap, logger)
        {
            this.variableMap = variableMap;
            this.logger = logger;
        }


        public override void SynchronizeMachine(bool isReloadMachineConfig = false)
        {

            if (WaitUntilReadyToSynchronize(isReloadMachineConfig))
            {
                try
                {
                    var physicalMachineSettings = Machine.PhysicalMachineSettings as EmPhysicalMachineSettings;

                    if (physicalMachineSettings == null)
                        throw new InvalidCastException(
                            "Unable to cast machine.PhysicalMachineSettings as EmPhysicalMachineSettings in EmCommunicator");

                    // ensure that the first thing we do is setup the communication channel with .Server
                    SetVariableValue(variableMap.RunPrestart, false);

                    SetEventNotificationParameters(physicalMachineSettings.EventNotifierIp);
                    SetUnitsOfMeasurement(physicalMachineSettings.Unit);
                    InitVariables();

                    SetTrackParameters(physicalMachineSettings.TrackParameters);
                    SetLongHeadParameters(physicalMachineSettings.LongHeadParameters);
                    SetCrossHeadParameters(physicalMachineSettings.CrossHeadParameters);
                    SetFeedRollerParameters(physicalMachineSettings.FeedRollerParameters);
                    ClearMachineErrors();
                    SetVariableValue(variableMap.RunPrestart, true);
                    UpdateMachinePlcVersion();
                }
                catch (ConnectionException ce) //TODO: Don't throw this exception... No one cares
                {
                    logger.LogException(LogLevel.Warning, "ConnectionException", ce);
                }
            }
        }

        public override void OnVariableChanged(VariableChangedEventArgs e)
        {
            if (e.IsOfType(variableMap.CrossheadOutOfPositionWarning))
            {
                var warningCode = Convert.ToUInt16(e.Value);
                if (warningCode != 0)
                {
                    HandleCrossheadOutOfPositionWarning(GetWarningArguments(e.VariableName));
                }
            }
            else if (e.IsOfType(variableMap.LongheadsOutOfPositionWarning))
            {
                var warningCode = Convert.ToUInt16(e.Value);
                if (warningCode != 0)
                {
                    HandleLongheadsOutOfPositionWarning(GetWarningArguments(e.VariableName));
                }
            }
            else if (e.IsOfType(variableMap.Track1ChangingCorrugate) || e.IsOfType(variableMap.Track2ChangingCorrugate) || e.IsOfType(variableMap.Track3ChangingCorrugate) || e.IsOfType(variableMap.Track4ChangingCorrugate))
            {
                var isChangingCorrugate = Convert.ToBoolean(e.Value);
                var trackNumber = GetTrackNumberFromVariableName(e.VariableName);
                var track = machine.Tracks.SingleOrDefault(c => c.TrackNumber == trackNumber);
                if (track != null)
                {
                    track.IsInChangingCorrugateState = isChangingCorrugate;

                    var message = new Message<IMachine>
                    {
                        MessageType = MachineMessages.MachineStatusChanged,
                        Data = machine
                    };
                    publisher.Publish(message);
                }
            }
            else
            {
                base.OnVariableChanged(e);
            }
        }

        private int GetTrackNumberFromVariableName(string variableName)
        {
            if (variableName == variableMap.Track1ChangingCorrugate.VariableName)
            {
                return 1;
            }

            if (variableName == variableMap.Track2ChangingCorrugate.VariableName)
            {
                return 2;
            }

            return variableName == variableMap.Track3ChangingCorrugate.VariableName ? 3 : 4;
        }

        /// <summary>
        /// Sets the change corrugate.
        /// </summary>
        /// <param name="value">if set to <c>true</c> Machine is changing corrugate.</param>
        /// <param name="trackNumber">The track number.</param>
        public void SetChangeCorrugate(bool value, int trackNumber)
        {
            try
            {
                switch (trackNumber)
                {
                    case 1:
                        SetVariableValue(variableMap.Track1ChangingCorrugate, true);
                        break;
                    case 2:
                        SetVariableValue(variableMap.Track2ChangingCorrugate, true);
                        break;
                    case 3:
                        SetVariableValue(variableMap.Track3ChangingCorrugate, true);
                        break;
                    default:
                        SetVariableValue(variableMap.Track4ChangingCorrugate, true);
                        break;
                }
            }
            catch (ConnectionException ce) 
            {
                //Don't throw this exception... No one cares
                logger.LogException(LogLevel.Warning, "ConnectionException", ce);
            }
        }

        public void LeaveChangeCorrugate()
        {
            SetVariableValue(variableMap.ChangeCorrugate, false);
        }

        private void HandleLongheadsOutOfPositionWarning(double[] arguments)
        {
            var warningTolerance = (machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters.OutOfPositionWarningThreshold;

            var sb = new StringBuilder();
            for (int i = 0; i < arguments.Length; i++)
            {
                if (arguments[i] != 0.0)
                    sb.Append(string.Format("LH{0}: {1}, ", i + 1, arguments[i]));
            }

            if (arguments.Any(a => Math.Abs(a) >= warningTolerance))
            {
                var message = new Message<PlcMachineWarning>
                {
                    Data = new PlcMachineWarning(machine.Id, PacksizeMachineWarningCodes.LongheadOutOfPosition, arguments),
                    MessageType = MachineMessages.PlcWarning
                };

                publisher.Publish(message);
            }
            

            logger.Log(
                LogLevel.Warning,
                string.Format(
                    "One or more longheads were out of position. {0}", sb.ToString()));
        }

        private void HandleCrossheadOutOfPositionWarning(double[] arguments)
        {
            var warningTolerance = (machine.PhysicalMachineSettings as EmPhysicalMachineSettings).CrossHeadParameters.OutOfPositionWarningThreshold;

            if (Math.Abs(arguments[0]) >= warningTolerance)
            {
                var message = new Message<PlcMachineWarning>
                              {
                                  Data =
                                      new PlcMachineWarning(
                                      machine.Id,
                                      PacksizeMachineWarningCodes.CrossheadOutOfPosition,
                                      arguments),
                                  MessageType = MachineMessages.PlcWarning
                              };
                publisher.Publish(message);
            }

            logger.Log(LogLevel.Warning, string.Format("Crosshead was out of position with: {0} units", arguments[0]));
        }

        protected override void GetSettingsFromMachine()
        {
            ACP10Scaling = GetVariableValue<double>(variableMap.ACP10Scaling);
            GetEmLongheadSettingsFromMachine(Machine.PhysicalMachineSettings as EmPhysicalMachineSettings);
            GetCrossHeadSettingsFromMachine(Machine.PhysicalMachineSettings as EmPhysicalMachineSettings);
            GetFeedRollerSettingsFromMachine(Machine.PhysicalMachineSettings as EmPhysicalMachineSettings);
        }

        private void GetFeedRollerSettingsFromMachine(EmPhysicalMachineSettings emPhysicalMachineSettings)
        {
            emPhysicalMachineSettings.FeedRollerParameters.MovementData.Speed = GetVariableValue<double>(variableMap.FeedRollerSpeed) / ACP10Scaling;
            emPhysicalMachineSettings.FeedRollerParameters.MovementData.Acceleration =
                GetVariableValue<double>(variableMap.FeedRollerAcceleration) / ACP10Scaling;
        }

        private void GetCrossHeadSettingsFromMachine(EmPhysicalMachineSettings emPhysicalMachineSettings)
        {
            emPhysicalMachineSettings.CrossHeadParameters.Position = GetVariableValue<double>(variableMap.CrossHeadPosition);
            emPhysicalMachineSettings.CrossHeadParameters.MovementData.Speed = GetVariableValue<double>(variableMap.CrossHeadSpeed) / ACP10Scaling;
            emPhysicalMachineSettings.CrossHeadParameters.MovementData.Acceleration = GetVariableValue<double>(variableMap.CrossHeadAcceleration) / ACP10Scaling;
        }

        private void GetEmLongheadSettingsFromMachine(EmPhysicalMachineSettings emPhysicalMachineSettings)
        {
            var positions = GetVariableValue<double[]>(variableMap.LongHeadPositions);

            if (positions == null)
            {
                return;
            }

            for (var i = 0; i < positions.Where(v => Math.Abs(v) > 0.000001).Count(); i++)
            {
                var sensorOffset = emPhysicalMachineSettings.LongHeadParameters.LongHeads.ElementAt(i).LeftSideToSensorPin;

                var compensatedPosition =
                    GetLongHeadToolPositionFromSensorPosition(
                        emPhysicalMachineSettings.LongHeadParameters.LongHeads.ElementAt(i),
                        sensorOffset, positions[i]);
                emPhysicalMachineSettings.LongHeadParameters
                    .LongHeads
                    .ElementAt(i)
                    .Position = compensatedPosition;
            }
      
            emPhysicalMachineSettings.LongHeadParameters.MovementData.Speed = GetVariableValue<double>(variableMap.CrossHeadSpeed) / ACP10Scaling;
            emPhysicalMachineSettings.LongHeadParameters.MovementData.Acceleration = GetVariableValue<double>(variableMap.CrossHeadAcceleration) / ACP10Scaling;
        }

        private void SetFeedRollerParameters(EmFeedRollerParameters feedRollerParameters)
        {
            SetVariableValue(variableMap.FeedRollerOutFeedLength, (double)feedRollerParameters.OutFeedLength);
            SetVariableValue(variableMap.FeedRollerToolsGearRatio, feedRollerParameters.ToolsGearRatio);

            SetVariableValue(variableMap.FeedRollerNormalSpeedInPercent, feedRollerParameters.MovementData.NormalMovement.SpeedInPercent);
            SetVariableValue(variableMap.FeedRollerNormalAccelerationInPercent, feedRollerParameters.MovementData.NormalMovement.AccelerationInPercent);
            SetVariableValue(variableMap.FeedRollerNormalTorque, feedRollerParameters.MovementData.NormalMovement.Torque);
          
            SetVariableValue(variableMap.FeedRollerReverseSpeedInPercent, feedRollerParameters.MovementData.ReverseMovement.SpeedInPercent);
            SetVariableValue(variableMap.FeedRollerReverseAccelerationInPercent, feedRollerParameters.MovementData.ReverseMovement.AccelerationInPercent);
            SetVariableValue(variableMap.FeedRollerReverseTorque, feedRollerParameters.MovementData.ReverseMovement.Torque);

            SetVariableValue(variableMap.RubberRollerStartingPositions, feedRollerParameters.RubberRollerStartingPositions.OrderBy(r => r.TrackNumber).Select(r => r.Position).ToArray());
        }

        private void SetCrossHeadParameters(EmCrossHeadParameters crossHeadParameters)
        {
            SetVariableValue(variableMap.CrossHeadMaximumPosition, crossHeadParameters.MaxHeadPosition);
            SetVariableValue(variableMap.CrossHeadMinimumPosition, crossHeadParameters.MinHeadPosition);
            SetVariableValue(variableMap.CrossHeadConnectDelayOn, crossHeadParameters.ConnectDelayOn);
            SetVariableValue(variableMap.CrossHeadConnectDelayOff, crossHeadParameters.ConnectDelayOff);
            SetVariableValue(variableMap.CrossHeadCreaseDelayOff, crossHeadParameters.CreaseDelayOff);
            SetVariableValue(variableMap.CrossHeadCreaseDelayOn, crossHeadParameters.CreaseDelayOn);
            SetVariableValue(variableMap.CrossHeadCutDelayOff, crossHeadParameters.CutDelayOff);
            SetVariableValue(variableMap.CrossHeadCutDelayOn, crossHeadParameters.CutDelayOn);
            SetVariableValue(variableMap.CrossHeadReferenceQuantity, crossHeadParameters.ReferenceQuantity);
            SetVariableValue(variableMap.CrossHeadToolPositionToSensor, crossHeadParameters.ToolPositionToSensor);

            SetVariableValue(variableMap.CrossHeadNormalSpeedInPercent, crossHeadParameters.MovementData.NormalMovement.SpeedInPercent);
            SetVariableValue(variableMap.CrossHeadNormalAccelerationInPercent, crossHeadParameters.MovementData.NormalMovement.AccelerationInPercent);
            SetVariableValue(variableMap.CrossHeadNormalTorque, crossHeadParameters.MovementData.NormalMovement.Torque);

        }

        private void SetLongHeadParameters(EmLongHeadParameters longHeadParameters)
        {
            SetVariableValue(variableMap.LongHeadConnectionDelay, longHeadParameters.ConnectDelayOn);
            SetVariableValue(variableMap.LongheadDisconnectDelay, longHeadParameters.ConnectDelayOff);
            SetVariableValue(variableMap.LongheadCreaseDelayOff, longHeadParameters.CreaseDelayOff);
            SetVariableValue(variableMap.LongheadCreaseDelayOn, longHeadParameters.CreaseDelayOn);
            SetVariableValue(variableMap.LongheadCutDelayOn, longHeadParameters.CutDelayOn);
            SetVariableValue(variableMap.LongheadCutDelayOff, longHeadParameters.CutDelayOff);
            SetVariableValue(variableMap.LongheadPositionInWindow, longHeadParameters.LHInPosWindow);

            SetVariableValue(variableMap.LongheadSpeedInPercent, longHeadParameters.MovementData.NormalMovement.SpeedInPercent);
            SetVariableValue(variableMap.LongheadAccelerationInPercent, longHeadParameters.MovementData.NormalMovement.AccelerationInPercent);
            SetVariableValue(variableMap.LongheadTorque, longHeadParameters.MovementData.NormalMovement.Torque);

            SetVariableValue(variableMap.LongheadMaximumPosition, longHeadParameters.MaximumPosition);
            SetVariableValue(variableMap.LongheadMinimumPosition, longHeadParameters.MinimumPosition);

            SetVariableValue(variableMap.NumberOfLongHeads, longHeadParameters.LongHeads.Count());
        }

        private void SetTrackParameters(EmTrackParameters trackParameters)
        {
            SetVariableValue(variableMap.CorrugateWidthTolerance, trackParameters.CorrugateWidthTolerance);
            SetVariableValue(variableMap.TrackChangeDelay, trackParameters.TrackChangeDelay);
            SetVariableValue(variableMap.TrackServiceFeedingDistance, trackParameters.TrackServiceFeedingDistance);
            SetVariableValue(variableMap.UseCorrugateSensors, trackParameters.UseCorrugateSensors);

            SetVariableValue(variableMap.NumberOfTracks, Machine.Tracks.Count());
            SetVariableValue(variableMap.TrackOffsets, Machine.Tracks.OrderBy(t => t.TrackNumber).Select(t => (double)t.TrackOffset).ToArray());
            SetCorrugateWidths(Machine.Tracks); 
        }

        public void SetTrackStartinPositions(IEnumerable<Track> tracks)
        {
            if (IsConnected)
            {
                SetVariableValue(variableMap.TrackOffsets, tracks.Select(t => t.TrackOffset).ToArray());
            }
        }

        private double[] GetWarningArguments(string variableName)
        {
            if (variableName == variableMap.CrossheadOutOfPositionWarning.VariableName)
            {
                return GetVariableValue<double[]>(variableMap.CrossheadOutOfPositionWarningArguments);
            }

            return GetVariableValue<double[]>(variableMap.LongheadsOutOfPositionWarningArguments);
        }

        public void Shutdown()
        {
            Dispose(true);
        }

        public new void Dispose()
        {
            Dispose(true);
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                base.Dispose();
            }
        }
    }
}

