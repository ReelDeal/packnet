﻿using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Communication.Communicators.EM
{
    public interface IEmMachineVariableMap : IPackagingMachineVariableMap
    {
        PacksizePlcVariable ACP10Scaling { get; }

        PacksizePlcVariable TrackChangeDelay { get; }

        PacksizePlcVariable TrackOffsets { get; }

        PacksizePlcVariable LongheadDisconnectDelay { get; }

        PacksizePlcVariable LongheadCreaseDelayOff { get; }

        PacksizePlcVariable LongheadCutDelayOn { get; }

        PacksizePlcVariable LongheadCreaseDelayOn { get; }

        PacksizePlcVariable LongheadCutDelayOff { get; }

        PacksizePlcVariable LongheadPositionInWindow { get; }

        PacksizePlcVariable LongheadSpeedInPercent { get; }

        PacksizePlcVariable LongheadAccelerationInPercent { get; }

        PacksizePlcVariable LongheadSpeed { get; }

        PacksizePlcVariable LongheadAcceleration { get; }

        PacksizePlcVariable LongheadTorque { get; }

        PacksizePlcVariable LongheadMaximumPosition { get; }

        PacksizePlcVariable LongheadMinimumPosition { get; }

        PacksizePlcVariable CrossHeadConnectDelayOn { get; }

        PacksizePlcVariable CrossHeadConnectDelayOff { get; }

        PacksizePlcVariable CrossHeadCreaseDelayOff { get; }

        PacksizePlcVariable CrossHeadCreaseDelayOn { get; }

        PacksizePlcVariable CrossHeadCutDelayOff { get; }

        PacksizePlcVariable CrossHeadCutDelayOn { get; }

        PacksizePlcVariable CrossHeadReferenceQuantity { get; }

        PacksizePlcVariable CrossHeadToolPositionToSensor { get; }

        PacksizePlcVariable CrossHeadPosition { get; }

        PacksizePlcVariable CrossHeadSpeed { get; }

        PacksizePlcVariable CrossHeadAcceleration { get; }

        PacksizePlcVariable CrossHeadNormalSpeedInPercent { get; }

        PacksizePlcVariable CrossHeadNormalAccelerationInPercent { get; }

        PacksizePlcVariable CrossHeadNormalTorque { get; }

        PacksizePlcVariable FeedRollerToolsGearRatio { get; }

        PacksizePlcVariable FeedRollerOutFeedLength { get; }

        PacksizePlcVariable FeedRollerSpeed { get; }

        PacksizePlcVariable FeedRollerAcceleration { get; }

        PacksizePlcVariable FeedRollerNormalSpeedInPercent { get; }

        PacksizePlcVariable FeedRollerNormalAccelerationInPercent { get; }

        PacksizePlcVariable FeedRollerNormalTorque { get; }

        PacksizePlcVariable FeedRollerReverseSpeedInPercent { get; }

        PacksizePlcVariable FeedRollerReverseAccelerationInPercent { get; }

        PacksizePlcVariable FeedRollerReverseTorque { get; }

        PacksizePlcVariable RubberRollerStartingPositions { get; }

        PacksizePlcVariable TrackServiceFeedingDistance { get; }

        PacksizePlcVariable UseCorrugateSensors { get; }

        PacksizePlcVariable CrossheadOutOfPositionWarning { get; }

        PacksizePlcVariable CrossheadOutOfPositionWarningArguments { get; }

        PacksizePlcVariable LongheadsOutOfPositionWarning { get; }

        PacksizePlcVariable LongheadsOutOfPositionWarningArguments { get; }

        PacksizePlcVariable Track1ChangingCorrugate { get; }

        PacksizePlcVariable Track2ChangingCorrugate { get; }

        PacksizePlcVariable Track3ChangingCorrugate { get; }

        PacksizePlcVariable Track4ChangingCorrugate { get; }
    }
}
