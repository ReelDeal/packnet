﻿
namespace PackNet.Communication.Communicators.EM
{
    using System.Collections.Generic;
    using System.Linq;

    using Common.Interfaces.DTO.PLC;

    public class EmMachineVariables : PacksizeBaseMachineVariableMap, IEmMachineVariableMap
    {
        public new static EmMachineVariables Instance;

        static EmMachineVariables()
        {
            Instance = new EmMachineVariables();
        }

        private EmMachineVariables()
        {
            ACP10Scaling = new PacksizePlcVariable(string.Empty, "ACP10MC_SCALINGFACTOR");
            MapFeedRollerParameters();
            MapEmCrossHeadParameters();
            MapEmLongHeadParameters();
            MapEmTrackParameters();

            CrossheadOutOfPositionWarning = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CrossheadOutOfPositionWarning.Code", true, true);
            LongheadsOutOfPositionWarning = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "LongheadsOutOfPositionWarning.Code", true, true);
            CrossheadOutOfPositionWarningArguments = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CrossheadOutOfPositionWarning.Arguments", false, true);
            LongheadsOutOfPositionWarningArguments = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "LongheadsOutOfPositionWarning.Arguments", false, true);
        }
        
        public PacksizePlcVariable CrossheadOutOfPositionWarning { get; private set; }
        public PacksizePlcVariable CrossheadOutOfPositionWarningArguments { get; private set; }
        public PacksizePlcVariable LongheadsOutOfPositionWarning { get; private set; }
        public PacksizePlcVariable LongheadsOutOfPositionWarningArguments { get; private set; }


        #region TrackParameters

        public PacksizePlcVariable ACP10Scaling { get; private set; }
        public PacksizePlcVariable TrackChangeDelay { get; private set; }
        public PacksizePlcVariable TrackOffsets { get; private set; }
        public PacksizePlcVariable RubberRollerStartingPositions { get; private set; }
        public PacksizePlcVariable UseCorrugateSensors { get; private set; }
        public PacksizePlcVariable Track1ChangingCorrugate { get; private set; }
        public PacksizePlcVariable Track2ChangingCorrugate { get; private set; }
        public PacksizePlcVariable Track3ChangingCorrugate { get; private set; }
        public PacksizePlcVariable Track4ChangingCorrugate { get; private set; }

        public void MapEmTrackParameters()
        {
            UseCorrugateSensors = new PacksizePlcVariable(EmTasks.Main.ToString(), "TRACK_SETTINGS.UseCorrugateSensors");
            TrackChangeDelay = new PacksizePlcVariable(EmTasks.Main.ToString(), "TRACK_SETTINGS.ChangeDelay");
            TrackOffsets = new PacksizePlcVariable(EmTasks.Main.ToString(), "TRACK_SETTINGS.TrackStartingPosition");
            RubberRollerStartingPositions = new PacksizePlcVariable(EmTasks.Main.ToString(), "TRACK_SETTINGS.PressureRollerStartingPosition");
            Track1ChangingCorrugate = new PacksizePlcVariable(EmTasks.Main.ToString(), "trackNeedsCleanCut[0]", true);
            Track2ChangingCorrugate = new PacksizePlcVariable(EmTasks.Main.ToString(), "trackNeedsCleanCut[1]", true);
            Track3ChangingCorrugate = new PacksizePlcVariable(EmTasks.Main.ToString(), "trackNeedsCleanCut[2]", true);
            Track4ChangingCorrugate = new PacksizePlcVariable(EmTasks.Main.ToString(), "trackNeedsCleanCut[3]", true);
        }

        #endregion

        #region LongHeadParameters

        public PacksizePlcVariable LongheadDisconnectDelay { get; private set; }
        public PacksizePlcVariable LongheadCreaseDelayOff { get; private set; }
        public PacksizePlcVariable LongheadCutDelayOn { get; private set; }
        public PacksizePlcVariable LongheadCreaseDelayOn { get; private set; }
        public PacksizePlcVariable LongheadCutDelayOff { get; private set; }
        public PacksizePlcVariable LongheadPositionInWindow { get; private set; }

        public PacksizePlcVariable LongheadSpeedInPercent { get; private set; }
        public PacksizePlcVariable LongheadSpeed { get; private set; }
        public PacksizePlcVariable LongheadAccelerationInPercent { get; private set; }
        public PacksizePlcVariable LongheadAcceleration { get; private set; }
        public PacksizePlcVariable LongheadTorque { get; private set; }

        public PacksizePlcVariable LongheadMaximumPosition { get; private set; }
        public PacksizePlcVariable LongheadMinimumPosition { get; private set; }

        private void MapEmLongHeadParameters()
        {
            LongheadDisconnectDelay = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.DisconnectDelay");

            LongheadCreaseDelayOff = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.CreaseDelayOff");
            LongheadCreaseDelayOn = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.CreaseDelayOn");

            LongheadCutDelayOn = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.CutDelayOn");
            LongheadCutDelayOff = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.CutDelayOff");

            LongheadPositionInWindow = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.PositionInWindow");

            LongheadSpeedInPercent = new PacksizePlcVariable("", "LongheadNormalMovement.SpeedInPercent");
            LongheadSpeed = new PacksizePlcVariable("", "LongheadNormalMovement.Speed");
            LongheadAccelerationInPercent = new PacksizePlcVariable("", "LongheadNormalMovement.AccelerationInPercent");
            LongheadAcceleration = new PacksizePlcVariable("", "LongheadNormalMovement.Acceleration");
            LongheadTorque = new PacksizePlcVariable("", "LongheadNormalMovement.Torque");

            LongheadMaximumPosition = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.MaximumPosition");
            LongheadMinimumPosition = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.MinimumPosition");
        }

        #endregion

        #region CrossHeadParameters
        public PacksizePlcVariable CrossHeadConnectDelayOn { get; private set; }
        public PacksizePlcVariable CrossHeadConnectDelayOff { get; private set; }

        public PacksizePlcVariable CrossHeadCreaseDelayOff { get; private set; }
        public PacksizePlcVariable CrossHeadCreaseDelayOn { get; private set; }

        public PacksizePlcVariable CrossHeadCutDelayOff { get; private set; }
        public PacksizePlcVariable CrossHeadCutDelayOn { get; private set; }

        public PacksizePlcVariable CrossHeadReferenceQuantity { get; private set; }
        public PacksizePlcVariable CrossHeadToolPositionToSensor { get; private set; }
        public PacksizePlcVariable CrossHeadPosition { get; private set; }
   
        public PacksizePlcVariable CrossHeadSpeed { get; private set; }
        public PacksizePlcVariable CrossHeadAcceleration { get; private set; }
        
        public PacksizePlcVariable CrossHeadNormalSpeedInPercent { get; private set; }
        public PacksizePlcVariable CrossHeadNormalAccelerationInPercent { get; private set; }
        public PacksizePlcVariable CrossHeadNormalTorque { get; private set; }

        public void MapEmCrossHeadParameters()
        {
            CrossHeadConnectDelayOn = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.ConnectionDelayOn");
            CrossHeadConnectDelayOff = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.ConnectionDelayOff");

            CrossHeadCreaseDelayOff = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.CreaseDelayOff");
            CrossHeadCreaseDelayOn = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.CreaseDelayOn");

            CrossHeadCutDelayOff = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.CutDelayOff");
            CrossHeadCutDelayOn = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.CutDelayOn");

            CrossHeadReferenceQuantity = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.ReferenceQuantity");
            CrossHeadToolPositionToSensor = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.ToolToSensor"); //TODO is this one needed on the AS side or is it just used for code generation?

            CrossHeadSpeed = new PacksizePlcVariable("", "toolAxis.move.basis.parameter.v_pos", false, true);
            CrossHeadAcceleration = new PacksizePlcVariable("", "toolAxis.move.basis.parameter.a1_pos", false, true);
            
            CrossHeadNormalSpeedInPercent = new PacksizePlcVariable("", "CrossHeadNormalMovement.SpeedInPercent");
            CrossHeadNormalAccelerationInPercent = new PacksizePlcVariable("", "CrossHeadNormalMovement.AccelerationInPercent");
            CrossHeadNormalTorque = new PacksizePlcVariable("", "CrossHeadNormalMovement.Torque");
            
            CrossHeadPosition = new PacksizePlcVariable(EmTasks.Main.ToString(), "CH_SETTINGS.Position");
        }

        #endregion

        #region FeedRollerParameters
        public PacksizePlcVariable FeedRollerToolsGearRatio { get; private set; }
        public PacksizePlcVariable FeedRollerOutFeedLength { get; private set; }

        public PacksizePlcVariable FeedRollerSpeed { get; private set; }
        public PacksizePlcVariable FeedRollerAcceleration { get; private set; }
        
        public PacksizePlcVariable FeedRollerNormalSpeedInPercent { get; private set; }
        public PacksizePlcVariable FeedRollerNormalAccelerationInPercent { get; private set; }
        public PacksizePlcVariable FeedRollerNormalTorque { get; private set; }
 
        public PacksizePlcVariable FeedRollerReverseSpeedInPercent { get; private set; }
        public PacksizePlcVariable FeedRollerReverseAccelerationInPercent { get; private set; }
        public PacksizePlcVariable FeedRollerReverseTorque { get; private set; }

        private void MapFeedRollerParameters()
        {
            FeedRollerToolsGearRatio = new PacksizePlcVariable(EmTasks.Main.ToString(), "GEAR_RATIO");
            FeedRollerOutFeedLength = new PacksizePlcVariable(EmTasks.Main.ToString(), "OUTFEEDLENGTH");

            FeedRollerSpeed = new PacksizePlcVariable("", "rollAxis.move.basis.parameter.v_pos", false, true);
            FeedRollerAcceleration = new PacksizePlcVariable("", "rollAxis.move.basis.parameter.a1_pos", false, true);
            
            FeedRollerNormalSpeedInPercent = new PacksizePlcVariable("", "FeedRollerNormalMovement.SpeedInPercent");
            FeedRollerNormalAccelerationInPercent = new PacksizePlcVariable("", "FeedRollerNormalMovement.AccelerationInPercent");
            FeedRollerNormalTorque = new PacksizePlcVariable("", "FeedRollerNormalMovement.Torque");

            FeedRollerReverseSpeedInPercent = new PacksizePlcVariable("", "FeedRollerReverseMovement.SpeedInPercent");
            FeedRollerReverseAccelerationInPercent = new PacksizePlcVariable("", "FeedRollerReverseMovement.AccelerationInPercent");
            FeedRollerReverseTorque = new PacksizePlcVariable("", "FeedRollerReverseMovement.Torque");
        }

        #endregion      
        
        public new IEnumerable<PacksizePlcVariable> AsEnumerable()
        {
            var info = typeof(EmMachineVariables).GetProperties();

            return info.Select(pi => (PacksizePlcVariable)pi.GetValue(this, null)).ToList();
        }
    }
}
