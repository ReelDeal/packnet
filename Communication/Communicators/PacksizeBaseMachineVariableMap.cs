﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Communication.Communicators
{
    public class PacksizeBaseMachineVariableMap : IPackagingMachineVariableMap
    {
        public static PacksizeBaseMachineVariableMap Instance;

        static PacksizeBaseMachineVariableMap()
        {
            Instance = new PacksizeBaseMachineVariableMap();
        }

        protected PacksizeBaseMachineVariableMap()
        {
            MapProductionParameters();
            MapStatusParameters();
            MapNetworkParameters();
            MapErrorParameters();
            MapTrackParameters();
            MapServiceParameters();
            MapLongHeadParameters();
            MapCrossHeadParameters();
            MapConfigurationParameters();
        }

        #region Configuration Parameters

        public PacksizePlcVariable IsInches { get; private set; }

        private void MapConfigurationParameters()
        {
            IsInches = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "isInches");
        }

        #endregion

        #region ProductionParameters
        public PacksizePlcVariable StartedProductionItemId { get; private set; }

        public PacksizePlcVariable ProductionHistoryItemId { get; private set; }
        public PacksizePlcVariable ProductionHistoryItemCompletionStatus { get; private set; }
        public PacksizePlcVariable ProductionHistoryItemCorrugateUsage { get; private set; }
        public PacksizePlcVariable ProductionHistoryItemCompletionTime { get; private set; }

        public PacksizePlcVariable RemovedProductionItemId1 { get; private set; }
        public PacksizePlcVariable RemovedProductionItemId2 { get; private set; }

        public PacksizePlcVariable ReleaseBox { get; private set; }
        public PacksizePlcVariable TransferBuffer { get; private set; }
        public PacksizePlcVariable ReadyForProcessing { get; private set; }

        public PacksizePlcVariable WaitingForBoxReleaseId { get; private set; }


        public PacksizePlcVariable FailingInstructionListId { get; private set; }
        public PacksizePlcVariable FailingInstructionList { get; private set; }


        private void MapProductionParameters()
        {
            ReleaseBox = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ReleaseBox");
            TransferBuffer = new PacksizePlcVariable("", "PcTransferBuffer");
            ReadyForProcessing = new PacksizePlcVariable("", "PcTransferBuffer.ReadyForProcessing");

            WaitingForBoxReleaseId = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "WaitingForBoxReleaseId.Parts", true, true);

            StartedProductionItemId = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "StartedProductionItemId.Parts", true);

            ProductionHistoryItemId = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentProductionHistoryItem.Id.Parts", true);
            ProductionHistoryItemCompletionStatus = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentProductionHistoryItem.CompletionStatus");
            ProductionHistoryItemCorrugateUsage = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentProductionHistoryItem.CorrugateUsage");
            ProductionHistoryItemCompletionTime = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentProductionHistoryItem.CompletionTime");

            RemovedProductionItemId1 = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "RemovedProductionItemId[0].Parts", true);
            RemovedProductionItemId2 = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "RemovedProductionItemId[1].Parts", true);

            FailingInstructionListId = new PacksizePlcVariable("", "FailingTransferData.Id.Parts", false, true);
            FailingInstructionList = new PacksizePlcVariable("", "FailingTransferData.InstructionList", false, true);
        }
        #endregion

        #region StatusParameters

        public PacksizePlcVariable ChangeCorrugate { get; private set; }
        public PacksizePlcVariable MachinePaused { get; private set; }
        public PacksizePlcVariable StartupProcedureCompleted { get; private set; }
        public PacksizePlcVariable MachineInPausedState { get; private set; }
        public PacksizePlcVariable RunPrestart { get; private set; }
        public PacksizePlcVariable OutOfCorrugate { get; private set; }
        public PacksizePlcVariable InCalibrationMode { get; private set; }
        public PacksizePlcVariable InServiceMode { get; private set; }

        private void MapStatusParameters()
        {
            OutOfCorrugate = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "outOfCorrugate", true);

            MachinePaused = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "MACHINE_PAUSED", true);

            StartupProcedureCompleted = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "StartupProcedureCompleted", true);
            RunPrestart = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "RUN_PRESTART");

            ChangeCorrugate = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "changeCorrugate", true);
            MachineInPausedState = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "EnteredPausedState");

            InServiceMode = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "InServiceMode", true);
            InCalibrationMode = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "InCalibrationMode", true);
        }
        #endregion

        #region NetworkParameters
        public PacksizePlcVariable PcLifeSign { get; private set; }
        public PacksizePlcVariable PlcLifeSign { get; private set; }
        public PacksizePlcVariable ConnectionStatus { get; private set; }

        public PacksizePlcVariable EventNotificationRegistrationBuffer { get; private set; }
        public PacksizePlcVariable EventNotificationVariablesCounter { get; private set; }

        public PacksizePlcVariable EventNotifierAddress { get; private set; }
        public PacksizePlcVariable EventNotifierPort { get; private set; }
        
        public PacksizePlcVariable HostName { get; private set; }
        public PacksizePlcVariable DefaultGateway { get; private set; }
        public PacksizePlcVariable StaticIp { get; private set; }
        public PacksizePlcVariable SubnetMask { get; private set; }
        public PacksizePlcVariable DHCP { get; private set; }
        
        public PacksizePlcVariable ExecuteNetworkSettings { get; private set; }
        public PacksizePlcVariable CurrentHostname { get; private set; }
        public PacksizePlcVariable CurrentNetworkMode { get; private set; }
        public PacksizePlcVariable CurrentIpAddress { get; private set; }
        public PacksizePlcVariable CurrentSubnetMask { get; private set; }
        public PacksizePlcVariable CurrentGateway { get; private set; }
        public PacksizePlcVariable MacAddress { get; private set; }
        
        private void MapNetworkParameters()
        {
            PcLifeSign = new PacksizePlcVariable(PacksizeMachineTasks.PCCommunic.ToString(), "PcLifeSign");
            PlcLifeSign = new PacksizePlcVariable(PacksizeMachineTasks.PCCommunic.ToString(), "PlcLifeSign");
            ConnectionStatus = new PacksizePlcVariable(PacksizeMachineTasks.PCCommunic.ToString(), "ConnectionStatus");

            EventNotificationRegistrationBuffer = new PacksizePlcVariable(PacksizeMachineTasks.VariableEv.ToString(), "VariablesToNotifyChanges");
            EventNotificationVariablesCounter = new PacksizePlcVariable(PacksizeMachineTasks.VariableEv.ToString(), "NotifyVariableCounter");

            EventNotifierAddress = new PacksizePlcVariable(PacksizeMachineTasks.VariableEv.ToString(), "eventNotifierAddress");
            EventNotifierPort = new PacksizePlcVariable(PacksizeMachineTasks.VariableEv.ToString(), "eventNotifierPort");

            HostName = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "hostName");
            StaticIp = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "staticIp");
            DHCP = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "setDhcp");
            SubnetMask = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "subnetMask");
            DefaultGateway = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "DefaultGateway");

            ExecuteNetworkSettings = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "SetNetworkSettings");
            CurrentGateway = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "CurrentGateway");
            CurrentHostname = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "CurrentHostName");
            CurrentIpAddress = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "CurrentIpAddress");
            CurrentNetworkMode = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "CurrentNetworkMode");
            CurrentSubnetMask = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "CurrentSubnetMask");
            MacAddress = new PacksizePlcVariable(IqFusionTasks.RestPostSe.ToString(), "MacAddress");
        }

        #endregion

        #region ErrorParameters

        public PacksizePlcVariable ClearMachineErrors { get; private set; }
        public PacksizePlcVariable ErrorCode1 { get; private set; }
        public PacksizePlcVariable ErrorCode1Arguments { get; private set; }
        public PacksizePlcVariable ErrorCode2 { get; private set; }
        public PacksizePlcVariable ErrorCode2Arguments { get; private set; }
        public PacksizePlcVariable ErrorCode3 { get; private set; }
        public PacksizePlcVariable ErrorCode3Arguments { get; private set; }
        public PacksizePlcVariable ErrorCode4 { get; private set; }
        public PacksizePlcVariable ErrorCode4Arguments { get; private set; }
        public PacksizePlcVariable ErrorCode5 { get; private set; }
        public PacksizePlcVariable ErrorCode5Arguments { get; private set; }

        private void MapErrorParameters()
        {
            ClearMachineErrors = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CLEAR_MACHINE_ERRORS");
            ErrorCode1 = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[0].Code", true);
            ErrorCode1Arguments = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[0].Arguments");
            ErrorCode2 = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[1].Code", true);
            ErrorCode2Arguments = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[1].Arguments");
            ErrorCode3 = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[2].Code", true);
            ErrorCode3Arguments = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[2].Arguments");
            ErrorCode4 = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[3].Code", true);
            ErrorCode4Arguments = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[3].Arguments");
            ErrorCode5 = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[4].Code", true);
            ErrorCode5Arguments = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ERROR_CODES[4].Arguments");
        }

        #endregion

        #region TrackParameters
        public PacksizePlcVariable NumberOfTracks { get; private set; }
        public PacksizePlcVariable TrackWidths { get; private set; }
        public PacksizePlcVariable CorrugateWidthTolerance { get; private set; }

        private void MapTrackParameters()
        {
            NumberOfTracks = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "TRACK_SETTINGS.NumberOfTracks");
            TrackWidths = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "TRACK_SETTINGS.TrackWidth");
            CorrugateWidthTolerance = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "TRACK_SETTINGS.CorrugateWidthTolerance");
        }
        #endregion

        #region LongHeadParameters

        public PacksizePlcVariable LongHeadConnectionDelay { get; private set; }
        public PacksizePlcVariable NumberOfLongHeads { get; private set; }
        public PacksizePlcVariable LongHeadPositions { get; private set; }

        private void MapLongHeadParameters()
        {
            LongHeadConnectionDelay = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.ConnectionDelay");
            NumberOfLongHeads = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.NumberOfLongHeads");
            LongHeadPositions = new PacksizePlcVariable(EmTasks.Main.ToString(), "LH_SETTINGS.Positions");
        }
        #endregion

        #region CrossHeadParameters

        public PacksizePlcVariable CrossHeadMinimumPosition { get; private set; }
        public PacksizePlcVariable CrossHeadMaximumPosition { get; private set; }

        private void MapCrossHeadParameters()
        {
            CrossHeadMaximumPosition = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CH_SETTINGS.MaxPosition");
            CrossHeadMinimumPosition = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CH_SETTINGS.MinPosition");
        }
        #endregion

        #region ServiceParameters

        public PacksizePlcVariable TrackServiceFeedingDistance { get; private set; }

        public PacksizePlcVariable MachineType { get; private set; }
        public PacksizePlcVariable MachinePlcVersion { get; private set; }

        private void MapServiceParameters()
        {
            TrackServiceFeedingDistance = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "TRACK_SETTINGS.ServiceFeedingDistance");

            MachineType = new PacksizePlcVariable(string.Empty, "MACHINE_TYPE");
            MachinePlcVersion = new PacksizePlcVariable(string.Empty, "PLC_VERSION");
        }

        #endregion

        public virtual IEnumerable<PacksizePlcVariable> AsEnumerable()
        {
            var info = typeof(PacksizeBaseMachineVariableMap).GetProperties();

            return info.Select(pi => (PacksizePlcVariable)pi.GetValue(this, null)).ToList();
        }
        public bool ContainsVariable( string var)
        {
            return AsEnumerable().Any(v => v.ToString().Equals(var.Trim()));
        }
    }
}
