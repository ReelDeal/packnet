﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using Newtonsoft.Json;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Communication.L10N;
using PackNet.Communication.PLCBase;

namespace PackNet.Communication
{
    /// <summary>
    /// Responsible for establishing communication to the B and R Simple PLC and communicating over http
    /// </summary>
    public class SimplePLCCommunicator : ISimplePLCCommunicator
    {
        private const int DefaultWebRequestRetry = 3;

        private readonly object semaphore = new object();

        private IWebRequestCreator webRequestCreator;
        private readonly ILogger logger;

        /// <summary>
        /// Creates an instance to be used to communicate with a Packsize machine
        /// </summary>
        /// <param name="webRequestCreator"></param>
        /// <param name="logger"></param>
        public SimplePLCCommunicator(IWebRequestCreator webRequestCreator,
            ILogger logger)
        {
            this.webRequestCreator = webRequestCreator;
            this.logger = logger;
        }

        public bool UpdateWebRequestCreator(IWebRequestCreator newWebRequestCreator)
        {
            if (newWebRequestCreator.ServerAddress != webRequestCreator.ServerAddress)
            {
                webRequestCreator = newWebRequestCreator;
                return true;
            }

            return false;
        }

        public string PlcIpAddressWithPort
        {
            get { return webRequestCreator.ServerAddress; }
        }

        /// <summary>
        /// Reads a value from the PLC
        /// </summary>
        /// <typeparam name="T">Type of value to read</typeparam>
        /// <param name="variable">Variable to read</param>
        /// <param name="retryCount"></param>
        /// <returns></returns>
        public T ReadValue<T>(PacksizePlcVariable variable, int retryCount = -1)
        {
            return CommunicateToPLC(false, variable, default(T), retryCount < 0 ? DefaultWebRequestRetry : retryCount);
        }

        /// <summary>
        /// Writes a value into the PLC
        /// </summary>
        /// <typeparam name="T">Type of value to write</typeparam>
        /// <param name="variable">Variable to write</param>
        /// <param name="value">value to write</param>
        /// <param name="retryCount"></param>
        public void WriteValue<T>(PacksizePlcVariable variable, T value, int retryCount = -1)
        {
            CommunicateToPLC(true, variable, value, retryCount < 0 ? DefaultWebRequestRetry : retryCount);
        }

        private T CommunicateToPLC<T>(bool isWriteOperation, PacksizePlcVariable variable, T value, int retries)
        {
            lock (semaphore)
            {
                logger.Log(LogLevel.Trace, "CommunicateToPLC<T>(isWriteOperation: '{0}' variable: '{1}' value: '{2}' retries: '{3}'", isWriteOperation, variable, value, retries);
                
                try
                {
                    if (isWriteOperation)
                    {
                        if (variable.ReadOnly)
                        {
                            throw new ReadOnlyException(string.Format("Variable {0}:{1} is Readonly", variable.TaskName,
                                variable.VariableName));
                        }
                        var sw = Stopwatch.StartNew();
                        var request = webRequestCreator.CreatePostRequest(variable, value);
                        var response = SendRequestAndGetResponse(variable.TaskName, variable.VariableName, request);
                        var successfulWrite = JsonConvert.DeserializeObject<bool>(response);
                        logger.Log(LogLevel.Trace, "PLCCommunication took {3} to '{0}' Write Variable:{1} Value:{2}", webRequestCreator.ServerAddress, variable.VariableName, JsonConvert.SerializeObject(value), sw.ElapsedMilliseconds);
                        
                        if (successfulWrite == false)
                        {
                            throw new WriteException(variable.TaskName, variable.VariableName, value);
                        }

                        return default(T); //Return value is ignored
                    }
                    else
                    {
                        var request = webRequestCreator.CreateGetRequest(variable);
                        var response = SendRequestAndGetResponse(variable.TaskName, variable.VariableName, request);
                        
                        if (string.IsNullOrWhiteSpace(response) || response == "null")
                        {
                            return default(T);
                        }

                        var deserializeObject = JsonConvert.DeserializeObject<T>(response);
                        logger.Log(LogLevel.Trace, "PLCCommunication to '{0}' Read Variable:{1} has Value:{2}", webRequestCreator.ServerAddress, variable.VariableName, response);

                        return deserializeObject;
                    }
                }
                catch (IOException e)
                {
                    logger.Log(LogLevel.Error, e.ToString());
                    throw new ConnectionException(webRequestCreator.ServerAddress, e);
                }
                catch (WebException ex)
                {
                    retries--;
                    var webResponse = ex.Response;
                    
                    if (webResponse != null)
                    {
                        ((IDisposable)webResponse).Dispose();
                    }

                    if (ex.Message.Contains(Strings.SimplePLCCommunicator_CommunicateToPLC__404__Not_Found_))
                    {
                        //site is not accessible.  Machine is probably turned off.
                        logger.Log(LogLevel.Trace,
                            "Unable to contact machine '" + webRequestCreator.ServerAddress +
                            "', Verify machine is powered on and network connectivity can be established");

                        if (retries <= 0)
                        {
                            throw new ConnectionException(webRequestCreator.ServerAddress,
                                "Unable to contact machine '" + webRequestCreator.ServerAddress +
                                "', Verify machine is powered on and network connectivity can be established",
                                ex);
                        }
                    }
                    else if (webResponse != null)
                    {
                        logger.LogException(LogLevel.Debug, webResponse.ToString(),
                            new Exception("webResponse was not null, disposing." + webRequestCreator.ServerAddress));
                    }
                    
                    //Retry logic
                    logger.LogException(
                        LogLevel.Trace,
                        string.Format("Get web request exception. Retries Left: {0} Task: {1} Variable: {2} Fusion: {3} ",
                            retries, variable.TaskName, variable.VariableName, webRequestCreator.ServerAddress),
                        ex);

                    if (retries > 0)
                    {
                        Thread.Sleep(300);
                        return CommunicateToPLC(isWriteOperation, variable, value, retries);
                    }

                    throw new ConnectionException(webRequestCreator.ServerAddress, ex);
                }
                catch (MissingVariableException e)
                {
                    logger.Log(LogLevel.Error,
                        "Error while communicating to Packsize Cut Crease Machine '" + webRequestCreator.ServerAddress +
                        "' Variable Name: " +
                        e.VariableName + " Task Name: " + e.TaskName + "' Details: " + e);
                    throw;
                }
                catch (WriteException e)
                {
                    logger.Log(LogLevel.Error,
                        "Error while communicating to Packsize Cut Crease Machine '" + webRequestCreator.ServerAddress +
                        "' Variable Name: " +
                        e.VariableName + " Task Name: " + e.TaskName + " Value: " + e.Value + "' Details: " + e);
                    throw;
                }
                catch (ReadOnlyException e)
                {
                    logger.Log(LogLevel.Error,
                           "Error while communicating to Packsize Cut Crease Machine '" + webRequestCreator.ServerAddress +
                           "' Message:: " + e.Message);
                    throw;
                    
                }
                catch (Exception e)
                {
                    logger.Log(LogLevel.Error, "Error while communicating to Packsize Cut Crease Machine '" + webRequestCreator.ServerAddress + "' Details: " + e);
                    throw;
                }
            }
        }

        private string SendRequestAndGetResponse(string taskName, string variableName, WebRequest request)
        {
            string result;

            using (var ws = request.GetResponse())
            {
                using (var stream = ws.GetResponseStream())
                {
                    result = ParseResponseData(stream, taskName, variableName);
                }

                ws.Close();
            }

            return result;
        }

        private string ParseResponseData(Stream stream, string taskName, string variableName)
        {
            using (var sr = new StreamReader(stream))
            {
                string response = sr.ReadToEnd();
                
                if (response == string.Empty)
                {
                    throw new MissingVariableException(taskName, variableName);
                }

                response = response.Remove(0, response.IndexOf("\":", StringComparison.Ordinal) + 2);
                response = response.Remove(response.Length - 1);
                return response;
            }
        }

        public void Dispose()
        {
            logger.Log(LogLevel.Trace, "Tearing down simplePLCComm to {0}",this.webRequestCreator.ServerAddress);
        }
    }
}
