﻿using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Communication.PLCBase;

namespace PackNet.Communication
{
    public interface IPackagingMachineVariableMap : IProviderCommunicatorVariableMap
    {
        PacksizePlcVariable ErrorCode1 { get; }

        PacksizePlcVariable ErrorCode1Arguments { get; }

        PacksizePlcVariable ErrorCode2 { get; }

        PacksizePlcVariable ErrorCode2Arguments { get; }

        PacksizePlcVariable ErrorCode3 { get; }

        PacksizePlcVariable ErrorCode3Arguments { get; }

        PacksizePlcVariable ErrorCode4 { get; }

        PacksizePlcVariable ErrorCode4Arguments { get; }

        PacksizePlcVariable ErrorCode5 { get; }

        PacksizePlcVariable ErrorCode5Arguments { get; }

        PacksizePlcVariable MachinePaused { get; }

        PacksizePlcVariable StartupProcedureCompleted { get; }
        
        PacksizePlcVariable MachineInPausedState { get; }
        
        PacksizePlcVariable NumberOfTracks { get; }
        
        PacksizePlcVariable TrackWidths { get; }
        
        PacksizePlcVariable ChangeCorrugate { get; }
        
        PacksizePlcVariable ReadyForProcessing { get; }

        PacksizePlcVariable TransferBuffer { get; }
        
        PacksizePlcVariable ProductionHistoryItemCorrugateUsage { get; }
        
        PacksizePlcVariable ProductionHistoryItemCompletionTime { get; }
        
        PacksizePlcVariable ClearMachineErrors { get; }

        PacksizePlcVariable CorrugateWidthTolerance { get; }

        PacksizePlcVariable LongHeadConnectionDelay { get; }

        PacksizePlcVariable CrossHeadMinimumPosition { get; }

        PacksizePlcVariable CrossHeadMaximumPosition { get; }

        PacksizePlcVariable NumberOfLongHeads { get; }

        PacksizePlcVariable LongHeadPositions { get; }

        PacksizePlcVariable ProductionHistoryItemCompletionStatus { get; }

        PacksizePlcVariable ProductionHistoryItemId { get; }

        PacksizePlcVariable RemovedProductionItemId1 { get; }

        PacksizePlcVariable RemovedProductionItemId2 { get; }

        PacksizePlcVariable StartedProductionItemId { get; }

        PacksizePlcVariable RunPrestart { get; }

        PacksizePlcVariable OutOfCorrugate { get; }

        PacksizePlcVariable ReleaseBox { get; }

        PacksizePlcVariable WaitingForBoxReleaseId { get; }
        
        PacksizePlcVariable MachineType { get; }

        PacksizePlcVariable MachinePlcVersion { get; }

        PacksizePlcVariable InCalibrationMode { get; }

        PacksizePlcVariable InServiceMode { get; }

        PacksizePlcVariable HostName { get; }
        PacksizePlcVariable DefaultGateway { get; }
        PacksizePlcVariable StaticIp { get; }
        PacksizePlcVariable SubnetMask { get; }
        PacksizePlcVariable DHCP { get; }
        PacksizePlcVariable ExecuteNetworkSettings { get; }
        PacksizePlcVariable CurrentHostname { get; }
        PacksizePlcVariable CurrentNetworkMode { get; }
        PacksizePlcVariable CurrentIpAddress { get; }
        PacksizePlcVariable CurrentSubnetMask { get; }
        PacksizePlcVariable CurrentGateway { get; }
        PacksizePlcVariable MacAddress { get; }

        PacksizePlcVariable IsInches { get; }

        PacksizePlcVariable FailingInstructionListId { get; }
        PacksizePlcVariable FailingInstructionList { get; }
    }
}
