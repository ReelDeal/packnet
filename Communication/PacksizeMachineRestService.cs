﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.L10N;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services.Machines;

namespace PackNet.Communication
{
    using System.ServiceModel.Description;
    using System.Web;

    /// <summary>
    /// WCF REST service that handles commands from Packsize machines
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class PacksizeMachineRestService : IPacksizeMachineRESTService
    {
        private ServiceHost host;
        private Uri HostUri;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;

        public IPEndPoint EndPoint
        {
            get { return new IPEndPoint(Dns.GetHostAddresses(HostUri.Host)[0], HostUri.Port); }
        }

        public string Name { get { return "MachineRESTService"; } }

        public PacksizeMachineRestService(IEventAggregatorPublisher publisher, IEventAggregatorSubscriber subscriber, ILogger logger, PackNetServerSettings settings)
        {
            this.publisher = publisher;
            this.logger = logger;

            CreateHost(settings.MachineCallbackPort);
            subscriber.GetEvent<ResponseMessage<PackNetServerSettings>>()
                .Where(m => m.MessageType == PackNetServerSettingsMessages.ServerSettingsUpdated)
                .DurableSubscribe(
                    m =>
                    {
                        if (HostUri.Port == m.Data.MachineCallbackPort)
                            return;

                        CloseHost();
                        CreateHost(m.Data.MachineCallbackPort);
                    });
        }

        private void CreateHost(int port)
        {
            if (!Uri.TryCreate("http://localhost:" + port, UriKind.Absolute, out HostUri))
            {
                HostUri = new Uri("http://localhost:" + port);
            }

            host = new ServiceHost(this, HostUri);
            var sep = host.AddServiceEndpoint(typeof(IPacksizeMachineRESTService), new WebHttpBinding(), "");
            var whb = sep.Behaviors.Find<WebHttpBehavior>();

            if (whb != null)
            {
                whb.AutomaticFormatSelectionEnabled = true;
            }
            else
            {
                var webBehavior = new WebHttpBehavior { AutomaticFormatSelectionEnabled = true };
                sep.Behaviors.Add(webBehavior);
            }

            OpenHost();
        }

        private void OpenHost()
        {
            if (host.State == CommunicationState.Created
                && (host.State != CommunicationState.Opened || host.State != CommunicationState.Opening))
            {
                try
                {
                    host.Open();
                }
                catch (Exception e)
                {
                    logger.LogException(LogLevel.Fatal, string.Format(Strings.PacksizeMachineRestService_OpenHost_Could_not_bind_Json_Eventnotifier_listener_to_address__0__, HostUri), e);
                    throw;
                }
            }
        }

        private void CloseHost()
        {
            if (host.State == CommunicationState.Opened
                && (host.State != CommunicationState.Closed || host.State != CommunicationState.Closing))
            {
                host.Abort();
                host.Close();
            }
        }

        /// <summary>
        /// Machines post their data to this method exposed as a wcf rest service
        /// </summary>
        /// <param name="data"></param>
        public void PostData(Stream data)
        {
            // We figure out who is talking to us
            var address = ((RemoteEndpointMessageProperty)(OperationContext.Current).IncomingMessageProperties[RemoteEndpointMessageProperty.Name]).Address;

            //Support multiple sims on a single machine. That is why we check for ports
            var httpRequest = OperationContext.Current.IncomingMessageProperties[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
            if (httpRequest != null && httpRequest.Headers.AllKeys.Contains("MachineAddressPort"))
            {
                var clientString = httpRequest.Headers["MachineAddressPort"];
                if (!String.IsNullOrEmpty(clientString))
                {
                    address = HttpUtility.UrlDecode(clientString);
                    if (address.EndsWith(":80"))
                        address = address.Replace(":80", "");
                }

            }

            
            // Parse the payload 
            var reader = new StreamReader(data);
            var dataAsString = reader.ReadToEnd().TrimEnd('\0');

            var firstColonPos = dataAsString.IndexOf(':');
            var nextColonPos = dataAsString.IndexOf(':', firstColonPos + 1);

            var variableName = dataAsString.Substring(firstColonPos + 1, nextColonPos - firstColonPos - 2);
            var value = JsonConvert.DeserializeObject<object>(dataAsString.Substring(nextColonPos + 1, dataAsString.LastIndexOf('}') - 1 - nextColonPos));

            reader.Close();

            // Dispatch the message 
            Trace.WriteLine("REST Service received message from " + address + ": " + variableName + " = " + value);
            logger.Log(LogLevel.Trace, "REST Service received message from " + address + ": " + variableName + " = " + value);
            publisher.Publish(new Message<VariableChangedEventArgs>
            {
                Data = new VariableChangedEventArgs(variableName, value, address)
            });
        }

        public void Dispose()
        {
            CloseHost();
        }

    }
}
