﻿using PackNet.Communication.PLCBase;

namespace PackNet.Communication
{
    public interface IPLCHeartBeatCommunicator : ISimplePLCCommunicator
{
    bool IsConnected { get; }
}
}
