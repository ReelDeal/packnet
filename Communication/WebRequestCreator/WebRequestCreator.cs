using Newtonsoft.Json;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.PLC;
using System;
using System.Globalization;
using System.Net;
using System.Text;

namespace PackNet.Communication.WebRequestCreator
{
    public class WebRequestCreator : IWebRequestCreator
    {
        private readonly string serverAddress;
        private readonly string ipAddress;
        private readonly int port;

        public string IpAddress { get { return ipAddress; } }
        public string ServerAddress { get { return serverAddress; } }
        public int Port { get { return port; } }
        
        public WebRequestCreator(string serverAddress, int port)
        {
            this.serverAddress = serverAddress;
            this.ipAddress = serverAddress;
            this.port = port;

            if (port != 80)
                this.serverAddress = serverAddress + ":" + port;

            ServicePointManager.Expect100Continue = false;// send payload w/ initial post of data to machines.
        }
        
        public WebRequest CreateGetRequest(PacksizePlcVariable variable)
        {
            var url = string.Format("http://{0}/GetVariable?{1}", serverAddress, variable.ToString());
            var request = CreateWebRequest(url);
            return request;
        }

        public WebRequest CreatePostRequest(PacksizePlcVariable variable, object value)
        {
            var url = string.Format("http://{0}/PostVariable", serverAddress);
            var request = CreateWebRequest(url);
            request.Method = "POST";
            request.ContentType = "application/json";

            var jsonObject = CreateJsonObjectToSend(variable, value);
            var byteArray = Encoding.UTF8.GetBytes(jsonObject);

            request.ContentLength = byteArray.Length;
            PopulatePost(byteArray, request);

            return request;
        }

        /// <summary>
        /// Keeps track of the serialized value that was written to the request (for unit testing)
        /// </summary>
        public string LastWrittenValue { get; private set; }

        private string CreateJsonObjectToSend(PacksizePlcVariable variable, object value)
        {
            var name = variable.ToString(); 
            
            var data = string.Format(CultureInfo.InvariantCulture, "{{\"{0}\":{1}}}", name, JsonConvert.SerializeObject(value, SerializationSettings.GetJsonSerializerSettings()));
            return data;
        }

        private WebRequest CreateWebRequest(string url)
        {
            //TODO: Fix BNR variable names so they conform to web standards
            var webRequest = (HttpWebRequest)WebRequest.Create(new Uri(url, true));
            webRequest.KeepAlive = false;
            webRequest.Timeout = Convert.ToInt32(TimeSpan.FromSeconds(10).TotalMilliseconds);
            return webRequest;
        }

        private void PopulatePost(byte[] byteArray, WebRequest request)
        {
            using (var reqStream = request.GetRequestStream())
            {
                reqStream.Write(byteArray, 0, byteArray.Length);
            }
            LastWrittenValue = Encoding.UTF8.GetString(byteArray);
        }
    }
}