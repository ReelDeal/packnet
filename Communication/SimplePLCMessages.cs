using PackNet.Common.Interfaces.Enums;

namespace PackNet.Communication
{
    public class SimplePLCMessages : MessageTypes
    {
        public static readonly SimplePLCMessages SimplePLCConnectionStatus = new SimplePLCMessages("SimplePLCConnectionStatus");
        public static readonly SimplePLCMessages SimplePLCVariableChanged = new SimplePLCMessages("SimplePLCVariableChanged");
        
        protected SimplePLCMessages(string name) : base(name) { }
    }
}