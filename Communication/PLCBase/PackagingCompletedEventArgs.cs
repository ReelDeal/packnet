﻿using System;

namespace PackNet.Communication.PLCBase
{
    public class PackagingCompletedEventArgs
    {
        public PackagingCompletedEventArgs(Guid id, double corrugateUsage, TimeSpan completionTime)
        {
            Id = id;
            CorrugateUsage = corrugateUsage;
            CompletionTime = completionTime;            
        }
        public Guid Id { get; private set; }

        public double CorrugateUsage { get; private set; }

        public TimeSpan CompletionTime { get; private set; }
    }
}