﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Communication.PLCBase
{
    public interface IProviderCommunicatorVariableMap
    {
        PacksizePlcVariable PcLifeSign { get; }

        PacksizePlcVariable PlcLifeSign { get; }

        PacksizePlcVariable ConnectionStatus { get; }

        PacksizePlcVariable EventNotificationRegistrationBuffer { get; }

        PacksizePlcVariable EventNotificationVariablesCounter { get; }

        PacksizePlcVariable EventNotifierAddress { get; }

        PacksizePlcVariable EventNotifierPort { get; }
        
        IEnumerable<PacksizePlcVariable> AsEnumerable();
    }
}
