﻿using System;

namespace PackNet.Communication.PLCBase
{
    public enum PackagingStatus
    {
        Idle = 0,
        Running = 1,
        Completed = 2,
        OutOfFanfold = 3,
        Failure = 4
    }
}