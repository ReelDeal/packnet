﻿using System;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Exceptions;

namespace PackNet.Communication.PLCBase
{
    /// <summary>
    /// Handles connection status and read/write to a PLC
    /// </summary>
    public interface ISimplePLCCommunicator : IDisposable
    {
        /// <summary>
        /// Write a value to the plc
        /// </summary>
        /// <param name="variable"></param>
        /// <param name="value"></param>
        /// <param name="retryCount"></param>
        /// <exception cref="ConnectionException">Can happen if connection is lost or timed out.</exception>
        void WriteValue<T>(PacksizePlcVariable variable, T value, int retryCount = -1);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="variable"></param>
        /// <param name="retryCount"></param>
        /// <returns></returns>
        /// <exception cref="ConnectionException">Can happen if connection is lost or timed out.</exception>
        T ReadValue<T>(PacksizePlcVariable variable, int retryCount = -1);

        bool UpdateWebRequestCreator(IWebRequestCreator newWebRequestCreator);

        string PlcIpAddressWithPort { get; }
    }
}
