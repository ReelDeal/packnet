﻿using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

using MockExternalPrintService.L10N;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;

namespace MockExternalPrintService
{
    [Export(typeof(IService))]
    public class MockExternalPrintService : IService
    {
        private const string settingsSubPath = @"ServicePlugins\MockExternalPrintService\MockExternalPrintSettings.xml";
        private ILogger logger;
        private IAggregateMachineService machineService;
        private Task task;
        private bool disposed;
        private readonly string labelCommand = "^XA^FO50,50^ADN36,30^FD{0}^FS^XZ";
        [ImportingConstructor]
        public MockExternalPrintService(IServiceLocator serviceLocator, ILogger logger)
        {
            this.logger = logger;
            serviceLocator.RegisterAsService(this);
            machineService = serviceLocator.Locate<IAggregateMachineService>();

            StartConnectionHandlerThread();

        }

        private void StartConnectionHandlerThread()
        {
            task = Task.Factory.StartNew(() =>
            {
                Console.WriteLine("MockPrintServer started");
                logger.Log(LogLevel.Error, "MockPrintServer started");
                var serverSocket = new TcpListener(IPAddress.Any, 9090);

                var clientSocket = default(TcpClient);
                serverSocket.Start();
                var msg = Strings.MockExternalPrintService_MockExternalPrintService_____Mock_External_Print_Server_Started_at_port_9090;
                Console.WriteLine(msg);
                logger.Log(LogLevel.Debug, msg);
                while (!disposed)
                {
                    using (clientSocket = serverSocket.AcceptTcpClient())
                    {
                        HandleConnection(clientSocket, labelCommand);
                    }
                }
            });
            task.ContinueWith(t =>
            {
                Console.WriteLine("MockPrintServer died: " + t.Exception.ToString());
                logger.Log(LogLevel.Error, "MockPrintServer died: " + t.Exception.ToString());
                if (!disposed)
                {
                    StartConnectionHandlerThread();
                }
            });
        }

        public void HandleConnection(TcpClient clientSocket, string labelCommand)
        {
            char stx;
            char etx;
            string ack;
            var ackFormat = "";

            var settingsPath = Path.Combine(Directory.GetCurrentDirectory(), settingsSubPath);

            //See if there is a settings file, and that it has a valid ack format.  If not, set the default format and use that.
            try
            {
                if (File.Exists(settingsPath))
                {
                    var xdoc = XDocument.Load(settingsPath);
                    ackFormat = xdoc.Descendants("ackFormat").First().Value;
                }
            }
            catch (Exception)
            {
                logger.Log(LogLevel.Error,
                    string.Format(
                        "Error with opening {0}.  File either does not exist, or ackFormat is not set.  Using default value.",
                        settingsPath));

                ackFormat = "|ACK|";
            }

            var msg = " >> Accept connection from socket";
            Console.WriteLine(msg);
            logger.Log(LogLevel.Debug, msg);

            stx = Convert.ToChar(2);
            etx = Convert.ToChar(3);
            ack = stx + "{0}" + ackFormat + etx;

            using (var networkStream = clientSocket.GetStream())
            {
                while ((clientSocket.Connected))
                {
                    if (!clientSocket.Connected)
                    {
                        continue;
                    }

                    var bytesFrom = new byte[10025];
                    networkStream.Read(bytesFrom, 0, 10025);
                    var dataFromClient = Encoding.UTF8.GetString(bytesFrom);
                    var index = dataFromClient.IndexOf(etx);
                    if (index >= 0)
                    {
                        dataFromClient = dataFromClient.Substring(0, index)
                            .Replace(stx.ToString(), string.Empty);
                    }
                    else
                    {
                        Console.WriteLine("Invalid message (no etx)");
                        logger.Log(LogLevel.Debug, "Invalid message (no etx)");
                        return;
                    }
                    msg = " >> Data from socket - " + dataFromClient + " | " + DateTime.Now.ToLongTimeString();
                    Console.WriteLine(msg);
                    logger.Log(LogLevel.Debug, msg);
                    var sequenceNr = string.Empty;

                    //KPA is the keep alive message
                    if (dataFromClient.Contains("KPA"))
                    {
                        sequenceNr = dataFromClient.Substring(0, dataFromClient.IndexOf("KPA"));
                    }
                    //RLP is the request label print
                    else if (dataFromClient.Contains("RLP"))
                    {
                        sequenceNr = dataFromClient.Substring(0, dataFromClient.IndexOf("RLP"));
                    }
                    //TODO: what is this?
                    else if (dataFromClient.Contains("RCP"))
                    {
                        sequenceNr = dataFromClient.Substring(0, dataFromClient.IndexOf("RCP"));
                    }

                    var parts = dataFromClient.Split(';');
                    var sendBytes = Encoding.ASCII.GetBytes(string.Format(ack, sequenceNr));
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    networkStream.Flush();

                    if (parts[0].EndsWith("RLP"))
                    {
                        msg = "Printer to use in message: " + parts[5];
                        Console.WriteLine(msg);
                        logger.Log(LogLevel.Debug, msg);

                        var machine =
                            machineService.Machines.FirstOrDefault(m => m.Alias == parts[5] && m is INetworkConnectedMachine);
                        if (machine == null)
                        {
                            msg = "Failed to locate printer with alias " + parts[5];
                            Console.WriteLine(msg);
                            logger.Log(LogLevel.Debug, msg);
                        }
                        else
                        {
                            var endPoint = new IPEndPoint(IPAddress.Parse(((INetworkConnectedMachine)machine).IpOrDnsName),
                                ((INetworkConnectedMachine)machine).Port);
                            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                            try
                            {
                                socket.Connect(endPoint);

                                var format = string.Format(labelCommand, parts[1]);
                                socket.Send(Encoding.UTF8.GetBytes(format));

                                msg = "Sent '" + format + "' to printer " + machine.Alias + " at " +
                                      endPoint.Address + ":" + endPoint.Port;
                                Console.WriteLine(msg);
                                logger.Log(LogLevel.Debug, msg);
                            }
                            catch (Exception e)
                            {
                                msg = "Print failed: " + e.Message;
                                Console.WriteLine(msg);
                                logger.Log(LogLevel.Debug, msg);
                            }
                            finally
                            {
                                socket.Close();
                                socket.Dispose();
                            }
                        }
                    }
                    else if (parts[0].EndsWith("RCP"))
                    {
                        msg = String.Format("Report Carton w/ SerialNo:{0} Produced!", parts[1]);
                        Console.WriteLine(msg);
                        logger.Log(LogLevel.Debug, msg);
                    }
                }
            }
        }

        public void Dispose()
        {
            disposed = true;
            task.Dispose();
        }

        public string Name { get { return "MockExternalPrintService"; } }
    }
}
