﻿using System;
using System.IO;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Services.MachineServices;

using Shielded;

namespace WorkflowTests
{
    [TestClass]
    public class WorkflowCustomActivityTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var complete = false;
            var workflowComplete = false;
            var ea = new EventAggregator();
            var slMock = new Mock<IServiceLocator>();
            var loggerMock = new Mock<ILogger>();
            var mg = new MachineGroup
            {
                Alias = "testmg",
                WorkflowPath = Path.Combine(Environment.CurrentDirectory, "ProducibleWaitAndMachineGroupError.xaml")
            };

            var kit = new Kit { CustomerUniqueId = "333" };
            var label = new Label { CustomerUniqueId = "44" };
            kit.AddProducible(label);
            var carton = new Carton { CustomerUniqueId = "55" };
            kit.AddProducible(carton);

            kit.ProducibleStatusObservable.DurableSubscribe(status =>
            {
                Console.WriteLine(status);
                if (status == ProducibleStatuses.ProducibleCompleted)
                    complete = true;

            }, loggerMock.Object);


            var dispatcher = new MachineGroupWorkflowDispatcher(slMock.Object, ea, loggerMock.Object);
            Task.Factory.StartNew(() =>
            {
                dispatcher.DispatchCreateProducibleWorkflow(mg, kit);
                workflowComplete = true;
                Console.WriteLine("workflow complete");
            });

            // start label
            Shield.InTransaction(() => label.ProducibleStatus.Value = InProductionProducibleStatuses.ProducibleProductionStarted);
            Assert.IsFalse(complete);
            Assert.IsFalse(workflowComplete);
            Shield.InTransaction(() => label.ProducibleStatus.Value = ProducibleStatuses.ProducibleCompleted);
            Assert.IsFalse(complete);
            Assert.IsFalse(workflowComplete);

                
            Shield.InTransaction(() => carton.ProducibleStatus.Value = InProductionProducibleStatuses.ProducibleProductionStarted);
            Assert.IsFalse(complete);
            Assert.IsFalse(workflowComplete);
            Shield.InTransaction(() => carton.ProducibleStatus.Value = ProducibleStatuses.ProducibleCompleted);


            Retry.For(() => { return complete; }, TimeSpan.FromMilliseconds(500), TimeSpan.FromMilliseconds(50));
            Retry.For(() => { return workflowComplete; }, TimeSpan.FromMilliseconds(500), TimeSpan.FromMilliseconds(50));

        }
    }
}
