﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeTests
{
    using System.Linq;

    using PackNet.Cube;
    using PackNet.Cube.DTO;

    using Testing.Specificity;

    [TestClass]
    public class BoxListGeneratorTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void IncrementReturnsExpectedResults()
        {
            var incrementedList = BoxListGenerator.Increment(1.0m, 5.0m, 1.0m);
            var list = incrementedList.ToList();
            Specify.That(list.Count).Should.BeEqualTo(5);
            Specify.That(list[0]).Should.BeEqualTo(1.0m);
            Specify.That(list[1]).Should.BeEqualTo(2.0m);
            Specify.That(list[2]).Should.BeEqualTo(3.0m);
            Specify.That(list[3]).Should.BeEqualTo(4.0m);
            Specify.That(list[4]).Should.BeEqualTo(5.0m);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void IncrementFloatReturnsExpectedResults()
        {
            var incrementedList = BoxListGenerator.Increment(1.0f, 5.0f, 1.0f);
            var list = incrementedList.ToList();
            Specify.That(list.Count).Should.BeEqualTo(5);
            Specify.That(list[0]).Should.BeEqualTo(1.0f);
            Specify.That(list[1]).Should.BeEqualTo(2.0f);
            Specify.That(list[2]).Should.BeEqualTo(3.0f);
            Specify.That(list[3]).Should.BeEqualTo(4.0f);
            Specify.That(list[4]).Should.BeEqualTo(5.0f);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void IncrementDoubleReturnsExpectedResults()
        {
            var incrementedList = BoxListGenerator.Increment(1.0d, 5.0d, 1.0d);
            var list = incrementedList.ToList();
            Specify.That(list.Count).Should.BeEqualTo(5);
            Specify.That(list[0]).Should.BeEqualTo(1.0d);
            Specify.That(list[1]).Should.BeEqualTo(2.0d);
            Specify.That(list[2]).Should.BeEqualTo(3.0d);
            Specify.That(list[3]).Should.BeEqualTo(4.0d);
            Specify.That(list[4]).Should.BeEqualTo(5.0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void IncrementDoubleReturnsExpectedResultsWithModifiedIncrement()
        {
            var incrementedList = BoxListGenerator.Increment(1.0d, 2.0d, .1d);
            var list = incrementedList.ToList();
            Specify.That(list.Count).Should.BeEqualTo(11);
            Specify.That(list[0]).Should.BeEqualTo(1.0d);
            Specify.That(list[1]).Should.BeEqualTo(1.1d);
            Specify.That(list[2]).Should.BeEqualTo(1.2d);
            Specify.That(list[3]).Should.BeEqualTo(1.3d);
            Specify.That(list[4]).Should.BeEqualTo(1.4d);
            Specify.That(list[5]).Should.BeEqualTo(1.5d);
            Specify.That(list[6]).Should.BeEqualTo(1.6d);
            Specify.That(list[7]).Should.BeEqualTo(1.7d);
            Specify.That(list[8]).Should.BeEqualTo(1.8d);
            Specify.That(list[9]).Should.BeEqualTo(1.9d);
            Specify.That(list[10]).Should.BeEqualTo(2.0d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void IncrementFloatReturnsExpectedResultsWithModifiedIncrement()
        {
            var incrementedList = BoxListGenerator.Increment(1.0f, 2.0f, .1f);
            var list = incrementedList.ToList();
            Specify.That(list.Count).Should.BeEqualTo(11);
            Specify.That(list[0]).Should.BeEqualTo(1.0f);
            Specify.That(list[1]).Should.BeEqualTo(1.1f);
            Specify.That(list[2]).Should.BeEqualTo(1.2f);
            Specify.That(list[3]).Should.BeEqualTo(1.3f);
            Specify.That(list[4]).Should.BeEqualTo(1.4f);
            Specify.That(list[5]).Should.BeEqualTo(1.5f);
            Specify.That(list[6]).Should.BeEqualTo(1.6f);
            Specify.That(list[7]).Should.BeEqualTo(1.7f);
            Specify.That(list[8]).Should.BeEqualTo(1.8f);
            Specify.That(list[9]).Should.BeEqualTo(1.9f);
            Specify.That(list[10]).Should.BeEqualTo(2.0f);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void IncrementReturnsExpectedResultsWithModifiedIncrement()
        {
            var incrementedList = BoxListGenerator.Increment(1.0m, 2.0m, .1m);
            var list = incrementedList.ToList();
            Specify.That(list.Count).Should.BeEqualTo(11);
            Specify.That(list[0]).Should.BeEqualTo(1.0m);
            Specify.That(list[1]).Should.BeEqualTo(1.1m);
            Specify.That(list[2]).Should.BeEqualTo(1.2m);
            Specify.That(list[3]).Should.BeEqualTo(1.3m);
            Specify.That(list[4]).Should.BeEqualTo(1.4m);
            Specify.That(list[5]).Should.BeEqualTo(1.5m);
            Specify.That(list[6]).Should.BeEqualTo(1.6m);
            Specify.That(list[7]).Should.BeEqualTo(1.7m);
            Specify.That(list[8]).Should.BeEqualTo(1.8m);
            Specify.That(list[9]).Should.BeEqualTo(1.9m);
            Specify.That(list[10]).Should.BeEqualTo(2.0m);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void BoxListCanBeFilteredWithPredicate()
        {
            //Run a valid test that creates a box file
            var min = new BoxDefinition {
                Length = 4.0f,
                Width = 4.0f,
                Height = 6.0f,
            };
            var max = new BoxDefinition {
                Length = 5.0f,
                Width = 5.0f,
                Height = 7.0f,
            };

            var result = BoxListGenerator.Generate(min, max, 1.0f);
            Assert.AreEqual(8, result.Count);

            //Apply a predicate... make sure no results
            result = BoxListGenerator.Generate(min, max, 1.0f, (l, w, h) => false);
            Assert.AreEqual(0, result.Count);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void BoxListCanBeFilteredWithPredicate2()
        {
            //Arrange
            //Run a valid test that creates a box file
            var min = new BoxDefinition {
                Length = 4.0f,
                Width = 4.0f,
                Height = 6.0f,
            };
            var max = new BoxDefinition {
                Length = 5.0f,
                Width = 5.0f,
                Height = 7.0f,
            };

            var result = BoxListGenerator.Generate(min, max, 1.0f);
            Assert.AreEqual(8, result.Count);

            //Act
            result = BoxListGenerator.Generate(min, max, 1.0f,
                (l, w, h) =>
                {
                   if (l == 5f && w == 5f && h == 6f) 
                        return false;
                   return true;
                }
            );

            //Assert
            Specify.That(result.Count).Should.BeEqualTo(7);
            Specify.That(result.Count(b => b.StringDimension == "5 X 5 X 6")).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void BoxListCanBeFilteredWithMaxRscWidth()
        {
            //Arrange
            //Run a valid test that creates a box file
            var min = new BoxDefinition {
                Length = 4.0f,
                Width = 4.0f,
                Height = 6.0f,
            };
            var max = new BoxDefinition {
                Length = 5.0f,
                Width = 5.0f,
                Height = 7.0f,
            };

            var result = BoxListGenerator.Generate(min, max, 1.0f);
            Assert.AreEqual(8, result.Count);

            //Act
            result = BoxListGenerator.GenerateWithRscMaxCorrugateFilter(min, max, 1.0f, 11f);

            //Assert
            Specify.That(result.Count).Should.BeEqualTo(2);
            Specify.That(result[0].StringDimension).Should.BeEqualTo("4 X 4 X 6");
            Specify.That(result[1].StringDimension).Should.BeEqualTo("5 X 4 X 6");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void BoxListIsGeneratedInCorrectOrder()
        {
            var min = new BoxDefinition {
                          Length = 4.0f,
                          Width = 4.0f,
                          Height = 6.0f,
                      };
            var max = new BoxDefinition {
                          Length = 5.0f,
                          Width = 5.0f,
                          Height = 7.0f,
                      };

            var result = BoxListGenerator.Generate(min, max, 1.0f);
            //Expected
            //4.0, 4.0, 6.0
            //4.0, 4.0, 7.0
            //4.0, 5.0, 6.0
            //4.0, 5.0, 7.0
            //5.0, 4.0, 6.0
            //5.0, 4.0, 7.0
            //5.0, 5.0, 6.0
            //5.0, 5.0, 7.0
            Assert.AreEqual(8, result.Count);
            Assert.AreEqual(4.0, result[0].Length);
            Assert.AreEqual(4.0, result[0].Width);
            Assert.AreEqual(6.0, result[0].Height);

            Assert.AreEqual(4.0, result[1].Length);
            Assert.AreEqual(4.0, result[1].Width);
            Assert.AreEqual(7.0, result[1].Height);

            Assert.AreEqual(4.0, result[2].Length);
            Assert.AreEqual(5.0, result[2].Width);
            Assert.AreEqual(6.0, result[2].Height);

            Assert.AreEqual(4.0, result[3].Length);
            Assert.AreEqual(5.0, result[3].Width);
            Assert.AreEqual(7.0, result[3].Height);

            Assert.AreEqual(5.0, result[4].Length);
            Assert.AreEqual(4.0, result[4].Width);
            Assert.AreEqual(6.0, result[4].Height);

            Assert.AreEqual(5.0, result[5].Length);
            Assert.AreEqual(4.0, result[5].Width);
            Assert.AreEqual(7.0, result[5].Height);

            Assert.AreEqual(5.0, result[6].Length);
            Assert.AreEqual(5.0, result[6].Width);
            Assert.AreEqual(6.0, result[6].Height);

            Assert.AreEqual(5.0, result[7].Length);
            Assert.AreEqual(5.0, result[7].Width);
            Assert.AreEqual(7.0, result[7].Height);
        }
    }
}
