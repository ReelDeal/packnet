﻿using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.ConveyorRelated
{
    public interface IConveyorRepository : IAccessorieRepository<Conveyor>
    {
    }
}
