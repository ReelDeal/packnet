﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.ConveyorRelated
{
    public class ConveyorRepository : MongoDbRepository<Conveyor>, IConveyorRepository
    { 
        private readonly object syncObject = new object();
        private readonly bool serializerRegistered;

        public ConveyorRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "Conveyors")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;
        
                MongoDbHelpers.TryRegisterSerializer<ConveyorType>(new EnumerationSerializer());

                serializerRegistered = true;
            }
        }

        public IEnumerable<Conveyor> WithAlias(string alias)
        {
            return Collection.FindAll().Where(c => c.Alias == alias);
        }
    }
}
