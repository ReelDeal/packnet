﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.ConveyorRelated
{
    public interface IConveyorService : IService
    {
        IEnumerable<IConveyor> GetConveyorsForMachine(Guid machineId);

        IEnumerable<IConveyor> GetConveyorsForMachineGroup(Guid machineGroupId);

        void DeleteConveyorsForMachine(Guid id);

        void FeedConveyor(Conveyor conveyor, TimeSpan time);

        void FeedConveyorToPickZone(Conveyor conveyor, TimeSpan time, PickZone pickZone);
    }
}
