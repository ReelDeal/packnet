﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Enums;
using PackNet.IODeviceService.Exceptions;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.ConveyorRelated
{
    public class ConveyorService : IConveyorService
    {
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IMachineGroupService machineGroupService;
        private readonly IAggregateMachineService machineService;
        private readonly ILogger logger;

        private readonly IConveyors conveyors;

        public string Name { get { return "Conveyor Service"; } }
        
        public ConveyorService(IServiceLocator serviceLocator, IConveyorRepository repo, IIoDeviceCommunicatorFactory communicatorFactory)
        {
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            logger = serviceLocator.Locate<ILogger>();
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            machineGroupService = serviceLocator.Locate<IMachineGroupService>();
            machineService = serviceLocator.Locate<IAggregateMachineService>();

            conveyors = new Conveyors(repo, communicatorFactory);
            machineService.Machines.Where(m => m is IMachineWithCapabilities).ForEach(mc => UpdateCapabilitiesOfMachine(mc.Id));

            RegisterUiCommunication();

            serviceLocator.RegisterAsService(this);
        }

        public IEnumerable<IConveyor> GetConveyorsForMachine(Guid machineId)
        {
            return conveyors.Current.Where(c => c.MachineId == machineId);
        }
        public IEnumerable<IConveyor> GetConveyorsForMachineGroup(Guid machineGroupId)
        {
            var machinesInMg = machineGroupService.GetMachinesInGroup(machineGroupId);

            return conveyors.Current.Where(conveyor => machinesInMg.Any(machine => machine.Id == conveyor.MachineId));
        }

        public void DeleteConveyorsForMachine(Guid id)
        {
            conveyors.DeleteForMachine(id);
        }

        public void FeedConveyor(Conveyor conveyor, TimeSpan time)
        {
            conveyors.FeedConveyor(conveyor, time);
        }

        public void FeedConveyorToPickZone(Conveyor conveyor, TimeSpan time, PickZone pickZone)
        {
            conveyors.FeedConveyorToPickZone(conveyor, time, pickZone);
        }

        private void RegisterUiCommunication()
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Conveyor>(AccessoryMessages.CreateConveyorAccessory);
            AccessoryMessages.CreateConveyorAccessory.OnMessage<Conveyor>(subscriber, logger, msg =>
            {
                var result = ResultTypes.Success;

                Exception thrownException = null;
                Conveyor createdConveyor = null;

                try
                {
                    createdConveyor = conveyors.Add(msg.Data);
                    UpdateCapabilitiesOfMachine(msg.Data.MachineId);
                }
                catch (AlreadyPersistedException e)
                {
                    result = ResultTypes.Exists;
                    thrownException = e;
                }
                catch (ItemExistsException e)
                {
                    result = ResultTypes.Exists;
                    thrownException = e;
                }
                catch (Exception e)
                {
                    result = ResultTypes.Fail;
                    thrownException = e;
                }
                finally
                {
                    if (thrownException != null)
                    {
                        logger.Log(LogLevel.Error, string.Format("Unable to create Conveyor with exception: {0}", thrownException));
                    }
                }

                uiCommunicationService.SendMessageToUI(new ResponseMessage<Conveyor>
                {
                    MessageType = AccessoryMessages.IOAccessoryCreated,
                    Result = result,
                    ReplyTo = msg.ReplyTo,
                    Data = createdConveyor,
                    Message = thrownException == null ? string.Empty : thrownException.Message
                });
            });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Conveyor>(AccessoryMessages.UpdateConveyorAccessory);
            AccessoryMessages.UpdateConveyorAccessory.OnMessage<Conveyor>(subscriber, logger, msg =>
            {
                var result = ResultTypes.Success;

                Exception thrownException = null;

                Conveyor updatedConveyor = null;
                try
                {
                    updatedConveyor = conveyors.Update(msg.Data);
                    UpdateCapabilitiesOfMachine(msg.Data.MachineId);
                }
                catch (Exception e)
                {
                    result = ResultTypes.Fail;
                    thrownException = e;
                }
                finally
                {
                    if (thrownException != null)
                    {
                        logger.Log(LogLevel.Error, string.Format("Unable to create Conveyor with exception: {0}", thrownException));
                    }
                }

                uiCommunicationService.SendMessageToUI(new ResponseMessage<Conveyor>
                {
                    MessageType = AccessoryMessages.IOAccessoryUpdated,
                    Result = result,
                    ReplyTo = msg.ReplyTo,
                    Data = updatedConveyor,
                    Message = thrownException == null ? string.Empty : thrownException.Message
                });
            });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Conveyor>(AccessoryMessages.DeleteConveyorAccessory);
            AccessoryMessages.DeleteConveyorAccessory.OnMessage<Conveyor>(subscriber, logger, msg =>
            {
                var result = ResultTypes.Success;

                Exception thrownException = null;
                
                try
                {
                    conveyors.Delete(msg.Data);
                    UpdateCapabilitiesOfMachine(msg.Data.MachineId);
                }
                catch (Exception e)
                {
                    result = ResultTypes.Fail;
                    thrownException = e;
                }
                finally
                {
                    if (thrownException != null)
                    {
                        logger.Log(LogLevel.Error, string.Format("Unable to create Conveyor with exception: {0}", thrownException));
                    }
                }

                uiCommunicationService.SendMessageToUI(new ResponseMessage<Conveyor>
                {
                    MessageType = AccessoryMessages.IOAccessoryDeleted,
                    Result = result,
                    ReplyTo = msg.ReplyTo,
                    Data = msg.Data,
                    Message = thrownException == null ? string.Empty : thrownException.Message
                });

                publisher.Publish(new Message<Guid>
                {
                    ReplyTo = msg.ReplyTo,
                    MessageType = AccessoryMessages.GetIoAccessoriesForMachine,
                    Data = msg.Data.MachineId
                });
            });
        }

        private void UpdateCapabilitiesOfMachine(Guid machineId)
        {
            var convs = GetConveyorsForMachine(machineId);
            var correspondingMachine = machineService.FindById(machineId) as IMachineWithCapabilities;

            if (correspondingMachine == null)
                return;

            correspondingMachine.RemoveCapabilitiesOfType(typeof(ConveyorMoveCapability));
            correspondingMachine.RemoveCapabilitiesOfType(typeof(ConveyorPickzoneCapability));

            correspondingMachine.AddOrUpdateCapabilities(convs.SelectMany(c => c.MoveCapabilities));
        }

        public void Dispose()
        {
        }
    }
}