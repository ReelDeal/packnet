﻿using System;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.ConveyorRelated
{
    public interface IConveyors : IAccessories<Conveyor, IConveyor>
    {
        Conveyor Update(Conveyor newData);

        void FeedConveyor(Conveyor conveyor, TimeSpan time);

        void FeedConveyorToPickZone(Conveyor conveyor, TimeSpan time, PickZone pickZone);
    }
}
