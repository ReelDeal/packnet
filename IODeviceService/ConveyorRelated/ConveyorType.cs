﻿using System;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.IODeviceService.ConveyorRelated
{
    public class ConveyorType : Enumeration
    {
        public static ConveyorType CrossConveyor = new ConveyorType("CrossConveyor");
        public static ConveyorType WasteConveyor = new ConveyorType("WasteConveyor");

        //This touch method is needed for the constructors to be run since the mef-composer are picking up the service plugins after the priming of the enumerations is done.
        //If this isn't used the serialization of these enumerations won't work
        public static void Touch()
        {
            //Disables the warning for this so the build server doesn't fail due to flagging this as an error
#pragma warning disable 1717
            CrossConveyor = CrossConveyor;
            WasteConveyor = WasteConveyor;
#pragma warning restore 1717
        }

        public ConveyorType(string displayName) : base(0, displayName)
        {
        }
    }
}
