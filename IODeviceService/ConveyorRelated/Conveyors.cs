﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Exceptions;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.ConveyorRelated
{
    public class Conveyors : Accessories<Conveyor, IConveyor>, IConveyors
    {
        public Conveyors(IConveyorRepository repository, IIoDeviceCommunicatorFactory ioDeviceCommunicatorFactory) : base(repository, ioDeviceCommunicatorFactory)
        {
        }

        public void FeedConveyor(Conveyor conveyor, TimeSpan time)
        {
            if (conveyor.CurrentStatus != AccessoryStatuses.Idle)
                throw new AccessoryNotIdleException(
                    string.Format("Unable to feed Conveyor to the right, the conveyor is not Idle. Current Status: {0}",
                        conveyor.CurrentStatus));

            if (conveyor.MoveCapabilities.FirstOrDefault() == null)
                throw new MissingFeedingCapabilityException("Unable to feed conveyor right due to missing capability", conveyor);

            var communicator = ioDeviceCommunicatorFactory.GetCommunicatorForMachine(conveyor.MachineId);
            communicator.EnableOutputSignalTemporarily((conveyor.MoveCapabilities.First() as ConveyorMoveCapability).Value, time);
            SetupFeedingCallback(conveyor, time);
        }

        public void FeedConveyorToPickZone(Conveyor crossConveyor, TimeSpan time, PickZone pickZone)
        {
            var capability =
                crossConveyor.MoveCapabilities.OfType<ConveyorPickzoneCapability>()
                    .FirstOrDefault(cap => cap.PickZone.Id.Equals(pickZone.Id));
          
            if(capability == null)
                throw new Exception("Cannot feed this conveyor to the selected pickzone");

            var communicator = ioDeviceCommunicatorFactory.GetCommunicatorForMachine(crossConveyor.MachineId);
            communicator.EnableOutputSignalTemporarily(capability.Value, time);
            SetupFeedingCallback(crossConveyor, time);
        }

        public Conveyor Update(Conveyor newData)
        {
            if (Repository.Find(newData.Id) == null)
                throw new ItemDoesNotExistException("NotExists");

            Conveyor local;
            accessories.TryGetValue(newData.Id, out local);

            if (local == null)
                throw new ItemDoesNotExistException("NotExists");

            if (ValidatePortAndModule(newData) == false)
                throw new AccessoryModuleAndPortInUseException("AccessoryModuleAndPortInUse");

            local.UpdateFrom(newData);

            return Repository.Update(local);
        }

        private static void SetupFeedingCallback(IConveyor conveyor, TimeSpan time)
        {
            conveyor.CurrentStatus = AccessoryStatuses.Feeding;
            var task = Task.Factory.StartNew(() =>
            {
                Thread.Sleep(time);
                conveyor.CurrentStatus = AccessoryStatuses.FeedingDone;
                conveyor.CurrentStatus = AccessoryStatuses.Idle;
            });
            task.ContinueWith((t =>
            {
                //Don't care, just don't allow any exceptions to escape the thread
            }));
        }
    }
}
