﻿using Newtonsoft.Json;

using PackNet.IODeviceService.Intefaces;

namespace PackNet.IODeviceService.DTO
{
    public class OutputCommand
    {
        public OutputCommand(OutputTypes type, int index, int module, int t1InMs, int t2InMs)
        {
            Type = type;
            ModuleNumber = module;
            OutputIndex = index;
            T1 = t1InMs;
            T2 = t2InMs;
        }

        [JsonProperty(Order = 1)]
        public OutputTypes Type { get; set; }

        [JsonProperty(Order = 2)]
        public int ModuleNumber { get; set; }

        [JsonProperty(Order = 3)]
        public int OutputIndex { get; set; }

        [JsonProperty(Order = 4)]
        public int T1 { get; set; }

        [JsonProperty(Order = 5)]
        public int T2 { get; set; }
    }
}
