﻿using Newtonsoft.Json;

using PackNet.IODeviceService.Intefaces;

namespace PackNet.IODeviceService.DTO
{
    public class OutputCommandTrigger
    {
        public OutputCommandTrigger(OutputTypes type, int index, int module, int t1InMs, int t2InMs)
        {
            Command = new OutputCommand(type, index, module, t1InMs, t2InMs);
            Execute = false;
        }

        [JsonProperty(Order = 1)]
        public OutputCommand Command { get; set; }

        [JsonProperty(Order = 2)]
        public bool Execute { get; set; }
    }
}
