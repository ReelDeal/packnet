﻿using System;

using Newtonsoft.Json;

using PackNet.IODeviceService.Intefaces;

namespace PackNet.IODeviceService.DTO
{
    public class InputEvent
    {
        public Guid Sender { get; private set; }

        [JsonProperty(PropertyName = "InputIndex")]
        public int Port { get; private set; }

        public int Module { get; private set; }

        public InputEventTypes Type { get; private set; }

        public InputEvent(Guid sender, int module, int port, InputEventTypes type)
        {
            Sender = sender;
            Port = port;
            Type = type;
            Module = module;
        }
    }
}
