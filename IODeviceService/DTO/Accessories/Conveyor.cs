﻿using System.Collections.Generic;
using System.Linq;

using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.Machines;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.Enums;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.DTO.Accessories
{
    public class Conveyor : Accessory, IConveyor
    {
        private List<ICapability> moveCapabilities; 

        [BsonIgnore]
        public AccessoryTypes AccessoryType { get { return AccessoryTypes.Conveyor; } }

        public ConveyorType ConveyorType { get; set; }

        public List<ICapability> MoveCapabilities
        {
            get { return moveCapabilities; }
            set
            {
                moveCapabilities = value;
                UsedPorts = moveCapabilities.Select(mc => (mc as ConveyorMoveCapability).Value).ToList();
            }
        }

        public void UpdateFrom(Conveyor newData)
        {
            MachineId = newData.MachineId;
            Alias = newData.Alias;
            Description = newData.Description;
            ConveyorType = newData.ConveyorType;
            MoveCapabilities = newData.MoveCapabilities;
        }
    }
}
