﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Repositories;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.DTO.Accessories
{
    public interface IAccessory : IPersistable
    {
        Guid MachineId { get; set; }

        string Alias { get; }

        string Description { get; }

        IEnumerable<AccessoryPort> UsedPorts { get; }

        IObservable<AccessoryStatuses> AccessoryStatusObservable { get; }

        AccessoryStatuses CurrentStatus { get; set; }
    }
}
