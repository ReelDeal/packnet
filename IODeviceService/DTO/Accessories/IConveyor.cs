﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Machines;
using PackNet.IODeviceService.ConveyorRelated;

namespace PackNet.IODeviceService.DTO.Accessories
{
    public interface IConveyor : IAccessory
    {
        ConveyorType ConveyorType { get; set; }

        List<ICapability> MoveCapabilities { get; set; }

        void UpdateFrom(Conveyor newData);
    }
}
