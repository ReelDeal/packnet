﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.DTO.Accessories
{
    public abstract class Accessory : IAccessory
    {
        private readonly Subject<AccessoryStatuses> currentStatusSubject = new Subject<AccessoryStatuses>();
        protected readonly ConcurrentList<ICapability> capabilities = new ConcurrentList<ICapability>();

        private AccessoryStatuses currentStatus;
        
        public Guid Id { get; set; }

        public string Alias { get; set; }

        public string Description { get; set; }

        public Guid MachineId { get; set; }

        [JsonIgnore]
        [BsonIgnore]
        public AccessoryStatuses CurrentStatus
        {
            get { return currentStatus; }
            set
            {
                if (currentStatus != value)
                {
                    currentStatus = value;
                    currentStatusSubject.OnNext(currentStatus);
                }
            }
        }

        [JsonIgnore]
        [BsonIgnore]
        public IObservable<AccessoryStatuses> AccessoryStatusObservable
        {
            get { return currentStatusSubject.AsObservable(); }
        }
        
        [JsonIgnore]
        [BsonIgnore]
        public IEnumerable<AccessoryPort> UsedPorts { get; set; }
    }
}
