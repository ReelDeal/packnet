﻿using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.DTO.Accessories
{
    public interface IFootPedal : IAccessory
    {
        RestrictionsAndCapabilities.Capabilities.AccessoryPort FootPedalTrigger { get; }

        RestrictionsAndCapabilities.Capabilities.AccessoryPort MachineGroupPausePlay { get; }

        void UpdateFrom(FootPedal newData);
    }
}
