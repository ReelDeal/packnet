﻿using System.Collections.Generic;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.IODeviceService.Enums;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.DTO.Accessories
{
    public class FootPedal : Accessory, IFootPedal
    {
        private AccessoryPort footpedalTrigger;
        private AccessoryPort machineGroupPausePlay;

        public void UpdateFrom(FootPedal newData)
        {
            MachineId = newData.MachineId;
            Alias = newData.Alias;
            Description = newData.Description;

            FootPedalTrigger = newData.FootPedalTrigger;
            MachineGroupPausePlay = newData.MachineGroupPausePlay;
        }

        public AccessoryPort FootPedalTrigger
        {
            get { return footpedalTrigger; }
            set
            {
                if (footpedalTrigger == value)
                    return;

                footpedalTrigger = value;
                UsedPorts = new List<AccessoryPort> { footpedalTrigger };
            }
        }

        public AccessoryPort MachineGroupPausePlay
        {
            get { return machineGroupPausePlay; }
            set
            {
                if (machineGroupPausePlay == value)
                    return;

                machineGroupPausePlay = value;
                UsedPorts = new List<AccessoryPort> { machineGroupPausePlay };
            }
        }

        [BsonIgnore]
        public AccessoryTypes AccessoryType { get { return AccessoryTypes.FootPedal; } }
    }
}
