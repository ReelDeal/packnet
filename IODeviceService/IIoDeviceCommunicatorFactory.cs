﻿using System;

using PackNet.IODeviceService.Intefaces;

namespace PackNet.IODeviceService
{
    public interface IIoDeviceCommunicatorFactory
    {
        IIoDeviceCommunicator GetCommunicatorForMachine(Guid machineId);
    }
}
