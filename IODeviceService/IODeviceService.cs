﻿using System.ComponentModel.Composition;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Services;
using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.Intefaces;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions;

namespace PackNet.IODeviceService
{
    [Export(typeof(IService))]
    public class IODeviceService : IIoDeviceService
    {
        private IAggregateIoDeviceService aggregateIoDeviceService;
        private IInputMessageHandler inputMessageHandler;

        public string Name
        {
            get { return "IODeviceService"; }
        }

        [ImportingConstructor]
        public IODeviceService(IServiceLocator serviceLocator)
        {
            TouchTypes();

            inputMessageHandler = new InputMessageHandler(serviceLocator);
            aggregateIoDeviceService = new AggregateIoDeviceService(serviceLocator, inputMessageHandler);
            
            RegisterResolvers(serviceLocator.Locate<IRestrictionResolverService>());
            serviceLocator.RegisterAsService(this);
        }

        private static void TouchTypes()
        {
            ConveyorType.Touch();
        }

        private static void RegisterResolvers(IRestrictionResolverService restrictionResolverService)
        {
            restrictionResolverService.AddResolver(new ConveyorPickzoneRestrictionResolver());

            MongoDbHelpers.TryRegisterDiscriminatorConvention<ConveyorPickzoneRestriction>(
                   new RestrictionDiscriminatorConvention());

            MongoDbHelpers.TryRegisterDiscriminatorConvention<ConveyorPickzoneCapability>(
                new CapabilityDiscriminatorConvention());

            MongoDbHelpers.TryRegisterDiscriminatorConvention<ConveyorMoveCapability>(
                new CapabilityDiscriminatorConvention());
        }

        public void Dispose()
        {
            
        }
    }
}
