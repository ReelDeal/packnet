﻿using System;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Enums;
using PackNet.IODeviceService.FootPedalRelated;

namespace PackNet.IODeviceService
{
    public class AggregateIoDeviceService : IAggregateIoDeviceService
    {
        private readonly IConveyorService conveyorService;
        private readonly IFootPedalService footPedalService;

        private readonly IUICommunicationService uiCommunicationService;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ILogger logger;

        public AggregateIoDeviceService(IServiceLocator serviceLocator, IInputMessageHandler inputMessageHandler)
        {
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();

            var communicatorFactory = new IoDeviceCommunicatorFactory(serviceLocator);
            conveyorService = new ConveyorService(serviceLocator, new ConveyorRepository(), communicatorFactory);
            footPedalService = new FootPedalService(serviceLocator, new FootPedalRepository(), inputMessageHandler, communicatorFactory);

            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            logger = serviceLocator.Locate<ILogger>();

            serviceLocator.RegisterAsService(this);

            RegisterUiCommunication();
        }

        private void RegisterUiCommunication()
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(AccessoryMessages.GetIoAccessoriesForMachine);
            AccessoryMessages.GetIoAccessoriesForMachine.OnMessage<Guid>(subscriber, logger, m =>
            {
                var accessories = new List<IAccessory>();

                conveyorService.GetConveyorsForMachine(m.Data).ForEach(accessories.Add);
                footPedalService.GetFootPedalsForMachine(m.Data).ForEach(accessories.Add);

                uiCommunicationService.SendMessageToUI(new Message<List<IAccessory>>
                {
                    MessageType = AccessoryMessages.MachineIoAccessories,
                    ReplyTo = m.ReplyTo,
                    Data = accessories
                });
            });

            subscriber.GetEvent<ResponseMessage<EmMachine>>()
                .Where(m => m.MessageType == MachineMessages.MachineDeleted && m.Result == ResultTypes.Success)
                .DurableSubscribe(m => DeleteAccessoriesForMachine(m.Data.Id), logger);

            subscriber.GetEvent<ResponseMessage<FusionMachine>>()
                .Where(m => m.MessageType == MachineMessages.MachineDeleted && m.Result == ResultTypes.Success)
                .DurableSubscribe(m => DeleteAccessoriesForMachine(m.Data.Id), logger);
        }

        private void DeleteAccessoriesForMachine(Guid id)
        {
            conveyorService.DeleteConveyorsForMachine(id);
            footPedalService.DeleteFootPedalsForMachine(id);
        }

        public void Dispose()
        {
        }

        public string Name { get { return "Aggregate Io Device Service"; } }
    }
}
