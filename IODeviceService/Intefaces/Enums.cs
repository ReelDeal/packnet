﻿namespace PackNet.IODeviceService.Intefaces
{
    public enum OutputTypes
    {
        Enable = 0,
        Disable = 1,
        TemporaryEnable = 2,
        TemporaryDisable = 3,
        OscillatingSignal = 4
    }

    public enum InputTypes
    {
        SinglePositiveEdge = 0,
        MultiplePositiveEdge = 1,
        SingleNegativeEdge = 2,
        MultipleNegativeEdge = 3,
        RemoveMultipleEdge = 4
    }

    public enum InputEventTypes
    {
        Unknown = -1,
        PositiveEdge = 0,
        NegativeEdge = 1
    }
}
