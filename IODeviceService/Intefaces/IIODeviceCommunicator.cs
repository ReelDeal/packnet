﻿using System;

using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.Intefaces
{
    public interface IIoDeviceCommunicator
    {
        /// <summary>
        /// Enables an output signal on the specified accessory, the signal will remain on until disabled
        /// </summary>
        /// <param name="accessory"></param>
        /// <returns></returns>
        bool EnableOutputSignal(AccessoryPort capability);

        /// <summary>
        /// Disables an output signal on the specified accessory
        /// </summary>
        /// <param name="accessory"></param>
        /// <returns></returns>
        bool DisableOutputSignal(AccessoryPort capability);

        /// <summary>
        /// Enables an output signal on the specified accessory for the specified length of time
        /// </summary>
        /// <param name="capability"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        bool EnableOutputSignalTemporarily(AccessoryPort capability, TimeSpan time);

        /// <summary>
        /// Disables an output signal on the specified accessory for the specified length of time
        /// </summary>
        /// <param name="accessory"></param>
        /// <param name="time"></param>
        /// <returns></returns>
        bool DisableOutputSignalTemporarily(AccessoryPort capability, TimeSpan time);

        /// <summary>
        /// Oscillate an output signal on a specified Accessory
        /// </summary>
        /// <param name="accessory"></param>
        /// <param name="highTime"></param>
        /// <param name="lowTime"></param>
        /// <returns></returns>
        bool OscillateOutputSignal(AccessoryPort capability, TimeSpan highTime, TimeSpan lowTime);

        bool AddSinglePositiveEdgeListener(AccessoryPort capability);

        bool AddSingleNegativeEdgeListener(AccessoryPort capability);

        bool AddMultiplePositiveEdgeListener(AccessoryPort capability);

        bool AddMultipleNegativeEdgeListener(AccessoryPort capability);

        bool RemoveInputListener(AccessoryPort capability);

        string GetServerAddress();

        string IpAddress { get; }

        int Port { get; }

        bool IsConnected { get; }

        IObservable<bool> ConnectionStatusChangedObservable { get; }
    }
}
