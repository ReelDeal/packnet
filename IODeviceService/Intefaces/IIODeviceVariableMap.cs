﻿using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.IODeviceService.Intefaces
{
    public interface IIoDeviceVariableMap
    {
        PacksizePlcVariable OutputCommand { get; }
       
        PacksizePlcVariable OutputCommandExecute { get; }

        PacksizePlcVariable InputCommandType { get; }

        PacksizePlcVariable InputCommandInputIndex { get; }

        PacksizePlcVariable InputCommandModuleIndex { get; }
        
        PacksizePlcVariable InputCommandExecute { get; }

        PacksizePlcVariable InputEventMessageContainer { get; }

        PacksizePlcVariable IoDeviceCallbackIp { get; }

        PacksizePlcVariable IoDeviceCallbackPort { get; }
    }
}
