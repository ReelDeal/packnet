﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.IODeviceService.Base
{
    public interface IAccessorieRepository<T> : IRepository<T> where T : IPersistable
    {
        IEnumerable<T> WithAlias(string @alias);
    }
}
