﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Exceptions;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Exceptions;

namespace PackNet.IODeviceService.Base
{
    public abstract class Accessories<T, I> : IAccessories<T, I> where T : class, IAccessory, I
    {
        protected readonly IAccessorieRepository<T> Repository;
        protected readonly ConcurrentDictionary<Guid, T> accessories = new ConcurrentDictionary<Guid, T>();
        protected readonly IIoDeviceCommunicatorFactory ioDeviceCommunicatorFactory;

        protected Accessories(IAccessorieRepository<T> repository, IIoDeviceCommunicatorFactory ioDeviceCommunicatorFactory )
        {
            Repository = repository;
            this.ioDeviceCommunicatorFactory = ioDeviceCommunicatorFactory;

            LoadAccessories(Repository.All());
        }

        public IEnumerable<I> Current
        {
            get { return accessories.Values; }
        }

        private void LoadAccessories(IEnumerable<T> accessoriesToLoad)
        {
            foreach (var accessory in accessoriesToLoad)
            {
                accessories.TryAdd(accessory.Id, accessory);
            }
        }

        public virtual T Add(T accessorieToAdd)
        {
            if (Repository.Find(accessorieToAdd.Id) != null)
                throw new AlreadyPersistedException("Id");

            if (Repository.WithAlias(accessorieToAdd.Alias).Any(c => c.MachineId.Equals(accessorieToAdd.MachineId)))
                throw new ItemExistsException("Alias");

            if(ValidatePortAndModule(accessorieToAdd) == false)
                throw new AccessoryModuleAndPortInUseException("AccessoryModuleAndPortInUse");

            Repository.Create(accessorieToAdd);

            accessories.TryAdd(accessorieToAdd.Id, accessorieToAdd);

            return accessorieToAdd;
        }

        protected bool ValidatePortAndModule(T accessorieToAdd)
        {
            var localCap =
                accessories.Values.Where(a => a.Id != accessorieToAdd.Id && a.MachineId.Equals(accessorieToAdd.MachineId))
                    .SelectMany(a => a.UsedPorts);

            return accessorieToAdd.UsedPorts.Any(localCap.Contains) == false;
        }

        public virtual void Delete(T accessoryToDelete)
        {
            if (Repository.Find(accessoryToDelete.Id) == null)
                throw new ItemDoesNotExistException("NotExists");

            T conv;
            accessories.TryRemove(accessoryToDelete.Id, out conv);
            
            Repository.Delete(accessoryToDelete.Id);
        }

        public virtual void DeleteForMachine(Guid machineId)
        {
            var accessoriesRemove = Repository.All().Where(c => c.MachineId == machineId).ToList();

            Repository.Delete(accessoriesRemove);
            accessoriesRemove.ForEach(c =>
            {
                T conv;
                accessories.TryRemove(c.Id, out conv);
            });
        }
    }
}
