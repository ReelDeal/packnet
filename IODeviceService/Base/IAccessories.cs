﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.IODeviceService.Base
{
    public interface IAccessories<T, I> where T : class, IPersistable, I
    {
        T Add(T accessorieToAdd);

        void Delete(T footPedalToDelete);

        void DeleteForMachine(Guid machineId);

        IEnumerable<I> Current { get; }
    }
}
