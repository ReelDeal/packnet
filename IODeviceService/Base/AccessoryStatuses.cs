﻿namespace PackNet.IODeviceService.Base
{
    public enum AccessoryStatuses
    {
        Idle,
        Feeding,
        FeedingDone,
        WaitingForSignal,
        SignalReceived,
    }
}
