﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Communication.WebRequestCreator;
using PackNet.IODeviceService.Communication;
using PackNet.IODeviceService.Intefaces;

namespace PackNet.IODeviceService
{
    public class IoDeviceCommunicatorFactory : IIoDeviceCommunicatorFactory
    {
        private readonly IAggregateMachineService machineService;
        private readonly ILogger logger;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;

        private readonly Dictionary<Guid, IIoDeviceCommunicator> communicators = new Dictionary<Guid, IIoDeviceCommunicator>();

        public IoDeviceCommunicatorFactory(IServiceLocator serviceLocator)
        {
            machineService = serviceLocator.Locate<IAggregateMachineService>();
            logger = serviceLocator.Locate<ILogger>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
        }

        public IIoDeviceCommunicator GetCommunicatorForMachine(Guid machineId)
        {
            if (!communicators.ContainsKey(machineId))
            {
                communicators.Add(machineId, CreateCommunicatorForMachine(machineId));
            }

            if (communicators.ContainsKey(machineId))
            {
                UpdateCommunicatorIfMachineIpHasChanged(machineId);
            }

            return communicators[machineId];
        }

        private void UpdateCommunicatorIfMachineIpHasChanged(Guid machineId)
        {
            var machine = machineService.FindById(machineId) as IPacksizeCutCreaseMachine;

            var ioDeviceCommunicator = communicators[machineId];
            if (ioDeviceCommunicator == null)
                return;

            if (!HasSameIpAdress(machine, ioDeviceCommunicator))
            {
                communicators[machineId] = new IoDeviceCommunicator(machine, new WebRequestCreator(machine.IpOrDnsName, machine.Port),
                    IoDeviceVariableMap.Instance, logger, subscriber, publisher);
            }
        }

        private bool HasSameIpAdress(IPacksizeCutCreaseMachine machine, IIoDeviceCommunicator ioDeviceCommunicator)
        {
            return (ioDeviceCommunicator.IpAddress == machine.IpOrDnsName && ioDeviceCommunicator.Port == machine.Port);
        }

        private IIoDeviceCommunicator CreateCommunicatorForMachine(Guid machineId)
        {
            var machine = machineService.FindById(machineId) as IPacksizeCutCreaseMachine;
            if (machine == null)
            {
                //TODO: delete io device
                return null;
            }
            var communicator = new IoDeviceCommunicator(machine, new WebRequestCreator(machine.IpOrDnsName, machine.Port), IoDeviceVariableMap.Instance, logger, subscriber, publisher);

            return communicator;
        }
    }
}
