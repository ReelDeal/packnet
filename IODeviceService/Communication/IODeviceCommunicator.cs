﻿using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Communication;
using PackNet.Communication.Properties;
using PackNet.IODeviceService.DTO;
using PackNet.IODeviceService.Intefaces;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;

namespace PackNet.IODeviceService.Communication
{
    public class IoDeviceCommunicator : SimplePLCCommunicator, IIoDeviceCommunicator
    {
        private readonly ISubject<bool> connectionStatusChangedSubject = new Subject<bool>(); 
        
        private readonly object writeLock = new object();

        private readonly IIoDeviceVariableMap variableMap;
        private readonly ILogger logger;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;
        private readonly string serverAddress;
        private readonly string ipAddress;
        private readonly int port;
        private readonly IPacksizeCutCreaseMachine machine;
        private bool isConnected;

        public string IpAddress { get { return ipAddress; } }

        public int Port { get { return port; } }

        public IoDeviceCommunicator(IPacksizeCutCreaseMachine machine, IWebRequestCreator webRequestCreator, IIoDeviceVariableMap variableMap, ILogger logger, IEventAggregatorSubscriber subscriber, IEventAggregatorPublisher publisher) :
            base(webRequestCreator, logger)
        {
            this.machine = machine;
            this.variableMap = variableMap;
            this.logger = logger;
            this.subscriber = subscriber;
            this.publisher = publisher;
            serverAddress = webRequestCreator.ServerAddress;
            this.port = webRequestCreator.Port;
            this.ipAddress = webRequestCreator.IpAddress;
            this.logger = logger;

            Wireup();
        }

        private void Wireup()
        {
            subscriber.GetEvent<Message<VariableChangedEventArgs>>()
              //  .Where(m => m.Data.VariableName == variableMap.InputEventMessageContainer.VariableName && m.Data.Address == serverAddress)
                .ObserveOn(NewThreadScheduler.Default)//WARNING: VariableChanged is fired from the rest service... It always needs to be on a new thread
                .DurableSubscribe(m => OnVariableChanged(m.Data), logger);

            subscriber.GetEvent<Message<Tuple<string, bool>>>()
               .Where(m => m.MessageType == SimplePLCMessages.SimplePLCConnectionStatus &&
                m.Data.Item1 == serverAddress)
                .Subscribe(m =>
                {
                    if(m.Data.Item2)
                        UpdateCallbackIp();
                });


            machine.CurrentStatusChangedObservable.DurableSubscribe(
                status =>
                {
                    if (isConnected && status != MachineStatuses.MachineOffline)
                        return;

                    if (isConnected == false && status == MachineStatuses.MachineOffline)
                        return;

                    isConnected = status != MachineStatuses.MachineOffline;

                    connectionStatusChangedSubject.OnNext(isConnected);
                }, logger);
        }

        private void UpdateCallbackIp()
        {
            WriteValue(variableMap.IoDeviceCallbackIp, machine.PhysicalMachineSettings.EventNotifierIp.Address.ToString());
            WriteValue(variableMap.IoDeviceCallbackPort, machine.PhysicalMachineSettings.EventNotifierIp.Port);
        }

        private void OnVariableChanged(VariableChangedEventArgs data)
        {
            if (data.VariableName == variableMap.InputEventMessageContainer.VariableName && data.Address == serverAddress)
            {
                var received = JsonConvert.DeserializeObject<InputEvent>(data.Value.ToString());
                var inputEvent = new InputEvent(machine.Id, received.Module + 1, received.Port + 1, received.Type);

                publisher.Publish(new Message<InputEvent>() { Data = inputEvent });
            }
        }
        
        public bool EnableOutputSignal(AccessoryPort accessory)
        {
            try
            {
                WriteOutputCommandToPlc(OutputTypes.Enable, accessory.Port, accessory.Module, 0, 0);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write EnableOutput command to output index {0}, failed with exception {1}", accessory.Port, e));
                return false;
            }

            return true;
        }

        public bool DisableOutputSignal(AccessoryPort accessory)
        {
            try
            {
                WriteOutputCommandToPlc(OutputTypes.Disable, accessory.Port, accessory.Module, 0, 0);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write EnableOutput command to output index {0}, failed with exception {1}", accessory.Port, e));
                return false;
            }

            return true;
        }
        
        public bool EnableOutputSignalTemporarily(AccessoryPort capability, TimeSpan time)
        {
            try
            {
                WriteOutputCommandToPlc(OutputTypes.TemporaryEnable, capability.Port, capability.Module, 0, (int)Math.Round(time.TotalMilliseconds));
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write TemporaryEnable command to output index {0}, failed with exception {1}", capability.Port, e));
                return false;
            }

            return true;
        }

        public bool DisableOutputSignalTemporarily(AccessoryPort accessory, TimeSpan time)
        {
            try
            {
                WriteOutputCommandToPlc(OutputTypes.TemporaryDisable, accessory.Port, accessory.Module, 0, (int)Math.Round(time.TotalMilliseconds));
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write TemporaryDisable command to output index {0}, failed with exception {1}", accessory.Port, e));
                return false;
            }

            return true;
        }

        public bool OscillateOutputSignal(AccessoryPort accessory, TimeSpan highTime, TimeSpan lowTime)
        {
            try
            {
                WriteOutputCommandToPlc(OutputTypes.OscillatingSignal, accessory.Port, accessory.Module, (int)Math.Round(highTime.TotalMilliseconds), (int)Math.Round(lowTime.TotalMilliseconds));
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write TemporaryDisable command to output index {0}, failed with exception {1}", accessory.Port, e));
                return false;
            }

            return true;
        }

        private void WriteOutputCommandToPlc(OutputTypes type, int outputIndex, int moduleNumber, int t1InMs, int t2InMs)
        {
            for (var i = 0; i < 100; i++)
            {
                if (ReadValue<bool>(variableMap.OutputCommandExecute) == false)
                {
                    var command = new OutputCommandTrigger(type, outputIndex - 1, moduleNumber - 1, t1InMs, t2InMs);

                    lock (writeLock)
                    {
                        if (Settings.Default.SingleWriteToPlc)
                        {
                            command.Execute = true;
                            WriteValue(variableMap.OutputCommand, command);
                        }
                        else
                        {
                            WriteValue(variableMap.OutputCommand, command);
                            WriteValue(variableMap.OutputCommandExecute, true);
                        }
                        return;
                    }
                }

                Thread.Sleep(10);
            }
            
            throw new Exception(string.Format("Unable to write command of type {0} on index {1} with T1 {2} and T2 {3} to the PLC within a suitable timespan. The PLC was processing another command", type, outputIndex, t1InMs, t2InMs));
        }

        public bool AddSinglePositiveEdgeListener(AccessoryPort capability)
        {
            try
            {
                WriteInputCommandToPlc(InputTypes.SinglePositiveEdge, capability.Port, capability.Module);
                
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write SinglePositiveEdge command to module {0} input index {1}, failed with exception {2}", capability.Port, capability.Module, e));
                return false;
            }

            return true;
        }

        public bool AddSingleNegativeEdgeListener(AccessoryPort capability)
        {
            try
            {
                WriteInputCommandToPlc(InputTypes.SingleNegativeEdge, capability.Port, capability.Module);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write SingleNegativeEdge command to module {0} input index {1}, failed with exception {2}", capability.Port, capability.Module, e));
                return false;
            }

            return true;
        }

        public bool AddMultiplePositiveEdgeListener(AccessoryPort capability)
        {
            try
            {
                WriteInputCommandToPlc(InputTypes.MultiplePositiveEdge, capability.Port, capability.Module);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write MultiplePositiveEdge command to module {0} input index {1}, failed with exception {2}", capability.Port, capability.Module, e));
                return false;
            }

            return true;
        }

        public bool AddMultipleNegativeEdgeListener(AccessoryPort capability)
        {
            try
            {
                WriteInputCommandToPlc(InputTypes.MultipleNegativeEdge, capability.Port, capability.Module);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write MultipleNegativeEdge command to module {0} input index {1}, failed with exception {2}", capability.Port, capability.Module, e));
                return false;
            }

            return true;
        }

        public bool RemoveInputListener(AccessoryPort capability)
        {
            try
            {
                WriteInputCommandToPlc(InputTypes.RemoveMultipleEdge, capability.Port, capability.Module);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Info, string.Format("Unable to write RemoveMultipleEdge command to module {0} input index {1}, failed with exception {2}", capability.Port, capability.Module, e));
                return false;
            }

            return true;
        }

        public string GetServerAddress()
        {
            return serverAddress;
        }

        public bool IsConnected
        {
            get { return isConnected; }
        }

        public IObservable<bool> ConnectionStatusChangedObservable
        {
            get { return connectionStatusChangedSubject.AsObservable(); }
    }

        private void WriteInputCommandToPlc(InputTypes type, int index, int module)
        {
            WriteValue(variableMap.InputCommandType, type);
            WriteValue(variableMap.InputCommandInputIndex, index - 1);
            WriteValue(variableMap.InputCommandModuleIndex, module - 1);
            WriteValue(variableMap.InputCommandExecute, true);
        }
    }
};