﻿using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.IODeviceService.Intefaces;
using System.Collections.Generic;
using System.Linq;


namespace PackNet.IODeviceService.Communication
{
    public class IoDeviceVariableMap : IIoDeviceVariableMap
    {
        private const string CyclicName = "IODevice";

        public static IoDeviceVariableMap Instance = new IoDeviceVariableMap();

        private IoDeviceVariableMap()
        {
            MapOutputVariables();
            MapInputVariables();
            MapCallBackVariables();
        }
        
        public PacksizePlcVariable OutputCommand { get; private set; }
        public PacksizePlcVariable OutputCommandExecute { get; private set; }

        private void MapOutputVariables()
        {
            OutputCommand = new PacksizePlcVariable(CyclicName, "CurrentOutputCommand");
            OutputCommandExecute = new PacksizePlcVariable(CyclicName, "CurrentOutputCommand.Execute");
        }

        public PacksizePlcVariable InputCommandType { get; private set; }
        public PacksizePlcVariable InputCommandInputIndex { get; private set; }
        public PacksizePlcVariable InputCommandModuleIndex { get; private set; }
        public PacksizePlcVariable InputCommandExecute { get; private set; }
        public PacksizePlcVariable InputEventMessageContainer { get; private set; }

        private void MapInputVariables()
        {
            InputCommandType = new PacksizePlcVariable(CyclicName, "CurrentInputCommand.Command.Type");
            InputCommandInputIndex = new PacksizePlcVariable(CyclicName, "CurrentInputCommand.Command.InputIndex");
            InputCommandModuleIndex = new PacksizePlcVariable(CyclicName, "CurrentInputCommand.Command.ModuleNumber");
            InputCommandExecute = new PacksizePlcVariable(CyclicName, "CurrentInputCommand.Execute");
            InputEventMessageContainer = new PacksizePlcVariable(CyclicName, "InputEventMessageContainer");
        }


        public PacksizePlcVariable IoDeviceCallbackIp { get; private set; }
        public PacksizePlcVariable IoDeviceCallbackPort { get; private set; }
        
        private void MapCallBackVariables()
        {
            IoDeviceCallbackIp = new PacksizePlcVariable(CyclicName, "ServerIp");
            IoDeviceCallbackPort = new PacksizePlcVariable(CyclicName, "ServerPort");
        }

        public virtual IEnumerable<PacksizePlcVariable> AsEnumerable()
        {
            var info = typeof(IoDeviceVariableMap).GetProperties();

            return info.Select(pi => (PacksizePlcVariable)pi.GetValue(this, null)).ToList();
        }


        public bool ContainsVariable(string var)
        {
            return AsEnumerable().Any(v => v.ToString().Equals(var.Trim()));
        }
    }
}
