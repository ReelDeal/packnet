﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackNet.IODeviceService.Exceptions
{
    public class CommunicatorNotConnectedException : Exception
    {
        public CommunicatorNotConnectedException(string message) : base(message)
        {
            
        }
    }
}
