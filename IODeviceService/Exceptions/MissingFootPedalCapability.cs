﻿using System;

using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.Exceptions
{
    public class MissingFootPedalCapability : Exception
    {
        public IFootPedal FootPedal { get; set; }

        public MissingFootPedalCapability(string message, IFootPedal footPedal) : base(message)
        {
            FootPedal = footPedal;
        }
    }
}
