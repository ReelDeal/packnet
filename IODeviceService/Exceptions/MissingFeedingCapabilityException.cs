﻿using System;

using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.Exceptions
{
    public class MissingFeedingCapabilityException : Exception
    {
        public IConveyor Conveyor { get; set; }

        public MissingFeedingCapabilityException(string m, IConveyor conveyor) : base(m)
        {
            Conveyor = conveyor;
        }
    }
}
