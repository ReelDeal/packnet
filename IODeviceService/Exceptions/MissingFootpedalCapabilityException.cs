﻿using System;

using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.Exceptions
{
    public class MissingFootpedalCapabilityException : Exception
    {
        public IFootPedal FootPedal { get; set; }

        public MissingFootpedalCapabilityException(string m, IFootPedal footPedal)
            : base(m)
        {
            FootPedal = footPedal;
        }
    }
}
