﻿using System;

namespace PackNet.IODeviceService.Exceptions
{
    public class AccessoryModuleAndPortInUseException : Exception
    {
        public AccessoryModuleAndPortInUseException(string message) : base(message)
        {
            
        }
    }
}
