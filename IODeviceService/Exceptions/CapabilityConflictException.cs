﻿using System;

namespace PackNet.IODeviceService.Exceptions
{
    public class CapabilityConflictException : Exception
    {
        public CapabilityConflictException(string message) : base(message)
        {

        }
    }
}
