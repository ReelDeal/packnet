﻿using System;

namespace PackNet.IODeviceService.Exceptions
{
    public class AccessoryNotIdleException : Exception
    {
        public AccessoryNotIdleException(string message) : base(message)
        {
            
        }
    }
}
