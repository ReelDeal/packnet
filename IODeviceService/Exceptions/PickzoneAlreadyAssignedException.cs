﻿using System;

namespace PackNet.IODeviceService.Exceptions
{
    public class PickzoneAlreadyAssignedException : Exception
    {
        public PickzoneAlreadyAssignedException(string message) : base(message)
        {
        }
    }
}
