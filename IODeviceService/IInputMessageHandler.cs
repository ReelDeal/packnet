﻿using System;

using PackNet.IODeviceService.DTO;

namespace PackNet.IODeviceService
{
    public interface IInputMessageHandler
    {
        IObservable<InputEvent> SignalReceivedObservable { get; }
    }
}
