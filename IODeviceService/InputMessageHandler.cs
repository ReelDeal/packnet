﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.DTO;
using PackNet.IODeviceService.L10N;

namespace PackNet.IODeviceService
{
    public class InputMessageHandler : IInputMessageHandler
    {
        private readonly Subject<InputEvent> signalReceivedSubject = new Subject<InputEvent>(); 

        private readonly IEventAggregatorSubscriber subscriber;
        private readonly ILogger logger;

        public InputMessageHandler(IServiceLocator serviceLocator)
        {
            logger = serviceLocator.Locate<ILogger>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();

            subscriber.GetEvent<IMessage<InputEvent>>().DurableSubscribe(InputMessageReceived);
        }

        public IObservable<InputEvent> SignalReceivedObservable { get { return signalReceivedSubject.AsObservable(); } }
        
        private void InputMessageReceived(IMessage<InputEvent> message)
        {
            if (message.Data != null)
            {
                signalReceivedSubject.OnNext(message.Data);
                logger.Log(LogLevel.Trace,
                    String.Format("Input signal received on Machine: {0} Module: {1} Port: {2}", message.Data.Sender,
                        message.Data.Module, message.Data.Port));
                return;
            }

            logger.Log(LogLevel.Trace,String.Format(Strings.InputMessageHandler_InputMessageReceived_Input_signal_message_received_but_the_data_was_NULL));
        }
    }
}
