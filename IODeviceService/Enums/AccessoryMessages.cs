﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.IODeviceService.Enums
{
    public class AccessoryTypes : Enumeration
    {
        public static readonly AccessoryTypes FootPedal = new AccessoryTypes("FootPedal");
        public static readonly AccessoryTypes LightTower = new AccessoryTypes("LightTower");
        public static readonly AccessoryTypes Conveyor = new AccessoryTypes("Conveyor");

        private AccessoryTypes(string displayName)
            : base(0, displayName)
        {
        }
    }
    
    public class AccessoryMessages : MessageTypes
    {
        public static readonly AccessoryMessages GetIoAccessoriesForMachine = new AccessoryMessages("GetIoAccessoriesForMachine");
        public static readonly AccessoryMessages MachineIoAccessories = new AccessoryMessages("MachineIoAccessories");

        public static readonly AccessoryMessages CreateFootPedalAccessory = new AccessoryMessages("CreateFootPedalAccessory");
        public static readonly AccessoryMessages UpdateFootPedalAccessory = new AccessoryMessages("UpdateFootPedalAccessory");
        public static readonly AccessoryMessages DeleteFootPedalAccessory = new AccessoryMessages("DeleteFootPedalAccessory");

        public static readonly AccessoryMessages CreateConveyorAccessory = new AccessoryMessages("CreateConveyorAccessory");
        public static readonly AccessoryMessages UpdateConveyorAccessory = new AccessoryMessages("UpdateConveyorAccessory");
        public static readonly AccessoryMessages DeleteConveyorAccessory = new AccessoryMessages("DeleteConveyorAccessory");
        
        public static readonly AccessoryMessages IOAccessoryCreated = new AccessoryMessages("IOAccessoryCreated");
        public static readonly AccessoryMessages IOAccessoryUpdated = new AccessoryMessages("IOAccessoryUpdated");
        public static readonly AccessoryMessages IOAccessoryDeleted = new AccessoryMessages("IOAccessoryDeleted");

        private AccessoryMessages(string name) : base(name)
        {
        }
    }
}