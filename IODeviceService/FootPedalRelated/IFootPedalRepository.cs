﻿using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.FootPedalRelated
{
    public interface IFootPedalRepository : IAccessorieRepository<FootPedal>
    {
    }
}
