﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Data;
using PackNet.Data.Repositories.MongoDB;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.FootPedalRelated
{
    public class FootPedalRepository : MongoDbRepository<FootPedal>, IFootPedalRepository
    {
        private readonly object syncObject = new object();
        private readonly bool serializerRegistered;

        public FootPedalRepository()
            : base(Defaults.Default.ConnectionString, Defaults.Default.DatabaseName, "FootPedals")
        {
            lock (syncObject)
            {
                if (serializerRegistered)
                    return;

                serializerRegistered = true;
            }
        }
        
        public IEnumerable<FootPedal> WithAlias(string alias)
        {
            return Collection.FindAll().Where(c => c.Alias == alias);
        }
    }
}
