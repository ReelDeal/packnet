﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Enums;
using PackNet.IODeviceService.Exceptions;

namespace PackNet.IODeviceService.FootPedalRelated
{
    public class FootPedalService : IFootPedalService
    {
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IEventAggregatorPublisher publisher;
        private readonly ILogger logger;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IMachineGroupService machineGroupService;

        private readonly IFootPedals footPedals;

        public string Name { get { return "FootPedalService"; } }

        public FootPedalService(IServiceLocator serviceLocator, IFootPedalRepository repo, IInputMessageHandler inputMessageHandler, IIoDeviceCommunicatorFactory communicatorFactory)
        {
            serviceLocator.RegisterAsService(this);
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            logger = serviceLocator.Locate<ILogger>();
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            machineGroupService = serviceLocator.Locate<IMachineGroupService>();

            footPedals = new FootPedals(repo, communicatorFactory, inputMessageHandler, serviceLocator);

            RegisterUiCommunication();
        }

        private void RegisterUiCommunication()
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<FootPedal>(AccessoryMessages.CreateFootPedalAccessory);
            AccessoryMessages.CreateFootPedalAccessory.OnMessage<FootPedal>(subscriber, logger, msg =>
                {
                    var result = ResultTypes.Success;

                    FootPedal createdFootPedal = null;
                    Exception thrownException = null;

                    try
                    {
                        createdFootPedal = footPedals.Add(msg.Data);
                    }
                    catch (AlreadyPersistedException e)
                    {
                        result = ResultTypes.Exists;
                        thrownException = e;
                    }
                    catch (ItemExistsException e)
                    {
                        result = ResultTypes.Exists;
                        thrownException = e;
                    }
                    catch (Exception e)
                    {
                        result = ResultTypes.Fail;
                        thrownException = e;
                    }
                    finally
                    {
                        if (thrownException != null)
                        {
                            logger.Log(LogLevel.Error, string.Format("Unable to create Foot Pedal  with exception: {0}", thrownException));
                        }
                    }
                    uiCommunicationService.SendMessageToUI(new ResponseMessage<FootPedal>
                    {
                        MessageType = AccessoryMessages.IOAccessoryCreated,
                        Result = result,
                        ReplyTo = msg.ReplyTo,
                        Data = createdFootPedal,
                        Message = thrownException == null ? string.Empty : thrownException.Message
                    });
                });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<FootPedal>(AccessoryMessages.UpdateFootPedalAccessory);
            AccessoryMessages.UpdateFootPedalAccessory.OnMessage<FootPedal>(subscriber, logger, msg =>
                {
                    var result = ResultTypes.Success;

                    Exception thrownException = null;
                    FootPedal updatedFootPedal = null;

                    try
                    {
                        updatedFootPedal = footPedals.Update(msg.Data);
                    }
                    catch (Exception e)
                    {
                        result = ResultTypes.Fail;
                        thrownException = e;
                    }
                    finally
                    {
                        if (thrownException != null)
                        {
                            logger.Log(LogLevel.Error, string.Format("Unable to create Foot Pedal  with exception: {0}", thrownException));
                        }
                    }

                    uiCommunicationService.SendMessageToUI(new ResponseMessage<FootPedal>
                    {
                        MessageType = AccessoryMessages.IOAccessoryUpdated,
                        Result = result,
                        ReplyTo = msg.ReplyTo,
                        Data = updatedFootPedal,
                        Message = thrownException == null ? string.Empty : thrownException.Message
                    });
                });

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<FootPedal>(AccessoryMessages.DeleteFootPedalAccessory);
            AccessoryMessages.DeleteFootPedalAccessory.OnMessage<FootPedal>(subscriber, logger, msg =>
            {
                var result = ResultTypes.Success;

                Exception thrownException = null;
                
                try
                {
                    footPedals.Delete(msg.Data);
                }
                catch (Exception e)
                {
                    result = ResultTypes.Fail;
                    thrownException = e;
                }
                finally
                {
                    if (thrownException != null)
                    {
                        logger.Log(LogLevel.Error, string.Format("Unable to create Foot Pedal with exception: {0}", thrownException));
                    }
                }

                uiCommunicationService.SendMessageToUI(new ResponseMessage<FootPedal>
                {
                    MessageType = AccessoryMessages.IOAccessoryDeleted,
                    Result = result,
                    ReplyTo = msg.ReplyTo,
                    Data = msg.Data,
                    Message = thrownException == null ? string.Empty : thrownException.Message
                });

                publisher.Publish(new Message<Guid>
                {
                    ReplyTo = msg.ReplyTo,
                    MessageType = AccessoryMessages.GetIoAccessoriesForMachine,
                    Data = msg.Data.MachineId
                });
            });
        }
        
        public IEnumerable<IFootPedal> GetFootPedalsForMachine(Guid machineId)
        {
            return footPedals.Current.Where(c => c.MachineId == machineId);
        }

        public IEnumerable<IFootPedal> GetFootPedalsForMachineGroup(Guid machineGroupId)
        {
            var machinesInMg = machineGroupService.GetMachinesInGroup(machineGroupId);

            return footPedals.Current.Where(footPedal => machinesInMg.Any(machine => machine.Id == footPedal.MachineId));
        }

        public void DeleteFootPedalsForMachine(Guid machineId)
        {
            footPedals.DeleteForMachine(machineId);
        }

        public void Dispose()
        {

        }
    }
}