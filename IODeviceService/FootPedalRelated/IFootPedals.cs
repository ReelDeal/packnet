﻿using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.FootPedalRelated
{
    public interface IFootPedals : IAccessories<FootPedal, IFootPedal>
    {
        FootPedal Update(FootPedal newData);
    }
}
