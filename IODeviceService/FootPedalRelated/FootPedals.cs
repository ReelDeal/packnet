﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reactive.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.Exceptions;
using PackNet.IODeviceService.Intefaces;

namespace PackNet.IODeviceService.FootPedalRelated
{
    public class FootPedals : Accessories<FootPedal, IFootPedal>, IFootPedals
    {
        private readonly IInputMessageHandler inputMessageHandler;
        private readonly ILogger logger;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IMachineGroupService machineGroupService;

        private readonly ConcurrentDictionary<Guid, IDisposable> machineGroupPausePlayListeners;
        private readonly ConcurrentDictionary<Guid, IDisposable> connectionStatusChangedListeners;
        private readonly ConcurrentDictionary<Guid, IDisposable> triggerObservables;

        public FootPedals(IFootPedalRepository repository, IIoDeviceCommunicatorFactory ioDeviceCommunicatorFactory, IInputMessageHandler inputMessageHandler, IServiceLocator serviceLocator)
            : base(repository, ioDeviceCommunicatorFactory)
        {
            this.inputMessageHandler = inputMessageHandler;
            
            logger = serviceLocator.Locate<ILogger>();
            publisher = serviceLocator.Locate<IEventAggregatorPublisher>();
            machineGroupService = serviceLocator.Locate<IMachineGroupService>();

            machineGroupPausePlayListeners = new ConcurrentDictionary<Guid, IDisposable>();
            connectionStatusChangedListeners = new ConcurrentDictionary<Guid, IDisposable>();
            triggerObservables = new ConcurrentDictionary<Guid, IDisposable>();

            accessories.Values.OfType<IFootPedal>().ForEach(AddListeners);
        }

        public override FootPedal Add(FootPedal footPedal)
        {
            var fp = base.Add(footPedal);

            AddListeners(footPedal);

            return fp;
        }
        
        public FootPedal Update(FootPedal newData)
        {
            if (Repository.Find(newData.Id) == null)
                throw new ItemDoesNotExistException("NotExists");

            FootPedal local;
            accessories.TryGetValue(newData.Id, out local);

            if (local == null)
                throw new ItemDoesNotExistException("NotExists");

            if (ValidatePortAndModule(newData) == false)
                throw new AccessoryModuleAndPortInUseException("AccessoryModuleAndPortInUse");

            RemoveListenersForFootPedal(local);

            local.UpdateFrom(newData);
            
            var fp = Repository.Update(local);

            AddListeners(fp);

            return fp;
        }

        private void AddListeners(IFootPedal footPedal)
        {
            var communicator = ioDeviceCommunicatorFactory.GetCommunicatorForMachine(footPedal.MachineId);
            if (communicator == null)
                return;

            if (communicator.IsConnected)
            {
                if (footPedal.MachineGroupPausePlay != null)
                {
                    RegisterMachineGroupPausePlayListener(footPedal, communicator);
                }
                else if (footPedal.FootPedalTrigger != null)
                {
                    RegisterTriggerListener(footPedal, communicator);
                }
            }


            var o = communicator.ConnectionStatusChangedObservable.DurableSubscribe(connected =>
            {
                if (connected)
                {
                    if (footPedal.MachineGroupPausePlay != null)
                    {
                        RegisterMachineGroupPausePlayListener(footPedal, communicator);
                    }
                    else if (footPedal.FootPedalTrigger != null)
                    {
                        RegisterTriggerListener(footPedal, communicator);
                    }
                }
                else
                {
                    RemoveFootPedalActionListeners(footPedal);
                    footPedal.CurrentStatus = AccessoryStatuses.Idle;
                }
            });

            connectionStatusChangedListeners.AddOrUpdate(footPedal.Id, o, (key, obs) => o);
        }
        
        private void RemoveListenersForFootPedal(IFootPedal fp)
        {
            IDisposable disposable;
            if (connectionStatusChangedListeners.TryRemove(fp.Id, out disposable))
            {
                disposable.Dispose();
            }

            RemoveFootPedalActionListeners(fp);
        }

        private void RemoveFootPedalActionListeners(IFootPedal fp)
        {
            IDisposable disposable;
            if (machineGroupPausePlayListeners.TryRemove(fp.Id, out disposable))
            {
                disposable.Dispose();
            }

            if (triggerObservables.TryRemove(fp.Id, out disposable))
            {
                disposable.Dispose();
            }
        }

        private void RegisterMachineGroupPausePlayListener(IFootPedal footPedal, IIoDeviceCommunicator communicator)
        {
            communicator.AddMultiplePositiveEdgeListener(footPedal.MachineGroupPausePlay);
            SetupMachineGroupPausePlayCallBack(footPedal);
        }

        private void RegisterTriggerListener(IFootPedal footPedal, IIoDeviceCommunicator communicator)
        {
            communicator.AddMultiplePositiveEdgeListener(footPedal.FootPedalTrigger);
            SetupTriggerCallback(footPedal);
        }

        private void SetupMachineGroupPausePlayCallBack(IFootPedal footPedal)
        {
            var o = inputMessageHandler.SignalReceivedObservable.Where(
                m => m.Sender == footPedal.MachineId && m.Module == footPedal.MachineGroupPausePlay.Module && m.Port == footPedal.MachineGroupPausePlay.Port).DurableSubscribe(
                    (data) =>
                    {
                        var mg = machineGroupService.FindByMachineId(footPedal.MachineId);

                        if (mg == null)
                            return;

                        if (mg.CurrentStatus is MachineGroupAvailableStatuses == false)
                            return;

                        publisher.Publish(new Message<MachineGroup>
                        {
                            MessageType = MachineGroupMessages.ChangeMachineGroupStatus,
                            Data = mg
                        });
                    }, logger);

            footPedal.CurrentStatus = AccessoryStatuses.WaitingForSignal;

            machineGroupPausePlayListeners.AddOrUpdate(footPedal.Id, o, (key, obs) => o);
        }

        private void SetupTriggerCallback(IFootPedal footPedal)
        {
            var o = inputMessageHandler.SignalReceivedObservable.Where(
                m =>
                    m.Sender == footPedal.MachineId && m.Module == footPedal.FootPedalTrigger.Module &&
                    m.Port == footPedal.FootPedalTrigger.Port).DurableSubscribe(
                        (data) =>
                        {
                            footPedal.CurrentStatus = AccessoryStatuses.SignalReceived;
                            footPedal.CurrentStatus = AccessoryStatuses.WaitingForSignal;
                        }, logger);

            footPedal.CurrentStatus = AccessoryStatuses.WaitingForSignal;

            triggerObservables.AddOrUpdate(footPedal.Id, o, (key, obs) => o);
        }

        public override void Delete(FootPedal footPedalToDelete)
        {
            base.Delete(footPedalToDelete);

            RemoveListenersForFootPedal(footPedalToDelete);
        }

        public override void DeleteForMachine(Guid machineId)
        {
            var accessoriesRemove = Repository.All().Where(c => c.MachineId == machineId).ToList();

            base.DeleteForMachine(machineId);

            accessoriesRemove.ForEach(RemoveListenersForFootPedal);
        }
    }
}
