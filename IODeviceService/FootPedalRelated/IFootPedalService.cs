﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.FootPedalRelated
{
    public interface IFootPedalService : IService
    {
        IEnumerable<IFootPedal> GetFootPedalsForMachine(Guid machineId);

        void DeleteFootPedalsForMachine(Guid machineId);
    }
}
