﻿using System;

namespace PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities
{
    public class AccessoryPort : IEquatable<AccessoryPort>
    {
        /// <summary>
        /// The number of the IO module that the accessory is connected to
        /// </summary>
        public int Module { get; set; }

        /// <summary>
        /// The port that the device is connected to
        /// </summary>
        public int Port { get; set; }
        
        public bool Equals(AccessoryPort other)
        {
            return other.Module == Module && other.Port == Port;
        }
    }
}
