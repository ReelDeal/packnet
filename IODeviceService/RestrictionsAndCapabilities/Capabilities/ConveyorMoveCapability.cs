﻿using System;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions;

namespace PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities
{
    public class ConveyorMoveCapability : BasicCapability<AccessoryPort>
    {
        public override Type RestrictionType { get { return typeof(ConveyorMoveRestriction); } }

        public ConveyorMoveCapability(AccessoryPort value) : base(value)
        {
        }

        public new string DisplayValue
        {
            get { return "Conveyor Simple Feed"; }
        }
    }
}
