﻿using System;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Producible;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions;

namespace PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities
{
    public class ConveyorPickzoneCapability : ConveyorMoveCapability
    {
        public ConveyorPickzoneCapability(AccessoryPort value) : base(value)
        {
        }

        public PickZone PickZone { get; set; }

        public override Type RestrictionType { get { return typeof(ConveyorPickzoneRestriction); } }

        public new string DisplayValue
        {
            get { return "Can Feed Conveyor to Pick Zone: " + PickZone.Alias; }
        }

        public override bool Evaluate(IRestriction restriction)
        {
            var res = restriction as ConveyorPickzoneRestriction;

            if (res == null)
                return false;

            return res.Value.Id == PickZone.Id;
        }

        public override bool Equals(object obj)
        {
            var other = obj as ConveyorPickzoneCapability;
            if (other == null)
                return false;

            var baseEquals = base.Equals(obj);
            return baseEquals && PickZone.Id == other.PickZone.Id;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (PickZone != null ? PickZone.GetHashCode() : 0);
            }
        }

    }
}
