﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions
{
    public class ConveyorPickzoneRestrictionResolver : BasicRestrictionResolver
    {
        public override IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { typeof(ConveyorPickzoneRestriction) };
            }
        }
    }
}
