﻿using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions
{
    public class ConveyorPickzoneRestriction : BasicRestriction<PickZone>
    {
    }
}
