﻿using System;
using System.Activities;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.CodeActivities
{
    public sealed class FeedConveyor : CodeActivity
    {
        [RequiredArgument]
        public InArgument<Conveyor> Conveyor { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<int> TimeInMs { get; set; }

        [RequiredArgument]
        public InArgument<PickZone> Pickzone { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);

            var conveyorService = serviceLocator.Locate<IConveyorService>();
            var logger = serviceLocator.Locate<ILogger>();

            var conveyor = context.GetValue(Conveyor);
            var feedingTime = TimeSpan.FromMilliseconds(context.GetValue(TimeInMs));
            var pickzone = context.GetValue(Pickzone);

            try
            {
                conveyorService.FeedConveyorToPickZone(conveyor, feedingTime, pickzone);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error,
                    string.Format("Unable to feed Conveyor with id: {0} and Alias: {1} to the left. with exception {2}",
                        conveyor.Id, conveyor.Alias, e));
            }
        }
    }
}
