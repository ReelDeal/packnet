﻿using System;
using System.Activities;
using System.Linq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Capabilities;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions;

namespace PackNet.IODeviceService.CodeActivities
{
    public sealed class FeedProducibleToPickZone : CodeActivity<IConveyor>
    {
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<int> TimeInMs { get; set; }
        
        protected override IConveyor Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);

            var conveyorService = serviceLocator.Locate<IConveyorService>();
            var logger = serviceLocator.Locate<ILogger>();
            var userNotificationService = serviceLocator.Locate<IUserNotificationService>();

            var producible = context.GetValue(Producible);

            var time = TimeSpan.FromMilliseconds(context.GetValue(TimeInMs));

            var destinationRestriction = producible.Restrictions.FirstOrDefault(restriction => restriction is ConveyorPickzoneRestriction);

            if (destinationRestriction == null)
            {
                userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error,
                       string.Format("Unable to find the destination Pick Zone for Producible with Id: {0}", producible.Id), string.Empty, producible.ProducedOnMachineGroupId);
                return null;
            }
            var destinationPickZone = (destinationRestriction as ConveyorPickzoneRestriction).Value;

            var conveyors = conveyorService.GetConveyorsForMachineGroup(producible.ProducedOnMachineGroupId);
            var conveyor =
                conveyors.FirstOrDefault(
                    c =>
                        c.MoveCapabilities.OfType<ConveyorPickzoneCapability>()
                            .Any(cap => cap.PickZone.Id == destinationPickZone.Id));

            if (conveyor == null)
            {
                userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Error,
                    string.Format("Unable to find a conveyor for Producible with Id: {0} for Pick Zone: {1}", producible.Id,
                        destinationPickZone.Alias), string.Empty, producible.ProducedOnMachineGroupId);
                
                return null;
            }

            try
            {
                conveyorService.FeedConveyorToPickZone(conveyor as Conveyor, time, destinationPickZone);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error,
                    string.Format("Unable to feed Conveyor with id: {0} and Alias: {1} to the left. with exception {2}",
                        conveyor.Id, conveyor.Alias, e));
            }


            return conveyor;
        }
    }
}
