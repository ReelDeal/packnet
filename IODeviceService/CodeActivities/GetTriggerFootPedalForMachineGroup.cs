﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.DTO.Accessories;
using PackNet.IODeviceService.FootPedalRelated;

namespace PackNet.IODeviceService.CodeActivities
{

    public sealed class GetTriggerFootPedalForMachineGroup : CodeActivity<IFootPedal>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }
        
        protected override IFootPedal Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);
            var machineGroupId = context.GetValue(MachineGroupId);
            var footPedalService = serviceLocator.Locate<FootPedalService>();

            var footpedals = footPedalService.GetFootPedalsForMachineGroup(machineGroupId);

            var footPedalTrigger = footpedals.FirstOrDefault(fp => fp.FootPedalTrigger != null);

            //Should we throw exception here? Or just return footpedalTrigger even it is null, and Send notifications & logs in WF.
            if (footPedalTrigger == null)
            {
                throw new Exception(string.Format("Unable to locate a trigger footpedal for machine group {0}", machineGroupId));
            }

            return footPedalTrigger;
        }
    }

    public sealed class GetAllTriggerFootPedalsForMachineGroup : CodeActivity<IEnumerable<IFootPedal>>
    {
        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<Guid> MachineGroupId { get; set; }

        protected override IEnumerable<IFootPedal> Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);
            var machineGroupId = context.GetValue(MachineGroupId);
            var footPedalService = serviceLocator.Locate<FootPedalService>();

            var footpedals = footPedalService.GetFootPedalsForMachineGroup(machineGroupId);

            var footPedalTriggers = footpedals.Where(fp => fp.FootPedalTrigger != null);

            if (footPedalTriggers.Any())
            {
                throw new Exception(string.Format("Unable to locate a trigger footpedal for machine group {0}", machineGroupId));
            }

            return footPedalTriggers;
        }
    }
}
