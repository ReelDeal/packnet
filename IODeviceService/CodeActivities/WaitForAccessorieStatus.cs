﻿using System;
using System.Activities;
using System.Activities.Hosting;
using System.Collections.Generic;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.Base;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.CodeActivities
{
    public sealed class WaitForAccessorieStatus : NativeActivity
    {
        private static ILogger logger;

        [RequiredArgument]
        public InArgument<IAccessory> Accessory { get; set; }

        [RequiredArgument]
        public InArgument<AccessoryStatuses> Status { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        protected override void CacheMetadata(NativeActivityMetadata metadata)
        {
            base.CacheMetadata(metadata);
            metadata.AddDefaultExtensionProvider(() => new WaitForAccessoryStatusExtension());
        }

        protected override bool CanInduceIdle
        {
            get { return true; }
        }

        protected override void Execute(NativeActivityContext context)
        {
            var accessory = Accessory.Get(context);
            if (accessory == null)
                throw new Exception("Unable to wait for Accessory Status, No Accessory supplied");

            //When we have a foot pedal used for play/pause, we don't want to wait for a trigger, so
            //we only execute the following when we are not a foot pedal, or we are a footpedal used for
            //trigger
            var notFootPedal = !(accessory is FootPedal);
            if (notFootPedal || ((FootPedal)accessory).FootPedalTrigger != null)
            {
                var status = Status.Get(context);
                logger = ServiceLocator.Get(context).Locate<ILogger>();

                /*Note: the use of Guid.NewGuid in the bookmark at the end is necessary because multiple bookmarks can be created with the same producible at different parts 
                of execution of the workflow.  with out the guid we were getting "bookmark already exists" exceptions here.*/
                var bookmark =
                    context.CreateBookmark(string.Format("WaitForFeedCompletion-AccessoryId:{0}-:::{1}", accessory.Id, Guid.NewGuid()), BookmarkResumed);
                var extension = context.GetExtension<WaitForAccessoryStatusExtension>();
                extension.WaitForAccessoryStatus(bookmark, accessory, status, logger);
            }
        }

        private void BookmarkResumed(NativeActivityContext context, Bookmark bookmark, object value)
        {
        }
    }

    internal class WaitForAccessoryStatusExtension : IWorkflowInstanceExtension
    {
        private WorkflowInstanceProxy instance;

        public IEnumerable<object> GetAdditionalExtensions()
        {
            return null;
        }

        public void SetInstance(WorkflowInstanceProxy instance)
        {
            this.instance = instance;
        }

        /// <summary>
        /// Waits for the message, resumes the bookmark with the message
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="status"></param>
        /// <param name="subscriber"></param>
        /// <param name="logger"></param>
        /// <param name="accessory"></param>
        public void WaitForAccessoryStatus(Bookmark bookmark, IAccessory accessory, AccessoryStatuses status, ILogger logger)
        {
            var doneLock = new object();
            var done = false;

            IDisposable o = null;
            o = accessory.AccessoryStatusObservable
                .Where(ms => ms == status)
                .DurableSubscribe(ms =>
                {
                    o.Dispose();
                    lock (doneLock)
                    {
                        if (!done)
                        {
                            done = true;
                            var ias = instance.BeginResumeBookmark(bookmark, ms, null, null);

                            if (ias.IsCompleted)
                            {
                                var result = instance.EndResumeBookmark(ias);
                                logger.Log(LogLevel.Debug, "accessory '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'", accessory.Alias, accessory.Id, ms, bookmark.Name, result);
                            }
                        }
                    }
                }, logger);

            //last minute race check
            if (accessory.CurrentStatus == status)
            {
                o.Dispose();
                lock (doneLock)
                {
                    if (!done)
                    {
                        done = true;
                        var ias = instance.BeginResumeBookmark(bookmark, accessory.CurrentStatus, null, null);

                        if (ias.IsCompleted)
                        {
                            var result = instance.EndResumeBookmark(ias);
                            logger.Log(LogLevel.Debug, "accessory '{0}'({1}) moved to status '{2}', Resuming bookmark '{3}' -- '{4}'",
                                accessory.Alias, accessory.Id, accessory.CurrentStatus, bookmark.Name, result);
                        }
                    }
                }
            }
        }
    }
}

