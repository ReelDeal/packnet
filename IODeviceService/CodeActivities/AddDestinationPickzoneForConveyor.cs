﻿using System.Activities;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Producible;
using PackNet.IODeviceService.RestrictionsAndCapabilities.Restrictions;

namespace PackNet.IODeviceService.CodeActivities
{
    public class AddDestinationPickzoneForConveyor : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }

        [RequiredArgument]
        public InArgument<PickZone> Destination { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var producible = context.GetValue(Producible);
            var pickZone = context.GetValue(Destination);

            producible.Restrictions.Add(new ConveyorPickzoneRestriction { Value = pickZone });
        }
    }
}
