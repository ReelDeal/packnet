﻿using System;
using System.Activities;
using System.Linq;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.IODeviceService.ConveyorRelated;
using PackNet.IODeviceService.DTO.Accessories;

namespace PackNet.IODeviceService.CodeActivities
{
    public sealed class FeedWasteConveyor : CodeActivity
    {
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }

        [RequiredArgument]
        public InArgument<IServiceLocator> ServiceLocator { get; set; }

        [RequiredArgument]
        public InArgument<int> TimeInMs { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var serviceLocator = context.GetValue(ServiceLocator);
            var producible = context.GetValue(Producible);

            var conveyorService = serviceLocator.Locate<IConveyorService>();
            var logger = serviceLocator.Locate<ILogger>();

            var feedingTime = TimeSpan.FromMilliseconds(context.GetValue(TimeInMs));

            var conveyor = conveyorService.GetConveyorsForMachine(producible.ProduceOnMachineId).FirstOrDefault(c => c.ConveyorType.Equals(ConveyorType.WasteConveyor));
            
            try
            {
                if(conveyor == null)
                    throw new Exception("Unable to find a Waste conveyor for Machine with Id: " + producible.ProduceOnMachineId);

                conveyorService.FeedConveyor(conveyor as Conveyor, feedingTime);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Warning,
                    string.Format("Unable to feed waste Conveyor for Machine with exception: {0}", e));
            }
        }
    }
}
