﻿using System;

namespace PackNet.Common.Interfaces.Networking
{
    public interface ISocketListener : IDisposable
    {
        Func<string, string> ResponseOnConnection { get; set; }
        void Bind();
        void Start();
        void Stop();
    }
}