﻿using System;

namespace PackNet.Common.Interfaces.Networking
{
    public interface IObservableTcpListener : ISocketListener
    {
        int Port { get; set; }
        Guid MachineId  { get; }
    }
}