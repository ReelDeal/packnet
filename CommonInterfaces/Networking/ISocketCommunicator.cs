﻿using System;
using System.Net;

namespace PackNet.Common.Interfaces.Networking
{
    public interface ISocketCommunicator : IDisposable
    {
        IPEndPoint EndPoint { get; }
        void Send(string message, int retryTimes = 5);
        string ReceiveLines(int linesToRead = 1, int retryTimes = 5);
        string ReceiveUntilDelimiter(string delimiter, int retryTimes = 5);
        string ReceiveUntilDelimiter(char delimiter, int retryTimes = 5);

        string SendReceiveLines(string message, int linesToRead = 1, int sendRetryTimes = 5, int receiveRetryTimes = 5);
        string SendReceiveUntilDelimiter(string message, string delimiter, int sendRetryTimes = 5, int receiveRetryTimes = 5);
        string SendReceiveUntilDelimiter(string message, char delimiter, int sendRetryTimes = 5, int receiveRetryTimes = 5);

        /// <summary>
        /// Reconnects the socket.
        /// </summary>
        void ReconnectSocket();
        /// <summary>
        /// Disconnects this instance.
        /// </summary>
        void Disconnect();
    }
}