﻿using System;
using System.Reactive.Subjects;

namespace PackNet.Common.Interfaces.Networking
{
    public interface IObservableStream<T> : IDisposable, ISubject<T>
    {
        /// <summary>
        /// Writes the given payload out to the stream
        /// </summary>
        /// <param name="payload">What will be written to the stream, using the specified encoding</param>
        void WriteToStream(string payload);
    }
}