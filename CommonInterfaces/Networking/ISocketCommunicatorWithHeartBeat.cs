﻿using System;
using System.Net;

namespace PackNet.Common.Interfaces.Networking
{
    public interface ISocketCommunicatorWithHeartBeat : ISocketCommunicator
    {
        IObservable<Tuple<bool, string>> HeartbeatObservable { get; }
    }
}