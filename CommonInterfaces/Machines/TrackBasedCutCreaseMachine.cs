using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Machines
{
    /// <summary>
    /// This class is used for serializing track based machines across communication boundaries. The real implementation of a machine will have Produce,LoadCorrugate, Etc
    /// </summary>
    public class TrackBasedCutCreaseMachine : BaseStatusReportingMachine<MachineStatuses>, ITrackBasedCutCreaseMachine
    {
        public MachineTypes MachineType { get { return null; } }

        public string Description { get; set; }
        public int NumTracks { get; set; }
        public ConcurrentList<Track> Tracks { get; set; }

        public void LoadCorrugate(Track track, Corrugate corrugate)
        {
            throw new NotImplementedException();
        }
    }
}