﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Producible;

using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace PackNet.Common.Interfaces.Machines
{
    public interface IMachineCommunicator : IDisposable
    {
        void PauseMachine();

        void Produce(IProducible producible, IEnumerable<IInstructionItem> instructionList);

        void SetNumberOfTracks(int numTracks);
        
        void SetCorrugateWidths(IEnumerable<Track> tracks);
        
        bool IsConnected { get; }

        void OnVariableChanged(VariableChangedEventArgs e);

        void SynchronizeMachine(bool isReloadMachineConfig = false);

        void Shutdown();

        void UpdateWebRequestCreator(IWebRequestCreator getWebRequestCreator);
    }
}
