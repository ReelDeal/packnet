﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.Common.Interfaces.Machines
{
    public interface IEmMachineCommunicator : IMachineCommunicator
    {
        void SetChangeCorrugate(bool value, int trackNumber);

        void LeaveChangeCorrugate();

        void SetTrackStartinPositions(IEnumerable<Track> tracks);
    }
}
