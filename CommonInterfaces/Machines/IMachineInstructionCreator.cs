﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;

using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace PackNet.Common.Interfaces.Machines
{
    public interface IMachineInstructionCreator<in T> where T: PhysicalMachineSettings
    {
        /// <summary>
        /// Gets the instruction list for job.
        /// </summary>
        /// <param name="cartonRequestOrder">The carton request order.</param>
        /// <param name="design">The design.</param>
        /// <param name="track">The track.</param>
        /// <param name="machineSettings">The machine settings.</param>
        /// <returns></returns>
        IEnumerable<IInstructionItem> GetInstructionListForJob(IPacksizeCarton cartonRequestOrder, PhysicalDesign design, Track track, T machineSettings);

        /// <summary>
        /// Determines whether is the long head positioning valid for the specified carton.
        /// </summary>
        /// <param name="cartonRequestOrder">The carton request order.</param>
        /// <param name="design">The design.</param>
        /// <param name="track">The track.</param>
        /// <param name="machineSettings">The machine settings.</param>
        /// <returns></returns>
        bool IsLongHeadPositioningValidFor(IPacksizeCarton cartonRequestOrder, PhysicalDesign design, Track track, T machineSettings);

        /// <summary>
        /// Resets the code generator.
        /// </summary>
        void ResetCodeGenerator();

        /// <summary>
        /// Determines whether this instance can produce packaging for the Carton.
        /// </summary>
        /// <param name="parameters">The parameters.</param>
        /// <param name="machineSettings">The machine settings.</param>
        /// <param name="numberOfTracks">The number of track available on the machine</param>
        /// <param name="maxPosition">Maximum allowed longhead position</param>
        /// <returns></returns>
        bool CanProducePackagingFor(CanProducePackagingForParameters parameters, T machineSettings, int numberOfTracks, MicroMeter maxPosition);
    }
}
