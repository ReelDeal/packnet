using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.PhysicalMachine;

namespace PackNet.Common.Interfaces.Machines.Fusion
{
    public interface IFusionCommunicator : IMachineCommunicator
    {
        void SetChangeCorrugate(bool value);

        void SetAlignments();

        void SetCorrugateThicknesses(IEnumerable<double> tracks);
    }
}