﻿using System;
using System.Net;

using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.DTO.PhysicalMachine;

namespace PackNet.Common.Interfaces.Machines
{
    /// <summary>
    /// Packsize Cut Crease Machine
    /// </summary>
    /// <remarks>
    /// This interface is used also by Installer Solution on CloudAtlas Repository, if you remove or change the name of any property please verify.
    /// </remarks>
    public interface IPacksizeCutCreaseMachine : ITrackBasedCutCreaseMachine, INetworkConnectedMachine, IHumanOperatedMachine
    {
        void UpdatePhysicalMachineSettings(IPEndPoint eventNotifierIp);

        void UpdateFrom(IPacksizeCutCreaseMachine copyFrom);

        /// <summary>
        /// Gets or sets the physical settings file path.
        /// </summary>
        /// <value>
        /// The physical settings file path.
        /// </value>
        string PhysicalSettingsFilePath { get; set; }

        /// <summary>
        /// Gets or sets the physical machine settings.
        /// </summary>
        /// <value>
        /// The physical machine settings.
        /// </value>
        [BsonIgnore]
        PhysicalMachineSettings PhysicalMachineSettings { get; set; }
       
        /// <summary>
        /// Gets or sets the version number of the PLC.
        /// This is read during synchronization
        /// </summary>
        /// <value>
        /// The machine PLC version
        /// </value>
        string PlcVersion { get; set; }

        /// <summary>
        /// Observable that is invoked when the plc version string is changed
        /// </summary>
        IObservable<string> PlcVersionChangedObservable { get; }
    }
}
