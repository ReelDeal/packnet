﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Machines
{
    public interface IMachine : IPersistable
    {
        /// <summary>
        /// The type of machine that is represented
        /// </summary>
        MachineTypes MachineType { get; }
        
        /// <summary>
        /// Detailed information regarding the current errors
        /// </summary>
        List<MachineError> Errors { get; set; }

        /// <summary>
        /// Human readable identifier
        /// </summary>
        string Alias { get; set; }

        /// <summary>
        /// Optional data for the user to describe the machine
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Current physical machine status
        /// </summary>
        MachineStatuses CurrentStatus { get; set; }

        /// <summary>
        /// Observable that is invoked when the Machine Status changes
        /// </summary>
        IObservable<MachineStatuses> CurrentStatusChangedObservable { get; }

        /// <summary>
        /// Current machine production status
        /// </summary>
        MachineProductionStatuses CurrentProductionStatus { get; set; }

        /// <summary>
        /// Observable that is invoked when the Machine Production Status changes
        /// </summary>
        IObservable<MachineProductionStatuses> CurrentProductionStatusObservable { get; }

        /// <summary>
        /// Capabilities of the machine. You should never modify this collection unless you are the owner of this machine.
        /// Instead use the CurrentMachineCapabilities property.
        /// </summary>
        IEnumerable<ICapability> CurrentCapabilities { get; }

        /// <summary>
        /// Observable that is invoked when the machine capabilities change
        /// </summary>
        IObservable<IEnumerable<ICapability>> CurrentCapabilitiesChangedObservable { get; }
        
    }
}
