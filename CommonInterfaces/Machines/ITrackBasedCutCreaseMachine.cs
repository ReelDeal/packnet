﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Machines
{
    public interface ITrackBasedCutCreaseMachine : IMachine
    {
        /// <summary>
        /// Number of tracks configured on this machine
        /// </summary>
        int NumTracks { get; set; }

        /// <summary>
        /// Track configuration for machine
        /// </summary>
        ConcurrentList<Track> Tracks { get; set; }

        /// <summary>
        /// Loads a corrugate onto the track
        /// </summary>
        /// <param name="track"></param>
        /// <param name="corrugate"></param>
        void LoadCorrugate(Track track, Corrugate corrugate);
    }
}