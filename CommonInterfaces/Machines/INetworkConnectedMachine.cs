﻿namespace PackNet.Common.Interfaces.Machines
{
    public interface INetworkConnectedMachine : IMachine
    {
        /// <summary>
        /// Network Information used to communicate to the machine
        /// </summary>
        string IpOrDnsName { get; set; }

        /// <summary>
        /// The IP version 4 subnet mask for the machine
        /// </summary>
        string IpV4SubnetMask { get; set; }

        /// <summary>
        /// The IP version 4 default gateway for the machine
        /// </summary>
        string IpV4DefaultGateway { get; set; }

        /// <summary>
        /// The MacAddress for the machine
        /// </summary>
        string MacAddress { get; set; }

        /// <summary>
        /// Port used to connect to the machine
        /// </summary>
        int Port { get; set; }
    }
}
