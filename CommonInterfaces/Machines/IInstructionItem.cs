﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackNet.Common.Interfaces.Machines
{
    public interface IInstructionItem
    {
        short[] ToPlcArray();
    }
}
