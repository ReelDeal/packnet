﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Common.Interfaces.Machines
{
    public interface IMovementInstructionItem : IInstructionItem
    {
        MicroMeter Position { get; set; }
    }
}
