﻿using System;
using System.Security.Cryptography.X509Certificates;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Machines
{
    /// <summary>
    /// Capability represents something that a machine can do. It works directlky with IProducible IRestriction to determine which machine should/can create a IProducible
    /// You should override gethashcode so that the system can dispose of duplicate capabilities in a machine group.
    /// </summary>
    public interface ICapability
    {
        /// <summary>
        /// Used to display to users.
        /// </summary>
        string DisplayValue { get; }

        /// <summary>
        /// The restriction types that this capability can satisfy, think polymorphism.
        /// </summary>
        Type RestrictionType { get; }

        /// <summary>
        /// This is where you decide if your capability can satisfy the specified restriction.
        /// </summary>
        /// <param name="restriction"></param>
        /// <returns></returns>
        bool Evaluate(IRestriction restriction);

        //This is hear and will be automatically satisfied for you but you should override it in your impl.
        int GetHashCode();
    }
}
