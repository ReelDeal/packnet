﻿using System;

namespace PackNet.Common.Interfaces.Machines
{
    using System.Collections.Generic;
    using Enums;

    public class MachineError
    {
        private List<string> arguments; 

        public MachineStatuses Error { get; set; }

        public List<string> Arguments
        {
            get { return arguments; } //.Where(a => a.Equals("0") == false).ToList(); } Add back in when UI can ignore string placeholders
            set { arguments = value; } 
        }

        public MachineError()
        {
        }

        public MachineError(MachineErrorStatuses error, List<string> arguments = null )
        {
            Error = error;
            Arguments = arguments;
        }

        public override string ToString()
        {
            if(Arguments == null)
                Arguments = new List<string>();

            var args = String.Join(", ", Arguments);
            return "Error: " + Error + " Arguments: " + args;
        }
    }
}