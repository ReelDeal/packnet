﻿namespace PackNet.Common.Interfaces.Machines
{
    public interface IHumanOperatedMachine
    {
        //TODO: should this be a User instead of string????
        /// <summary>
        /// Operator currently running the machine
        /// </summary>
        string AssignedOperator { get; set; }
    }
}
