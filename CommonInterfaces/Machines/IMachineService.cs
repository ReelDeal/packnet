﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.Interfaces.Machines
{
    /// <summary>
    /// Machine services must implement this interface to be used by the machine group and roll up machine service
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMachineService<out T> : IService where T : IMachine
    {
        /// <summary>
        /// List of machines that are currently managed by this service
        /// </summary>
        IEnumerable<T> Machines { get; }

        void Produce(Guid machine, IProducible producible);

        IObservable<T> MachineErrorOccuredObservable { get; }

        /// <summary>
        /// Determine if the machine can produce the producible
        /// </summary>
        /// <param name="machineId"></param>
        /// <param name="producible"></param>
        /// <returns></returns>
        bool CanProduce(Guid machineId, IProducible producible);
    }
}
