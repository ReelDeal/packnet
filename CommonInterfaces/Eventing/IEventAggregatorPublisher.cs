﻿using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.Interfaces.Eventing
{
    /// <summary>
    /// Internal event publishing
    /// </summary>
    public interface IEventAggregatorPublisher : IService
    {
        /// <summary>
        /// Publishes event of type T
        /// </summary>
        /// <typeparam name="T">Type of event</typeparam>
        /// <param name="dataObject">DTO</param>
        void Publish<T>(T dataObject);
    }
}
