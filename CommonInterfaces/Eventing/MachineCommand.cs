﻿using System;

namespace PackNet.Common.Interfaces.Eventing
{
    public class MachineCommand
    {
        public Guid MachineId { get; set; }
        public int TrackNumber { get; set; }
    }
}
