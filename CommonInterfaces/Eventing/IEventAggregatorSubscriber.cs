﻿using System.Reactive.Concurrency;

using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.Interfaces.Eventing
{
    using System;

    /// <summary>
    /// Internal event subscribing
    /// </summary>
    public interface IEventAggregatorSubscriber : IService
    {
        /// <summary>
        /// GetEvent to internal events
        /// </summary>
        /// <typeparam name="T">Type of event</typeparam>
        /// <returns>IObservable representing event stream</returns>
        IObservable<T> GetEvent<T>();

        IScheduler Scheduler { get; }
    }
}
