﻿namespace PackNet.Common.Interfaces.Eventing
{
    using System;

    [Obsolete("Use IEventAggregator")]
    public enum PublisherType
    {
        JobDone,
        PackagingDone
    }

    [Obsolete("Use IEventAggregator")]
    public interface IPublisher
    {
        PublisherType Type { get; set; }

        void Publish(object data, DateTime eventDate);
    }
}