﻿using System;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.Interfaces.Eventing
{
    /// <summary>
    /// Allows plug-ins to dispatch and respond to User Interface events
    /// </summary>
    public interface IUICommunicationService : IService
    {
        /// <summary>
        /// Observable stream of Tuple representing the MessageType of the payload and the serialized json payload of the event coming from the user interface
        /// </summary>
        IObservable<Tuple<MessageTypes, string>> UIEventObservable { get; }

        /// <summary>
        /// Gets the json serializer settings.
        /// </summary>
        /// <value>
        /// The json serializer settings.
        /// </value>
        JsonSerializerSettings JsonSerializerSettings { get; }

        /// <summary>
        /// Send a message to the user interface
        /// </summary>
        /// <param name="message">Message to send to user interface</param>
        /// <param name="publishInternally"></param>
        void SendMessageToUI(IMessage message, bool publishInternally = false);

        /// <summary>
        /// Send a message to the user interface
        /// </summary>
        /// <typeparam name="T">Type of payload serialized in the message</typeparam>
        /// <param name="message">Message to send to user interface</param>
        /// <param name="publishInternally">if set to <c>true</c> the message is publish internally.</param>
        /// <param name="compress">if set to <c>true</c> the message is compressed, use false only in development..</param>
        void SendMessageToUI<T>(IMessage<T> message, bool publishInternally = false, bool compress = true);

        /// <summary>
        /// Send a message to the user interface
        /// </summary>
        /// <typeparam name="T">Type of payload serialized in the message</typeparam>
        /// <param name="message">Message to send to user interface</param>
        /// <param name="throttleTime">The throttle time.</param>
        void SendThrottledMessageToUI<T>(IMessage<T> message, TimeSpan throttleTime);


        /// <summary>
        /// Registers the message type and T with the message listener, every time a message is seen with the given message type... the payload will be 
        /// de-serialized into T and published on the internal event aggregator to be consumed by a service 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="messageType"></param>
        /// <returns></returns>
        IDisposable RegisterUIEventWithInternalEventAggregator<T>(MessageTypes messageType);

        /// <summary>
        /// Registers a message type with the message listener, every time a message is seen with the given message type 
        /// the message will be published on the internal event aggregator to be consumed by a service 
        /// </summary>
        /// <param name="messageType"></param>
        /// <returns></returns>
        IDisposable RegisterUIEventWithInternalEventAggregator(MessageTypes messageType);

        /// <summary>
        /// Adds a converter to the serializer settings
        /// </summary>
        /// <param name="converter"></param>
        void AddJsonConverter(JsonConverter converter);
    }
}
