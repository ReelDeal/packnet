﻿namespace PackNet.Common.Interfaces.ExtensionMethods
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Resources;

    public static class AssemblyExtensions
    {
        public static string GetResourceString(this Assembly assembly, string resourceKey, string nameSpaceHint = null)
        {
            var stringFromResource = GetStringFromResource(assembly, resourceKey, nameSpaceHint);
            if (!String.IsNullOrEmpty(stringFromResource))
                return stringFromResource;

            nameSpaceHint = string.Format("{0}.Properties.Resources", assembly.GetName().Name);
            stringFromResource = GetStringFromResource(assembly, resourceKey, nameSpaceHint);
            if (!String.IsNullOrEmpty(stringFromResource))
                return stringFromResource;

            //look in embedded resources
            var manifestResourceNames = assembly.GetManifestResourceNames().Select(n => n.ToLower().ReplaceLast(".resources", ""));
            return manifestResourceNames
                .Select(n => assembly.GetStringFromResource(resourceKey, n))
                .FirstOrDefault(n => !String.IsNullOrEmpty(n));
        }

        public static string GetStringFromResource(this Assembly assembly, string resourceName, string resourceNameSpace)
        {
            if (String.IsNullOrEmpty(resourceNameSpace))
                return String.Empty;

            try
            {
                var rm = new ResourceManager(resourceNameSpace, assembly);
                return rm.GetString(resourceName);
            }
            catch (MissingManifestResourceException)
            { } //dont care
            return String.Empty;
        }
    }
}
