﻿using System;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Interfaces.ExtensionMethods
{
    public static class MessageTypesExtensions
    {
        /// <summary>
        /// Subscribes to an event based on the message type and Payload of T
        /// </summary>
        /// <typeparam name="T">Message payload</typeparam>
        /// <param name="messageType">Type of message to filter by</param>
        /// <param name="subscriber">event subscriber</param>
        /// <param name="logger">for logging errors</param>
        /// <param name="onNext">Action to execute on event</param>
        /// <returns></returns>
        public static IDisposable OnMessage<T>(this MessageTypes messageType, IEventAggregatorSubscriber subscriber, ILogger logger, Action<IMessage<T>> onNext)
        {
            return subscriber.GetEvent<IMessage<T>>()
                .Where(m=> m.MessageType == messageType)
                .DurableSubscribe(onNext, logger);
        }

        /// <summary>
        /// Subscribes to an event based on the message type
        /// </summary>
        /// <param name="messageType">Type of message to filter by</param>
        /// <param name="subscriber">event subscriber</param>
        /// <param name="logger">for logging errors</param>
        /// <param name="onNext">Action to execute on event</param>
        /// <returns></returns>
        public static IDisposable OnMessage(this MessageTypes messageType, IEventAggregatorSubscriber subscriber, ILogger logger, Action<IMessage> onNext)
        {
            return subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == messageType)
                .DurableSubscribe(onNext, logger);
        }
    }
}
