﻿using System;

namespace PackNet.Common.Interfaces.ExtensionMethods
{
    using System.IO;
    using System.Text;

    public static class StringExtensions
    {
        public static string ReplaceLast(this string source, string replaceThis, string withThis)
        {
            var location = source.LastIndexOf(replaceThis);
            if (location < 0)
                return source;
            var result = source.Remove(location, replaceThis.Length).Insert(location, withThis);
            return result;
        }

        /// <summary>
        /// Converts a string to a byte array.
        /// </summary>
        /// <param name="source">The string to convert</param>
        /// <param name="encoding">If specified, the encoding will be used.  Otherwise UTF8 will be used</param>
        /// <returns>Supplied string as byte array</returns>
        public static byte[] ToBytes(this string source, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;

            return encoding.GetBytes(source);
        }

        public static Stream ToStream(this string source)
        {
            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream);

            streamWriter.Write(source);
            streamWriter.Flush();

            memoryStream.Position = 0L;
            return memoryStream;
        }

        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }
    }
}
