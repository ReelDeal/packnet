﻿namespace PackNet.Common.Interfaces.ExtensionMethods
{
    using System;
    using System.Linq;
    using System.Resources;

    using Utils.LocalizationUtils;

    public static class EnumExtensions
    {
        private static string GetEnumLocalizedString(this Enum enumValue, ResourceManager resourceManager = null, string nameSpaceHint = null, string resourceKey = null)
        {
            var enumType = enumValue.GetType();
            if(String.IsNullOrEmpty(resourceKey))
                resourceKey = string.Format("{0}_{1}", enumType.Name, enumValue);

            if (resourceManager != null)
                return resourceManager.GetString(resourceKey);

            return enumType.Assembly.GetResourceString(resourceKey, nameSpaceHint);
        }

        public static string GetLocalizedString(this Enum enumValue, ResourceManager resourceManager = null)
        {
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());
            if (fieldInfo == null)
                return null;


            var attributes = fieldInfo.GetCustomAttributes(typeof(LocalizableAttribute), false) as LocalizableAttribute[];
            if (attributes == null)
                return null;

            if (attributes.Length == 0)
                return enumValue.ToString();

            var resourceString = enumValue.GetEnumLocalizedString(resourceManager, attributes.First().ResourceNameHint, attributes.First().ResourceKeyName);
            if(!String.IsNullOrEmpty(resourceString))
                return resourceString;

            //Check for default value
            if(!String.IsNullOrEmpty(attributes.First().DefaultValue))
                return attributes.First().DefaultValue;

            return enumValue.ToString();
        }
    }
}
