﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackNet.Common.Interfaces.ExtensionMethods
{
    public static class TypeExtensions
    {
        public static bool IsTypeOrSubclassOf(this Type potentialDescendant, Type potentialBase)
        {
            
            return potentialBase.IsAssignableFrom(potentialDescendant) || potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase || (potentialDescendant.BaseType != null && potentialDescendant.BaseType.IsTypeOrSubclassOf(potentialBase));
        }

        public static bool IsTypeOrDerivedFrom(this Type potentialBase, Type potentialDescendant)
        {
            return potentialBase.IsAssignableFrom(potentialDescendant) || potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase || (potentialDescendant.BaseType != null && potentialDescendant.BaseType.IsTypeOrSubclassOf(potentialBase));
        }

        public static bool IsTypeOrSubclassOf(this Type potentialDescendant, object potentialBase)
        {
            var typed = potentialBase.GetType();
            return potentialDescendant.IsTypeOrSubclassOf(typed);
        }

        public static bool IsTypeOrSubclassOf(this object potentialDescendant, Type potentialBase)
        {
            var typed = potentialDescendant.GetType();
            return typed.IsTypeOrSubclassOf(potentialBase);
        }

        public static bool IsTypeOrSubclassOf(this object potentialDescendant, object potentialBase)
        {
            var typed1 = potentialDescendant.GetType();
            var typed2 = potentialBase.GetType();
            return typed1.IsTypeOrSubclassOf(typed2);
        }
    }
}
