﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.ExtensionMethods
{
    public static class ProducibleExtensions
    {
        public static IEnumerable<IProducible> GetByStatus(this IEnumerable<IProducible> source, ProducibleStatuses status)
        {
            return source != null ? source.Where(p => p.ProducibleStatus == status).ToList() : new List<IProducible>();
        }

        public static bool DoStatusTransition(this IProducible source, ProducibleStatuses neededStatus, ProducibleStatuses endStatus)
        {
            lock (source)
            {
                if (source.ProducibleStatus != neededStatus)
                {
                    return false;
                }

                source.ProducibleStatus = endStatus;

                return true;
            }
        }


        public static ICarton FindFirstICarton(this IProducible producible)
        {
            if (producible is ICarton)
                return producible as Carton;

            if (producible is IProducibleWrapper)
                return ((IProducibleWrapper)producible).Producible.FindFirstICarton();

            if (producible is Kit)
                return ((Kit)producible).ItemsToProduce.Select(p => p.FindFirstICarton()).FirstOrDefault(carton => carton != null);
            return null;
        }


        public static IEnumerable<ICarton> FindAllICarton(this IProducible producible)
        {
            var list = new List<ICarton>();
             FindAllICarton(producible, list);
            return list;
        }

        public static void FindAllICarton(this IProducible producible, List<ICarton> found)
        {
            if (producible is ICarton)
                found.Add( producible as ICarton);

            if (producible is IProducibleWrapper)
                ((IProducibleWrapper)producible).Producible.FindAllICarton(found);

            if (producible is Kit)
                foreach (var p in ((Kit)producible).ItemsToProduce)
                {
                    p.FindAllICarton(found);
                }

        }

        public static void RecuseAndSetPropertiesAsNew(this IProducible producible)
        {
            producible.Created = DateTime.UtcNow;
            producible.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported;
            // see if we need to recuse
            if (producible is IProducibleWrapper)
                ((IProducibleWrapper)producible).Producible.RecuseAndSetPropertiesAsNew();
            else if (producible is Kit)
                ((Kit)producible).ItemsToProduce.ForEach(p => p.RecuseAndSetPropertiesAsNew());

        }
    }
}
