﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace PackNet.Common.Interfaces.ExtensionMethods
{
    public static class OrientationExtensions
    {
        public static bool IsRotated(this OrientationEnum source)
        {
            return source == OrientationEnum.Degree90 ||
                       source == OrientationEnum.Degree90_flip ||
                       source == OrientationEnum.Degree270 ||
                       source == OrientationEnum.Degree270_flip;
        }
    }
}
