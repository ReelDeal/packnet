﻿using System;
using System.Diagnostics;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Interfaces.ExtensionMethods
{
    public static class ObservableExtensions
    {
        /// <summary>
        /// Subscribes to an observable but does not call onError when an exception occurs
        /// </summary>
        public static IDisposable DurableSubscribe<T>(this IObservable<T> source, Action<T> onNext, ILogger logger)
        {
            return source.Subscribe
            (a =>
                {
                    try
                    {
                        onNext(a);
                    }
                    catch (Exception e)
                    {
                        Trace.WriteLine("Error in durable subscription: " + e);
                        logger.LogException(LogLevel.Error, "Error in durable subscription. {0}", e, a.GetType());
                    }
                }
            );
        }

        public static IDisposable DurableSubscribe<T>(this IObservable<T> source, Action<T> onNext)
        {
            return source.Subscribe
             (a =>
             {
                 try
                 {
                     onNext(a);
                 }
                 catch (Exception e)
                 {
                     Trace.WriteLine("Error in durable subscription: " + e);
                 }
             }
             );
        }

        /// <summary>
        /// User care when using this subscribe it will create a new thread and reserve ~1MB of memory for the stack.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="onNext"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static IDisposable DurableSubscribeOnTaskPool<T>(this IObservable<T> source, Action<T> onNext, ILogger logger)
        {
            return source.ObserveOn(TaskPoolScheduler.Default).Subscribe
            (a =>
            {
                try
                {
                    onNext(a);
                }
                catch (Exception e)
                {
                    Trace.WriteLine("Error in durable subscription: " + e);
                    logger.LogException(LogLevel.Error, "Error in durable subscription. {0}", e, a.GetType());
                }
            }
            );
        }

        public static IDisposable DurableSubscribeOnTaskPool<T>(this IObservable<T> source, Action<T> onNext)
        {
            return source.ObserveOn(TaskPoolScheduler.Default).Subscribe
             (a =>
             {
                 try
                 {
                     onNext(a);
                 }
                 catch (Exception e)
                 {
                     Trace.WriteLine("Error in durable subscription: " + e);
                 }
             }
             );
        }

    }
}
