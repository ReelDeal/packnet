﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace PackNet.Common.Interfaces.DTO.Settings
{
    [DataContract(Name = "SelectorSettings", Namespace = "http://Packsize.MachineManager.com")]
    public class SelectorSettings : ISelectorSettings
    {
        public SelectorSettings()
        {
        }

        [Obsolete("Why encapsulate a dto?")]
        public SelectorSettings(int maxNumberOfTiles, bool alignTilesInEnd, double nunaTabLength, double nunaTabOffset, double timeLimit)
        {
            AlignTilesInEnd = alignTilesInEnd;
            MaxNumberOfTiles = maxNumberOfTiles;
            NunaTabLength = nunaTabLength;
            NunaTabOffset = nunaTabOffset;
            TimeLimit = timeLimit;
        }

        [DataMember(Name = "AlignTilesInEnd")]
        public bool AlignTilesInEnd { get; set; }

        [DataMember(Name = "MaxNumberOfTiles")]
        public int MaxNumberOfTiles { get; set; }

        [DataMember(Name = "NunaTabLength")]
        public double NunaTabLength { get; set; }

        [DataMember(Name = "NunaTabOffset")]
        public double NunaTabOffset { get; set; }

        [DataMember(Name = "TimeLimit")]
        [Description("Number of milliseconds that the carton selection algoritm is allowed to use. When the interval has elapsed, algoritm returns the best match that far.\n\n 0 = disable the time limit")]
        public double TimeLimit { get; set; }
    }
}
