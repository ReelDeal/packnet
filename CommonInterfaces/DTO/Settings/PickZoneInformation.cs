﻿using System.Runtime.Serialization;

namespace PackNet.Common.Interfaces.DTO.Settings
{
    [DataContract(Name = "PickZone", Namespace = "http://Packsize.MachineManager.com")]
    public class PickZoneInformation
    {
        [DataMember(Name = "Id")]
        public virtual string Id { get; set; }
    }
}
