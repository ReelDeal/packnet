﻿    using System;
    using System.Runtime.Serialization;
    using PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.Common.Interfaces.DTO.Settings
{
    [Serializable]
    [KnownType(typeof(PacksizeSuiteWmsZebraPrinterInformation))]
    [DataContract(Name = "Printer", Namespace = "http://Packsize.MachineManager.com")]
    public class PrinterInformation
    {
        [DataMember(Name = "Id")]
        public virtual Guid Id { get; set; }

        [DataMember(Name = "Alias")]
        public virtual string Alias { get; set; }

        [DataMember(Name = "SerialPort")]
        public virtual string SerialPort { get; set; }

        [DataMember(Name = "IsPriorityPrinter")]
        public virtual bool IsPriorityPrinter { get; set; }

        [DataMember(Name = "IpOrDnsNameAndPort")]
        public virtual string IpOrDnsNameAndPort { get; set; }
    }


}
