﻿using System;
using System.Net;
using System.Runtime.Serialization;

namespace PackNet.Common.Interfaces.DTO.Settings
{
    [DataContract(Namespace = "http://Packsize.MachineManager.com")]
    public class ZebraPrinterSettings : IPrinterSettings
    {
        private int? labelSecondCheckWaitTimeMilliseconds;
        private int retryCount = 50;
        private int pollingIntervalMilliseconds = 100;

        public const int DefaultLabelSecondCheckWaitTimeMilliseconds = 100;

        [DataMember]
        public string PrinterName { get; set; }
        public Guid Id { get; set; }

        public IPEndPoint Address { get; set; }

        [DataMember]
        public int PollingIntervalMilliseconds
        {
            get { return pollingIntervalMilliseconds; }
            set { pollingIntervalMilliseconds = value; }
        }

        [DataMember]
        public int RetryCount
        {
            get { return retryCount; }
            set { retryCount = value; }
        }

        /// <summary>
        /// This value is used to validate a label has been peeled off. We double check because we may poll the printer too soon after
        /// the queue is empty but before the label peeloff flag is set
        /// </summary>
        [DataMember]
        public int LabelSecondCheckWaitTimeMilliseconds
        {
            get
            {
                return labelSecondCheckWaitTimeMilliseconds.HasValue ? labelSecondCheckWaitTimeMilliseconds.Value : DefaultLabelSecondCheckWaitTimeMilliseconds;
            }
            set
            {
                labelSecondCheckWaitTimeMilliseconds = value;
            }
        }
    }
}