﻿using System;
using System.Net;

using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.Settings
{
	/// <summary>
	/// Settings that are global to PackNet.Server
	/// </summary>
	public class PackNetServerSettings : IPersistable
	{
		public Guid Id { get; set; }

	    public IPAddress MachineCallbackIpAddress { get; set; }
		public int MachineCallbackPort { get; set; }
		public int LoginExpiresSeconds { get; set; }
		public FileImportSettings FileImportSettings { get; set; }
        public WorkflowTrackingLevel WorkflowTrackingLevel { get; set; } 
	}
}