﻿namespace PackNet.Common.Interfaces.DTO.Settings
{
    public interface ISelectorSettings
    {
        bool AlignTilesInEnd { get; }

        int MaxNumberOfTiles { get; }

        double NunaTabLength { get; }

        double NunaTabOffset { get; }

        double TimeLimit { get; }
    }
}
