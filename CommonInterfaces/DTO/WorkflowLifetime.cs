﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO
{
    public class WorkflowLifetime : IPersistable
    {
        public WorkflowLifetime()
        {
            CreatedTime = DateTime.UtcNow;
        }
        private Subject<WorkflowLifetime> lifeTimeSubject = new Subject<WorkflowLifetime>();
        private DateTime createdTime;
        public Guid Id { get; set; }
        public WorkflowTypes Type { get; set; }
        public string WorkflowPath { get; set; }

        [BsonElement("expiry")]
        public DateTime Expiry { get; set; }

        public IObservable<WorkflowLifetime> WorkflowLifeTimeChangedObservable
        {
            get
            {
                return lifeTimeSubject.AsObservable()
                    .ObserveOn(System.Reactive.Concurrency.Scheduler.ThreadPool);
            }
        }

        public void StartTracking()
        {
            Started = DateTime.UtcNow;
            lifeTimeSubject.OnNext(this);
        }

        public void StopTracking()
        {
            Ended = DateTime.UtcNow;
            TotalTimeToRun = Ended - Started;
            lifeTimeSubject.OnCompleted();
        }

        public DateTime CreatedTime
        {
            get { return createdTime; }
            set { createdTime = Expiry = value; }
        }
        
        public DateTime? Started { get; set; }
        public DateTime? Ended { get; set; }
        public TimeSpan? TotalTimeToRun { get; set; }
        public Dictionary<string, object> AdditionalInformation { get; set; }
    }
}
