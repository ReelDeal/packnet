﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.Scheduler
{
    public class SchedulerJob : IPersistable
    {
        public Guid Id { get; set; }
        public ScheduledJobTypes ScheduledJobType { get; set; }
    }
}
