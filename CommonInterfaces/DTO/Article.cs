﻿using System.Activities.Statements;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.DTO
{
    using System;

    using Newtonsoft.Json;

    using Converters;
    using Repositories;

    //TODO: Make obsolete
    //[Obsolete("Use Article<T>")]
    public class Article : IPersistable
    {
        public Article()
        {
            XValues = new Dictionary<string, double>();
        }

        public DateTime LastUpdated { get; set; }
        
        public string UserName { get; set; }
        
        //public List<int> ProducibleMachines { get; set; }
        
        public string ArticleId { get; set; }
        
        public string Description { get; set; }
        
        public double Height { get; set; }

        [JsonConverter(typeof(StringToNullableInt))]
        public int? CorrugateQuality { get; set; }
        
        public double Length { get; set; }
        
        public Guid Id { get; set; }

        public double Width { get; set; }
        
        public int DesignId { get; set; }

        public string DesignName { get; set; }

        public bool MissingDesignFile { get; set; }

        public IDictionary<string, double> XValues { get; set; }
    }
}
