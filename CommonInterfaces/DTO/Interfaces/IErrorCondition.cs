﻿using System;

namespace PackNet.Common.Interfaces.DTO.Interfaces
{
    public interface IErrorCondition
    {
        DateTime TimeOccuredUtc { get; }
        string Message { get; set; }
    }
}