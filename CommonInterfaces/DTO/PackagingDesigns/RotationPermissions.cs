﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    using System;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    [JsonConverter(typeof(StringEnumConverter))]
    public enum OrientationEnum
    {
        [EnumMember(Value = "Degree0")]
        Degree0 = 0,
        [EnumMember(Value = "Degree0Flip")]
        Degree0_flip = 1,
        [EnumMember(Value = "Degree90")]
        Degree90 = 2,
        [EnumMember(Value = "Degree90Flip")]
        Degree90_flip = 3,
        [EnumMember(Value = "Degree180")]
        Degree180 = 4,
        [EnumMember(Value = "Degree180Flip")]
        Degree180_flip = 5,
        [EnumMember(Value = "Degree270")]
        Degree270 = 6,
        [EnumMember(Value = "Degree270Flip")]
        Degree270_flip = 7
    }

    public enum OrientationPermissionEnum
    {
        both = 3,
        normal = 1,
        flip = 2,
        none = 0
    }

    [Serializable]
    [DataContract]
    public class RotationPermissions
    {
        /// <summary>
        /// Returns true if 0 degrees rotation is allowed.
        /// </summary>
        [XmlElement("Degree0")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree0 { get; set; }

        /// <summary>
        /// Returns the orientation permission for 0 degree rotation
        /// </summary>
        [XmlElement("degree0")]
        [DataMember]
        public OrientationPermissionEnum degree0
        {
            get
            {
                return new OrientationPermissionEnum();
            }
            set
            {
                SetPropertiesFromPermission(value, (v) => Degree0 = v, (v) => Degree0Flip = v);
            }
        }

        /// <summary>
        /// Returns the orientation permission for 0 degree rotation
        /// </summary>
        [XmlElement("degree270")]
        [DataMember]
        public OrientationPermissionEnum degree270
        {
            get
            {
                return new OrientationPermissionEnum();
            }
            set
            {
                SetPropertiesFromPermission(value, (v) => Degree270 = v, (v) => Degree270Flip = v);
            }
        }

        /// <summary>
        /// Returns the orientation permission for 0 degree rotation
        /// </summary>
        [XmlElement("degree90")]
        [DataMember]
        public OrientationPermissionEnum degree90
        {
            get
            {
                return new OrientationPermissionEnum();
            }
            set
            {
                SetPropertiesFromPermission(value, (v) => Degree90 = v, (v) => Degree90Flip = v);
            }
        }

        /// <summary>
        /// Returns the orientation permission for 0 degree rotation
        /// </summary>
        [XmlElement("degree180")]
        [DataMember]
        public OrientationPermissionEnum degree180
        {
            get
            {
                return new OrientationPermissionEnum();
            }
            set
            {
                SetPropertiesFromPermission(value, (v) => Degree180 = v, (v) => Degree180Flip = v);
            }
        }

        /// <summary>
        /// Returns true if 0 degrees flipped rotation is allowed.
        /// </summary>
        [XmlElement("Degree0Flip")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree0Flip { get; set; }

        /// <summary>
        /// Returns true if 90 degrees rotation is allowed.
        /// </summary>
        [XmlElement("Degree90")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree90 { get; set; }

        /// <summary>
        /// Returns true if 90 degrees rotation is allowed.
        /// </summary>
        [XmlElement("Degree90Flip")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree90Flip { get; set; }

        /// <summary>
        /// Returns true if 180 degrees rotation is allowed.
        /// </summary>
        [XmlElement("Degree180")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree180 { get; set; }

        /// <summary>
        /// Returns true if 180 degrees rotation is allowed.
        /// </summary>
        [XmlElement("Degree180Flip")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree180Flip { get; set; }

        /// <summary>
        /// Returns true if 270 degrees rotation is allowed.
        /// </summary>
        [XmlElement("Degree270")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree270 { get; set; }

        /// <summary>
        /// Returns true if 270 degrees rotation is allowed.
        /// </summary>
        [XmlElement("Degree270Flip")]
        [DataMember]
        public ConditionalBooleanDesignElement Degree270Flip { get; set; }

        private static void SetPropertiesFromPermission(OrientationPermissionEnum value, Action<ConditionalBooleanDesignElement> prop, Action<ConditionalBooleanDesignElement> flipProp)
        {
            switch (value)
            {
                case OrientationPermissionEnum.both:
                    prop(new ConditionalBooleanDesignElement { Allowed = true });
                    flipProp(new ConditionalBooleanDesignElement { Allowed = true });
                    break;
                case OrientationPermissionEnum.normal:
                    prop(new ConditionalBooleanDesignElement { Allowed = true });
                    flipProp(new ConditionalBooleanDesignElement { Allowed = false });
                    break;
                case OrientationPermissionEnum.flip:
                    prop(new ConditionalBooleanDesignElement { Allowed = false });
                    flipProp(new ConditionalBooleanDesignElement { Allowed = true });
                    break;
                default:
                    prop(new ConditionalBooleanDesignElement { Allowed = false });
                    flipProp(new ConditionalBooleanDesignElement { Allowed = false });
                    break;
            }
        }
    }
}
