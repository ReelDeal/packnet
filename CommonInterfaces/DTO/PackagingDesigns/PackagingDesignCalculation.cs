﻿using System;

namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    [Serializable]
    public class PackagingDesignCalculation
    {
        /// <summary>
        /// Length of a single packaging on this corrugate
        /// </summary>
        public MicroMeter Length { get; set; }

        /// <summary>
        /// Width of a single packaging on this corrugate
        /// </summary>
        public MicroMeter Width { get; set; }

        /// <summary>
        /// Area of a single packaging on this corrugate
        /// </summary>
        public double Area
        {
            get { return (double)Length * (double)Width; }
        }

        public static PackagingDesignCalculation MaxValue
        {
            get
            {
                return new PackagingDesignCalculation
                {
                    Length = MicroMeter.MaxValue,
                    Width = MicroMeter.MaxValue,
                };
            }
        }

        public PackagingDesignCalculation Clone()
        {
            return new PackagingDesignCalculation
            {
                Length = Length,
                Width = Width,
            };
        }
    }
}
