﻿
namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    public struct PackagingDesignIdToFileNameMapping
    {
        public string FileName;
        public int Id;

        /// <summary>
        /// Constructor taking filename and design ID.
        /// </summary>
        /// <param name="filename">Full path to the filename of the XML design file containing the Design object.</param>
        /// <param name="id">The ID number of the Design object.</param>
        public PackagingDesignIdToFileNameMapping(string filename, int id)
        {
            FileName = filename;
            Id = id;
        }

        public override string ToString()
        {
            return Id + " -> " + FileName;
        }
    }
}
