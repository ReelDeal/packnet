﻿using System.Collections.Generic;

namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{        
    public class DesignFormulaParameters
    {
        public DesignFormulaParameters()
        {
            XValues = new Dictionary<string, double>();
        }

        public double ZfoldWidth;
        public double ZfoldThickness;
        public double CartonLength;
        public double CartonWidth;
        public double CartonHeight;
        public IDictionary<string, double> XValues { get; private set; }
    }
}
