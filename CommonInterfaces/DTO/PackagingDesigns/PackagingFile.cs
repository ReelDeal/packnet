﻿namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    using System.IO;
    using System.Xml.Serialization;

    /// <summary>
    /// This is the root of a Packaging Design XML file. If can hold 0..n number of Design objects.
    /// </summary>
    [XmlRoot("packaging")]
    public class PackagingFile
    {

        /// <summary>
        /// Default constructor
        /// </summary>
        public PackagingFile() { }

        /// <summary>
        /// The Design object array.
        /// </summary>
        [XmlElement("design")]
        public PackagingDesign[] Designs;

        public static PackagingFile Deserialize(string filename)
        {
            PackagingFile packaging;
            FileStream fs = null;
            try
            {
                var serializer = new XmlSerializer(typeof(PackagingFile));
                fs = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                packaging = (PackagingFile)serializer.Deserialize(fs);
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
                fs = null;
            }

            return packaging;
        }
    }
}


