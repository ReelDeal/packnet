﻿using System.ComponentModel;

namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;
    
    [XmlRoot("design")]
    [Serializable]
    [DataContract]
    public class PackagingDesign : IPackagingDesign
    {        
        public PackagingDesign()
        {
            Lines = new List<Line>();
            MaximumWidthOnZFold = "";
        }

        /// <summary>
        /// The design ID is used as an int reference to a packaging design used in the databases to reduce database size. The alternative, to use a string reference, would be more flexible to the user, but would require a much larger database (which would particularly affect the history database). A third alternative would be to have a separate database table with the available packaging design names and reference the records there, but that would require a loosly connected table because packaging files might get added/removed while running the program.
        /// </summary>
        [XmlAttribute("id")]
        [DataMember]
        public int Id { get; set; }

        // make exstension
        public IDictionary<string, object> PopulateParameters(DesignFormulaParameters formulaParameters)
        {
            var dic = new Dictionary<string, object>
                {
                    { "L", formulaParameters.CartonLength },
                    { "W", formulaParameters.CartonWidth },
                    { "H", formulaParameters.CartonHeight },
                    { "FT", formulaParameters.ZfoldThickness },
                    { "FW", formulaParameters.ZfoldWidth }
                };

            foreach (var xval in formulaParameters.XValues)
            {
                dic[xval.Key] = xval.Value;                
            }

            return dic;
        }

        public bool IsElasticLineDesign
        {
            get
            {
                return this.Lines.OfType<ElasticLine>().Any();
            }
        }

        /// <summary>
        /// The name of the packaging. If no name tag was provided in the XML file, the base file name is used.
        /// </summary>
        [XmlElement("name")]
        [DataMember]
        public string Name { get; set; }
        
        /// <summary>
        /// Returns the length of the design. Even though this property can be calculated if the design has physical values applied to the parameter formulas, 
        /// this property is required in the XML specification to allow rotations in the graphical designer.
        /// </summary>
        [XmlElement("length")]
        [DataMember]
        public string LengthOnZFold { get; set; }

        /// <summary>
        /// Returns the width of the design. Even though this property can be calculated when the design has physical values applied to the parameter formulas, 
        /// this property is required in the XML specification to allow rotations in the graphical designer.
        /// </summary>
        [XmlElement("width")]
        [DataMember]
        public string WidthOnZFold { get; set; }

        /// <summary>
        /// Returns the maximum width of the design. Only used together with rubber lines
        /// If no elastic/rubber line this value will be WidthOnZFold
        /// </summary>
        [XmlElement("maximumwidth")]
        [DataMember]
        public string MaximumWidthOnZFold { get; set; }

        /// <summary>
        /// A List of all parameters (L, W, X1-Xn) used in the design
        /// </summary>        
        [XmlArrayItem("parameter", typeof(DesignParameter))]
        [XmlArray("parameters")]
        [DataMember]        
        public List<DesignParameter> DesignParameters { get; set; }

        /// <summary>
        /// All lines in the design.
        /// </summary>
        [XmlArrayItem("line", typeof(Line)), XmlArrayItem("RubberLine", typeof(ElasticLine))]
        [XmlArray("lines")]
        [DataMember]
        public List<Line> Lines { get; set; }

        /// <summary>
        /// The allowed orientations for this Design. 
        /// </summary>
        [XmlElement("rotate")]
        [DataMember]
        public RotationPermissions AllowedRotations { get; set; }

        public object Clone()
        {
            var clone = new PackagingDesign
                        {
                            Id = this.Id,
                            Name = this.Name,
                            LengthOnZFold = this.LengthOnZFold,
                            WidthOnZFold = this.WidthOnZFold,
                            MaximumWidthOnZFold = this.MaximumWidthOnZFold,
                        };

            clone.DesignParameters = new List<DesignParameter>();
            if(this.DesignParameters != null)
                clone.DesignParameters.AddRange(this.DesignParameters.Select(p => new DesignParameter { Alias = p.Alias, Name = p.Name }));

            Lines.OfType<ElasticLine>().ForEach(l => clone.Lines.Add(l.Clone() as ElasticLine));
            Lines.Except(this.Lines.OfType<ElasticLine>()).ForEach(l => clone.Lines.Add(l.Clone() as Line));

            clone.AllowedRotations = AllowedRotations;

            return clone;
        }
    }
    
    [Serializable]
    [DataContract]
    public class DesignParameter
    {        
        [XmlAttribute("name")]
        [DataMember]
        public string Name;

        [DefaultValue("")]
        [XmlAttribute("alias")]
        [DataMember]
        public string Alias;
    }
}

