﻿namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    public interface IPackagingDesign : ICloneable
    {
        [DataMember]
        int Id { get; set; }

        [DataMember]
        string Name { get; set; }

        [DataMember]
        string LengthOnZFold { get; set; }

        [DataMember]
        string WidthOnZFold { get; set; }

        [DataMember]
        string MaximumWidthOnZFold { get; set; }

        [DataMember]
        List<DesignParameter> DesignParameters { get; set; }

        [DataMember]
        List<Line> Lines { get; set; }

        [DataMember]
        RotationPermissions AllowedRotations { get; set; }

        bool IsElasticLineDesign { get; }

        IDictionary<string, object> PopulateParameters(DesignFormulaParameters formulaParameters);
    }
}