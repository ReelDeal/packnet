﻿namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    using System;
    using System.Runtime.Serialization;
    using System.Xml.Serialization;

    [XmlRoot("designEvaluation")]
    [Serializable]
    [DataContract]
    public class DesignEvaluation
    {
        [XmlAttribute("designId")]
        [DataMember]
        public int Id { get; set; }
       
        [XmlAttribute("condition")]
        [DataMember]
        public string Condition { get; set; }
    }
}
