﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    /// <summary>
    /// The LineClass determines who created it (the original XML or the CodeGenerator)
    /// </summary>
    public enum LineClass
    {
        /// <summary>
        /// Normal line, loaded from the Design XML file.
        /// </summary>
        Normal,
        /// <summary>
        /// Cut-off line, added by Design.addCutOffLines() method.
        /// </summary>
        CutOff,
        /// <summary>
        /// Part of previously splitted line, added by Design.createSections() method.
        /// </summary>
        SplittedPart,
        /// <summary>
        /// Part of splitted perforation line, added by Design.convertPerforationLines() method.
        /// </summary>
        PerforationPart,
        /// <summary>
        /// This line is a waste cutting line used to override regular cut compensation.
        /// </summary>
        WasteCut
    }

    /// <summary>
    ///  Used only by the XML serializer to provide a nicer markup than vertical="false"/vertical="true" 
    /// </summary>
    public enum LineDirection
    {
        vertical,
        horizontal
    }

    /// <summary>
    /// A line in a packaging design.
    /// </summary>
    [Serializable]
    [DataContract]
    public class Line : ICloneable
    {
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Line()
        {
        }

        public Line(int startX, int startY, int endX, int endY)
        {
            StartX = startX.ToString();
            StartY = startY.ToString();
            EndX = endX.ToString();
            EndY = endY.ToString();
        }

        /// <summary>
        /// The basic type of the line, e.g. Cut, Crease, etc.
        /// </summary>
        [XmlAttribute("type")]
        [DataMember]
        public LineType Type { get; set; }

        /// <summary>
        /// The ID of a line is only used for debugging purposes. It is set by the id="" property of a line in the XMl file.
        /// The id string for the line.
        /// </summary>
        /// <remarks>It is currently not used and was only needed before for debugging purposes. 
        /// However, it is still kept because it might be requested in future debugging.
        /// If it's no longer required, it can safely be removed (by first removing m_id,
        /// then compile and fix all subsequent compilation errors).</remarks>
        [XmlAttribute("id")]
        [DefaultValue("")] // Don't include property in XML serialization if empty
        [DataMember]
        public string ID { get; set; }

        /// <summary>
        /// The line label, which is drawn on screen as text in the center of the line. Usually empty or containing the parametric value of the Line.Length, e.g. "H".
        /// </summary>
        [XmlText]
        [DataMember]
        public string Label { get; set; }

        /// <summary>
        /// Specifies whether or not the line is enabled, i.e. to be processed by the m_code generator. Normally, all lines are enabled, but sometimes the m_code generator disables unnecessary or overlapping lines.
        /// Íf false, the Line is not enabled and will not be displayed or used.
        /// </summary>
        /// <remarks>Recently, the Design.prepare() method calls the Design.removeDisabledLines() method, which removes any disabled line before it's processed by the Code Generator. Therefore, the Enabled property is probably unnecessary, as the Line could simply be removed instead.</remarks>
        [XmlAttribute("enabled")]
        [DefaultValue(true)]
        [DataMember]
        public bool Enabled { get; set; }

        /// <summary>
        /// AKA Direction
        /// </summary>
        [XmlAttribute("angle")]
        [DataMember]
        public LineDirection Direction;

        /// <summary>
        /// Only used for XML serialization of string as an attribute.
        /// </summary>
        [XmlAttribute("x")]
        [DataMember]
        public string StartX;

        /// <summary>
        /// Only used for XML serialization of string as an attribute.
        /// </summary>
        [XmlAttribute("y")]
        [DataMember]
        public string StartY;

        /// <summary>
        /// Only used for XML serialization of string as an attribute.
        /// </summary>
        [XmlIgnore]
        [DataMember]
        public string EndY;
        /// <summary>
        /// Only used for XML serialization of string as an attribute.
        /// </summary>
        [XmlIgnore]
        [DataMember]
        public string EndX;

        /// <summary>
        /// Only used for XML serialization of string as an attribute.
        /// </summary>
        [XmlAttribute("length")]
        [DataMember]
        public string Length;

        /// <summary>
        /// Only used for XML serialization of string as an attribute.
        /// </summary>
        [DefaultValue("0")] // Don't include property in XML serialization if empty
        [XmlAttribute("cut")]
        [DataMember]
        public string CutLength;

        /// <summary>
        /// Only used for XML serialization of string as an attribute.
        /// </summary>
        [DefaultValue("0")] // Don't include property in XML serialization if empty
        [XmlAttribute("crease")]
        [DataMember]
        public string XMLSerializePerforationCreaseLength;

        [DefaultValue("0")]
        [XmlAttribute("crackReduction")]
        [DataMember]
        public string XMLSerializeCrackReduction;


        /// <summary>
        /// Returns a text representation of the line. Currently, the string is used by the GUI list in the prototype.
        /// </summary>
        public override string ToString()
        {
            return string.Format("{0} {6} Start:({1}, {2}) End:({3}, {4}) Length:{5}", Direction, StartX, StartY, EndX, EndY, Length, Type);
        
        }

        public object Clone()
        {
            return new Line()
                   {
                       CutLength = this.CutLength,
                       Direction = this.Direction,
                       Enabled = this.Enabled,
                       EndX = this.EndX,
                       EndY = this.EndY,
                       ID = this.ID,
                       Label = this.Label,
                       Length = this.Length,
                       StartX = this.StartX,
                       StartY = this.StartY,
                       Type = this.Type,
                   };
        }
    }

    [Serializable]
    [DataContract]
    public class ElasticLine : Line
    {
        public ElasticLine()
        {
            SwellPercent = 0;
        }

        [XmlAttribute("swell")]
        public int SwellPercent { get; set; }

        public new object Clone()
        {
            return new ElasticLine()
            {
                CutLength = this.CutLength,
                Direction = this.Direction,
                Enabled = this.Enabled,
                EndX = this.EndX,
                EndY = this.EndY,
                ID = this.ID,
                Label = this.Label,
                Length = this.Length,
                StartX = this.StartX,
                StartY = this.StartY,
                Type = this.Type,
                SwellPercent = this.SwellPercent
            };
        }
    }
}