﻿namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class ConditionalBooleanDesignElement
    {
        /// <summary>
        /// Return the value whether or not the element is allowed at all.
        /// </summary>
        [XmlAttribute("Allowed")]
        public bool Allowed { get; set; }
    }
}
