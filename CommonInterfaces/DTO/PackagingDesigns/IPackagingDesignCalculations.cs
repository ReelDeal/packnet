using System;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;

namespace PackNet.Common.Interfaces.DTO.PackagingDesigns
{
    public interface IPackagingDesignCalculations
    {
        PackagingDesignCalculation CalculateUsage(
            ICarton carton,
            Corrugate corrugate,
            int allowedTileCount,
            bool isRotated);
    }
}