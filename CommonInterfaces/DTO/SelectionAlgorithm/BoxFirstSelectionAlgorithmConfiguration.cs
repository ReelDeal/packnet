﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.SelectionAlgorithm
{
    public class BoxFirstSelectionAlgorithmConfiguration
    {
        public int SurgeCount { get; set; }

        public ConcurrentList<CartonPropertyGroup> ConfiguredCartonPropertyGroups { get; set; }
    }
}