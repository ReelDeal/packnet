﻿
namespace PackNet.Common.Interfaces.DTO.SelectionAlgorithm
{
    public class SearchConfiguration
    {
        public int MaxBoxFirstSearchResults { get; set; }
        public int MaxBoxLastSearchResults { get; set; }
        public int MaxOrdersSearchResults { get; set; }
        public int MaxScanToCreateSearchResults { get; set; }
    }
}
