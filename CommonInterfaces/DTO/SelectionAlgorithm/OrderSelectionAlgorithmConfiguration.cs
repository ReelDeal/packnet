﻿namespace PackNet.Common.Interfaces.DTO.SelectionAlgorithm
{
    public class OrderSelectionAlgorithmConfiguration
    {
        public bool PauseMachineBetweenOrders { get; set; }

        public bool CanDistribute { get; set; }

        public bool DefaultDistributeValue { get; set; }
    }
}