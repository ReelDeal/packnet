﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.SelectionAlgorithm
{
    using System;
    using System.Collections.Generic;

    public class SearchProducibles
    {
        public SelectionAlgorithmTypes SelectionAlgorithmTypes { get; set; }
        public string Criteria { get; set; }
    }

    public class SearchProduciblesWithRestrictions : SearchProducibles
    {
        public Dictionary<string, Guid> Restrictions { get; set; }
    }
}