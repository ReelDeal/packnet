﻿namespace PackNet.Common.Interfaces.DTO.SelectionAlgorithm
{
    public class ScanToCreateSelectionAlgorithmConfiguration
    {
        public bool ShowTriggerInterface { get; set; }
        public string BarcodeParsingWorkflow { get; set; }
    }
}