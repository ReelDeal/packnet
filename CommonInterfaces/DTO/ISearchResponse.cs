using PackNet.Common.Interfaces.DTO.Messaging;

namespace PackNet.Common.Interfaces.DTO
{
    /// <summary>
    /// Should be used in conjunction with ISearchRequest
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISearchResponse<T> : IResponseMessage<T>
    {
        string SearchCriteria { get; set; } // we cannot get this by inheriting from ISearchResult because we don't want to tie that T to the Response T.

        int TotalNumberOfItems { get; set; }
    }
}