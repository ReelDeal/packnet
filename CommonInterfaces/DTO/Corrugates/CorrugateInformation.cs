﻿using System;

namespace PackNet.Common.Interfaces.DTO.Corrugates
{
    using System.Runtime.Serialization;

    [DataContract(Name = "Corrugate", Namespace = "http://Packsize.MachineManager.com")]
    public class CorrugateInformation
    {
        [DataMember(Name = "Id")]
        public virtual Guid Id { get; set; }
    }
}
