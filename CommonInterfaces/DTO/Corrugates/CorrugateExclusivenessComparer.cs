﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.Corrugates
{
    public class CorrugateExclusivenessComparer : IComparer<Corrugate>
    {
        private IEnumerable<IPacksizeCutCreaseMachine> machines;
        
        public CorrugateExclusivenessComparer(IEnumerable<IPacksizeCutCreaseMachine> machines)
        {
            this.machines = machines;
        }

        public int Compare(Corrugate x, Corrugate y)
        {
            if (ReferenceEquals(x, y))
            {
                return 0;
            }

            if (x == null)
            {
                return 1;
            }

            if (y == null)
            {
                return -1;
            }

            int result = CompareExclusiveness(x, y);
            
            if (result == 0)
            {
                result = CompareWidth(x, y);
            }

            if (result == 0)
            {
                result = CompareQuality(x, y);
            }

            return result;
            }

        private int CompareQuality(Corrugate x, Corrugate y)
        {
            if (x.Quality > y.Quality)
            {
                return -1;
            }
            if (x.Quality < y.Quality)
            {
                return 1;
            }

            return 0;
        }

        private int CompareWidth(Corrugate x, Corrugate y)
        {
            if (x.Width > y.Width)
            {
                return -1;
            }
            if (x.Width < y.Width)
            {
                return 1;
            }

            return 0;
        }

        private int CompareExclusiveness(Corrugate x, Corrugate y)
        {
            int valueX = GetCorrugateExclusiveness(x);
            int valueY = GetCorrugateExclusiveness(y);

            if (valueX < valueY)
            {
                return -1;
            }
            if (valueX > valueY)
            {
                return 1;
            }

            return 0;
        }

        private int GetCorrugateExclusiveness(Corrugate fanfold)
        {
            int exclusiveness = 1;
            foreach (var m in machines)
            {
                if (m.Tracks != null)
                {
                    foreach (var track in m.Tracks.Where(t => t.LoadedCorrugate != null))
                    {
                        if (AreCorrugatesEqual(track.LoadedCorrugate, fanfold))
                        {
                            exclusiveness++;
                        }
                    }
                }
            }

            return exclusiveness;
        }

        private bool AreCorrugatesEqual(Corrugate f, Corrugate fanfold)
        {
            return f.Width == fanfold.Width
                && f.Thickness == fanfold.Thickness
                && f.Quality == fanfold.Quality;
        }
    }
}
