﻿using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.ExtensionMethods;

namespace PackNet.Common.Interfaces.DTO.Corrugates
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 
    /// </summary>
    public class CartonOnCorrugate : IEquatable<CartonOnCorrugate>, ICloneable
    {
        public string Key { get; set; }
        public int TileCount { get; set; }
        public Corrugate Corrugate { get; set; }
        public Carton.Carton OriginalCarton { get; set; }
        /// <summary>
        /// Width of a single packaging on this corrugate
        /// </summary>
        public double CartonWidthOnCorrugate { get; set; }
        /// <summary>
        /// Length of a single packaging on this corrugate
        /// </summary>
        public double CartonLengthOnCorrugate { get; set; }
        
        public bool Rotated {
            get { return Rotation.IsRotated(); }
            set { return; } // Rotated cannot be set anymore. Need to load old cartons though.
        }

        public OrientationEnum Rotation { get; set; }

        /// <summary>
        /// A zero value means that the carton cannot be produced on this corrugate.
        /// </summary>
        public double AreaOnCorrugate { get; set; }

        /// <summary>
        /// The machine Id and alias
        /// </summary>
        public Dictionary<Guid, string> ProducibleMachines { get; set; }

        public double Yield
        {
            get { return TileCount * CartonWidthOnCorrugate / (double)Corrugate.Width; }
        }

        public CartonOnCorrugate()
        {
            ProducibleMachines = new Dictionary<Guid, string>();
        }

        public CartonOnCorrugate(Carton.Carton originalCarton, Corrugate corrugate, string key, PackagingDesignCalculation calculation, OrientationEnum rotation)
        {
            Key = key;
            double corrugateWidth = corrugate.Width;
            Corrugate = corrugate;
            Rotation = rotation;
            ProducibleMachines = new Dictionary<Guid, string>();

            if (calculation != null) 
            {
              
                CartonWidthOnCorrugate = calculation.Width;
                    TileCount = Convert.ToInt32(Math.Floor(corrugateWidth/CartonWidthOnCorrugate));
                    CartonLengthOnCorrugate = calculation.Length;
                    AreaOnCorrugate = CartonLengthOnCorrugate*corrugateWidth;
                    if (AreaOnCorrugate < calculation.Area)
                        AreaOnCorrugate = 0;
                
            }
        }
        
        public bool Equals(CartonOnCorrugate other)
        {
            return Corrugate.Equals(other.Corrugate);
        }

        public object Clone()
        {
            var coc = new CartonOnCorrugate {
                    Key = Key,
                    CartonLengthOnCorrugate = CartonLengthOnCorrugate,
                    CartonWidthOnCorrugate = CartonWidthOnCorrugate,
                    TileCount = TileCount,
                    Corrugate = Corrugate,
                    AreaOnCorrugate = AreaOnCorrugate,
                    Rotation = Rotation,
                    ProducibleMachines = new Dictionary<Guid, string>()
                };

            foreach (var kvp in ProducibleMachines)
            {
                coc.ProducibleMachines.Add(kvp.Key, kvp.Value);
            }

            return coc;
        }

        public override string ToString()
        {
            return
                string.Format(
                    "CartonOnCorrugate - CartonWidthOnCorrugate:{0}, CartonLengthOnCorrugate:{1}, AreaOnCorrugate:{2}, TileCount:{3}, Rotated:{4}, {5}",
                    CartonWidthOnCorrugate, CartonLengthOnCorrugate, AreaOnCorrugate, TileCount, Rotated, Corrugate);
        }
    }
}
