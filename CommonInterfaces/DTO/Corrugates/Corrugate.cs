using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.Corrugates
{
    using System;
    using DTO;

    public class Corrugate : IEquatable<Corrugate>, IPersistable
    {
        public string Alias { get; set; }
        
        public MicroMeter Width { get; set; }

        public MicroMeter Thickness { get; set; }

        public int Quality { get; set; }

        public Guid Id { get; set; }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool Equals(Corrugate corrugate)
        {
            return Equals((object)corrugate);
        }

        public override bool Equals(object corrugate)
        {
            var corrugateToCompare = corrugate as Corrugate;
            if (corrugateToCompare == null)
                return false;

            return Width == corrugateToCompare.Width && Thickness == corrugateToCompare.Thickness
               && Quality == corrugateToCompare.Quality;
        }

        public static bool operator ==(Corrugate c1, Corrugate c2)
        {
            var o1 = (object)c1;
            var o2 = (object)c2;
            if (o1 == null && o2 == null)
                return true;
            if (o1 == null)
                return false;
            if (o2 == null)
                return false;

            return c1.Equals(c2);
        }
        public static bool operator !=(Corrugate c1, Corrugate c2)
        {
            var o1 = (object)c1;
            var o2 = (object)c2;
            if (o1 == null && o2 == null)
                return false;
            if (o1 == null)
                return true;
            if (o2 == null)
                return true;

            return !c1.Equals(c2);
        }

        public override string ToString()
        {
            return string.Format("Corrugate - Alias:{0}, Width: {1}, Thickness:{2}, Quality: {3}", Alias, Width, Thickness, Quality);
        }
    }
}