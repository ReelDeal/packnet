﻿namespace PackNet.Common.Interfaces.DTO
{
    using System.Collections.Generic;

    public interface IPhysicalLineModifier
    {
        MicroMeter GetStartPos(PhysicalLine line);

        MicroMeter GetEndPos(PhysicalLine line);

        MicroMeter GetLineCoordinate(PhysicalLine line);

        void SetStartPos(PhysicalLine line, MicroMeter pos);

        void SetEndPos(PhysicalLine line, MicroMeter pos);

        IEnumerable<MicroMeter> GetLineCoordinates(IEnumerable<PhysicalLine> lines);

        IEnumerable<PhysicalLine> GetLinesAtCoordinate(IEnumerable<PhysicalLine> lines, MicroMeter coordinate);
    }
}
