﻿namespace PackNet.Common.Interfaces.DTO
{
    public interface IPrinterSettings
    {
        string PrinterName { get; }
    }
}