﻿using System;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.ComponentsConfiguration
{
    public class ComponentsConfiguration : IPersistable
    {
        public CodeGeneratorSettings CodeGeneratorSettings { get; set; }

        public Guid Id { get; set; }
    }
}
