﻿namespace PackNet.Common.Interfaces.DTO.ComponentsConfiguration
{
    public class CodeGeneratorSettings
    {
        public double MinimumLineDistanceToCorrugateEdge { get; set; }
        public double MinimumLineLength { get; set; }
        public double PerforationSafetyDistance { get; set; }
    }
}
