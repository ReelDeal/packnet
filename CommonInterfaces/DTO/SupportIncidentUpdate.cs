﻿using System;
using System.Collections.Generic;

namespace PackNet.Common.Interfaces.DTO
{
    public class SupportIncidentUpdate
    {
        public SupportIncidentUpdate()
        {
            UiLogMessages = new List<string>();
        }

        public Guid IncidentToUpdate { get; set; }
        public string UserMessage { get; set; } 
        public IEnumerable<string> UiLogMessages { get; set; } 
    }
}