﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.PrintingMachines;

namespace PackNet.Common.Interfaces.DTO
{
    public interface IPrinterFactory : IDisposable
    {
        IEnumerable<string> ProvidedPrinterTypes { get; }

        IPrinter GetPrinter(string printerTypeIdentifier, IPrinterSettings printerSettings);
    }
}