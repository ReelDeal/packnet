﻿using System;
using System.Collections.Generic;

using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO
{
	public class UserNotification : IPersistable
	{
	    private DateTime createdDate;

	    public UserNotification()
		{
			AlertUser = false;
			AlertUserTimeoutInMilliseconds = 5000;
			CreatedDate = DateTime.UtcNow;
            DismissedByUsers = new ConcurrentList<Guid>();
			NotificationType = NotificationType.System;
			Severity = NotificationSeverity.Info;
            NotifiedMachineGroups = new ConcurrentList<Guid>();
            NotifiedUsers = new ConcurrentList<Guid>();
	        MessageCount = 1;
		}

		public Guid Id { get; set; }
		
		public NotificationType NotificationType { get; set; }

		public NotificationSeverity Severity { get; set; }
		
		public string Message { get; set; }

        public int MessageCount { get; set; }
		
		public string AdditionalInfo { get; set; }

        [BsonElement("expiry")]
        public DateTime Expiry { get; set; }

	    public DateTime CreatedDate
	    {
	        get { return createdDate; }
	        set { createdDate = Expiry = value; }
	    }

	    [BsonIgnore]
		public bool AlertUser { get; set; }
		
		[BsonIgnore]
		public int AlertUserTimeoutInMilliseconds { get; set; }

        public ConcurrentList<Guid> DismissedByUsers { get; set; }

        public ConcurrentList<Guid> NotifiedMachineGroups { get; set; }

        public ConcurrentList<Guid> NotifiedUsers { get; set; }
	}
}