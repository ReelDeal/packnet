﻿using System;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.Users
{
    public class Preference : IPersistable
    {
        public string LogLevel { get; set; }
        public int MaximumNumberOfLogMessagesToRecord { get; set; }
        public Guid Id { get; set; }
        public string PreferredLocale { get; set; }

        public Guid UserId { get; set; }
    }
}