﻿namespace PackNet.Common.Interfaces.DTO.Users
{
    using System;
    using Repositories;

    public class User : IEquatable<User>, IPersistable
    {
        public Guid Id { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public bool IsAutoLogin { get; set; }

        public bool IsAdmin { get; set; }

        public bool IsSystem { get; set; }

        public string Token { get; set; }
        
        public bool Equals(User other)
        {
            return UserName == other.UserName;
        }
    }
}
