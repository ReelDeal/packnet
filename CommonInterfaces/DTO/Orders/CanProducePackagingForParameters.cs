﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace PackNet.Common.Interfaces.DTO.Orders
{
    [Obsolete("Should be using ICarton to determine if we can produce")]
    public struct CanProducePackagingForParameters
    {
        [Obsolete]
        public Guid MachineId { get; set; }
        /// <summary>
        /// Gets or sets the packaging design.
        /// </summary>
        /// <value>
        /// The packaging design.
        /// </value>
        /// <remarks>
        /// IN USE
        /// </remarks>
        public PhysicalDesign PackagingDesign { get; set; }

        public Corrugate Corrugate { get; set; }

        [Obsolete]
        public MicroMeter Length { get; set; }

        [Obsolete]
        public MicroMeter Width { get; set; }

        [Obsolete]
        public MicroMeter Height { get; set; }

        [Obsolete]
        public IDictionary<string, double> XValues { get; set; }
    }
}