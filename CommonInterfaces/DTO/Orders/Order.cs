﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.Orders
{
    /// <summary>
    /// Represents a group of identical <see cref="IProducible"/> to create together 
    /// </summary>
    public class Order : BaseProducible, IProducibleWrapper
    {
        
        
        /// <summary>
        /// Creates an IProducible Order
        /// </summary>
        /// <param name="producible"></param>
        /// <param name="originalQuantity"></param>
        [JsonConstructor]//!!! Do not rename parameters or deserialization will not be able to figure out what values to put in here.
        public Order(IProducible producible, int originalQuantity)
        {
            Id = Guid.NewGuid();
            //InProgress = new ConcurrentList<Guid>();
            ProducibleInProgress = new ConcurrentList<IProducible>();
            Produced = new ConcurrentList<Guid>();
            Failed = new ConcurrentList<Guid>();
            
            MachinesThatHaveWorkedOnOrder = new ConcurrentList<IMachine>();
            OriginalQuantity = originalQuantity;
            Producible = producible;

            if (producible != null && producible.Restrictions != null)
            {
                foreach (var restriction in producible.Restrictions)
                {
                    Restrictions.Add(restriction);
                }
            }
        }

        /// <summary>
        /// Identifier of the type of producible item
        /// </summary>
        public override ProducibleTypes ProducibleType { get { return ProducibleTypes.Order; } }

        /// <summary>
        /// The customer defined order identifier
        /// </summary>
        public string OrderId { get { return base.CustomerUniqueId; } set { base.CustomerUniqueId = value; } }
        
        /// <summary>
        /// If true, this order can be distributed to multiple machine groups, otherwise it is required to be created entirely on one machine
        /// </summary>
        public bool IsDistributable { get; set; }


        /// <summary>
        /// The producible item that will be created in the order
        /// </summary>
        public IProducible Producible { get; set; }

        /// <summary>
        /// The quantity of items originally requested
        /// </summary>
        public int OriginalQuantity { get; private set; }

        /// <summary>
        /// The quantity of items that have been produced
        /// </summary>
        public int ProducedQuantity { get { return Produced == null ? 0 : Produced.Count; } }

        /// <summary>
        /// The quantity of items that have yet to be produced.
        /// Failed jobs need to be reproduced before they are tried again.
        /// </summary>
        public int RemainingQuantity {
            get
            {
                var remaining = OriginalQuantity + ReproducedQuantity - FailedQuantity - ProducedQuantity - InProgressQuantity;
                // TODO: we should write a note in the log when the remaining quantity is negative
                return remaining > 0 ? remaining : 0;
            } 
        }

        /// <summary>
        /// The items that have failed
        /// </summary>
        [JsonIgnore]
        public ConcurrentList<Guid> Failed { get; set; }

        public int FailedQuantity { get { return Failed == null ? 0 : Failed.Count; } }

        /// <summary>
        /// The quantity of items requested to be reproduced (accumulated)
        /// </summary>
        public int ReproducedQuantity { get; set; }

        /// <summary>
        /// The quantity of items requested to be reproduced (currently)
        /// </summary>
        public int CurrentReproduceQuantity { get; set; }

        // <summary>
        // The items that have been produced
        // </summary>
        //[JsonIgnore]
        public ConcurrentList<Guid> InProgress { get; private set; }

        /// <summary>
        /// The items that have been produced
        /// </summary>
        [JsonIgnore]
        public ConcurrentList<IProducible> ProducibleInProgress { get; private set; }

        public int InProgressQuantity { get { return ProducibleInProgress == null ? 0 : ProducibleInProgress.Count; } }

        /// <summary>
        /// The items that have been produced
        /// </summary>
        [JsonIgnore]
        public ConcurrentList<Guid> Produced { get; private set; }
        
        /// <summary>
        /// The sequence in which to produce the order 
        /// </summary>
        public long ProductionSequence { get; set; }

        public DateTime StatusChangedDateTime { get; set; }

        [JsonConverter(typeof(MachineIdentifiersConverter))]
        public ConcurrentList<IMachine> MachinesThatHaveWorkedOnOrder { get; private set; }
    }
}
