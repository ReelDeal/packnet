﻿using System;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.BoxFirst
{
    using System.Collections.Generic;
    using System.Linq;
    using Producible;

    public class BoxFirstProducible : BaseProducible, IProducibleWrapper
    {
        private IProducible producible;
        private Guid producedOnMachineGroupId;

        public BoxFirstProducible()
        {
            CartonOnCorrugates = new ConcurrentList<CartonOnCorrugate>();
        }

        public BoxFirstProducible(IProducible producible) : this()
        {
            Producible = producible;
        }

        public override ProducibleTypes ProducibleType
        {
            get { return ProducibleTypes.BoxFirstProducible; }
        }

        public override Guid ProducedOnMachineGroupId
        {
            get
            {
                if (producedOnMachineGroupId == Guid.Empty
                    && producible != null
                    && producible.ProducedOnMachineGroupId != Guid.Empty)
                {
                    producedOnMachineGroupId = producible.ProducedOnMachineGroupId;
                }
                return producedOnMachineGroupId;
            }
            set
            {
                producedOnMachineGroupId = value;
                if(producible != null)
                    producible.ProducedOnMachineGroupId = value;
            }
        }

        public IProducible Producible
        {
            get { return producible; }
            set
            {
                producible = value;
                Id = value.Id;
                CustomerUniqueId = producible.CustomerUniqueId;
            }
        }

        [BsonIgnore]
        [JsonIgnore]
        public IEnumerable<CartonOnCorrugate> CartonOnCorrugates { get; set; }
        [BsonIgnore]
        [JsonIgnore]
        public IEnumerable<BoxFirstProducible> PossibleTilingPartners { get; set; }

    }
}
