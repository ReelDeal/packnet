﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.BoxFirst
{
    public class BoxFirstProducibleContainer
    {
        private ConcurrentDictionary<IProducible, ConcurrentList<BoxFirstProducible>> producibles;
        private IEqualityComparer<IProducible> equalityComparer;
 
        public BoxFirstProducibleContainer()
        {
            producibles = new ConcurrentDictionary<IProducible, ConcurrentList<BoxFirstProducible>>();
            equalityComparer = new BoxFirstCartonTilingEqualityComparer();
        }

        public void AddProducible(BoxFirstProducible producible)
        {
            producibles.AddOrUpdate(GetId(producible), v => AddValue(producible), (k, v) => UpdateValue(producible, v));
        }

        public void AddProducibles(IEnumerable<BoxFirstProducible> producibles)
        {
            producibles.ForEach(AddProducible);
        }

        public IEnumerable<BoxFirstProducible> GetIdenticalProducibles(BoxFirstProducible producible)
        {
            return producibles.GetOrAdd(GetId(producible), new ConcurrentList<BoxFirstProducible>());
        }

        public IEnumerable<IEnumerable<BoxFirstProducible>> GetGroupedProducibles()
        {
            return producibles.Values;
        }

        public IEnumerable<IProducible> GetAllProducibles()
        {
            return producibles.Values.SelectMany(v => v).ToList();
        }

        private IProducible GetId(BoxFirstProducible producible)
        {
            return producibles.Keys.FirstOrDefault(k => equalityComparer.Equals(k, producible.Producible)) ?? producible.Producible;
        }

        private ConcurrentList<BoxFirstProducible> AddValue(BoxFirstProducible producible)
        {
            return new ConcurrentList<BoxFirstProducible>() { producible };
        }

        private ConcurrentList<BoxFirstProducible> UpdateValue(BoxFirstProducible producible, ConcurrentList<BoxFirstProducible> lst)
        {
            lst.Add(producible);
            return lst;
        }
    }
}
