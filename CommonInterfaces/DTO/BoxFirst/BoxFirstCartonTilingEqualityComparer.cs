﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.DTO.BoxFirst
{
    public class BoxFirstCartonTilingEqualityComparer : IEqualityComparer<IProducible>
    {
        public bool Equals(IProducible x, IProducible y)
        {
            var kitX = x as Kit;
            var kitY = y as Kit;
            if (kitY != null && kitX != null)
                return AreCartonsInKitsEqual(kitX, kitY);

            var cartonX = x as ICarton;
            var cartonY = y as ICarton;
            if (cartonX != null && cartonY != null)
                return AreCartonsEqual(cartonX, cartonY);

            return false;
        }

        private bool AreCartonsEqual(ICarton x, ICarton y)
        {
            return x.DesignId == y.DesignId &&
                x.Length == y.Length &&
                x.Width == y.Width &&
                x.Height == y.Height &&
                x.CorrugateQuality == y.CorrugateQuality;
        }

        private bool AreCartonsInKitsEqual(Kit x, Kit y)
        {
            return Do<bool>(() => //This function continually throws System.InvalidOperationException: Collection was modified; enumeration operation may not execute. Retrying for now
            {
                var cartonsInX = x.ItemsToProduce.OfType<ICarton>().ToList();
                var cartonsInY = y.ItemsToProduce.OfType<ICarton>().ToList();

                if (cartonsInX.Count == 0)
                    return false;

                return (cartonsInX.Count == cartonsInY.Count) && !cartonsInX.Except(cartonsInY, this).Any();
            }, TimeSpan.FromMilliseconds(10), 5);
        }

        public int GetHashCode(IProducible obj)
        {
            var kit = obj as Kit;
            if (kit != null)
                return GetHashCodeForKit(kit);
            var carton = obj as ICarton;
            if (carton != null)
                return GetHashCodeForCarton(carton);

            return obj.GetHashCode();
        }

        private int GetHashCodeForCarton(ICarton c)
        {
            return (c.DesignId + c.Length + c.Width + c.Height + c.CorrugateQuality).GetHashCode();
        }

        private int GetHashCodeForKit(Kit k)
        {
            var cartonsInKit = k.ItemsToProduce.OfType<ICarton>().ToList();
            return cartonsInKit.Sum(c => GetHashCodeForCarton(c));
        }

        public static T Do<T>(Func<T> action, TimeSpan retryInterval, int retryCount = 3)
        {
            var exceptions = new List<Exception>();

            for (int retry = 0; retry < retryCount; retry++)
            {
                try
                {
                    return action();
                }
                catch (Exception ex)
                {
                    exceptions.Add(ex);
                    Thread.Sleep(retryInterval);
                }
            }

            throw new AggregateException(exceptions);
        }
    }
}
