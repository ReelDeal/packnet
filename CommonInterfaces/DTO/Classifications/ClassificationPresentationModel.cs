﻿namespace PackNet.Common.Interfaces.DTO.Classifications
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    using Enums;

    public class ClassificationPresentationModel
    {
        public ClassificationPresentationModel(Classification classification, int numberOfRequests)
        {
            Alias = classification.Alias;
            IsPriorityLabel = classification.IsPriorityLabel;
            Number = classification.Number;
            Status = classification.Status;

            NumberOfRequests = numberOfRequests;
        }

        public string Number { get; private set; }

        public string Alias { get; private set; }

        public bool IsPriorityLabel { get; private set; }

        public int NumberOfRequests { get; private set; }

        public ClassificationStatuses Status { get; private set; }
    }
}
