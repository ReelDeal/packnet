﻿using System;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.Classifications
{
    using Enums;

    public class Classification : IPersistable
    {
        public Classification()
        {
            Status = ClassificationStatuses.Normal;
        }

        public bool BuiltInClassification { get; set; }

        public string Number { get; set; }

        public string Alias { get; set; }

        public ClassificationStatuses Status { get; set; }

        public bool IsPriorityLabel { get; set; }

        public Guid Id { get; set; }

        public int NumberOfRequests { get; set; }
    }
}
