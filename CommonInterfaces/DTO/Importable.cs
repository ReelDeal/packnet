﻿using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO
{
    public class Importable
    {
        public ImportTypes ImportType { get; set; }
        public IEnumerable<dynamic> ImportedItems { get; set; }
        /// <summary>
        /// Were did this data come from?  File name, session id, etc.
        /// </summary>
        public string Source { get; set; }

        public int GetRowNumber(dynamic item)
        {
            object rowNumber;
            if ((item as IDictionary<string, object>).TryGetValue("rowNumber", out rowNumber))
            {
                return (int)rowNumber;
            }
            return -1;
        }

        public override string ToString()
        {
            var s = string.Empty;

            var count = 0;
            foreach (var item in ImportedItems.Select(importedItem => importedItem as IDictionary<string, object>))
            {
                object value;
                
                if (item == null) return string.Empty;
                
                if (item.TryGetValue("CustomerUniqueId", out value))
                {
                    s += value;
                }
                else if (item.TryGetValue("OrderId", out value))
                {
                    s += value;
                }
                else if (item.TryGetValue("SerialNumber", out value))
                {
                    s += value;
                }
                else
                {
                    s += "no identifier";
                }

                if (++count < ImportedItems.Count()) s += ",";
            }

            return s;
        }
    }
}
