﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PackNet.Common.Interfaces.DTO
{
    public class PhysicalDesign: ICloneable
    {
        protected List<PhysicalLine> lines = new List<PhysicalLine>();

        public MicroMeter Length { get; set; }

        public MicroMeter Width { get; set; }

        public PerforationParameters PerforationParameters { get; set; }

        public IEnumerable<PhysicalLine> Lines { get { return lines; } }

        public PhysicalDesign()
        {
            
        }

        public PhysicalDesign(IEnumerable<PhysicalLine> lines)
        {
            lines.ForEach(Add);
        }

        public IEnumerable<PhysicalLine> GetVerticalPhysicalLines()
        {           
            return Lines.GetVerticalLines();  
        }

        public IEnumerable<PhysicalLine> GetHorizontalPhysicalLines()
        {
            return Lines.GetHorizontalLines();
        }

        public IEnumerable<MicroMeter> GetHorizontalLineCoordinates()
        {
            return Lines.GetHorizontalLineCoordinates();    
        }

        public IEnumerable<PhysicalLine> GetHorizontalLinesAtCoordinate(MicroMeter coordinate)
        {
            return Lines.GetHorizontalLinesAtCoordinate(coordinate);
        }

        public void Add(PhysicalLine line)
        {
            lines.Add(line);
        }

        public void RemoveLine(PhysicalLine line)
        {
            lines.Remove(line);
        }

        public object Clone()
        {
            var clone = new PhysicalDesign
            {
                Length = this.Length,
                Width = this.Width,
            };

            if (PerforationParameters != null)
            {
                clone.PerforationParameters = new PerforationParameters
                {
                    CreaseLength = this.PerforationParameters.CreaseLength,
                    CutLength = this.PerforationParameters.CutLength,
                };
            }

            lines.ForEach(
                l =>
                    clone.Add(new PhysicalLine(new PhysicalCoordinate(l.StartCoordinate.X, l.StartCoordinate.Y),
                        new PhysicalCoordinate(l.EndCoordinate.X, l.EndCoordinate.Y), l.Type)));

            return clone;
        }
    }

    public enum LineType
    {
        /// <summary>
        /// Crease line
        /// </summary>
        crease,
        /// <summary>
        /// Cut line
        /// </summary>
        cut,
        /// <summary>
        /// Perforation line
        /// </summary>
        perforation,
        /// <summary>
        /// Waste separtor line, both cut and separator is activated
        /// </summary>
        separate,
        nunatab,
    }

    public class LineTypeComparer : IComparer<LineType>
    {
        public int Compare(LineType x, LineType y)
        {
            if (x == y)
            {
                return 0;
            }

            if (x == LineType.nunatab)
                return 1;

            if (x == LineType.cut && (y != LineType.nunatab))
            {
                return 1;
            }

            if ((x == LineType.perforation) && (y == LineType.perforation || y == LineType.crease))
            {
                return 1;
            }

            return -1;
        }
    }

    public class PhysicalLine
    {
        public PhysicalLine(PhysicalCoordinate startCoordinate, PhysicalCoordinate endCoordinate, LineType type)
        {
            StartCoordinate = startCoordinate;
            EndCoordinate = endCoordinate;
            Type = type;

            FlipCoordinatesIfReveresed();
        }

        public PhysicalCoordinate StartCoordinate { get; private set; }

        public PhysicalCoordinate EndCoordinate { get; private set; }

        public LineType Type { get; set; }

        public override string ToString()
        {
            return string.Format("Start: ({0}, {1}), End: ({2}, {3}), Type: {4}", StartCoordinate.X, StartCoordinate.Y, EndCoordinate.X,
                EndCoordinate.Y, Type);
        }

        private void FlipCoordinatesIfReveresed()
        {
            if (StartCoordinate.X > EndCoordinate.X || StartCoordinate.Y > EndCoordinate.Y)
            {
                var start = EndCoordinate;
                EndCoordinate = StartCoordinate;
                StartCoordinate = start;
            }
        }
    }

    public class PerforationParameters
    {
        public MicroMeter CreaseLength { get; set; }

        public MicroMeter CutLength { get; set; }
    }
    
    public static class PhysicalLineExtensions
    {
        public static IEnumerable<PhysicalLine> GetHorizontalLines(this IEnumerable<PhysicalLine> lines)
        {            
            return lines.Where(l => l.StartCoordinate.Y == l.EndCoordinate.Y).ToList();
        }
        
        public static IEnumerable<PhysicalLine> GetHorizontalLinesAtCoordinate(this IEnumerable<PhysicalLine> lines, MicroMeter coordinate)
        {
            return lines.GetHorizontalLines().Where(l => l.StartCoordinate.Y == coordinate).ToList();
        }

        public static IEnumerable<MicroMeter> GetHorizontalLineCoordinates(this IEnumerable<PhysicalLine> lines)
        {
            var horizontalLines = lines.GetHorizontalLines();
            IList<MicroMeter> coordinates = new List<MicroMeter>();
            foreach (PhysicalLine line in horizontalLines)
            {
                var currentCoordinate = line.StartCoordinate.Y;
                if (coordinates.Contains(currentCoordinate) == false)
                {
                    coordinates.Add(currentCoordinate);
                }
            }

            return coordinates;
        }

        public static IEnumerable<MicroMeter> GetVerticalLineCoordinates(this IEnumerable<PhysicalLine> lines)
        {
            var verticalLines = GetVerticalLines(lines);
            IList<MicroMeter> coordinates = new List<MicroMeter>();
            foreach (PhysicalLine line in verticalLines.OrderBy(l => l.StartCoordinate.X))
            {
                var currentCoordinate = line.StartCoordinate.X;
                if (coordinates.Contains(currentCoordinate) == false)
                {
                    coordinates.Add(currentCoordinate);
                }
            }

            return coordinates;
        }

        public static IEnumerable<PhysicalLine> GetVerticalLines(this IEnumerable<PhysicalLine> lines)
        {
            return lines.Where(l => l.StartCoordinate.X == l.EndCoordinate.X).ToList();
        }

        public static IEnumerable<PhysicalLine> GetVerticalLinesAtCoordinate(this IEnumerable<PhysicalLine> lines, MicroMeter coordinate)
        {
            return GetVerticalLines(lines).Where(l => l.StartCoordinate.X == coordinate).ToList();
        }

        public static IEnumerable<PhysicalLine> GetVerticalLinesWithMaximumCoordinate(this IEnumerable<PhysicalLine> lines, MicroMeter coordinate)
        {
            return GetVerticalLines(lines).Where(l => l.StartCoordinate.X <= coordinate).ToList();
        }

        public static IEnumerable<PhysicalLine> GetVerticalLinesWithMinimumCoordinate(this IEnumerable<PhysicalLine> lines, MicroMeter coordinate)
        {
            return GetVerticalLines(lines).Where(l => l.StartCoordinate.X >= coordinate).ToList();
        }

        public static bool IsDominantType(this PhysicalLine line, LineType comparison)
        {
            return line.Type.IsDominantOver(comparison);
        }

        public static bool IsDominantOver(this LineType first, LineType comparison)
        {
            if (first == LineType.nunatab)
            {
                return true;
            }

            if (first == LineType.cut && (comparison != LineType.nunatab))
            {
                return true;
            }

            if ((first == LineType.perforation) && (comparison == LineType.perforation || comparison == LineType.crease))
            {
                return true;
            }

            if ((first == LineType.crease) && (comparison == LineType.crease))
            {
                return true;
            }

            return false;
        }
    }

    public class PhysicalCoordinate
    {
  
        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public PhysicalCoordinate(MicroMeter x, MicroMeter y)
        {
            X = x;
            Y = y;
        }

        public MicroMeter X { get; set; }

        public MicroMeter Y { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != GetType())
            {
                return false;
            }
            return Equals((PhysicalCoordinate)obj);
        }

        protected bool Equals(PhysicalCoordinate other)
        {
            return X.Equals(other.X) && Y.Equals(other.Y);
        }


        public override string ToString()
        {
            return String.Format("[{0}, {1}]", X, Y);
        }
    }
}
