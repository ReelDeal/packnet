﻿namespace PackNet.Common.Interfaces.DTO
{
    using System;
    using System.Collections.Generic;

    [Obsolete("Not sure why this exists, use Message instead")]
    public class SearchArticle
    {
        public string SearchString { get; set; }

        public IEnumerable<Article> Articles { get; set; }
    }

}
