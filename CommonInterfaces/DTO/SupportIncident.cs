﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO
{
    public class SupportIncident : IPersistable
    {
        public SupportIncident()
        {
            CreatedOn = DateTime.UtcNow;
            UiLogMessages = new List<string>();
            Languages = new List<string>();
            BrowserPlugins = new List<string>();
        }

        public DateTime CreatedOn { get; set; }
        public List<string> UiLogMessages { get; set; }
        public string UserAgent { get; set; }
        public bool CookiesEnabled { get; set; }
        public string Language { get; set; }
        public List<string> Languages { get; set; }
        public string Platform { get; set; }
        public List<string> BrowserPlugins { get; set; }
        public string Vendor { get; set; }
        public string Product { get; set; }
        public string UserMessage { get; set; }
        public Guid Id { get; set; }

        public string ReportingUser { get; set; }
        public DateTime? DispatchedOn { get; set; }
    }
}