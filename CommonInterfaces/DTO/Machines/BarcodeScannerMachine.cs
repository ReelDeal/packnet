﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public class BarcodeScannerMachine : BaseStatusReportingMachine<MachineStatuses>, INetworkConnectedMachine
    {

        /// <summary>
        /// Machine type
        /// </summary>
        public MachineTypes MachineType { get { return BarcodeScannerMachineTypes.WifiBarcodeScanner; } }

        /// <summary>
        /// Optional data for the user to describe the machine
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Network Information used to communicate to the machine
        /// </summary>
        public string IpOrDnsName { get; set; }

        /// <summary>
        /// The IP version 4 subnet mask for the machine
        /// </summary>
        public string IpV4SubnetMask { get; set; }

        public string IpV4DefaultGateway { get; set; }

        /// <summary>
        /// The MacAddress for the machine
        /// </summary>
        public string MacAddress { get; set; }

        /// <summary>
        /// Port used to connect to the machine
        /// </summary>
        public int Port { get; set; }

        public bool NotifyBarcodeToUIOnManualMode { get; set; }
        
        /// <summary>
        /// Copy properties from one machine to another
        /// </summary>
        /// <param name="machine"></param>
        public void CopyTo(BarcodeScannerMachine machine)
        {
            machine.Alias = Alias;
            machine.Description = Description;
            machine.IpOrDnsName = IpOrDnsName;
            machine.Port = Port;
            machine.NotifyBarcodeToUIOnManualMode = NotifyBarcodeToUIOnManualMode;
        }
    }
}
