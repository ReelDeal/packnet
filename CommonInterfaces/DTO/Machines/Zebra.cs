﻿using System;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public static class Zebra
    {
        public static class Commands
        {
            public const string HostStatus = "~HS";
            public const string PrinterStart = "~PS";
            public const string PrinterPause = "~PP";
            public static readonly string SetToTearOff = "^XA^MMT,Y^JUS^XZ";
            public static readonly string SetToPeelOff = "^XA^MMP,Y^JUS^XZ";
        }

        public static class Constants
        {
            public const int HostStatusLinesToRead = 3;
        }

        public static class Formats
        {
            private static class Response
            {
                public const char StartText = (char)2;
                public const char EndText = (char)3;
                public const char NewLine = (char)10;
                public const char CarriageReturn = (char)13;
            }

            public static readonly char[] RowDelimiter = { Response.StartText, Response.EndText, Response.NewLine, Response.CarriageReturn };
        }
    }

    /// <summary>
    /// http://www.servopack.de/support/zebra/ZPLbasics.pdf
    /// Host Memory Status
    /// Sending the ~HM (Host Memory Status) instruction to the printer
    /// immediately returns a memory status message to the host. Use this
    /// instruction whenever you need to know the status of the memory.
    /// 
    /// When the Host Memory Status Command, ~HM, is sent to the Zebra
    /// printer, a line of data containing three numbers is sent back to the
    /// Host. The information contained in that line is described here.
    /// 
    /// Memory Status Line
    /// 1024,0780,0780
    /// 
    /// The first value is the total amount of RAM (Random Access Memory)
    /// installed in the printer. This number is in Kilobytes. In our
    /// example, the Zebra printer has 1024K RAM installed.
    /// 
    /// The second value is the maximum amount of RAM (Random Access
    /// Memory) available to the user. This number is in Kilobytes. In our
    /// example, the Zebra printer has a Maximum of 780K RAM available.
    /// 
    /// The third value is the amount of RAM (Random Access Memory)
    /// currently available to the user. This number is in Kilobytes. In our
    /// example, there is 780K of RAM in the Zebra printer currently available
    /// to the user.
    /// 
    /// NOTE 1: Memory taken up by bitmaps is not excluded from the
    /// memory currently available value. (Due to ^MCN.)
    /// 
    /// NOTE 2: Downloading a graphic image or saving a bitmap only
    /// affects the 3rd value. The 1st and 2nd values will not change after
    /// the printer is turned on.
    ///
    ///
    /// Host Status Return
    /// 
    /// When the Printer Status Command, ~HS, is sent to the Zebra printer,
    /// three data strings are sent back to the Host. Each string starts with an
    /// <STX> Control Code and is terminated by an <ETX><CR><LF>
    /// Control Code sequence. In this way, to avoid confusion, each String
    /// will be displayed/printed on a separate line by the Host.
    /// 
    /// ******************
    /// **** String 1 ****
    /// ******************
    /// 
    /// <STX>aaa,b,c,dddd,eee,f,g,h,iii,j,k,l<ETX><CR><LF>
    ///     aaa = Communication (Interface) Settings (*)
    ///     b = “Paper Out” Flag (1=Paper Out)
    ///     c = “Pause” Flag (1=Pause Active)
    ///     dddd = Label Length (Value in Number of Dots)
    ///     eee = Number of Formats in Receive Buffer
    ///     f = “Buffer Full” Flag (1=Receive Buffer Full)
    ///     g = “Communications Diagnostic Mode” Flag (1= Diagnostic Mode Active)
    ///     h = “Partial Format” Flag (1=Partial Format in Progress)
    ///     iii = UNUSED (Always 000)
    ///     j = “Corrupt RAM” Flag (1=Configuration Data Lost)
    ///     k = Temperature Range (1 = Under Temperature)
    ///     l = Temperature Range (1 = Over Temperature)
    /// 
    /// (*) This parameter specifies the Printer’s baud rate, # of data bits, # of
    /// stop bits, parity setting and type of handshaking. This value is a
    /// 3-digit decimal representation of an eight-bit binary number. To
    /// evaluate this parameter, first convert the decimal number to a binary
    /// number. Then, the 9-digit binary number is read as follows:
    /// 
    ///          aaa = a8 a7 a6 a5 a4 a3 a2 a1 a0
    ///          a8 = High Speed Baud Rate
    ///          0 = 110 thru 19200 baud
    ///          1 = 28800 baud and above
    ///          
    ///          a7 = Handshake
    ///          0 = Xon/Xoff
    ///          1 = DTR
    ///          
    ///          a6 = Parity Odd/Even
    ///          0 = Odd
    ///          1 = Even
    ///          
    ///          a5 = Disable/Enable
    ///          0 = Disable
    ///          1 = Enable
    ///          
    ///          a4
    ///          = Stop Bits
    ///          0 = 2 Bits
    ///          1 = 1 Bit
    ///          
    ///          a3
    ///          = Data Bits
    ///          0 = 7 Bits
    ///          1 = 8 Bits
    ///          
    ///          a8 a2 a1 a0 = Baud
    ///          0 000 = 110
    ///          0 001 = 300
    ///          0 010 = 600
    ///          0 011 = 1200
    ///          0 100 = 2400
    ///          0 101 = 4800
    ///          0 110 = 9600
    ///          0 111 = 19200
    ///          1 000 = 28800
    ///          1 001 = 38400 (available only on certain printer models)
    ///          1 010 = 57600
    ///
    /// ******************
    /// **** String 2 ****
    /// ******************
    ///
    /// <STX>mmm,n,o,p,q,r,s,t,uuuuuuuu,v,www<ETX><CR><LF>
    ///      mmm = Function Settings(*)
    ///      n = 0 (Unused)
    ///      o = “Head Up” Flag (1 = Head in UP Position)
    ///      p = “Ribbon Out” Flag (1= Ribbon Out)
    ///      q = “Thermal Transfer Mode” Flag (1 = Thermal Transfer Mode Selected)
    ///      r = Print Mode
    ///         0 = Rewind
    ///         1 = Peel Off
    ///         2 = Tear Off
    ///         3 = Reserved
    ///      s = Print Width Mode
    ///         6= 4.41"
    ///      t = “Label Waiting” Flag (1=Label Waiting in Peel-Off Mode)
    ///      uuuuuuuu = Labels remaining in Batch
    ///      v = “Format While Printing” Flag (Always 1)
    ///      www = Number of Graphic Images stored in Memory
    /// 
    /// (*) This parameter specifies the Printer’s media type, sensor profile
    /// status, and communication diagnostics status. As in String 1, this is a
    /// 3-digit decimal representation of an eight-bit binary number. First,
    /// convert the decimal number to a binary number. Then, the 8-digit
    /// binary number is read as follows:
    ///     mmm = m7 m6 m5 m4 m3 m2 m1 m0
    ///     m7 = Media Type
    ///     0 = Die-Cut
    ///     1 = Continuous
    ///     m6= Sensor Profile
    ///     0 = Off
    ///     242 ZPL II COMMAND REFERENCE
    ///     m5= Communications Diagnostics
    ///     0 = Off
    ///     1 = On
    ///     m4 m3 m2 m1 = Unused
    ///     0 = Always
    ///     m0 = Print Mode
    ///     0 = Direct Thermal
    ///     1 = Thermal Transfer
    /// 
    /// ******************
    /// **** String 3 ****
    /// ******************
    /// 
    /// <STX>xxxx,y<ETX><CR><LF>
    ///  xxxx = 0000 (Reserved for future use)
    ///  y = 0 (Reserved for future use)
    /// </summary>
    public class HostStatus
    {
        public HostStatus(string hostStatusMessage)
        {
            var hostStatusMessageRows = hostStatusMessage.Split(Zebra.Formats.RowDelimiter, StringSplitOptions.RemoveEmptyEntries);

            var valueDelimiter = new[] { ',' };

            var row0 = hostStatusMessageRows[0].Split(valueDelimiter);
            var row1 = hostStatusMessageRows[1].Split(valueDelimiter);
            //row0[0] //Baud mask
            OutOfLabels = row0[1] == "1";
            Paused = row0[2] == "1";
            HasLabelsInQueue = row0[4] != "000";
            //PrinterQueueFull = row0[5] == "1";

            HeadOpen = row1[2] == "1";
            OutOfRibbon = row1[3] == "1";
            ThermalTransferMode = row1[4] == "1";
            PeelOffMode = row1[5] == "1";
            LabelWaitingForPeelOff = row1[7] == "1";
            //var labelsRemainingInBatch = row1[8];
            //HasLabelsInQueue = string.IsNullOrEmpty(labelsInQueue.Trim('0')) == false;
        }

        public bool OutOfLabels { get; private set; }
        public bool OutOfRibbon { get; private set; }
        public bool HeadOpen { get; private set; }
        public bool LabelWaitingForPeelOff { get; private set; }
        public bool ThermalTransferMode { get; private set; }
        //public bool PrinterQueueFull { get; private set; }
        public bool PeelOffMode { get; private set; }
        public bool HasLabelsInQueue { get; private set; }
        public bool Paused { get; private set; }

        public ZebraMachineErrorStatuses GetErrorStatus()
        {
            if(OutOfLabels)
                return ZebraMachineErrorStatuses.OutOfLabels;

            if (OutOfRibbon)
                return ZebraMachineErrorStatuses.OutOfRibbon;

            if(HeadOpen)
                return ZebraMachineErrorStatuses.HeadOpen;

            return null;
        }

        public ZebraMachineErrorStatuses GetUnexpectedLabelStatus()
        {
            if(LabelWaitingForPeelOff)
                return ZebraMachineErrorStatuses.UnexpectedLabelOnPrinter;

            if (HasLabelsInQueue)
                return ZebraMachineErrorStatuses.UnexpectedLabelInPrinterQueue;

            return null;
        }
        
    }
}
