﻿using System;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public interface IProductionData
    {
        Guid Id { get; }

        IMachineProductionData MachineData { get; }

        string MachineName { get; }

        int MachineId { get; }
    }
}
