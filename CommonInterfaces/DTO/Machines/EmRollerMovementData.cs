﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public class EmRollerMovementData : EmNormalMovementData
    {
        public EmRollerMovementData()
        {
            ReverseMovement = new EmMovementData();
        }

        public EmMovementData ReverseMovement { get; set; }

        public double ActualMaximumReverseMovementSpeed
        {
            get { return ReverseMovement != null ? ReverseMovement.SpeedInPercent / 100.0 * Speed : 0.0; }
        }

        public double ActualMaximumReverseAcceleration
        {
            get { return ReverseMovement != null ? ReverseMovement.AccelerationInPercent / 100.0 * Acceleration : 0.0; }
        }
    }
}
