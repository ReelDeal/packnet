namespace PackNet.Common.Interfaces.DTO.Machines
{
    using System;
    using System.Runtime.Serialization;

    [DataContract(Name = "TrackInfo", Namespace = "http://Packsize.MachineManager.com")]
    [KnownType(typeof(LegacyMachineTrackInfo))]
    [Serializable]
    public class TrackInfo
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public Guid? CorrugateId { get; set; }
    }
}