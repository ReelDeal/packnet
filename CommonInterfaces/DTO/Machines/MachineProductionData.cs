﻿using System;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public class MachineProductionData : IMachineProductionData
    {
        private string userName;

        public IPacksizeCarton Carton { get; set; }

        public double FedLength { get; set; }

        public bool Failure { get; set; }

        public double ProductionTime { get; set; }

        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                userName = value ?? "Unknown user";
            }
        }
        
        public int ProducibleCount { get; set; }
        
        public Corrugate Corrugate { get; set; }

        public IMachine Machine { get; set; }
        
        public CartonOnCorrugate CartonOnCorrugate { get; set; }

        public Guid MachineGroup { get; set; }

        public MachineProductionData()
        {
            ProducibleCount = 1;
        }
    }
}
