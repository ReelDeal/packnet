﻿using System;
using System.Linq;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    using System.Collections.Generic;

    using Corrugates;
    using ProductionGroups;

    public class MachinePresentationModel<T>
    {
        public Guid Id { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public MachineTypes MachineType { get; set; }
        public T Data { get; set; }
    }
}
