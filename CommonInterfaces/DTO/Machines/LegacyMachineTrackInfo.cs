namespace PackNet.Common.Interfaces.DTO.Machines
{
    using System;
    using System.Runtime.Serialization;

    using DTO;

    [DataContract(Name = "LegacyMachineTrackInfo", Namespace = "http://Packsize.MachineManager.com")]
    [Serializable]
    public class LegacyMachineTrackInfo : TrackInfo
    {
        [DataMember(Order = 3)]
        public MicroMeter Offset  { get; set; }
    }
}