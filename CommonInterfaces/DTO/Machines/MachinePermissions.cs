﻿namespace PackNet.Common.Interfaces.DTO.Machines
{
    public class MachinePermissions
    {
        public bool CanManuallyCleanCut { get; set; }
        public bool CanRollForward { get; set; }
        public bool CanRollBack { get; set; }
    }
}