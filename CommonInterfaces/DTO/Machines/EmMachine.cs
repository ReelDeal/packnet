﻿

using System.Net;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using MongoDB.Bson.Serialization.Attributes;

    using Corrugates;
    using PhysicalMachine;
    using PhysicalMachine.EM;
    using Enums;
    using Common.Interfaces.Machines;

    public class EmMachine : StatusReportingMachineWithVersion<MachineStatuses>, IPacksizeCutCreaseMachine
    {
        public EmMachine()
        {
            CurrentStatus = MachineStatuses.MachineOffline;
            CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;
        }

        private EmPhysicalMachineSettings physicalMachineSettings;
        /// <summary>
        /// Machine type
        /// </summary>
        public MachineTypes MachineType { get { return MachineTypes.Em; } }
        
        /// <summary>
        /// Optional data for the user to describe the machine
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Network Information used to communicate to the machine
        /// </summary>
        public string IpOrDnsName { get; set; }

        /// <summary>
        /// The IP version 4 subnet mask for the machine
        /// </summary>
        public string IpV4SubnetMask { get; set; }

        public string IpV4DefaultGateway { get; set; }

        /// <summary>
        /// The MacAddress for the machine
        /// </summary>
        public string MacAddress { get; set; }
        
        /// <summary>
        /// Port used to connect to the machine
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Track configuration for machine
        /// </summary>
        public ConcurrentList<Track> Tracks { get; set; }

        /// <summary>
        /// Number of tracks configured on this machine
        /// </summary>
        //[BsonIgnore]
        public int NumTracks 
        {
            get { return Tracks == null ? 0 : Tracks.Count; }
            //This one is needed from the UI
            set
            {
                if (Tracks == null)
                    Tracks = new ConcurrentList<Track>();
                
                if (Tracks.Count < value)
                {
                    Tracks = new ConcurrentList<Track>(Tracks.Concat(Enumerable.Range(Tracks.Count + 1, value - Tracks.Count).Select(i => new Track() { TrackNumber = i}).ToList()).ToList());
                }
                else if (Tracks.Count > value)
                {
                    Tracks = new ConcurrentList<Track>(Tracks.Take(value).ToList());
                }
            }
        }
        
        /// <summary>
        /// Loads a corrugate onto the track
        /// </summary>
        /// <param name="track"></param>
        /// <param name="corrugate"></param>
        public void LoadCorrugate(Track track, Corrugate corrugate)
        {
            var trackToUpdate = Tracks.Single(t => t.TrackNumber == track.TrackNumber);
            trackToUpdate.LoadedCorrugate = corrugate;
            trackToUpdate.TrackOffset = track.TrackOffset;
        }

        public void UpdatePhysicalMachineSettings(IPEndPoint eventNotifierIp)
        {
            if (string.IsNullOrEmpty(PhysicalSettingsFilePath) == false)
                physicalMachineSettings = EmPhysicalMachineSettings.CreateFromFile(eventNotifierIp, PhysicalSettingsFilePath);
        }

        public void UpdateFrom(IPacksizeCutCreaseMachine copyFrom)
        {
            var other = copyFrom as EmMachine;
            if(other == null)
                throw new ArgumentException("Cannot update em machine with unkown type");

            AssignedOperator = other.AssignedOperator;
            physicalMachineSettings = other.physicalMachineSettings;
            PhysicalSettingsFilePath = other.PhysicalSettingsFilePath;
            Port = other.Port;
            IpOrDnsName = other.IpOrDnsName;
            MacAddress = other.MacAddress;
            IpV4DefaultGateway = other.IpV4DefaultGateway;
            IpV4SubnetMask = other.IpV4SubnetMask;
            Alias = other.Alias;
            Description = other.Description;
        }

        /// <summary>
        /// 
        /// </summary>
        public string PhysicalSettingsFilePath { get; set; }

        [BsonIgnore]
        [JsonIgnore]
        public PhysicalMachineSettings PhysicalMachineSettings
        {
            get { return physicalMachineSettings; }
            set
            {
                physicalMachineSettings = value as EmPhysicalMachineSettings;
            }
        }

        /// <summary>
        /// Operator currently running the machine
        /// </summary>
		[BsonIgnore]
        public string AssignedOperator { get; set; }

        /// <summary>
        /// Copy properties from one machine to another
        /// </summary>
        /// <param name="machine"></param>
        public void CopyTo(IPacksizeCutCreaseMachine machine)
        {
            machine.Alias = Alias;
            machine.Description = Description;
            machine.IpOrDnsName = IpOrDnsName;
            machine.Port = Port;
            machine.AssignedOperator = AssignedOperator;
            machine.NumTracks = NumTracks;
            machine.Tracks = Tracks;
            machine.PhysicalSettingsFilePath = PhysicalSettingsFilePath;
        }
    }
}
