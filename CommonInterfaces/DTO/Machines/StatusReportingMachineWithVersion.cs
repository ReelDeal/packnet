﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

namespace PackNet.Common.Interfaces.DTO.Machines
{ 
    public abstract class StatusReportingMachineWithVersion<T> : BaseStatusReportingMachine<T>
    {
        private readonly Subject<string> plcVersionSubject = new Subject<string>();
        private string plcVersion;

        [BsonIgnore]
        [JsonIgnore]
        public string PlcVersion
        {
            get { return plcVersion; }
            set
            {
                if (plcVersion != value)
                {
                    plcVersion = value;
                    plcVersionSubject.OnNext(plcVersion);
                }
            }
        }

        [BsonIgnore]
        [JsonIgnore]
        public IObservable<string> PlcVersionChangedObservable
        {
            get { return plcVersionSubject.AsObservable(); }
        }
    }
}
