﻿using System;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public class ZebraPrinter : BaseStatusReportingMachine<MachineStatuses>, ILabelPrintingMachine, INetworkPrinter
    {
        private int pollInterval;
        private int retryCount;
        private int labelSecondCheckWaitTime;
        private int socketSendTimeout;
        private int socketHeartbeatWaitTime;
        private int socketReceiveTimeout;
        private int labelInQueueRetryWaitTime;

        public int PollInterval
        {
            get
            {
                return pollInterval;
            }
            set
            {
                if (value >= 50)
                    pollInterval = value;
            }
        }

        public int RetryCount
        {
            get { return retryCount; }
            set
            {
                if (value > 0)
                    retryCount = value;
            }
        }
        public int SocketSendTimeout
        {
            get { return socketSendTimeout; }
            set
            {
                if (value >= 100)
                    socketSendTimeout = value;
                else
                {
                    socketSendTimeout = 3000;
                }
            }
        }
        public int SocketReceiveTimeout
        {
            get { return socketReceiveTimeout; }
            set
            {
                if (value >= 100)
                    socketReceiveTimeout = value;
                else
                {
                    socketReceiveTimeout = 3000;
                }
            }
        }
        public int SocketHeartbeatWaitTime
        {
            get { return socketHeartbeatWaitTime; }
            set
            {
                if (value >= 100)
                    socketHeartbeatWaitTime = value;
                else
                {
                    socketHeartbeatWaitTime = 3000;
                }
            }
        }

        public int LabelSecondCheckWaitTime
        {
            get { return labelSecondCheckWaitTime; }
            set
            {
                if (value >= 100) //Don't want to poll that quickly
                    labelSecondCheckWaitTime = value;
            }
        }
        
        public int LabelInQueueRetryWaitTime
        {
            get { return labelInQueueRetryWaitTime; }
            set
            {
                if (value >= 100) //Don't want to poll that quickly
                    labelInQueueRetryWaitTime = value;
                else
                {
                    labelInQueueRetryWaitTime = 800;
                }
            }
        }

        public MachineTypes MachineType { get { return PrintMachineTypes.ZebraPrinter; } }

        public string Description { get; set; }
        public bool IsPeelOff { get; set; }
        public bool IsPriorityPrinter { get; set; }
        public string IpOrDnsName { get; set; }
        public string IpV4SubnetMask { get; set; }
        public string IpV4DefaultGateway { get; set; }
        public string MacAddress { get; set; }

        public int Port { get; set; }
        /// <summary>
        /// Gets or sets the printer mode.
        /// </summary>
        /// <value>
        /// The printer mode.
        /// </value>
        /// <remarks>
        /// This value is for support on UI, not real value on the system.
        /// </remarks>
        public string PrinterMode { get; set; }
   
        [JsonConstructor]
        public ZebraPrinter()
        {
            PollInterval = 200;
            RetryCount = 10;
            LabelSecondCheckWaitTime = 100;
            SocketHeartbeatWaitTime = 3000;
            SocketReceiveTimeout = 3000;
            SocketSendTimeout = 3000;
            LabelInQueueRetryWaitTime = 800;
            CurrentStatus = MachineStatuses.MachineOffline;
            CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;
        }

        /// <summary>
        /// Copy properties from one machine to another
        /// </summary>
        /// <param name="machine"></param>
        public void CopyTo(ZebraPrinter machine)
        {
            machine.Alias = Alias;
            machine.Description = Description;
            machine.IpOrDnsName = IpOrDnsName;
            machine.Port = Port;
            machine.IsPriorityPrinter = IsPriorityPrinter;
        }

        public void UpdateFrom(ZebraPrinter copyFrom)
        {
            IpOrDnsName = copyFrom.IpOrDnsName;
            Port = copyFrom.Port;
            PollInterval = copyFrom.PollInterval;
            IsPeelOff = copyFrom.IsPeelOff;
            IsPriorityPrinter = copyFrom.IsPriorityPrinter;
            LabelSecondCheckWaitTime = copyFrom.LabelSecondCheckWaitTime;
            LabelInQueueRetryWaitTime = copyFrom.labelInQueueRetryWaitTime;
            SocketSendTimeout = copyFrom.SocketSendTimeout;
            SocketReceiveTimeout = copyFrom.SocketReceiveTimeout;
            SocketHeartbeatWaitTime = copyFrom.SocketHeartbeatWaitTime;
            PrinterMode = copyFrom.PrinterMode;
            RetryCount = copyFrom.RetryCount;
            Alias = copyFrom.Alias;
            Description = copyFrom.Description;
        }
    }
}