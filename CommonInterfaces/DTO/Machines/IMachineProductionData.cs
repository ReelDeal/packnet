﻿using System;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    /// <summary>
    /// Machine Production Data
    /// </summary>
    /// <remarks>
    /// This interface is used also by Installer Solution on CloudAtlas Repository, if you remove or change the name of any property please verify.
    /// </remarks>
    public interface IMachineProductionData
    {
        /// <summary>
        /// The carton that was produced
        /// </summary>
        IPacksizeCarton Carton { get; }
        
        /// <summary>
        /// Gets the length of the feeded.
        /// </summary>
        /// <value>
        /// The length of the feeded.
        /// </value>
        double FedLength { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IMachineProductionData"/> is failure.
        /// </summary>
        /// <value>
        ///   <c>true</c> if failure; otherwise, <c>false</c>.
        /// </value>
        bool Failure { get; }

        /// <summary>
        /// Time in seconds to create the item
        /// </summary>
        double ProductionTime { get; }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        string UserName { get; set; }
        
        /// <summary>
        /// Number of items that were created (for tiling)
        /// </summary>
        int ProducibleCount { get; }

        /// <summary>
        /// Gets or sets the corrugate.
        /// </summary>
        /// <value>
        /// The corrugate.
        /// </value>
        Corrugate Corrugate { get; set; }

        /// <summary>
        /// Gets or sets the machine.
        /// </summary>
        /// <value>
        /// The machine.
        /// </value>
        IMachine Machine { get; set; }

        /// <summary>
        /// Gets or sets the carton on corrugate.
        /// </summary>
        /// <value>
        /// The carton on corrugate.
        /// </value>
        CartonOnCorrugate CartonOnCorrugate { get; set; }

        /// <summary>
        /// Gets or sets the machine group of which the carton was produced on
        /// </summary>
        /// <value>
        /// The ID of the machine group.
        /// </value>
        Guid MachineGroup { get; set; }
    }
}
