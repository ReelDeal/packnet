﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    [Serializable]
    [DataContract]
    public class LegacyTracksLimitParameters
    {
        public LegacyTracksLimitParameters(double min, double max, List<double> startingPositions)
        {
            MinPosition = min;
            MaxPosition = max;
            StartingPositions = startingPositions;
        }

        [DataMember]
        public double MinPosition { get; private set; }

        [DataMember]
        public double MaxPosition { get; private set; }

        [DataMember]
        public List<double> StartingPositions { get; private set; }
    }
}
