﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public class EmNormalMovementData : IKinematicDataHolder
    {
        public EmNormalMovementData()
        {
            NormalMovement = new EmMovementData();
        }

        public double Speed { get; set; }

        public double Acceleration { get; set; }

        public double ActualMaximumMovementSpeed
        {
            get { return NormalMovement != null ? NormalMovement.SpeedInPercent / 100.0 * Speed : 0.0; }
        }

        public double ActualMaximumAcceleration
        {
            get { return NormalMovement != null ? NormalMovement.AccelerationInPercent / 100.0 * Acceleration : 0.0; }
        }

        public EmMovementData NormalMovement { get; set; }
    }
}
