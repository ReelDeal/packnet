﻿using PackNet.Common.Interfaces.DTO.Corrugates;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    public class Track
    {
        public int TrackNumber { get; set; }

        public Corrugate LoadedCorrugate { get; set; }

        public MicroMeter TrackOffset { get; set; }

        public bool IsInChangingCorrugateState { get; set; }
    }
}