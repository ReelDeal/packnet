﻿using System.Net;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    using Common.Interfaces.Machines;

    public interface IMachineCommunicatorFactory
    {
        IMachineCommunicator GetMachineCommunicator(IMachine machine);

        void UpdateWebRequestCreator(IPacksizeCutCreaseMachine machine, IMachineCommunicator communicator);

        IPEndPoint GetCallbackSetting();
    }
}
