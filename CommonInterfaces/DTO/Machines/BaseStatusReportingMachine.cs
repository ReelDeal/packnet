﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using MongoDB.Bson.Serialization.Attributes;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    /// <summary>
    /// Class that manages reporting status changed for a machine as well as resetting communication when needed
    /// </summary>
    public abstract class BaseStatusReportingMachine<T>: IMachineWithCapabilities, IDisposable
    {
        private readonly Subject<T> machineStatusSubject = new Subject<T>();
        protected readonly Subject<MachineProductionStatuses> productionStatusSubject = new Subject<MachineProductionStatuses>();
        private readonly Subject<IEnumerable<ICapability>> capabilitiesSubject = new Subject<IEnumerable<ICapability>>();
        private readonly ConcurrentDictionary<Type, IEnumerable<ICapability>> currentCapabilities;
        
        /// <summary>
        /// Unique identifier
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Human readable identifier
        /// </summary>
        public string Alias { get; set; }

        [BsonIgnore]
        public T CurrentStatus
        {
            get { return currentStatus; }
            set
            {
                if (currentStatus == null || !currentStatus.Equals(value))//only update if it has changed
                {
                    if ((value is MachineErrorStatuses || value.Equals(MachineStatuses.MachineOffline)) && CurrentProductionStatus is MachineProductionInProgressStatuses)
                        CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;

                    LastStatus = currentStatus;
                    currentStatus = value;
                    machineStatusSubject.OnNext(currentStatus);
                }
            }
        }

        [BsonIgnore]
        public T LastStatus { get; private set; }

        /// <summary>
        /// Detailed information regarding the current errors
        /// </summary>
        [BsonIgnore]
        public List<MachineError> Errors { get; set; }

        [BsonIgnore]
        public MachineProductionStatuses CurrentProductionStatus
        {
            get { return currentProductionStatus; }
            set
            {
                if (currentProductionStatus== null || !currentProductionStatus.Equals(value))//only update if it has changed
                {
                    currentProductionStatus = value;
                    productionStatusSubject.OnNext(currentProductionStatus);
                }
            }
        }

        //todo we want this to work eventually but now we're reassigned only corruagte capabilities when assingCorrugate called[JsonConverter(typeof(ShieldedConverter<List<ICapability>>))]
        [BsonIgnore]
        public IEnumerable<ICapability> CurrentCapabilities
        {
            get { return currentCapabilities.SelectMany(c => c.Value).ToList(); }
        }

        [BsonIgnore]
        [JsonIgnore]
        public IObservable<T> CurrentStatusChangedObservable { get { return machineStatusSubject.AsObservable(); } }

        [BsonIgnore]
        [JsonIgnore]
        public IObservable<MachineProductionStatuses> CurrentProductionStatusObservable { get { return productionStatusSubject.AsObservable(); } }


        [BsonIgnore]
        [JsonIgnore]
        public IObservable<IEnumerable<ICapability>> CurrentCapabilitiesChangedObservable { get { return capabilitiesSubject.AsObservable(); } }

        protected BaseStatusReportingMachine()
        {
            CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;
            currentCapabilities = new ConcurrentDictionary<Type, IEnumerable<ICapability>>();
            capabilitiesSubject = new Subject<IEnumerable<ICapability>>();
            machineStatusSubject = new Subject<T>();
            productionStatusSubject = new Subject<MachineProductionStatuses>();
            Errors = new List<MachineError>();
        }

        /// <summary>
        /// Adds or updates the capabilities of a machine.
        /// The capabilities are grouped by type and only the supplied types will be replaced.
        /// I.e. if a collection of corrugate and conveyor capabilities is passed and the machine already has corrugate, conveyor and some other capabilities.
        /// Only the corrugate and the conveyor capabilities will be replaced. The others will remain.
        /// </summary>
        /// <param name="capabilities"></param>
        public virtual void AddOrUpdateCapabilities(IEnumerable<ICapability> capabilities)
        {
            if (capabilities == null || capabilities.Any() == false)
                return;

            var groupedCapabilities = capabilities.GroupBy(c => c.GetType());

            groupedCapabilities.ForEach(capabilityGroup => currentCapabilities.AddOrUpdate(capabilityGroup.Key, capabilityGroup.ToList(), (k, v) => capabilityGroup.ToList()));
            
            capabilitiesSubject.OnNext(CurrentCapabilities);
        }

        public void RemoveCapabilitiesOfType(Type type)
        {
            IEnumerable<ICapability> removedCapabilities;

            currentCapabilities.TryRemove(type, out removedCapabilities);

            capabilitiesSubject.OnNext(CurrentCapabilities);
        }

        private bool disposed = false;
        private T currentStatus;
        protected MachineProductionStatuses currentProductionStatus;

        public override string ToString()
        {
            return String.Format("Machine '{0}' (Id:'{1}', Current Status:'{2}', Current Production Status:'{3}')", Alias, Id, CurrentStatus, CurrentProductionStatus);
        }

        public void Dispose()
        {
            if (disposed)
                return;

            disposed = true;
            machineStatusSubject.OnCompleted();
            capabilitiesSubject.OnCompleted();
            machineStatusSubject.Dispose();
            capabilitiesSubject.Dispose();
        }
    }
}
