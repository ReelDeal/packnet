using PackNet.Common.Interfaces.DTO.Settings;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    [DataContract(Name = "PacksizeSuiteWmsZebraPrinter", Namespace = "http://Packsize.MachineManager.com")]
    public class PacksizeSuiteWmsZebraPrinterInformation : PrinterInformation
    {
        [DataMember(Name = "PrinterSettings")]
        public ZebraPrinterSettings PrinterSettings { get; set; }
    }
}