﻿namespace PackNet.Common.Interfaces.DTO.Machines
{
    using System.Net;
    using System.Runtime.Serialization;

    using Newtonsoft.Json;

    [DataContract(Name = "MachineManagerCallbackIp", Namespace = "http://Packsize.MachineManager.com")]
    public class MachineManagerCallbackIp
    {
        public IPEndPoint ipEndPoint = null;

        [DataMember(Order = 1)]
        [JsonProperty]
        public virtual string IpOrDnsName { get; set; }

        [DataMember(Order = 2)]
        [JsonProperty]
        public virtual int Port { get; set; }


        
    }
}
