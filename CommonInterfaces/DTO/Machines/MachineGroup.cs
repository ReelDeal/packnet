﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.Machines
{
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;

    [DebuggerDisplay(" Alias = {Alias}, Id = {Id}, Machines = {ConfiguredMachines.Count}, Status = {CurrentStatus.Value}")]
    public class MachineGroup : BaseStatusReportingMachine<MachineGroupStatuses>, IHumanOperatedMachine, IPersistable
    {
        private ConcurrentDictionary<Guid, IEnumerable<ICapability>> machineCapabilities;

        public MachineGroup()
        {
            ConfiguredMachines = new ConcurrentList<Guid>();
            machineCapabilities = new ConcurrentDictionary<Guid, IEnumerable<ICapability>>();
            ProductionMode = MachineGroupProductionModes.ManualProductionMode;
            CurrentStatus = MachineGroupUnavailableStatuses.MachineGroupOffline;
        }
        
        /// <summary>
        /// Optional data for the user to describe the machine
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// List of machines that have been added to this group.  Machines are created by some service for their type.
        /// </summary>
        public ConcurrentList<Guid> ConfiguredMachines { get; set; }

        /// <summary>
        /// The path of the work flow that will be invoked to create work for this group of machines
        /// </summary>
        public string WorkflowPath { get; set; }

        /// <summary>
        /// The number of allowed items in the queue for this machine group
        /// </summary>
        public int MaxQueueLength { get; set; }
        
        /// <summary>
        /// The current production mode of the machine group
        /// </summary>
        [BsonIgnore]
        public MachineGroupProductionModes ProductionMode { get; set; }

        [BsonIgnore]
            //todo: add serializer for this
        public IEnumerable<ICapability> AllMachineCapabilities
        {
            get
            {
                //Add into the capabilities a machine id capability for each configured machine to support carton-on-corrugate treatment
                var caps = new ConcurrentList<ICapability>();
                ConfiguredMachines.ForEach(a => caps.Add(new MachineIdCapability(a)));
                CurrentCapabilities.ForEach(caps.Add);
                caps.Add(new MachineGroupSpecificCapability(this.Id));
                //caps.AddRange(CurrentCapabilities); // some how some way this was throwing an array see error below
                /*Exception: Newtonsoft.Json.JsonSerializationException: Error getting value from 'AllMachineCapabilities' on 'PackNet.Common.Interfaces.DTO.Machines.MachineGroup'. ---> System.ArgumentException: Source array was not long enough. Check srcIndex and length, and the array's lower bounds.
                   at System.Array.Copy(Array sourceArray, Int32 sourceIndex, Array destinationArray, Int32 destinationIndex, Int32 length, Boolean reliable)
                   at System.Array.Copy(Array sourceArray, Int32 sourceIndex, Array destinationArray, Int32 destinationIndex, Int32 length)
                   at System.Collections.Generic.List`1.CopyTo(T[] array, Int32 arrayIndex)
                   at System.Collections.Generic.List`1.InsertRange(Int32 index, IEnumerable`1 collection)*/
                return caps.Distinct().ToList();
            }
        }
        /// <summary>
        /// Current assigned operator
        /// </summary>
        [BsonIgnore]
        public string AssignedOperator { get; set; }

        /// <summary>
        /// Current assigned operators UI reference.  Used to send messages to only that Operator panel when things change. 
        /// </summary>
        [BsonIgnore]
        public string AssignedOperatorReplyTo { get; set; }
        
        [BsonIgnore]
        public new MachineProductionStatuses CurrentProductionStatus
        {
            get { return currentProductionStatus; }
            set
            {
                if (currentProductionStatus != value)
                {
                    currentProductionStatus = value;
                    productionStatusSubject.OnNext(currentProductionStatus);
                }
                //Mg needs to trigger it's observable all the time since that one is an aggregate of the machines
                //Otherwise actions relating to the changes of a machines production status might be missed
                //This should be changed when technical debt story 10729 is finished
            }
        }

        public void UpdateMachineGroupCapabilities(Guid machineId, IEnumerable<ICapability> newCapabilities)
        {
            if (newCapabilities == null)
            {
                IEnumerable<ICapability> removed;
                machineCapabilities.TryRemove(machineId, out removed);
                removed.ForEach(r => RemoveCapabilitiesOfType(r.GetType()));
            }
            else if (machineCapabilities.ContainsKey(machineId))
                machineCapabilities[machineId] = newCapabilities.ToList();
            else
                machineCapabilities.TryAdd(machineId, newCapabilities.ToList());

           
            var all = new ConcurrentList<ICapability>();
                machineCapabilities.Values.ForEach(a =>
                {
                    if (a.Any())
                        all.AddRange(a);
                });
            
            AddOrUpdateCapabilities(all.Distinct().ToList());

        }

        public override string ToString()
        {
            return String.Format("Machine Group '{0}' (Id:'{1}', Current Status:'{2}', Current Production Status:'{3}', ConfiguredMachines: {4})", Alias, Id, CurrentStatus, CurrentProductionStatus, String.Join(", ", ConfiguredMachines));
        }
    }
}
