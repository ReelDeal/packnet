﻿using System;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.PickZones
{
    using System.Collections.Generic;

    public class PickArea : IPersistable
    {
        public Guid Id { get; set; }

        public string Alias { get; set; }

        public string Description { get; set; }

        public IEnumerable<Guid> ConfiguredPickZones { get; set; }
    }
}
