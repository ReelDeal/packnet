﻿using System;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.PickZones
{
    using Enums;

    public class PickZone : IEquatable<PickZone>, IPersistable
    {
        public PickZone()
        {
            Status = PickZoneStatuses.Normal;
        }

        public Guid Id { get; set; }

        public string Alias { get; set; }

        public string Description { get; set; }

        public PickZoneStatuses Status { get; set; }

        public int NumberOfRequests { get; set; }

        public bool Equals(PickZone other)
        {
            return Id == other.Id;
        }
    }
}