﻿namespace PackNet.Common.Interfaces.DTO.PLC
{
    public class PacksizePlcVariable
    {
        public PacksizePlcVariable(string taskName, string variable, bool notifyChanges = false, bool readOnly = false)
        {
            TaskName = taskName;
            VariableName = variable;
            NotifyChanges = notifyChanges;
            ReadOnly = readOnly;
        }

        public string TaskName { get; private set; }

        public string VariableName { get; private set; }

        public bool ReadOnly { get; private set; }

        public bool NotifyChanges { get; private set; }

        public override string ToString()
        {
            return string.IsNullOrWhiteSpace(TaskName) ?VariableName : TaskName + ":" + VariableName;
        }
    }
}
