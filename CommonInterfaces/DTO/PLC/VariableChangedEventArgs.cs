﻿using System;

namespace PackNet.Common.Interfaces.DTO.PLC
{
    public class VariableChangedEventArgs : EventArgs
    {
        public VariableChangedEventArgs(string variableName, object value, string address = "127.0.0.1")
        {
            VariableName = variableName;
            Value = value;
            Address = address;
        }

        public string VariableName { get; private set; }

        public object Value { get; private set; }

        public string Address { get; private set; }

        public bool IsOfType(PacksizePlcVariable variable)
        {
            return VariableName == variable.VariableName;
        }
    }
}
