﻿using Newtonsoft.Json;

namespace PackNet.Common.Interfaces.DTO.PLC
{
    public class PLCProducible
    {
        public PLCProducible(int[] producibleId, short[] instructionList, bool shouldWaitForBoxRelease, bool readyForProcessing)
        {
            Id = producibleId;
            InstructionList = instructionList;
            ShouldWaitForBoxRelease = shouldWaitForBoxRelease;
            ReadyForProcessing = readyForProcessing;
        }

        [JsonProperty(PropertyName = "Id.Parts", Order = 1)]
        public int[] Id { get; set; }

        public short[] InstructionList { get; set; }

        public bool ShouldWaitForBoxRelease { get; set; }

        [JsonProperty(Order = 4)]
        public bool ReadyForProcessing { get; set; }
    }
}