using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.Messaging;

namespace PackNet.Common.Interfaces.DTO
{
    using Enums;

    public class SearchResponse<T> : ResponseMessage<T>, ISearchResponse<T>
    {
        public SearchResponse()
        {
        }
        public SearchResponse(string searchCriteria, int totalNumberOfItems, MessageTypes responseMessageType, ResultTypes result, T data, string replyTo)
        {
            MessageType = responseMessageType;
            Result = result;
            Data = data;
            ReplyTo = replyTo;
            TotalNumberOfItems = totalNumberOfItems;
            SearchCriteria = searchCriteria;
        }
        
        public string SearchCriteria { get; set; }

        [JsonProperty(Order = 1)]
        public int TotalNumberOfItems { get; set; }
    }
}