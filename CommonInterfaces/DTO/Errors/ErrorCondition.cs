﻿using System;
using PackNet.Common.Interfaces.DTO.Interfaces;

namespace PackNet.Common.Interfaces.DTO.Errors
{
    public class ErrorCondition : IErrorCondition
    {
        public ErrorCondition()
        {
            TimeOccuredUtc = DateTime.UtcNow;
        }

        public DateTime TimeOccuredUtc { get; private set; }
        public string Message { get; set; }
    }
}