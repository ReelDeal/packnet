﻿namespace PackNet.Common.Interfaces.DTO.Errors
{
    public class NetworkScannerFailedToBindErrorCondition : ErrorCondition
    {
        public NetworkScannerFailedToBindErrorCondition(int port, string pgAlias)
        {
            base.Message = string.Format("Failed to bind to port {0} for production group {1}", port, pgAlias);
            ProductionGroup = pgAlias;
        }

        public string ProductionGroup { get; set; }
    }
}