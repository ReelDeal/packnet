﻿namespace PackNet.Common.Interfaces.DTO
{
    using System.Collections.Generic;

    public class PhysicalHorizontalLineModifier : IPhysicalLineModifier
    {
        public IEnumerable<MicroMeter> GetLineCoordinates(IEnumerable<PhysicalLine> lines)
        {
            return lines.GetHorizontalLineCoordinates();
        }

        public IEnumerable<PhysicalLine> GetLinesAtCoordinate(IEnumerable<PhysicalLine> lines, MicroMeter coordinate)
        {
            return lines.GetHorizontalLinesAtCoordinate(coordinate);
        }

        public MicroMeter GetStartPos(PhysicalLine line)
        {
            return line.StartCoordinate.X;
        }

        public MicroMeter GetEndPos(PhysicalLine line)
        {
            return line.EndCoordinate.X;
        }

        public MicroMeter GetLineCoordinate(PhysicalLine line)
        {
            return line.StartCoordinate.Y;
        }

        public void SetStartPos(PhysicalLine line, MicroMeter pos)
        {
            line.StartCoordinate.X = pos;
        }

        public void SetEndPos(PhysicalLine line, MicroMeter pos)
        {
            line.EndCoordinate.X = pos;
        }
    }
}
