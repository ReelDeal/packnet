﻿using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    /// <summary>
    /// Network connected Printer
    /// </summary>
    public interface INetworkPrinter : IPrinter, INetworkConnectedMachine
    {

    }
}
