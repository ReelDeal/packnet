﻿using System;

using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    /// <summary>
    /// Printer
    /// </summary>
    public interface IPrinter : IMachine
    {
    }
}
