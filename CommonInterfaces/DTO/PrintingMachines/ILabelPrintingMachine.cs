﻿namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    public interface ILabelPrintingMachine : IPrinter
    {
        /// <summary>
        /// Is this printer configured with priority labels
        /// </summary>
        bool IsPriorityPrinter { get; set; }
    }
}
