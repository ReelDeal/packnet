﻿using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using System;

namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    public class Printable : BaseProducible, IPrintable
    {
        /// <summary>
        /// Gets or sets the print data.
        /// </summary>
        /// <value>
        /// The print data.
        /// </value>
        public object PrintData { get; set; }

        /// <summary>
        /// Constructor used in importer when constructing producibles
        /// </summary>
        public Printable()
        {
            Id = Guid.NewGuid();
        }

        /// <summary>
        /// Printable data of object type.
        /// </summary>
        /// <remarks>Zebra printing expects a Dictionary&lt;string, string&gt; of a Carton</remarks>
        /// <example>new Printable(Carton.AsDictionary())</example>
        /// <param name="printData">Any derived object type. See remarks for special cases.</param>
        public Printable(object printData)
        {
            Id = Guid.NewGuid();
            PrintData = printData;
        }

        public override ProducibleTypes ProducibleType
        {
            get { return ProducibleTypes.Printable; }
        }
    }
}
