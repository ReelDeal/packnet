﻿using System;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    /// <summary>
    /// The status of a printable item sent to a IPrinter implementation.
    /// </summary>
    public class PrintJobStatus
    {
        /// <summary>
        /// Unique Identifier of the Printer
        /// </summary>
        public Guid PrinterId { get; set; }

        /// <summary>
        /// Current Item associated to the status
        /// </summary>
        public IPrintable ItemToPrint { get; set; }
        
        /// <summary>
        /// Status of the current item
        /// </summary>
        public ProducibleStatuses JobStatus { get; set; }

        /// <summary>
        /// String representation of the error
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}
