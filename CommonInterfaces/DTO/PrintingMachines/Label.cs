﻿using System.Diagnostics;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    [DebuggerDisplay("Label:{CustomerUniqueId}, Status:{ProducibleStatus}, Id:{Id}")]
    public class Label : Printable
    {
        public override ProducibleTypes ProducibleType { get { return ProducibleTypes.Label; } }

    }
}
