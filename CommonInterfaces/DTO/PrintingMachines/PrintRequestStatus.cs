﻿
using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    /// <summary>
    /// Public Status of one or many print requests.
    /// </summary>
    public class PrintRequestStatus
    {
        /// <summary>
        /// Unique Identifier of the Printer Reporting status
        /// </summary>
        public Guid UniqueID { get; set; }

        public PrintRequestStatuses RequestStatus { get; set; }

        /// <summary>
        /// Collection of one or more Print Jobs associated with the PrintRequestStatus
        /// </summary>
        public ConcurrentList<PrintJobStatus> PrintRequests;

    }
}
