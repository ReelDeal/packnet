﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.PrintingMachines
{
    /// <summary>
    /// Printer Status represents the status of an IPrinter
    /// </summary>
    public class PrinterStatus
    {
        /// <summary>
        /// Unique Identifier of the Printer Reporting status
        /// </summary>
        public Guid PrinterId { get; set; }

        /// <summary>
        /// Current status of the printer
        /// </summary>
        public PrinterMachineStatuses PrinterMachineStatus { get; set; }

        /// <summary>
        /// Message detailing the status of the printer
        /// </summary>
        public ConcurrentList<string> Messages { get; set; } 
    }
}
