﻿using System;

namespace PackNet.Common.Interfaces.DTO.Scanning
{
    public interface IScanTrigger
    {
        DateTime TriggeredOn { get; set; }
        string ProductionGroupAlias { get; set; }
        string BarcodeData { get; set; }
    }
}