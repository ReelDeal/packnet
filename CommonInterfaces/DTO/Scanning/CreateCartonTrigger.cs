﻿using System;

namespace PackNet.Common.Interfaces.DTO.Scanning
{
    public class CreateCartonTrigger : IScanTrigger
    {
        public DateTime TriggeredOn { get; set; }
        public string ProductionGroupAlias { get; set; }
        public string BarcodeData { get; set; }
    }
}