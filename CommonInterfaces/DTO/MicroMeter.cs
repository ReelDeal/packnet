﻿namespace PackNet.Common.Interfaces.DTO
{
    using System;
    using System.Globalization;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    [Serializable]
    public struct MicroMeter : IComparable<MicroMeter>, IEquatable<MicroMeter>, IXmlSerializable
    {
        private static NumberFormatInfo numberFormat = null;
        private const int Precision = 1000;
        private int rawValue;

        #region Constructors

        public MicroMeter(int micrometer)
        {
            rawValue = micrometer;
        }

        public MicroMeter(double millimeter)
        {
            rawValue = (int)Math.Round(millimeter * Precision);
        }

        #endregion

        public static MicroMeter MaxValue
        {
            get { return new MicroMeter(int.MaxValue); }
        }

        public static MicroMeter MinValue
        {
            get { return new MicroMeter(int.MinValue); }
        }

        private double ValueOnOutside
        {
            get
            {
                return (double)rawValue / (double)Precision;
            }
        }

        #region implicit operators

        public static implicit operator MicroMeter(int value)
        {
            return new MicroMeter(value * Precision);
        }

        public static implicit operator int(MicroMeter value)
        {
            return (int)value.ValueOnOutside;
        }
        
        public static implicit operator MicroMeter(double value)
        {
            return new MicroMeter(value);
        }

        public static implicit operator double(MicroMeter value)
        {
            return value.ValueOnOutside;
        }

        public static implicit operator MicroMeter(double? value)
        {
            if (value.HasValue)
            {
                return new MicroMeter(value.Value);
            }
            return new MicroMeter(0);
        }

        public static implicit operator MicroMeter?(double? value)
        {
            if (value.HasValue)
            {
                return new MicroMeter?(value.Value);
            }
            return (MicroMeter?)null;
        }

        #endregion implicit operators

        #region Overloaded operators
        public static MicroMeter operator +(MicroMeter c1, MicroMeter c2)
        {
            return new MicroMeter(c1.rawValue + c2.rawValue);
        }

        public static MicroMeter operator -(MicroMeter c1, MicroMeter c2)
        {
            return new MicroMeter(c1.rawValue - c2.rawValue);
        }

        public static MicroMeter operator -(MicroMeter c1)
        {
            return new MicroMeter(-c1.rawValue);
        }

        public static MicroMeter operator *(MicroMeter c1, MicroMeter c2)
        {
            return new MicroMeter((int)((long)c1.rawValue * (long)c2.rawValue / (long)Precision));
        }

        public static MicroMeter operator /(MicroMeter c1, MicroMeter c2)
        {
            return new MicroMeter((int)((long)c1.rawValue * (long)Precision / (long)c2.rawValue));
        }

        public static bool operator >(MicroMeter c1, MicroMeter c2)
        {
            return c1.rawValue > c2.rawValue;
        }

        public static bool operator <(MicroMeter c1, MicroMeter c2)
        {
            return c1.rawValue < c2.rawValue;
        }

        public static bool operator >=(MicroMeter c1, MicroMeter c2)
        {
            return c1.rawValue >= c2.rawValue;
        }

        public static bool operator <=(MicroMeter c1, MicroMeter c2)
        {
            return c1.rawValue <= c2.rawValue;
        }

        public static bool operator ==(MicroMeter c1, MicroMeter c2)
        {
            return c1.rawValue == c2.rawValue;
        }

        public static bool operator !=(MicroMeter c1, MicroMeter c2)
        {
            return c1.rawValue != c2.rawValue;
        }
        
        #endregion
        
        public int CompareTo(MicroMeter other)
        {
            return rawValue.CompareTo(other.rawValue);
        }

        public bool Equals(MicroMeter other)
        {
            return rawValue.Equals(other.rawValue);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return rawValue.GetHashCode();
        }

        public override string ToString()
        {
            return ValueOnOutside.ToString(NumberFormat);
        }

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            string val = reader.ReadString();
            if (string.IsNullOrEmpty(val))
            {
                rawValue = 0;
            }
            else
            {
                ConvertFromString(val);
            }
            reader.Read();
        }

        public void WriteXml(XmlWriter writer)
        {
            string serialized = ValueOnOutside.ToString().Replace(",", ".");
            writer.WriteString(serialized);
        }

        #endregion

        private void ConvertFromString(string millimeter)
        {
            millimeter = millimeter.Replace(",", ".");
            rawValue = (int)(Decimal.Parse(millimeter, NumberFormat) * (Decimal)Precision);
            rawValue = (int)(Decimal.Parse(millimeter) * (Decimal)Precision);
        }

        private static NumberFormatInfo NumberFormat
        {
            get
            {
                if (numberFormat == null)
                {
                    CultureInfo ci = CultureInfo.InstalledUICulture;
                    numberFormat = (NumberFormatInfo)ci.NumberFormat.Clone();
                    numberFormat.NumberDecimalSeparator = ".";
                }

                return numberFormat;
            }
        }
    }
}
