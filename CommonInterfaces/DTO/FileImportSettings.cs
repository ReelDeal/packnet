﻿namespace PackNet.Common.Interfaces.DTO
{
	public class FileImportSettings
	{
		private char commentIndicator;
		private string defaultImportType;
		private char fieldDelimiter;
		private string fileExtension;
		private string monitoredFolderPath;
		private int secondsToRetryToAccessMonitoredFilePath = 60;
		private int timesToRetryToAccessMonitoredFilePath = 5;
		private int secondsToRetryLockedFile = 15;
		private int timesToRetryLockedFile = 5;

		/// <summary>
		/// The character that indicates the line is a comment line. (e.g., '\' or '-' or '~'). Default is tilde if has not been set.
		/// </summary>
		/// <remarks>Place comment indicator at the beginning of the line to comment</remarks>
		public char CommentIndicator
		{
			get { return commentIndicator == '\0' /* default char */ ? '~' : commentIndicator; }
			set { commentIndicator = value; }
		}

		/// <summary>
		/// Type of importer to use (e.g., BoxFirst, Order, Trigger, etc.). Default is BoxFirst if not set.
		/// </summary>
		public string DefaultImportType
		{
			get { return string.IsNullOrEmpty(defaultImportType) ? "BoxFirst" : defaultImportType; }
			set { defaultImportType = value; }
		}

		/// <summary>
		/// True if the files should be deleted after all is imported successfully, otherwise false.
		/// </summary>
		public bool DeleteImportedFiles { get; set; }

		/// <summary>
		/// The character that separates one field from another (e.g., ',' or ';' or ':'). Default is semicolon if not set.
		/// </summary>
		public char FieldDelimiter
		{
			get { return fieldDelimiter == '\0' /* default char */ ? ';' : fieldDelimiter; }
			set { fieldDelimiter = value; }
		}

		/// <summary>
		/// File extension of the files to import (e.g., csv, txt). Default value is csv if not set.
		/// </summary>
		public string FileExtension
		{
			get { return string.IsNullOrEmpty(fileExtension) ? "csv" : fileExtension; }
			set { fileExtension = value; }
		}
		
		/// <summary>
		/// Specify the fields in the order they appear in the file
		/// </summary>
		public string[] HeaderFields { get; set; }

		/// <summary>
		/// The path to where the files to import will be found.
		/// </summary>
		public string MonitoredFolderPath
		{
			get { return string.IsNullOrEmpty(monitoredFolderPath) ? "DropFolder" : monitoredFolderPath; }
			set { monitoredFolderPath = value; }
		}

		/// <summary>
		/// Number of seconds to wait between retry attempts to connect to monitored file path when it has become unavailable. Default is 60.
		/// </summary>
		/// <remarks>
		/// The monitored file path may become unavailable if it has been deleted or the network location is no longer available.
		/// </remarks>
		public int SecondsToRetryToAccessMonitoredFilePath
		{
			get { return secondsToRetryToAccessMonitoredFilePath; }
			set { secondsToRetryToAccessMonitoredFilePath = value; }
		}

		/// <summary>
		/// Number of times to retry to connect to monitored file path when it has become unavailable. Default is five.
		/// </summary>
		/// <remarks>
		/// The monitored file path may become unavailable if it has been deleted or the network location is no longer available.
		/// </remarks>
		public int TimesToRetryToAccessMonitoredFilePath {
			get { return timesToRetryToAccessMonitoredFilePath; }
			set { timesToRetryToAccessMonitoredFilePath = value; } 
		}

		/// <summary>
		/// Number of seconds to retry reading a locked file. Default is 15 seconds if not set.
		/// </summary>
		public int SecondsToRetryLockedFile
		{
			get { return secondsToRetryLockedFile; }
			set { secondsToRetryLockedFile = value; }
		}
			
		/// <summary>
		/// Number of times to retry reading a locked file. Default is five.
		/// </summary>
		public int TimesToRetryLockedFile
		{
			get { return timesToRetryLockedFile; }
			set { timesToRetryLockedFile = value; }
		}
	}
}