﻿namespace PackNet.Common.Interfaces.DTO
{
    using System.Collections.Generic;

    public class PhysicalVerticalLineModifier : IPhysicalLineModifier
    {        
        public MicroMeter GetStartPos(PhysicalLine line)
        {
            return line.StartCoordinate.Y;
        }

        public MicroMeter GetEndPos(PhysicalLine line)
        {
            return line.EndCoordinate.Y;
        }

        public MicroMeter GetLineCoordinate(PhysicalLine line)
        {
            return line.StartCoordinate.X;
        }

        public void SetStartPos(PhysicalLine line, MicroMeter pos)
        {
            line.StartCoordinate.Y = pos;
        }

        public void SetEndPos(PhysicalLine line, MicroMeter pos)
        {
            line.EndCoordinate.Y = pos;
        }

        public IEnumerable<MicroMeter> GetLineCoordinates(IEnumerable<PhysicalLine> lines)
        {
            return lines.GetVerticalLineCoordinates();
        }

        public IEnumerable<PhysicalLine> GetLinesAtCoordinate(IEnumerable<PhysicalLine> lines, MicroMeter coordinate)
        {
            return lines.GetVerticalLinesAtCoordinate(coordinate);
        }
    }
}
