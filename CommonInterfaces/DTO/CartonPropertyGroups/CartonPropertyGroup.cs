﻿using System;

using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO.CartonPropertyGroups
{
    public class CartonPropertyGroup : IPersistable
    {
        public CartonPropertyGroup()
        {
            Status = CartonPropertyGroupStatuses.Normal;
        }

        public Guid Id { get; set; }

        public string Alias { get; set; }

        /// <summary>
        /// The quantity out of 100 to produce
        /// </summary>
        public double MixQuantity { get; set; }

        /// <summary>
        /// The number of producible items to surge
        /// </summary>
        public int RemainingProduciblesToSurge { get; set; }

        /// <summary>
        /// Current status of the CPG
        /// </summary>
        public CartonPropertyGroupStatuses Status { get; set; }

        /// <summary>
        /// Number of requests that currently are assigned to said carton property group expedited status
        /// </summary>
        public int NumberOfExpeditedRequests { get; set; }

        /// <summary>
        /// Number of requests that currently are assigned to said carton property group in stopped status
        /// </summary>
        public int NumberOfStoppedRequests { get; set; }

        /// <summary>
        /// Number of requests that currently are assigned to said carton property group in Last status
        /// </summary>
        public int NumberOfLastRequests { get; set; }

        /// <summary>
        /// Number of requests that currently are assigned to said carton property group in normal status
        /// </summary>
        public int NumberOfNormalRequests { get; set; }

        /// <summary>
        /// Number of requests that currently are assigned to said carton property group
        /// </summary>
        public int NumberOfRequests { get; set; }

        public int SurgeCount { get; set; }
    }
}
