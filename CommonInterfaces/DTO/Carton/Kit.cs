using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.Carton
{
    [DebuggerDisplay("Kit:{CustomerUniqueId}, Status:{ProducibleStatus}, Id:{Id}")]
    public class Kit : BaseProducible
    {
        [JsonConstructor]
        public Kit()
        {
        }
        private ConcurrentList<IDisposable> wireups = new ConcurrentList<IDisposable>();
        private ConcurrentList<IProducible> itemsToProduce;
        private Guid producedOnMachineGroupId;
        private Guid produceOnMachineId;

        public override ProducibleTypes ProducibleType
        {
            get { return ProducibleTypes.Kit; }
        }

        public override Guid ProducedOnMachineGroupId
        {
            get
            {
                if (producedOnMachineGroupId == Guid.Empty)
                {
                    var pt =
                        ItemsToProduce.FirstOrDefault(p => p.ProducedOnMachineGroupId != Guid.Empty);
                    if (pt != null)
                    {
                        producedOnMachineGroupId = pt.ProducedOnMachineGroupId;
                    }
                }

                return producedOnMachineGroupId;
            }
            set
            {
                producedOnMachineGroupId = value;
                if (itemsToProduce != null)
                    itemsToProduce.ForEach(p => p.ProducedOnMachineGroupId = value);
            }
        }

        public override Guid ProduceOnMachineId
        {
            get
            {
                return produceOnMachineId;
            }
            set
            {
                produceOnMachineId = value;
                if (itemsToProduce != null)
                    itemsToProduce.ForEach(p => p.ProduceOnMachineId = value);

            }
        }

        public ConcurrentList<IProducible> ItemsToProduce
        {
            get
            {
                itemsToProduce = itemsToProduce ?? new ConcurrentList<IProducible>();                                
                return itemsToProduce;
            }
            set
            {
                itemsToProduce = value;
                itemsToProduce.ForEach(Wireup);
                UpdateMyRestrictions();
            }
        }

        public override ProducibleStatuses ProducibleStatus
        {
            get { return base.ProducibleStatus; }
            set
            {
                ItemsToProduce.ForEach(p => p.ProducibleStatus = value);
                base.ProducibleStatus = value;
            }
        }

        public void AddProducible(IProducible producible)
        {
            ItemsToProduce.Add(producible);
            Wireup(producible);
            UpdateMyRestrictions();
        }

        public void UpdateMyRestrictions()
        {
            Restrictions.ReplaceAll(ItemsToProduce.SelectMany(p => p.Restrictions).Distinct().ToList());
        }

        private void Wireup(IProducible producible)
        {
            //todo: should we be listening on the error and wiring up again?
            wireups.Add(producible.ProducibleStatusObservable.DurableSubscribe(ItemStatusChanged));
        }

        private void ItemStatusChanged(ProducibleStatuses status)
        {
            //todo: can we use the value in Enumeration to get the highest value to set the kit status
            IProducible producible = null;
            producible = itemsToProduce.FirstOrDefault(p => p.ProducibleStatus is ErrorProducibleStatuses);
            if (producible == null)
                producible = itemsToProduce.FirstOrDefault(p => p.ProducibleStatus == ProducibleStatuses.ProducibleRemoved);
            if (producible == null)
                producible = itemsToProduce.FirstOrDefault(p => p.ProducibleStatus is InProductionProducibleStatuses);
            if (producible == null)
                producible = itemsToProduce.FirstOrDefault(p => p.ProducibleStatus is NotInProductionProducibleStatuses
                                                                && p.ProducibleStatus != ProducibleStatuses.ProducibleCompleted
                                                                && p.ProducibleStatus != ProducibleStatuses.ProducibleRemoved);

            if (producible == null)
            {
                base.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                return;
            }
            //we should never have a null producible status at this point.
            if (ProducibleStatus != producible.ProducibleStatus)
            {
                base.ProducibleStatus = producible.ProducibleStatus;
            }
            else
            {
                //we always want to update the parent kit observable when an item in it's list gets changed.
                base.producibleStatusSubject.OnNext(base.ProducibleStatus);
            }
        }
    }
}