using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using System;
using System.Collections.Generic;

namespace PackNet.Common.Interfaces.DTO.Carton
{
    public class BoxLastProducible : BaseProducible, IProducibleWrapper
    {
        public BoxLastProducible()
        {
        }

        public BoxLastProducible(IProducible producible)
        {
            Producible = producible;
        }

        private IProducible producible;
        private Guid producedOnMachineGroupId;

        /// <summary>
        /// The production group that should produce this item
        /// </summary>
        public string ProductionGroupAlias { get; set; }

        [BsonIgnore]
        public override string CustomerUniqueId
        {
            get
            {
                if (producible != null)
                    return producible.CustomerUniqueId;
                return string.Empty;
            }
            set { var v = value; }
        }

        /// <summary>
        /// Time the carton was triggered for creation
        /// </summary>
        public DateTime? TimeTriggered { get; set; }

        /// <summary>
        /// Gets or sets the producible.
        /// </summary>
        /// <value>
        /// The producible.
        /// </value>
        public IProducible Producible
        {
            get { return producible; }
            set
            {
                producible = value;
                Id = value.Id;
                CustomerUniqueId = producible.CustomerUniqueId;
            }
        }

        public override Guid ProducedOnMachineGroupId
        {
            get
            {
                if (producedOnMachineGroupId == Guid.Empty
                    && producible != null
                    && producible.ProducedOnMachineGroupId != Guid.Empty)
                {
                    producedOnMachineGroupId = producible.ProducedOnMachineGroupId;
                }
                return producedOnMachineGroupId;
            }
            set
            {
                producedOnMachineGroupId = value;
                if (producible != null)
                    producible.ProducedOnMachineGroupId = value;
            }
        }

        public override ProducibleTypes ProducibleType
        {
            get { return ProducibleTypes.BoxLastProducible; }
        }
    }
}