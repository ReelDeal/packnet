﻿namespace PackNet.Common.Interfaces.DTO.Carton
{
    using System;
    using System.Runtime.Serialization;

    public interface ICarton : IPacksizeCarton
    {
        /// <summary>
        /// Required corrugate quality, this should be backed by a value in the Restrictions.
        /// </summary>
        int? CorrugateQuality { get; set; }
        
    }
}
