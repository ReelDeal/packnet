﻿using Newtonsoft.Json;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.DTO.Carton
{
    using System.Collections.Generic;
    using Corrugates;
    using PackagingDesigns;
    
    /// <summary>
    /// Represents a carton that can be used on a PacksizeCutCreaseMachine
    /// </summary>
    public interface IPacksizeCarton: IProducible
    {
        /// <summary>
        /// Gets or sets the article identifier.
        /// </summary>
        /// <value>
        /// The article identifier.
        /// </value>
        //TODO: review this when implementing Articles for Kits: Feature 1808
        string ArticleId { get; set; }

        /// <summary>
        /// Track number to create the carton on
        /// </summary>
        int TrackNumber { get; set; }

        /// <summary>
        /// Id of the design
        /// </summary>
        int DesignId { get; set; }

        /// <summary>
        /// Carton laid out on corrugate
        /// </summary>
        CartonOnCorrugate CartonOnCorrugate { get; set; }

        /// <summary>
        /// Length
        /// </summary>
        /// <returns></returns>
        double Length { get; set; }

        /// <summary>
        /// Width
        /// </summary>
        /// <returns></returns>
        double Width { get; set; }

        /// <summary>
        /// Height
        /// </summary>
        /// <returns></returns>
        double Height { get; set; }

        /// <summary>
        /// Variables for the design
        /// </summary>
        /// <returns></returns>
        IDictionary<string, double> XValues { get; set; }

        OrientationEnum? Rotation { get; set; }
    }
}