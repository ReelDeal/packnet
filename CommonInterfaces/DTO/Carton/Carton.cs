﻿using System.Linq;

using MongoDB.Bson.Serialization.Attributes;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using System;
using System.Collections.Generic;
using System.Diagnostics;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;

namespace PackNet.Common.Interfaces.DTO.Carton
{
    [DebuggerDisplay("Carton:{CustomerUniqueId}, Design: {DesignId}, LxWxH: {Length}x{Width}x{Height}")]
    [BsonIgnoreExtraElements]
    public class Carton : BaseProducible, ICarton, IEqualityComparer<ICarton>
    {
        private double length, width, height;
        /// <summary>
        /// The quantity
        /// </summary>
        private int quantity;
        /// <summary>
        /// Initializes a new instance of the <see cref="Carton"/> class.
        /// </summary>
        public Carton()
        {
            Created = DateTime.UtcNow;
            XValues = new Dictionary<string, double>();
        }

        public override ProducibleTypes ProducibleType { get { return ProducibleTypes.Carton; } }

        /// <summary>
        /// Gets or sets the article identifier.
        /// </summary>
        /// <value>
        /// The article identifier.
        /// </value>
        public string ArticleId { get; set; }
        /// <summary>
        /// Track number to create the carton on
        /// </summary>
        public int TrackNumber { get; set; }
        /// <summary>
        /// Carton laid out on corrugate
        /// </summary>
        public CartonOnCorrugate CartonOnCorrugate { get; set; }

        public double Length
        {
            get
            {
                return length;
            }

            set
            {
                length = value > 0 ? value : 0;
            }
        }

        public double Width
        {
            get
            {
                return width;
            }

            set
            {
                width = value > 0 ? value : 0;
            }
        }

        public double Height
        {
            get
            {
                return height;
            }

            set
            {
                height = value > 0 ? value : 0;
            }
        }

        public IDictionary<string, double> XValues { get; set; }

        /// <summary>
        /// Required corrugate quality
        /// </summary>
        public int? CorrugateQuality
        {
            get
            {
                var x= Restrictions.OfType<MustProduceOnCorrugateWithPropertiesRestriction>().FirstOrDefault();
                return x == null ? null : x.Quality;
            }

            set
            {
                var x = Restrictions.OfType<MustProduceOnCorrugateWithPropertiesRestriction>().FirstOrDefault();
                if (x != null)
                {
                    Restrictions.Remove(x);
                }

                if (value != null)
                {
                    Restrictions.Add(new MustProduceOnCorrugateWithPropertiesRestriction { 
                        Quality = value, 
                        Width = x != null ? x.Width : null,  
                        Thickness = x != null? x.Thickness:null
                    });
                }
            }
        }

        /// <summary>
        /// Requried rotation
        /// </summary>
        public OrientationEnum? Rotation
        {
            get
            {
                var x = Restrictions.OfType<MustProduceWithSpecifiedRotationRestriction>().FirstOrDefault();
                return x == null ? null : x.Orientation;
            }
            set
            {
                var x = Restrictions.OfType<MustProduceWithSpecifiedRotationRestriction>().FirstOrDefault();
                
                if (x != null)
                    Restrictions.Remove(x);

                if (value != null)
                    Restrictions.Add(new MustProduceWithSpecifiedRotationRestriction { Orientation = value });
            }
        }
        
        /// <summary>
        /// Id of the design
        /// </summary>
        public int DesignId { get; set; }

        /// <summary>
        /// Number of items to produce
        /// </summary>
        public int Quantity
        {
            get
            {
                return quantity < 1 ? 1 : quantity;
            }
            set
            {
                quantity = value;
            }
        }

        /// <summary>
        /// The production group that should produce this item
        /// </summary>
        public string ProductionGroupAlias { get; set; }
        
        /// <summary>
        /// Update the id for this carton carton to the id of the original instance of this carton.
        /// The original instance is found by matching on serial number. Normally used when the
        /// original instance of this carton has been persisted to the database.
        /// </summary>
        /// <param name="originalId">Id of the original carton carton</param>
        public void UpdateId(Guid originalId)
        {
            Id = originalId;
        }

        /// <summary>
        /// Determines whether the specified objects are equal.
        /// </summary>
        /// <param name="x">The first object of type <paramref name="PackNet.Common.Interfaces.DTO.Carton.ICarton" /> to compare.</param>
        /// <param name="y">The second object of type <paramref name="PackNet.Common.Interfaces.DTO.Carton.ICarton" /> to compare.</param>
        /// <returns>
        /// true if the specified objects are equal; otherwise, false.
        /// </returns>
        public bool Equals(ICarton x, ICarton y)
        {
            return x.CustomerUniqueId == y.CustomerUniqueId;
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public int GetHashCode(ICarton obj)
        {
            return obj.Id.GetHashCode();
        }
        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.
        /// </returns>
        public override bool Equals(IProducible other)
        {
            return (other is Carton) && CustomerUniqueId == ((Carton)other).CustomerUniqueId;
        }
    }
}