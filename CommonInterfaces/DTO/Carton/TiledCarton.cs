﻿using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.Carton
{
    [BsonIgnoreExtraElements]
    public class TiledCarton : BaseProducible, ICarton
    {
        private readonly ConcurrentList<ICarton> tiles = new ConcurrentList<ICarton>();
        private string customerId = string.Empty;
        private Guid producedOnMachineGroupId;
        private Guid produceOnMachineId;

        public override ProducibleTypes ProducibleType
        {
            get { return ProducibleTypes.TiledCarton; }
        }

        public override string CustomerUniqueId
        {
            get { return customerId; }
            set { customerId = value; }
        }

        public override Guid ProducedOnMachineGroupId
        {
            get { return producedOnMachineGroupId; }
            set
            {
                producedOnMachineGroupId = value;
                tiles.ForEach(p => p.ProducedOnMachineGroupId = value);
            }
        }

        public override Guid ProduceOnMachineId
        {
            get { return produceOnMachineId; }
            set
            {
                produceOnMachineId = value;
                tiles.ForEach(p => p.ProduceOnMachineId = value);
            }
        }

        public override ProducibleStatuses ProducibleStatus
        {
            get { return base.ProducibleStatus; }
            set
            {
                tiles.ForEach(t => t.ProducibleStatus = value);
                base.ProducibleStatus = value;
            }
        }

        public IEnumerable<ICarton> Tiles
        {
            get { return tiles; }
        }

        public void AddTiles(IEnumerable<ICarton> cartons)
        {
            tiles.AddRange(cartons);
            UpdateMyRestrictions();
        }

        private void UpdateMyRestrictions()
        {
            Restrictions.ReplaceAll(tiles.SelectMany(p => p.Restrictions).Distinct().ToList());
        }

        public int TrackNumber { get; set; }

        public int DesignId
        {
            get { return tiles.Any() ? tiles.First().DesignId : 0; }
            set { }
        }

        public CartonOnCorrugate CartonOnCorrugate
        {
            get { return tiles.Any() ? tiles.First().CartonOnCorrugate : null; }
            set { }
        }

        public double Length
        {
            get { return tiles.Any() ? tiles.First().Length : 0; }
            set { }
        }

        public double Width
        {
            get { return tiles.Any() ? tiles.First().Width : 0; }
            set { }
        }

        public double Height
        {
            get { return tiles.Any() ? tiles.First().Height : 0; }
            set { }
        }

        public IDictionary<string, double> XValues
        {
            get { return tiles.Any() ? tiles.First().XValues : new Dictionary<string, double>(); }
            set { }
        }

        public int? CorrugateQuality
        {
            get { return tiles.Any() ? tiles.First().CorrugateQuality: null; }
            set { }
        }

        public string ArticleId { get; set; }

        public OrientationEnum? Rotation
        {
            get { return tiles.Any() ? tiles.First().Rotation : null; }
            set { }
        }
    }
}
