﻿namespace PackNet.Common.Interfaces.DTO.Carton
{
    using System;

    using Corrugates;

    [Serializable]
    public class LongHeadValidation
    {
        public LongHeadValidation()
        {
        }

        public LongHeadValidation(Guid machineId, Corrugate corrugate, int numberOfTiles, bool isValid)
        {
            MachineId = machineId;
            Corrugate = corrugate;
            NumberOfTiles = numberOfTiles;
            IsValid = isValid;
        }

        public Guid MachineId { get; private set; }

        public Corrugate Corrugate { get; private set; }

        public int NumberOfTiles { get; private set; }

        public bool IsValid { get; set; }
    }
}
