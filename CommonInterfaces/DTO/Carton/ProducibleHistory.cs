﻿using System;
using PackNet.Common.Interfaces.Enums.ProducibleStates;

namespace PackNet.Common.Interfaces.DTO.Carton
{
    /// <summary>
    /// Represents a particular state that a producible entered
    /// </summary>
    public class ProducibleHistory
    {
        public ProducibleHistory()
        {
        }

        public ProducibleHistory(ProducibleStatuses previousState, ProducibleStatuses newState, string notes = null)
        {
            PreviousStatus = previousState;
            NewStatus = newState;
            OccuredOn = DateTime.UtcNow;
            Notes = notes;
        }

        public ProducibleStatuses PreviousStatus { get; set; }
        public ProducibleStatuses NewStatus { get; set; }
        public DateTime OccuredOn { get; set; }
        public string Notes { get; set; }
    }
}