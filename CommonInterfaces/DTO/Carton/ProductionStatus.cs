﻿using System;
using System.Runtime.Serialization;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Carton
{
    

    [Serializable]
    [DataContract]
    public class ProductionStatus
    {
        public ProductionStatus()
        {
            EventTime = DateTime.UtcNow;
            Status = ProductionStatusEnum.Queued;
        }

        public ProductionStatus(ProductionStatusEnum status)
        {
            EventTime = DateTime.UtcNow;
            Status = status;
        }

        [DataMember]
        public virtual ProductionStatusEnum Status { get; private set; }

        [DataMember]
        public DateTime EventTime { get; private set; }
    }
}
