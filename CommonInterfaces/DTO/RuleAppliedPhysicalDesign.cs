﻿using System.Collections.Generic;
using System.Linq;

namespace PackNet.Common.Interfaces.DTO
{
    using System;

    public class RuleAppliedPhysicalDesign : PhysicalDesign
    {
        private readonly List<WasteArea> wasteAreas;

        public IEnumerable<WasteArea> WasteAreas { get { return wasteAreas; } }

        public RuleAppliedPhysicalDesign()
        {
            wasteAreas = new List<WasteArea>();
        }

        public RuleAppliedPhysicalDesign(PhysicalDesign physicalDesign, IEnumerable<WasteArea> wasteAreas, bool shouldCopyLines = true)
        {
            this.wasteAreas = wasteAreas.ToList();
            Populate(physicalDesign, shouldCopyLines);
        }

        public RuleAppliedPhysicalDesign(PhysicalDesign physicalDesign, bool shouldCopyLines = true)
            : this(physicalDesign, new List<WasteArea>(), shouldCopyLines)
        {
        }

        public void AddWasteArea(WasteArea area)
        {
            wasteAreas.Add(area);
        }

        private void Populate(PhysicalDesign d, bool shouldCopyLines)
        {
            Length = d.Length;
            Width = d.Width;

            if (d.PerforationParameters != null)
            {
                PerforationParameters = new PerforationParameters
                {
                    CreaseLength = d.PerforationParameters.CreaseLength,
                    CutLength = d.PerforationParameters.CutLength,
                };
            }

            if (shouldCopyLines)
            {
                foreach (var l in d.Lines)
                    this.lines.Add(new PhysicalLine(new PhysicalCoordinate(l.StartCoordinate.X, l.StartCoordinate.Y),
                        new PhysicalCoordinate(l.EndCoordinate.X, l.EndCoordinate.Y), l.Type));
            }
            //d.Lines.ForEach(l => Add(new PhysicalLine(new PhysicalCoordinate(l.StartCoordinate.X, l.StartCoordinate.Y), new PhysicalCoordinate(l.EndCoordinate.X, l.EndCoordinate.Y), l.Type)));
            //d.Lines.ForEach(l => lines.Add(new PhysicalLine(new PhysicalCoordinate(l.StartCoordinate.X, l.StartCoordinate.Y), new PhysicalCoordinate(l.EndCoordinate.X, l.EndCoordinate.Y), l.Type)));
        }
    }
}
