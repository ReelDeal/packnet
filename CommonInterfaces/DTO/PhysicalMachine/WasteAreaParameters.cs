﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    public class WasteAreaParameters
    {
        public bool Enable { get; set; }
        public MicroMeter MaximumWasteWidth { get; set; }
        public MicroMeter MaximumWasteLength { get; set; }
        public MicroMeter MinimumWasteWidth { get; set; }
        public MicroMeter MinimumWasteLength { get; set; }
    }
}
