﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    public enum ToolType
    {
        Cut,
        Crease
    }
}
