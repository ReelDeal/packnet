﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    public class TrackSideSteeringCompensation
    {
        public TrackAlignment Alignment { get; set; }

        public int SpeedPercentage { get; set; }

        public double Compensation { get; set; }
    }
}
