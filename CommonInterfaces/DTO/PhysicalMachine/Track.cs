﻿using System.Linq;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    using System.Collections.Generic;

    public class Track
    {
        public int Number { get; set; }

        private readonly List<TrackSideSteeringCompensation> leftCompensations = new List<TrackSideSteeringCompensation>();

        private readonly List<TrackSideSteeringCompensation> rightCompensations = new List<TrackSideSteeringCompensation>(); 

        public MicroMeter LeftSensorPlateToCorrugate { get; set; }

        public MicroMeter RightSensorPlateToCorrugate { get; set; }

        public bool RightSideFixed { get; set; }

        public MicroMeter OutOfCorrugatePosition { get; set; }

        //public MicroMeter TrackOffset { get; set; }

        //public MicroMeter CorrugateWidth { get; set; } 
        
        //public MicroMeter CorrugateThickness { get; set; }
    
        public IEnumerable<TrackSideSteeringCompensation> LeftswordsensorCompensations
        {
            get { return leftCompensations; }
        }

        public IEnumerable<TrackSideSteeringCompensation> RightswordsensorCompensations
        {
            get { return rightCompensations; }
        }

        public void AddLeftSideSteeringCompensationItem(TrackSideSteeringCompensation trackSideSteeringCompensation)
        {
            leftCompensations.Add(trackSideSteeringCompensation);
        }

        public void UpdateLeftSideSteeringCompenstaionItem(int speedPercentage, double newCompensation)
        {
            var trackSideSteeringCompensation = leftCompensations.FirstOrDefault(c => c.SpeedPercentage == speedPercentage);
            if (trackSideSteeringCompensation != null)
            {
                trackSideSteeringCompensation.Compensation = newCompensation;
            }
        }

        public void AddRightSideSteeringCompensationItem(TrackSideSteeringCompensation trackSideSteeringCompensation)
        {
            rightCompensations.Add(trackSideSteeringCompensation);
        }

        public void UpdateRightSideSteeringCompenstaionItem(int speedPercentage, double newCompensation)
        {
            var trackSideSteeringCompensation = leftCompensations.FirstOrDefault(c => c.SpeedPercentage == speedPercentage);
            if (trackSideSteeringCompensation != null)
            {
                trackSideSteeringCompensation.Compensation = newCompensation;
            }
        }
    }
}
