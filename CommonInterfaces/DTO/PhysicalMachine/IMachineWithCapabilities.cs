﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    public interface IMachineWithCapabilities
    {
        IEnumerable<ICapability> CurrentCapabilities { get; }

        void AddOrUpdateCapabilities(IEnumerable<ICapability> capabilities);

        void RemoveCapabilitiesOfType(Type type);
    }
}
