﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    public enum TrackAlignment
    {
        Left,
        Right
    }
}
