﻿using System;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{

    public abstract class LongHead
    {
        public MicroMeter LeftSideToTool { get; set; }

        public MicroMeter Position { get; set; }

        public MicroMeter MinimumPosition { get; set; }

        public MicroMeter MaximumPosition { get; set; }

        public MicroMeter Width { get; set; }
        
        public int Number { get; set; }

        public MicroMeter CurrentLeftEdge
        {
            get
            {
                return this.Position - this.LeftSideToTool;
            }
        }

        public MicroMeter CurrentRightEdge
        {
            get
            {
                return this.Position + (this.Width - this.LeftSideToTool);
            }
        }

        public MicroMeter GetRightEdgeForPosition(MicroMeter position)
        {
            return position + (this.Width - this.LeftSideToTool);
        }

        public MicroMeter GetLeftEdgeForPosition(MicroMeter position)
        {
            return position - this.LeftSideToTool;
        }
    }
}

