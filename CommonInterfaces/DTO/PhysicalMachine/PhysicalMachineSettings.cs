﻿using System.Net;
using System.Xml;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;

    public abstract class PhysicalMachineSettings
    {
        protected PhysicalMachineSettings()
        {
            Unit = "inch";
        }

        protected PhysicalMachineSettings(XmlNode machineDocument)
        {
            Unit = GetUnit(machineDocument);
            CodeGeneratorSettings = GetCodeGeneratorSettings(machineDocument);
        }

        public IPEndPoint EventNotifierIp { get; set; }

        public string FileName { get; set; }

        public string Unit { get; set; }

        public bool OnTheFlyActive { get; set; }
                   
        public CodeGeneratorSettings CodeGeneratorSettings { get; set; }

        public abstract void UpdateToFile();

        protected static string GetUnit(XmlNode machineDocument)
        {
            var node = machineDocument.SelectSingleNode("machine/Unit");
            return node == null ? "inch" : node.InnerText;
        }

        private CodeGeneratorSettings GetCodeGeneratorSettings(XmlNode machineDocument)
        {
            return new CodeGeneratorSettings()
            {
                MinimumLineDistanceToCorrugateEdge = GetUnit(machineDocument) == "mm" ? 5 : 0.2,
                MinimumLineLength = GetUnit(machineDocument) == "mm" ? 2 : 0.078d,
                PerforationSafetyDistance = GetUnit(machineDocument) == "mm" ? 40 : 1.57,
            };
        }

        protected static bool GetOnTheFlyActive(XmlDocument machineDocument)
        {
            XmlNode node = machineDocument.SelectSingleNode("machine/onthefly");

            if (node == null)
            {
                return true;
            }

            return bool.Parse(node.Attributes["active"].Value);
        }

        protected void SetOnTheFlyActive(XmlDocument machineDocument)
        {
            XmlNode node;

            node = machineDocument.SelectSingleNode("machine/onthefly");

            if (node != null)
            {
                //Active
                node.Attributes["active"].Value = OnTheFlyActive.ToString().ToLower();
            }
        }
    }
}