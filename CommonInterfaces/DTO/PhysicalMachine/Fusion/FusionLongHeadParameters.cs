﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion
{
    public class FusionLongHeadParameters : LongHeadParameters<FusionLongHead>
    {
        public double ConnectionDelay { get; set; }

        public MicroMeter LeftSideToSensorPin { get; set; }

        public double PositioningAcceleration { get; set; }

        public double PositioningSpeed { get; set; }
    }
}
