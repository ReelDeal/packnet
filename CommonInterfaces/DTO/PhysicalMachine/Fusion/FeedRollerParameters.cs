﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion
{
    public abstract class FeedRollerParameters
    {
        public MicroMeter OutFeedLength { get; set; }
    }
}
