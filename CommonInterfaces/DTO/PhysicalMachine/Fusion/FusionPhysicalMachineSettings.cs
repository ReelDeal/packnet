﻿using System;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Xml;

using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion
{
    public class FusionPhysicalMachineSettings : PhysicalMachineSettings
    {
        public FusionPhysicalMachineSettings()
        {
            EventNotifierIp = new IPEndPoint(IPAddress.Loopback, 9000);
            CrossHeadParameters = new FusionCrossHeadParameters();
            LongHeadParameters = new FusionLongHeadParameters();
            FeedRollerParameters = new FusionFeedRollerParameters();
            TrackParameters = new FusionTracksParameters();
        }

        public FusionPhysicalMachineSettings(XmlDocument machineDocument) : base(machineDocument)
        {
        }

        public FusionLongHeadParameters LongHeadParameters { get; set; }

        public FusionFeedRollerParameters FeedRollerParameters { get; set; }

        public FusionTracksParameters TrackParameters { get; set; }

        public FusionCrossHeadParameters CrossHeadParameters { get; set; }

        public static FusionPhysicalMachineSettings CreateFromFile(IPEndPoint callbackIP, string fileName)
        {
            var machineDocument = LoadDocument(fileName);

            var machine = new FusionPhysicalMachineSettings(machineDocument)
            {
                EventNotifierIp = callbackIP,
                FileName = fileName,
                OnTheFlyActive = GetOnTheFlyActive(machineDocument),
                CrossHeadParameters = GetCrossHeadParameters(machineDocument),
                LongHeadParameters = GetLongHeadParameters(machineDocument),
                FeedRollerParameters = GetFeedRollerParameters(machineDocument),
                TrackParameters = GetTracksParameters(machineDocument)
            };

            return machine;
        }

        public override void UpdateToFile()
        {
            var machineDocument = LoadDocument(FileName);

            SetCrossHeadParameters(machineDocument);
            SetFeedRollerParameters(machineDocument);
            SetLongHeadParameters(machineDocument);
            SetOnTheFlyActive(machineDocument);
            SetTracksParameters(machineDocument);

            machineDocument.Save(FileName);
        }

        private static XmlDocument LoadDocument(string fileName)
        {
            var document = new XmlDocument();
            document.Load(fileName);
            return document;
        }

        private static FusionCrossHeadParameters GetCrossHeadParameters(XmlDocument machineDocument)
        {
            var node = machineDocument.SelectSingleNode("machine/crosshead");
            return new FusionCrossHeadParameters
            {
                Acceleration = int.Parse(node.Attributes["acceleration"].Value),
                CreaseCompensation = double.Parse(node.Attributes["creaseCompensation"].Value, CultureInfo.InvariantCulture),
                CreaseWheelOffset = double.Parse(node.Attributes["creaseWheelOffset"].Value, CultureInfo.InvariantCulture),
                CutCompensation = double.Parse(node.Attributes["cutCompensation"].Value, CultureInfo.InvariantCulture),
                HomingPosition = double.Parse(node.Attributes["homingPosition"].Value, CultureInfo.InvariantCulture),
                KnifeActivationDelay = double.Parse(node.Attributes["knifeActivationDelay"].Value, CultureInfo.InvariantCulture),
                KnifeDeactivationDelay = double.Parse(node.Attributes["knifeDeactivationDelay"].Value, CultureInfo.InvariantCulture),
                LeftSideToTool = double.Parse(node.Attributes["leftSideToTool"].Value, CultureInfo.InvariantCulture),
                LongHeadSensorToTool = double.Parse(node.Attributes["longHeadSensorToTool"].Value, CultureInfo.InvariantCulture),
                MaximumAcceleration = int.Parse(node.Attributes["maximumAcceleration"].Value),
                MaximumDeceleration = int.Parse(node.Attributes["maximumDeceleration"].Value),
                MaximumPosition = double.Parse(node.Attributes["maximumPosition"].Value, CultureInfo.InvariantCulture),
                MaximumSpeed = int.Parse(node.Attributes["maximumSpeed"].Value),
                MinimumLineLength = double.Parse(node.Attributes["minimumLineLength"].Value, CultureInfo.InvariantCulture),
                MinimumPosition = double.Parse(node.Attributes["minimumPosition"].Value, CultureInfo.InvariantCulture),
                Speed = int.Parse(node.Attributes["speed"].Value),
                TrackSensorToTool = double.Parse(node.Attributes["trackSensorToTool"].Value, CultureInfo.InvariantCulture),
                Width = double.Parse(node.Attributes["width"].Value, CultureInfo.InvariantCulture)
            };
        }

        private static FusionLongHeadParameters GetLongHeadParameters(XmlDocument machineDocument)
        {
            XmlNode node = machineDocument.SelectSingleNode("machine/longheads");

            var longHeadParameters = new FusionLongHeadParameters {
                ConnectionDelay = double.Parse(node.Attributes["connectionDelay"].Value, CultureInfo.InvariantCulture),
                LeftSideToSensorPin =
                    double.Parse(node.Attributes["leftSideToSensorPin"].Value, CultureInfo.InvariantCulture),
                MaximumPosition = double.Parse(node.Attributes["maximumPosition"].Value, CultureInfo.InvariantCulture),
                MinimumPosition = double.Parse(node.Attributes["minimumPosition"].Value, CultureInfo.InvariantCulture),
                PositioningAcceleration = int.Parse(node.Attributes["positioningAcceleration"].Value),
                PositioningSpeed = int.Parse(node.Attributes["positioningSpeed"].Value),
                LongheadWidth = double.Parse(node.Attributes["width"].Value, CultureInfo.InvariantCulture)
            };

            foreach (XmlNode lhNode in machineDocument.SelectNodes("machine/longheads/longhead"))
            {
                var longHead = new FusionLongHead {
                    LeftSideToTool =
                        double.Parse(lhNode.Attributes["leftSideToTool"].Value, CultureInfo.InvariantCulture),
                    Number = int.Parse(lhNode.Attributes["number"].Value),
                    Type = ParseToolType(lhNode)
                };

                longHeadParameters.AddLongHead(longHead);
            }

            return longHeadParameters;
        }

        private static FusionFeedRollerParameters GetFeedRollerParameters(XmlDocument machineDocument)
        {
            var node = machineDocument.SelectSingleNode("machine/feedroller");
            return new FusionFeedRollerParameters {
                Acceleration = int.Parse(node.Attributes["acceleration"].Value),
                CleanCutLength = double.Parse(node.Attributes["cleancutlength"].Value, CultureInfo.InvariantCulture),
                LoweringOffset = double.Parse(node.Attributes["loweringOffset"].Value, CultureInfo.InvariantCulture),
                OutFeedLength = double.Parse(node.Attributes["outfeedLength"].Value, CultureInfo.InvariantCulture),
                PercentMovementSpeed = int.Parse(node.Attributes["speed"].Value),
                ToolsGearRatio = double.Parse(node.Attributes["toolsGearRatio"].Value, CultureInfo.InvariantCulture)
            };
        }

        private static FusionTracksParameters GetTracksParameters(XmlDocument machineDocument)
        {
            var tracksParameterNode = machineDocument.SelectSingleNode("machine/tracks");
            var tracksParameters = new FusionTracksParameters {
                CorrugateWidthTolerance = double.Parse(tracksParameterNode.Attributes["corrugateWidthTolerance"].Value, CultureInfo.InvariantCulture),
                TrackActivateDelay = double.Parse(tracksParameterNode.Attributes["trackActivateDelay"].Value, CultureInfo.InvariantCulture),
                TrackLatchDistance = double.Parse(tracksParameterNode.Attributes["trackLatchDistance"].Value, CultureInfo.InvariantCulture),
                TrackOffsetTolerance = double.Parse(tracksParameterNode.Attributes["trackOffsetTolerance"].Value, CultureInfo.InvariantCulture),
                TrackSideSteeringTolerance = double.Parse(tracksParameterNode.Attributes["trackSideSteeringTolerance"].Value, CultureInfo.InvariantCulture)
            };

            foreach (XmlNode node in machineDocument.SelectNodes("machine/tracks/track"))
            {
                var track = new Track {
                    LeftSensorPlateToCorrugate =
                        double.Parse(node.Attributes["leftSensorPlateToCorrugate"].Value, CultureInfo.InvariantCulture),
                    Number = int.Parse(node.Attributes["number"].Value),
                    OutOfCorrugatePosition =
                        double.Parse(node.Attributes["outOfCorrugatePosition"].Value, CultureInfo.InvariantCulture),
                    RightSensorPlateToCorrugate =
                        double.Parse(node.Attributes["rightSensorPlateToCorrugate"].Value, CultureInfo.InvariantCulture),
                    RightSideFixed = bool.Parse(node.Attributes["rightSideFixed"].Value)
                };

                foreach (XmlNode compensationsnode in node.SelectNodes("leftswordsensorCompensations/compensation"))
                {
                    track.AddLeftSideSteeringCompensationItem(new TrackSideSteeringCompensation {
                        Alignment = TrackAlignment.Left,
                        Compensation = double.Parse(compensationsnode.FirstChild.Value, CultureInfo.InvariantCulture),
                        SpeedPercentage = int.Parse(compensationsnode.Attributes["speed"].Value)
                    });
                }

                foreach (XmlNode compensationsnode in node.SelectNodes("rightswordsensorCompensations/compensation"))
                {
                    track.AddRightSideSteeringCompensationItem(new TrackSideSteeringCompensation {
                        Alignment = TrackAlignment.Right,
                        Compensation = double.Parse(compensationsnode.FirstChild.Value, CultureInfo.InvariantCulture),
                        SpeedPercentage = int.Parse(compensationsnode.Attributes["speed"].Value)
                    });
                }

                tracksParameters.AddTrack(track);
            }

            return tracksParameters;
        }

        private void SetCrossHeadParameters(XmlDocument machineDocument)
        {
            XmlNode node;

            node = machineDocument.SelectSingleNode("machine/crosshead");

            //Acceleration
            node.Attributes["acceleration"].Value =
                CrossHeadParameters.Acceleration.ToString(CultureInfo.InvariantCulture);

            //Crease Compensation
            node.Attributes["creaseCompensation"].Value =
                CrossHeadParameters.CreaseCompensation.ToString();
            
            //Crease Wheel Offset
            node.Attributes["creaseWheelOffset"].Value =
                CrossHeadParameters.CreaseWheelOffset.ToString();

            //Cut Compensation
            node.Attributes["cutCompensation"].Value =
                CrossHeadParameters.CutCompensation.ToString();

            //Homing Position
            node.Attributes["homingPosition"].Value =
                CrossHeadParameters.HomingPosition.ToString();

            //Knife Activation Delay
            node.Attributes["knifeActivationDelay"].Value =
                CrossHeadParameters.KnifeActivationDelay.ToString(CultureInfo.InvariantCulture);

            //Knife Deactivation Delay
            node.Attributes["knifeDeactivationDelay"].Value =
                CrossHeadParameters.KnifeDeactivationDelay.ToString(CultureInfo.InvariantCulture);

            //Left Side To Tool
            node.Attributes["leftSideToTool"].Value =
                CrossHeadParameters.LeftSideToTool.ToString();

            //Longhead Sensor To Tool
            node.Attributes["longHeadSensorToTool"].Value =
                CrossHeadParameters.LongHeadSensorToTool.ToString();

            //Maximum Acceleration
            node.Attributes["maximumAcceleration"].Value =
                CrossHeadParameters.MaximumAcceleration.ToString(CultureInfo.InvariantCulture);

            //Maximum Deceleration
            node.Attributes["maximumDeceleration"].Value =
                CrossHeadParameters.MaximumDeceleration.ToString(CultureInfo.InvariantCulture);

            //Maximum Position
            node.Attributes["maximumPosition"].Value =
                CrossHeadParameters.MaximumPosition.ToString();

            //Maximum Speed
            node.Attributes["maximumSpeed"].Value =
                CrossHeadParameters.MaximumSpeed.ToString(CultureInfo.InvariantCulture);

            //Minimum Line Length
            node.Attributes["minimumLineLength"].Value =
                CrossHeadParameters.MinimumLineLength.ToString();

            //Minimum Position
            node.Attributes["minimumPosition"].Value =
                CrossHeadParameters.MinimumPosition.ToString();

            //Speed
            node.Attributes["speed"].Value =
                CrossHeadParameters.Speed.ToString(CultureInfo.InvariantCulture);

            //Track Sensor To Tool
            node.Attributes["trackSensorToTool"].Value =
                CrossHeadParameters.TrackSensorToTool.ToString();

            //Width
            node.Attributes["width"].Value =
                CrossHeadParameters.Width.ToString();
        }

        private void SetFeedRollerParameters(XmlDocument machineDocument)
        {
            XmlNode node;

            node = machineDocument.SelectSingleNode("machine/feedroller");

            //Acceleration
            node.Attributes["acceleration"].Value =
                FeedRollerParameters.Acceleration.ToString(CultureInfo.InvariantCulture);

            //Clean Cut Length
            node.Attributes["cleancutlength"].Value =
                FeedRollerParameters.CleanCutLength.ToString();

            //Lowering Offset
            node.Attributes["loweringOffset"].Value =
                FeedRollerParameters.LoweringOffset.ToString();

            //Out Feed Length
            node.Attributes["outfeedLength"].Value =
                FeedRollerParameters.OutFeedLength.ToString();

            //Percent Movement Speed
            node.Attributes["speed"].Value =
                FeedRollerParameters.PercentMovementSpeed.ToString(CultureInfo.InvariantCulture);

            //Tools Gear Ratio
            node.Attributes["toolsGearRatio"].Value =
                FeedRollerParameters.ToolsGearRatio.ToString(CultureInfo.InvariantCulture);
        }

        private void SetLongHeadParameters(XmlDocument machineDocument)
        {
            XmlNode node;
            XmlNodeList nodeList;

            node = machineDocument.SelectSingleNode("machine/longheads");

            //Connection Delay
            node.Attributes["connectionDelay"].Value =
                LongHeadParameters.ConnectionDelay.ToString(CultureInfo.InvariantCulture);

            //Left Side to Sensor Pin
            node.Attributes["leftSideToSensorPin"].Value =
                LongHeadParameters.LeftSideToSensorPin.ToString();

            //Long Head
            nodeList = machineDocument.SelectNodes("machine/longheads/longhead");

            foreach (XmlNode nodeItem in nodeList)
            {
                var longHead =
                    LongHeadParameters.LongHeads.ToList()[Int32.Parse(nodeItem.Attributes["number"].Value) - 1];

                nodeItem.Attributes["leftSideToTool"].Value = longHead.LeftSideToTool.ToString();
                nodeItem.Attributes["type"].Value = longHead.Type.ToString().ToLower();
            }

            //Maximum Position
            node.Attributes["maximumPosition"].Value =
                LongHeadParameters.MaximumPosition.ToString();

            //Minimum Position
            node.Attributes["minimumPosition"].Value =
                LongHeadParameters.MinimumPosition.ToString();

            //Positioning Acceleration
            node.Attributes["positioningAcceleration"].Value =
                LongHeadParameters.PositioningAcceleration.ToString(CultureInfo.InvariantCulture);

            //Positioning Speed
            node.Attributes["positioningSpeed"].Value =
                LongHeadParameters.PositioningSpeed.ToString(CultureInfo.InvariantCulture);

            //Width
            node.Attributes["width"].Value =
                LongHeadParameters.LongheadWidth.ToString();
        }

        private void SetTracksParameters(XmlDocument machineDocument)
        {
            XmlNode node;
            XmlNodeList nodeList;

            node = machineDocument.SelectSingleNode("machine/tracks");

            //Corrugate Width Tolerance
            node.Attributes["corrugateWidthTolerance"].Value =
                TrackParameters.CorrugateWidthTolerance.ToString();

            //Track
            nodeList = machineDocument.SelectNodes("machine/tracks/track");

            foreach (XmlNode nodeItem in nodeList)
            {
                var track =
                    TrackParameters.Tracks.ToList()[Int32.Parse(nodeItem.Attributes["number"].Value) - 1];

                nodeItem.Attributes["leftSensorPlateToCorrugate"].Value = track.LeftSensorPlateToCorrugate.ToString();
                nodeItem.Attributes["outOfCorrugatePosition"].Value = track.OutOfCorrugatePosition.ToString();
                nodeItem.Attributes["rightSensorPlateToCorrugate"].Value = track.RightSensorPlateToCorrugate.ToString();
                nodeItem.Attributes["rightSideFixed"].Value = track.RightSideFixed.ToString().ToLower();
            }

            //Track Activate Delay
            node.Attributes["trackActivateDelay"].Value =
                TrackParameters.TrackActivateDelay.ToString(CultureInfo.InvariantCulture);

            //Track Latch Distance
            node.Attributes["trackLatchDistance"].Value =
                TrackParameters.TrackLatchDistance.ToString();

            //Track Offset Tolerance
            node.Attributes["trackOffsetTolerance"].Value =
                TrackParameters.TrackOffsetTolerance.ToString();

            //Track Side Steering Tolerance
            node.Attributes["trackSideSteeringTolerance"].Value =
                TrackParameters.TrackSideSteeringTolerance.ToString();
        }

        private static ToolType ParseToolType(XmlNode node)
        {
            if (node.Attributes["type"].Value == "cut")
            {
                return ToolType.Cut;
            }
            return ToolType.Crease;
        }

        public static bool IsFusionPhysicalMachineSettingsFilePath(string path, ILogger logger)
        {
            try
            {
                CreateFromFile(new IPEndPoint(1, 1), path);
                return true;
            }
            catch (Exception ex)
            {
               logger.Log(LogLevel.Warning, string.Format("Unable to parse the path: {0} as Fusion Physical Machine Settings file", path), ex.Message);
                return false;
            }
        }
    }
}
