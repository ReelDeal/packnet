﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion
{
    public class FusionCrossHeadParameters
    {
        public int Acceleration { get; set; }

        public MicroMeter CreaseCompensation { get; set; }

        public MicroMeter CreaseWheelOffset { get; set; }

        public MicroMeter CutCompensation { get; set; }

        public MicroMeter HomingPosition { get; set; }

        public double KnifeActivationDelay { get; set; }

        public double KnifeDeactivationDelay { get; set; }

        public MicroMeter LeftSideToTool { get; set; }

        public MicroMeter LongHeadSensorToTool { get; set; }

        public int MaximumAcceleration { get; set; }

        public int MaximumDeceleration { get; set; }

        public MicroMeter MaximumPosition { get; set; }

        public int MaximumSpeed { get; set; }

        public MicroMeter MinimumPosition { get; set; }

        public MicroMeter MinimumLineLength { get; set; }

        public MicroMeter Position { get; set; }

        public int Speed { get; set; }

        public MicroMeter TrackSensorToTool { get; set; }

        public MicroMeter Width { get; set; }
    }
}
