﻿using System.Collections.Generic;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion
{
    public class FusionTracksParameters
    {
        private readonly List<Track> tracks = new List<Track>();

        public FusionTracksParameters()
        {
            
        }

        protected FusionTracksParameters(List<Track> tracks)
        {
            this.tracks = tracks;
        }
        
        public MicroMeter CorrugateWidthTolerance { get; set; }

        public double TrackActivateDelay { get; set; }

        public MicroMeter TrackLatchDistance { get; set; }

        public MicroMeter TrackOffsetTolerance { get; set; }

        public IEnumerable<Track> Tracks
        {
            get { return tracks; }
        }

        public MicroMeter TrackSideSteeringTolerance { get; set; }

        public void AddTrack(Track track)
        {
            tracks.Add(track);
        }
    }
}
