﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion
{
    public class FusionFeedRollerParameters : FeedRollerParameters, IKinematicDataHolder
    {
        public MicroMeter CleanCutLength { get; set; }

        public MicroMeter LoweringOffset { get; set; }

        public double ToolsGearRatio { get; set; }

        public double PercentMovementSpeed { get; set; }

        public double MovementSpeed { get; set; }

        public double Acceleration { get; set; }

        public double PercentageAcceleration { get; set; }

        public double ActualMaximumMovementSpeed
        {
            get { return MovementSpeed * PercentMovementSpeed; }
        }

        public double ActualMaximumAcceleration
        {
            get { return Acceleration * this.PercentageAcceleration; }
        }

    }
}
