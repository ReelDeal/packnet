﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion
{
    public class FusionLongHead : LongHead
    {
        public ToolType Type { get; set; }
    }
}
