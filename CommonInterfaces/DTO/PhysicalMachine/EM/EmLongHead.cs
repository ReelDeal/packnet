﻿using System;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmLongHead : LongHead, ICloneable
    {

        public object Clone()
        {
            return new EmLongHead()
            {
                LeftSideToTool = this.LeftSideToTool,
                MaximumPosition = this.MaximumPosition,
                MinimumPosition = this.MinimumPosition,
                Position = this.Position,
                Width = this.Width,
                LeftSideToSensorPin = this.LeftSideToSensorPin,
                LeftSideToWasteSeparator = this.LeftSideToWasteSeparator,
                WasteSeparatorWidth = this.WasteSeparatorWidth
            };
        }

        public MicroMeter LeftSideToSensorPin { get; set; }

        public double ToolToSensorOffset { get; set; }

        public MicroMeter LeftSideToWasteSeparator { get; set; }

        public MicroMeter WasteSeparatorWidth { get; set; }

        public MicroMeter GetDistanceFromKnifeToEndOfWasteSeparator()
        {
            return LeftSideToWasteSeparator < LeftSideToTool
                ? LeftSideToTool - LeftSideToWasteSeparator
                : LeftSideToWasteSeparator + WasteSeparatorWidth - LeftSideToTool;
        }
    }
}

