﻿using PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmCrossHeadParameters
    {
        public double Position { get; set; }

        public MicroMeter CutCompensation { get; set; }

        public MicroMeter CreaseCompensation { get; set; }

        public MicroMeter MinimumLineLength { get; set; }
        public MicroMeter MaxHeadPosition { get; set; }
        public MicroMeter MinHeadPosition { get; set; }
        public double ConnectDelayOff { get; set; }
        public double ConnectDelayOn { get; set; }
        public double CreaseDelayOff { get; set; }
        public double CreaseDelayOn { get; set; }
        public double CutDelayOff { get; set; }
        public double CutDelayOn { get; set; }
        public int ReferenceQuantity { get; set; }
        public double ToolPositionToSensor { get; set; }
        public EmNormalMovementData MovementData { get; set; }

        public double OutOfPositionWarningThreshold { get; set; }
    }
}
