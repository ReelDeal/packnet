﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmLongHeadParameters : LongHeadParameters<EmLongHead>
    {
        public int LongheadConnectionDelay { get; private set; }

        public MicroMeter LongHeadYOffset { get; set; }

        public double ConnectDelayOffInSeconds { get { return ConnectDelayOff/1000d; } }

        public double ConnectDelayOnInSeconds { get { return ConnectDelayOn / 1000d; } }

        public double ConnectDelayOff { get; set; }

        public double ConnectDelayOn { get; set; }

        public double CreaseDelayOff { get; set; }

        public double CreaseDelayOn { get; set; }

        public double CutDelayOff { get; set; }
        
        public double CutDelayOn { get; set; }
        
        public double CreaseCompensation { get; set; }
        
        public double CutCompensation { get; set; }
        
        public double OutOfPositionWarningThreshold { get; set; }

        public MicroMeter LHInPosWindow { get; set; }

        public EmNormalMovementData MovementData { get; set; }

        public double LeftSideToWasteSeparator { get; set; }

        public double WasteSeparatorWidth { get; set; }

        public double WasteSeparatorActivationOffset { get; set; }

        public double WasteSeparatorDeactivationOffset { get; set; }

        public EmLongHeadParameters(IEnumerable<EmLongHead> longHeads, double maximumMovementSpeed, int percentMovementSpeed, double maximumAcceleration, int percentAcceleration,
            int longheadConnectionDelay, MicroMeter maximumPosition, MicroMeter minimumPosition, MicroMeter longheadWidth, MicroMeter longheadYOffset, MicroMeter leftSideToWasteSeparator, MicroMeter wasteSeparatorWidth)
        {

            MovementData = new EmNormalMovementData()
            {
                Speed = maximumMovementSpeed,
                Acceleration = maximumAcceleration,
                NormalMovement = new EmMovementData()
                {
                    SpeedInPercent = percentMovementSpeed,
                    AccelerationInPercent = percentAcceleration,
                }
            };

            ConnectDelayOff = longheadConnectionDelay / 2;
            ConnectDelayOn = longheadConnectionDelay / 2;
            LongheadConnectionDelay = longheadConnectionDelay;
            LongHeadYOffset = longheadYOffset;
            MaximumPosition = maximumPosition;
            MinimumPosition = minimumPosition;
            LongheadWidth = longheadWidth;
            LeftSideToWasteSeparator = leftSideToWasteSeparator;
            WasteSeparatorWidth = wasteSeparatorWidth;
            longHeads.ForEach(this.AddLongHead);
            SetWasteSeparatorOffsets();
        }

        private void SetWasteSeparatorOffsets()
        {
            longHeads.ForEach(
                lh =>
                {
                    lh.LeftSideToWasteSeparator = LeftSideToWasteSeparator;
                    lh.WasteSeparatorWidth = WasteSeparatorWidth;
                });
        }

        public EmLongHeadParameters()
        {

        }

        public EmLongHead GetPreviousLongHead(EmLongHead longHead)
        {
            return GetLongHeadByNumber(longHead.Number - 1);
        }

        public EmLongHead GetNextLongHead(EmLongHead longHead)
        {
            return GetLongHeadByNumber(longHead.Number + 1);
        }

        public EmLongHead GetLongHeadByNumber(int number)
        {
            if (number <= 0)
                return null;

            return number > this.longHeads.Count ? null : this.longHeads[number - 1];
        }
    }
}
