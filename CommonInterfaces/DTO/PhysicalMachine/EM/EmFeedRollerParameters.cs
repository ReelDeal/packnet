﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmFeedRollerParameters : FeedRollerParameters
    {
        public MicroMeter MaximumReverseDistance { get; set; }

        public double ToolsGearRatio { get; set; }
        
        public IEnumerable<EmRubberRollerStartingPosition> RubberRollerStartingPositions { get; set; }

        public EmRollerMovementData MovementData { get; set; }

        public EmFeedRollerParameters(double maximumMovementSpeed, int percentMovementSpeed, double maximumAcceleration, int percentageAcceleration, MicroMeter maximumReverseDistance, MicroMeter outFeedLength)
        {
            MovementData = new EmRollerMovementData()
            {
                Acceleration = maximumAcceleration,
                Speed = maximumMovementSpeed,
                NormalMovement = new EmMovementData()
                {
                    AccelerationInPercent = percentageAcceleration,
                    SpeedInPercent = percentMovementSpeed
                }
            };

            MaximumReverseDistance = maximumReverseDistance;
            OutFeedLength = outFeedLength;
        }

        public EmFeedRollerParameters()
        {
        }
    }
}
