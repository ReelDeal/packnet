﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmTrackParameters
    {
        public int TrackChangeDelay { get; set; }

        public double TrackServiceFeedingDistance { get; set; }

        public double CorrugateWidthTolerance { get; set; }

        public bool UseCorrugateSensors { get; set; }
    }
}
