﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmMovementData
    {
        public int SpeedInPercent { get; set; }

        public int AccelerationInPercent { get; set; }

        public int Torque { get; set; }
    }
}
