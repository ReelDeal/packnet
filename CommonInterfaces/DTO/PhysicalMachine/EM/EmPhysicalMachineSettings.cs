﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Xml;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmPhysicalMachineSettings : PhysicalMachineSettings
    {
        public EmPhysicalMachineSettings()
        {
            
        }

        public EmPhysicalMachineSettings(XmlNode doc)
            : base(doc)
        {
            
        }

        public EmLongHeadParameters LongHeadParameters { get; set; }

        public EmFeedRollerParameters FeedRollerParameters { get; set; }

        public EmTrackParameters TrackParameters { get; set; }

        public EmCrossHeadParameters CrossHeadParameters { get; set; }

        public WasteAreaParameters WasteAreaParameters { get; set; }

        public static EmPhysicalMachineSettings CreateFromFile(IPEndPoint callbackIP, string fileName)
        {
            var machineDocument = LoadDocument(fileName);
            var machine = new EmPhysicalMachineSettings(machineDocument)
            {
                EventNotifierIp = callbackIP,
                FileName = fileName,
                OnTheFlyActive = GetOnTheFlyActive(machineDocument),
                CrossHeadParameters = GetCrossHeadParameters(machineDocument),
                LongHeadParameters = GetLongHeadParameters(machineDocument),
                FeedRollerParameters = GetFeedRollerParameters(machineDocument),
                TrackParameters = GetTracksParameters(machineDocument),
                WasteAreaParameters = GetWasteAreaParamenters(machineDocument)
            };

            return machine;
        }

        public override void UpdateToFile()
        {
            var machineDocument = LoadDocument(FileName);

            SetCrossHeadParameters(machineDocument);
            SetFeedRollerParameters(machineDocument);
            SetLongHeadParameters(machineDocument);
            SetOnTheFlyActive(machineDocument);
            SetTracksParameters(machineDocument);
            SetWasteAreaParameters(machineDocument);

            machineDocument.Save(FileName);
        }

        private static XmlDocument LoadDocument(string fileName)
        {
            var document = new XmlDocument();
            document.Load(fileName);
            return document;
        }

        private static EmCrossHeadParameters GetCrossHeadParameters(XmlDocument machineDocument)
        {
            var normalMovementNode = machineDocument.SelectSingleNode("machine/CrossHead/MovementData");

            return new EmCrossHeadParameters()
            {
                CreaseCompensation = double.Parse(machineDocument.SelectSingleNode("machine/CrossHead/CreaseCompensation").InnerText, CultureInfo.InvariantCulture),
                CutCompensation = double.Parse(machineDocument.SelectSingleNode("machine/CrossHead/CutCompensation").InnerText, CultureInfo.InvariantCulture),
                MaxHeadPosition = double.Parse(machineDocument.SelectSingleNode("machine/CrossHead/MaxHeadPosition").InnerText, CultureInfo.InvariantCulture),
                MinHeadPosition = double.Parse(machineDocument.SelectSingleNode("machine/CrossHead/MinHeadPosition").InnerText, CultureInfo.InvariantCulture),
                ConnectDelayOff = int.Parse( machineDocument.SelectSingleNode("machine/CrossHead/ConnectDelayOff").InnerText, CultureInfo.InvariantCulture),
                ConnectDelayOn = int.Parse(machineDocument.SelectSingleNode("machine/CrossHead/ConnectDelayOn").InnerText, CultureInfo.InvariantCulture),
                CreaseDelayOff = int.Parse(machineDocument.SelectSingleNode("machine/CrossHead/CreaseDelayOff").InnerText, CultureInfo.InvariantCulture),
                CreaseDelayOn = int.Parse(machineDocument.SelectSingleNode("machine/CrossHead/CreaseDelayOn").InnerText, CultureInfo.InvariantCulture),
                CutDelayOff = int.Parse(machineDocument.SelectSingleNode("machine/CrossHead/CutDelayOff").InnerText, CultureInfo.InvariantCulture),
                CutDelayOn = int.Parse(machineDocument.SelectSingleNode("machine/CrossHead/CutDelayOn").InnerText, CultureInfo.InvariantCulture),
                ReferenceQuantity = int.Parse(machineDocument.SelectSingleNode("machine/CrossHead/ReferenceQuantity").InnerText, CultureInfo.InvariantCulture),
                ToolPositionToSensor = double.Parse(machineDocument.SelectSingleNode("machine/CrossHead/ToolPositionToSensor").InnerText, CultureInfo.InvariantCulture),
                OutOfPositionWarningThreshold = double.Parse(machineDocument.SelectSingleNode("machine/CrossHead/OutOfPositionWarningThreshold").InnerText, CultureInfo.InvariantCulture),
                
                MovementData  = new EmNormalMovementData()
                {
                    NormalMovement = new EmMovementData()
                    {
                        SpeedInPercent = int.Parse(normalMovementNode.Attributes["speedInPercent"].Value, CultureInfo.InvariantCulture),
                        AccelerationInPercent = int.Parse(normalMovementNode.Attributes["accelerationInPercent"].Value, CultureInfo.InvariantCulture),
                        Torque = int.Parse(normalMovementNode.Attributes["torque"].Value, CultureInfo.InvariantCulture)
                    }
                }
            };
        }

        private static EmLongHeadParameters GetLongHeadParameters(XmlDocument machineDocument)
        {
            var normalMovementNode = machineDocument.SelectSingleNode("machine/LongHeads/MovementData");

            var longHeadParameters = new EmLongHeadParameters
            {
                ConnectDelayOff = int.Parse(machineDocument.SelectSingleNode("machine/LongHeads/ConnectDelayOff").InnerText, CultureInfo.InvariantCulture),
                ConnectDelayOn = int.Parse(machineDocument.SelectSingleNode("machine/LongHeads/ConnectDelayOn").InnerText, CultureInfo.InvariantCulture),
                CreaseDelayOff = int.Parse(machineDocument.SelectSingleNode("machine/LongHeads/CreaseDelayOff").InnerText, CultureInfo.InvariantCulture),
                CreaseDelayOn = int.Parse(machineDocument.SelectSingleNode("machine/LongHeads/CreaseDelayOn").InnerText, CultureInfo.InvariantCulture),
                CutDelayOff = int.Parse(machineDocument.SelectSingleNode("machine/LongHeads/CutDelayOff").InnerText, CultureInfo.InvariantCulture),
                CutDelayOn = int.Parse(machineDocument.SelectSingleNode("machine/LongHeads/CutDelayOn").InnerText, CultureInfo.InvariantCulture),
                LongheadWidth = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/Width").InnerText, CultureInfo.InvariantCulture),
                CreaseCompensation = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/CreaseCompensation").InnerText, CultureInfo.InvariantCulture),
                CutCompensation = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/CutCompensation").InnerText, CultureInfo.InvariantCulture),
                MaximumPosition = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/MaxHeadPosition").InnerText, CultureInfo.InvariantCulture),
                MinimumPosition = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/MinHeadPosition").InnerText, CultureInfo.InvariantCulture),
                LHInPosWindow = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/LHInPosWindow").InnerText, CultureInfo.InvariantCulture),
                LongHeadYOffset = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/YOffset").InnerText, CultureInfo.InvariantCulture),
                OutOfPositionWarningThreshold = double.Parse(machineDocument.SelectSingleNode("machine/LongHeads/OutOfPositionWarningThreshold").InnerText, CultureInfo.InvariantCulture),
                
                LeftSideToWasteSeparator = GetUnit(machineDocument) == "mm" ? 15d : 0.59d,
                WasteSeparatorWidth = GetUnit(machineDocument) == "mm" ? 24d : 0.945d,

                MovementData = new EmNormalMovementData()
                {
                    NormalMovement = new EmMovementData()
                    {
                        SpeedInPercent = int.Parse(normalMovementNode.Attributes["speedInPercent"].Value, CultureInfo.InvariantCulture),
                        AccelerationInPercent = int.Parse(normalMovementNode.Attributes["accelerationInPercent"].Value, CultureInfo.InvariantCulture),
                        Torque = int.Parse(normalMovementNode.Attributes["torque"].Value, CultureInfo.InvariantCulture),
                    }
                }
            };

            foreach (XmlNode lhNode in machineDocument.SelectNodes("machine/LongHeads/LongHead"))
            {
                var leftSideToTool = double.Parse(lhNode.Attributes["leftSideToTool"].Value, CultureInfo.InvariantCulture);
                var toolToSensor = double.Parse(lhNode.Attributes["toolToSensorOffset"].Value, CultureInfo.InvariantCulture);

                var longHead = new EmLongHead()
                {
                    LeftSideToTool = leftSideToTool,
                    Number = int.Parse(lhNode.Attributes["number"].Value, CultureInfo.InvariantCulture),
                    ToolToSensorOffset = toolToSensor,
                    LeftSideToSensorPin = leftSideToTool + toolToSensor
                };

                longHeadParameters.AddLongHead(longHead);
            }

            return longHeadParameters;
        }

        private static EmFeedRollerParameters GetFeedRollerParameters(XmlDocument machineDocument)
        {
            var rubberRollerStartingPositions = new List<EmRubberRollerStartingPosition>();
            foreach (XmlNode startingPosition in machineDocument.SelectNodes("machine/RubberRoller/StartingPosition"))
            {
                rubberRollerStartingPositions.Add(new EmRubberRollerStartingPosition()
                {
                    TrackNumber = int.Parse(startingPosition.Attributes["track"].Value, CultureInfo.InvariantCulture),
                    Position = double.Parse(startingPosition.Attributes["position"].Value, CultureInfo.InvariantCulture),
                });
            }

            var normalMovementNode = machineDocument.SelectSingleNode("machine/RubberRoller/NormalMovementData");
            var reverseMovementNode = machineDocument.SelectSingleNode("machine/RubberRoller/ReverseMovementData");

            return new EmFeedRollerParameters
            {
                MaximumReverseDistance =
                    double.Parse(machineDocument.SelectSingleNode("machine/RubberRoller/ReverseDistance").InnerText,
                        CultureInfo.InvariantCulture),
                MovementData = new EmRollerMovementData()
                {
                    NormalMovement = new EmMovementData()
                    {
                        SpeedInPercent =
                            int.Parse(normalMovementNode.Attributes["speedInPercent"].Value, CultureInfo.InvariantCulture),
                        AccelerationInPercent =
                            int.Parse(normalMovementNode.Attributes["accelerationInPercent"].Value, CultureInfo.InvariantCulture),
                        Torque = int.Parse(normalMovementNode.Attributes["torque"].Value, CultureInfo.InvariantCulture)
                    },
                    ReverseMovement = new EmMovementData()
                    {
                        SpeedInPercent =
                            int.Parse(reverseMovementNode.Attributes["speedInPercent"].Value, CultureInfo.InvariantCulture),
                        AccelerationInPercent =
                            int.Parse(reverseMovementNode.Attributes["accelerationInPercent"].Value, CultureInfo.InvariantCulture),
                        Torque = int.Parse(reverseMovementNode.Attributes["torque"].Value, CultureInfo.InvariantCulture)
                    }
                },
                ToolsGearRatio =
                    double.Parse(machineDocument.SelectSingleNode("machine/RubberRoller/ToolsGearRatio").InnerText,
                        CultureInfo.InvariantCulture),
                OutFeedLength =
                    double.Parse(machineDocument.SelectSingleNode("machine/RubberRoller/ExtraLengthOnOutfeed").InnerText,
                        CultureInfo.InvariantCulture),
                RubberRollerStartingPositions = rubberRollerStartingPositions
            };
        }

        private static EmTrackParameters GetTracksParameters(XmlDocument machineDocument)
        {
            var trackParams = new EmTrackParameters()
            {
                TrackChangeDelay = int.Parse(machineDocument.SelectSingleNode("machine/Tracks/TrackChangeDelay").InnerText, CultureInfo.InvariantCulture),
                CorrugateWidthTolerance = double.Parse(machineDocument.SelectSingleNode("machine/Tracks/CorrugateWidthTolerance").InnerText, CultureInfo.InvariantCulture),
                TrackServiceFeedingDistance = double.Parse(machineDocument.SelectSingleNode("machine/Tracks/TrackServiceFeedingDistance").InnerText, CultureInfo.InvariantCulture),
                UseCorrugateSensors = bool.Parse(machineDocument.SelectSingleNode("machine/Tracks/UseCorrugateSensors").InnerText)
            };
         
            return trackParams;
        }
        
        private static WasteAreaParameters GetWasteAreaParamenters(XmlDocument machineDocument)
        {
            var wasteAreaParameters = new WasteAreaParameters()
            {
                MaximumWasteLength = double.Parse(machineDocument.SelectSingleNode("machine/WasteArea/MaximumWasteLength").InnerText, CultureInfo.InvariantCulture),
                MinimumWasteLength = double.Parse(machineDocument.SelectSingleNode("machine/WasteArea/MinimumWasteLength").InnerText, CultureInfo.InvariantCulture),
                Enable = bool.Parse(machineDocument.SelectSingleNode("machine/WasteArea/Enable").InnerText),
                //TODO: replace with configuration values when waste separation is complete
                MaximumWasteWidth = MicroMeter.MaxValue,
                MinimumWasteWidth = 0,
                
            };

            return wasteAreaParameters;
        }

        private void SetCrossHeadParameters(XmlDocument machineDocument)
        {
            XmlNode node;

            //Connect Delay
            machineDocument.SelectSingleNode("machine/CrossHead/ConnectDelayOff").InnerText =
                CrossHeadParameters.ConnectDelayOff.ToString(CultureInfo.InvariantCulture);
            machineDocument.SelectSingleNode("machine/CrossHead/ConnectDelayOn").InnerText =
                CrossHeadParameters.ConnectDelayOn.ToString(CultureInfo.InvariantCulture);

            //Crease Delay
            machineDocument.SelectSingleNode("machine/CrossHead/CreaseDelayOff").InnerText =
                CrossHeadParameters.CreaseDelayOff.ToString(CultureInfo.InvariantCulture);
            machineDocument.SelectSingleNode("machine/CrossHead/CreaseDelayOn").InnerText =
                CrossHeadParameters.CreaseDelayOn.ToString(CultureInfo.InvariantCulture);

            //Cut Delay
            machineDocument.SelectSingleNode("machine/CrossHead/CutDelayOff").InnerText =
                CrossHeadParameters.CutDelayOff.ToString(CultureInfo.InvariantCulture);
            machineDocument.SelectSingleNode("machine/CrossHead/CutDelayOn").InnerText =
                CrossHeadParameters.CutDelayOn.ToString(CultureInfo.InvariantCulture);

            //Movement Data
            node = machineDocument.SelectSingleNode("machine/CrossHead/MovementData");

            node.Attributes["speedInPercent"].Value =
                CrossHeadParameters.MovementData.NormalMovement.SpeedInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["accelerationInPercent"].Value =
                CrossHeadParameters.MovementData.NormalMovement.AccelerationInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["torque"].Value =
                CrossHeadParameters.MovementData.NormalMovement.Torque.ToString(CultureInfo.InvariantCulture);

            //Reference Quantity
            machineDocument.SelectSingleNode("machine/CrossHead/ReferenceQuantity").InnerText =
                CrossHeadParameters.ReferenceQuantity.ToString(CultureInfo.InvariantCulture);

            //Tool Position to Sensor
            machineDocument.SelectSingleNode("machine/CrossHead/ToolPositionToSensor").InnerText =
                CrossHeadParameters.ToolPositionToSensor.ToString(CultureInfo.InvariantCulture);

            //Out Of Position Warning Threshold
            machineDocument.SelectSingleNode("machine/CrossHead/OutOfPositionWarningThreshold").InnerText =
                CrossHeadParameters.OutOfPositionWarningThreshold.ToString(CultureInfo.InvariantCulture);
        }

        private void SetFeedRollerParameters(XmlDocument machineDocument)
        {
            XmlNode node;
            XmlNodeList nodeList;

            //Extra Length on Out Feed
            machineDocument.SelectSingleNode("machine/RubberRoller/ExtraLengthOnOutfeed").InnerText =
                FeedRollerParameters.OutFeedLength.ToString();

            //Normal Movement Data
            node = machineDocument.SelectSingleNode("machine/RubberRoller/NormalMovementData");

            node.Attributes["speedInPercent"].Value =
                FeedRollerParameters.MovementData.NormalMovement.SpeedInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["accelerationInPercent"].Value =
                FeedRollerParameters.MovementData.NormalMovement.AccelerationInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["torque"].Value =
                FeedRollerParameters.MovementData.NormalMovement.Torque.ToString(CultureInfo.InvariantCulture);

            //Reverse Distance
            machineDocument.SelectSingleNode("machine/RubberRoller/ReverseDistance").InnerText =
                FeedRollerParameters.MaximumReverseDistance.ToString();

            //Reverse Movement Data
            node = machineDocument.SelectSingleNode("machine/RubberRoller/ReverseMovementData");

            node.Attributes["speedInPercent"].Value =
                FeedRollerParameters.MovementData.ReverseMovement.SpeedInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["accelerationInPercent"].Value =
                FeedRollerParameters.MovementData.ReverseMovement.AccelerationInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["torque"].Value =
                FeedRollerParameters.MovementData.ReverseMovement.Torque.ToString(CultureInfo.InvariantCulture);

            //Starting Position
            nodeList = machineDocument.SelectNodes("machine/RubberRoller/StartingPosition");

            foreach (XmlNode nodeItem in nodeList)
            {
                var startingPosition =
                    FeedRollerParameters.RubberRollerStartingPositions.ToList()[Int32.Parse(nodeItem.Attributes["track"].Value) - 1];

                nodeItem.Attributes["position"].Value = startingPosition.Position.ToString(CultureInfo.InvariantCulture);
            }

            //Tools Gear Ratio
            machineDocument.SelectSingleNode("machine/RubberRoller/ToolsGearRatio").InnerText =
                FeedRollerParameters.ToolsGearRatio.ToString(CultureInfo.InvariantCulture);
        }

        private void SetLongHeadParameters(XmlDocument machineDocument)
        {
            XmlNode node;
            XmlNodeList nodeList;

            //Connect Delay
            machineDocument.SelectSingleNode("machine/LongHeads/ConnectDelayOff").InnerText =
                LongHeadParameters.ConnectDelayOff.ToString(CultureInfo.InvariantCulture);
            machineDocument.SelectSingleNode("machine/LongHeads/ConnectDelayOn").InnerText =
                LongHeadParameters.ConnectDelayOn.ToString(CultureInfo.InvariantCulture);

            //Crease Delay
            machineDocument.SelectSingleNode("machine/LongHeads/CreaseDelayOff").InnerText =
                LongHeadParameters.CreaseDelayOff.ToString(CultureInfo.InvariantCulture);
            machineDocument.SelectSingleNode("machine/LongHeads/CreaseDelayOn").InnerText =
                LongHeadParameters.CreaseDelayOn.ToString(CultureInfo.InvariantCulture);

            //Cut Delay
            machineDocument.SelectSingleNode("machine/LongHeads/CutDelayOff").InnerText =
                LongHeadParameters.CutDelayOff.ToString(CultureInfo.InvariantCulture);
            machineDocument.SelectSingleNode("machine/LongHeads/CutDelayOn").InnerText =
                LongHeadParameters.CutDelayOn.ToString(CultureInfo.InvariantCulture);

            //Long Head in Position Window
            machineDocument.SelectSingleNode("machine/LongHeads/LHInPosWindow").InnerText =
                LongHeadParameters.LHInPosWindow.ToString();

            //Long Head
            nodeList = machineDocument.SelectNodes("machine/LongHeads/LongHead");

            foreach (XmlNode nodeItem in nodeList)
            {
                var longHead =
                    LongHeadParameters.LongHeads.ToList()[Int32.Parse(nodeItem.Attributes["number"].Value) - 1];

                nodeItem.Attributes["leftSideToTool"].Value = longHead.LeftSideToTool.ToString();
                nodeItem.Attributes["toolToSensorOffset"].Value = longHead.ToolToSensorOffset.ToString(CultureInfo.InvariantCulture);
            }

            //Movement Data
            node = machineDocument.SelectSingleNode("machine/LongHeads/MovementData");

            node.Attributes["speedInPercent"].Value =
                LongHeadParameters.MovementData.NormalMovement.SpeedInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["accelerationInPercent"].Value =
                LongHeadParameters.MovementData.NormalMovement.AccelerationInPercent.ToString(CultureInfo.InvariantCulture);
            node.Attributes["torque"].Value =
                LongHeadParameters.MovementData.NormalMovement.Torque.ToString(CultureInfo.InvariantCulture);

            //Out Of Position Warning Threshold
            machineDocument.SelectSingleNode("machine/LongHeads/OutOfPositionWarningThreshold").InnerText =
                LongHeadParameters.OutOfPositionWarningThreshold.ToString(CultureInfo.InvariantCulture);

            //Y Offset
            machineDocument.SelectSingleNode("machine/LongHeads/YOffset").InnerText =
                LongHeadParameters.LongHeadYOffset.ToString();
        }

        private void SetTracksParameters(XmlDocument machineDocument)
        {
            //Corrugate Width Tolerance
            machineDocument.SelectSingleNode("machine/Tracks/CorrugateWidthTolerance").InnerText =
                TrackParameters.CorrugateWidthTolerance.ToString(CultureInfo.InvariantCulture);

            //Track Change Delay
            machineDocument.SelectSingleNode("machine/Tracks/TrackChangeDelay").InnerText =
                TrackParameters.TrackChangeDelay.ToString(CultureInfo.InvariantCulture);

            //Track Service Feeding Distance
            machineDocument.SelectSingleNode("machine/Tracks/TrackServiceFeedingDistance").InnerText =
                TrackParameters.TrackServiceFeedingDistance.ToString(CultureInfo.InvariantCulture);

            //Use Corrugate Sensors
            machineDocument.SelectSingleNode("machine/Tracks/UseCorrugateSensors").InnerText =
                TrackParameters.UseCorrugateSensors.ToString().ToLower();
        }

        private void SetWasteAreaParameters(XmlDocument machineDocument)
        {
            //Enable
            machineDocument.SelectSingleNode("machine/WasteArea/Enable").InnerText =
                WasteAreaParameters.Enable.ToString().ToLower();

            //Maximum Waste Length
            machineDocument.SelectSingleNode("machine/WasteArea/MaximumWasteLength").InnerText =
                WasteAreaParameters.MaximumWasteLength.ToString();

            //Minimum Waste Length
            machineDocument.SelectSingleNode("machine/WasteArea/MinimumWasteLength").InnerText =
                WasteAreaParameters.MinimumWasteLength.ToString();
        }

        public static bool IsEmPhysicalMachineSettingsFilePath(string path, ILogger logger)
        {
            try
            {
                CreateFromFile(new IPEndPoint(1, 1), path);
                return true;
            }
            catch (Exception)
            {
                logger.Log(LogLevel.Warning, string.Format("Unable to parse the path: {0} as Em Physical Machine Settings file", path));
                return false;
            }
        }
    }
}