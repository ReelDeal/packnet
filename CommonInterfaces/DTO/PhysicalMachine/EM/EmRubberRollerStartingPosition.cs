﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackNet.Common.Interfaces.DTO.PhysicalMachine.EM
{
    public class EmRubberRollerStartingPosition
    {
        public int TrackNumber { get; set; }

        public double Position { get; set; }
    }
}
