﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    using System.Collections.Generic;

    public abstract class LongHeadParameters<T> where T:LongHead
    {
        protected List<T> longHeads = new List<T>();

        public MicroMeter MaximumPosition { get; set; }

        public MicroMeter MinimumPosition { get; set; }

        public MicroMeter LongheadWidth { get; set; }

        public IEnumerable<T> LongHeads
        {
            get { return longHeads; }
        }
        
        public void AddLongHead(T longHead)
        {
            this.longHeads.Add(longHead);
            SetMinimumPositionForLonghead(longHead.Number);
            SetMaximumPositionForLongheads();
        }

        private void SetMaximumPositionForLongheads()
        {
            for (var i = longHeads.Count - 1; i >= 0; i--)
            {
                var longHead = longHeads[i];
                if (i == longHeads.Count - 1)
                    longHead.MaximumPosition = MaximumPosition;
                else
                {
                    var nextLongHead = longHeads[i + 1];
                    longHead.MaximumPosition = nextLongHead.MaximumPosition - nextLongHead.LeftSideToTool
                                               - (this.LongheadWidth - longHead.LeftSideToTool);
                }
            }
        }

        private void SetMinimumPositionForLonghead(int number)
        {
            if (longHeads.Count < number)
            {
                return;
            }

            var currentLonghead = longHeads[number - 1];
            currentLonghead.Width = this.LongheadWidth;
            if (currentLonghead.Number == 1)
            {
                currentLonghead.MinimumPosition = MinimumPosition;
            }
            else
            {
                var previousLh = longHeads[currentLonghead.Number - 2];
                currentLonghead.MinimumPosition = previousLh.MinimumPosition + (this.LongheadWidth - previousLh.LeftSideToTool)
                                                  + currentLonghead.LeftSideToTool;
            }
        }
    }
}
