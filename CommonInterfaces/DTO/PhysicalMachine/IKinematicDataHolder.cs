﻿namespace PackNet.Common.Interfaces.DTO.PhysicalMachine
{
    public interface IKinematicDataHolder
    {
        double ActualMaximumMovementSpeed { get; }

        double ActualMaximumAcceleration { get; }

    }
}
