﻿using System;

using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.DTO
{
    public class DatabaseTask : IPersistable 
    {
        public string Message { get; set; }
        public string Path { get; set; }
        public bool IsComplete { get; set; }
        public Guid Id { get; set; }
    }
}