using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Messaging
{
    /// <summary>
    /// The message format used to communicate a result with the UI, The properties and names are tightly coupled to serialization and UI communication
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseMessage<T> : Message<T>, IResponseMessage<T>
    {
        public ResultTypes Result { get; set; }
        public string Message { get; set; }
    }

    /// <summary>
    /// The message format used to communicate a result with the UI, The properties and names are tightly coupled to serialization and UI communication
    /// </summary>
    public class ResponseMessage : Message, IResponseMessage
    {
        public ResultTypes Result { get; set; }

        public string Message { get; set; }
    }
}