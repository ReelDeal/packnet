using System;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Messaging
{
    /// <summary>
    /// The standard message format used to pass internal messages as well as User Interface messages
    /// </summary>
    public interface IMessage
    {
        /// <summary>
        /// The type of message being sent
        /// </summary>
        MessageTypes MessageType { get; }

        /// <summary>
        /// Who to route a response to if required
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        string ReplyTo { get; set; }

        /// <summary>
        /// The User that originated the message
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        string UserName { get; set; }

        /// <summary>
        /// The machine group name that originated the message
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        string MachineGroupName { get; set; }

        /// <summary>
        /// The machine group Id that originated the message
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        Guid? MachineGroupId { get; set; }
        
        /// <summary>
        /// The date the message was created
        /// </summary>
        [JsonProperty]
        DateTime Created { get; }

        bool Compressed { get; set; }
    }

    /// <summary>
    /// A <see cref="IMessage"/> with a message payload
    /// </summary>
    /// <typeparam name="T">The type of payload included with the message</typeparam>
    public interface IMessage<T> : IMessage
    {
        /// <summary>
        /// The payload of the message
        /// </summary>
        T Data { get; }
    }

    /// <summary>
    /// A <see cref="IMessage{T}"/> with additional arguments
    /// </summary>
    /// <typeparam name="T">The type of payload included with the message</typeparam>
    /// <typeparam name="TArgs">The type of arguments included with the message</typeparam>
    public interface IMessage<T, TArgs> : IMessage<T>
    {
        /// <summary>
        /// The arguments of the message
        /// </summary>
        TArgs Arguments { get; }
    }
}