namespace PackNet.Common.Interfaces.DTO.Messaging
{
    /// <summary>
    /// TODO: not sure what this is or why we need it
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FingerPrintedMessage<T> : Message<T>, IMessage<T>
    {
        public string BrowserFingerPrint { get; set; }
    }
}