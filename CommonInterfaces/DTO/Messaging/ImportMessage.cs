using System;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Messaging
{
    /// <summary>
    /// A message representing data that has just been imported into the system and needs to be staged by a selection algorithm
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ImportMessage<T> : Message<T>
    {
        public ImportMessage()
        {
            MessageType = MessageTypes.DataImported;
            UniqueImportId = Guid.NewGuid();
        }

        public Guid UniqueImportId { get; set; }
        public ImportTypes ImportType { get; set; }
        public SelectionAlgorithmTypes SelectionAlgorithmType { get; set; }
        public Guid ProductionGroupId { get; set; }
    }
}