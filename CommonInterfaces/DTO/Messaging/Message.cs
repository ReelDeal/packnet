﻿using System;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Messaging
{
    /// <summary>
    /// The message format used to communicate with the UI, The properties and names are tightly coupled to serialization and UI communication
    /// </summary>
    public class Message : IMessage
    {
        public MessageTypes MessageType { get; set; }

        public string ReplyTo { get; set; }

        public string UserName { get; set; }

        public Guid? MachineGroupId { get; set; }

        public string MachineGroupName { get; set; }

        public DateTime Created { get; set; }

        public bool Compressed { get; set; }

        public Message()
        {
            Created = DateTime.UtcNow;
        }
    }

    /// <summary>
    /// The message format used to communicate with the UI, The properties and names are tightly coupled to serialization and UI communication
    /// </summary>
    /// <typeparam name="T">The payload to be serialized across the wire</typeparam>
    public class Message<T> : Message, IMessage<T>
    {
        public T Data { get; set; }
    }

    /// <summary>
    /// The message format used to communicate with the UI, The properties and names are tightly coupled to serialization and UI communication
    /// </summary>
    /// <typeparam name="T">The payload of the message that will be serialized over the wire</typeparam>
    /// <typeparam name="TArgs">Additional arguments of the message that will be serialized over the wire</typeparam>
    public class Message<T, TArgs> : Message<T>, IMessage<T, TArgs>
    {
        public TArgs Arguments { get; set; }
    }
}
