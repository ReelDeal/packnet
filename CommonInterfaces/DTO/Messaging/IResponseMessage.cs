using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.DTO.Messaging
{
    /// <summary>
    /// Used to send back a response from some sent IMessage
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IResponseMessage<T> : IResponseMessage, IMessage<T>
    {
    }

    public interface IResponseMessage : IMessage
    {
        ResultTypes Result { get; }
        
        string Message { get; set; }
    }
}