﻿namespace PackNet.Common.Interfaces.DTO.Articles
{
    using System;

    public class ArticleScanTriggerRequest
    {
        public string ArticleId { get; set; }
        public string ProductionGroup { get; set; }
        public DateTime TriggerTime { get; set; }
    }
}
