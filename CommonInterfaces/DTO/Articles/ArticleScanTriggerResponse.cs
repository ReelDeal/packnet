﻿
namespace PackNet.Common.Interfaces.DTO.Articles
{
    using System;

    using DTO;

    public class ArticleScanTriggerResponse
    {
        public ArticleScanTriggerResponse()
        {
            
        }

        public ArticleScanTriggerResponse(ArticleScanTriggerRequest request)
        {
            ProductionGroup = request.ProductionGroup;
            BarcodeData = request.ArticleId;
            TriggerOn = request.TriggerTime;
        }

        public Article Article { get; set; }

        public string ProductionGroup { get; set; }

        public DateTime TriggerOn { get; set; }

        public string BarcodeData { get; set; }
    }
}
