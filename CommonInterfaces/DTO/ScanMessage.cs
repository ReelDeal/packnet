﻿using PackNet.Common.Interfaces.DTO.Errors;

namespace PackNet.Common.Interfaces.DTO
{
    public class ScanMessage : ErrorCondition
    {
        public string ProductionGroup { get; set; }

        public ScanMessage(string message, string productionGroup)
        {
            ProductionGroup = productionGroup;
            base.Message = message;            
        } 
    }
}
