﻿using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.DTO.ProductionGroups
{
    using System;
    using System.Collections.Generic;
    using Enums;

    public class ProductionGroup : IPersistable
    {
        private ConcurrentList<Guid> configuredCorrugates;
        private ConcurrentList<Guid> configuredMachineGroups;
        private SelectionAlgorithmTypes selectionAlgorithm;

        public Guid Id { get; set; }

        public string Alias { get; set; }
        public string Description { get; set; }

        public Dictionary<string, string> Options { get; set; }

        public ProductionGroup()
        {
            Options = new Dictionary<string, string>();
        }

        public SelectionAlgorithmTypes SelectionAlgorithm
        {
            get { return selectionAlgorithm ?? (selectionAlgorithm = SelectionAlgorithmTypes.BoxFirst); }
            set { selectionAlgorithm = value; }
        }

        public ConcurrentList<Guid> ConfiguredCorrugates
        {
            get { return configuredCorrugates ?? (configuredCorrugates = new ConcurrentList<Guid>()); }
            set { configuredCorrugates = value; }
        }

        public ConcurrentList<Guid> ConfiguredMachineGroups
        {
            get { return configuredMachineGroups ?? (configuredMachineGroups = new ConcurrentList<Guid>()); }
            set { configuredMachineGroups = value; }
        }
    }
}