﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Services.SelectionAlgorithm.ScanToCreate
{
    using PackNet.Common.Interfaces.Converters;

    public static class ProductionGroupConfigurationExtensions
    {
        public static ScanToCreateSelectionAlgorithmConfiguration ScanToCreateConfiguration(
            this Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            var showTriggerInterface = false;
            string showTriggerInterfaceString = "";
            if (productionGroup.Options.TryGetValue("ShowTriggerInterface", out showTriggerInterfaceString))
            {
                showTriggerInterface = Convert.ToBoolean(showTriggerInterfaceString);
            }
            
            string barcodeParsingWorkflow = "";
            productionGroup.Options.TryGetValue("BarcodeParsingWorkflow", out barcodeParsingWorkflow);
            
            return new ScanToCreateSelectionAlgorithmConfiguration
            {
                ShowTriggerInterface = showTriggerInterface,
                BarcodeParsingWorkflow = barcodeParsingWorkflow
            };
        }

        public static OrderSelectionAlgorithmConfiguration OrdersConfiguration(
          this Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            var pauseMachineBetweenOrders = false;
            string pauseBetweenOrdersString = "";
            if (productionGroup.Options.TryGetValue("PauseMachineBetweenOrders", out pauseBetweenOrdersString))
            {
                pauseMachineBetweenOrders = Convert.ToBoolean(pauseBetweenOrdersString);
            }

            var canDistribute = false;
            string canDistributeString = "";
            if (productionGroup.Options.TryGetValue("CanDistribute", out canDistributeString))
            {
                canDistribute = Convert.ToBoolean(canDistributeString);
            }

            var defaultDistributeValue = false;
            string defaultDistributeValueString = "";
            if (productionGroup.Options.TryGetValue("DefaultDistributeValue", out defaultDistributeValueString))
            {
                defaultDistributeValue = Convert.ToBoolean(defaultDistributeValueString);
            }

            return new OrderSelectionAlgorithmConfiguration
            {
                PauseMachineBetweenOrders = pauseMachineBetweenOrders,
                CanDistribute = canDistribute,
                DefaultDistributeValue = defaultDistributeValue
            };
        }

        public static BoxLastSelectionAlgorithmConfiguration BoxLastConfiguration(
         this Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            //var scannerSettings = new ScannerSettings();
            string scannerSettingsString = "";
            if (productionGroup.Options.TryGetValue("ScannerSettings", out scannerSettingsString))
            {
                //scannerSettings = JsonConvert.DeserializeObject<ScannerSettings>(scannerSettingsString);
            }

            return new BoxLastSelectionAlgorithmConfiguration
            {

            };
        }

        public static BoxFirstSelectionAlgorithmConfiguration BoxFirstConfiguration(
           this Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup)
        {
            var configuredCartonPropertyGroups = new ConcurrentList<CartonPropertyGroup>();
            string configuredCartonPropertyGroupsString = "";
            string surgeCountString = "";
            int surgeCount = 0;
            if (productionGroup.Options.TryGetValue("ConfiguredCartonPropertyGroups", out configuredCartonPropertyGroupsString))
            {
                configuredCartonPropertyGroups = JsonConvert.DeserializeObject<ConcurrentList<CartonPropertyGroup>>(configuredCartonPropertyGroupsString, SerializationSettings.GetJsonSerializerSettings());
            }

            if (!(productionGroup.Options.TryGetValue("SurgeCount", out surgeCountString) 
                  && int.TryParse(surgeCountString, out surgeCount)))
            {
                surgeCount = 10;
            }

            return new BoxFirstSelectionAlgorithmConfiguration
            {
                ConfiguredCartonPropertyGroups = configuredCartonPropertyGroups,
                SurgeCount = surgeCount
            };
        }
    }
}
