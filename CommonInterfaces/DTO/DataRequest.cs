﻿namespace PackNet.Common.Interfaces.DTO
{
    using System.Collections.Generic;

    using Newtonsoft.Json;

    public class DataRequest
    {
        [JsonProperty(Order = 1)]
        public Dictionary<string, string> Filters { get; private set; }

        [JsonProperty(Order = 2)]
        public string DataSource { get; private set; }
        
        [JsonProperty(Order = 3)]
        public int ChunkSize { get; private set; }

        [JsonProperty(Order = 4)]
        public int Chunk { get; private set; }

        public DataRequest(string dataSource, Dictionary<string, string> filters, int chunk, int chunkSize)
        {
            Filters = filters;
            DataSource = dataSource;
            Chunk = chunk;
            ChunkSize = chunkSize;
        }
    }
}
