﻿using System;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.DTO.ExternalSystems
{
	public class ExternalCustomerSystem : BaseStatusReportingMachine<MachineStatuses>, INetworkConnectedMachine
	{
        public MachineTypes MachineType { get { return MachineTypes.ExternalCustomerSystem; } }
        public ExternalCustomerSystemTypes ExternalCustomerSystemType { get; set; }
        
		public string Description { get; set; }
		public string IpOrDnsName { get; set; }
        public string IpV4SubnetMask { get; set; }
        public string IpV4DefaultGateway { get; set; }
        public string MacAddress { get; set; }
		public int Port { get; set; }
        public string WorkflowPath { get; set; }
    }
}