﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace PackNet.Common.Interfaces.DTO
{
    public class WasteArea
    {
        public WasteArea(PhysicalCoordinate corner, PhysicalCoordinate oppositeCorner)
        {
            if (corner.X == oppositeCorner.X || corner.Y == oppositeCorner.Y)
                throw new ArgumentException("The area of the corners must be greater than 0");

            TopLeft = corner;
            BottomRight = oppositeCorner;

            FlipCornersIfReversed();
        }

        private void FlipCornersIfReversed()
        {
            var tempTopLeft = new PhysicalCoordinate(TopLeft.X, TopLeft.Y);

            if (TopLeft.X > BottomRight.X)
            {
                TopLeft.X = BottomRight.X;
                BottomRight.X = tempTopLeft.X;
            }

            if (TopLeft.Y > BottomRight.Y)
            {
                TopLeft.Y = BottomRight.Y;
                BottomRight.Y = tempTopLeft.Y;
            }
        }

        public PhysicalCoordinate TopLeft { get; private set; }
        public PhysicalCoordinate BottomRight { get; private set; }

        public MicroMeter Length
        {
            get { return BottomRight.Y - TopLeft.Y; }
        }

        public MicroMeter Width
        {
            get { return BottomRight.X - TopLeft.X; }
        }

        public override string ToString()
        {
            return string.Format("TL: {0}, BR: {1}", TopLeft, BottomRight);
        }

        public IEnumerable<PhysicalLine> GetCutLines()
        {
            yield return GetCutLineForTopBorder();
            yield return GetCutLineForRightBorder();
            yield return GetCutLineForBottomBorder();
            yield return GetCutLineForLeftBorder();
        }

        private PhysicalLine GetCutLineForTopBorder()
        {
            return new PhysicalLine(new PhysicalCoordinate(TopLeft.X, TopLeft.Y),
                new PhysicalCoordinate(BottomRight.X, TopLeft.Y),
                LineType.cut);
        }

        private PhysicalLine GetCutLineForRightBorder()
        {
            return new PhysicalLine(new PhysicalCoordinate(BottomRight.X, TopLeft.Y),
                new PhysicalCoordinate(BottomRight.X, BottomRight.Y),
                LineType.cut);
        }

        private PhysicalLine GetCutLineForBottomBorder()
        {
            return new PhysicalLine(new PhysicalCoordinate(TopLeft.X, BottomRight.Y),
                new PhysicalCoordinate(BottomRight.X, BottomRight.Y),
                LineType.cut);
        }

        private PhysicalLine GetCutLineForLeftBorder()
        {
            return new PhysicalLine(new PhysicalCoordinate(TopLeft.X, TopLeft.Y),
                new PhysicalCoordinate(TopLeft.X, BottomRight.Y),
                LineType.cut);
        }

        public bool CanDecreaseWidth(WasteArea comparison)
        {
            return comparison.TopLeft.X >= TopLeft.X && comparison.BottomRight.X <= BottomRight.X;
        }

        public bool TopOrBottomIsMatchingTo(WasteArea comparison)
        {
            return comparison.TopLeft.Y == BottomRight.Y || comparison.BottomRight.Y == TopLeft.Y;
        }

        public WasteArea SplitAreaBasedOnShorterArea(WasteArea shorterArea)
        {
            return new WasteArea(new PhysicalCoordinate(TopLeft.X, shorterArea.BottomRight.Y),
                new PhysicalCoordinate(BottomRight.X, BottomRight.Y));
        }
    }
}
