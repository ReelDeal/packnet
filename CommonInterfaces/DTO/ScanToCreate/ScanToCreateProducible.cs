﻿using System;

using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.DTO.ScanToCreate
{
    //public class ScanToCreateProducible : BaseProducible, IProducibleWrapper
    public class ScanToCreateProducible : Order
    {
        //private IDisposable producibleStatusDisposable;
        //private IProducible producible;
        
        public override ProducibleTypes ProducibleType
        {
            get { return ProducibleTypes.ScanToCreateProducible; }
        }
        
        public string BarcodeData { get; set; }

        public override Guid ProducedOnMachineGroupId
        {
            get
            {
                if (Producible == null)
                    return Guid.Empty;

                return Producible.ProducedOnMachineGroupId;
            }
            set
            {
                if (Producible == null)
                    return;

                Producible.ProducedOnMachineGroupId = value;
            }
        }

        public override ProducibleStatuses ProducibleStatus
        {
            get { return base.ProducibleStatus; }
            set
            {
                if (base.ProducibleStatus == value)
                    return;

                if (base.ProducibleStatus == ProducibleStatuses.ProducibleRemoved)
                {
                    return;
                }

                base.ProducibleStatus = value;
                StatusChangedDateTime = DateTime.UtcNow;

                if (Producible != null)
                    Producible.ProducibleStatus = value;
            }
        }

        public ScanToCreateProducible() : base(null, 1)
        {
            
        }

        public ScanToCreateProducible(int quantity): base(null, quantity)
        {

        }

        public ScanToCreateProducible(IProducible producible, int quantity) : base(producible, quantity)
        {
            
        }

        //public IProducible Producible
        //{
        //    get
        //    {
        //        return producible;
        //    }
        //    set
        //    {
        //        if(producibleStatusDisposable != null)
        //            producibleStatusDisposable.Dispose();

        //        producible = value;
        //        producibleStatusDisposable = producible.ProducibleStatusObservable.DurableSubscribe((status) => base.ProducibleStatus = status);
        //    }
        //}
    }
}
