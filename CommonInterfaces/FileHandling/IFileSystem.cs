﻿using System.IO;

namespace PackNet.Common.Interfaces.FileHandling
{
    public interface IFileSystem
    {
        DirectoryInfo CreateTemporaryDirectory();

        string GetTimestampedFileName(string fileExtension);
    }
}