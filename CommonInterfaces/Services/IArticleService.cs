﻿using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.Services
{
    using System;
    using System.Collections.Generic;

    using DTO;

    /// <summary>
    /// Represents events and functions used to communicate with the article service
    /// </summary>
    public interface IArticleService : IService
    {
        /// <summary>
        /// Notifications when articles are created
        /// </summary>
        IObservable<IResponseMessage<Article>> CreatedObservable { get; }

        /// <summary>
        /// Notifications when articles are deleted
        /// </summary>
        IObservable<IResponseMessage<List<string>>> DeletedObservable { get; }

        /// <summary>
        /// Notifications when articles are modified
        /// </summary>
        IObservable<IResponseMessage<Article>> ModifiedObservable { get; }

        /// <summary>
        /// Notifications when articles are failed
        /// </summary>
        IObservable<IResponseMessage<Article>> FailedObservable { get; }

        /// <summary>
        /// Creates an article
        /// </summary>
        /// <param name="article"></param>
        /// <returns></returns>
        Article Create(Article article);

        /// <summary>
        /// Updates an article
        /// </summary>
        /// <param name="articleId"></param>
        /// <param name="article"></param>
        /// <param name="doSearch">If set to false, update won't publish a search message when completed</param>
        /// <returns></returns>
        Article Update(string articleId, Article article, bool doSearch = true);

        /// <summary>
        /// Deletes articles
        /// </summary>
        /// <param name="articlesToUpdate"></param>
        /// <returns></returns>
        void Delete(List<string> articlesToUpdate);

        /// <summary>
        /// Retrieves an article by article Id
        /// </summary>
        /// <param name="articleId">Id of the article to retrieve</param>
        /// <returns></returns>
        Article Find(string articleId);
        
        /// <summary>
        /// Retrieves all articles matching the given search pattern
        /// </summary>
        /// <param name="articleSearchPattern"></param>
        /// <returns></returns>
        IEnumerable<Article> Search(string articleSearchPattern);
    }
}
