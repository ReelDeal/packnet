﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.Services
{
    public interface IWorkflowTrackingService : IService
    {
        WorkflowLifetime Factory(WorkflowTypes type, string workflow, Dictionary<string, object> additionalInfo = null);
    }
}