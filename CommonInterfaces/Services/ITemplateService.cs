﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.Services
{
    public interface ITemplateService : IService
    {
        Template FindByName(string name);

        Template FindById(Guid id);
    }
}