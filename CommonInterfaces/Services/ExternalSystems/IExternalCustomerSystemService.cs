﻿using System;

using PackNet.Common.Interfaces.DTO.ExternalSystems;
using PackNet.Common.Interfaces.DTO.Messaging;

namespace PackNet.Common.Interfaces.Services.ExternalSystems
{
    /// <summary>
    /// Workflow based integration service with customer systems 
    /// </summary>
    public interface IExternalCustomerSystemService : IService
    {
        void SendMessage<T>(ExternalCustomerSystem customerSystem, IMessage<T> message);
        void SendMessage<T>(Guid customerSystemId, IMessage<T> message);

        ExternalCustomerSystem Find(string name);
        ExternalCustomerSystem Find(Guid id);
    }
}
