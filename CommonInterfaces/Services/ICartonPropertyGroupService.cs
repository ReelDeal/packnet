﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Services
{
    public interface ICartonPropertyGroupService : IService
    {
        IEnumerable<CartonPropertyGroup> Groups { get; }

        CartonPropertyGroup GetMatchingPropertyGroupForCarton(BoxFirstProducible producible);

        CartonPropertyGroup GetCartonPropertyGroupByAlias(string alias);

        /// <summary>
        /// Based on the configured mix, returns the CPG that should be selected next
        /// </summary>
        /// <param name="productionGroup"></param>
        /// <param name="produciblesToGetCpgFor"></param>
        /// <param name="availableProducibles"></param>
        /// <returns></returns>
        CartonPropertyGroup GetNextCPGToDispatchWorkTo(Common.Interfaces.DTO.ProductionGroups.ProductionGroup productionGroup,
                                                        IEnumerable<IProducible> produciblesToGetCpgFor, IEnumerable<IProducible> availableProducibles);

        void DecreaseSurgeCounter(string cpgAlias);

        IEnumerable<CartonPropertyGroup> GetUnstoppedCartonPropertyGroups();

        IEnumerable<CartonPropertyGroup> GetCartonPropertyGroupsByStatus(CartonPropertyGroupStatuses status);

        /// <summary>
        /// Add cpg to be skipped from the sequence one time.
        /// This is needed to get the correct mix when a cpg that was not next in mix was produced 
        /// due to tiling of next cpg was not a tiling option.
        /// <param name="groupToSkip"></param>
        /// </summary>
        void AddCpgToSkipList(DTO.ProductionGroups.ProductionGroup productionGroup, CartonPropertyGroup groupToSkip);

        void SetSurgeCount(string cpgAlias, int surgeCount);

        CartonPropertyGroup CreateCartonPropertyGroup(CartonPropertyGroup cartonPropertyGroup);

        void DeleteCartonPropertyGroup(CartonPropertyGroup cartonPropertyGroup);

        CartonPropertyGroup UpdateCartonPropertyGroup(CartonPropertyGroup cartonPropertyGroup);

        void SetCartonPropertyGroupStatus(CartonPropertyGroup cartonPropertyGroup);

        ConcurrentList<CartonPropertyGroup> GetCartonPropertyGroups();
    }
}
