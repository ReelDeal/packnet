﻿using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.Common.Interfaces.Services
{
    public interface IRestrictionResolverService: IService, IRestrictionResolver
    {
        void AddResolver(IRestrictionResolver resolver);
    }
}