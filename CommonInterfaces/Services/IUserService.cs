﻿using PackNet.Common.Interfaces.DTO.Users;

namespace PackNet.Common.Interfaces.Services
{
    using System;
    using System.Collections.Generic;

    public interface IUserService : IService
    {
        IEnumerable<User> Users { get; }
        User Login(User user);
        User ValidateToken(User user);
        User GetAutoLoginUser();
        User Create(User user);
        User Update(User user);
        void Delete(User user);
        User Find(string userName);
        User Find(Guid id);

        Preference UpdatePreferences(Preference preference);
        Preference FindPreferences(User data);
    }
}