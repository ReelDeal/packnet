﻿using System;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.Services
{
	public interface IUserNotificationService : IService
	{
		void SendNotificationToMachineGroup(NotificationSeverity severity, string message, string additionalInfo, Guid machineGroupId);
		void SendNotificationToAllMachineGroupsInMySameProductionGroup(NotificationSeverity severity, string message, string additionalInfo, Guid machineGroupId);

	    void SendNotificationToProductionGroup(NotificationSeverity severity, string message, string additionalInfo,
	        Guid productionGroupId);

		void SendNotificationToSystem(NotificationSeverity severity, string message, string additionalInfo);
	}
}