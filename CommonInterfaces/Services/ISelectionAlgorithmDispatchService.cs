﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services
{
    /// <summary>
    /// Responsible for interactions with the selection algorithm plugins... Manages plugins and invokes workflows for the plugins 
    /// </summary>
    public interface ISelectionAlgorithmDispatchService : IService
    {
        /// <summary>
        /// Dispatches the Importable to the corresponding workflow that will publish the output on the event aggregator.
        /// </summary>
        /// <param name="producibles"></param>
        /// <param name="selectionAlgorithmTypes"></param>
        /// <exception cref="Exception">Throws an Exception if the corresponding workflow for this Importable.ImportType cannot be found</exception>
        /// <remarks>
        /// The corresponding workflow is found by a case-insensitive match of the IWorkflowImport.Name to the importable.ImportType 
        /// </remarks>
        void DispatchStagingWorkflow(IEnumerable<IProducible> producibles, SelectionAlgorithmTypes selectionAlgorithmTypes);

        /// <summary>
        /// Dispatches the "Select Producible" workflow for a given machine group and producible type
        /// </summary>
        /// <param name="machineGroup">Machine group we are working with here.</param>
        /// <returns>The Producible that the machine group should Produce</returns>
        IProducible DispatchSelectProducibleWorkflow(MachineGroup machineGroup);
    }
}