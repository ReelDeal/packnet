﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services
{
    public interface ICorrugateService : IService
    {
        CartonOnCorrugate GetOptimalCorrugate(
                      IEnumerable<Corrugate> corrugatesToCompare,
                      IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate,
                      ICarton carton,
                      int allowedTileCount,
                      bool recalculateCachedItem = false);

        IEnumerable<CartonOnCorrugate> GetOptimalCorrugates(
                      IEnumerable<Corrugate> corrugatesToCompare,
                      IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate,
                      ICarton carton,
                      bool recalculateCachedItem = false, int allowedTileCount = Int32.MaxValue);

        CartonOnCorrugate GetOptimalCorrugateForProductionGroup(IProducible producible, DTO.ProductionGroups.ProductionGroup machinePg, int tileCount);

        CartonOnCorrugate GetOptimalCorrugateForMachineGroup(IProducible producible, MachineGroup machineGroup, int tileCount);

        IEnumerable<Corrugate> Corrugates { get; }
    }
}