﻿namespace PackNet.Common.Interfaces.Services
{
    public interface ICachingService
    {
        bool TryGet<T>(string cacheKey, out T item);
        T GetItem<T>(string cacheKey);
        void CacheItem(string cacheKey, object obj);
    }
}