﻿
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services
{
    public interface ICodeGenerationService : IService
    {
        bool CanProduce(IProducible producible, IPacksizeCutCreaseMachine machine);
    }
}
