﻿using System;

namespace PackNet.Common.Interfaces.Services
{
    public interface IServiceLocator : IDisposable
    {
        T Locate<T>() where T: IService;

        bool TryLocate<T>(out T service) where T: IService;

        void RegisterAsService<T>(T service) where T : IService;

        void LoadPlugins();

        IObservable<IService> ServiceAddedObservable { get; }
    }

    public class ServiceNotFoundException : Exception
    {
      public ServiceNotFoundException(string format):base(format)
        {
        }
    }
}