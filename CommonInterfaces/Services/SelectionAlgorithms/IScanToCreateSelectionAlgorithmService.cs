﻿using System.Collections;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.ScanToCreate;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.SelectionAlgorithms
{
    public interface IScanToCreateSelectionAlgorithmService : ISelectionAlgorithmService
    {
        void AddToProductionGroupQueue(ScanToCreateProducible producible);

        void Persist(ScanToCreateProducible producible);

        IProducible GetNextJobForMachineGroup(MachineGroup machineGroup);
    }

    public interface ISelectionAlgorithmBase : IService
    {
        /// <summary>
        /// Get all known items
        /// You must return a collection that will not be modified while the workflow is running.  For example if an item is selected for production perhaps it was removed from this list...
        /// </summary>
        /// <returns></returns>
        IEnumerable<IProducible> GetAllStagedProducibles();

        /// <summary>
        /// Save and persist the items that have been staged.
        /// Ensure that you handle the possibility that items that are in this list may have been removed or completed by another thread.
        /// </summary>
        /// <param name="producibles"></param>
        void SaveStagedProducibles(IEnumerable<IProducible> producibles);
    }
}