﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.SelectionAlgorithms
{
    public interface IBoxFirstSelectionAlgorithmService : ISelectionAlgorithmService
    {
        void AddProducibles(IEnumerable<IProducible> producibles);

        IEnumerable<BoxFirstProducible> GetAllStagedJobsForMachineGroup(Guid machineGroupId);

        IProducible PrepareNextJobForMachineGroup(MachineGroup machineGroup, IEnumerable<IProducible> boxFirstProducibles);

        IEnumerable<IProducible> FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(MachineGroup machineGroup, IEnumerable<BoxFirstProducible> produciblesToFilter);

        IEnumerable<IProducible> GetProduciblesWhereMachineGroupContainsMostOptimalCorrugate(MachineGroup machienGroup, IEnumerable<BoxFirstProducible> produciblesToFilter);

        int GetTileCountToUse(Guid machineGroupId, BoxFirstProducible producible, IEnumerable<IProducible> availableProducibles);

        IEnumerable<IProducible> GetAllProduciblesWithStatuses(IEnumerable<ProducibleStatuses> statuses);

        void UpdateProducibleStatuses(IEnumerable<BoxFirstProducible> producibles, ProducibleStatuses status);

        IEnumerable<IProducible> AssignOptimalCorrugateAndTilingPartnersForProducibles(IEnumerable<BoxFirstProducible> producibles);

        void AddProduciblesButEnforceUniqueness(IEnumerable<IProducible> produciblesToAdd);
    }
}