using System;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.SelectionAlgorithms
{
    public interface IOrdersSelectionAlgorithmService : ISelectionAlgorithmService
    {
        /// <summary>
        /// Adds an order to the machineGroup queue
        /// </summary>
        /// <param name="machineGroupId"></param>
        /// <param name="order"></param>
        void AddOrderToMachineGroupQueue(Guid machineGroupId, Order order);
        
        /// <summary>
        /// Adds an order to the production group queue
        /// </summary>
        /// <param name="productionGroupId"></param>
        /// <param name="order"></param>
        void AddOrderToProductionGroupQueue(Guid productionGroupId, Order order);

        /// <summary>
        /// Get a Auto Job for the machine group
        /// </summary>
        /// <param name="machineGroupId"></param>
        /// <returns></returns>
        IProducible GetAutoJobForMachineGroup(Guid machineGroupId);

        /// <summary>
        /// Get a manual job for the machine group
        /// </summary>
        /// <param name="machineGroupId"></param>
        /// <returns></returns>
        IProducible GetManualJobForMachineGroup(Guid machineGroupId);
    }
}