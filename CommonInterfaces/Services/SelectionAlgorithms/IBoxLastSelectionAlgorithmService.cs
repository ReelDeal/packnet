﻿using PackNet.Common.Interfaces.DTO.Carton;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Scanning;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.SelectionAlgorithms
{
    public interface IBoxLastSelectionAlgorithmService : ISelectionAlgorithmService
    {
        IReadOnlyCollection<BoxLastProducible> Producibles { get; }
        IReadOnlyCollection<BoxLastProducible> TriggeredRequests { get; }

        /// <summary>
        /// Locates the next producible that should be created by the machine group
        /// </summary>
        /// <param name="machineGroup"></param>
        /// <returns></returns>
        IProducible GetNextJobForMachineGroup(MachineGroup machineGroup);

        /// <summary>
        /// Adds producibles to the selection algorithm, sorting them by production group
        /// </summary>
        void AddProducibles(IEnumerable<IProducible> producibles);

        bool Trigger(IScanTrigger triggerDetails);
    }
}