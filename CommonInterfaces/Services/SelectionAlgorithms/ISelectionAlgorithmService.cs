﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.SelectionAlgorithms
{
    public interface ISelectionAlgorithmService : IService
    {
        /// <summary>
        /// Get the producibles that the selection algorithm is currently aware of, optionally filtering by status
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        IReadOnlyCollection<IProducible> GetProducibles(ProducibleStatuses status = null);
    }
}
