﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services
{
    public interface IMachineGroupService : IService
    {
        IEnumerable<MachineGroup> MachineGroups { get; }
        MachineGroup Create(MachineGroup machineGroup);
        MachineGroup Update(MachineGroup machineGroup);
        void Delete(MachineGroup machineGroup);

        MachineGroup FindByMachineId(Guid id);
        MachineGroup FindByMachineGroupId(Guid id);

        /// <summary>
        /// Returns all machines configured for the machine group with the given id. 
        /// </summary>
        /// <param name="id">Id of the machine group</param>
        /// <returns>All machines configured for the group or null if the machine group does not exist</returns>
        IEnumerable<IMachine> GetMachinesInGroup(Guid id); 

        void CompleteQueueAndPause(Guid machineGroupId);

        IProducible GetProducibleFirstInQueue(MachineGroup machineGroup);
    }
}