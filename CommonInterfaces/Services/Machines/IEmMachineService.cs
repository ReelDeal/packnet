﻿using System;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.Machines
{
    public interface IEmMachineService : ICorrugateBasedMachineService<EmMachine>
    {
        EmMachine Create(EmMachine machine);

        void Delete(EmMachine machine);

        EmMachine Update(EmMachine machine);
        
        EmMachine Find(Guid machineId);

        void Produce(EmMachine machine, IProducible producible);
    }
}
