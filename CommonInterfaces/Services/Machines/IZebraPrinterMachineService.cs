﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.Services.Machines
{
    public interface IZebraPrinterMachineService : IMachineService<ZebraPrinter>
    {
        ZebraPrinter Create(ZebraPrinter machine);
        void Delete(ZebraPrinter machine);
        ZebraPrinter Update(ZebraPrinter machine);

        IEnumerable<ZebraPrinter> GetMachines();

        ZebraPrinter Find(Guid machineId);
    }
}