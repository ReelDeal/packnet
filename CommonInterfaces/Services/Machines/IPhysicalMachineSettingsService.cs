﻿using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.Common.Interfaces.Services.Machines
{
    public interface IPhysicalMachineSettingsService : IService
    {
        EmPhysicalMachineSettings GetEmPhysicalMachineSettings(string fileName);
        FusionPhysicalMachineSettings GetFusionPhysicalMachineSettings(string fileName);
        void UpdatePhysicalMachineSettings(PhysicalMachineSettings physicalMachineSettings);
    }
}