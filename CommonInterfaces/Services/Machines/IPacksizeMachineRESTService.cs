﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace PackNet.Common.Interfaces.Services.Machines
{
    /// <summary>
    /// RESt Service that accepts machine commands from Packsize machines
    /// </summary>
    [ServiceContract]
    public interface IPacksizeMachineRESTService : IService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "post")]
        void PostData(Stream data);
    }
}
