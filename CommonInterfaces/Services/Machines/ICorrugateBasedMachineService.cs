﻿using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.Services.Machines
{
    public interface ICorrugateBasedMachineService<T> : IMachineService<T> where T : IMachine
    {
        void ConfigureTracks(ITrackBasedCutCreaseMachine machine);

        bool IsCorrugatesUsableOnMachine(ITrackBasedCutCreaseMachine machine);
    }
}
