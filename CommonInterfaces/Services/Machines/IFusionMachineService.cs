﻿using System;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.Machines
{
    public interface IFusionMachineService : ICorrugateBasedMachineService<FusionMachine>
    {
        FusionMachine Create(FusionMachine machine);
        void Delete(FusionMachine machine);
        FusionMachine Update(FusionMachine machine);
        
        FusionMachine Find(Guid machineId);

        void Produce(FusionMachine machine, IProducible producible);
    }
}
