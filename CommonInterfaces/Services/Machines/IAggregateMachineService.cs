﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services.Machines
{
	public interface IAggregateMachineService : IService
	{
		/// <summary>
		/// All configured machines in the system
		/// </summary>
		IEnumerable<IMachine> Machines { get; }
		
		/// <summary>
		/// Locate a machine by machine Identifier
		/// </summary>
		/// <param name="machineId"></param>
		/// <returns></returns>
		IMachine FindById(Guid machineId);

        /// <summary>
        /// Locate a machine by machine Alias
        /// </summary>
        /// <param name="alias"></param>
        /// <returns></returns>
	    IMachine FindByAlias(string alias);
            
        /// <summary>
        /// Determines if a network connected machine already exists in the system with the specified Port
        /// </summary>
        /// <param name="machine"></param>
        /// <returns></returns>
	    ResultTypes ValidateNetworkMachinePortConfiguration(INetworkConnectedMachine machine);

		/// <summary>
		/// Determines if a network connected machine already exists in the system with the specified IP/Port combination
		/// </summary>
		/// <param name="machine"></param>
		/// <returns>True if a machine exists</returns>
		ResultTypes ValidateNetworkMachineConfiguration(INetworkConnectedMachine machine);

		/// <summary>
		/// Registers a machine service 
		/// </summary>
		/// <param name="machineServiceToRegister"></param>
		void RegisterMachineService(IMachineService<IMachine> machineServiceToRegister);

		/// <summary>
		/// returns true if any machine has the corrugate currently loaded
		/// </summary>
		/// <param name="corrugate"></param>
		/// <returns></returns>
		bool IsCorrugateLoadedOnAnyMachine(Corrugate corrugate);

		/// <summary>
		/// Look up machine service and send producible to machine.
		/// </summary>
		/// <param name="machineId"></param>
		/// <param name="producible"></param>
		void Produce(Guid machineId, IProducible producible);

        /// <summary>
        /// Determine if the machine can produce the producible
        /// </summary>
        /// <param name="machineId"></param>
        /// <param name="producible"></param>
        /// <returns></returns>
	    bool CanProduce(Guid machineId, IProducible producible);
	}
}

