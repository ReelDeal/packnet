﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.Services
{
    public interface ICartonService : IService
    {
        IPacksizeCarton CreateCartonRequestOrder(
           IEnumerable<ICarton> requests,
           CartonOnCorrugate cartonOnCorrugate,
           IPacksizeCutCreaseMachine machine,
           Classification classification,
           bool alignTilesInEnd,
           bool internalPrinting,
           int? packStation = null,
           bool isRotated = false);

        IPacksizeCarton CreateCartonRequestOrder(
            IEnumerable<ICarton> requests,
            CartonOnCorrugate cartonOnCorrugate,
            IPacksizeCutCreaseMachine machine,
            Classification classification,
            bool alignTilesInEnd,
            bool internalPrinting,
            Func<int, bool, IPackagingDesign> designRetriever,
            int? packStation,
            bool isRotated);

        bool RequestsOkToSendToMachineAndRegister(IEnumerable<ICarton> cartonsInOrder, Guid? machineId = null);
    }
}