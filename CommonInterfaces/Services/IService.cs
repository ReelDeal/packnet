﻿using System;

namespace PackNet.Common.Interfaces.Services
{
    public interface IService : IDisposable
    {
        string Name { get; }
    }
}