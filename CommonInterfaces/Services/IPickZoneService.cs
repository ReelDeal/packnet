﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Services
{
    public interface IPickZoneService : IService
    {
        /// <summary>
        /// Gets all pick zones in a specified status
        /// </summary>
        /// <param name="zoneStatus"></param>
        /// <returns></returns>
        IEnumerable<PickZone> GetPickZonesByStatus(PickZoneStatuses zoneStatus);

        IEnumerable<PickZone> PickZones { get; }

        /// <summary>
        /// Sets a pick zone status
        /// </summary>
        /// <param name="pickZone"></param>
        void SetPickZoneStatus(PickZone pickZone);

        /// <summary>
        /// Returns all pick zones
        /// </summary>
        /// <returns></returns>
        ConcurrentList<PickZone> GetPickZones();

        /// <summary>
        /// Updates an existing pick zone
        /// </summary>
        /// <param name="pickZoneToUpdate"></param>
        /// <returns></returns>
        PickZone UpdatePickZone(PickZone pickZoneToUpdate);

        /// <summary>
        /// Deletes a pick zone
        /// </summary>
        /// <param name="pickZoneToDelete"></param>
        void DeletePickZone(PickZone pickZoneToDelete);

        /// <summary>
        /// Creates a pick zone
        /// </summary>
        /// <param name="pickZoneToCreate"></param>
        /// <returns></returns>
        PickZone CreatePickZone(PickZone pickZoneToCreate);
    }
}
