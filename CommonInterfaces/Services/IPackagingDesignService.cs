﻿using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;

namespace PackNet.Common.Interfaces.Services
{
    public interface IPackagingDesignService : IService
    {
        PackagingDesign GetDesignFromId(int designId);

        PhysicalDesign GetDesignForCarton(ICarton carton, Corrugate corrugate, OrientationEnum rotation);

        string GetDesignName(int designId);
        
        bool HasDesignWithId(int designId);
    }
}