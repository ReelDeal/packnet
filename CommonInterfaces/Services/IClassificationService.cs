﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Services
{
    public interface IClassificationService : IService
    {
        IEnumerable<Classification> Classifications { get; }

        IEnumerable<Classification> GetClassificationsByStatus(ClassificationStatuses status);

        void DeleteClassification(Classification classification);

        ConcurrentList<Classification> GetClassifications();

        Classification UpdateClassification(Classification classification);

        void CreateClassification(Classification classification);

        void UpdateProducibleCount(Message<IEnumerable<BoxFirstProducible>> msg);
    }
}