﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Services
{
    public interface IViewModelService:IService
    {
        JArray CreateSearchResult(IEnumerable<IProducible> convertFrom);
        Tuple<DateTime,JObject> Convert(IProducible producible);
    }
}
