﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Services
{
    public interface IProductionGroupService : IService
    {
        IEnumerable<ProductionGroup> ProductionGroups { get; }

        //ProductionGroup FindByMachineGroupId(Guid id);
        ProductionGroup FindByAlias(String alias);
        ProductionGroup Find(Guid id);
        
        /// <summary>
        /// Returns the currently configured production group that the machine group is assigned to
        /// </summary>
        /// <param name="machineGroupId"></param>
        /// <returns></returns>
        ProductionGroup GetProductionGroupForMachineGroup(Guid machineGroupId);

        /// <summary>
        /// Removes the machine group from the production group that the machine group is assigned to
        /// </summary>
        /// <param name="machineGroup"></param>
        void RemoveMachineGroupFromProductionGroup(MachineGroup machineGroup);

        /// <summary>
        /// Returns all production groups that have the corrugate assigned
        /// </summary>
        /// <param name="corrugateId"></param>
        /// <returns></returns>
        ConcurrentList<ProductionGroup> GetProductionGroupsForCorrugate(Guid corrugateId);

        /// <summary>
        /// Removes the corrugate from all the production groups that the corrugate is assigned to
        /// </summary>
        /// <param name="corrugate"></param>
        void RemoveCorrugateFromProductionGroups(Corrugate corrugate);

        ProductionGroup FindByCartonPropertyGroupId(Guid id);

        ProductionGroup FindByCartonPropertyGroupId(IEnumerable<ProductionGroup> productionGroups, Guid id);

        ProductionGroup FindByCartonPropertyGroupAlias(string alias);

        ProductionGroup FindByCartonPropertyGroupAlias(IEnumerable<ProductionGroup> productionGroups, string alias);
    }
}