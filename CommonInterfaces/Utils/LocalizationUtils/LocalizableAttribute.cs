﻿namespace PackNet.Common.Interfaces.Utils.LocalizationUtils
{
    using System;

    [AttributeUsage(AttributeTargets.All)]
    public sealed class LocalizableAttribute : Attribute
    {
        /// <summary>
        /// A hint to use when looking for the string, should be the fully qualified namespace of the resource class
        /// </summary>
        public string ResourceNameHint { get; set; }

        /// <summary>
        /// A default value to use if the resource value is not found
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// The name of the key to use in the resource file
        /// </summary>
        public string ResourceKeyName { get; set; }
    }
}