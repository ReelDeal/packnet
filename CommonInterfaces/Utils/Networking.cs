﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

using PackNet.Common.Interfaces.L10N;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.Common.Interfaces.Utils
{
    public static class Networking
    {
        /// <summary>
        /// Return an IPEndPoint object given a IP/DNS entry and a port
        /// </summary>
        /// <param name="ipOrDnsName">string value of an IP address or DNS name</param>
        /// <param name="port">A valid port number</param>
        /// <param name="localPreference">
        /// Return the local IP address instead of the internet facing IP if available.  
        /// Commonly known local IP class 1: 192.168.x.x, 10.x.x.x, 172.16-31.x.x
        /// </param>
        /// <returns></returns>
        public static IPEndPoint FindIpEndPoint(string ipOrDnsName, int port, bool localPreference)
        {
            return new IPEndPoint(FindIPAddress(ipOrDnsName, localPreference), port);
        }

        public static IPEndPoint FindIpEndPointFromAddressWithPort(string ipOrDnsName, bool localPreference)
        {
            var addressAndPort = ipOrDnsName.Split(':');
            if (addressAndPort.Length != 2)
            {
                throw new ArgumentException(Strings.Networking_FindIpEndPointFromAddressWithPort_IpOrDnsName_is_expected_in_the_format_address_port_, "ipOrDnsName");
            }

            var address = addressAndPort[0];
            int port;
            try
            {
                port = int.Parse(addressAndPort[1]);
            }
            catch (Exception)
            {
                throw new ArgumentException("Port number must be expressed as an integer.", "ipOrDnsName");
            }

            return new IPEndPoint(FindIPAddress(address, localPreference), port);
        }

        /// <summary>
        /// Given an IP Address and optional port will return an IPEndPoint
        /// </summary>
        /// <param name="ipOrDnsName">IP or DNS name with or withour port (see examples)</param>
        /// <returns>IPEndPoint representation of the given IPAddress or DNS name with Port, defaul zero (any available) if not provided</returns>
        /// <example>192,168.1.1
        ///192.168.1.1:80
        ///localhost
        ///localhost:80</example>
        public static IPEndPoint ParseIpAddress(string ipOrDnsName)
        {
            var addressAndPort = ipOrDnsName.Split(':');
            return addressAndPort.Length > 1
                ? FindIpEndPointFromAddressWithPort(ipOrDnsName, true)
                : new IPEndPoint(FindIPAddress(ipOrDnsName, true), 0);
        }

        #region DNS
        /// <summary>
        /// For the machine calling this method find it's IP address
        /// </summary>
        /// <param name="localPreference">
        /// Return the local IP address instead of the internet facing IP if available.  
        /// Commonly known local IP class 1: 192.168.x.x, 10.x.x.x, 172.16-31.x.x
        /// </param>
        /// <returns></returns>
        public static IPAddress FindIPAddress(bool localPreference)
        {
            return FindIPAddress(Dns.GetHostEntry(Dns.GetHostName()), localPreference);
        }

        /// <summary>
        /// Given a string value of an IP or DNS name return us an object
        /// </summary>
        /// <param name="ipOrDnsName">Parse the string value</param>
        /// <param name="localPreference">
        /// Return the local IP address instead of the internet facing IP if available.  
        /// Commonly known local IP class 1: 192.168.x.x, 10.x.x.x, 172.16-31.x.x
        /// </param>
        /// <returns></returns>
        public static IPAddress FindIPAddress(string ipOrDnsName, bool localPreference)
        {
            IPAddress address;
            return IPAddress.TryParse(ipOrDnsName, out address)
                       ? address
                       : FindIPAddress(Dns.GetHostEntry(ipOrDnsName), localPreference);
        }

        /// <summary>
        /// Given host and port create an IPAddress object.
        /// </summary>
        /// <param name="host"></param>
        /// <param name="localPreference">
        /// Return the local IP address instead of the internet facing IP if available.  
        /// Commonly known local IP class 1&2: 10.x.x.x, 192.168.x.x,  172.16-31.x.x</param>
        /// <returns></returns>
        public static IPAddress FindIPAddress(IPHostEntry host, bool localPreference)
        {
            IPAddress lastValidAddress = null;
            if (host == null)
            {
                throw new ArgumentNullException("host");
            }

            if (host.AddressList.Length == 1)
            {
                return host.AddressList[0];
            }

            foreach (IPAddress address in host.AddressList)
            {
                bool local = IsLocal(address);
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                    if (local == localPreference)
                    {
                        return address;
                    }

                    lastValidAddress = address;
                }
            }

            return lastValidAddress;
        }

        public static bool IsLocal(IPAddress address)
        {
            if (address == null)
            {
                throw new ArgumentNullException("address");
            }

            var addr = address.GetAddressBytes();

            return addr[0] == 10
            || (addr[0] == 192 && addr[1] == 168)
            || (addr[0] == 172 && addr[1] >= 16 && addr[1] <= 31);
        }
        #endregion

        public static IPEndPoint ParseHostName(string hostName)
        {
            try
            {
                if (hostName.Split(':').Length == 2)
                {
                    return FindIpEndPointFromAddressWithPort(hostName, false);
                }
                return FindIpEndPoint(hostName, 80, false);
            }
            catch (Exception)
            {
                throw new Exception("Cannot resolve host name '" + hostName + "' to an IPv4 address.");
            }
        }

        /// <summary>
        /// Checks if an IP address is reachable. Spurred by need to to test a default gateway before setting it on a discovered device.
        /// </summary>
        public static bool IsIpAddressReachable(string ipAddressToCheck)
        {
            bool parsed = false;
            IPAddress parsedIpAddress;
            parsed = IPAddress.TryParse(ipAddressToCheck, out parsedIpAddress);

            if (!parsed)
            {
                return false;
            }

            return IsPingable(parsedIpAddress);
        }

        private static bool IsPingable(IPAddress ipAddressToCheck)
        {
            const string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            const int timeout = 120;

            Ping pingSender = new Ping();
            // Use default TTL (128) but change the fragmentation behavior to don't fragment
            PingOptions options = new PingOptions { DontFragment = true };
            byte[] buffer = Encoding.ASCII.GetBytes(data);
            PingReply reply = pingSender.Send(ipAddressToCheck, timeout, buffer, options);

            return reply != null && reply.Status == IPStatus.Success;
        }

        public static void CheckIpAddressAndWarnIfNotBoundToThisMachine(IPAddress ipAddressToTest, ILogger logger)
        {
            if (ipAddressToTest != null)
            {
                bool isIpAddressOnMachine = Networking.IsIpAddressBoundToThisMachine(ipAddressToTest);

                if (!isIpAddressOnMachine)
                {
                    string badCallbackErrorMessage = string.Format(
                            "WARNING: You are attempting to use a callback address which is not bound to this server. The ip address in question is '{0}'.",
                            ipAddressToTest);
                    Console.WriteLine(badCallbackErrorMessage);
                    logger.Log(LogLevel.Error, badCallbackErrorMessage);
                }
            }
        }

        private static bool IsIpAddressBoundToThisMachine(IPAddress ipV4AddressToTest)
        {
            //Bail out
            if (ipV4AddressToTest == null)
            {
                return false;
            }

            const string localhostIpAddressString = "127.0.0.1";

            if (ipV4AddressToTest.ToString() == localhostIpAddressString)
            {
                Console.WriteLine("WARNING: Your IP address is set to {0} (probably for simulators). You should not see this if you are using all real machines.", localhostIpAddressString);
                return true;
            }

            // Find host by this machine's hostname
            IPHostEntry iphostentry = Dns.GetHostEntry(Environment.MachineName);

            //return true if the ip address matches anything bound to the machine
            return iphostentry.AddressList.Any(ipaddress => ipaddress.ToString() == ipV4AddressToTest.ToString());
        }
    }
}
