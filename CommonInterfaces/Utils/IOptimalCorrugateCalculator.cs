﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.Utils
{
    public interface IOptimalCorrugateCalculator
    {
        CartonOnCorrugate GetOptimalCorrugate(
            IEnumerable<Corrugate> corrugatesToCompare,
            IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate,
            ICarton carton,
            int allowedTileCount, bool recalculateCachedItem = false);

        IEnumerable<CartonOnCorrugate> GetOptimalCorrugates(
            IEnumerable<Corrugate> corrugatesToCompare,
            IEnumerable<IPacksizeCutCreaseMachine> machinesToEvaluate,
            ICarton carton, bool recalculateCachedItem = false, int allowedTileCount = Int32.MaxValue);

    }
}