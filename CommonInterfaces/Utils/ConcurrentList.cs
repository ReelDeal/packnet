﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace PackNet.Common.Interfaces.Utils
{
    public class ConcurrentList<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable, IList, ICollection, IReadOnlyList<T>, IReadOnlyCollection<T>
    {
        private object lockObj = new object();
        private ImmutableList<T> backingList = ImmutableList<T>.Empty;

        public ConcurrentList()
        {

        }

        public ConcurrentList(IEnumerable<T> seedList)
        {
            backingList = backingList.AddRange(seedList);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return backingList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(T item)
        {
            lock (lockObj)
                backingList = backingList.Add(item);
        }

        public void AddRange(IEnumerable<T> list)
        {
            lock (lockObj)
            {
                backingList = backingList.AddRange(list);
            }
        }

        public void ReplaceAll(IEnumerable<T> list)
        {
            lock (lockObj)
            {
                backingList = backingList.Clear();
                backingList = backingList.AddRange(list);
            }
        }

        public void RemoveAll(Predicate<T> pred)
        {
            lock (lockObj)
            {
                backingList = backingList.RemoveAll(pred);
            }
        }

        public int Add(object value)
        {
            lock (lockObj)
            {
                var val = GetT(value);
                backingList = backingList.Add(val);
                return backingList.Count;
            }
        }

        public bool Contains(object value)
        {
            return backingList.Contains(GetT(value));
        }

        public void Clear()
        {
            lock (lockObj)
                backingList = ImmutableList<T>.Empty;
        }

        public int IndexOf(object value)
        {
            return backingList.IndexOf(GetT(value));
        }

        public void Insert(int index, object value)
        {
            lock (lockObj)
                backingList = backingList.Insert(index, GetT(value));
        }

        public void Remove(object value)
        {
            lock (lockObj)
                backingList = backingList.Remove(GetT(value));
        }

        public void RemoveAt(int index)
        {
            lock (lockObj)
                backingList = backingList.RemoveAt(index);
        }

        public bool IsFixedSize { get; private set; }

        public bool Contains(T item)
        {
            return backingList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            backingList.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item)
        {
            if(!Contains(item))
                return false;

            lock (lockObj)
                backingList = backingList.Remove(item);
            return true;
        }

        public void CopyTo(Array array, int index)
        {
            var destCounter = index;
            for (var i = 0; i < backingList.Count; i++)
            {
                array.SetValue(backingList[i], destCounter);
            }
        }

        public int Count
        {
            get { return backingList.Count; }
        }

        public object SyncRoot { get { return lockObj; } }
        public bool IsSynchronized { get; private set; }

        public bool IsReadOnly { get; private set; }

        public int IndexOf(T item)
        {
            return backingList.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            lock (lockObj)
                backingList = backingList.Insert(index, item);
        }

        object IList.this[int index]
        {
            get { return backingList[index]; }
            set { 
                lock (lockObj)
                {
                    backingList = backingList.RemoveAt(index);
                    backingList = backingList.Insert(index, GetT(value));
                } }
        }

        public T this[int index]
        {
            get { return backingList[index]; }
            set
            {
                lock (lockObj)
                {
                    backingList = backingList.RemoveAt(index);
                    backingList = backingList.Insert(index, value);
                }
            }
        }

        /// <summary>
        /// Probably needs a little error handling here
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private T GetT(object value)
        {
            return (T)value;
        }
    }
}
