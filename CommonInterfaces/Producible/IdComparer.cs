﻿using System.Collections.Generic;

namespace PackNet.Common.Interfaces.Producible
{
    public class IdComparer<T> : IEqualityComparer<T> where T: IProducible
    {
        public bool Equals(T x, T y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(T obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}