﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.Common.Interfaces.Producible
{
    public static class RestrictionExtensions
    {
        public static T GetRestrictionOfTypeInBasicRestriction<T>(this IEnumerable<IRestriction> x)
        {
            var restriction = x.FirstOrDefault(r => r is BasicRestriction<T>) as BasicRestriction<T>;
            if (restriction != null)
                return restriction.Value;
            return default(T);
        }

        public static T GetRestrictionOfType<T>(this IEnumerable<IRestriction> x) where T:class 
        {
            T restriction;
            restriction = x.FirstOrDefault(r => r is T) as T;
            if (restriction != null)
                return restriction;
            return default(T);
        }


    }
}
