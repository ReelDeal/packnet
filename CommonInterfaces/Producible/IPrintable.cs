﻿
namespace PackNet.Common.Interfaces.Producible
{
    public interface IPrintable : IProducible
    {
        /// <summary>
        /// Gets or sets the print data.
        /// </summary>
        /// <value>
        /// The print data.
        /// </value>
        object PrintData { get; set; }
    }
}
