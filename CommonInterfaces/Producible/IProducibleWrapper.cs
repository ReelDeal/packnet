﻿using PackNet.Common.Interfaces.Repositories;

namespace PackNet.Common.Interfaces.Producible
{
    /// <summary>
    /// Use this interface to wrap a producible with decorator properties
    /// </summary>
    public interface IProducibleWrapper:IProducible
    {
        IProducible Producible { get;  }
    }
}