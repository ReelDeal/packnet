﻿using System.Collections.Generic;

namespace PackNet.Common.Interfaces.Producible
{
    public class CustomerUniqueIdComparer<T> : IEqualityComparer<T> where T : IProducible
    {
        public bool Equals(T x, T y)
        {
            return x.CustomerUniqueId == y.CustomerUniqueId;
        }

        public int GetHashCode(T obj)
        {
            return obj.CustomerUniqueId.GetHashCode();
        }
    }
}