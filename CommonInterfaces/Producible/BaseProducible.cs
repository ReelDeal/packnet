﻿using System.Linq;

using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Producible
{
    /// <summary>
    /// Base implementation of an IProducible
    /// </summary>
    public abstract class BaseProducible : IProducible
    {
        protected Subject<ProducibleStatuses> producibleStatusSubject = new Subject<ProducibleStatuses>();
        private ProducibleStatuses producibleStatus;
        private Guid id;
        private ConcurrentList<IRestriction> restrictions;
        private ConcurrentList<ProducibleHistory> history;

        private object statusLock = new object();
        private DateTime created;

        protected BaseProducible()
        {
            producibleStatusSubject = new Subject<ProducibleStatuses>();
            Created = DateTime.UtcNow;
        }

        /// <summary>
        /// Unique id for the instance of the producible item
        /// </summary>
        public virtual Guid Id
        {
            get
            {
                if (id == Guid.Empty)
                    id = Guid.NewGuid();
                return id;
            }
            set { id = value; }
        }

        [BsonElement("expiry")]
        public DateTime Expiry { get; set; }

        public virtual DateTime Created
        {
            get { return created; }
            set { created = Expiry = value; }
        }

        /// <summary>
        /// Gets or sets the customer unique identifier.
        /// </summary>
        /// <value>
        /// The customer unique identifier.
        /// </value>
        public virtual string CustomerUniqueId { get; set; }
        public virtual string CustomerDescription { get; set; }

        /// <summary>
        /// Identifier of the type of producible item
        /// </summary>
        public abstract ProducibleTypes ProducibleType { get; }

        /// <summary>
        /// A list of extra properties that need to be considered when determining when, who and how this item can be produced.
        /// </summary>
        public virtual ConcurrentList<IRestriction> Restrictions
        {
            get { return restrictions ?? (restrictions = new ConcurrentList<IRestriction>()); }
            protected set { restrictions = value; }
        }

        /// <summary>
        /// Current Producible status
        /// </summary>
        public virtual ProducibleStatuses ProducibleStatus
        {
            get { return producibleStatus ?? (producibleStatus = NotInProductionProducibleStatuses.ProducibleImported); }
            set
            {
                lock (statusLock ?? (statusLock = new object()))
                {
                    if (value == producibleStatus)
                        return;

                    if (History != null)
                        //this happens only when we are reading from the db and the property has not yet been set.  Ignore.
                    {
                        //Ensure that the notes field follows the ; to seperate key value pairs and : seperates the key and value
                        //Example: ProducedOnMachineGroupId:uhasdf0-weadf-333;
                        //TODO: What the hell is this HACK???
                        string machineInfo = null;
                        if (ProducedOnMachineGroupId != Guid.Empty)
                            machineInfo = new JObject { { "ProducedOnMachineGroupId", ProducedOnMachineGroupId.ToString() } }.ToString(Formatting.None);
                        if (ProduceOnMachineId != Guid.Empty)
                            machineInfo = new JObject { { "ProducedOnMachineId", ProduceOnMachineId.ToString() } }.ToString(Formatting.None);

                        var crh = new ProducibleHistory(producibleStatus, value, machineInfo);
                        History.Add(crh);
                    }

                    producibleStatus = value;
                }
             
                producibleStatusSubject = producibleStatusSubject ?? new Subject<ProducibleStatuses>();
                producibleStatusSubject.OnNext(producibleStatus);
            }
        }
        
        public virtual DateTime ProducibleStatusLastChangedDateTime {
            get
            {
                var lastItem = History.LastOrDefault();
                if (lastItem == null)
                {
                    return DateTime.MinValue;
                }

                return lastItem.OccuredOn;
            }
        }

        /// <summary>
        /// Observable to notify listeners of status changes
        /// </summary>
        [BsonIgnore]
        [JsonIgnore]
        public IObservable<ProducibleStatuses> ProducibleStatusObservable { get { return producibleStatusSubject.AsObservable(); } }

        /// <summary>
        /// History of all ProducibleStatuses the carton has entered
        /// </summary>
        public virtual ConcurrentList<ProducibleHistory> History
        {
            get { return history ?? (history = new ConcurrentList<ProducibleHistory>()); }
            set { history = value; }
        }

        /// <summary>
        /// Machine group that this producible was produced on
        /// </summary>
        public virtual Guid ProducedOnMachineGroupId { get; set; }

        /// <summary>
        /// Gets or sets the machine produce on identifier.
        /// </summary>
        /// <value>
        /// The machine identifier.
        /// </value>
        public virtual Guid ProduceOnMachineId { get; set; }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public virtual bool Equals(IProducible other)
        {
            return Id == other.Id;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Producible:{1} (Id:'{0}', Type:'{2}', Producible Status:'{3}')", Id, CustomerUniqueId, ProducibleType, ProducibleStatus);
        }

    }
}
