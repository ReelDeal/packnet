﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.Producible
{
    public class ProductionStatusMessages : MessageTypes
    {
        public static ProductionStatusMessages ProductionInProgress = new ProductionStatusMessages("ProductionInProgress");
        public static ProductionStatusMessages ProductionStarted = new ProductionStatusMessages("ProductionStarted");
        public static ProductionStatusMessages ProductionCompleted = new ProductionStatusMessages("ProductionCompleted");
        public static ProductionStatusMessages ProductionCancelled = new ProductionStatusMessages("ProductionCancelled");

        protected ProductionStatusMessages(string displayName) : base(0, displayName){ }
    }
}
