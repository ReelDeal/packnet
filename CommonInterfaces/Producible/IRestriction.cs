﻿namespace PackNet.Common.Interfaces.Producible
{
    /// <summary>
    /// IRestriction represents the requirements that have been identified for a producible, the machine that creates the producible must satisfy these requirements.
    /// *** Ensure that when you create a restriction that you register it with the discriminators using MongoDbHelpers.TryRegisterDiscriminator(your restriction)
    /// </summary>
    public interface IRestriction
    {
    }
}