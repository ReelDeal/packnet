﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Repositories;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Producible
{
    /// <summary>
    /// An item that can be produced
    /// </summary>
    public interface IProducible : IPersistable, IEquatable<IProducible>
    {
        DateTime Created { get; set; }
        /// <summary>
        /// Gets or sets the customer unique identifier.
        /// </summary>
        /// <value>
        /// The customer unique identifier.
        /// </value>
        string CustomerUniqueId { get; set; }

        /// <summary>
        /// Description used by customer
        /// </summary>
        string CustomerDescription { get; set; }
        
        /// <summary>
        /// Identifier of the type of producible item
        /// </summary>
        ProducibleTypes ProducibleType { get; }

        /// <summary>
        /// A list of extra properties that need to be considered when determining when, who and how this item can be produced.
        /// By default all restrictions here must be satisfied by the machine group making this by default work like AllAggregateRestriction.  
        /// Another aggregate restriction is AnyAggregateRestriction which means only one of the items must be satisfied.  
        /// </summary>
        ConcurrentList<IRestriction> Restrictions { get; }

        /// <summary>
        /// Current Producible status
        /// </summary>
        ProducibleStatuses ProducibleStatus { get; set; }

        /// <summary>
        /// Observable to notify listeners of status changes
        /// </summary>
        IObservable<ProducibleStatuses> ProducibleStatusObservable { get; }

        /// <summary>
        /// History of all ProducibleStatuses the carton has entered
        /// </summary>
        ConcurrentList<ProducibleHistory> History { get; set; }

        /// <summary>
        /// Machine group that this producible was produced on
        /// </summary>
        Guid ProducedOnMachineGroupId { get; set; }

        /// <summary>
        /// Machine that was selected to produce this item.
        /// </summary>
        Guid ProduceOnMachineId { get; set; }

        DateTime ProducibleStatusLastChangedDateTime { get; }


    }


    /// <summary>
    /// Do not use this class directly.  It is only used so we can deserialize data from the UI
    /// </summary>
    public class Producible : IProducible
    {
        public Guid Id { get; set; }

        public bool Equals(IProducible other)
        {
            throw new NotImplementedException();
        }

        public DateTime Created { get; set; }
        public string CustomerUniqueId { get; set; }
        public string CustomerDescription { get; set; }
        public ProducibleTypes ProducibleType { get; private set; }
        public ConcurrentList<IRestriction> Restrictions { get; private set; }
        public ProducibleStatuses ProducibleStatus { get; set; }
        public IObservable<ProducibleStatuses> ProducibleStatusObservable { get; private set; }
        public ConcurrentList<ProducibleHistory> History { get; set; }
        public Guid ProducedOnMachineGroupId { get; set; }
        public Guid ProduceOnMachineId { get; set; }

        public DateTime ProducibleStatusLastChangedDateTime { get; private set; }
        
    }
}
