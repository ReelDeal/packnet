﻿namespace PackNet.Common.Interfaces.Logging
{
    public enum LogLevel
    {
        Info = 0,
        Trace = 1,
        Debug = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5
    }

    public interface ILogMessage
    {
        ILogger Log { get; }

        string Message { get; }

        LogLevel LogLevel { get; }
    }
}
