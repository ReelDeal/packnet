﻿namespace PackNet.Common.Interfaces.Logging
{
    public interface ILoggingBackend
    {
        ILogger GetLog();

        ILogger GetLogFor(string logName);
    }
}
