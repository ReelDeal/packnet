﻿using System;

using PackNet.Common.Interfaces.Services;

namespace PackNet.Common.Interfaces.Logging
{
    public interface ILogger : IService
    {
        string LogName { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logLevel">
        /// You should only log message equal to or above the Configured log level
        /// </param>
        /// <param name="logMessage"></param>
        /// <param name="args"></param>

        void Log(LogLevel logLevel, string logMessage, params object[] args);

        void Log<T>(LogLevel logLevel, string id, string logMessage, params object[] args);
        
        /// <summary>
        /// Use this call any time you may have some conditional logging like log some IEnumerable.Count() only if we are in debug mode.
        /// Log(LogLevel.Debug, ()=> "somestring");
        /// Log(LogLevel.Debug, ()=> string.Format("{0}", someVar));
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="logMessage"></param>
        void Log(LogLevel logLevel, Func<string> logMessage);

        void Log(LogLevel logLevel, string logMessage);

        /// <summary>
        /// 
        /// </summary>
        /// You should only log message equal to or above the Configured log level
        /// <param name="logLevel"></param>
        /// <param name="message"></param>
        /// <param name="exception"></param>
        /// <param name="args"></param>
        void LogException(LogLevel logLevel, string message, Exception exception, params object[] args);
        
    }
}
