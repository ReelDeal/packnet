﻿namespace PackNet.Common.Interfaces.Enums
{
	public sealed class UserNotificationMessages : MessageTypes
	{
		/* Commands */
		public static readonly UserNotificationMessages GetActiveUserNotificationsByUserId = new UserNotificationMessages("GetActiveUserNotificationsByUserId");
		public static readonly UserNotificationMessages DismissUserNotification = new UserNotificationMessages("DismissUserNotification");
        public static readonly UserNotificationMessages DismissAllUserNotificationsForMg = new UserNotificationMessages("DismissAllUserNotificationsForMg");
        public static readonly UserNotificationMessages DismissAllUserNotifications = new UserNotificationMessages("DismissAllUserNotifications");
		public static readonly UserNotificationMessages SearchUserNotifications = new UserNotificationMessages("SearchUserNotifications");

		/* Responses */
		public static readonly UserNotificationMessages ActiveUserNotificationsByUserId = new UserNotificationMessages("ActiveUserNotificationsByUserId");
		public static readonly UserNotificationMessages DismissedUserNotification = new UserNotificationMessages("DismissedUserNotification");
		public static readonly UserNotificationMessages DismissedAllUserNotifications = new UserNotificationMessages("DismissedAllUserNotifications");
		public static readonly UserNotificationMessages PublishedUserNotification = new UserNotificationMessages("PublishedUserNotification");
		public static readonly UserNotificationMessages SearchedUserNotifications = new UserNotificationMessages("SearchedUserNotifications");

		private UserNotificationMessages(string name)
			: base(name)
		{
		}
	}
}