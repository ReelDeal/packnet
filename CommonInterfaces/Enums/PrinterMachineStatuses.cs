﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class PrinterMachineStatuses : Enumeration
    {
        //todo remove this type
        //public static readonly PrinterMachineStatuses Offline = new PrinterMachineStatuses("Offline");
        //public static readonly PrinterMachineStatuses Ready = new PrinterMachineStatuses("Ready");
        //public static readonly PrinterMachineStatuses Busy = new PrinterMachineStatuses("Busy");
        //public static readonly PrinterMachineStatuses NotReady = new PrinterMachineStatuses("NotReady");
        //public static readonly PrinterMachineStatuses Paused = new PrinterMachineStatuses("Paused");
        //public static readonly PrinterMachineStatuses Error = new PrinterMachineStatuses("Error");
        //public static readonly PrinterMachineStatuses Started = new PrinterMachineStatuses("Started");

        private PrinterMachineStatuses(string name)
            : base(0, name)
        {
        }
    }
}