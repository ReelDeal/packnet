﻿namespace PackNet.Common.Interfaces.Enums
{
    public static class CartonRestrictions
    {
        /// <summary>
        /// Restriction added by workflow for label sync to work.
        /// </summary>
        public static readonly string ShouldWaitForBoxRelease = "ShouldWaitForBoxRelease";
      

    }
}