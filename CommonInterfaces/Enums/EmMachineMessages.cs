﻿namespace PackNet.Common.Interfaces.Enums
{
    public class EmMachineMessages : MachineMessages
    {
        //Messages
        public static readonly EmMachineMessages CreateEmMachine = new EmMachineMessages("CreateEmMachine");
        public static readonly EmMachineMessages UpdateEmMachine = new EmMachineMessages("UpdateEmMachine");
        public static readonly EmMachineMessages DeleteEmMachine = new EmMachineMessages("DeleteEmMachine");
        public static readonly EmMachineMessages GetEmMachineSettingsFileNames = new EmMachineMessages("GetEmMachineSettingsFileNames");
        public static readonly EmMachineMessages EmMachineSettingsFileNames = new EmMachineMessages("EmMachineSettingsFileNames");
        public static readonly EmMachineMessages EnterChangeCorrugateTrack = new EmMachineMessages("EnterChangeCorrugateTrack");
        
        protected EmMachineMessages(string name) : base(name) { }
    }
}