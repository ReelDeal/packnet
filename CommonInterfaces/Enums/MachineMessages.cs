﻿using System;

namespace PackNet.Common.Interfaces.Enums
{
    public class MachineMessages : MessageTypes
    {
        public static readonly MachineMessages GetMachines = new MachineMessages("GetMachines");
        public static readonly MachineMessages Machines = new MachineMessages("Machines");
        public static readonly MachineMessages MachineCreated = new MachineMessages("MachineCreated");
        public static readonly MachineMessages MachineUpdated = new MachineMessages("MachineUpdated");
        public static readonly MachineMessages MachineDeleted = new MachineMessages("MachineDeleted");

        public static readonly MachineMessages MachineProductionCompleted = new MachineMessages("MachineProductionCompleted");

        public static readonly MachineMessages MachineSettingsUpdated = new MachineMessages("MachineSettingsUpdated");

        [Obsolete("Use ChangeMachineProductionStatus and ChangeMachineStatus")]
        public static readonly MachineMessages ChangeMachineState = new MachineMessages("ChangeMachineState");
        public static readonly MachineMessages MachineStatusChanged = new MachineMessages("MachineStatedChanged");

        public static readonly MachineMessages AssignCorrugates = new MachineMessages("AssignCorrugates");
        public static readonly MachineMessages AssignCorrugatesResponse = new MachineMessages("AssignCorrugatesResponse");

        public static readonly MachineMessages PauseMachine = new MachineMessages("PauseMachine");

        public static readonly MachineMessages EnterChangeCorrugate = new MachineMessages("EnterChangeCorrugate");
        public static readonly MachineMessages LeaveChangeCorrugate = new MachineMessages("LeaveChangeCorrugate");

        public static readonly MachineMessages UpdateMachineSettings = new MachineMessages("UpdateMachineSettings");
        public static readonly MachineMessages SynchronizeMachineSettings = new MachineMessages("SynchronizeMachineSettings");
        public static readonly MachineMessages MachineSynchronized = new MachineMessages("MachineSynchronized");
        public static readonly MachineMessages StartupProcedureCompleted = new MachineMessages("StartupProcedureCompleted");
        
        public static readonly MachineMessages MachineErrorOccurred = new MachineMessages("MachineErrorOccurred");
        public static readonly MachineMessages GetCorrugatesLoadedOnMachines = new MachineMessages("GetCorrugatesLoadedOnMachines");
        public static readonly MachineMessages CorrugatesLoadedOnMachines = new MachineMessages("CorrugatesLoadedOnMachines");
        public static readonly MachineMessages CorrugatesOnMachines = new MachineMessages("CorrugatesOnMachines"); //TODO: why do we have duplicate messages
        public static readonly MachineMessages MachineProduction = new MachineMessages("MachineProduction");
        public static readonly MachineMessages GetProductionHistory = new MachineMessages("GetProductionHistory");
        public static readonly MachineMessages MachineProductionHistory = new MachineMessages("MachineProductionHistory");

        public static readonly MachineMessages UnableToPauseMachine = new MachineMessages("UnableToPauseMachine");

        public static readonly MachineMessages PlcWarning = new MachineMessages("PlcWarning");

        protected MachineMessages(string name) : base(name) {}

    }
}