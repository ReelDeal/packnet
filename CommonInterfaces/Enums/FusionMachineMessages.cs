﻿namespace PackNet.Common.Interfaces.Enums
{
    public class FusionMachineMessages : MachineMessages
    {
        public static readonly FusionMachineMessages CreateFusionMachine = new FusionMachineMessages("CreateFusionMachine");
        public static readonly FusionMachineMessages UpdateFusionMachine = new FusionMachineMessages("UpdateFusionMachine");
        public static readonly FusionMachineMessages DeleteFusionMachine = new FusionMachineMessages("DeleteFusionMachine");
        public static readonly FusionMachineMessages GetFusionMachineSettingsFileNames = new FusionMachineMessages("GetFusionMachineSettingsFileNames");
        public static readonly FusionMachineMessages FusionMachineSettingsFileNames = new FusionMachineMessages("FusionMachineSettingsFileNames");

        protected FusionMachineMessages(string name) : base(name) { }
    }
}