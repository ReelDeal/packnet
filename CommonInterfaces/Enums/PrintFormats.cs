namespace PackNet.Common.Interfaces.Enums
{
    public sealed class PrintFormats : Enumeration
    {
        public static readonly PrintFormats BarCode = new PrintFormats("BarCode");
        public static readonly PrintFormats PlainText = new PrintFormats("PlainText");

        private PrintFormats(string name)
            : base(0, name)
        {
        }
    }
}