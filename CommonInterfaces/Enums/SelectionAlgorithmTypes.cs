﻿namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// List of selection algorithms known by the system
    /// </summary>
    public class SelectionAlgorithmTypes : Enumeration
    {
        public static readonly SelectionAlgorithmTypes BoxLast = new SelectionAlgorithmTypes("BoxLast");
        public static readonly SelectionAlgorithmTypes BoxFirst = new SelectionAlgorithmTypes("BoxFirst");
        public static readonly SelectionAlgorithmTypes Order = new SelectionAlgorithmTypes("Order");
        public static readonly SelectionAlgorithmTypes ScanToCreate = new SelectionAlgorithmTypes("ScanToCreate");

        protected SelectionAlgorithmTypes(string name) : base(0, name) { }
    }
}