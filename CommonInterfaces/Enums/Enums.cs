﻿using Newtonsoft.Json;
using PackNet.Common.Interfaces.Converters;
using PackNet.Common.Interfaces.Utils;
using System;
using System.Collections.Generic;
using System.Linq;


namespace PackNet.Common.Interfaces.Enums
{
    [JsonConverter(typeof(EnumerationConverter))]
    public abstract class Enumeration : IComparable
    {
        private readonly int value;
        private readonly string displayName;
        private static ConcurrentList<Enumeration> allValues = new ConcurrentList<Enumeration>();

        private static bool primed = false;

        public static bool Primed
        {
            get { return primed; }
            set { primed = value; }
        }

        protected Enumeration(int value, string displayName)
        {
            //The enums are not initialized until they are accessed ????  Prime them so serialization will work (_allValues needs to be populated)
            if (!primed)
            {
                primed = true;
                var t = typeof(Enumeration);
                var allAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                var derived = allAssemblies.SelectMany(a=> a.GetTypes().Where(tt => tt.IsSubclassOf(t)));
                var fields = derived.SelectMany(d => d.GetFields()).ToArray();
                fields.ForEach(f => f.GetValue(null));
            }
            this.value = value;
            this.displayName = displayName;
            allValues.Add(this);
                
        }

        public int Value
        {
            get { return value; }
        }

        public string DisplayName
        {
            get { return displayName; }
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public static IEnumerable<Enumeration> GetAll()
        {
            return allValues;
        }

        public static IEnumerable<T> GetAllByType<T>()
        {
            return allValues.Where(e=>e is T).Cast<T>();
        }

        public static bool operator ==(Enumeration x, Enumeration y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (((object)x == null) || ((object)y== null))
                return false;


            return x.DisplayName == y.DisplayName;
        }

        public static bool operator !=(Enumeration x, Enumeration y)
        {
            if (ReferenceEquals(x, y))
                return false;

            if (((object)x == null) && ((object)y == null))
                return false;

            if (((object)x == null) && ((object)y != null))
                return true;

            if (((object)x != null) && ((object)y == null))

                return true;

            return x.DisplayName != y.DisplayName;
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration;

            if (otherValue == null)
            {
                return false;
            }
            return otherValue == this;  // since these objects are all static Equals and == should be the same.
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }
            
        public int CompareTo(object other)
        {
            return Value.CompareTo(((Enumeration)other).Value);
        }
    }

    public enum ProductionStatusEnum
    {
        Queued,
        Completed,
    }

    public enum PacksizeMachineErrorCodes
    {
        EmergencyStop = 1,
        RollAxisError = 2,
        ToolAxisError = 3,
        ConnectionError = 4,
        OutOfCorrugate = 5,
        PaperJam = 6,
        CorrugateMismatch = 7,
        LongHeadQuantity = 8,
        LongHeadPosition = 9,
        TrackOffset = 10,
        TrackQuantity = 11,
        InstructionList = 12,
        MachineError = 13,
        ChangeCorrugate = 14,
        AirPressureLow = 15,
        MovablePressureRollerOutOfPosition = 16,
        LightBarrierBroken = 17,
        ReferenceQuantityMissmatch = 18,
        LongHeadPositionOutsideOfTolerance = 19,
        CrossHeadPositionOutsideOfTolerance = 20,
        PositionLatchError = 21,
        AlreadyConnectedError = 22,
        InfeedLightBarrierBroken = 23,
        CorrugateGuideQuantityError = 24
    }

    public enum PacksizeMachineWarningCodes
    {
        CrossheadOutOfPosition = 101,
        LongheadOutOfPosition = 102
    }
}

