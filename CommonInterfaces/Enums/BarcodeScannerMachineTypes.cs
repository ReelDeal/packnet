﻿namespace PackNet.Common.Interfaces.Enums
{
    public class BarcodeScannerMachineTypes : MachineTypes
    {
        public static readonly BarcodeScannerMachineTypes WifiBarcodeScanner = new BarcodeScannerMachineTypes("WifiBarcodeScanner");

        protected BarcodeScannerMachineTypes(string name) : base(name) { }
    }
}