﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class PickZoneMessages : MessageTypes
    {
        public static readonly PickZoneMessages CreatePickZone = new PickZoneMessages("CreatePickZone");
        public static readonly PickZoneMessages UpdatePickZone = new PickZoneMessages("UpdatePickZone");
        public static readonly PickZoneMessages DeletePickZone = new PickZoneMessages("DeletePickZone");
        public static readonly PickZoneMessages GetPickZones = new PickZoneMessages("GetPickZones");
        public static readonly PickZoneMessages PickZones = new PickZoneMessages("PickZones");
        public static readonly PickZoneMessages UpdatePickZoneStatus = new PickZoneMessages("UpdatePickZoneStatus");

        /* Responses */
        public static readonly PickZoneMessages PickZoneCreated = new PickZoneMessages("PickZoneCreated");
        public static readonly PickZoneMessages PickZoneUpdated = new PickZoneMessages("PickZoneUpdated");
        public static readonly PickZoneMessages PickZoneDeleted = new PickZoneMessages("PickZoneDeleted");
        public static readonly PickZoneMessages StatusChanged = new PickZoneMessages("PickZoneStatusChanged");

        private PickZoneMessages(string name)
            : base(name)
        {
        }
    }
}