﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class UserMessages : MessageTypes
    {
        /* Commands */
        public static readonly UserMessages GetUsers = new UserMessages("GetUsers");
        public static readonly UserMessages CreateUser = new UserMessages("CreateUser");
        public static readonly UserMessages DeleteUser = new UserMessages("DeleteUser");
        public static readonly UserMessages UpdateUser = new UserMessages("UpdateUser");
        public static readonly UserMessages LoginUser = new UserMessages("LoginUser");
        public static readonly UserMessages LoginUserByToken = new UserMessages("LoginUserByToken");
        public static readonly UserMessages LogoffUser = new UserMessages("LogoffUser");
        public static readonly UserMessages AutoLogin = new UserMessages("AutoLogin");
        public static readonly UserMessages UpdateUserPreferences = new UserMessages("UpdateUserPreferences");
        public static readonly UserMessages FindUserPreferences = new UserMessages("FindUserPreferences");

        /* Responses */
        public static readonly UserMessages Users = new UserMessages("Users");
        public static readonly UserMessages UserCreated = new UserMessages("UserCreated");
        public static readonly UserMessages UserUpdated = new UserMessages("UserUpdated");
        public static readonly UserMessages UserDeleted = new UserMessages("UserDeleted");
        public static readonly UserMessages UserLoggedIn = new UserMessages("UserLoggedIn");
        public static readonly UserMessages AutoLoggedIn = new UserMessages("AutoLoggedIn");
        public static readonly UserMessages UserPreferencesUpdated = new UserMessages("UserPreferencesUpdated");
        public static readonly UserMessages UserPreferences = new UserMessages("UserPreferences");

        private UserMessages(string name)
            : base(name)
        {
        }
    }
}