namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// Check comments on each individual status in this class
    /// </summary>
    public sealed class PrintRequestStatuses : Enumeration
    {
        public static readonly PrintRequestStatuses Unknown = new PrintRequestStatuses("Unknown");
        /// <summary>
        /// Started will be called when the first printrequest in a batch is started
        /// </summary>
        public static PrintRequestStatuses Started = new PrintRequestStatuses("Started");
        /// <summary>
        /// InProgress will be called n-1 times where n is the number of printrequests in the job.  
        /// ***So, if you only have one print request this will never be used.  Instead you will only get a single 'Complete' status
        /// </summary>
        public static readonly PrintRequestStatuses InProgress = new PrintRequestStatuses("InProgress");
        /// <summary>U
        /// Complete will be called only once and in the case of only one printReqeust in the print job you will not get an 'InProgress' status
        /// </summary>
        public static readonly PrintRequestStatuses Complete = new PrintRequestStatuses("Complete");
        public static readonly PrintRequestStatuses Error = new PrintRequestStatuses("Error");

        private PrintRequestStatuses(string name)
            : base(0, name)
        {
        }
    }
}