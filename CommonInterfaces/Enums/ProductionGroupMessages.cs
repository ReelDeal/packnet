﻿namespace PackNet.Common.Interfaces.Enums
{
    public class ProductionGroupMessages : MessageTypes
    {
        public static readonly ProductionGroupMessages GetProductionGroups = new ProductionGroupMessages("GetProductionGroups");
        public static readonly ProductionGroupMessages CreateProductionGroup = new ProductionGroupMessages("CreateProductionGroup");
        public static readonly ProductionGroupMessages UpdateProductionGroup = new ProductionGroupMessages("UpdateProductionGroup");
        public static readonly ProductionGroupMessages DeleteProductionGroup = new ProductionGroupMessages("DeleteProductionGroup");
        
        /// <summary>
        /// Triggers selection of work for all machine groups included in specified production group
        /// </summary>
        public static readonly ProductionGroupMessages GetWorkForProductionGroup = new ProductionGroupMessages("GetWorkForProductionGroup");

        public static readonly ProductionGroupMessages ProductionGroups = new ProductionGroupMessages("ProductionGroups");
        public static readonly ProductionGroupMessages UpdateProductionGroups = new ProductionGroupMessages("UpdateProductionGroups");
        public static readonly ProductionGroupMessages ProductionGroupUpdated = new ProductionGroupMessages("ProductionGroupUpdated");
        public static readonly ProductionGroupMessages ProductionGroupCreated = new ProductionGroupMessages("ProductionGroupCreated");
        public static readonly ProductionGroupMessages ProductionGroupDeleted = new ProductionGroupMessages("ProductionGroupDeleted");

        //todo: this does nothave any usages, what is it for?
        public static readonly ProductionGroupMessages ProductionGroupConfiguration = new ProductionGroupMessages("ProductionGroupConfiguration");
        //
        public static readonly ProductionGroupMessages ProductionGroupChanged = new ProductionGroupMessages("ProductionGroupChanged");
        
        private ProductionGroupMessages(string name)
            : base(name)
        {
        }
    }
}