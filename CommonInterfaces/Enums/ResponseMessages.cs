﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class ResponseMessages : MessageTypes
    {

        public static readonly ResponseMessages SearchedProducibles = new ResponseMessages("SearchedProducibles");
        public static readonly ResponseMessages ProductionGroupUpdated = new ResponseMessages("ProductionGroupUpdated");

        private ResponseMessages(string name)
            : base(name)
        {
        }

    }
}