﻿namespace PackNet.Common.Interfaces.Enums
{
    public class CorrugateConfigurationMessages : Enumeration
    {
        public static readonly CorrugateConfigurationMessages OverlappingTracks = new CorrugateConfigurationMessages("OverlappingTracks");

        public static readonly CorrugateConfigurationMessages TrackOutsideOfMachine = new CorrugateConfigurationMessages("TrackOutsideOfMachine");

        public static readonly CorrugateConfigurationMessages CorrugateTooWideForMachine = new CorrugateConfigurationMessages("CorrugateTooWideForMachine");

        public static readonly CorrugateConfigurationMessages IdenticalCorrugate = new CorrugateConfigurationMessages("IdenticalCorrugate");

        public static readonly CorrugateConfigurationMessages OverlappingRubberrollerWithCorrugateLoaded = new CorrugateConfigurationMessages("OverlappingRubberrollerWithCorrugateLoaded");

        public CorrugateConfigurationMessages(string displayName) : base(0, displayName)
        {
        }

    }
}
