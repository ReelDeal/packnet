﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class StagingMessages : MessageTypes
    {
        /* Commands */
        public static readonly StagingMessages ProduciblesStaged = new StagingMessages("ProducibleStaged");

        /* Responses */

        private StagingMessages(string name)
            : base(name)
        {
        }
    }
}