﻿namespace PackNet.Common.Interfaces.Enums
{
    public class WorkflowTrackingLevel : Enumeration
    {
        /// <summary>
        /// Include all Workflow TrackingRecords and the arguments passed to them.  Most Verbose
        /// </summary>
        public static readonly WorkflowTrackingLevel TrackEverything = new WorkflowTrackingLevel("TrackEverything");
        /// <summary>
        /// Track all records but do not include the arguments passed to activities.  Recommened
        /// </summary>
        public static readonly WorkflowTrackingLevel TrackWithoutArguments = new WorkflowTrackingLevel("TrackWithoutArguments");
        /// <summary>
        /// Track only workflow instances and not the individual activities running within.
        /// </summary>
        public static readonly WorkflowTrackingLevel TrackInstances = new WorkflowTrackingLevel("TrackInstances");
                                                                              
        protected WorkflowTrackingLevel(string name)
            : base(0, name)
        {
        }

        protected WorkflowTrackingLevel(int value, string name)
            : base(value, name)
        {
        }
    }
}