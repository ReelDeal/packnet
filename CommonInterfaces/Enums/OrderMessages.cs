﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class OrderMessages : MessageTypes
    {
        public static readonly OrderMessages CancelOrders = new OrderMessages("CancelOrders");
        public static readonly OrderMessages DistributeOrders = new OrderMessages("DistributeOrders");
        public static readonly OrderMessages UpdateOrdersPriority = new OrderMessages("UpdateOrdersPriority");
        public static readonly OrderMessages GetOrders = new OrderMessages("GetOrders");
        public static readonly OrderMessages SearchOrdersResponse = new OrderMessages("SearchOrdersResponse");
        public static readonly OrderMessages Orders = new OrderMessages("Orders");
        public static readonly OrderMessages OrderChanged = new OrderMessages("OrderChanged");
        public static readonly OrderMessages GetOrdersHistory = new OrderMessages("GetOrdersHistory");
        public static readonly OrderMessages OrdersHistory = new OrderMessages("OrdersHistory");
        public static readonly OrderMessages ReproduceCartonForOrder = new OrderMessages("ReproduceCartonForOrder");
        public static readonly OrderMessages ReproduceOrders = new OrderMessages("ReproduceOrders");
        


        private OrderMessages(string name) : base(name) {}
    }
}