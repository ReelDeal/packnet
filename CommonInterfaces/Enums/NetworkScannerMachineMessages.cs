﻿namespace PackNet.Common.Interfaces.Enums
{
    public class NetworkScannerMachineMessages : MachineMessages
    {
        public static readonly NetworkScannerMachineMessages CreateNetworkScanner = new NetworkScannerMachineMessages("CreateNetworkScanner");
        public static readonly NetworkScannerMachineMessages UpdateNetworkScanner = new NetworkScannerMachineMessages("UpdateNetworkScanner");
        public static readonly NetworkScannerMachineMessages DeleteNetworkScanner = new NetworkScannerMachineMessages("DeleteNetworkScanner");
        public static readonly NetworkScannerMachineMessages GetScanDataWorkflows = new NetworkScannerMachineMessages("GetScanDataWorkflows");
        public static readonly NetworkScannerMachineMessages ScanDataWorkflows = new NetworkScannerMachineMessages("ScanDataWorkflows");

        protected NetworkScannerMachineMessages(string name) : base(name) { }
    }
}