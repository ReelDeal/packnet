﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class CorrugateMessages : MessageTypes
    {
        /* Commands */
        public static readonly CorrugateMessages CreateCorrugate = new CorrugateMessages("CreateCorrugate");        
        public static readonly CorrugateMessages UpdateCorrugate = new CorrugateMessages("UpdateCorrugate");
        public static readonly CorrugateMessages DeleteCorrugates = new CorrugateMessages("DeleteCorrugate");
        public static readonly CorrugateMessages GetCorrugates = new CorrugateMessages("GetCorrugates");

        /* Responses */
        public static readonly CorrugateMessages CorrugateCreated = new CorrugateMessages("CorrugateCreated");
        public static readonly CorrugateMessages CorrugateUpdated = new CorrugateMessages("CorrugateUpdated");
        public static readonly CorrugateMessages CorrugateDeleted = new CorrugateMessages("CorrugateDeleted");
        public static readonly CorrugateMessages Corrugates = new CorrugateMessages("Corrugates");


        public static readonly CorrugateMessages CartonCanBeProducedOnCorrugate = new CorrugateMessages("CartonCanBeProducedOnCorrugate");
        public static readonly CorrugateMessages GetCartonsThatCanBeProducedOnCorrugates = new CorrugateMessages("GetCartonsThatCanBeProducedOnCorrugates");
        public static readonly CorrugateMessages CartonsThatCanBeProducedOnCorrugates = new CorrugateMessages("CartonsThatCanBeProducedOnCorrugates");
        public static readonly CorrugateMessages ClearCartonCanBeProducedOnCorrugate = new CorrugateMessages("ClearCartonCanBeProducedOnCorrugate");

        private CorrugateMessages(string name)
            : base(name)
        {
        }
    }
}