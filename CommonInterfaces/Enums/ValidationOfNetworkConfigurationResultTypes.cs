﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class ValidationOfNetworkConfigurationResultTypes : ResultTypes
    {
        public static readonly ValidationOfNetworkConfigurationResultTypes IpAddressAlreadyInUse = new ValidationOfNetworkConfigurationResultTypes("IpAddressAlreadyInUse");
        public static readonly ValidationOfNetworkConfigurationResultTypes InvalidNetworkIpOrDnsValue = new ValidationOfNetworkConfigurationResultTypes("InvalidNetworkIpOrDnsValue");
        public static readonly ValidationOfNetworkConfigurationResultTypes PortOutOfRange_1to65535 = new ValidationOfNetworkConfigurationResultTypes("PortOutOfRange");

        private ValidationOfNetworkConfigurationResultTypes(string name) : base(name) { }
    }
}