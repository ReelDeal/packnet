﻿namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// Represents production modes that a machine group can be in
    /// </summary>
    public class MachineGroupProductionModes : Enumeration
    {
        /// <summary>
        /// The machine group is configured to create items from manual user input (operator in front of machine adding items by hand)
        /// </summary>
        public static readonly MachineGroupProductionModes ManualProductionMode = new MachineGroupProductionModes("ManualProductionMode");

        /// <summary>
        /// The machine group is configured to create items from the configured production group and selection algorithms
        /// </summary>
        public static readonly MachineGroupProductionModes AutoProductionMode = new MachineGroupProductionModes("AutoProductionMode");

        protected MachineGroupProductionModes(string name) : base(0, name) { }
    }
}