﻿namespace PackNet.Common.Interfaces.Enums
{
    public class MessageTypes : Enumeration
    {
        /// <summary>
        /// Request for .Server version information
        /// </summary>
        public static readonly MessageTypes GetVersionInfo = new MessageTypes("GetVersionInfo");

        /// <summary>
        /// Response for version information, Payload should be a Assembly Version (ex: "3.0.0.3456")
        /// </summary>
        public static readonly MessageTypes VersionInfo = new MessageTypes("VersionInfo");

        //TODO: remove this or rename it and put it in the right place
        public static readonly MessageTypes ProducibleStatusChanged = new MessageTypes("ProducibleStatusChanged");


        public static readonly MessageTypes AdminNotification = new MessageTypes("AdminNotification");
        public static readonly MessageTypes OperatorNotification = new MessageTypes("OperatorNotification");
        public static readonly MessageTypes ConnectionAlive = new MessageTypes("ConnectionAlive");
        public static readonly MessageTypes GetProducibles = new MessageTypes("GetProducibles");
        public static readonly MessageTypes ImportFailed = new MessageTypes("ImportFailed");
        

        /// <summary>
        /// Message is sent when the importer workflow has parsed the data
        /// </summary>
        public static readonly MessageTypes DataImported = new MessageTypes("DataImported");
        public static readonly MessageTypes Restaged = new MessageTypes("Restaged");

        /// <summary>
        /// Message is sent when the algorithm service overwrites or skips a producible based on the customer unique id. 
        /// Created to adjust the Producible Items Imported metric (band-aid in other words)
        /// </summary>
        public static readonly MessageTypes DataOverwrittenOrSkipped = new MessageTypes("DataOverwrittenOrSkipped");

        /// <summary>
        /// Message is sent when the csv file date is read
        /// </summary>
        public static readonly MessageTypes RawDataImported = new MessageTypes("RawDataImported");

        public static readonly MessageTypes ScanTriggerError = new MessageTypes("ScanTriggerError");
        public static readonly MessageTypes ScanTriggerSuccess = new MessageTypes("ScanTriggerSuccess");

        /// <summary>
        /// Message is sent when a day changes. Used for reseting metrics.
        /// </summary>
        public static readonly MessageTypes DateChanged = new MessageTypes("DateChanged");
        
        protected MessageTypes(string name)
            : base(0, name)
        {
        }

        protected MessageTypes(int value, string name) : base(value, name)
        {
        }
    }
}