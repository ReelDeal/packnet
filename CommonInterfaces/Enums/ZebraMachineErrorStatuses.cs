﻿using PackNet.Common.Interfaces.Enums.ProducibleStates;

namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// Error states for machines
    /// </summary>
    public class ZebraMachineErrorStatuses : MachineErrorStatuses
    {
        public static readonly ZebraMachineErrorStatuses ZebraErrorUnableToPause = new ZebraMachineErrorStatuses("ZebraErrorUnableToPause");
        public static readonly ZebraMachineErrorStatuses OutOfLabels = new ZebraMachineErrorStatuses("OutOfLabels");
        public static readonly ZebraMachineErrorStatuses OutOfRibbon = new ZebraMachineErrorStatuses("OutOfRibbon");
        public static readonly ZebraMachineErrorStatuses HeadOpen = new ZebraMachineErrorStatuses("HeadOpen");
        public static readonly ZebraMachineErrorStatuses UnexpectedLabelOnPrinter = new ZebraMachineErrorStatuses("UnexpectedLabelOnPrinter");
        public static readonly ZebraMachineErrorStatuses UnexpectedLabelInPrinterQueue = new ZebraMachineErrorStatuses("UnexpectedLabelInPrinterQueue");

        protected ZebraMachineErrorStatuses(string name) : base(name) { }
    }

    public class ZebraInProductionProductionStatus : MachineProductionInProgressStatuses
    {
        public static readonly ZebraInProductionProductionStatus WaitingForPeelOff = new ZebraInProductionProductionStatus("WaitingForPeelOff");
        public static readonly ZebraInProductionProductionStatus LabelInQueue = new ZebraInProductionProductionStatus("LabelInQueue");

        protected ZebraInProductionProductionStatus(string name) : base(name) { }
    }

    public class ZebraErrorProducibleStatus : ErrorProducibleStatuses
    {
        public static readonly ZebraErrorProducibleStatus InvalidPrintData = new ZebraErrorProducibleStatus("InvalidPrintData");
        public static readonly ZebraErrorProducibleStatus FailedToStart = new ZebraErrorProducibleStatus("FailedToStart");
        public static readonly ZebraErrorProducibleStatus FillPrinterQueueNotConfirmed = new ZebraErrorProducibleStatus("FillPrinterQueueNotConfirmed");
        public static readonly ZebraErrorProducibleStatus LabelRequestedButNotConfirmedInPrinterQueue = new ZebraErrorProducibleStatus("LabelRequestedButNotConfirmedInPrinterQueue");

        protected ZebraErrorProducibleStatus(string name) : base(name) { }
        
    }
}