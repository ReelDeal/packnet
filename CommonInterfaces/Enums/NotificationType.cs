﻿using System;
using System.Collections.Generic;

namespace PackNet.Common.Interfaces.Enums
{
	public sealed class NotificationType : Enumeration
	{
		private static readonly Dictionary<string, NotificationType> instance = new Dictionary<string, NotificationType>();

		public static readonly NotificationType MachineGroup = new NotificationType("MG");
		public static readonly NotificationType ProductionGroup = new NotificationType("PG");
		public static readonly NotificationType System = new NotificationType("System");

		private NotificationType(string name)
			: base(0, name)
		{
			instance[name] = this;
		}

		/* This method is to support explicit conversion */
		public static explicit operator NotificationType(string value)
		{
			NotificationType result;

			if (!instance.TryGetValue(value, out result))
				throw new InvalidCastException();

			return result;
		}
	}
}