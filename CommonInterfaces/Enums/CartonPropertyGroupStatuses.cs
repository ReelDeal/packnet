﻿namespace PackNet.Common.Interfaces.Enums
{
    public class CartonPropertyGroupStatuses : Enumeration
    {
        public static readonly CartonPropertyGroupStatuses Normal = new CartonPropertyGroupStatuses("Normal");
        public static readonly CartonPropertyGroupStatuses Surge = new CartonPropertyGroupStatuses("Surge");
        public static readonly CartonPropertyGroupStatuses Stop = new CartonPropertyGroupStatuses("Stop");
        
        protected CartonPropertyGroupStatuses(string name) : base(0, name) { }
    }
}