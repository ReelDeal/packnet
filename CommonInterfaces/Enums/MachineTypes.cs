﻿using System;

namespace PackNet.Common.Interfaces.Enums
{
    public class MachineTypes : Enumeration, IEquatable<MachineTypes>
    {
        
        public static readonly MachineTypes Fusion = new MachineTypes("Fusion");
        public static readonly MachineTypes Em = new MachineTypes("Em");
        public static readonly MachineTypes ExternalCustomerSystem = new MachineTypes("ExternalCustomerSystem");

        protected MachineTypes(string name) : base(0, name) { }

        public static MachineTypes FromString(string machineType)
        {
            if (machineType.Equals(Fusion.DisplayName, StringComparison.CurrentCultureIgnoreCase))
                return Fusion;

            if (machineType.Equals(Em.DisplayName, StringComparison.CurrentCultureIgnoreCase))
                return Em;

            throw new InvalidCastException(string.Format("Unable to convert string: {0} to a machine type", machineType));
        }

        public bool Equals(MachineTypes other)
        {
            return DisplayName.Equals(other.DisplayName, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}