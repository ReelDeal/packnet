﻿namespace PackNet.Common.Interfaces.Enums
{
    //todo: remove this class and all it's usages.  Instead use ProducibleStatuses and other enums
    public sealed class CartonMessages : MessageTypes
    {
        public static readonly CartonMessages GetFailedProducibles = new CartonMessages("GetFailedProducibles");

        public static readonly CartonMessages CreateCartonTrigger = new CartonMessages("CreateCartonTrigger");
        public static readonly CartonMessages CreateCustomCarton = new CartonMessages("CreateCustomCarton");
        public static readonly CartonMessages CustomCartonCreated = new CartonMessages("CustomCartonCreated");
        public static readonly CartonMessages RemoveBoxFirstCartons = new CartonMessages("RemoveBoxFirstCartons");

        public static readonly CartonMessages RemoveScanToCreateCartons = new CartonMessages("RemoveScanToCreateCartons");

        public static readonly CartonMessages ReproduceCarton = new CartonMessages("ReproduceCarton");
        //TODO: The use of ReproduceCartons (plural) vs ReproduceCarton (single) should be re-evaluated for consistency across
        //TODO: the system as it led to TFS 13115 - 3.0 Produced items imported by day recording negative value when reproducing a completed job
        //TODO: Either pass a collection and loop internally in callee or make a loop in caller and pass single items - Take a stance and don't do both.
        public static readonly CartonMessages ReproduceCartons = new CartonMessages("ReproduceCartons");
        public static readonly CartonMessages NotProducible = new CartonMessages("NotProducible");
        public static readonly CartonMessages ProducibleChanged = new CartonMessages("ProducibleChanged");
        public static readonly CartonMessages CartonRemovalBetweenDate = new CartonMessages("CartonRemovalBetweenDate");
        public static readonly CartonMessages ReleaseBox = new CartonMessages("ReleaseBox");

        public static readonly CartonMessages CartonAdded = new CartonMessages("CartonAdded");
        public static readonly CartonMessages CartonCanNotBeProduced = new CartonMessages("CartonCanNotBeProduced");
        public static readonly CartonMessages CartonCompleted = new CartonMessages("CartonCompleted");
        public static readonly CartonMessages CartonRemoved = new CartonMessages("CartonRemoved");
        public static readonly CartonMessages CartonRemovedCount = new CartonMessages("CartonRemovedCount");
        public static readonly CartonMessages SentToMachine = new CartonMessages("SentToMachine");
        public static readonly CartonMessages NoCorrugateAvailable = new CartonMessages("NoCorrugateAvailable");

        public static readonly CartonMessages SearchBoxFirstResponse = new CartonMessages("SearchBoxFirstResponse");
        public static readonly CartonMessages SearchBoxLastResponse = new CartonMessages("SearchBoxLastResponse");
        public static readonly CartonMessages SearchScanToCreateResponse = new CartonMessages("SearchScanToCreateResponse");

        private CartonMessages(string name) : base(name) { }


    }

}