﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class PrintJobStatuses : Enumeration
    {
        public static readonly PrintJobStatuses Queued = new PrintJobStatuses("Queued");
        public static readonly PrintJobStatuses Started = new PrintJobStatuses("Started");
        public static readonly PrintJobStatuses Success = new PrintJobStatuses("Success");
        public static readonly PrintJobStatuses Error = new PrintJobStatuses("Error");
        public static readonly PrintJobStatuses Unknown = new PrintJobStatuses("Unknown");

        private PrintJobStatuses(string name)
            : base(0, name)
        {
        }
    }
}