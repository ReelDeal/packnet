﻿namespace PackNet.Common.Interfaces.Enums
{
    public class PhysicalMachineSettingsMessages : MessageTypes
    {
        public static readonly PhysicalMachineSettingsMessages GetEmPhysicalMachineSettings = new PhysicalMachineSettingsMessages("GetEmPhysicalMachineSettings");
        public static readonly PhysicalMachineSettingsMessages GetFusionPhysicalMachineSettings = new PhysicalMachineSettingsMessages("GetFusionPhysicalMachineSettings");
        public static readonly PhysicalMachineSettingsMessages PhysicalMachineSettings = new PhysicalMachineSettingsMessages("PhysicalMachineSettings");

        public static readonly PhysicalMachineSettingsMessages PhysicalMachineSettingsUpdated = new PhysicalMachineSettingsMessages("PhysicalMachineSettingsUpdated");
        public static readonly PhysicalMachineSettingsMessages UpdateEmPhysicalMachineSettings = new PhysicalMachineSettingsMessages("UpdateEmPhysicalMachineSettings");
        public static readonly PhysicalMachineSettingsMessages UpdateFusionPhysicalMachineSettings = new PhysicalMachineSettingsMessages("UpdateFusionPhysicalMachineSettings");
        
        private PhysicalMachineSettingsMessages(string name)
            : base(name)
        {
        }
    }
}