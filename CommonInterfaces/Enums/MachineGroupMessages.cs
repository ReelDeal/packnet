﻿using System;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Eventing;

namespace PackNet.Common.Interfaces.Enums
{
    public sealed class MachineGroupMessages : MessageTypes
    {
        /* Commands */
        public static readonly MachineGroupMessages GetMachineGroups = new MachineGroupMessages("GetMachineGroups");
        public static readonly MachineGroupMessages GetMachineGroupWorkflowPaths = new MachineGroupMessages("GetMachineGroupWorkflowPaths");
        public static readonly MachineGroupMessages CreateMachineGroup = new MachineGroupMessages("CreateMachineGroup");
        public static readonly MachineGroupMessages DeleteMachineGroup = new MachineGroupMessages("DeleteMachineGroup");
        public static readonly MachineGroupMessages UpdateMachineGroup = new MachineGroupMessages("UpdateMachineGroup");
        public static readonly MachineGroupMessages ChangeMachineGroupProductionMode = new MachineGroupMessages("ChangeMachineGroupProductionMode");
        public static readonly MachineGroupMessages ChangeMachineGroupStatus = new MachineGroupMessages("ChangeMachineGroupStatus");
        public static readonly MachineGroupMessages ClearAssignedOperator = new MachineGroupMessages("ClearAssignedOperator");
        public static readonly MachineGroupMessages SetAssignedOperator = new MachineGroupMessages("SetAssignedOperator");
        public static readonly MachineGroupMessages CompleteQueueAndPauseMachineGroup = new MachineGroupMessages("CompleteQueueAndPauseMachineGroup");
        public static readonly MachineGroupMessages GetMachineGroupProductionQueue = new MachineGroupMessages("GetMachineGroupProductionQueue");
        public static readonly MachineGroupMessages MachineGroupInProduction = new MachineGroupMessages("MachineGroupInProduction");
        /// <summary>
        /// Triggers selection of work for the specified machine group
        /// </summary>
        public static readonly MachineGroupMessages GetWorkForMachineGroup = new MachineGroupMessages("GetWorkForMachineGroup");

        /// <summary>
        /// When current capabilities changed manual orders must be recalculated since it can be triggered by a corrugate change
        /// </summary>
        public static readonly MachineGroupMessages CapabilitiesChanged = new MachineGroupMessages("CapabilitiesChanged");
        
        /* Responses */
        public static readonly MachineGroupMessages MachineGroups = new MachineGroupMessages("MachineGroups");
        public static readonly MachineGroupMessages MachineGroupWorkflowPaths = new MachineGroupMessages("MachineGroupWorkflowPaths");
        
        public static readonly MachineGroupMessages MachineGroupCreated = new MachineGroupMessages("MachineGroupCreated");
        public static readonly MachineGroupMessages MachineGroupCreateFailedAliasExists = new MachineGroupMessages("MachineGroupCreatedFailedAliasExists");

        public static readonly MachineGroupMessages MachineGroupUpdated = new MachineGroupMessages("MachineGroupUpdated");
        public static readonly MachineGroupMessages MachineGroupUpdateFailedAliasExists = new MachineGroupMessages("MachineGroupUpdateFailedAliasExists");

        public static readonly MachineGroupMessages MachineGroupDeleted = new MachineGroupMessages("MachineGroupDeleted");

        private MachineGroupMessages(string name) : base(name) { }

    }

    public static class MachineGroupMessagesExtensions
    {
        public static void PublishGetWorkFor(this MachineGroupMessages messageType, IEventAggregatorPublisher publisher, Guid machineGroupId)
        {
            publisher.Publish(new Message
            {
                MessageType = messageType,
                MachineGroupId = machineGroupId
            });
        }
    }
}