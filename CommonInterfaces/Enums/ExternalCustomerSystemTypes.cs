﻿namespace PackNet.Common.Interfaces.Enums
{
    public class ExternalCustomerSystemTypes : Enumeration
    {
        public static readonly ExternalCustomerSystemTypes StaplesWmsIntegration = new ExternalCustomerSystemTypes("StaplesWmsIntegration");

        protected ExternalCustomerSystemTypes(string name) : base(0, name) { }
    }


}