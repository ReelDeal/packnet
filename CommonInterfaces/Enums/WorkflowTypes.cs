﻿namespace PackNet.Common.Interfaces.Enums
{
    public class WorkflowTypes : Enumeration
    {
        public static readonly WorkflowTypes ImportWorkflow = new WorkflowTypes("ImportWorkflow");
        public static readonly WorkflowTypes StagingWorkflow = new WorkflowTypes("StagingWorkflow");
        public static readonly WorkflowTypes SelectionWorkflow = new WorkflowTypes("SelectionWorkflow");
        public static readonly WorkflowTypes MachineGroupWorkflow = new WorkflowTypes("MachineGroupWorkflow");
        public static readonly WorkflowTypes NestedWorkflow = new WorkflowTypes("NestedWorkflow");

        protected WorkflowTypes(string name)
            : base(0, name)
        {
        }

        protected WorkflowTypes(int value, string name)
            : base(value, name)
        {
        }
    }
}