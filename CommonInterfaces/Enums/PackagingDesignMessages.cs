﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class PackagingDesignMessages : MessageTypes
    {
        public static readonly PackagingDesignMessages GetPackagingDesigns = new PackagingDesignMessages("GetPackagingDesigns");
        public static readonly PackagingDesignMessages PackagingDesigns = new PackagingDesignMessages("PackagingDesigns");
        public static readonly PackagingDesignMessages GetPackagingDesignWithAppliedValues = new PackagingDesignMessages("GetPackagingDesignWithAppliedValues");
        public static readonly PackagingDesignMessages PackagingDesignWithAppliedValues = new PackagingDesignMessages("PackagingDesignWithAppliedValues");

        private PackagingDesignMessages(string name) : base(name) { }
    }
}