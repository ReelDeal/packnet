﻿using System;

namespace PackNet.Common.Interfaces.Enums
{
    public class ScheduledJobTypes : Enumeration
    {
        
        public static readonly ScheduledJobTypes WorkflowScheduledJob = new ScheduledJobTypes("Fusion");

        protected ScheduledJobTypes(string name) : base(0, name) { }
    }
}