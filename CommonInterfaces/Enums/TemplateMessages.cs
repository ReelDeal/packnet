﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class TemplateMessages : MessageTypes
    {
        /* Commands */
        public static readonly TemplateMessages GetTemplates = new TemplateMessages("GetTemplates");

        /* Responses */
        public static readonly TemplateMessages Templates = new TemplateMessages("Templates");

        private TemplateMessages(string name)
            : base(name)
        {
        }
    }
}