﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class ArticleMessages : MessageTypes
    {
        public static readonly ArticleMessages GetArticle = new ArticleMessages("GetArticle");
        public static readonly ArticleMessages SearchArticles = new ArticleMessages("SearchArticles");
        public static readonly ArticleMessages Articles = new ArticleMessages("Articles");
        public static readonly ArticleMessages CreateArticle = new ArticleMessages("CreateArticle");
        public static readonly ArticleMessages DeleteArticles = new ArticleMessages("DeleteArticles");
        public static readonly ArticleMessages UpdateArticle = new ArticleMessages("UpdateArticle");
        public static readonly ArticleMessages ProduceArticles = new ArticleMessages("ProduceArticles");
        public static readonly ArticleMessages CartonRequests = new ArticleMessages("CartonRequests");
        public static readonly ArticleMessages ArticleProductionRequest = new ArticleMessages("ArticleProductionRequest");

        public static readonly ArticleMessages ArticleCreated = new ArticleMessages("ArticleCreated");
        public static readonly ArticleMessages ArticleUpdated = new ArticleMessages("ArticleUpdated");
        public static readonly ArticleMessages SearchArticle = new ArticleMessages("SearchArticle");
        public static readonly ArticleMessages ArticleDeleted = new ArticleMessages("ArticleDeleted");

        public static readonly ArticleMessages ImportArticles = new ArticleMessages("ImportArticles");
        public static readonly ArticleMessages ArticleImportStarted = new ArticleMessages("ArticleImportStarted");

        public static readonly ArticleMessages ExportArticles = new ArticleMessages("ExportArticles");
        public static readonly ArticleMessages ArticlesExported = new ArticleMessages("ArticlesExported");

        private ArticleMessages(string name)
            : base(name)
        {
        }
    }
}