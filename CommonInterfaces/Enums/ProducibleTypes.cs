﻿using System;

namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// List of items that can be produced in the system
    /// </summary>
    public class ProducibleTypes : Enumeration
    {
        public static readonly ProducibleTypes BoxFirstProducible = new ProducibleTypes("BoxFirstProducible");
        public static readonly ProducibleTypes BoxLastProducible = new ProducibleTypes("BoxLastProducible");
        public static readonly ProducibleTypes ScanToCreateProducible = new ProducibleTypes("ScanToCreateProducible");

        public static readonly ProducibleTypes Carton = new ProducibleTypes("Carton");
        public static readonly ProducibleTypes Order = new ProducibleTypes("Order");
        public static readonly ProducibleTypes Printable = new ProducibleTypes("Printable");
        public static readonly ProducibleTypes Label = new ProducibleTypes("Label");
        public static readonly ProducibleTypes SLD = new ProducibleTypes("SLD");

        public static readonly ProducibleTypes Kit = new ProducibleTypes("Kit");

        public static readonly ProducibleTypes TiledCarton = new ProducibleTypes("TiledCarton");

        protected ProducibleTypes(string name)
            : base(0, name)
        {
        }

    }
}