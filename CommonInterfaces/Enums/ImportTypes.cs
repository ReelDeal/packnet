﻿namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// List of importers known by the system
    /// </summary>
    public class ImportTypes : Enumeration
    {
        public static readonly ImportTypes BoxLast = new ImportTypes("BoxLast");
        public static readonly ImportTypes BoxFirst = new ImportTypes("BoxFirst");
        public static readonly ImportTypes Order = new ImportTypes("Order");
        public static readonly ImportTypes Trigger = new ImportTypes("Trigger");
        public static readonly ImportTypes ScanToCreate = new ImportTypes("ScanToCreate");
        public static readonly ImportTypes Label = new ImportTypes("Label");
        public static readonly ImportTypes SLD = new ImportTypes("SLD");

        protected ImportTypes(string name)
            : base(0, name)
        {
        }

        
    }
}