﻿namespace PackNet.Common.Interfaces.Enums
{
    public class SupportMessages : MessageTypes
    {
        protected SupportMessages(string name) : base(name)
        {
        }

         public static readonly SupportMessages CreateSupportIncident = new SupportMessages("CreateSupportIncident");
         public static readonly SupportMessages SupportIncidentUpdateLogEntry = new SupportMessages("SupportIncidentUpdateLogEntry");
         public static readonly SupportMessages SupportIncidentUpdateUserMessage = new SupportMessages("SupportIncidentUpdateUserMessage");
         public static readonly SupportMessages GetAllUnsentSupportIncidents = new SupportMessages("GetAllUnsentSupportIncidents");
         public static readonly SupportMessages AllUnsentSupportIncidents = new SupportMessages("AllUnsentSupportIncidents");
         public static readonly SupportMessages SupportIncidentCreated = new SupportMessages("SupportIncidentCreated");
         public static readonly SupportMessages SupportIncidentFinished = new SupportMessages("SupportIncidentFinished");

        
    }
}