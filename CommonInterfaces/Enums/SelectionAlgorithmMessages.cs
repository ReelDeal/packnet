﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class SelectionAlgorithmMessages : MessageTypes
    {
        public static readonly SelectionAlgorithmMessages StagingComplete = new SelectionAlgorithmMessages("StagingComplete");

        public static readonly SelectionAlgorithmMessages SelectionEnvironmentChanged  = new SelectionAlgorithmMessages("SelectionEnvironmentChanged");

        public static readonly SelectionAlgorithmMessages StagingEnvironmentChanged = new SelectionAlgorithmMessages("StagingEnvionmentChanged");

        public static readonly SelectionAlgorithmMessages SearchForProducibles = new SelectionAlgorithmMessages("SearchForProducibles");

        public static readonly SelectionAlgorithmMessages SearchForProduciblesWithRestrictions = new SelectionAlgorithmMessages("SearchForProduciblesWithRestrictions");

        private SelectionAlgorithmMessages(string name)
            : base(name)
        {
        }
    }
}
