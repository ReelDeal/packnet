﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class CorrugateResultTypes : ResultTypes
    {
        public static readonly CorrugateResultTypes CorrugateCurrentlyLoadedOnMachine = new CorrugateResultTypes("CorrugateCurrentlyLoadedOnMachine");

        private CorrugateResultTypes(string name) : base(name) { }
    }
}