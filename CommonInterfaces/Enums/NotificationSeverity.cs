using System;
using System.Collections.Generic;

namespace PackNet.Common.Interfaces.Enums
{
	public sealed class NotificationSeverity : Enumeration
	{
		private static readonly Dictionary<string, NotificationSeverity> instance = new Dictionary<string, NotificationSeverity>();
		
		/* Each string value is used to obtain a CSS style. Do not modify. */
		public static readonly NotificationSeverity Info = new NotificationSeverity("Info");
		public static readonly NotificationSeverity Warning = new NotificationSeverity("Warning");
		public static readonly NotificationSeverity Success = new NotificationSeverity("Success");
		public static readonly NotificationSeverity Error = new NotificationSeverity("Error");

		private NotificationSeverity(string name)
			: base(0, name)
		{
			instance[name] = this;
		}

		/* This method is to support explicit conversion */
		public static explicit operator NotificationSeverity(string value)
		{
			NotificationSeverity result;

			if (!instance.TryGetValue(value, out result))
				throw new InvalidCastException();

			return result;
		}
	}
}