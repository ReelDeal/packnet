﻿using System;

namespace PackNet.Common.Interfaces.Enums
{
    public class SchedulerMessages : MessageTypes
    {
        public static readonly SchedulerMessages GetScheduledJobs = new SchedulerMessages("GetMachines");


        protected SchedulerMessages(string name) : base(name) { }

    }
}