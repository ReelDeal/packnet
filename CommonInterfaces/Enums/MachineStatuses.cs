﻿namespace PackNet.Common.Interfaces.Enums
{
    public class MachineStatuses : Enumeration
    {
        /// <summary>
        /// Machine is currently off-line and has not communicated with PackNet.Server 
        /// </summary>
        public static readonly MachineStatuses MachineOffline = new MachineStatuses("MachineOffline");

        /// <summary>
        /// Machine is currently on-line and working as expected
        /// </summary>
        public static readonly MachineStatuses MachineOnline = new MachineStatuses("MachineOnline");

        /// <summary>
        /// Machine has contacted PackNet.Server and is currently undergoing it's startup initialization
        /// </summary>
        public static readonly MachineStatuses MachineInitializing = new MachineStatuses("MachineInitializing");

        protected MachineStatuses(string name) : base(0, name) { }
    }
}