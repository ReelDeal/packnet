namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// Describes states that a machine group can be in
    /// </summary>
    public class MachineGroupStatuses : Enumeration
    {
        /// <summary>
        /// The machine group has a configuration error (No machines configured, etc)
        /// </summary>
        public static readonly MachineGroupStatuses MachineGroupNotConfigured = new MachineGroupStatuses("MachineGroupNotConfigured");

        protected MachineGroupStatuses(string name) : base(0, name) { }
    }

    /// <summary>
    /// Describes states that a machine group can be in
    /// </summary>
    public class MachineGroupUnavailableStatuses : MachineGroupStatuses
    {
        /// <summary>
        /// One of the machines in the machine group is off-line, therefore the machine group is off-line
        /// </summary>
        public static readonly MachineGroupUnavailableStatuses MachineGroupOffline = new MachineGroupUnavailableStatuses("MachineGroupOffline");

        /// <summary>
        /// One of the machines in the machine group is initializing, therefore the machine group is initializing
        /// </summary>
        public static readonly MachineGroupUnavailableStatuses MachineGroupInitializing = new MachineGroupUnavailableStatuses("MachineGroupInitializing");

        /// <summary>
        /// One of the machines in the machine group is in an error state, therefore the machine group is in error
        /// </summary>
        public static readonly MachineGroupUnavailableStatuses MachineGroupError = new MachineGroupUnavailableStatuses("MachineGroupError");

        /// <summary>
        /// One of the machines in the machine group is in the changing corrugate state, therefore the machine group is in error
        /// </summary>
        public static readonly MachineGroupUnavailableStatuses MachineGroupChangingCorrugate = new MachineGroupUnavailableStatuses("MachineGroupChangingCorrugate");


        protected MachineGroupUnavailableStatuses(string name) : base( name) { }
    }

    public class MachineGroupWarningStatuses : MachineGroupStatuses
    {
        /// <summary>
        /// The machine group has currently a machine with its light barrier broken
        /// </summary>
        public static readonly MachineGroupWarningStatuses MachineGroupLightBarrierBroken = new MachineGroupWarningStatuses("MachineGroupLightBarrierBroken");

        protected MachineGroupWarningStatuses(string name) : base(name) { }
    }

    /// <summary>
    /// Describes states that a machine group can be in
    /// </summary>
    public class MachineGroupAvailableStatuses : MachineGroupStatuses
    {
        /// <summary>
        /// The machine group and all machines are on-line and functioning correctly
        /// </summary>
        public static readonly MachineGroupAvailableStatuses MachineGroupOnline = new MachineGroupAvailableStatuses("MachineGroupOnline");

        /// <summary>
        /// The machine group is currently in a paused state
        /// </summary>
        public static readonly MachineGroupAvailableStatuses MachineGroupPaused = new MachineGroupAvailableStatuses("MachineGroupPaused");

        /// <summary>
        /// The machine group is currently clearing the queue and going to pause
        /// </summary>
        public static readonly MachineGroupAvailableStatuses ProduceQueueAndPause = new MachineGroupAvailableStatuses("ProduceQueueAndPause");
        public static readonly MachineGroupAvailableStatuses ProduceCurrentAndPause = new MachineGroupAvailableStatuses("ProduceCurrentAndPause");

        protected MachineGroupAvailableStatuses(string name) : base(name) { }
    }
}