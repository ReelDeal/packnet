﻿namespace PackNet.Common.Interfaces.Enums
{
    public class PrintMachineTypes : MachineTypes
    {
        public static readonly PrintMachineTypes WindowsPrinter = new PrintMachineTypes("WindowsPrinter");
        public static readonly PrintMachineTypes ZebraPrinter = new PrintMachineTypes("ZebraPrinter");

        protected PrintMachineTypes(string name) : base(name) { }
    }
}