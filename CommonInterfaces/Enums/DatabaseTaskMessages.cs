﻿namespace PackNet.Common.Interfaces.Enums
{
    public class DatabaseTaskMessages : MessageTypes
    {
        public static DatabaseTaskMessages DatabaseActionInProgress = new DatabaseTaskMessages("DatabaseActionInProgress");
        public static DatabaseTaskMessages DatabaseActionComplete = new DatabaseTaskMessages("DatabaseActionComplete");
        public static DatabaseTaskMessages DatabaseRestoreFilesResponse = new DatabaseTaskMessages("DatabaseRestoreFilesResponse");

        public static DatabaseTaskMessages DatabaseBackupRequested = new DatabaseTaskMessages("DatabaseBackupRequested");
        public static DatabaseTaskMessages DatabaseRestoreFilesRequested = new DatabaseTaskMessages("DatabaseRestoreFilesRequested");
        public static DatabaseTaskMessages RestoreDatabase = new DatabaseTaskMessages("RestoreDatabase");

        protected DatabaseTaskMessages(string name)
            : base(name)
        {
        }

        protected DatabaseTaskMessages(int value, string name)
            : base(value, name)
        {
        }
    }
}