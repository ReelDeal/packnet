﻿namespace PackNet.Common.Interfaces.Enums
{
    public class PackagingMachineErrorStatuses : MachineErrorStatuses
    {
        public static readonly PackagingMachineErrorStatuses OutOfCorrugate = new PackagingMachineErrorStatuses("PackagingMachineOutOfCorrugate");

        protected PackagingMachineErrorStatuses(string name) : base(name) { }
    }

    public class PacksizePackagingMachineErrorStatuses : MachineErrorStatuses
    {
        public static readonly PacksizePackagingMachineErrorStatuses RollAxis = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorRollAxis");
        public static readonly PacksizePackagingMachineErrorStatuses ToolAxis = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorToolAxis");
        public static readonly PacksizePackagingMachineErrorStatuses PaperJam = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorPaperJam");
        public static readonly PacksizePackagingMachineErrorStatuses ConnectionProblem = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorConnectionProblem");
        public static readonly PacksizePackagingMachineErrorStatuses CorrugateMismatch = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorCorrugateMismatch");
        public static readonly PacksizePackagingMachineErrorStatuses LongHeadQuantity = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorLongHeadQuantity");
        public static readonly PacksizePackagingMachineErrorStatuses LongHeadPosition = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorLongHeadPosition");
        public static readonly PacksizePackagingMachineErrorStatuses TrackQuantity = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorTrackQuantity");
        public static readonly PacksizePackagingMachineErrorStatuses TrackOffset = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorTrackOffset");
        public static readonly PacksizePackagingMachineErrorStatuses InstructionList = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorInstructionList");
        public static readonly PacksizePackagingMachineErrorStatuses AirPressureLow = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorAirPressureLow");
        public static readonly PacksizePackagingMachineErrorStatuses MovablePressureRollerOutOfPosition = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorMovablePressureRollerOutOfPosition");
        public static readonly PacksizePackagingMachineErrorStatuses ReferenceQuantityMissmatch = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorReferenceQuantityMissmatch");
        public static readonly PacksizePackagingMachineErrorStatuses LongHeadPositionOutsideOfTolerance = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorLongHeadPositionOutsideOfTolerance");
        public static readonly PacksizePackagingMachineErrorStatuses CrossHeadPositionOutsideOfTolerance = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorCrossHeadPositionOutsideOfTolerance");
        public static readonly PacksizePackagingMachineErrorStatuses PositionLatchError = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorPositionLatchError");
        public static readonly PacksizePackagingMachineErrorStatuses MachineInOutOfCorrugate = new PacksizePackagingMachineErrorStatuses("MachineInOutOfCorrugate");
        public static readonly PacksizePackagingMachineErrorStatuses CorrugateGuideQuantity = new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineErrorCorrugateGuideQuantity");

        protected PacksizePackagingMachineErrorStatuses(string name) : base(name) { }

        //Todo this should be removed when we aren't adding any more error codes
        public static PacksizePackagingMachineErrorStatuses GetUnknownErrorCode(ushort errorCode)
        {
            return new PacksizePackagingMachineErrorStatuses("PacksizePackagingMachineError" + errorCode);
        }
    }
}