﻿namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// Represent paused states for machines
    /// </summary>
    public class MachinePausedStatuses : MachineStatuses
    {
        /// <summary>
        /// Machine is paused
        /// </summary>
        public static readonly MachinePausedStatuses MachinePaused = new MachinePausedStatuses("MachinePaused");

        /// <summary>
        /// Machine is in change corrugate mode
        /// </summary>
        public static readonly MachinePausedStatuses MachineChangingCorrugate = new MachinePausedStatuses("MachineChangingCorrugate");
        

        /// <summary>
        /// Machine is in service mode
        /// </summary>
        public static readonly MachinePausedStatuses MachineService = new MachinePausedStatuses("MachineService");

        ///<summary>
        /// Machine is currently calibrating
        /// </summary>
        public static readonly MachinePausedStatuses MachineCalibrating = new MachinePausedStatuses("MachineCalibrating");

        ///<summary>
        /// Light Barrier of Machine is broken
        /// </summary>
        public static readonly MachinePausedStatuses LightBarrierBroken = new MachinePausedStatuses("LightBarrierBroken");

        protected MachinePausedStatuses(string name) : base(name) { }
    }
}