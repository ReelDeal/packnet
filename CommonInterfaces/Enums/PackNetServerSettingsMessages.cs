﻿

namespace PackNet.Common.Interfaces.Enums
{
	public sealed class PackNetServerSettingsMessages : MessageTypes
	{
		/* Commands */
		public static readonly PackNetServerSettingsMessages GetSettings = new PackNetServerSettingsMessages("GetSettings");
		public static readonly PackNetServerSettingsMessages UpdateSettings = new PackNetServerSettingsMessages("UpdateSettings");
        public static readonly PackNetServerSettingsMessages GetSearchConfiguration = new PackNetServerSettingsMessages("GetSearchConfiguration");

		/* Responses */
		public static readonly PackNetServerSettingsMessages ServerSettings = new PackNetServerSettingsMessages("ServerSettings");
        public static readonly PackNetServerSettingsMessages ServerSettingsUpdated = new PackNetServerSettingsMessages("ServerSettingsUpdated");
        public static readonly PackNetServerSettingsMessages ServerCallBackIPSettingsUpdated = new PackNetServerSettingsMessages("ServerCallBackIPSettingsUpdated");
        public static readonly PackNetServerSettingsMessages WorkflowTrackingLevelChanged = new PackNetServerSettingsMessages("WorkflowTrackingLevelChanged");
        public static readonly PackNetServerSettingsMessages SearchConfiguration = new PackNetServerSettingsMessages("SearchConfiguration");

		private PackNetServerSettingsMessages(string name)
			: base(name)
		{
		}
	}
}