﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class ClassificationMessages : MessageTypes
    {
        /*Commands*/
        public static readonly ClassificationMessages GetClassifications = new ClassificationMessages("GetClassifications");
        public static readonly ClassificationMessages CreateClassification = new ClassificationMessages("CreateClassification");
        public static readonly ClassificationMessages UpdateClassification = new ClassificationMessages("UpdateClassification");
        public static readonly ClassificationMessages DeleteClassification = new ClassificationMessages("DeleteClassification");

        /*Responses*/
        public static readonly ClassificationMessages Classifications = new ClassificationMessages("Classifications");
        public static readonly ClassificationMessages ClassificationCreated = new ClassificationMessages("ClassificationCreated");
        public static readonly ClassificationMessages ClassificationUpdated = new ClassificationMessages("ClassificationUpdated");
        public static readonly ClassificationMessages ClassificationDeleted = new ClassificationMessages("ClassificationDeleted");

        //TODO: Use UpdateClassification instead
        public static readonly ClassificationMessages UpdateClassificationStatus = new ClassificationMessages("UpdateClassificationStatus");

        private ClassificationMessages(string name) : base(name) {}
    }
}