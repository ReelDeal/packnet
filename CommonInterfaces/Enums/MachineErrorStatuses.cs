﻿namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// Error states for machines
    /// </summary>
    public class MachineErrorStatuses : MachineStatuses
    {
        public static readonly MachineErrorStatuses MachineError = new MachineErrorStatuses("MachineError");
        public static readonly MachineErrorStatuses MachineAlreadyInProductionError = new MachineErrorStatuses("MachineAlreadyInProductionError");
        public static readonly MachineErrorStatuses MachineNotConfigured = new MachineErrorStatuses("MachineNotConfigured");
        public static readonly MachineErrorStatuses EmergencyStop = new MachineErrorStatuses("MachineEmergencyStop");
        

        protected MachineErrorStatuses(string name) : base(name) { }
    }
}