﻿namespace PackNet.Common.Interfaces.Enums
{
    public sealed class CartonPropertyGroupMessages : MessageTypes
    {
        /* Commands */
        public static readonly CartonPropertyGroupMessages CreateCartonPropertyGroup = new CartonPropertyGroupMessages("CreateCartonPropertyGroup");
        public static readonly CartonPropertyGroupMessages UpdateCartonPropertyGroup = new CartonPropertyGroupMessages("UpdateCartonPropertyGroup");
        public static readonly CartonPropertyGroupMessages DeleteCartonPropertyGroup = new CartonPropertyGroupMessages("DeleteCartonPropertyGroup");
        public static readonly CartonPropertyGroupMessages GetCartonPropertyGroups = new CartonPropertyGroupMessages("GetCartonPropertyGroups");
        public static readonly CartonPropertyGroupMessages UpdateCartonPropertyGroupStatus = new CartonPropertyGroupMessages("UpdateCartonPropertyGroupStatus");

        /* Responses */
        public static readonly CartonPropertyGroupMessages CartonPropertyGroups = new CartonPropertyGroupMessages("CartonPropertyGroups");
        public static readonly CartonPropertyGroupMessages CartonPropertyGroupCreated = new CartonPropertyGroupMessages("CartonPropertyGroupCreated");
        public static readonly CartonPropertyGroupMessages CartonPropertyGroupUpdated = new CartonPropertyGroupMessages("CartonPropertyGroupUpdated");
        public static readonly CartonPropertyGroupMessages CartonPropertyGroupDeleted = new CartonPropertyGroupMessages("CartonPropertyGroupDeleted");

        public static readonly CartonPropertyGroupMessages CartonPropertyGroupsCountersUpdated = new CartonPropertyGroupMessages("CartonPropertyGroupsCountersUpdated");
        public static readonly CartonPropertyGroupMessages ProducibleCompleted = new CartonPropertyGroupMessages("ProducibleCompleted");

        private CartonPropertyGroupMessages(string name)
            : base(name)
        {
        }
    }
}