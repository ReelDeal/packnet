﻿namespace PackNet.Common.Interfaces.Enums
{
    public class ExternalCustomerSystemMessages : MachineMessages
    {
        public static readonly ExternalCustomerSystemMessages GetExternalCustomerSystemWorkflows = new ExternalCustomerSystemMessages("GetExternalCustomerSystemWorkflows");
        public static readonly ExternalCustomerSystemMessages ExternalCustomerSystemWorkflows = new ExternalCustomerSystemMessages("ExternalCustomerSystemWorkflows");

        public static readonly ExternalCustomerSystemMessages GetExternalCustomerSystems = new ExternalCustomerSystemMessages("GetExternalCustomerSystems");
        public static readonly ExternalCustomerSystemMessages ExternalCustomerSystems = new ExternalCustomerSystemMessages("ExternalCustomerSystems");

        public static readonly ExternalCustomerSystemMessages CreateExternalCustomerSystem = new ExternalCustomerSystemMessages("CreateExternalCustomerSystem");
        public static readonly ExternalCustomerSystemMessages ExternalCustomerSystemCreated = new ExternalCustomerSystemMessages("ExternalCustomerSystemCreated");
        public static readonly ExternalCustomerSystemMessages UpdateExternalCustomerSystem = new ExternalCustomerSystemMessages("UpdateExternalCustomerSystem");
        public static readonly ExternalCustomerSystemMessages ExternalCustomerSystemUpdated = new ExternalCustomerSystemMessages("ExternalCustomerSystemUpdated");
        public static readonly ExternalCustomerSystemMessages DeleteExternalCustomerSystem = new ExternalCustomerSystemMessages("DeleteExternalCustomerSystem");

        public static readonly ExternalCustomerSystemMessages ExternalPrint = new ExternalCustomerSystemMessages("ExternalPrint");
        public static readonly ExternalCustomerSystemMessages ReportProductionComplete = new ExternalCustomerSystemMessages("ReportProductionComplete");

        protected ExternalCustomerSystemMessages(string name) : base(name) { }
    }
}