﻿namespace PackNet.Common.Interfaces.Enums.ProducibleStates
{
    /// <summary>
    /// Error States that a producible may enter while in production
    /// </summary>
    public class ErrorProducibleStatuses : ProducibleStatuses
    {
        /// <summary>
        /// Production was started but error occurred before producible was completed
        /// </summary>
        public static readonly ErrorProducibleStatuses ProducibleFailed = new ErrorProducibleStatuses("ProducibleFailed");

        /// <summary>
        /// The producible is staged but is not currently producible on any machine.  
        /// Any time this value is used there should have been a log or notification added to the system to know why it was set to not producible
        /// </summary>
        public static readonly ErrorProducibleStatuses NotProducible = new ErrorProducibleStatuses("NotProducible");

        protected ErrorProducibleStatuses(string name) : base(name) { }
    }
}