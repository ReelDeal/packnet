﻿namespace PackNet.Common.Interfaces.Enums.ProducibleStates
{
    public class ZebraInProductionProducibleStatuses : InProductionProducibleStatuses
    {

        public static readonly ZebraInProductionProducibleStatuses LabelWaitingToBePeeledOff = new ZebraInProductionProducibleStatuses("LabelWaitingToBePeeledOff");
        protected ZebraInProductionProducibleStatuses(string name) : base(name) { }

    }
}