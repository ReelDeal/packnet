﻿namespace PackNet.Common.Interfaces.Enums.ProducibleStates
{
    public class ExternalPrintProducibleStatuses : InProductionProducibleStatuses
    {
        public static readonly ExternalPrintProducibleStatuses LabelRequested = new ExternalPrintProducibleStatuses("LabelRequested");
        protected ExternalPrintProducibleStatuses(string name) : base(name) { }
    }

    public class ErrorExternalPrintProducibleStatuses : ErrorProducibleStatuses
    {
        public static readonly ErrorExternalPrintProducibleStatuses LabelRequestFailed = new ErrorExternalPrintProducibleStatuses("LabelRequestFailed");

        protected ErrorExternalPrintProducibleStatuses(string name) : base(name) { }
    }
}