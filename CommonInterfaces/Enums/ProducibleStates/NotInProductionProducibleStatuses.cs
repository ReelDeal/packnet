namespace PackNet.Common.Interfaces.Enums.ProducibleStates
{
    /// <summary>
    /// Producibles that are in the system but have not yet made it to a machine for production
    /// </summary>
    public class NotInProductionProducibleStatuses : ProducibleStatuses
    {
        /// <summary>
        /// The item has not been created in the .Server system
        /// </summary>
        public static readonly NotInProductionProducibleStatuses ProducibleStatusUnknown = new NotInProductionProducibleStatuses("ProducibleStatusUnknown");

        /// <summary>
        /// The producible has been processed by the importer and forwarded on to staging, do not use this status unless you are importing.
        /// </summary>
        public static readonly NotInProductionProducibleStatuses ProducibleImported = new NotInProductionProducibleStatuses("ProducibleImported");

        /// <summary>
        /// User of the system has flagged the item to be reproduced.  Item may need to go through staging again.
        /// </summary>
        public static readonly NotInProductionProducibleStatuses ProducibleFlaggedForReproduction = new NotInProductionProducibleStatuses("ProducibleFlaggedForReproduction");

        /// <summary>
        /// The producible has been staged, additionally it should now have been persisted in the DB.  Use this status when .Server is reading items from DB and restaging
        /// </summary>
        public static readonly NotInProductionProducibleStatuses ProducibleStaged = new NotInProductionProducibleStatuses("ProducibleStaged");

        /// <summary>
        /// The Producible has been selected by the selection algorithm for production on a machine group
        /// </summary>
        public static readonly NotInProductionProducibleStatuses ProducibleSelected = new NotInProductionProducibleStatuses("ProducibleSelected");

        public static readonly NotInProductionProducibleStatuses AddedToMachineGroupQueue = new NotInProductionProducibleStatuses("AddedToMachineGroupQueue");

        protected NotInProductionProducibleStatuses(string name) : base(name) { }
    }
}