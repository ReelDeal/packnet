﻿namespace PackNet.Common.Interfaces.Enums.ProducibleStates
{
    /// <summary>
    /// States that a producible may enter while in production
    /// </summary>
    public class InProductionProducibleStatuses : ProducibleStatuses
    {
        /// <summary>
        /// Producible has been selected for production and the machine groups workflow to produce the item has been invoked.
        /// </summary>
        public static readonly InProductionProducibleStatuses ProducibleSentToMachineGroup = new InProductionProducibleStatuses("ProducibleSentToMachineGroup");
        
        /// <summary>
        /// The Producible has been sent to a machine but not started yet.
        /// </summary>
        public static readonly InProductionProducibleStatuses ProducibleSentToMachine = new InProductionProducibleStatuses("ProducibleSentToMachine");

        /// <summary>
        /// A machine has started to work on this item.
        /// </summary>
        public static readonly InProductionProducibleStatuses ProducibleProductionStarted = new InProductionProducibleStatuses("ProducibleProductionStarted");

        public static readonly InProductionProducibleStatuses ProducibleWaitingForTrigger = new InProductionProducibleStatuses("ProducibleWaitingForTrigger");
        /// A machine has paused production of this producible and is waiting for a signal to release
        /// </summary>
        /// <summary>
        /// The machine has recommenced production of the box and released it
        /// </summary>
        public static readonly InProductionProducibleStatuses ProducibleTriggered = new InProductionProducibleStatuses("ProducibleTriggered");

        protected InProductionProducibleStatuses(string name) : base(name) { }
    }
}