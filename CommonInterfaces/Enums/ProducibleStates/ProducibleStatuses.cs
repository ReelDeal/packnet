﻿namespace PackNet.Common.Interfaces.Enums.ProducibleStates
{
    /// <summary>
    /// Any status that a producible can enter
    /// </summary>
    public class ProducibleStatuses : Enumeration
    {
        /// <summary>
        /// The Producible item was completed by a machine
        /// </summary>
        public static readonly ProducibleStatuses ProducibleCompleted = new ProducibleStatuses("ProducibleCompleted");

        /// <summary>
        /// use this status any time the system has selected an item for production and needed to be removed like clear queue
        /// </summary>
        public static readonly ProducibleStatuses ProducibleRemoved = new ProducibleStatuses("ProducibleRemoved");

        protected ProducibleStatuses(string name) : base(0, name) { }
    }
}
