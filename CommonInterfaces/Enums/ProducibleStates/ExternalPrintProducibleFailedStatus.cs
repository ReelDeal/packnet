﻿namespace PackNet.Common.Interfaces.Enums.ProducibleStates
{
    public class ExternalPrintProducibleFailedStatus : ErrorProducibleStatuses
    {
        public static readonly ExternalPrintProducibleFailedStatus LabelRequestFailed = new ExternalPrintProducibleFailedStatus("LabelRequestFailed");
        protected ExternalPrintProducibleFailedStatus(string name) : base(name) { }
    }
}