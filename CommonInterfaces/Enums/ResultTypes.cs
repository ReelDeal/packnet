﻿namespace PackNet.Common.Interfaces.Enums
{
    public class ResultTypes : Enumeration
    {
        public static readonly ResultTypes PartialSuccess = new ResultTypes("PartialSuccess");
        public static readonly ResultTypes Success = new ResultTypes("Success");
        public static readonly ResultTypes Fail = new ResultTypes("Fail");
        public static readonly ResultTypes Exists = new ResultTypes("Exists");

        protected ResultTypes(string name) : base(0, name)
        {
        }
    }
}