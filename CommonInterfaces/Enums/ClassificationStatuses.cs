﻿namespace PackNet.Common.Interfaces.Enums
{
    public class ClassificationStatuses : Enumeration
    {
        public static readonly ClassificationStatuses Normal = new ClassificationStatuses("Normal");
        public static readonly ClassificationStatuses Expedite = new ClassificationStatuses("Expedite");
        public static readonly ClassificationStatuses Last = new ClassificationStatuses("Last");
        public static readonly ClassificationStatuses Stop = new ClassificationStatuses("Stop");

        protected ClassificationStatuses(string name) : base(0, name) { }
    }
}