﻿namespace PackNet.Common.Interfaces.Enums
{
    public class ZebraPrinterMessages : MachineMessages
    {
        public static readonly ZebraPrinterMessages CreateZebraPrinter = new ZebraPrinterMessages("CreateZebraPrinter");
        public static readonly ZebraPrinterMessages UpdateZebraPrinter = new ZebraPrinterMessages("UpdateZebraPrinter");
        public static readonly ZebraPrinterMessages DeleteZebraPrinter = new ZebraPrinterMessages("DeleteZebraPrinter");

        protected ZebraPrinterMessages(string name) : base(name) { }
    }
}