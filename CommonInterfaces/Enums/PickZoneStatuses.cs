﻿namespace PackNet.Common.Interfaces.Enums
{
    public class PickZoneStatuses : Enumeration
    {
        public static readonly PickZoneStatuses Normal = new PickZoneStatuses("Normal");
        public static readonly PickZoneStatuses Stop = new PickZoneStatuses("Stop");

        protected PickZoneStatuses(string name) : base(0, name) { }
    }
}