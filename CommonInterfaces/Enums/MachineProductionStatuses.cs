﻿namespace PackNet.Common.Interfaces.Enums
{
    /// <summary>
    /// Represents the status of items being created by a machine
    /// </summary>
    public class MachineProductionStatuses : Enumeration
    {
        public static readonly MachineProductionStatuses ProductionCompleted = new MachineProductionStatuses("ProductionCompleted");
        public static readonly MachineProductionStatuses ProductionIdle = new MachineProductionStatuses("ProductionIdle");

        protected MachineProductionStatuses(string name) : base(0, name) { }
    }

    /// <summary>
    /// Represents the status of items currently in error
    /// </summary>
    public class MachineProductionErrorStatuses : MachineProductionStatuses
    {
        public static readonly MachineProductionErrorStatuses ProductionError = new MachineProductionErrorStatuses("ProductionError");

        protected MachineProductionErrorStatuses(string name) : base(name) { }
    }
    /// <summary>
    /// Represents the status of items currently in progress
    /// </summary>
    public class MachineProductionInProgressStatuses : MachineProductionStatuses
    {
        public static readonly MachineProductionInProgressStatuses ProductionInProgress = new MachineProductionInProgressStatuses("ProductionInProgress");
        public static readonly MachineProductionInProgressStatuses ProductionPaused = new MachineProductionInProgressStatuses("ProductionPaused");

        protected MachineProductionInProgressStatuses(string name) : base(name) { }
    }
}