﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.Importing
{
    public interface IWorkflowImporter
    {
        /// <summary>
        /// Identifies the type of producible that will be dispatched by the importer, The Selection algorithm will listen for this type
        /// </summary>
        ProducibleTypes ProducibleType { get; }

        /// <summary>
        /// Identifies the Importer
        /// </summary>
        ImportTypes ImportType { get; }

        /// <summary>
        /// Location on disk of the workflow.xaml file, relative to the install path of .Server
        /// </summary>
        string Workflow { get; }

        /// <summary>
        /// Validation method used to make sure the environment is setup correctly
        /// </summary>
        /// <param name="pluginDirectory"></param>
        void Validate(string pluginDirectory);
    }
}