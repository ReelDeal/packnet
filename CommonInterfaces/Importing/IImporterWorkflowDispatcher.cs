using PackNet.Common.Interfaces.DTO;
using System;

namespace PackNet.Common.Interfaces.Importing
{
    public interface IImporterWorkflowDispatcher
    {
        /// <summary>
        /// Notifications when dispatched importable status changes.
        /// </summary>
        IObservable<string> StatusObservable { get; }

        /// <summary>
        /// Dispatches an Importable to a process that converts it into a concrete object usable in the system 
        /// </summary>
        /// <param name="importable">Importable to convert into a concrete object</param>
        void Dispatch(Importable importable);
    }
}