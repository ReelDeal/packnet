﻿namespace PackNet.Common.Interfaces
{
    using System;

    public interface ISearchResult
    {
        Guid Id { get; set; }

        Guid? ProductionId { get; set; }

        DateTime Created { get; set; }

        DateTime? Completed { get; set; }

        string SerialNumber { get; set; }

        string Name { get; set; }

        double? Length { get; set; }

        double? Width { get; set; }

        double? Height { get; set; }

        int DesignId { get; set; }

        string ClassificationNumber { get; set; }

        int? CorrugateQuality { get; set; }

        string CartonPropertyGroup { get; set; }

        string PickZone { get; set; }

        int NextLabelRequestNumber { get; set; }

        long TimesProduced { get; set; }

        string UserName { get; set; }

        string MachineName { get; set; }

        int MachineId { get; set; }

        string ProductionGroupAlias { get; set; }
    }
}
