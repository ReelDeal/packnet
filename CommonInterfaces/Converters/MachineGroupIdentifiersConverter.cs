﻿using System;
using System.Diagnostics;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.ExtensionMethods;

namespace PackNet.Common.Interfaces.Converters
{
    using Newtonsoft.Json;

    public class MachineGroupIdentifiersConverter : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var item = value as MachineGroup;
            serializer.Serialize(writer, new { MachineGroup = item.Alias, Status = item.CurrentStatus.DisplayName, item.Id });

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsTypeOrSubclassOf(typeof(MachineGroup));
        }


    }
}
