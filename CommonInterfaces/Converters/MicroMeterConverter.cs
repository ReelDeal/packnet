﻿using System;
using System.Globalization;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Common.Interfaces.Converters
{
    public class MicroMeterConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            double doubleValue = (MicroMeter)value;
            writer.WriteValue(doubleValue);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var val = reader.Value.ToString().Replace(',', '.');

            var readJson = double.Parse(val, CultureInfo.InvariantCulture);

            return new MicroMeter(readJson);
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(MicroMeter);
        }
    }
}
