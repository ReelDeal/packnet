﻿using System.Globalization;
using System.Runtime.Serialization.Formatters;

using Newtonsoft.Json;

namespace PackNet.Common.Interfaces.Converters
{
    public static class SerializationSettings
    {
        private static object syncObject = new object();

        private static JsonSerializerSettings settings;
        public static JsonSerializerSettings GetJsonSerializerSettings()
        {
            if (settings == null)
            {
                lock (syncObject)
                {
                    settings = new JsonSerializerSettings
                    {
                        Culture = CultureInfo.InvariantCulture,
                        TypeNameHandling = TypeNameHandling.Auto,
                        TypeNameAssemblyFormat = FormatterAssemblyStyle.Simple,
                        
                        //TODO: Fix this, it's breaking a test and possibly the PLC communication NullValueHandling = NullValueHandling.Ignore
                    };
                    settings.Converters.Add(new BrPlcBoolConverter());
                    settings.Converters.Add(new EnumerationConverter());
                    settings.Converters.Add(new IPAddressConverter());
                    settings.Converters.Add(new MicroMeterConverter());
                    settings.Converters.Add(new StringToNullableInt());
                }
            }

            return settings;
        }

        public static void AddConverter(JsonConverter converter)
        {
            lock (syncObject)
            {
                settings.Converters.Add(converter);
            }
        }
        public static JsonSerializerSettings ResetJsonSerializerSettings()
        {
            lock (syncObject)
            {
                settings = null;
                return GetJsonSerializerSettings();
            }
        }
    }
}
