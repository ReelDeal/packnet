﻿using System;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.DTO;

namespace PackNet.Common.Interfaces.Converters
{
    public static class JsonToDataRequestConverter
    {
        public static DataRequest ConvertJsonToDataRequest(string jsonMessage)
        {
            var jObject = JObject.Parse(jsonMessage);
            if (jObject == null)
                return null;

            try
            {
                var dataRequest = JsonConvert.DeserializeObject<DataRequest>(jObject["Data"].ToString());
                return dataRequest;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
