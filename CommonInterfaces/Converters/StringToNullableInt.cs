﻿using System;

namespace PackNet.Common.Interfaces.Converters
{
    using Newtonsoft.Json;

    /// <summary>
    /// Converts a json string to a nullable int... Us as an attribute above the property to convert
    /// Ex: [JsonConverter(typeof(StringToNullableInt))]
    ///     public Int32? Id { get; set; }
    /// </summary>
    public class StringToNullableInt : JsonConverter
    {

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(int?);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;
            if (reader.TokenType == JsonToken.Integer)
            {
                if (objectType == typeof(int?))
                {
                    return Convert.ToInt32(reader.Value);
                }

                return reader.Value as long?;
            }


            if (reader.TokenType == JsonToken.String)
            {
                if (string.IsNullOrEmpty((string)reader.Value))
                    return null;
                int num;
                if (int.TryParse((string)reader.Value, out num))
                    return num;

                throw new JsonReaderException(string.Format("Expected integer, got {0}", reader.Value));
            }
            throw new JsonReaderException(string.Format("Unexpected token {0}", reader.TokenType));
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(value);
        }
    }

}
