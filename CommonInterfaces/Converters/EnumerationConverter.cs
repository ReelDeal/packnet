﻿using System;
using System.Diagnostics;
using System.Linq;

namespace PackNet.Common.Interfaces.Converters
{
    using Newtonsoft.Json;

    using Enums;

    public class EnumerationConverter : JsonConverter
    {
        public static object syncObject = new object();

        public EnumerationConverter()
        {
            lock (syncObject)
            {
                if (Enumeration.Primed)
                    return;

                //The serialization needs all derived classes primed
                var subclasses =
                    from assembly in AppDomain.CurrentDomain.GetAssemblies()
                    where assembly.IsDynamic == false
                    from type in assembly.GetTypes()
                    where type.IsSubclassOf(typeof(Enumeration))
                    select type;
                subclasses.ForEach(t =>
                {
                    var fields = t.GetFields();
                    fields.ForEach(f =>
                    {
                        var val = f.GetValue(null) as Enumeration;
                       // if (val != null)
                         //   Trace.WriteLine("Primed enumeration field " + val.DisplayName);
                    });
                });
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var enumeration = (Enumeration)value;
            serializer.Serialize(writer, enumeration.DisplayName);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
                return null;

            var stringValue = serializer.Deserialize<string>(reader);

            var result = Enumeration.GetAll().FirstOrDefault(e => e.DisplayName.ToLower() == stringValue.ToLower() && IsSameOrSubclass(objectType, e.GetType()));

            if (result == null)
                throw new Exception(String.Format("Enumeration Name not found in enumeration. Type:{0} Value:{1}", objectType, stringValue));

            return result;
        }

        public override bool CanConvert(Type objectType)
        {
            return IsSameOrSubclass(typeof(Enumeration), objectType);
        }

        private static bool IsSameOrSubclass(Type potentialBase, Type potentialDescendant)
        {
            return potentialDescendant.IsSubclassOf(potentialBase)
                   || potentialDescendant == potentialBase;
        }
    }
}
