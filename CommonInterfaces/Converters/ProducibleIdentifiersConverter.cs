﻿using System;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.Converters
{
    public class ProducibleIdentifiersConverter : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var item = value as IProducible;
            serializer.Serialize(writer, new { Producible = item.ProducibleType.DisplayName, item.CustomerUniqueId, Status = item.ProducibleStatus.DisplayName, item.Id });

        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsTypeOrSubclassOf(typeof(IProducible));
        }


    }
}