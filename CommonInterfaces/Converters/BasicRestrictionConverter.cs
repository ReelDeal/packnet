﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Machines;
using System;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

namespace PackNet.Common.Interfaces.Converters
{
    /// <summary>
    /// Reduce Message Size for the UI.
    /// </summary>
    
    public class BasicCapabilityConverter:JsonConverter
    {
        /// <summary>
        /// Writes the JSON representation of the object.
        /// </summary>
        /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
        /// <param name="value">The value.</param>
        /// <param name="serializer">The calling serializer.</param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            
            //TODO: we can trim even more this message by only sending the value and not an object, but that would require modification on the UI
            var capability = (ICapability)value;
            writer.WriteStartObject();
            writer.WritePropertyName("DisplayValue");
            writer.WriteValue(capability.DisplayValue);
            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return null;
        }

        public override bool CanConvert(Type objectType)
        {
            // TODO: We should include more capabilities, but this one is the biggest one. We should not do this for AccessoryCapability
            return objectType == typeof(BasicCapability<Template>);
        }
    }
}
