using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.Converters
{
    public class MachineIdentifiersConverter : JsonConverter
    {

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var item = value as IMachine;
            if (item != null)

                serializer.Serialize(writer, new
                {
                    Alias = item.Alias ?? "",
                    Status = item.CurrentStatus.DisplayName,
                    ProductionStatus = item.CurrentProductionStatus.DisplayName,
                    item.Id
                });
            
             var items = value as IEnumerable<IMachine>;
            if (items != null)
            {
                var newItems = items.Select(m=> new 
                {
                    Alias = m.Alias ?? "",
                    Status = m.CurrentStatus.DisplayName,
                    ProductionStatus = m.CurrentProductionStatus.DisplayName,
                    m.Id
                });
                serializer.Serialize(writer, newItems);
            }
        }

        public override bool CanRead
        {
            get { return false; }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return 
                objectType.IsTypeOrSubclassOf(typeof(IMachine));
        }
    }
}