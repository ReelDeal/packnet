﻿using System;
using System.Net;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PackNet.Common.Interfaces.Converters
{
	public class IPAddressConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return (objectType == typeof(IPAddress));
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			IPAddress ip = (IPAddress)value;
			writer.WriteValue(ip.ToString());
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			JToken token = JToken.Load(reader);
            IPAddress ipAddress = Utils.Networking.FindIPAddress(token.Value<string>(), true);
            return ipAddress;
		}
	}
}