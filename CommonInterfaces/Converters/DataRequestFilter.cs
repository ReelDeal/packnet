﻿using System;

using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.Converters
{
    public abstract class DataRequestFilter
    {
    }

    [Obsolete("create your own message type")]
    public class DataRequestFilterConverter : JsonCreationConverter<DataRequestFilter>
    {
        protected override DataRequestFilter Create(Type objectType, JObject jObject)
        {
            if (FieldExists("SinceDate", jObject))
            {
                return new HistorySearchFilter();
            }
            return new ProductionSearchFilter();
        }

        private bool FieldExists(string fieldName, JObject jObject)
        {
            return jObject[fieldName] != null;
        }
    }
    [Obsolete("create your own message type")]
    public class HistorySearchFilter : ProductionSearchFilter
    {
        public HistorySearchFilter()
        {
        }

        public HistorySearchFilter(SearchField type, string searchText, DateTime sinceDate)
            : base(type, searchText)
        {
            SinceDate = sinceDate;
        }

        public DateTime SinceDate { get; set; }
    }
    [Obsolete("create your own message type")]
    public class ProductionSearchFilter : DataRequestFilter
    {
        public ProductionSearchFilter()
        {
            SearchText = string.Empty;
        }

        public ProductionSearchFilter(SearchField type, string searchText)
        {
            Type = type;
            SearchText = searchText;
        }

        public SearchField Type { get; set; }

        public string SearchText { get; set; }
    }
}
