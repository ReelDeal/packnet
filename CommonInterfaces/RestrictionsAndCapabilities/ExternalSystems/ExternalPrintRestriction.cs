using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.ExternalSystems
{
    public class ExternalPrintRestriction : BasicRestriction<Guid>
    {
        public ExternalPrintRestriction(Guid value)
            : base(value)
        {
        }
    }
}