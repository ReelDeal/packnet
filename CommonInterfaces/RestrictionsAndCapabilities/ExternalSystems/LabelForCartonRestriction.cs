using System;

using PackNet.Common.Interfaces.DTO.Carton;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.ExternalSystems
{
    /// <summary>
    /// This is being used as Metadata for External Print System 
    /// </summary>
    public class LabelForCartonRestriction : BasicRestriction<Carton>
    {
        public LabelForCartonRestriction(Carton value)
            : base(value)
        {
        }
    }
}