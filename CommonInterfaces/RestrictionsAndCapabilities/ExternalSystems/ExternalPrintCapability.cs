using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.ExternalSystems
{
    public class ExternalPrintCapability : BasicCapability<Guid>
    {
        public override Type RestrictionType { get { return typeof(ExternalPrintRestriction); } }
        public ExternalPrintCapability(Guid value)
            : base(value)
        {
        }
    }
}