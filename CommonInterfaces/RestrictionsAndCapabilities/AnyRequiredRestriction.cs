﻿namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    /// <summary>
    /// Use this restriction when you have a list of restrictions and at least one of them must be satisfied by the resolver
    /// </summary>
    /// <code><![CDATA[
    /// var aby = new AnyRequiredRestriction { new BasicRestriction<string>("something"), new BasicRestriction<int>(13), new BasicRestriction<SomeClass>(new SomeClass()) };
    /// ]]>
    /// </code>
    /// <remarks>Most of the time you will be using this in a workflow.  We should have some code activities that will allow you to add this restriction.</remarks>
    public class AnyRequiredRestriction : AggregateRestriction { }
}