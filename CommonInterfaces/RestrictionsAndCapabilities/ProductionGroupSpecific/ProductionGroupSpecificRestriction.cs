using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific
{
    /// <summary>
    /// The producible is required to be created on a specific production group
    /// The Guid payload is the ID of the production group
    /// </summary>
    public class ProductionGroupSpecificRestriction : BasicRestriction<Guid>
    {
        public ProductionGroupSpecificRestriction(Guid value) : base(value)
        {
        }
    }
}