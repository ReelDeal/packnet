using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific
{
    /// <summary>
    /// A capability used to route work to a specific machine
    /// </summary>
    public class ProductionGroupSpecificCapability : BasicCapability<Guid>
    {
        public override Type RestrictionType { get { return typeof(ProductionGroupSpecificRestriction); } }
        public ProductionGroupSpecificCapability(Guid value)
            : base(value)
        {
        }
    }
}