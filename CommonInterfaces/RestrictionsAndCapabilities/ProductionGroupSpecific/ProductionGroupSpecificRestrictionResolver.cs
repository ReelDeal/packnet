﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific
{
    public class ProductionGroupSpecificRestrictionResolver : BasicRestrictionResolver
    {
        public override IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { typeof(MachineGroupSpecificRestriction) };
            }
        }
    }
}