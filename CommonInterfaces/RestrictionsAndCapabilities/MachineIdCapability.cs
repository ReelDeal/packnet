using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    public class MachineIdCapability : BasicCapability<Guid>
    {
        public MachineIdCapability(Guid value) : base(value){}
    }
}