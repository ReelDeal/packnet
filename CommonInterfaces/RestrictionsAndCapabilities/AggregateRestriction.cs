﻿using System.Collections.Generic;

using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>Do not try and make this implement IEnumerable.  Tried that and had a real issue w/ persisting to database.</remarks>
   public abstract class AggregateRestriction : IRestriction
    {
       private ConcurrentList<IRestriction> restrictions = new ConcurrentList<IRestriction>();

       public ConcurrentList<IRestriction> Restrictions
       {
           get { return restrictions; }
           private set { restrictions = value; }
       }

       public void Add(IRestriction restriction)
        {
            Restrictions.Add(restriction);
        }
        
        public override string ToString()
        {
            return string.Join(", ", Restrictions);
        }
    }
}