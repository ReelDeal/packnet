﻿using System;
using System.Collections.Generic;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific
{
    public class MachineGroupSpecificRestrictionResolver : BasicRestrictionResolver
    {
        public override IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { typeof(MachineGroupSpecificRestriction) };
            }
        }
    }
}