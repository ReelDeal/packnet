using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific
{
    /// <summary>
    /// A capability used to route work to a specific machine
    /// </summary>
    public class MachineGroupSpecificCapability : BasicCapability<Guid>
    {
        public override Type RestrictionType { get { return typeof(MachineGroupSpecificRestriction); } }
        public MachineGroupSpecificCapability(Guid value)
            : base(value)
        {
        }
    }
}