using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific
{
    /// <summary>
    /// The producible is required to be created on a specific machine group
    /// </summary>
    public class MachineGroupSpecificRestriction : BasicRestriction<Guid>
    {
        public MachineGroupSpecificRestriction(Guid value) : base(value)
        {
        }
    }
}