﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
	public class BasicAggregateResolver : IRestrictionResolver
	{
		private readonly IRestrictionResolver rs;

		public BasicAggregateResolver(IRestrictionResolver rs)
		{
			this.rs = rs;
		}

		public IEnumerable<Type> HandlesRestrictions
		{
			get { return new List<Type> { typeof(AllRequiredRestriction), typeof(AnyRequiredRestriction) }; }
		}

		public bool Resolve(IRestriction restriction, IEnumerable<ICapability> capabilities)
		{
			var allRequiredRestriction = restriction as AllRequiredRestriction;
			if (allRequiredRestriction != null)
				return allRequiredRestriction.Restrictions.All(r => rs.Resolve(r, capabilities));

			var anyRequiredRestriction = restriction as AnyRequiredRestriction;
			if (anyRequiredRestriction != null)
				return anyRequiredRestriction.Restrictions.Any(r => rs.Resolve(r, capabilities));

			return false;
		}
	}
}