﻿using System.Activities;

using PackNet.Common.Interfaces.Machines;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.WorkFlow
{
    public sealed class AddMachineCapability<T> : CodeActivity where T : ICapability
    {
        [RequiredArgument]
        public InArgument<ICapability> Capability { get; set; }
        [RequiredArgument]
        public InArgument<IMachine> Machine { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var capability = context.GetValue(Capability);
            var machine = context.GetValue(Machine);
            // todo update using CurrentMachineCapa...
            //machine.Capabilities.Add(capability);
        }
    }
}