﻿using System;
using System.Activities;

using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.WorkFlow
{
    public sealed class AddRestriction<T> : CodeActivity where T : IRestriction
    {
        [RequiredArgument]
        public InArgument<IRestriction> Restriction { get; set; }
        [RequiredArgument]
        public InArgument<IProducible> Producible { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            var restriction = context.GetValue(Restriction);
            var producible = context.GetValue(Producible);

            if(restriction is T == false)
                throw new ArgumentException(string.Format("The passed restriction does not match the intended type, expected {0}, got {1}", typeof(T), restriction.GetType()));

            producible.Restrictions.Add(restriction);
        }
    }
}
