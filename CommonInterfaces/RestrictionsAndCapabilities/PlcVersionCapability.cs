namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    public class PlcVersionCapability : BasicCapability<string>
    {
        public PlcVersionCapability(string value) : base(value)
        {
        }

        public new string DisplayValue
        {
            get { return string.Format("Machine Version: {0}", Value); } 
        }
    }
}