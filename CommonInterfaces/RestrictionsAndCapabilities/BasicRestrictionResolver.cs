﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    using PackNet.Common.Interfaces.ExtensionMethods;

    public class BasicRestrictionResolver : IRestrictionResolver
    {
        public BasicRestrictionResolver()
        {
        }

        // TODO: This should be more configurable or read all types that implements IEquality 
        public virtual IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { 
                    typeof(BasicRestriction<string>), 
                    typeof(BasicRestriction<int>), 
                    typeof(BasicRestriction<Corrugate>), 
                    typeof(BasicRestriction<Template>), 
                    typeof(BasicRestriction<Guid>), 
                    typeof(BasicRestriction<double>), 
                    typeof(BasicRestriction<Int64>), 
                    typeof(BasicRestriction<float>) };
            }
        }

        public bool Resolve(IRestriction restriction, IEnumerable<ICapability> capabilities)
        {
            return
                capabilities.Where(c => c.RestrictionType.IsTypeOrDerivedFrom(restriction.GetType()))
                    .Any(c => c.Evaluate(restriction));
        }
    }
}