﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.Corrugates;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    /// <summary>
    /// Used to make sure that a required corrugate is loaded on a machine that is able to produce it.
    /// </summary>
    public class CorrugatesOnMachineId : IEquatable<CorrugatesOnMachineId>
    {
        public Guid MachineId { get; private set; }
        public IEnumerable<Corrugate> Corrugates { get; private set; }

        public CorrugatesOnMachineId(Guid machineId, IEnumerable<Corrugate> corrugates)
        {
            MachineId = machineId;
            Corrugates = corrugates;
        }

        public bool Equals(CorrugatesOnMachineId other)
        {
            return MachineId == other.MachineId && Corrugates.SequenceEqual(other.Corrugates);
        }
    }

    public class CorrugatesOnMachineIdCapability : BasicCapability<CorrugatesOnMachineId>
    {
        public override string DisplayValue { get { return String.Join(", ", Value.Corrugates.Select(c => c.Alias).ToArray()); } }

        public CorrugatesOnMachineIdCapability(CorrugatesOnMachineId value) : base(value)
        {
        }
    }
}
