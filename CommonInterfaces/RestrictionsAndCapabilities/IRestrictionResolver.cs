﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
	/// <summary>
	/// 
	/// </summary>
	/// <code>
	/// Configuring the 
#pragma warning disable 1570
	/// MongoDbHelpers.TryRegisterSerializer<BasicRestriction<string>>(new BasicRestrictionSerializer<string>());
#pragma warning restore 1570
	/// </code>
	public interface IRestrictionResolver
	{
		/// <summary>
		/// The Restriction types you have here must be serializable.  
		/// </summary>
		IEnumerable<Type> HandlesRestrictions { get; }

		bool Resolve(IRestriction restriction, IEnumerable<ICapability> capabilities);
	}
}