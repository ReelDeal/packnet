using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific
{
    public class MachineSpecificResolver : IRestrictionResolver
    {
        public IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { typeof(MachineSpecificRestriction) };
            }
        }

        public bool Resolve(IRestriction restriction, IEnumerable<ICapability> capabilities)
        {
            return true;
        }
    }
}