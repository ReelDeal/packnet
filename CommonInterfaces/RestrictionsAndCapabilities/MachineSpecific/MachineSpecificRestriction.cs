using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific
{
    /// <summary>
    /// The producible is required to be created on a specific machine
    /// </summary>
    public class MachineSpecificRestriction : BasicRestriction<Guid>
    {
        public MachineSpecificRestriction(Guid value) : base(value)
        {
        }
    }
}