using System;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific
{
    /// <summary>
    /// A capability used to route work to a specific machine
    /// </summary>
    public class MachineSpecificCapability : BasicCapability<Guid>
    {
        public override Type RestrictionType { get { return typeof(MachineSpecificRestriction); } }
        public MachineSpecificCapability(Guid value)
            : base(value)
        {
        }
    }
}