﻿using System;
using System.Collections.Generic;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific
{
    public class MachineSpecificRestrictionResolver : BasicRestrictionResolver
    {
        public override IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { typeof(MachineSpecificRestriction) };
            }
        }
    }
}