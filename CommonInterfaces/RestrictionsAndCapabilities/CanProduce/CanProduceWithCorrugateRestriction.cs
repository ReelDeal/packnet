﻿using PackNet.Common.Interfaces.DTO.Corrugates;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    public class CanProduceWithCorrugateRestriction : BasicRestriction<Corrugate>
    {
        public CanProduceWithCorrugateRestriction(Corrugate value) : base(value)
        {
        }
    }
}
