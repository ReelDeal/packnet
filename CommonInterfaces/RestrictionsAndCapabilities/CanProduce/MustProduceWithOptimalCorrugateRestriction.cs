﻿using PackNet.Common.Interfaces.DTO.Corrugates;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    public class MustProduceWithOptimalCorrugateRestriction : BasicRestriction<CartonOnCorrugate>
    {
        public MustProduceWithOptimalCorrugateRestriction(CartonOnCorrugate value)
            : base(value)
        {
        }
    }
}
