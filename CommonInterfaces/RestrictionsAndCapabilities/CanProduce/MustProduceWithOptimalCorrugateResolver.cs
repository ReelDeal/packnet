using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    public class MustProduceWithOptimalCorrugateResolver : IRestrictionResolver
    {
        public IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { typeof(MustProduceWithOptimalCorrugateRestriction) };
            }
        }

        public bool Resolve(IRestriction restriction, IEnumerable<ICapability> capabilities)
        {
            MustProduceWithOptimalCorrugateRestriction restrictionAsType = restriction as MustProduceWithOptimalCorrugateRestriction;
            var corrugateIdsOnMachineIdCapabilities = capabilities.OfType<CorrugatesOnMachineIdCapability>();

            return corrugateIdsOnMachineIdCapabilities.Any(corrugateCapability =>
                                                    restrictionAsType.Value.ProducibleMachines.ContainsKey(corrugateCapability.Value.MachineId) &&
                                                    corrugateCapability.Value.Corrugates.Contains(restrictionAsType.Value.Corrugate));
        }
    }
}