﻿namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    public class CanProduceWithRotationRestriction : BasicRestriction<bool>
    {
        public CanProduceWithRotationRestriction(bool value) : base(value)
        {
        }
    }
}
