using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    public class MustProduceOnCorrugateWithPropertiesResolver : IRestrictionResolver
    {
        
        public IEnumerable<Type> HandlesRestrictions
        {
            get
            {
                return new List<Type> { typeof(MustProduceOnCorrugateWithPropertiesRestriction) };
            }
        }

        public bool Resolve(IRestriction restriction, IEnumerable<ICapability> capabilities)
        {
            var res = restriction as MustProduceOnCorrugateWithPropertiesRestriction;

            Func<int, MustProduceOnCorrugateWithPropertiesRestriction, bool> qualityFunc =
                (int corrugateQuality, MustProduceOnCorrugateWithPropertiesRestriction r) =>
                {
                    if (!r.Quality.HasValue)
                        return true;
                    if (r.Quality.HasValue && r.Quality.Value == corrugateQuality)
                        return true;
                    return false;
                }; 
            Func<MicroMeter, MustProduceOnCorrugateWithPropertiesRestriction, bool> thicknessFunc =
                (MicroMeter corrugateThickness, MustProduceOnCorrugateWithPropertiesRestriction r)=>
                {
                    if (!r.Thickness.HasValue)
                        return true;
                    if (r.Thickness.HasValue && r.Thickness.Value == corrugateThickness)
                        return true;
                    return false;
                };
            Func<MicroMeter, MustProduceOnCorrugateWithPropertiesRestriction, bool> WidthFunc =
                (MicroMeter corrugateWidth, MustProduceOnCorrugateWithPropertiesRestriction r)=>
                {
                    if (!r.Width.HasValue)
                        return true;
                    if (r.Width.HasValue && r.Width.Value == corrugateWidth)
                        return true;
                    return false;
                }; 
            // Iterate over all loaded corrugates on and see if they have the necessary properties
            var capas = capabilities.OfType<CorrugatesOnMachineIdCapability>().Select(c => c);
            var corrugatesCapabilities = new List<BasicCapability<Corrugate>>();

            if (capas.Any())
            {
                capas.ForEach(
                    cp => cp.Value.Corrugates.ForEach(c => corrugatesCapabilities.Add(new BasicCapability<Corrugate>(c))));
            }
            capabilities.OfType<BasicCapability<Corrugate>>().ForEach(corrugatesCapabilities.Add);

            return corrugatesCapabilities.Any(cap =>
            {
                return qualityFunc(cap.Value.Quality, res)
                    && WidthFunc(cap.Value.Width,res) 
                    && thicknessFunc(cap.Value.Thickness, res);
            });
        }
    }
}