﻿namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    public class CanProduceWithTileCountRestriction : BasicRestriction<int>
    {
        public CanProduceWithTileCountRestriction(int value) : base(value)
        {
        }
    }
}
