﻿using System;
using System.Collections.Generic;
using System.Text;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    /// <summary>
    /// Use when you want any or all of these properties specified for the carton to be produced on.
    /// </summary>
    public class MustProduceOnCorrugateWithPropertiesRestriction : IRestriction
    {
       
        public double? Width { get; set; }

        public double? Thickness { get; set; }

        public int? Quality { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder("Corrugate must match the following properties: ");
            var properties = new List<string>();
            if (Width.HasValue)
                properties.Add("Width " + Width);
            if (Thickness.HasValue)
                properties.Add("Thickness " + Thickness);
            if (Quality.HasValue)
                properties.Add("Quality " + Quality);
            sb.Append(string.Join(", ", properties));
            sb.Append(". ");
            return sb.ToString();
        }
    }
}
