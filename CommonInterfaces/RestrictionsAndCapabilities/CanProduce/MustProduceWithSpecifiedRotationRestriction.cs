﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce
{
    public class MustProduceWithSpecifiedRotationRestriction : IRestriction
    {
        public OrientationEnum? Orientation { get; set; }

        public override string ToString()
        {
            return "Corrugate must match the following orientation: " + Orientation.ToString() + ". ";
        }
    }
}
