using System;

using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    /// <summary>
    /// Represents a capability of a machine or machine group
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BasicCapability<T> : ICapability where T : IEquatable<T>
    {
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public T Value { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicCapability{T}"/> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public BasicCapability(T value)
        {
            Value = value;
        }

        public virtual string DisplayValue { get { return Value.ToString(); } }

        public virtual Type RestrictionType { get { return typeof(BasicRestriction<T>); } }

        /// <summary>
        /// Evaluates the specified restriction.
        /// </summary>
        /// <param name="restriction">The restriction.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Invalid Restriction:  + restriction +  can not be converted to  + typeof(BasicRestriction&lt;T&gt;)</exception>
        public virtual bool Evaluate(IRestriction restriction)
        {
            var basicRestriction = restriction as BasicRestriction<T>;
            if (basicRestriction == null)
                throw new Exception("Invalid Restriction: " + restriction + " can not be converted to " + typeof(BasicRestriction<T>));

            return Value.Equals(basicRestriction.Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as BasicCapability<T>;

            if (otherValue == null)
                return false;

            if (GetType() != obj.GetType())
            {
                return false;
            }

            return Value.GetType() == otherValue.Value.GetType() && Value.Equals(otherValue.Value);
        }
    }
}