using PackNet.Common.Interfaces.Producible;

namespace PackNet.Common.Interfaces.RestrictionsAndCapabilities
{
    /// <summary>
    /// A restriction with a payload
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BasicRestriction<T> : IRestriction
    {
        public T Value { get; set; }

        public BasicRestriction()
        {
        }

        public BasicRestriction(T value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return string.Format("BasicRestriction:{0}", Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as BasicRestriction<T>;

            if (otherValue == null)
                return false;

            var typeMatches = GetType().Equals(obj.GetType());
            var genericTypesMactch = Value.GetType().Equals(otherValue.Value.GetType());
            var valueMatches = Value.Equals(otherValue.Value);

            return typeMatches && valueMatches && genericTypesMactch;
        }
    }
}