﻿using System;

using PackNet.Common.Interfaces.Enums;

namespace PackNet.Common.Interfaces.SelectionAlgorithms
{
    public interface ISelectionAlgorithm : IDisposable
    {
        string UniqueId { get; }
       
        SelectionAlgorithmTypes SelectionAlgorithmType { get; }
        string StagingWorkflowName { get; }
        string SelectionWorkflowName { get; }
        void Validate(string pluginDirectory);
    }
}