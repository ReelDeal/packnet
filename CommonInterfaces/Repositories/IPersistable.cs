﻿using System;

namespace PackNet.Common.Interfaces.Repositories
{
    using Newtonsoft.Json;

    public interface IPersistable
    {
        /// <summary>
        /// Unique identifier for persistence
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        Guid Id { get; set; }

    }
}
