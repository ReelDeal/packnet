﻿using System;

using MongoDB.Bson.Serialization;

namespace PackNet.Common.Interfaces.Repositories
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface used to persist object
    /// </summary>
    public interface IRepository<T> where T:IPersistable
    {
        /// <summary>
        /// Creates the object in the data store
        /// </summary>
        /// <param name="objectToCreate"></param>
        void Create(T objectToCreate);

        /// <summary>
        /// Creates the object in the data store
        /// </summary>
        /// <param name="objectToCreate"></param>
        void CreateEnsureNoDuplicate(T objectToCreate);

        /// <summary>
        /// Creates multiple items
        /// </summary>
        /// <param name="objectsToCreate"></param>
        void Create(IEnumerable<T> objectsToCreate);

        /// <summary>
        /// Retrieves all items from the data store
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> All();

        /// <summary>
        /// Updates the given item in the data store
        /// </summary>
        /// <param name="objectToUpdate"></param>
        /// <returns></returns>
        T Update(T objectToUpdate);

        /// <summary>
        /// Convenience method to update multiple items... NOT efficient (Read foreach) as mongo doesn't provide a bulk update
        /// </summary>
        /// <param name="objectToUpdate"></param>
        /// <returns></returns>
        void Update(IEnumerable<T> objectToUpdate);

        /// <summary>
        /// Deletes an item from the data store
        /// </summary>
        /// <param name="objectToDelete"></param>
        void Delete(T objectToDelete);

        /// <summary>
        /// Delete by id
        /// </summary>
        /// <param name="id"></param>
        void Delete(Guid id);

        /// <summary>
        /// Delete multiple items
        /// </summary>
        /// <param name="objectsToDelete"></param>
        void Delete(IEnumerable<T> objectsToDelete);

        /// <summary>
        /// Delete multiple items
        /// </summary>
        void Delete(IEnumerable<Guid> idsToDelete);

        /// <summary>
        /// Find the object in the repository by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T Find(Guid id);

        /// <summary>
        /// Find objects in the repository by Ids
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        IEnumerable<T> Find(IEnumerable<Guid> ids);

        /// <summary>
        /// Search the db for some criteria
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<T> Search(string criteria, int limit = -1);
    }
}