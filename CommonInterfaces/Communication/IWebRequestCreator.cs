using System.Net;

using PackNet.Common.Interfaces.DTO.PLC;

namespace PackNet.Common.Interfaces.Communication
{
    public interface IWebRequestCreator
    {
        string ServerAddress { get; }

        string IpAddress { get; }

        int Port { get; }

        WebRequest CreateGetRequest(PacksizePlcVariable variable);

        WebRequest CreatePostRequest(PacksizePlcVariable variable, object value);
    }
}