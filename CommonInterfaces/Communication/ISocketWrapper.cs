﻿using System;
using System.Net;

namespace PackNet.Common.Interfaces.Communication
{
    public interface ISocketWrapper : IDisposable
    {
        bool IsBound { get; }

        bool IsConnected { get; }

        int ReceiveTimeout { get; set; }

        int SendTimeout { get; set; }

        int Send(byte[] buffer);

        int Receive(byte[] buffer);

        void Listen(int backlog);

        void Connect(string host, int port);

        IAsyncResult BeginConnect(IPAddress address, int port, AsyncCallback requestCallback, object state);
    }
}
