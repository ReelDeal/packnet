﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class AlreadyConnectedException : Exception
    {
        public string ServerAddress { get; private set; }
        public string ConnectedAddress { get; private set; }

        public AlreadyConnectedException(string serverAddress, string connectedAddress)
            : base(FormatMessage(serverAddress, connectedAddress))
        {
            ServerAddress = serverAddress;
            ConnectedAddress = connectedAddress;
        }

        private static string FormatMessage(string serverAddress, string connectedAddress)
        {
            var msg = String.Format("Machine connection error: .Server instance with addresss {0} is already connected to machine with address {1}", serverAddress, connectedAddress);
            Trace.WriteLine(msg);
            return msg;
        }
    }
}
