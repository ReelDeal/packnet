﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class InUseException: Exception
    {
        public InUseException(string message) : base(message)
        {
        }
    }
}