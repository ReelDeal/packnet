﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class ImportFailedException : Exception
    {
        public ImportFailedException(string message)
            : base(message)
        {
        }
    }
}
