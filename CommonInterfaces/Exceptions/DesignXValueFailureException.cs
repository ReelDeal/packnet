﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    [Serializable]
    public class DesignXValueFailureException : Exception
    {
        public DesignXValueFailureException(string formula, int designId)
        {
            Formula = formula;
            DesignId = designId;
        }

        public string Formula { get; private set; }

        public int DesignId { get; private set; }
    }
}
