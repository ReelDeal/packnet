﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class PrinterBusyException : Exception
    {
        public PrinterBusyException(string message) : base(message)
        {
        }
    }
}
