﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class RelationshipExistsException : Exception
    {
        public RelationshipExistsException(string message) :
            base(message)
        {
        }
    }
}