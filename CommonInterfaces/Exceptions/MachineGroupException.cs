﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    using PackNet.Common.Interfaces.Enums;

    public class MachineGroupException : Exception
    {
        public readonly Guid MachineGroupId;
        public readonly MessageTypes MessageType;

        public MachineGroupException(Guid machinegroupId, MessageTypes messageType)
        {
            MachineGroupId = machinegroupId;
            MessageType = messageType;
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}", MessageType, MachineGroupId);
        }
    }
}