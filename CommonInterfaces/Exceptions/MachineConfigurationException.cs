﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Utils;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class MachineConfigurationException : Exception
    {
        public Enumeration ErrorMessageType { get; set; }

        public ConcurrentList<int> TrackNumbers { get; set; }

        public string MachineAlias { get; set; }
    }
}
