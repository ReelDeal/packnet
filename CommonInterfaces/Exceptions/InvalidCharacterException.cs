﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class InvalidCharacterException : Exception
    {
        public InvalidCharacterException(string message, params object[] args)
            : base(string.Format(message, args))
        {
        }
    }
}