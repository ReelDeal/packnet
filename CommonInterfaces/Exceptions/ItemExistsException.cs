﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class ItemExistsException : Exception
    {
        public ItemExistsException(string message) : 
            base(message)
        {
        }
    }
}
