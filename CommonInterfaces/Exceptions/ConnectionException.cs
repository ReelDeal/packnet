﻿using System;
using System.Diagnostics;

namespace PackNet.Common.Interfaces.Exceptions
{
    [Serializable]
    public class ConnectionException : Exception
    {
        public ConnectionException(string serverAddress, string message)
            : base(FormatMessage(serverAddress, message))
        {
            
        }
        public ConnectionException(string serverAddress, Exception exception)
            : base(FormatMessage(serverAddress), exception)
        {
        }

        public ConnectionException(string serverAddress, string message, Exception exception)
            : base(FormatMessage(serverAddress, message), exception)
        {
        }

        private static string FormatMessage(string serverAddress, string message = "")
        {
            var msg = "Machine connection error: " + serverAddress;
            if (!String.IsNullOrEmpty(message))
                msg += (" - " + message);
            Trace.WriteLine(msg);
            return msg;
        }
    }
}
