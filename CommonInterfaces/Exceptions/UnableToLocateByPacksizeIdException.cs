﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class UnableToLocateByPacksizeIdException : Exception
    {
        public UnableToLocateByPacksizeIdException(string message) : base(message)
        {
            
        }
    }
}
