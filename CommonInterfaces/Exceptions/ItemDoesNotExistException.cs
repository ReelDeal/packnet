﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class ItemDoesNotExistException : Exception
    {
        public ItemDoesNotExistException(string message) : base(message)
        {
        }
    }
}
