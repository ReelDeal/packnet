﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    [Serializable]
    public class WriteException : Exception
    {
        public WriteException(string taskName, string variableName, object value)
        {
            TaskName = taskName;
            VariableName = variableName;
            Value = value;
        }

        public string TaskName { get; private set; }

        public string VariableName { get; private set; }

        public object Value { get; private set; }
    }
}
