﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class EditOnLoadedCorrugateException : Exception
    {
        public EditOnLoadedCorrugateException(string message) : base(message)
        {            
        }
    }
}
