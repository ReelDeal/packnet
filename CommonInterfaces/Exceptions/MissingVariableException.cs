﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    [Serializable]
    public class MissingVariableException : Exception
    {
        public MissingVariableException(string taskName, string variableName)
        {
            TaskName = taskName;
            VariableName = variableName;
        }

        public string TaskName { get; private set; }

        public string VariableName { get; private set; }
    }
}
