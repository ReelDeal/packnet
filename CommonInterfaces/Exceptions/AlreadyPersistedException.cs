﻿using System;

namespace PackNet.Common.Interfaces.Exceptions
{
    public class AlreadyPersistedException : Exception
    {
        public AlreadyPersistedException(string message) : base(message)
        {            
        }
    }
}
