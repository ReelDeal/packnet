﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.FusionSim.Utilities;

using Testing.Specificity;

namespace FusionSimTest.Utilities
{
    [TestClass]
    public class WebServiceShould
    {
        [TestMethod]
        public void Be_created()
        {
            var simulator = new Mock<IFusionSimulator>();
            var webService = new WebService(simulator.Object);
            Specify.That(webService).Should.Not.BeNull();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Throw_exception_when_simulator_is_not_defined()
        {
            new WebService(null);
        }


        // TODO: Need to find a way to override the WebOperationContext.Current.IncomingRequest.UriTemplateMatch.RequestUri
        //[TestMethod]
        //public void GetVariable()
        //{
        //    const string result = "value";
        //    var simulator = new Mock<IFusionSimulator>();
        //    simulator.Setup(e => e.GetMachineVariable("key")).Returns(result);
        //    var webService = new WebService(simulator.Object);
        //    Specify.That(webService.GetVariable()).Should.BeEqualTo(result);
        //}

        [TestMethod]
        public void SetVariable()
        {
            
        }
    }
}
