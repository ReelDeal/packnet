﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Globalization;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;

namespace Packnet.Plugin.ViewModels
{
    [Export(typeof(IService))]
    public class ViewModelService : IService, IViewModelService
    {
        private ILogger logger;


        [ImportingConstructor]
        public ViewModelService(IServiceLocator serviceLocator, ILogger logger)
        {
            this.logger = logger;

            //do this last
            serviceLocator.RegisterAsService(this);
        }

        /// <summary>
        /// Testing constructor only.
        /// </summary>
        public ViewModelService()
        {

        }

        public JArray CreateSearchResult(IEnumerable<IProducible> convertFrom)
        {
            var sw = new Stopwatch();
            var result = convertFrom.Select(p => Convert(p, null).Item2);// originally tried to do an asParallel but then the ordering of the items after was taking too long.  This is good times.
            var output = new JArray(result);
            return output;
        }

        public Tuple<DateTime /*created date time*/, JObject> Convert(IProducible producible)
        {
            return ViewModelService.Convert(producible, null);
        }

        public static Tuple<DateTime/*created date time*/, JObject> Convert(IProducible producible, JObject baseViewModel = null)
        {
            //basically ignore wrappers.
            if (producible is IProducibleWrapper)
                return Convert(((IProducibleWrapper)producible).Producible, baseViewModel);

            if (baseViewModel == null)
                baseViewModel = GetBase(producible);

            if (producible is ICarton)
                LoadDetails(producible as ICarton, baseViewModel);
            else if (producible is Label)
                LoadDetails(producible as Label, baseViewModel);
            else if (producible is Kit)
                LoadDetails(producible as Kit, baseViewModel);

            return new Tuple<DateTime, JObject>(DateTime.Parse(baseViewModel["Created"].ToString()), baseViewModel);
        }

        private static void LoadDetails(Label l, JObject baseViewModel)
        {
            var template = l.Restrictions.OfType<BasicRestriction<Template>>().FirstOrDefault();
            if (template != null)
                baseViewModel["Template"] = template.Value.Name;

            var priority = l.Restrictions.OfType<BasicRestriction<string>>().FirstOrDefault();
            if (priority != null)
                baseViewModel["IsPriority"] = priority.Value;

        }

        private static void LoadDetails(ICarton c, JObject baseViewModel)
        {
            baseViewModel["DesignId"] = c.DesignId;
            baseViewModel["Length"] = c.Length;
            baseViewModel["Width"] = c.Width;
            baseViewModel["Height"] = c.Height;
            if (c.CartonOnCorrugate != null && c.CartonOnCorrugate.Corrugate != null)
            {
                baseViewModel["OptimalCorrugate"] = new JObject
                {
                    {"Alias", c.CartonOnCorrugate.Corrugate.Alias},
                    {"Width", Double.Parse(c.CartonOnCorrugate.Corrugate.Width.ToString(), CultureInfo.InvariantCulture)},
                    {"Thickness", Double.Parse(c.CartonOnCorrugate.Corrugate.Thickness.ToString(), CultureInfo.InvariantCulture)},
                    {"Quality", c.CartonOnCorrugate.Corrugate.Quality}
                };
                baseViewModel["TrackNumber"] = c.TrackNumber;
            }
            if (c.XValues != null && c.XValues.Count > 0)
            {
                var values = c.XValues.Select(v => v.Key + ":" + v.Value);
                baseViewModel["XValues"] = String.Join(", ", values);
            }
        }

        private static void LoadDetails(Kit kit, JObject baseViewModel)
        {
            var items = new JArray();
            kit.ItemsToProduce.ForEach(p => items.Add(Convert(p, null).Item2));
            baseViewModel["ItemsToProduce"] = items;
        }

        private static DateTime? GetCompletedData(IProducible producible)
        {
            if (producible == null || producible.History == null)
                return null;

            var checkHistoryForComplete = producible.History.FirstOrDefault(a => a.NewStatus == ProducibleStatuses.ProducibleCompleted);

            if (checkHistoryForComplete == null)
                return null;

            return checkHistoryForComplete.OccuredOn;
        }

        private static JObject GetBase(IProducible convertFrom)
        {
            var viewModel = new JObject
            {
                {"Id" ,convertFrom.Id.ToString()},
                {"ProducibleType", convertFrom.ProducibleType.DisplayName},
                {"CustomerUniqueId", convertFrom.CustomerUniqueId},
                {"ProducibleStatus", convertFrom.ProducibleStatus.DisplayName },
                {"Created" , convertFrom.Created},
                {"Completed" , GetCompletedData(convertFrom)} //Add the completed so staples has it TFS 10537
            };

            if (convertFrom.ProducedOnMachineGroupId != Guid.Empty)
                viewModel["ProducedOnMachineGroupId"] = convertFrom.ProducedOnMachineGroupId.ToString();

            viewModel["ProducibleStatusDateTime"] = convertFrom.ProducibleStatusLastChangedDateTime;
            
            //load up production info
            var prodInfo = convertFrom.History.Where(h => h.NewStatus == ProducibleStatuses.ProducibleCompleted
                                                            || h.NewStatus == ProducibleStatuses.ProducibleRemoved
                                                            || h.NewStatus == ErrorProducibleStatuses.ProducibleFailed)
                .Select(h => new JObject
                {
                    { "OccuredOn", h.OccuredOn },
                    { "Status", h.NewStatus.DisplayName },
                    { "MachineGroupId", LookupMachineGroupIdInHistoryRecord(h.Notes) }
                });
            viewModel["ProductionInfo"] = new JArray(prodInfo);

            //This is part of the fix for TFS 10570: BF Search - Failed w/ no CUID yields zero results
            //Check child producibles and mark their parent (Kit) as failed in the search results
            Kit kit = convertFrom as Kit;

            if (kit != null)
            {
                if(kit.ItemsToProduce.Any(a => a.ProducibleStatus == ErrorProducibleStatuses.ProducibleFailed))
                {
                    viewModel["ProducibleStatus"] = ErrorProducibleStatuses.ProducibleFailed.DisplayName;
                }
            }
            
            return viewModel;
        }

        public static string LookupMachineGroupIdInHistoryRecord(string notes)
        {
            if (string.IsNullOrWhiteSpace(notes))
                return string.Empty;
            var o = JsonConvert.DeserializeObject<JObject>(notes);
            JToken token;
            if (o.TryGetValue("ProducedOnMachineGroupId", out token))
                return token.ToString();
            return string.Empty;
        }

        public void Dispose()
        {

        }

        public string Name { get { return "Packnet Server ViewModels"; } }
    }
}