﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Interfaces.DTO.Carton;
using Testing.Specificity;
using WorkflowProfiling;

namespace WorkflowProfilerTests
{
    [TestClass]
    public class ProfilingResultsComparerTests
    {
        private readonly IEnumerable<string> _propertiesToIgnore = new[] { "PrintInfoField2" };

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDisplayDifferencesBetweenProfilingResults()
        {
            var comparison = PropertyComparer.Compare(GetFirstProfilingResult().Result.Select(c => c.CustomerUniqueId), GetSecondProfilingResult().Result.Select(c => c.CustomerUniqueId), _propertiesToIgnore);

            Specify.That(comparison).Should.Not.BeNull();
            Specify.That(comparison.Count()).Should.BeEqualTo(1);
            
            var diffsForFirstCarton = comparison.First();
            Specify.That(diffsForFirstCarton.Value.Count()).Should.BeEqualTo(1);
            Specify.That(diffsForFirstCarton.Value.First().PropertyName).Should.BeEqualTo("PrintInfoField1");
            Specify.That((string)diffsForFirstCarton.Value.First().OldValue).Should.BeNull();
            Specify.That((string)diffsForFirstCarton.Value.First().NewValue).Should.BeEqualTo("123456789");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ShouldThrowExceptionIfResultCountDiffers()
        {
            var res1 = new[] {"blah"};
            var res2 = new[] { "blah", "blah" };
            PropertyComparer.Compare(res1, res2, _propertiesToIgnore);
        }

        private ProfilingResult GetFirstProfilingResult()
        {
            var cmd = new Carton();
            var req = GetFirstCartonRequest();

            var results = new[] {cmd};
            var res = new ProfilingResult(new TimeSpan(1000), results, 1, false);

            return res;
        }

        private ProfilingResult GetSecondProfilingResult()
        {
            var cmd = new Carton();
            var req = GetSecondCartonRequest();

            var results = new[] { cmd };
            var res = new ProfilingResult(new TimeSpan(1000), results, 1, false);

            return res;
        }


        private Carton GetFirstCartonRequest()
        {
            var req = new Carton();
            req.ArticleId = "123";
            req.ProductionGroupAlias = "propGroup";
            //req.ClassificationNumber = "12345";
            req.CorrugateQuality = 4;
            req.Created = new DateTime(2014, 10, 28);
            //req.Custom = false;
            req.CustomerUniqueId = "123456789";
            //req.RequestGroupId = Guid.Parse("4d30668c-5c1e-4797-b1fa-082b021cca8d");

            return req;
        }

        private Carton GetSecondCartonRequest()
        {
            var req = new Carton();
            req.ArticleId = "123";
            req.ProductionGroupAlias = "propGroup";
            //req.ClassificationNumber = "12345";
            req.CorrugateQuality = 4;
            req.Created = new DateTime(2014, 10, 28);
            //req.Custom = false;
            req.CustomerUniqueId = "123456789";
            //req.RequestGroupId = Guid.Parse("4d30668c-5c1e-4797-b1fa-082b021cca8d");
            //req.PrintInfoField1 = "123456789";
            //req.PrintInfoField2 = "blah";

            return req;
        }
    }
}
