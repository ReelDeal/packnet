﻿using System;
using System.Globalization;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Producible;

namespace WorkflowProfilerTests
{
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq;

    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Exceptions;
    using PackNet.Common.Utils;
    using WorkflowProfiling;
    using WorkflowProfiling.Events;

    [TestClass]
    public class ProfilerTests
    {
        [TestMethod]
        public void CanRunProfilerOnValidWorkflow()
        {
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");
            var profiler = new WorkflowProfiler(TimeSpan.FromSeconds(10));
            var workflow = @"..\..\..\..\Importer\ImportPlugins\Carton\CartonImport.xaml";

            var importable = new Importable() { ImportType = ImportTypes.Order, ImportedItems = this.GetValidImportableData() };

            var baselineResult = profiler.ProfileWorkflow(workflow, importable);

            Assert.IsNotNull(baselineResult);
            Assert.IsFalse(baselineResult.ThrewException);
            Assert.AreEqual(baselineResult.NumberOfItemsInResult, 2);
            Assert.IsNotNull(baselineResult.Result);
            var resultList = ((IEnumerable<IProducible>)baselineResult.Result).ToList();
            Assert.AreEqual(resultList.Count, 2);
            Assert.IsTrue(baselineResult.TotalTimeToComplete.TotalMilliseconds > 0 );
            Assert.IsTrue(baselineResult.TotalTimeToComplete.TotalMilliseconds < 1000);
        }

        //private void ProfilingCompleted(object sender, ProfilingResultEventArgs<ICartonRequestCommand> profilingResultEventArgs)
        
            
        

        [TestMethod]
        public void CanRunProfilerOnTwoValidWorkflowsAndGetResultsForBoth()
        {
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
            CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("en-US");
            var profiler = new WorkflowProfiler(TimeSpan.FromSeconds(10));
            var workflow = @"..\..\..\..\Importer\ImportPlugins\Carton\CartonImport.xaml";

            var importable = new Importable() { ImportType = ImportTypes.BoxFirst, ImportedItems = this.GetValidImportableData() };

            var comparisonResult = profiler.CompareWorkflows(workflow, workflow, importable, Enumerable.Empty<string>());

            Assert.IsNotNull(comparisonResult.BaseProfilingResult);
            Assert.IsFalse(comparisonResult.BaseProfilingResult.ThrewException);
            Assert.AreEqual(comparisonResult.BaseProfilingResult.NumberOfItemsInResult, 2);
            Assert.IsNotNull(comparisonResult.BaseProfilingResult.Result);
            var resultList = comparisonResult.BaseProfilingResult.Result.ToList();
            Assert.AreEqual(resultList.Count, 2);
            Assert.IsTrue(comparisonResult.BaseProfilingResult.TotalTimeToComplete.TotalMilliseconds > 0);
            Assert.IsTrue(comparisonResult.BaseProfilingResult.TotalTimeToComplete.TotalMilliseconds < 1000);

            Assert.IsNotNull(comparisonResult.ComparisonProfilingResult);
            Assert.IsFalse(comparisonResult.ComparisonProfilingResult.ThrewException);
            Assert.AreEqual(comparisonResult.ComparisonProfilingResult.NumberOfItemsInResult, 2);
            Assert.IsNotNull(comparisonResult.ComparisonProfilingResult.Result);
            resultList = comparisonResult.ComparisonProfilingResult.Result.ToList();
            Assert.AreEqual(resultList.Count, 2);
            Assert.IsTrue(comparisonResult.ComparisonProfilingResult.TotalTimeToComplete.TotalMilliseconds > 0);
            Assert.IsTrue(comparisonResult.ComparisonProfilingResult.TotalTimeToComplete.TotalMilliseconds < 1000);
        }

        [TestMethod]
        public void InvalidWorkflowReturnsResultWithException()
        {
            var profiler = new WorkflowProfiler(TimeSpan.FromSeconds(10));
            var workflow = @"..\..\..\..\Importer\ImportPlugins\Carton\CartonImport.xaml";

            var importable = new Importable() { ImportType = ImportTypes.BoxFirst, ImportedItems = this.GetInvalidImportableData() };

            var baselineResult = profiler.ProfileWorkflow(workflow, importable);

            Assert.IsNotNull(baselineResult);
            Assert.IsTrue(baselineResult.ThrewException);
            Assert.IsTrue(baselineResult.ThrownException.GetType() == typeof(ImportFailedException), string.Format("Threw exception {0} but expected exception {1}", baselineResult.ThrownException.GetType(), typeof(ImportFailedException)));
            Assert.IsTrue(baselineResult.TotalTimeToComplete.TotalMilliseconds > 0);
            Assert.IsTrue(baselineResult.TotalTimeToComplete.TotalMilliseconds < 1000);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [ExpectedException(typeof (TimeoutException))]
        public void ShouldThrowTimeoutExceptionIfTimeoutExpires()
        {
            var profiler = new WorkflowProfiler(TimeSpan.FromMilliseconds(0.01));
            var workflow = @"..\..\..\..\Importer\ImportPlugins\Carton\CartonImport.xaml";
            var importable = new Importable() { ImportType = ImportTypes.BoxFirst, ImportedItems = this.GetInvalidImportableData() };
            profiler.ProfileWorkflow(workflow, importable);
        }

        private IEnumerable<Object> GetValidImportableData()
        {
            dynamic data = new ExpandoObject();
            data.Command = "1";
            data.SerialNumber = "serial number 1";
            data.OrderId = "order1";
            data.Quantity = "1";
            data.Name = "Test1";
            data.Length = "1500";
            data.Width = "1.444";
            data.Height = "900.635465494";
            data.ClassificationNumber = "1";
            data.CorrugateQuality = "";
            data.PickZone = "AZ1";
            data.DesignId = "2010002"; 
            data.PrintInfoField1 = "r1";
            data.PrintInfoField2 = "r2";
            data.PrintInfoField3 = "r3";
            data.PrintInfoField4 = "r4";
            data.PrintInfoField5 = "r5";
            data.PrintInfoField6 = "r6";
            data.PrintInfoField7 = "r7";
            data.PrintInfoField8 = "r8";
            data.PrintInfoField9 = "r9";
            data.PrintInfoField10 = "r10";

            dynamic data2 = new ExpandoObject();
            data2.Command = "1";
            data2.SerialNumber = "serial number 2";
            data2.OrderId = "order2";
            data2.Quantity = "2";
            data2.Name = "Test2";
            data2.Length = "1222"; 
            data2.Width = "1.111"; 
            data2.Height = "900.25465494"; 
            data2.ClassificationNumber = "1";
            data2.CorrugateQuality = "";
            data2.PickZone = "AZ1";
            data2.DesignId = "2010002";
            data2.PrintInfoField1 = "r1"; 
            data2.PrintInfoField2 = "r2";
            data2.PrintInfoField3 = "r3";
            data2.PrintInfoField4 = "r4";
            data2.PrintInfoField5 = "r5";
            data2.PrintInfoField6 = "r6";
            data2.PrintInfoField7 = "r7";
            data2.PrintInfoField8 = "r8";
            data2.PrintInfoField9 = "r9";
            data2.PrintInfoField10 = "r10";

            return new[] { data, data2 };
        }

        private IEnumerable<Object> GetInvalidImportableData()
        {
            dynamic data = new ExpandoObject();
            data.Command = "1";
            data.SerialNumber = "serial number 1"; 
            data.OrderId = "order1";
            data.Quantity = "1";
            data.Name = "Test1";
            data.Length = "1   a 500";
            data.Width = "1.444"; 
            data.Height = "900.635465494";
            data.ClassificationNumber = "1";
            data.CorrugateQuality = ""; 
            data.PickZone = "AZ1";
            data.DesignId = "2010002";
            data.PrintInfoField1 = "r1";
            data.PrintInfoField2 = "r2";
            data.PrintInfoField3 = "r3";
            data.PrintInfoField4 = "r4";
            data.PrintInfoField5 = "r5";
            data.PrintInfoField6 = "r6";
            data.PrintInfoField7 = "r7";
            data.PrintInfoField8 = "r8";
            data.PrintInfoField9 = "r9";
            data.PrintInfoField10 = "r10";

            return new[] { data };
        }
    }
}
