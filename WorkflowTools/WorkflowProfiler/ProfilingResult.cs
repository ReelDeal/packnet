﻿using System;
using System.Collections.Generic;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.Producible;

namespace WorkflowProfiling
{
    using System.Activities;

    public class ProfilingResult
    {
        public ProfilingResult(
            TimeSpan timeToComplete,
            IEnumerable<IProducible> result,
            int numberOfItemsInResult,
            bool threwException,
            Exception exception = null)
        {
            this.TotalTimeToComplete = timeToComplete;
            this.ThrewException = threwException;
            this.ThrownException = exception;
            this.NumberOfItemsInResult = numberOfItemsInResult;
            this.Result = result;

            if (numberOfItemsInResult > 0)
            {
                this.AverageTimePerItem = TimeSpan.FromMilliseconds(this.TotalTimeToComplete.TotalMilliseconds / (double)numberOfItemsInResult);
            }
        }

        public TimeSpan TotalTimeToComplete { get; private set; }
        public TimeSpan AverageTimePerItem { get; private set; }
        public bool ThrewException { get; private set; }
        public Exception ThrownException { get; private set; }
        public int NumberOfItemsInResult { get; private set; }
        public IEnumerable<IProducible> Result { get; private set; }
    }
}
