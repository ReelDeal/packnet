﻿namespace WorkflowProfiling.Events
{
    using System;

    public class ProfilingResultEventArgs : EventArgs
    {
        public ProfilingResultEventArgs(ProfilingResult baselineResult)
        {
            this.BaselineResult = baselineResult;
        }

        public ProfilingResult BaselineResult { get; private set; }
    }
}
