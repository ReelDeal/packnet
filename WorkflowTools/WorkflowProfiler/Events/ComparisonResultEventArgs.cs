﻿using System.Collections.Generic;

namespace WorkflowProfiling.Events
{
    using System;

    public class ComparisonResultEventArgs : ProfilingResultEventArgs
    {
        public ComparisonResultEventArgs(ProfilingResult baselineResult, ProfilingResult comparisonResult, IDictionary<int, IEnumerable<PropertyDiff>> diffs)
            :base(baselineResult)
        {

            ComparisonResult = comparisonResult;
            Differences = diffs;
        }

        public ProfilingResult ComparisonResult { get; private set; }
        public IDictionary<int, IEnumerable<PropertyDiff>> Differences { get; private set; }
    }
}
