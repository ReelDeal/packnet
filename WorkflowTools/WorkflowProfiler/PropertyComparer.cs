﻿using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Common.WebServiceConsumer;

namespace WorkflowProfiling
{
    public class PropertyDiff
    {
        public string PropertyName { get; private set; }
        public dynamic OldValue { get; private set; }
        public dynamic NewValue { get; private set; }

        public PropertyDiff(string propertyName, dynamic oldValue, dynamic newValue)
        {
            PropertyName = propertyName;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }

    public static class PropertyComparer
    {
        public static Dictionary<int, IEnumerable<PropertyDiff>> Compare(IEnumerable<object> baseItems, IEnumerable<object> comparisonItems, IEnumerable<string> propertiesToIgnore)
        {
            var localBaseItems = baseItems.ToArray();
            var localComparisonItems = comparisonItems.ToArray();
            var localPropertiesToIgnore = propertiesToIgnore.ToArray();

            if (localBaseItems.Count() == localComparisonItems.Count())
            {
                return GetDifferences(localBaseItems, localComparisonItems, localPropertiesToIgnore);
            }
            throw new ArgumentException("Count of base results must be equal to count of comparison results");
        }

        private static Dictionary<int, IEnumerable<PropertyDiff>> GetDifferences(IEnumerable<object> baseItems, IEnumerable<object> comparisonItems, IEnumerable<string> propertiesToIgnore)
        {
            var diffs = new Dictionary<int, IEnumerable<PropertyDiff>>();
            
            for (int i = 0; i < baseItems.Count(); i++)
            {
                var diff = CompareObjects(baseItems.ElementAt(i), comparisonItems.ElementAt(i), propertiesToIgnore);
                if (diff.Any())
                    diffs.Add(i+1, diff);
            }

            return diffs;
        }

        private static IEnumerable<PropertyDiff> CompareObjects(object baseObject, object newObject, IEnumerable<string> propertiesToIgnore)
        {
            var diffs = new List<PropertyDiff>();
            var baseType = baseObject.GetType();
            
            foreach (var prop in baseType.GetProperties().Where(p => !propertiesToIgnore.Contains(p.Name)))
            {
                var propName = prop.Name;
                var baseValue = prop.GetValue(baseObject);
                var newValue = prop.GetValue(newObject);
                if (baseValue != null)
                {
                    if (!baseValue.Equals(newValue))
                        diffs.Add(new PropertyDiff(propName, baseValue, newValue));
                }
                else
                {
                    if (newValue != null)
                        diffs.Add(new PropertyDiff(propName, baseValue, newValue));
                }
                
            }

            return diffs;
        }
    }
}
