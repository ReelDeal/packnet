﻿using PackNet.Common.Interfaces.Enums;

namespace WorkflowProfiling
{
    using PackNet.Common.Interfaces.Importing;

    public class CustomImporter : IWorkflowImporter
    {
        public CustomImporter(ImportTypes importTypeName, string workflowPath)
        {
            this.ImportType = importTypeName;
            this.Workflow = workflowPath;
        }

        public string Name { get; private set; }

        public void Validate(string pluginDirectory)
        {
            
        }

        public ProducibleTypes ProducibleType { get; private set; }
        public ImportTypes ImportType { get; private set; }
        public string Workflow { get; private set; }
        
    }
}
