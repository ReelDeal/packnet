﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkflowProfiling
{
    public class ComparisonResult
    {
        public ProfilingResult BaseProfilingResult { get; private set; }
        public ProfilingResult ComparisonProfilingResult { get; private set; }
        public IDictionary<int, IEnumerable<PropertyDiff>> Differences { get; private set; }

        public ComparisonResult(ProfilingResult baseProfilingResult, ProfilingResult comparisonProfilingResult, IDictionary<int, IEnumerable<PropertyDiff>> differences)
        {
            BaseProfilingResult = baseProfilingResult;
            ComparisonProfilingResult = comparisonProfilingResult;
            Differences = differences;
        }
    }
}
