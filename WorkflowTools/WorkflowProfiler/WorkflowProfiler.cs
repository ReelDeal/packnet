﻿using System.Linq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Logging.NLogBackend;
using PackNet.Common.Utils;
using PackNet.Services.Importer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using WorkflowProfiling.Events;

namespace WorkflowProfiling
{
    

    public class WorkflowProfiler
    {
        private readonly ILogger logger;
        private readonly IServiceLocator serviceLocator;
        private readonly TimeSpan timeout;
        
        public WorkflowProfiler(TimeSpan timeout)
        {
            //TODO: send this in the constructor?
            var configPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "NLog.config");
            this.logger = new NLogImplementation("Profiler", configPath);
            this.timeout = timeout;
        }

        public ProfilingResult ProfileWorkflow(string workflowPath, Importable importables)
        {
            ProfilingResult baselineResult = null;
            EventHandler<ProfilingResultEventArgs> baselineProfilingCompleted = null;
            baselineProfilingCompleted += (sender, args) => baselineResult = args.BaselineResult;

            this.ProfileWorkflow(workflowPath, importables, baselineProfilingCompleted);

            Retry.For(() => baselineResult != null, timeout);

            if (baselineResult == null)
                throw new TimeoutException("Timeout expired");

            return baselineResult;
        }

        public ComparisonResult CompareWorkflows(string baselineWorkflow, string comparisonWorkflow, Importable importables, IEnumerable<string> propertiesToIgnore)
        {
            ProfilingResult baselineResult = null;
            ProfilingResult comparisonResult = null;
            EventHandler<ProfilingResultEventArgs> baselineProfilingCompleted = null;
            baselineProfilingCompleted += (sender, args) => baselineResult = args.BaselineResult;
            EventHandler<ProfilingResultEventArgs> comparisonProfilingCompleted = null;
            comparisonProfilingCompleted += (sender, args) => comparisonResult = args.BaselineResult;

            this.ProfileWorkflow(baselineWorkflow, importables, baselineProfilingCompleted);
            this.ProfileWorkflow(comparisonWorkflow, importables, comparisonProfilingCompleted);

            Retry.For(() => baselineResult != null && comparisonResult != null, TimeSpan.FromMilliseconds(timeout.TotalMilliseconds * 2));

            if(baselineResult == null || comparisonResult == null)
                throw new TimeoutException("Timeout expired");

            if (baselineResult.Result.Count() != comparisonResult.Result.Count())
                return new ComparisonResult(null, null, null);

            return new ComparisonResult(baselineResult, comparisonResult,
                PropertyComparer.Compare(baselineResult.Result.Select(cmd => cmd), comparisonResult.Result.Select(cmd => cmd), propertiesToIgnore));
        }

        private void ProfileWorkflow(string workflowPath, Importable importables, EventHandler<ProfilingResultEventArgs> handler)
        {
            var sw = new Stopwatch();
            var importer = new CustomImporter(importables.ImportType, workflowPath);
            var publisher = new EventAggregator();
            
            IEnumerable<IProducible> result = null;
            publisher
                .GetEvent<IEnumerable<IProducible>>()
                .Subscribe(
                    items =>
                    {
                        result = items;
                        sw.Stop();
                        this.FireProfilingCompletedEvent(new ProfilingResult(TimeSpan.FromMilliseconds(sw.ElapsedMilliseconds), result, result.Count(), false), handler);
                    });

            var workflowDispatcher = new ImporterWorkflowDispatcher(string.Empty, new[] { importer }, publisher, serviceLocator, logger);

            workflowDispatcher.StatusObservable.Subscribe(
                _ => { },
                error =>
                {
                    sw.Stop();
                    this.FireProfilingCompletedEvent(new ProfilingResult(TimeSpan.FromMilliseconds(sw.ElapsedMilliseconds), new List<IProducible>(), 0, true, error), handler);
                });
            sw.Start();
            workflowDispatcher.Dispatch(importables);
        }

        private void FireProfilingCompletedEvent(ProfilingResult baseline, EventHandler<ProfilingResultEventArgs> profilingResultEventHandler)
        {
            var handler = profilingResultEventHandler;
            if (handler != null)
                handler.BeginInvoke(this, new ProfilingResultEventArgs(baseline), null, null);
        }
    }
}