﻿//-----------------------------------------------------------------------
// <copyright file="BooleanToErrorTypeConverter.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Windows.Data;

using PackNet.Workflow.Studio.Properties;

namespace PackNet.Workflow.Studio.Converters
{
    public class BooleanToErrorTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var isWarning = value as bool?;
            return isWarning.HasValue && isWarning.Value ? Resources.WarningValidationItem : Resources.ErrorValidationItem;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
