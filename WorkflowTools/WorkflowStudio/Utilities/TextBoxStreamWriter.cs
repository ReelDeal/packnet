﻿//-----------------------------------------------------------------------
// <copyright file="TextBoxStreamWriter.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Controls;

namespace PackNet.Workflow.Studio.Utilities
{
    public class TextBoxStreamWriter : TextWriter
    {
        private readonly TextBox output;
        private readonly TraceSource traceSource;
        private readonly TraceSource allTraceSource;
        private readonly string workflowName;

        public TextBoxStreamWriter(TextBox output, string workflowName)
        {
            this.output = output;
            this.workflowName = workflowName;
            traceSource = new TraceSource(workflowName + "Output", SourceLevels.Verbose);
            allTraceSource = new TraceSource("AllOutput", SourceLevels.Verbose);
        }

        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }

        public override void WriteLine(string value)
        {
            if (value == null)
            {
                return;
            }

            base.WriteLine(value);
            traceSource.TraceData(TraceEventType.Verbose, 0, value);
            allTraceSource.TraceData(TraceEventType.Verbose, 0, workflowName, value);
        }

        public override void Write(char value)
        {
            base.Write(value);
            output.Dispatcher.BeginInvoke(new Action(() => output.AppendText(value.ToString(CultureInfo.InvariantCulture))));
        }
    }
}