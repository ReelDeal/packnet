﻿//-----------------------------------------------------------------------
// <copyright file="ExceptionHelper.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Text;

namespace PackNet.Workflow.Studio.Utilities
{
    public class ExceptionHelper
    {
        public static string FormatStackTrace(Exception exception)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append(exception.Message);
            stringBuilder.Append(exception.StackTrace);

            return exception.InnerException != null
                ? stringBuilder.Append(FormatStackTrace(exception.InnerException)).ToString()
                : stringBuilder.ToString();
        }
    }
}
