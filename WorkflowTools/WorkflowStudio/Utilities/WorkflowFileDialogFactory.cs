﻿//-----------------------------------------------------------------------
// <copyright file="WorkflowFileDialogFactory.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using Microsoft.Win32;

namespace PackNet.Workflow.Studio.Utilities
{
    public class WorkflowFileDialogFactory
    {
        public static SaveFileDialog CreateSaveFileDialog(string defaultFilename)
        {
            var fileDialog = new SaveFileDialog
            {
                DefaultExt = "xaml",
                FileName = defaultFilename,
                Filter = "xaml files (*.xaml,*.xamlx)|*.xaml;*.xamlx;|All files (*.*)|*.*"
            };
            return fileDialog;
        }

        public static OpenFileDialog CreateOpenFileDialog()
        {
            var fileDialog = new OpenFileDialog
            {
                DefaultExt = "xaml",
                Filter = "xaml files (*.xaml,*.xamlx)|*.xaml;*.xamlx;|All files (*.*)|*.*"
            };
            return fileDialog;
        }
    }
}