﻿//-----------------------------------------------------------------------
// <copyright file="WorkflowDebugger.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.XamlIntegration;
using System.IO;
using System.Text;

using PackNet.Workflow.Studio.Properties;
using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.ViewModel;

namespace PackNet.Workflow.Studio.Execution
{
    public class WorkflowDebugger : WorkflowDebuggerBase
    {
        private WorkflowApplication workflowApplication;

        public WorkflowDebugger(TextWriter output, string workflowName, WorkflowDesigner workflowDesigner, bool disableDebugViewOutput)
            : base(output, workflowName, workflowDesigner, disableDebugViewOutput)
        {
        }

        public override void Abort()
        {
            if (!Running || workflowApplication == null)
            {
                return;
            }

            StatusViewModel.SetStatusText(Resources.AbortingInDebugStatus, WorkflowName);
            workflowApplication.Abort();
        }

        protected override void LoadAndExecute()
        {
            var memoryStream = new MemoryStream(Encoding.Default.GetBytes(WorkflowDesigner.Text));
            var workflowToRun = ActivityXamlServices.Load(memoryStream) as DynamicActivity;

            if (workflowToRun == null)
            {
                return;
            }

            WorkflowInspectionServices.CacheMetadata(workflowToRun);

            workflowApplication = new WorkflowApplication(workflowToRun);

            workflowApplication.Extensions.Add(Output);

            workflowApplication.Completed = WorkflowCompleted;
            workflowApplication.OnUnhandledException = WorkflowUnhandledException;
            workflowApplication.Aborted = WorkflowAborted;

            workflowApplication.Extensions.Add(InitialiseVisualTrackingParticipant(workflowToRun));

            try
            {
                Running = true;
                workflowApplication.Run();
            }
            catch (Exception e)
            {
                Output.WriteLine(ExceptionHelper.FormatStackTrace(e));
                StatusViewModel.SetStatusText(Resources.ExceptionInDebugStatus, WorkflowName);
            }
        }

        protected override Activity GetRootRuntimeWorkflowElement(Activity root)
        {
            WorkflowInspectionServices.CacheMetadata(root);

            var enumerator = WorkflowInspectionServices.GetActivities(root).GetEnumerator();

            // Get the first child
            enumerator.MoveNext();
            root = enumerator.Current;
            return root;
        }

        private void WorkflowCompleted(WorkflowApplicationCompletedEventArgs e)
        {
            Running = false;
            StatusViewModel.SetStatusText(string.Format(Resources.CompletedInDebugStatus, e.CompletionState), WorkflowName);
        }

        private void WorkflowAborted(WorkflowApplicationAbortedEventArgs e)
        {
            Running = false;
            StatusViewModel.SetStatusText(Resources.AbortedInDebugStatus, WorkflowName);
        }

        private static UnhandledExceptionAction WorkflowUnhandledException(WorkflowApplicationUnhandledExceptionEventArgs e)
        {
            Console.WriteLine(ExceptionHelper.FormatStackTrace(e.UnhandledException));
            return UnhandledExceptionAction.Terminate;
        }
    }
}
