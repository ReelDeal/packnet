﻿//-----------------------------------------------------------------------
// <copyright file="WorkflowDebuggerBase.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Debugger;
using System.Activities.Presentation;
using System.Activities.Presentation.Debug;
using System.Activities.Presentation.Services;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

using PackNet.Workflow.Studio.Properties;
using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.ViewModel;

namespace PackNet.Workflow.Studio.Execution
{
    public abstract class WorkflowDebuggerBase : IWorkflowDebugger
    {
        protected TextWriter Output;
        protected IDesignerDebugView DebuggerService;
        protected WorkflowDesigner WorkflowDesigner;
        protected IList<SourceLocationDebugItem> SourceLocationDebugItems;
        protected Dictionary<int, SourceLocation> TextLineToSourceLocationMap;
        protected bool Running;
        protected int SourceLocationSteppedCount;
        protected int PauseBetweenDebugStepsInMilliseconds = 1000;
        protected bool DisableDebugViewOutput = false;
        protected DataGrid DebugView;
        protected string WorkflowName;
        protected TraceSource DebugTraceSource;
        protected TraceSource AllDebugTraceSource;

        protected WorkflowDebuggerBase(TextWriter output, string workflowName, WorkflowDesigner workflowDesigner, bool disableDebugViewOutput)
        {
            Output = output;
            WorkflowDesigner = workflowDesigner;
            WorkflowName = workflowName;

            DebugView = new DataGrid { IsReadOnly = true, SelectionMode = DataGridSelectionMode.Single };

            DebugView.SelectionChanged += (sender, args) => HighlightActivity(DebugView.SelectedIndex);

            DisableDebugViewOutput = disableDebugViewOutput;
            DebugTraceSource = new TraceSource(Path.GetFileNameWithoutExtension(workflowName) + "Debug");
            AllDebugTraceSource = new TraceSource("AllDebug");

            ConfigurationManager.RefreshSection("appSettings");

            var pauseBetweenDebugStepsInMillisecondsValue = ConfigurationManager.AppSettings["PauseBetweenDebugStepsInMilliseconds"];
            if (!string.IsNullOrEmpty(pauseBetweenDebugStepsInMillisecondsValue))
            {
                PauseBetweenDebugStepsInMilliseconds = int.Parse(pauseBetweenDebugStepsInMillisecondsValue);
            }
        }

        public IList<SourceLocationDebugItem> SourceLocations
        {
            get
            {
                return SourceLocationDebugItems;
            }
        }

        public bool IsRunning
        {
            get
            {
                return Running;
            }
        }

        public UIElement GetDebugView()
        {
            return DebugView;
        }

        public abstract void Abort();

        public void Run()
        {
            WorkflowDesigner.Flush();

            DebuggerService = WorkflowDesigner.DebugManagerView;

            StatusViewModel.SetStatusText(Resources.DebuggingStatus, WorkflowName);

            SourceLocationDebugItems = new List<SourceLocationDebugItem>();
            TextLineToSourceLocationMap = new Dictionary<int, SourceLocation>();
            SourceLocationSteppedCount = 0;

            try
            {
                LoadAndExecute();
            }
            catch (Exception e)
            {
                Output.WriteLine(ExceptionHelper.FormatStackTrace(e));
                StatusViewModel.SetStatusText(Resources.ExceptionInDebugStatus, WorkflowName);
                Running = false;
            }
        }

        public void HighlightActivity(int selectedRowNumber)
        {
            DispatcherService.Dispatch(() =>
            {
                try
                {
                    if (selectedRowNumber >= 0 && selectedRowNumber < TextLineToSourceLocationMap.Count)
                    {
                        WorkflowDesigner.DebugManagerView.CurrentLocation = TextLineToSourceLocationMap[selectedRowNumber];
                    }
                }
                catch (Exception)
                {
                    // If the user clicks other than on the tracking records themselves.
                    WorkflowDesigner.DebugManagerView.CurrentLocation = new SourceLocation("Workflow.xaml", 1, 1, 1, 10);
                }
            });
        }

        protected VisualTrackingParticipant InitialiseVisualTrackingParticipant(Activity workflowToRun)
        {
            // Mapping between the object and Line No.
            var elementToSourceLocationMap = UpdateSourceLocationMappingInDebuggerService(workflowToRun);

            // Mapping between the object and the Instance Id
            var activityIdToElementMap = BuildActivityIdToElementMap(elementToSourceLocationMap);

            // Setup custom tracking
            const string all = "*";
            var simTracker = new VisualTrackingParticipant
            {
                TrackingProfile = new TrackingProfile
                {
                    Name = "CustomTrackingProfile",
                    Queries =
                    {
                        new CustomTrackingQuery
                        {
                            Name = all,
                            ActivityName = all
                        },
                        new WorkflowInstanceQuery
                        {
                            // Limit workflow instance tracking records for started and completed workflow states
                            States = { WorkflowInstanceStates.Started, WorkflowInstanceStates.Completed },
                        },
                        new ActivityStateQuery
                        {
                            // Subscribe to track records from all activities for all states
                            ActivityName = all,
                            States = { all },

                            // Extract workflow variables and arguments as a part of the activity tracking record
                            // VariableName = "*" allows for extraction of all variables in the scope
                            // of the activity
                            Variables =
                            {
                                all
                            }
                        }
                    }
                },
                ActivityIdToWorkflowElementMap = activityIdToElementMap
            };

            // As the tracking events are received
            simTracker.TrackingRecordReceived += (trackingParticpant, trackingEventArgs) =>
            {
                if (trackingEventArgs.Activity == null)
                {
                    return;
                }

                ShowDebug(elementToSourceLocationMap[trackingEventArgs.Activity]);

                Thread.Sleep(PauseBetweenDebugStepsInMilliseconds);

                var debugItem = new SourceLocationDebugItem
                {
                    ActivityName = trackingEventArgs.Activity.DisplayName,
                    Id = trackingEventArgs.Activity.Id,
                    State = ((ActivityStateRecord)trackingEventArgs.Record).State,
                    StepCount = SourceLocationSteppedCount,
                    InstanceId = trackingEventArgs.Record.InstanceId
                };

                DebugTraceSource.TraceData(
                    TraceEventType.Information,
                    0,
                    trackingEventArgs.Activity.DisplayName,
                    trackingEventArgs.Activity.Id,
                    ((ActivityStateRecord)trackingEventArgs.Record).State,
                    SourceLocationSteppedCount,
                    trackingEventArgs.Record.InstanceId);

                AllDebugTraceSource.TraceData(
                    TraceEventType.Information,
                    0,
                    Path.GetFileNameWithoutExtension(WorkflowName),
                    trackingEventArgs.Activity.DisplayName,
                    trackingEventArgs.Activity.Id,
                    ((ActivityStateRecord)trackingEventArgs.Record).State,
                    SourceLocationSteppedCount,
                    trackingEventArgs.Record.InstanceId);

                SourceLocationSteppedCount++;

                if (DisableDebugViewOutput)
                {
                    return;
                }

                TextLineToSourceLocationMap.Add(SourceLocationDebugItems.Count, elementToSourceLocationMap[trackingEventArgs.Activity]);
                SourceLocationDebugItems.Add(debugItem);

                DispatcherService.Dispatch(() =>
                {
                    DebugView.ItemsSource = SourceLocations;
                    DebugView.Items.Refresh();
                });
            };

            return simTracker;
        }

        protected abstract void LoadAndExecute();

        protected abstract Activity GetRootRuntimeWorkflowElement(Activity root);

        private void ShowDebug(SourceLocation srcLoc)
        {
            DispatcherService.Dispatch(() =>
            {
                WorkflowDesigner.DebugManagerView.CurrentLocation = srcLoc;
            });
        }

        private static Dictionary<string, Activity> BuildActivityIdToElementMap(Dictionary<object, SourceLocation> elementToSourceLocationMap)
        {
            var map = new Dictionary<string, Activity>();

            foreach (var instance in elementToSourceLocationMap.Keys)
            {
                var workflowElement = instance as Activity;
                if (workflowElement == null || workflowElement.Id == null)
                {
                    continue;
                }

                map.Add(workflowElement.Id, workflowElement);
            }

            return map;
        }

        private Dictionary<object, SourceLocation> UpdateSourceLocationMappingInDebuggerService(Activity root)
        {
            var rootInstance = GetRootInstance();
            var sourceLocationMapping = new Dictionary<object, SourceLocation>();
            var designerSourceLocationMapping = new Dictionary<object, SourceLocation>();

            if (rootInstance != null)
            {
                var documentRootElement = GetRootWorkflowElement(rootInstance);
                SourceLocationProvider.CollectMapping(
                    GetRootRuntimeWorkflowElement(root), 
                    documentRootElement, 
                    sourceLocationMapping,
                    WorkflowDesigner.Context.Items.GetValue<WorkflowFileItem>().LoadedFile);
                SourceLocationProvider.CollectMapping(
                   documentRootElement,
                   documentRootElement,
                   designerSourceLocationMapping,
                   WorkflowDesigner.Context.Items.GetValue<WorkflowFileItem>().LoadedFile);
            }

            // Notify the DebuggerService of the new sourceLocationMapping.
            // When rootInstance == null, it'll just reset the mapping.
            if (DebuggerService != null)
            {
                ((DebuggerService)DebuggerService).UpdateSourceLocations(designerSourceLocationMapping);
            }

            return sourceLocationMapping;
        }

        private object GetRootInstance()
        {
            var modelService = WorkflowDesigner.Context.Services.GetService<ModelService>();
            return modelService != null ? modelService.Root.GetCurrentValue() : null;
        }

        private static Activity GetRootWorkflowElement(object rootModelObject)
        {
            // Get root WorkflowElement.  Currently only handle when the object is ActivitySchemaType or WorkflowElement.
            // May return null if it does not know how to get the root activity.
            Debug.Assert(rootModelObject != null, "Cannot pass null as rootModelObject");

            Activity rootWorkflowElement;
            var debuggableWorkflowTree = rootModelObject as IDebuggableWorkflowTree;
            if (debuggableWorkflowTree != null)
            {
                rootWorkflowElement = debuggableWorkflowTree.GetWorkflowRoot();
            }
            else 
            {
                // Loose xaml case.
                rootWorkflowElement = rootModelObject as Activity;
            }

            return rootWorkflowElement;
        }
    }
}
