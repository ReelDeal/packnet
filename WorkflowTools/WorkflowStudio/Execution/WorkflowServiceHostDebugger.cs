﻿//-----------------------------------------------------------------------
// <copyright file="WorkflowServiceHostDebugger.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Presentation;
using System.IO;
using System.ServiceModel.Activities;
using System.Text;
using System.Xaml;

using PackNet.Workflow.Studio.Properties;
using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.ViewModel;

namespace PackNet.Workflow.Studio.Execution
{
    public class WorkflowServiceHostDebugger : WorkflowDebuggerBase
    {
        private WorkflowServiceHost workflowServiceHost;

        public WorkflowServiceHostDebugger(TextWriter output, string workflowName, WorkflowDesigner workflowDesigner, bool disableDebugViewOutput)
            : base(output, workflowName, workflowDesigner, disableDebugViewOutput)
        {
        }

        public override void Abort()
        {
            if (!Running || workflowServiceHost == null)
            {
                return;
            }

            StatusViewModel.SetStatusText(Resources.AbortingInDebugStatus, WorkflowName);
            workflowServiceHost.Abort();
        }

        protected override void LoadAndExecute()
        {
            var memoryStream = new MemoryStream(Encoding.Default.GetBytes(WorkflowDesigner.Text));
            var workflowToRun = XamlServices.Load(memoryStream) as WorkflowService;

            if (workflowToRun == null)
            {
                return;
            }

            workflowServiceHost = new WorkflowServiceHost(workflowToRun);
            workflowServiceHost.WorkflowExtensions.Add(Output);
            workflowServiceHost.WorkflowExtensions.Add(InitialiseVisualTrackingParticipant(workflowToRun.GetWorkflowRoot()));
            AddHandlers();

            try
            {
                Running = true;
                workflowServiceHost.Open();
            }
            catch (Exception e)
            {
                Output.WriteLine(ExceptionHelper.FormatStackTrace(e));
                StatusViewModel.SetStatusText(Resources.ExceptionInDebugStatus, WorkflowName);
            }
        }

        protected override Activity GetRootRuntimeWorkflowElement(Activity root)
        {
            WorkflowInspectionServices.CacheMetadata(root);
            return root;
        }

        private void WorkflowServiceHostClosed(object sender, EventArgs e)
        {
            Running = false;
            RemoverHandlers();
            StatusViewModel.SetStatusText(Resources.ClosedServiceHostInDebugStatus, WorkflowName);
        }

        private void WorkflowServiceHostOpened(object sender, EventArgs e)
        {
            StatusViewModel.SetStatusText(Resources.OpenServiceHostInDebugStatus, WorkflowName);
        }

        private void WorkflowServiceHostFaulted(object sender, EventArgs e)
        {
            Running = false;
            RemoverHandlers();
            StatusViewModel.SetStatusText(Resources.FaultedServiceHostInDebugStatus, WorkflowName);
        }

        private void AddHandlers()
        {
            workflowServiceHost.Closed += WorkflowServiceHostClosed;
            workflowServiceHost.Opened += WorkflowServiceHostOpened;
            workflowServiceHost.Faulted += WorkflowServiceHostFaulted;
        }

        private void RemoverHandlers()
        {
            workflowServiceHost.Closed += WorkflowServiceHostClosed;
            workflowServiceHost.Opened += WorkflowServiceHostOpened;
            workflowServiceHost.Faulted += WorkflowServiceHostFaulted;
        }
    }
}
