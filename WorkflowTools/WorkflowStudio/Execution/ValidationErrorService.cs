﻿//-----------------------------------------------------------------------
// <copyright file="ValidationErrorService.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities.Presentation.Validation;
using System.Collections.Generic;

namespace PackNet.Workflow.Studio.Execution
{
    public class ValidationErrorService : IValidationErrorService
    {
        private readonly IList<ValidationErrorInfo> errorList;

        public ValidationErrorService(IList<ValidationErrorInfo> errorList)
        {
            this.errorList = errorList;
        }

        public delegate void ErrorsChangedHandler(object sender, EventArgs e);

        public event ErrorsChangedHandler ErrorsChangedEvent;

        public void ShowValidationErrors(IList<ValidationErrorInfo> errors)
        {
            errorList.Clear();

            foreach (var error in errors)
            {
                errorList.Add(error);
            }

            if (ErrorsChangedEvent != null)
            {
                ErrorsChangedEvent(this, null);
            }
        }
    }
}