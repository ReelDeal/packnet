﻿//-----------------------------------------------------------------------
// <copyright file="WorkflowServiceHostRunner.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities.Presentation;
using System.IO;
using System.ServiceModel.Activities;
using System.Text;
using System.Xaml;

using PackNet.Workflow.Studio.Properties;
using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.ViewModel;

namespace PackNet.Workflow.Studio.Execution
{
    public class WorkflowServiceHostRunner : IWorkflowRunner
    {
        private readonly TextWriter output;
        private WorkflowServiceHost workflowServiceHost;
        private readonly WorkflowDesigner workflowDesigner;
        private readonly string workflowName;

        public WorkflowServiceHostRunner(TextWriter output, string workflowName, WorkflowDesigner workflowDesigner)
        {
            this.output = output;
            this.workflowName = workflowName;
            this.workflowDesigner = workflowDesigner;
        }

        public bool IsRunning { get; private set; }

        public void Abort()
        {
            if (!IsRunning || workflowServiceHost == null)
            {
                return;
            }

            StatusViewModel.SetStatusText(Resources.AbortingServiceHostStatus, workflowName);
            workflowServiceHost.Abort();
        }

        public void Run()
        {
            workflowDesigner.Flush();
            var memoryStream = new MemoryStream(Encoding.Default.GetBytes(workflowDesigner.Text));
            var workflowToRun = XamlServices.Load(memoryStream) as WorkflowService;

            workflowServiceHost = new WorkflowServiceHost(workflowToRun);

            workflowServiceHost.WorkflowExtensions.Add(output);
            try
            {
                AddHandlers();
                IsRunning = true;
                workflowServiceHost.Open();
            }
            catch (Exception e)
            {
                output.WriteLine(ExceptionHelper.FormatStackTrace(e));
                StatusViewModel.SetStatusText(Resources.ExceptionServiceHostStatus, workflowName);
                IsRunning = false;
            }
        }

        private void WorkflowServiceHostClosed(object sender, EventArgs e)
        {
            IsRunning = false;
            RemoverHandlers();
            StatusViewModel.SetStatusText(Resources.ClosedServiceHostStatus, workflowName);
        }

        private void WorkflowServiceHostOpened(object sender, EventArgs e)
        {
            StatusViewModel.SetStatusText(Resources.OpenServiceHostStatus, workflowName);
        }

        private void WorkflowServiceHostFaulted(object sender, EventArgs e)
        {
            IsRunning = false;
            RemoverHandlers();
            StatusViewModel.SetStatusText(Resources.FaultedServiceHostStatus, workflowName);
        }

        private void AddHandlers()
        {
            workflowServiceHost.Closed += WorkflowServiceHostClosed;
            workflowServiceHost.Opened += WorkflowServiceHostOpened;
            workflowServiceHost.Faulted += WorkflowServiceHostFaulted;
        }

        private void RemoverHandlers()
        {
            workflowServiceHost.Closed += WorkflowServiceHostClosed;
            workflowServiceHost.Opened += WorkflowServiceHostOpened;
            workflowServiceHost.Faulted += WorkflowServiceHostFaulted;
        }
    }
}
