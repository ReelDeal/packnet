﻿//-----------------------------------------------------------------------
// <copyright file="VisualTrackingParticipant.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Tracking;
using System.Collections.Generic;

namespace PackNet.Workflow.Studio.Execution
{
    public class VisualTrackingParticipant : TrackingParticipant
    {
        public event EventHandler<TrackingEventArgs> TrackingRecordReceived;

        public Dictionary<string, Activity> ActivityIdToWorkflowElementMap { get; set; }

        protected override void Track(TrackingRecord record, TimeSpan timeout)
        {
            OnTrackingRecordReceived(record, timeout);
        }

        protected void OnTrackingRecordReceived(TrackingRecord record, TimeSpan timeout)
        {
            if (TrackingRecordReceived == null)
            {
                return;
            }

            var activityStateRecord = record as ActivityStateRecord;

            if ((activityStateRecord != null) && (!activityStateRecord.Activity.TypeName.Contains("System.Activities.Expressions")))
            {
                if (ActivityIdToWorkflowElementMap.ContainsKey(activityStateRecord.Activity.Id))
                {
                    TrackingRecordReceived(
                        this,
                        new TrackingEventArgs(
                            record,
                            timeout,
                            ActivityIdToWorkflowElementMap[activityStateRecord.Activity.Id]));
                }
            }
            else
            {
                TrackingRecordReceived(this, new TrackingEventArgs(record, timeout, null));
            }
        }
    }
}
