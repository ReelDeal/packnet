﻿//-----------------------------------------------------------------------
// <copyright file="WorkflowRunner.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Presentation;
using System.Activities.XamlIntegration;
using System.IO;
using System.Text;

using PackNet.Workflow.Studio.Properties;
using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.ViewModel;

namespace PackNet.Workflow.Studio.Execution
{
    public class WorkflowRunner : IWorkflowRunner
    {
        private readonly TextWriter output;
        private WorkflowApplication workflowApplication;
        private readonly WorkflowDesigner workflowDesigner;

        private readonly string workflowName;

        public WorkflowRunner(TextWriter output, string workflowName, WorkflowDesigner workflowDesigner)
        {
            this.output = output;
            this.workflowName = workflowName;
            this.workflowDesigner = workflowDesigner;
        }

        public bool IsRunning { get; private set; }

        public void Abort()
        {
            if (!IsRunning || workflowApplication == null)
            {
                return;
            }

            StatusViewModel.SetStatusText(Resources.AbortingStatus, workflowName);
            workflowApplication.Abort();
        }

        public void Run()
        {
            workflowDesigner.Flush();
            var memoryStream = new MemoryStream(Encoding.Default.GetBytes(workflowDesigner.Text));
            var activityToRun = ActivityXamlServices.Load(memoryStream) as DynamicActivity;

            if (activityToRun == null)
            {
                return;
            }

            workflowApplication = new WorkflowApplication(activityToRun);

            workflowApplication.Extensions.Add(output);
            workflowApplication.Completed = WorkflowCompleted;
            workflowApplication.Aborted = WorkflowAborted;
            workflowApplication.OnUnhandledException = WorkflowUnhandledException;
            StatusViewModel.SetStatusText(Resources.RunningStatus, workflowName);

            try
            {
                IsRunning = true;
                workflowApplication.Run();
            }
            catch (Exception e)
            {
                output.WriteLine(ExceptionHelper.FormatStackTrace(e));
                StatusViewModel.SetStatusText(Resources.ExceptionStatus, workflowName);
                IsRunning = false;
            }
        }

        private void WorkflowCompleted(WorkflowApplicationCompletedEventArgs e)
        {
            IsRunning = false;
            StatusViewModel.SetStatusText(string.Format(Resources.CompletedStatus, e.CompletionState), workflowName);
        }

        private void WorkflowAborted(WorkflowApplicationAbortedEventArgs e)
        {
            IsRunning = false;
            StatusViewModel.SetStatusText(Resources.AbortedStatus, workflowName);
        }

        private static UnhandledExceptionAction WorkflowUnhandledException(WorkflowApplicationUnhandledExceptionEventArgs e)
        {
            Console.WriteLine(ExceptionHelper.FormatStackTrace(e.UnhandledException));
            return UnhandledExceptionAction.Terminate;
        }
    }
}
