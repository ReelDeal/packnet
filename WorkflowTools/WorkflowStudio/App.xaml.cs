﻿//-----------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging.NLogBackend;
using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.Views;

using Resx = PackNet.Workflow.Studio.Properties;

namespace PackNet.Workflow.Studio
{
    public partial class App
    {
        private readonly ILogger logger;
        private readonly TraceSource errorSource;

        public App()
        {
            logger = new NLogImplementation("WorkflowStudio", ".\\NLog.config");
            FrameworkElement.LanguageProperty.OverrideMetadata(
              typeof(FrameworkElement),
              new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            Dispatcher.UnhandledException += Dispatcher_UnhandledException;

            errorSource = new TraceSource("ErrorTraceSource", SourceLevels.Error);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            var window = new MainWindow();
            window.Show();
        }

        private void Dispatcher_UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var errorMessage = string.Format(Resx.Resources.UnhandledExceptionMessage, e.Exception.Message);
            logger.Log(LogLevel.Fatal, errorMessage + "\r\n" + e.Exception.InnerException.Message);
            //MessageBox.Show("Encountered an unhandled exception. See the log for more info.", Resx.Resources.DialogCaptionError, MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
            errorSource.TraceData(TraceEventType.Error, 0,
                string.Format(Resx.Resources.UnhandledExceptionTrace, DateTime.UtcNow,
                    ExceptionHelper.FormatStackTrace(e.Exception)));
            Current.Shutdown();
        }
    }
}