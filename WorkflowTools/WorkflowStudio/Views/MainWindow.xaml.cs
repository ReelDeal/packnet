﻿//-----------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.ViewModel;

namespace PackNet.Workflow.Studio.Views
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            var viewModel = new MainWindowViewModel(DockManager, HorizontalResizingPanel, VerticalResizingPanel, TabsPane);
            DataContext = viewModel;
            Status.DataContext = StatusViewModel.GetInstance;
            DispatcherService.DispatchObject = Dispatcher;
            Closing += viewModel.Closing;
        }
    }
}