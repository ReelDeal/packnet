﻿

using System.Windows.Documents;
using PackNet.Common.WebServiceConsumer;
using PackNet.Workflow.Studio.ViewModel;

namespace PackNet.Workflow.Studio.UserControls
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Controls;

    using Microsoft.Win32;

    using PackNet.Common.FileHandling.DynamicImporter;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Logging;
    using PackNet.Common.Logging.NLogBackend;

    using ReactiveUI;

    using WorkflowProfiling;

    /// <summary>
    /// Interaction logic for ImporterProfiler.xaml
    /// </summary>
    public partial class ImporterProfiler : UserControl
    {


        public ImporterProfiler()
        {
            this.DataContext = new ImportProfilerViewModel();
            InitializeComponent();
        }
    }

    
}
