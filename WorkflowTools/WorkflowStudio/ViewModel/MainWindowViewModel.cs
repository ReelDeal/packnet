﻿//-----------------------------------------------------------------------
// <copyright file="MainWindowViewModel.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// Third Party Code: This file is based on or incorporates material from the projects listed below (collectively, “Third Party Code”).
// Microsoft is not the original author of the Third Party Code. The original copyright notice, as well as the license under which 
// Microsoft received such Third Party Code, are set forth below. Such licenses and notices are provided for informational purposes only.
// Microsoft, not the third party, licenses the Third Party Code to you under the terms set forth in the EULA for AvalonDock.
// Unless applicable law gives you more rights, Microsoft reserves all other rights not expressly granted under this agreement,
// whether by implication, estoppel or otherwise.  
//
// AvalonDock project, available at http://avalondock.codeplex.com. Copyright (c) 2007-2009, Adolfo Marinucci. All rights reserved.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
// IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
// OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
// OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities;
using System.Activities.Core.Presentation.Factories;
using System.Activities.Presentation;
using System.Activities.Presentation.Services;
using System.Activities.Presentation.Toolbox;
using System.Activities.Statements;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Activities;
using System.ServiceModel.Activities.Presentation.Factories;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

using AvalonDock;

using PackNet.Workflow.Studio.Properties;
using PackNet.Workflow.Studio.Utilities;

namespace PackNet.Workflow.Studio.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        private static readonly List<string> namespacesToIgnore = new List<string>
            {
                "Microsoft.VisualBasic.Activities",
                "System.Activities.Expressions",
                "System.Activities.Statements",
                "System.ServiceModel.Activities",
                "System.ServiceModel.Activities.Presentation.Factories",
                "System.Activities.Presentation"
            };

        private readonly DockingManager dockingManager;
        private ICommand openWorkflowCommand;
        private ICommand newWorkflowCommand;
        private ICommand newServiceCommand;
        private ICommand closeWorkflowCommand;
        private ICommand closeAllWorkflowsCommand;
        private ICommand saveWorkflowCommand;
        private ICommand saveAsWorkflowCommand;
        private ICommand startWithoutDebuggingCommand;
        private ICommand startWithDebuggingCommand;
        private ICommand viewToolboxCommand;
        private ICommand viewPropertyInspectorCommand;
        private ICommand viewOutputCommand;
        private ICommand viewErrorsCommand;
        private ICommand viewDebugCommand;
        private ICommand viewOutlineCommand;
        private ICommand abortCommand;
        private ICommand exitCommand;
        private ICommand saveAllWorkflowsCommand;
        private ICommand aboutCommand;
        private ICommand undoCommand;
        private ICommand redoCommand;
        private readonly ResizingPanel horizontalResizingPanel;
        private readonly ResizingPanel verticalResizingPanel;
        private readonly DockablePane tabsPane;
        private readonly IDictionary<ContentTypes, DockableContent> dockableContents;
        private readonly ToolboxControl toolboxControl;
        private IDictionary<ToolboxCategory, IList<string>> loadedToolboxActivities;
        private IDictionary<string, ToolboxCategory> toolboxCategoryMap;
        private readonly bool disableDebugViewOutput;

        public MainWindowViewModel(DockingManager dockingManager, ResizingPanel horizontalResizingPanel, ResizingPanel verticalResizingPanel, DockablePane tabsPane)
        {
            this.dockingManager = dockingManager;

            dockingManager.ActiveDocumentChanged += (sender, args) => UpdateViews();

            toolboxControl = new ToolboxControl();
            InitialiseToolbox();

            this.horizontalResizingPanel = horizontalResizingPanel;
            this.verticalResizingPanel = verticalResizingPanel;

            this.tabsPane = tabsPane;

            dockableContents = new Dictionary<ContentTypes, DockableContent>();
            ViewToolbox();

            var disableDebugViewOutputValue = ConfigurationManager.AppSettings["DisableDebugViewOutput"];
            if (!string.IsNullOrEmpty(disableDebugViewOutputValue))
            {
                disableDebugViewOutput = bool.Parse(disableDebugViewOutputValue);
            }

            AddPackNetActivities();
        }

        #region Presentation Properties

        public bool HasValidationErrors
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                return model != null && model.HasValidationErrors;
            }
        }

        public UIElement ValidationErrorsView
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                return model == null ? null : model.ValidationErrorsView;
            }
        }

        public UIElement DebugView
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                return model == null ? null : model.DebugView;
            }
        }

        public UIElement OutlineView
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                return model == null ? null : model.OutlineView;
            }
        }

        public UIElement OutputView
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                if (model == null)
                {
                    return null;
                }

                Console.SetOut(model.Output);
                return model.OutputView;
            }
        }

        public UIElement ToolboxView
        {
            get
            {
                return toolboxControl;
            }
        }

        public UIElement PropertyInspectorView
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                return model == null ? null : model.Designer.PropertyInspectorView;
            }
        }

        #endregion

        #region Commands

        public ICommand OpenWorkflowCommand
        {
            get
            {
                return openWorkflowCommand ?? (openWorkflowCommand = new RelayCommand(
                    param => OpenWorkflow(),
                    param => CanOpen));
            }
        }

        public ICommand NewWorkflowCommand
        {
            get
            {
                return newWorkflowCommand ?? (newWorkflowCommand = new RelayCommand(
                    param => NewWorkflow(WorkflowTypes.Activity),
                    param => CanNew));
            }
        }

        public ICommand NewServiceCommand
        {
            get
            {
                return newServiceCommand ?? (newServiceCommand = new RelayCommand(
                    param => NewWorkflow(WorkflowTypes.WorkflowService),
                    param => CanNew));
            }
        }

        public ICommand CloseWorkflowCommand
        {
            get
            {
                return closeWorkflowCommand ?? (closeWorkflowCommand = new RelayCommand(
                    param => CloseWorkflow(),
                    param => CanClose));
            }
        }

        public ICommand CloseAllWorkflowsCommand
        {
            get
            {
                return closeAllWorkflowsCommand ?? (closeAllWorkflowsCommand = new RelayCommand(
                    param => CloseAllWorkflows(),
                    param => CanCloseAll));
            }
        }

        public ICommand SaveWorkflowCommand
        {
            get
            {
                return saveWorkflowCommand ?? (saveWorkflowCommand = new RelayCommand(
                    param => SaveWorkflow(),
                    param => CanSave));
            }
        }

        public ICommand SaveAsWorkflowCommand
        {
            get
            {
                return saveAsWorkflowCommand ?? (saveAsWorkflowCommand = new RelayCommand(
                    param => SaveAsWorkflow(),
                    param => CanSaveAs));
            }
        }

        public ICommand StartWithoutDebuggingCommand
        {
            get
            {
                return startWithoutDebuggingCommand ?? (startWithoutDebuggingCommand = new RelayCommand(
                    param => StartWithoutDebugging(),
                    param => CanStart));
            }
        }

        public ICommand StartWithDebuggingCommand
        {
            get
            {
                return startWithDebuggingCommand ?? (startWithDebuggingCommand = new RelayCommand(
                    param => StartWithDebugging(),
                    param => CanStart));
            }
        }

        public ICommand AbortCommand
        {
            get
            {
                return abortCommand ?? (abortCommand = new RelayCommand(
                    param => Abort(),
                    param => CanAbort));
            }
        }

        public ICommand ViewToolboxCommand
        {
            get
            {
                return viewToolboxCommand ?? (viewToolboxCommand = new RelayCommand(
                    param => ViewToolbox(),
                    param => CanViewToolbox));
            }
        }

        public ICommand ViewPropertyInspectorCommand
        {
            get
            {
                return viewPropertyInspectorCommand ?? (viewPropertyInspectorCommand = new RelayCommand(
                    param => ViewPropertyInspector(),
                    param => CanViewPropertyInspector));
            }
        }

        public ICommand ViewOutputCommand
        {
            get
            {
                return viewOutputCommand ?? (viewOutputCommand = new RelayCommand(
                    param => ViewOutput(),
                    param => CanViewOutput));
            }
        }

        public ICommand ViewErrorsCommand
        {
            get
            {
                return viewErrorsCommand ?? (viewErrorsCommand = new RelayCommand(
                    param => ViewErrors(),
                    param => CanViewErrors));
            }
        }

        public ICommand ViewOutlineCommand
        {
            get
            {
                return viewOutlineCommand ?? (viewOutlineCommand = new RelayCommand(
                    param => ViewOutline(),
                    param => CanViewOutline));
            }
        }

        public ICommand ViewDebugCommand
        {
            get
            {
                return viewDebugCommand ?? (viewDebugCommand = new RelayCommand(
                    param => ViewDebug(),
                    param => CanViewDebug));
            }
        }

        public ICommand ExitCommand
        {
            get
            {
                return exitCommand ?? (exitCommand = new RelayCommand(
                    param => Exit(),
                    param => CanExit));
            }
        }

        public ICommand SaveAllWorkflowsCommand
        {
            get
            {
                return saveAllWorkflowsCommand ?? (saveAllWorkflowsCommand = new RelayCommand(
                    param => SaveAll(),
                    param => CanSaveAll));
            }
        }

        public ICommand AboutCommand
        {
            get
            {
                return aboutCommand ?? (aboutCommand = new RelayCommand(
                    param => About(),
                    param => CanAbout));
            }
        }

        public ICommand UndoCommand
        {
            get
            {
                return undoCommand ?? (undoCommand = new RelayCommand(
                    param => Undo(),
                    param => CanUndo));
            }
        }

        public ICommand RedoCommand
        {
            get
            {
                return redoCommand ?? (redoCommand = new RelayCommand(
                    param => Redo(),
                    param => CanRedo));
            }
        }

        private static bool CanNew
        {
            get { return true; }
        }

        private static bool CanOpen
        {
            get { return true; }
        }

        private bool CanClose
        {
            get { return dockingManager.ActiveDocument != null; }
        }

        private bool CanCloseAll
        {
            get { return dockingManager.Documents.Count > 1; }
        }

        private bool CanSave
        {
            get
            {
                var document = dockingManager.ActiveDocument as WorkflowDocumentContent;
                if (document == null)
                {
                    return false;
                }
                var model = document.DataContext as WorkflowViewModel;

                return model != null && model.IsModified;
            }
        }

        private bool CanSaveAs
        {
            get { return dockingManager.ActiveDocument != null; }
        }

        private bool IsRunning
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                return model != null && model.IsRunning;
            }
        }

        private bool CanStart
        {
            get
            {
                return dockingManager.ActiveDocument != null && !IsRunning;
            }
        }

        private bool CanAbort
        {
            get
            {
                return IsRunning;
            }
        }

        private static bool CanViewToolbox
        {
            get { return true; }
        }

        private static bool CanViewPropertyInspector
        {
            get { return true; }
        }

        private static bool CanViewOutput
        {
            get { return true; }
        }

        private static bool CanViewErrors
        {
            get { return true; }
        }

        private bool CanViewDebug
        {
            get { return !disableDebugViewOutput; }
        }

        private static bool CanViewOutline
        {
            get { return true; }
        }

        private static bool CanExit
        {
            get { return true; }
        }

        private bool CanSaveAll
        {
            get
            {
                var modifiedWorkflows = GetModifiedWorkflows();
                return modifiedWorkflows.Count > 1;
            }
        }

        private static bool CanAbout
        {
            get { return true; }
        }

        private bool CanUndo
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                if (model == null) return false;

                var undoEngine = model.Designer.Context.Services.GetService<UndoEngine>();
                return undoEngine.GetUndoActions().Any() && !undoEngine.IsUndoRedoInProgress;
            }
        }

        private bool CanRedo
        {
            get
            {
                var model = ActiveWorkflowViewModel;
                if (model == null) return false;

                var undoEngine = model.Designer.Context.Services.GetService<UndoEngine>();
                return undoEngine.GetRedoActions().Any() && !undoEngine.IsUndoRedoInProgress;
            }
        }

        #endregion

        private WorkflowViewModel ActiveWorkflowViewModel
        {
            get
            {
                var content = dockingManager.ActiveDocument as WorkflowDocumentContent;
                if (content == null)
                {
                    return null;
                }

                return content.DataContext as WorkflowViewModel;
            }
        }

        private bool HasRunningWorkflows
        {
            get
            {
                foreach (var document in dockingManager.Documents)
                {
                    if (!(document is WorkflowDocumentContent))
                    {
                        continue;
                    }
                    var model = document.DataContext as WorkflowViewModel;
                    if (model != null && model.IsRunning)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public void Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = CancelCloseAllWorkflows();
        }

        #region Private helpers

        private void NewWorkflow(WorkflowTypes workflowType)
        {
            var workspaceViewModel = new WorkflowViewModel(disableDebugViewOutput);
            var content = new WorkflowDocumentContent(workspaceViewModel, workflowType)
            {
                Title = workspaceViewModel.DisplayNameWithModifiedIndicator
            };
            content.Show(dockingManager);
            dockingManager.ActiveDocument = content;
            content.Closing += workspaceViewModel.Closing;
            ViewPropertyInspector();
            ViewErrors();
            SetSelectedTab(ContentTypes.Errors);
        }

        private void OpenWorkflow()
        {
            var fileDialog = WorkflowFileDialogFactory.CreateOpenFileDialog();
            if (fileDialog.ShowDialog() == true)
            {
                var workspaceViewModel = new WorkflowViewModel(disableDebugViewOutput) { FullFilePath = fileDialog.FileName };
                var content = new WorkflowDocumentContent(workspaceViewModel);

                var modelService = workspaceViewModel.Designer.Context.Services.GetService<ModelService>();
                if (modelService != null)
                {
                    var referencedActivities = GetReferencedActivities(modelService);
                    AddActivitiesToToolbox(referencedActivities);
                }

                content.Title = workspaceViewModel.DisplayNameWithModifiedIndicator;
                content.Show(dockingManager);
                dockingManager.ActiveDocument = content;
                content.Closing += workspaceViewModel.Closing;
            }

            ViewPropertyInspector();

            if (HasValidationErrors)
            {
                ViewErrors();
                SetSelectedTab(ContentTypes.Errors);
            }
        }

        private void CloseWorkflow()
        {
            dockingManager.ActiveDocument.Close();
        }

        private void CloseAllWorkflows()
        {
            if (CancelCloseAllWorkflows())
            {
                return;
            }

            foreach (var documentContent in new List<DocumentContent>(dockingManager.Documents))
            {
                var viewModel = documentContent.DataContext as WorkflowViewModel;
                if (viewModel != null)
                {
                    documentContent.Closing -= viewModel.Closing;
                }
                documentContent.Close();
            }
        }

        private bool CancelCloseAllWorkflows()
        {
            var cancel = false;

            if (HasRunningWorkflows)
            {
                var closingResult = MessageBox.Show(Resources.ConfirmCloseWhenRunningWorkflowsDialogMessage,
                    Resources.ConfirmCloseWhenRunningWorkflowsDialogTitle, MessageBoxButton.YesNo);
                switch (closingResult)
                {
                    case MessageBoxResult.No:
                        cancel = true;
                        break;
                    case MessageBoxResult.Yes:
                        // do nothing, cancel already set to false
                        break;
                    case MessageBoxResult.Cancel:
                        cancel = true;
                        break;
                }
            }

            if (cancel)
            {
                return true;
            }

            var modifiedWorkflows = GetModifiedWorkflows();
            if (modifiedWorkflows.Count > 0)
            {
                var closingResult =
                    MessageBox.Show(
                        string.Format(Resources.SaveChangesDialogMessage, FormatUnsavedWorkflowNames()),
                        Resources.SaveChangesDialogTitle, MessageBoxButton.YesNoCancel);
                switch (closingResult)
                {
                    case MessageBoxResult.No:
                        // do nothing, cancel already set to false
                        break;
                    case MessageBoxResult.Yes:
                        cancel = !SaveAllWorkflows();
                        break;
                    case MessageBoxResult.Cancel:
                        cancel = true;
                        break;
                }
            }

            return cancel;
        }

        private void SaveWorkflow()
        {
            var model = ActiveWorkflowViewModel;
            if (model != null)
            {
                model.SaveWorkflow();
            }
        }

        private void SaveAsWorkflow()
        {
            var model = ActiveWorkflowViewModel;
            if (model != null)
            {
                model.SaveAsWorkflow();
            }
        }

        private void StartWithoutDebugging()
        {
            var model = ActiveWorkflowViewModel;
            if (model == null)
            {
                return;
            }

            ViewOutput();
            SetSelectedTab(ContentTypes.Output);
            if (HasValidationErrors)
            {
                ViewErrors();
                SetSelectedTab(ContentTypes.Errors);
            }

            model.RunWorkflow();
        }

        private void Abort()
        {
            var model = ActiveWorkflowViewModel;
            if (model != null)
            {
                model.Abort();
            }
        }

        private void StartWithDebugging()
        {
            var model = ActiveWorkflowViewModel;
            if (model == null)
            {
                return;
            }

            ViewOutput();
            ViewDebug();
            SetSelectedTab(ContentTypes.Debug);
            if (HasValidationErrors)
            {
                ViewErrors();
                SetSelectedTab(ContentTypes.Errors);
            }

            model.DebugWorkflow();
            OnPropertyChanged("DebugView");
        }

        private void ViewToolbox()
        {
            CreateOrUnhideDockableContent(ContentTypes.Toolbox, "Toolbox", "ToolboxView", horizontalResizingPanel);
        }

        private void ViewPropertyInspector()
        {
            CreateOrUnhideDockableContent(ContentTypes.PropertyInspector, "Properties", "PropertyInspectorView", horizontalResizingPanel);
        }

        private void ViewOutput()
        {
            CreateOrUnhideDockableContent(ContentTypes.Output, "Output", "OutputView", tabsPane);
        }

        private void ViewErrors()
        {
            CreateOrUnhideDockableContent(ContentTypes.Errors, "Errors", "ValidationErrorsView", tabsPane);
        }

        private void ViewDebug()
        {
            if (!disableDebugViewOutput)
            {
                CreateOrUnhideDockableContent(ContentTypes.Debug, "Debug", "DebugView", tabsPane);
            }
        }

        private void ViewOutline()
        {
            CreateOrUnhideDockableContent(ContentTypes.Outline, "Outline", "OutlineView", horizontalResizingPanel);
        }

        private void Exit()
        {
            if (CancelCloseAllWorkflows())
            {
                return;
            }

            foreach (var documentContent in new List<DocumentContent>(dockingManager.Documents))
            {
                var viewModel = documentContent.DataContext as WorkflowViewModel;
                if (viewModel != null)
                {
                    documentContent.Closing -= viewModel.Closing;
                }
                documentContent.Close();
            }

            Application.Current.ShutdownMode = ShutdownMode.OnMainWindowClose;
            Application.Current.Shutdown();
        }

        private void SaveAll()
        {
            SaveAllWorkflows();
        }

        private static void About()
        {
            var version = Assembly.GetExecutingAssembly().GetName().Version;
            MessageBox.Show(string.Format(CultureInfo.InvariantCulture, Resources.AboutDialogMessage, version.ToString(4)), Resources.AboutDialogTitle, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void Undo()
        {
            var model = ActiveWorkflowViewModel;
            model.Designer.Context.Services.GetService<UndoEngine>().Undo();
        }

        private void Redo()
        {
            var model = ActiveWorkflowViewModel;
            model.Designer.Context.Services.GetService<UndoEngine>().Redo();
        }

        private void InitialiseToolbox()
        {
            loadedToolboxActivities = new Dictionary<ToolboxCategory, IList<string>>();
            toolboxCategoryMap = new Dictionary<string, ToolboxCategory>();

            AddCategoryToToolbox(
                Resources.ControlFlowCategoryToToolbox,
                new List<Type>
                {
                    typeof(ForEachWithBodyFactory<>),
                    typeof(If),
                    typeof(Parallel),
                    typeof(ParallelForEachWithBodyFactory<>),
                    typeof(DoWhile),
                    typeof(Pick),
                    typeof(PickBranch),
                    typeof(Sequence),
                    typeof(Switch<>),
                    typeof(While)
                });

            AddCategoryToToolbox(
                Resources.FlowchartCategoryToToolbox,
                new List<Type>
                {
                    typeof(Flowchart),
                    typeof(FlowDecision),
                    typeof(FlowSwitch<>)
                });

            AddCategoryToToolbox(
                Resources.MessagingCategoryToToolbox,
                new List<Type>
                {
                    typeof(CorrelationScope),
                    typeof(InitializeCorrelation),
                    typeof(Receive),
                    typeof(ReceiveAndSendReplyFactory),
                    typeof(Send),
                    typeof(SendAndReceiveReplyFactory),
                    typeof(TransactedReceiveScope)
                });

            AddCategoryToToolbox(
                Resources.RuntimeCategoryToToolbox,
                new List<Type>
                {
                    typeof(Persist),
                    typeof(TerminateWorkflow),
                });

            AddCategoryToToolbox(
                Resources.PrimitivesCategoryToToolbox,
                new List<Type>
                {
                    typeof(Assign),
                    typeof(Delay),
                    typeof(InvokeMethod),
                    typeof(WriteLine),
                });

            AddCategoryToToolbox(
                Resources.TransactionCategoryToToolbox,
                new List<Type>
                {
                    typeof(CancellationScope),
                    typeof(CompensableActivity),
                    typeof(Compensate),
                    typeof(Confirm),
                    typeof(TransactionScope),
                });

            AddCategoryToToolbox(
                Resources.CollectionCategoryToToolbox,
                new List<Type>
                {
                    typeof(AddToCollection<>),
                    typeof(ClearCollection<>),
                    typeof(ExistsInCollection<>),
                    typeof(RemoveFromCollection<>),
                });

            AddCategoryToToolbox(
                Resources.ErrorHandlingCategoryToToolbox,
                new List<Type>
                {
                    typeof(Rethrow),
                    typeof(Throw),
                    typeof(TryCatch),
                });
        }

        private void AddPackNetActivities()
        {
            var dir = new DirectoryInfo(Environment.CurrentDirectory);
            var packNetAssemblies = dir.GetFiles("PackNet.Common*.dll").Select(f => f.FullName);
            AddActivitiesFromAssemblies(packNetAssemblies);
        }

        private void AddActivitiesFromAssemblies(IEnumerable<string> assemblyFiles)
        {
            var assemblies = new List<Assembly>();

            foreach (var assembly in assemblyFiles.Select(Assembly.LoadFrom))
            {
                assemblies.Add(assembly);
                AddCategoryToToolbox(assemblies);
            }
        }

        private static bool IsValidToolboxActivity(Type activityType)
        {
            return activityType.IsPublic &&
                   !activityType.IsNested &&
                   !activityType.IsAbstract &&
                   (typeof(Activity).IsAssignableFrom(activityType) ||
                    typeof(IActivityTemplateFactory).IsAssignableFrom(activityType) ||
                    typeof(FlowNode).IsAssignableFrom(activityType));
        }

        /// <summary>
        /// Iterate through the given assemblies, find all the activities in them, then add them to the toolbox.
        /// </summary>
        /// <param name="assemblies">Assemblies to iterate over for activities.</param>
        /// <remarks>
        /// When reflecting on assemblies, types that throw a load exception are ignored. For additional information 
        /// see remarks http://msdn.microsoft.com/en-us/library/System.Reflection.ReflectionTypeLoadException(v=vs.110).aspx
        /// </remarks>
        private void AddCategoryToToolbox(IEnumerable<Assembly> assemblies)
        {
            foreach (var assembly in assemblies)
            {
                IEnumerable<Type> assemblyTypes;
                try
                {
                    assemblyTypes = assembly.GetTypes();
                }
                catch (ReflectionTypeLoadException e)
                {
                    assemblyTypes = e.Types.Where(t => t != null);
                }

                AddActivitiesToToolbox(assemblyTypes);
            }
        }

        /// <summary>
        /// Creates a category from the categoryName then adds an activity for each of the activities.
        /// </summary>
        /// <param name="categoryName">Name of the category to create.</param>
        /// <param name="activities">The activities to add to the category.</param>
        private void AddCategoryToToolbox(string categoryName, IEnumerable<Type> activities)
        {
            foreach (var activityType in activities)
            {
                if (!IsValidToolboxActivity(activityType))
                {
                    continue;
                }
                var category = GetToolboxCategory(categoryName);

                if (loadedToolboxActivities[category].Contains(GetTypeFullNameWithGenericsRemoved(activityType.FullName)))
                {
                    continue;
                }

                AddActivity(activityType, category);
            }
        }

        /// <summary>
        /// Creates a category for the given activities based on the namespace of each activity then adds
        /// all of the activities.
        /// </summary>
        /// <param name="activities">Activities to create a category for then add them to</param>
        private void AddActivitiesToToolbox(IEnumerable<Type> activities)
        {
            foreach (var activityType in activities)
            {
                if (!IsValidToolboxActivity(activityType))
                {
                    continue;
                }
                var category = GetToolboxCategory(activityType.Namespace);

                if (loadedToolboxActivities[category].Contains(GetTypeFullNameWithGenericsRemoved(activityType.FullName)))
                {
                    continue;
                }

                AddActivity(activityType, category);
            }
        }

        /// <summary>
        /// Removes the generic portion for a type. A Type's generic portion is appended after a back tick 
        /// character (`), then a number indicating how many generics are on this type then square brackets.
        /// </summary>
        /// <param name="typeName">Type name that needs the generic portion removed</param>
        /// <returns>The type name with the generic portion removed or the type name if it does not contain a generic part</returns>
        /// <example>
        /// PackNet.Common.WorkflowActivities.EventAggregatorPublishActivity`1[[System.Collections.Generic.IEnumerable`1[[PackNet.Common.Interfaces.DTO.Carton.ICartonRequestCommand, PackNet.Common.Interfaces, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]
        /// would return PackNet.Common.WorkflowActivities.EventAggregatorPublishActivity`1
        /// </example>
        private static string GetTypeFullNameWithGenericsRemoved(string typeName)
        {
            var regex = new Regex(@"`[0-9]+");
            var match = regex.Match(typeName);

            return match.Success ? typeName.Substring(0, match.Index + match.Value.Length) : typeName;
        }

        /// <summary>
        /// Adds an activity to the given category
        /// </summary>
        /// <param name="activityType">Activity type to add</param>
        /// <param name="category">ToolboxCategory that the activity is added to</param>
        private void AddActivity(Type activityType, ToolboxCategory category)
        {
            loadedToolboxActivities[category].Add(GetTypeFullNameWithGenericsRemoved(activityType.FullName));
            category.Add(new ToolboxItemWrapper(GetTypeFullNameWithGenericsRemoved(activityType.FullName),
                activityType.Assembly.FullName, null, GetDisplayName(activityType.Name)));
        }

        /// <summary>
        /// The full name of the generic type contains a back tick character (`) followed by a digit that specifies 
        /// the number of type arguments, followed by a square bracket containing each type argument (possibly in 
        /// an additional square bracket). This method splits the name into its constituent parts.
        /// </summary>
        /// <param name="typeName">Name of the type to check for the generic back tick character</param>
        /// <returns></returns>
        private static string GetDisplayName(string typeName)
        {
            var nameWithBodyFactoryRemoved = typeName.Replace("WithBodyFactory", string.Empty);
            var splitName = nameWithBodyFactoryRemoved.Split('`');
            return splitName.Length == 1 ? typeName : string.Format("{0}<>", splitName[0]);
        }

        private static IEnumerable<Type> GetReferencedActivities(ModelService modelService)
        {
            var items = modelService.Find(modelService.Root, typeof(Activity));
            var activities = new List<Type>();
            foreach (var item in items)
            {
                if (!namespacesToIgnore.Contains(item.ItemType.Namespace))
                {
                    activities.Add(item.ItemType);
                }
            }

            return activities;
        }

        private ToolboxCategory GetToolboxCategory(string name)
        {
            if (toolboxCategoryMap.ContainsKey(name))
            {
                return toolboxCategoryMap[name];
            }

            var category = new ToolboxCategory(name);
            toolboxCategoryMap[name] = category;
            loadedToolboxActivities.Add(category, new List<string>());
            toolboxControl.Categories.Add(category);
            return category;
        }

        private void CreateOrUnhideDockableContent(ContentTypes contentType, string title, string viewPropertyName, object parent)
        {
            if (!dockableContents.Keys.Contains(contentType))
            {
                var dockableContent = new DockableContent();

                var contentControl = new ContentControl();

                dockableContent.IsCloseable = true;
                dockableContent.HideOnClose = false;

                dockableContent.Title = title;

                dockableContent.Content = contentControl;

                if (parent is ResizingPanel)
                {
                    var dockablePane = new DockablePane();
                    dockablePane.Items.Add(dockableContent);
                    var resizingPanel = parent as ResizingPanel;

                    switch (contentType)
                    {
                        case ContentTypes.PropertyInspector:
                            resizingPanel.Children.Add(dockablePane);
                            ResizingPanel.SetResizeWidth(dockablePane, new GridLength(300));
                            break;
                        case ContentTypes.Outline:
                            resizingPanel.Children.Insert(1, dockablePane);
                            ResizingPanel.SetResizeWidth(dockablePane, new GridLength(250));
                            break;
                        case ContentTypes.Toolbox:
                            resizingPanel.Children.Insert(0, dockablePane);
                            ResizingPanel.SetResizeWidth(dockablePane, new GridLength(250));
                            break;
                    }
                }
                else if (parent is DockablePane)
                {
                    var dockablePane = parent as DockablePane;
                    dockablePane.Items.Add(dockableContent);
                    if (dockablePane.Parent == null)
                    {
                        verticalResizingPanel.Children.Add(dockablePane);
                    }
                }

                var dataContextBinding = new Binding(viewPropertyName);
                dockableContent.SetBinding(FrameworkElement.DataContextProperty, dataContextBinding);

                var contentBinding = new Binding(".");
                contentControl.SetBinding(ContentControl.ContentProperty, contentBinding);

                dockableContents[contentType] = dockableContent;

                dockableContent.Closed += delegate
                {
                    contentControl.Content = null;
                    dockableContents[contentType].DataContext = null;
                    dockableContents.Remove(contentType);
                };
            }
            else
            {
                if (dockableContents[contentType].State == DockableContentState.Hidden ||
                    dockableContents[contentType].State == DockableContentState.AutoHide)
                {
                    dockableContents[contentType].Show();
                }
            }
        }

        private void SetSelectedTab(ContentTypes contentType)
        {
            if (dockableContents.Keys.Contains(contentType))
            {
                tabsPane.SelectedItem = dockableContents[contentType];
            }
        }

        private IList<WorkflowViewModel> GetModifiedWorkflows()
        {
            var modifiedWorkflows = new List<WorkflowViewModel>();

            foreach (var document in dockingManager.Documents)
            {
                if (!(document is WorkflowDocumentContent))
                {
                    continue;
                }

                var model = document.DataContext as WorkflowViewModel;
                if (model != null && model.IsModified)
                {
                    modifiedWorkflows.Add(model);
                }
            }

            return modifiedWorkflows;
        }

        private bool SaveAllWorkflows()
        {
            foreach (var model in GetModifiedWorkflows())
            {
                if (!model.SaveWorkflow())
                {
                    return false;
                }
            }

            return true;
        }

        private string FormatUnsavedWorkflowNames()
        {
            var stringBuilder = new StringBuilder();

            foreach (var model in GetModifiedWorkflows())
            {
                stringBuilder.Append(model.DisplayName);
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
        }

        private void UpdateViews()
        {
            OnPropertyChanged("PropertyInspectorView");
            OnPropertyChanged("ToolboxView");
            OnPropertyChanged("ValidationErrorsView");
            OnPropertyChanged("DebugView");
            OnPropertyChanged("OutputView");
            OnPropertyChanged("OutlineView");
        }
        #endregion
    }
}
