﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Microsoft.Win32;
using PackNet.Common.FileHandling.DynamicImporter;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging;
using PackNet.Common.Logging.NLogBackend;
using ReactiveUI;
using WorkflowProfiling;

namespace PackNet.Workflow.Studio.ViewModel
{
    public class ImportProfilerViewModel : ReactiveObject
    {
        private ILogger logger;

        private string pathToComparison;

        private string pathToBaseline;

        private string pathToImport;

        public ProfilingResult baseResult;

        public ProfilingResult comparisonResult;

        private WorkflowProfiler profiler;

        private bool showComparison;

        private bool showBase;

        private int numberOfElementsToShow;

        private IDictionary<int, IDictionary<string, object>> samples;

        private IDictionary<int, IEnumerable<PropertyDiff>> comparisonDiff;

        private bool showBaseSample;

        private string baseExceptionMessage;

        private string comparisonExceptionMessage;

        public ProfilingResult BaseResult
        {
            get
            {
                return this.baseResult;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref this.baseResult, value);
            }
        }

        public ProfilingResult ComparisonResult
        {
            get
            {
                return this.comparisonResult;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref this.comparisonResult, value);
            }
        }

        public IDictionary<int, IEnumerable<PropertyDiff>> ComparisonDiff
        {
            get
            {
                return this.comparisonDiff;
            }
            private set
            {
                this.RaiseAndSetIfChanged(ref this.comparisonDiff, value);
            }
        }

        public bool ShowComparison
        {
            get
            {
                return this.showComparison;
            }
            private set
            {
                this.ShowBaseSample = value == false;
                this.RaiseAndSetIfChanged(ref this.showComparison, value);
            }
        }

        public bool ShowBaseSample
        {
            get { return this.showBaseSample; }

            private set { this.RaiseAndSetIfChanged(ref showBaseSample, value); }
        }

        public bool ShowBase
        {
            get
            {
                return this.showBase;
            }
            private set
            {
                this.RaiseAndSetIfChanged(ref this.showBase, value);
            }
        }

        public string PathToBaseline
        {
            get
            {
                return this.pathToBaseline;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref this.pathToBaseline, value);
            }
        }

        public string PathToImport
        {
            get
            {
                return this.pathToImport;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref this.pathToImport, value);
            }
        }

        public string PathToComparison
        {
            get
            {
                return this.pathToComparison;
            }
            set
            {
                this.RaiseAndSetIfChanged(ref this.pathToComparison, value);
            }
        }

        public string BaseExceptionMessage
        {
            get { return this.baseExceptionMessage; }
            set { this.RaiseAndSetIfChanged(ref this.baseExceptionMessage, value); }
        }

        public string ComparisonExceptionMessage
        {
            get { return this.comparisonExceptionMessage; }
            set { this.RaiseAndSetIfChanged(ref this.comparisonExceptionMessage, value); }
        }

        public ReactiveCommand<object> BrowseImportFile { get; protected set; }

        public ReactiveCommand<object> BrowseBaseline { get; protected set; }

        public ReactiveCommand<object> BrowseComparison { get; protected set; }

        public ReactiveCommand<object> ProfileWorkFlow { get; protected set; }

        public int NumberOfElementsToShow
        {
            get { return this.numberOfElementsToShow; }
            set { this.RaiseAndSetIfChanged(ref this.numberOfElementsToShow, value); }
        }

        public IEnumerable<int> ElementsToShowSelection { get; set; }

        public IDictionary<int, IDictionary<string, object>> Samples
        {
            get { return this.samples; }
            set { this.RaiseAndSetIfChanged(ref this.samples, value); }
        }

        public ImportProfilerViewModel()
        {
            this.BaseResult = null;
            this.ComparisonResult = null;
            this.ShowComparison = false;
            this.ComparisonDiff = new Dictionary<int, IEnumerable<PropertyDiff>>();
            this.Samples = new Dictionary<int, IDictionary<string, object>>();
            this.NumberOfElementsToShow = 1;

            var configPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "NLog.config");

            this.logger = new NLogImplementation("Profiler", configPath);
            this.profiler = new WorkflowProfiler(new TimeSpan(0, 10, 0));

            this.BrowseImportFile = ReactiveCommand.Create();
            this.BrowseImportFile.Subscribe(x => this.PathToImport = this.OpenFileDialog());

            this.BrowseBaseline = ReactiveCommand.Create();
            this.BrowseBaseline.Subscribe(x => this.PathToBaseline = this.OpenFileDialog());

            this.BrowseComparison = ReactiveCommand.Create();
            this.BrowseComparison.Subscribe(x => this.PathToComparison = this.OpenFileDialog());

            this.ElementsToShowSelection = new List<int>() { 1, 10, 50 };

            this.ProfileWorkFlow =
                ReactiveCommand.Create(
                    this.WhenAny(
                        x => x.PathToImport,
                        x => x.PathToBaseline,
                        x => x.PathToComparison,
                        (import, baseline, comparison) =>
                            File.Exists(import.Value) && File.Exists(baseline.Value)
                            && (string.IsNullOrWhiteSpace(comparison.Value) || File.Exists(comparison.Value))));
            this.ProfileWorkFlow.Subscribe(x => this.RunProfiling());
        }

        private void RunProfiling()
        {
            this.BaseResult = null;
            this.ComparisonResult = null;
            this.ShowBase = false;
            this.ShowComparison = false;
            var importables = this.ParseFile(this.PathToImport);

            if (!string.IsNullOrWhiteSpace(this.pathToComparison))
            {
                var result = this.profiler.CompareWorkflows(
                    this.pathToBaseline,
                    this.pathToComparison,
                    importables,
                    new[] { "RequestGroupId", "Created" });
                this.BaseResult = result.BaseProfilingResult;
                this.ComparisonResult = result.ComparisonProfilingResult;
                this.ShowComparison = true;
                this.ShowBase = true;
                PopulateDiffs(result.Differences);
            }
            else
            {
                var result = this.profiler.ProfileWorkflow(this.pathToBaseline, importables);
                this.BaseResult = result;
                this.ShowBase = true;
                this.PopulateSamples(result.Result.Select(c => c));
            }

            SetExceptionMessages();
        }

        private void SetExceptionMessages()
        {
            this.BaseExceptionMessage = String.Empty;
            this.ComparisonExceptionMessage = String.Empty;

            if (this.baseResult != null && this.baseResult.ThrewException)
                this.BaseExceptionMessage = GetExceptionMessage(this.baseResult.ThrownException);
            if (this.comparisonResult != null && this.comparisonResult.ThrewException)
                this.ComparisonExceptionMessage = GetExceptionMessage(this.comparisonResult.ThrownException);
        }

        private string GetExceptionMessage(Exception exception)
        {
            if (exception == null)
                return String.Empty;

            var message = "Type: " + exception.GetType() + "\r\n" + exception.Message;
            
            if (exception.InnerException != null)
                return message + "\r\n" + GetExceptionMessage(exception.InnerException);
            return message;
        }

        private void PopulateDiffs(IDictionary<int, IEnumerable<PropertyDiff>> diffs)
        {
            ComparisonDiff = new Dictionary<int, IEnumerable<PropertyDiff>>();
            var localDiffs = diffs.ToArray();
            var rate = localDiffs.Count() / numberOfElementsToShow;

            for (int i = 0; i < localDiffs.Count(); i += rate)
            {
                ComparisonDiff.Add(diffs.ElementAt(i));
            }
        }

        private void PopulateSamples(IEnumerable<object> objects)
        {
            Samples = new Dictionary<int, IDictionary<string, object>>();
            var localObjects = objects.ToArray();
            var rate = localObjects.Count() / numberOfElementsToShow;
            for (int i = 0; i < localObjects.Count() && samples.Count < numberOfElementsToShow; i += rate)
            {
                var type = localObjects[i].GetType();
                var propDict = new Dictionary<string, object>();
                foreach (var prop in type.GetProperties())
                {
                    var val = prop.GetValue(localObjects[i]);
                    propDict.Add(prop.Name, val);
                }
                Samples.Add(i + 1, propDict);
            }
        }

        private Importable ParseFile(string filePath)
        {
            var dynamicData = new FileEnumerator(filePath, this.logger, null, ';');
            var data = dynamicData.ToList<dynamic>();
            return new Importable() { ImportType = ImportTypes.BoxFirst, ImportedItems = data };
        }

        private string OpenFileDialog()
        {
            var dialog = new OpenFileDialog();
            var result = dialog.ShowDialog();
            if (result == true)
                return dialog.FileName;
            return string.Empty;
        }
    }
}
