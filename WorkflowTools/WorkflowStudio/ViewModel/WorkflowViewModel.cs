﻿//-----------------------------------------------------------------------
// <copyright file="WorkflowViewModel.cs" company="Microsoft Corporation">
// Copyright (c) Microsoft Corporation. All rights reserved.
// THIS CODE AND INFORMATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, 
// INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Activities.Presentation;
using System.Activities.Presentation.Services;
using System.Activities.Presentation.Validation;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Runtime.Versioning;
using System.ServiceModel.Activities;
using System.Windows;
using System.Windows.Controls;

using PackNet.Workflow.Studio.Execution;
using PackNet.Workflow.Studio.Properties;
using PackNet.Workflow.Studio.Utilities;
using PackNet.Workflow.Studio.Views;

namespace PackNet.Workflow.Studio.ViewModel
{
    public class WorkflowViewModel : ViewModelBase
    {
        private static int designerCount;
        private readonly WorkflowDesigner workflowDesigner;
        private readonly IList<ValidationErrorInfo> validationErrors;
        private readonly ValidationErrorsUserControl validationErrorsView;
        private TextWriter output;
        private readonly TextBox outputTextBox;
        private IWorkflowRunner runner;
        private readonly int id;
        private string fullFilePath;
        private readonly bool disableDebugViewOutput;

        public WorkflowViewModel(bool disableDebugViewOutput)
        {
            workflowDesigner = new WorkflowDesigner();
            id = ++designerCount;
            validationErrors = new List<ValidationErrorInfo>();
            var validationErrorService = new ValidationErrorService(validationErrors);
            workflowDesigner.Context.Services.Publish<IValidationErrorService>(validationErrorService);

            workflowDesigner.ModelChanged += delegate
            {
                IsModified = true;
                OnPropertyChanged("DisplayNameWithModifiedIndicator");
            };

            validationErrorsView = new ValidationErrorsUserControl();

            outputTextBox = new TextBox();
            output = new TextBoxStreamWriter(outputTextBox, DisplayName);
            this.disableDebugViewOutput = disableDebugViewOutput;

            workflowDesigner.Context.Services.GetService<DesignerConfigurationService>().TargetFrameworkName = new FrameworkName(".NETFramework", new Version(4, 5));
            workflowDesigner.Context.Services.GetService<DesignerConfigurationService>().LoadingFromUntrustedSourceEnabled =
                bool.Parse(ConfigurationManager.AppSettings["LoadingFromUntrustedSourceEnabled"]);

            validationErrorService.ErrorsChangedEvent += delegate
            {
                DispatcherService.Dispatch(() =>
                {
                    validationErrorsView.ErrorsDataGrid.ItemsSource = validationErrors;
                    validationErrorsView.ErrorsDataGrid.Items.Refresh();
                });
            };
        }

        #region Presentation Properties

        public UIElement DebugView
        {
            get
            {
                var debugger = runner as IWorkflowDebugger;
                return debugger != null ? debugger.GetDebugView() : null;
            }
        }

        public UIElement OutlineView
        {
            get
            {
                return workflowDesigner.OutlineView;
            }
        }

        public UIElement ValidationErrorsView
        {
            get
            {
                return validationErrorsView;
            }
        }

        public bool HasValidationErrors
        {
            get
            {
                return validationErrors.Count > 0;
            }
        }

        public UIElement OutputView
        {
            get
            {
                return outputTextBox;
            }
        }

        public TextWriter Output
        {
            get
            {
                return output;
            }
        }

        public WorkflowDesigner Designer
        {
            get
            {
                return workflowDesigner;
            }
        }

        public string DisplayName
        {
            get
            {
                return string.IsNullOrEmpty(FullFilePath)
                    ? string.Format(Resources.NewWorkflowTabTitle, id)
                    : Path.GetFileName(FullFilePath);
            }
        }

        public string DisplayNameWithModifiedIndicator
        {
            get
            {
                var modifiedIndicator = IsModified ? "*" : string.Empty;
                return string.IsNullOrEmpty(FullFilePath)
                    ? string.Format(Resources.NewWorkflowWithModifierTabTitle, id, modifiedIndicator)
                    : string.Format("{0} {1}", Path.GetFileName(FullFilePath), modifiedIndicator);
            }
        }

        public string FullFilePath
        {
            get
            {
                return fullFilePath;
            }
            set
            {
                fullFilePath = value;
                output = new TextBoxStreamWriter(outputTextBox, Path.GetFileNameWithoutExtension(fullFilePath));
            }
        }

        public bool IsRunning
        {
            get
            {
                return runner != null && runner.IsRunning;
            }
        }

        public bool IsModified { get; private set; }

        #endregion

        public void Abort()
        {
            if (runner != null)
            {
                runner.Abort();
            }
        }

        public void Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = false;

            MessageBoxResult closingResult;
            if (IsRunning)
            {
                closingResult = MessageBox.Show(Resources.ConfirmCloseWhenRunningWorkflowDialogMessage,
                    Resources.ConfirmCloseWhenRunningWorkflowDialogTitle, MessageBoxButton.YesNo);
                switch (closingResult)
                {
                    case MessageBoxResult.No:
                        e.Cancel = true;
                        break;
                    case MessageBoxResult.Yes:
                        e.Cancel = false;
                        Abort();
                        break;
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }

            if (e.Cancel)
            {
                return;
            }

            if (!IsModified)
            {
                return;
            }

            closingResult = MessageBox.Show(string.Format(Resources.SaveChangesDialogMessage, DisplayName),
                Resources.SaveChangesDialogTitle, MessageBoxButton.YesNoCancel);
            switch (closingResult)
            {
                case MessageBoxResult.No:
                    e.Cancel = false;
                    break;
                case MessageBoxResult.Yes:
                    e.Cancel = !SaveWorkflow();
                    break;
                case MessageBoxResult.Cancel:
                    e.Cancel = true;
                    break;
            }
        }

        public Type GetRootType()
        {
            var modelService = workflowDesigner.Context.Services.GetService<ModelService>();
            return modelService != null ? modelService.Root.GetCurrentValue().GetType() : null;
        }

        public bool SaveWorkflow()
        {
            if (!string.IsNullOrEmpty(FullFilePath))
            {
                SaveWorkflow(FullFilePath);
                return true;
            }
            var fileDialog = WorkflowFileDialogFactory.CreateSaveFileDialog(DisplayName);

            if (fileDialog.ShowDialog() != true)
            {
                return false;
            }
            SaveWorkflow(fileDialog.FileName);
            return true;
        }

        public void SaveAsWorkflow()
        {
            var fileDialog = WorkflowFileDialogFactory.CreateSaveFileDialog(DisplayName);

            if (fileDialog.ShowDialog() == true)
            {
                SaveWorkflow(fileDialog.FileName);
            }
        }

        public void RunWorkflow()
        {
            if (GetRootType() == typeof(WorkflowService))
            {
                runner = new WorkflowServiceHostRunner(output, DisplayName, workflowDesigner);
            }
            else
            {
                runner = new WorkflowRunner(output, DisplayName, workflowDesigner);
            }

            try
            {
                outputTextBox.Clear();
                runner.Run();
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(Resources.ErrorRunningDialogMessage, ExceptionHelper.FormatStackTrace(e)), Resources.ErrorRunningDialogTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void DebugWorkflow()
        {
            if (GetRootType() == typeof(WorkflowService))
            {
                runner = new WorkflowServiceHostDebugger(output, DisplayName, workflowDesigner, disableDebugViewOutput);
            }
            else
            {
                runner = new WorkflowDebugger(output, DisplayName, workflowDesigner, disableDebugViewOutput);
            }

            try
            {
                outputTextBox.Clear();
                runner.Run();
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(Resources.ErrorLoadingInDebugDialogMessage, ExceptionHelper.FormatStackTrace(e)),
                    Resources.ErrorLoadingInDebugDialogTitle, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveWorkflow(string path)
        {
            StatusViewModel.SetStatusText(Resources.SavingStatus, DisplayName);

            FullFilePath = path;
            workflowDesigner.Save(FullFilePath);
            IsModified = false;
            OnPropertyChanged("DisplayName");
            OnPropertyChanged("DisplayNameWithModifiedIndicator");

            StatusViewModel.SetStatusText(Resources.SaveSuccessfulStatus, DisplayName);
        }
    }
}