﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Workflow.Studio.ViewModel;
using Testing.Specificity;

namespace WorkflowStudioTests
{
    [TestClass]
    public class ImporterProfilerViewModelTests
    {
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldPopulateProfilingResult()
        {
            var vm = new ImportProfilerViewModel();
            vm.PathToImport = "./Files/sampleWave.txt";
            vm.PathToBaseline = "./Files/CartonImport.xaml";
            vm.NumberOfElementsToShow = 10;
            vm.ProfileWorkFlow.Execute(null);

            Specify.That(vm.BaseResult).Should.Not.BeNull();
            Specify.That(vm.BaseResult.NumberOfItemsInResult).Should.BeEqualTo(54);
            Specify.That(vm.ShowBaseSample).Should.BeTrue();
            Specify.That(vm.ShowComparison).Should.BeFalse();
            Specify.That(vm.Samples.Count()).Should.BeEqualTo(10);
            Specify.That(vm.Samples.ElementAt(0).Key).Should.BeEqualTo(1);
            Specify.That(vm.Samples.ElementAt(1).Key).Should.BeEqualTo(6);
            Specify.That(vm.Samples.ElementAt(2).Key).Should.BeEqualTo(11);

            Specify.That(vm.ComparisonResult).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldDisplayException()
        {
            var vm = new ImportProfilerViewModel();
            vm.PathToImport = "./Files/sampleWave_err.txt";
            vm.PathToBaseline = "./Files/CartonImport.xaml";

            Specify.That(vm.ProfileWorkFlow.CanExecute(null)).Should.BeTrue();
            
            vm.ProfileWorkFlow.Execute(null);

            Specify.That(vm.BaseResult).Should.Not.BeNull();
            Specify.That(vm.ComparisonResult).Should.BeNull();
            Specify.That(vm.BaseResult.ThrewException).Should.BeTrue();
            Specify.That(vm.BaseExceptionMessage).Should.Not.BeEmpty();
            Specify.That(vm.BaseResult.ThrownException.GetType()).Should.BeLogicallyEqualTo(typeof(ImportFailedException));
        }
    }
}
