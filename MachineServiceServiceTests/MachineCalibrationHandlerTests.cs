﻿using System.Collections.Generic;
using System.Globalization;
using System.Xml;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.MachineCommandService;
using PackNet.MachineCommandService.DTO;

using Testing.Specificity;

namespace MachineCommandServiceTests
{
    [TestClass]
    public class MachineCalibrationHandlerTests
    {

        private IMachineCalibrationHandler calibrationHandler;
        private FusionMachine machine;

        private Mock<IMachineCommandCommunicator> serviceCommunicatorMock;

        [TestInitialize]
        public void Setup()
        {
            calibrationHandler = new MachineCalibrationHandler();

            serviceCommunicatorMock = new Mock<IMachineCommandCommunicator>();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("TestFiles", "MachineCalibrationTestFiles")]
        public void HandleFusionCalibrationCorrectlyWritesValuesToPhysicalMachineSettingsFile()
        {
            machine = new FusionMachine
            {
                PhysicalSettingsFilePath = @"MachineCalibrationTestFiles/machineTheoretical_inches_NarrowLongheads.xml",
                PhysicalMachineSettings = new FusionPhysicalMachineSettings()
            };

            serviceCommunicatorMock.Setup(m => m.GetFusionCalibrationValues()).Returns(() => new FusionTrackCalibrationValues()
            {
                LeftCompensations = new List<List<double>>()
                {
                    new List<double> { 11,12,13,14,15,16,17,18,19,20},
                    new List<double> { 21,22,23,24,25,26,27,28,29,30},
                },
                RightCompensations = new List<List<double>>()
                {
                    new List<double> { 11,12,13,14,15,16,17,18,19,20},
                    new List<double> { 21,22,23,24,25,26,27,28,29,30},
                } 
            });

            calibrationHandler.HandleFusionCalibration(machine, serviceCommunicatorMock.Object);
            
            var doc = new XmlDocument();
            doc.Load(@"MachineCalibrationTestFiles/machineTheoretical_inches_NarrowLongheads.xml");

            var trackCounter = 0;

            var tracks = doc.SelectNodes("machine/tracks/track");
            foreach (XmlNode track in tracks)
            {
                trackCounter++;

                var compensationCounter = 1;
                foreach (XmlNode compensationsnode in track.SelectNodes("leftswordsensorCompensations/compensation"))
                {
                    Specify.That(double.Parse(compensationsnode.FirstChild.Value, CultureInfo.InvariantCulture)).Should.BeLogicallyEqualTo((trackCounter * 10) + compensationCounter++);
                }

                compensationCounter = 1;
                foreach (XmlNode compensationsnode in track.SelectNodes("rightswordsensorCompensations/compensation"))
                {
                    Specify.That(double.Parse(compensationsnode.FirstChild.Value, CultureInfo.InvariantCulture)).Should.BeLogicallyEqualTo((trackCounter * 10) + compensationCounter++);
                }
            }

        }
    }
}
