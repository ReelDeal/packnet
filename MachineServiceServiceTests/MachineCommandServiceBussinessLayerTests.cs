﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Utils;
using PackNet.MachineCommandService;
using PackNet.MachineCommandService.DTO;

using Testing.Specificity;

using Track = PackNet.Common.Interfaces.DTO.PhysicalMachine.Track;

namespace MachineCommandServiceTests
{
    [TestClass]
    public class MachineCommandServiceBussinessLayerTests
    {
        private IMachineCommandServiceBussinessLayer bussinessLayer;
        private Mock<ILogger> loggerMock;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IAggregateMachineService> machineServiceMock;
        private Mock<IMachineCalibrationHandler> calibrationHandlerMock;
        private Mock<IMachineCommandCommunicatorFactory> communicatorFactoryMock;
        private Mock<IMachineCommandCommunicator> communicatorMock;
        private EventAggregator aggregator;

        private EmMachine emMachine;
        private FusionMachine fusionMachine;
        private ZebraPrinter printer;

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();

            loggerMock = new Mock<ILogger>();
            calibrationHandlerMock = new Mock<IMachineCalibrationHandler>();
            communicatorMock = GetCommunicatorMock();
            communicatorFactoryMock = GetMachineCommunicatorFactoryMock();
            uiCommunicationServiceMock = GetUiCommunicationServiceMock();
            machineServiceMock = GetMachineServiceMock();

            bussinessLayer = new MachineCommandServiceBussinessLayer(machineServiceMock.Object,
                uiCommunicationServiceMock.Object, calibrationHandlerMock.Object, communicatorFactoryMock.Object, loggerMock.Object);
        }

        private Mock<IMachineCommandCommunicator> GetCommunicatorMock()
        {
            var mock = new Mock<IMachineCommandCommunicator>();


            return mock;
        }

        private Mock<IMachineCommandCommunicatorFactory> GetMachineCommunicatorFactoryMock()
        {
            var mock = new Mock<IMachineCommandCommunicatorFactory>();

            mock.Setup(m => m.GetInstance(It.IsAny<IPacksizeCutCreaseMachine>())).Returns<IPacksizeCutCreaseMachine>(m =>
            {
                if(m.Id.Equals(emMachine.Id))
                    return communicatorMock.Object;
                return null;
            });

            return mock;
        }

        private Mock<IAggregateMachineService> GetMachineServiceMock()
        {
            var mock = new Mock<IAggregateMachineService>();

            printer = new ZebraPrinter() { Id = Guid.NewGuid() };

            mock.Setup(m => m.Machines).Returns(new List<IMachine>() { GetEmMachine(), GetFusionMachine(), printer });
            mock.Setup(m => m.FindById(It.IsAny<Guid>())).Returns<Guid>(g =>
            {
                if (g == emMachine.Id)
                    return emMachine;

                if (g == fusionMachine.Id)
                    return fusionMachine;

                if (g == printer.Id)
                    return printer;

                return null;
            });

            return mock;
        }

        private EmMachine GetEmMachine()
        {
            var emPhysical = new EmPhysicalMachineSettings()
            {
                TrackParameters = new EmTrackParameters(),
                LongHeadParameters = new EmLongHeadParameters()
            };
            emPhysical.LongHeadParameters.AddLongHead(new EmLongHead() { Number = 1 });
            emPhysical.LongHeadParameters.AddLongHead(new EmLongHead() { Number = 2 });
            emPhysical.LongHeadParameters.AddLongHead(new EmLongHead() { Number = 3 });

            emMachine = new EmMachine() { Id = Guid.NewGuid(), PhysicalMachineSettings = emPhysical, NumTracks = 2};

            return emMachine;
        }

        private FusionMachine GetFusionMachine()
        {
            fusionMachine = new FusionMachine() { Id = Guid.NewGuid(), IpOrDnsName = "127.0.0.1", Port = 82 };

            return fusionMachine;
        }

        private Mock<IUICommunicationService> GetUiCommunicationServiceMock()
        {
            var mock = new Mock<IUICommunicationService>();

            return mock;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ServiceShouldPublishEmAndFusionMachinesOnMessage()
        {
            var responsePublished = false;

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachinePresentationModel<IMachine>>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachinePresentationModel<IMachine>>>, bool, bool>(
                    (msg, publishInternally, b2) =>
                    {
                        Specify.That(msg.MessageType).Should.BeEqualTo(MachineServiceMessages.GetMachinesResponse);
                        Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                        Specify.That(msg.Data.FirstOrDefault(m => m.Id == fusionMachine.Id)).Should.Not.BeNull();
                        Specify.That(msg.Data.FirstOrDefault(m => m.Id == emMachine.Id)).Should.Not.BeNull();
                        Specify.That(msg.Data.FirstOrDefault(m => m.Id == printer.Id)).Should.BeNull();

                        Specify.That(publishInternally).Should.BeFalse();

                        responsePublished = true;
                    });

            bussinessLayer.GetMachines();

            Retry.For(() => responsePublished, TimeSpan.FromSeconds(1));
            Specify.That(responsePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishEmMachineInfoWhenRequestingMachineInfoForEmMachine()
        {
            var responsePublished = false;
            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<EmMachineServiceModel>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<EmMachineServiceModel>, bool, bool>(
                    (m, p, b2) =>
                    {
                        Specify.That(m is ResponseMessage<EmMachineServiceModel>)
                            .Should.BeTrue("Incorrect response message type, expected ResponseMessage<EmMachineServiceModel");
                        Specify.That(p).Should.BeFalse("Should not publish internally");

                        var responseMessage = m as ResponseMessage<EmMachineServiceModel>;
                        var serviceModel = responseMessage.Data;

                        Specify.That(serviceModel.Id)
                            .Should.BeEqualTo(emMachine.Id, "Incorrect machine id in published service model");
                        Specify.That(serviceModel.LongHeads.Count)
                            .Should.BeEqualTo(3, "Incorrect number of longheads in Service Model");
                        Specify.That(serviceModel.Tracks.Count).Should.BeEqualTo(2, "Incorrect number of tracks in Service Model");

                        responsePublished = true;
                    });

            bussinessLayer.GetMachineInfo(emMachine.Id);

            Retry.For(() => responsePublished, TimeSpan.FromSeconds(1));
            Specify.That(responsePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishFusionMachineINfoWhenRequestingMachineInfoForFusionMachine()
        {
            var responsePublished = false;
            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<FusionMachineServiceModel>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<FusionMachineServiceModel>, bool, bool>(
                    (m, p, b2) =>
                    {
                        Specify.That(m is ResponseMessage<FusionMachineServiceModel>)
                            .Should.BeTrue("Incorrect response message type, expected ResponseMessage<FusionMachineServiceModel>");
                        Specify.That(p).Should.BeFalse("Should not publish internally");

                        var responseMessage = m as ResponseMessage<FusionMachineServiceModel>;
                        var serviceModel = responseMessage.Data;

                        Specify.That(serviceModel.Id)
                            .Should.BeEqualTo(fusionMachine.Id, "Incorrect machine id in published service model");

                        responsePublished = true;
                    });

            bussinessLayer.GetMachineInfo(fusionMachine.Id);

            Retry.For(() => responsePublished, TimeSpan.FromSeconds(1));
            Specify.That(responsePublished).Should.BeTrue();

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleCalibratingFusion()
        {
            var handleFusionCalibrationCalled = false;

            calibrationHandlerMock.Setup(
                m => m.HandleFusionCalibration(It.IsAny<FusionMachine>(), It.IsAny<IMachineCommandCommunicator>()))
                .Callback<FusionMachine, IMachineCommandCommunicator>(
                    (m, c) =>
                    {
                        Specify.That(m.Id).Should.BeEqualTo(fusionMachine.Id);
                        handleFusionCalibrationCalled = true;
                    });

            bussinessLayer.GetMachineInfo(fusionMachine.Id);

            fusionMachine.CurrentStatus = MachinePausedStatuses.MachineService;
            fusionMachine.CurrentStatus = MachinePausedStatuses.MachineCalibrating;
            fusionMachine.CurrentStatus = MachinePausedStatuses.MachineService;

            Retry.For(() => handleFusionCalibrationCalled, TimeSpan.FromSeconds(1));
            Specify.That(handleFusionCalibrationCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotDoAnythingWhenCalibrationIsAbortedDueToError()
        {
            var handleFusionCalibrationCalled = false;

            calibrationHandlerMock.Setup(
                m => m.HandleFusionCalibration(It.IsAny<FusionMachine>(), It.IsAny<IMachineCommandCommunicator>()))
                .Callback<FusionMachine, IMachineCommandCommunicator>(
                    (m, c) =>
                    {
                        Specify.That(m.Id).Should.BeEqualTo(fusionMachine.Id);
                        handleFusionCalibrationCalled = true;
                    });

            bussinessLayer.GetMachineInfo(fusionMachine.Id);

            fusionMachine.CurrentStatus = MachinePausedStatuses.MachineService;
            fusionMachine.CurrentStatus = MachinePausedStatuses.MachineCalibrating;
            fusionMachine.CurrentStatus = MachineErrorStatuses.MachineError;

            Retry.For(() => handleFusionCalibrationCalled, TimeSpan.FromSeconds(1));
            Specify.That(handleFusionCalibrationCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotDoAnythingWhenCalibrationIsAbortedDueToEmergencyStop()
        {
            var handleFusionCalibrationCalled = false;

            calibrationHandlerMock.Setup(
                m => m.HandleFusionCalibration(It.IsAny<FusionMachine>(), It.IsAny<IMachineCommandCommunicator>()))
                .Callback<FusionMachine, IMachineCommandCommunicator>(
                    (m, c) =>
                    {
                        Specify.That(m.Id).Should.BeEqualTo(fusionMachine.Id);
                        handleFusionCalibrationCalled = true;
                    });

            bussinessLayer.GetMachineInfo(fusionMachine.Id);

            fusionMachine.CurrentStatus = MachinePausedStatuses.MachineService;
            fusionMachine.CurrentStatus = MachinePausedStatuses.MachineCalibrating;
            fusionMachine.CurrentStatus = MachineErrorStatuses.EmergencyStop;

            Retry.For(() => handleFusionCalibrationCalled, TimeSpan.FromSeconds(1));
            Specify.That(handleFusionCalibrationCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotHandleCommandsForNonCutCreaseMachines()
        {
            bussinessLayer.PerformMachineCommand(MachineCommandMessages.CleanCut, printer.Id, 1);
            communicatorMock.Verify(m => m.PerformMachineCommand(MachineCommandMessages.CleanCut, 1), Times.Never);

            bussinessLayer.PerformMachineCommand(MachineCommandMessages.ForwardFeed, printer.Id, 1);
            communicatorMock.Verify(m => m.PerformMachineCommand(MachineCommandMessages.CleanCut, 1), Times.Never);

            bussinessLayer.PerformMachineCommand(MachineCommandMessages.ReverseFeed, printer.Id, 1);
            communicatorMock.Verify(m => m.PerformMachineCommand(MachineCommandMessages.CleanCut, 1), Times.Never);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleCleanCutCommand()
        {
            bussinessLayer.PerformMachineCommand(MachineCommandMessages.CleanCut, emMachine.Id, 1);
            communicatorMock.Verify(m => m.PerformMachineCommand(MachineCommandMessages.CleanCut, 1), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleFeedForwardCommand()
        {
            bussinessLayer.PerformMachineCommand(MachineCommandMessages.ForwardFeed, emMachine.Id, 1);
            communicatorMock.Verify(m => m.PerformMachineCommand(MachineCommandMessages.CleanCut, 1), Times.Never);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleReverseFeedCommand()
        {
            bussinessLayer.PerformMachineCommand(MachineCommandMessages.ReverseFeed, emMachine.Id, 1);
            communicatorMock.Verify(m => m.PerformMachineCommand(MachineCommandMessages.CleanCut, 1), Times.Never);
        }
    }
}
