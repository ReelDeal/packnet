﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.MachineCommandService;
using Testing.Specificity;

namespace MachineCommandServiceTests
{
    [TestClass]
    public class MachineCommandCommunicatorFactoryTests
    {
        private EmMachine machine;
        private Guid machineId;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<ILogger> loggerMock;        
        private EventAggregator subscriber;
        private IEventAggregatorPublisher publisher;

        [TestInitialize]
        public void Setup()
        {
            loggerMock = new Mock<ILogger>();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();

            machineId = new Guid("31eedff2-b69d-4079-b9d1-b8b9f4ca84fa");

            subscriber = new EventAggregator();
            publisher = subscriber as IEventAggregatorPublisher;

            machine = new EmMachine
            {
                Id = machineId,
                IpOrDnsName = "127.0.0.1",
                Port = 80
            };

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateANewMachineCommandCommunicatorIfItDoesNotExist()
        {
            var factory = new MachineCommandCommunicatorFactory(uiCommunicationServiceMock.Object, publisher, subscriber, loggerMock.Object);

            var communicator = factory.GetInstance(machine);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("127.0.0.1");
            Specify.That(communicator.Port).Should.BeEqualTo(80);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateIpOfMachineCommamndCommunicatorIfMachineIpHasChanged()
        {
            var factory = new MachineCommandCommunicatorFactory(uiCommunicationServiceMock.Object, publisher, subscriber, loggerMock.Object);

            var communicator = factory.GetInstance(machine);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("127.0.0.1");
            Specify.That(communicator.Port).Should.BeEqualTo(80);

            machine.IpOrDnsName = "10.9.8.7";

            communicator = factory.GetInstance(machine);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("10.9.8.7");
            Specify.That(communicator.Port).Should.BeEqualTo(80);

            machine.Port = 90;

            communicator = factory.GetInstance(machine);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("10.9.8.7");
            Specify.That(communicator.Port).Should.BeEqualTo(90);

            machine.Port = 100;
            machine.IpOrDnsName = "10.10.10.10";

            communicator = factory.GetInstance(machine);

            Specify.That(communicator).Should.Not.BeNull();
            Specify.That(communicator.IpAddress).Should.BeEqualTo("10.10.10.10");
            Specify.That(communicator.Port).Should.BeEqualTo(100);
        }
    }
}
