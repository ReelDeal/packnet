﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.MachineCommandService;
using PackNet.MachineCommandService.DTO;

using Track = PackNet.Common.Interfaces.DTO.PhysicalMachine.Track;

namespace MachineCommandServiceTests
{
    [TestClass]
    public class MachineCommandServiceTests
    {
        private Mock<IServiceLocator> serviceLocatorMock;
        private MachineCommandService commandService;
        private Mock<ILogger> logerMock;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IAggregateMachineService> machineServiceMock;
        private EventAggregator aggregator;

        private EmMachine emMachine;
        private FusionMachine fusionMachine;
        private ZebraPrinter printer;

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();

            logerMock = new Mock<ILogger>();
            uiCommunicationServiceMock = GetUiCommunicationServiceMock();
            machineServiceMock = GetMachineServiceMock();
            serviceLocatorMock = GetServiceLocator();

            commandService = new MachineCommandService(serviceLocatorMock.Object);
        }

        private Mock<IAggregateMachineService> GetMachineServiceMock()
        {
            var mock = new Mock<IAggregateMachineService>();

            fusionMachine = new FusionMachine() { Id = Guid.NewGuid() };
            printer = new ZebraPrinter() { Id = Guid.NewGuid() };

            mock.Setup(m => m.Machines).Returns(new List<IMachine>() { GetEmMachine(), fusionMachine, printer });
            mock.Setup(m => m.FindById(It.IsAny<Guid>())).Returns<Guid>(g =>
            {
                if (g == emMachine.Id)
                    return emMachine;

                if (g == fusionMachine.Id)
                    return fusionMachine;

                if (g == printer.Id)
                    return printer;

                return null;
            });

            return mock;
        }

        private EmMachine GetEmMachine()
        {
            var emPhysical = new EmPhysicalMachineSettings()
            {
                TrackParameters = new EmTrackParameters(),
                LongHeadParameters = new EmLongHeadParameters()
            };
            emPhysical.LongHeadParameters.AddLongHead(new EmLongHead(){Number = 1});
            emPhysical.LongHeadParameters.AddLongHead(new EmLongHead(){Number = 2});
            emPhysical.LongHeadParameters.AddLongHead(new EmLongHead() { Number = 3 });
            
            emMachine = new EmMachine() { Id = Guid.NewGuid(), PhysicalMachineSettings = emPhysical, NumTracks = 2};

            return emMachine;
        }

        private Mock<IUICommunicationService> GetUiCommunicationServiceMock()
        {
            var mock = new Mock<IUICommunicationService>();

            return mock;
        }

        private Mock<IServiceLocator> GetServiceLocator()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(aggregator);
            mock.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);
            mock.Setup(m => m.Locate<ILogger>()).Returns(logerMock.Object);
            mock.Setup(m => m.Locate<IAggregateMachineService>()).Returns(machineServiceMock.Object);
            mock.Setup(m => m.Locate<IUICommunicationService>()).Returns(uiCommunicationServiceMock.Object);

            return mock;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRegisterCorrectMessagesOnUiComService()
        {
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<Guid>(MachineServiceMessages.GetMachineInfo));
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator(MachineServiceMessages.GetMachines));
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<Command>(MachineServiceMessages.PerformCommand));
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<MachineCommand>(MachineCommandMessages.ForwardFeed));
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<MachineCommand>(MachineCommandMessages.CleanCut));
            uiCommunicationServiceMock.Verify(m => m.RegisterUIEventWithInternalEventAggregator<MachineCommand>(MachineCommandMessages.ReverseFeed));
        }
    }
}
