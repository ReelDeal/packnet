﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;
using PackNet.MachineCommandService;
using PackNet.MachineCommandService.DTO;

using Testing.Specificity;

namespace MachineCommandServiceTests
{

    [TestClass]
    public class MachineCommandCommunicatorTests
    {
        private EmMachine machine;
        private IMachineCommandCommunicator communicator;

        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IWebRequestCreator> webRequestCreatorMock;
        private Mock<ILogger> loggerMock;
        private EventAggregator aggregator;
        private List<Tuple<PacksizePlcVariable, object>> writtenValues;

        [TestInitialize]
        public void Setup()
        {
            webRequestCreatorMock = GetWebRequestCreatorMock();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
            loggerMock = new Mock<ILogger>();

            machine = new EmMachine()
            {
                Id = Guid.NewGuid(),
                IpOrDnsName = "127.0.0.1"
            };

            aggregator = new EventAggregator();
            writtenValues = new List<Tuple<PacksizePlcVariable, object>>();
            communicator = new MachineCommandCommunicator(machine, webRequestCreatorMock.Object, uiCommunicationServiceMock.Object, aggregator,
                loggerMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void CleanCutSetsExpectedValues()
        {
            communicator.PerformMachineCommand(MachineCommandMessages.CleanCut, 1);

            Retry.For(() => writtenValues.Count.Equals(4), TimeSpan.FromSeconds(1));

            Specify.That(writtenValues.Count).Should.BeEqualTo(4);
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceCommand,
                MachineServiceCommands.CleanCut.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServicePart, ServicePart.NotApplicable.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceUnitNumber, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void FeedForwardSetsExpectedValues()
        {
            communicator.PerformMachineCommand(MachineCommandMessages.ForwardFeed, 1);

            Retry.For(() => writtenValues.Count.Equals(5), TimeSpan.FromSeconds(1));

            Specify.That(writtenValues.Count).Should.BeEqualTo(5);
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceCommand, MachineServiceCommands.Track.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServicePart, ServicePart.Feed.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceToolState, ServiceToolState.Forward.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceUnitNumber, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ReverseFeedSetsExpectedValues()
        {
            communicator.PerformMachineCommand(MachineCommandMessages.ReverseFeed, 1);

            Retry.For(() => writtenValues.Count.Equals(5), TimeSpan.FromSeconds(1));

            Specify.That(writtenValues.Count).Should.BeEqualTo(5);
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceCommand, MachineServiceCommands.Track.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServicePart, ServicePart.Feed.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceToolState, ServiceToolState.Backwards.Value)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceUnitNumber, 1)).Should.BeTrue();
            Specify.That(VerifyWrittenValue(MachineCommandVariableMap.Instance.ServiceCommandExecute, true)).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishCommandSentToMachineToUI()
        {
            var eventPublished = false;

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<ResponseMessage<Guid>>(), false, It.IsAny<bool>())).Callback<IMessage<Guid>, bool, bool>(
                (msg, publisInternally, b2) =>
                {
                    var message = msg as ResponseMessage<Guid>;
                    if (message == null)
                        return;

                    Specify.That(message.Result).Should.BeLogicallyEqualTo(ResultTypes.Success);
                    Specify.That(message.MessageType).Should.BeLogicallyEqualTo(MachineCommandMessages.MachineCommandResponse);
                    Specify.That(message.Data).Should.BeLogicallyEqualTo(machine.Id);

                    eventPublished = true;
                });
            
            communicator.PerformMachineCommand(MachineCommandMessages.ReverseFeed, 1);

            Retry.For(() => eventPublished, TimeSpan.FromSeconds(1));
            Specify.That(eventPublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishCommandDoneToUI()
        {
            var eventPublished = false;

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<Guid>>(), false, It.IsAny<bool>())).Callback<IMessage<Guid>, bool, bool>(
             (message, publisInternally, b2) =>
             {
                 if (message.MessageType != MachineCommandMessages.MachineCommandDoneResponse)
                     return;

                 Specify.That(message).Should.Not.BeNull();
                 Specify.That(message.MessageType).Should.BeLogicallyEqualTo(MachineCommandMessages.MachineCommandDoneResponse);
                 Specify.That(message.Data).Should.BeLogicallyEqualTo(machine.Id);

                 eventPublished = true;
             });

            communicator.PerformMachineCommand(MachineCommandMessages.ReverseFeed, 1);

            Retry.For(() => eventPublished, TimeSpan.FromSeconds(1));
            Specify.That(eventPublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAllowServiceMode_WhenMachineIsPaused()
        {
            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;
            var called = false;
            webRequestCreatorMock.Setup(
                s => s.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), true)).Returns(WebRequest.Create(new Uri("http://127.0.0.1:80"))).Callback(
                    () =>
                    {
                        called = true;       
                    });

            try
            {
                communicator.HandleServiceMode(ServiceToolState.Activate);
            }
            catch (Exception)
            {
                // We get an exception because we can't communicate with the plc
                // All that matters is that we try when the machine is paused
            }
            
            Retry.For(() => called, TimeSpan.FromSeconds(5));
            Specify.That(called).Should.BeTrue("Should toggle servicemode when machines is paused");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAllowServiceMode_WhenMachineIsInError()
        {
            machine.CurrentStatus = MachineErrorStatuses.MachineError;
            var called = false;
            webRequestCreatorMock.Setup(
                s => s.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), true)).Returns(WebRequest.Create(new Uri("http://127.0.0.1:80"))).Callback(
                    () =>
                    {
                        called = true;
                    });

            try
            {
                communicator.HandleServiceMode(ServiceToolState.Activate);
            }
            catch (Exception)
            {
                // We get an exception because we can't communicate with the plc
                // All that matters is that we try when the machine is in error
            }

            Retry.For(() => called, TimeSpan.FromSeconds(5));
            Specify.That(called).Should.BeTrue("Should toggle servicemode when machines is in error");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAllowServiceMode_WhenMachineIsInEStop()
        {
            machine.CurrentStatus = MachineErrorStatuses.EmergencyStop;
            var called = false;
            webRequestCreatorMock.Setup(
                s => s.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), true)).Returns(WebRequest.Create(new Uri("http://127.0.0.1:80"))).Callback(
                    () =>
                    {
                        called = true;
                    });

            try
            {
                communicator.HandleServiceMode(ServiceToolState.Activate);
            }
            catch (Exception)
            {
                // We get an exception because we can't communicate with the plc
                // All that matters is that we try when the machine is in error
            }

            Retry.For(() => called, TimeSpan.FromSeconds(5));
            Specify.That(called).Should.BeTrue("Should toggle servicemode when machines is in error");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotAllowServiceMode_WhenMachineIsUnpaused()
        {
            machine.CurrentStatus = MachineStatuses.MachineOnline;
            var notCalled = true;
            webRequestCreatorMock.Setup(
                s => s.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), true)).Returns(WebRequest.Create(new Uri("http://127.0.0.1:80"))).Callback(
                    () =>
                    {
                        notCalled = false;
                    });

            try
            {
                communicator.HandleServiceMode(ServiceToolState.Activate);
            }
            catch (Exception)
            {
                // We get an exception because we can't communicate with the plc
                // All that matters is that we don't try when the machine is unpaused
            }

            Retry.For(() => notCalled, TimeSpan.FromSeconds(5));
            Specify.That(notCalled).Should.BeTrue("Should not toggle servicemode when machines is online");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldToggleServiceMode_WhemMachineIsInServiceMode()
        {
            machine.CurrentStatus = MachinePausedStatuses.MachineService;
            var called = false;
            webRequestCreatorMock.Setup(
                s => s.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), true)).Returns(WebRequest.Create(new Uri("http://127.0.0.1:80"))).Callback(
                    () =>
                    {
                        called = true;
                    });

            try
            {
                communicator.HandleServiceMode(ServiceToolState.Deactivate);
            }
            catch (Exception)
            {
                // We get an exception because we can't communicate with the plc
                // All that matters is that we don't try when the machine is unpaused
            }

            Retry.For(() => called, TimeSpan.FromSeconds(5));
            Specify.That(called).Should.BeTrue("Should toggle servicemode when machines is in service mode");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotActivateServiceMode_WhemMachineIsInServiceMode()
        {
            machine.CurrentStatus = MachinePausedStatuses.MachineService;
            var notCalled = true;
            webRequestCreatorMock.Setup(
                s => s.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), true)).Returns(WebRequest.Create(new Uri("http://127.0.0.1:80"))).Callback(
                    () =>
                    {
                        notCalled = false;
                    });

            try
            {
                communicator.HandleServiceMode(ServiceToolState.Activate);
            }
            catch (Exception)
            {
                // We get an exception because we can't communicate with the plc
                // All that matters is that we don't try when the machine is unpaused
            }

            Retry.For(() => notCalled, TimeSpan.FromSeconds(5));
            Specify.That(notCalled).Should.BeTrue("Should not activate servicemode when machines is in service mode");
        }

#if DEBUG
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldOnlyHandleOneCommandAtATime()
        {
            var exceptionThrown = false;
            webRequestCreatorMock.Setup(ws=> ws.CreatePostRequest(It.IsAny<PacksizePlcVariable>(),It.IsAny<object>())).Callback((PacksizePlcVariable v,object o)=>{ Thread.Sleep(1000); });
            Task.Factory.StartNew(()=> communicator.PerformMachineCommand(MachineCommandMessages.ReverseFeed, 1));
            try
            {
                
                Task.Factory.StartNew(() => communicator.PerformMachineCommand(MachineCommandMessages.ReverseFeed, 1));
            }
            catch (Exception e)
            {
                Specify.That(e.Message).Should.BeLogicallyEqualTo("Machine command already in progress");
                exceptionThrown = true;
            }
         
            Specify.That(exceptionThrown).Should.BeTrue();
        }
#endif

        private bool VerifyWrittenValue(PacksizePlcVariable variable, object value)
        {
            return writtenValues.Any(writtenValue => writtenValue.Item1.Equals(variable) && writtenValue.Item2.Equals(value));
        }

        private Mock<IWebRequestCreator> GetWebRequestCreatorMock()
        {
            var mock = new Mock<IWebRequestCreator>();

            mock.Setup(m => m.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), It.IsAny<object>()))
                .Returns<PacksizePlcVariable, object>((variable, value) =>
                {
                    writtenValues.Add(new Tuple<PacksizePlcVariable, object>(variable, value));
                    return new WebRequestTester(true);
                });

            mock.Setup(m => m.CreateGetRequest(MachineCommandVariableMap.Instance.MachineCommandDone))
                .Returns(new WebRequestTester(true));

            mock.Setup(m => m.ServerAddress).Returns("127.0.0.1");
            return mock;
        }

        public class WebRequestTester : WebRequest
        {
            readonly bool value;

            public WebRequestTester(bool value)
            {
                this.value = value;
            }

            public override WebResponse GetResponse()
            {
                return new WebResponseTester(value);
            }
        }

        public class WebResponseTester : WebResponse
        {
            readonly bool value;

            public WebResponseTester(bool value)
            {
                this.value = value;

            }

            public override Stream GetResponseStream()
            {
                return ("{" + value.ToString().ToLower() + "}").ToStream();
            }

            public override void Close()
            {
            }
        }
    }
}
