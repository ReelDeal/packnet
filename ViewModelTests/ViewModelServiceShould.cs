﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;

using Packnet.Plugin.ViewModels;

using Testing.Specificity;

namespace ViewModelTests
{
    [TestClass]
    public class ViewModelServiceShould
    {
        [TestMethod]
        public void ConvertCarton()
        {
            var objectInTest = new ViewModelService();
            var corrugate = new Corrugate
            {
                Alias = "Asd",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = .16,
                Width = 17.88
            };
            var cartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate, TileCount = 1 };
            var kit = new Kit { CustomerUniqueId = "Kit333", Id = Guid.NewGuid() };
            var label = new Label { CustomerUniqueId = "Label44" };
            label.Restrictions.Add(new BasicRestriction<Template>(new Template{Name = "myTemplate"}));
            kit.AddProducible(label);
            var carton = new Carton { CustomerUniqueId = "Carton55", Length = 10, Width = 20, Height = 30, CartonOnCorrugate = cartonOnCorrugate };
            kit.AddProducible(carton);
            //set intial history values
            label.ProducedOnMachineGroupId = Guid.NewGuid();
            label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            carton.ProducedOnMachineGroupId = Guid.NewGuid();
            carton.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
            kit.ProducedOnMachineGroupId = Guid.NewGuid();
            kit.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;


            Tuple<DateTime,JObject> result = objectInTest.Convert(carton);
            Specify.That(result.Item2["CustomerUniqueId"]).Should.Not.BeNull();

            result = objectInTest.Convert(label);
            Specify.That(result.Item2["Template"].ToString()).Should.BeEqualTo("myTemplate");

            result = objectInTest.Convert(kit);
            Specify.That(result.Item2["CustomerUniqueId"].ToString()).Should.BeEqualTo("Kit333");


        }
    }
}
