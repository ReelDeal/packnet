﻿using System;
using System.Collections.Generic;
using System.Linq;
using PackNet.Server.Bootstrapper;
using Newtonsoft.Json;
using System.Windows.Forms;
using PackNet.Data.PostgresDBSetup;

namespace ArticleImportExport
{
    using System.Globalization;
    using System.IO;

    using Npgsql;

    

    internal class Program
    {
        private static readonly NpgsqlConnection Conn =
            new NpgsqlConnection(@"Server=localhost;database=runtime;User ID=postgres;Password=trazan123@;");

        private static int duplicates;

        private static int added;

        private static int total;

        private static int failed;

        private static bool update = true;

        private static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings {
                                                                                        Culture = CultureInfo.InvariantCulture
                                                                                    };

        private static bool dontWaitToExit;

        private static void Main(string[] args)
        {
            if (args.Select(a => a.ToLower()).Any(a => a == "-nowait"))
            {
                dontWaitToExit = true;
                args = args.Select(a => a.ToLower()).Where(a => a != "-nowait").ToArray();
            }

            //TODO: Why the hell did we do this???
            // Create Article Database in the event it hasn't been created yet.
            var bootstrapper = new Bootstrapper(new RegistryHelper(), new ServiceHelper(), new MachineHelper(), new HistoryDatabaseSetup(new DatabaseSetup()), new RuntimeDatabaseSetup(new DatabaseSetup()));
            bootstrapper.ValidateDatabase();

            if (args.Count() == 2)
            {
                switch (args[0])
                {
                    case "-export":
                        ExportArticles(args[1]);
                        break;
                    case "-uniqueimport":
                        update = false;
                        ImportArticles(args[1]);
                        break;
                    case "-import":
                        ImportArticles(args[1]);
                        break;
                    default:
                        InvalidArguments();
                        break;
                }
            }
            else InvalidArguments();
            Conn.Dispose();
            if (dontWaitToExit)
                return;
            Console.WriteLine();
            Console.Write("Press any key to exit ...");
            Console.ReadKey();
        }

        private static void ExportArticles(string exportFile)
        {
            try
            {
                var file = new FileInfo(exportFile);
                var tempFile = new FileInfo(Path.Combine(Application.CommonAppDataPath, file.Name));
                Console.WriteLine("Exporting to '{0}'", file.FullName);
                var sql = string.Format(
                    "COPY articles TO '{0}' WITH DELIMITER ';'", tempFile.FullName.Replace("\\", "\\\\"));
                // Needs double backslashes because of how COPY works. 

                try
                {
                    ExecuteSql("runtime", sql);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error exporting data to file: {0}, error {1}", file, e);
                }

                ParseExportDataToJsonAndSaveToFile(tempFile.FullName, file.FullName);
                File.Delete(tempFile.FullName);
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not write to file at '{0}'", exportFile);
                Console.WriteLine(e.Message);
                Console.WriteLine("If you get a \"permission denied\" error, please restart this tool as an administrator.");
            }
        }

        private static void ParseExportDataToJsonAndSaveToFile(string tempFile, string saveFile)
        {
            using (
                var reader = new StreamReader(new FileStream(tempFile, FileMode.Open, FileAccess.Read, FileShare.Read)))
            {
                using (
                    var writer =
                        new StreamWriter(
                            new FileStream(saveFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read)))
                {
                    if (reader.EndOfStream)
                    {
                        Console.WriteLine("No rows were exported from the database. Is it empty?");
                    }
                    else
                    {
                        var writtenHeaders = false;
                        var counter = 0;
                        do
                        {
                            var articleEntry = reader.ReadLine();
                            if (articleEntry != null)
                            {
                                var parts = articleEntry.Split(';');
                                var raw = parts[1].Replace("\\\\", "\\");

                                var jsonData = new Dictionary<string, string>();
                                try
                                {
                                    jsonData = JsonConvert.DeserializeObject<Dictionary<string, string>>(raw, JsonSerializerSettings);

                                }
                                catch (Exception e)
                                {
                                    Console.WriteLine("Error JSON converting {0}, error {1}", raw, e);
                                }

                                if (!jsonData.ContainsKey("MachineType")) jsonData.Add("MachineType", "");

                                if (!writtenHeaders)
                                {
                                    var headers = jsonData.Keys.Aggregate("", (current, key) => current + (";" + key));
                                    writer.WriteLine("RowId" + headers);
                                    writtenHeaders = true;
                                }
                                writer.Write(parts[0]);
                                foreach (var key in jsonData.Keys)
                                {
                                    writer.Write(";" + jsonData[key]);
                                }
                                writer.WriteLine();
                                counter++;
                            }
                        }
                        while (!reader.EndOfStream);
                        Console.WriteLine("Done! Exported {0} rows from the database.", counter);
                    }
                }
            }
            File.Delete(tempFile);
        }

        private static void ImportArticles(string importFile)
        {
            var path = "";
            duplicates = 0;
            added = 0;
            total = 0;
            failed = 0;

            if ((path = GetAbsolutePath(importFile)) == String.Empty)
            {
                Console.WriteLine("Could not find absolute path to '{0}'", importFile);
                return;
            }

            Console.WriteLine("Importing from '{0}'", path);
            try
            {
                Conn.Open();

                using (var file = new StreamReader(path))
                {
                    var article = file.ReadLine();
                    var headers = article;
                    do
                    {
                        ImportArticle(file.ReadLine(), headers);
                        Console.SetCursorPosition(0, Console.CursorTop);
                        Console.Write("Lines read: {0}", total);
                    } while (!String.IsNullOrWhiteSpace(article) && !file.EndOfStream);

                    Console.WriteLine(
                        "\r\n\tImported: {0}\r\n\t{1}: {2}\r\n\tFailed: {3}",
                        added,
                        update ? "Duplicates (updated)" : "Duplicates (unchanged)",
                        duplicates,
                        failed);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                Conn.Close();
            }
        }

        private static void ImportArticle(string input, string headers)
        {
            if (input == null) return;

            var article = ParseImportDataToJsonImportData(input.Split(';'), headers);

            total++;

            var row = article.Split(new[] { ';' }, 2);
            var articleId = row[0];
            var articleData = row[1];
            try
            {
                var sql = string.Format("INSERT INTO articles (id, data) VALUES ('{0}','{1}');", articleId, articleData);
                ExecuteSql("runtime", sql);
                added++;
            }
            catch (Exception e)
            {
                if (e.Message.Contains("ERROR: 23505:"))
                {
                    if (update)
                    {
                        try
                        {
                            var sql = string.Format("UPDATE articles SET data='{0}' WHERE id='{1}';", articleData, articleId);
                            ExecuteSql("runtime", sql);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error updating article id: {0}, error: {1}", articleId, ex);
                        }
                    }

                    duplicates++;
                }
                else
                {
                    Console.WriteLine("Error inserting article id: {0}, error: {1}", articleId, e);
                    failed++;
                    Console.WriteLine(e.Message);
                }

            }
        }

        private static string ParseImportDataToJsonImportData(string[] importData, string headers)
        {
            var returnValue = importData[0] + ";";
            var realHeaders = headers.Split(';');

            if (realHeaders.Count() == importData.Count() || realHeaders.Count() == (importData.Count() + 1))
            {
                var jsonDict = new Dictionary<string, object>();
                for (var i = 1; i < realHeaders.Count(); i++)
                {
                    jsonDict.Add(realHeaders[i], importData.Count() > i ? importData[i] : "");
                }
                var json = JsonConvert.SerializeObject(jsonDict)/*.Replace("\"", "\"\"")+"\""*/;
                return returnValue + json;
            }
            throw new InvalidDataException(
                string.Format(
                    "The number of headers found {0} does not equal the number of data fields {1}.",
                    realHeaders.Count(),
                    importData.Count()));
        }

        private static string GetAbsolutePath(string path)
        {
            try
            {
                var p = Path.GetFullPath(path);
                return p;
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        private static void InvalidArguments()
        {
            Console.WriteLine(
                "Invalid arguments. Valid arguments are:\r\n"
                + "ArticleImportExport.exe -import <path-to-import-file> <-- This will import and update if they already exist.\r\n"
                + "ArticleImportExport.exe -uniqueimport <path-to-import-file> <-- This will skip rows that already exist in the database.\r\n"
                + "ArticleImportExport.exe -export <path-to-export-file> <-- This will export all articles in the database to the file specified.");
        }

        private static void ExecuteSql(string databaseName, string sql)
        {
            using (var command = new NpgsqlCommand(string.Format("{0};", sql), Conn))
            {
                command.Connection.ChangeDatabase(databaseName);
                command.ExecuteNonQuery();
            }
        }
    }
}
