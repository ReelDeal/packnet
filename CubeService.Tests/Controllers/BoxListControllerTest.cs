﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CubeService.Controllers;

namespace CubeService.Tests.Controllers
{
    [TestClass]
    public class BoxListControllerTest
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void Get()
        {
            var controller = new BoxListController();

            var result = controller.Get(4.0f, 4.0f, 6.0f, 5.0f, 5.0f, 7.0f, 1.0f).ToList();
            //Expected
            //4.0, 4.0, 6.0
            //4.0, 4.0, 7.0
            //4.0, 5.0, 6.0
            //4.0, 5.0, 7.0
            //5.0, 4.0, 6.0
            //5.0, 4.0, 7.0
            //5.0, 5.0, 6.0
            //5.0, 5.0, 7.0
            Assert.AreEqual(8, result.Count);
            Assert.AreEqual(4.0, result[0].Length);
            Assert.AreEqual(4.0, result[0].Width);
            Assert.AreEqual(6.0, result[0].Height);

            Assert.AreEqual(4.0, result[1].Length);
            Assert.AreEqual(4.0, result[1].Width);
            Assert.AreEqual(7.0, result[1].Height);

            Assert.AreEqual(4.0, result[2].Length);
            Assert.AreEqual(5.0, result[2].Width);
            Assert.AreEqual(6.0, result[2].Height);

            Assert.AreEqual(4.0, result[3].Length);
            Assert.AreEqual(5.0, result[3].Width);
            Assert.AreEqual(7.0, result[3].Height);

            Assert.AreEqual(5.0, result[4].Length);
            Assert.AreEqual(4.0, result[4].Width);
            Assert.AreEqual(6.0, result[4].Height);

            Assert.AreEqual(5.0, result[5].Length);
            Assert.AreEqual(4.0, result[5].Width);
            Assert.AreEqual(7.0, result[5].Height);

            Assert.AreEqual(5.0, result[6].Length);
            Assert.AreEqual(5.0, result[6].Width);
            Assert.AreEqual(6.0, result[6].Height);

            Assert.AreEqual(5.0, result[7].Length);
            Assert.AreEqual(5.0, result[7].Width);
            Assert.AreEqual(7.0, result[7].Height);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void GetWithMaxWeightAndFill()
        {
            var controller = new BoxListController();

            var result = controller.Get(4.0f, 4.0f, 6.0f, 5.0f, 5.0f, 7.0f, 1.0f, 100f, 100f).ToList();
            //Expected
            //4.0, 4.0, 6.0
            //4.0, 4.0, 7.0
            //4.0, 5.0, 6.0
            //4.0, 5.0, 7.0
            //5.0, 4.0, 6.0
            //5.0, 4.0, 7.0
            //5.0, 5.0, 6.0
            //5.0, 5.0, 7.0
            Assert.AreEqual(8, result.Count);
            Assert.AreEqual(4.0, result[0].Length);
            Assert.AreEqual(4.0, result[0].Width);
            Assert.AreEqual(6.0, result[0].Height);

            Assert.AreEqual(4.0, result[1].Length);
            Assert.AreEqual(4.0, result[1].Width);
            Assert.AreEqual(7.0, result[1].Height);

            Assert.AreEqual(4.0, result[2].Length);
            Assert.AreEqual(5.0, result[2].Width);
            Assert.AreEqual(6.0, result[2].Height);

            Assert.AreEqual(4.0, result[3].Length);
            Assert.AreEqual(5.0, result[3].Width);
            Assert.AreEqual(7.0, result[3].Height);

            Assert.AreEqual(5.0, result[4].Length);
            Assert.AreEqual(4.0, result[4].Width);
            Assert.AreEqual(6.0, result[4].Height);

            Assert.AreEqual(5.0, result[5].Length);
            Assert.AreEqual(4.0, result[5].Width);
            Assert.AreEqual(7.0, result[5].Height);

            Assert.AreEqual(5.0, result[6].Length);
            Assert.AreEqual(5.0, result[6].Width);
            Assert.AreEqual(6.0, result[6].Height);

            Assert.AreEqual(5.0, result[7].Length);
            Assert.AreEqual(5.0, result[7].Width);
            Assert.AreEqual(7.0, result[7].Height);
        }
    }

    //[TestMethod]
    //public void GetById()
    //{
    //    // Arrange
    //    ValuesController controller = new ValuesController();

    //    // Act
    //    string result = controller.Get(5);

    //    // Assert
    //    Assert.AreEqual("value", result);
    //}

    //[TestMethod]
    //public void Post()
    //{
    //    // Arrange
    //    ValuesController controller = new ValuesController();

    //    // Act
    //    controller.Post("value");

    //    // Assert
    //}

    //[TestMethod]
    //public void Put()
    //{
    //    // Arrange
    //    ValuesController controller = new ValuesController();

    //    // Act
    //    controller.Put(5, "value");

    //    // Assert
    //}

    //[TestMethod]
    //public void Delete()
    //{
    //    // Arrange
    //    ValuesController controller = new ValuesController();

    //    // Act
    //    controller.Delete(5);

    //    // Assert
    //}
    //}
}
