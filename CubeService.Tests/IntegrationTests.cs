﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CubeService.Tests
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;

    using Models;

    using CubeTests;

    using PackNet.Cube;
    using PackNet.Cube.DTO;

    using Testing.Specificity;

    [TestClass]
    [Ignore]
    public class IntegrationTests
    {
        [TestMethod]
        [TestCategory("Integration")]
        public void TestMethod1()
        {
            var orderItem1 = new OrderLineItem
            {
                Id = "orderItem1",
                Quantity = 1,
                Weight = 1f,
                OrderId = "order1",
                Length = 10f,
                Width = 10f,
                Height = 15f,
            };
            var orderItem2 = new OrderLineItem
            {
                Id = "orderItem2",
                Quantity = 1,
                Weight = 1f,
                OrderId = "order1",
                Length = 10f,
                Width = 10f,
                Height = 12f,
            };
            var itemsToCube = new List<OrderLineItem> { orderItem1, orderItem2 };

            var cubeParameters = new CubeParameters { GenerateBoxList = true, ItemsToCube = itemsToCube, Increment = 1f, MinimumLength = 20f, MinimumWidth = 10f, MinimumHeight = 10f, MaximumLength = 30f, MaximumWidth = 30f, MaximumHeight = 30f, MaximumWeight = 10f};

            var client = new HttpClient { BaseAddress = new Uri("http://localhost:64416/") };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // Create the JSON formatter.
            var jsonFormatter = new JsonMediaTypeFormatter();

            // Use the JSON formatter to create the content of the request body.
            var content = new ObjectContent<CubeParameters>(cubeParameters, jsonFormatter);

            // Send the request.
            var response = client.PostAsync("api/cube", content).Result;

            var result = response.Content.ReadAsStringAsync().Result;
            var calculated = result.CubingResultsListFromJson();

            Specify.That(calculated.Count()).Should.BeEqualTo(1);
            Specify.That(calculated.First().Order.LineItems.Count()).Should.BeEqualTo(2);
            Specify.That(calculated.First().Order.SelectedBoxes.Count()).Should.BeEqualTo(1);
            Specify.That(calculated.First().Order.SelectedBoxes.First().StringDimension).Should.BeEqualTo("27 X 10 X 10");
            Specify.That(calculated.First().Order.SelectedBoxes.First().Id).Should.BeEqualTo("3088");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void TestMethod2()
        {
            var orderItem1 = new OrderLineItem
            {
                Id = "orderItem1",
                Quantity = 1,
                Weight = 1f,
                OrderId = "order1",
                Length = 323f,
                Width = 242f,
                Height = 44f,
            };
            var orderItem2 = new OrderLineItem
            {
                Id = "orderItem2",
                Quantity = 1,
                Weight = 1f,
                OrderId = "order1",
                Length = 293f,
                Width = 242f,
                Height = 52f,
            };
            var itemsToCube = new List<OrderLineItem> { orderItem1, orderItem2 };
            var boxList = CubeOrderTests.BoxFileContents.ToList();

            var cubeParameters = new CubeParameters {ItemsToCube = itemsToCube, BoxList = boxList};

            var client = new HttpClient { BaseAddress = new Uri("http://localhost:64416/") };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            // Create the JSON formatter.
            var jsonFormatter = new JsonMediaTypeFormatter();

            // Use the JSON formatter to create the content of the request body.
            var content = new ObjectContent<CubeParameters>(cubeParameters, jsonFormatter);

            // Send the request.
            var response = client.PostAsync("api/cube", content).Result;

            var result = response.Content.ReadAsStringAsync().Result;
            var calculated = result.CubingResultsListFromJson();

            Specify.That(calculated.Count()).Should.BeEqualTo(1);
            Specify.That(calculated.First().Order.LineItems.Count()).Should.BeEqualTo(2);
            Specify.That(calculated.First().Order.SelectedBoxes.Count()).Should.BeEqualTo(1);
            Specify.That(calculated.First().Order.SelectedBoxes.First().StringDimension).Should.BeEqualTo("340 X 260 X 100");
            Specify.That(calculated.First().Order.SelectedBoxes.First().Id).Should.BeEqualTo("524");
        }
    }
}
