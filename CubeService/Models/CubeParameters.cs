﻿using System.Collections.Generic;

namespace CubeService.Models
{
    using PackNet.Cube.DTO;
    using PackNet.Cube.Enums;

    /// <summary>
    /// Cubing parameters
    /// </summary>
    public class CubeParameters
    {
        public CubeParameters()
        {
            PreferredLength = 999f;
            CubeSpeed = CubeSpeed.BestMatch;
            FillPercentage = 100f;
        }

        /// <summary>
        /// The list of items to cube
        /// </summary>
        public List<OrderLineItem> ItemsToCube { get; set; }

        /// <summary>
        /// The list of available boxes to select from
        /// </summary>
        public List<BoxDefinition> BoxList { get; set; }

        /// <summary>
        /// The cube speed setting to use. This setting controls quality as well as speed
        /// </summary>
        public CubeSpeed CubeSpeed { get; set; }

        /// <summary>
        /// True if the box list should be generated
        /// </summary>
        public bool GenerateBoxList { get; set; }

        /// <summary>
        /// Minimum length of the box to be used to generate a box list
        /// </summary>
        public float MinimumLength { get; set; }

        /// <summary>
        /// Minimum width of the box to be used to generate a box list
        /// </summary>
        public float MinimumWidth { get; set; }

        /// <summary>
        /// Minimum height of the box to be used to generate a box list
        /// </summary>
        public float MinimumHeight { get; set; }

        /// <summary>
        /// Maximum length of the box to be used to generate a box list
        /// </summary>
        public float MaximumLength { get; set; }

        /// <summary>
        /// Maximum width of the box to be used to generate a box list
        /// </summary>
        public float MaximumWidth { get; set; }

        /// <summary>
        /// Maximum height of the box to be used to generate a box list
        /// </summary>
        public float MaximumHeight { get; set; }

        /// <summary>
        /// Increment to be used when generating a box list
        /// </summary>
        public float Increment { get; set; }

        /// <summary>
        /// Maximum weight to be used when generating a box list
        /// </summary>
        public float MaximumWeight  { get; set; }

        /// <summary>
        /// Percentage of box to fill when generating a box list 
        /// </summary>
        public float FillPercentage { get; set; }

        /// <summary>
        /// If a number of small items are going to be created, the will grow Lengthwise first. Setting the preferred length will cause the length to stay below the setting and width and height will grow. This is useful for environments with very small items and creating items that are restricted to a certain dimension based on a conveyor system.
        /// </summary>
        public float PreferredLength { get; set; }
    }
}