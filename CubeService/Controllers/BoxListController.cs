﻿using System.Collections.Generic;
using System.Web.Http;

namespace CubeService.Controllers
{
    using PackNet.Cube;
    using PackNet.Cube.DTO;

    /// <summary>
    /// Generates box lists for use with PackNet.Cube
    /// </summary>
    public class BoxListController : ApiController
    {
        // GET api/boxlist?minLength=4.0&minWidth=4.0&minHeight=6.0&maxLength=5.0&maxWidth=5.0&maxHeight=7.0&increment=1.0
        /// <summary>
        /// Retrieves an incremented list of box definitions based on the given min dimensions, max dimensions, and increment
        /// </summary>
        /// <param name="minLength">Smallest length acceptable for a box</param>
        /// <param name="minWidth">Smallest width acceptable for a box</param>
        /// <param name="minHeight">Smallest height acceptable for a box</param>
        /// <param name="maxLength">Maximum length acceptable for a box</param>
        /// <param name="maxWidth">Maximum Width acceptable for a box</param>
        /// <param name="maxHeight">Maximum height acceptable for a box</param>
        /// <param name="increment">Increment set for growth of each Dimention in Length, Width and Height, for example if Lengtth = 4 and the increment is set to 1 , the next possible usabele box dimentsion will be Length=5</param>
        /// <returns></returns>
        public IEnumerable<BoxDefinition> Get(
            [FromUri] float minLength, [FromUri] float minWidth, [FromUri] float minHeight,
            [FromUri] float maxLength, [FromUri] float maxWidth, [FromUri] float maxHeight,
            [FromUri] float increment
            )
        {
            return Get(minLength, minWidth, minHeight, maxLength, maxWidth, maxHeight, increment, 100, 100);
        }

        // GET api/boxlist?minLength=4.0&minWidth=4.0&minHeight=6.0&maxLength=5.0&maxWidth=5.0&maxHeight=7.0&increment=1.0&maxWeight=100&fillPercentage=100
        /// <summary>
        /// Retrieves an incremented list of box definitions based on the given min dimensions, max dimensions, increment, Max Weight and Fill percentage.
        /// </summary>
        /// <param name="minLength">Smallest length acceptable for a box</param>
        /// <param name="minWidth">Smallest width acceptable for a box</param>
        /// <param name="minHeight">Smallest height acceptable for a box</param>
        /// <param name="maxLength">Maximum length acceptable for a box</param>
        /// <param name="maxWidth">Maximum Width acceptable for a box</param>
        /// <param name="maxHeight">Maximum height acceptable for a box</param>
        /// <param name="increment">Increment set for growth of each Dimention in Length, Width and Height, for example if Length = 4 and the increment is set to 1 , the next possible usabele box dimentsion will be Length=5</param>
        /// <param name="maxWeight">Maximum Weight acceptable for a box</param>
        /// <param name="fillPercentage">Percent that the Box will be set as Full <br />Example if fillPercentage = 80, The box will have 20% void space</param>
        /// <returns></returns>
public IEnumerable<BoxDefinition> Get(
            [FromUri] float minLength, [FromUri] float minWidth, [FromUri] float minHeight,
            [FromUri] float maxLength, [FromUri] float maxWidth, [FromUri] float maxHeight,
            [FromUri] float increment,
            [FromUri] float maxWeight,
            [FromUri] float fillPercentage
            )
        {
            var minimumDimentions = new BoxDefinition { Length = minLength, Width = minWidth, Height = minHeight};
            var maximumDimentions = new BoxDefinition { Length = maxLength, Width = maxWidth, Height = maxHeight };
            var ret = BoxListGenerator.Generate(minimumDimentions, maximumDimentions, increment, maxWeight, fillPercentage);

            return ret;
        }
    }
}
