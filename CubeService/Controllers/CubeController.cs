﻿using System.Collections.Generic;
using System.Web.Http;

using CubeService.L10N;

using PackNet.Common.Interfaces.Logging;

namespace CubeService.Controllers
{
    using System;
    using System.Diagnostics;
    using System.Linq;

    using Models;

    using PackNet.Cube;
    using PackNet.Cube.DTO;

    /// <summary>
    /// PackNet.Cube web API
    /// </summary>
    public class CubeController : ApiController
    {
        private readonly ILogger logger;

        /// <summary>
        /// Creates a CubeController
        /// </summary>
        /// <param name="logger"></param>
        public CubeController(ILogger logger)
        {
            this.logger = logger;
            if (BoxListGenerator.Logger == null)
                BoxListGenerator.Logger = logger;
        }

        /// <summary>
        /// Cube a list of items based on the provided <see cref="CubeParameters"/>
        /// </summary>
        /// <param name="cubeParameters">Options that define how the items should be cubed: ItemsToCube, BoxList, CubeParameters</param>
        /// <returns>A list of cubing results</returns>
        public List<CubingResult> Post([FromBody]CubeParameters cubeParameters)
        {
            var stopWatch = Stopwatch.StartNew();
            var itemsToCube = cubeParameters.ItemsToCube;
            if (!itemsToCube.Any())
                return new List<CubingResult> { new CubingResult { Succeeded = false, Errors = Strings.CubeController_Post_Invalid_input__No_items_to_cube } };

            var boxList = cubeParameters.BoxList;
            if (cubeParameters.GenerateBoxList)
            {
                try
                {
                    var minimumDimentions = new BoxDefinition { Length = cubeParameters.MinimumLength, Width = cubeParameters.MinimumWidth, Height = cubeParameters.MinimumHeight };
                    var maximumDimentions = new BoxDefinition { Length = cubeParameters.MaximumLength, Width = cubeParameters.MaximumWidth, Height = cubeParameters.MaximumHeight };
                    boxList = BoxListGenerator.Generate(minimumDimentions, maximumDimentions, cubeParameters.Increment, cubeParameters.MaximumWeight, cubeParameters.FillPercentage);
                    if (!boxList.Any())
                        return new List<CubingResult> { new CubingResult { Succeeded = false, Errors = Strings.CubeController_Post_Box_list_generated_with_0_boxes } };
                }
                catch (Exception e)
                {
                    return new List<CubingResult> { new CubingResult { Succeeded = false, Errors = e.Message } };
                }
            }

            try
            {
                var cubeWrapper = new CubeWrapper(logger)
                                  {
                                      BoxDefinitionList = boxList, 
                                      SpeedControl = cubeParameters.CubeSpeed,
                                      DeltaValue = cubeParameters.PreferredLength
                                      };
                var cubeResult = cubeWrapper.Calculate(itemsToCube).ToList();
                logger.Log(LogLevel.Info, Strings.CubeController_Post_Cubing_completed_in__0__milliseconds,  stopWatch.ElapsedMilliseconds);
                return cubeResult;
            }
            catch (Exception e)
            {
                return new List<CubingResult> { new CubingResult { Succeeded = false, Errors = e.Message } };
            }
        }
    }
}
