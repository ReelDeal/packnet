﻿using System.Web.Mvc;

using CubeService.L10N;

namespace CubeService.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = Strings.HomeController_Index_Home_Page;

            return View();
        }
    }
}
