﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Logging.NLogBackend;

namespace CubeService
{
    using System.Collections.Generic;
    using System.IO;
    using System.Reflection;
    using System.Runtime.Serialization;
    using System.Web;
    using System.Web.Http;

    using Autofac;
    using Autofac.Integration.WebApi;

    using PackNet.Common.Logging;
    using PackNet.Cube.DTO;

    using NLogBackend = NLogBackend;

    public class WebApiApplication : HttpApplication
    {
        private static ILogger logger;
        protected void Application_Start()
        {
            SetupIOC();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration
                   .Formatters
                   .XmlFormatter.SetSerializer<List<CubingResult>>(
                       new DataContractSerializer(typeof(List<CubingResult>),
                           new[] { typeof(CubeOrder), typeof(OrderLineItem), typeof(SelectedBox), typeof(BoxDefinition) }));
        }
        
        private void SetupIOC()
        {
            // Create the container builder.
            var builder = new ContainerBuilder();

            // Register the Web API controllers.
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // Register other dependencies.
            ILoggingBackend loggingBackend = new NLogBackend("Web API", Path.Combine(HttpRuntime.AppDomainAppPath, "Nlog.config"));
            LogManager.LoggingBackend = loggingBackend;
            logger = LogManager.GetLogForApplication();
            builder.Register(c => logger).As<ILogger>().SingleInstance();

            // Build the container.
            var container = builder.Build();

            // Create the dependency resolver.
            var resolver = new AutofacWebApiDependencyResolver(container);

            // Configure Web API with the dependency resolver.
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }
    }
}
