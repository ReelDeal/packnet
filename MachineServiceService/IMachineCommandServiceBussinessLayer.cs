﻿using System;

using PackNet.MachineCommandService.DTO;

namespace PackNet.MachineCommandService
{
    public interface IMachineCommandServiceBussinessLayer : IDisposable
    {
        void GetMachines();

        void GetMachineInfo(Guid data);

        void HandleServiceCommand(Command data);

        void PerformMachineCommand(MachineCommandMessages type, Guid machineId, int trackNumber);
    }
}
