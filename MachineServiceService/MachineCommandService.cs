﻿using System;
using System.ComponentModel.Composition;
using System.Reactive.Linq;

using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.MachineCommandService.DTO;

namespace PackNet.MachineCommandService
{
    [Export(typeof(IService))]
    public class MachineCommandService : IService
    {
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IUICommunicationService uiCommunicationService;
        
        private readonly ILogger logger;

        private readonly IMachineCommandServiceBussinessLayer bussinessLayer;
        
        [ImportingConstructor]
        public MachineCommandService(IServiceLocator serviceLocator)
        {
            logger = serviceLocator.Locate<ILogger>();
            subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            uiCommunicationService = serviceLocator.Locate<IUICommunicationService>();
            bussinessLayer = new MachineCommandServiceBussinessLayer(serviceLocator.Locate<IAggregateMachineService>(), uiCommunicationService, new MachineCalibrationHandler(), new MachineCommandCommunicatorFactory(uiCommunicationService, serviceLocator.Locate<IEventAggregatorPublisher>(), subscriber, logger),  logger);

            SetupListeners();
        }

        private void SetupListeners()
        {
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServiceToolState.Raise);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServiceToolState.Crease);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServiceToolState.Cut);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServiceToolState.Activate);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServiceToolState.Deactivate);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServiceToolState.Forward);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServiceToolState.Backwards);
            
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServicePart.NotApplicable);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServicePart.Latch);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServicePart.Tool);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServicePart.Break);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServicePart.PressureRoller);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(ServicePart.Feed);
            
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceCommands.Calibration);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceCommands.LongHead);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceCommands.CrossHead);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceCommands.Track);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceCommands.CleanCut);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceCommands.Pause);
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceCommands.ServiceMode);
            
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator(MachineServiceMessages.GetMachines);
            subscriber.GetEvent<IMessage>()
                .Where(m => m.MessageType == MachineServiceMessages.GetMachines)
                .DurableSubscribe(m => bussinessLayer.GetMachines(), logger);
            
            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Guid>(MachineServiceMessages.GetMachineInfo);
            subscriber.GetEvent<IMessage<Guid>>()
                .Where(m => m.MessageType == MachineServiceMessages.GetMachineInfo)
                .DurableSubscribe(m => bussinessLayer.GetMachineInfo(m.Data), logger);

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<Command>(MachineServiceMessages.PerformCommand);
            subscriber.GetEvent<IMessage<Command>>()
                .Where(m => m.MessageType == MachineServiceMessages.PerformCommand)
                .DurableSubscribe(m => bussinessLayer.HandleServiceCommand(m.Data), logger);
            

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<MachineCommand>(MachineCommandMessages.ForwardFeed);
            MachineCommandMessages.ForwardFeed
                .OnMessage<MachineCommand>(subscriber, logger,
                    msg => bussinessLayer.PerformMachineCommand(MachineCommandMessages.ForwardFeed, msg.Data.MachineId, msg.Data.TrackNumber));

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<MachineCommand>(MachineCommandMessages.ReverseFeed);
            MachineCommandMessages.ReverseFeed
                .OnMessage<MachineCommand>(subscriber, logger,
                    msg => bussinessLayer.PerformMachineCommand(MachineCommandMessages.ReverseFeed, msg.Data.MachineId, msg.Data.TrackNumber));

            uiCommunicationService.RegisterUIEventWithInternalEventAggregator<MachineCommand>(MachineCommandMessages.CleanCut);
            MachineCommandMessages.CleanCut
                .OnMessage<MachineCommand>(subscriber, logger,
                    msg => bussinessLayer.PerformMachineCommand(MachineCommandMessages.CleanCut, msg.Data.MachineId, msg.Data.TrackNumber));
        }

        public string Name
        {
            get { return "MachineServiceService"; }
        }

        public void Dispose()
        {
            if (bussinessLayer != null)
                bussinessLayer.Dispose();
        }
    }
}
