﻿using System.Globalization;
using System.Linq;
using System.Xml;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;

namespace PackNet.MachineCommandService
{
    public class MachineCalibrationHandler : IMachineCalibrationHandler
    {
        public void HandleFusionCalibration(FusionMachine machine, IMachineCommandCommunicator com)
        {
            var calibrationValues = com.GetFusionCalibrationValues();
            var physicalMachineSettings = machine.PhysicalMachineSettings as FusionPhysicalMachineSettings;
            if (physicalMachineSettings == null)
                return;

            var doc = new XmlDocument();
            doc.Load(machine.PhysicalSettingsFilePath);
            var trackCounter = 0;
            foreach (XmlNode node in doc.SelectNodes("machine/tracks/track"))
            {
                var leftCounter = 0;
                var rightCounter = 0;

                foreach (XmlNode compensationsnode in node.SelectNodes("leftswordsensorCompensations/compensation"))
                {
                    compensationsnode.FirstChild.Value =
                        calibrationValues.LeftCompensations.ElementAt(trackCounter)
                            .ElementAt(leftCounter)
                            .ToString(CultureInfo.InvariantCulture);
                    leftCounter++;
                }

                foreach (XmlNode compensationsnode in node.SelectNodes("rightswordsensorCompensations/compensation"))
                {
                    compensationsnode.FirstChild.Value =
                           calibrationValues.RightCompensations.ElementAt(trackCounter)
                               .ElementAt(rightCounter)
                               .ToString(CultureInfo.InvariantCulture);
                    rightCounter++;
                }

                trackCounter++;
            }
            doc.Save(machine.PhysicalSettingsFilePath);
            machine.PhysicalMachineSettings =
                FusionPhysicalMachineSettings.CreateFromFile(machine.PhysicalMachineSettings.EventNotifierIp,
                    machine.PhysicalSettingsFilePath);
        }
    }
}
