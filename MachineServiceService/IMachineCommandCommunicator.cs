﻿using PackNet.MachineCommandService.DTO;

namespace PackNet.MachineCommandService
{
    public interface IMachineCommandCommunicator
    {
        void PauseMachine();

        void HandleServiceMode(ServiceToolState state);

        void PerformServiceCommand(Command command);

        void PerformMachineCommand(MachineCommandMessages type, int trackNumber);

        FusionTrackCalibrationValues GetFusionCalibrationValues();

        string IpAddress { get; }

        int Port { get; }
    }
}
