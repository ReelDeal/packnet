﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.MachineCommandService.DTO;

namespace PackNet.MachineCommandService
{
    public class MachineCommandServiceBussinessLayer : IMachineCommandServiceBussinessLayer
    {
        private readonly IAggregateMachineService machineService;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IMachineCalibrationHandler calibrationHandler;
        private readonly IMachineCommandCommunicatorFactory communicatorFactory;

        private readonly ConcurrentBag<Tuple<Guid, IDisposable>> machineObservables;
        private readonly ConcurrentDictionary<Guid, IDisposable> calibrationObservables;
        
        private readonly ILogger logger;

        public MachineCommandServiceBussinessLayer(IAggregateMachineService machineService, IUICommunicationService uiCommunicationService, IMachineCalibrationHandler calibrationHandler, IMachineCommandCommunicatorFactory communicatorFactory, ILogger logger)
        {
            this.machineService = machineService;
            this.uiCommunicationService = uiCommunicationService;
            this.calibrationHandler = calibrationHandler;
            this.communicatorFactory = communicatorFactory;
            this.logger = logger;

            machineObservables = new ConcurrentBag<Tuple<Guid, IDisposable>>();
            calibrationObservables = new ConcurrentDictionary<Guid, IDisposable>();
        }

        public void HandleServiceCommand(Command command)
        {
            try
            {
                if (command.Type == MachineServiceCommands.Pause)
                {
                    communicatorFactory.GetInstance(machineService.FindById(command.MachineId) as IPacksizeCutCreaseMachine).PauseMachine();
                    PublishMachineServiceResponse(command.MachineId, ResultTypes.Success,"PauseCommandSend");
                }
                else if (command.Type == MachineServiceCommands.ServiceMode)
                {
                    communicatorFactory.GetInstance(machineService.FindById(command.MachineId) as IPacksizeCutCreaseMachine).HandleServiceMode(command.ToolState);
                    PublishMachineServiceResponse(command.MachineId, ResultTypes.Success, "ServiceCommandSend");
                }
                else
                {
                    var machine = machineService.FindById(command.MachineId) as IPacksizeCutCreaseMachine;

                    if (command.Type == MachineServiceCommands.Calibration && machine.Errors.Any())
                        return;
                    communicatorFactory.GetInstance(machine).PerformServiceCommand(command);
                    PublishMachineServiceResponse(command.MachineId, ResultTypes.Success, "CommandSend");
                }
            }
            catch (Exception e)
            {
                //TODO: Localize details of the exception
                PublishMachineServiceResponse(command.MachineId, ResultTypes.Fail, "CommandSend");
                logger.LogException(LogLevel.Error, "Failed to Send Machine Command", e);
            }
        }

        public void PerformMachineCommand(MachineCommandMessages type, Guid machineId, int trackNumber)
        {
            var machine = machineService.FindById(machineId) as IPacksizeCutCreaseMachine;

            if (machine == null)
                return;

            var com = communicatorFactory.GetInstance(machine);

            try
            {
                com.PerformMachineCommand(type, trackNumber);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, "Failed to Send Machine Command", e);
            }
        }

        private void PublishMachineServiceResponse(Guid machineId, ResultTypes result, string message = null)
        {
            uiCommunicationService.SendMessageToUI(new ResponseMessage<Guid>
            {
                MessageType = MachineServiceMessages.MachineServiceResponse,
                Data = machineId,
                Message = message,
                Result = result
            });
        }

        public void GetMachineInfo(Guid machineId)
        {
            var machine = machineService.FindById(machineId) as IPacksizeCutCreaseMachine;

            if (machine == null)
            {
                PublishMachineServiceResponse(machineId, ResultTypes.Fail, string.Format("MachineIdNotExists, {0}", machineId));
            }
            
            SetupMachineStatusObserver(machine);
            PublishMachineServiceModel(machine);
        }

        private void PublishMachineServiceModel(IMachine machine)
        {
            if (machine is FusionMachine)
            {
                uiCommunicationService.SendMessageToUI(new ResponseMessage<FusionMachineServiceModel>
                {
                    MessageType = MachineServiceMessages.GetMachineInfoResponse,
                    Result = ResultTypes.Success,
                    Data = new FusionMachineServiceModel(machine as FusionMachine)
                });
                return;
            }

            if (machine is EmMachine)
            {
                uiCommunicationService.SendMessageToUI(new ResponseMessage<EmMachineServiceModel>
                {
                    MessageType = MachineServiceMessages.GetMachineInfoResponse,
                    Result = ResultTypes.Success,
                    Data = new EmMachineServiceModel(machine as EmMachine)
                });
                return;
            }

            uiCommunicationService.SendMessageToUI(new ResponseMessage
            {
                MessageType = MachineServiceMessages.GetMachineInfoResponse,
                Result = ResultTypes.Fail,
            });
        }

        private void SetupMachineStatusObserver(IMachine machine)
        {
            machine.CurrentStatusChangedObservable.Subscribe(statuses => PublishMachineServiceModel(machine));
            machine.CurrentStatusChangedObservable.Where(s => s == MachinePausedStatuses.MachineCalibrating)
                .Subscribe(stautses => HandleCalibratingMachine(machine));
        }

        private void HandleCalibratingMachine(IMachine machine)
        {
            if (calibrationObservables.ContainsKey(machine.Id))
                return;

            var observable = machine.CurrentStatusChangedObservable.Subscribe(newStatus =>
            {
                if (newStatus == MachineErrorStatuses.MachineError || newStatus == MachineErrorStatuses.EmergencyStop)
                {
                    PublishMachineServiceResponse(machine.Id, ResultTypes.Fail, "Calibration");
                    RemoveCalibrationObservable(machine);
                    return;
                }

                if (newStatus == MachinePausedStatuses.MachineService)
                {
                    var com = communicatorFactory.GetInstance(machineService.FindById(machine.Id) as IPacksizeCutCreaseMachine);
                    calibrationHandler.HandleFusionCalibration(machine as FusionMachine, com);
                }

                RemoveCalibrationObservable(machine);

                PublishMachineServiceResponse(machine.Id, ResultTypes.Success, "CalibrationCompleted");
            });

            calibrationObservables.TryAdd(machine.Id, observable);
            PublishMachineServiceResponse(machine.Id, ResultTypes.Success, "CalibrationStarted");
        }

        private void RemoveCalibrationObservable(IMachine machine)
        {
            if (!calibrationObservables.ContainsKey(machine.Id))
            {
                return;
            }

            IDisposable disposable;
            calibrationObservables.TryGetValue(machine.Id, out disposable);
            if (disposable == null)
            {
                return;
            }
            
            disposable.Dispose();
            calibrationObservables.TryRemove(machine.Id, out disposable);
        }

        public void GetMachines()
        {
            var machines =
                machineService.Machines.Where(m => m.MachineType == MachineTypes.Fusion || m.MachineType == MachineTypes.Em).ToList();

            machines.ForEach(m =>
            {
                if (machineObservables.Any(t => t.Item1.Equals(m.Id)) == false)
                    machineObservables.Add(new Tuple<Guid, IDisposable>(m.Id,
                        m.CurrentStatusChangedObservable.Subscribe(mach => PublishMachinesToUi(machines))));
            });

            PublishMachinesToUi(machines);
        }

        private void PublishMachinesToUi(IEnumerable<IMachine> machines)
        {
            uiCommunicationService.SendMessageToUI(new ResponseMessage<IEnumerable<MachinePresentationModel<IMachine>>>
            {
                MessageType = MachineServiceMessages.GetMachinesResponse,
                Result = ResultTypes.Success,
                Data = machines.Select(m => new MachinePresentationModel<IMachine>
                {
                    Id = m.Id,
                    MachineType = m.MachineType,
                    Alias = m.Alias,
                    Description = m.Description,
                    Data = m
                }).ToList()
            });
        }

        public void Dispose()
        {
            if (machineObservables != null)
            {
                machineObservables.ForEach(o => o.Item2.Dispose());
            }

            if (calibrationObservables != null)
            {
                calibrationObservables.ForEach(g => g.Value.Dispose());
            }
        }
    }
}
