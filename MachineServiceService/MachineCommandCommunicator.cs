﻿using System;
using System.Collections.Generic;
using System.Threading;

using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Utils;
using PackNet.Communication;
using PackNet.Communication.Communicators.Fusion;
using PackNet.Communication.PLCBase;
using PackNet.MachineCommandService.DTO;
using PackNet.MachineCommandService.L10N;

namespace PackNet.MachineCommandService
{
    public class MachineCommandCommunicator : IMachineCommandCommunicator
    {
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IUICommunicationService uiCommunicationService;
        private readonly IPacksizeCutCreaseMachine machine;
        private readonly ISimplePLCCommunicator simplePlcCommunicator;
        private readonly ILogger logger;
        private readonly string ipAddress;
        private readonly int port;
        private SpinLock currentlyPerformingMachineCommand = new SpinLock(false);        
        private bool lockToken;

        public string IpAddress { get { return ipAddress; } }
        public int Port { get { return port; } }

        public MachineCommandCommunicator(IPacksizeCutCreaseMachine machine, IWebRequestCreator webRequestCreator,
            IUICommunicationService uiCommunicationService, IEventAggregatorSubscriber subscriber, ILogger logger)
        {
            this.logger = logger;
            this.machine = machine;
            this.subscriber = subscriber;
            this.uiCommunicationService = uiCommunicationService;

            this.ipAddress = webRequestCreator.IpAddress;
            this.port = webRequestCreator.Port;

            simplePlcCommunicator = new SimplePLCCommunicator(webRequestCreator, logger);
        }

        public void PauseMachine()
        {
            if(machine.CurrentStatus == MachineStatuses.MachineOnline)
                simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.MachinePaused, true);
        }

        public void HandleServiceMode(ServiceToolState state)
        {
            if (((machine.CurrentStatus == MachinePausedStatuses.MachinePaused || machine.CurrentStatus is MachineErrorStatuses) && state == ServiceToolState.Activate) || (machine.CurrentStatus == MachinePausedStatuses.MachineService && state == ServiceToolState.Deactivate))
                simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ToggleServiceMode, true);
        }

        public void PerformServiceCommand(Command command)
        {
            try
            {
                simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceCommand, command.Type.Value);
                simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServicePart, command.Part.Value);
                simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceUnitNumber, command.Number);
                simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceToolState, command.ToolState.Value);
                simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceCommandExecute, true);
            }
            catch (Exception e)
            {
                logger.LogException(LogLevel.Error, Strings.MachineCommandCommunicator_PerformServiceCommand_Unable_to_Perform_Service_Command_On_Machine_with__0_, e,machine);
            }
        }

        public void PerformMachineCommand(MachineCommandMessages type, int trackNumber)
        {
            currentlyPerformingMachineCommand.Enter(ref lockToken);

            try
            {
                if (!lockToken)
                {
                    logger.Log(LogLevel.Error,
                        string.Format("Unable to aquire locktoken when performing a machine command for machine {0} aborting",
                            machine.Id));
                    return;
                }

                if (type == MachineCommandMessages.ForwardFeed)
                {
                    FeedTrackForward(trackNumber);
                }

                if (type == MachineCommandMessages.ReverseFeed)
                {
                    FeedTrackBackward(trackNumber);
                }

                if (type == MachineCommandMessages.CleanCut)
                {
                    PerformCleanCut(trackNumber);
                }

                PublishCommandSentToMachine();
                SetupResponseListener();
            }
            catch
            {
                currentlyPerformingMachineCommand.Exit();
                lockToken = false;
             
                logger.Log(LogLevel.Error,
                       string.Format("Unable to send command to machine {0}",
                           machine.Id));
            }
        }

        private void PerformCleanCut(int trackNumber)
        {
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceCommand, MachineServiceCommands.CleanCut.Value);
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServicePart, ServicePart.NotApplicable.Value);
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceUnitNumber, trackNumber);
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceCommandExecute, true);
        }

        private void FeedTrackForward(int trackNumber)
        {
            FeedTrack(trackNumber, ServiceToolState.Forward);
        }

        private void FeedTrackBackward(int trackNumber)
        {
            FeedTrack(trackNumber, ServiceToolState.Backwards);
        }

        private void FeedTrack(int trackNumber, ServiceToolState direction)
        {
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceCommand, MachineServiceCommands.Track.Value);
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServicePart, ServicePart.Feed.Value);
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceToolState, direction.Value);
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceUnitNumber, trackNumber);
            simplePlcCommunicator.WriteValue(MachineCommandVariableMap.Instance.ServiceCommandExecute, true);
        }

        private void PublishCommandSentToMachine()
        {
            uiCommunicationService.SendMessageToUI(new ResponseMessage<Guid>
            {
                Result = ResultTypes.Success,
                MessageType = MachineCommandMessages.MachineCommandResponse,
                Data = machine.Id
            });
        
        }

        private void SetupResponseListener()
        {
            var done = false;
            Retry.For(() =>
            {
                done = simplePlcCommunicator.ReadValue<bool>(MachineCommandVariableMap.Instance.MachineCommandDone);
                return done;
            }, TimeSpan.FromSeconds(15));
            
            uiCommunicationService.SendMessageToUI(new ResponseMessage<Guid>()
            {
                Result = done ? ResultTypes.Success : ResultTypes.Fail,
                Data = machine.Id,
                MessageType = MachineCommandMessages.MachineCommandDoneResponse
            });

            currentlyPerformingMachineCommand.Exit();
            lockToken = false;
        }
        public FusionTrackCalibrationValues GetFusionCalibrationValues()
        {
            return new FusionTrackCalibrationValues()
            {
                LeftCompensations = new List<List<double>>
                {
                    simplePlcCommunicator.ReadValue<List<double>>(IqFusionMachineVariables.Instance.TrackSensorCompensationLeftTrack1),
                    simplePlcCommunicator.ReadValue<List<double>>(IqFusionMachineVariables.Instance.TrackSensorCompensationLeftTrack2)
                },
                RightCompensations = new List<List<double>>
                {
                    simplePlcCommunicator.ReadValue<List<double>>(IqFusionMachineVariables.Instance.TrackSensorCompensationRightTrack1),
                    simplePlcCommunicator.ReadValue<List<double>>(IqFusionMachineVariables.Instance.TrackSensorCompensationRightTrack2)
                }
            };
        }
    }
}
