﻿using PackNet.Common.Interfaces.DTO.Machines;

namespace PackNet.MachineCommandService
{
    public interface IMachineCalibrationHandler
    {
        void HandleFusionCalibration(FusionMachine machine, IMachineCommandCommunicator com);
    }
}
