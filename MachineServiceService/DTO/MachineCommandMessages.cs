﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.MachineCommandService.DTO
{
    public class MachineCommandMessages : MachineMessages
    {
        //Messages
        public static readonly MachineCommandMessages ForwardFeed = new MachineCommandMessages("ForwardFeed");
        public static readonly MachineCommandMessages ReverseFeed = new MachineCommandMessages("ReverseFeed");
        public static readonly MachineCommandMessages CleanCut = new MachineCommandMessages("CleanCut");

        //Responses
        public static readonly MachineCommandMessages MachineCommandResponse = new MachineCommandMessages("MachineCommandResponse");
        public static readonly MachineCommandMessages MachineCommandDoneResponse = new MachineCommandMessages("MachineCommandDoneResponse");

        protected MachineCommandMessages(string name) : base(name) { }
    }
}