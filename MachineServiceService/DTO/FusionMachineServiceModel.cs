﻿using System;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.MachineCommandService.DTO
{
    using System.Collections.Generic;

    using PackNet.Common.Interfaces.Machines;

    public class FusionMachineServiceModel
    {
        public FusionMachineServiceModel(FusionMachine machine)
        {
            Id = machine.Id;
            MachineStatus = machine.CurrentStatus;
            Alias = machine.Alias;
            IpOrDnsName = machine.IpOrDnsName;
            Port = machine.Port;
            Errors = machine.Errors;
        }

        public Guid Id { get; set; }
        public string Alias { get; set; }
        public MachineStatuses MachineStatus { get; set; }

        public string IpOrDnsName { get; set; }
        public int Port { get; set; }
        public bool Latcher { get; set; }
        public bool Knife { get; set; }
        public IEnumerable<MachineError> Errors { get; set; }

        public bool Track1PressureRoller { get; set; }
        public bool Track2PressureRoller { get; set; }
        public bool RollerLowered { get; set; }
    }
}
