﻿using System;

namespace PackNet.MachineCommandService.DTO
{
    public class Command
    {
        public Guid MachineId { get; set; }
        public MachineServiceCommands Type { get; set; }
        public ServicePart Part { get; set; }
        public int Number { get; set; }
        public ServiceToolState ToolState { get; set; }
    }
}
