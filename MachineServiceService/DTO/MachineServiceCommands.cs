﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.MachineCommandService.DTO
{
    public class ServicePart : MessageTypes
    {
        public static ServicePart NotApplicable = new ServicePart("NotApplicable", 0);
        public static ServicePart Latch = new ServicePart("Latch", 1);
        public static ServicePart Tool = new ServicePart("Tool", 2);
        public static ServicePart Break = new ServicePart("Break", 3);
        public static ServicePart PressureRoller = new ServicePart("PressureRoller", 4);
        public static ServicePart Feed = new ServicePart("Feed", 5);

        private ServicePart(string displayName, int value)
            : base(value, displayName)
        {
        }
    }

    public class MachineServiceCommands : MessageTypes
    {
        public static MachineServiceCommands Calibration = new MachineServiceCommands("Calibration", 0);
        public static MachineServiceCommands LongHead = new MachineServiceCommands("LongHead", 1);
        public static MachineServiceCommands CrossHead = new MachineServiceCommands("CrossHead", 2);
        public static MachineServiceCommands Track = new MachineServiceCommands("Track", 3);
        public static MachineServiceCommands CleanCut = new MachineServiceCommands("CleanCut", 4);
        public static MachineServiceCommands Pause = new MachineServiceCommands("Pause", 5);
        public static MachineServiceCommands ServiceMode = new MachineServiceCommands("ServiceMode", 6);

        public MachineServiceCommands(string displayName, int value) : base(value, displayName)
        {
        }
    }

    public class ServiceToolState : MessageTypes
    {
        public static ServiceToolState Raise = new ServiceToolState("Raise", 0);
        public static ServiceToolState Crease = new ServiceToolState("Crease", 1);
        public static ServiceToolState Cut = new ServiceToolState("Cut", 2);
        public static ServiceToolState Activate = new ServiceToolState("Activate", 3);
        public static ServiceToolState Deactivate = new ServiceToolState("Deactivate", 4);
        public static ServiceToolState Forward = new ServiceToolState("Forward", 5);
        public static ServiceToolState Backwards = new ServiceToolState("Backwards", 6);

        public ServiceToolState(string displayName, int value) : base(value, displayName)
        {   
        }
    }
}
