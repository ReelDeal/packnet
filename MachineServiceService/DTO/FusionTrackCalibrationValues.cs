﻿using System.Collections.Generic;

namespace PackNet.MachineCommandService.DTO
{
    public class FusionTrackCalibrationValues
    {
        public List<List<double>> LeftCompensations { get; set; }
        public List<List<double>> RightCompensations { get; set; }
    }
}
