﻿using PackNet.Common.Interfaces.Enums;

namespace PackNet.MachineCommandService.DTO
{
    public class MachineServiceMessages : MessageTypes
    {
        //Messages
        public static readonly MachineServiceMessages GetMachines = new MachineServiceMessages("ServiceGetMachines");
        public static readonly MachineServiceMessages GetMachineInfo = new MachineServiceMessages("ServiceGetMachineInfo");
        
        //Responses
        public static readonly MachineServiceMessages GetMachinesResponse = new MachineServiceMessages("ServiceGetMachinesResponse");
        public static readonly MachineServiceMessages GetMachineInfoResponse = new MachineServiceMessages("ServiceGetMachineInfoResponse");

        //Command Messages
        public static readonly MachineServiceMessages PauseMachine = new MachineServiceMessages("ServicePauseMachine");
        public static readonly MachineServiceMessages HandleServiceMode = new MachineServiceMessages("ServiceHandleServiceMode");
        public static readonly MachineServiceMessages PerformCommand = new MachineServiceMessages("ServicePerformCommand");

        //Command Messages Responses
        public static readonly MachineServiceMessages PauseMachineResponse = new MachineServiceMessages("ServicePauseMachineResponse");
        public static readonly MachineServiceMessages HandleServiceModeResponse = new MachineServiceMessages("ServiceHandleServiceModeResponse");
        public static readonly MachineServiceMessages MachineServiceResponse = new MachineServiceMessages("MachineServiceResponse");

        //Machine State handling
        public static readonly MachineServiceMessages Pause = new MachineServiceMessages("Pause");
        public static readonly MachineServiceMessages ServiceMode = new MachineServiceMessages("ServiceMode");

        //Command Types
        public static readonly MachineServiceMessages CalibrationCommand = new MachineServiceMessages("Calibration", 0);
        public static readonly MachineServiceMessages LongHeadCommand = new MachineServiceMessages("LongHead", 1);
        public static readonly MachineServiceMessages CrossHeadCommand = new MachineServiceMessages("CrossHead", 2);
        public static readonly MachineServiceMessages TrackCommand = new MachineServiceMessages("Track", 3);
        public static readonly MachineServiceMessages CleanCut = new MachineServiceMessages("CleanCut", 4);

        //Command parts
        public static readonly MachineServiceMessages NotApplicable = new MachineServiceMessages("NotApplicable", 0);
        public static readonly MachineServiceMessages Latch = new MachineServiceMessages("Latch", 1);
        public static readonly MachineServiceMessages Knife = new MachineServiceMessages("Knife", 2);
        public static readonly MachineServiceMessages Crease = new MachineServiceMessages("Crease", 3);
        public static readonly MachineServiceMessages Break = new MachineServiceMessages("Break", 4);
        public static readonly MachineServiceMessages PressureRoller = new MachineServiceMessages("PressureRoller", 5);



        protected MachineServiceMessages(string name) : base(0, name) { }

        protected MachineServiceMessages(string name, int index) : base(index, name) { }

    }
}