﻿using System;
using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Enums;

namespace PackNet.MachineCommandService.DTO
{
    using PackNet.Common.Interfaces.Machines;

    public class EmMachineServiceModel
    {
        public EmMachineServiceModel(EmMachine machine)
        {
            var phyicalMachineSettings = (machine.PhysicalMachineSettings as EmPhysicalMachineSettings);

            if (phyicalMachineSettings == null)
                return;

            LongHeads = phyicalMachineSettings.LongHeadParameters.LongHeads.Select(l => new ServiceLongHead(l.Number)).ToList();
            Tracks = machine.Tracks.Select(t => new ServiceTrack(t.TrackNumber)).ToList();
            CrossHead = new ServiceCrossHead();
            MachineStatus = machine.CurrentStatus;
            Id = machine.Id;
            Alias = machine.Alias;
            IpOrDnsName = machine.IpOrDnsName;
            Port = machine.Port;
            Errors = machine.Errors;
        }

        public Guid Id { get; set; }
        public string Alias { get; set; }

        public string IpOrDnsName { get; set; }

        public int Port { get; set; }

        public IEnumerable<MachineError> Errors { get; set; }

        public MachineStatuses MachineStatus { get; set; }

        public List<ServiceLongHead> LongHeads { get; set; }
        
        public List<ServiceTrack> Tracks { get; set; }
        
        public ServiceCrossHead CrossHead { get; set; }
    }

    public class ServiceCrossHead
    {
        public bool Latcher { get; set; }
        public ServiceToolState ToolState { get; set; }
    }

    public class ServiceLongHead
    {
        public ServiceLongHead(int number)
        {
            Number = number;
        }

        public int Number { get; set; }
        public bool Latcher { get; set; }
        public ServiceToolState ToolState { get; set; }
    }

    public class ServiceTrack
    {
        public ServiceTrack(int number)
        {
            Number = number;
        }

        public int Number { get; set; }
        public bool Break { get; set; }
        public bool PressureRoller { get; set; }
    }
}
