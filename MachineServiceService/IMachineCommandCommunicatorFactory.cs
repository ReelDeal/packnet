﻿using PackNet.Common.Interfaces.Machines;

namespace PackNet.MachineCommandService
{
    public interface IMachineCommandCommunicatorFactory
    {
        IMachineCommandCommunicator GetInstance(IPacksizeCutCreaseMachine machine);
    }
}
