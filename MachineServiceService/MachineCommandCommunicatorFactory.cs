﻿using System;
using System.Collections.Generic;

using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Communication.WebRequestCreator;

namespace PackNet.MachineCommandService
{
    public class MachineCommandCommunicatorFactory : IMachineCommandCommunicatorFactory
    {
        private readonly ILogger logger;
        private readonly IEventAggregatorPublisher publisher;
        private readonly IEventAggregatorSubscriber subscriber;
        private readonly IUICommunicationService uiCommunicationService;

        private readonly Dictionary<Guid, IMachineCommandCommunicator> communicators;

        public MachineCommandCommunicatorFactory(IUICommunicationService uiCommunicationService,
            IEventAggregatorPublisher publisher, IEventAggregatorSubscriber subscriber, ILogger logger)
        {
            this.logger = logger;
            this.publisher = publisher;
            this.subscriber = subscriber;
            this.uiCommunicationService = uiCommunicationService;
            communicators = new Dictionary<Guid, IMachineCommandCommunicator>();
        }

        public IMachineCommandCommunicator GetInstance(IPacksizeCutCreaseMachine machine)
        {
            if (communicators.ContainsKey(machine.Id) == false)
                communicators.Add(machine.Id,
                    new MachineCommandCommunicator(machine, new WebRequestCreator(machine.IpOrDnsName, machine.Port),
                        uiCommunicationService, subscriber, logger));

            if (communicators.ContainsKey(machine.Id))
            {
                if (!HasSameIpAndPort(machine, communicators[machine.Id]))
                {
                    UpdateCommunicatorIp(machine);
                }                
            }

            return communicators[machine.Id];
        }

        private bool HasSameIpAndPort(IPacksizeCutCreaseMachine machine, IMachineCommandCommunicator commandCommunicator)
        {
            return commandCommunicator.Port == machine.Port && commandCommunicator.IpAddress == machine.IpOrDnsName;
        }

        private void UpdateCommunicatorIp(IPacksizeCutCreaseMachine machine)
        {
            communicators[machine.Id] = new MachineCommandCommunicator(machine, new WebRequestCreator(machine.IpOrDnsName, machine.Port), uiCommunicationService, subscriber, logger);
        }
    }
}
