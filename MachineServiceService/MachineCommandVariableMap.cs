﻿using System.Collections.Generic;
using System.Linq;

using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Communication.Communicators;

namespace PackNet.MachineCommandService
{
    public class MachineCommandVariableMap
    {
        public static MachineCommandVariableMap Instance;

        static MachineCommandVariableMap()
        {
            Instance = new MachineCommandVariableMap();
        }

        public MachineCommandVariableMap()
        {
            MapServiceParameters();
        }

        public PacksizePlcVariable MachinePaused { get; private set; }
        public PacksizePlcVariable ToggleServiceMode { get; private set; }
        public PacksizePlcVariable ServiceCommand { get; private set; }
        public PacksizePlcVariable ServicePart { get; private set; }
        public PacksizePlcVariable ServiceUnitNumber { get; private set; }
        public PacksizePlcVariable ServiceToolState { get; private set; }
        public PacksizePlcVariable ServiceCommandExecute { get; private set; }
        public PacksizePlcVariable MachineCommandDone { get; private set; }
        
        private void MapServiceParameters()
        { 
            ToggleServiceMode = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "ToggleServiceMode");
            MachinePaused = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "MACHINE_PAUSED");
           
            ServiceCommand = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentServiceCommand.Command");
            ServicePart = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentServiceCommand.Part");
            ServiceUnitNumber = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentServiceCommand.Index");
            ServiceToolState = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentServiceCommand.ToolState");
            ServiceCommandExecute = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentServiceCommand.Execute");
            MachineCommandDone = new PacksizePlcVariable(PacksizeMachineTasks.Main.ToString(), "CurrentServiceCommand.Done");
        }

        public virtual IEnumerable<PacksizePlcVariable> AsEnumerable()
        {
            var info = typeof(MachineCommandVariableMap).GetProperties();

            return info.Select(pi => (PacksizePlcVariable)pi.GetValue(this)).ToList();
        }

        public bool ContainsVariable(string var)
        {
            return AsEnumerable().Any(v => v.ToString().Equals(var));
        }
    }
}
