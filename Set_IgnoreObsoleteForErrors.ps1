Get-ChildItem -Recurse -Filter "*.*csproj" | % {
    Write-Host $_.Name
     
    $filename = $_.Fullname
     
    $proj = [xml]( Get-Content $_.Fullname )
 
    $xmlNameSpace = new-object System.Xml.XmlNamespaceManager($proj.NameTable)
 
    $xmlNameSpace.AddNamespace("p", "http://schemas.microsoft.com/developer/msbuild/2003")
     
	 $projNode = $proj.SelectSingleNode("/p:Project",$xmlNameSpace);
	 
    $node = $proj.SelectSingleNode("/p:Project/p:PropertyGroup/p:WarningsNotAsErrors", $xmlNameSpace)
     
	 if(-not $node)
	 {	 
		write-host ("Node not found");
		$node = $proj.CreateElement("PropertyGroup","http://schemas.microsoft.com/developer/msbuild/2003");
		$warningNotAsError = $proj.CreateElement("WarningsNotAsErrors","http://schemas.microsoft.com/developer/msbuild/2003");
		$warningNotAsError.set_InnerText("612");
		
		$node.AppendChild($warningNotAsError);		
		$projNode.AppendChild($node);				
		$proj.Save("$($filename)") | Out-Null
	 }
	 else
	 {
		write-host ("Node exists already, skipping");
	 }
	 
    
}