﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ServicesTests
{
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.Machines;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;
    using PackNet.Services;

    using Testing.Specificity;

    [TestClass]
    public class RestrictionResolverTests
    {
        [TestMethod]
        public void ShouldResolveMachineGroupSpecificRestriction()
        {
            var mgGuid = Guid.NewGuid();
            var resolver = new RestrictionResolverService();

            var carton = new Carton();
            carton.Restrictions.Add(new MachineGroupSpecificRestriction(mgGuid));

            var capability = new List<ICapability> { new MachineIdCapability(mgGuid) };

            var r = resolver.Resolve(carton.Restrictions.First(), capability);
            Specify.That(r).Should.BeTrue();
        }
    }
}
