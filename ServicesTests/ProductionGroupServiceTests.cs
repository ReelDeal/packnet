﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Business.ProductionGroups;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Services.ProductionGroup;

namespace ServicesTests
{
    using PackNet.Common.Interfaces.DTO.ProductionGroups;

    [TestClass]
    public class ProductionGroupServiceTests
    {
        private EventAggregator eventAggregator;
        private Mock<ILogger> logger;

        private Mock<IUICommunicationService> uiCommunicationServiceMock;
    
        [TestInitialize]
        public void Setup()
        {
            eventAggregator = new EventAggregator();
            logger = new Mock<ILogger>();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodCreate()
        {
            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroups = new Mock<IProductionGroup>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var pg = new ProductionGroup();

            productionGroupService.Create(new ProductionGroup());

            productionGroups.Verify(p => p.Create(It.Is<ProductionGroup>(g => g.Id == pg.Id)), Times.Once);
        }
    
        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodUpdate()
        {
            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroups = new Mock<IProductionGroup>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var pg = new ProductionGroup();

            productionGroupService.Update(pg);

            productionGroups.Verify(p => p.Update(It.Is<ProductionGroup>(g => g.Id == pg.Id)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodDelete()
        {
            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroups = new Mock<IProductionGroup>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var pg = new ProductionGroup();

            productionGroupService.Delete(pg);

            productionGroups.Verify(p => p.Delete(It.Is<ProductionGroup>(g => g.Id == pg.Id)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodFindByMachineGroupId()
        {
            var productionGroups = new Mock<IProductionGroup>();

            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var mgId = new Guid();

            productionGroupService.GetProductionGroupForMachineGroup(mgId);

            productionGroups.Verify(p => p.GetProductionGroupForMachineGroup(It.Is<Guid>(g => g == mgId)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodFind()
        {
            var productionGroups = new Mock<IProductionGroup>();

            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var pgId = new Guid();

            productionGroupService.Find(pgId);

            productionGroups.Verify(p => p.Find(It.Is<Guid>(g => g == pgId)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodFindByCartonPropertyGroupId()
        {
            var productionGroups = new Mock<IProductionGroup>();

            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var cPgId = new Guid();

            productionGroupService.FindByCartonPropertyGroupId(cPgId);

            productionGroups.Verify(p => p.FindByCartonPropertyGroupId(It.Is<Guid>(g => g == cPgId)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodFindByAlias()
        {
            var productionGroups = new Mock<IProductionGroup>();

            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var alias = "PG1";

            productionGroupService.FindByAlias(alias);

            productionGroups.Verify(p => p.FindByAlias(It.Is<string>(g => g == alias)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodFindProductionGroupsByCorrugateId()
        {
            var productionGroups = new Mock<IProductionGroup>();

            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var corrugateId = new Guid();

            productionGroupService.GetProductionGroupsForCorrugate(corrugateId);

            productionGroups.Verify(p => p.GetProductionGroupsForCorrugate(It.Is<Guid>(g => g == corrugateId)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodRemoveCorrugateFromProductionGroups()
        {
            var productionGroups = new Mock<IProductionGroup>();

            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var corrugate = new Corrugate {Id = new Guid()};

            productionGroupService.RemoveCorrugateFromProductionGroups(corrugate);

            productionGroups.Verify(p => p.RemoveCorrugateFromProductionGroups(It.Is<Corrugate>(g => g.Id == corrugate.Id)), Times.Once);
        }

        [TestMethod]
        public void ShouldCallProductionGroupManagerMethodRemoveMachineGroupFromProductionGroup()
        {
            var productionGroups = new Mock<IProductionGroup>();

            var serviceLocator = new Mock<IServiceLocator>();

            var productionGroupService = new ProductionGroupService(productionGroups.Object, serviceLocator.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, null, logger.Object);

            var machineGroup = new MachineGroup {Id = new Guid()};

            productionGroupService.RemoveMachineGroupFromProductionGroup(machineGroup);

            productionGroups.Verify(p => p.RemoveMachineGroupFromProductionGroup(It.Is<MachineGroup>(g => g.Id == machineGroup.Id)), Times.Once);
        }
    }
}
