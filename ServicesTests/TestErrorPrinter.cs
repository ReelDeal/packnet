using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Utils;

namespace ServicesTests
{
    using PackNet.Common.Interfaces.Enums;

    public class TestErrorPrinter : IPrinter
    {
        Subject<PrintJobStatus> printJobStatusChangedSubject = new Subject<PrintJobStatus>();
        Subject<PrinterStatus> printerStatusChangedSubject = new Subject<PrinterStatus>();

        public IObservable<PrinterStatus> PrinterStatusChangedObservable { get { return printerStatusChangedSubject.AsObservable(); } }

        public IObservable<PrintJobStatus> PrintJobStatusChangedObservable { get { return printJobStatusChangedSubject.AsObservable(); } }

        public void Print(Printable itemToPrint)
        {
            var task = Task.Factory.StartNew(
                () =>
                { throw new Exception("I am a bad printer"); });
            task.ContinueWith(
                (t) =>
                {
                    //Simulate error handling on the printer side
                    if (t.IsFaulted)
                    {
                        Debug.WriteLine(t.Exception.InnerExceptions.Aggregate("", (s, exception) => exception.Message + ", "));
                        printJobStatusChangedSubject.OnNext(
                            new PrintJobStatus
                            {
                                JobStatus = ErrorProducibleStatuses.ProducibleRemoved,
                                ErrorMessage = t.Exception.InnerExceptions.Aggregate("", (s, exception) => exception.Message + ", "),
                                PrinterId = Id,
                                ItemToPrint = itemToPrint
                            });
                    }
                });
        }

        public Guid Id { get; set; }

        public MachineTypes MachineType { get; private set; }


        public MachineStatuses CurrentStatus { get; set; }
        public IObservable<MachineProductionStatuses> CurrentProductionStatusObservable { get; private set; }
        public IEnumerable<ICapability> CurrentCapabilities { get; private set; }
        public MachineProductionStatuses CurrentProductionStatus { get; set; }
        public List<MachineError> Errors { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }

        public IObservable<MachineStatuses> CurrentStatusChangedObservable { get; private set; }
        public IObservable<MachineProductionStatuses> MachineProductionStatusesObservable { get; private set; }
        public IObservable<IEnumerable<ICapability>> CurrentCapabilitiesChangedObservable { get; private set; }

        public void ResetEventCommunication()
        {
            throw new NotImplementedException();
        }

        public void BeginEventCommunication()
        {
            throw new NotImplementedException();
        }
    }
}