﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Business.PackagingDesigns;
using PackNet.Common.Eventing;
using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.ComponentsConfigurations;
using PackNet.Services.CodeGeneration;

using Testing.Specificity;

namespace ServicesTests
{
    [TestClass]
    public class FringeCodegenBugsTests
    {
        private Corrugate track1Corrugate;
        private Corrugate track2Corrugate;

        IMachine machine;

        private Mock<IAggregateMachineService> machineServiceMock;
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IPackagingDesignManager> packagingDesignMock;
        private Mock<IComponentsConfigurationRepository> componentsConfigurationMock;
        private Mock<ILogger> loggerMock;
        private EventAggregator aggregator;

        private CodeGenerationService service;



        [TestInitialize]
        public void Setup()
        {
            machineServiceMock = new Mock<IAggregateMachineService>();
            packagingDesignMock = new Mock<IPackagingDesignManager>();
            componentsConfigurationMock = GetComponentsConfigurationMock();
            loggerMock = new Mock<ILogger>();
            aggregator = new EventAggregator();


            serviceLocatorMock = GetServiceLocator();

            service = new CodeGenerationService(serviceLocatorMock.Object, packagingDesignMock.Object);

        }

        private static Mock<IComponentsConfigurationRepository> GetComponentsConfigurationMock()
        {
            var mock = new Mock<IComponentsConfigurationRepository>();
            mock.Setup(m => m.All()).Returns(new List<ComponentsConfiguration>
            {
                new ComponentsConfiguration
                {
                    Id = Guid.NewGuid(),
                    CodeGeneratorSettings = new CodeGeneratorSettings { MinimumLineDistanceToCorrugateEdge = 1 }
                }
            });

            return mock;
        }

        private Mock<IServiceLocator> GetServiceLocator()
        {
            var locator = new Mock<IServiceLocator>();

            locator.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(aggregator);
            locator.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);
            locator.Setup(m => m.Locate<ILogger>()).Returns(loggerMock.Object);
            locator.Setup(m => m.Locate<IAggregateMachineService>()).Returns(machineServiceMock.Object);

            return locator;
        }

        [TestMethod]
        [Ignore]
        [TestCategory("Integration")]
        [DeploymentItem("TestFiles", "FringeTestFiles")]
        public void ArcherBoxCrashBugThingy()
        {
            track1Corrugate = new Corrugate { Alias = "T1Corr", Id = Guid.NewGuid(), Thickness = 3, Quality = 1, Width = 500 };
            track2Corrugate = new Corrugate { Alias = "T2Corr", Id = Guid.NewGuid(), Thickness = 3, Quality = 1, Width = 700 };

            machine = CreateFusionMachine(@"FringeTestFiles/PhysicalMachineSettings/machineTheoretical_mm.xml", Guid.NewGuid(), track1Corrugate, track2Corrugate);
            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { machine });

            var cartonOnCorrugateTrack1 = new CartonOnCorrugate { Corrugate = track1Corrugate, TileCount = 1 };
            
            var sequence = CartonSequence();
            var instructionLists = new Dictionary<double, List<IInstructionItem>>();
            InstructionLongHeadPositionItem lastPositioningItem = null;
            for (var i = 0; i < 1000; i++)
            {
                var carton = GetProducibleWithMeasurementsAndCoc(2010002, sequence.ElementAt(i % sequence.Count),
                    sequence.ElementAt(i % sequence.Count), sequence.ElementAt(i % sequence.Count), cartonOnCorrugateTrack1);

                SetupDesignManagerMockToReturnDesignFromFile(carton);

                var instructions = service.GenerateCode(machine.Id, carton);

                if (i != 0)
                {
                    Specify.That(instructions).Should.Not.BeNull();

                    Specify.That(VerifyLongHeadPositioning(instructions, lastPositioningItem)).Should.BeTrue();
                    
                   var temp =
                        instructions.First(inst => inst is InstructionLongHeadPositionItem) as InstructionLongHeadPositionItem;
                    if (temp != null && temp.LongHeadsToPosition.Any())
                        lastPositioningItem = temp;
                }
            }
        }

        private bool VerifyLongHeadPositioning(IEnumerable<IInstructionItem> instructions, InstructionLongHeadPositionItem lastPositioningItem)
        {
            var currentLongheadPositioing = instructions.FirstOrDefault(i => i is InstructionLongHeadPositionItem) as InstructionLongHeadPositionItem;
            
            Specify.That(currentLongheadPositioing).Should.Not.BeNull();

            if (lastPositioningItem != null)
            {
                var lhPos = lastPositioningItem.LongHeadsToPosition.FirstOrDefault();
                if (lhPos == null || lhPos.LongHeadNumber != 1)
                    return true;

                var oldPos = lastPositioningItem.LongHeadsToPosition.First(l => l.LongHeadNumber == 2).Position;
                var currentPos = currentLongheadPositioing.LongHeadsToPosition.First(l => l.LongHeadNumber == 1).Position;

                Console.WriteLine("C LH1: {0}, O LH2: {1} d: {2}", currentPos, oldPos, Math.Abs(oldPos - currentPos));
                Specify.That(Math.Abs(currentPos - oldPos) >= 64).Should.BeTrue();
            }
           
            return true;
        }

        private void SetupDesignManagerMockToReturnDesignFromFile(ICarton carton)
        {
            var testDataPath = Path.Combine(Environment.CurrentDirectory, "FringeTestFiles");

            var designManager =
                new PackagingDesignManager(testDataPath, new Mock<ICachingService>().Object,
                    new Mock<ILogger>().Object);

            designManager.LoadDesigns();
            var design =
                designManager.GetDesigns()
                    .FirstOrDefault(d => d.Id == carton.DesignId);

            var physicalDesign = design.AsPhysicalDesign(carton.GetDesignFormulaParameters(carton.CartonOnCorrugate.Corrugate));

            packagingDesignMock.Setup(
                dm => dm.GetPhyscialDesignForCarton(carton, carton.CartonOnCorrugate.Corrugate, carton.CartonOnCorrugate.Rotated ? OrientationEnum.Degree90 : OrientationEnum.Degree0, 1))
                .Returns<ICarton, Corrugate, bool>((a, b, c) => design.AsPhysicalDesign(carton.GetDesignFormulaParameters(carton.CartonOnCorrugate.Corrugate)));

            packagingDesignMock.Setup(
                dm => dm.CalculateUsage(carton, carton.CartonOnCorrugate.Corrugate, 1, carton.CartonOnCorrugate.Rotated))
                .Returns<ICarton, Corrugate, bool>((a, b, c) => new PackagingDesignCalculation { Length = physicalDesign.Length, Width = physicalDesign.Width });
        }

        private ICarton GetProducibleWithMeasurementsAndCoc(int designId, double length, double width, double height, CartonOnCorrugate coc)
        {
            return new Carton
            {
                DesignId = designId,
                Id = Guid.NewGuid(),
                Length = length,
                Width = width,
                Height = height,
                CartonOnCorrugate = coc,
                TrackNumber = 1,
                XValues = new Dictionary<string, double>()
            };
        }

        private static List<double> CartonSequence()
        {
            return new List<double>()
            {
                5 * 25.4,
                6 * 25.4,
                7 * 25.4,
                5 * 25.4,
                5 * 25.4,
                6 * 25.4,
                5 * 25.4,
                6 * 25.4,
                7 * 25.4,
                5 * 25.4,
                5 * 25.4,
                6 * 25.4,
                5 * 25.4,
                7 * 25.4,
                5 * 25.4,
                5 * 25.4,
                6 * 25.4,
                5 * 25.4,
                6 * 25.4,
                7 * 25.4,
                5 * 25.4,
                5 * 25.4,
            };
        }

        private IMachine CreateFusionMachine(string physicalPath, Guid machineId, Corrugate track1Corrugate, Corrugate track2Corrugate)
        {
            var fusionMachine = new FusionMachine()
            {
                Id = machineId,
                PhysicalMachineSettings = FusionPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1), physicalPath),
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        TrackNumber = 1,
                        TrackOffset = track1Corrugate.Width + 20
                    },
                    new Track()
                    {
                        TrackNumber = 2,
                        TrackOffset = 20
                    }
                }
            };
            
            fusionMachine.LoadCorrugate(fusionMachine.Tracks.First(), track1Corrugate);
            if (fusionMachine.Tracks.Count == 2)
            {
                fusionMachine.LoadCorrugate(fusionMachine.Tracks.Last(), track2Corrugate);
            }


            (fusionMachine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters.LongHeads.ForEach(l => l.Position = l.Number * 100);

            return fusionMachine;
        }
    }
}
