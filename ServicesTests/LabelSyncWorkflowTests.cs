﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Consulting.Workflow.Activities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Business.Workflows;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Common.WorkflowTracking;
using PackNet.Services;
using PackNet.Services.MachineServices;
using Testing.Specificity;

using TestUtils;

namespace WorkflowTests
{
    [TestClass]
    public class LabelSyncWorkflowTests
    {
        private Mock<IServiceLocator> serviceLocatorMock;
        private ILogger logger;
        private Mock<IAggregateMachineService> aggregateMachineService;
        private ZebraPrinter printerPriority;
        private ZebraPrinter printerStandard;
        private IPacksizeCutCreaseMachine packagingMachine;
        private EventAggregator eventAggregator;
        private MachineGroup machineGroup;
        private Mock<IMachineGroupService> machineGroupService;
        private Kit kit;
        private Label label;
        private Carton carton;
        private Kit kit2;
        private Label label2;
        private Carton carton2;
        private Mock<IWorkflowTrackingService> tpMock;
        private Corrugate corrugate1;

        [TestInitialize]
        public void Init()
        {
            var dummyToIncludeInBuild = new ExecuteXamlWorkflow();

            Environment.SetEnvironmentVariable("PackNetWorkflows", Path.Combine(Environment.CurrentDirectory, "Workflows"));

            corrugate1 = new Corrugate { Alias = "one" };
            
            aggregateMachineService = new Mock<IAggregateMachineService>();
            serviceLocatorMock = new Mock<IServiceLocator>();
            logger = new ConsoleLogger();
            tpMock = new Mock<IWorkflowTrackingService>();

            eventAggregator = new EventAggregator();
            machineGroupService = new Mock<IMachineGroupService>();
            serviceLocatorMock.Setup(sl => sl.Locate<ILogger>()).Returns(logger);
            serviceLocatorMock.Setup(sl => sl.Locate<IAggregateMachineService>()).Returns(aggregateMachineService.Object);
            serviceLocatorMock.Setup(sl => sl.Locate<IMachineGroupService>()).Returns(machineGroupService.Object);
            serviceLocatorMock.Setup(sl => sl.Locate<IEventAggregatorPublisher>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(sl => sl.Locate<IEventAggregatorSubscriber>()).Returns(eventAggregator);
            serviceLocatorMock.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            serviceLocatorMock.Setup(sl=> sl.Locate<IWorkflowTrackingService>()).Returns(tpMock.Object);
            packagingMachine = new FusionMachine { Id = Guid.NewGuid(), Tracks = new ConcurrentList<Track> { new Track { LoadedCorrugate = corrugate1 } } };
            packagingMachine.CurrentProductionStatus = MachineProductionStatuses.ProductionIdle;

            printerStandard = new ZebraPrinter { 
                Id = Guid.NewGuid(), 
                CurrentProductionStatus = MachineProductionStatuses.ProductionIdle,
                CurrentStatus = MachineStatuses.MachineOnline
            };
            printerStandard.AddOrUpdateCapabilities(new List<ICapability> { new BasicCapability<string>("StandardLabel") });

            printerPriority = new ZebraPrinter
            {
                Id = Guid.NewGuid(),
                CurrentProductionStatus = MachineProductionStatuses.ProductionIdle,
                CurrentStatus = MachineStatuses.MachineOnline
            };
            printerPriority.AddOrUpdateCapabilities(new List<ICapability> { new BasicCapability<string>("PriorityLabel") });

            aggregateMachineService.Setup(ms => ms.FindById(packagingMachine.Id)).Returns(packagingMachine);
            aggregateMachineService.Setup(ms => ms.FindById(printerPriority.Id)).Returns(printerPriority);
            aggregateMachineService.Setup(ms => ms.FindById(printerStandard.Id)).Returns(printerStandard);

            machineGroup = new MachineGroup
            {
                Alias = "testmg",
                Id = Guid.NewGuid(),
                WorkflowPath = Path.Combine(Environment.CurrentDirectory, "Workflows", "PacksizeLabelSync.xaml"),
                ConfiguredMachines = new ConcurrentList<Guid> { packagingMachine.Id, printerPriority.Id, printerStandard.Id },
                CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline
            };




            //setup our producible
            kit = new Kit { CustomerUniqueId = "333", Id = Guid.NewGuid() };
            label = new Label { CustomerUniqueId = "44" };
            label.Restrictions.Add(new BasicRestriction<string>("StandardLabel"));
            kit.AddProducible(label);
            carton = new Carton { CustomerUniqueId = "55", CartonOnCorrugate = new CartonOnCorrugate{Corrugate = corrugate1 }};
            carton.CartonOnCorrugate.ProducibleMachines.Add(packagingMachine.Id, packagingMachine.Alias);
            kit.AddProducible(carton);
            
            kit2 = new Kit { CustomerUniqueId = "666", Id = Guid.NewGuid() };
            label2 = new Label { CustomerUniqueId = "77" };
            label2.Restrictions.Add(new BasicRestriction<string>("PriorityLabel"));
            kit2.AddProducible(label2);
            carton2 = new Carton { CustomerUniqueId = "88" };
            kit2.AddProducible(carton2);

        }

        [TestMethod]
        [DeploymentItem("TestFiles\\Workflows", "Workflows")]
        public void ProduceBoxWithLabelOnMgWhenNoOtherItemsCurrentlyBeingProduced()
        {
            /*
             Machine group with 2 printers and a box machine
             * Producible is a kit with 1 box and 1 labelstandard
             * Printers will complete job immediately 
             * cut crease machine will complete job immediatly after told to release box.
             * Scenerio:
             * Expected label to be requested
             * Expected box to be requested
             * Expect box to be signaled for release
             * Expect worklfow to end.
             */
            
            var complete = false;

            //When we are told to create box set status to started.
            aggregateMachineService.Setup(ms => ms.Produce(It.IsAny<Guid>(), It.IsAny<IProducible>()))
                .Callback<Guid, IProducible>((machineid, producible) =>
                {
                    // start it
                    producible.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
                    if (producible is IPrintable)
                    {
                        Thread.Sleep(200);
                        producible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                    }
                    else
                    {
                        //ensure we don't hold the box
                        Specify.That(producible.Restrictions.Any(r => r is BasicRestriction<string>
                            && ((BasicRestriction<string>)r).Value == CartonRestrictions.ShouldWaitForBoxRelease)).Should.BeFalse();
                        Specify.That(kit.Restrictions.Any(r => r is BasicRestriction<string>
                            && ((BasicRestriction<string>)r).Value == CartonRestrictions.ShouldWaitForBoxRelease)).Should.BeFalse();
                        complete = true;
                    }
                    // complete happens when release box called.
                    Console.WriteLine("--Test Produce({0}) exiting status {1}", producible.CustomerUniqueId, producible.ProducibleStatus);

                });

            
            //set up that we are only working on this producible.
            machineGroupService.Setup(mgs => mgs.GetProducibleFirstInQueue(machineGroup)).Returns(kit);

            //Fire it up momma
            var dispatcher = new MachineGroupWorkflowDispatcher(serviceLocatorMock.Object, eventAggregator, logger);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    dispatcher.DispatchCreateProducibleWorkflow(machineGroup, kit);
                    Console.WriteLine("workflow complete");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Workflow exception:{0}", e);
                    throw;
                }
            });

            Console.WriteLine("workflow complete");

            Retry.UntilTrue(()=> complete, TimeSpan.FromSeconds(3), TimeSpan.FromMilliseconds(200));
            
            Specify.That(complete).Should.BeTrue("producible not set to complete via kits.");
        }

        [TestMethod]
        [DeploymentItem("TestFiles\\Workflows", "Workflows")]
        public void LabelError_AfterProducibleStarted_ShouldFailKit()
        {
            Assert.Inconclusive("Reactivated bug 13912 since it changed the label sync");
            Func<bool> boxRequested = () => carton.ProducibleStatus == InProductionProducibleStatuses.ProducibleProductionStarted;
            var complete = false;

            kit.ProducibleStatusObservable.DurableSubscribe(status =>
            {
                if (status == ErrorProducibleStatuses.ProducibleFailed)
                    complete = true;
                Console.WriteLine("--Test kit({0}) exiting status {1}", kit.CustomerUniqueId, status);
            }, logger);

            Kit nullKit = null;
            //set up that we have another job already being worked by the mg
            machineGroupService.Setup(mgs => mgs.GetProducibleFirstInQueue(machineGroup)).Returns(nullKit);

            //Fire it up momma
            var dispatcher = new MachineGroupWorkflowDispatcher(serviceLocatorMock.Object, eventAggregator, logger);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    dispatcher.DispatchCreateProducibleWorkflow(machineGroup, kit);
                    Console.WriteLine("workflow complete");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Workflow exception:{0}", e);
                    throw;
                }
            });
            carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            label.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;

            Retry.UntilTrue(boxRequested, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(boxRequested()).Should.BeEqualTo(true, "box should have been requested");

            label.ProducibleStatus = ErrorExternalPrintProducibleStatuses.LabelRequestFailed;

            Retry.UntilTrue(() => complete, TimeSpan.FromSeconds(5), TimeSpan.FromMilliseconds(200));

            Specify.That(kit.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed);
        }

        [TestMethod]
        [DeploymentItem("TestFiles\\Workflows", "Workflows")]
        public void ProduceBoxWithLabelOnMgWhenLastJobLabelHangingBoxComplete()
        {
            /*
             Machine group with 2 printers and a box machine
             * Producible is a kit with 1 box and 1 labelstandard
             * Printers will complete job immediately 
             * cut crease machine will complete job immediatly after told to release box.
             * scenario:*/
            // previous job has label sitting on different printer in mg
            label2.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
            // previous box has been completed
            carton2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            // Expectation send box to packaging machine
            Func<bool> boxRequested = () => carton.ProducibleStatus == InProductionProducibleStatuses.ProducibleProductionStarted;
            // Expectation not to release box until last jobs label is complete
            Func<bool> boxReleased = () =>
                carton.ProducibleStatus == ProducibleStatuses.ProducibleCompleted;
            // Expectation request label print when last label complete
            Func<bool> labelRequested = () => label.ProducibleStatus == InProductionProducibleStatuses.ProducibleProductionStarted;
            // Expect workflow to end.

            var complete = false;
            var workflowComplete = false;

            //When we are told to create box set status to started.
            aggregateMachineService.Setup(ms => ms.Produce(It.IsAny<Guid>(), It.IsAny<IProducible>()))
                .Callback<Guid, IProducible>((machineid, producible) =>
                {
                    // start it
                    producible.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
                    if (producible is IPrintable)
                    {
                        Thread.Sleep(200);
                        producible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                    }
                    else
                    {
                        //ensure we don't hold the box
                        Specify.That(carton.Restrictions.Any(r => r is BasicRestriction<string>
                            && ((BasicRestriction<string>)r).Value == CartonRestrictions.ShouldWaitForBoxRelease)).Should.BeTrue();
                       
                    }
                    // complete happens when release box called.
                    Console.WriteLine( "--Test Produce({0}) exiting status {1}", producible.CustomerUniqueId, producible.ProducibleStatus);

                });

            // when we're told to release box complete producible.
            eventAggregator.GetEvent<Message<IProducible>>().Where(m => m.MessageType == CartonMessages.ReleaseBox).Subscribe(m =>
            {
             //   if (m.MachineGroupId != machineGroup.Id) Assert.Fail("Machine group id not set when call to release box made.");

                m.Data.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                Console.WriteLine("--Test ReleaseBox({0}) exiting status {1}", m.Data.CustomerUniqueId, m.Data.ProducibleStatus);

            });

            kit.ProducibleStatusObservable.DurableSubscribe(status =>
            {
                if (status == ProducibleStatuses.ProducibleCompleted)
                    complete = true;
                Console.WriteLine("--Test kit({0}) exiting status {1}", kit.CustomerUniqueId, status);
            }, logger);

            //set up that we have another job already being worked by the mg
            machineGroupService.Setup(mgs => mgs.GetProducibleFirstInQueue(machineGroup)).Returns(kit2);

            //Fire it up momma
            var dispatcher = new MachineGroupWorkflowDispatcher(serviceLocatorMock.Object, eventAggregator, logger);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    dispatcher.DispatchCreateProducibleWorkflow(machineGroup, kit);
                    workflowComplete = true;
                    Console.WriteLine("workflow complete");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Workflow exception:{0}", e);
                    throw;
                }
            });

            Retry.UntilTrue(boxRequested, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(boxRequested()).Should.BeEqualTo(true, "box should have been requested");
            Specify.That(labelRequested()).Should.BeEqualTo(false, "label should not yet be requested");
            Specify.That(boxReleased()).Should.BeEqualTo(false, "box was signaled for release before expected");

            //signal label peel off has completed.
            label2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            Retry.UntilTrue(boxReleased, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(boxReleased()).Should.BeEqualTo(true, "box was never signaled for release");
            Retry.UntilTrue(labelRequested, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(labelRequested()).Should.BeEqualTo(true, "label never requested");

            Retry.UntilTrue(() => complete == true, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(complete).Should.BeTrue("producible not set to complete via kits.");

            Retry.UntilTrue(() => workflowComplete == true, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(workflowComplete).Should.BeTrue("workflow has not ended.");
        }

        [TestMethod]
        [DeploymentItem("TestFiles\\Workflows", "Workflows")]
        public void MachineGroupError_AfterProducibleStarted_ShouldFailKit()
        {
            Assert.Inconclusive("Reactivated bug 13912 since it changed the label sync");
            // Expectation send box to packaging machine
            Func<bool> boxRequested = () => carton.ProducibleStatus == InProductionProducibleStatuses.ProducibleProductionStarted;

            // Expect workflow to end.
            var complete = false;

            kit.ProducibleStatusObservable.DurableSubscribe(status =>
            {
                if (status == ErrorProducibleStatuses.ProducibleFailed)
                    complete = true;
                Console.WriteLine("--Test kit({0}) exiting status {1}", kit.CustomerUniqueId, status);
            }, logger);

            Kit nullKit = null;
            machineGroupService.Setup(mgs => mgs.GetProducibleFirstInQueue(machineGroup)).Returns(nullKit);

            //Fire it up momma
            var dispatcher = new MachineGroupWorkflowDispatcher(serviceLocatorMock.Object, eventAggregator, logger);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    dispatcher.DispatchCreateProducibleWorkflow(machineGroup, kit);
                    Console.WriteLine("workflow complete");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Workflow exception:{0}", e);
                    throw;
                }
            });
            carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;

            Retry.UntilTrue(boxRequested, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));

            Specify.That(boxRequested()).Should.BeEqualTo(true, "box should have been requested");

            machineGroup.CurrentStatus = MachineGroupUnavailableStatuses.MachineGroupChangingCorrugate;

            Retry.UntilTrue(() => complete, TimeSpan.FromSeconds(5), TimeSpan.FromMilliseconds(200));

            Specify.That(kit.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed);
        }

        [TestMethod]
        [DeploymentItem(@"TestFiles\Workflows", "Workflows")]
        public void ProduceBoxWithLabelOnMgWhenLastJobsBeingCutButLabelHasBeenRemoved()
        {
            /*
             Machine group with 2 printers and a box machine
             * Producible is a kit with 1 box and 1 labelstandard
             * Printers will complete job immediately 
             * cut crease machine will complete job immediatly after told to release box.
             * scenario:*/
            // previous job has label in operators hand peeled off
            label2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            // previous box is being cut
            carton2.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            // Expectation no box should be started before last box is completed
            Func<bool> boxStarted = () => carton.ProducibleStatus == InProductionProducibleStatuses.ProducibleProductionStarted;
            // Expectation no label should be requested before last job completed
            Func<bool> labelWaitingForPeeloff = () => label.ProducibleStatus ==  ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
            // Expectation not to release box until last jobs label is complete
            Func<bool> boxReleased = () => carton.ProducibleStatus == ProducibleStatuses.ProducibleCompleted;
           // Expect workflow to end.

            var workflowComplete = false;
            //When we are told to create box set status to started.
            aggregateMachineService.Setup(ms => ms.Produce(It.IsAny<Guid>(), It.IsAny<IProducible>()))
                .Callback<Guid, IProducible>((machineid, producible) =>
                {
                    // start it
                    producible.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
                    if (machineid == printerPriority.Id || machineid == printerStandard.Id)
                    {
                        producible.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
                    }
                    else
                    {
                        //ensure we don't hold the box
                        Specify.That(carton.Restrictions.Any(r => r is BasicRestriction<string>
                            && ((BasicRestriction<string>)r).Value == CartonRestrictions.ShouldWaitForBoxRelease)).Should.BeTrue();

                    }
                    // complete happens when release box called.
                    Console.WriteLine("--Test Produce({0}) exiting status {1}", producible.CustomerUniqueId, producible.ProducibleStatus);

                });

            // when we're told to release box complete producible.
            eventAggregator.GetEvent<Message<IProducible>>().Where(m => m.MessageType == CartonMessages.ReleaseBox).Subscribe(m =>
            {
                m.Data.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
                Console.WriteLine("--Test ReleaseBox({0}) exiting status {1}", m.Data.CustomerUniqueId, m.Data.ProducibleStatus);

            });

            //set up that we have another job already being worked by the mg
            machineGroupService.Setup(mgs => mgs.GetProducibleFirstInQueue(machineGroup)).Returns(kit2);

            //Fire it up momma
            var dispatcher = new MachineGroupWorkflowDispatcher(serviceLocatorMock.Object, eventAggregator, logger);
            Task.Factory.StartNew(() =>
            {
                try
                {
                    dispatcher.DispatchCreateProducibleWorkflow(machineGroup, kit);
                    workflowComplete = true;
                    Console.WriteLine("workflow complete");
                }
                catch (Exception e)
                {
                    Console.WriteLine("Workflow exception:{0}", e);
                    Specify.Failure("Workflow exception");
                }
            });
            
            Specify.That(boxStarted()).Should.BeEqualTo(false, "box was started before expected");
            Specify.That(boxReleased()).Should.BeEqualTo(false, "box was signaled for release before expected");

            //signal last job box completed.
            //This should trigger request of next box, release of that box and request of label since last job is now fully completed
            carton2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Retry.UntilTrue(boxReleased, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(boxReleased()).Should.BeEqualTo(true, "box never released");

            //never removed label so this should be our last state
            Retry.Do(() => { if (!labelWaitingForPeeloff()) throw new Exception("label never requested"); }, TimeSpan.FromMilliseconds(500), 10);

            Retry.UntilTrue(() => workflowComplete == true, TimeSpan.FromSeconds(20), TimeSpan.FromMilliseconds(200));
            Specify.That(workflowComplete).Should.BeTrue("workflow has not ended.");
        }
    }
}