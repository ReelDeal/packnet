﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Common.WorkflowActivities.Eventing;
using PackNet.Services.MachineServices;

using Testing.Specificity;

using TestUtils;

namespace WorkflowTests
{
    [TestClass]
    public class CodeActivityWorkflowTests
    {
        [TestMethod]
        [Timeout(2000)]
        public void ShouldReturnIfWaitStatusIsAlreadySatisfied()
        {
            var serviceLocatorMock = new Mock<IServiceLocator>();
            var loggerMock = new Mock<ILogger>();
            serviceLocatorMock.Setup(sl => sl.Locate<ILogger>()).Returns(loggerMock.Object);
            var p = new Carton { ProducibleStatus = ProducibleStatuses.ProducibleCompleted };
            var activity = new WaitForProducibleStatus();
            activity.Producible = new InArgument<IProducible>(context => p);
            activity.ProducibleStatus = new InArgument<ProducibleStatuses>(context => ProducibleStatuses.ProducibleCompleted);
            activity.ServiceLocator = new InArgument<IServiceLocator>(context => serviceLocatorMock.Object);
            IDictionary<string, object> inArgs = new Dictionary<string, object>();

            var result = WorkflowInvoker.Invoke(activity, inArgs);
            Specify.That(result).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        [Timeout(2000)]
        public void ShouldReturnIfWaitStatusAllIsAlreadySatisfied()
        {
            var serviceLocatorMock = new Mock<IServiceLocator>();
            var logger = new ConsoleLogger();
            serviceLocatorMock.Setup(sl => sl.Locate<ILogger>()).Returns(logger);
            var p1 = new Carton { ProducibleStatus = ProducibleStatuses.ProducibleCompleted };
            var p2 = new Carton { ProducibleStatus = ProducibleStatuses.ProducibleCompleted };
            var p3 = new Carton { ProducibleStatus = ProducibleStatuses.ProducibleCompleted };
            var activity = new WaitForProducibleStatusForAll();
            activity.Producibles = new InArgument<IEnumerable<IProducible>>(context => new List<IProducible> { p1, p2, p3 });
            activity.ProducibleStatus = new InArgument<ProducibleStatuses>(context => ProducibleStatuses.ProducibleCompleted);
            activity.ServiceLocator = new InArgument<IServiceLocator>(context => serviceLocatorMock.Object);
            IDictionary<string, object> inArgs = new Dictionary<string, object>();

            var result = WorkflowInvoker.Invoke(activity, inArgs);
            Specify.That(result).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        //[Timeout(3000)]
        public void ShouldReturnWhenWaitStatusAllIsSatisfied()
        {
            var serviceLocatorMock = new Mock<IServiceLocator>();
            var logger = new ConsoleLogger();
            serviceLocatorMock.Setup(sl => sl.Locate<ILogger>()).Returns(logger);
            var p1 = new Carton { ProducibleStatus = ProducibleStatuses.ProducibleRemoved };
            var p2 = new Carton { ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported };
            var p3 = new Carton { ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted };
            var activity = new WaitForProducibleStatusForAll();
            activity.Producibles = new InArgument<IEnumerable<IProducible>>(context => new List<IProducible> { p1, p2, p3 });
            activity.ProducibleStatus = new InArgument<ProducibleStatuses>(context => ProducibleStatuses.ProducibleCompleted);
            activity.ServiceLocator = new InArgument<IServiceLocator>(context => serviceLocatorMock.Object);
            IDictionary<string, object> inArgs = new Dictionary<string, object>();

            var result = ProducibleStatuses.ProducibleRemoved;
            var activityComplete = false;
            Func<bool> IsActivityComplete = () =>
                activityComplete;
            Task.Factory.StartNew(() =>
            {
                result = WorkflowInvoker.Invoke(activity, inArgs);
                activityComplete = true;
            });
            Thread.Sleep(100);
            Specify.That(activityComplete).Should.BeFalse();
            p2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Thread.Sleep(500);

            Specify.That(activityComplete).Should.BeFalse();
            p1.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Thread.Sleep(100);

            Specify.That(activityComplete).Should.BeFalse();
            p3.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            Thread.Sleep(500);

            Specify.That(activityComplete).Should.BeFalse();
            p3.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Thread.Sleep(100);

            Retry.UntilTrue(IsActivityComplete, TimeSpan.FromSeconds(2));
            Specify.That(result).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        [DeploymentItem("TestFiles", "TestFiles")]
        public void ShouldWaitUntilProducibleStatusChanged()
        {
            var complete = false;
            var workflowComplete = false;
            var ea = new EventAggregator();
            var slMock = new Mock<IServiceLocator>();
            var loggerMock = new Mock<ILogger>();
            slMock.Setup(sl => sl.Locate<ILogger>()).Returns(loggerMock.Object);
            var mg = new MachineGroup
            {
                Alias = "testmg",
                WorkflowPath = Path.Combine(Environment.CurrentDirectory, "TestFiles", "Workflows", "ProducibleWaitAndMachineGroupError.xaml")
            };
            mg.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            var kit = new Kit { CustomerUniqueId = "333", Id = Guid.NewGuid() };
            var label = new Label { CustomerUniqueId = "44" };
            kit.AddProducible(label);
            var carton = new Carton { CustomerUniqueId = "55" };
            kit.AddProducible(carton);

            kit.ProducibleStatusObservable.DurableSubscribe(status =>
            {
                if (status == ProducibleStatuses.ProducibleCompleted)
                    complete = true;

            }, loggerMock.Object);

            var dispatcher = new MachineGroupWorkflowDispatcher(slMock.Object, ea, loggerMock.Object);
            Task.Factory.StartNew(() =>
            {
                dispatcher.DispatchCreateProducibleWorkflow(mg, kit);
                workflowComplete = true;
                Console.WriteLine("workflow complete");
            });
            Thread.Sleep(500);

            //wait for wf to update status to started
            Retry.Do(() => { if (label.ProducibleStatus != InProductionProducibleStatuses.ProducibleProductionStarted) throw new Exception("not complete"); }, TimeSpan.FromMilliseconds(1000), 10);
            Assert.IsFalse(complete);
            Assert.IsFalse(workflowComplete);// workflow should be in a pick waiting
            label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            //wait cfor wf to update status to started
            Retry.Do(() => { if (carton.ProducibleStatus != InProductionProducibleStatuses.ProducibleProductionStarted) throw new Exception("not complete"); }, TimeSpan.FromMilliseconds(200), 10);
            Assert.IsFalse(complete);
            Assert.IsFalse(workflowComplete);// workflow should be in apick waiting

            // complete carton which will also complete kit and should end the workflow
            carton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;// completes the carton which inturn automatically completes the kit.  Workflow should finish.

            Retry.Do(() => { if (!complete) throw new Exception("not complete"); }, TimeSpan.FromMilliseconds(200), 10);
            Retry.Do(() => { if (!workflowComplete) throw new Exception("workflow not complete"); }, TimeSpan.FromMilliseconds(200), 10);

            Specify.That(complete).Should.BeTrue("producible not set to complete via kits.");
            Specify.That(workflowComplete).Should.BeTrue("workflow did not exit.");
        }

        //[TestMethod]
        public void ShouldWaitUntilProducibleStatusChangedWhen2WorkflowsRunning()
        {
            var complete = false;
            var workflowComplete = false;
            var complete2 = false;
            var workflowComplete2 = false;
            var ea = new EventAggregator();
            var slMock = new Mock<IServiceLocator>();

            var logger = new ConsoleLogger();
            slMock.Setup(sl => sl.Locate<ILogger>()).Returns(logger);

            var mg = new MachineGroup
            {
                Alias = "testmg",
                WorkflowPath = Path.Combine(Environment.CurrentDirectory, "TestFiles", "Workflows", "ProducibleWaitAndMachineGroupError.xaml")
            };

            var kit = new Kit { CustomerUniqueId = "333", Id = Guid.NewGuid() };
            var label = new Label { CustomerUniqueId = "44" };
            kit.AddProducible(label);
            var carton = new Carton { CustomerUniqueId = "55" };
            kit.AddProducible(carton);

            kit.ProducibleStatusObservable.DurableSubscribe(status =>
            {
                if (status == ProducibleStatuses.ProducibleCompleted)
                    complete = true;

            }, logger);

            var kit2 = new Kit { CustomerUniqueId = "666", Id = Guid.NewGuid() };
            var label2 = new Label { CustomerUniqueId = "77" };
            kit.AddProducible(label2);
            var carton2 = new Carton { CustomerUniqueId = "88" };
            kit.AddProducible(carton2);

            kit2.ProducibleStatusObservable.DurableSubscribe(status =>
            {
                if (status == ProducibleStatuses.ProducibleCompleted)
                    complete2 = true;

            }, logger);



            var dispatcher = new MachineGroupWorkflowDispatcher(slMock.Object, ea, logger);
            Task.Factory.StartNew(() =>
            {
                dispatcher.DispatchCreateProducibleWorkflow(mg, kit);
                workflowComplete = true;
                Console.WriteLine("workflow complete");
            });
            Task.Factory.StartNew(() =>
            {
                dispatcher.DispatchCreateProducibleWorkflow(mg, kit2);
                workflowComplete2 = true;
                Console.WriteLine("workflow complete");
            });

            Thread.Sleep(500);
            Task.Factory.StartNew(() =>
            {
                //wait for wf to update status to started
                Retry.Do(() =>
                {
                    if (label.ProducibleStatus != InProductionProducibleStatuses.ProducibleProductionStarted)
                        throw new Exception("not complete");
                }, TimeSpan.FromMilliseconds(200), 10);
                Assert.IsFalse(complete);
                Assert.IsFalse(workflowComplete);

                label.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

                //wait cfor wf to update status to started
                Retry.Do(() =>
                {
                    if (carton.ProducibleStatus != InProductionProducibleStatuses.ProducibleProductionStarted)
                        throw new Exception("not complete");
                }, TimeSpan.FromMilliseconds(200), 10);
                Assert.IsFalse(complete);
                Assert.IsFalse(workflowComplete);

                // complete carton which will also complete kit and should end the workflow
                carton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

                Retry.Do(() =>
                {
                    if (!complete)
                        throw new Exception("not complete");
                }, TimeSpan.FromMilliseconds(200), 10);
                Retry.Do(() =>
                {
                    if (!complete)
                        throw new Exception("workflow not complete");
                }, TimeSpan.FromMilliseconds(200), 10);
            });

            Task.Factory.StartNew(() =>
            {
                //wait for wf to update status to started
                Retry.Do(() =>
                {
                    if (label2.ProducibleStatus != InProductionProducibleStatuses.ProducibleProductionStarted)
                        throw new Exception("not complete2");
                }, TimeSpan.FromMilliseconds(200), 10);
                Assert.IsFalse(complete2);
                Assert.IsFalse(workflowComplete2);

                label2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

                //wait cfor wf to update status to started
                Retry.Do(() =>
                {
                    if (carton2.ProducibleStatus != InProductionProducibleStatuses.ProducibleProductionStarted)
                        throw new Exception("not complete2");
                }, TimeSpan.FromMilliseconds(200), 10);
                Assert.IsFalse(complete2);
                Assert.IsFalse(workflowComplete2);

                // complete carton which will also complete kit and should end the workflow
                carton2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

                Retry.Do(() =>
                {
                    if (!complete2)
                        throw new Exception("not complete2");
                }, TimeSpan.FromMilliseconds(200), 10);
                Retry.Do(() =>
                {
                    if (!complete2)
                        throw new Exception("workflow2 not complete");
                }, TimeSpan.FromMilliseconds(200), 10);
            });
            Thread.Sleep(15000);
            Specify.That(complete).Should.BeTrue("producible not set to complete via kits.");
            Specify.That(workflowComplete).Should.BeTrue("workflow did not exit.");
            Specify.That(complete2).Should.BeTrue("producible not set to complete via kits.");
            Specify.That(workflowComplete2).Should.BeTrue("workflow did not exit.");
        }
    }
}
