﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Services.MachineServices;
using PackNet.Services.MachineServices.CutCreaseMachines;

using Testing.Specificity;

namespace ServicesTests
{
    [TestClass]
    public class AggregateMachineServiceTests
    {
        private AggregateMachineService aggregateMachineService;

        private EventAggregator aggregator;
        private EmMachine emMachine;
        private EmMachineService emMachineService;

        private Mock<IEmMachines> emMachinesMock;

        private FusionMachine fusionMachine;
        private FusionMachineService fusionMachineService;

        private Mock<IFusionMachines> fusionMachinesMock;
        private Mock<ILogger> loggerMock;
        private Mock<IMachineServicesImporter> machineServiceImporterMock;
        private Mock<IServiceLocator> serviceLocatoMock;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;

        [TestInitialize]
        public void Setup()
        {
            emMachine = new EmMachine
            {
                Id = Guid.NewGuid(),
                PhysicalMachineSettings = new EmPhysicalMachineSettings
                {
                    CrossHeadParameters = new EmCrossHeadParameters
                    {
                        MaxHeadPosition = 3000
                    },
                    FeedRollerParameters = new EmFeedRollerParameters()
                    {
                        RubberRollerStartingPositions = new List<EmRubberRollerStartingPosition>
                        {
                            new EmRubberRollerStartingPosition()
                            {
                                Position = 100,
                                TrackNumber = 1
                            },
                            new EmRubberRollerStartingPosition()
                            {
                                Position = 1000,
                                TrackNumber = 2
                            },
                            new EmRubberRollerStartingPosition()
                            {
                                Position = 2000,
                                TrackNumber = 3
                            },
                        }
                    }
                }
            };

            fusionMachine = new FusionMachine()
            {
                Id = Guid.NewGuid(),
                PhysicalMachineSettings = new FusionPhysicalMachineSettings()
                {
                    CrossHeadParameters = new FusionCrossHeadParameters()
                    {
                        MaximumPosition = 1378
                    }
                }
            };

            aggregator = new EventAggregator();
            loggerMock = new Mock<ILogger>();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
            emMachinesMock = GetEmMachinesMock();
            fusionMachinesMock = GetFusionMachinesMock();

            serviceLocatoMock = GetServiceLocatorMock();

            machineServiceImporterMock = GetMachineServiceImporterMock();

            aggregateMachineService = new AggregateMachineService(uiCommunicationServiceMock.Object, aggregator, aggregator,
                machineServiceImporterMock.Object, serviceLocatoMock.Object, loggerMock.Object);
            serviceLocatoMock.Setup(m => m.Locate<IAggregateMachineService>()).Returns(aggregateMachineService);

            emMachineService = new EmMachineService(emMachinesMock.Object, serviceLocatoMock.Object,
                uiCommunicationServiceMock.Object, aggregator, aggregator, loggerMock.Object);
            fusionMachineService = new FusionMachineService(fusionMachinesMock.Object, serviceLocatoMock.Object,
                uiCommunicationServiceMock.Object, aggregator, aggregator, loggerMock.Object);
        }

        private Mock<IFusionMachines> GetFusionMachinesMock()
        {
            var mock = new Mock<IFusionMachines>();

            mock.Setup(m => m.GetMachines()).Returns(new List<FusionMachine> { fusionMachine });

            return mock;
        }

        private Mock<IEmMachines> GetEmMachinesMock()
        {
            var mock = new Mock<IEmMachines>();

            mock.Setup(m => m.GetMachines()).Returns(new List<EmMachine> { emMachine });

            return mock;
        }

        private Mock<IMachineServicesImporter> GetMachineServiceImporterMock()
        {
            var mock = new Mock<IMachineServicesImporter>();

            mock.SetupGet(m => m.MachineServices).Returns(new List<IMachineService<IMachine>>());

            return mock;
        }

        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(aggregator);
            mock.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);
            mock.Setup(m => m.Locate<ILogger>()).Returns(loggerMock.Object);

            return mock;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainTrackOutsideMachineMaxExceptionAssigningCorrugatesWithBadDataForEmMachine()
        {
            var eventCalled = false;
            var machine = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = null
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(10000.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 100
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(1, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        var exc = rmsg.Data.FirstOrDefault();

                        Specify.That(exc.ErrorMessageType).Should.BeEqualTo(CorrugateConfigurationMessages.TrackOutsideOfMachine);
                        Specify.That(exc.TrackNumbers).Should.BeEquivalentTo(new List<int> { 2 });
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainRubberRollerOverlapIndicationWhenAssigingCorrugatesToEmMachineWithBadData()
        {
            bool eventCalled = false;
            var machine = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate()
                        {
                            Id = Guid.NewGuid(),
                            Width = 1000
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(1200.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 100
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(1, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        var exc = rmsg.Data.FirstOrDefault();

                        Specify.That(exc.ErrorMessageType).Should.BeEqualTo(CorrugateConfigurationMessages.OverlappingRubberrollerWithCorrugateLoaded);
                        Specify.That(exc.TrackNumbers).Should.BeEquivalentTo(new List<int> { 1 });
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();

            eventCalled = false;
            machine = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate()
                        {
                            Id = Guid.NewGuid(),
                            Width = 2000
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(1200.0),
                        LoadedCorrugate = null
                    },
                    new Track()
                    {
                        TrackNumber = 3,
                        TrackOffset = new MicroMeter(2100.0),
                        LoadedCorrugate = new Corrugate()
                        {
                            Id = Guid.NewGuid(),
                            Width = 100
                        }
                    }
                }
            };

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotProducedARemovedProducible()
        {
            var producible = new Carton();
            var machine = new EmMachine { Id = Guid.NewGuid() };

            var machineServiceMock = new Mock<IMachineService<IMachine>>();

            machineServiceMock.SetupGet(m => m.Machines).Returns(new List<IMachine> { machine });

            machineServiceImporterMock.SetupGet(m => m.MachineServices)
                .Returns(new List<IMachineService<IMachine>> { machineServiceMock.Object });
            
            producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

            aggregateMachineService = new AggregateMachineService(uiCommunicationServiceMock.Object, aggregator, aggregator,
                machineServiceImporterMock.Object, serviceLocatoMock.Object, loggerMock.Object);

            aggregateMachineService.Produce(machine.Id, producible);

            machineServiceMock.Verify(m => m.Produce(It.IsAny<Guid>(), It.IsAny<IProducible>()), Times.Never);                        
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainTrackOutsideMachineMinExceptionWhenAssigningCorrugatesWithBadDataForEmMachine()
        {
            var eventCalled = false;
            var machine = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = null
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(-10000.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 100
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(1, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        var exc = rmsg.Data.FirstOrDefault();

                        Specify.That(exc.ErrorMessageType).Should.BeEqualTo(CorrugateConfigurationMessages.TrackOutsideOfMachine);
                        Specify.That(exc.TrackNumbers).Should.BeEquivalentTo(new List<int> { 2 });
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainIdenticalCorrugateExceptionWhenAssigningCorrugatesWithBadDataForEmMachine()
        {
            var eventCalled = false;
            var id = Guid.NewGuid();
            var machine = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = id
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(150.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = id
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(1, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        var exc = rmsg.Data.FirstOrDefault();

                        Specify.That(exc.ErrorMessageType).Should.BeEqualTo(CorrugateConfigurationMessages.IdenticalCorrugate);
                        Specify.That(exc.TrackNumbers).Should.BeEquivalentTo(new List<int> { 1, 2 });
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainIdenticalCorrugateExceptionWhenAssigningCorrugatesWithBadDataForFusionMachine()
        {
            var eventCalled = false;
            var id = Guid.NewGuid();
            var machine = new FusionMachine
            {
                Id = fusionMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = id
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(150.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = id
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(1, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        var exc = rmsg.Data.FirstOrDefault();

                        Specify.That(exc.ErrorMessageType).Should.BeEqualTo(CorrugateConfigurationMessages.IdenticalCorrugate);
                        Specify.That(exc.TrackNumbers).Should.BeEquivalentTo(new List<int> { 1, 2 });
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainToWideCorrugateErrorWhenAssigningCorrugatesWithBadDataForFusionMachine()
        {
            var eventCalled = false;
            var machine = new FusionMachine
            {
                Id = fusionMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 10000
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(150.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 10000
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(1, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        var exc = rmsg.Data.FirstOrDefault();

                        Specify.That(exc.ErrorMessageType).Should.BeEqualTo(CorrugateConfigurationMessages.CorrugateTooWideForMachine);
                        Specify.That(exc.TrackNumbers).Should.BeEquivalentTo(new List<int>());
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainOverlappingTracksMessageWhenAssigningCorrugatesWithBadDataForEmMachine()
        {
            var eventCalled = false;
            var machine = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 300
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(150.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 300
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(1, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        var exc = rmsg.Data.FirstOrDefault();

                        Specify.That(exc.ErrorMessageType).Should.BeEqualTo(CorrugateConfigurationMessages.OverlappingTracks);
                        Specify.That(exc.TrackNumbers).Should.BeEquivalentTo(new List<int> { 1, 2 });
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToAssignCorrugatesWithoutErrorsIfTheyAreOk()
        {
            emMachine.Tracks = new ConcurrentList<Track>()
            {
                new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(5.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 500
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(800.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 600
                        }
                    },
                    new Track()
                    {
                        TrackNumber = 3,
                        LoadedCorrugate = null,
                        TrackOffset = 0
                    }
            };

            var eventCalled = false;
            var machine = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(5.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 500
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(800.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 500
                        }
                    },
                    new Track()
                    {
                        TrackNumber = 3,
                        LoadedCorrugate = new Corrugate()
                        {
                            Id = Guid.NewGuid(),
                            Width = 500
                        },
                        TrackOffset = new MicroMeter(1700.0)
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    machine
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ResponseMessageShouldContainOneErrorMessageForEachFaultyMachineWhenAssigningCorrugatesWithBadData()
        {
            var eventCalled = false;
            var localEm = new EmMachine
            {
                Id = emMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 300
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(150.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = Guid.NewGuid(),
                            Width = 300
                        }
                    }
                }
            };

            var id = Guid.NewGuid();
            var localFusion = new FusionMachine
            {
                Id = fusionMachine.Id,
                Tracks = new ConcurrentList<Track>
                {
                    new Track
                    {
                        TrackNumber = 1,
                        TrackOffset = new MicroMeter(50.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = id,
                            Width = 300
                        }
                    },
                    new Track
                    {
                        TrackNumber = 2,
                        TrackOffset = new MicroMeter(150.0),
                        LoadedCorrugate = new Corrugate
                        {
                            Id = id,
                            Width = 300
                        }
                    }
                }
            };

            uiCommunicationServiceMock.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<MachineConfigurationErrorPresentationModel>>, bool, bool>(
                    (rmsg, publishInternally, b) =>
                    {
                        Specify.That(rmsg.MessageType).Should.BeEqualTo(MachineMessages.AssignCorrugatesResponse);
                        Specify.That(rmsg.Data.Count())
                            .Should.BeEqualTo(2, "No corrugate exceptions thrown when assigning corrugates with bad data");
                        
                        Specify.That(rmsg.Data.Any(ex => ex.ErrorMessageType == CorrugateConfigurationMessages.OverlappingTracks)).Should.BeTrue();
                        Specify.That(rmsg.Data.Any(ex => ex.ErrorMessageType == CorrugateConfigurationMessages.IdenticalCorrugate)).Should.BeTrue();
                        Specify.That(rmsg.Data.All(ex => ex.TrackNumbers.Contains(1))).Should.BeTrue();
                        Specify.That(rmsg.Data.All(ex => ex.TrackNumbers.Contains(2))).Should.BeTrue();
                        Specify.That(rmsg.Data.All(ex => ex.TrackNumbers.Count() == 2)).Should.BeTrue();

                        eventCalled = true;
                    });

            aggregator.Publish(new Message<List<ITrackBasedCutCreaseMachine>>
            {
                MessageType = MachineMessages.AssignCorrugates,
                Data = new List<ITrackBasedCutCreaseMachine>
                {
                    localEm,localFusion
                }
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }
    }
}