﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Data.Machines;
using PackNet.Services.MachineServices.CutCreaseMachines;



using Testing.Specificity;

namespace ServicesTests
{
    [TestClass]
    [DeploymentItem(@"TestFiles\PhysicalMachineSettings", @"PhysicalMachineSettings")]
    public class MachineServiceTests
    {
        private Mock<IEmMachineRepository> emMachineRepoMock;
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IUICommunicationService> uicommunicationServiceMock;
        private Mock<ILogger> loggerMock;
        private Mock<IMachineCommunicatorFactory> machineComFactoryMock;
        private Mock<IAggregateMachineService> aggregateMachineServiceMock;
        private Mock<IEmMachineCommunicator> comMock;

        private EmMachine machine = null;
        
        private readonly Corrugate corrugateToLoad = new Corrugate()
        {
            Alias = "1",
            Id = Guid.NewGuid(),
            Quality = 1,
            Thickness = 1.2,
            Width = 400
        };

        private EmMachines emMachines;
        private EmMachineService emMachineService;
        private EventAggregator aggregator;

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();
            emMachineRepoMock = new Mock<IEmMachineRepository>();
            serviceLocatorMock = new Mock<IServiceLocator>();
            uicommunicationServiceMock = new Mock<IUICommunicationService>();
            machineComFactoryMock = new Mock<IMachineCommunicatorFactory>();
            loggerMock = new Mock<ILogger>();
            aggregateMachineServiceMock = new Mock<IAggregateMachineService>();
            comMock = new Mock<IEmMachineCommunicator>();

            SetupMachine();
            SetupRepoMock();
            SetupServiceLocator();
            SetupMachineComFactoryMock();

            emMachines = new EmMachines(emMachineRepoMock.Object, machineComFactoryMock.Object, aggregator, aggregator, serviceLocatorMock.Object, loggerMock.Object);
            emMachineService = new EmMachineService(emMachines, serviceLocatorMock.Object, uicommunicationServiceMock.Object, aggregator, aggregator, loggerMock.Object);
        }

        private void SetupMachineComFactoryMock()
        {
            machineComFactoryMock.Setup(m => m.GetMachineCommunicator(machine)).Returns(comMock.Object);
        }

        private void SetupMachine()
        {
            machine = new EmMachine()
            {
                Id = Guid.NewGuid(),
                Tracks = new ConcurrentList<Track>()
                {
                    new Track()
                    {
                        LoadedCorrugate = null,
                        TrackNumber = 1,
                        TrackOffset = 2
                    }
                },
                PhysicalMachineSettings =
                    EmPhysicalMachineSettings.CreateFromFile(new IPEndPoint(1, 1),
                        @"PhysicalMachineSettings\EmPhysicalMachineSettings_inches.cfg.xml"),
            };

             machine.CurrentStatus = MachinePausedStatuses.MachinePaused;
        }

        private void SetupServiceLocator()
        {
            serviceLocatorMock.Setup(m => m.Locate<IAggregateMachineService>()).Returns(aggregateMachineServiceMock.Object);
        }

        private void SetupRepoMock()
        {
            emMachineRepoMock.Setup(m => m.All()).Returns(new List<EmMachine>() { machine });
        }
    }
}
