﻿using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Scanning;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.ScandataService;
using PackNet.ScandataService.Scanning;
using PackNet.Services;
using System;
using Testing.Specificity;

namespace ServicesTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using PackNet.Common.Interfaces.Enums;

    [TestClass]
    public class ScanDataTranslatorServiceTests
    {

        //TODO: Unfuck this
        //private EventAggregator eventAggregator;
        //private ScanDataTranslatorService service;
        //private Mock<ILogger> loggerMock;
        //private Mock<IUICommunicationService> uiCommunicationServiceMock;
        //private Mock<IProductionGroupService> mockProductionGroupService;
        //private Mock<IMachineGroupService> mockMachineGroupService;

        //[TestInitialize]
        //public void Setup()
        //{
        //    loggerMock = new Mock<ILogger>();

        //    eventAggregator = new EventAggregator();
        //    uiCommunicationServiceMock = new Mock<IUICommunicationService>();
        //    mockProductionGroupService = new Mock<IProductionGroupService>();
        //    mockMachineGroupService = new Mock<IMachineGroupService>();

        //    service = new ScanDataTranslatorService(mockMachineGroupService.Object, mockProductionGroupService.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, loggerMock.Object);
        //}

        //[TestMethod]
        //public void ShouldRequestSelectionAlgorithmForProductionGroup()
        //{
        //    var eventCalled = false;

        //    eventAggregator.GetEvent<Message<IScanTrigger>>().Subscribe(trigger =>
        //    {
        //        eventCalled = true;
        //        Specify.That(trigger.Data.BarcodeData).Should.BeEqualTo("BARCODE");
        //        Specify.That(trigger.Data.ProductionGroupAlias).Should.BeEqualTo("PG1");
        //        Specify.That(trigger.MessageType).Should.BeEqualTo(MessageTypes.GetSelectionAlgorithmForProductionGroup);
        //    });

        //    eventAggregator.Publish(new Message<CreateCartonTrigger>
        //        {
        //            MessageType = CartonMessages.CreateCartonTrigger,
        //            Data = new CreateCartonTrigger
        //            {
        //                BarcodeData = "BARCODE",
        //                ProductionGroupAlias = "PG1",
        //                TriggeredOn = DateTime.Now
        //            }
        //        });

        //    Specify.That(eventCalled).Should.BeTrue();
        //}

        //[TestMethod]
        //public void ShouldTranslateCreateCartonTriggerToScanToQueueTrigger()
        //{
        //    var eventCalled = false;

        //    eventAggregator.GetEvent<IMessage<ScanToQueueTrigger>>().Subscribe(trigger =>
        //    {
        //        eventCalled = true;

        //        Specify.That(trigger.Data.BarcodeData).Should.BeEqualTo("BARCODE");
        //        Specify.That(trigger.Data.ProductionGroupAlias).Should.BeEqualTo("PG1");
        //    });

        //    eventAggregator.GetEvent<Message<IScanTrigger>>().Subscribe(data =>
        //        eventAggregator.Publish(
        //        new Message<Tuple<SelectionAlgorithmTypes, IScanTrigger>>
        //        {
        //            MessageType = MessageTypes.SelectionAlgorithmForProductionGroup,
        //            Data = new Tuple<SelectionAlgorithmTypes, IScanTrigger>(SelectionAlgorithmTypes.ScanToCreate, data.Data)
        //        }));

        //    eventAggregator.Publish(new Message<CreateCartonTrigger>
        //    {
        //        MessageType = CartonMessages.CreateCartonTrigger,
        //        Data = new CreateCartonTrigger
        //        {
        //            BarcodeData = "BARCODE",
        //            ProductionGroupAlias = "PG1",
        //            TriggeredOn = DateTime.Now
        //        }
        //    });

        //    Specify.That(eventCalled).Should.BeTrue();
        //}

        //[TestMethod]
        //public void ShouldTranslateCreateCartonTriggerToBoxLastTrigger()
        //{
        //    var eventCalled = false;

        //    eventAggregator.GetEvent<IMessage<BoxLastTrigger>>().Subscribe(trigger =>
        //    {
        //        eventCalled = true;

        //        Specify.That(trigger.Data.BarcodeData).Should.BeEqualTo("BARCODE");
        //        Specify.That(trigger.Data.ProductionGroupAlias).Should.BeEqualTo("PG1");
        //    });

        //    eventAggregator.GetEvent<Message<IScanTrigger>>().Subscribe(data =>
        //        eventAggregator.Publish(
        //        new Message<Tuple<SelectionAlgorithmTypes, IScanTrigger>>
        //        {
        //            MessageType = MessageTypes.SelectionAlgorithmForProductionGroup,
        //            Data = new Tuple<SelectionAlgorithmTypes, IScanTrigger>(SelectionAlgorithmTypes.BoxLast, data.Data)
        //        }));

        //    eventAggregator.Publish(new Message<CreateCartonTrigger>
        //    {
        //        MessageType = CartonMessages.CreateCartonTrigger,
        //        Data = new CreateCartonTrigger
        //        {
        //            BarcodeData = "BARCODE",
        //            ProductionGroupAlias = "PG1",
        //            TriggeredOn = DateTime.Now
        //        }
        //    });

        //    Specify.That(eventCalled).Should.BeTrue();
        //}

        //[TestCleanup]
        //public void TearDown()
        //{
        //}
    }
}
