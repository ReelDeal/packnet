﻿using System;
using System.Net;
using System.Reactive.Linq;
using System.Reactive.Subjects;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Eventing;

using Moq;

using PackNet.Business.Settings;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;
using PackNet.Services;

using Testing.Specificity;

using PackNetServerSettings = PackNet.Common.Interfaces.DTO.Settings.PackNetServerSettings;

namespace ServicesTests
{
	[TestClass]
	public class PackNetServerSettingsServiceTests
	{
		private EventAggregator eventAggregator;
		private PackNetServerSettingsService packNetServerSettingsService;

		private Mock<ILogger> logger;
		private Mock<IPackNetServerSettings> packNetServerSettings;
		private Mock<IUICommunicationService> uiCommunicationService;

		[TestInitialize]
		public void Setup()
		{
			eventAggregator = new EventAggregator();
			logger = new Mock<ILogger>();
			packNetServerSettings = new Mock<IPackNetServerSettings>();

			uiCommunicationService = new Mock<IUICommunicationService>();
			uiCommunicationService.Setup(u => u.UIEventObservable).Returns(new Subject<Tuple<MessageTypes, string>>());
		}

		[TestMethod]
		[TestCategory("Unit")]
		public void GetSettings()
		{
			//Setup
			packNetServerSettings.Setup(m => m.GetSettings()).Returns(new PackNetServerSettings
			{
				LoginExpiresSeconds = 60,
				FileImportSettings = new FileImportSettings
				{
					CommentIndicator = '/',
					FieldDelimiter = ';',
					FileExtension = "csv"
				}
			});

			packNetServerSettingsService = new PackNetServerSettingsService(packNetServerSettings.Object, eventAggregator, uiCommunicationService.Object, logger.Object); 

			//Test
			var result = packNetServerSettingsService.GetSettings();

			Specify.That(result).Should.Not.BeNull();
			Specify.That(result.LoginExpiresSeconds).Should.BeEqualTo(60);
			Specify.That(result.FileImportSettings.CommentIndicator).Should.BeEqualTo('/');
			Specify.That(result.FileImportSettings.FieldDelimiter).Should.BeEqualTo(';');
			Specify.That(result.FileImportSettings.FileExtension).Should.BeEqualTo("csv");
		}

		[TestMethod]
		[TestCategory("Unit")]
		public void UpdateSettings()
		{
			PackNetServerSettings packNetServerSettingsDto = new PackNetServerSettings
			{
				LoginExpiresSeconds = 3600,
				FileImportSettings = new FileImportSettings
				{
					CommentIndicator = '*',
					FieldDelimiter = '^',
					FileExtension = "csv"
				}
			};

			//Setup
			packNetServerSettings.Setup(m => m.Update(It.IsAny<PackNetServerSettings>())).Returns(packNetServerSettingsDto);

			packNetServerSettingsService = new PackNetServerSettingsService(packNetServerSettings.Object, eventAggregator, uiCommunicationService.Object, logger.Object);

			//Test
			var result = packNetServerSettingsService.UpdateSettings(packNetServerSettingsDto);

			Specify.That(result).Should.Not.BeNull();
			Specify.That(result.LoginExpiresSeconds).Should.BeEqualTo(3600);
			Specify.That(result.FileImportSettings.CommentIndicator).Should.BeEqualTo('*');
			Specify.That(result.FileImportSettings.FieldDelimiter).Should.BeEqualTo('^');
			Specify.That(result.FileImportSettings.FileExtension).Should.BeEqualTo("csv");
		}

	    [TestMethod]
	    [TestCategory("Unit")]
	    public void ShouldPublishNewSettingsOutOnAggregatorAfterUpdate()
	    {
	        var eventCalled = false;
            var packNetServerSettingsDto = new PackNetServerSettings();

            //Setup
            packNetServerSettings.Setup(m => m.Update(It.IsAny<PackNetServerSettings>())).Returns(packNetServerSettingsDto);

            packNetServerSettingsService = new PackNetServerSettingsService(packNetServerSettings.Object, eventAggregator, uiCommunicationService.Object, logger.Object);

            uiCommunicationService.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<PackNetServerSettings>>(), It.IsAny<bool>(), It.IsAny<bool>())).Callback<IMessage, bool, bool>((a, b, b2) =>
	        {
                Specify.That(b).Should.BeTrue();
	            eventCalled = true;
	        });

            eventAggregator.Publish(new Message<PackNetServerSettings>()
            {
                Data = packNetServerSettingsDto,
                MessageType = PackNetServerSettingsMessages.UpdateSettings
            });

	        Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
	        Specify.That(eventCalled).Should.BeTrue();
	    }
	}
}