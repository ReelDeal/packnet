﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Business.UserNotifications;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Services;

using Testing.Specificity;

namespace ServicesTests
{
	[TestClass]
	public class UserNotificationServiceTests
	{
		private EventAggregator eventAggregator;
		private UserNotificationService userNotificationService;

		private Mock<ILogger> logger;
		private Mock<IServiceLocator> serviceLocator;
		private Mock<IUserNotifications> userNotifications;
		private Mock<IProductionGroupService> productionGroupService;
		private Mock<IUICommunicationService> uiCommunicationService;
		private Mock<IUserService> userService;

		private readonly Guid machineGroup1Id = Guid.NewGuid();
		private readonly Guid machineGroup2Id = Guid.NewGuid();

		private readonly Guid user1Id = Guid.NewGuid();
		private readonly Guid user2Id = Guid.NewGuid();

		private const string userNotificationMessage = "User Notification Message";

		private Message<UserNotification> response;

		[TestInitialize]
		public void Initialize()
		{
			eventAggregator = new EventAggregator();

			logger = new Mock<ILogger>();
			serviceLocator = new Mock<IServiceLocator>();
			
			userNotifications = new Mock<IUserNotifications>();
			userNotifications
				.Setup(m => m.Create(It.IsAny<UserNotification>()))
				.Returns<UserNotification>(m => m);
			
			productionGroupService = new Mock<IProductionGroupService>();
			productionGroupService
				.Setup(m => m.GetProductionGroupForMachineGroup(It.IsAny<Guid>()))
                .Returns(new ProductionGroup { ConfiguredMachineGroups = new ConcurrentList<Guid>(new[] { machineGroup1Id, machineGroup2Id }) });

			uiCommunicationService = new Mock<IUICommunicationService>();
			uiCommunicationService
                .Setup(m => m.SendMessageToUI(It.IsAny<IMessage<UserNotification>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<UserNotification>, bool, bool>((message, publishInternally, b2) => response = (Message<UserNotification>)message);

			userService = new Mock<IUserService>();
			userService
				.Setup(m => m.Users)
				.Returns(new List<User>(new[] { new User { Id = user1Id }, new User { Id = user2Id } }));
			
			userNotificationService = new UserNotificationService(
				userNotifications.Object, 
				eventAggregator, 
				eventAggregator,
				uiCommunicationService.Object,
				productionGroupService.Object,
				userService.Object,
				logger.Object,
				serviceLocator.Object);
		}

		[TestMethod]
		public void AnySendNotificationCommonInformationIsCorrect()
		{
			// Machine Group
			userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.Data.Severity).Should.BeEqualTo(NotificationSeverity.Success);
			Specify.That(response.Data.Message).Should.BeEqualTo(userNotificationMessage);
			Specify.That(response.Data.AdditionalInfo).Should.BeEqualTo(string.Empty);

			// Production Group
			userNotificationService.SendNotificationToAllMachineGroupsInMySameProductionGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.Data.Severity).Should.BeEqualTo(NotificationSeverity.Success);
			Specify.That(response.Data.Message).Should.BeEqualTo(userNotificationMessage);
			Specify.That(response.Data.AdditionalInfo).Should.BeEqualTo(string.Empty);

			// System
			userNotificationService.SendNotificationToSystem(NotificationSeverity.Success, userNotificationMessage, string.Empty);

			Specify.That(response.Data.Severity).Should.BeEqualTo(NotificationSeverity.Success);
			Specify.That(response.Data.Message).Should.BeEqualTo(userNotificationMessage);
			Specify.That(response.Data.AdditionalInfo).Should.BeEqualTo(string.Empty);
		}

		[TestMethod]
		public void AnySendNotificationIsPublished()
		{
			// Machine Group
			userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.MessageType).Should.BeEqualTo(UserNotificationMessages.PublishedUserNotification);

			// Production Group
			userNotificationService.SendNotificationToAllMachineGroupsInMySameProductionGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.MessageType).Should.BeEqualTo(UserNotificationMessages.PublishedUserNotification);

			// System
			userNotificationService.SendNotificationToSystem(NotificationSeverity.Success, userNotificationMessage, string.Empty);

			Specify.That(response.MessageType).Should.BeEqualTo(UserNotificationMessages.PublishedUserNotification);
		}

		[TestMethod]
		public void AnySendNotificationNotifiesAllUsers()
		{
			// Machine Group
			userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.Data.NotifiedUsers.Count).Should.BeEqualTo(2);
			Specify.That(response.Data.NotifiedUsers).Should.Contain(user1Id);
			Specify.That(response.Data.NotifiedUsers).Should.Contain(user2Id);

			// Production Group
			userNotificationService.SendNotificationToAllMachineGroupsInMySameProductionGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.Data.NotifiedUsers.Count).Should.BeEqualTo(2);
			Specify.That(response.Data.NotifiedUsers).Should.Contain(user1Id);
			Specify.That(response.Data.NotifiedUsers).Should.Contain(user2Id);

			// System
			userNotificationService.SendNotificationToSystem(NotificationSeverity.Success, userNotificationMessage, string.Empty);

			Specify.That(response.Data.NotifiedUsers.Count).Should.BeEqualTo(2);
			Specify.That(response.Data.NotifiedUsers).Should.Contain(user1Id);
			Specify.That(response.Data.NotifiedUsers).Should.Contain(user2Id);
		}

		[TestMethod]
		public void SendNotificationToMachineGroup()
		{
			userNotificationService.SendNotificationToMachineGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.Data.NotificationType).Should.BeEqualTo(NotificationType.MachineGroup);
			Specify.That(response.Data.NotifiedMachineGroups.Count).Should.BeEqualTo(1);
			Specify.That(response.Data.NotifiedMachineGroups).Should.Contain(machineGroup1Id);
		}

		[TestMethod]
		public void SendNotificationToProductionGroup()
		{
			userNotificationService.SendNotificationToAllMachineGroupsInMySameProductionGroup(NotificationSeverity.Success, userNotificationMessage, string.Empty, machineGroup1Id);

			Specify.That(response.Data.NotificationType).Should.BeEqualTo(NotificationType.ProductionGroup);
			Specify.That(response.Data.NotifiedMachineGroups.Count).Should.BeEqualTo(2);
			Specify.That(response.Data.NotifiedMachineGroups).Should.Contain(machineGroup1Id);
			Specify.That(response.Data.NotifiedMachineGroups).Should.Contain(machineGroup2Id);
		}

		[TestMethod]
		public void SendNotificationToSystem()
		{
			userNotificationService.SendNotificationToSystem(NotificationSeverity.Success, userNotificationMessage, string.Empty);

			Specify.That(response.Data.NotificationType).Should.BeEqualTo(NotificationType.System);
			Specify.That(response.Data.NotifiedMachineGroups.Count).Should.BeEqualTo(0);
		}
	}
}