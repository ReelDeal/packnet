﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Classifications;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Services;

using Testing.Specificity;

namespace ServicesTests
{
    using PackNet.Common.Interfaces.Exceptions;

    [TestClass]
    public class ClassificationsServiceTests
    {
        private EventAggregator eventAggregator;
        private Mock<IClassifications> classifications;
        private Mock<IUICommunicationService> uiCommunicationsServcieMock;
        private Mock<ILogger> logger;
        private Mock<IServiceLocator> serviceLocatorMock;

        [TestInitialize]
        public void Setup()
        {
            eventAggregator = new EventAggregator();
            classifications = new Mock<IClassifications>();
            uiCommunicationsServcieMock = new Mock<IUICommunicationService>();
            logger = new Mock<ILogger>();
            serviceLocatorMock = new Mock<IServiceLocator>();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var classification1Updated = false;
            var classification2Updated = false;

            var classification1 = new Classification() { Id = Guid.NewGuid(), Alias = "classification1" };
            var classification2 = new Classification() { Id = Guid.NewGuid(), Alias = "classification2" };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            classifications.Setup(m => m.GetAll())
                .Returns(new List<Classification>() { classification1, classification2 });

            var classificationsService = new ClassificationsService(classifications.Object, serviceLocatorMock.Object, uiCommunicationsServcieMock.Object, eventAggregator, eventAggregator, logger.Object);

            uiCommunicationsServcieMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<Classification>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<Classification>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(ClassificationMessages.Classifications);
                    responsePublished = true;
                });

            classifications.Setup(m => m.Update(It.IsAny<Classification>())).Callback<Classification>(classification =>
            {
                if (classification.Id.Equals(classification1.Id))
                {
                    Specify.That(classification.NumberOfRequests).Should.BeEqualTo(2);
                    classification1Updated = true;
                }
                else if (classification.Id.Equals(classification2.Id))
                {
                    Specify.That(classification.NumberOfRequests).Should.BeEqualTo(1);
                    classification2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", classification1),
                    GetBoxFirstProducibleCarton("2", classification2),
                    GetBoxFirstProducibleCarton("3", classification1),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(classification1Updated).Should.BeTrue();
            Specify.That(classification2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 8274")]
        public void ShouldClearNumberOfRequestsInRepo_OnMessage_WhenNoProduciblesForClassificationIsPassed_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var classification1Updated = false;
            var classification2Updated = false;

            var classification1 = new Classification() { Id = Guid.NewGuid(), Alias = "classification1"};
            var classification2 = new Classification() { Id = Guid.NewGuid(), Alias = "classification2"};

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            classifications.Setup(m => m.GetAll())
                .Returns(new List<Classification>() { classification1, classification2 });

            var classificationsService = new ClassificationsService(classifications.Object, serviceLocatorMock.Object, uiCommunicationsServcieMock.Object, eventAggregator, eventAggregator, logger.Object);

            uiCommunicationsServcieMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<Classification>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<Classification>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(ClassificationMessages.Classifications);
                    responsePublished = true;
                });

            classifications.Setup(m => m.Update(It.IsAny<Classification>())).Callback<Classification>(classification =>
            {
                if (classification.Id.Equals(classification1.Id))
                {
                    Specify.That(classification.NumberOfRequests).Should.BeEqualTo(0);
                    classification1Updated = true;
                }
                else if (classification.Id.Equals(classification2.Id))
                {
                    Specify.That(classification.NumberOfRequests).Should.BeEqualTo(0);
                    classification2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(classification1Updated).Should.BeTrue();
            Specify.That(classification2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 8018")]
        public void ShouldUpdateIsPriorityLabel_OnUiMessages()
        {
            var eventCalled = false;

            var classification = new Classification() { Id = Guid.NewGuid(), Alias = "classification"};
            
            var classificationsService = new ClassificationsService(classifications.Object, serviceLocatorMock.Object, uiCommunicationsServcieMock.Object, eventAggregator, eventAggregator, logger.Object);

            classifications.Setup(m => m.Get(It.IsAny<Guid>())).Returns<Guid>(gd =>
            {
                Specify.That(gd).Should.BeEqualTo(classification.Id);
                return classification;
            });

            classifications.Setup(m => m.Update(It.IsAny<Classification>())).Callback<Classification>(c =>
            {
                Specify.That(c.IsPriorityLabel).Should.BeEqualTo(true);
                eventCalled = true;
            });

            classification.IsPriorityLabel = true;
            eventAggregator.Publish(new Message<Classification>()
            {
                MessageType = ClassificationMessages.UpdateClassificationStatus,
                Data = classification
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleStatusChangeMessage()
        {
            var messagePublished = false;
            var classificationToUpdate = new Classification() { Alias = "G1", Status = ClassificationStatuses.Normal };

            eventAggregator.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == SelectionAlgorithmMessages.SelectionEnvironmentChanged)
                .Subscribe(
                (msg) =>
                {
                    messagePublished = true;
                });

            classifications.Setup(m => m.Get(It.IsAny<Guid>())).Returns<Guid>(gd =>
            {
                Specify.That(gd).Should.BeEqualTo(classificationToUpdate.Id);
                return classificationToUpdate;
            });

            var classificationsService = new ClassificationsService(classifications.Object, serviceLocatorMock.Object, uiCommunicationsServcieMock.Object, eventAggregator, eventAggregator, logger.Object);
            eventAggregator.Publish(new Message<Classification> { MessageType = ClassificationMessages.UpdateClassificationStatus, Data = classificationToUpdate });

            Retry.For(() => messagePublished, TimeSpan.FromSeconds(1));
            classifications.Verify(m => m.Update(classificationToUpdate), Times.Once);
            Specify.That(messagePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToCreateClassification_WithSameAliasOrNumberAsExistingOne()
        {
            var classificationToWithSameAliasAndNumber = new Classification() { Alias = "c1", Status = ClassificationStatuses.Normal, Number = "1" };
            var classificationToWithSameAlias = new Classification() { Alias = "c1", Status = ClassificationStatuses.Normal, Number = "2" };
            var classificationToWithSameNumber = new Classification() { Alias = "c2", Status = ClassificationStatuses.Normal, Number = "1" };
            var validClassification = new Classification() { Alias = "c2", Status = ClassificationStatuses.Normal, Number = "2" };
            classifications.Setup(m => m.GetAll())
                .Returns(
                    new List<Classification>()
                    {
                        new Classification()
                        {
                            Alias = "c1",
                            Status = ClassificationStatuses.Normal,
                            Number = "1"
                        },
                    });

            var classificationsService = new ClassificationsService(classifications.Object, serviceLocatorMock.Object, uiCommunicationsServcieMock.Object, eventAggregator, eventAggregator, logger.Object);

            try
            {
                classificationsService.CreateClassification(classificationToWithSameAliasAndNumber);
                Assert.Fail("Should not be able to create a classification with the same alias and number as an existing one");
            }
            catch (AlreadyPersistedException){}
            catch (Exception) { Assert.Fail("Wrong exception (Expected AlreadyPersistedException)"); }

            try
            {
                classificationsService.CreateClassification(classificationToWithSameAlias);
                Assert.Fail("Should not be able to create a classification with the same alias as an existing one");
            }
            catch (AlreadyPersistedException) { }
            catch (Exception) { Assert.Fail("Wrong exception (Expected AlreadyPersistedException)"); }

            try
            {
                classificationsService.CreateClassification(classificationToWithSameNumber);
                Assert.Fail("Should not be able to create a classification with the same number as an existing one");
            }
            catch (AlreadyPersistedException) { }
            catch (Exception) { Assert.Fail("Wrong exception (Expected AlreadyPersistedException)"); }

            try
            {
                classificationsService.CreateClassification(validClassification);
            }
            catch (Exception) { Assert.Fail("Should not throw exception when creating a unique classification"); }
            
            
        }

        private BoxFirstProducible GetBoxFirstProducibleCarton(string customerId, Classification classification)
        {
            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<Classification>(classification));
            p1.CustomerUniqueId = customerId;
            p1.Producible = new Carton();
            p1.Producible.CustomerUniqueId = customerId;

            return p1;
        }

    }
}
