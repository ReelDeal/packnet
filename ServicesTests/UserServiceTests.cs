﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Business.Settings;
using PackNet.Business.UserManagement;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Common.Interfaces.DTO.Users;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Services;
using Testing.Specificity;

using PackNetServerSettings = PackNet.Common.Interfaces.DTO.Settings.PackNetServerSettings;

namespace ServicesTests
{
    [TestClass]
    public class UserServiceTests
    {
        private UserService userService;
        private EventAggregator eventAggregator = new EventAggregator();
        private Mock<ILogger> mockLogger;
        private Mock<IUsers> mockUsersBiz;
        private User user;
        private Mock<IPreferences> mockPreferencesBiz;
        private Mock<IUICommunicationService> communicationService = new Mock<IUICommunicationService>();
        private Mock<IPackNetServerSettingsService> mockPackNetServerSettings;
        private Mock<IServiceLocator> serviceLocatorMock;

        [TestInitialize]
        public void Setup()
        {
            mockPreferencesBiz = new Mock<IPreferences>();
            mockUsersBiz = new Mock<IUsers>();
            mockLogger = new Mock<ILogger>();
            mockPackNetServerSettings = new Mock<IPackNetServerSettingsService>();
            mockPackNetServerSettings.Setup(m => m.GetSettings()).Returns(new PackNetServerSettings { LoginExpiresSeconds = 2000 });
            serviceLocatorMock = new Mock<IServiceLocator>();
            
            user = new User { UserName = "TestUser", Password = "TestPassword", IsAdmin = false, IsAutoLogin = false, Id = Guid.NewGuid() };

            userService = new UserService(
                mockUsersBiz.Object,
                eventAggregator,
                eventAggregator,
                communicationService.Object,
                mockPreferencesBiz.Object,
                mockPackNetServerSettings.Object, 
                serviceLocatorMock.Object,
                mockLogger.Object);

            mockUsersBiz.Setup(x => x.All()).Returns(new List<User> { user });
            mockUsersBiz.Setup(x => x.Create(It.IsAny<User>())).Returns(user);
            mockUsersBiz.Setup(x => x.Update(It.IsAny<User>())).Returns(user);
        }

        #region Create Tests

        [TestMethod]
        public void CreateReturnsUserWhenSuccessful()
        {
            mockUsersBiz.Setup(x => x.Create(It.IsAny<User>())).Returns(user);
            var userResult = userService.Create(user);
            mockUsersBiz.Verify(x => x.Create(user));
            Specify.That(userResult).Should.BeEqualTo(user);
        }

        [TestMethod]
        public void CreateLogExceptionWhenThrown()
        {
            mockUsersBiz.Setup(x => x.Create(It.IsAny<User>())).Throws(new Exception("Doesn't matter"));

            try
            {
                userService.Create(user);
            }
            catch { }

            mockLogger.Verify(x => x.LogException(LogLevel.Error, It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestMethod]
        public void CreateStillPublishesToUIWhenExceptionIsThrown()
        {
            mockUsersBiz.Setup(x => x.Create(It.IsAny<User>())).Throws(new Exception("Doesn't matter"));
            IResponseMessage<User> responseMessage = null;

            communicationService
                .Setup(c => c.SendMessageToUI(It.IsAny<IResponseMessage<User>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<User>, bool, bool>((msg, b, b2) =>
                {
                    responseMessage = msg as IResponseMessage<User>;
                });
            eventAggregator.Publish(new Message<User> { MessageType = UserMessages.CreateUser, Data = user });

            Specify.That(responseMessage).Should.Not.BeNull();
            Specify.That(responseMessage.Data).Should.BeNull();
            Specify.That(responseMessage.Result).Should.BeEqualTo(ResultTypes.Fail);
            Specify.That(responseMessage.MessageType).Should.BeEqualTo(UserMessages.UserCreated);
        }

        [TestMethod]
        public void CreatePublishesToUIWhenSuccessful()
        {
            IResponseMessage<User> responseMessage = null;

            communicationService
                .Setup(c => c.SendMessageToUI(It.IsAny<IResponseMessage<User>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<User>, bool, bool>((msg, b, b2) =>
                {
                    responseMessage = msg as IResponseMessage<User>;
                });
            eventAggregator.Publish(new Message<User> { MessageType = UserMessages.CreateUser, Data = user });

            Retry.For(() => responseMessage != null, TimeSpan.FromSeconds(1));
            Specify.That(responseMessage).Should.Not.BeNull();
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(user);
            Specify.That(responseMessage.Result).Should.BeEqualTo(ResultTypes.Success);
            Specify.That(responseMessage.MessageType).Should.BeEqualTo(UserMessages.UserCreated);
        }

        #endregion Create Tests

        #region Update Tests

        [TestMethod]
        public void UpdateReturnsUserWhenSuccessful()
        {
            mockUsersBiz.Setup(x => x.Update(It.IsAny<User>())).Returns(user);
            var userResult = userService.Update(user);
            mockUsersBiz.Verify(x => x.Update(user));
            Specify.That(userResult).Should.BeEqualTo(user);
        }

        [TestMethod]
        public void UpdateLogExceptionWhenThrown()
        {
            mockUsersBiz.Setup(x => x.Update(It.IsAny<User>())).Throws(new Exception("Doesn't matter"));

            try
            {
                userService.Update(user);
            }
            catch { }

            mockLogger.Verify(x => x.LogException(LogLevel.Error, It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestMethod]
        public void UpdateStillPublishesToEventAggregatorWhenExceptionIsThrown()
        {
            mockUsersBiz.Setup(x => x.Update(It.IsAny<User>())).Throws(new Exception("Doesn't matter"));
            IResponseMessage<User> responseMessage = null;

            communicationService
                .Setup(c => c.SendMessageToUI(It.IsAny<IResponseMessage<User>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<User>, bool, bool>((msg, b, b2) =>
                {
                    responseMessage = msg as IResponseMessage<User>;
                });
            eventAggregator.Publish(new Message<User> { MessageType = UserMessages.UpdateUser, Data = user });

            Specify.That(responseMessage).Should.Not.BeNull();
            Specify.That(responseMessage.Data).Should.BeNull();
            Specify.That(responseMessage.Result).Should.BeEqualTo(ResultTypes.Fail);
            Specify.That(responseMessage.MessageType).Should.BeEqualTo(UserMessages.UserUpdated);
        }

        [TestMethod]
        public void UpdatePublishesToUIWhenSuccessful()
        {
            IResponseMessage<User> responseMessage = null;

            communicationService
                            .Setup(c => c.SendMessageToUI(It.IsAny<IResponseMessage<User>>(), false, It.IsAny<bool>()))
                            .Callback<IMessage<User>, bool, bool>((msg, b, b2) =>
                            {
                                responseMessage = msg as IResponseMessage<User>;
                            });
            eventAggregator.Publish(new Message<User> { MessageType = UserMessages.UpdateUser, Data = user });

            Specify.That(responseMessage).Should.Not.BeNull();
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(user);
            Specify.That(responseMessage.Result).Should.BeEqualTo(ResultTypes.Success);
            Specify.That(responseMessage.MessageType).Should.BeEqualTo(UserMessages.UserUpdated);
        }

        #endregion Update Tests

        #region DeleteTests

        [TestMethod]
        public void DeleteLogExceptionWhenThrown()
        {
            mockUsersBiz.Setup(x => x.Delete(It.IsAny<User>())).Throws(new Exception("Doesn't matter"));

            try
            {
                userService.Delete(user);
            }
            catch { }

            mockLogger.Verify(x => x.LogException(LogLevel.Error, It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestMethod]
        public void DeleteStillPublishesToUIWhenExceptionIsThrown()
        {
            mockUsersBiz.Setup(x => x.Delete(It.IsAny<User>())).Throws(new Exception("Doesn't matter"));
            IResponseMessage<User> responseMessage = null;

            communicationService
                            .Setup(c => c.SendMessageToUI(It.IsAny<IResponseMessage<User>>(), false, It.IsAny<bool>()))
                            .Callback<IMessage<User>, bool, bool>((msg, b, b2) =>
                            {
                                responseMessage = msg as IResponseMessage<User>;
                            });
            eventAggregator.Publish(new Message<User> { MessageType = UserMessages.DeleteUser, Data = user });

            Retry.For(() => responseMessage != null, TimeSpan.FromSeconds(1));
            Specify.That(responseMessage).Should.Not.BeNull();
            Specify.That(responseMessage.Data).Should.BeLogicallyEqualTo(user);
            Specify.That(responseMessage.Result).Should.BeEqualTo(ResultTypes.Fail);
        }

        [TestMethod]
        public void DeletePublishesGetUsersToUIWhenSuccessful()
        {
            var called = false;
            eventAggregator.GetEvent<Message>()
                .Where(m => m.MessageType == UserMessages.GetUsers)
                .Subscribe(msg => { called = true; });
            eventAggregator.Publish(new Message<User> { MessageType = UserMessages.DeleteUser, Data = user });

            Retry.For(() => called, TimeSpan.FromSeconds(1));
            Specify.That(called).Should.BeTrue();
        }
        #endregion DeleteTests

        [TestMethod]
        public void GetAllReturnsAllUsers()
        {
            var allUsers = new List<User> { user, new User() };
            mockUsersBiz.Setup(x => x.All()).Returns(allUsers);

            var allusersReturn = userService.All();

            mockUsersBiz.Verify(x => x.All(), Times.Once);

            Specify.That(allusersReturn).Should.BeLogicallyEqualTo(allUsers);
        }

        [TestMethod]
        public void GetAllReturnsEmptyListWhenExceptionIsThrown()
        {
            mockUsersBiz.Setup(x => x.All()).Throws(new Exception("Doesn't matter"));

            var allusersReturn = userService.All();

            mockUsersBiz.Verify(x => x.All(), Times.Once);

            Specify.That(allusersReturn).Should.BeEmpty();
            mockLogger.Verify(x => x.LogException(LogLevel.Error, It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestMethod]
        public void FindByUserNameReturnsUserWhenFound()
        {
            mockUsersBiz.Setup(x => x.Find(It.IsAny<string>())).Returns(user);

            var foundUser = userService.Find(user.UserName);

            mockUsersBiz.Verify(x => x.Find(user.UserName), Times.Once);

            Specify.That(foundUser).Should.BeEqualTo(user);
        }

        [TestMethod]
        public void FindByUserNameReturnsNullWhenNotFound()
        {
            mockUsersBiz.Setup(x => x.Find(It.IsAny<string>())).Returns((User)null);

            var foundUser = userService.Find(user.UserName);

            mockUsersBiz.Verify(x => x.Find(user.UserName), Times.Once);

            Specify.That(foundUser).Should.BeNull();
        }

        [TestMethod]
        public void FindByUserNameReturnsNullWhenExceptionIsThrown()
        {
            mockUsersBiz.Setup(x => x.Find(It.IsAny<string>())).Throws(new Exception("Doesn't matter"));

            var foundUser = userService.Find(user.UserName);

            mockUsersBiz.Verify(x => x.Find(user.UserName), Times.Once);

            Specify.That(foundUser).Should.BeNull();
            mockLogger.Verify(x => x.LogException(LogLevel.Error, It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestMethod]
        public void FindByIdReturnsUserWhenFound()
        {
            mockUsersBiz.Setup(x => x.Find(It.IsAny<Guid>())).Returns(user);

            var foundUser = userService.Find(user.Id);

            mockUsersBiz.Verify(x => x.Find(user.Id), Times.Once);

            Specify.That(foundUser).Should.BeEqualTo(user);
        }

        [TestMethod]
        public void FindByIdReturnsNullWhenNotFound()
        {
            mockUsersBiz.Setup(x => x.Find(It.IsAny<Guid>())).Returns((User)null);

            var foundUser = userService.Find(user.Id);

            mockUsersBiz.Verify(x => x.Find(user.Id), Times.Once);

            Specify.That(foundUser).Should.BeNull();
        }

        [TestMethod]
        public void FindByIdReturnsNullWhenExceptionIsThrown()
        {
            mockUsersBiz.Setup(x => x.Find(It.IsAny<Guid>())).Throws(new Exception("Doesn't matter"));

            var foundUser = userService.Find(user.Id);

            mockUsersBiz.Verify(x => x.Find(user.Id), Times.Once);

            Specify.That(foundUser).Should.BeNull();
            mockLogger.Verify(x => x.LogException(LogLevel.Error, It.IsAny<string>(), It.IsAny<Exception>()));
        }

        [TestMethod]
        public void FindPreferencesUsesPreferenceBizLayer()
        {
            userService.FindPreferences(user);

            mockPreferencesBiz.Verify(x => x.Find(user, true), Times.Once());
        }

        [TestMethod]
        public void UpdatePreferencesUsesPreferenceBizLayer()
        {
            var pref = new Preference() { Id = Guid.NewGuid(), UserId = Guid.NewGuid() };
            userService.UpdatePreferences(pref);

            mockPreferencesBiz.Verify(x => x.Update(pref), Times.Once());
        }

        [TestMethod]
        public void LoginByToken_Fails_IfUserHasLoggedInBefore()
        {
            user.Token = Guid.NewGuid().ToString("N");

            mockUsersBiz.Setup(x => x.ValidateToken(user.Token)).Returns(user);

            var result = userService.ValidateToken(user);

            Specify.That(result).Should.BeNull("User was not null. Login by token should only be possible if a user has logged in before");
        }

        [TestMethod]
        public void LoginByToken_Succeeds_IfUserHasLoggedInBefore()
        {
            user.Token = Guid.NewGuid().ToString("N");

            mockUsersBiz.Setup(x => x.ValidateToken(user.Token)).Returns(user);

            userService.LoggedInUsers.Add(user.Token, DateTime.UtcNow);

            var result = userService.ValidateToken(user);

            Specify.That(result).Should.Not.BeNull();
        }

        [TestMethod]
        public void LoginByToken_SFails_IfUserHasLoggedInBefore_ButTokenIsTimedOut()
        {
            user.Token = Guid.NewGuid().ToString("N");

            mockUsersBiz.Setup(x => x.ValidateToken(user.Token)).Returns(user);

            userService.LoggedInUsers.Add(user.Token, DateTime.UtcNow.AddDays(-1));

            mockPackNetServerSettings.Setup(x => x.GetSettings()).Returns(new PackNetServerSettings(){LoginExpiresSeconds = 10});

            var result = userService.ValidateToken(user);

            Specify.That(result).Should.BeNull();
        }
    }
}