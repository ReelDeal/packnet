﻿//using System;
//using System.IO;
//using System.Linq;
//using System.Net;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using PackNet.Common.Interfaces.DTO.Settings;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Plugins;
//using PackNet.Common.Printing;
//using PackNet.Services;

//using Testing.Specificity;

//namespace ServicesTests
//{
//    [TestClass]
//    public class PrinterFactoryComposerTests
//    {
//        [TestMethod]
//        public void PrinterFactoryComposer_LoadsZebraPrinterFactoryCorrectly()
//        {
//            var mockLogger = new Mock<ILogger>();
//            var pfc = new PrinterFactoryComposer(mockLogger.Object);

//            Specify.That(pfc.PrinterFactories.Any()).Should.BeTrue();
//            Specify.That(pfc.PrinterFactories.First().GetType()).Should.BeEqualTo(typeof(ZebraPrinterFactory));
//        }

//        [TestMethod]
//        [ExpectedException(typeof(ArgumentException))]
//        public void PrinterFactoryComposer_ThrowsException_IfNoFactoryCanHandleRequest()
//        {
//            var mockLogger = new Mock<ILogger>();
//            var pfc = new PrinterFactoryComposer(mockLogger.Object);

//            pfc.GetPrinter(Guid.NewGuid().ToString("N"), null);
//        }

//        [TestMethod]
//        public void PrinterFactoryComposer_LoadsZebraPrinterFactoryCorrectlyAndSuppliesLogger()
//        {
//            var templateFilePath = Path.Combine(Environment.CurrentDirectory, "Data\\Labels\\Template.prn");
//            Directory.CreateDirectory(Path.GetDirectoryName(templateFilePath));
//            var stream = File.Create(templateFilePath);
//            stream.Dispose();

//            var mockLogger = new Mock<ILogger>();
            
//            var mockPrinterSettings = new ZebraPrinterSettings { Address = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9100), PrinterName = "MyPrinter"};
            
//            var pfc = new PrinterFactoryComposer(mockLogger.Object);

//            var printer = pfc.GetPrinter("ZebraGx420T", mockPrinterSettings);

//            Specify.That(printer).Should.Not.BeNull();
//            Specify.That(printer.GetType()).Should.BeEqualTo(typeof(ZebraPrinter));
            
//            //We are validating that the logger is correct in the CTOR
//            var expectedLogMessage = String.Format("Zebra printer created. Name {0}, EndPoint: {1}", mockPrinterSettings.PrinterName,
//                mockPrinterSettings.Address);

//            mockLogger.Verify(x => x.Log(LogLevel.Info, expectedLogMessage), Times.Once);

//            File.Delete(templateFilePath);
//        }
//    }
//}