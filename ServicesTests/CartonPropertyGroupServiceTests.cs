﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;

using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Business.CartonPropertyGroup;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Services;
using Testing.Specificity;

namespace ServicesTests
{
    using Newtonsoft.Json;

    using PackNet.Common.Interfaces.Converters;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;

    [TestClass]
    public class CartonPropertyGroupServiceTests
    {
        private EventAggregator eventAggregator;

        private Mock<ICartonPropertyGroups> cartonPropertyGroupManager;
        private Mock<ILogger> logger;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IClassificationService> classificationServiceMock;
        private Mock<IPickZoneService> pickZoneServiceMock;
        private List<Classification> classifications;
        private List<PickZone> pickzones;

        [TestInitialize]
        public void Setup()
        {
            pickzones = new List<PickZone>();
            classifications = new List<Classification>();
            eventAggregator = new EventAggregator();
            cartonPropertyGroupManager = new Mock<ICartonPropertyGroups>();
            logger = new Mock<ILogger>();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
            classificationServiceMock = new Mock<IClassificationService>();
            classificationServiceMock.SetupGet(m => m.Classifications).Returns(classifications);
            pickZoneServiceMock = new Mock<IPickZoneService>();
            pickZoneServiceMock.SetupGet(m => m.PickZones).Returns(pickzones);
            serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(m => m.Locate<IClassificationService>()).Returns(classificationServiceMock.Object);
            serviceLocatorMock.Setup(m => m.Locate<IPickZoneService>()).Returns(pickZoneServiceMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfExpeditedRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var expeditedClassification = new Classification { Id = Guid.NewGuid(), Alias = "Expedited", Status = ClassificationStatuses.Expedite };
            classifications.Add(expeditedClassification);
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfExpeditedRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<Classification>(expeditedClassification)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<Classification>(expeditedClassification)}),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfLastRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var lastClassification = new Classification { Id = Guid.NewGuid(), Alias = "Last", Status = ClassificationStatuses.Last };
            classifications.Add(lastClassification);
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfLastRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfStoppedRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var stoppedClassification = new Classification { Id = Guid.NewGuid(), Alias = "Stopped", Status = ClassificationStatuses.Stop };
            classifications.Add(stoppedClassification);
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfStoppedRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfNormalRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var normalClassification = new Classification { Id = Guid.NewGuid(), Alias = "Normal", Status = ClassificationStatuses.Normal };
            classifications.Add(normalClassification);
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfNormalRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<Classification>(normalClassification)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<Classification>(normalClassification)}),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfClassificationRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var normalClassification = new Classification { Id = Guid.NewGuid(), Alias = "Normal", Status = ClassificationStatuses.Normal };
            var expediteClassification = new Classification { Id = Guid.NewGuid(), Alias = "Expedite", Status = ClassificationStatuses.Expedite };
            var stoppedClassification = new Classification { Id = Guid.NewGuid(), Alias = "Stop", Status = ClassificationStatuses.Stop };
            var lastClassification = new Classification { Id = Guid.NewGuid(), Alias = "Last", Status = ClassificationStatuses.Last };
            classifications.Add(normalClassification);
            classifications.Add(expediteClassification);
            classifications.Add(stoppedClassification);
            classifications.Add(lastClassification);

            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(11);
                    Specify.That(msg.Data.ElementAt(0).NumberOfNormalRequests).Should.BeEqualTo(1);
                    Specify.That(msg.Data.ElementAt(0).NumberOfExpeditedRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfLastRequests).Should.BeEqualTo(3);
                    Specify.That(msg.Data.ElementAt(0).NumberOfStoppedRequests).Should.BeEqualTo(5);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(11);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<Classification>(normalClassification)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<Classification>(expediteClassification)}),
                    GetBoxFirstProducibleCarton("4", cpg1, new []{ new BasicRestriction<Classification>(expediteClassification)}),
                    GetBoxFirstProducibleCarton("5", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                    GetBoxFirstProducibleCarton("6", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                    GetBoxFirstProducibleCarton("7", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                    GetBoxFirstProducibleCarton("8", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("9", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("10", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("11", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("12", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfClassificationAndPickzoneRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var normalClassification = new Classification { Id = Guid.NewGuid(), Alias = "Normal", Status = ClassificationStatuses.Normal };
            var expediteClassification = new Classification { Id = Guid.NewGuid(), Alias = "Expedite", Status = ClassificationStatuses.Expedite };
            var stoppedClassification = new Classification { Id = Guid.NewGuid(), Alias = "Stop", Status = ClassificationStatuses.Stop };
            var lastClassification = new Classification { Id = Guid.NewGuid(), Alias = "Last", Status = ClassificationStatuses.Last };
            classifications.Add(normalClassification);
            classifications.Add(expediteClassification);
            classifications.Add(stoppedClassification);
            classifications.Add(lastClassification);

            var normalPickZone = new PickZone { Id = Guid.NewGuid(), Alias = "Normal", Status = PickZoneStatuses.Normal };
            var stoppedPickZone = new PickZone { Id = Guid.NewGuid(), Alias = "Stop", Status = PickZoneStatuses.Stop };
            pickzones.Add(normalPickZone);
            pickzones.Add(stoppedPickZone);

            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(12);
                    Specify.That(msg.Data.ElementAt(0).NumberOfNormalRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfExpeditedRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfLastRequests).Should.BeEqualTo(3);
                    Specify.That(msg.Data.ElementAt(0).NumberOfStoppedRequests).Should.BeEqualTo(5);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(12);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<Classification>(normalClassification)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<Classification>(expediteClassification)}),
                    GetBoxFirstProducibleCarton("4", cpg1, new []{ new BasicRestriction<Classification>(expediteClassification)}),
                    GetBoxFirstProducibleCarton("5", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                    GetBoxFirstProducibleCarton("6", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                    GetBoxFirstProducibleCarton("7", cpg1, new []{ new BasicRestriction<Classification>(lastClassification)}),
                    GetBoxFirstProducibleCarton("8", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("9", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("10", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("11", cpg1, new []{ new BasicRestriction<Classification>(stoppedClassification)}),
                    GetBoxFirstProducibleCarton("12", cpg1, new []{ new BasicRestriction<PickZone>(stoppedPickZone)}),
                    GetBoxFirstProducibleCarton("13", cpg1, new []{ new BasicRestriction<PickZone>(normalPickZone)}),

                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfStoppedPickzoneRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var stoppedPickzone = new PickZone { Id = Guid.NewGuid(), Alias = "Stopped", Status = PickZoneStatuses.Stop };
            pickzones.Add(stoppedPickzone);
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfStoppedRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<PickZone>(stoppedPickzone)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<PickZone>(stoppedPickzone)}),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfNormalPickzoneRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var normalPickzone = new PickZone() { Id = Guid.NewGuid(), Alias = "Normal", Status = PickZoneStatuses.Normal };
            pickzones.Add(normalPickzone);
            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;

                    eventAggregator.Publish(new Message()
                    {
                        MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups
                    });
                });

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<CartonPropertyGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfNormalRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", cpg1, new []{ new BasicRestriction<PickZone>(normalPickzone)}),
                    GetBoxFirstProducibleCarton("2", cpg2),
                    GetBoxFirstProducibleCarton("3", cpg1, new []{ new BasicRestriction<PickZone>(normalPickzone)}),
                }
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldClearNumberOfRequestsInRepo_OnMessage_WhenNoProduciblesForCPGIsPassed_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var cpg1 = new CartonPropertyGroup() { Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Id = Guid.NewGuid() };

            var testScheduler = new TestScheduler();
            eventAggregator.Scheduler = testScheduler;

            cartonPropertyGroupManager.Setup(m => m.GetCartonPropertyGroups())
                .Returns(new List<CartonPropertyGroup>() { cpg1, cpg2 });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message>(), It.IsAny<bool>()))
                .Callback<IMessage, bool>((msg, b) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupsCountersUpdated);
                    responsePublished = true;
                });

            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Callback<CartonPropertyGroup>(cpg =>
            {
                if (cpg.Id.Equals(cpg1.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(0);
                    cpg1Updated = true;
                }
                else if (cpg.Id.Equals(cpg2.Id))
                {
                    Specify.That(cpg.NumberOfRequests).Should.BeEqualTo(0);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
            });

            testScheduler.AdvanceBy(500);
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleStatusChangeMessage()
        {
            var messagePublished = false;

            eventAggregator.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == SelectionAlgorithmMessages.SelectionEnvironmentChanged)
                .Subscribe(
                (msg) =>
                {
                    messagePublished = true;
                });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);
            eventAggregator.Publish(new Message<CartonPropertyGroup> { MessageType = CartonPropertyGroupMessages.UpdateCartonPropertyGroupStatus, Data = new CartonPropertyGroup() { Alias = "G1", Status = CartonPropertyGroupStatuses.Normal } });

            Retry.For(() => messagePublished, TimeSpan.FromSeconds(1));
            cartonPropertyGroupManager.Verify(m => m.SetCartonPropertyGroupStatus("G1", CartonPropertyGroupStatuses.Normal), Times.Once);
            Specify.That(messagePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void UpdatingProductionGroupsShouldResetMixEnumerator()
        {
            var cpg1 = new CartonPropertyGroup { Alias = "AZ1", Id = Guid.NewGuid(), MixQuantity = 1, };
            var cpg2 = new CartonPropertyGroup { Alias = "AZ2", Id = Guid.NewGuid(), MixQuantity = 2, };

            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2));
            var availableProds = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2));

            var pgId = Guid.NewGuid();
            var productionGroup = new ProductionGroup
                                  {
                                      Id = pgId,
                                      Alias = "PG1",
                                      SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                                      Options =
                                          new Dictionary<string, string> { { "ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup> { cpg1, cpg2 }, new EnumerationConverter()) } }
                                  };

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            var cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ1");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");

            cpg2.MixQuantity = 3;
            productionGroup.Options["ConfiguredCartonPropertyGroups"] = JsonConvert.SerializeObject(new List<CartonPropertyGroup> { cpg1, cpg2 }, new EnumerationConverter());
            eventAggregator.Publish(new Message<ProductionGroup> { MessageType = ProductionGroupMessages.ProductionGroupChanged, Data = productionGroup });

            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ1");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void UpdatingProductionGroupShouldResetSkipList()
        {
            var cpg1 = new CartonPropertyGroup { Alias = "AZ1", Id = Guid.NewGuid(), MixQuantity = 1, };
            var cpg2 = new CartonPropertyGroup { Alias = "AZ2", Id = Guid.NewGuid(), MixQuantity = 1, };
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg2 };

            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2));
            var availableProds = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2));

            var pgId = Guid.NewGuid();
            var productionGroup = new ProductionGroup
            {
                Id = pgId,
                Alias = "PG1",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                Options =
                    new Dictionary<string, string> { { "ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup> { cpg1, cpg2 }, new EnumerationConverter()) } }
            };

            cartonPropertyGroupManager.Setup(mgr => mgr.GetCartonPropertyGroupByAlias(It.IsAny<String>())).Returns<String>(s => cpgs.FirstOrDefault(c => c.Alias == s));
            cartonPropertyGroupManager.Setup(mgr => mgr.GetCartonPropertyGroups()).Returns(cpgs.ToList);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);

            cpg2.MixQuantity = 3;
            productionGroup.Options["ConfiguredCartonPropertyGroups"] = JsonConvert.SerializeObject(new List<CartonPropertyGroup> { cpg1, cpg2 }, new EnumerationConverter());
            eventAggregator.Publish(new Message<ProductionGroup> { MessageType = ProductionGroupMessages.ProductionGroupChanged, Data = productionGroup });

            var cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ1");
            cpg = cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Alias).Should.BeEqualTo("AZ2");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleHugeSkipListsAndManyCpgs()
        {
            var cpg1 = new CartonPropertyGroup { Alias = "AZ1", Id = Guid.NewGuid(), MixQuantity = 20 };
            var cpg2 = new CartonPropertyGroup { Alias = "AZ2", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg3 = new CartonPropertyGroup { Alias = "AZ3", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg4 = new CartonPropertyGroup { Alias = "AZ4", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg5 = new CartonPropertyGroup { Alias = "AZ5", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg6 = new CartonPropertyGroup { Alias = "AZ6", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg7 = new CartonPropertyGroup { Alias = "AZ7", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg8 = new CartonPropertyGroup { Alias = "AZ8", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg9 = new CartonPropertyGroup { Alias = "AZ9", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg10 = new CartonPropertyGroup { Alias = "AZ10", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg11 = new CartonPropertyGroup { Alias = "AZ11", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg12 = new CartonPropertyGroup { Alias = "AZ12", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg13 = new CartonPropertyGroup { Alias = "AZ13", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg14 = new CartonPropertyGroup { Alias = "AZ14", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg15 = new CartonPropertyGroup { Alias = "AZ15", Id = Guid.NewGuid(), MixQuantity = 1 };

            const int numberOfBoxesInEnumeration = 34;

            var availableProducibles = new List<IProducible>();
            
            var cpg1Prod = GetProduciblesThatBelongToCpg(5, cpg1);
            var cpg2Prod = GetProduciblesThatBelongToCpg(5, cpg2);
            var cpg3Prod = GetProduciblesThatBelongToCpg(5, cpg3);
            var cpg4Prod = GetProduciblesThatBelongToCpg(5, cpg4);
            var cpg5Prod = GetProduciblesThatBelongToCpg(5, cpg5);
            var cpg6Prod = GetProduciblesThatBelongToCpg(5, cpg6);
            var cpg7Prod = GetProduciblesThatBelongToCpg(5, cpg7);
            var cpg8Prod = GetProduciblesThatBelongToCpg(5, cpg8);
            var cpg9Prod = GetProduciblesThatBelongToCpg(5, cpg9);
            var cpg10Prod = GetProduciblesThatBelongToCpg(5, cpg10);
            var cpg11Prod = GetProduciblesThatBelongToCpg(5, cpg11);
            var cpg12Prod = GetProduciblesThatBelongToCpg(5, cpg12);
            var cpg13Prod = GetProduciblesThatBelongToCpg(5, cpg13);
            var cpg14Prod = GetProduciblesThatBelongToCpg(5, cpg14);
            var cpg15Prod = GetProduciblesThatBelongToCpg(5, cpg15);

            availableProducibles.AddRange(cpg1Prod);
            availableProducibles.AddRange(cpg2Prod);
            availableProducibles.AddRange(cpg3Prod);
            availableProducibles.AddRange(cpg4Prod);
            availableProducibles.AddRange(cpg5Prod);
            availableProducibles.AddRange(cpg6Prod);
            availableProducibles.AddRange(cpg7Prod);
            availableProducibles.AddRange(cpg8Prod);
            availableProducibles.AddRange(cpg9Prod);
            availableProducibles.AddRange(cpg10Prod);
            availableProducibles.AddRange(cpg11Prod);
            availableProducibles.AddRange(cpg12Prod);
            availableProducibles.AddRange(cpg13Prod);
            availableProducibles.AddRange(cpg14Prod);
            availableProducibles.AddRange(cpg15Prod);

            var availableProducilesExceptOne = availableProducibles.Where(p => { 
                                                                                    var res = (BasicRestriction<CartonPropertyGroup>)p.Restrictions.First(r => r is BasicRestriction<CartonPropertyGroup>);
                                                                                    return res != null && res.Value.Alias != "AZ1"; }).ToList();

            var pgId = Guid.NewGuid();
            var cpgs = new List<CartonPropertyGroup>(){ cpg1, cpg2, cpg3, cpg4, cpg5, cpg6, cpg7, cpg8, cpg8, cpg10, cpg11, cpg12, cpg13, cpg14, cpg15};
            var productionGroup = new ProductionGroup
                                  {
                                      Id = pgId,
                                      Alias = "PG1",
                                      SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                                      Options =
                                          new Dictionary<string, string> { { "ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(cpgs, new EnumerationConverter()) } }
                                  };

            cartonPropertyGroupManager.Setup(mgr => mgr.GetCartonPropertyGroupByAlias(It.IsAny<String>())).Returns<String>(s => cpgs.FirstOrDefault(c => c.Alias == s));
            cartonPropertyGroupManager.Setup(mgr => mgr.GetCartonPropertyGroups()).Returns(cpgs.ToList);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            var cpgToGetCount = 2;

            for (int i = 0; i < 1000; i++)
            {
                if (i % 100 == 0)
                {
                    var cpgToGet = cpgs.FirstOrDefault(c => c.Alias == "AZ" + cpgToGetCount);
                    if (cpgToGetCount > 14)
                        cpgToGetCount = 2;
                    //try getting mix several times with only one available producible to make sure that the other cpg's also gets added to the skip list
                    for (int j = 0; j < numberOfBoxesInEnumeration; j++)
                    {
                        cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, new List<IProducible>() {GetProduciblesThatBelongToCpg(1, cpgToGet).First()},
                            availableProducibles);
                    }
                }
                
                cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducilesExceptOne, availableProducibles);
            }

            var stopWatch = new Stopwatch();
            stopWatch.Start();
            cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducilesExceptOne, availableProducibles);
            stopWatch.Stop();

            Specify.That(stopWatch.ElapsedMilliseconds < 21).Should.BeTrue(stopWatch.ElapsedMilliseconds + " ms is more than 20 ms");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldProcessSkipListInOrder()
        {
            var cpg1 = new CartonPropertyGroup { Alias = "AZ1", Id = Guid.NewGuid(), MixQuantity = 20 };
            var cpg2 = new CartonPropertyGroup { Alias = "AZ2", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg3 = new CartonPropertyGroup { Alias = "AZ3", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg4 = new CartonPropertyGroup { Alias = "AZ4", Id = Guid.NewGuid(), MixQuantity = 1 };
            var cpg5 = new CartonPropertyGroup { Alias = "AZ5", Id = Guid.NewGuid(), MixQuantity = 1 };

            var cpg1Prod = GetProduciblesThatBelongToCpg(1, cpg1).First();
            var cpg2Prod = GetProduciblesThatBelongToCpg(1, cpg2).First();
            var cpg3Prod = GetProduciblesThatBelongToCpg(1, cpg3).First();
            var cpg4Prod = GetProduciblesThatBelongToCpg(1, cpg4).First();
            var cpg5Prod = GetProduciblesThatBelongToCpg(1, cpg5).First();

            var availableProducibles = new List<IProducible>() { cpg1Prod, cpg2Prod, cpg3Prod, cpg4Prod, cpg5Prod };

            var pgId = Guid.NewGuid();
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg2, cpg3, cpg4, cpg5 };
            var productionGroup = new ProductionGroup
            {
                Id = pgId,
                Alias = "PG1",
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst,
                Options =
                    new Dictionary<string, string> { { "ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(cpgs, new EnumerationConverter()) } }
            };

            cartonPropertyGroupManager.Setup(mgr => mgr.GetCartonPropertyGroupByAlias(It.IsAny<String>())).Returns<String>(s => cpgs.FirstOrDefault(c => c.Alias == s));
            cartonPropertyGroupManager.Setup(mgr => mgr.GetCartonPropertyGroups()).Returns(cpgs.ToList);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg2);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg3);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg2);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg5);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg2);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg4);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg2);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg1);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg2);
            cartonPropertyGroupService.AddCpgToSkipList(productionGroup, cpg2);

            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ2");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ1");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ1");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ3");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ1");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ1");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ2");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ5");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ2");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ4");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ2");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ1");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ1");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ2");
            Specify.That(cartonPropertyGroupService.GetNextCPGToDispatchWorkTo(productionGroup, availableProducibles, availableProducibles).Alias).Should.BeEqualTo("AZ2");

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SettingCPGSurgeCount_ShouldUpdateSurgeCount()
        {
            var cpg1 = new CartonPropertyGroup
            {
                Alias = "AZ1",
                Id = Guid.NewGuid(),
                MixQuantity = 1,
            };

            cartonPropertyGroupManager.Setup(cpgm => cpgm.GetCartonPropertyGroupByAlias(cpg1.Alias)).Returns(cpg1);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);
            Specify.That(cpg1.SurgeCount).Should.BeEqualTo(0);
            cartonPropertyGroupService.SetSurgeCount(cpg1.Alias, 10);
            Specify.That(cpg1.SurgeCount).Should.BeEqualTo(10);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SettingStatusToSurge_ShouldSetRemainingProduciblesToSurge_ToTheNumberOfProduciblesToSurge()
        {
            var cpg1 = new CartonPropertyGroup
            {
                Alias = "AZ1",
                Id = Guid.NewGuid(),
                MixQuantity = 1,
            };

            cartonPropertyGroupManager.Setup(cpgm => cpgm.GetCartonPropertyGroupByAlias(cpg1.Alias)).Returns(cpg1);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);
            cartonPropertyGroupService.SetSurgeCount(cpg1.Alias, 10);

            cartonPropertyGroupManager.Setup(s => s.SetCartonPropertyGroupStatus(cpg1.Alias, It.Is<CartonPropertyGroupStatuses>(status => status == CartonPropertyGroupStatuses.Surge)))
                .Callback(() =>
                {
                    cpg1.Status = CartonPropertyGroupStatuses.Surge;
                    cpg1.RemainingProduciblesToSurge = cpg1.SurgeCount;
                });

            cartonPropertyGroupManager.Setup(s => s.SetCartonPropertyGroupStatus(cpg1.Alias, It.Is<CartonPropertyGroupStatuses>(status => status == CartonPropertyGroupStatuses.Normal)))
                .Callback(() => { cpg1.Status = CartonPropertyGroupStatuses.Normal; });

            cartonPropertyGroupManager.Object.SetCartonPropertyGroupStatus(cpg1.Alias, CartonPropertyGroupStatuses.Surge);
            Specify.That(cpg1.RemainingProduciblesToSurge).Should.BeEqualTo(cpg1.SurgeCount);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldTurnOffSurge_AfterSurgingX_BoxesToCPG()
        {
            var cpg1 = new CartonPropertyGroup
                       {
                           Alias = "AZ1",
                           Id = Guid.NewGuid(),
                           MixQuantity = 1,
                       };

            cartonPropertyGroupManager.Setup(cpgm => cpgm.GetCartonPropertyGroupByAlias(cpg1.Alias)).Returns(cpg1);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);
            cartonPropertyGroupService.SetSurgeCount(cpg1.Alias, 10);
            cartonPropertyGroupManager.Setup(s => s.SetCartonPropertyGroupStatus(cpg1.Alias, It.Is<CartonPropertyGroupStatuses>(status => status == CartonPropertyGroupStatuses.Surge)))
                .Callback(() =>
                {
                    cpg1.Status = CartonPropertyGroupStatuses.Surge;
                    cpg1.RemainingProduciblesToSurge = cpg1.SurgeCount;
                });

            cartonPropertyGroupManager.Setup(s => s.SetCartonPropertyGroupStatus(cpg1.Alias, It.Is<CartonPropertyGroupStatuses>(status => status == CartonPropertyGroupStatuses.Normal)))
                .Callback(() => { cpg1.Status = CartonPropertyGroupStatuses.Normal; });

            cartonPropertyGroupManager.Object.SetCartonPropertyGroupStatus(cpg1.Alias, CartonPropertyGroupStatuses.Surge);

            for (var i = 0; i < cpg1.SurgeCount; i++)
            {
                cartonPropertyGroupService.DecreaseSurgeCounter(cpg1.Alias);
            }

            cartonPropertyGroupManager.Verify(s => s.SetCartonPropertyGroupStatus(cpg1.Alias, It.Is<CartonPropertyGroupStatuses>(status => status == CartonPropertyGroupStatuses.Normal)), Times.Once);

            Specify.That(cpg1.RemainingProduciblesToSurge).Should.BeEqualTo(0);

            cartonPropertyGroupManager.ResetCalls();

            for (var i = 0; i < 10; i++)
            {
                cartonPropertyGroupService.DecreaseSurgeCounter(cpg1.Alias);
            }

            cartonPropertyGroupManager.Verify(s => s.SetCartonPropertyGroupStatus(cpg1.Alias, It.Is<CartonPropertyGroupStatuses>(status => status == CartonPropertyGroupStatuses.Normal)), Times.Never);
            Specify.That(cpg1.RemainingProduciblesToSurge).Should.BeEqualTo(0);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleDeleteMessage()
        {
            var responsePublished = false;

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            var cartonPropertyGroupToDelete = new CartonPropertyGroup()
                {
                    Alias = "hi",
                    Id = Guid.NewGuid()
                };


            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<IMessage<CartonPropertyGroup>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<CartonPropertyGroup>, bool, bool>((msg, b, b2) =>
                {
                    var x = msg as ResponseMessage<CartonPropertyGroup>;
                    Specify.That(x.Result).Should.BeEqualTo(ResultTypes.Success);
                    Specify.That(x.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupDeleted);
                    responsePublished = true;
                });

            eventAggregator.Publish(new Message<CartonPropertyGroup> { MessageType = CartonPropertyGroupMessages.DeleteCartonPropertyGroup, Data = cartonPropertyGroupToDelete });

            Specify.That(responsePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogMessagesWhenHandlingCrudMessages()
        {
            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.CreateCartonPropertyGroup,
                Data = new CartonPropertyGroup()
                {
                    Alias = "CPG1",
                    Id = Guid.Empty,
                },
                UserName = "User1"
            });
            logger.Verify(l => l.Log(LogLevel.Info, string.Format("Carton Property Group with Alias: {0} was Created by User: {1}", "CPG1", "User1")));

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.UpdateCartonPropertyGroup,
                Data = new CartonPropertyGroup()
                {
                    Alias = "CPG1",
                    Id = Guid.NewGuid()
                },
                UserName = "User1"
            });
            logger.Verify(l => l.Log(LogLevel.Info, string.Format("Carton Property Group with Alias: {0} was Edited by User: {1}", "CPG1", "User1")));
            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.DeleteCartonPropertyGroup,
                Data = new CartonPropertyGroup()
                    {
                        Alias = "CPG1",
                        Id = Guid.NewGuid(),
                    },
                UserName = "User1"
            });
            logger.Verify(l => l.Log(LogLevel.Info, string.Format("Carton Property Group with Alias: {0} was Deleted by User: {1}", "CPG1", "User1")));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogFailingCreateMessages()
        {
            cartonPropertyGroupManager.Setup(m => m.Create(It.IsAny<CartonPropertyGroup>())).Returns<CartonPropertyGroup>((pz) =>
            {
                if (pz.Alias == "CPG1")
                    throw new Exception("Ex1");

                if (pz.Alias == "CPG2")
                    throw new AlreadyPersistedException("Persisted");

                return null;
            });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.CreateCartonPropertyGroup,
                Data = new CartonPropertyGroup()
                {
                    Alias = "CPG1",
                    Id = Guid.NewGuid()
                },
                UserName = "User1"
            });
            logger.Verify(
                l =>
                    l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Unable to create Carton Property Group with Alias: CPG1, message: System.Exception: Ex1"))));

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.CreateCartonPropertyGroup,
                Data = new CartonPropertyGroup()
                {
                    Alias = "CPG2"
                },
                UserName = "User1"
            });
            logger.Verify(
                l =>
                    l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Carton Property Group with Alias: CPG2 already exists in the database, message: PackNet.Common.Interfaces.Exceptions.AlreadyPersistedException:"))));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogFailingEditMessages()
        {
            cartonPropertyGroupManager.Setup(m => m.Update(It.IsAny<CartonPropertyGroup>())).Returns<CartonPropertyGroup>((pz) =>
            {
                if (pz.Alias == "CPG1")
                    throw new Exception("Ex1");

                if (pz.Alias == "CPG2")
                    throw new UnableToLocateByPacksizeIdException("Not locatable");

                return null;
            });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.UpdateCartonPropertyGroup,
                Data = new CartonPropertyGroup()
                {
                    Alias = "CPG1",
                    Id = Guid.Empty
                },
                UserName = "User1"
            });

            logger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Unable to edit Carton Property Group with Alias: CPG1, message: System.Exception: Ex1"))));

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.UpdateCartonPropertyGroup,
                Data = new CartonPropertyGroup()
                {
                    Alias = "CPG2",
                    Id = Guid.NewGuid()
                },
                UserName = "User1"
            });

            logger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Unable to edit Carton Property Group with Alias: CPG2, message: PackNet.Common.Interfaces.Exceptions.UnableToLocateByPacksizeIdException: Not locatable"))));
        }

        [TestMethod]
        [TestCategory("")]
        public void ShouldLogFailingDeleteMessages()
        {
            cartonPropertyGroupManager.Setup(m => m.Delete(It.IsAny<CartonPropertyGroup>())).Callback(() => { throw new Exception("e"); });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<CartonPropertyGroup>
            {
                MessageType = CartonPropertyGroupMessages.DeleteCartonPropertyGroup,
                Data =
                    new CartonPropertyGroup()
                    {
                        Alias = "CPG1",
                        Id = Guid.Empty
                    },
                UserName = "User1"
            });

            logger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Unable to delete Carton Property Group with Alias: CPG1, message: System.Exception: e"))));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleCreateMessage()
        {
            var responsePublished = false;

            cartonPropertyGroupManager.Setup(manager => manager.Create(It.IsAny<CartonPropertyGroup>())).Returns<CartonPropertyGroup>(c => c);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            var pickzoneToCreate = new CartonPropertyGroup()
            {
                Alias = "hi",
                Id = Guid.NewGuid()
            };

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<IMessage<CartonPropertyGroup>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<CartonPropertyGroup>, bool, bool>((msg, b, b2) =>
                {
                    var x = msg as ResponseMessage<CartonPropertyGroup>;
                    Specify.That(x.Result).Should.BeEqualTo(ResultTypes.Success);
                    Specify.That(x.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupCreated);

                    responsePublished = true;
                });

            eventAggregator.Publish(new Message<CartonPropertyGroup> { MessageType = CartonPropertyGroupMessages.CreateCartonPropertyGroup, Data = pickzoneToCreate });

            Specify.That(responsePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleEditMessage()
        {
            var responsePublished = false;

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object, serviceLocatorMock.Object, logger.Object);

            var cartonPropertyGroupToEdit = new CartonPropertyGroup()
            {
                Alias = "hi",
                Id = Guid.NewGuid()
            };

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<IMessage<CartonPropertyGroup>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<CartonPropertyGroup>, bool, bool>((msg, b, b2) =>
                {
                    var x = msg as ResponseMessage<CartonPropertyGroup>;
                    Specify.That(x.Result).Should.BeEqualTo(ResultTypes.Success);
                    Specify.That(x.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroupUpdated);

                    responsePublished = true;
                });

            eventAggregator.Publish(new Message<CartonPropertyGroup> { MessageType = CartonPropertyGroupMessages.UpdateCartonPropertyGroup, Data = cartonPropertyGroupToEdit });

            Specify.That(responsePublished).Should.BeTrue();
        }
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleGetPickzonesMessage()
        {
            var responsePosted = false;

            var cartonPropertyGroups = new List<CartonPropertyGroup>()
            {
                new CartonPropertyGroup()
                {
                    Alias = "1"
                },
                new CartonPropertyGroup()
                {
                    Alias = "2"
                }
            };

            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroups())
                .Returns(cartonPropertyGroups);

            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<CartonPropertyGroup>>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<CartonPropertyGroup>>, bool, bool>(
                    (msg, b, b2) =>
                    {
                        Specify.That(msg.MessageType).Should.BeEqualTo(CartonPropertyGroupMessages.CartonPropertyGroups);
                        Specify.That(msg.Data.SequenceEqual(cartonPropertyGroups)).Should.BeTrue();
                        responsePosted = true;
                    });

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object,
                serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message { MessageType = CartonPropertyGroupMessages.GetCartonPropertyGroups });
            Retry.For(() => responsePosted, TimeSpan.FromSeconds(1));
            Specify.That(responsePosted).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPostGetCartonPropertyGroupsMessageWhenUpdateIsFinished()
        {
            var responsePosted = false;

            var cartonPropertyGroups = new List<CartonPropertyGroup>()
            {
                new CartonPropertyGroup()
                {
                    Alias = "1"
                },
                new CartonPropertyGroup()
                {
                    Alias = "2"
                }
            };

            cartonPropertyGroupManager.Setup(manager => manager.GetCartonPropertyGroups())
                .Returns(cartonPropertyGroups);

            var cartonPropertyGroupService = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroupManager.Object, uiCommunicationServiceMock.Object,
                serviceLocatorMock.Object, logger.Object);

            eventAggregator.GetEvent<IMessage>().Where(m => m.MessageType == CartonPropertyGroupMessages.GetCartonPropertyGroups).Subscribe(x =>
            {
                responsePosted = true;
            });

            eventAggregator.Publish(new Message<CartonPropertyGroup> { MessageType = CartonPropertyGroupMessages.UpdateCartonPropertyGroup, Data = new CartonPropertyGroup() });
            Specify.That(responsePosted).Should.BeTrue();
            responsePosted = false;

            eventAggregator.Publish(new Message<CartonPropertyGroup> { MessageType = CartonPropertyGroupMessages.CreateCartonPropertyGroup, Data = new CartonPropertyGroup() });
            Specify.That(responsePosted).Should.BeTrue();
            responsePosted = false;

            eventAggregator.Publish(new Message<CartonPropertyGroup> { MessageType = CartonPropertyGroupMessages.DeleteCartonPropertyGroup, Data = new CartonPropertyGroup() });
            Specify.That(responsePosted).Should.BeTrue();
            responsePosted = false;
        }

        private BoxFirstProducible GetBoxFirstProducibleCarton(string customerId, CartonPropertyGroup cpg, IEnumerable<IRestriction> restrictions = null)
        {
            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg));
            p1.CustomerUniqueId = customerId;
            p1.Producible = new Carton();
            p1.Producible.CustomerUniqueId = customerId;
            if (restrictions != null)
            {
                restrictions.ForEach(p1.Restrictions.Add);
            }
            return p1;
        }

        private IEnumerable<IProducible> GetProduciblesThatBelongToCpg(int numberOfProducibles, CartonPropertyGroup cpg)
        {
            var produblies = new List<IProducible>();
            for (int i = 0; i < numberOfProducibles; i++)
            {
                var p = new BoxFirstProducible();
                p.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg));
                produblies.Add(p);
            }
            return produblies;
        }
    }
}
