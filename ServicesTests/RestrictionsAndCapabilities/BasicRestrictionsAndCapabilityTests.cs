﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Services;

using Testing.Specificity;

namespace ServicesTests.RestrictionsAndCapabilities
{
    [TestClass]
    public class BasicRestrictionsAndCapabilityTests
    {
        private IRestrictionResolverService rs;

        [TestInitialize]
        public void init()
        {
            rs = new RestrictionResolverService();
        }

        [TestMethod]
        public void BasicRestrictionWillCompareCorrectlyWhenTrue()
        {

            var mg = new MachineGroup();
            var m1 = Guid.NewGuid();
            var m2 = Guid.NewGuid();
            mg.UpdateMachineGroupCapabilities(m1, new List<ICapability> { new BasicCapability<string>("LabelPrinter") });
            mg.UpdateMachineGroupCapabilities(m2, new List<ICapability> { new BasicCapability<string>("CorrugateThicknes10"), new BasicCapability<int>(1) });

            //test object
            var basicReq1 = new BasicRestriction<string>("LabelPrinter");
            Specify.That(rs.Resolve(basicReq1, mg.AllMachineCapabilities)).Should.BeTrue();

            var basicReq2 = new BasicRestriction<int>(1);
            Specify.That(rs.Resolve(basicReq2, mg.AllMachineCapabilities)).Should.BeTrue();
        }

        [TestMethod]
        public void BasicRestrictionWillCompareCorrectlyWhenFalse()
        {

            var mg = new MachineGroup();
            var m1 = Guid.NewGuid();
            var m2 = Guid.NewGuid();
            mg.UpdateMachineGroupCapabilities(m1, new List<ICapability> { new BasicCapability<string>("LabelPrinter") });
            mg.UpdateMachineGroupCapabilities(m2, new List<ICapability> { new BasicCapability<string>("CorrugateThicknes10"), new BasicCapability<int>(1) });

            //test object
            var basicReq1 = new BasicRestriction<string>("LabelPrinter2");
            Specify.That(rs.Resolve(basicReq1, mg.AllMachineCapabilities)).Should.BeFalse();

            var basicReq2 = new BasicRestriction<int>(10);
            Specify.That(rs.Resolve(basicReq2, mg.AllMachineCapabilities)).Should.BeFalse();
        }

        [TestMethod]
        public void AllRestrictionWillCompareCorrectlyWhenTrue()
        {

            var mg = new MachineGroup();
            var m1 = Guid.NewGuid();
            var m2 = Guid.NewGuid();
            mg.UpdateMachineGroupCapabilities(m1, new List<ICapability> { new BasicCapability<string>("LabelPrinter") });
            mg.UpdateMachineGroupCapabilities(m2, new List<ICapability> { new BasicCapability<string>("CorrugateThicknes10"), new BasicCapability<int>(1) });

            //test object
            var basicReq1 = new BasicRestriction<string>("LabelPrinter");
            var basicReq2 = new BasicRestriction<int>(1);
            var basicReq3 = new BasicRestriction<int>(2);
            var aggregateRestriction = new AllRequiredRestriction();
            aggregateRestriction.Restrictions.Add(basicReq1);
            aggregateRestriction.Restrictions.Add(basicReq2);
            Specify.That(rs.Resolve(aggregateRestriction, mg.AllMachineCapabilities)).Should.BeTrue();
            //another test
            aggregateRestriction = new AllRequiredRestriction();
            aggregateRestriction.Restrictions.Add(basicReq1);
            aggregateRestriction.Restrictions.Add(basicReq3);
            Specify.That(rs.Resolve(aggregateRestriction, mg.AllMachineCapabilities)).Should.BeFalse();
        }

        [TestMethod]
        public void AnyRestrictionWillCompareCorrectlyWhenUsingAnyTrue()
        {

            var mg = new MachineGroup();
            var m1 = Guid.NewGuid();
            var m2 = Guid.NewGuid();
            mg.UpdateMachineGroupCapabilities(m1, new List<ICapability> { new BasicCapability<string>("CorrugateThicknes20") });
            mg.UpdateMachineGroupCapabilities(m2, new List<ICapability> { new BasicCapability<string>("CorrugateFlutingHorizontal"), new BasicCapability<int>(1) });

            //test object
            var basicReq1 = new BasicRestriction<string>("CorrugateThicknes10");
            var basicReq2 = new BasicRestriction<string>("CorrugateFlutingHorizontal");
            var restrictions = new AnyRequiredRestriction();
            restrictions.Restrictions.Add(basicReq1);
            restrictions.Restrictions.Add(basicReq2);
            Specify.That(rs.Resolve(restrictions, mg.AllMachineCapabilities)).Should.BeTrue();
        }
    }
}