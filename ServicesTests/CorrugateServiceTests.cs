﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Business.Corrugates;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Services;
using PackNet.Services.MachineServices;

using Testing.Specificity;

namespace ServicesTests
{
#if DEBUG
    [TestClass]
    public class CorrugateServiceTests
    {
        private Mock<IUICommunicationService> uiCommunicationServiceMock;

        [TestInitialize]
        public void Setup()
        {
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
        }

        [TestMethod]
        [Category("Unit")]
        public void Should_Handle_CreateMessage()
        {
            var eventAggregator = new EventAggregator();

            var corrugatesManagerMock = new Mock<ICorrugates>();

            corrugatesManagerMock.Setup(manager => manager.Create(It.IsAny<Corrugate>())).Returns<Corrugate>(c => c);

            var corrugateService = new CorrugateService(
                new Mock<IOptimalCorrugateCalculator>().Object, corrugatesManagerMock.Object,
                new Mock<IServiceLocator>().Object,
                new Mock<IAggregateMachineService>().Object,
                uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, new Mock<ILogger>(MockBehavior.Loose).Object);

            var newCorrugate = new Corrugate
            {
                Alias = "hi",
                Width = 2,
                Quality = 3,
                Thickness = 1
            };

            IResponseMessage<string> createdCorrugate = null;

            eventAggregator.GetEvent<IResponseMessage<string>>().Subscribe(x => createdCorrugate = x);

            eventAggregator.Publish(new Message<Corrugate> { MessageType = CorrugateMessages.CreateCorrugate, Data = newCorrugate });

            Specify.That(createdCorrugate).Should.Not.BeNull();
            Specify.That(createdCorrugate.Data).Should.Not.BeEqualTo(string.Empty);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_Handle_UpdateMessage()
        {
            var eventAggregator = new EventAggregator();

            var corrugatesManagerMock = new Mock<ICorrugates>();

            var oldCorrugate = new Corrugate
            {
                Alias = "hi",
                Width = 2,
                Quality = 3,
                Thickness = 1
            };

            var updatedCorrugate = new Corrugate
            {
                Alias = "I was updated",
                Width = 3,
                Quality = 4,
                Thickness = 5
            };

            var machines = new List<IPacksizeCutCreaseMachine>()
			{
				new FusionMachine()
			};

            var productionGroupService = new Mock<IProductionGroupService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var mockMachineServicesImporter = new Mock<IMachineServicesImporter>();
            var mockIServiceLocator = new Mock<IServiceLocator>();

            var machinesService = new AggregateMachineService(uiCommunicationService.Object, eventAggregator, eventAggregator, mockMachineServicesImporter.Object, mockIServiceLocator.Object,
                new Mock<ILogger>().Object);

            corrugatesManagerMock.Setup(
                c =>
                    c.Update(It.Is<Corrugate>(corr => corr.Id == updatedCorrugate.Id),
                        It.IsAny<IEnumerable<Corrugate>>())).Returns(updatedCorrugate);

            var corrugateService = new CorrugateService(
                new Mock<IOptimalCorrugateCalculator>().Object, corrugatesManagerMock.Object,
                new Mock<IServiceLocator>().Object,
                new Mock<IAggregateMachineService>().Object,
                uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, new Mock<ILogger>(MockBehavior.Loose).Object);

            IResponseMessage<string> updatedCorrugateMessage = null;

            eventAggregator.GetEvent<IResponseMessage<string>>().Subscribe(x => updatedCorrugateMessage = x);

            eventAggregator.Publish(new Message<Corrugate> { MessageType = CorrugateMessages.UpdateCorrugate, Data = oldCorrugate });

            corrugatesManagerMock.Verify(m => m.Update(oldCorrugate, It.IsAny<IEnumerable<Corrugate>>()), Times.Once);

            Specify.That(updatedCorrugateMessage).Should.Not.BeNull();
            Specify.That(updatedCorrugateMessage.Result).Should.BeEqualTo(ResultTypes.Success);
            Specify.That(updatedCorrugateMessage.Data).Should.Not.BeEqualTo(string.Empty);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_Not_UpdateCorrugate_When_CorrugateIsLoadedOnMachine()
        {
            var eventAggregator = new EventAggregator();

            var corrugatesManagerMock = new Mock<ICorrugates>();

            var oldCorrugate = new Corrugate
            {
                Alias = "hi",
                Width = 2,
                Quality = 3,
                Thickness = 1
            };

            var updatedCorrugate = new Corrugate
            {
                Alias = "I was updated",
                Width = 3,
                Quality = 4,
                Thickness = 5
            };

            var machines = new List<IPacksizeCutCreaseMachine>()
			{
				new FusionMachine()
				{
					Alias = "1",
					Tracks = new ConcurrentList<Track> { new Track{ LoadedCorrugate = oldCorrugate } }
				}
			};

            var productionGroupService = new Mock<IProductionGroupService>();
            var uiCommunicationService = new Mock<IUICommunicationService>();
            var mockMachineServicesImporter = new Mock<IMachineServicesImporter>();
            var mockIServiceLocator = new Mock<IServiceLocator>();

            var machinesService = new AggregateMachineService(uiCommunicationService.Object, eventAggregator, eventAggregator, mockMachineServicesImporter.Object, mockIServiceLocator.Object, new Mock<ILogger>().Object);

            corrugatesManagerMock.Setup(
                c =>
                    c.Update(It.Is<Corrugate>(corr => corr.Id == updatedCorrugate.Id),
                        It.IsAny<IEnumerable<Corrugate>>())).Returns(updatedCorrugate);

            var corrugateService = new CorrugateService(
                new Mock<IOptimalCorrugateCalculator>().Object, corrugatesManagerMock.Object,
                new Mock<IServiceLocator>().Object,
                new Mock<IAggregateMachineService>().Object,
                uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, new Mock<ILogger>(MockBehavior.Loose).Object);

            IResponseMessage<string> updatedCorrugateMessage = null;

            eventAggregator.GetEvent<IResponseMessage<string>>().Subscribe(x => updatedCorrugateMessage = x);
            eventAggregator.GetEvent<IResponseMessage<IEnumerable<Corrugate>>>().Subscribe(x =>
            {
                Specify.That(x.Data.SequenceEqual(new List<Corrugate> { oldCorrugate }));
            });

            eventAggregator.Publish(new Message<Corrugate> { MessageType = CorrugateMessages.UpdateCorrugate, Data = oldCorrugate });

            corrugatesManagerMock.Verify(m => m.Update(oldCorrugate, It.IsAny<IEnumerable<Corrugate>>()), Times.Once);

            Specify.That(updatedCorrugateMessage).Should.Not.BeNull();
            Specify.That(updatedCorrugateMessage.Result).Should.BeEqualTo(ResultTypes.Fail);
            Specify.That(updatedCorrugateMessage.Data).Should.Not.BeEqualTo(string.Empty);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_Handle_GetAllCorrugates()
        {
            var eventAggregator = new EventAggregator();

            var corrugatesManagerMock = new Mock<ICorrugates>();

            var corrugateService = new CorrugateService(
                new Mock<IOptimalCorrugateCalculator>().Object, corrugatesManagerMock.Object,
                new Mock<IServiceLocator>().Object,
                new Mock<IAggregateMachineService>().Object,
                uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, new Mock<ILogger>(MockBehavior.Loose).Object);

            corrugatesManagerMock.Setup(m => m.GetCorrugates()).Returns(new List<Corrugate>()
			{
				new Corrugate()
				{
					Id = Guid.NewGuid(),
					Alias = "hi"
				}
			});

            var retList = new List<Corrugate>();

            eventAggregator.GetEvent<IResponseMessage<IEnumerable<Corrugate>>>().Subscribe(x => retList = x.Data.ToList());

            eventAggregator.Publish(new Message<Corrugate> { MessageType = CorrugateMessages.GetCorrugates });

            corrugatesManagerMock.Verify(m => m.GetCorrugates(), Times.Once);

            Specify.That(retList).Should.Not.BeEmpty();
            Specify.That(retList.Count()).Should.BeEqualTo(1);
            Specify.That(retList.First().Alias).Should.BeEqualTo("hi");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_Handle_DeleteMessage()
        {
            var eventAggregator = new EventAggregator();

            var corrugatesManagerMock = new Mock<ICorrugates>();

            var corrugateService = new CorrugateService(
                new Mock<IOptimalCorrugateCalculator>().Object, corrugatesManagerMock.Object,
                new Mock<IServiceLocator>().Object,
                new Mock<IAggregateMachineService>().Object,
                uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, new Mock<ILogger>(MockBehavior.Loose).Object);

            var corrugateToDelete = new Corrugate
            {
                Id = Guid.NewGuid(),
                Alias = "hi"
            };

            var isCalled = false;

            eventAggregator.GetEvent<IResponseMessage<IEnumerable<Corrugate>>>().Subscribe(x => isCalled = true);

            eventAggregator.Publish(new Message<IEnumerable<Corrugate>> { MessageType = CorrugateMessages.DeleteCorrugates, Data = new List<Corrugate> { corrugateToDelete } });

            corrugatesManagerMock.Verify(m => m.Delete(It.Is<IEnumerable<Corrugate>>(l => l.Any(c => c.Alias == corrugateToDelete.Alias))), Times.Once);

            Specify.That(isCalled).Should.BeTrue();
        }
    }
#endif
}
