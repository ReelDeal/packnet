﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using Newtonsoft.Json;

using PackNet.Business.CartonPropertyGroup;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Services;

using Testing.Specificity;

using TestUtils;

namespace ServicesTests
{
    using PackNet.Common.Interfaces.Converters;

    [TestClass]
    public class CartonPropertyGroupLevelLoadingTests
    {
        ILogger logger = null;
        ICartonPropertyGroupService service = null;

        CartonPropertyGroup cpg1 = null;
        CartonPropertyGroup cpg2 = null;
        CartonPropertyGroup cpg3 = null;

        readonly Mock<ICartonPropertyGroups> cartonPropertyGroups = new Mock<ICartonPropertyGroups>();
        readonly Mock<IUICommunicationService> uiCommunicationService = new Mock<IUICommunicationService>();
        readonly EventAggregator eventAggregator = new EventAggregator();
        private Mock<IServiceLocator> serviceLocatorMock;

        [TestInitialize]
        public void TestInitialize()
        {
            cpg1 = new CartonPropertyGroup { Alias = "CPG1", Id = Guid.NewGuid() };
            cpg2 = new CartonPropertyGroup { Alias = "CPG2", Id = Guid.NewGuid() };
            cpg3 = new CartonPropertyGroup { Alias = "CPG3", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg2, cpg3 };

            cartonPropertyGroups.Setup(groups => groups.GetCartonPropertyGroups()).Returns(cpgs);

            cartonPropertyGroups.Setup(g => g.GetCartonPropertyGroupByAlias(It.IsAny<string>()))
                .Returns<string>(alias => cpgs.FirstOrDefault(g => g.Alias.Equals(alias)));
            serviceLocatorMock = new Mock<IServiceLocator>();

            new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "PG1",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2}) }
                }
            };
            logger = new ConsoleLogger();
            service = new CartonPropertyGroupService(eventAggregator, eventAggregator, cartonPropertyGroups.Object, uiCommunicationService.Object, serviceLocatorMock.Object, logger);
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandlesMixThatHasNotBeenSet()
        {
            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, cpg1);
            var availableProds = GetProduciblesThatBelongToCpg(100, cpg1);
            //When a PG has been configured with no CPG with a mix, return null and skip it altogether
            var noCpgPg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "No CPG PG",
                Options = new Dictionary<string, string> { }
            };
            var cpg = service.GetNextCPGToDispatchWorkTo(noCpgPg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg).Should.BeNull("No CPG has been configured for this pg so skip CPG selection altogether");
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandlesSingleCPG()
        {
            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, cpg1);
            var availableProds = GetProduciblesThatBelongToCpg(100, cpg1);
            //When a PG has been configured with no CPG with a mix, return null and skip it altogether
            cpg1.MixQuantity = 1;
            var singleCpgPg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "Single CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1}, SerializationSettings.GetJsonSerializerSettings())}
                }
            };
            var cpg = service.GetNextCPGToDispatchWorkTo(singleCpgPg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the only configured cpg... should always be returned");
            cpg = service.GetNextCPGToDispatchWorkTo(singleCpgPg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the only configured cpg... should always be returned");
            cpg = service.GetNextCPGToDispatchWorkTo(singleCpgPg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the only configured cpg... should always be returned");
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_50_50_Mix()
        {
            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2));
            var availableProds = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2));
            //When a PG has been configured with no CPG with a mix, return null and skip it altogether
            cpg1.MixQuantity = 1d;

            cpg2.MixQuantity = 1d;
            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "Single CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2}, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };
            var cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the first configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the first configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the first configured cpg...");
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 9141")]
        public void ItemsAreNotAddedToTheSkipList_WhenCpgToProduceWorkFor_IsSurged()
        {
            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, cpg1);
            var availableProds = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2));
            //When a PG has been configured with no CPG with a mix, return null and skip it altogether
            cpg1.MixQuantity = 1d;
            cpg1.Status = CartonPropertyGroupStatuses.Surge;

            cpg2.MixQuantity = 1d;
            cpg2.Status = CartonPropertyGroupStatuses.Normal;
            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "Single CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2}, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };
            var cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the first configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is still surged");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is still surged");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is still surged");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is still surged");

            cpg1.Status = CartonPropertyGroupStatuses.Normal;

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is still surged");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is still surged");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is still surged");

            prodsToGetCpgFor = prodsToGetCpgFor.Union(GetProduciblesThatBelongToCpg(100, cpg2));

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg1.Id, "CPG1 is the next configured cpg...");

            cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            Specify.That(cpg.Id).Should.BeEqualTo(cpg2.Id, "CPG2 is the next configured cpg...");
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_25_75_Mix()
        {
            //When a PG has been configured with no CPG with a mix, return null and skip it altogether
            cpg1.MixQuantity = 1d;
            cpg2.MixQuantity = 3d;
            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "Single CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2}, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };

            VerifyCPGIsReturnedNTimes(pg, cpg2, 2);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 3);

            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);

            VerifyCPGIsReturnedNTimes(pg, cpg2, 3);

            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);

            VerifyCPGIsReturnedNTimes(pg, cpg2, 3);
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_10_90_Mix()
        {
            //When a PG has been configured with no CPG with a mix, return null and skip it altogether
            cpg1.MixQuantity = 1d;

            cpg2.MixQuantity = 9d;
            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "Single CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2}, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };

            VerifyCPGIsReturnedNTimes(pg, cpg2, 9);

            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 9);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 9);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_20_20_60_Mix()
        {
            //When a PG has been configured with no CPG with a mix, return null and skip it altogether
            cpg1.MixQuantity = 2d;
            cpg2.MixQuantity = 2d;
            cpg3.MixQuantity = 6d;
            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "Single CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2, cpg3}, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };

            VerifyCPGIsReturnedNTimes(pg, cpg3, 2);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 1);

            VerifyCPGIsReturnedNTimes(pg, cpg3, 3);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 1);
            VerifyCPGIsReturnedNTimes(pg, cpg3, 1);
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_33_67_Mix_WhenCpg1IsSkipped()
        {
            cpg1.MixQuantity = 1d;
            cpg2.MixQuantity = 2d;
            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "1/2 CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2}, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };

            var availableProducibles = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2).Union(GetProduciblesThatBelongToCpg(100, cpg3)));

            //machine 1 can produce cpg1, but not cpg2
            var machine1Prods = GetProduciblesThatBelongToCpg(100, cpg1);

            //machine 2 can produce cpg2, but not cpg1
            var machine2Prods = GetProduciblesThatBelongToCpg(100, cpg2);

            //machine 3 can produce cpg1 and cpg2
            var machine3Prods = machine1Prods.Union(machine2Prods);

            VerifyCPGIsReturnedNTimes(pg, cpg2, 1, machine3Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 1, machine2Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1, machine1Prods, availableProducibles);

            VerifyCPGIsReturnedNTimes(pg, cpg2, 4, machine2Prods, availableProducibles);

            VerifyCPGIsReturnedNTimes(pg, cpg1, 2, machine3Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 1, machine3Prods, availableProducibles);
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_33_67_Mix_WhenCpg2IsSkipped()
        {
            cpg1.MixQuantity = 1d;
            cpg2.MixQuantity = 2d;
            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "1/2 CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(new List<CartonPropertyGroup>{cpg1, cpg2}, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };

            var availableProducibles = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2).Union(GetProduciblesThatBelongToCpg(100, cpg3)));

            //machine 1 can produce tiled cpg1, but not cpg2
            var machine1Prods = GetProduciblesThatBelongToCpg(100, cpg1);

            //machine 2 can produce untiled cpg1 and cpg2
            var machine2Prods = GetProduciblesThatBelongToCpg(100, cpg2).Union(machine1Prods);

            VerifyCPGIsReturnedNTimes(pg, cpg1, 2, machine1Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 5, machine2Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1, machine2Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg2, 2, machine2Prods, availableProducibles);
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_33_33_33_Mix_WhenCpg2IsSkipped()
        {
            cpg1.MixQuantity = 1d;
            cpg2.MixQuantity = 1d;
            cpg3.MixQuantity = 1d;

            var pg = new ProductionGroup()
            {
                Id = Guid.NewGuid(),
                Alias = "1/2/3 CPG PG",
                Options = new Dictionary<string, string>()
                {
                    {
                        "ConfiguredCartonPropertyGroups",
                        JsonConvert.SerializeObject(new List<CartonPropertyGroup> { cpg1, cpg2, cpg3 }, SerializationSettings.GetJsonSerializerSettings())
                    }
                }
            };

            var availableProducibles = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2).Union(GetProduciblesThatBelongToCpg(100, cpg3)));

            //machine 1 can produce cpg1 and cpg3
            var machine1Prods = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg3));

            //machine 2 can produce cpg1, cpg2 and cpg3
            var machine2Prods = availableProducibles.ToList();

            VerifyCPGIsReturnedNTimes(pg, cpg1, 1, machine1Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg3, 1, machine1Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1, machine1Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg3, 1, machine1Prods, availableProducibles);

            VerifyCPGIsReturnedNTimes(pg, cpg2, 2, machine2Prods, availableProducibles);

            VerifyCPGIsReturnedNTimes(pg, cpg2, 1, machine2Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg1, 1, machine2Prods, availableProducibles);
            VerifyCPGIsReturnedNTimes(pg, cpg3, 1, machine2Prods, availableProducibles);
        }

        [TestMethod]
        public void GetNextCPGToDispatchWorkToHandles_Staples_Mix()
        {
            var aToD = new CartonPropertyGroup { Alias = "A to D", Id = Guid.NewGuid() };
            var bToC = new CartonPropertyGroup { Alias = "B to C", Id = Guid.NewGuid() };
            var aToC = new CartonPropertyGroup { Alias = "A to C", Id = Guid.NewGuid() };
            var bToD = new CartonPropertyGroup { Alias = "B to D", Id = Guid.NewGuid() };
            var aToB = new CartonPropertyGroup { Alias = "A to B", Id = Guid.NewGuid() };
            var cToD = new CartonPropertyGroup { Alias = "C to D", Id = Guid.NewGuid() };
            var a = new CartonPropertyGroup { Alias = "A", Id = Guid.NewGuid() };
            var b = new CartonPropertyGroup { Alias = "B", Id = Guid.NewGuid() };
            var c = new CartonPropertyGroup { Alias = "C", Id = Guid.NewGuid() };
            var d = new CartonPropertyGroup { Alias = "D", Id = Guid.NewGuid() };

            aToD.MixQuantity = 37d;
            bToC.MixQuantity = 4d;
            aToC.MixQuantity = 10d;
            bToD.MixQuantity = 5d;
            aToB.MixQuantity = 6d;
            cToD.MixQuantity = 4d;
            a.MixQuantity = 10d;
            b.MixQuantity = 7d;
            c.MixQuantity = 8d;
            d.MixQuantity = 8d;

            var propertyGroups = new List<CartonPropertyGroup>
            {
                aToD,
                bToC,
                aToC,
                bToD,
                aToB,
                cToD,
                a,
                b,
                c,
                d
            };

            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, c)
                .Union(GetProduciblesThatBelongToCpg(100, d))
                .Union(GetProduciblesThatBelongToCpg(100, aToD))
                .Union(GetProduciblesThatBelongToCpg(100, bToC))
                .Union(GetProduciblesThatBelongToCpg(100, aToC))
                .Union(GetProduciblesThatBelongToCpg(100, bToD))
                .Union(GetProduciblesThatBelongToCpg(100, aToB))
                .Union(GetProduciblesThatBelongToCpg(100, cToD))
                .Union(GetProduciblesThatBelongToCpg(100, a))
                .Union(GetProduciblesThatBelongToCpg(100, b));
            var availableProds = GetProduciblesThatBelongToCpg(100, c)
                .Union(GetProduciblesThatBelongToCpg(100, d))
                .Union(GetProduciblesThatBelongToCpg(100, aToD))
                .Union(GetProduciblesThatBelongToCpg(100, bToC))
                .Union(GetProduciblesThatBelongToCpg(100, aToC))
                .Union(GetProduciblesThatBelongToCpg(100, bToD))
                .Union(GetProduciblesThatBelongToCpg(100, aToB))
                .Union(GetProduciblesThatBelongToCpg(100, cToD))
                .Union(GetProduciblesThatBelongToCpg(100, a))
                .Union(GetProduciblesThatBelongToCpg(100, b));

            var pg = new ProductionGroup
            {
                Id = Guid.NewGuid(),
                Alias = "Single CPG PG",
                Options = new Dictionary<string, string>
                {
                    {"ConfiguredCartonPropertyGroups", JsonConvert.SerializeObject(propertyGroups, SerializationSettings.GetJsonSerializerSettings()) }
                }
            };

            //for (int i = 0; i < 100; i++)
            //{
            //    var cpg = service.GetNextCPGToDispatchWorkTo(pg, prodsToGetCpgFor, availableProds);
            //    Trace.WriteLine("VerifyCPGIsReturnedNTimes(pg, " + cpg.Alias + ", 1, prodsToGetCpgFor, availableProds);");
            //}

            VerifyCPGIsReturnedNTimes(pg, aToD, 3, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, b, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToB, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 2, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, cToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, b, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToB, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, b, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 3, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToB, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, cToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 3, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, b, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToB, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, b, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, cToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 2, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToB, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, b, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 3, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, b, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToB, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, d, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, c, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, bToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, cToD, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToC, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, a, 1, prodsToGetCpgFor, availableProds);
            VerifyCPGIsReturnedNTimes(pg, aToD, 3, prodsToGetCpgFor, availableProds);
        }


        private void VerifyCPGIsReturnedNTimes(ProductionGroup pg, CartonPropertyGroup expectedCpg, int timesExpected)
        {
            var prodsToGetCpgFor = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2)).Union(GetProduciblesThatBelongToCpg(100, cpg3));
            var availableProds = GetProduciblesThatBelongToCpg(100, cpg1).Union(GetProduciblesThatBelongToCpg(100, cpg2)).Union(GetProduciblesThatBelongToCpg(100, cpg3));
            VerifyCPGIsReturnedNTimes(pg, expectedCpg, timesExpected, prodsToGetCpgFor, availableProds);
        }

        private void VerifyCPGIsReturnedNTimes(ProductionGroup pg, CartonPropertyGroup expectedCpg, int timesExpected, IEnumerable<IProducible> produciblesToGetCpgFor, IEnumerable<IProducible> availableProducibles)
        {
            var executedTimes = 0;
            while (executedTimes < timesExpected)
            {
                var cpg = service.GetNextCPGToDispatchWorkTo(pg, produciblesToGetCpgFor, availableProducibles);
                Trace.WriteLine("GetNextCPGToDispatchWorkTo returned cpg " + cpg.Alias);
                Specify.That(cpg.Id).Should.BeEqualTo(expectedCpg.Id);
                executedTimes++;
            }
        }

        private IEnumerable<IProducible> GetProduciblesThatBelongToCpg(int numberOfProducibles, CartonPropertyGroup cpg)
        {
            var produblies = new List<IProducible>();
            for (int i = 0; i < numberOfProducibles; i++)
            {
                var p = new BoxFirstProducible();
                p.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg));
                produblies.Add(p);
            }
            return produblies;
        }
    }
}
