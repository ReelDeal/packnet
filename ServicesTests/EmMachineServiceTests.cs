﻿using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Utils;
using PackNet.Services.MachineServices.CutCreaseMachines;

using Testing.Specificity;

namespace ServicesTests
{
    [TestClass]
    public class EmMachineServiceTests
    {
        private EmMachine machine;
        private IEmMachineService emMachineService;
        private EventAggregator aggregator;


        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IEmMachines> emMachinesMock;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IAggregateMachineService> iAggregateMachineServiceMock;
        private Mock<ILogger> loggerMock;

        [TestInitialize]
        public void Setup()
        {
            aggregator = new EventAggregator();
            machine = new EmMachine
            {
                Id = Guid.NewGuid()
            };

            loggerMock = new Mock<ILogger>();
            serviceLocatorMock = GetServiceLocatorMock();
            emMachinesMock = GetEmMachinesMock();
            uiCommunicationServiceMock = GetUiCommunicationsServiceMock();


            emMachineService = new EmMachineService(emMachinesMock.Object, serviceLocatorMock.Object,
                uiCommunicationServiceMock.Object, aggregator, aggregator, loggerMock.Object);
        }

        private Mock<IUICommunicationService> GetUiCommunicationsServiceMock()
        {
            var mock = new Mock<IUICommunicationService>();

            return mock;
        }

        private Mock<IEmMachines> GetEmMachinesMock()
        {
            var mock = new Mock<IEmMachines>();

            mock.Setup(m => m.GetMachines()).Returns(new List<EmMachine> { machine });

            return mock;
        }

        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            iAggregateMachineServiceMock = new Mock<IAggregateMachineService>();

            mock.Setup(m => m.Locate<IAggregateMachineService>()).Returns(iAggregateMachineServiceMock.Object);

            return mock;
        }
    }
}
