﻿//using System;
//using System.Reactive.Subjects;

//using Microsoft.VisualStudio.TestTools.UnitTesting;

//using PackNet.Common.Interfaces.DTO.PrintingMachines;
//using PackNet.Common.Interfaces.Logging;

//using TestUtils;

//namespace ServicesTests
//{
//    using System.Collections.Generic;
//    using System.Diagnostics;
//    using System.Linq;
//    using System.Reactive.Linq;
//    using System.Threading;
//    using Moq;

//    using PackNet.Common.Eventing;
//    using PackNet.Common.Interfaces.Enums;
//    using PackNet.Common.Interfaces.Exceptions;
//    using PackNet.Common.Interfaces.Services;
//    using PackNet.Common.Utils;
//    using PackNet.Services;

//    using Testing.Specificity;

//    [TestClass]
//    public class PrintServiceTests
//    {

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintServiceCanHandleASingleJobSentToASinglePrinter()
//        {
//            //Arrange
//            var eventFired = false;
//            var calledBackId = Guid.Empty;
//            var printerId = Guid.NewGuid();
//            var printData = new Printable("test printer");
//            var printRequestStatus = PrintRequestStatuses.Unknown;
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testPrinter = new TestPrinter { Id = printerId, Alias = "printer1" };
//            PrintRequestStatus printRequest = null;

//            IPrintService printService = new PrintService(new List<IPrinter> { testPrinter }, eventAggregator, mockLogger.Object);

//            //Wire up to the print events
//            printService.PrintRequestStatusChangedObservable
//                .Where(e => e.PrintRequests.First().JobStatus == PrintJobStatuses.Success)
//                .Subscribe(
//                    e =>
//                    {
//                        printRequest = e;
//                        calledBackId = e.UniqueID;
//                        eventFired = true;
//                        printRequestStatus = e.RequestStatus;
//                        Trace.WriteLine(DateTime.Now + " PrintRequestStatusChangedObservable callback complete " + Thread.CurrentThread.ManagedThreadId);
//                    });

//            var dataToPrint = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData}}
//                            };

//            //Act
//            var batchId = printService.Print(dataToPrint);

//            Retry.For(() => eventFired, TimeSpan.FromSeconds(10));

//            //Assert
//            Specify.That(eventFired).Should.BeTrue("Event was not fired when job completed");
//            Specify.That(batchId).Should.BeEqualTo(calledBackId, "RequestId from event should match requestId");
//            Specify.That(printRequestStatus).Should.BeEqualTo(PrintRequestStatuses.Complete, "Job should be complete");
//            mockLogger.Verify(m => m.Log(LogLevel.Info, "PrintService created with Printers: , printer1"), Times.Once);
//            mockLogger.Verify(m => m.Log(LogLevel.Info, "PrintService job " + printRequest.PrintRequests.First().UniqueId + " changed status to Success, batch status is Complete"), Times.Once);
//            mockLogger.Verify(m => m.Log(LogLevel.Info, String.Format("PrintService dispatching printable {0} to printer {1} from batch {2}", printData.Id, printerId, batchId)), Times.Once);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintServiceNotifiesWhenBatchStarted()
//        {
//            //Arrange
//            var eventFired = false;
//            var calledBackId = Guid.Empty;
//            var printerId = Guid.NewGuid();
//            var printData = new Printable("test printer");
//            var printRequestStatus = PrintRequestStatuses.Unknown;
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testStartedPrinter = new TestStartedPrinter { Id = printerId };

//            IPrintService printService = new PrintService(new List<IPrinter> { testStartedPrinter }, eventAggregator, mockLogger.Object);

//            //Wire up to the print events
//            printService.PrintRequestStatusChangedObservable
//                .Where(e => e.RequestStatus == PrintRequestStatuses.Started)
//                .Subscribe(
//                    e =>
//                    {
//                        calledBackId = e.UniqueID;
//                        eventFired = true;
//                        printRequestStatus = e.RequestStatus;
//                        Trace.WriteLine(DateTime.Now + " PrintRequestStatusChangedObservable callback complete " + Thread.CurrentThread.ManagedThreadId);
//                    });

//            var dataToPrint = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData}}
//                            };

//            //Act
//            var batchId = printService.Print(dataToPrint);

//            Retry.For(() => eventFired, TimeSpan.FromSeconds(10));

//            //Assert
//            Specify.That(eventFired).Should.BeTrue("Event was not fired when job started");
//            Specify.That(batchId).Should.BeEqualTo(calledBackId, "RequestId from event should match requestId");
//            Specify.That(printRequestStatus).Should.BeEqualTo(PrintRequestStatuses.Started, "Job should be started");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintServiceCanHandleMultipleJobsSentToASinglePrinter()
//        {
//            //Arrange
//            var eventFiredCount = 0;
//            var calledBackId = Guid.Empty;
//            var printerId = Guid.NewGuid();
//            var printData1 = new Printable("test printer");
//            var printData2 = new Printable("test printer");
//            var printData3 = new Printable("test printer");

//            var printRequestStatus = PrintRequestStatuses.Unknown;
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testPrinter = new TestPrinter { Id = printerId };

//            IPrintService printService = new PrintService(new List<IPrinter> { testPrinter }, eventAggregator, mockLogger.Object);

//            var inProgressCount = 0;
//            var completedCount = 0;


//            //Wire up to the print events
//            printService.PrintRequestStatusChangedObservable
//                //.Where(e => e.PrintRequests.First().JobStatus == Enums.PrintJobStatuses.Success)
//                .Subscribe(
//                    e =>
//                    {
//                        calledBackId = e.UniqueID;
//                        eventFiredCount++;
//                        printRequestStatus = e.RequestStatus;

//                        if (e.RequestStatus == PrintRequestStatuses.InProgress) inProgressCount++;
//                        if (e.RequestStatus == PrintRequestStatuses.Complete) completedCount++;
//                    });

//            var dataToPrint = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData1, printData2, printData3}}
//                            };

//            //Act
//            var printJobId = printService.Print(dataToPrint);

//            Retry.For(() => eventFiredCount == 3, TimeSpan.FromSeconds(10));

//            //Assert

//            Specify.That(eventFiredCount).Should.BeEqualTo(3, "Event was not fired when job completed");
//            Specify.That(printJobId).Should.BeEqualTo(calledBackId, "RequestId from event should match requestId");
//            Specify.That(printRequestStatus).Should.BeEqualTo(PrintRequestStatuses.Complete, "Job should be complete");

//            Specify.That(inProgressCount).Should.BeEqualTo(2, "InProgress should be fired when single jobs are in progress");
//            Specify.That(completedCount).Should.BeEqualTo(1, "Completed should be fired when the entire batch is complete");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        [Ignore]
//        public void PrintServiceNotifiesOnceWhenMultipleItemsInBatchStarted()
//        {
//            //Arrange
//            var calledBackId = Guid.Empty;
//            var printerId = Guid.NewGuid();
//            var printData1 = new Printable("test printer");
//            var printData2 = new Printable("test printer");
//            var printData3 = new Printable("test printer");

//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testStartedPrinter = new TestStartedPrinter { Id = printerId };

//            IPrintService printService = new PrintService(new List<IPrinter> { testStartedPrinter }, eventAggregator, mockLogger.Object);

//            // Wire up to count how many times the batch is started
//            var startedCount = 0;
//            printService.PrintRequestStatusChangedObservable
//                .Where(e => e.RequestStatus == PrintRequestStatuses.Started)
//                .Subscribe(
//                    e =>
//                    {
//                        calledBackId = e.UniqueID;
//                        if (e.RequestStatus == PrintRequestStatuses.Started) startedCount++;
//                        Trace.WriteLine("Started status from printer: " + e.UniqueID);
//                    });

//            printService.PrintRequestStatusChangedObservable
//                .Where(e => e.RequestStatus == PrintRequestStatuses.Error)
//                .Subscribe(
//                    e => Trace.WriteLine("Error status from printer: " + e.UniqueID + " " + e.PrintRequests.First().ErrorMessage));

//            // Wire up to validate when batch complete to ensure that we correctly validate the number of times that started called
//            var batchComplete = false;
//            printService.PrintRequestStatusChangedObservable
//                .Where(e => e.RequestStatus == PrintRequestStatuses.Complete)
//                .Subscribe(
//                    e =>
//                    {
//                        batchComplete = true;
//                        Trace.WriteLine("Complete status from printer: " + e.UniqueID);
//                    });

//            var dataToPrint = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData1, printData2, printData3}}
//                            };

//            //Act
//            var printJobId = printService.Print(dataToPrint);

//            Retry.For(() => batchComplete, TimeSpan.FromSeconds(15));

//            //Assert
//            Specify.That(batchComplete).Should.BeTrue("Batch was not completed, validated to ensure that that all requests complete but started only called once");
//            Specify.That(printJobId).Should.BeEqualTo(calledBackId, "RequestId from event should match requestId");
//            Specify.That(startedCount).Should.BeEqualTo(1, "Started should be fired only once when the batch is started");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintServiceShouldReturnAFailureIfThePrinterIsCurrentlyInUse()
//        {
//            var printerId = Guid.NewGuid();
//            var printData1 = new Printable("test printer");
//            var printData2 = new Printable("test printer");
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testPrinter = new TestPrinter { Id = printerId };

//            var dataToPrint1 = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData1}}
//                            };
//            var dataToPrint2 = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData2}}
//                            };

//            IPrintService printService = new PrintService(new List<IPrinter> { testPrinter }, eventAggregator, mockLogger.Object);

//            //Act
//            var batchId1 = printService.Print(dataToPrint1);
//            try
//            {
//                var batchId2 = printService.Print(dataToPrint2);
//            }
//            catch (Exception)
//            {
//                return;
//            }
//            Assert.Fail("Should not be allowed to send multiple jobs without the first batch completing");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintServiceShouldReturnAFailureIfThePrinterReportsItIsCurrentlyInUse()
//        {
//            var error = false;
//            PrintRequestStatus printRequestStatus = null;
//            var printerId = Guid.NewGuid();
//            var printData1 = new Printable("test printer");
            
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testPrinter = new Mock<IPrinter>();
//            testPrinter.Setup(m => m.Id).Returns(printerId);
//            testPrinter.Setup(m => m.PrinterStatusChangedObservable).Returns(new Subject<PrinterStatus>());
//            testPrinter.Setup(m => m.PrintJobStatusChangedObservable).Returns(new Subject<PrintJobStatus>());
//            testPrinter.Setup(m => m.Print(It.IsAny<Printable>())).Throws(new PrinterBusyException("I am a busy printer"));
//            var dataToPrint1 = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData1}}
//                            };

//            IPrintService printService = new PrintService(new List<IPrinter> { testPrinter.Object }, eventAggregator, mockLogger.Object);
//            printService.PrintRequestStatusChangedObservable.Where(e => e.RequestStatus == PrintRequestStatuses.Error).Subscribe((e) =>
//            {
//                printRequestStatus = e;
//                error = true;
//            });
//            //Act
//            var batchId1 = printService.Print(dataToPrint1);
//            Retry.For(() => error, TimeSpan.FromSeconds(10));
//            Specify.That(error).Should.BeTrue("Error should have been reported");
//            Specify.That(printRequestStatus.RequestStatus).Should.BeEqualTo(PrintRequestStatuses.Error, "Error should have been reported");
//            Specify.That(printRequestStatus.PrintRequests.First().ErrorMessage).Should.BeEqualTo("I am a busy printer", "Error should have been reported");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintServiceForwardsPrinterErrors()
//        {
//            var error = false;
//            var printerId = Guid.NewGuid();
//            var printData1 = new Printable("test printer");
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testPrinter = new TestErrorPrinter { Id = printerId };

//            var dataToPrint1 = new Dictionary<Guid, List<Printable>> {
//                                {printerId, new List<Printable>{printData1}}
//                            };

//            IPrintService printService = new PrintService(new List<IPrinter> { testPrinter }, eventAggregator, mockLogger.Object);
//            printService.PrintRequestStatusChangedObservable.Where(e => e.RequestStatus == PrintRequestStatuses.Error).Subscribe((e) => error = true);

//            //Act
//            var batchId1 = printService.Print(dataToPrint1);
//            Retry.For(() => error, TimeSpan.FromSeconds(1));
//            Specify.That(error).Should.BeTrue("Error should have been reported");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CallerIsNotifiedThatAPrinterDoesNotExist()
//        {
//            var printer1Id = Guid.NewGuid();
//            var printer2Id = Guid.NewGuid();
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();
//            IPrintService printService = new PrintService(new List<IPrinter> { }, eventAggregator, mockLogger.Object);
//            AssertUtil.Throws<PrinterNotFoundException>(() =>
//                {
//                    printService.Print(new Dictionary<Guid, List<Printable>> { { printer1Id, new List<Printable> { new Printable("test printer") } } });
//                }, "Printer '"+printer1Id+"' has not been configured");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CallerIsNotifiedThatAPrintJobIsNull()
//        {
//            var printer1Id = Guid.NewGuid();
//            var printer2Id = Guid.NewGuid();
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();
//            IPrintService printService = new PrintService(new List<IPrinter> { new TestPrinter{Id = printer1Id}}, eventAggregator, mockLogger.Object);
//            AssertUtil.Throws<ArgumentNullException>(() =>
//            {
//                printService.Print(new Dictionary<Guid, List<Printable>>
//                                   {
//                                       { printer1Id, new List<Printable> {new Printable("test printer") } },
//                                       { printer2Id, null }
//                                   });
//            }, "Data for printer '"+printer2Id+"' has invalid data (null or empty)" + Environment.NewLine + "Parameter name: itemsToPrint");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CallerIsNotifiedThatAPrintJobIsEmpty()
//        {
//            var printer1Id = Guid.NewGuid();
//            var printer2Id = Guid.NewGuid();
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();
//            IPrintService printService = new PrintService(new List<IPrinter> { new TestPrinter { Id = printer1Id } }, eventAggregator, mockLogger.Object);
//            AssertUtil.Throws<ArgumentNullException>(() =>
//            {
//                printService.Print(new Dictionary<Guid, List<Printable>>
//                                   {
//                                       { printer1Id, new List<Printable> {new Printable("test printer") } },
//                                       { printer2Id, new List<Printable>() }
//                                   });
//            }, "Data for printer '"+printer2Id+"' has invalid data (null or empty)" + Environment.NewLine + "Parameter name: itemsToPrint");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CallerIsNotifiedWithInvalidPrinterId()
//        {
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();
//            IPrintService printService = new PrintService(new List<IPrinter> { }, eventAggregator, mockLogger.Object);
//            AssertUtil.Throws<ArgumentNullException>(() =>
//            {
//                printService.Print(new Dictionary<Guid, List<Printable>>
//                                   {
//                                       { Guid.NewGuid(), new List<Printable> {new Printable("test printer") } },
//                                       { Guid.NewGuid(), new List<Printable>() }
//                                   });
//            }, "Referenced printer id has invalid data (null or empty)" + Environment.NewLine + "Parameter name: itemsToPrint");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintCanHandleSingleJobsSentToMultiplePrinters()
//        {
//            //Arrange
//            var eventFiredCount = 0;
//            var printerId1 = Guid.NewGuid();
//            var printerId2 = Guid.NewGuid();
//            var printData1 = new Printable("test printer");
//            var printData2 = new Printable("test printer2");
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();

//            var testPrinter = new TestPrinter { Id = printerId1 };
//            var testPrinter2 = new TestPrinter { Id = printerId2 };

//            IPrintService printService = new PrintService(new List<IPrinter> { testPrinter, testPrinter2 }, eventAggregator, mockLogger.Object);

//            //Wire up to the print events
//            printService.PrintRequestStatusChangedObservable
//                .Where(e => e.PrintRequests.First().JobStatus == PrintJobStatuses.Success)
//                .Subscribe(
//                    e =>
//                    {
//                        eventFiredCount++;
//                    });

//            var dataToPrint = new Dictionary<Guid, List<Printable>> {
//                                {printerId1, new List<Printable>{printData1}},
//                                {printerId2, new List<Printable>{printData2}}
//                            };

//            //Act
//            printService.Print(dataToPrint);

//            Retry.For(() => eventFiredCount == 2, TimeSpan.FromSeconds(10));

//            //Assert
//            Specify.That(eventFiredCount).Should.BeEqualTo(2, "Event was not fired when job completed");
//            Specify.That(testPrinter.ItemsPrinted.First().Id).Should.BeEqualTo(printData1.Id, "Item should have been sent to printer 1");
//            Specify.That(testPrinter2.ItemsPrinted.First().Id).Should.BeEqualTo(printData2.Id, "Item should have been sent to printer 2");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void PrintCanHandleMultipleJobsSentToMultiplePrinters()
//        {
//            //Arrange
//            var eventFiredCount = 0;
//            var printerId1 = Guid.NewGuid();
//            var printerId2 = Guid.NewGuid();
//            var printData1 = new Printable("test printer");
//            var printData2 = new Printable("test printer2");
//            var printData3 = new Printable("test printer3");
//            var printData4 = new Printable("test printer4");
//            var printData5 = new Printable("test printer5");
//            var printData6 = new Printable("test printer6");
//            var mockLogger = new Mock<ILogger>();
//            var eventAggregator = new EventAggregator();
//            var batchCompletedStatusCount = 0;

//            var testPrinter1 = new TestPrinter { Id = printerId1 };
//            var testPrinter2 = new TestPrinter { Id = printerId2 };

//            IPrintService printService = new PrintService(new List<IPrinter> { testPrinter1, testPrinter2 }, eventAggregator, mockLogger.Object);

//            //Wire up to the print events
//            printService.PrintRequestStatusChangedObservable
//                .Subscribe(
//                    e =>
//                    {
//                        if (e.RequestStatus == PrintRequestStatuses.Complete) 
//                            batchCompletedStatusCount++;
//                        eventFiredCount++;
//                    });

//            var dataToPrint = new Dictionary<Guid, List<Printable>> {
//                                {printerId1, new List<Printable>{printData1, printData2}},
//                                {printerId2, new List<Printable>{printData3, printData4, printData5, printData6}}
//                            };

//            //Act
//            printService.Print(dataToPrint);

//            Retry.For(() => eventFiredCount == 6, TimeSpan.FromSeconds(10));

//            //Assert
//            Specify.That(eventFiredCount).Should.BeEqualTo(6, "Event was not fired when job completed");
//            Specify.That(testPrinter1.ItemsPrinted.First().Id).Should.BeEqualTo(printData1.Id, "Item should have been sent to printer 1");
//            Specify.That(testPrinter1.ItemsPrinted.Last().Id).Should.BeEqualTo(printData2.Id, "Item should have been sent to printer 1");
//            Specify.That(testPrinter2.ItemsPrinted.First().Id).Should.BeEqualTo(printData3.Id, "Item should have been sent to printer 2");
//            Specify.That(testPrinter2.ItemsPrinted.Skip(1).Take(1).First().Id).Should.BeEqualTo(printData4.Id, "Item should have been sent to printer 2");
//            Specify.That(testPrinter2.ItemsPrinted.Skip(2).Take(1).First().Id).Should.BeEqualTo(printData5.Id, "Item should have been sent to printer 2");
//            Specify.That(testPrinter2.ItemsPrinted.Last().Id).Should.BeEqualTo(printData6.Id, "Item should have been sent to printer 2");
//            Specify.That(batchCompletedStatusCount).Should.BeEqualTo(1);
//        }
//    }
//}
