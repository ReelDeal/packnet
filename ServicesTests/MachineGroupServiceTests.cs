﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Utils;
using PackNet.Communication.Communicators;
using PackNet.Services.MachineServices;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using Testing.Specificity;
using TestUtils;

namespace ServicesTests
{
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.Enums.ProducibleStates;
    using PackNet.Common.Interfaces.Machines;
    using PackNet.Common.Interfaces.Producible;
    using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
    using PackNet.Common.Interfaces.Utils;

    [TestClass]
    public class MachineGroupServiceTests
    {
        private Mock<IMachineGroups> machineGroupsMock = new Mock<IMachineGroups>();
        private Mock<IProductionGroupService> productionGroupService = new Mock<IProductionGroupService>();
        private Mock<IAggregateMachineService> machineService = new Mock<IAggregateMachineService>();
        private Mock<IServiceLocator> serviceLocator = new Mock<IServiceLocator>();
        private Mock<ISelectionAlgorithmDispatchService> selectionAlgorithmService = new Mock<ISelectionAlgorithmDispatchService>();
        private Mock<IUICommunicationService> uiCommunicationService = new Mock<IUICommunicationService>();
        private Mock<IUserNotificationService> userNotificationService = new Mock<IUserNotificationService>();
        private ILogger logger = new ConsoleLogger();
        private EventAggregator eventAggregator = new EventAggregator();

        [TestMethod]
        public void ChangingMachineGroupProductionModeShouldSetMGStatusToCompleteQueueAndPause()
        {
            var mgs = MachineGroupService;
            //run test
            var mgId = Guid.NewGuid();
            var mg = new MachineGroup{Alias = "test MG", Id = mgId};
            //do what the rabbit service does when we recieve the message from the UI.
            eventAggregator.Publish(new Message<MachineGroup>
            {
                MessageType = MachineGroupMessages.ChangeMachineGroupProductionMode,
                Data = mg
            });

            Expression<Func<MachineGroup, bool>> machineIdExpression = p => p.Id == mgId;

            machineGroupsMock.Verify(v => v.UpdateProductionMode(It.Is(machineIdExpression)));
            machineGroupsMock.Verify(v => v.CompleteQueueAndPause(mgId));
        }

        [TestMethod]
        public void ShouldSendFailResponseWhenUpdateFails()
        {
            //setup
            machineGroupsMock.Setup(m => m.Update(It.IsAny<MachineGroup>())).Throws(new ApplicationException("failed to update"));


            var mgs = MachineGroupService;
            //run test
            var mgId = Guid.NewGuid();
            var mg = new MachineGroup { Alias = "test MG", Id = mgId };
            //do what the rabbit service does when we recieve the message from the UI.
            eventAggregator.Publish(new Message<MachineGroup>
            {
                MessageType = MachineGroupMessages.UpdateMachineGroup,
                Data = mg,
                ReplyTo = "replyToMe"
            });
            Thread.Sleep(100);
            uiCommunicationService.Verify(u => u.SendMessageToUI(It.Is<ResponseMessage<MachineGroup>>(r => r.ReplyTo == "replyToMe" && r.MessageType == MachineGroupMessages.MachineGroupUpdated && r.Data == mg && r.Result == ResultTypes.Fail), It.IsAny<bool>(), It.IsAny<bool>()));
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 13912")]
        [TestCategory("Integration")]        
        public void ShouldClearQueue_WhenMachineGroup_RunsOutOfCorrugate()
        {
            var corrugate = new Corrugate() { Alias = "C1", Quality = 1, Thickness = 3, Width = 800 };
            var emMachine = new EmMachine() { Alias = "EmMachine" };
            var machineGroup = new MachineGroup(){Alias = "MG1", Id = Guid.NewGuid()};

            emMachine.AddOrUpdateCapabilities(new List<ICapability>{new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(emMachine.Id, new List<Corrugate>{corrugate}))});
            emMachine.CurrentStatus = MachineStatuses.MachineOnline;
            machineGroup.ConfiguredMachines.Add(machineGroup.Id);
            machineGroup.AddOrUpdateCapabilities(emMachine.CurrentCapabilities);
            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;
            //setup
            machineGroupsMock.Setup(m => m.GetMachineGroups()).Returns(new List<MachineGroup> { machineGroup });
            machineGroupsMock.Setup(m => m.Find(It.IsAny<Guid>())).Returns(machineGroup);
            machineGroupsMock.Setup(m => m.FindByMachineGroupId(It.IsAny<Guid>())).Returns(machineGroup);
            machineGroupsMock.Setup(m => m.FindByMachineId(It.Is<Guid>(g => g == emMachine.Id))).Returns(machineGroup);
            machineGroupsMock.Setup(m => m.FindByAlias(It.IsAny<string>())).Returns(machineGroup);
            var canAddQueue = new Queue<bool>();
            canAddQueue.Enqueue(true);
            canAddQueue.Enqueue(false);
            canAddQueue.Enqueue(false);
            canAddQueue.Enqueue(false);
            machineGroupsMock.Setup(m => m.CanAddItemToQueue(machineGroup)).Returns(canAddQueue.Dequeue());
           
            var coc = new CartonOnCorrugate() { Corrugate = corrugate, TileCount = 1 };
            var optimalCorrugateCalculator = new Mock<IOptimalCorrugateCalculator>();
            optimalCorrugateCalculator.Setup(m => m.GetOptimalCorrugate(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(), It.IsAny<ICarton>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(coc);

            var carton = new Carton()
                         {
                             Length = 200,
                             Width = 200,
                             Height = 200,
                             DesignId = 2010002,
                             CorrugateQuality = 1,
                             CustomerUniqueId = "MyCarton"
                         };
            carton.CartonOnCorrugate = coc;
            var kit = new Kit() { CustomerUniqueId = "MyKit" };
            kit.AddProducible(carton);
            kit.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleSelected;
            var queue = new Queue<IProducible>();
            
            queue.Enqueue(kit);
            selectionAlgorithmService.Setup(s => s.DispatchSelectProducibleWorkflow(It.IsAny<MachineGroup>())).Returns(queue.Dequeue());
            machineGroupsMock.Setup(
                m =>
                    m.AddItemToQueue(It.IsAny<MachineGroup>(), It.Is<IProducible>(p => p.CustomerUniqueId == kit.CustomerUniqueId)))
                .Returns(true);

            var mgs = MachineGroupService;

            eventAggregator.Publish(new Message()
                                    {
                                        MessageType = MachineGroupMessages.GetWorkForMachineGroup,
                                        MachineGroupId = machineGroup.Id,
                                        MachineGroupName = machineGroup.Alias
                                    });

            var clearQueueCalled = false;
            machineGroupsMock.Setup(m => m.ClearQueue(It.Is<Guid>(mg => mg == machineGroup.Id))).Callback(
                () => { clearQueueCalled = true; });

            Retry.For(() => mgs.GetProducibleFirstInQueue(machineGroup) != null, TimeSpan.FromSeconds(10));
            Specify.That(mgs.GetProducibleFirstInQueue(machineGroup) == null).Should.BeFalse("Expected something to be in the queue by now");
            kit.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup;
            machineGroup.CurrentStatus = MachineGroupUnavailableStatuses.MachineGroupChangingCorrugate;

            Retry.For(() => clearQueueCalled, TimeSpan.FromSeconds(5));
            Specify.That(clearQueueCalled).Should.BeTrue("Clear queue was not called when the machine group ran out of corrugate.");
            machineGroupsMock.Verify(m => m.ClearQueue(It.Is<Guid>(mg => mg == machineGroup.Id)), Times.Once(), "Clear queue should only have been called once.");

        }


        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 10307")]
        [TestCategory("Unit")]
        public void ShouldNotBeAbleToCreateMachineGroupWithSameAliasAsOneThatExists()
        {
            var eventCalled = false;
            machineGroupsMock.Setup(m => m.FindByAlias(It.IsAny<string>())).Returns(new MachineGroup());

            uiCommunicationService.Setup(
                m => m.SendMessageToUI(It.IsAny<IMessage<MachineGroup>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<MachineGroup>, bool, bool>(
                    (mg, a, b) =>
                    {
                        var responseMessage = mg as ResponseMessage<MachineGroup>;
                      
                        Specify.That(responseMessage.Result).Should.BeEqualTo(ResultTypes.Fail);

                        eventCalled = true;
                    });

            var mgs = MachineGroupService;

            eventAggregator.Publish(new Message<MachineGroup>()
            {
                MessageType = MachineGroupMessages.CreateMachineGroup,
                Data = new MachineGroup()
            });
            
            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void ShouldSendFailResponseWhenDeleteFails()
        {
            //setup
            machineGroupsMock.Setup(m => m.Delete(It.IsAny<MachineGroup>())).Throws(new ApplicationException("failed to delete"));


            var mgs = MachineGroupService;
            //run test
            var mgId = Guid.NewGuid();
            var mg = new MachineGroup { Alias = "test MG", Id = mgId };
            //do what the rabbit service does when we recieve the message from the UI.
            eventAggregator.Publish(new Message<List<MachineGroup>>
            {
                MessageType = MachineGroupMessages.DeleteMachineGroup,
                Data = new List<MachineGroup> { mg}
            });
            Thread.Sleep(100);
            uiCommunicationService.Verify(u => u.SendMessageToUI(It.Is<ResponseMessage<MachineGroup>>(r => r.MessageType == MachineGroupMessages.MachineGroupDeleted && r.Data == mg && r.Result == ResultTypes.Fail), It.IsAny<bool>(), It.IsAny<bool>()));
        }

        [TestMethod]
        public void ShouldSendExistsResponseWhenDeleteFailsBecauseMgIsAttachedToPg()
        {
            //setup
            machineGroupsMock.Setup(m => m.Delete(It.IsAny<MachineGroup>())).Throws(new RelationshipExistsException("failed to delete"));

            var mgs = MachineGroupService;
            //run test
            var mgId = Guid.NewGuid();
            var mg = new MachineGroup { Alias = "test MG", Id = mgId };
            //do what the rabbit service does when we recieve the message from the UI.
            eventAggregator.Publish(new Message<List<MachineGroup>>
            {
                MessageType = MachineGroupMessages.DeleteMachineGroup,
                Data = new List<MachineGroup> { mg }
            });
            Thread.Sleep(100);
            uiCommunicationService.Verify(u => u.SendMessageToUI(It.Is<ResponseMessage<MachineGroup>>(r => r.MessageType == MachineGroupMessages.MachineGroupDeleted && r.Data == mg && r.Result == ResultTypes.Exists), It.IsAny<bool>(), It.IsAny<bool>()));
        }

        [TestMethod]
        [ExpectedException(typeof(RelationshipExistsException))]
        public void ShouldThrowExceptionWhenTryingToDeleteMgAttachedToPg()
        {
            machineGroupsMock.Setup(m => m.Delete(It.IsAny<MachineGroup>())).Throws(new RelationshipExistsException("failed to delete"));
            var mgId = Guid.NewGuid();
            productionGroupService.Setup(p => p.GetProductionGroupForMachineGroup(It.Is<Guid>(g => g == mgId))).Returns(new ProductionGroup() { Id = Guid.NewGuid(), Alias = "PG" });
            var mgs = MachineGroupService;

            var mg = new MachineGroup { Alias = "test MG", Id = mgId };
            mgs.Delete(mg);

            ResetMocks();
        }

        [TestMethod]
        public void ShouldSendFailResponseWhenCreateFails()
        {
            //setup
            machineGroupsMock.Setup(m => m.Create(It.IsAny<MachineGroup>())).Throws(new ApplicationException("failed to create"));


            var mgs = MachineGroupService;
            //run test
            var mgId = Guid.NewGuid();
            var mg = new MachineGroup { Alias = "test MG", Id = mgId };
            //do what the rabbit service does when we recieve the message from the UI.
            eventAggregator.Publish(new Message<MachineGroup>
            {
                MessageType = MachineGroupMessages.CreateMachineGroup,
                Data = mg
            });
            Thread.Sleep(100);
            uiCommunicationService.Verify(u => u.SendMessageToUI(It.Is<ResponseMessage<MachineGroup>>(r => r.MessageType == MachineGroupMessages.MachineGroupCreated && r.Data == mg && r.Result == ResultTypes.Fail), It.IsAny<bool>(), It.IsAny<bool>()));

        }

        [TestMethod]
        public void ShouldSendUserNotification_WhenLongheadsOutOfPositionWarning()
        {
            //setup
            var mgId = Guid.NewGuid();
            var mg = new MachineGroup { Alias = "test MG", Id = mgId };   
            var machineId = Guid.NewGuid();

            machineService.Setup(ms => ms.FindById(machineId)).Returns(new EmMachine() { Alias = "Test EM", Id = machineId });
            machineGroupsMock.Setup(m => m.FindByMachineId(machineId)).Returns(mg);
            serviceLocator.Setup(sl => sl.Locate<IUserNotificationService>()).Returns(userNotificationService.Object);
            var mgs = MachineGroupService;

            //do what the rabbit service does when we recieve the message from the UI.
            eventAggregator.Publish(new Message<PlcMachineWarning>
            {
                MessageType = MachineMessages.PlcWarning,
                Data = new PlcMachineWarning(machineId, PacksizeMachineWarningCodes.LongheadOutOfPosition, new []{5.0, 5.0, 5,0, 5.0}),
            });
            Thread.Sleep(100);
            userNotificationService.Verify(u => u.SendNotificationToMachineGroup(NotificationSeverity.Error, It.IsAny<string>(), It.IsAny<string>(), mg.Id), Times.Once, "No notification was sent to the machine group");
        }

        [TestMethod]
        public void ShouldSendUserNotification_WhenCrossheadOutOfPositionWarning()
        {
            //setup
            var mgId = Guid.NewGuid();
            var mg = new MachineGroup { Alias = "test MG", Id = mgId };
            var machineId = Guid.NewGuid();

            machineService.Setup(ms => ms.FindById(machineId)).Returns(new EmMachine() { Alias = "Test EM", Id = machineId });
            machineGroupsMock.Setup(m => m.FindByMachineId(machineId)).Returns(mg);
            serviceLocator.Setup(sl => sl.Locate<IUserNotificationService>()).Returns(userNotificationService.Object);
            var mgs = MachineGroupService;

            //do what the rabbit service does when we recieve the message from the UI.
            eventAggregator.Publish(new Message<PlcMachineWarning>
            {
                MessageType = MachineMessages.PlcWarning,
                Data = new PlcMachineWarning(machineId, PacksizeMachineWarningCodes.CrossheadOutOfPosition, new[] { 0.0 }),
            });
            Thread.Sleep(100);
            userNotificationService.Verify(u => u.SendNotificationToMachineGroup(NotificationSeverity.Error, It.IsAny<string>(), It.IsAny<string>(), mg.Id), Times.Once, "No notification was sent to the machine group");
        }

        public MachineGroupService MachineGroupService
        {
            get
            {
                return new MachineGroupService(
                    machineGroupsMock.Object,
                    machineService.Object
                    ,productionGroupService.Object
                    , selectionAlgorithmService.Object
                    ,serviceLocator.Object,
                    uiCommunicationService.Object,
                    eventAggregator,eventAggregator,logger);
            }
        }

        private void ResetMocks()
        {
            machineGroupsMock = new Mock<IMachineGroups>();
            productionGroupService = new Mock<IProductionGroupService>();
            machineService = new Mock<IAggregateMachineService>();
            serviceLocator = new Mock<IServiceLocator>();
            selectionAlgorithmService = new Mock<ISelectionAlgorithmDispatchService>();
            uiCommunicationService = new Mock<IUICommunicationService>();
            userNotificationService = new Mock<IUserNotificationService>();
        }
    }
}
