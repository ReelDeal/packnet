﻿using System.Diagnostics;
using System.Reactive.Subjects;

using PackNet.Business.ArticleManagement;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Services.Articles;

namespace ServicesTests
{
    using System.Activities.Expressions;

    using Moq;
    using Newtonsoft.Json;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using PackNet.Business.PackagingDesigns;
    using PackNet.Common.Eventing;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Articles;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Interfaces.Exceptions;
    using System.Reactive.Linq;
    using Testing.Specificity;

    [TestClass]
    public class ArticleServiceTests
    {
        private EventAggregator eventAggregator;
        private Mock<IServiceLocator> serviceLocator;
        private ArticleService service;
        private Mock<IArticles> articleMock;
        private Mock<IPackagingDesignManager> packagingDesignManager;
        
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<ILogger> logger;
        private Mock<IProductionGroupService> productionGroupService;

        [TestInitialize]
        public void Init()
        {
            logger = new Mock<ILogger>();
            serviceLocator = new Mock<IServiceLocator>();
            eventAggregator = new EventAggregator();
            articleMock = new Mock<IArticles>();
            packagingDesignManager = new Mock<IPackagingDesignManager>();

            productionGroupService = new Mock<IProductionGroupService>();
            packagingDesignManager.Setup(a => a.GetDesignName(2000001)).Returns("IQ 0200-0001 INCHES");
            packagingDesignManager.Setup(a => a.GetDesignName(2010001)).Returns("IQ 0201-0001 INCHES");
            packagingDesignManager.Setup(a => a.GetDesignName(2030001)).Returns(string.Empty);
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
            uiCommunicationServiceMock.Setup(u => u.UIEventObservable).Returns(new Subject<Tuple<MessageTypes, string>>());
        }

        #region Get

        [TestMethod]
        public void GetArticlePerformsExactMatchOnArticleId()
        {
            articleMock.Setup(m => m.Find("555")).Returns(new Article{ArticleId = "555"});
            service = Service;

            var article = service.Find("555");

            Specify.That(article).Should.Not.BeNull();
            Specify.That(article.ArticleId).Should.BeEqualTo("555");
        }

        private ArticleService Service
        {
            get
            {
                return new ArticleService(articleMock.Object, serviceLocator.Object, productionGroupService.Object, packagingDesignManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, logger.Object);
            }
        }

        [TestMethod]
        public void GetArticleReturnsNullWhenGivenEmptyStringForArticleId()
        {
            service = Service;

            var article = service.Find(string.Empty);

            Specify.That(article).Should.BeNull();
        }

        [TestMethod]
        public void GetArticleReturnsNullWhenNoMatchInDatabase()
        {
            service = Service;

            var article = service.Find("555");

            Specify.That(article).Should.BeNull();
        }

        [TestMethod]
        public void GetArticleReturnsNullAndLogsExceptionWhenExeptionThrown()
        {
            service = Service;

            const string someArticleId = "any article";
            var article = service.Find(someArticleId);

            Specify.That(article).Should.BeNull();
            logger.Verify(l => l.LogException(LogLevel.Error, It.Is<string>(s => s.Contains(someArticleId)), It.IsAny<Exception>()), Times.Once);
        }

        [TestMethod]
        public void GetArticleWillLookupDesignNameWithDesignIdWhenDesignNameIsNullOrEmptyString()
        {
            articleMock.Setup(m => m.Find("555")).Returns(new Article { ArticleId = "555", DesignId = 555});
            packagingDesignManager.Setup(m => m.GetDesignName(555)).Returns("IQ 0200-0001 INCHES");
            service = Service;

            var article = service.Find("555");

            Specify.That(article).Should.Not.BeNull();
            Specify.That(article.ArticleId).Should.BeEqualTo("555");
            Specify.That(article.DesignName).Should.BeEqualTo("IQ 0200-0001 INCHES");
        }

        [TestMethod]
        public void GetArticleWillDefaultDesignNameToMissingDesignIdAndSetMissingDesignFileToFalseWhenPackagingDesignManagerCannotFindDesign()
        {
            articleMock.Setup(m => m.Find("666")).Returns(new Article { ArticleId = "666", DesignId = 666 });
            packagingDesignManager.Setup(m => m.GetDesignName(666)).Returns("");
            service = Service;

            var article = service.Find("666");

            Specify.That(article).Should.Not.BeNull();
            Specify.That(article.DesignName).Should.BeEqualTo("Missing file (design id: 666)");
            Specify.That(article.MissingDesignFile).Should.BeTrue();
        }

        #endregion

        #region Search

        [TestMethod]
        public void SearchWillFilterResults()
        {
            var articleList = new List<Article>
            {
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D98"), ArticleId = "665" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D97"), ArticleId = "666" }
            };

            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            var articles = service.Search("5").ToList();

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(2);
            Specify.That(articles.Any(a => a.ArticleId == "555")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "665")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "666")).Should.BeFalse();
        }

        [TestMethod]
        public void SearchWillFilterResultsForDesignName()
        {

            var articleList = new List<Article>
            {
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555", DesignName = "IQ 201-0001 INCHES" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D98"), ArticleId = "665", DesignName = "IQ 200-0001 INCHES" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D97"), ArticleId = "666", DesignName = "IQ 203-0001 INCHES" }
            };


            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            var articles = service.Search("201-0001").ToList();

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(1);
            Specify.That(articles.Any(a => a.ArticleId == "555")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "665")).Should.BeFalse();
            Specify.That(articles.Any(a => a.ArticleId == "666")).Should.BeFalse();
        }

        [TestMethod]
        public void SearchWillReturnAllResultsOnNullFilter()
        {

            var articleList = new List<Article>
            {
                new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" },
                new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D98"), ArticleId = "665" },
                new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D97"), ArticleId = "666" }
            };


            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            var articles = service.Search(null).ToList();

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(3);
        }

        [TestMethod]
        public void SearchWillLookupDesignNameWithDesignIdWhenDesignNameIsNullOrEmptyString()
        {

            var articleList = new List<Article>
            {
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555", DesignId = 2000001, DesignName = null} ,
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D98"), ArticleId = "665", DesignId = 2010001, DesignName = string.Empty}
            };

            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            var articles = service.Search(null).ToList();

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(2);

            var article555 = articles.FirstOrDefault(a => a.ArticleId == "555");
            Assert.IsNotNull(article555);
            Specify.That(article555.DesignName).Should.BeEqualTo("IQ 0200-0001 INCHES");

            var article665 = articles.FirstOrDefault(a => a.ArticleId == "665");
            Assert.IsNotNull(article665);
            Specify.That(article665.DesignName).Should.BeEqualTo("IQ 0201-0001 INCHES");
        }

        [TestMethod]
        public void SearchWillDefaultDesignNameToMissingDesignIdAndSetMissingDesignFileToFalseWhenPackagingDesignManagerCannotFindDesign()
        {

            var articleList = new List<Article>
            {
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "666", DesignId = 2030001, DesignName = null}
            };

            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            var articles = service.Search(null).ToList();

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(1);

            var article666 = articles.FirstOrDefault(a => a.ArticleId == "666");
            Assert.IsNotNull(article666);
            Specify.That(article666.DesignName).Should.BeEqualTo("Missing file (design id: 2030001)");
            Specify.That(article666.MissingDesignFile).Should.BeTrue();
        }

        [TestMethod]
        public void SearchWillReturnAllResultsOnEmptyFilter()
        {

            var articleList = new List<Article>
            {
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D98"), ArticleId = "665" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D97"), ArticleId = "666" }
            };

            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            var articles = service.Search("").ToList();

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(3);
        }

        [TestMethod]
        public void SearchWillSearchAllFieldsCaseInsensitively()
        {
            var articleShouldMatch1 = new Article
            {
                Id = Guid.NewGuid(),
                ArticleId = "555",  // search for string value 5 will match on string field
                CorrugateQuality = 1,
                Description = "Five",
                DesignId = 200101,
                Height = 10,
                Length = 10,
                Width = 10,
                UserName = "SomeUser"
            };
            var articleShouldMatch2 = new Article
            {
                Id = Guid.NewGuid(),
                ArticleId = "123",
                CorrugateQuality = 2,
                Description = "one two three",
                DesignId = 5,   // search for string value 5 will match on numerical field
                Height = 9,
                Length = 9,
                Width = 9,
                UserName = "Another User"
            };
            var articleShouldNotMatch = new Article
            {
                Id = Guid.NewGuid(),
                ArticleId = "777",
                CorrugateQuality = 1,
                Description = "Seven",
                DesignId = 200101,
                Height = 10,
                Length = 10,
                Width = 10,
                UserName = "SomeUser"
            };


            var articleList = new List<Article>
            {
                articleShouldMatch1,
                articleShouldMatch2,
                articleShouldNotMatch
            };

            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            var articles = service.Search("5").ToList();

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(2);
            Specify.That(articles.Any(a => a.ArticleId == "555")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "123")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "777")).Should.BeFalse();
        }
                //todo: fix unit test
#if DEBUG
        [TestMethod]
        [Conditional("Debug")]
        public void EventAggregatorIsNotifiedWhenSearchSucceeds()
        {
            var articleList = new List<Article>
            {
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D98"), ArticleId = "665" },
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D97"), ArticleId = "666" }
            };

            IEnumerable<Article> articles = new List<Article>();
            eventAggregator
                .GetEvent<IResponseMessage<SearchArticle>>()
                .Where(
                    m =>
                        m.Result == ResultTypes.Success &&
                        m.MessageType == ArticleMessages.SearchArticle)
                .Subscribe(a => articles = a.Data.Articles);

            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;

            service.Search("5");

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(2);
            Specify.That(articles.Any(a => a.ArticleId == "555")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "665")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "666")).Should.BeFalse();
        }
        
#endif
        [TestMethod]
        public void ObserversAreNotifiedWhenSearchSucceeds()
        {
            var articleList = new List<Article>
            {
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555"},
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D98"), ArticleId = "665"},
                new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D97"), ArticleId = "666"}
            };


            articleMock.Setup(m => m.All()).Returns(articleList);
            service = Service;
            var articles = new List<Article>();
            service.SearchResultObservable.Subscribe(x => articles = x.ToList());

            service.Search("5");

            Specify.That(articles).Should.Not.BeNull();
            Specify.That(articles.Count()).Should.BeEqualTo(2);
            Specify.That(articles.Any(a => a.ArticleId == "555")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "665")).Should.BeTrue();
            Specify.That(articles.Any(a => a.ArticleId == "666")).Should.BeFalse();
        }

        [TestMethod]
        public void FailedObservableNotifiedWhenFailedSearchAndErrorLogged()
        {
            var eventCalled = false;

            articleMock.Setup(m => m.All()).Throws<Exception>();
            service = Service;
            service.FailedObservable.Subscribe(x => eventCalled = true);

            service.Search("this search throws error");

            Specify.That(eventCalled);
            logger.Verify(l => l.LogException(LogLevel.Error, It.Is<string>(s => s.Contains("search")), It.IsAny<Exception>()), Times.Once);
        }

        [TestMethod]
        public void ObserversAreNotifiedWhenSearchFailedSearchAndErrorLogged()
        {
            var eventCalled = false;

            articleMock.Setup(m => m.All()).Throws<Exception>();
            service = Service;
            service.FailedObservable.Subscribe(x => eventCalled = true);

            service.Search("this search throws error");

            Specify.That(eventCalled);
            logger.Verify(l => l.LogException(LogLevel.Error, It.Is<string>(s => s.Contains("search")), It.IsAny<Exception>()), Times.Once);
        }

        [TestMethod]
        public void EventAggregatorIsNotifiedWhenSearchFailedSearchAndErrorLogged()
        {
            var eventCalled = false;

            articleMock.Setup(m => m.All()).Throws<Exception>();
            service = Service;
            eventAggregator
                .GetEvent<IResponseMessage<Article>>()
                .Where(
                    m =>
                        m.Result == ResultTypes.Fail &&
                        m.MessageType == ArticleMessages.SearchArticle)
                .Subscribe(a => eventCalled = true);

            service.Search("this search throws error");

            Specify.That(eventCalled);
            logger.Verify(l => l.LogException(LogLevel.Error, It.Is<string>(s => s.Contains("search")), It.IsAny<Exception>()), Times.Once);
        }

        #endregion

        #region Create

        [TestMethod]
        public void ShouldBeAbleToSerializeXvaluesIntoArticle()
        {
            var someArticle = JsonConvert.DeserializeObject<Article>("{\r\n \"Id\": null,\r\n    \"Custom\": false,\r\n    \"ArticleId\": \"123\",\r\n    \"Description\": null,\r\n    \"Length\": 123,\r\n    \"Width\": 123,\r\n    \"Height\": 123,\r\n    \"DesignId\": 2000001,\r\n    \"MachineType\": null,\r\n    \"CorrugateQuality\": \"\",\r\n    \"DesignName\": null,\r\n    \"MissingDesignFile\": false,\r\n    \"XValues\": {\r\n      \"X1\": \"123\",\r\n      \"X2\": \"123\"\r\n    },\r\n    \"UserName\": \"PacksizeAdmin\",\r\n    \"LastUpdated\": \"2014-11-04T08:43:52+01:00\"\r\n}");
            
            Specify.That(someArticle.XValues.Count).Should.BeEqualTo(2);
        }

        [TestMethod]
        public void ObserversAreNotifiedWhenCreateArticle()
        {
            var someArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" };
            service = Service;

            var eventCalled = false;
            service.CreatedObservable.Subscribe(a => eventCalled = true);
            someArticle.Id = Guid.Empty;
            service.Create(someArticle);

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void ObserversAreNotifiedWhenCreateArticleFails()
        {
            var eventCalled = false;
            articleMock
                .Setup(r => r.Create(It.IsAny<Article>()))
                .Throws(new Exception());

            service = Service;
            service.FailedObservable.Subscribe(a => eventCalled = true);

            service.Create(new Article());

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void EventAggregatorIsNotifiedWhenCreateArticleFails()
        {
            var eventCalled = false;
            articleMock
                .Setup(r => r.Create(It.IsAny<Article>()))
                .Throws(new Exception());

            service = Service;
            eventAggregator
                .GetEvent<IResponseMessage<Article>>()
                .Where(m => m.Result == ResultTypes.Fail && m.MessageType == ArticleMessages.ArticleCreated)
                .Subscribe(a => eventCalled = true);

            service.Create(new Article());

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void ArticleServiceLogsFailedCreateAttemptAndReturnsNull()
        {
            var someArticle = new Article();
            articleMock.Setup((a => a.Create(It.IsAny<Article>()))).Throws(new Exception());

            service = Service;

            var result = service.Create(someArticle);

            logger.Verify(l => l.LogException(LogLevel.Error, "Failed to create article", It.IsAny<Exception>()));
            Specify.That(result).Should.BeNull();
        }

        [TestMethod]
        public void ObserversAreNotifiedWhenCreateArticleWithExistingArticle()
        {
            var eventCalled = false;
            var someArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" };
            articleMock
                .Setup(r => r.Create(It.IsAny<Article>()))
                .Throws(new ItemExistsException(""));

            service = Service;
            eventAggregator
                .GetEvent<IResponseMessage<Article>>()
                .Where(
                    m =>
                        m.MessageType == ArticleMessages.ArticleCreated &&
                        m.Result == ResultTypes.Exists)
                .Subscribe(a => eventCalled = true);
            someArticle.Id = Guid.Empty;
            service.Create(someArticle);

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void EventAggregatorIsNotifiedWhenCreateArticleWithExistingArticle()
        {
            var eventCalled = false;
            var someArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" };
            articleMock.Setup((a => a.Create(It.IsAny<Article>()))).Throws(new ItemExistsException(""));
            
            service = Service;
            service.FailedObservable.Where(
                m =>
                    m.MessageType == ArticleMessages.ArticleCreated &&
                    m.Result == ResultTypes.Exists).Subscribe(a => eventCalled = true);
            someArticle.Id = Guid.Empty;
            service.Create(someArticle);

            Specify.That(eventCalled).Should.BeTrue();
        }

        #endregion

        #region Update
        [TestMethod]
        public void ObserversAreNotifiedWhenUpdateArticle()
        {
            var someArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" };
            articleMock.Setup((a => a.Update(It.IsAny<Article>()))).Returns(someArticle);

            service = Service;

            var eventCalled = false;
            service.ModifiedObservable.Subscribe(a => eventCalled = true);

            service.Update("", someArticle);

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void ObserversAreNotifiedWhenUpdateArticleFails()
        {
            var eventCalled = false;
            articleMock.Setup(m => m.Update(It.IsAny<Article>())).Throws<Exception>();
            service = Service;
            service.FailedObservable.Subscribe(a => eventCalled = true);

            service.Update("", new Article());

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void EventAggregatorIsNotifiedWhenUpdateArticle()
        {
            var eventCalled = false;
            var someArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" };
            articleMock.Setup((a => a.Update(It.IsAny<Article>()))).Returns(someArticle);
            
            service = Service;
            eventAggregator
                .GetEvent<IResponseMessage<Article>>()
                .Where(m => m.Result == ResultTypes.Success && m.Data.Id == someArticle.Id)
                .Subscribe(a => eventCalled = true);

            service.Update("", someArticle);

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void EventAggregatorIsNotifiedWhenUpdateArticleFails()
        {
            var eventCalled = false;

            articleMock.Setup((a => a.Update(It.IsAny<Article>()))).Throws(new Exception());

            service = Service;
            eventAggregator
                .GetEvent<IResponseMessage<Article>>()
                .Where(m => m.Result == ResultTypes.Fail && m.MessageType == ArticleMessages.ArticleUpdated)
                .Subscribe(a => eventCalled = true);

            service.Update("", new Article());

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void ArticleServiceLogsFailedUpdateAttemptAndReturnNull()
        {
            const string someArticleId = "someArticleId";
            var someArticle = new Article { ArticleId = someArticleId };
            articleMock.Setup((a => a.Update(It.IsAny<Article>()))).Throws(new Exception());

            service = Service;

            var result = service.Update(someArticleId, someArticle);

            logger.Verify(l => l.LogException(LogLevel.Error, "Failed to update article", It.IsAny<Exception>()));
            Specify.That(result).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishScanTriggerResponseOnScanRequest()
        {
            var called = false;

            eventAggregator.GetEvent<ArticleScanTriggerResponse>().Subscribe(
                data =>
                {
                    called = true;
                    Specify.That(data.Article.ArticleId).Should.BeEqualTo("123");
                    Specify.That(data.Article.Id).Should.BeEqualTo(Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"));
                });

            articleMock.Setup(m => m.Find("123")).Returns(new Article {Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "123"});
            service = Service;

            eventAggregator.Publish(new ArticleScanTriggerRequest { ArticleId = "123", ProductionGroup = "PG1", TriggerTime = DateTime.Now });

            Specify.That(called).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishNullArticleIfTheArticleDoesNotExist()
        {
            var called = false;

            eventAggregator.GetEvent<ArticleScanTriggerResponse>().Subscribe(
                data =>
                {
                    called = true;
                    Specify.That(data.Article).Should.BeNull();
                });

            service = Service;

            eventAggregator.Publish(new ArticleScanTriggerRequest { ArticleId = "Evil", ProductionGroup = "PG1", TriggerTime = DateTime.Now });

            Specify.That(called).Should.BeTrue();
        }

        #endregion

        #region Delete
        [TestMethod]
        public void ObserversAreNotifiedWhenDeleteArticle()
        {
            Guid id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99");
            var someArticle = new Article { Id = id, ArticleId = "555" };
            service = Service;

            var eventCalled = false;
            service.DeletedObservable.Subscribe(a => eventCalled = true);

            service.Delete(new List<String> { someArticle.Id.ToString() });

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void ObserversAreNotifiedWhenDeleteArticleFails()
        {
            var eventCalled = false;

            service = Service;
            service.FailedObservable.Subscribe(a => eventCalled = true);

            service.Delete(new List<String> { "" });

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void EventAggregatorIsNotifiedWhenDeleteArticle()
        {
            var eventCalled = false;
            var someArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" };
            articleMock.Setup(m => m.Delete(It.IsAny<Guid>()));
            service = Service;
            eventAggregator
                .GetEvent<IResponseMessage<Article>>()
                .Where(m => m.MessageType == ArticleMessages.ArticleDeleted && m.Result == ResultTypes.Success)
                .Subscribe(a => eventCalled = true);

            service.Delete(new List<String> { someArticle.Id.ToString() });

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void EventAggregatorIsNotifiedWhenDeleteArticleFails()
        {
            var eventCalled = false;

            service = Service;
            eventAggregator
                .GetEvent<IResponseMessage<Article>>()
                .Where(m => m.Result == ResultTypes.Fail && m.MessageType == ArticleMessages.ArticleDeleted)
                .Subscribe(a => eventCalled = true);

            service.Delete(new List<String> { "" });

            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        public void RepositoryCalledWhenArticleIsDeleted()
        {
            const string someArticleId = "someArticleId";
            var someArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = someArticleId };

            service = Service;
            service.Delete(new List<String> { someArticle.Id.ToString() });
        }

        [TestMethod]
        public void ArticleServiceLogsFailedDeleteAttemptAndReturnNull()
        {
            const string someArticleId = "someArticleId";
            var someArticle = new Article { ArticleId = someArticleId };
            articleMock.Setup(m => m.Delete(It.IsAny<Guid>())).Throws(new Exception());
            service = Service;

            service.Delete(new List<String> { someArticle.Id.ToString() });

            logger.Verify(l => l.LogException(LogLevel.Error, It.Is<string>(i => i.Contains("Failed to delete article:")), It.IsAny<Exception>()));
        }
        #endregion

        #region produce

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddMachineTypeRestrictionToProducibleWhenProducingMachineTypeSpecificArticle()
        {
            var order1Called = false;
            var order2Called = false;

            var machineSpecificArticle = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "555" };
            var nonSpecificArticle  = new Article { Id = Guid.Parse("1F27188A-1DB1-4EDD-B15A-290699F89D99"), ArticleId = "456" };

            articleMock.Setup(m => m.Find("555")).Returns(machineSpecificArticle);
            articleMock.Setup(m => m.Find("456")).Returns(nonSpecificArticle);

            eventAggregator.GetEvent<ImportMessage<IEnumerable<IProducible>>>().Subscribe(m => m.Data.OfType<Order>().ForEach(o =>
            {
                if (o.OrderId == "1")
                {
                    Specify.That(o.Producible).Should.Not.BeNull();                    
                    order1Called = true;
                }
                else if (o.OrderId == "2")
                {
                    Specify.That(o.Producible).Should.Not.BeNull();                    
                    order2Called = true;
                }
            }));

            service = Service;

            eventAggregator.Publish(new Message<List<Carton>>
            {
                MessageType = ArticleMessages.ProduceArticles,
                Data = new List<Carton>()
                {
                    new Carton() { ArticleId = "555", Quantity = 1, CustomerUniqueId = "1" },
                    new Carton() { ArticleId = "456", Quantity = 1, CustomerUniqueId = "2" }
                }
            });

            Retry.For(() => order1Called && order2Called, TimeSpan.FromSeconds(1));
            Specify.That(order1Called).Should.BeTrue();
            Specify.That(order2Called).Should.BeTrue();
        }

        #endregion
    }
}
