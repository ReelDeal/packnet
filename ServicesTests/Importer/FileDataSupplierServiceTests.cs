﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;
using PackNet.Services.Importer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;

using PackNet.Common.Interfaces.Services;
using PackNet.Services;

using Testing.Specificity;

using PackNetServerSettings = PackNet.Common.Interfaces.DTO.Settings.PackNetServerSettings;

namespace ServicesTests.Importer
{
	[TestClass]
	public class FileDataSupplierServiceTests
	{
		private Mock<ILogger> logger;
		private Mock<IPackNetServerSettingsService> packNetServerSettingsService;
		private Mock<IUserNotificationService> userNotificationService;

		private PackNetServerSettings packNetServerSettings;

		[TestInitialize]
		public void Init()
		{
			packNetServerSettings = new PackNetServerSettings
			{
				FileImportSettings = null
			};

			packNetServerSettingsService = new Mock<IPackNetServerSettingsService>();
			packNetServerSettingsService.Setup(m => m.GetSettings()).Returns(packNetServerSettings);

			userNotificationService = new Mock<IUserNotificationService>();
			
			logger = new Mock<ILogger>();
		}

		[TestMethod]
		[TestCategory("Integration")]
		[DeploymentItem(@"TestFiles\100Articles.csv")]
		public void FileWatcherWillProcessAFileDroppedIntoWatchedDirectory()
		{
			IEnumerable<dynamic> processedFileData = null;
			var calledCount = 0;
			var eventAggregator = new EventAggregator();
			eventAggregator.GetEvent<IMessage<IEnumerable<dynamic>>>()
				.Where(m => m.MessageType == MessageTypes.RawDataImported)
				.Subscribe(
					m =>
					{
						processedFileData = m.Data;
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ';',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				File.Copy(Path.Combine(Environment.CurrentDirectory, "100Articles.csv"), Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "100Articles.csv"));
				Thread.Sleep(1000);
			}

			Retry.For(() => processedFileData != null, TimeSpan.FromSeconds(4));

			var processedFileDataEnumerated = processedFileData as dynamic[] ?? processedFileData.ToArray();
			Specify.That(processedFileDataEnumerated).Should.Not.BeNull();
			Specify.That(calledCount).Should.BeEqualTo(1);
			Specify.That(processedFileDataEnumerated.Count()).Should.BeEqualTo(100);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[DeploymentItem(@"TestFiles\100Articles.csv")]
		public void FileWatcherWillFailFileAndCreateLogAndPublishFailWhenFileLockedLongerThanRetry()
		{
			var eventAggregator = new EventAggregator();
			Tuple<string, Exception> message = null;
			eventAggregator.GetEvent<IMessage<Tuple<string, Exception>>>()
				.Where(m => m.MessageType == MessageTypes.ImportFailed)
				.Subscribe(
					m =>
					{
						message = m.Data;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ';',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 2
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);
			var filePath = Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "100Articles.csv");
			File.Copy(Path.Combine(Environment.CurrentDirectory, "100Articles.csv"), filePath);

			// Lock the file so that the file watcher cannot access it
			var fs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);

			// Create file watcher
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				Thread.Sleep(1000);
			}

			Retry.For(() => message != null, TimeSpan.FromSeconds(4));
			fs.Dispose();

			Specify.That(message.Item1).Should.BeEqualTo(filePath);
			Specify.That(message.Item2).Should.BeInstanceOfType(typeof(IOException));

			Thread.Sleep(1000);
			Specify.That(Directory.GetFiles(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "*.failed*").Length).Should.BeEqualTo(1);
			logger.Verify(l => l.Log(LogLevel.Error, It.IsAny<string>(), It.Is<object[]>(o => o.Contains(filePath))));
		}

		[TestMethod]
		[TestCategory("Unit")]
		[Description("We cannot monitor for all files or the .success and fail will be imported. Also, we decided to not just ignore those extensions by fiat.")]
		public void FileWatcherWillLogAndNotifyUserWhenExtensionGivenIsAsterisk()
		{
			var eventAggregator = new EventAggregator();

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ';',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "*", // Empty file extension will result in all files being monitored
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);

		    new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object,
		        userNotificationService.Object, logger.Object);
            logger.Verify(l => l.Log(LogLevel.Error, "Cannot use an asterisk(*) as the file extension to watch. Change file extension in file import settings."), Times.Once);
            userNotificationService.Verify(ns => ns.SendNotificationToSystem(NotificationSeverity.Error, 
                "Cannot use an asterisk(*) as the file extension to watch. Change file extension in file import settings.",
                string.Empty), Times.Once);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[DeploymentItem(@"TestFiles\100Articles.csv")]
		public void FileWatcherWillProcessAnExistingFileWhenStarted()
		{
			var calledCount = 0;
			IEnumerable<dynamic> processedFileData = null;
			var eventAggregator = new EventAggregator();
			eventAggregator.GetEvent<IMessage<IEnumerable<dynamic>>>()
				.Where(m => m.MessageType == MessageTypes.RawDataImported)
				.Subscribe(
					m =>
					{
						processedFileData = m.Data;
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ';',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);
			File.Copy(Path.Combine(Environment.CurrentDirectory, "100Articles.csv"), Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "100Articles.csv"));
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				Thread.Sleep(1000);
			}

			Retry.For(() => processedFileData != null, TimeSpan.FromSeconds(4));

			var processedFileDataEnumerated = processedFileData as dynamic[] ?? processedFileData.ToArray();
			Specify.That(processedFileDataEnumerated).Should.Not.BeNull();
			Specify.That(calledCount).Should.BeEqualTo(1);
			Specify.That(processedFileDataEnumerated.Count()).Should.BeEqualTo(100);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[DeploymentItem(@"TestFiles\100Articles.csv")]
		public void FileWatcherWillDeleteFileAfterCompleted()
		{
			var calledCount = 0;
			IEnumerable<dynamic> processedFileData = null;
			var eventAggregator = new EventAggregator();
			eventAggregator.GetEvent<IMessage<IEnumerable<dynamic>>>()
				.Where(m => m.MessageType == MessageTypes.RawDataImported)
				.Subscribe(
					m =>
					{
						processedFileData = m.Data;
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = true,
				FieldDelimiter = ';',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				File.Copy(Path.Combine(Environment.CurrentDirectory, "100Articles.csv"), Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "100Articles.csv"));
				Thread.Sleep(1000);
			}

			Retry.For(() => processedFileData != null, TimeSpan.FromSeconds(4));

			var processedFileDataEnumerated = processedFileData as dynamic[] ?? processedFileData.ToArray();
			Specify.That(processedFileDataEnumerated).Should.Not.BeNull();
			Specify.That(calledCount).Should.BeEqualTo(1);
			Specify.That(processedFileDataEnumerated.Count()).Should.BeEqualTo(100);
			Specify.That(Directory.GetFiles((packNetServerSettings.FileImportSettings.MonitoredFolderPath)).Count()).Should.BeEqualTo(0);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[DeploymentItem(@"TestFiles\100Articles - Invalid header.csv")]
		public void FileWatcherHandlesException()
		{
			var eventAggregator = new EventAggregator();
			var calledCount = 0;
			Tuple<string, Exception> failedMessage = null;
			eventAggregator.GetEvent<IMessage<Tuple<string, Exception>>>()
				.Where(m => m.MessageType == MessageTypes.ImportFailed)
				.Subscribe(
					m =>
					{
						failedMessage = m.Data;
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = true,
				FieldDelimiter = ';',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				File.Copy(Path.Combine(Environment.CurrentDirectory, "100Articles - Invalid header.csv"),
					Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "100Articles - Invalid header.csv"));
				Thread.Sleep(1000);
			}

			Retry.For(() => calledCount == 1, TimeSpan.FromSeconds(5));

			Assert.IsNotNull(failedMessage);
			Specify.That(failedMessage.Item1.Contains("100Articles - Invalid header.csv")).Should.BeTrue();
			Specify.That(failedMessage.Item2.Message.ToLower().Contains("invalid as it contains special characters")).Should.BeTrue();
		}

		[TestMethod]
		[TestCategory("Unit")]
		public void FileWatcherLogsAndPublishesAdminNotificationWhenMonitoredFolderIsDeleted()
		{
			var eventAggregator = new EventAggregator();
			var errors = new List<string>();
			eventAggregator.GetEvent<IMessage<string>>()
				.Where(m => m.MessageType == MessageTypes.AdminNotification)
				.Subscribe(m => errors.Add(m.Data));
			var fileWatcherDir = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName());

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = true,
				FieldDelimiter = ';',
				MonitoredFolderPath = fileWatcherDir,
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15,
				SecondsToRetryToAccessMonitoredFilePath = 1,
				TimesToRetryToAccessMonitoredFilePath = 5
			};

			Directory.CreateDirectory(fileWatcherDir);
			var fw = new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object);

			Directory.Delete(fileWatcherDir);
			Retry.For(() => errors.Count == 2, TimeSpan.FromSeconds(10));

			logger.Verify(l => l.Log(LogLevel.Fatal, It.Is<string>(s => s.ToLower().Contains("no longer available")), It.IsAny<object[]>()), Times.Once);
			logger.Verify(l => l.Log(LogLevel.Error, It.Is<string>(s => s.ToLower().Contains("failed attempt")), It.IsAny<object[]>()), Times.Exactly(5));
			Specify.That(errors.Count).Should.BeEqualTo(2);
			Specify.That(errors.Any(e => e.ToLower().Contains("no further data will be imported until it becomes available. will retry reconnection"))).Should.BeTrue();
			Specify.That(errors.Any(e => e.ToLower().Contains("all attempts failed to reconnect"))).Should.BeTrue();

			fw.Dispose();
		}

		[TestMethod]
		[TestCategory("Integration")]
		[Description("This test is to benchmark the file watcher")]
		public void FileWatcherLogsWhenFileIsAlreadyBeingProcessed()
		{
			var eventAggregator = new EventAggregator();

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = true,
				FieldDelimiter = ',',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);

			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				for (var i = 0; i < 2; i++)
				{
					CreateDropFile(packNetServerSettings.FileImportSettings.MonitoredFolderPath, 1, packNetServerSettings.FileImportSettings.FieldDelimiter, packNetServerSettings.FileImportSettings.FileExtension,
						"duplicate.csv");
					Thread.Sleep(50);
				}
			}

			logger.Verify(
				l =>
					l.Log(LogLevel.Trace, It.Is<string>(s => s.ToLower().Contains("already being processed")),
						It.Is<object[]>(o => o.Any(x => x.ToString().Contains("duplicate.csv")))), Times.AtLeastOnce);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[Description("This test is to benchmark the file watcher")]
		[DeploymentItem(@"TestFiles\1000_MOCK_DATA.csv")]
		public void Benchmark_Import_1000_Records()
		{
			IEnumerable<dynamic> processedFileData = null;
			var calledCount = 0;
			var eventAggregator = new EventAggregator();
			eventAggregator.GetEvent<IMessage<IEnumerable<dynamic>>>()
				.Where(m => m.MessageType == MessageTypes.RawDataImported)
				.Subscribe(
					m =>
					{
						processedFileData = m.Data;
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ',',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);

			var sw = Stopwatch.StartNew();
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				File.Copy(Path.Combine(Environment.CurrentDirectory, "1000_MOCK_DATA.csv"), Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "1000_MOCK_DATA.csv"));
				Thread.Sleep(1000);
			}

			Retry.For(() => processedFileData != null, TimeSpan.FromSeconds(3));
			sw.Stop();

			Debug.WriteLine("Processing 1,000 records took:{0}ms", sw.ElapsedMilliseconds);

			var processedFileDataEnumerated = processedFileData as dynamic[] ?? processedFileData.ToArray();
			Specify.That(processedFileDataEnumerated).Should.Not.BeNull();
			Specify.That(calledCount).Should.BeEqualTo(1);
			Specify.That(processedFileDataEnumerated.Count()).Should.BeEqualTo(1000);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[Description("This test is to benchmark the file watcher")]
		[DeploymentItem(@"TestFiles\10000_MOCK_DATA.csv")]
		public void Benchmark_Import_10000_Records()
		{
			IEnumerable<dynamic> processedFileData = null;
			var calledCount = 0;
			var eventAggregator = new EventAggregator();
			eventAggregator.GetEvent<IMessage<IEnumerable<dynamic>>>()
				.Where(m => m.MessageType == MessageTypes.RawDataImported)
				.Subscribe(
					m =>
					{
						processedFileData = m.Data;
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ',',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);

			var sw = Stopwatch.StartNew();
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				File.Copy(Path.Combine(Environment.CurrentDirectory, "10000_MOCK_DATA.csv"), Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "10000_MOCK_DATA.csv"));
				Thread.Sleep(1000);
			}

			Retry.For(() => processedFileData != null, TimeSpan.FromSeconds(3));
			sw.Stop();

			Debug.WriteLine("Processing 10,000 records took:{0}ms", sw.ElapsedMilliseconds);

			var processedFileDataEnumerated = processedFileData as dynamic[] ?? processedFileData.ToArray();
			Specify.That(processedFileDataEnumerated).Should.Not.BeNull();
			Specify.That(calledCount).Should.BeEqualTo(1);
			Specify.That(processedFileDataEnumerated.Count()).Should.BeEqualTo(10000);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[Description("This test is to benchmark the file watcher")]
		[DeploymentItem(@"TestFiles\100000_MOCK_DATA.csv")]
		public void Benchmark_Import_100000_Records()
		{
			IEnumerable<dynamic> processedFileData = null;
			var calledCount = 0;
			var eventAggregator = new EventAggregator();
			eventAggregator.GetEvent<IMessage<IEnumerable<dynamic>>>()
				.Where(m => m.MessageType == MessageTypes.RawDataImported)
				.Subscribe(
					m =>
					{
						processedFileData = m.Data;
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ',',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);

			var sw = Stopwatch.StartNew();
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				File.Copy(Path.Combine(Environment.CurrentDirectory, "100000_MOCK_DATA.csv"), Path.Combine(packNetServerSettings.FileImportSettings.MonitoredFolderPath, "100000_MOCK_DATA.csv"));
				Thread.Sleep(1000);
			}

			Retry.UntilTrue(() => processedFileData != null, TimeSpan.FromSeconds(10));
			sw.Stop();

			Debug.WriteLine("Processing 100,000 records took:{0}ms", sw.ElapsedMilliseconds);
            Specify.That(processedFileData).Should.Not.BeNull("processedFileData was null.  We never received the data message from file watcher.");
			var processedFileDataEnumerated = processedFileData as dynamic[] ?? processedFileData.ToArray();
			Specify.That(processedFileDataEnumerated).Should.Not.BeNull();
			Specify.That(calledCount).Should.BeEqualTo(1);
			Specify.That(processedFileDataEnumerated.Count()).Should.BeEqualTo(100000);
		}

		[TestMethod]
		[TestCategory("Integration")]
		[Description("This test is to benchmark the file watcher")]
		public void Benchmark_Import_100_Files_Every_50_Milliseconds()
		{
			var processedFileData = new List<IEnumerable<dynamic>>();
			var calledCount = 0;
			var eventAggregator = new EventAggregator();
			eventAggregator.GetEvent<IMessage<IEnumerable<dynamic>>>()
				.Where(m => m.MessageType == MessageTypes.RawDataImported)
				.Subscribe(
					m =>
					{
						processedFileData.Add(m.Data);
						calledCount++;
					});

			packNetServerSettings.FileImportSettings = new FileImportSettings
			{
				DeleteImportedFiles = false,
				FieldDelimiter = ',',
				MonitoredFolderPath = Path.Combine(Environment.CurrentDirectory, Path.GetRandomFileName()),
				FileExtension = "csv",
				SecondsToRetryLockedFile = 15
			};

			Directory.CreateDirectory(packNetServerSettings.FileImportSettings.MonitoredFolderPath);

			var sw = Stopwatch.StartNew();
			using (new FileDataSupplierService(eventAggregator, eventAggregator, packNetServerSettingsService.Object, userNotificationService.Object, logger.Object))
			{
				for (var i = 0; i < 100; i++)
				{
					CreateDropFile(packNetServerSettings.FileImportSettings.MonitoredFolderPath, 1);
					Thread.Sleep(50);
				}
			}

			Retry.For(() => calledCount == 100, TimeSpan.FromSeconds(10));
			sw.Stop();

			Debug.WriteLine("Processing 100 files dropped every 50 milliseconds took:{0}ms", sw.ElapsedMilliseconds);
			Specify.That(calledCount).Should.BeEqualTo(100);
			Specify.That(processedFileData.Count).Should.BeEqualTo(100);
		}

		/// <summary>
		/// Creates a drop file with a random file name containing records equal to the given count.
		/// </summary>
		/// <param name="path">Path to create the file</param>
		/// <param name="count">Number of records to create</param>
		/// <param name="delimiter">Delimiter to separate fields</param>
		/// <param name="extension">Extension to append to the file</param>
		/// <param name="setFileName">Use to set the name of the file created</param>
		private static void CreateDropFile(string path, int count = 100, char delimiter = ',', string extension = ".csv",
			string setFileName = null)
		{
			var fileName = setFileName ?? Path.GetRandomFileName() + extension;
			var fields = new[]
			{
				"Command", "SerialNumber", "OrderId", "Quantity", "Name", "Length", "Width", "Height",
				"ClassificationNumber", "CorrugateQuality", "PickZone", "PrintInfoField1", "PrintInfoField2",
				"PrintInfoField3", "PrintInfoField4", "PrintInfoField5", "PrintInfoField6", "PrintInfoField7",
				"PrintInfoField8", "PrintInfoField9", "PrintInfoField10"
			};
			using (var sw = new StreamWriter(Path.Combine(path, fileName)))
			{
				var sb = new StringBuilder();
				foreach (var field in fields)
				{
					sb.Append(field == "PrintInfoField10" ? string.Format("{0}", field) : string.Format("{0}{1}", field, delimiter));
				}

				sw.WriteLine(sb);

				for (var i = 0; i < count; i++)
				{
					sb = new StringBuilder();
					foreach (var field in fields)
					{
						if (field == "Command") sb.Append(string.Format("{0}{1}", 1, delimiter));
						if (field == "SerialNumber") sb.Append(string.Format("{0} {1}{2}", "serial number", i + 1, delimiter));
						if (field == "OrderId") sb.Append(string.Format("{0} {1}{2}", "order", i + 1, delimiter));
						if (field == "Quantity") sb.Append(string.Format("{0}{1}", 1, delimiter));
						if (field == "Name") sb.Append(string.Format("{0} {1}{2}", "name", i + 1, delimiter));
						if (field == "Length") sb.Append(string.Format("{0}{1}", (new Random()).Next(12, 15), delimiter));
						if (field == "Width") sb.Append(string.Format("{0}{1}", (new Random()).Next(10, 12), delimiter));
						if (field == "Height") sb.Append(string.Format("{0}{1}", (new Random()).Next(8, 10), delimiter));
						if (field == "ClassificationNumber") sb.Append(string.Format("{0}{1}", 1, delimiter));
						if (field == "CorrugateQuality") sb.Append(string.Format("{0}{1}", 1, delimiter));
						if (field == "PickZone") sb.Append(string.Format("{0}{1}", "AZ1", delimiter));
						if (field == "PrintInfoField1") sb.Append(string.Format("{0}{1}", "pr1", delimiter));
						if (field == "PrintInfoField2") sb.Append(string.Format("{0}{1}", "pr2", delimiter));
						if (field == "PrintInfoField3") sb.Append(string.Format("{0}{1}", "pr3", delimiter));
						if (field == "PrintInfoField4") sb.Append(string.Format("{0}{1}", "pr4", delimiter));
						if (field == "PrintInfoField5") sb.Append(string.Format("{0}{1}", "pr5", delimiter));
						if (field == "PrintInfoField6") sb.Append(string.Format("{0}{1}", "pr6", delimiter));
						if (field == "PrintInfoField7") sb.Append(string.Format("{0}{1}", "pr7", delimiter));
						if (field == "PrintInfoField8") sb.Append(string.Format("{0}{1}", "pr8", delimiter));
						if (field == "PrintInfoField9") sb.Append(string.Format("{0}{1}", "pr9", delimiter));
						if (field == "PrintInfoField10") sb.Append(string.Format("{0}", "pr10"));
					}

					sw.WriteLine(sb);
				}
			}
		}
	}
}