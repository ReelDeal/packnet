﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Importer.Workflows.ImportPlugins.BoxFirst;
using PackNet.Services.Importer;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Testing.Specificity;

namespace ServicesTests.Importer
{
    using System.Reactive.Linq;

    using PackNet.Business.ProductionGroups;
    using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
    using PackNet.Common.Interfaces.DTO.Classifications;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.ExternalSystems;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.PickZones;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;
    using PackNet.Common.Interfaces.Eventing;
    using PackNet.Common.Interfaces.Services.ExternalSystems;
    using PackNet.Common.Interfaces.Utils;
    using PackNet.Services.ProductionGroup;

    [TestClass]
    public class ImporterWorkflowDispatcherTests
    {
        private ImporterWorkflowDispatcher importerWorkflowDispatcher;
        private IWorkflowImporter importer;
        private EventAggregator aggregator;
        private Mock<IServiceLocator> serviceLocator;

        private Mock<IUserNotificationService> userNotificationService;

        private Mock<IUICommunicationService> uiComService;
        private Mock<ILogger> loggerMock;

        [TestInitialize]
        public void Init()
        {
            importer = new BoxFirstCartonImporter(new Mock<ILogger>().Object);
            aggregator = new EventAggregator();
            serviceLocator = new Mock<IServiceLocator>();

            var articleService = new Mock<IArticleService>();
            articleService.Setup(a => a.Find(It.IsAny<string>())).Returns(new Article());
            serviceLocator.Setup(s => s.Locate<IArticleService>()).Returns(articleService.Object);
            
            var designService = new Mock<IPackagingDesignService>();
            designService.Setup(d => d.GetDesignName(It.IsAny<int>())).Returns("design");
            serviceLocator.Setup(s => s.Locate<IPackagingDesignService>()).Returns(designService.Object);
            
            loggerMock = new Mock<ILogger>();
            loggerMock.Setup(s => s.Log(It.IsAny<LogLevel>(), It.IsAny<string>)).Verifiable();
            serviceLocator.Setup(s => s.Locate<ILogger>()).Returns(loggerMock.Object);

            userNotificationService = new Mock<IUserNotificationService>();
            userNotificationService.Setup(s => s.SendNotificationToSystem(It.IsAny<NotificationSeverity>(), It.IsAny<string>(), It.IsAny<string>())).Verifiable();
            serviceLocator.Setup(s => s.Locate<IUserNotificationService>()).Returns(userNotificationService.Object);

            uiComService = new Mock<IUICommunicationService>();
            uiComService.Setup(s => s.SendMessageToUI(It.IsAny<IMessage>(), It.IsAny<bool>())).Verifiable();
            uiComService.Setup(s => s.SendMessageToUI(It.IsAny<IMessage<IEnumerable<ProductionGroup>>>(), It.IsAny<bool>(), It.IsAny<bool>())).Verifiable();
            uiComService.Setup(s => s.RegisterUIEventWithInternalEventAggregator(It.IsAny<MessageTypes>())).Verifiable();
            
            serviceLocator.Setup(s => s.Locate<IUICommunicationService>()).Returns(uiComService.Object);


            importerWorkflowDispatcher = new ImporterWorkflowDispatcher("ImportPlugins", new List<IWorkflowImporter> { importer }, aggregator, serviceLocator.Object, loggerMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishOnErrorExceptionWhenDispatchCannotFindMatchingImporter()
        {
            // Arrange
            dynamic d = new ExpandoObject();
            d.Length = "10";
            d.Width = "9";
            d.Height = "8";
            //BoxLastImporter is not loaded
            var importables = new Importable { ImportType = ImportTypes.BoxLast, ImportedItems = new List<object> { d } };

            Exception exception = null;
            importerWorkflowDispatcher.StatusObservable.Subscribe(
                _ => { },
                e =>
                {
                    exception = e;
                });

            // Act
            importerWorkflowDispatcher.Dispatch(importables);

            Retry.For(() => exception != null, TimeSpan.FromSeconds(2));

            // Assert
            Specify.That(exception).Should.Not.BeNull();
            loggerMock.Verify(l => l.Log(LogLevel.Error, It.IsAny<string>(), importables.ImportType));
        }

        // Tests fails because the build server doesn't have the %PackNetWorkflows% env variable and that would have to be updated 
        // for every build
#if DEBUG
        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("ImportPlugins\\BoxFirst\\BoxFirstCartonImport.xaml", "ImportPlugins\\BoxFirst")]
        [DeploymentItem("ImportPlugins\\PopulateCarton.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\PopulateLabel.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\CartonEvaluation.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\LabelEvaluation.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\ImportEvaluation.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\CartonPropertyGroup.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\ValidateCarton.xaml", "ImportPlugins")]
        public void ShouldDispatchToWorkflowAndPublishStartAndComplete()
        {
            if(string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PackNetWorkflows")))
                Assert.Inconclusive("The variable %PackNetWorkflows% does not exist");

            dynamic d = new ExpandoObject();
            d.Command = "1";
            d.CustomerUniqueId = "112233";
            d.Quantity = "1";
            d.Name = "Test1";
            d.Length = "15";
            d.Width = "14";
            d.Height = "9";
            d.ClassificationNumber = "1";
            d.CorrugateQuality = "1";
            d.PickZone = "AZ1";
            d.FirstPickZone = "AZ1";
            d.DesignId = "1";
            d.ProductionGroupAlias = "PG1";
            d.PrintInfoField1 = "r1";
            d.PrintInfoField2 = "r2";
            d.PrintInfoField3 = "r3";
            d.PrintInfoField4 = "r4";
            d.PrintInfoField5 = "r5";
            d.PrintInfoField6 = "r6";
            d.PrintInfoField7 = "r7";
            d.PrintInfoField8 = "r8";
            d.PrintInfoField9 = "r9";
            d.PrintInfoField10 = "r10";
            var importables = new Importable { ImportType = ImportTypes.BoxFirst, ImportedItems = new List<object> { d } };

            var classificationServiceMock = new Mock<IClassificationService>();
            var classificationGuid = Guid.NewGuid();
            classificationServiceMock.Setup(s => s.Classifications).Returns(new List<Classification>() { new Classification() { Alias = "C1", Number = "1", Id = classificationGuid } });
            serviceLocator.Setup(s => s.Locate<IClassificationService>()).Returns(classificationServiceMock.Object);

            var pickZoneServiceMock = new Mock<IPickZoneService>();
            var pzGuid = Guid.NewGuid();
            pickZoneServiceMock.Setup(s => s.PickZones).Returns(new List<PickZone>() { new PickZone() { Alias = "AZ1", Id = pzGuid } });
            serviceLocator.Setup(s => s.Locate<IPickZoneService>()).Returns(pickZoneServiceMock.Object);

            var cartonPropertyGroupServiceMock = new Mock<ICartonPropertyGroupService>();
            var cpgGuid = Guid.NewGuid();
            cartonPropertyGroupServiceMock.Setup(s => s.Groups).Returns(new List<CartonPropertyGroup>() { new CartonPropertyGroup() { Id = cpgGuid, Alias = "AZ1" } });
            serviceLocator.Setup(s => s.Locate<ICartonPropertyGroupService>()).Returns(cartonPropertyGroupServiceMock.Object);

            var externalServiceMock = new Mock<IExternalCustomerSystemService>();
            var externalSystemGuid = Guid.NewGuid();
            externalServiceMock.Setup(s => s.Find("External Print Service")).Returns(new ExternalCustomerSystem() { Alias = "External Print Service", Id = externalSystemGuid });
            serviceLocator.Setup(s => s.Locate<IExternalCustomerSystemService>()).Returns(externalServiceMock.Object);

            var packagingDesignService = new Mock<IPackagingDesignService>();
            packagingDesignService.Setup(s => s.HasDesignWithId(1)).Returns(true);
            serviceLocator.Setup(s => s.Locate<IPackagingDesignService>()).Returns(packagingDesignService.Object);

            var pgId = Guid.NewGuid();
            var pg = new ProductionGroup() { Alias = "PG1", Id = pgId };
            var testPgs = new TestProductionGroup();
            testPgs.ProductionGroups = new List<ProductionGroup>() { pg };
            var pgService = new ProductionGroupService(
                testPgs,
                serviceLocator.Object,
                uiComService.Object,
                aggregator,
                aggregator,
                cartonPropertyGroupServiceMock.Object,
                loggerMock.Object);
            //pgService.ExpectedPg = pg;
            serviceLocator.Setup(s => s.Locate<ProductionGroupService>()).Returns(pgService);

            var statuses = new List<string>();
            importerWorkflowDispatcher.StatusObservable.Subscribe(statuses.Add);

            ImportMessage<IEnumerable<IProducible>> result = null;
            aggregator
                .GetEvent<ImportMessage<IEnumerable<IProducible>>>()
                .Subscribe(items => result = items);

            importerWorkflowDispatcher.Dispatch(importables);
            
            Retry.For(() => result != null, TimeSpan.FromSeconds(5));

            Specify.That(result).Should.Not.BeNull();
            var carton = result.Data.First() as ICarton;
            loggerMock.Verify(l => l.Log(LogLevel.Trace, It.Is<string>(s => s.ToLower().Contains("cartonimport"))));
            Specify.That(statuses.Any(s => s.ToLower().Contains("started"))).Should.BeTrue();
            Specify.That(statuses.Any(s => s.ToLower().Contains("completed"))).Should.BeTrue();
        }


        [TestMethod]
        [TestCategory("Integration")]
        [DeploymentItem("ImportPlugins\\BoxFirst\\BoxFirstCartonImport.xaml", "ImportPlugins\\BoxFirst")]
        [DeploymentItem("ImportPlugins\\PopulateCarton.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\PopulateLabel.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\CartonEvaluation.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\LabelEvaluation.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\ImportEvaluation.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\CartonPropertyGroup.xaml", "ImportPlugins")]
        [DeploymentItem("ImportPlugins\\ValidateCarton.xaml", "ImportPlugins")]
        public void ShouldOnlyFailImportables_WithMissingDesignId()
        {
            if (string.IsNullOrEmpty(Environment.GetEnvironmentVariable("PackNetWorkflows")))
                Assert.Inconclusive("The variable %PackNetWorkflows% does not exist");

            #region importables
            dynamic d1 = new ExpandoObject();
            d1.Command = "1";
            d1.CustomerUniqueId = "112266";
            d1.Quantity = "1";
            d1.Name = "Test1";
            d1.Length = "15";
            d1.Width = "14";
            d1.Height = "9";
            d1.ClassificationNumber = "1";
            d1.CorrugateQuality = "1";
            d1.PickZone = "AZ1";
            d1.FirstPickZone = "AZ1";
            d1.DesignId = "666";
            d1.ProductionGroupAlias = "PG1";
            d1.PrintInfoField1 = "r1";
            d1.PrintInfoField2 = "r2";
            d1.PrintInfoField3 = "r3";
            d1.PrintInfoField4 = "r4";
            d1.PrintInfoField5 = "r5";
            d1.PrintInfoField6 = "r6";
            d1.PrintInfoField7 = "r7";
            d1.PrintInfoField8 = "r8";
            d1.PrintInfoField9 = "r9";
            d1.PrintInfoField10 = "r10";

            dynamic d2 = new ExpandoObject();
            d2.Command = "1";
            d2.CustomerUniqueId = "112233";
            d2.Quantity = "1";
            d2.Name = "Test1";
            d2.Length = "15";
            d2.Width = "14";
            d2.Height = "9";
            d2.ClassificationNumber = "1";
            d2.CorrugateQuality = "1";
            d2.PickZone = "AZ1";
            d2.FirstPickZone = "AZ1";
            d2.DesignId = "1";
            d2.ProductionGroupAlias = "PG1";
            d2.PrintInfoField1 = "r1";
            d2.PrintInfoField2 = "r2";
            d2.PrintInfoField3 = "r3";
            d2.PrintInfoField4 = "r4";
            d2.PrintInfoField5 = "r5";
            d2.PrintInfoField6 = "r6";
            d2.PrintInfoField7 = "r7";
            d2.PrintInfoField8 = "r8";
            d2.PrintInfoField9 = "r9";
            d2.PrintInfoField10 = "r10";

            dynamic d3 = new ExpandoObject();
            d3.Command = "1";
            d3.CustomerUniqueId = "112244";
            d3.Quantity = "1";
            d3.Name = "Test1";
            d3.Length = "15";
            d3.Width = "14";
            d3.Height = "9";
            d3.ClassificationNumber = "1";
            d3.CorrugateQuality = "1";
            d3.PickZone = "AZ1";
            d3.FirstPickZone = "AZ1";
            d3.DesignId = "1";
            d3.ProductionGroupAlias = "PG1";
            d3.PrintInfoField1 = "r1";
            d3.PrintInfoField2 = "r2";
            d3.PrintInfoField3 = "r3";
            d3.PrintInfoField4 = "r4";
            d3.PrintInfoField5 = "r5";
            d3.PrintInfoField6 = "r6";
            d3.PrintInfoField7 = "r7";
            d3.PrintInfoField8 = "r8";
            d3.PrintInfoField9 = "r9";
            d3.PrintInfoField10 = "r10";
            #endregion
            var importables = new Importable { ImportType = ImportTypes.BoxFirst, ImportedItems = new List<object> { d1, d2, d3 } };

            var classificationServiceMock = new Mock<IClassificationService>();
            var classificationGuid = Guid.NewGuid();
            classificationServiceMock.Setup(s => s.Classifications).Returns(new List<Classification>() { new Classification() { Alias = "C1", Number = "1", Id = classificationGuid } });
            serviceLocator.Setup(s => s.Locate<IClassificationService>()).Returns(classificationServiceMock.Object);

            var pickZoneServiceMock = new Mock<IPickZoneService>();
            var pzGuid = Guid.NewGuid();
            pickZoneServiceMock.Setup(s => s.PickZones).Returns(new List<PickZone>() { new PickZone() { Alias = "AZ1", Id = pzGuid } });
            serviceLocator.Setup(s => s.Locate<IPickZoneService>()).Returns(pickZoneServiceMock.Object);

            var cartonPropertyGroupServiceMock = new Mock<ICartonPropertyGroupService>();
            var cpgGuid = Guid.NewGuid();
            cartonPropertyGroupServiceMock.Setup(s => s.Groups).Returns(new List<CartonPropertyGroup>() { new CartonPropertyGroup() { Id = cpgGuid, Alias = "AZ1" } });
            serviceLocator.Setup(s => s.Locate<ICartonPropertyGroupService>()).Returns(cartonPropertyGroupServiceMock.Object);

            var externalServiceMock = new Mock<IExternalCustomerSystemService>();
            var externalSystemGuid = Guid.NewGuid();
            externalServiceMock.Setup(s => s.Find("External Print Service")).Returns(new ExternalCustomerSystem() { Alias = "External Print Service", Id = externalSystemGuid });
            serviceLocator.Setup(s => s.Locate<IExternalCustomerSystemService>()).Returns(externalServiceMock.Object);

            var packagingDesignService = new Mock<IPackagingDesignService>();
            packagingDesignService.Setup(s => s.HasDesignWithId(666)).Returns(false);
            packagingDesignService.Setup(s => s.HasDesignWithId(1)).Returns(true);

            serviceLocator.Setup(s => s.Locate<IPackagingDesignService>()).Returns(packagingDesignService.Object);

            var pgId = Guid.NewGuid();
            var pg = new ProductionGroup() { Alias = "PG1", Id = pgId };
            var testPgs = new TestProductionGroup();
            testPgs.ProductionGroups = new List<ProductionGroup>() { pg };
            var pgService = new ProductionGroupService(
                testPgs,
                serviceLocator.Object,
                uiComService.Object,
                aggregator,
                aggregator,
                cartonPropertyGroupServiceMock.Object,
                loggerMock.Object);
            //pgService.ExpectedPg = pg;
            serviceLocator.Setup(s => s.Locate<ProductionGroupService>()).Returns(pgService);

            var statuses = new List<string>();
            importerWorkflowDispatcher.StatusObservable.Subscribe(statuses.Add);

            ImportMessage<IEnumerable<IProducible>> result = null;
            aggregator
                .GetEvent<ImportMessage<IEnumerable<IProducible>>>()
                .Subscribe(items => result = items);

            List<Object> failedImports = null;
            aggregator.GetEvent<IMessage<List<Object>>>()
                .Where(m => m.MessageType == MessageTypes.ImportFailed)
                .Subscribe(
                    m =>
                    {
                        failedImports = m.Data;
                    });

            importerWorkflowDispatcher.Dispatch(importables);



            Retry.For(() => result != null, TimeSpan.FromSeconds(5));

            Specify.That(result).Should.Not.BeNull();
            Specify.That(result.Data.Count()).Should.BeLogicallyEqualTo(2);
            Specify.That(result.Data.Any(p => p.CustomerUniqueId == "112233")).Should.BeTrue();
            Specify.That(result.Data.Any(p => p.CustomerUniqueId == "112244")).Should.BeTrue();
            Specify.That(result.Data.Any(p => p.CustomerUniqueId == "112266")).Should.BeFalse();
            Specify.That(failedImports.Count).Should.BeLogicallyEqualTo(1);
            var failedItem = failedImports.First() as IDictionary<string, Object>;
            Specify.That(failedItem["CustomerUniqueId"]).Should.BeLogicallyEqualTo("112266");
            userNotificationService.Verify(s => s.SendNotificationToSystem(NotificationSeverity.Error, It.Is<string>(str => str.Contains("Design with ID 666 does not exist")), It.IsAny<string>()), Times.Once);
            userNotificationService.Verify(s => s.SendNotificationToSystem(NotificationSeverity.Warning, It.Is<string>(str => str.Contains("Import Failed")), It.IsAny<string>()),Times.Once);
        }

        private class TestProductionGroup : IProductionGroup
        {
            public IEnumerable<ProductionGroup> ProductionGroups { get; set; } 

            public ProductionGroup Create(ProductionGroup productionGroup)
            {
                throw new NotImplementedException();
            }

            public void Delete(ProductionGroup productionGroup)
            {
                throw new NotImplementedException();
            }

            public ProductionGroup Update(ProductionGroup productionGroup)
            {
                throw new NotImplementedException();
            }

            public ProductionGroup Find(Guid productionGroupId)
            {
                throw new NotImplementedException();
            }

            public IEnumerable<ProductionGroup> GetProductionGroups()
            {
                return ProductionGroups;
            }

            public ProductionGroup GetProductionGroupForMachineGroup(Guid machineGroupId)
            {
                return ProductionGroups.FirstOrDefault(a => a.Id == machineGroupId);
            }

            public ProductionGroup FindByCartonPropertyGroupId(Guid cartonPropertyGroupId)
            {
                return null;
            }

            public ProductionGroup FindByCartonPropertyGroupId(IEnumerable<ProductionGroup> productionGroups, Guid cartonPropertyGroupId)
            {
                throw new NotImplementedException();
            }
            public ProductionGroup FindByCartonPropertyGroupAlias(string cartonPropertyGroupAlias)
            {
                return null;
            }

            public ProductionGroup FindByCartonPropertyGroupAlias(IEnumerable<ProductionGroup> productionGroups, string cartonPropertyGroupAlias)
            {
                throw new NotImplementedException();
            }

            public ProductionGroup FindByAlias(string alias)
            {
                return ProductionGroups.FirstOrDefault(a => a.Alias == alias);
            }

            public ConcurrentList<ProductionGroup> GetProductionGroupsForCorrugate(Guid corrugateId)
            {
                throw new NotImplementedException();
            }

            public void RemoveCorrugateFromProductionGroups(Corrugate corrugate)
            {
                throw new NotImplementedException();
            }

            public void RemoveMachineGroupFromProductionGroup(MachineGroup machineGroup)
            {
                throw new NotImplementedException();
            }

            public void UpdateRelatedCartonPropertyGroup(CartonPropertyGroup cartonPropertyGroup)
            {
                throw new NotImplementedException();
            }
        }
#endif
    }
}
