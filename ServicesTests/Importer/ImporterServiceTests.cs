﻿using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Importing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Services.Importer;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reactive.Subjects;

using PackNet.Services;

using Testing.Specificity;

using PackNetServerSettings = PackNet.Common.Interfaces.DTO.Settings.PackNetServerSettings;

namespace ServicesTests.Importer
{
	using PackNet.Common.Interfaces.Enums;

	[TestClass]
	public class ImporterServiceTests
	{
		private ImporterService importer;
		private EventAggregator eventAggregator;
		private Mock<IImporterWorkflowDispatcher> dispatcher;
		private Mock<ILogger> logger;
		private Mock<IPackNetServerSettingsService> packNetServerSettingsService;
		private PackNetServerSettings packNetServerSettings;

		private readonly Subject<string> statusSubject = new Subject<string>();

		[TestInitialize]
		public void Init()
		{
			dispatcher = new Mock<IImporterWorkflowDispatcher>();
			dispatcher.Setup(d => d.StatusObservable).Returns(statusSubject);

			packNetServerSettings = new PackNetServerSettings
			{
				FileImportSettings = new FileImportSettings
				{
					DefaultImportType = ProducibleTypes.Order.ToString()
				}
			};

			packNetServerSettingsService = new Mock<IPackNetServerSettingsService>();
			packNetServerSettingsService.Setup(m => m.GetSettings()).Returns(packNetServerSettings);
			
			logger = new Mock<ILogger>();
			
			eventAggregator = new EventAggregator();

			importer = new ImporterService(eventAggregator, dispatcher.Object, packNetServerSettingsService.Object, logger.Object);
		}

		[TestCleanup]
		[TestCategory("Unit")]
		public void Cleanup()
		{
			importer.Dispose();
		}
		//todo: fix unit test
#if DEBUG
		[TestMethod]
		[TestCategory("Unit")]
		[Conditional("Debug")]
		public void ImportableShouldBeDispatchedBasedOnType()
		{
			// Arrange
			var importable = new ExpandoObject() as IDictionary<string, object>;
			importable.Add(new KeyValuePair<string, object>("Command", "New"));
			importable.Add(new KeyValuePair<string, object>("SerialNumber", "serial number 1"));
			importable.Add(new KeyValuePair<string, object>("Quantity", "1"));
			importable.Add(new KeyValuePair<string, object>("Length", "10"));
			importable.Add(new KeyValuePair<string, object>("Width", "9"));
			importable.Add(new KeyValuePair<string, object>("Height", "8"));
			importable.Add(new KeyValuePair<string, object>("Type", ProducibleTypes.BoxFirstProducible.ToString()));

			var importables = new List<dynamic> { importable };

			// Act
			eventAggregator.Publish(new Message<IEnumerable<dynamic>> { MessageType = MessageTypes.RawDataImported, Data = importables });

			// Assert
			logger.Verify(l => l.Log(LogLevel.Debug, It.IsAny<string>(), It.IsAny<object[]>()), Times.Once);
			dispatcher.Verify(
				d => d.Dispatch(It.Is<Importable>(i => i.ImportType == ProducibleTypes.BoxFirstProducible && i.ImportedItems.Count() == 1)),
				Times.Once);
		}

#endif
		[TestMethod]
		[TestCategory("Unit")]
		public void ShouldImportUsingDefaultTypeFromSettingsWhenTypeNotGiven()
		{
			// Arrange
			var importable = new ExpandoObject() as IDictionary<string, object>;
			importable.Add(new KeyValuePair<string, object>("Command", "New"));
			importable.Add(new KeyValuePair<string, object>("SerialNumber", "serial number 1"));
			importable.Add(new KeyValuePair<string, object>("Quantity", "1"));
			importable.Add(new KeyValuePair<string, object>("Length", "10"));
			importable.Add(new KeyValuePair<string, object>("Width", "9"));
			importable.Add(new KeyValuePair<string, object>("Height", "8"));
			// importable.Add(new KeyValuePair<string, object>("Type", Enums.ProducibleTypes.Carton.ToString())); do not include

			var importables = new List<dynamic> { importable };

			// Act
			eventAggregator.Publish(new Message<IEnumerable<dynamic>> { MessageType = MessageTypes.RawDataImported, Data = importables });

			// Assert
			logger.Verify(
				l =>
					l.Log(LogLevel.Debug, It.Is<string>(s => s.ToLower().Contains("default type")),
						It.Is<object[]>(o => o.Contains(packNetServerSettings.FileImportSettings.DefaultImportType))), Times.Once);
			dispatcher.Verify(
				d => d.Dispatch(It.Is<Importable>(i => i.ImportType == ImportTypes.Order && i.ImportedItems.Count() == 1)),
				Times.Once);
		}
		//todo: fix unit test
#if DEBUG
		[TestMethod]
		[TestCategory("Unit")]
		[Conditional("Debug")]
		public void ShouldAcceptMultipleImportTypesAndGroupByType()
		{
			// Arrange
			var importableCarton1 = new ExpandoObject() as IDictionary<string, object>;
			importableCarton1.Add(new KeyValuePair<string, object>("Command", "New"));
			importableCarton1.Add(new KeyValuePair<string, object>("SerialNumber", "serial number 1"));
			importableCarton1.Add(new KeyValuePair<string, object>("Quantity", "1"));
			importableCarton1.Add(new KeyValuePair<string, object>("Length", "10"));
			importableCarton1.Add(new KeyValuePair<string, object>("Width", "9"));
			importableCarton1.Add(new KeyValuePair<string, object>("Height", "8"));
			importableCarton1.Add(new KeyValuePair<string, object>("Type", ProducibleTypes.BoxFirstProducible.ToString()));

			var importableOrder1 = new ExpandoObject() as IDictionary<string, object>;
			importableOrder1.Add(new KeyValuePair<string, object>("Command", "New"));
			importableOrder1.Add(new KeyValuePair<string, object>("SerialNumber", "serial number 3"));
			importableOrder1.Add(new KeyValuePair<string, object>("OrderId", "order 1"));
			importableOrder1.Add(new KeyValuePair<string, object>("Quantity", "1"));
			importableOrder1.Add(new KeyValuePair<string, object>("Length", "10"));
			importableOrder1.Add(new KeyValuePair<string, object>("Width", "9"));
			importableOrder1.Add(new KeyValuePair<string, object>("Height", "8"));
			importableOrder1.Add(new KeyValuePair<string, object>("Type", ProducibleTypes.Order.ToString()));

			var importableOrder2 = new ExpandoObject() as IDictionary<string, object>;
			importableOrder2.Add(new KeyValuePair<string, object>("Command", "New"));
			importableOrder2.Add(new KeyValuePair<string, object>("SerialNumber", "serial number 4"));
			importableOrder2.Add(new KeyValuePair<string, object>("OrderId", "order 2"));
			importableOrder2.Add(new KeyValuePair<string, object>("Quantity", "1"));
			importableOrder2.Add(new KeyValuePair<string, object>("Length", "10"));
			importableOrder2.Add(new KeyValuePair<string, object>("Width", "9"));
			importableOrder2.Add(new KeyValuePair<string, object>("Height", "8"));
			importableOrder2.Add(new KeyValuePair<string, object>("Type", ProducibleTypes.Order.ToString()));

			var importables = new List<dynamic> { importableCarton1, importableOrder1, importableOrder2 };

			// Act
			eventAggregator.Publish(new Message<IEnumerable<dynamic>>
			{
				MessageType = MessageTypes.RawDataImported,
				Data = importables
			});

			// Assert
			logger.Verify(l => l.Log(LogLevel.Debug, It.IsAny<string>(), It.IsAny<object[]>()), Times.Exactly(2));
			dispatcher.Verify(
				d =>
					d.Dispatch(
						It.Is<Importable>(i => i.ImportType == ProducibleTypes.BoxFirstProducible && i.ImportedItems.Count() == 1)),
				Times.Once);
			dispatcher.Verify(
				d => d.Dispatch(It.Is<Importable>(i => i.ImportType == ProducibleTypes.Order && i.ImportedItems.Count() == 2)),
				Times.Once);
		}
#endif

		[TestMethod]
		[TestCategory("Unit")]
		public void ShouldHandleExceptionByDispatchingDefaultImportType()
		{
			// Arrange
			dynamic importable = "should throw exception";

			var importables = new List<dynamic> { importable };

			// Act
			eventAggregator.Publish(new Message<IEnumerable<dynamic>> { MessageType = MessageTypes.RawDataImported, Data = importables });

			// Assert
			logger.Verify(l => l.Log(LogLevel.Error, It.IsAny<string>(), It.IsAny<object[]>()), Times.Once);
			dispatcher.Verify(
				d => d.Dispatch(It.Is<Importable>(i => i.ImportType == ImportTypes.Order && i.ImportedItems.Count() == 1)),
				Times.Once);
		}

		[TestMethod]
		[TestCategory("Unit")]
		public void NamePropertyReturnsServiceName()
		{
			// Assert
			Specify.That(importer.Name).Should.BeEqualTo("ImporterService");
		}

		[TestMethod]
		[TestCategory("Unit")]
		public void ExceptionsOtherThanWin32ExceptionFromEventAggregatorSubscriberAreIgnored()
		{
			// Act (any exepction except for Win32Exception should be ignored)
			Specify.ThatAction(() =>
				eventAggregator.Publish(new Message<Exception>
				{
					MessageType = MessageTypes.ImportFailed,
					Data = new ApplicationException()
				})).Should.Not.HaveThrown();
		}

		[TestMethod]
		[TestCategory("Unit")]
		[ExpectedException(typeof(ImportFailedException))]
		public void OnErrorExceptionFromDispatcherStatusObservableIsRethrown()
		{
			statusSubject.OnError(new ImportFailedException("The workflow had an unhandled exception"));
		}
	}
}