﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Common.ExtensionMethods;
using PackNet.Common.Interfaces.DTO.ComponentsConfiguration;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.ComponentsConfigurations;

namespace ServicesTests
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using Moq;
    using PackNet.Business.PackagingDesigns;
    using PackNet.Common.Eventing;
    using PackNet.Common.Interfaces.DTO;
    using PackNet.Common.Interfaces.DTO.Carton;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.Machines;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.PhysicalMachine;
    using PackNet.Common.Interfaces.Eventing;
    using PackNet.Common.Interfaces.Logging;
    using PackNet.Common.Interfaces.Machines;
    using PackNet.Common.Interfaces.Services;
    using PackNet.Common.Interfaces.Services.Machines;
    using PackNet.Services.CodeGeneration;
    using Testing.Specificity;
    using Track = PackNet.Common.Interfaces.DTO.PhysicalMachine.Track;
    using PackNet.Common.Interfaces.DTO.Messaging;
    using PackNet.Common.Interfaces.Enums;

    [TestClass]
    [DeploymentItem("TestFiles", "TestFiles")]
    public class CodeGenerationServiceTest
    {
        private IEventAggregatorSubscriber subscriber;
        private IEventAggregatorPublisher publisher;

        private CodeGenerationService service;
        private Mock<IServiceLocator> serviceLocatorMock;
        private Mock<IAggregateMachineService> machineServiceMock;
        private Mock<IPackagingDesignManager> packagingDesignMock;
        private Mock<IComponentsConfigurationRepository> componentsConfigurationMock;
        private Mock<IUserNotificationService> userNotificationServiceMock;

        [TestInitialize]
        public void Setup()
        {
            subscriber = new EventAggregator();
            publisher = subscriber as IEventAggregatorPublisher;

            var loggerMock = new Mock<ILogger>();
            machineServiceMock = new Mock<IAggregateMachineService>();
            serviceLocatorMock = new Mock<IServiceLocator>();
            packagingDesignMock = new Mock<IPackagingDesignManager>();
            userNotificationServiceMock = new Mock<IUserNotificationService>();
            componentsConfigurationMock = new Mock<IComponentsConfigurationRepository>();
            componentsConfigurationMock.Setup(m => m.All()).Returns(new List<ComponentsConfiguration>
            {
                new ComponentsConfiguration()
                {
                    CodeGeneratorSettings = new CodeGeneratorSettings()
                    {
                        MinimumLineDistanceToCorrugateEdge = 10
                    }
                }
            });


            loggerMock.Setup(l => l.Log(It.IsNotNull<LogLevel>(), It.IsAny<string>));


            serviceLocatorMock.Setup(l => l.Locate<IEventAggregatorSubscriber>()).Returns(subscriber);
            serviceLocatorMock.Setup(l => l.Locate<IEventAggregatorPublisher>()).Returns(publisher);
            serviceLocatorMock.Setup(l => l.Locate<ILogger>()).Returns(loggerMock.Object);
            serviceLocatorMock.Setup(l => l.Locate<IAggregateMachineService>()).Returns(machineServiceMock.Object);
            serviceLocatorMock.Setup(l => l.Locate<IUserNotificationService>()).Returns(userNotificationServiceMock.Object);

            service = new CodeGenerationService(serviceLocatorMock.Object, packagingDesignMock.Object);
            Specify.That(service).Should.Not.BeNull();
        }

        [TestCleanup]
        public void TearDown()
        {
            subscriber.Dispose();
            subscriber = null;

            publisher.Dispose();
            publisher = null;
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void CodeGenerationShouldReturnNull_ForUnsupportedMachineTypes()
        {
            var barcodeScanner = new BarcodeScannerMachine
                          {
                              Alias = "My BarcodeScanner",
                              Description = "No description",
                              Id = Guid.NewGuid(),
                              IpOrDnsName = "127.0.0.1",
                              Port = 9090
                          };
            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { barcodeScanner });

            Specify.That(subscriber).Should.Not.BeNull();
            Specify.That(publisher).Should.Not.BeNull();

            var corrugate = new Corrugate
            {
                Alias = "Asd",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = 3,
                Width = 1000
            };
            var cartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate, TileCount = 1 };

            var cartonGuid = Guid.NewGuid();
            var carton = new Carton
            {
                DesignId = 2010002,
                Id = cartonGuid,
                Length = 100d,
                Width = 100d,
                Height = 100d,
                CartonOnCorrugate = cartonOnCorrugate,
                TrackNumber = 1,
                XValues = new Dictionary<string, double>()
            };

            SetupDesignManagerMockToReturnDesignFromFile(carton);

            var instructions = service.GenerateCode(barcodeScanner.Id, carton);

            Specify.That(instructions).Should.BeNull("We shouldn't be able to generate code for a barcode scanner");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldGenerateCodeForValidFusionProducible()
        {
            MicroMeter corrugateWidth = 1000d;
            var fusionMachine = this.SetupFusionMachineWithOneTrack(corrugateWidth, Guid.NewGuid());
            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { fusionMachine });

            Specify.That(subscriber).Should.Not.BeNull();
            Specify.That(publisher).Should.Not.BeNull();

            var corrugate = new Corrugate
            {
                Alias = "Asd",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = 3,
                Width = corrugateWidth
            };
            var cartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate, TileCount = 1 };

            var carton = new Carton
            {
                DesignId = 2010002,
                Id = Guid.NewGuid(),
                Length = 100d,
                Width = 100d,
                Height = 100d,
                CartonOnCorrugate = cartonOnCorrugate,
                TrackNumber = 1,
                XValues = new Dictionary<string, double>()
            };

            SetupDesignManagerMockToReturnDesignFromFile(carton);

            var instructions = service.GenerateCode(fusionMachine.Id, carton);

            Specify.That(instructions).Should.Not.BeNull();
            Specify.That(instructions.Any()).Should.BeTrue("We got an empty instruction list.");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ServiceShouldHandle_ManyRapidlyRequestsInQuickSuccession()
        {
            MicroMeter corrugateWidth = 1000d;
            var f1Id = Guid.NewGuid();
            var f2Id = Guid.NewGuid();

            var e1Id = Guid.NewGuid();
            var e2Id = Guid.NewGuid();

            var fusionMachine1 = SetupFusionMachineWithOneTrack(corrugateWidth, f1Id);
            var fusionMachine2 = SetupFusionMachineWithOneTrack(corrugateWidth, f2Id);
            var emMachine1 = SetupFusionMachineWithOneTrack(corrugateWidth, e1Id);
            var emMachine2 = SetupFusionMachineWithOneTrack(corrugateWidth, e2Id);
            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { fusionMachine1, emMachine1, fusionMachine2, emMachine2 });

            Specify.That(subscriber).Should.Not.BeNull();
            Specify.That(publisher).Should.Not.BeNull();

            var corrugate = new Corrugate
            {
                Alias = "notimportant",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = 3,
                Width = corrugateWidth
            };
            var cartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate, TileCount = 1 };

            var carton = new Carton
            {
                DesignId = 2010002,
                Id = Guid.NewGuid(),
                Length = 100d,
                Width = 100d,
                Height = 100d,
                CartonOnCorrugate = cartonOnCorrugate,
                TrackNumber = 1,
                XValues = new Dictionary<string, double>()
            };

            SetupDesignManagerMockToReturnDesignFromFile(carton);

            var sw = new Stopwatch();

            const int nrOfRequests = 8000;
            sw.Start();
            for (int i = 0; i < (nrOfRequests / 4); i++)
            {
                IEnumerable<IInstructionItem> instructions = null;
                instructions = service.GenerateCode(fusionMachine1.Id, carton);
                Specify.That(instructions).Should.Not.BeNull();
                Specify.That(instructions.Any()).Should.BeTrue();
                instructions = service.GenerateCode(emMachine1.Id, carton);
                Specify.That(instructions).Should.Not.BeNull();
                Specify.That(instructions.Any()).Should.BeTrue();
                instructions = service.GenerateCode(fusionMachine2.Id, carton);
                Specify.That(instructions).Should.Not.BeNull();
                Specify.That(instructions.Any()).Should.BeTrue();
                instructions = service.GenerateCode(emMachine2.Id, carton);
                Specify.That(instructions).Should.Not.BeNull();
                Specify.That(instructions.Any()).Should.BeTrue();
            }

            sw.Stop();
            Console.WriteLine("Elapsed time: {0}s", sw.ElapsedMilliseconds / 1000d);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldGenerateCodeForValidEmProducible()
        {
            MicroMeter corrugateWidth = 1000d;
            var emMachine = this.SetupEmMachineWithOneTrack(corrugateWidth, Guid.NewGuid());
            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { emMachine });

            Specify.That(subscriber).Should.Not.BeNull();
            Specify.That(publisher).Should.Not.BeNull();

            var corrugate = new Corrugate
            {
                Alias = "notimportant",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = 3,
                Width = corrugateWidth
            };

            var cartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate, TileCount = 1 };

            var carton = new Carton
            {
                DesignId = 2010002,
                Id = Guid.NewGuid(),
                Length = 200d,
                Width = 200d,
                Height = 200d,
                CartonOnCorrugate = cartonOnCorrugate,
                TrackNumber = 1,
                XValues = new Dictionary<string, double>()
            };

            SetupDesignManagerMockToReturnDesignFromFile(carton);

            var instructions = service.GenerateCode(emMachine.Id, carton);

            Specify.That(instructions).Should.Not.BeNull("We expected an instruction list for this producible but didn't get one");
            Specify.That(instructions.Any()).Should.BeTrue("We got an empty instruction list.");
        }

        [Ignore]
        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldResetCodegenerator_WhenStartupProcedureDoneIsTriggered()
        {
            MicroMeter corrugateWidth = 1000d;
            var emMachine = this.SetupEmMachineWithOneTrack(corrugateWidth, Guid.NewGuid());

            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { emMachine });
            
            Specify.That(publisher).Should.Not.BeNull();

            var corrugate = new Corrugate
            {
                Alias = "notimportant",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = 3,
                Width = corrugateWidth
            };

            var cartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate, TileCount = 1, CartonWidthOnCorrugate = corrugateWidth * 2, CartonLengthOnCorrugate = corrugateWidth * 2 };
            var carton = new Carton
            {
                DesignId = 10201,
                Id = Guid.NewGuid(),
                Length = 2d,
                Width = 2d,
                Height = 2d,
                CartonOnCorrugate = cartonOnCorrugate,
                TrackNumber = 1,
                XValues = new Dictionary<string, double>()
            };

            SetupDesignManagerMockToReturnDesignFromFile(carton);

            var instructions = service.GenerateCode(emMachine.Id, carton);

            var chOTFInstruction = instructions.OfType<InstructionCrossHeadOnTheFlyItem>().Last();

            Specify.That(chOTFInstruction.FinalPosition).Should.BeLogicallyEqualTo(2000d, "Should move the CH from position 0 to position 1000 in first pass");

            instructions = service.GenerateCode(emMachine.Id, carton);

            var chMoveInstruction = instructions.OfType<InstructionCrossHeadMovementItem>().Last();

            Specify.That(chMoveInstruction.Position).Should.BeLogicallyEqualTo(1000d, "Should move the CH to position 0 from position 1000 in second pass");

            // fake machine stop and crosshead reposition after startupprocedure 
            (emMachine.PhysicalMachineSettings as EmPhysicalMachineSettings).CrossHeadParameters.Position = 2000;

            publisher.Publish(new Message<IMachine>
            {
                MessageType = MachineMessages.StartupProcedureCompleted,
                Data = emMachine
            });

            instructions = service.GenerateCode(emMachine.Id, carton);

            chMoveInstruction = instructions.OfType<InstructionCrossHeadMovementItem>().Last();

            Specify.That(chMoveInstruction.Position).Should.BeLogicallyEqualTo(1000d);
        }

        [TestMethod]
        public void ShouldReturnNull_IfCodeCouldNotBeGenerated()
        {
            MicroMeter corrugateWidth = 100d;
            var fusionMachine = SetupFusionMachineWithOneTrack(corrugateWidth, Guid.NewGuid());
            var emMachine = SetupEmMachineWithOneTrack(corrugateWidth, Guid.NewGuid());
            machineServiceMock.Setup(m => m.Machines).Returns(new List<IMachine> { fusionMachine, emMachine });

            Specify.That(subscriber).Should.Not.BeNull();
            Specify.That(publisher).Should.Not.BeNull();

            var corrugate = new Corrugate
            {
                Alias = "notimportant",
                Id = Guid.NewGuid(),
                Quality = 1,
                Thickness = 3,
                Width = corrugateWidth
            };
            var cartonOnCorrugate = new CartonOnCorrugate { Corrugate = corrugate, TileCount = 1, CartonWidthOnCorrugate = corrugateWidth * 2, CartonLengthOnCorrugate = corrugateWidth * 2 };
            var carton = new Carton
            {
                DesignId = 2010002,
                Id = Guid.NewGuid(),
                Length = 2d,
                Width = 2d,
                Height = 2d,
                CartonOnCorrugate = cartonOnCorrugate,
                TrackNumber = 1,
                XValues = new Dictionary<string, double>()
            };

            SetupDesignManagerMockToReturnDesignFromFile(carton);

            var instructions = service.GenerateCode(fusionMachine.Id, carton);

            Specify.That(instructions).Should.BeNull("Got an instruction list for a job we shouldn't be able to generate gode for.");
        }

        private FusionMachine SetupFusionMachineWithOneTrack(MicroMeter corrugateWidth, Guid machineId)
        {

            var fusionMachine = new FusionMachine
            {
                Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
                {
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        TrackNumber = 1,
                        LoadedCorrugate = new Corrugate() { Width = corrugateWidth },
                        TrackOffset = corrugateWidth
                    }
                },
                PhysicalMachineSettings =
                    new FusionPhysicalMachineSettings
                    {
                        CrossHeadParameters = new FusionCrossHeadParameters() { Position = 200d },
                        CodeGeneratorSettings = new CodeGeneratorSettings() { MinimumLineDistanceToCorrugateEdge = 5d, MinimumLineLength = 2d, PerforationSafetyDistance = 40d }
                    }
            };

            #region FusionLongHeadParameters
            var fusionLongHeadParameters = new FusionLongHeadParameters();

            foreach (var lh in GetFusionLongheads())
            {
                fusionLongHeadParameters.AddLongHead(lh);
            }

            fusionLongHeadParameters.MinimumPosition = 2.54;
            fusionLongHeadParameters.MinimumPosition = 2.54;
            fusionLongHeadParameters.MaximumPosition = 1358;
            fusionLongHeadParameters.ConnectionDelay = 150;
            fusionLongHeadParameters.LongheadWidth = 64;
            fusionLongHeadParameters.LeftSideToSensorPin = 48;
            fusionLongHeadParameters.PositioningSpeed = 100;
            fusionLongHeadParameters.PositioningAcceleration = 100;

            (fusionMachine.PhysicalMachineSettings as FusionPhysicalMachineSettings).LongHeadParameters = fusionLongHeadParameters;
            #endregion

            #region TracksParameters

            (fusionMachine.PhysicalMachineSettings as FusionPhysicalMachineSettings).TrackParameters = new FusionTracksParameters(); { };
            var track = new Track()
                        {
                            Number = 1,
                            LeftSensorPlateToCorrugate = 3d,
                            OutOfCorrugatePosition = 30.25d,
                            RightSensorPlateToCorrugate = -3d,
                            RightSideFixed = true,
                        };

            (fusionMachine.PhysicalMachineSettings as FusionPhysicalMachineSettings).TrackParameters.AddTrack(track);
            #endregion

            fusionMachine.Id = machineId;
            return fusionMachine;
        }

        private EmMachine SetupEmMachineWithOneTrack(MicroMeter corrugateWidth, Guid machineId)
        {
            var emMachine = new EmMachine
            {
                PhysicalMachineSettings = new EmPhysicalMachineSettings
                {
                    CrossHeadParameters = new EmCrossHeadParameters()
                    {
                        Position = 200d,
                        MovementData = new EmNormalMovementData()
                        {
                            Acceleration = 10000d,
                            Speed = 3600d,
                            NormalMovement = new EmMovementData()
                            {
                                AccelerationInPercent = 100,
                                SpeedInPercent = 100,
                                Torque = 100
                            }
                        },
                        ConnectDelayOff = 50,
                        ConnectDelayOn = 50,
                        CreaseDelayOff = 50,
                        CreaseDelayOn = 50,
                        CutDelayOff = 50,
                        CutDelayOn = 50
                    },
                    CodeGeneratorSettings = new CodeGeneratorSettings() { MinimumLineDistanceToCorrugateEdge = 5d, MinimumLineLength = 2d, PerforationSafetyDistance = 40d },
                    WasteAreaParameters = new WasteAreaParameters() { Enable = false }
                },
                Tracks = new ConcurrentList<PackNet.Common.Interfaces.DTO.Machines.Track>()
                {
                    new PackNet.Common.Interfaces.DTO.Machines.Track()
                    {
                        LoadedCorrugate = new Corrugate(){Width = corrugateWidth},
                        TrackNumber = 1,
                        TrackOffset = corrugateWidth
                    }
                }
            };

            #region FusionLongHeadParameters

            (emMachine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters = new EmLongHeadParameters(GetEmLongheads(), 1500, 90, 10, 55, 7, 2464, -50, 54, 143, 15d, 24d);
            #endregion
      
            var feedRollerParameters = new EmFeedRollerParameters(1500, 30, 1, 55, 600, 45);

            (emMachine.PhysicalMachineSettings as EmPhysicalMachineSettings).FeedRollerParameters = feedRollerParameters;
            emMachine.Id = machineId;

            return emMachine;
        }

        private static IEnumerable<EmLongHead> GetEmLongheads()
        {
            return new List<EmLongHead> {
                       new EmLongHead {
                           LeftSideToTool = 47,
                           Number = 1,
                           Position = 100,
                       },
                       new EmLongHead {
                           LeftSideToTool = 47,
                           Number = 2,
                           Position = 200,
                       },
                       new EmLongHead {
                           LeftSideToTool = 7,
                           Number = 3,
                           Position = 300,
                       },
                       new EmLongHead {
                           LeftSideToTool = 47,
                           Number = 4,
                           Position = 400,
                       },
                       new EmLongHead {
                           LeftSideToTool = 7,
                           Number = 5,
                           Position = 500,
                       },
                       new EmLongHead {
                           LeftSideToTool = 7,
                           Number = 6,
                           Position = 600,
                       },
                       new EmLongHead {
                           LeftSideToTool = 47,
                           Number = 7,
                           Position = 700,
                       },
                   };
        }

        private static IEnumerable<FusionLongHead> GetFusionLongheads()
        {
            return new List<FusionLongHead> {
                       new FusionLongHead {
                           LeftSideToTool = 56.8d,
                           Number = 1,
                           Position = 100,
                           Type = ToolType.Cut
                       },
                       new FusionLongHead {
                           LeftSideToTool = 40.5d,
                           Number = 2,
                           Position = 200,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = 40.5d,
                           Number = 3,
                           Position = 300,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = 41.2d,
                           Number = 4,
                           Position = 400,
                           Type = ToolType.Cut
                       },
                       new FusionLongHead {
                           LeftSideToTool = 40.5d,
                           Number = 5,
                           Position = 500,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = 23.5d,
                           Number = 6,
                           Position = 600,
                           Type = ToolType.Crease
                       },
                       new FusionLongHead {
                           LeftSideToTool = 7.2d,
                           Number = 7,
                           Position = 700,
                           Type = ToolType.Cut
                       },
                   };
        }

        private void SetupDesignManagerMockToReturnDesignFromFile(ICarton carton)
        {
            var testDataPath = Path.Combine(Environment.CurrentDirectory, "TestFiles");

            var designManager =
                new PackagingDesignManager(testDataPath, new Mock<ICachingService>().Object,
                    new Mock<ILogger>().Object);

            designManager.LoadDesigns();
            var design =
                designManager.GetDesigns()
                    .FirstOrDefault(d => d.Id == carton.DesignId);

            var physicalDesign = design.AsPhysicalDesign(carton.GetDesignFormulaParameters(carton.CartonOnCorrugate.Corrugate));

            packagingDesignMock.Setup(
                dm => dm.GetPhyscialDesignForCarton(carton, carton.CartonOnCorrugate.Corrugate, carton.CartonOnCorrugate.Rotated ? OrientationEnum.Degree90 : OrientationEnum.Degree0))
                .Returns<ICarton, Corrugate, OrientationEnum>((a, b, c) => design.AsPhysicalDesign(carton.GetDesignFormulaParameters(carton.CartonOnCorrugate.Corrugate)));

            packagingDesignMock.Setup(
                dm => dm.GetPhyscialDesignForCarton(carton, carton.CartonOnCorrugate.Corrugate, carton.CartonOnCorrugate.Rotated ? OrientationEnum.Degree90 : OrientationEnum.Degree0, 1))
                .Returns<ICarton, Corrugate, OrientationEnum, int>((a, b, c, d) => design.AsPhysicalDesign(carton.GetDesignFormulaParameters(carton.CartonOnCorrugate.Corrugate), 1));

            packagingDesignMock.Setup(
                dm => dm.CalculateUsage(carton, carton.CartonOnCorrugate.Corrugate, 1, carton.CartonOnCorrugate.Rotated))
                .Returns<ICarton, Corrugate, int, bool>((a, b, c, d) => new PackagingDesignCalculation { Length = physicalDesign.Length, Width = physicalDesign.Width });
        }
    }
}
